Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_acccnDebitNote
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim BSU_IsTAXEnabled As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then

            'TAX CODE
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = Session("sBSUID")
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
            ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
            If BSU_IsTAXEnabled = True Then
                trTaxType.Visible = True
                LOAD_TAX_CODES()
            End If
            'TAX CODE

            Session("CHECKLAST") = 0
            usrCostCenter1.TotalAmountControlName = "txtDAmount"
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            initialize_components()
            '''''check menu rights
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            'txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150001"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            '40=DN
            Session("gintGridLine") = 1
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A150040" And ViewState("MainMnu_code") <> "A150045" And ViewState("MainMnu_code") <> "A200020" And ViewState("MainMnu_code") <> "A200025") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("str_doctype") = "DN"
                lblHead.Text = "Debit Note"
                If ViewState("MainMnu_code") = "A150045" Or ViewState("MainMnu_code") = "A200025" Then
                    ViewState("str_doctype") = "CN"
                    lblHead.Text = "Credit Note"
                End If
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                'btnDelete.Visible = True
                btnEditCancel.Visible = True
                'btnEdit.Visible = False
                'btnEdit.Visible = True
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()

            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = getErrorMessage("Invalid Editid")
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                'btnDelete.Visible = False
                btnEditCancel.Visible = False
                getnextdocid()
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        Else
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If

    End Sub
    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub

    Sub initialize_components()
        txtdocDate.Text = GetDiplayDate()
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        txtHamount.Attributes.Add("readonly", "readonly")
        txtDAccountName.Attributes.Add("readonly", "readonly")
        txtHAccountname.Attributes.Add("readonly", "readonly")
        txtHExchRate.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        txtDTotalamount.Attributes.Add("readonly", "readonly")
        ViewState("str_timestamp") = New Byte()
        Session("dtJournal") = DataTables.CreateDataTable_DN()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        Session("idCostChild") = 0
        ViewState("idJournal") = 0
        btnEditCancel.Visible = False
        btnAdddetails.Visible = True
        btnUpdate.Visible = False
    End Sub


    Private Sub setViewData() 'SET CONTROLS IN THE VIEW MODE
        btnEditCancel.Visible = False
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")

        gvJournal.Columns(7).Visible = False
        gvJournal.Columns(5).Visible = False
        If BSU_IsTAXEnabled Then
            gvJournal.Columns(6).Visible = True
        Else
            gvJournal.Columns(6).Visible = False
        End If

        imgCalendar.Enabled = False
        btnHaccount.Enabled = False
        txtdocDate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        txtDNarration.Attributes.Add("readonly", "readonly")
        txtHOldrefno.Attributes.Add("readonly", "readonly")
        DDCurrency.Enabled = False
    End Sub


    Private Sub ResetViewData() 'ST CONTROLS IN ADD/EDIT MODE
        If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
            btnEditCancel.Visible = False
        Else
            btnEditCancel.Visible = True
        End If
        'tbl_Details.Visible = True
        tbl_Details.Attributes.Add("style", "display:table")
        gvJournal.Columns(5).Visible = True
        gvJournal.Columns(7).Visible = True

        If BSU_IsTAXEnabled = True Then
            gvJournal.Columns(6).Visible = True
        Else
            gvJournal.Columns(6).Visible = False
        End If
        imgCalendar.Enabled = True
        btnHaccount.Enabled = True
        txtdocDate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        txtDNarration.Attributes.Remove("readonly")
        txtHOldrefno.Attributes.Remove("readonly")
        DDCurrency.Enabled = True
    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try 'SET THE HEADER DETAILS
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM VOUCHER_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtdocDate.Text = Format(CDate(ds.Tables(0).Rows(0)("VHH_DOCDT")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                txtHDocno.Text = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                txtHOldrefno.Text = ds.Tables(0).Rows(0)("VHH_REFNO") & ""
                txtHAccountcode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID") & ""
                txtHAccountname.Text = AccountFunctions.check_accountid(ds.Tables(0).Rows(0)("VHH_ACT_ID"), Session("sBsuid"))
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Try 'SET DETAIL TABLE IN VIEW/EDIT MODE
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim dtDTL As DataTable = CostCenterFunctions.ViewVoucherDetails(Session("Sub_ID"), Session("sBsuid"), _
                            ViewState("str_doctype"), ViewState("str_editData").Split("|")(0))

            If dtDTL.Rows.Count > 0 Then
                For iIndex As Integer = 0 To dtDTL.Rows.Count - 1
                    Dim rDt As DataRow
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = dtDTL.Rows(iIndex)("GUID")
                    rDt("Id") = dtDTL.Rows(iIndex)("VHD_LINEID")
                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = dtDTL.Rows(iIndex)("VHD_ACT_ID")
                    rDt("Accountname") = dtDTL.Rows(iIndex)("ACT_NAME")
                    rDt("Narration") = dtDTL.Rows(iIndex)("VHD_NARRATION")
                    rDt("Amount") = dtDTL.Rows(iIndex)("VHD_AMOUNT")
                    rDt("Status") = "Normal"
                    If BSU_IsTAXEnabled Then
                        rDt("TaxCode") = dtDTL.Rows(iIndex)("TaxCode")
                    Else
                        rDt("TaxCode") = ""
                    End If

                    Session("dtJournal").Rows.Add(rDt)
                Next
                Dim ds2 As DataSet
                str_Sql = "SELECT MAx(VHD_LINEID) as Id,Max(VHD_CHQID) as CHQID FROM VOUCHER_D A " _
                          & " WHERE A.VHD_SUB_ID='" & Session("Sub_ID") & "'  AND VHD_BSU_ID='" & Session("sBsuid") & "'   AND A.VHD_DOCTYPE='" & ViewState("str_doctype") & "' AND A.VHD_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'  "
                ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Session("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1
                h_NextLine.Value = Session("gintGridLine")
                '   ----  Initalize the Cost Center Grid
                Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(Session("Sub_ID"), Session("sBsuid"), _
                                         ViewState("str_doctype"), ViewState("str_editData").Split("|")(0))

                Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(Session("Sub_ID"), Session("sBsuid"), _
                ViewState("str_doctype"), ViewState("str_editData").Split("|")(0))
            End If
            ViewState("idJournal") = ViewState("idJournal") + 1
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyCost(ByVal p_Modifyid As String)
        Try 'SET COSTCHILD TABLE IN VIEW/EDIT MODE

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT GUID, VDS_DOCTYPE ,VDS_DOCNO ," _
            & "VDS_ACT_ID ,VDS_CCS_ID ,VDS_CODE ,VDS_AMOUNT,VDS_CSS_CSS_ID," _
            & "VDS_SLNO,VDS_CCS_ID FROM VOUCHER_D_S WHERE VDS_DOCNO='" _
             & ViewState("str_editData").Split("|")(0) & "' and VDS_BDELETED='False' " _
            & " AND (VDS_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (VDS_SUB_ID = '" & Session("Sub_ID") & "') AND VDS_Auto IS NULL "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")

                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("VDS_CODE")
                    If ds.Tables(0).Rows(iIndex)("VDS_SLNO") = 0 Then
                        rDt("VoucherId") = "hd"
                    Else
                        rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("VDS_SLNO")
                    End If

                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("VDS_CCS_ID")
                    rDt("Name") = ""
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VDS_AMOUNT")
                    rDt("SubMemberid") = ds.Tables(0).Rows(iIndex)("VDS_CSS_CSS_ID")


                    'btnAdddetails.Visible = False

                    rDt("Status") = "Normal"
                    'idCostChild = idCostChild + 1
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub getnextdocid() 'GET NEXT DOC ID
        If ViewState("datamode") = "add" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId(ViewState("str_doctype"), Session("sBsuid"), CType(txtdocDate.Text, Date).Month, CType(txtdocDate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub


    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub



    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next

            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function


    Protected Sub txtdocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
    End Sub


    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdddetails.Click
        txtDAccountName.Text = AccountFunctions.check_accountid(Detail_ACT_ID.Text & "", Session("sBsuid"))
        '''''FIND ACCOUNT IS THERE
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
          & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        Try
            Dim rDt As DataRow

            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = CDbl(txtDAmount.Text.Trim)
            If txtDAccountName.Text = "" Then
                lblError.Text = getErrorMessage("303") ' account already there
                Exit Sub
            End If
            If dCrorDb > 0 Then
                rDt = Session("dtJournal").NewRow
                rDt("Id") = Session("gintGridLine") 'ViewState("idJournal")
                'ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = Detail_ACT_ID.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                rDt("Amount") = dCrorDb
                rDt("GUID") = System.DBNull.Value
                If BSU_IsTAXEnabled Then
                    rDt("TaxCode") = ddlVATCode.SelectedValue
                Else
                    rDt("TaxCode") = ""
                End If
                'rDt("Cashflow") = txtDCashflowcode.Text.Trim
                'rDt("Cashflowname") = txtDCashflowname.Text.Trim
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtJournal").Rows(i)("Amount") = rDt("Amount") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                    CostCenterFunctions.AddCostCenter(Session("gintGridLine"), Session("sBsuid"), _
                                      Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                     Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                     Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                End If
                Session("gintGridLine") = Session("gintGridLine") + 1
                h_NextLine.Value = Session("gintGridLine")
                Session("dtJournal").Rows.Add(rDt)
                gridbind()
                Clear_Details()
            Else
                lblError.Text = getErrorMessage("510")
            End If

        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try

    End Sub
    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub

    Private Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_DN()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        dCredit = dCredit + Session("dtJournal").Rows(i)("Amount")
                        ' dTotAmount = dTotAmount + dtCostChild.Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtDTotalamount.Text = AccountFunctions.Round(dCredit)
            txtHamount.Text = AccountFunctions.Round(dCredit)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try


    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblReqd As New Label
            Dim lblid As New Label
            Dim lblAmount As New Label
            Dim btnAlloca As New LinkButton

            lblReqd = e.Row.FindControl("lblRequired")
            lblid = e.Row.FindControl("lblId")
            lblAmount = e.Row.FindControl("lblAmount")
            btnAlloca = e.Row.FindControl("btnAlloca")
            Dim gvCostchild As New GridView
            gvCostchild = e.Row.FindControl("gvCostchild")
            If gvCostchild IsNot Nothing Then
                gvCostchild.Attributes.Add("bordercolor", "#fc7f03")
                'ClientScript.RegisterStartupScript([GetType](), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" & lblid.Text & "','one');</script>")
                If Not Session("dtCostChild") Is Nothing Then
                    Dim dv As New DataView(Session("dtCostChild"))
                    dv.RowFilter = "VoucherId='" & lblid.Text & "' "
                    dv.Sort = "MemberId"
                    gvCostchild.DataSource = dv.ToTable
                    gvCostchild.DataBind()
                End If
            End If

            If btnAlloca IsNot Nothing Then
                Dim dAmt As Double
                If CDbl(lblAmount.Text) > 0 Then
                    dAmt = CDbl(lblAmount.Text)
                End If
                btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.Text) & "');return false;"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtJournal").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(i)("id") & "" = p_id Then
                    Return Session("dtJournal").Rows(i)("Costcenter")
                End If
            Next
        End If
        Return ""
    End Function


    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdddetails.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                '        Dim lblGrpCode As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        If ViewState("datamode") <> "edit" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        'session("dtCostChild").Rows(iRemove).Delete()
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        Session("gintEditLine") = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                Detail_ACT_ID.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Amount"))
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Trim(Session("dtJournal").Rows(iIndex)("TaxCode"))
                End If
                'txtDCashflowcode.Text =  Session("dtJournal").Rows(iIndex)("Cashflow")
                'txtDCashflowname.Text =  Session("dtJournal").Rows(iIndex)("Cashflowname")
                btnAdddetails.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                gvJournal.SelectedIndex = iIndex
                ClearRadGridandCombo()
                RecreateSsssionDataSource()
                h_NextLine.Value = Session("gintEditLine")
                CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()

                Exit For
            End If
        Next
    End Sub
    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            h_editorview.Value = ""
            If ViewState("datamode") = "edit" Then
                unlock()
            End If
            setViewData()
            'clear_All()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = CDbl(txtDAmount.Text)
            '''''
            If ViewState("datamode") = "view" Then
                lblError.Text = "Record not in edit mode !!!"
                Exit Sub
            End If
            RecreateSsssionDataSource()
            If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                                 txtDAmount.Text) Then
                lblError.Text = "Invalid Cost Center Allocation!!!"
                Exit Sub
            End If
            txtDAccountName.Text = AccountFunctions.check_accountid(Detail_ACT_ID.Text & "", Session("sBsuid"))
            '''''FIND ACCOUNT IS THERE
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                str_Sql = "SELECT ACT_ID,ACT_NAME " _
                & " FROM ACCOUNTS_M AM WHERE" _
                & " ACT_Bctrlac='FALSE'" _
                & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
              & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                '& " order by gm.GPM_DESCR "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                Else
                    txtDAccountName.Text = ""
                    lblError.Text = getErrorMessage("303")
                    Exit Sub
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try

            '''''
            str_Search = h_Editid.Value
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If (Session("dtJournal").Rows(iIndex)("Accountid") <> Detail_ACT_ID.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While
                        End If

                        ''updation handle here
                    End If
                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("gintEditLine"), Session("sBsuid"), _
                                          Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If

                    Session("dtJournal").Rows(iIndex)("Accountid") = Detail_ACT_ID.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = AccountFunctions.check_accountid(Detail_ACT_ID.Text.Trim, Session("sBsuid"))
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Amount") = dCrordb
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim

                    If BSU_IsTAXEnabled Then
                        Session("dtJournal").Rows(iIndex)("TaxCode") = ddlVATCode.SelectedValue
                    Else
                        Session("dtJournal").Rows(iIndex)("TaxCode") = ""
                    End If

                    btnAdddetails.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message, "UPDATE")
        End Try
    End Sub


    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        Session("dtCostChild").Rows.Clear()
        gridbind()
        Clear_Details()
        txtDNarration.Text = ""
        txtHAccountcode.Text = ""
        txtHAccountname.Text = ""
        txtHOldrefno.Text = ""
        getnextdocid()
        txtHNarration.Text = ""
        Session("gintGridLine") = 1
    End Sub


    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        Detail_ACT_ID.Text = ""
        ClearRadGridandCombo()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            txtHAccountname.Text = AccountFunctions.Validate_Account(txtHAccountcode.Text, Session("sbsuid"), "CUSTSUPPnIJV")
            If txtHAccountname.Text = "" Then
                lblError.Text = "Please select Account / Invalid Party selected"
                txtHAccountcode.Focus()
                Exit Sub
            Else
                lblError.Text = ""
            End If
            Dim strfDate As String = txtdocDate.Text.Trim

            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtdocDate.Text = strfDate
            End If
            If Session("dtJournal").Rows.Count = 0 Then
                lblError.Text = getErrorMessage(523)
                Exit Sub
            Else
                gridbind()
                Dim j As String = check_Errors_head()
                Dim s As String = j & check_Errors()
                If s <> "" Then
                    lblError.Text = s
                    Exit Sub
                End If
            End If

            'Checking wether credit and debit accounts are same 
            For Each gvr As GridViewRow In Me.gvJournal.Rows
                If Me.txtHAccountcode.Text.Trim() = gvr.Cells(2).Text.Trim() Then
                    lblError.Text = "Debit and Credit accounts cannot be same"
                    Exit Sub
                End If
            Next
            '------------------------------------------------------


            ViewState("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                '@GUID = NULL,
                '@VHH_SUB_ID = N'007',
                '@VHH_BSU_ID = N'125016',
                '@VHH_FYEAR = 2007,
                '@VHH_DOCTYPE = N'CP',
                '@VHH_DOCNO = N'1',
                '@VHH_TYPE = N'P',
                '@VHH_CHB_ID = NULL,                
                '@VHH_DOCDT = N'20 APR 2007',

                'Adding header info
                Dim cmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpsqlpVHH_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpVHH_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpVHH_BSU_ID)

                Dim VHH_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                VHH_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(VHH_FYEAR)

                Dim sqlpVHH_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpVHH_DOCTYPE.Value = ViewState("str_doctype")
                cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                Dim sqlpVHH_CHB_ID As New SqlParameter("@VHH_CHB_ID", SqlDbType.Int)
                sqlpVHH_CHB_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CHB_ID)

                Dim sqlpVHH_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpVHH_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpVHH_DOCNO.Value = "1"
                End If
                cmd.Parameters.Add(sqlpVHH_DOCNO)

                Dim sqlpVHH_TYPE As New SqlParameter("@VHH_TYPE", SqlDbType.VarChar, 20)
                If ViewState("str_doctype") = "CN" Then
                    sqlpVHH_TYPE.Value = "C"
                Else
                    sqlpVHH_TYPE.Value = "D"
                End If

                cmd.Parameters.Add(sqlpVHH_TYPE)


                Dim sqlpVHH_DOCDT As New SqlParameter("@VHH_DOCDT", SqlDbType.DateTime, 30)
                sqlpVHH_DOCDT.Value = txtdocDate.Text & ""
                cmd.Parameters.Add(sqlpVHH_DOCDT)

                '@VHH_CHQDT = NULL,
                '@VHH_ACT_ID = N'24301002',
                '@VHH_NOOFINST = 0,
                '@VHH_MONTHINTERVEL = 0,
                '@VHH_PARTY_ACT_ID = NULL,
                '@VHH_INSTAMT = NULL,
                '@VHH_INTPERCT = NULL,


                Dim sqlpVHH_CHQDT As New SqlParameter("@VHH_CHQDT", SqlDbType.VarChar, 100)
                sqlpVHH_CHQDT.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CHQDT)

                Dim sqlpVHH_ACT_ID As New SqlParameter("@VHH_ACT_ID", SqlDbType.VarChar, 20)
                sqlpVHH_ACT_ID.Value = txtHAccountcode.Text & ""
                cmd.Parameters.Add(sqlpVHH_ACT_ID)

                Dim sqlpVHH_NOOFINST As New SqlParameter("@VHH_NOOFINST", SqlDbType.Int)
                sqlpVHH_NOOFINST.Value = 0
                cmd.Parameters.Add(sqlpVHH_NOOFINST)

                Dim sqlpVHH_MONTHINTERVEL As New SqlParameter("@VHH_MONTHINTERVEL", SqlDbType.Int, 8)
                sqlpVHH_MONTHINTERVEL.Value = 0
                cmd.Parameters.Add(sqlpVHH_MONTHINTERVEL)

                Dim sqlpVHH_PARTY_ACT_ID As New SqlParameter("@VHH_PARTY_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_PARTY_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_PARTY_ACT_ID)

                Dim sqlpVHH_INSTAMT As New SqlParameter("@VHH_INSTAMT", SqlDbType.Decimal, 20)
                sqlpVHH_INSTAMT.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INSTAMT)

                Dim sqlpVHH_INTPERCT As New SqlParameter("@VHH_INTPERCT", SqlDbType.Decimal, 20)
                sqlpVHH_INTPERCT.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INTPERCT)

                '@VHH_bINTEREST = NULL,
                '@VHH_CALCTYP = NULL,
                '@VHH_INT_ACT_ID = NULL,
                '@VHH_ACRU_INT_ACT_ID = NULL,
                '@VHH_CHQ_pdc_ACT_ID = NULL,
                '@VHH_PROV_ACT_ID = NULL,
                '@VHH_COL_ACT_ID = NULL,
                '@VHH_CUR_ID = N'DHS',
                '@VHH_EXGRATE1 = 10,
                '@VHH_EXGRATE2 = 20,
                Dim sqlpVHH_bINTEREST As New SqlParameter("@VHH_bINTEREST", SqlDbType.Bit)
                sqlpVHH_bINTEREST.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_bINTEREST)

                Dim sqlpVHH_CALCTYP As New SqlParameter("@VHH_CALCTYP", SqlDbType.VarChar)
                sqlpVHH_CALCTYP.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CALCTYP)

                Dim sqlpVHH_INT_ACT_ID As New SqlParameter("@VHH_INT_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_INT_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INT_ACT_ID)

                Dim sqlpVHH_ACRU_INT_ACT_ID As New SqlParameter("@VHH_ACRU_INT_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_ACRU_INT_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_ACRU_INT_ACT_ID)

                Dim sqlpVHH_CHQ_pdc_ACT_ID As New SqlParameter("@VHH_CHQ_pdc_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_CHQ_pdc_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CHQ_pdc_ACT_ID)

                Dim sqlpVHH_PROV_ACT_ID As New SqlParameter("@VHH_PROV_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_PROV_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_PROV_ACT_ID)

                Dim sqlpVHH_COL_ACT_ID As New SqlParameter("@VHH_COL_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_COL_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_COL_ACT_ID)

                Dim sqlpVHH_CUR_ID As New SqlParameter("@VHH_CUR_ID", SqlDbType.VarChar, 12)
                sqlpVHH_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpVHH_CUR_ID)

                Dim sqlpVHH_EXGRATE1 As New SqlParameter("@VHH_EXGRATE1", SqlDbType.Decimal, 8)
                sqlpVHH_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpVHH_EXGRATE1)

                Dim sqlpVHH_EXGRATE2 As New SqlParameter("@VHH_EXGRATE2", SqlDbType.Decimal, 8)
                sqlpVHH_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpVHH_EXGRATE2)

                '@VHH_NARRATION = N'CHUMMA',
                '@VHH_bDELETED = 0,
                '@VHH_bPOSTED = 0,
                '@bGenerateNewNo = 1,
                '@VHH_TIMESTAMP = NULL,

                Dim sqlpVHH_NARRATION As New SqlParameter("@VHH_NARRATION", SqlDbType.VarChar, 300)
                sqlpVHH_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpVHH_NARRATION)

                Dim sqlpVHH_bPOSTED As New SqlParameter("@VHH_bPOSTED", SqlDbType.Bit)
                sqlpVHH_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpVHH_bPOSTED)

                Dim sqlpVHH_bDELETED As New SqlParameter("@VHH_bDELETED", SqlDbType.Bit)
                sqlpVHH_bDELETED.Value = False
                cmd.Parameters.Add(sqlpVHH_bDELETED)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpVHH_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpVHH_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpVHH_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpVHH_TIMESTAMP)

                '@VHH_SESSIONID =  N'123',
                '@VHH_LOCK =  N'master',
                '@bEdit = 0,
                '@VHH_bPDC = 0,
                '@VHH_COL_ID = NULL,
                '' @VHH_AMOUNT
                '@VHH_NEWDOCNO = @VHH_NEWDOCNO OUTPUT

                Dim sqlpVHH_SESSIONID As New SqlParameter("@VHH_SESSIONID", SqlDbType.VarChar, 50)
                sqlpVHH_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpVHH_SESSIONID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 50)
                sqlpVHH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim sqlpVHH_bPDC As New SqlParameter("@VHH_bPDC", SqlDbType.Bit)
                sqlpVHH_bPDC.Value = 0
                cmd.Parameters.Add(sqlpVHH_bPDC)

                Dim sqlpVHH_COL_ID As New SqlParameter("@VHH_COL_ID", SqlDbType.Int)
                sqlpVHH_COL_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_COL_ID)

                Dim sqlpVHH_AMOUNT As New SqlParameter("@VHH_AMOUNT", SqlDbType.Decimal, 21)
                sqlpVHH_AMOUNT.Value = CDbl(txtHamount.Text)
                cmd.Parameters.Add(sqlpVHH_AMOUNT)

                Dim sqlpVHH_REFNO As New SqlParameter("@VHH_REFNO", SqlDbType.VarChar, 50)
                sqlpVHH_REFNO.Value = txtHOldrefno.Text
                cmd.Parameters.Add(sqlpVHH_REFNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopVHH_NEWDOCNO As New SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopVHH_NEWDOCNO)
                cmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    'stTrans.Commit()
                    Dim str_crdr As String = "DR"
                    If ViewState("str_doctype") = "CN" Then
                        str_crdr = "CR"
                    End If

                    If ViewState("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopVHH_NEWDOCNO.Value)
                    Else
                        str_err = DeleteVOUCHER_D_S_ALL(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        End If
                    End If


                    If str_err = "0" Then
                        h_editorview.Value = ""
                        stTrans.Commit()
                        'btnSave.Enabled = False
                        gvJournal.Enabled = True
                        txtDNarration.Text = ""
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlopVHH_NEWDOCNO.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage("0")
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If

                            lblError.Text = getErrorMessage("0")
                        End If
                        clear_All()
                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = ViewState("str_doctype")
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function
    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer


        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                cmd.Dispose()
                cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table
                'Dim str_crdb As String = "CR"
                'If  Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                '    dTotal =  Session("dtJournal").Rows(iIndex)("Debit")
                '    str_crdb = "DR"
                'Else
                '    dTotal =  Session("dtJournal").Rows(iIndex)("Credit")
                'End If
                '' ''
                Dim str_crdr As String = "CR"
                If ViewState("str_doctype") = "CN" Then
                    str_crdr = "DR"
                End If
                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtJournal").Rows(iIndex)("id"), _
                str_crdr, iIndex + 1 - ViewState("iDeleteCount"), Session("dtJournal").Rows(iIndex)("Accountid"), Session("dtJournal").Rows(iIndex)("Amount"))
                If str_err <> "0" Then
                    Return str_err
                End If

                '@GUID = NULL,
                '@VHD_SUB_ID = N'007',
                '@VHD_BSU_ID = N'125016',
                '@VHD_FYEAR = 2007,
                '@VHD_DOCTYPE = N'CP',
                '@VHD_DOCNO = N'CP000007',
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpVHD_SUB_ID As New SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHD_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHD_SUB_ID)

                Dim sqlpsqlpVHD_BSU_ID As New SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpVHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpVHD_BSU_ID)

                Dim sqlpVHD_FYEAR As New SqlParameter("@VHD_FYEAR", SqlDbType.Int)
                sqlpVHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpVHD_FYEAR)

                Dim sqlpVHD_DOCTYPE As New SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpVHD_DOCTYPE.Value = ViewState("str_doctype")
                cmd.Parameters.Add(sqlpVHD_DOCTYPE)

                Dim sqlpVHD_DOCNO As New SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
                sqlpVHD_DOCNO.Value = p_docno & ""
                cmd.Parameters.Add(sqlpVHD_DOCNO)


                '@VHD_LINEID = 1,
                '@VHD_ACT_ID = N'02201003',
                '@VHD_AMOUNT = 100,
                '@VHD_NARRATION = N'NARRA1',
                '@VHD_CHQID = NULL,
                '@VHD_CHQNO = NULL,
                '@VHD_CHQDT = '',

                Dim sqlpVHD_LINEID As New SqlParameter("@VHD_LINEID", SqlDbType.Int)
                sqlpVHD_LINEID.Value = iIndex + 1 - ViewState("iDeleteCount")
                cmd.Parameters.Add(sqlpVHD_LINEID)

                Dim sqlpVHD_ACT_ID As New SqlParameter("@VHD_ACT_ID", SqlDbType.VarChar, 12)
                sqlpVHD_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                cmd.Parameters.Add(sqlpVHD_ACT_ID)

                Dim sqlpVHD_AMOUNT As New SqlParameter("@VHD_AMOUNT", SqlDbType.Decimal, 21)
                sqlpVHD_AMOUNT.Value = Session("dtJournal").Rows(iIndex)("Amount") & ""
                cmd.Parameters.Add(sqlpVHD_AMOUNT)

                Dim sqlpVHD_NARRATION As New SqlParameter("@VHD_NARRATION", SqlDbType.VarChar, 300)
                sqlpVHD_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                cmd.Parameters.Add(sqlpVHD_NARRATION)

                Dim sqlpVHD_CHQID As New SqlParameter("@VHD_CHQID", SqlDbType.VarChar, 8)
                sqlpVHD_CHQID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_CHQID)

                Dim sqlpVHD_CHQNO As New SqlParameter("@VHD_CHQNO", SqlDbType.VarChar, 8)
                sqlpVHD_CHQNO.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_CHQNO)

                Dim sqlpVHD_CHQDT As New SqlParameter("@VHD_CHQDT", SqlDbType.VarChar, 2)
                sqlpVHD_CHQDT.Value = " "
                cmd.Parameters.Add(sqlpVHD_CHQDT)

                '@VHD_RSS_ID = 68,
                '@VHD_OPBAL = 0,
                '@VHD_INTEREST = 0,
                '@VHD_bBOUNCED = 0,
                '@VHD_bCANCELLED = 0,
                '@VHD_bDISCONTED = 0,
                '@bEdit = 0  

                Dim sqlpVHD_RSS_ID As New SqlParameter("@VHD_RSS_ID", SqlDbType.Int)
                sqlpVHD_RSS_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_RSS_ID)

                Dim sqlpVHD_OPBAL As New SqlParameter("@VHD_OPBAL", SqlDbType.Decimal, 20)
                sqlpVHD_OPBAL.Value = 0
                cmd.Parameters.Add(sqlpVHD_OPBAL)

                Dim sqlpVHD_INTEREST As New SqlParameter("@VHD_INTEREST", SqlDbType.Decimal, 30)
                sqlpVHD_INTEREST.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_INTEREST)

                Dim sqlpVHD_bBOUNCED As New SqlParameter("@VHD_bBOUNCED", SqlDbType.Bit)
                sqlpVHD_bBOUNCED.Value = False
                cmd.Parameters.Add(sqlpVHD_bBOUNCED)

                Dim sqlpVHD_bCANCELLED As New SqlParameter("@VHD_bCANCELLED", SqlDbType.Bit)
                sqlpVHD_bCANCELLED.Value = False
                cmd.Parameters.Add(sqlpVHD_bCANCELLED)

                Dim sqlpVHD_COL_ID As New SqlParameter("@VHD_COL_ID", SqlDbType.Int)
                sqlpVHD_COL_ID.Value = 0
                cmd.Parameters.Add(sqlpVHD_COL_ID)

                Dim sqlpbVHD_bDISCONTED As New SqlParameter("@VHD_bDISCONTED", SqlDbType.Int)
                sqlpbVHD_bDISCONTED.Value = 0
                cmd.Parameters.Add(sqlpbVHD_bDISCONTED)

                If BSU_IsTAXEnabled Then
                    cmd.Parameters.AddWithValue("@VHD_TAX_CODE", Session("dtJournal").Rows(iIndex)("TaxCode") & "")
                Else
                    cmd.Parameters.AddWithValue("@VHD_TAX_CODE", "")
                End If

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                '' ''Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                '' ''If iIndex =  Session("dtJournal").Rows.Count - 1 Then
                '' ''    sqlpbLastRec.Value = True
                '' ''Else
                '' ''    sqlpbLastRec.Value = False
                '' ''End If
                '' ''cmd.Parameters.Add(sqlpbLastRec)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then
                    Exit For
                Else

                End If
                cmd.Parameters.Clear()
            Else
                If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                    cmd = New SqlCommand("DeleteVOUCHER_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtJournal").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If
        'Adding transaction info

    End Function


    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), ViewState("str_doctype"), _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtdocDate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), ViewState("str_doctype"), "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function


    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean

        Try
            Dim dTotal As Double = 0

            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try


    End Function



    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'txtdocDate.Text = ds.Tables(0).Rows(0)("VHH_DOCDT")
                'txtHDocno.Text = Request.QueryString("VIEWID")
                'txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim stTrans As SqlTransaction = objConn.BeginTransaction
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("VHH_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_FYEAR") & "|"

                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpVHH_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpVHH_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpVHH_BSU_ID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = ViewState("str_doctype")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = ViewState("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpVHH_DOCNO)


                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                        stTrans.Rollback()
                        Return iReturnvalue
                    End If
                    ViewState("str_timestamp") = sqlopVHH_TIMESTAMP.Value
                    stTrans.Commit()
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                ViewState("str_editData") = ""
                lblError.Text = "Invalid Edit id"
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = "Invalid Edit id"
            Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " VHH_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'txtdocDate.Text = ds.Tables(0).Rows(0)("VHH_DOCDT")
                'txtHDocno.Text =  viewstate("str_editData").Split("|")(0)
                'txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = ViewState("str_doctype")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = ViewState("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        btnEditCancel.Visible = True
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")

        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
                btnEditCancel.Visible = False
            End If

        Else
            h_editorview.Value = "Edit"
            btnEditCancel.Visible = True
            ResetViewData()
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'setModifyHeader(Request.QueryString(""))
            'txtHDocno.Text =  viewstate("str_editData").Split("|")(0)
            btnSave.Enabled = True
        End If
    End Sub


    Protected Sub btnEditcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        btnAdddetails.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        'btnCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Dim ds As New DataSet

        Dim objConn As New SqlConnection(str_conn)


        Dim str_Sql As String

        str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
               & " GUID='" & Request.QueryString("viewid") & "'"

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("DeleteVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)
                '@DOCNO	varchar(20),
                '@DOCTYPE	varchar(20) ,
                '@VHH_BSU_ID	varchar(10), 
                '@VHH_FYEAR	int,
                '@VHH_SUB_ID varchar(20),
                '@VHH_LOCK varc
                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = ViewState("str_doctype")
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 30)
                sqlpVHH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    lblError.Text = getErrorMessage("519")
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    ViewState("datamode") = Encr_decrData.Encrypt("add")
                    clear_All()
                    Response.Redirect("acccnViewDebitNote.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try

    End Sub


    Protected Sub txtOAmt_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("onBlur", "find_Ototal();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub


    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" ' _
            If Request.QueryString("BSUID") Is Nothing Then
                str_Sql += " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Else
                str_Sql += " AND VHH_BSU_ID='" & Request.QueryString("BSUID") & "'"
            End If

            '& " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = ds.Tables(0).Rows(0)("VHH_DOCDT")
                'txtHDocno.Text = Request.QueryString("editid")
                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Try
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("VHH_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_FYEAR") & "|"
                    Return ""
                Catch ex As Exception
                    Errorlog(ex.Message)
                End Try
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("acccnDebitNote.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("acccnDebitNote.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
        End Try
        Return True
    End Function


    Protected Sub lblAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Try
            'Adding transaction info
            Dim iIndex As Integer
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                Dim str_manndatory_costcenter As String
                If Convert.ToBoolean(Session("dtJournal").Rows(iIndex)("required")) = True Then
                    str_manndatory_costcenter = Session("dtJournal").Rows(iIndex)("Costcenter")
                Else
                    str_manndatory_costcenter = ""
                End If
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                            Session("dtJournal").Rows(iIndex)("Amount"), str_manndatory_costcenter)
                    If str_Err <> "" Then
                        str_Error = str_Error & "<br/>" & str_Err & " At Line - " & iIndex + 1
                    End If
                End If
            Next
            'Adding transaction info
            Return str_Error
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function


    Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
    Optional ByVal p_mandatory_costcenter As String = "") As String
        Try
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer
            Dim bool_check_other, bool_mandatory_exists As Boolean

            Dim str_err As String = ""
            If p_mandatory_costcenter <> "" Then
                bool_mandatory_exists = False
            Else
                bool_mandatory_exists = True
            End If
            Dim str_balanced As Boolean = True
            bool_check_other = False
            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtCostChild").Rows(iIndex)("costcenter") = p_mandatory_costcenter Then
                        bool_mandatory_exists = True
                    End If
                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                        If str_balanced = False Then
                            str_err = str_err & " <BR> Invalid Allocation for cost center " & AccountFunctions.get_cost_center(Session("dtCostChild").Rows(iIndex)("costcenter"))
                        End If
                    End If
                    str_balanced = True
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
                        iLineid = -1
                        bool_check_other = True
                        str_balanced = check_others(p_voucherid, p_amount)
                        If str_balanced = False Then
                            If str_err = "" Then
                                str_err = "Invalid Allocation for other cost center"
                            Else
                                str_err = str_err & " <BR> Invalid Allocation for other cost centers "
                            End If

                        End If
                    End If
                    iLineid = iLineid + 1
                    'If str_balanced = False Then
                    '    Exit For
                    'End If
                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                End If
            Next
            If bool_mandatory_exists = False Then
                str_err = "<br>Mandatory Cost center - " & AccountFunctions.get_cost_center(p_mandatory_costcenter) & " not allocated"
            End If
            Return str_err
        Catch ex As Exception
            Errorlog(ex.Message, "child")
            Return ""
        End Try
    End Function


    Private Function check_Errors_head() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Try
            Dim dTotal As Double = 0
            ''CHECK MANDATORY
            txtDAccountName.Text = AccountFunctions.check_accountid(Detail_ACT_ID.Text & "", Session("sBsuid"))
            Dim str_cost_center As String = ""
            '''''FIND ACCOUNT IS THERE
            Dim str_mandatory_costcenter As String = ""
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                & " AND ACT_ID='" & txtHAccountcode.Text & "'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    If Convert.ToBoolean(ds.Tables(0).Rows(0)("PLY_BMANDATORY")) = True Then
                        str_mandatory_costcenter = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                    End If
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try
            '''''FIND ACCOUNT IS THERE
            ''CHK MANDATRY
            'str_Err = ""
            str_Err = check_Errors_sub("hd", txtHamount.Text, str_mandatory_costcenter)
            If str_Err <> "" Then
                str_Error = str_Error & "<br/>" & str_Err & " In Header"
            End If
            Return str_Error
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function


    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        h_editorview.Value = ""
        If ViewState("datamode") = "edit" Then
            unlock()
        End If
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.DebitCreditNoteVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), ViewState("str_doctype"), txtHDocno.Text, Session("HideCC"))
        Session("ReportSource") = repSource
        '    Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Protected Sub imgCalendar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub txtHAccountcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHAccountcode.TextChanged
        chk_Partycode()
    End Sub

    Sub chk_Partycode()
        txtHAccountname.Text = AccountFunctions.Validate_Account(txtHAccountcode.Text, Session("sbsuid"), "CUSTSUPPnIJV")
        If txtHAccountname.Text = "" Then
            lblError.Text = "Invalid Party selected"
            txtHAccountcode.Focus()
        Else
            lblError.Text = ""
            txtHamount.Focus()
        End If
    End Sub

    Protected Sub btnHaccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHaccount.Click
        chk_Partycode()
    End Sub

    Protected Sub Detail_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail_ACT_ID.TextChanged
        txtDAccountName.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "NORMAL")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            Detail_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtDAmount.Focus()
        End If
        ClearRadGridandCombo()
    End Sub

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub
End Class
