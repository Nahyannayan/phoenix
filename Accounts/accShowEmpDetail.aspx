<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accShowEmpDetail.aspx.vb" Inherits="ShowEmpDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />
    <script src="../vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>



    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">

        <div>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td  colspan="4" align="center">
                                                    <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="ID" CssClass="table table-row table-bordered">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("No") %>'></asp:LinkButton>&nbsp;
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID" CssClass="gridheader_text"></asp:Label><br />
                                                                    <asp:TextBox ID="txtcode" runat="server"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpId_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text="Business Unit Name"  CssClass="gridheader_text"></asp:Label><br />
                                                                    <asp:TextBox ID="txtName" runat="server"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                        Text='<%# Eval("E_Name") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <RowStyle  />
                                                    </asp:GridView>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td  ></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4"  
                            valign="middle">
                            <input type="hidden" id="h_SelectedId" runat="server" value="0" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>


</body>
</html>
