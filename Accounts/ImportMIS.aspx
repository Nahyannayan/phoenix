<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ImportMIS.aspx.vb" Inherits="Accounts_ImportMIS" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
        function getFile() {

            var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
            document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Import MIS Data
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtHDocdate"
                                            ErrorMessage="Invalid Date" ValidationGroup="Details">Invalid Currency</asp:RequiredFieldValidator></td>
                                     <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="lblBSUnit" runat="server" AutoCompleteType="Disabled" MaxLength="18" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Currency</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="drpCurrency" runat="server">
                                        </asp:DropDownList></td>
                                      <td align="left" width="20%"><span class="field-label">Group Currency</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="lblGroupCurr" runat="server" AutoCompleteType="Disabled" MaxLength="18"></asp:TextBox>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">P/L Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtExchangeRate" runat="server" AutoCompleteType="Disabled" MaxLength="18"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtExchangeRate"
                                            ErrorMessage="Exchange Rate Cannot be blank" ValidationGroup="Details">Exchange Rate Cannot be blank</asp:RequiredFieldValidator>
                                    </td>
                                     <td align="left" width="20%"><span class="field-label">B/S Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBSExchangeRate" runat="server" AutoCompleteType="Disabled" MaxLength="18"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtBSExchangeRate"
                                            ErrorMessage="Exchange Rate Cannot be blank" ValidationGroup="Details">BS Exchange Rate Cannot be blank</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" TabIndex="160"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtRemarks"
                                            ErrorMessage="Narration Cannot be blank" ValidationGroup="Details">Narration Cannot be blank</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">File Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"></asp:FileUpload>
                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload Excel" CausesValidation="False"
                                            TabIndex="30" OnClick="btnFind_Click" OnClientClick="getFile()" /></td>
                                </tr>
                            </table>
                            <br />
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left">
                                        <asp:Label ID="labdetailHead" runat="server" EnableViewState="False">Details</asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" ShowFooter="True">
                                            <FooterStyle />
                                            <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                            <Columns>
                                                <asp:BoundField HtmlEncode="False" HeaderText="ID" DataField="ID">
                                                    <HeaderStyle Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField ReadOnly="True" DataField="CODE"
                                                    SortExpression="AccountType" HeaderText="CODE">
                                                    <HeaderStyle Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField SortExpression="AccountNo"
                                                    HeaderText="Sl No" Visible="False">
                                                    <HeaderStyle Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Descr" HeaderText="Description" HtmlEncode="False">
                                                    <HeaderStyle Width="40%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Type" HeaderText="Type">
                                                    <HeaderStyle Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Metric" Visible="False">
                                                    <HeaderStyle Width="13%" />
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LocalCurrency" HeaderText="Local Currency">
                                                    <HeaderStyle Width="13%" />
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="AED" DataField="AED">
                                                    <HeaderStyle Width="14%" />
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Not Valid" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NotValid") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNotValid" runat="server" Text='<%# Bind("NotValid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle></SelectedRowStyle>
                                            <HeaderStyle></HeaderStyle>

                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table id="Table1" align="center" width="100%">
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" OnClick="btnEdit_Click" />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" CausesValidation="False" TabIndex="24" Width="48px" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClick="btnDelete_Click"
                                            OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                                        <br />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Editid" runat="server" Value="0" />
                <asp:HiddenField ID="HidUpload" runat="server" />
                <asp:HiddenField ID="h_SelectId" runat="server" />
                <asp:HiddenField ID="h_Posted" runat="server" Value="0" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <script>
 
                </script>

            </div>
        </div>
    </div>
</asp:Content>

