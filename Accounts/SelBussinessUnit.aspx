<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelBussinessUnit.aspx.vb" Inherits="SelBussinessUnit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Business Unit Selection</title>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
      
         </script>

     <script>
         function GetRadWindow() {
             var oWindow = null;
             if (window.radWindow) oWindow = window.radWindow;
             else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
             return oWindow;
         }
    </script>

</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
     <table id="tbl" width="100%" align="center">
                    <tr valign = "top">
                        <td colspan="3">
                        <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                    <tr valign = "top">
                        <td>
                        </td>
                        <td align="left">
                            <asp:GridView ID="gvGroup" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                           <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("BSU_ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtName" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtBSUName" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("BSU_NAME") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                                 AutoPostBack="True" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                             <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" /></td>
                    </tr>
                </table>
            
    </form>
</body>
</html>