Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj

Partial Class Accounts_AccVoucherAdministration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("../noAccess.aspx")
        End If
        Dim MainMnu_code As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Page.Title = OASISConstants.Gemstitle

        UtilityObj.beforeLoopingControls(Me.Page)

        If Not Page.IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            txtDocNo.Attributes.Add("ReadOnly", "ReadOnly")
            lblrptCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            'h_BSU_ID.Value = Session("sBsuid")
            h_FYEAR.Value = Session("F_YEAR")
            h_SUB_ID.Value = "007"
            filldrp()
        End If

    End Sub
    Sub ClearAll()
        Me.txtDocNo.Text = ""
        h_FYEAR.Value = Session("F_YEAR")
        h_SUB_ID.Value = "007"
        filldrp()

    End Sub

    Private Sub FillValues()
        Try
            Dim dtDTL As DataTable = ViewVoucher(Me.ddlBSU.SelectedValue, h_DocNo.Value)
            Dim BBearer As Boolean
            'Session("dtDTL") = dtDTL
            txtReceivedby.Text = Mainclass.getDataValue("select VHH_RECEIVEDBY from dbo.VOUCHER_H where VHH_BSU_ID='" & Session("sBsuid") & "' and VHH_DOCNO ='" & txtDocNo.Text & "' and VHH_FYEAR ='" & Session("F_YEAR") & "'", "MainDB")
            BBearer = Mainclass.getDataValue("select VHH_BBearer from dbo.VOUCHER_H where VHH_BSU_ID='" & Session("sBsuid") & "' and VHH_DOCNO ='" & txtDocNo.Text & "' and VHH_FYEAR ='" & Session("F_YEAR") & "'", "MainDB")
            If BBearer = True Then
                ChkBearer.Checked = True
            Else
                ChkBearer.Checked = False
            End If
            'ChkBearer.Checked = Not Mainclass.getDataValue("select VHH_BBearer from dbo.VOUCHER_H where VHH_BSU_ID='" & Session("sBsuid") & "' and VHH_DOCNO ='" & txtDocNo.Text & "' and VHH_FYEAR ='" & Session("F_YEAR") & "'", "MainDB")
            gvDTL.DataSource = dtDTL
            gvDTL.DataBind()
        Catch ex As Exception
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub

    Public Function ViewVoucher(ByVal VHD_BSU_ID As String, ByVal VHD_DOCNO As String) As DataTable
        Dim qry As String = "SELECT A.VHD_ID,B.ACT_ID,B.ACT_NAME,A.VHD_AMOUNT,A.VHD_CHQNO,A.VHD_CHQDT, LTRIM(RTRIM(A.VHD_NARRATION))VHD_NARRATION " & _
        "FROM dbo.VOUCHER_D A INNER JOIN dbo.ACCOUNTS_M B ON A.VHD_ACT_ID=B.ACT_ID " & _
        "WHERE VHD_BSU_ID='" & VHD_BSU_ID & "' AND VHD_DOCNO='" & VHD_DOCNO & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.Text, qry)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT ID,DESCR FROM VW_MST_CC_DocumentTypes where ID='BP'"
            Dim ddlds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Query)
            Me.ddDocumentType.DataSource = ddlds
            Me.ddDocumentType.DataTextField = "DESCR"
            Me.ddDocumentType.DataValueField = "ID"
            Me.ddDocumentType.DataBind()

            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                    CommandType.Text, "select BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
            ddlBSU.DataSource = ds.Tables(0)
            ddlBSU1.DataSource = ds.Tables(0)
            Me.ddlBSU.DataTextField = "BSU_NAME"
            Me.ddlBSU.DataValueField = "BSU_ID"

            Me.ddlBSU1.DataTextField = "BSU_NAME"
            Me.ddlBSU1.DataValueField = "BSU_ID"

            ddlBSU.DataBind()
            ddlBSU1.DataBind()
            If ddlBSU.Items.Count > 0 Then
                ddlBSU.SelectedValue = Session("sBsuid")
            End If
            If ddlBSU1.Items.Count > 0 Then
                ddlBSU1.SelectedValue = Session("sBsuid")
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        ClearAll()
        If Not Request.UrlReferrer Is Nothing Then
            Response.Redirect(Request.UrlReferrer.ToString())
        End If
    End Sub

    Protected Sub imgDocNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDocNo.Click
        FillValues()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Not isPageExpired() Then
        Dim flag As Boolean = True
        Dim cmd As New SqlCommand
        cmd.Dispose()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction
        Try
            stTrans = objConn.BeginTransaction
            Dim iReturnvalue As Integer
            For Each gvr As GridViewRow In gvDTL.Rows
                Dim txtChqNo As TextBox = DirectCast(gvr.FindControl("txtChqNo"), TextBox)
                Dim txtChqDt As TextBox = DirectCast(gvr.FindControl("txtChqDt"), TextBox)
                Dim chkUnAll As CheckBox = DirectCast(gvr.FindControl("chkUnAllocate"), CheckBox)
                Dim txtNarration As TextBox = TryCast(gvr.FindControl("txtNarration"), TextBox)
                Me.gvDTL.SelectedIndex = gvr.RowIndex
                Dim VHD_ID As Integer = CInt(gvDTL.SelectedDataKey.Item(0))

                Dim pParms(9) As SqlClient.SqlParameter

                pParms(0) = New SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar)
                pParms(0).Value = Me.ddlBSU.SelectedValue

                pParms(1) = New SqlParameter("@VHD_ID", SqlDbType.Int)
                pParms(1).Value = VHD_ID

                pParms(2) = New SqlParameter("@NEWCHQ_NO", SqlDbType.VarChar)
                pParms(2).Value = txtChqNo.Text.Trim

                pParms(3) = New SqlParameter("@NEWCHQ_DT", SqlDbType.DateTime)
                pParms(3).Value = txtChqDt.Text

                pParms(4) = New SqlParameter("@NEWNARRATION", SqlDbType.VarChar, 300)
                pParms(4).Value = txtNarration.Text.Trim

                pParms(5) = New SqlParameter("@bUNALLOCATE", SqlDbType.Bit)
                pParms(5).Value = chkUnAll.Checked
                pParms(6) = New SqlParameter("@USER", SqlDbType.VarChar)
                pParms(6).Value = Session("sUsr_name")

                pParms(7) = New SqlParameter("@NEWRECEIVEDBY", SqlDbType.VarChar)
                pParms(7).Value = txtReceivedby.Text

                pParms(8) = New SqlParameter("@NEWBBEARER", SqlDbType.Bit)
                pParms(8).Value = ChkBearer.Checked

                pParms(9) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue

                iReturnvalue = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "UPDATE_CHQ_DETAIL", pParms)
                iReturnvalue = pParms(9).Value

                If iReturnvalue <> 0 Then
                    flag = False
                    Exit For
                End If

            Next
            If flag = True Then
                stTrans.Commit()
                lblError.Text = "Cheque details updated successfully"
                FillValues()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(iReturnvalue)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        'Else
        '    lblError.Text = "Session Expired"
        'End If

    End Sub

    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Dim flag As Boolean = True
        Dim cmd As New SqlCommand
        cmd.Dispose()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction
        Try
            stTrans = objConn.BeginTransaction
            Dim iReturnvalue As Integer
            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlParameter("@IJH_BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = Me.ddlBSU1.SelectedValue

            pParms(1) = New SqlParameter("@IJH_DOC_NO", SqlDbType.VarChar)
            pParms(1).Value = txtDocNo1.Text

            pParms(2) = New SqlParameter("@USER", SqlDbType.VarChar)
            pParms(2).Value = Session("sUsr_name")

            pParms(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue

            iReturnvalue = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "UNLOCK_IJV_VOUCHER", pParms)
            iReturnvalue = pParms(3).Value

            If iReturnvalue = 0 Then
                stTrans.Commit()
                lblError.Text = "IJV Unlocked successfully"
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(iReturnvalue)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel1.Click
        ClearAll()
        If Not Request.UrlReferrer Is Nothing Then
            Response.Redirect(Request.UrlReferrer.ToString())
        End If
    End Sub
End Class
