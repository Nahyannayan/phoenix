Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_AccTreasuryTransferView
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
           

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A150077") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            'gvChild.Attributes.Add("bordercolor", "#1b80b6")
            'gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
           
            Dim url As String
            ViewState("datamode") = "add"
            Dim Queryusername As String = Session("sUsr_name")
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            url = "AccTreasuryTransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & dataModeAdd
            hlAddNew.NavigateUrl = url
        End If
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function


    Private Sub PrintVoucher()


        If gvJournal.Rows.Count > 1 Then


            Try

                Dim str_Sql As String = Session("SQLQUERY")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.VoucherName = "FUND TRANSFER MEMO"
                repSource.Command = cmd
                repSource.IncludeBSUImage = True

                repSource.ResourceName = "../RPT_Files/Treasurytransferview.rpt"

                Session("ReportSource") = repSource
                h_print.Value = "print"
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End If
    End Sub


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
        
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "AccTreasuryTransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                hlCEdit.Enabled = True
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If
        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim SqlQuery As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrPayedUnite As String = String.Empty
            Dim lstrReceivedUnite As String = String.Empty
            Dim lstrDocDate As String = String.Empty
            Dim lstrNarration As String = String.Empty
            Dim lstrAmount As String = String.Empty
            Dim lstrDocno As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim lstrFiltPayedUnite As String = String.Empty
            Dim lstrFiltreReceivedUnite As String = String.Empty
            Dim lstrFiltDocDate As String = String.Empty
            Dim lstrFiltNarration As String = String.Empty
            Dim lstrFiltAmount As String = String.Empty
            Dim lstrFiltDocno As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 3333   refno
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtPayedunit")
                lstrPayedUnite = Trim(txtSearch.Text)
                If (lstrPayedUnite <> "") Then lstrFiltPayedUnite = SetCondn(lstrOpr, "vw_OSO_BUSINESSUNIT_M_1.BSU_NAME", lstrFiltPayedUnite)

                '   -- 4444  docno
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceivedunite")
                lstrReceivedUnite = Trim(txtSearch.Text)
                If (lstrReceivedUnite <> "") Then lstrFiltreReceivedUnite = SetCondn(lstrOpr, "vw_OSO_BUSINESSUNIT_M.BSU_NAME", lstrReceivedUnite)

                '   -- 1111  DocDate
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
                lstrDocDate = txtSearch.Text
                If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "ITF_DATE", lstrDocDate)

                '   -- 2222  Narration
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
                lstrNarration = txtSearch.Text
                If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "ITF_NARRATION", lstrNarration)


                '-- 555 Amount
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrAmount = txtSearch.Text
                If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "ITF_AMOUNT", lstrAmount)

                '-- 666 Docno
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocno")
                lstrDocno = txtSearch.Text
                If (lstrDocno <> "") Then lstrFiltDocno = SetCondn(lstrOpr, "ITF_DOCNO", lstrDocno)

            End If
            Dim str_Filter As String = ""
            Dim str_ListDoc As String = String.Empty
            If Session("ListDays") IsNot Nothing Then
                'If String.Compare(Session("ListDays"), "all", True) <> 0 Then
                str_ListDoc = " AND INTERUNIT_TRANSFER.ITF_DATE BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
            End If
            If rbPosted.Checked = True Then
                str_Filter = " AND INTERUNIT_TRANSFER.ITF_bPOSTED=1"
                str_Filter += str_ListDoc
            End If
            If rbUnposted.Checked = True Then
                str_Filter = " AND INTERUNIT_TRANSFER.ITF_bPOSTED=0"
            End If

            If rbAll.Checked = True Then
                str_Filter = str_ListDoc
            End If
            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If



            str_Sql = " SELECT " _
            & " INTERUNIT_TRANSFER.IRF_ID,ITF_DOCNO, " _
            & " INTERUNIT_TRANSFER.ITF_DATE, " _
            & " vw_OSO_BUSINESSUNIT_M.BSU_NAME AS RECEIVEDUNIT, " _
            & " vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS PAYEDUNIT," _
            & " INTERUNIT_TRANSFER.ITF_NARRATION, " _
            & " INTERUNIT_TRANSFER.ITF_AMOUNT, " _
            & " INTERUNIT_TRANSFER.ITF_bPOSTED" _
            & " FROM " _
            & " INTERUNIT_TRANSFER INNER JOIN " _
            & " vw_OSO_BUSINESSUNIT_M ON INTERUNIT_TRANSFER.ITF_DRBSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
            & " vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON  " _
            & " INTERUNIT_TRANSFER.ITF_CRBSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID WHERE ITF_bDELETED = 0 " _
            & " AND ITF_FYEAR = " & Session("F_YEAR") & " AND ITF_BSU_ID = '" & Session("sBsuid") & "'" _
            & str_Filter & lstrFiltPayedUnite & lstrFiltreReceivedUnite & lstrFiltDocDate & lstrFiltNarration & lstrFiltAmount & lstrFiltDocno _
            & " ORDER BY IRF_ID DESC  "


            SqlQuery = " SELECT " _
          & " INTERUNIT_TRANSFER.ITF_DOCNO as IRF_ID, " _
          & " INTERUNIT_TRANSFER.ITF_DATE, " _
          & " vw_OSO_BUSINESSUNIT_M.BSU_NAME AS TOUNIT, " _
          & " vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS FROMUNIT," _
          & " INTERUNIT_TRANSFER.ITF_NARRATION, " _
          & " INTERUNIT_TRANSFER.ITF_AMOUNT, " _
          & " INTERUNIT_TRANSFER.ITF_bPOSTED" _
          & " FROM " _
          & " INTERUNIT_TRANSFER INNER JOIN " _
          & " vw_OSO_BUSINESSUNIT_M ON INTERUNIT_TRANSFER.ITF_DRBSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
          & " vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON  " _
          & " INTERUNIT_TRANSFER.ITF_CRBSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID WHERE ITF_bDELETED = 0 " _
          & " AND ITF_FYEAR = " & Session("F_YEAR") & " AND ITF_BSU_ID = '" & Session("sBsuid") & "'" _
          & str_Filter & lstrFiltPayedUnite & lstrFiltreReceivedUnite & lstrFiltDocDate & lstrFiltNarration & lstrFiltDocno & lstrFiltAmount _
          & " ORDER BY IRF_ID DESC  "
            Session("SQLQUERY") = SqlQuery



            Dim ds As New DataTable
            Dim MainObj As Mainclass = New Mainclass()

            ds = MainObj.ListRecords(str_Sql, "mainDB")
            gvJournal.DataSource = ds
            If ds.Rows.Count = 0 Then
                ds.Rows.Add()
                'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
            txtSearch.Text = lstrDocDate


            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            txtSearch.Text = lstrNarration

            txtSearch = gvJournal.HeaderRow.FindControl("txtPayedunit")
            txtSearch.Text = lstrPayedUnite

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceivedunite")
            txtSearch.Text = lstrReceivedUnite

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrAmount

            gvJournal.SelectedIndex = p_selected_id
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
       
    End Sub


    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        gridbind()
      
    End Sub


    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        gridbind()
      
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvJournal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)



    End Sub

    
   
    

    Protected Sub LinkPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PrintVoucher()
    End Sub
End Class
