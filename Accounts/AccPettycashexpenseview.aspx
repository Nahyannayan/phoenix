<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccPettycashexpenseview.aspx.vb" Inherits="Accounts_AccPettycashexpenseview" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">

        function filterTable(term, _id, cellNr) {
            var suche = term
            var table = document.getElementById(_id);
            var minRoleID = 3
            var ele;
            for (var r = 1; r < table.rows.length; r++) {

                ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g, "");
                if (ele.toLowerCase().indexOf(suche) >= 0) {
                    table.rows[r].style.display = '';

                    if (parseInt(table.rows[r].cells[5].innerHTML) <= minRoleID && (table.rows[r].cells[4].innerHTML == 'N')
                    && table.rows[r].cells[3].outerText == ' ') {
                        table.rows[r].style.color = "Red";
                        table.rows[r].style.background = "#F0E68C"

                        minRoleID = parseInt(table.rows[r].cells[5].innerHTML);
                    }
                    else {
                        table.rows[r].style.display = '';
                        table.rows[r].style.color = "";
                    }

                }

                else table.rows[r].style.display = 'none';
                table.rows[r].cells[5].style.display = 'none';

            }

        }





        function fetchId(ObjId, ColId, Trow) {

            var x = 2;
            var valuY = "N";
            var hCellid = ColId + "_TR1_TD1"
            document.getElementById(hCellid).style.display = 'none'
            document.getElementById(ColId).style.display = 'none'
            for (i = 0; i < Trow ; i++) {
                var RowId = ColId + "_TR" + x
                var CellId = RowId + "_TD" + x


                var CheckValue = document.getElementById(CellId).innerText;
                document.getElementById(RowId).style.display = 'none'
                document.getElementById(CellId).style.display = 'none'
                if (document.forms[0].elements[i].type == 'td')
                    alert(document.forms[0].elements[i].name.search(/td[i]/));


                if (Number(CheckValue) == Number(ObjId)) {
                    valuY = "Y";

                    document.getElementById(RowId).style.display = 'inline'
                }
                x++;
            }
            if (valuY == 'Y')
                document.getElementById(ColId).style.display = 'inline'
        }

        function Mouse_Move(Obj) {

            document.getElementById(Obj).style.color = "Red";
        }
        function Mouse_Out(Obj) {
            document.getElementById(Obj).style.color = "#1b80b6"
        }



        //function scroll_page()
        //{  
        // document.location.hash ='<%=h_Grid.value %>';
        // }   
        // window.onload = scroll_page;

    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Petty Cash Expense <%--<asp:Label ID="lblHead" runat="server"></asp:Label>--%>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                        <input id="h_Grid" runat="server" type="hidden" value="top" />
                                        <a id='top'></a>

                                        <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                                    </td>
                                    <td align="right" width="50%">
                                        <asp:RadioButton ID="rbUnposted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="Open" Checked="True" />
                                        <asp:RadioButton ID="RdForword" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="Forward" OnCheckedChanged="RdForword_CheckedChanged" />
                                        <asp:RadioButton ID="RdApprove" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="Approved" OnCheckedChanged="RdApprove_CheckedChanged" />
                                        <asp:RadioButton ID="RdRejected" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="Rejected"
                                            OnCheckedChanged="RdApprove_CheckedChanged" />
                                        <asp:RadioButton ID="rbPosted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="Posted" />
                                        <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                            GroupName="POST" Text="All" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="left" colspan="2">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="center" width="100%">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Cash Payment Vouchers found"
                                            Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField SortExpression="PCH_DOCNO" HeaderText="Document No">
                                                    <HeaderTemplate>
                                                        Document No
                                                        <br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label25" runat="server" Text='<%# Bind("PCH_DOCNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="ITF_DATE" HeaderText="Document Date">
                                                    <HeaderTemplate>
                                                        Date<br />
                                                        <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDate" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("PCH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="PCH_REMARKS" HeaderText="Remarks">
                                                    <HeaderTemplate>
                                                        Narration
                                                                <br />
                                                        <asp:TextBox ID="txtNarrationF" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("PCH_REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employe">
                                                    <HeaderTemplate>
                                                        Employee
                                                                <br />
                                                        <asp:TextBox ID="txtEmploye" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEmployeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMP_FNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expense Type">
                                                    <HeaderTemplate>
                                                        Expense Type
                                                               <br />
                                                        <asp:TextBox ID="txtExpensetype" runat="server"></asp:TextBox>

                                                        <asp:ImageButton ID="btnDescription" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("TYP_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <HeaderTemplate>
                                                        Amount<br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("PCH_AMOUNT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="PCH_Bposted" HeaderText="Posted">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("PCH_Bposted")) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="PCH_VHH_DOCNO" HeaderText="Paid">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="vhDocno" runat="server" Text='<%# Bind("PCH_VHH_DOCNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View" ShowHeader="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                                        <ajaxToolkit:HoverMenuExtender ID="HoverId" PopupControlID="PanelView" PopupPosition="Left"
                                                            TargetControlID="hlView" runat="server">
                                                        </ajaxToolkit:HoverMenuExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="PCH_ID" Visible="false" HeaderText="PCH_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("PCH_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <a id='detail'></a>
                                        <br />
                                        <a id='child'></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PanelView" runat="server" Visible="False" Width="50%">
                    <asp:GridView ID="gvStatus" Width="100%" runat="server" AutoGenerateColumns="False"
                        OnRowDataBound="gvStatus_RowDataBound" CssClass="table table-bordered table-row" CellPadding="0">
                        <Columns>
                            <asp:BoundField DataField="APS_DOC_ID" SortExpression="APS_DOC_ID" HeaderText="..">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="APS_DATE" SortExpression="APS_DATE" HeaderText="Date">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="APS_USR_ID" SortExpression="EMP_FNAME" HeaderText="Approve User">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="APS_REMARKS" SortExpression="APS_REMARKS" HeaderText="Remarks">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="APS_STATUS" SortExpression="APS_STATUS" HeaderText="Status">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="APS_ORDERID" SortExpression="APS_ORDERID" Visible="true"
                                ShowHeader="False" HeaderStyle-BorderStyle="None">
                                <HeaderStyle BackColor="White" BorderColor="White" BorderStyle="None" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>

</asp:Content>
