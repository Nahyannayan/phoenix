Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class accChqCollection
    Inherits System.Web.UI.Page

    Shared MainMnu_code As String
    Shared menu_rights As Integer
    Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64
    Shared content As ContentPlaceHolder

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            Else
                lblError.Text = ""
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                datamode = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Then 'Or MainMnu_code <> "A200014" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

            Response.CacheControl = "no-cache"
            'calling pageright class to get the access rights
            menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
            Call AccessRight.setpage(content, menu_rights, datamode)

            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")

            gridbind()
        End If

    End Sub

    ' For the Search Controls

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub


    Private Sub gridbind()
        Dim lstrDocNo As String = String.Empty
        Dim lstrBank As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrCreditor As String = String.Empty
        Dim lstrChqNo As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltBank As String = String.Empty
        Dim lstrFiltCreditor As String = String.Empty
        Dim lstrFiltChqNo As String = String.Empty
        Dim lstrFiltCurrency As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim txtSearch As New TextBox
        Dim larrSearchOpr() As String
        If gvJournal.Rows.Count > 0 Then
            ' --- Initialize The Variables
            lstrDocNo = ""
            lstrBank = ""
            lstrPartyAC = ""
            lstrCreditor = ""
            lstrChqNo = ""
            lstrCurrency = ""
            lstrAmount = ""

            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            If txtSearch IsNot Nothing Then
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "VHD_DOCNO", lstrDocNo)
            End If
            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtBank")
            If txtSearch IsNot Nothing Then
                lstrBank = txtSearch.Text
                If (lstrBank <> "") Then lstrFiltBank = SetCondn(lstrOpr, "ACT_NAME", lstrBank)
            End If
            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
            If txtSearch IsNot Nothing Then
                lstrCreditor = txtSearch.Text
                If (lstrCreditor <> "") Then lstrFiltCreditor = SetCondn(lstrOpr, "PARTY_ACT_NAME", lstrCreditor)
            End If

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
            If txtSearch IsNot Nothing Then
                lstrChqNo = txtSearch.Text
                If (lstrChqNo <> "") Then lstrFiltChqNo = SetCondn(lstrOpr, " VHD_CHQNO", lstrChqNo)
            End If

            '   -- 7  Currency
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
            If txtSearch IsNot Nothing Then
                lstrCurrency = txtSearch.Text
                If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "CUR_DESCR", lstrCurrency)
            End If
        End If
        Dim str_Filter As String = lstrFiltDocNo & lstrFiltBank & lstrFiltCreditor & lstrFiltChqNo & lstrFiltCurrency

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT * FROM vw_OSA_CHEQCOLLECTION WHERE VHD_CHQDT <= Getdate() " & _
        " AND VHH_BSU_ID IN('" & Session("sBSUID") & "') " & str_Filter & _
        " ORDER BY VHD_CHQDT DESC, ACT_NAME DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvJournal.DataBind()
            Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
            gvJournal.Rows(0).Cells.Clear()
            gvJournal.Rows(0).Cells.Add(New TableCell)
            gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
            gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvJournal.DataBind()
        End If
        'gvJournal.DataBind()

        If gvJournal.Rows.Count > 0 Then

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            If txtSearch IsNot Nothing Then
                txtSearch.Text = lstrDocNo
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtBank")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrBank

            txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrCreditor


            txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrChqNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrCurrency

            set_Menu_Img()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '' content = Page.Controls
        Find_Checked(gvJournal) 'Page)
        gridbind()
    End Sub

    Private Sub Find_Checked(ByVal ctr As Control)
        For Each ctrl As Control In ctr.Controls
            If TypeOf ctrl Is GridViewRow Then
                Dim gvrow As GridViewRow = ctrl
                If TypeOf gvrow.FindControl("chkPosted") Is HtmlInputCheckBox Then
                    Dim chk As HtmlInputCheckBox = CType(gvrow.FindControl("chkPosted"), HtmlInputCheckBox)
                    If chk.Checked = True And chk.Disabled = False Then
                        Dim dt As DateTime = DateTime.MinValue
                        If TypeOf gvrow.FindControl("txtChqDate") Is TextBox Then
                            Dim txtChqDate As TextBox = gvrow.FindControl("txtChqDate")
                            dt = CDate(txtChqDate.Text)
                        End If
                        ClearChequeReceipts(chk.Value, dt)
                    End If
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
            'If TypeOf ctrl Is HtmlInputCheckBox Then
            '    Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
            '    If chk.Checked = True And chk.Disabled = False Then
            '        'chk.Visible = False
            '        h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
            '        ClearChequeReceipts(chk.Value)
            '    End If
            ''Else
            'If ctrl.Controls.Count > 0 Then
            '    Find_Checked(ctrl)
            'End If
            'End If
        Next
        'Response.Write(h_SelectedId.Value)
    End Sub



    Private Function ClearChequeReceipts(ByVal guid As String, ByVal chqDate As DateTime) As Boolean

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim stTrans As SqlTransaction
        Try
            objConn.Open()
            stTrans = objConn.BeginTransaction
            Dim str_Sql As String
            str_Sql = "SELECT * FROM vw_OSA_CHEQCOLLECTION WHERE VHD_CHQDT <= Getdate() " & _
            " AND GUID = '" & guid & "' AND VHH_BSU_ID IN('" & Session("sBSUID") & "') " & _
            " ORDER BY VHD_CHQDT DESC, ACT_NAME DESC"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then

                Dim cmd As New SqlCommand("CLEARCHEQUERECIEPTS", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDOC_NO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                sqlpDOC_NO.Value = ds.Tables(0).Rows(0)("VHD_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOC_NO)

                Dim sqlpDOC_TYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar)
                sqlpDOC_TYPE.Value = "BR"
                cmd.Parameters.Add(sqlpDOC_TYPE)

                Dim sqlpCHQ_NO As New SqlParameter("@CHQ_NO", SqlDbType.VarChar, 20)
                sqlpCHQ_NO.Value = ds.Tables(0).Rows(0)("VHD_CHQNO") & ""
                cmd.Parameters.Add(sqlpCHQ_NO)

                'If DateTime.Compare(ds.Tables(0).Rows(0)("VHD_CHQDT"), chqDate) = 0 Then
                Dim sqlpCHQ_DATE As New SqlParameter("@CHQ_DT", SqlDbType.DateTime)
                sqlpCHQ_DATE.Value = Format(chqDate, OASISConstants.DateFormat) 'ds.Tables(0).Rows(0)("VHD_CHQDT") & ""
                cmd.Parameters.Add(sqlpCHQ_DATE)
                'End If

                Dim sqlpVHD_ID As New SqlParameter("@VHD_ID", SqlDbType.VarChar, 20)
                sqlpVHD_ID.Value = ds.Tables(0).Rows(0)("VHD_ID") & ""
                cmd.Parameters.Add(sqlpVHD_ID)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                ' Dim str_err As String
                If (iReturnvalue = 0) Then
                    'stTrans.Rollback()
                    stTrans.Commit()
                Else
                    stTrans.Rollback()
                End If
                If (iReturnvalue = 0) Then
                    Return True
                Else
                    lblError.Text = getErrorMessage(iReturnvalue)
                    Return False
                End If ' ds.Tables(0).Rows(0)("ERR_MSG")
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return False
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
    End Function
    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()        
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If TypeOf e.Row.FindControl("txtChqDate") Is TextBox Then
            Dim txtChqDate As TextBox = e.Row.FindControl("txtChqDate")
            txtChqDate.Attributes.Add("onBlur", "checkdate(this)")
        End If
    End Sub
End Class
