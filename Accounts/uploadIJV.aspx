﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="UploadIJV.aspx.vb" Inherits="Accounts_UploadIJV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Upload Interunit Journal
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <tr id="trUpload" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Upload Excel File</span></td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="UploadFile" runat="server"
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                        <asp:Button ID="btnUploadPrf" runat="server" CssClass="button" Text="Upload" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Account</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAccount" runat="server" Text="24203014"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtUPDDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="lnkUPDDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtUPDDate" PopupButtonID="lnkUPDDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4">
                                        <asp:GridView ID="gvUpload" runat="server"
                                            EmptyDataText="No Data" Width="100%" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="BSU" HeaderText="BSU Code" />
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="BSU Name" />
                                                <asp:BoundField DataField="AMOUNT" HeaderText="Amount" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnUpdate" runat="server" CausesValidation="False" CssClass="button" Text="Post" TabIndex="5" Visible="false" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_Mode" runat="server" Value="Add" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" TabIndex="5" Visible="false" /></td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

