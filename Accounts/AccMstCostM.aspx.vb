Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class AccMstCostM
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            Try

                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
              
                gridbind()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            'Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            ' Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()

    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
       
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup1.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    

    Private Sub gridbind()
        Try
            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            str_mode = Request.QueryString("mode")
            Response.Write(str_mode)
            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""

            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup1.Rows.Count > 0 Then







                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND AM.CCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM.CCT_DESCR NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM.CCT_DESCR LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM.CCT_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM.CCT_DESCR LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM.CCT_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If
                ''name
               


            End If


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT CCT_ID as CHB_ID,CCT_DESCR as ACT_NAME" _
            & " FROM COSTCENTER_M AM WHERE 1=1"



            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code)
            gvGroup1.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup1.DataBind()
                Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup1.Rows(0).Cells.Clear()
                gvGroup1.Rows(0).Cells.Add(New TableCell)
                gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup1.DataBind()
                sp_message.InnerHtml = ""
            End If
            '  gvGroup.Columns(0).Visible = False
            'ddcust = gvGroup1.HeaderRow.FindControl("DDCutomerSupplier")



            txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup1.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            'txtSearch = gvGroup1.HeaderRow.FindControl("txtControl")
            'txtSearch.Text = str_txtControl

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub

    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        'set_Menu_Img()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lblSGPID As New Label
        Dim lblSGPDescr As New Label
        Dim lbClose As New LinkButton
        Dim lstrAccNum As String

        lbClose = sender

        lblcode = sender.Parent.FindControl("lblCode")
        lblSGPID = sender.Parent.FindControl("lblSGP_ID")
        lblSGPDescr = sender.Parent.FindControl("lblSGP_DESCR")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        ' --- Get The Max Number For That Account
        lstrAccNum = GetNext(lblcode.Text)



        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblSGPID.Text & "||" & lblSGPDescr.Text.Replace("'", "\'") & "||" & lstrAccNum & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub
    Public Function GetNext(ByVal pControlAcc As String) As String
        Dim lstrRetVal As String = String.Empty


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()

        Try
            Dim SqlCmd As New SqlCommand("NextAccountNo", objConn)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@pControlAcc", pControlAcc)
            SqlCmd.Parameters.AddWithValue("@pLength", 5)

            ' Dim pRetVal As New SqlParameter("@pReturnVal", SqlDbType.VarChar)
            'pRetVal.Direction = ParameterDirection.Output
            'SqlCmd.Parameters.Add(pRetVal)
            'SqlCmd.ExecuteNonQuery()
            lstrRetVal = SqlCmd.ExecuteScalar()
        Catch ex As Exception

        End Try

        Return lstrRetVal
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
