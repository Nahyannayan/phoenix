Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class AccTranPDCEntry
    Inherits System.Web.UI.Page 
    Dim Encr_decrData As New Encryption64 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            Dim url As String = ""
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            Select Case Session("MainMnu_code").ToString
                Case "A150013"
                    url = String.Format("AccAddPDC.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                    lblHead.Text = "PDC LIST "
                    lblHead2.Text = lblHead.Text
                Case "A150010"
                    lblHead.Text = "BANK PAYMENTS"
                    lblHead2.Text = lblHead.Text
                    url = String.Format("AccAddBankTran.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                Case "A150015"
                    url = String.Format("AccBankR.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                    lblHead.Text = "BANK RECEIPTS"
                    lblHead2.Text = lblHead.Text
                    gvGroup1.Columns(6).Visible = True
                Case OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER
                    url = String.Format("accFeeBankDepositVoucher.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                    optUnPosted.Checked = False
                    optPosted.Checked = True
                    lblHead.Text = "Bank Deposit Voucher"
                    lblHead2.Text = lblHead.Text
                Case OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT
                    url = String.Format("accFeeBankDepositVoucher.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                    optUnPosted.Checked = False
                    optPosted.Checked = True
                    lblHead.Text = "Bank Deposit Voucher"
                    lblHead2.Text = lblHead.Text
                Case OASISConstants.MNU_CHEQUE_PAYMENT
                    url = String.Format("AccChqPayment.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
                    lblHead.Text = "Cheque Payment Voucher"
                    lblHead2.Text = lblHead.Text
            End Select 
            
            hlAddnew.NavigateUrl = url
            Try
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                '   --- Lijo's Code ---
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (Session("MainMnu_code") <> "A150013" _
                And Session("MainMnu_code") <> OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER And Session("MainMnu_code") <> "A150015" _
                And Session("MainMnu_code") <> "A150010" And Session("MainMnu_code") <> OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT _
                And Session("MainMnu_code") <> OASISConstants.MNU_CHEQUE_PAYMENT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle
                    gvGroup1.Columns(6).Visible = False

                End If
                gridbind()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
        End If
    End Sub


    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvGroup1.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid("mnu_4_img", str_Sid_img(2))


        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub


    Private Sub gridbind()
        Dim lstrDocNo As String = String.Empty
        Dim lstrOldDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrBankAC As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltOldDocNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltBankAC As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltCurrency As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty
        Dim larrSearchOpr() As String
        Dim txtSearch As New TextBox

        If gvGroup1.Rows.Count > 0 Then
            ' --- Initialize The Variables 
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "A.VHH_DocNo", lstrDocNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "A.VHH_DocDt", lstrDocDate)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
            lstrBankAC = txtSearch.Text
            If (lstrBankAC <> "") Then lstrFiltBankAC = SetCondn(lstrOpr, "A.HeaderAccount", lstrBankAC)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtOldDocNo")
            lstrOldDocNo = txtSearch.Text
            If (lstrOldDocNo <> "") Then lstrFiltOldDocNo = SetCondn(lstrOpr, "A.VHH_REFNO", lstrOldDocNo)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "A.VHD_Narration", lstrNarration)

            '   -- 6 Amount
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
            lstrAmount = txtSearch.Text
            If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "A.VHD_AMOUNT", lstrAmount)
            '   -- 7  Currency
            'larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            'lstrOpr = larrSearchOpr(0)
            'txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
            'lstrCurrency = txtSearch.Text
            'If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "A.VHD_bBANKRECNO", lstrCurrency)
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String

        Dim str_Topfilter As String = ""
        If UsrTopFilter1.FilterCondition <> "All" Then
            str_Topfilter = " top " & UsrTopFilter1.FilterCondition
        End If
        str_Sql = "SELECT DISTINCT  " & str_Topfilter _
        & " A.GUID,a.vhh_docdt,A.VHH_DocNo as DocNo ,A.VHH_bauto, Replace(Left(Convert(VarChar,A.VHH_DOCDT,113),11),' ','/') as DocDate ,A.HeaderAccount ,Max(A.DetailAccount) as DetailAccount," _
        & " MAx(A.VHD_Narration) as Narration,Max(A.Cur_Descr) as Currency,Sum(A.VHD_AMOUNT) as Amount,Max(isNUll(VHH_REFNO,'')) as OldDocNo " _
        & " FROM vw_OSA_ALLVOUCHER A WHERE VHH_BSU_ID='" & Session("sBsuid") & "' AND VHH_FYEAR = " & Session("F_YEAR")
        str_Sql = str_Sql & lstrFiltDocNo & lstrFiltOldDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltNarration & lstrFiltAmount & lstrFiltCurrency
        If (Session("MainMnu_code") = "A150010") Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='BP' AND VHH_bPDC='False'"
        ElseIf (Session("MainMnu_code") = "A150015") Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='BR' AND VHH_bPDC='False'"
        ElseIf (Session("MainMnu_code") = "A150013") Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='BP' AND VHH_bPDC='True'"
        ElseIf (Session("MainMnu_code") = OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER) Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='QR' AND VHH_bPDC='False'"
        ElseIf (Session("MainMnu_code") = OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT) Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='QR' AND VHH_bPDC='False'"
        ElseIf (Session("MainMnu_code") = OASISConstants.MNU_CHEQUE_PAYMENT) Then
            str_Sql = str_Sql & " AND VHH_DOCTYPE='QP' AND VHH_bPDC='True'"
        End If
        Dim str_ListDoc As String = String.Empty
        If Session("ListDays") IsNot Nothing Then
            'If String.Compare(Session("ListDays"), "all", True) <> 0 Then
            str_ListDoc = " AND A.VHH_DOCDT BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
        End If
        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            str_Sql += str_ListDoc
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
            str_Sql += str_ListDoc
        End If

        If (Session("FetchType") = "0") Then
            str_Sql = str_Sql & " AND VHH_bPOsted='False'"
        ElseIf (Session("FetchType") = "1") Then
            str_Sql = str_Sql & " AND VHH_bPOsted='True'"
        End If
        str_Sql = str_Sql & "AND (VHH_bDELETED = 'False') GROUP BY A.VHH_DocDt,A.VHH_DocNo,A.HeaderAccount ,A.GUID, A.VHH_bauto ORDER BY 2 Desc,3 DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvGroup1.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup1.DataBind()
            Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count

            gvGroup1.Rows(0).Cells.Clear()
            gvGroup1.Rows(0).Cells.Add(New TableCell)
            gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvGroup1.DataBind()
        End If

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
        txtSearch.Text = lstrDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
        txtSearch.Text = lstrDocDate

        txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
        txtSearch.Text = lstrBankAC

        txtSearch = gvGroup1.HeaderRow.FindControl("txtOldDocNo")
        txtSearch.Text = lstrOldDocNo


        txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
        txtSearch.Text = lstrNarration

        txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
        txtSearch.Text = lstrAmount
        'txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
        'txtSearch.Text = lstrCurrency
        set_Menu_Img()
    End Sub


    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub optPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPosted.CheckedChanged
        gridbind()
    End Sub


    Protected Sub optAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAll.CheckedChanged
        gridbind()
    End Sub


    Protected Sub optUnPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optUnPosted.CheckedChanged
        gridbind()
    End Sub


    Protected Sub gvGroup1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup1.SelectedIndexChanged

    End Sub


    Protected Sub gvGroup1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim lblGUID As New Label
        lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
        Dim hlview As New HyperLink
        Dim eid, url As String
        url = ""
        Dim lblAmount As New Label
        Dim lnkAmount As New LinkButton
        Dim mnuCodeEnc As String = Session("MainMnu_code")
        lblAmount = TryCast(e.Row.FindControl("lblAmount"), Label)
        lnkAmount = TryCast(e.Row.FindControl("lnkAmount"), LinkButton)
        If lblAmount IsNot Nothing And lnkAmount IsNot Nothing Then
            If mnuCodeEnc = "A150015" Then
                lblAmount.Visible = False
                lnkAmount.Visible = True
            Else
                lblAmount.Visible = True
                lnkAmount.Visible = False
            End If
        End If
        hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
        If hlview IsNot Nothing And lblGUID IsNot Nothing Then
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            eid = Encr_decrData.Encrypt(lblGUID.Text)
            Session("datamode") = "view"
            Dim DataModeView As String = Encr_decrData.Encrypt("view")
            Select Case Session("MainMnu_code").ToString
                Case "A150013"
                    url = String.Format("AccAddPDC.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
                Case "A150010"
                    url = String.Format("AccAddBankTran.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
                Case "A150015"
                    url = String.Format("AccBankR.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
                Case OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER
                    url = String.Format("accFeeBankDepositVoucher.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
                Case OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT
                    url = String.Format("accFeeBankDepositVoucher.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
                Case OASISConstants.MNU_CHEQUE_PAYMENT
                    url = String.Format("AccChqPayment.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), DataModeView, eid)
            End Select
            hlview.NavigateUrl = url
        End If
    End Sub


    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vDOC_NO As String = contextKey
        Dim drReader As SqlDataReader
        Try
            Dim str_sql As String = "SELECT VHD_DOCNO, " & _
            "SUM(VOUCHER_D.VHD_AMOUNT) AS VHD_AMOUNT FROM VOUCHER_D " & _
            "WHERE  (VOUCHER_D.VHD_bBANKRECNO = '" & vDOC_NO & "') " & _
            "AND (VOUCHER_D.VHD_BSU_ID = '" & HttpContext.Current.Session("sBSUID") & "') " & _
            "GROUP BY VHD_DOCNO "

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=2><b>Related Vouchers</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>REC. No</b></td>")
            sTemp.Append("<td><b>AMOUNT</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                If drReader("VHD_DOCNO").ToString() = "" Then
                    Return ""
                End If
                sTemp.Append("<td>" & drReader("VHD_DOCNO").ToString & "</td>")
                sTemp.Append("<td>" & drReader("VHD_AMOUNT").ToString & "</td>")
                sTemp.Append("</tr>")
            End While
            '    If HttpContext.Current.Session("STUD_DET") Is Nothing Then
            '        sTemp.Append("</table>")
            '        Return sTemp.ToString()
            '    End If
            '    Dim httab As Hashtable = HttpContext.Current.Session("STUD_DET")
            '    Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(STUD_ID)
            '    If Not vFEE_PERF Is Nothing Then
            '        Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            '        Dim ienum As IEnumerator = arrList.GetEnumerator()
            '        While (ienum.MoveNext())
            '            Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
            '            sTemp.Append("<tr>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_ID.ToString & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_DESCR & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
            '            sTemp.Append("</tr>")
            '        End While
            '    End If
        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function


    Protected Sub gvGroup1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvGroup1.RowCreated
        If Session("MainMnu_code") = "A150015" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                ' Programmatically reference the PopupControlExtender 
                Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)

                ' Set the BehaviorID 
                Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
                pce.BehaviorID = behaviorID

                ' Programmatically reference the Image control 
                Dim i As LinkButton = DirectCast(e.Row.Cells(1).FindControl("lnkAmount"), LinkButton)

                ' Add the clie nt-side attributes (onmouseover & onmouseout) 
                Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
                Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)

                'i.Attributes.Add("onmouseover", OnMouseOverScript)
                i.Attributes.Add("onclick", OnMouseOverScript)
                i.Attributes.Add("onmouseout", OnMouseOutScript)
            End If
        End If
    End Sub
    Protected Function PrintFeeBankDepositVoucher(ByVal DocNo As String) As MyReportClass
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
        ' GetBankDepositData 
        '@VHD_DOCTYPE varchar(10),
        '@VHD_BSU_ID  varchar(20),
        '@VHD_FYEAR  varchar(20),
        '@VHD_SUB_ID  varchar(20),
        '@VHD_DOCNO  varchar(20)  ViewState("VHD_DOCNO")  ViewState("FYear")

        Dim cmd As New SqlCommand("GetBankDepositData", New SqlConnection(str_conn))
        cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "QR")
        cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
        cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
        cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
        cmd.Parameters.AddWithValue("@VHD_DOCNO", DocNo)
        cmd.CommandType = CommandType.StoredProcedure
        Dim ds As New DataSet
        Dim adp As New SqlDataAdapter(cmd)
        adp.Fill(ds)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True

            repSource.ResourceName = "../../fees/Reports/RPT/rptFeeBankDepositSlip.rpt"
            Session("ReportSource") = repSource
            Return repSource
        End If
    End Function
    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblDocNo As Label
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    Select Case Session("MainMnu_code").ToString
                        Case "A150013"
                            Dim PrintChq As Boolean = False
                            repSource = VoucherReports.PDCVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", lblDocNo.Text, PrintChq, Session("HideCC"))
                            If PrintChq Then
                                If repSource.Equals(Nothing) Then
                                    lblError.Text = "Cheque Format not supported for printing"
                                    Exit Sub
                                Else
                                    Session("ReportSource") = repSource
                                    Response.Redirect("accChqPrint.aspx?ChequePrint=PDC", True)
                                End If
                            End If
                        Case "A150017"
                            repSource = VoucherReports.PDCVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "QP", lblDocNo.Text, False, Session("HideCC"))
                        Case "A150010"
                            Dim PrintChq As Boolean = False
                            repSource = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", lblDocNo.Text, PrintChq, Session("HideCC"))
                            If PrintChq Then
                                If repSource.Equals(Nothing) Then
                                    lblError.Text = "Cheque Format not supported for printing"
                                    Exit Sub
                                Else
                                    Session("ReportSource") = repSource
                                    Response.Redirect("accChqPrint.aspx?ChequePrint=BP", True)
                                End If
                            End If
                        Case "A150015"
                            repSource = VoucherReports.BankReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BR", lblDocNo.Text, Session("HideCC"))
                        Case "F300120"
                            repSource = PrintFeeBankDepositVoucher(lblDocNo.Text)
                    End Select
                    Session("ReportSource") = repSource
                    '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                    ReportLoadSelection()

                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
