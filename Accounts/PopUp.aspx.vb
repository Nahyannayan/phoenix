Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj


Partial Class PopUp
    Inherits BasePage
    Dim lstrCheck As String
    Dim lstrSQL As String
    Dim lstrField1 As String
    Dim lstrField2 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lstrCheck = Request.QueryString("ShowType")
        If lstrCheck = "SUBGRP" Then
            lstrField1 = "SGP_ID"
            lstrField2 = "SGP_DESCR"
            lstrSQL = " SELECT " _
            & " AM.SGP_ID as Code ,AM.SGP_DESCR as Description " _
            & " FROM ACCSGRP_M AM WHERE 1=1"
        ElseIf lstrCheck = "RFS" Then
            lstrField1 = "RFS_ID"
            lstrField2 = "RFS_DESCR"
            lstrSQL = " SELECT " _
            & " AM.RFS_ID as Code ,AM.RFS_DESCR as Description " _
            & " FROM RPTLINES_S AM WHERE 1=1 AND RFS_TYP='" & Session("LIneType") & "'"
        ElseIf lstrCheck = "COLLN" Then
            lstrField1 = "COL_ID"
            lstrField2 = "COL_DESCR"
            lstrSQL = " SELECT " _
            & " AM.COL_ID as Code ,AM.COL_DESCR as Description " _
            & " FROM COLLECTION_M AM WHERE 1=1 "
        ElseIf lstrCheck = "RSS" Then
            lstrField1 = "RSS_SLNO"
            lstrField2 = "RSS_DESCR"
            lstrSQL = " SELECT " _
            & " AM.RSS_SLNO as Code ,AM.RSS_DESCR as Description " _
            & " FROM RPTSetUp_S AM WHERE 1=1 AND RSS_TYP='" & Session("LIneType") & "' ORDER BY RSS_SLNO"

        ElseIf lstrCheck = "BANK" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False'"
        ElseIf lstrCheck = "BANKBSU" Then
            Dim vbsu_id As String = IIf(Request.QueryString("BSU_ID") <> Nothing, Request.QueryString("BSU_ID"), "")
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE AM.ACT_BSU_ID Like   '%" & vbsu_id & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False'"
            'Response.Write(lstrSQL)
        ElseIf lstrCheck = "DOCTYPE" Then
            lstrField1 = "DOC_ID"
            lstrField2 = "DOC_NAME"
            lstrSQL = " SELECT " _
            & " AM.DOC_ID as Code ,AM.DOC_NAME as Description " _
            & " FROM DOCUMENT_M AM WHERE 1=1 "
        ElseIf lstrCheck = "INTRAC" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE 1=1 AND ACT_SGP_ID='" _
            & Session("IntrAC") & "'  AND ACT_Bctrlac='False'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("SBSUID") & "%'"
        ElseIf lstrCheck = "ACRDAC" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE 1=1 AND ACT_CTRLACC='" & Session("AcrdAc") & "'  AND ACT_Bctrlac='False'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("SBSUID") & "%'"
        ElseIf lstrCheck = "PREPDAC" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE 1=1 AND ACT_SGP_ID='" & Session("PrepdAC") & "'  AND ACT_Bctrlac='False'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("SBSUID") & "%'"
        ElseIf lstrCheck = "CHQISSAC" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "aCT_NAME"
            lstrSQL = " SELECT " _
            & " AM.ACT_ID as Code ,AM.ACT_NAME as Description " _
            & " FROM ACCOUNTS_M AM WHERE 1=1 AND ACT_SGP_ID='" & Session("ChqissAC") & "'  AND ACT_Bctrlac='False'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("SBSUID") & "%'"

        ElseIf lstrCheck = "CCM" Then
            lstrField1 = "CCT_ID"
            lstrField2 = "CCT_DESCR"
            lstrSQL = " SELECT " _
            & " AM.CCT_ID as Code ,AM.CCT_DESCR as Description " _
            & " FROM COSTCENTER_M AM WHERE 1=1 "
        ElseIf lstrCheck = "CCS" Then
            lstrField1 = "CCS_ID"
            lstrField2 = "CCS_DESCR"
            lstrSQL = " SELECT " _
            & " AM.CCS_ID as Code ,AM.CCS_DESCR as Description " _
            & " FROM COSTCENTER_S AM WHERE 1=1 AND isNull(CCS_CCT_ID,'')<>'9999'"
        ElseIf lstrCheck = "CTY" Then
            lstrField1 = "CTY_ID"
            lstrField2 = "CTY_DESCR"
            lstrSQL = " SELECT " _
            & " AM.CTY_ID as Code ,AM.CTY_DESCR as Description " _
            & " FROM vw_OSO_COUNTRY_M AM WHERE 1=1 "
        ElseIf lstrCheck = "RRY" Then
            lstrField1 = "ACT_ID"
            lstrField2 = "ACT_DISPLAYNAME"
            lstrSQL = " SELECT " _
            & " DISTINCT AM.ACT_ID as Code ,AM.ACT_DISPLAYNAME as Description " _
            & " FROM OASIS_FEES.FEES.FEES_M A INNER JOIN OASISFIN.dbo.ACCOUNTS_M AM ON A.FEE_CHRG_INADV_ACT_ID=AM.ACT_ID "

        End If


        If Page.IsPostBack = False Then

            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                ' h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()
                
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()

    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        'str_Sid_img = h_Selected_menu_3.Value.Split("__")
        'getid2(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            str_mode = Request.QueryString("mode")
            Response.Write(str_mode)
            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""

            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            '''''''''''''''''''


            ' ''If IsNumeric(str_CodeorName) = True Then
            ' ''    str_txtCode = str_CodeorName
            ' ''    str_filter_code = " AND AM." & lstrField1 & " LIKE '%" & str_CodeorName & "%'"

            ' ''Else
            ' ''    str_txtName = str_CodeorName
            ' ''    str_filter_name = " AND AM." & lstrField2 & " LIKE '%" & str_CodeorName & "%'"
            ' ''End If

            ''''''''''''''''
            ''''''''''
            Dim str_CodeorName As String = Request.QueryString("codeorname") & ""

            Dim i As Integer
            Dim iCharcount As Integer = -1
            Dim ch As String
            For i = 1 To Len(str_CodeorName)
                ch = Mid(str_CodeorName, i, 1)
                If IsNumeric(ch) = False Then
                    iCharcount = iCharcount + 1
                    If ch = "-" Then
                        iCharcount = 100
                    End If
                End If
            Next

            If IsNumeric(str_CodeorName) And iCharcount = -1 Then
                str_txtCode = str_CodeorName
                str_filter_code = " AND AM." & lstrField1 & " LIKE '%" & str_CodeorName & "%'"
            Else
                If iCharcount = 0 Then
                    str_txtCode = str_CodeorName
                    str_filter_code = " AND AM." & lstrField1 & " LIKE '%" & str_CodeorName & "%'"
                Else
                    str_txtName = str_CodeorName
                    str_filter_name = " AND AM." & lstrField2 & " LIKE '%" & str_CodeorName & "%'"
                End If
            End If
            'If IsNumeric(str_CodeorName) = True Then

            'End If

            ''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND AM." & lstrField1 & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM." & lstrField1 & " NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM." & lstrField1 & " LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM." & lstrField1 & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM." & lstrField1 & " LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM." & lstrField1 & " NOT LIKE '%" & txtSearch.Text & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_name = " AND AM." & lstrField2 & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = " AND AM." & lstrField2 & " NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND AM." & lstrField2 & " LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND AM." & lstrField2 & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND AM." & lstrField2 & " LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND AM." & lstrField2 & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString




            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSQL & str_filter_code & str_filter_name)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName


            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("lblCode")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")


            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

   
End Class
