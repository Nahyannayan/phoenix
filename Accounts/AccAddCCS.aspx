<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="True" CodeFile="AccAddCCS.aspx.vb" Inherits="AccAddCCS" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetCostCenter() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            //result = window.showModalDialog("showOtherCostCenter.aspx", "", sFeatures)
            var result = radopen("showOtherCostCenter.aspx", "pop_up2")
           <%-- if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtCCSCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCCSDescr.ClientID %>').value = NameandCode[1];
            }
            return false;--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCCSCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtCCSDescr.ClientID%>').value = NameandCode[1];


            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Cost Center Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <br />
                <table align="center" width="100%">

                    <tr id="mainCostCenter" visible="false" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cost Center Main</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCCMCode" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="javascript:popUp('460','400','CCM','<%=txtCCMCode.ClientId %>','<%=txtCCMDescr.ClientId %>') "></asp:ImageButton>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCCMDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Parent</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCCSCode" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetCostCenter(); return false;"></asp:ImageButton>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCCSDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="trCode" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Code</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCode" runat="server" MaxLength="20"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Description</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDescr" runat="server" MaxLength="100"></asp:TextBox></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnEdit_Click" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server"
                                Text="Save" CssClass="button" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnCancel_Click" Text="Cancel" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

