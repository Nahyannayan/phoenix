Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Acc_ImportMISView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "ImportMIS.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvMISView.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "A200359" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""

                    lblHead.Text = "Import MIS Data"
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvMISView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvMISView.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvMISView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvMISView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvMISView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvMISView.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvMISView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvMISView.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""
            Dim lstrCondn2, lstrCondn5, lstrCondn6, lstrCondn7 As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn2 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvMISView.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvMISView.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MIH_DATE", lstrCondn2)
                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvMISView.HeaderRow.FindControl("txtBSUName")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSU_NAME", lstrCondn5)
                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvMISView.HeaderRow.FindControl("txtRemarks")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MIH_REMARKS", lstrCondn7)
            End If

            If RdPost.Checked Then
                str_Filter = str_Filter & " AND isnull(MIH_Bposted,0) = 1 "
            Else
                str_Filter = str_Filter & " AND isnull(MIH_Bposted,0) = 0 "
            End If

            str_Sql = " select distinct MIH_ID,MIH_DATE,BU.BSU_NAME,MIH_REMARKS"
            str_Sql = str_Sql & " from MIS_IMPORT_H MIH,MIS_IMPORT_D MID,OASIS.dbo.BUSINESSUNIT_M BU"
            str_Sql = str_Sql & "  where MIH.MIH_ID = Mid.MID_MIH_ID And MIH_BSU_ID = BU.BSU_ID"
            str_Sql = str_Sql & " AND MIH_BSU_ID = '" & Session("sBSUID") & "' " & str_Filter
            str_Sql = str_Sql & " ORDER BY MIH_DATE DESC , MIH_ID DESC "

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            gvMISView.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvMISView.DataBind()
                Dim columnCount As Integer = gvMISView.Rows(0).Cells.Count
                gvMISView.Rows(0).Cells.Clear()
                gvMISView.Rows(0).Cells.Add(New TableCell)
                gvMISView.Rows(0).Cells(0).ColumnSpan = columnCount
                gvMISView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvMISView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvMISView.DataBind()
            End If
            txtSearch = gvMISView.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvMISView.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = lstrCondn5

            txtSearch = gvMISView.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvMISView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMISView.PageIndexChanging
        gvMISView.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvMISView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMISView.RowDataBound
        Try
            Dim lbVoucher As New HyperLink
            lbVoucher = TryCast(e.Row.FindControl("lbView"), HyperLink)
            If lbVoucher IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                lbVoucher.NavigateUrl = "ImportMIS.aspx?viewid=" & Encr_decrData.Encrypt(e.Row.Cells(0).Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            e.Row.Cells(0).Style("display") = "none"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub RdPost_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub RdOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

End Class
