Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO



Partial Class accMstCC
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = String.Format("~/accounts/accAddEditCC.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                Dim MainMnu_code As String = String.Empty

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or (MainMnu_code <> "A100030" And MainMnu_code <> "A100035" And MainMnu_code <> "P050008" And MainMnu_code <> "P050007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If MainMnu_code = "A100030" Or MainMnu_code = "P050008" Then

                        ViewState("str_Sql") = "Select * from (SELECT CIT_ID as ID,CIT_DESCR as DESCR   FROM CITY_M )a where id<>'' "
                        ViewState("Etype") = "C"
                        lblHead.Text = "City"
                    ElseIf MainMnu_code = "A100035" Or MainMnu_code = "P050007" Then

                        ViewState("str_Sql") = "Select * from (SELECT CTY_ID as ID,CTY_DESCR as DESCR  FROM COUNTRY_M)a where id<>'' "
                        ViewState("Etype") = "G"
                        lblHead.Text = "Country"

                    End If


                    gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception

                lblError.Text = "Request could not be processed "
            End Try

        End If

        set_Menu_Img()
    End Sub


    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim ds As New DataSet

            Dim str_filter_code, str_filter_name, str_txtCode, str_txtName As String
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If gvCommonCC.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = gvCommonCC.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = gvCommonCC.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, ViewState("str_Sql") & str_filter_code & str_filter_name & "  order by a.ID")
            gvCommonCC.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then


                gvCommonCC.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCommonCC.DataBind()
                Dim columnCount As Integer = gvCommonCC.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCommonCC.Rows(0).Cells.Clear()
                gvCommonCC.Rows(0).Cells.Add(New TableCell)
                gvCommonCC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommonCC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommonCC.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            set_Menu_Img()
            txtSearch = gvCommonCC.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvCommonCC.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)



        End Try
    End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonCC.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonCC.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonCC.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonCC.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()

    End Sub

    Protected Sub gvCommonCC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCommonCC.PageIndexChanging
        gvCommonCC.PageIndex = e.NewPageIndex
        gridbind()
    End Sub



    Protected Sub lbView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            Dim lblGuid As New Label
            Dim url As String
            Dim viewid As String
            Dim mainMnu_code As String = String.Empty
            lblGuid = TryCast(sender.FindControl("lblcode"), Label)
            viewid = lblGuid.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            mainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            ViewState("Etype") = Encr_decrData.Encrypt(ViewState("Etype"))
            url = String.Format("~\Accounts\accAddEditCC.aspx?MainMnu_code={0}&datamode={1}&viewid={2}&Etype={3}", mainMnu_code, ViewState("datamode"), viewid, ViewState("Etype"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed "
        End Try




    End Sub
End Class
