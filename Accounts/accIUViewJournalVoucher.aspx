<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accIUViewJournalVoucher.aspx.vb" Inherits="accIUViewJournalVoucher"
    Title="Untitled Page" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Interunit Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" border="0">
                    <tr>
                        <td width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" EnableViewState="False"></asp:Label>

                        </td>
                        <td align="right" colspan="2">
                            <asp:RadioButton ID="rbUnPosted" runat="server" GroupName="optControl" Text="Open"
                                AutoPostBack="True" Checked="True" />
                            <asp:RadioButton ID="rbPosted" runat="server" GroupName="optControl" Text="Posted"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbAll" runat="server" GroupName="optControl" Text="All" AutoPostBack="True" />
                        </td>
                    </tr>

                </table>
                <table width="100%" border="0">
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" Width="100%"
                                DataKeyNames="GUID" AllowPaging="True" EmptyDataText="No Data Found" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No
                                <br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("IJH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date
                                <br />
                                            <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("IJH_DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Unit (DR)
                                <br />
                                            <asp:TextBox ID="txtBankAC" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAC" runat="server" Text='<%# Bind("DR_BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Narration
                                <br />
                                            <asp:TextBox ID="txtNarrn" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarrn" runat="server" Text='<%# Bind("IJH_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Unit (CR)
                                <br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodewww" runat="server" Text='<%# Bind("CR_BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IJH_bMIRROR" HeaderText="Mirror Entry" SortExpression="IJH_bMIRROR">
                                        <HeaderStyle Font-Size="8pt" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Differ Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJH_DIFFAMT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DEBIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("DEBITTOTAL")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CREDIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("CREDITTOTAL")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("POSTED")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                OnClick="lbView_Click" Text="Summary"></asp:LinkButton>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Posted" Visible="false">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPosted" Text='<%# Bind("POSTED") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" DataKeyNames="IJL_ID" CssClass="table table-bordered table-row"
                                EmptyDataText="NO TRANSACTION DETAILS ADDED YET" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="IJL_DOCNO" HeaderText="Document No" SortExpression="IJL_DOCNO" />
                                    <asp:BoundField DataField="IJL_NARRATION" HeaderText="Narration" SortExpression="IJL_NARRATION" />
                                    <asp:BoundField DataField="IJL_ACT_ID" HeaderText="Account No" SortExpression="IJL_ACT_ID" />
                                    <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" SortExpression="ACT_NAME" />
                                    <asp:TemplateField HeaderText="DEBIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJL_DEBIT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CREDIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJL_CREDIT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbViewChild" runat="server" CausesValidation="false" CommandName=""
                                                OnClick="lbViewChild_Click" Text="View"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="slno" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("IJL_SLNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" valign="top">
                            <br />
                            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT" CssClass="table table-bordered table-row"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="VDS_ACT_ID" HeaderText="Account No" SortExpression="VDS_ACT_ID" />
                                    <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                        Visible="False" />
                                    <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                    <asp:BoundField DataField="VDS_CODE" HeaderText="CODE" SortExpression="VDS_CODE" />
                                    <asp:BoundField DataField="VDS_DESCR" HeaderText="Name" SortExpression="VDS_DESCR" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VDS_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit/Credit" SortExpression="VDS_DRCR">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCrDb" runat="server" Text='<%# returnCrDb(Container.DataItem("VDS_DRCR")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>



                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>
