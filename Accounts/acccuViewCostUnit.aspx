<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="acccuViewCostUnit.aspx.vb" Inherits="Accounts_acccuViewCostUnit" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Memo JV Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <a id='top'></a>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="center" width="100%">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Cash Receipt Vouchers found" Width="100%" AllowPaging="True">
                                <EmptyDataRowStyle Wrap="True" />
                                <Columns>
                                    <asp:BoundField DataField="CUT_DESCR" HeaderText="Description" ReadOnly="True"
                                        SortExpression="CUT_DESCR">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COST_DESCR" HeaderText="Type">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CUT_ITEM" HeaderText="Item">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PPAID" HeaderText="Prepaid Account" SortExpression="PPAID">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EXP" HeaderText="Expense Account" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGuid" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <a id='detail'></a>
                            <br />
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

