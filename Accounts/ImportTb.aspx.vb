Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_ImportTb
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""

        txtExchangerate.Attributes.Add("onkeypress", " return Numeric_Only()")
        'uploadFile.Attributes.Add("onblur", "javascript:UploadPhoto();")
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            filldrp()
            doClear()


            initialize_components()


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200354"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200354") Then
                'And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351"
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("MainMnu_code").Equals("A200354") Then
                    'btnAdd.Visible = False
                    'btnSave.Visible = True

                End If
            End If
            'If Request.QueryString("invalidedit") = "1" Then
            '    lblError.Text = getErrorMessage("Invalid Editid")
            '    Exit Sub
            'End If

            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
    End Sub
    Public Sub getdata(ByVal filePath As String)

        Try

            Dim excelQuery As String = ""
            'and year(DT) = " & Session("F_YEAR") & "  

            'excelQuery = " SELECT * FROM  [TableName] "
          

            excelQuery = " SELECT DT,ACT_TYPE,ACT_ID,ACT_NAME,DR,CR FROM  [TableName] " _
            & " where month(DT) = month('" & txtHDocdate.Text.ToString() & "') " _
            & " AND (DR <> 0 OR CR <> 0) and DT is not null  "

            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As DataTable
            _table = Mainclass.FetchFromExcel(excelQuery, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)

        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If


    End Sub

    Public Sub filldrp()
        Try


            Dim Query As String
            Query = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M  WHERE BSU_ID = '" & Session("sBsuid") & "' ORDER BY BSU_NAME "
            fillDropdown(drpBsunit, Query, "BSU_NAME", "BSU_ID", "OASISConnectionString", False)
            'drpBsunit.SelectedValue = Session("sBsuid").ToString()
            Query = "SELECT EXGRATE_S.EXG_RATE, CURRENCY_M.CUR_ID FROM CURRENCY_M INNER JOIN EXGRATE_S ON CURRENCY_M.CUR_ID = EXGRATE_S.EXG_CUR_ID WHERE EXG_BSU_ID = '" & Session("sBsuid").ToString() & "'"
            'WHERE EXG_BSU_ID = '" & Session("sBsuid").ToString() & "'
            fillDropdown(drpCurrency, Query, "CUR_ID", "EXG_RATE", "mainDB", False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
    End Sub


    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        ' bind_Currency()
        'getnextdocid()
    End Sub





    Private Function CheckSave() As Boolean
        Dim retValue As Boolean = True
        If txtHDocdate.Text.Equals("") Then
            lblError.Text = "Invalid Date...!"

        ElseIf txtExchangerate.Text.Equals("") Then
            lblError.Text = "Invalid Exchange Rate..!"
        ElseIf drpBsunit.SelectedValue.Equals("0") Then
            lblError.Text = "Select Business Unite.!"
        ElseIf h_Editid.Equals("0") Then
            lblError.Text = "Could not prosess you are request Please contact Admin...!"
        Else
            retValue = False

        End If
        Return retValue


    End Function
    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        'drpBsunit.SelectedValue = "0"
        txtExchangerate.Text = drpCurrency.SelectedValue.ToString()
        h_Editid.Value = "0"
        btnSave.Visible = False
        ' RdExcel.Visible = False
        btnFind.Enabled = True
        btnAdd.Visible = True

    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try


            If CheckSave.Equals(True) Then
                Exit Sub
            End If

            doInsert("SAVE")
            doClear()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'ViewState("datamode") = "add"
        doClear()
        'Session("dtSettle") = DataTables.CreateDataTable_Settle
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnSave.Visible = False

    End Sub
    Private Sub fillOtherdrp(ByVal selectId As String, ByVal drpObj As DropDownList)

        Dim Sqlstring As String
        Sqlstring = "SELECT ACT_ID ,ACT_NAME FROM ACCOUNTS_M WHERE ACT_BSU_ID Like   '%" & selectId & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False' AND isnull(ACT_BACTIVE,0)=1 ORDER BY ACT_NAME"
        fillDropdown(drpObj, Sqlstring, "ACT_NAME", "ACT_ID", "MainDB", True)
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Master.DisableScriptManager()
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad

    End Sub

    Private Sub UpLoadExcel()




        If uploadFile.HasFile Then



            'Dim filePath As String = Server.MapPath("")
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            If Not Directory.Exists(filePath & "\temp") Then
                Directory.CreateDirectory(filePath & "\temp")
            End If
            filePath = filePath & "\temp\" & uploadFile.FileName 'FileName

            If uploadFile.HasFile Then
                uploadFile.SaveAs(filePath)

                Try

                    getdata(filePath)
                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                    End If


                    'If Directory.Exists(Server.MapPath("") & "\ExcelTemp") Then
                    'Directory.Delete(Server.MapPath("") & "\ExcelTemp")
                    'End If
                Catch ex As Exception
                    lblError.Text = ex.Message
                End Try
            End If

        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If (h_Editid.Value = "0") Then

            If Not uploadFile.HasFile Then
                'If (HidUpload.Value.Equals("")) Then
                lblError.Text = "Select Excel Sheet...!"
                Exit Sub

            End If
            If Not (uploadFile.FileName.EndsWith("xls") Or uploadFile.FileName.EndsWith("xlsx")) Then
                lblError.Text = "Invalid file type.. Only excel files are alowed.!"
                Exit Sub
            End If
            If txtHDocdate.Text.Equals("") Then
                lblError.Text = "Invalid Date..."
                Exit Sub
            End If
            UpLoadExcel()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            'Exceldata(True)
            lblError.Text = "Data already retrieved "
        End If

    End Sub


    Protected Sub gvExcel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub
    Private Sub InsertTemtable(ByVal _table As DataTable)

        If _table.Rows.Count > 0 Then

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim _parameter(1) As SqlClient.SqlParameter


            Dim Query, SessId As String
            SessId = Session.SessionID.ToString()
            Query = "DELETE FROM ExcelExport " 'WHERE SessionId = '" & SessId & "'"
            _parameter(0) = New SqlClient.SqlParameter("@Query", SqlDbType.VarChar, 2000)
            _parameter(0).Value = Query
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "QueryInsert", _parameter)

            Dim x As Int32
            For x = 0 To _table.Rows.Count - 1
                Dim AcName, AcNo, AcDt, AcType As String
                AcName = _table.Rows(x)("ACT_NAME").ToString().Replace("'", "")
                AcNo = _table.Rows(x)("ACT_ID").ToString().Replace("'", "")
                AcDt = _table.Rows(x)("DT").ToString().Replace("'", "")
                AcType = _table.Rows(x)("ACT_TYPE").ToString().Replace("'", "")


                Query = "INSERT INTO ExcelExport (TransDate,AccountType,AccountNo,AccountName,DRAmount,CRAmount,SessionId)" _
                & " values ('" & _table.Rows(x)("DT") & "','" & AcType & "','" & AcNo & "'," _
                & " '" & AcName & "'," & _table.Rows(x)("DR") & "," & _table.Rows(x)("CR") & " ,'" & SessId & "')"
                _parameter(0) = New SqlClient.SqlParameter("@Query", SqlDbType.VarChar, 2000)
                _parameter(0).Value = Query

                If AcNo.Equals("") Then
                    stTrans.Rollback()
                    lblError.Text = "Invalid Account.Please Try again..." & AcName
                    Exit Sub
                End If
                Dim retval As Int32 = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "QueryInsert", _parameter)


            Next
            stTrans.Commit()
            h_Editid.Value = "1"
            btnSave.Visible = True
            btnFind.Enabled = False
            Exceldata("EXCEL")


        End If

    End Sub
    Private Sub Exceldata(ByVal Excel As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        If Not Excel.Equals("EXCEL") Then
            If CheckSave.Equals(True) Then
                Exit Sub
            End If
        End If
        If Excel.Equals("EXCEL") Then
            labdetailHead.Text = "Excel Details"
        ElseIf Excel.Equals("CURRENT") Then
            labdetailHead.Text = "Current Account Details"
        ElseIf Excel.Equals("BOTH") Then
            labdetailHead.Text = "Current Account with Excel Details"
        End If

        Dim _parameter As String(,) = New String(4, 1) {}
        _parameter(0, 0) = "@MODE"
        _parameter(0, 1) = Excel
        _parameter(1, 0) = "@TBHDATE"
        _parameter(1, 1) = Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(2, 0) = "@BUSID"
        _parameter(2, 1) = drpBsunit.SelectedValue.ToString()
        _parameter(3, 0) = "@TDHFYEAR"
        _parameter(3, 1) = Session("F_YEAR").ToString()
        _parameter(4, 0) = "@SESSIONID"
        _parameter(4, 1) = Session.SessionID.ToString()


        Dim _table As DataTable = MainObj.getRecords("ListTBDATA_D", _parameter, "MainDB")
        gvExcel.DataSource = _table
        gvExcel.DataBind()
        If _table.Rows.Count.Equals(0) Then
            If Excel.Equals("EXCEL") Then
                lblError.Text = "Please Check Excel Sheet"
            Else
                lblError.Text = "No details added ..!"
            End If
        Else
            ViewState("datamode") = "add"
        End If

        If gvExcel.Rows.Count > 0 Then
            balAmount = Math.Round(balAmount, 3)

            
            gvExcel.FooterRow.Cells(4).Text = DrAmount.ToString("#,###0.00")
            gvExcel.FooterRow.Cells(4).HorizontalAlign = HorizontalAlign.Right
            gvExcel.FooterRow.Cells(5).Text = CrAmount.ToString("#,###0.00")
            gvExcel.FooterRow.Cells(5).HorizontalAlign = HorizontalAlign.Right
            gvExcel.FooterRow.Cells(6).Text = balAmount.ToString("#,###0.00")
            gvExcel.FooterRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
            gvExcel.FooterRow.Height = 25
            If RdExcel.Checked = False Then
                gvExcel.FooterRow.Cells(4).Style("display") = "none"
                gvExcel.FooterRow.Cells(5).Style("display") = "none"
                gvExcel.FooterRow.Cells(0).Style("display") = "none"


                gvExcel.HeaderRow.Cells(4).Style("display") = "none"
                gvExcel.HeaderRow.Cells(5).Style("display") = "none"
                gvExcel.HeaderRow.Cells(0).Style("display") = "none"

            End If

        End If

    End Sub
    Public balAmount As Double = 0
    Public CrAmount As Double = 0
    Public DrAmount As Double = 0


    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)


        If Not e.Row.RowIndex.Equals(-1) Then
            If RdExcel.Checked = False Then
                e.Row.Cells(4).Style("display") = "none"
                e.Row.Cells(5).Style("display") = "none"
                e.Row.Cells(0).Style("display") = "none"

            End If
            balAmount += Convert.ToDouble(e.Row.Cells(6).Text.ToString())
            DrAmount += Convert.ToDouble(e.Row.Cells(4).Text.ToString())
            CrAmount += Convert.ToDouble(e.Row.Cells(5).Text.ToString())

        End If
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub doInsert(ByVal Mode As String)
        Dim _parameter As String(,) = New String(11, 1) {}

        Dim InsertMode As Int32 = 1
        If Mode.Equals("CHECK") Then
            InsertMode = 2
        End If
        _parameter(0, 0) = "@MODE"
        _parameter(0, 1) = InsertMode.ToString()
        _parameter(1, 0) = "@TDH_FYEAR"
        _parameter(1, 1) = Session("F_YEAR").ToString() 'Session("Sub_ID").ToString()
        _parameter(2, 0) = "@TDH_DATE"
        _parameter(2, 1) = Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(3, 0) = "@TDH_BSU_ID"
        _parameter(3, 1) = drpBsunit.SelectedValue.ToString()  'Session("sBsuid").ToString() 'Session("F_YEAR").ToString()
        _parameter(4, 0) = "@TDH_CUR_ID"
        _parameter(4, 1) = drpCurrency.SelectedItem.Text.ToString()
        _parameter(5, 0) = "@TDH_EXGRATE"
        _parameter(5, 1) = txtExchangerate.Text.ToString()
        _parameter(6, 0) = "@SESSIONID"
        _parameter(6, 1) = Session.SessionID.ToString() 'Page.User.Identity.Name.ToString()
        _parameter(7, 0) = "@AUD_WINUSER"
        _parameter(7, 1) = Page.User.Identity.Name.ToString()
        _parameter(8, 0) = "@Aud_form"
        _parameter(8, 1) = Master.MenuName.ToString()
        _parameter(9, 0) = "@Aud_user"
        _parameter(9, 1) = Session("sUsr_name").ToString()
        _parameter(10, 0) = "@Aud_module"
        _parameter(10, 1) = Session("sModule")



        MainObj.doExcutiveRetvalue("SaveTBDATA_H", _parameter, "mainDB", "")
        lblError.Text = MainObj.MESSAGE

        If MainObj.SPRETVALUE.Equals(0) Then
            btnAdd.Visible = True
            btnSave.Visible = False
            btnFind.Enabled = True
        End If


    End Sub

    Protected Sub RdExcel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Exceldata("EXCEL")
    End Sub

    Protected Sub RdCheck_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Exceldata("CURRENT")
    End Sub

    Protected Sub RdExcandAc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Exceldata("BOTH")
    End Sub
End Class

