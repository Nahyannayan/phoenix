Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Accounts_PdcCancelpost
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        If Page.IsPostBack = False Then
            lblHead.Text = "Posting PDC Cancellation / Deferral " 'Master.MenuName

            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")

            Try
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")
               
                '   --- Lijo's Code ---
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or Session("MainMnu_code") <> "A200099" Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle


                End If
                gridbind()
                If Session("datamode") = "none" Then
                    Session("datamode") = "add"
                End If
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub


    Private Sub gridbind()
       
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String

        Dim CurDate As String = DateTime.Now.ToString("dd/MMM/yyyy")
        str_Sql = " SELECT   VHR.VHR_ID,  REPLACE(CONVERT(CHAR,VHH.VHH_DOCDT,106),' ','/') AS VHH_DOCDT, " & _
        " VHD.VHD_CHQNO, REPLACE(CONVERT(CHAR,VHD.VHD_CHQDT,106),' ','/') AS VHD_CHQDT, " & _
        " BANK.ACT_NAME AS BANK, PARTY.ACT_NAME AS PARTY, VHD.VHD_AMOUNT,REPLACE(CONVERT(CHAR,VHR.VHR_CHQDT,106),' ','/') AS VHR_CHQDT, " & _
        " VHH.VHH_DOCNO, VHH.VHH_DOCTYPE, VHD.VHD_NARRATION , CASE WHEN VHR_bCANCELLED = 1 THEN 'Cancellation' ELSE 'Deferral' END AS DOACTION " & _
        " FROM  VOUCHER_H AS VHH INNER JOIN   " & _
        " VOUCHER_D AS VHD ON VHH.VHH_SUB_ID = VHD.VHD_SUB_ID AND VHH.VHH_BSU_ID = VHD.VHD_BSU_ID AND  " & _
        " VHH.VHH_FYEAR = VHD.VHD_FYEAR AND VHH.VHH_DOCTYPE = VHD.VHD_DOCTYPE AND VHH.VHH_DOCNO = VHD.VHD_DOCNO INNER JOIN " & _
        " ACCOUNTS_M AS BANK ON VHH.VHH_ACT_ID = BANK.ACT_ID LEFT OUTER JOIN ACCOUNTS_M AS PARTY ON VHH.VHH_ISSUEDTO = PARTY.ACT_ID " & _
        " INNER JOIN VOUCHER_D_REVISION AS VHR ON VHD.VHD_ID = VHR.VHR_VHD_ID   " & _
        " AND VHD.VHD_BSU_ID = VHR.VHR_BSU_ID AND VHD.VHD_FYEAR = VHR.VHR_FYEAR  " & _
        " WHERE VHH.VHH_BSU_ID ='" & Session("sBsuid") & "'  " & _
        " AND (isnull(VHR.VHR_bDeleted,0)=0)  " & _
        " AND isnull(VHR.VHR_bPosted,0)=0 "

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvGroup1.DataSource = ds
        gvGroup1.DataBind()
        If ds.Tables(0).Rows.Count = 0 Then
            TablePost.Visible = False
            
        End If

        
    End Sub

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

  

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            doInsert()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub
    Private Sub doInsert()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            For Each grow As GridViewRow In gvGroup1.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@VHR_ID", SqlDbType.Int)
                    pParms(0).Value = ChkSelect.Value
                    pParms(1) = New SqlClient.SqlParameter("@VHR_BSU_ID", SqlDbType.VarChar)
                    pParms(1).Value = Session("sBsuid")
                    pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(2).Direction = ParameterDirection.ReturnValue
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "PostChequeUpdate", pParms)
                    If pParms(2).Value = "0" Then
                        lblError.Text = getErrorMessage(pParms(2).Value)
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ChkSelect.Value, "Post", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        lblError.Text = getErrorMessage(pParms(2).Value)
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    End If
                End If

            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            stTrans.Rollback()
            objConn.Close()
        End Try
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        gridbind()
    End Sub
   
End Class

