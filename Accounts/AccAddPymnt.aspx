<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccAddPymnt.aspx.vb" Inherits="AccAddPymnt" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Payment Terms
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%"
                                align="center" border="0">
                                <tbody>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblErr" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table width="100%">

                                <tr>
                                    <td align="left"><span class="field-label">Description</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDescr" runat="server"
                                            MaxLength="100"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Allow PDC</span></td>
                                    <td align="left">
                                        <asp:RadioButton ID="optYes" runat="server" Checked="True" GroupName="optAllowPDC" CssClass="field-label"
                                            Text="Yes" />
                                        <asp:RadioButton
                                            ID="optNo" runat="server" GroupName="optAllowPDC" CssClass="field-label"
                                            Text="No" />
                                        <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                                    </td>
                                    <td><span class="field-label">Days</span></td>
                                    <td>
                                        <asp:TextBox ID="txtDays" runat="server" MaxLength="3"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server"
                                            Text="SAVE" CssClass="button" />
                                        <asp:Button ID="btnCancel" runat="server"
                                            Text="Cancel" CssClass="button" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

