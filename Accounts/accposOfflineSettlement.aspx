<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accposOfflineSettlement.aspx.vb" Inherits="Accounts_accposOfflineSettlement" Title="Settlement" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
            table td input[type=text],table td select {
                min-width:20% !important;
            }
        </style>
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }
        function Changet(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.value = '1';
        }
        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++) {
                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);

                    Changet(TextBoxDeposit[i], lstrChk); Changet(TextBoxPayment[i], lstrChk);
                }
            }
        }

        function ChangeTotal(checkState) {
            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++)
                    //TextBoxPayment//TextBoxDeposit  txtSAlloted
                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);
            }
        }
        function OnChange() {
            try {
                if (TextBoxAllocated != null && TextBoxAllocated != undefined) {
                    var sum = 0.0;
                    for (var i = 0; i < TextBoxAllocated.length; i++) {
                        TB = document.getElementById(TextBoxAllocated[i]);
                        if (TB != null) { //alert('1');
                            if (TB.value != '') {
                                temp = 0.0;
                                temp = sum + parseFloat(document.getElementById(TextBoxAllocated[i]).value);
                                //alert(temp);
                                if (isNaN(temp))
                                    document.getElementById(TextBoxAllocated[i]).value = '0';
                                else
                                    sum = sum + parseFloat(document.getElementById(TextBoxAllocated[i]).value);
                            }
                            else {
                                document.getElementById(TextBoxAllocated[i]).value = '0';
                            }
                        } //if
                    }//for txtBalance
                    document.getElementById('<%=txtSAlloted.ClientID %>').value = sum;
            document.getElementById('<%=txtBalance.ClientID %>').value = document.getElementById('<%=txtTBalance.ClientID %>').value - sum;
            }
        }//try
        catch (ex)
        { }
    }
    //TextBoxDate

    
          function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


            var lstrVal;
            var lintScrVal;

            document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
            document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
            document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var NameandCode;
            var result;
            if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");

                //if (result=='' || result==undefined)
                //{    return false;      } 
                //lstrVal=result.split('||');     
                //document.getElementById(ctrl).value=lstrVal[0];
                //document.getElementById(ctrl1).value=lstrVal[1];              
            }
        }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode=='ALLACC'||pMode=='BANKONLY'||pMode=='CASHONLY'||pMode=='CUSTSUPP'||pMode=='CUSTSUPPnIJV') {
                        document.getElementById(ctrl).value=NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                        __doPostBack(ctrl, 'TextChanged');
                    }

                }
            }

        function getAccountH() {
            popUp('960', '600', 'CUSTSUPP', '<%=txtHAccountcode.ClientId %>', '<%=txtHAccountname.ClientId %>');
            return false;
        }

    </script>
    
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Settlement of Creditors
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Supplier</span></td>

                                    <td align="left" valign="middle" colspan="2"> 

                                        <asp:TextBox ID="txtHAccountcode" runat="server" Width="20%" OnTextChanged="txtHAccountcode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="btnHaccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccountH(); return false;" />
                                        <asp:TextBox ID="txtHAccountname" runat="server" Width="60%"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                                <tr class="gridheader_pop">
                                    <td align="left" valign="middle" ><span class="field-label">Payments</span></td>
                                    
                                        <td align="center" colspan="3" width="100%" valign="middle">
                                            <asp:Panel ID="Panel1" runat="server"  Width="100%" ScrollBars="Auto">
                                                <asp:GridView ID="gvTrans" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                    EmptyDataText="There is no transactions" Width="100%" DataKeyNames="TRN_BSU_ID,TRN_FYEAR,TRN_DOCTYPE,TRN_DOCNO,TRN_LINEID,TRN_ACT_ID">
                                                    <EmptyDataRowStyle Wrap="True" />
                                                    <Columns>
                                                        <asp:BoundField DataField="TRN_DOCNO" HeaderText="Docno" ReadOnly="True" SortExpression="TRN_DOCNO" />
                                                        <asp:BoundField DataField="TRN_DOCDT" HeaderText="Date" SortExpression="TRN_DOCDT" HtmlEncode="False" ReadOnly="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_CUR_ID" HeaderText="Currency" SortExpression="TRN_CUR_ID" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_AMOUNT" HeaderText="Amount" SortExpression="TRN_AMOUNT" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_ALLOCAMT" HeaderText="Allocated" SortExpression="TRN_ALLOCAMT" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Balance">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblBalance" runat="server" Text='<%# Bind("BALANCE") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBalance" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("BALANCE")) %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="TRN_INVNO" HeaderText="Invoice no" SortExpression="TRN_INVNO" ReadOnly="True" />
                                                        <asp:BoundField DataField="TRN_CHQNO" HeaderText="Chqno" SortExpression="TRN_CHQNO" ReadOnly="True" />
                                                        <asp:BoundField DataField="TRN_VOUCHDT" HeaderText="VDate" SortExpression="TRN_VOUCHDT" ReadOnly="True" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_NARRATION" HeaderText="Narration" SortExpression="TRN_NARRATION" ReadOnly="True" />
                                                        <asp:TemplateField ShowHeader="False" HeaderText="Settle">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                    Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="Edit" runat="server" CausesValidation="False" CommandName="Edit"
                                                                    Text="Settle"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="GUID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGuid" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="griditem"  />
                                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                                    <HeaderStyle CssClass="gridheader_new"  />
                                                    <EditRowStyle CssClass="griditem_hilight" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="title-bg">
                                        <td align="left" colspan="4" >Receipts/Invoices</td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4" width="100%">
                                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Width="100%">
                                                <asp:GridView ID="gvSettle" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                    EmptyDataText="There is no transactions" Width="100%" DataKeyNames="TRN_BSU_ID,TRN_FYEAR,TRN_DOCTYPE,TRN_DOCNO,TRN_LINEID,TRN_ACT_ID">
                                                    <EmptyDataRowStyle Wrap="True" />
                                                    <Columns>
                                                        <asp:BoundField DataField="TRN_DOCNO" HeaderText="Docno" ReadOnly="True" SortExpression="TRN_DOCNO" />
                                                        <asp:BoundField DataField="TRN_DOCDT" HeaderText="Date" SortExpression="TRN_DOCDT" HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_CUR_ID" HeaderText="Currency" SortExpression="TRN_CUR_ID">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_AMOUNT" HeaderText="Amount" SortExpression="TRN_AMOUNT">
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_ALLOCAMT" HeaderText="Allocated" SortExpression="TRN_ALLOCAMT">
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_INVNO" HeaderText="Invoice No" SortExpression="TRN_INVNO" />
                                                        <asp:BoundField DataField="TRN_CHQNO" HeaderText="Chqno" SortExpression="TRN_CHQNO" />
                                                        <asp:BoundField DataField="BALANCE" HeaderText="Balance">
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Allocate Amount">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtSAllocated" runat="server" OnPreRender="txtSAllocated_PreRender">0</asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="TRN_VOUCHDT" HeaderText="VDate" SortExpression="TRN_VOUCHDT" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRN_NARRATION" HeaderText="Narration" SortExpression="TRN_NARRATION" />
                                                        <asp:TemplateField HeaderText="GUID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGuid" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="griditem" Height="25px" />
                                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4" >
                                           <span class="field-label"> Amount to Settle </span>
                                <asp:TextBox ID="txtTBalance" runat="server" Width="20%"
                                   >0</asp:TextBox>
                                      <span class="field-label">      Amount Alloted  </span>
                                <asp:TextBox ID="txtSAlloted" runat="server" ReadOnly="True" Width="20%"
                                    >0</asp:TextBox>
                                          <span class="field-label">  Balance  </span>
                                <asp:TextBox ID="txtBalance" runat="server" ReadOnly="True" Width="20%" >0</asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr id="tr_SaveButtons" runat="server">
                                        <td align="center" colspan="4" >
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                            <asp:Button ID="btnPreview" runat="server" CssClass="button" Text="Preview" ValidationGroup="Details" CausesValidation="False" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button"
                                                Text="Cancel" /><br />
                                        </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_selected" runat="server" type="hidden" value="" />

            </div>
        </div>
    </div>
</asp:Content>

