Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Accounts_ShowPrepaid
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()

    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim str_filter_acctype As String = String.Empty
            Dim str_mode As String = String.Empty
            Dim str_Prepaid As String = String.Empty
            Dim str_CHQISSAC As String = String.Empty
            Dim str_search As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_debit As String = String.Empty
           
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            
            If str_mode = "PREPDAC" Then
                str_Prepaid = " AND AM.ACT_SGP_ID='" & Session("PrepdAC") & "'"

            End If

            If str_mode = "CHQISSAC_PDC" Then
                str_CHQISSAC = " AND AM.ACT_SGP_ID='" & Session("ChqissAC") & "'"
            End If
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Try
                    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                Catch ex As Exception
                End Try
                
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & Trim(txtSearch.Text) & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & Trim(txtSearch.Text) & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "'"
                End If 
            End If


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT " _
            & " AM.ACT_ID ,AM.ACT_NAME ,AM.ACT_SGP_ID ,AM.ACT_TYPE," _
            & " CASE AM.ACT_BANKCASH" _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END " _
            & " ACT_BANKCASH" _
            & " ,AM.ACT_CTRLACC ,AM.ACT_Bctrlac ," _
            & " CASE AM.ACT_FLAG" _
            & " WHEN 'S' THEN 'Supliers'" _
            & " WHEN 'C' THEN 'Customers'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Others' END" _
            & " ACT_FLAG ," _
            & " AMC.ACT_NAME CTRL_NAME,isNULL(AM.PLY_COSTCENTER ,'OTH') as Ply,AM.PLY_BMANDATORY as CostReqd" _
            & " FROM vw_OSA_ACCOUNTS_M AM " _
            & " WHERE AM.ACT_Bctrlac='FALSE'" _
            & " AND AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%' " _
            & " AND AM.ACT_BACTIVE='TRUE'"

            str_Sql = "SELECT AM.ACT_ID, " _
            & " AM.ACT_NAME, AM.ACT_SGP_ID," _
            & " AM.ACT_CTRLACC, AM.ACT_Bctrlac, " _
            & " AM.ACT_OPP_ACT_ID AS RET_CODE," _
            & " AMOPP.ACT_NAME AS RET_NAME " _
            & " FROM vw_OSA_ACCOUNTS_M AS AM INNER JOIN" _
            & " ACCOUNTS_M AS AMOPP " _
            & " ON AM.ACT_OPP_ACT_ID = AMOPP.ACT_ID" _
            & " WHERE AM.ACT_Bctrlac='FALSE'" _
            & " AND AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%' " _
            & " AND AM.ACT_BACTIVE='TRUE'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_Prepaid & str_filter_debit & str_filter_acctype & str_CHQISSAC & str_filter_code & str_filter_name)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
           
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
        
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub




    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub



    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        'set_Menu_Img()
    End Sub



    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton

        Dim lblRET_NAME As New Label
        Dim lblRET_CODE As New Label
        lbClose = sender

        lblcode = sender.Parent.FindControl("lblCode")
        lblcode.Text = lblcode.Text.Replace("___", "||")
        lblRET_NAME = sender.Parent.FindControl("lblRET_NAME")
        lblRET_CODE = sender.Parent.FindControl("lblRET_CODE")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRET_CODE.Text & "||" & lblRET_NAME.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRET_CODE.Text & "||" & lblRET_NAME.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRET_CODE.Text & "||" & lblRET_NAME.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



End Class
