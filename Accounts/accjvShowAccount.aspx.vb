Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Collections.Generic
Imports UtilityObj

Partial Class ShowAccount
    Inherits BasePage  
    Shared multiSel As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            multiSel = IIf(Request.QueryString("multiSelect") <> Nothing, Request.QueryString("multiSelect"), False)
            If multiSel Then
                gvGroup.Columns(0).Visible = True
                gvGroup.Columns(3).Visible = True
                gvGroup.Columns(2).Visible = False
                Button2.Visible = True
            Else
                gvGroup.Columns(0).Visible = False
                gvGroup.Columns(3).Visible = False
                gvGroup.Columns(2).Visible = True 
                Button2.Visible = False
                chkSelAll.Visible = False
            End If

            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6") 
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                Session("liUserList") = New List(Of String)
                gridbind()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            Response.Write("} </script>" & vbCrLf)
            h_SelectedId.Value = ""
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        set_Menu_Img()
        SetChk(Me)
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
    End Sub


    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try
            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp,str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = "" 
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = "" 
            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Try
                    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                    ddcust = gvGroup.HeaderRow.FindControl("DDCutomerSupplier")
                    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                    ddbank = gvGroup.HeaderRow.FindControl("DDBankorCash")
                Catch ex As Exception
                End Try

                If ddacctype.SelectedItem.Value <> "All" Then
                    str_filter_acctype = " AND AM.ACT_TYPE='" & ddacctype.SelectedItem.Value & "'"
                End If 
                If ddcust.SelectedItem.Value = "C" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='C'"
                ElseIf ddcust.SelectedItem.Value = "S" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='S'"
                ElseIf ddcust.SelectedItem.Value = "N" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='N'"
                End If
                i_dd_bank = ddbank.SelectedIndex
                i_dd_acctype = ddacctype.SelectedIndex
                i_dd_custsupp = ddcust.SelectedIndex
                If Not Request.QueryString("bankmis") Is Nothing Then
                    str_filter_bankcash = " and act_id in (select rpa_acc_id from RPTSETUP_S_ACC inner join RPTSETUPSUB_S on rsb_id=rpa_rsb_id where rsb_rss_code='A028') "
                Else
                    If Request.QueryString("bankcash") <> "" Then
                        str_filter_bankcash = " AND AM.ACT_BANKCASH='" & Request.QueryString("bankcash") & "'"
                    Else
                        If ddbank.SelectedItem.Value = "B" Then
                            str_filter_bankcash = " AND AM.ACT_BANKCASH='B'"
                        ElseIf ddbank.SelectedItem.Value = "C" Then
                            str_filter_bankcash = " AND AM.ACT_BANKCASH='C'"
                        ElseIf ddbank.SelectedItem.Value = "N" Then
                            str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                        End If
                    End If
                End If
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = set_search_filter("AM.ACT_ID", str_search, txtSearch.Text)
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtActName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                End If
                str_txtName = txtSearch.Text
                str_filter_name = set_search_filter("AM.ACT_NAME", str_search, txtSearch.Text)
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = set_search_filter("AMC.ACT_NAME", str_search, txtSearch.Text)
            End If
            If Request.QueryString("bankmis") Is Nothing Then
                If Request.QueryString("bankcash") <> "" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='" & Request.QueryString("bankcash") & "'"
                End If
            Else
                str_filter_bankcash = " and am.act_id in (select rpa_acc_id from RPTSETUP_S_ACC inner join RPTSETUPSUB_S on rsb_id=rpa_rsb_id where rsb_rss_code='A028') "
            End If
            Dim mode As String = IIf(Request.QueryString("mode") <> "", Request.QueryString("mode"), String.Empty)
            If mode <> "" Then
                If mode = "g" Then
                    str_filter_custsupp = " AND isnull(AM.ACT_FLAG,'') NOT IN ('S','C')"
                Else
                    str_filter_custsupp = " AND AM.ACT_FLAG='" & mode & "'"
                End If
            End If
            Dim type As String = IIf(Request.QueryString("type") <> "", Request.QueryString("type"), String.Empty)
            If type <> "" Then
                Select Case type
                    Case "exp"
                        str_filter_custsupp += " AND AM.ACT_TYPE = 'EXPENSES'"
                    Case "pre"
                        str_filter_custsupp += " AND AM.ACT_TYPE = 'ASSET'"
                    Case "EXPINC"
                        str_filter_custsupp += " AND AM.ACT_TYPE in( 'EXPENSES','INCOME')"
                End Select
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT " _
            & " AM.ACT_ID ,AM.ACT_NAME ,AM.ACT_SGP_ID ,AM.ACT_TYPE," _
            & " CASE AM.ACT_BANKCASH" _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END " _
            & " ACT_BANKCASH" _
            & " ,AM.ACT_CTRLACC ,AM.ACT_Bctrlac ," _
            & " CASE AM.ACT_FLAG" _
            & " WHEN 'S' THEN 'Supliers'" _
            & " WHEN 'C' THEN 'Customers'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Others' END" _
            & " ACT_FLAG ," _
            & " AMC.ACT_NAME CTRL_NAME" _
            & " FROM ACCOUNTS_M AM, ACCOUNTS_M AMC" _
            & " WHERE AMC.ACT_ID=AM.ACT_CTRLACC"
            If Not Request.QueryString("bankmis") Is Nothing Then
                'str_Sql &= " AND AM.ACT_BSU_ID LIKE '%" & Session("sBSUID") & "%'"
            Else
                str_Sql &= " AND AM.ACT_BSU_ID LIKE '%" & Session("sBSUID") & "%'"
            End If
            If Not Request.QueryString("CONTROLACC") Is Nothing Then
                str_Sql += " AND AM.ACT_Bctrlac=1 "
            Else
                str_Sql += " AND AM.ACT_Bctrlac=0 "
            End If
            If Not Request.QueryString("forrpt") Is Nothing Then
                If Request.QueryString("forrpt").ToString <> "1" Then
                    str_Sql = str_Sql & " AND AM.ACT_BACTIVE = 1 "
                End If
            End If


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_bankcash & str_filter_acctype & str_filter_custsupp & str_filter_code & str_filter_name & str_filter_control)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            If Request.QueryString("bankcash") <> "" Then
                gvGroup.Columns(4).Visible = False
            End If
            '  gvGroup.Columns(0).Visible = False
            ddcust = gvGroup.HeaderRow.FindControl("DDCutomerSupplier")
            ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
            ddbank = gvGroup.HeaderRow.FindControl("DDBankorCash")

            ddbank.SelectedIndex = i_dd_bank
            ddacctype.SelectedIndex = i_dd_acctype
            ddcust.SelectedIndex = i_dd_custsupp

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtActName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            End If
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl

            set_Menu_Img()
            SetChk(Me)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub


    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub


    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function


    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind() 
    End Sub


    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender 
        lblcode = sender.Parent.FindControl("lblCode")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function


    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

    End Sub


    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        Dim Actreader As DataTableReader
        gridbind()
        If chkSelAll.Checked Then
            Dim ds As DataSet
            ds = gvGroup.DataSource
            Actreader = ds.CreateDataReader()

            Dim strAccessibleBSUIDs As String = String.Empty

            If Actreader.HasRows = True Then
                While (Actreader.Read())
                    Session("liUserList").Remove(Actreader(0))
                    Session("liUserList").Add(Actreader(0))
                End While
            End If
        Else
            Session("liUserList").Clear()
        End If
        gridbind()
    End Sub


End Class
