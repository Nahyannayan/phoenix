<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ImportMISPost.aspx.vb" Inherits="Acc_ImportMISPost" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
 
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvMISView" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True"
                                PageSize="25" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="MIH_ID" HeaderText="MIH_ID" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server"
                                                Text='<%# Bind("MIH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            Business Unit<br />
                                            <asp:TextBox ID="txtBSUName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Narration">
                                        <HeaderTemplate>
                                            Narration<br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("MIH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="button" CausesValidation="False" OnClick="btnPost_Click"></asp:Button>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

