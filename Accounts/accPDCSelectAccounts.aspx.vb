Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accPDCSelectAccounts
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        txtIntr.Attributes.Add("Readonly", "Readonly")
        txtAcrd.Attributes.Add("Readonly", "Readonly")
        txtPrepd.Attributes.Add("Readonly", "Readonly")
        txtChqiss.Attributes.Add("Readonly", "Readonly")

        txtIntrDescr.Attributes.Add("Readonly", "Readonly")
        txtAcrdDescr.Attributes.Add("Readonly", "Readonly")
        txtPrepdDescr.Attributes.Add("Readonly", "Readonly")
        txtChqissDescr.Attributes.Add("Readonly", "Readonly")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
                Dim CurBsUnit As String = Trim(Session("sBsuid") & "")
                Dim USR_NAME As String = Trim(Session("sUsr_name") & "")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "A100075") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("MainMnu_code") = "A100075" Then
                        If ViewState("datamode") = "view" Then
                            SetViewData()
                            Dim STR_SQL As String = "SELECT     VHS.VHS_ID, VHS.VHS_DESCRIPTION, VHS.VHS_ACR_INT_ACT_ID, " _
                              & " VHS.VHS_INT_ACT_ID, VHS.VHS_PREP_EXP_ACT_ID, VHS.VHS_CHQ_ISS_ACT_ID, " _
                              & " ACT_ACR.ACT_NAME AS VHS_ACR_INT_ACT_DESCR, ACT_INT.ACT_NAME AS VHS_INT_ACT_DESCR," _
                              & " ACT_PRE.ACT_NAME AS VHS_PREP_EXP_ACT_DESCR, ACT_CHQ.ACT_NAME AS VHS_CHQ_ISS_ACT_DESCR" _
                              & " FROM VOUCHERSETUP_S AS VHS LEFT OUTER JOIN" _
                              & " ACCOUNTS_M AS ACT_INT ON VHS.VHS_INT_ACT_ID = ACT_INT.ACT_ID LEFT OUTER JOIN" _
                              & " ACCOUNTS_M AS ACT_PRE ON VHS.VHS_PREP_EXP_ACT_ID = ACT_PRE.ACT_ID LEFT OUTER JOIN" _
                              & " ACCOUNTS_M AS ACT_CHQ ON VHS.VHS_CHQ_ISS_ACT_ID = ACT_CHQ.ACT_ID LEFT OUTER JOIN" _
                              & " ACCOUNTS_M AS ACT_ACR ON VHS.VHS_ACR_INT_ACT_ID = ACT_ACR.ACT_ID" _
                              & " WHERE     (VHS.VHS_ID = '" & Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) & "') "

                            Dim ds As DataSet
                            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)

                            If ds.Tables(0).Rows.Count > 0 Then
                                txtRemarks.Text = ds.Tables(0).Rows(0)("VHS_DESCRIPTION").ToString
                                txtIntr.Text = ds.Tables(0).Rows(0)("VHS_INT_ACT_ID").ToString
                                txtAcrd.Text = ds.Tables(0).Rows(0)("VHS_ACR_INT_ACT_ID").ToString
                                txtPrepd.Text = ds.Tables(0).Rows(0)("VHS_PREP_EXP_ACT_ID").ToString
                                txtChqiss.Text = ds.Tables(0).Rows(0)("VHS_CHQ_ISS_ACT_ID").ToString

                                txtIntrDescr.Text = ds.Tables(0).Rows(0)("VHS_INT_ACT_DESCR").ToString
                                txtAcrdDescr.Text = ds.Tables(0).Rows(0)("VHS_ACR_INT_ACT_DESCR").ToString
                                txtPrepdDescr.Text = ds.Tables(0).Rows(0)("VHS_PREP_EXP_ACT_DESCR").ToString
                                txtChqissDescr.Text = ds.Tables(0).Rows(0)("VHS_CHQ_ISS_ACT_DESCR").ToString
                            End If
                        Else
                            ResetViewData()
                        End If
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ResetViewData()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtRemarks.Text = "" Then
            lblError.Text = "Remarks Cannor be empty"
            Exit Sub
        End If

        If txtIntr.Text = "" And txtAcrd.Text = "" And txtPrepd.Text = "" And txtChqiss.Text = "" Then
            lblError.Text = "Please alect atleast one account"
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim flagAudit As Integer
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            If ViewState("datamode") = "edit" Then
                Dim str_update_id As String = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                retval = AccountFunctions.SaveVOUCHERSETUP_S(str_update_id, "BP", txtRemarks.Text, txtAcrd.Text, txtIntr.Text, _
                 txtPrepd.Text, txtChqiss.Text, stTrans)

                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, _
                 str_update_id, "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            Else
                retval = AccountFunctions.SaveVOUCHERSETUP_S(0, "BP", txtRemarks.Text, txtAcrd.Text, txtIntr.Text, _
              txtPrepd.Text, txtChqiss.Text, stTrans)
                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, _
                "", _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub


    Sub ResetViewData()
        imgAcrdint.Enabled = True
        imgChqiss.Enabled = True
        imgInterest.Enabled = True
        imgPrepaid.Enabled = True
    End Sub


    Sub SetViewData()
        imgAcrdint.Enabled = False
        imgChqiss.Enabled = False
        imgInterest.Enabled = False
        imgPrepaid.Enabled = False
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clear_All()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Sub clear_All()
        txtRemarks.Text = ""
        txtIntr.Text = ""
        txtAcrd.Text = ""
        txtPrepd.Text = ""
        txtChqiss.Text = ""

        txtIntrDescr.Text = ""
        txtAcrdDescr.Text = ""
        txtPrepdDescr.Text = ""
        txtChqissDescr.Text = ""

    End Sub


End Class
