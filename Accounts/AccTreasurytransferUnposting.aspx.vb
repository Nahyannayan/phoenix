Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_AccTreasurytransferUnposting
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            ''''' 
            Page.Title = OASISConstants.Gemstitle
            'bind_Vouchertype()
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A200353") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            ''''
            'gvChild.Attributes.Add("bordercolor", "#1b80b6")
            'gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            txtTodate.Text = GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(GetDiplayDate()).AddMonths(-2))

            gridbind()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                chkPost.Checked = False
                'If chkPost.Checked = True Then
                '    chkPost.Disabled = True
                'Else
                '    If hlCEdit IsNot Nothing And lblGUID IsNot Nothing Then
                '        ' Dim i As New Encryption64
                '        hlCEdit.NavigateUrl = "acccpCashPayments.aspx?editid=" & e.Row.Cells(1).Text
                '        hlview.NavigateUrl = "acccpCashPayments.aspx?viewid=" & e.Row.Cells(1).Text
                '        'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                '        hlCEdit.Enabled = True
                '    End If
                'End If
            End If

        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    
    Private Function findUnpostvalue() As String
        Dim postId, DocNo As String
        postId = ""
        Dim chk As HtmlInputCheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            chk = DirectCast(grow.FindControl("chkPosted"), HtmlInputCheckBox)
            If chk.Checked = True Then
                DocNo = grow.Cells(0).Text.ToString()
                postId = doUnpost(chk.Value.ToString(), DocNo)
                Return postId
                Exit Function
            End If
        Next
        If postId.Equals("") Then
            postId = "Could not prosess you are request Please Select DOCNO..!"
        End If
        Return postId
    End Function


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click

        Try
            If txtNarration.Text = "" Then
                Throw New Exception("Invalid Narration...!")
            End If
            Dim retMsg As String = findUnpostvalue()
            lblError.Text = retMsg
            gridbind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

        'Find_Checked(Me.Page)
        'gridbind()
        txtNarration.Text = ""
    End Sub


    

    Private Sub search_gridbind()
        Dim str_filter As String = " AND ITF_DR_DOCNO LIKE '%" & txtDocno.Text & "%' "
        If ddVouchertype.SelectedValue.Equals("BP") Then
            str_filter = " AND ITF_CR_DOCNO LIKE '%" & txtDocno.Text & "%' "
        End If
        Dim strfDate As String = txtFromDate.Text.Trim
        Dim strTDate As String = txtTodate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)

        Dim str_JHD_DOCDT As String = "ITF_DATE"
        

        If str_err = "" And CheckBox1.Checked = False Then
            txtTodate.Text = strTDate
            str_filter = str_filter & " AND ITF_DATE = '" & txtFromDate.Text & "' "
        Else
            lblError.Text = str_err
        End If

        str_err = str_err & DateFunctions.checkdate(strTDate)


        If str_err = "" And CheckBox1.Checked = True Then
            str_filter = str_filter & " AND ITF_DATE BETWEEN '" & txtFromDate.Text & "' AND '" & txtTodate.Text & "'"
            txtFromDate.Text = strfDate
        End If
        If str_err <> "" And CheckBox1.Checked = True Then
            lblError.Text = "To Date is not valid"
        End If
        gridbind(str_filter)
    End Sub

    Private Sub gridbind(Optional ByVal str_filter As String = "")
        Try
            If ddVouchertype.SelectedIndex = -1 Then
                ddVouchertype.SelectedIndex = 0
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String


          

            If ddVouchertype.SelectedItem.Value = "BP" Then
                str_Sql = " SELECT IRF_ID,ITF_CR_DOCNO AS DOCNO,ITF_DATE,ITF_NARRATION,ITF_CUR_ID,ITF_AMOUNT,ITF_bPOSTED FROM INTERUNIT_TRANSFER " _
                            & " WHERE ITF_bPOSTED = 1 And ITF_bDELETED = 0 And ITF_bPOSTEDCRBSU = 1  AND ITF_BSU_ID = '" & Session("sBsuid") & "' AND ITF_FYEAR = " & Session("F_YEAR") & " " _
                            & str_filter _
                            & " ORDER BY ITF_DATE, IRF_ID DESC"
           
            ElseIf ddVouchertype.SelectedItem.Value = "BR" Then
                str_Sql = " SELECT IRF_ID,ITF_DR_DOCNO AS DOCNO ,ITF_DATE,ITF_NARRATION,ITF_CUR_ID,ITF_AMOUNT ,ITF_bPOSTED FROM INTERUNIT_TRANSFER " _
                        & " WHERE ITF_bPOSTED = 1 And ITF_bDELETED = 0 And ITF_bPOSTEDDRBSU = 1  AND ITF_BSU_ID = '" & Session("sBsuid") & "' AND ITF_FYEAR = " & Session("F_YEAR") & " " _
                        & str_filter _
                        & " ORDER BY ITF_DATE, IRF_ID DESC"


            Else
                str_Sql = " SELECT IRF_ID,ITF_DR_DOCNO AS DOCNO,ITF_DATE,ITF_NARRATION,ITF_CUR_ID,ITF_AMOUNT,ITF_bPOSTED  FROM INTERUNIT_TRANSFER " _
                                        & " WHERE ITF_bPOSTED = 1 And ITF_bDELETED = 0  " _
                                        & " And ITF_bPOSTEDDRBSU = 0 And ITF_bPOSTEDCRBSU = 0 AND ITF_BSU_ID = '" & Session("sBsuid") & "' AND ITF_FYEAR = " & Session("F_YEAR") & " " _
                                        & str_filter _
                                        & " ORDER BY ITF_DATE, IRF_ID DESC"
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
            Else
                btnPost.Visible = False
            End If
            gvJournal.DataBind()
            For Each gvr As GridViewRow In gvJournal.Rows
                'Get a programmatic reference to the CheckBox control
                Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkPosted"), HtmlInputCheckBox)
                If cb IsNot Nothing Then
                    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Private Function doUnpost(ByVal PostValue As String, ByVal UnpostDocno As String) As String
        Dim MainObj As Mainclass = New Mainclass()
        Dim _parameter As String(,) = New String(12, 1) {}



        _parameter(0, 0) = "@VHH_SUB_ID"
        _parameter(0, 1) = Session("Sub_ID").ToString()
        _parameter(1, 0) = "@BSU_ID"
        _parameter(1, 1) = Session("sBsuid").ToString()
        _parameter(2, 0) = "@VHH_FYEAR"
        _parameter(2, 1) = Session("F_YEAR").ToString()
        _parameter(3, 0) = "@IRF_ID"
        _parameter(3, 1) = PostValue
        _parameter(4, 0) = "@AUD_WINUSER"
        _parameter(4, 1) = Page.User.Identity.Name.ToString()
        _parameter(5, 0) = "@Aud_form"
        _parameter(5, 1) = Master.MenuName.ToString()
        _parameter(6, 0) = "@Aud_user"
        _parameter(6, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(7, 0) = "@Aud_module"
        _parameter(7, 1) = HttpContext.Current.Session("sModule")
        _parameter(8, 0) = "@Narration"
        _parameter(8, 1) = txtNarration.Text.Trim().ToString()
        _parameter(9, 0) = "@DOCTYPE"
        _parameter(9, 1) = ddVouchertype.SelectedValue.ToString().Trim()
        _parameter(10, 0) = "@DOCNO"
        _parameter(10, 1) = UnpostDocno
        _parameter(11, 0) = "@ReturnValue"
        _parameter(11, 1) = "0"

        MainObj.doExcutive("UnpostINTERUNIT_TRANSFER", _parameter, "mainDB", "@v_ReturnMsg")

        'MainObj.doExcutive("UnpostINTERUNIT_TRANSFER", _parameter, "mainDB")
        Return MainObj.MESSAGE
    End Function

    Protected Sub ddVouchertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddVouchertype.SelectedIndexChanged
        gridbind()
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        search_gridbind()
    End Sub
End Class
