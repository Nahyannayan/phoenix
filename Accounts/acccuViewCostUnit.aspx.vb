Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_acccuViewCostUnit
    Inherits System.Web.UI.Page


    Dim MainMnu_code As String 
    Dim Encr_decrData As New Encryption64
 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            lblError.Text = ""
            ''''' 
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A100070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))

            End If

            ''''

           
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
            If Request.QueryString("deleted") <> "" Then
                lblError.Text = getErrorMessage("520")
            End If
            Dim url As String
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            url = "acccuCostUnit.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & dataModeAdd

            hlAddNew.NavigateUrl = url

        End If
    End Sub


    Public Function returnpath(ByVal p_posted As Boolean) As String
        If p_posted Then
            Return "~/Images/tick.gif"
        Else
            Return "~/Images/cross.gif"
        End If
    End Function


    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If

    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
 
    End Sub


    ''Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
    ''    Try
    ''        Dim lblGUID As New Label
    ''        lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
    ''        Dim cmdCol As Integer = gvJournal.Columns.Count - 1
    ''        Dim hlCEdit As New HyperLink
    ''        Dim hlview As New HyperLink
    ''        hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
    ''        If hlview IsNot Nothing And lblGUID IsNot Nothing Then
    ''            ' Dim i As New Encryption64
    ''            ' ''
    ''            ' ''datamode = "add"
    ''            ' ''MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)

    ''            ' ''Dim Queryusername As String = Session("sUsr_name")

    ''            datamode = Encr_decrData.Encrypt("view")
    ''            hlview.NavigateUrl = "acccuCostUnit.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode
    ''            'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
    ''            hlCEdit.Enabled = True
    ''        End If
    ''        'End If
    ''    Catch ex As Exception
    ''        Errorlog(ex.Message)
    ''    End Try
    ''End Sub


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""
         

        str_Sql = "SELECT COSTUNIT_M.GUID,COSTUNIT_M.CUT_ID, COSTUNIT_M.CUT_DESCR, " _
  & " COSTUNIT_M.CUT_TYPE, COSTUNIT_M.CUT_ITEM, " _
  & " COSTUNIT_M.CUT_PPAID_ACT_ID, COSTUNIT_M.CUT_EXP_ACT_ID, " _
  & " COSTTYPE_M.COST_DESCR," _
  & " COSTUNIT_M.CUT_PPAID_ACT_ID+' - '+PPAID.ACT_NAME AS PPAID, " _
  & " COSTUNIT_M.CUT_EXP_ACT_ID+' - '+EXP.ACT_NAME AS EXP" _
  & " FROM ACCOUNTS_M AS PPAID INNER JOIN" _
  & " COSTUNIT_M INNER JOIN COSTTYPE_M" _
  & " ON COSTUNIT_M.CUT_TYPE = COSTTYPE_M.COST_ID " _
  & " ON PPAID.ACT_ID = COSTUNIT_M.CUT_PPAID_ACT_ID " _
  & " INNER JOIN ACCOUNTS_M AS EXP" _
  & " ON COSTUNIT_M.CUT_EXP_ACT_ID = EXP.ACT_ID"
        '" SELECT * FROM JOURNAL_H WHERE VHD_bDELETED='False' AND VHD_BSU_ID='" _
        '        & Session("sBsuid") & "' ORDER BY VHD_DOCDT DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        gvJournal.DataBind()
        gvJournal.SelectedIndex = p_selected_id
    End Sub


     


     


    'Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
    '    Try
    '        Dim url As String
    '        viewstate("datamode") = "add"
    '        MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)

    '        Dim Queryusername As String = Session("sUsr_name")

    '        viewstate("datamode") = Encr_decrData.Encrypt(viewstate("datamode"))
    '        url = "acccuCostUnit.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")

    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '    End Try
    'End Sub


     


     

 

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ' Dim i As New Encryption64
                ' ''
                ' ''viewstate("datamode") = "add"
                ' ''MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)

                ' ''Dim Queryusername As String = Session("sUsr_name")

                viewstate("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "acccuCostUnit.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")
                'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                hlCEdit.Enabled = True
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
