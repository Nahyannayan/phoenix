﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accAdjJournalAdd
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvJouranls.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ddlBusinessunit.DataBind()
                ddlBusinessunitOpp.DataBind()
                gvJouranls.DataBind()
                If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                    ddlBusinessunit.SelectedIndex = -1
                    ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
                    ddlBusinessunitOpp.SelectedIndex = -1
                    ddlBusinessunitOpp.Items.FindByValue(Session("sBsuid")).Selected = True
                End If
                If Session("sUsr_name") Is Nothing Or Session("sBsuid") Is Nothing Or (ViewState("MainMnu_code").ToString <> "A150100") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code").ToString)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If hfJNL_ID.Value.Split("||")(0) = "" Then
            lblError.Text = "Please select voucher"
            Exit Sub
        End If
        If Not IsDate(txtdocDate.Text) Then
            lblError.Text = "Please enter a valid date"
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim stTrans As SqlTransaction
        Dim iReturnvalue As Integer
        objConn.Open()
        stTrans = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand("SaveJOURNAL_ADJ_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpVDC_ID As New SqlParameter("@JNL_ID", SqlDbType.BigInt)
            sqlpVDC_ID.Value = hfJNL_ID.Value.Split("||")(0)
            cmd.Parameters.Add(sqlpVDC_ID)

            Dim sqlpVHD_CHQNO As New SqlParameter("@JAL_BSU_ID", SqlDbType.VarChar, 20)
            sqlpVHD_CHQNO.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpVHD_CHQNO)

            Dim sqlpVDC_BSU_ID As New SqlParameter("@JAL_OPP_BSU_ID", SqlDbType.VarChar, 20)
            sqlpVDC_BSU_ID.Value = ddlBusinessunitOpp.SelectedItem.Value
            cmd.Parameters.Add(sqlpVDC_BSU_ID)

            Dim sqlpVDC_STATUS As New SqlParameter("@JAL_bExclude", SqlDbType.Bit)
            sqlpVDC_STATUS.Value = False
            cmd.Parameters.Add(sqlpVDC_STATUS)

            Dim sqlpVDC_SignedBy As New SqlParameter("@JAL_RSS_TYP", SqlDbType.VarChar, 100)
            sqlpVDC_SignedBy.Value = ddlReportType.SelectedItem.Value
            cmd.Parameters.Add(sqlpVDC_SignedBy)

            Dim sqlpVDC_SENDBY As New SqlParameter("@USER", SqlDbType.VarChar, 100)
            sqlpVDC_SENDBY.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpVDC_SENDBY)

            Dim sqlpJNL_NARRATION As New SqlParameter("@JAL_NARRATION", SqlDbType.VarChar)
            sqlpJNL_NARRATION.Value = txtNarration.Text
            cmd.Parameters.Add(sqlpJNL_NARRATION)

            Dim sqlpJAL_DOCDT As New SqlParameter("@JAL_DOCDT", SqlDbType.DateTime)
            sqlpJAL_DOCDT.Value = txtdocDate.Text
            cmd.Parameters.Add(sqlpJAL_DOCDT)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            cmd.Parameters.Clear()

            If iReturnvalue = 0 Then iReturnvalue = UtilityObj.operOnAudiTable(Master.MenuName, "JNL_ID:" & hfJNL_ID.Value, "", Page.User.Identity.Name.ToString, Me.Page, "Jouranl REGISTER")

            If (iReturnvalue = 0) Then
                stTrans.Commit()
                ClearDetails()
            Else
                stTrans.Rollback()
            End If
            lblError.Text = getErrorMessage(iReturnvalue)
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ClearDetails()
        hfJNL_ID.Value = ""
        gvJouranls.DataBind()
        txtNarration.Text = ""
        lblVoucherDate.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        hfJNL_ID.Value = ""
        gvJouranls.DataBind()
        ddlBusinessunitOpp.SelectedIndex = -1
        ddlBusinessunitOpp.Items.FindByValue(ddlBusinessunit.SelectedItem.Value).Selected = True
    End Sub

    Protected Sub imgBankSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindJouranl()
    End Sub

    Sub bindJouranl()
        Try
            Dim str_sql As String = " exec GetJOURNALLIST '" & hfJNL_ID.Value.Split("||")(0) & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            gvJouranls.DataSource = ds
            gvJouranls.DataBind()
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                lblVoucherDate.Text = "(Voucher Date : " & ds.Tables(0).Rows(0)("JHD_DOCDT") & ")"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

End Class
