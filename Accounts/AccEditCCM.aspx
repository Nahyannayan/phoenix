<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccEditCCM.aspx.vb" Inherits="AccEditCCM" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cost Element Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" width="100%" align="center">
                    <tr>
                        <td align="left" width="100%">
                            <table width="100%" align="center">
                                <tbody>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="100%">
                            <table width="100%" align="center">
                                <%--  <tr>
                                    <td class="title-bg-lite" colspan="2"><span class="field-label">Cost Center Details </span>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Description</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDescr" runat="server" MaxLength="100"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="100%">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

