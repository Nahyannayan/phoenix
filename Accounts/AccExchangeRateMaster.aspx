﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="AccExchangeRateMaster.aspx.vb" Inherits="Accounts_AccExchangeRateMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Exchange Rate Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtBUID" runat="server" Enabled="false"></asp:TextBox>
                                    <asp:TextBox ID="TxtBUName" runat="server" Enabled="false"></asp:TextBox>

                                </td>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Currency Code</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCurrencytype" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>

                                        <asp:ImageButton ID="imgFDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton>
                                        <asp:CalendarExtender
                                            ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFDT" TargetControlID="txtFromDate">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgTDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgTDT" TargetControlID="txtToDate">
                                            </asp:CalendarExtender>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Exchange Rate</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtExchRate" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <asp:HiddenField ID="h_Etr_Id" runat="server" />
                                <asp:HiddenField ID="h_Approve" runat="server" />



                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                            <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button" Text="Approve" TabIndex="10" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

