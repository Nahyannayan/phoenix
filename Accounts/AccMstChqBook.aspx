<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccMstChqBook.aspx.vb" Inherits="AccMstChqBook" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        //not in use


        function MoveCursorDown() {//alert("ii");
            var c = 16;
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            //alert(c)
            c++;
            if (c <= 0 || c == -1) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == '17';
                c = 16;
            }
            //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);

            //c++;
            //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null;
            var index = 0;
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c < rows.length - 1) {
                rows[c].className = "gridheader";
                if (c % 2 == 0)
                    rows[c - 1].className = "griditem_alternative";
                else
                    rows[c - 1].className = "griditem";
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

            }
            // alert(rows.length-900);

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Chequebook Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" id="tbl" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                    <tr>

                        <td align="center">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" DataKeyNames="CHB_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="Bank Account" SortExpression="CHB_ID">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Bank Account
                                <br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account Name" SortExpression="CHB_LOTNO">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Lot No
                                <br />
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbllotNo" runat="server" Text='<%# Bind("CHB_LOTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Account Name" SortExpression="CHB_PREFIX">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Prefix
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblprefix" runat="server" Text='<%# Bind("CHB_PREFIX") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CHB_ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="center" />
                            </asp:GridView>
                        </td>
                        <td></td>
                    </tr>
                </table>

                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>

