Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class jvPostJournalVoucher
    Inherits System.Web.UI.Page  
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache" 
        If Page.IsPostBack = False Then 
            h_Grid.Value = "top"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200015" And ViewState("MainMnu_code") <> "A200060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Select Case ViewState("MainMnu_code").ToString
                    Case "A200015"
                        lblHeader.Text = "Journal Voucher"
                        ViewState("doctype") = "JV"
                    Case "A200060"
                        lblHeader.Text = "Self Reversing Journal"
                        ViewState("doctype") = "SJV"
                End Select
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        viewstate("datamode") = Encr_decrData.Encrypt("view")
                        Select Case ViewState("MainMnu_code").ToString
                            Case "A200015"
                                hlview.NavigateUrl = "accjvjournalvoucher.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                            Case "A200060"
                                hlview.NavigateUrl = "accsjvsjournalvoucher.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        gvChild.Visible = False
        gvDetails.Visible = False
        h_FirstVoucher.Value = ""
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            Select Case ViewState("MainMnu_code").ToString
                Case "A200015" ' JV
                    PrintVoucher_JV(h_FirstVoucher.Value)
                Case "A200060" ' sjv
                    PrintVoucher_SJV(h_FirstVoucher.Value)
            End Select
        End If
        gridbind()
    End Sub

    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                If ViewState("doctype") = "SJV" Then
                    post_SJournalvoucher(p_guid, objConn, stTrans)
                Else
                    post_Journalvoucher(p_guid, objConn, stTrans)
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Function post_Journalvoucher(ByVal p_guid As String, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Boolean
        Try
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim str_Sql As String
                If ViewState("doctype") = "SJV" Then
                    str_Sql = "SELECT GUID, SHD_SUB_ID AS JHD_SUB_ID , SHD_BSU_ID AS JHD_BSU_ID," _
                     & " SHD_FYEAR AS JHD_FYEAR, SHD_DOCTYPE AS JHD_DOCTYPE, " _
                     & " SHD_DOCNO AS JHD_DOCNO, SHD_DOCDT AS JHD_DOCDT, SHD_CUR_ID AS JHD_CUR_ID, " _
                     & " SHD_EXGRATE1 AS JHD_EXGRATE1, SHD_EXGRATE2 AS JHD_EXGRATE2, " _
                     & " SHD_bPOSTED  AS JHD_bPOSTED, SHD_bDELETED AS JHD_bDELETED, " _
                     & " SHD_REFDOCTYPE AS JHD_REFDOCTYPE, SHD_REFDOCNO AS JHD_REFDOCNO, " _
                     & " SHD_NARRATION AS JHD_NARRATION, SHD_REFNO AS JHD_REFNO, SHD_ID AS JHD_ID " _
                     & " FROM  SJOURNAL_H AS JHD" _
                     & " WHERE  GUID='" & p_guid & "' "
                Else
                    str_Sql = "select * FROM JOURNAL_H where GUID='" & p_guid & "' "
                End If
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(objConn.ConnectionString, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTJOURNAL", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@JHD_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = ds.Tables(0).Rows(0)("JHD_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@JHD_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    If (iReturnvalue = 0) Then
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                        End If
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                    End If
                    If (iReturnvalue = 0) Then
                        lblError.Text = "Successfully Posted"
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("JHD_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    Else
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Function post_SJournalvoucher(ByVal p_guid As String, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Try
                Dim str_Sql As String
                If ViewState("doctype") = "SJV" Then
                    str_Sql = "SELECT GUID, SHD_SUB_ID AS JHD_SUB_ID , SHD_BSU_ID AS JHD_BSU_ID," _
                     & " SHD_FYEAR AS JHD_FYEAR, SHD_DOCTYPE AS JHD_DOCTYPE, " _
                     & " SHD_DOCNO AS JHD_DOCNO, SHD_DOCDT AS JHD_DOCDT, SHD_CUR_ID AS JHD_CUR_ID, " _
                     & " SHD_EXGRATE1 AS JHD_EXGRATE1, SHD_EXGRATE2 AS JHD_EXGRATE2, " _
                     & " SHD_bPOSTED  AS JHD_bPOSTED, SHD_bDELETED AS JHD_bDELETED, " _
                     & " SHD_REFDOCTYPE AS JHD_REFDOCTYPE, SHD_REFDOCNO AS JHD_REFDOCNO, " _
                     & " SHD_NARRATION AS JHD_NARRATION, SHD_REFNO AS JHD_REFNO, SHD_ID AS JHD_ID " _
                     & " FROM  SJOURNAL_H AS JHD" _
                     & " WHERE  GUID='" & p_guid & "' "
                Else
                    str_Sql = "select * FROM JOURNAL_H where GUID='" & p_guid & "' "
                End If
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTSJOURNAL", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@SHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@SHD_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@SHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@SHD_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = ds.Tables(0).Rows(0)("JHD_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@SHD_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    If (iReturnvalue = 0) Then
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                        End If
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                    End If
                    If (iReturnvalue = 0) Then
                        lblError.Text = "Successfully Posted"
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("JHD_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    Else
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                Else

                End If

            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        If ViewState("doctype") = "SJV" Then
            str_Sql = "SELECT GUID, SHD_SUB_ID AS JHD_SUB_ID , SHD_BSU_ID AS JHD_BSU_ID," _
           & " SHD_FYEAR AS JHD_FYEAR, SHD_DOCTYPE AS JHD_DOCTYPE, " _
           & " SHD_DOCNO AS JHD_DOCNO, SHD_DOCDT AS JHD_DOCDT, SHD_CUR_ID AS JHD_CUR_ID, " _
           & " SHD_EXGRATE1 AS JHD_EXGRATE1, SHD_EXGRATE2 AS JHD_EXGRATE2, " _
           & " SHD_bPOSTED  AS JHD_bPOSTED, SHD_bDELETED AS JHD_bDELETED, " _
           & " SHD_REFDOCTYPE AS JHD_REFDOCTYPE, SHD_REFDOCNO AS JHD_REFDOCNO, " _
           & " SHD_NARRATION AS JHD_NARRATION, SHD_REFNO AS JHD_REFNO, SHD_ID AS JHD_ID," _
           & " (SELECT     SUM(SJL_DEBIT) AS TOTAL FROM SJOURNAL_D" _
           & " WHERE    SHD_BSU_ID=SJL_BSU_ID and  (JHD.SHD_SUB_ID = '" & Session("Sub_ID") & "') AND (JHD.SHD_BSU_ID = '" & Session("sBsuid") & "') " _
           & " AND (JHD.SHD_DOCTYPE = 'SJV') AND (SJL_DOCNO = JHD.SHD_DOCNO)) AS TOTAL" _
           & " FROM  SJOURNAL_H AS JHD" _
           & " WHERE     (SHD_bDELETED = 'False') AND (SHD_DOCTYPE = 'SJV') " _
           & " AND (SHD_BSU_ID = '" & Session("sBsuid") & "') AND (SHD_bPOSTED = 0)" _
           & " AND (SHD_bDELETED = 0) AND SHD_FYEAR = " & Session("F_YEAR") _
           & " ORDER BY SHD_DOCDT DESC, SHD_DOCNO DESC"
        Else
            str_Sql = " SELECT *," _
                   & " (SELECT  SUM(JNL_DEBIT) AS TOTAL" _
                   & " FROM  JOURNAL_D " _
                   & " where JHD_BSU_ID=JNL_BSU_ID and JHD_SUB_ID='" & Session("Sub_ID") & "' and  JHD_BSU_ID='" & Session("sBsuid") & "' " _
                   & " AND JHD_DOCTYPE='JV'" _
                   & "  AND JOURNAL_D.JNL_DOCNO=JOURNAL_H.JHD_DOCNO and JOURNAL_D.JNL_BSU_ID=JOURNAL_H.JHD_BSU_ID   and JOURNAL_D.JNL_FYEAR=JOURNAL_H.JHD_FYEAR   ) AS TOTAL" _
                   & "  FROM JOURNAL_H WHERE JHD_bDELETED='False' " _
                   & " AND JHD_DOCTYPE='JV' AND JHD_BSU_ID='" & Session("sBsuid") & "' " _
                   & " AND  JHD_bPOSTED=0 AND JHD_bDELETED=0 AND JHD_FYEAR = " & Session("F_YEAR") _
                   & " ORDER BY JHD_DOCDT DESC, JHD_DOCNO DESC"
        End If

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        If ds.Tables(0).Rows.Count > 0 Then
            btnPost.Visible = True
            chkPrint.Visible = True
        Else
            btnPost.Visible = False
            chkPrint.Visible = False
        End If
        gvJournal.DataBind()
        gvJournal.SelectedIndex = p_selected_id
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvDetails.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                '        Dim lblGrpCode As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                h_Grid.Value = "detail"
                If ViewState("doctype") = "SJV" Then
                    str_Sql = "select * FROM SJOURNAL_H where GUID='" & lblGUID.Text & "' "
                Else
                    str_Sql = "select * FROM JOURNAL_H where GUID='" & lblGUID.Text & "' "
                End If
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    If ViewState("doctype") = "SJV" Then
                        str_Sql = " SELECT SJD.GUID, SJD.SJL_SUB_ID AS JNL_SUB_ID, SJD.SJL_BSU_ID AS JNL_BSU_ID," _
                 & " SJD.SJL_FYEAR AS JNL_FYEAR, SJD.SJL_DOCTYPE AS JNL_DOCTYPE, SJD.SJL_DOCNO AS JNL_DOCNO, " _
                 & " SJD.SJL_ACT_ID AS JNL_ACT_ID, SJD.SJL_SLNO AS JNL_SLNO, SJD.SJL_DOCDT AS JNL_DOCDT, " _
                 & " SJD.SJL_ACT_ID + '-' + AM.ACT_NAME AS ACCOUNT, " _
                 & " SJD.SJL_DEBIT AS JNL_DEBIT, SJD.SJL_CREDIT AS JNL_CREDIT, SJD.SJL_NARRATION AS JNL_NARRATION, " _
                 & " AM.ACT_NAME  , SJD.SJL_BDELETED AS JNL_BDELETED FROM SJOURNAL_D AS SJD INNER JOIN" _
                 & " ACCOUNTS_M AS AM ON SJD.SJL_ACT_ID = AM.ACT_ID " _
                 & " AND SJD.SJL_ACT_ID = AM.ACT_ID WHERE" _
                 & " (SJD.SJL_DOCNO = '" & ds.Tables(0).Rows(0)("SHD_DOCNO") & "') AND (SJD.SJL_SUB_ID = '" & Session("Sub_ID") & "') " _
                 & " AND (SJD.SJL_BSU_ID = 'XXXXXX') AND (SJD.SJL_DOCTYPE = 'SJV')"
                    Else
                        str_Sql = "SELECT JOURNAL_D.GUID, JOURNAL_D.JNL_SUB_ID," _
                          & " JOURNAL_D.JNL_BSU_ID, JOURNAL_D.JNL_FYEAR," _
                          & " JOURNAL_D.JNL_DOCTYPE, JOURNAL_D.JNL_DOCNO," _
                          & " JOURNAL_D.JNL_ACT_ID,JOURNAL_D.JNL_SLNO,JOURNAL_D.JNL_DOCDT," _
                          & " JOURNAL_D.JNL_ACT_ID + '-' + ACCOUNTS_M.ACT_NAME" _
                          & " AS ACCOUNT, JOURNAL_D.JNL_DEBIT, " _
                          & " JOURNAL_D.JNL_CREDIT, JOURNAL_D.JNL_NARRATION," _
                          & " ACCOUNTS_M.ACT_NAME, JOURNAL_D.JNL_BDELETED" _
                          & " FROM JOURNAL_D INNER JOIN ACCOUNTS_M" _
                          & " ON JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID " _
                          & " AND JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID" _
                          & " WHERE     (JOURNAL_D.JNL_DOCNO = '" & ds.Tables(0).Rows(0)("JHD_DOCNO") & "') " _
                          & " AND (JOURNAL_D.JNL_SUB_ID = '" & Session("Sub_ID") & "') " _
                          & " AND (JOURNAL_D.JNL_BSU_ID = '" & Session("sBsuid") & "') " _
                          & " AND  (JOURNAL_D.JNL_DOCTYPE = 'JV')"
                    End If
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvChild_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvChild.Sorting

    End Sub

    Protected Sub gvDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvDetails.SelectedIndexChanging
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            gvDetails.SelectedIndex = e.NewSelectedIndex
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label 
                lblGUID = TryCast(gvDetails.SelectedRow.FindControl("lblGUID"), Label)
                lblSlno = TryCast(gvDetails.SelectedRow.FindControl("lblSlno"), Label)

                str_Sql = "select * FROM JOURNAL_D where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then 
                    str_Sql = "SELECT JOURNAL_D_S.JDS_doctype,JOURNAL_D_S.JDS_DOCNO," _
                    & " JOURNAL_D_S.JDS_DESCR, ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, " _
                    & " case isnull(JOURNAL_D_S.JDS_CODE,'')  when '' then 'GENERAL'" _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD," _
                    & " COSTCENTER_S.CCS_DESCR ,JOURNAL_D_S.JDS_SLNO, JOURNAL_D_S.JDS_CODE," _
                    & " JOURNAL_D_S.JDS_AMOUNT, COSTCENTER_S.CCS_QUERY" _
                    & " FROM JOURNAL_D INNER JOIN ACCOUNTS_M ON  ACCOUNTS_M.ACT_ID = JOURNAL_D.JNL_ACT_ID " _
                    & " LEFT OUTER JOIN JOURNAL_D_S ON JOURNAL_D.JNL_SUB_ID=JOURNAL_D_S.JDS_SUB_ID AND" _
                    & " JOURNAL_D.JNL_BSU_ID=   JOURNAL_D_S.JDS_BSU_ID AND " _
                    & " JOURNAL_D.JNL_FYEAR =JOURNAL_D_S.JDS_FYEAR AND JOURNAL_D.JNL_DOCTYPE=JOURNAL_D_S.JDS_DOCTYPE AND " _
                    & " JOURNAL_D.JNL_DOCNO=JOURNAL_D_S.JDS_DOCNO  AND JOURNAL_D.JNL_SLNO=JOURNAL_D_S.JDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S ON  JOURNAL_D_S.JDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  JOURNAL_D_S.JDS_DOCTYPE='JV' AND " _
                    & " JOURNAL_D_S.JDS_DOCNO='" & ds.Tables(0).Rows(0)("JNL_DOCNO") & "' AND" _
                    & " JOURNAL_D_S.JDS_SUB_ID='" & Session("Sub_ID") & "' AND" _
                    & " JOURNAL_D_S.JDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " JOURNAL_D_S.JDS_FYEAR='" & Session("F_YEAR") & "'" _
                    & " AND JOURNAL_D_S.JDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)

                    If dsc.Tables(0).Rows.Count > 0 Then
                        h_Grid.Value = "child"
                    Else
                        h_Grid.Value = "detail"
                    End If
                    helper.RegisterSummary("JDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                Throw        'Bubble up the exception
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub PrintVoucher_SJV(ByVal p_Docno As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String = String.Empty
        'SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
        strFilter = "BSU_ID = '" & Session("SBSUID") & "' and FYEAR = " & Session("F_YEAR") & " and DOCTYPE = 'SJV' and SHD_DOCNO = '" & p_Docno & "' "
        str_Sql = "SELECT * FROM vw_OSA_SJOURNAL where " + strFilter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("Summary") = chkSummary.Checked
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(Session("SUB_ID"), "SJV", p_Docno)

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If
            repSource.IncludeBSUImage = True
            repSource.SubReport = repSourceSubRep

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT SJOURNAL_D.SJL_SUB_ID AS VHH_SUB_ID, SJL_BSU_ID AS VHH_BSU_ID, " & _
            " SJOURNAL_D.SJL_FYEAR AS VHH_FYEAR, SJOURNAL_D.SJL_DOCTYPE AS VHH_DOCTYPE, " & _
            " SJOURNAL_D.SJL_DOCNO AS VHH_DOCNO, '' AS VHD_LINEID, " & _
            " SJOURNAL_D.SJL_NARRATION AS VHD_NARRATION FROM SJOURNAL_D " & _
            " INNER JOIN SJOURNAL_H ON SJOURNAL_D.SJL_SUB_ID = SJOURNAL_H.SHD_SUB_ID " & _
            " AND SJOURNAL_D.SJL_BSU_ID = SJOURNAL_H.SHD_BSU_ID AND " & _
            " SJOURNAL_D.SJL_FYEAR = SJOURNAL_H.SHD_FYEAR AND " & _
            " SJOURNAL_D.SJL_DOCTYPE = SJOURNAL_H.SHD_DOCTYPE AND " & _
            " SJOURNAL_D.SJL_DOCNO = SJOURNAL_H.SHD_DOCNO WHERE " + strFilter.Replace("BSU_ID", "SHD_BSU_ID").Replace("FYEAR", "SJL_FYEAR").Replace("DOCTYPE", "SJL_DOCTYPE")
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails

            params("VoucherName") = "Self Reversing Journal Voucher"
            params("reportHeading") = "Self Reversing Journal Voucher"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/SJVoucherReport.rpt"
            Session("ReportSource") = repSource
            h_print.Value = "print"
        End If
    End Sub

    Sub PrintVoucher_JV(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

End Class
