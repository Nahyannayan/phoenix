﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Accounts_MenuDescriptionUpdate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim userSuper As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MainMnu_code As String
        Try
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ' Page.Title = OASISConstants.Gemstitle
                ShowMessage("", False)
                ViewState("datamode") = "none"
                If Request.QueryString("MainMnu_code") <> "" Then
                    MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                    ViewState("MainMnu_code") = MainMnu_code
                Else
                    MainMnu_code = ""
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Else
                    ViewState("datamode") = ""
                End If
                userSuper = Session("sBusper")
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub

    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Protected Sub ddlModule_SelectedIndexChanged(sender As Object, e As EventArgs)
       
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        Dim ds As New DataSet
        Dim sqlString As String = ""
        Dim moduleDecr As New Encryption64
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim bunit As String = Session("sBsuid")
        Dim userRol As String = Session("sroleid")
        Dim userMod As String = ddlModule.SelectedValue 'Session("sModule")
        Dim user As String = Session("sUsr_name")
        If (ddlModule.SelectedItem.Text = "Select" And ddlModule.SelectedValue = "0") Then
            rowgrid.Visible = False

        Else
            rowgrid.Visible = True
            '[OASIS].[]
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(3) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@userMod", userMod, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@bunit", bunit, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@userRol", userRol, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@userSuper", userSuper, SqlDbType.Bit)

            Dim dsMenu As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_MENUDETAILS]", Param)
            ViewState("MenuDetails") = dsMenu.Tables(0)
            grdMenu.DataSource = dsMenu.Tables(0)
            grdMenu.DataBind()

            ' Session("sroleid")
            'If userSuper = False Then
            '    sqlString = "select MNU_CODE, MNU_TEXT," & _
            '                  " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
            '                  " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
            '                   " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where  mnu_text not in ('Home','Help','Log Off') and mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
            'Else
            '    sqlString = "select MNU_CODE,  MNU_TEXT," & _
            '                  " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
            '                  " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
            '                   " where mnu_text not in ('Home','Help','Log Off') and (mnu_parentid is not null) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
            'End If
        End If
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs)
        Dim strans As SqlTransaction = Nothing
        Dim Param(1) As SqlParameter
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Try
            Dim txtdescr As TextBox = sender.parent.findcontrol("txtdescr")
            Dim hidmenucode As HiddenField = sender.parent.findcontrol("hidmenucode")
            If (txtdescr.Text.Length >= 500) Then
                ShowMessage("Exceeded charecter limit!", True)
            Else
                Dim MenuText = txtdescr.Text
                Dim MenuCode = hidmenucode.Value.ToString()
                ' [OASIS].[UPDATE_MENUDETAILS]
                Dim objConn As New SqlConnection(st_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                Param(0) = Mainclass.CreateSqlParameter("@DESCR", MenuText, SqlDbType.VarChar)
                Param(1) = Mainclass.CreateSqlParameter("@MNU_CODE", MenuCode, SqlDbType.VarChar)
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_MENUDETAILS]", Param)
                ShowMessage("Successfully Updated", False)
                strans.Commit()
            End If
            
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnUpdate_Click: " + ex.Message, "DESCRIPTION UPDATE FOR MENU")
        End Try
    End Sub

    Protected Sub grdMenu_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grdMenu.PageIndex = e.NewPageIndex
        grdMenu.DataSource = ViewState("MenuDetails")
        grdMenu.DataBind()
    End Sub
End Class
