<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowChqsPDC.aspx.vb" Inherits="ShowChqsPDC" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accounts</title>

    <base target="_self" />
    <link href="/../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/../cssfiles/sb-admin.css" rel="stylesheet">
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    
   <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
</script> --%>
    <style type="text/css">
        .odd {
            background-color: white;
        }

        .even {
            background-color: gray;
        }
    </style>


    <script language="javascript" type="text/javascript">
        //not in use


        function MoveCursorDown() {//alert("ii");
            var c = 16;
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            //alert(c)
            c++;
            if (c <= 0 || c == -1) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == '17';
                c = 16;
            }
            //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);

            //c++;
            //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null;
            var index = 0;
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c < rows.length - 1) {
                rows[c].className = "gridheader";
                if (c % 2 == 0)
                    rows[c - 1].className = "griditem_alternative";
                else
                    rows[c - 1].className = "griditem";
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

            }
            // alert(rows.length-900);

        }

    </script>

    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body class="matter" onload="listen_window();" onkeydown="MoveCursorDown();">
    <form id="form1" runat="server">

        <div>
            <div>
                <table width="100%" id="tbl" border="0">
                    <tr>
                        <td colspan="4">
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="CHB_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="50" CssClass="table table-row table-bordered">
                                <Columns>

                                    <asp:TemplateField HeaderText="Group Code" SortExpression="CHB_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Group Code" SortExpression="CHB_LOTNO" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLotNo" runat="server" Text='<%# Bind("CHB_LOTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Lot No" SortExpression="CHB_LOTNO">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Lot No<br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCode" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("CHB_LOTNO") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Account Name" SortExpression="CHB_LOTNO" Visible="FALSE">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Lot No<br />
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblCod111" SkinID="Grid" runat="server" Text='<%# Bind("CHB_LOTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Account Name" SortExpression="CHB_PREFIX">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Prefix
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblCodeh" SkinID="Grid" runat="server" Text='<%# Bind("AvlNos") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="Left" />
                            </asp:GridView>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </form>
</body>
</html>
