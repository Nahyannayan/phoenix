Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accUnpostVoucher
    Inherits System.Web.UI.Page


    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            ''''' 
            Page.Title = OASISConstants.Gemstitle
            bind_Vouchertype()
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A200035") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
               
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))

            End If
            ''''
            'gvChild.Attributes.Add("bordercolor", "#1b80b6")
            'gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            txtTodate.Text = GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(GetDiplayDate()).AddMonths(-2))

            gridbind()
        End If
    End Sub


    Sub bind_Vouchertype()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT  DISTINCT   DOCUMENT_M.DOC_NAME," _
            & " DOCUMENT_M.DOC_ID FROM DOCUMENT_M" _
            & " INNER JOIN JOURNAL_H ON " _
            & " DOCUMENT_M.DOC_ID = JOURNAL_H.JHD_DOCTYPE" _
            & " ORDER BY DOCUMENT_M.DOC_NAME"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ddVouchertype.DataTextField = "DOC_NAME"
            ddVouchertype.DataValueField = "DOC_ID"

            ddVouchertype.DataSource = ds.Tables(0)
            ddVouchertype.DataBind()
            ddVouchertype.SelectedIndex = 0 'SET SELECTED ITEM OF GROUP POLICY

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                chkPost.Checked = False
                'If chkPost.Checked = True Then
                '    chkPost.Disabled = True
                'Else
                '    If hlCEdit IsNot Nothing And lblGUID IsNot Nothing Then
                '        ' Dim i As New Encryption64
                '        hlCEdit.NavigateUrl = "acccpCashPayments.aspx?editid=" & e.Row.Cells(1).Text
                '        hlview.NavigateUrl = "acccpCashPayments.aspx?viewid=" & e.Row.Cells(1).Text
                '        'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                '        hlCEdit.Enabled = True
                '    End If
                'End If
            End If

        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    'chk.Visible = False
                    h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    post_voucher(chk.Value.ToString)
                    Exit Sub
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Find_Checked(Me.Page)
        gridbind()
        txtNarration.Text = ""
    End Sub


    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Try
                Dim iReturnvalue As String
                Dim unpost_docno As String = ""
                iReturnvalue = VoucherFunctions.UnPost_voucher(p_guid, ddVouchertype.SelectedItem.Value, Session("sUsr_name"), txtNarration.Text.Trim, unpost_docno)
                
                If (iReturnvalue = 0) Then
                    stTrans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, unpost_docno, "Unposting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                End If
                If (iReturnvalue = 0) Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                Else
                    lblError.Text = getErrorMessage(iReturnvalue)
                End If ' ds.Tables(0).Rows(0)("ERR_MSG")
                

            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Private Sub search_gridbind()
        Dim str_filter As String = " AND JHD_DOCNO LIKE '%" & txtDocno.Text & "%' "

        Dim strfDate As String = txtFromDate.Text.Trim
        Dim strTDate As String = txtTodate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)

        Dim str_JHD_DOCDT As String = "JHD.JHD_DOCDT"
        If ddVouchertype.SelectedItem.Value = "PP" Then
            str_JHD_DOCDT = "JHD_DOCDT"
        End If

        If str_err = "" And CheckBox1.Checked = False Then
            txtTodate.Text = strTDate
            str_filter = str_filter & " AND " & str_JHD_DOCDT & "='" & txtFromDate.Text & "' "
        Else
            lblError.Text = str_err
        End If

        str_err = str_err & DateFunctions.checkdate(strTDate)


        If str_err = "" And CheckBox1.Checked = True Then
            str_filter = str_filter & " AND " & str_JHD_DOCDT & " BETWEEN '" & txtFromDate.Text & "' AND '" & txtTodate.Text & "'"
            txtFromDate.Text = strfDate
        End If
        If str_err <> "" And CheckBox1.Checked = True Then
            lblError.Text = "To Date is not valid"
        End If
        gridbind(str_filter)
    End Sub

    Private Sub gridbind(Optional ByVal str_filter As String = "")
        Try
            If ddVouchertype.SelectedIndex = -1 Then
                ddVouchertype.SelectedIndex = 0
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String


            'str_Sql = "SELECT  *  FROM  JOURNAL_H" _
            '       & " where JHD_SUB_ID='" & Session("Sub_ID") & "' and " _
            '       & " JHD_BSU_ID='" & Session("sBsuid") & "' and" _
            '       & " JHD_bDELETED='false' AND JHD_bPOSTED='TRUE'  AND JHD_DOCTYPE='" & ddVouchertype.SelectedItem.Value & "'"
          
            If ddVouchertype.SelectedItem.Value = "PP" Then
                str_Sql = "SELECT * FROM(" _
                    & " SELECT GUID,  PRP_SUB_ID AS JHD_SUB_ID, " _
                    & " PRP_BSU_ID AS JHD_BSU_ID, PRP_DRNARRATION+' '+PRP_CRNARRATION " _
                    & " AS JHD_NARRATION , PRP_bPosted AS JHD_bPOSTED," _
                    & " PRP_AMOUNT AS AMOUNT, PRP_ID AS JHD_DOCNO, PRP_DOCDT AS JHD_DOCDT, " _
                    & " PRP_CUR_ID AS JHD_CUR_ID FROM PREPAYMENTS_H WHERE  PRP_FYEAR = " & Session("F_YEAR") & ") DB  " _
                    & " WHERE JHD_BSU_ID='" & Session("sBsuid") & "'" _
                    & " AND JHD_SUB_ID='" & Session("Sub_ID") & "'" _
                    & " AND JHD_bPOSTED='TRUE' " _
                     & str_filter _
                    & " order by JHD_DOCDT desc"
            ElseIf ddVouchertype.SelectedItem.Value = "IJV" Then
                str_Sql = "SELECT JHD.GUID," _
                            & " JHD.JHD_SUB_ID, JHD.JHD_BSU_ID,JHD.JHD_NARRATION,  JHD.JHD_bPOSTED, " _
                            & " SUM(JOURNAL_D.JNL_DEBIT) AS AMOUNT, JHD.JHD_DOCNO, JHD.JHD_DOCDT, JHD.JHD_CUR_ID" _
                            & " FROM JOURNAL_H AS JHD INNER JOIN JOURNAL_D ON JHD.JHD_SUB_ID = JOURNAL_D.JNL_SUB_ID" _
                            & " AND JHD.JHD_BSU_ID = JOURNAL_D.JNL_BSU_ID AND JHD.JHD_FYEAR = JOURNAL_D.JNL_FYEAR" _
                            & " AND JHD.JHD_DOCTYPE = JOURNAL_D.JNL_DOCTYPE AND JHD.JHD_DOCNO = JOURNAL_D.JNL_DOCNO" _
                            & " WHERE  JHD.JHD_BSU_ID='" & Session("sBsuid") & "' AND JHD.JHD_FYEAR = " & Session("F_YEAR") _
                            & " AND JHD.JHD_bDELETED='false' AND JHD.JHD_bPOSTED='TRUE'  AND JHD.JHD_DOCTYPE='" & ddVouchertype.SelectedItem.Value & "'" _
                            & str_filter _
                            & " GROUP BY JHD.GUID, JHD.JHD_SUB_ID," _
                            & " JHD.JHD_BSU_ID, JHD.JHD_DOCNO," _
                            & " JHD.JHD_DOCDT,  JHD.JHD_CUR_ID,JHD.JHD_NARRATION , JHD.JHD_bPOSTED" _
                            & " order by JHD.JHD_DOCDT desc"
            Else
                str_Sql = "SELECT JHD.GUID, JHD.JHD_SUB_ID, JHD.JHD_BSU_ID,JHD.JHD_NARRATION,  JHD.JHD_bPOSTED, " _
                          & " SUM(JOURNAL_D.JNL_DEBIT) AS AMOUNT, JHD.JHD_DOCNO, " _
                          & " JHD.JHD_DOCDT, JHD.JHD_CUR_ID FROM JOURNAL_H AS JHD INNER JOIN" _
                          & " JOURNAL_D ON JHD.JHD_SUB_ID = JOURNAL_D.JNL_SUB_ID AND JHD.JHD_BSU_ID = JOURNAL_D.JNL_BSU_ID AND " _
                          & " JHD.JHD_FYEAR = JOURNAL_D.JNL_FYEAR AND JHD.JHD_DOCTYPE = JOURNAL_D.JNL_DOCTYPE AND" _
                          & " JHD.JHD_DOCNO = JOURNAL_D.JNL_DOCNO WHERE JHD.JHD_SUB_ID='" & Session("Sub_ID") & "'" _
                          & " AND JHD.JHD_BSU_ID='" & Session("sBsuid") & "' AND JHD.JHD_FYEAR = " & Session("F_YEAR") _
                          & " AND JHD.JHD_bDELETED='false' AND JHD.JHD_bPOSTED='TRUE'  AND JHD.JHD_DOCTYPE='" & ddVouchertype.SelectedItem.Value & "'" _
                          & str_filter _
                          & " GROUP BY JHD.GUID, JHD.JHD_SUB_ID," _
                          & " JHD.JHD_BSU_ID, JHD.JHD_DOCNO," _
                          & " JHD.JHD_DOCDT,  JHD.JHD_CUR_ID,JHD.JHD_NARRATION , JHD.JHD_bPOSTED" _
                          & " order by JHD.JHD_DOCDT desc"
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
            Else
                btnPost.Visible = False
            End If
            gvJournal.DataBind()
            For Each gvr As GridViewRow In gvJournal.Rows
                'Get a programmatic reference to the CheckBox control
                Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkPosted"), HtmlInputCheckBox)
                If cb IsNot Nothing Then
                    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub ddVouchertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddVouchertype.SelectedIndexChanged
        gridbind()
    End Sub
 
     
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        search_gridbind()
    End Sub
End Class
