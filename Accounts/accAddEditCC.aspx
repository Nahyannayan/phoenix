<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAddEditCC.aspx.vb" Inherits="accAddEditCC" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Country"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                             <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:" ForeColor="" SkinID="error" CssClass="error" />
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <table align="center" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblID" runat="server" Text=" Country ID" CssClass="field-label"></asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtID" runat="server" CssClass="inputbox"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtID"
                                Display="Dynamic" ErrorMessage="ID required." CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>&nbsp;
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="field-label"> </asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDesc" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDesc"
                                            Display="Dynamic" ErrorMessage="Description can not be left empty." CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblCond" runat="server" Text=" Country ID" CssClass="field-label"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCountry" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">

                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" />

                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

