Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class AccAddBankTran
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrErrMsg As String
    Dim BSU_IsTAXEnabled As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbUploadEmployee)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(ddlCostCenter)
        LockControls()
        chkPrintChq.Visible = False


        If Page.IsPostBack = False Then
            Try
                'TAX CODE
                Dim ds7 As New DataSet
                Dim pParms0(2) As SqlClient.SqlParameter
                pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms0(1).Value = Session("sBSUID")
                ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
                BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
                ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
                If BSU_IsTAXEnabled = True Then
                    trTaxType.Visible = True
                    LOAD_TAX_CODES()
                End If
                'TAX CODE


                InitialiseControls()
                usrCostCenter1.TotalAmountControlName = "txtLineAmount"
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                btnFillCancel.Visible = False

                If (Session("datamode") = "add") Then
                    Call Clear_Details()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Session("gintGridLine") = 1
                    Session("dtSettle").rows.clear()
                    h_NextLine.Value = Session("gintGridLine")
                    GridInitialize()
                    Session("gDtlDataMode") = "ADD"
                    txtdocDate.Text = GetDiplayDate()
                    If Session("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                    End If
                    hCheqBook.Value = 0
                    bind_Currency()
                End If

                set_bankaccount()

                GetSignature()

                If Session("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If

                If USR_NAME = "" Or (Session("MainMnu_code") <> "A150010" And Session("MainMnu_code") <> "A200011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If Session("datamode") = "view" Then
                        '   --- Fill Data ---
                        bind_Currency()
                        FillValues()
                        If btnPrint.Visible = True Then
                            chkPrintChq.Visible = True
                        End If
                    End If
                End If
                '   --- Checking End  ---
                'SetCostcenterControls()
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
            lbSettle.Visible = True
        Else
            If (Session("datamode") = "add") Then

                'txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
                Session("SessDocDate") = txtdocDate.Text
            End If
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If
        If Session("datamode") <> "add" Then
            btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
            ImageButton1.Enabled = False
        Else
            ImageButton1.Enabled = True
        End If
    End Sub
    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub
    Private Sub InitialiseControls()
        Session("BANKTRAN") = "BP"
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Session("CHECKLAST") = 0
        lbSettle.Visible = False

        Session("dtSettle") = DataTables.CreateDataTable_Settle
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        Session("dtDTL") = DataTables.CreateDataTable_BP()

        txtNarrn.Attributes.Add("onBlur", " CopyDetails();narration_check('" & txtNarrn.ClientID & "');")
        txtLineNarrn.Attributes.Add("onBlur", "narration_check('" & txtLineNarrn.ClientID & "');")
        txtPettycashId.Attributes.Add("ReadOnly", "ReadOnly")
        txtPettycashdocno.Attributes.Add("ReadOnly", "ReadOnly")
        Session("idCostChild") = 0
        Session("idCostAlocation") = 0
        btnSettle.Visible = False
        lnkCostUnit.Enabled = False
        lnkCostUnit.Visible = False
        set_chequeno_controls()
        '   --- For Checking Rights And Initilize The Edit Variables --
        Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        gvDTL.Attributes.Add("bordercolor", "#1b80b6")
        imgCashFlow.OnClientClick = "popUp('460','400','CASHFLOW_BP','" & txtCashFlow.ClientID & "');return false;"
    End Sub

    Private Sub FillLotNo()
        If txtBankCode.Text <> "" Then
            Dim str_alloted_nos As String = String.Empty
            If Not Session("chqNos") Is Nothing Then
                str_alloted_nos = GetAllotedChequeNos(Session("chqNos"))
            End If

            Dim str_sql As String = " SELECT TOP 1 ISNULL(CHQBOOK_M.CHB_LOTNO,'') AS LOT_NO, " & _
            " CHB_ID, ISNULL(MIN(ISNULL(CHD_NO,'')),'') AS CHD_NO FROM CHQBOOK_M INNER JOIN " & _
            " CHQBOOK_D ON CHQBOOK_M.CHB_ID = CHQBOOK_D.CHD_CHB_ID WHERE " & _
            " (CHQBOOK_D.CHD_ALLOTED = 0 OR CHD_DOCNO = '" & txtdocNo.Text & "') AND (CHQBOOK_M.CHB_ACT_ID = '" & txtBankCode.Text & "')" & _
            " AND CHQBOOK_M.CHB_BSU_ID = '" & Session("sBSUID") & "'"
            If Not Session("chqNos") Is Nothing Then
                str_sql += " AND CHD_NO NOT IN (" & str_alloted_nos & ")"
            End If
            str_sql += "GROUP BY CHB_LOTNO, CHB_ID "
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            While (dr.Read())
                hCheqBook.Value = dr("CHB_ID")
                txtChqNo.Text = dr("CHD_NO")
                If dr("LOT_NO") <> 0 Then
                    txtChqBook.Text = dr("LOT_NO")
                Else
                    txtChqBook.Text = ""
                End If
            End While

        End If
    End Sub

    Private Function GetAllotedChequeNos(ByVal arrList As Hashtable) As String
        Dim str_chqNos As String = String.Empty
        Dim comma As String = String.Empty
        Dim ienum As IDictionaryEnumerator = arrList.GetEnumerator
        While (ienum.MoveNext())
            str_chqNos += comma & "'" & ienum.Value & "'"
            comma = ","
        End While
        Return str_chqNos
    End Function

    Private Sub LockControls()
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtProvDescr.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtPartyDescr.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtChqNo.Attributes.Add("readonly", "readonly")
        txtCashFlow.Attributes.Add("readonly", "readonly")
        txtAmount.Attributes.Add("readonly", "readonly")
    End Sub


    Private Sub bind_Currency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            If txtdocDate.Text = "" Then
                txtdocDate.Text = GetDiplayDate()
            End If
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If cmbCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            Else

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            'Else
            'End If
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub GridInitialize()
        gvDTL.DataBind()
        SetGridColumnVisibility(False)
    End Sub

    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If

        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If

        Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function

    Public Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        Dim i, lintIndex As Integer
        Dim lstrChqBookId, lstrChqBookLotNo As String
        Dim ldrNew As DataRow
        If ViewState("datamode") = "view" Then
            lblError.Text = "Record not in edit mode !!!"
            Exit Sub
        End If


        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If Trim(Detail_ACT_ID.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Party " & "<br>"
        End If
        If rbCheque.Checked = True Then
            If Trim(txtChqBook.Text) = "" Then
                lstrErrMsg = lstrErrMsg & "Enter the cheque book " & "<br>"
            End If
            If Trim(txtChqNo.Text) = "" Then
                lstrErrMsg = lstrErrMsg & "Enter the cheque book no " & "<br>"
            End If
        End If

        If Trim(txtChqDate.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the cheque/ref date " & "<br>"
        End If
        ''''
        Dim strfDate As String = txtChqDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lstrErrMsg = lstrErrMsg & str_err & "<br>"
        Else
            txtChqDate.Text = strfDate
        End If
        ''''
        If Not IsNumeric(txtLineAmount.Text) Then
            lstrErrMsg = lstrErrMsg & "Enter the Amount " & "<br>"
        End If

        If (IsNumeric(txtLineAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If Trim(txtCashFlow.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Cash Flow " & "<br>"
        End If

        If GetAccountDetails(Detail_ACT_ID.Text) Then
            If txtExpCode.Text = "" Then
                lblError.Text = "Please Specify CostUnit/Expense Details"
                Exit Sub
            End If
            If txtTo.Text = "" Then
                lblError.Text = "Please Specify CostUnit ToDate"
                Exit Sub
            End If
            If DateDiff("m", Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text)) < 2 Then
                lblError.Text = "CostUnit ToDate must be 2 months greater than FromDate"
                Exit Sub
            End If
            If IsDate(txtTo.Text) = False Then
                lblError.Text = "Invalid CostUnit ToDate"
                Exit Sub
            End If
        End If

        If (lstrErrMsg <> "") Then
            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
            Exit Sub
        End If
        'Check CostCenter Summary
        RecreateSsssionDataSource()
        If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                             txtLineAmount.Text) Then
            lblError.Text = "Invalid Cost Center Allocation!!!"
            Exit Sub
        End If
        '   --- (1) END OF VALIDATION DURING ADD
        '''''FIND ACCOUNT IS THERE

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
            & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPartyDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                lblError.Text = getErrorMessage("303") ' account already there
                Exit Sub
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Errorlog(ex.Message)
        End Try
        '''''FIND ACCOUNT IS THERE
        '   --- (2) PROCEED TO SAVE IN GRID IF NO ERRORS FOUND
        If (lstrErrMsg = "") Then
            'tr_errLNE.Visible = False
            Try
                If Session("gDtlDataMode") = "ADD" Then

                    ldrNew = Session("dtDTL").NewRow
                    ldrNew("Id") = Session("gintGridLine")

                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("gintGridLine"), Session("sBsuid"), _
                                          Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If

                    Session("gintGridLine") = Session("gintGridLine") + 1
                    h_NextLine.Value = Session("gintGridLine")
                    ldrNew("AccountId") = Trim(Detail_ACT_ID.Text)
                    ldrNew("AccountName") = Trim(txtPartyDescr.Text)
                    ldrNew("Narration") = Trim(txtLineNarrn.Text)
                    ldrNew("CLine") = Trim(txtCashFlow.Text)
                    ldrNew("PettycashId") = Trim(txtPettycashId.Text) 'sadhu add
                    ldrNew("PettycashName") = Trim(txtPettycashdocno.Text) 'sadhu add
                    If ldrNew("PettycashName").Equals("") Then
                        ldrNew("PettycashName") = "0"
                    End If

                    'lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False)
                    'lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                    'lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                    'lstrChqBookId = lstrChqDet.Split("|")(2)
                    'lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                    lstrChqBookId = hCheqBook.Value
                    lstrChqBookLotNo = txtChqBook.Text

                    If rbCheque.Checked = True Then
                        ldrNew("ChqBookId") = lstrChqBookId
                        ldrNew("ChqBookLot") = lstrChqBookLotNo
                        ldrNew("ChqNo") = txtChqNo.Text
                        If Session("chqNos") Is Nothing Then
                            Session("chqNos") = New Hashtable
                        End If
                        Session("chqNos")(CInt(ldrNew("Id"))) = txtChqNo.Text
                    Else
                        ldrNew("ChqBookId") = 0
                        ldrNew("ChqBookLot") = 0
                        ldrNew("ChqNo") = txtrefChequeno.Text
                    End If

                    ldrNew("ChqDate") = Trim(txtChqDate.Text)

                    ldrNew("Amount") = Convert.ToDecimal(Trim(txtLineAmount.Text))
                    ldrNew("Status") = ""
                    ldrNew("GUID") = System.DBNull.Value
                    ldrNew("isCheque") = rbCheque.Checked
                    If BSU_IsTAXEnabled Then
                        ldrNew("TaxCode") = ddlVATCode.SelectedValue
                    Else
                        ldrNew("TaxCode") = ""
                    End If


                    If GetAccountDetails(Detail_ACT_ID.Text) Then
                        ldrNew("CostUnit") = IIf(hfId1.Value <> "", hfId1.Value, "")
                        ldrNew("FromDate") = IIf(txtFrom.Text.Trim <> "", txtFrom.Text.Trim, "")
                        ldrNew("ToDate") = IIf(txtTo.Text.Trim <> "", txtTo.Text.Trim, "")
                        ldrNew("ExpAcc") = IIf(txtExpCode.Text.Trim <> "", txtExpCode.Text.Trim, "")
                        ldrNew("ExpAccName") = IIf(txtPreAcc.Text.Trim <> "", txtPreAcc.Text.Trim, "")
                    Else
                        ldrNew("CostUnit") = ""
                        ldrNew("FromDate") = ""
                        ldrNew("ToDate") = ""
                        ldrNew("ExpAcc") = ""
                        ldrNew("ExpAccName") = ""
                    End If



                    ' isCheque  --- Check if these details are already there before adding to the grid
                    For i = 0 To Session("dtDTL").Rows.Count - 1
                        If Session("dtDTL").Rows(i)("Accountid") = ldrNew("Accountid") And _
                            Session("dtDTL").Rows(i)("Accountname") = ldrNew("Accountname") And _
                             Session("dtDTL").Rows(i)("Narration") = ldrNew("Narration") And _
                             Session("dtDTL").Rows(i)("CLine") = ldrNew("CLine") And _
                             Session("dtDTL").Rows(i)("ChqNo") = ldrNew("ChqNo") And _
                             Session("dtDTL").Rows(i)("Amount") = ldrNew("Amount") Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            GridBind()
                            Exit Sub
                        End If
                        If Session("dtDTL").Rows(i)("PettycashId") = ldrNew("PettycashId") Then 'sadhu add
                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        End If
                    Next
                    Session("dtDTL").Rows.Add(ldrNew)
                    chkAdvance.Enabled = False


                    'If GetAccountDetails(Detail_ACT_ID.Text) Then
                    '    ldrCUTNew = Session("dtCUT").NewRow
                    '    ldrCUTNew("CostUnit") = txtCostUnit.Text
                    '    ldrCUTNew("CostUnitID") = hfId1.Value
                    '    ldrCUTNew("FromDate") = txtFrom.Text
                    '    ldrCUTNew("ToDate") = txtTo.Text
                    '    ldrCUTNew("ExpAcc") = txtExpCode.Text
                    '    ldrCUTNew("ExpAccName") = txtPreAcc.Text
                    '    Session("dtCUT").Rows.Add(ldrCUTNew)
                    'End If

                    GridBind()
                    'FillLotNo()



                ElseIf (Session("gDtlDataMode") = "UPDATE") Then
                    For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                        If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then

                            If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                                CostCenterFunctions.AddCostCenter(Session("gintEditLine"), Session("sBsuid"), _
                                                  Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                                 Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                                 Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                            End If

                            Session("dtDTL").Rows(lintIndex)("AccountId") = Detail_ACT_ID.Text
                            Session("dtDTL").Rows(lintIndex)("AccountName") = txtPartyDescr.Text
                            Session("dtDTL").Rows(lintIndex)("Narration") = txtLineNarrn.Text
                            Session("dtDTL").Rows(lintIndex)("CLine") = txtCashFlow.Text
                            Session("dtDTL").Rows(lintIndex)("PettycashId") = txtPettycashId.Text 'sadhu add
                            Session("dtDTL").Rows(lintIndex)("PettycashName") = txtPettycashdocno.Text ' sadhu add
                            Session("dtDTL").Rows(i)("Accountname") = txtPartyDescr.Text
                            If BSU_IsTAXEnabled Then
                                Session("dtDTL").Rows(i)("TaxCode") = ddlVATCode.SelectedValue
                            Else
                                Session("dtDTL").Rows(i)("TaxCode") = ""
                            End If



                            ''''''''''
                            If rbCheque.Checked = True Then
                                Session("dtDTL").Rows(lintIndex)("ChqBookId") = hCheqBook.Value
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtChqBook.Text
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = txtChqNo.Text
                                If Session("chqNos") Is Nothing Then
                                    Session("chqNos") = New Hashtable
                                End If
                                Session("chqNos")(CInt(Session("gintEditLine"))) = txtChqNo.Text
                            Else
                                Session("dtDTL").Rows(lintIndex)("ChqBookId") = 0
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = 0
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = txtrefChequeno.Text

                            End If
                            '''''''''''
                            'Session("dtDTL").Rows(lintIndex)("ChqBookId") = hCheqBook.Value
                            'Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtChqBook.Text
                            'Session("dtDTL").Rows(lintIndex)("ChqNo") = txtChqNo.Text

                            Session("dtDTL").Rows(lintIndex)("ChqDate") = txtChqDate.Text
                            Session("dtDTL").Rows(lintIndex)("Amount") = txtLineAmount.Text

                            Session("dtDTL").Rows(lintIndex)("isCheque") = rbCheque.Checked

                            If GetAccountDetails(Detail_ACT_ID.Text) Then
                                Session("dtDTL").Rows(lintIndex)("CostUnit") = hfId1.Value
                                Session("dtDTL").Rows(lintIndex)("FromDate") = txtFrom.Text
                                Session("dtDTL").Rows(lintIndex)("ToDate") = txtTo.Text
                                Session("dtDTL").Rows(lintIndex)("ExpAcc") = txtExpCode.Text
                                Session("dtDTL").Rows(lintIndex)("ExpAccName") = txtPreAcc.Text
                            End If
                            SetGridColumnVisibility(True)
                            Session("gDtlDataMode") = "ADD"
                            btnFill.Text = "ADD"
                            h_NextLine.Value = Session("gintGridLine")
                            gvDTL.SelectedIndex = -1
                            Clear_Details_Partial()
                            GridBind()
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
            CalcuTot()
        End If
        '   --- (2) END OF SAVE IN GRID IF NO ERRORS FOUND
    End Sub


    Private Sub CalcuTot()
        Dim ldblTot As Decimal
        Dim i As Integer
        ldblTot = 0
        For i = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                ldblTot = ldblTot + Session("dtDTL").Rows(i)("Amount")
            End If
        Next
        txtAmount.Text = AccountFunctions.Round(ldblTot)
    End Sub

    Private Sub GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = DataTables.CreateDataTable_BP()
        If Session("dtDTL").Rows.Count > 0 Then
            For i = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                        ldrTempNew.Item(j) = Session("dtDTL").Rows(i)(j)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvDTL.DataSource = dtTempDtl
        gvDTL.DataBind()
        SetGridColumnVisibility(True)
        If Session("dtDTL").Rows.Count <= 0 Then
            GridInitialize()
        End If
        Clear_Details_Partial()
    End Sub


    Private Sub Clear_Details()
        btnTRNSDetails.Visible = False
        btnUtilitPaymentDetails.Visible = False
        Detail_ACT_ID.Text = ""
        txtPartyDescr.Text = ""
        txtLineNarrn.Text = ""
        txtLineAmount.Text = ""
        txtCashFlow.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""

        txtFrom.Text = ""
        txtTo.Text = ""
        txtExpCode.Text = ""
        txtPreAcc.Text = ""
        txtCostUnit.Text = ""
        txtPettycashdocno.Text = ""
        txtPettycashId.Text = ""
        lnkCostUnit.Enabled = False
        lnkCostUnit.Visible = False
    End Sub


    Private Sub Clear_Details_Partial()
        btnTRNSDetails.Visible = False
        btnUtilitPaymentDetails.Visible = False
        Detail_ACT_ID.Text = ""
        txtPartyDescr.Text = ""
        txtLineNarrn.Text = txtNarrn.Text
        txtLineAmount.Text = ""
        txtPettycashdocno.Text = ""
        txtPettycashId.Text = ""
        ClearRadGridandCombo()
        btnFillCancel.Visible = False
    End Sub


    Private Sub Clear_Header()
        txtdocNo.Text = ""
        txtOldDocNo.Text = ""
        txtdocDate.Text = GetDiplayDate()
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtProvCode.Text = ""
        txtProvDescr.Text = ""
        txtNarrn.Text = ""
        ChkBearer.Checked = False
        bind_Currency()
        set_bankaccount()
        GetSignature()
    End Sub


    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub


    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lintIndex As Integer = 0
        Dim IntCUTid As Integer
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)
        btnFillCancel.Visible = True
        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                Detail_ACT_ID.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountId"))
                txtPartyDescr.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountName"))
                txtLineNarrn.Text = Trim(Session("dtDTL").Rows(lintIndex)("Narration"))
                txtCashFlow.Text = Trim(Session("dtDTL").Rows(lintIndex)("CLine"))
                txtPettycashId.Text = Trim(Session("dtDTL").Rows(lintIndex)("PettycashId"))
                txtPettycashdocno.Text = Trim(Session("dtDTL").Rows(lintIndex)("PettycashName"))

                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Trim(Session("dtDTL").Rows(lintIndex)("TaxCode"))
                End If


                h_NextLine.Value = Session("gintEditLine")
                txtChqDate.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqDate"))
                txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Amount"))


                ShowUploadOption()
                RecreateSsssionDataSource()
                CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()

                '''''

                If GetAccountDetails(Detail_ACT_ID.Text) = True Then
                    lnkCostUnit.Enabled = True
                    lnkCostUnit.Visible = True

                    txtFrom.Text = IIf(Trim(Session("dtDTL").Rows(lintIndex)("FromDate")) <> "", Convert.ToDateTime(Trim(Session("dtDTL").Rows(lintIndex)("FromDate"))).ToShortDateString, "")
                    txtTo.Text = IIf(Trim(Session("dtDTL").Rows(lintIndex)("ToDate")) <> "", Convert.ToDateTime(Trim(Session("dtDTL").Rows(lintIndex)("ToDate"))).ToShortDateString, "")
                    txtExpCode.Text = IIf(Trim(Session("dtDTL").Rows(lintIndex)("ExpAcc")) <> "", Trim(Session("dtDTL").Rows(lintIndex)("ExpAcc")), "")
                    txtPreAcc.Text = IIf(Trim(Session("dtDTL").Rows(lintIndex)("ExpAccName")) <> "", Trim(Session("dtDTL").Rows(lintIndex)("ExpAccName")), "")
                    IntCUTid = IIf(Trim(Session("dtDTL").Rows(lintIndex)("CostUnit")) <> "", Trim(Session("dtDTL").Rows(lintIndex)("CostUnit")), 0)
                    hfId1.Value = IntCUTid
                    If IntCUTid > 0 Then
                        Dim strsql As String
                        Dim dsCU As DataSet
                        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                        strsql = "SELECT CUT_DESCR FROM COSTUNIT_M WHERE CUT_ID=" & IntCUTid & ""
                        dsCU = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
                        If dsCU.Tables(0).Rows.Count > 0 Then
                            txtCostUnit.Text = dsCU.Tables(0).Rows(0)("CUT_DESCR")
                        Else
                            txtCostUnit.Text = ""
                        End If
                    End If
                Else
                    lnkCostUnit.Enabled = False
                    lnkCostUnit.Visible = False
                    txtFrom.Text = ""
                    txtTo.Text = ""
                    txtExpCode.Text = ""
                    txtPreAcc.Text = ""
                    txtCostUnit.Text = ""
                    hfId1.Value = ""
                End If
                'isCheque
                If Session("dtDTL").Rows(lintIndex)("isCheque") = True Then
                    rbCheque.Checked = True
                    rbOthers.Checked = False
                    hCheqBook.Value = Trim(Session("dtDTL").Rows(lintIndex)("ChqBookId"))
                    txtChqBook.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqBookLot"))
                    txtChqNo.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqNo"))
                    txtrefChequeno.Text = ""
                Else
                    btnTRNSDetails.Visible = (Session("datamode") = "edit" Or Session("datamode") = "view")
                    rbCheque.Checked = False
                    rbOthers.Checked = True
                    hCheqBook.Value = ""
                    txtChqBook.Text = ""
                    txtChqNo.Text = ""
                    txtrefChequeno.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqNo"))
                End If
                'btnUtilitPaymentDetails.Visible = (Session("sBsuid") = "125015" And (Detail_ACT_ID.Text = "063D0084" Or Detail_ACT_ID.Text = "063E0015"))
                btnUtilitPaymentDetails.Visible = (SqlHelper.ExecuteScalar(WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString, CommandType.Text, "select count(*) from utility where uti_bsu_id='" & Session("sBsuid") & "' and uti_act_id='" & Detail_ACT_ID.Text & "'") > 0)

                set_chequeno_controls()
                gvDTL.SelectedIndex = lintIndex
                gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                Session("gDtlDataMode") = "UPDATE"
                btnFill.Text = "UPDATE"
                SetGridColumnVisibility(False)
                Exit For
            End If
        Next
        Set_SettleControls()
        txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Amount"))
    End Sub


    Protected Sub SetGridColumnVisibility(ByVal pSet As Boolean)
        gvDTL.Columns(12).Visible = pSet
        gvDTL.Columns(13).Visible = pSet
        gvDTL.Columns(14).Visible = pSet

        If BSU_IsTAXEnabled = True Then
            gvDTL.Columns(11).Visible = True
        Else
            gvDTL.Columns(11).Visible = False
        End If
    End Sub

    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        h_NextLine.Value = Session("gintGridLine")
        SetGridColumnVisibility(True)
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
        ClearRadGridandCombo()
        btnFillCancel.Visible = False
    End Sub


    Protected Sub DeleteRecordByID(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim j As Integer
        For iRemove = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iRemove)("id") = pId) Then
                Session("dtDTL").Rows(iRemove)("Status") = "DELETED"

                '   --- Remove The Corresponding Rows From The Detail Grid Also
                'For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                While j < Session("dtCostChild").Rows.Count
                    If (Session("dtCostChild").Rows(j)("VoucherId") = pId) Then
                        Session("dtCostChild").Rows(j)("Status") = "DELETED"
                    Else
                        j = j + 1
                    End If
                End While
            End If
        Next
        GridBind()
        CalcuTot()
    End Sub

    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblReqd As New Label
            'Dim lbAllocate As New LinkButton
            Dim lblid As New Label
            Dim lblAmount As New Label

            lblReqd = e.Row.FindControl("lblCostReqd")
            lblid = e.Row.FindControl("lblId")
            lblAmount = e.Row.FindControl("lblAmount")
            'lbAllocate = e.Row.FindControl("lbAllocate")
            If lblReqd IsNot Nothing Then
                If lblReqd.Text = "True" Then
                    e.Row.BackColor = Drawing.Color.Pink
                End If
            End If
            Dim gvCostchild As New GridView
            gvCostchild = e.Row.FindControl("gvCostchild")
            gvCostchild.Attributes.Add("bordercolor", "#fc7f03")
            'ClientScript.RegisterStartupScript([GetType](), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" & lblid.Text & "','one');</script>")
            If Not Session("dtCostChild") Is Nothing Then
                Dim dv As New DataView(Session("dtCostChild"))
                dv.RowFilter = "VoucherId='" & lblid.Text & "' "
                dv.Sort = "MemberId"
                gvCostchild.DataSource = dv.ToTable
                gvCostchild.DataBind()
            End If
            Dim l As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
            If l IsNot Nothing Then
                l.Attributes.Add("onclick", "javascript:return " + "confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "id") + "')")
            End If
        End If
    End Sub

    Protected Sub gvDTL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDTL.RowDeleting
        Dim categoryID As Integer = CInt(gvDTL.DataKeys(e.RowIndex).Value)
        DeleteRecordByID(categoryID)
        Dim il As Integer = 0
        While il < Session("dtSettle").Rows.Count
            If Session("dtSettle").rows(il)("Id") = categoryID Then
                Session("dtSettle").rows(il).delete()
            Else
                il = il + 1
            End If
        End While
    End Sub

    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")

                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception

                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
           & " GUID='" & Session("Eid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = Session("sBSUId")
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR")
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        Dim iIndex As Integer
        Dim cIndex As Integer
        Dim lblnFound As Boolean

        lstrErrMsg = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If


        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If

        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If

        If (IsNumeric(txtAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If


        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Debit Details " & "<br>"
        End If

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (DateDiff(DateInterval.Day, Convert.ToDateTime(txtdocDate.Text), Convert.ToDateTime(Session("dtDTL").Rows(iIndex)("ChqDate"))) >= 1) Then
                If Trim(txtProvCode.Text = "") Then
                    lstrErrMsg = lstrErrMsg & "Invalid Provision Account " & "<br>"
                    Exit For
                End If
            End If
        Next


        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            'tr_errLNE.Visible = False
            Return True
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String = ""
        Dim lstrChqBookId As String
        Dim lintColId As Integer
        Dim lblnNoErr As Boolean

        '   ----------------- VALIDATIONS --------------------
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If

        lblnNoErr = SaveValidate()
        If (lblnNoErr = False) Then
            Exit Sub
        End If

        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            Exit Sub
        Else
            lblError.Text = ""
        End If
        'If CDate(txtDocDate.Text) < CDate(txtChqDate.Text)  Then
        '    txtProvDescr.Text = AccountFunctions.Validate_Account(txtProvCode.Text, Session("sbsuid"), "PREPDAC")
        '    If txtProvDescr.Text = "" Then
        '        lblError.Text = "Invalid Provision selected"
        '        txtProvCode.Focus()
        '        Exit Sub
        '    End If
        'End If
        If txtProvCode.Text.Trim <> "" Then
            txtProvDescr.Text = AccountFunctions.Validate_Account(txtProvCode.Text, Session("sbsuid"), "PREPDAC")
            If txtProvDescr.Text = "" Then
                lblError.Text = "Invalid Provision selected"
                txtProvCode.Focus()
                Exit Sub
            End If
        End If

        '   ----------------- END OG VALIDATE -----------------
        If (Session("BANKTRAN") = "BP") Then
            lstrChqBookId = hCheqBook.Value
            lintColId = 0
        Else
            lstrChqBookId = ""
            lintColId = 0
        End If

        Try
            Session("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                SqlCmd.Parameters.Add(sqlpGUID)
                SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
                SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", Session("BANKTRAN"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", Trim(txtdocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_REFNO", Trim(txtOldDocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
                If hCheqBook.Value.Trim = "" Then
                    hCheqBook.Value = "0"
                End If
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", Convert.ToInt32(hCheqBook.Value))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtdocDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChqDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", hNum.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", hNum.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", Convert.ToDecimal(txtAmount.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
                SqlCmd.Parameters.AddWithValue("@VHH_bAuto", False)
                SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

                SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", txtProvCode.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

                SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", cmbCurrency.SelectedItem.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", txtExchRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", txtLocalRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", lintColId)
                SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
                SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
                SqlCmd.Parameters.AddWithValue("@VHH_SIGN", txtSignedBy.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_SIGN_DESIG", txtDesignation.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", chkAdvance.Checked)

                SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True) '@VHH_bAdvance, 
                If txtReceivedby.Text.Trim = "" Then
                    SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", Session("dtDTL").Rows(0)("AccountName"))
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txtReceivedby.Text.Trim)
                End If

                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If Session("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
                End If
                SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
                SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
                If ChkBearer.Checked Then
                    SqlCmd.Parameters.AddWithValue("@VHH_BBearer", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_BBearer", False)
                End If

                SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                If (Session("datamode") = "edit") Then
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                End If
                SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)

                If (lintRetVal = 0) Then
                    If (Session("datamode") = "edit") Then
                        lstrNewDocNo = txtdocNo.Text
                    Else
                        lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
                    End If
                    'Adding header info
                    SqlCmd.Parameters.Clear()

                    If Session("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo)
                    Else
                        str_err = DeleteVOUCHER_D_S_ALL(objConn, stTrans, txtdocNo.Text)
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, txtdocNo.Text)
                        End If
                    End If

                    If str_err = "0" Then
                        h_editorview.Value = ""
                        stTrans.Commit()
                        If Session("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        End If
                        Try
                            Call Clear_Header()
                            Call Clear_Details()

                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                            Session("gintGridLine") = 1
                            Session("dtSettle").rows.clear()
                            'Session("dtCUT").rows.clear()
                            h_NextLine.Value = Session("gintGridLine")
                            GridInitialize()
                            Session("gDtlDataMode") = "ADD"
                            Session("dtDTL") = DataTables.CreateDataTable_BP()
                            Session("dtCostChild").rows.clear()
                            'txtdocDate.Text = Format(Session("EntryDate"), "dd/MMM/yyyy")
                            txtdocDate.Text = GetDiplayDate()
                            If Session("datamode") = "add" Then
                                txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                            End If
                            txtAmount.Text = ""
                            bind_Currency()
                        Catch ex As Exception
                            Dim err As String = Session("sUsr_name") + " - Docs Nos. " + lstrNewDocNo + " - " + ex.Message
                            Errorlog(err)
                        End Try

                        lblError.Text = "Data Successfully Saved..."
                    Else
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                    End If
                Else
                    If Session("datamode") = "add" And lintRetVal = "301" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                        lblError.Text = "Duplicate Journal Document.Please try to save again."
                    Else
                        lblError.Text = getErrorMessage(lintRetVal)
                    End If
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                Dim err As String = Session("sUsr_name") + " - Doc Nos. " + lstrNewDocNo + " - " + ex.Message
                Errorlog(err)
                stTrans.Rollback()

            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            'lblError.Text = getErrorMessage("1000")
            'Errorlog(ex.Message)
            Dim err As String = Session("sUsr_name") + " - Doc No. " + lstrNewDocNo + " - " + ex.Message
            UtilityObj.Errorlog(err)
        End Try
    End Sub

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString


        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = Session("BANKTRAN")
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Dim lstrChqBookId As String
        Dim lintColId As Integer
        Dim isSupplier As Boolean

        If (Session("BANKTRAN") = "BP") Then
            lstrChqBookId = hCheqBook.Value
            lintColId = 0
        Else
            lstrChqBookId = ""
            lintColId = 0
        End If

        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        Dim lstrChqNo As String
        Dim lintChbId As Integer

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1

            If Session("dtDTL").Rows(iIndex)("Status") & "" <> "DELETED" Then
                Dim acttype As String = AccountFunctions.check_accounttype(Session("dtDTL").Rows(iIndex)("Accountid"), Session("sBsuid"))
                If acttype = "S" And (Session("BANKTRAN") = "BP") Then
                    isSupplier = True
                Else
                    isSupplier = False
                End If
                If chkAdvance.Checked Then
                    isSupplier = False
                End If

                cmd.Dispose()
                cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table
                Dim str_crdb As String = "CR"
                If Session("BANKTRAN") = "BP" Then
                    dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                    str_crdb = "DR"

                    '   --- Get The Cheque Nos
                    'Dim str_Sql As String = "select CHB_NEXTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & Session("dtDTL").Rows(iIndex)("ChqBookId") & "'"
                    'Dim ds As New DataSet
                    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    'If ds.Tables(0).Rows.Count > 0 Then
                    lstrChqNo = Trim(Session("dtDTL").Rows(iIndex)("ChqNo"))
                    'End If
                Else
                    dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                    str_crdb = "CR"
                    lintChbId = 0
                    lstrChqNo = Trim(Session("dtDTL").Rows(iIndex)("ChqNo"))
                End If

                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtDTL").Rows(iIndex)("id"), _
                str_crdb, iIndex + 1 - Session("iDeleteCount"), Session("dtDTL").Rows(iIndex)("Accountid"), dTotal)
                If str_err <> "0" Then
                    Return str_err
                End If
                '' ''

                cmd.Parameters.AddWithValue("@GUID", Session("dtDTL").Rows(iIndex)("GUID"))
                cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
                cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
                cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
                cmd.Parameters.AddWithValue("@VHD_DOCTYPE", Session("BANKTRAN"))
                cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
                cmd.Parameters.AddWithValue("@VHD_LINEID", Session("dtDTL").Rows(iIndex)("Id"))
                cmd.Parameters.AddWithValue("@VHD_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
                cmd.Parameters.AddWithValue("@VHD_AMOUNT", Session("dtDTL").Rows(iIndex)("Amount"))
                cmd.Parameters.AddWithValue("@VHD_NARRATION", Session("dtDTL").Rows(iIndex)("Narration"))
                cmd.Parameters.AddWithValue("@VHD_CHQID", Session("dtDTL").Rows(iIndex)("ChqBookId"))
                cmd.Parameters.AddWithValue("@VHD_CHQNO", Session("dtDTL").Rows(iIndex)("ChqNo"))
                cmd.Parameters.AddWithValue("@VHD_CHQDT", Convert.ToDateTime(Session("dtDTL").Rows(iIndex)("ChqDate")))
                cmd.Parameters.AddWithValue("@VHD_RSS_ID", Session("dtDTL").Rows(iIndex)("CLine"))
                cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
                cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
                cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
                cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
                cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
                cmd.Parameters.AddWithValue("@VHD_bCheque", Session("dtDTL").Rows(iIndex)("isCheque")) '@VHD_bCheque
                '  Session("dtDTL").Rows(lintIndex)("isCheque")
                cmd.Parameters.AddWithValue("@VHD_COL_ID", Session("dtDTL").Rows(iIndex)("PettycashId"))


                If BSU_IsTAXEnabled = True Then
                    cmd.Parameters.AddWithValue("@VHD_TAX_CODE", Session("dtDTL").Rows(iIndex)("TaxCode"))
                Else
                    cmd.Parameters.AddWithValue("@VHD_TAX_CODE", "")
                End If

                If (Session("datamode") = "edit") Then
                    cmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    cmd.Parameters.AddWithValue("@bEdit", False)
                End If
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                    Exit For
                Else
                    AccountFunctions.SAVETRANHDRONLINE_D("", Session("SUB_ID"), _
                    Session("dtDTL").Rows(iIndex)("Id"), Session("BANKTRAN"), cmbCurrency.SelectedItem.Text, _
                    txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), Session("F_YEAR"), _
                    p_docno, Session("dtDTL").Rows(iIndex)("AccountId"), "DR", Session("dtDTL").Rows(iIndex)("Amount"), _
                    txtdocDate.Text, "", Session("dtDTL").Rows(iIndex)("ChqDate"), _
                    Session("dtDTL").Rows(iIndex)("ChqNo"), False, Session("dtDTL").Rows(iIndex)("Narration"), stTrans)
                    Dim dblAmtAlloc As Double = 0
                    For il As Integer = 0 To Session("dtSettle").Rows.count - 1
                        If Session("dtDTL").Rows(iIndex)("Id") = Session("dtSettle").rows(il)("Id") And _
                        Session("dtDTL").Rows(iIndex)("AccountId") = Session("dtSettle").rows(il)("Accountid") Then
                            Dim SOL_DOCDT As DateTime

                            If CDate(txtdocDate.Text) > CDate(Session("dtDTL").Rows(iIndex)("ChqDate")) Then
                                SOL_DOCDT = txtdocDate.Text
                            Else
                                SOL_DOCDT = Session("dtDTL").Rows(iIndex)("ChqDate")
                            End If


                            str_err = AccountFunctions.SAVESETTLEONLINE_D(Session("Sub_ID"), Session("dtDTL").Rows(iIndex)("Id"), _
                                  Session("BANKTRAN"), txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), _
                                  Session("F_YEAR"), p_docno, _
                                  Session("dtDTL").Rows(iIndex)("AccountId"), Session("dtSettle").rows(il)("Amount"), cmbCurrency.SelectedItem.Text, _
                                SOL_DOCDT, Session("dtSettle").rows(il)("jnlid"), stTrans)
                            dblAmtAlloc = dblAmtAlloc + Convert.ToDecimal(Session("dtSettle").rows(il)("Amount"))
                            dblAmtAlloc = AccountFunctions.Round(dblAmtAlloc)
                        End If

                    Next
                    If dblAmtAlloc <> Session("dtDTL").Rows(iIndex)("Amount") And isSupplier = True Then
                        Return "535"
                    End If
                    If str_err <> "0" Then
                        Return str_err
                        Exit For
                    End If

                End If
                cmd.Parameters.Clear()
                If Session("dtDTL").Rows(iIndex)("AccountId") <> "" Then
                    Dim cmdCST As SqlCommand
                    cmdCST = New SqlCommand("DeleteVOUCHER_D_SUB", objConn, stTrans)
                    cmdCST.CommandType = CommandType.StoredProcedure
                    cmdCST.Parameters.AddWithValue("@GUID", Session("dtDTL").Rows(iIndex)("GUID"))
                    cmdCST.Parameters.AddWithValue("@VSB_SUB_ID", Session("SUB_ID"))
                    cmdCST.Parameters.AddWithValue("@VSB_BSU_ID", Session("sBsuid"))
                    cmdCST.Parameters.AddWithValue("@VSB_FYEAR", Session("F_YEAR"))
                    cmdCST.Parameters.AddWithValue("@VSB_DOCTYPE", Session("BANKTRAN"))
                    cmdCST.Parameters.AddWithValue("@VSB_DOCNO", p_docno)
                    cmdCST.Parameters.AddWithValue("@VSB_LINEID", Session("dtDTL").Rows(iIndex)("Id"))
                    cmdCST.Parameters.AddWithValue("@VSB_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
                    cmdCST.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmdCST.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmdCST.ExecuteNonQuery()
                    iReturnvalue = CInt(cmdCST.Parameters("@ReturnValue").Value)
                End If

                ' If lnkCostUnit.Visible And lnkCostUnit.Enabled Then
                If GetAccountDetails(Session("dtDTL").Rows(iIndex)("AccountId")) Then
                    Dim cmdCUT As SqlCommand
                    cmdCUT = New SqlCommand("SaveVOUCHER_D_SUB", objConn, stTrans)
                    cmdCUT.CommandType = CommandType.StoredProcedure
                    cmdCUT.Parameters.AddWithValue("@GUID", Session("dtDTL").Rows(iIndex)("GUID"))
                    cmdCUT.Parameters.AddWithValue("@VSB_SUB_ID", Session("SUB_ID"))
                    cmdCUT.Parameters.AddWithValue("@VSB_BSU_ID", Session("sBsuid"))
                    cmdCUT.Parameters.AddWithValue("@VSB_FYEAR", Session("F_YEAR"))
                    cmdCUT.Parameters.AddWithValue("@VSB_DOCTYPE", Session("BANKTRAN"))
                    cmdCUT.Parameters.AddWithValue("@VSB_DOCNO", p_docno)
                    cmdCUT.Parameters.AddWithValue("@VSB_LINEID", Session("dtDTL").Rows(iIndex)("Id"))
                    cmdCUT.Parameters.AddWithValue("@VSB_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
                    cmdCUT.Parameters.AddWithValue("@VSB_DTFROM", Format(Date.Parse(Session("dtDTL").Rows(iIndex)("FromDate")), "dd/MMM/yyyy"))
                    cmdCUT.Parameters.AddWithValue("@VSB_DTTO", Format(Date.Parse(Session("dtDTL").Rows(iIndex)("ToDate")), "dd/MMM/yyyy"))
                    cmdCUT.Parameters.AddWithValue("@VSB_CUT_ID", Session("dtDTL").Rows(iIndex)("CostUnit"))
                    cmdCUT.Parameters.AddWithValue("@VSB_PRP_ID", p_docno)
                    cmdCUT.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmdCUT.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmdCUT.ExecuteNonQuery()
                    iReturnvalue = CInt(cmdCUT.Parameters("@ReturnValue").Value)

                End If
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                    Exit For
                End If


                '-----------------added by .. 12 jan
                If Session("dtDTL").Rows(iIndex)("PettycashId").Equals("") Then
                    Session("dtDTL").Rows(iIndex)("PettycashId") = "0"
                End If
                If Not Session("dtDTL").Rows(iIndex)("PettycashId").Equals("0") Then
                    Dim cmdPCH As SqlCommand
                    cmdPCH = New SqlCommand("UpdatePETTYEXP_HDOCNO", objConn, stTrans)
                    cmdPCH.CommandType = CommandType.StoredProcedure
                    cmdPCH.Parameters.AddWithValue("@PCH_ID", Session("dtDTL").Rows(iIndex)("PettycashId"))
                    cmdPCH.Parameters.AddWithValue("@PCH_FYEAR", Session("F_YEAR"))
                    cmdPCH.Parameters.AddWithValue("@PCH_VHH_DOCTYPE", Session("BANKTRAN"))
                    cmdPCH.Parameters.AddWithValue("@PCH_VHH_DOCNO", p_docno)

                    cmdPCH.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmdPCH.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmdPCH.ExecuteNonQuery()
                    iReturnvalue = CInt(cmdPCH.Parameters("@ReturnValue").Value)

                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                        Exit For
                    End If
                End If
            Else
                If Not Session("dtDTL").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    Session("iDeleteCount") = Session("iDeleteCount") + 1
                    cmd = New SqlCommand("DeleteVOUCHER_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Response.Write("DSF")
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtDTL").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If

    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtdocDate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            lbSettle.Visible = False
            Session("datamode") = "edit"
            chkAdvance.Enabled = False
            'Session("dtSettle") = DataTables.CreateDataTable_Settle
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim sqlStr = "select sol_lineid Id, sol_acctcode Accountid, convert(nvarchar(50), TRANHDR_d.[guid]) jnlid, sol_settledamt Amount " & _
            "from SETTLEONLINE_D inner join TRANHDR_d on TRN_BSU_ID=SOL_BSU_ID and SOL_REFDOCNO=TRN_DOCNO and SOL_ACCTCODE=TRN_ACT_ID and TRN_LINEID=SOL_REFLINEID " & _
            "where sol_BSU_ID='" & Session("sBsuid") & "' AND sol_docno in ('" & txtdocNo.Text & "')   "
            Session("dtSettle") = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlStr).Tables(0)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            ImageButton1.Enabled = False
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        h_editorview.Value = ""
        chkAdvance.Enabled = True
        Call Clear_Header()
        Call Clear_Details()
        Session("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Session("gintGridLine") = 1
        Session("dtSettle").rows.clear()

        GridInitialize()
        Session("gDtlDataMode") = "ADD"
        Session("dtDTL") = DataTables.CreateDataTable_BP()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("dtSettle") = DataTables.CreateDataTable_Settle
        'Session("dtCUT") = DataTables.CreateDataTableCostUnit

        Session("dtCostChild").rows.clear()
        txtdocDate.Text = GetDiplayDate()
        If Session("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        bind_Currency()
        lbSettle.Visible = False
        ImageButton1.Enabled = True
        chkAdvance.Enabled = True
    End Sub


    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim IntVHDID As Integer
            Dim StrSql As String
            Dim ds3 As DataSet
            Dim i As Integer
            lstrSQL = "SELECT A.*,isNUll(VHH_CHQ_PDC_ACT_ID,'') as ProvAccount,isNUll(VHH_PROV_ACT_ID,'') as ProvAccount2,REPLACE(CONVERT(VARCHAR(11), A.VHH_DOCDT, 106), ' ', '/') as DocDate ,isNULL(VHH_Amount,0) as Amount,isNULL(A.VHH_REFNO,'') as OldDocNo FROM VOUCHER_H A  WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            'btnSettle.Visible = True
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtOldDocNo.Text = ds.Tables(0).Rows(0)("OldDocNo")
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                txtBankCode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID")
                txtReceivedby.Text = ds.Tables(0).Rows(0)("VHH_RECEIVEDBY").ToString
                txtBankDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ACT_ID"))

                If ds.Tables(0).Rows(0)("ProvAccount") <> "" Then
                    txtProvCode.Text = ds.Tables(0).Rows(0)("ProvAccount")
                    txtProvDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("ProvAccount"))
                Else
                    txtProvCode.Text = ds.Tables(0).Rows(0)("ProvAccount2")
                    txtProvDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("ProvAccount2"))
                End If
                ChkBearer.Checked = True
                If ds.Tables(0).Rows(0)("VHH_BBearer").Equals(DBNull.Value) Then
                    ChkBearer.Checked = False
                ElseIf ds.Tables(0).Rows(0)("VHH_BBearer") = 0 Then
                    ChkBearer.Checked = False
                End If

                txtAmount.Text = AccountFunctions.Round(ds.Tables(0).Rows(0)("Amount"))
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("VHH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                txtSignedBy.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("VHH_SIGNATORY")) = False, ds.Tables(0).Rows(0)("VHH_SIGNATORY"), "")
                txtDesignation.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("VHH_SIGNATORY_DESIG")) = False, ds.Tables(0).Rows(0)("VHH_SIGNATORY_DESIG"), "")
                '   --- Initialize The Grid With The Data From The Detail Table
                'lstrSQL2 = "SELECT A.VHD_ID,Convert(VarChar,A.VHD_LINEID) as Id,A.VHD_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.VHD_NARRATION as Narration,A.VHD_RSS_ID as CLine,A.VHD_AMOUNT as Amount,A.VHD_CHQID as ChqBookId,isnull(CHB_LOTNO,0) as ChqBookLot,A.VHD_CHQNO as ChqNo,A.VHD_COL_ID as PettycashId," _
                '            & " REPLACE(CONVERT(VARCHAR(11), A.VHD_CHQDT, 106), ' ', '/') as  ChqDate,'' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd, A.VHD_bCheque as isCheque FROM VOUCHER_D A left outer JOIN CHQBOOK_M ON A.VHD_CHQID=CHQBOOK_M.CHB_ID INNER JOIN vw_OSA_ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID" _
                '            & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                'ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("dtDTL") = DataTables.CreateDataTable_BP()



                Dim dtDTL As DataTable = CostCenterFunctions.ViewVoucherDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
                               Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))
                If dtDTL.Rows.Count > 0 Then
                    For iIndex As Integer = 0 To dtDTL.Rows.Count - 1
                        Dim rDt As DataRow
                        IntVHDID = dtDTL.Rows(iIndex)("VHD_ID")
                        rDt = Session("dtDTL").NewRow
                        rDt("Id") = dtDTL.Rows(iIndex)("Id")
                        rDt("AccountId") = dtDTL.Rows(iIndex)("AccountId")
                        rDt("AccountName") = dtDTL.Rows(iIndex)("AccountName")
                        rDt("Narration") = dtDTL.Rows(iIndex)("Narration")
                        rDt("CLine") = dtDTL.Rows(iIndex)("CLine")
                        rDt("Amount") = dtDTL.Rows(iIndex)("Amount")
                        rDt("ChqBookId") = dtDTL.Rows(iIndex)("ChqBookId")
                        rDt("ChqBookLot") = dtDTL.Rows(iIndex)("ChqBookLot")
                        rDt("ChqNo") = dtDTL.Rows(iIndex)("ChqNo")
                        rDt("ChqDate") = dtDTL.Rows(iIndex)("ChqDate")
                        rDt("Status") = ""
                        rDt("GUID") = dtDTL.Rows(iIndex)("GUID")

                        rDt("isCheque") = dtDTL.Rows(iIndex)("isCheque")
                        rDt("PettycashName") = ""
                        If BSU_IsTAXEnabled Then
                            rDt("TaxCode") = dtDTL.Rows(iIndex)("TaxCode")
                        Else
                            rDt("TaxCode") = ""
                        End If


                        Dim PettyId As String = dtDTL.Rows(iIndex)("PettycashId")
                        If PettyId.Equals("0") Then
                            PettyId = ""
                        End If
                        rDt("PettycashId") = PettyId

                        If Not PettyId.Equals("") Then
                            StrSql = " SELECT vw_OSO_EMPLOYEE_M.EMP_FNAME as  EMP_FNAME" _
                                    & " FROM  PETTYEXP_H INNER JOIN vw_OSO_EMPLOYEE_M ON " _
                                    & " PETTYEXP_H.PCH_EMP_ID = vw_OSO_EMPLOYEE_M.EMP_ID  " _
                                    & " AND  PETTYEXP_H.PCH_BSU_ID = vw_OSO_EMPLOYEE_M.EMP_BSU_ID " _
                                    & " WHERE PCH_Fapproved = 1 AND PCH_BDELETED = 0 " _
                                    & " AND PCH_FYEAR = " & Session("F_YEAR") & " " _
                                    & " AND PCH_ID = " & PettyId & " "
                            ds3 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, StrSql)
                            If ds3.Tables(0).Rows.Count > 0 Then
                                rDt("PettycashName") = ds3.Tables(0).Rows(0)("EMP_FNAME")

                            End If

                        End If

                        If IntVHDID > 0 Then
                            StrSql = "SELECT VS.VSB_DTFROM as FromDate,VS.VSB_DTTO as ToDate,CU.CUT_ID as CostUnitID,CU.CUT_EXP_ACT_ID as ExpAcc,AM.ACT_NAME as ExpAccName FROM VOUCHER_D_SUB AS VS INNER JOIN COSTUNIT_M AS CU ON VS.VSB_CUT_ID=CU.CUT_ID" _
                                            & " INNER JOIN ACCOUNTS_M AS AM ON CU.CUT_EXP_ACT_ID=AM.ACT_ID WHERE VS.VSB_VHD_ID=" & IntVHDID & ""
                            ds3 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, StrSql)
                            If ds3.Tables(0).Rows.Count > 0 Then
                                rDt("FromDate") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("FromDate")) = False, ds3.Tables(0).Rows(0)("FromDate"), "")
                                rDt("ToDate") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ToDate")) = False, ds3.Tables(0).Rows(0)("ToDate"), "")
                                rDt("CostUnit") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("CostUnitID")) = False, ds3.Tables(0).Rows(0)("CostUnitID"), "")
                                rDt("ExpAcc") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ExpAcc")) = False, ds3.Tables(0).Rows(0)("ExpAcc"), "")
                                rDt("ExpAccName") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ExpAccName")) = False, ds3.Tables(0).Rows(0)("ExpAccName"), "")
                            Else
                                rDt("FromDate") = ""
                                rDt("ToDate") = ""
                                rDt("CostUnit") = ""
                                rDt("ExpAcc") = ""
                                rDt("ExpAccName") = ""
                            End If
                        End If

                        Session("dtDTL").Rows.Add(rDt)
                    Next
                End If

                lstrSQL2 = "SELECT MAx(VHD_LINEID) as Id,Max(VHD_CHQID) as CHQID FROM VOUCHER_D A " _
                           & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1
                hCheqBook.Value = ds2.Tables(0).Rows(0)("CHQID").ToString
                h_NextLine.Value = Session("gintGridLine")
                '   ----  Initalize the Cost Center Grid
                Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
         Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))

                Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
                Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))

                gvDTL.DataSource = Session("dtDTL")
                gvDTL.DataBind()
            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch ex As Exception
            lblError.Text = "Record Not Found !!! "
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Session("datamode") = "add" Or Session("datamode") = "edit" Then
            h_editorview.Value = ""
            unlock()
            Call Clear_Details()
            Session("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim Status As Integer
        Try

            '   --- CALL A GENERAL FUNCTION TO DELETE BY PASSING THE GUID AND TRAN TYPE ---
            Status = VoucherFunctions.DeleteVOUCHER(Session("BANKTRAN"), Session("sBsuid"), Session("Sub_ID"), Session("sUsr_name"), Session("Eid"))
            If Status <> 0 Then
                lblError.Text = (UtilityObj.getErrorMessage(Status))
                Exit Sub
            Else
                Status = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "delete", Page.User.Identity.Name.ToString, Me.Page)
                If Status <> 0 Then
                    Throw New ArgumentException("Could not complete your request")
                End If
                Call Clear_Details()
                lblError.Text = "Record Deleted Successfully"
            End If
        Catch myex As ArgumentException
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(myex.Message, Page.Title)
        Catch ex As Exception
            lblError.Text = "Record could not be Deleted"
            Errorlog(ex.Message)
        End Try

        Session("datamode") = "none"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
    End Sub


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", txtdocNo.Text, chkPrintChq.Checked, Session("HideCC"))
        If chkPrintChq.Checked Then
            If repSource.Equals(Nothing) Then
                lblError.Text = "Cheque Format not supported for printing"
                Exit Sub
            Else
                Session("ReportSource") = repSource
                Response.Redirect("accChqPrint.aspx?ChequePrint=BP", True)
            End If
        End If
        Session("ReportSource") = repSource
        '  Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        If Session("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
    End Sub


    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Sub set_chequeno_controls()
        If rbOthers.Checked = True Then
            txtrefChequeno.Enabled = True
            txtChqBook.Text = ""
            txtChqNo.Text = ""
        Else
            txtrefChequeno.Enabled = False
            txtrefChequeno.Text = ""
        End If
    End Sub


    Protected Sub rbCheque_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCheque.CheckedChanged
        set_chequeno_controls()
    End Sub


    Protected Sub rbOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOthers.CheckedChanged
        set_chequeno_controls()
    End Sub


    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        If Session("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
    End Sub


    Protected Sub btnSettle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSettle.Click
        Response.Redirect("accposOnlineSettlement.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&eid=" & Request.QueryString("Eid"))
    End Sub


    Protected Sub chkAdvance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdvance.CheckedChanged
        set_AdvanceControls()
    End Sub


    Sub set_AdvanceControls()
        If chkAdvance.Checked = True Then
            txtLineAmount.Attributes.Remove("readonly")
            lbSettle.Visible = False
            Session("dtSettle").Rows.Clear()
        Else
            txtLineAmount.Text = get_totalforaline()
            If txtLineAmount.Text = "0" Then
                txtLineAmount.Text = ""
            End If
            txtLineAmount.Attributes.Add("readonly", "readonly")

            lbSettle.Visible = True
        End If
    End Sub


    Protected Sub imgParty_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgParty.Click
        chk_DetailsAccount()
        Dim acttype As String = AccountFunctions.check_accounttype(Detail_ACT_ID.Text, Session("sBsuid"))
        Dim strPayTerm As String = AccountFunctions.GetPaymentTerm(Detail_ACT_ID.Text)
        lblPaymentTerm.Text = ""
        If strPayTerm <> "" Then
            lblPaymentTerm.Text = "Payment Term : " & strPayTerm
            trPayTerm.Visible = True
        Else
            trPayTerm.Visible = False
        End If
        If acttype = "S" Then
            set_AdvanceControls()
        Else
            txtLineAmount.Attributes.Remove("readonly")
            lbSettle.Visible = False
        End If

    End Sub


    Private Function get_totalforaline() As Decimal
        Try
            Dim dTotal As Decimal = 0
            For i As Integer = 0 To Session("dtSettle").rows.count - 1
                If Session("dtSettle").rows(i)("Accountid") = Detail_ACT_ID.Text And _
                 Session("dtSettle").rows(i)("Id") = h_NextLine.Value Then
                    dTotal = dTotal + Convert.ToDecimal(Session("dtSettle").rows(i)("Amount"))
                End If
            Next
            Return dTotal
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function


    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
        FillLotNo()
    End Sub


    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            txtProvCode.Focus()
        End If
    End Sub


    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub


    Protected Sub txtProvCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProvCode.TextChanged
        txtProvDescr.Text = AccountFunctions.Validate_Account(txtProvCode.Text, Session("sbsuid"), "PREPDAC")
        If txtProvDescr.Text = "" Then
            lblError.Text = "Invalid Provision selected"
            txtProvCode.Focus()
        Else
            lblError.Text = ""
            txtReceivedby.Focus()
        End If
    End Sub

    Sub Set_SettleControls()
        chk_DetailsAccount()
        Dim acttype As String = AccountFunctions.check_accounttype(Detail_ACT_ID.Text, Session("sBsuid"))
        If acttype = "S" Then
            set_AdvanceControls()
        Else
            txtLineAmount.Attributes.Remove("readonly")
            lbSettle.Visible = False
        End If
        txtFrom.Text = ""
        txtTo.Text = ""
        txtExpCode.Text = ""
        txtPreAcc.Text = ""
        txtCostUnit.Text = ""
        If Detail_ACT_ID.Text <> "" Then
            If GetAccountDetails(Detail_ACT_ID.Text) = True Then
                lnkCostUnit.Enabled = True
                lnkCostUnit.Visible = True
                txtFrom.Text = txtdocDate.Text

            Else
                lnkCostUnit.Enabled = False
                lnkCostUnit.Visible = False
            End If
        End If
    End Sub

    Protected Sub Detail_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail_ACT_ID.TextChanged
        Set_SettleControls()
        'ClearRadGridandCombo()
    End Sub


    Sub chk_DetailsAccount()
        txtPartyDescr.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "NOTCC")
        If txtPartyDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            Detail_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtLineNarrn.Focus()
        End If
    End Sub


    Protected Sub imgProv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProv.Click
        txtProvDescr.Text = AccountFunctions.Validate_Account(txtProvCode.Text, Session("sbsuid"), "PREPDAC")
        If txtProvDescr.Text = "" Then
            lblError.Text = "Invalid Provision selected"
            txtReceivedby.Focus()
        Else
            lblError.Text = ""
            txtNarrn.Focus()
        End If
    End Sub


    Protected Sub lnkCostUnit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Function GetAccountDetails(ByVal ACTID As String) As Boolean
        Dim strsql As String
        Dim ds As DataSet
        Dim blnStatus As Boolean
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        blnStatus = False
        strsql = "SELECT * FROM ACCOUNTS_M WHERE act_id='" & ACTID & "'"
        ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("act_bprepayment") = True Then

                blnStatus = True
            Else

                blnStatus = False
            End If
        End If
        Return blnStatus
    End Function


    Private Function GetVoucherID(ByVal LineID As Integer, ByVal SUBID As String, ByVal BUSID As String, ByVal DocType As String, ByVal DocNo As String, ByVal ACTID As String) As Integer
        Dim str_query As String
        Dim dsVcher As DataSet
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim IntVoucherID As Integer
        IntVoucherID = 0
        str_query = "SELECT VHD_ID FROM VOUCHER_ID WHERE VHD_LINEID=" & LineID & " AND VHD_SUB_ID='" & SUBID & "' AND VHD_BSU_ID='" & BUSID & "' AND VHD_DOCTYPE='" & DocType & "' AND VHD_DOCNO='" & DocNo & "' AND VHD_ACT_ID='" & ACTID & "'"
        dsVcher = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, str_query)
        If dsVcher.Tables(0).Rows.Count > 0 Then
            IntVoucherID = dsVcher.Tables(0).Rows(0)("VHD_ID")
        End If

        Return IntVoucherID
    End Function

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        If Page.IsPostBack = False Then
            If (Session("datamode") = "add") Then
                FillLotNo()
            End If
        End If
    End Sub

    Protected Sub txtTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strfDate As String = txtTo.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid ToDate"
            Exit Sub
        Else
            txtTo.Text = strfDate
        End If
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strfDate As String = txtFrom.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFrom.Text = strfDate
        End If
    End Sub

    Private Sub GetSignature()
        Dim strSql As String
        Dim dsSign As DataSet
        Dim strConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        strSql = "SELECT BSU_SIGNATORY,BSU_SIGNATORY_DESIG FROM VW_OSO_BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'"
        dsSign = SqlHelper.ExecuteDataset(strConn, CommandType.Text, strSql)
        If dsSign.Tables(0).Rows.Count > 0 Then
            txtSignedBy.Text = IIf(IsDBNull(dsSign.Tables(0).Rows(0)("BSU_SIGNATORY")) = False, dsSign.Tables(0).Rows(0)("BSU_SIGNATORY"), "")
            txtDesignation.Text = IIf(IsDBNull(dsSign.Tables(0).Rows(0)("BSU_SIGNATORY_DESIG")) = False, dsSign.Tables(0).Rows(0)("BSU_SIGNATORY_DESIG"), "")
        Else
            txtSignedBy.Text = ""
            txtDesignation.Text = ""
        End If
    End Sub

    'Sub SetCostcenterControls()
    '    ddlCostCenter.DataBind()
    '    If Not ddlCostCenter.Items.FindByText("DEPARTMENT") Is Nothing Then
    '        ddlCostCenter.ClearSelection()
    '        ddlCostCenter.Items.FindByText("DEPARTMENT").Selected = True
    '    End If
    '    ShowUploadOption()
    'End Sub

    Private Const ItemsPerRequest As Integer = 10
    Protected Sub RadComboBox1_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
        'Dim data As DataTable = CostCenterFunctions.GetCostCenterData(e.Text, ddlCostCenter.SelectedItem.Value, Session("sBsuid"), _
        'txtdocDate.Text, Session("CostOTH"))

        'Dim itemOffset As Integer = e.NumberOfItems
        'Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, data.Rows.Count)
        'e.EndOfItems = endOffset = data.Rows.Count

        'For i As Integer = itemOffset To endOffset
        '    If data.Rows.Count > i Then
        '        o.Items.Add(New RadComboBoxItem(data.Rows(i)("Name").ToString() & " - " & data.Rows(i)("Column1").ToString(), data.Rows(i)("ID").ToString()))
        '    End If
        'Next

        'e.Message = GetStatusMessage(endOffset, data.Rows.Count)
    End Sub

    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub

    'Protected Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
    '    ClearRadGridandCombo()
    '    ShowUploadOption()
    'End Sub

    Sub ShowUploadOption()
        'If ddlCostCenter.SelectedItem.Text.ToUpper = "EMPLOYEES" Then
        '    tr_UploadEmplyeeCostCenter.Visible = True
        'Else
        '    tr_UploadEmplyeeCostCenter.Visible = False
        'End If
    End Sub



    Protected Sub lbUploadEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUploadEmployee.Click
        If Not fuEmployeeData.HasFile Then
            lblError.Text = "Please select the file...!"
            Exit Sub
        End If
        If Not (fuEmployeeData.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Private Sub UpLoadDBF()
        RecreateSsssionDataSource()
        CostCenterFunctions.UploadAndPopulateEmployeeDataFromExcel(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), Session("sBSuid"), _
                     CostCenterIDRadComboBoxMain.SelectedValue, CostCenterIDRadComboBoxMain.Text, txtdocDate.Text, fuEmployeeData, Session("sUsr_name"))
        usrCostCenter1.BindCostCenter()
    End Sub

    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()

        CostCenterIDRadComboBoxMain.Text = ""
        CostCenterIDRadComboBoxMain.ClearSelection()
        CostCenterIDRadComboBoxMain.DataBind()
    End Sub




End Class
