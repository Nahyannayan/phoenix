<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accChequeRegisterViewBundle.aspx.vb" Inherits="Accounts_accChequeRegisterViewBundle" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem, checkbox_checked_status;
            var chk_name = master_box.id.replace("Selectall", "");
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && (curr_elem.id.indexOf(chk_name) > 0))
                    curr_elem.checked = master_box.checked;
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Send Cheque(s) for Signature
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>

                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True"
                                            DataSourceID="sdsBusinessunit" DataTextField="BSU_NAME" DataValueField="BSU_ID"
                                            OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                            SelectCommand="SELECT BSU_ID, BSU_NAME FROM dbo.fn_GetBusinessUnits(@sUsr_name) AS fn_GetBusinessUnits_1 WHERE (ISNULL(BSU_bShow, 1) = 1) UNION SELECT 'ALL' AS BSU_ID, '' AS BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="sUsr_name" SessionField="sUsr_name" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select status</span></td>
                                    <td align="left" width="30%">

                                        <asp:DropDownList ID="ddChequeStatus" runat="server" AutoPostBack="True"
                                            DataSourceID="sdsChequeStatus" DataTextField="CST_DESCR"
                                            DataValueField="CST_ID">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsChequeStatus" runat="server"
                                            ConnectionString="<%$ ConnectionStrings:MainDB %>" SelectCommand="SELECT CST_ID, CST_DESCR FROM CHEQUESTATUS UNION SELECT 0 AS CST_ID, 'All' AS CST_DESCR order by CST_ID"></asp:SqlDataSource>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                            EmptyDataText="No Cash Receipt Vouchers for posting" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="Unit"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Doc. No.">
                                                    <HeaderTemplate>
                                                        Doc. No.<br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Chq. No.">
                                                    <HeaderTemplate>
                                                        Chq. No<br />
                                                        <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnChqNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("VDC_VHD_CHQNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="VDC_STATUS" HeaderText="Status" ReadOnly="True"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Document Date">
                                                    <HeaderTemplate>
                                                        Bundle No<br />
                                                        <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocDateSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("VDC_BundleNo") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Narration">
                                                    <HeaderTemplate>
                                                        Narration<br />
                                                        <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnNarrationSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("VDC_REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <HeaderTemplate>
                                                        Amount<br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAmountSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("VHD_AMOUNT", "{0:0.00}") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="VDC_APPROVEDATE" HeaderText="Doc Date"
                                                    DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>


