﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Script.Serialization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports Newtonsoft.Json
Imports System.IO
Imports System.Data.OleDb
Imports GemBox.Spreadsheet
Imports System.Web.Configuration

Partial Class Accounts_ImportTempletExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim smScriptManager As New AjaxControlToolkit.ToolkitScriptManager
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_Create_table)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_save)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_get_Upload_history)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtn_Delete_History)
        Try
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            End If
            If Page.IsPostBack = False Then
                BindBSU()
                BindTemplate()
                If chb_NewTable.Checked Then
                    DIV_Create_Table_holder.Visible = True
                    btn_Create_table.Visible = True
                    btn_save.Visible = False
                    DIV_Import_Files_holder.Visible = False
                Else
                    DIV_Create_Table_holder.Visible = False
                    btn_Create_table.Visible = False
                    btn_save.Visible = True
                    DIV_Import_Files_holder.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub Accounts_ImportTempletExcel_Init(sender As Object, e As EventArgs) Handles Me.Init
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_save)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(gvExcelImport)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtn_Delete_History)

    End Sub

    Sub BindBSU()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlParameter("@USER", Session("sUsr_name"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_ASSIGNED_BSU", param)
            ddlBsu.DataSource = ds
            ddlBsu.DataTextField = "BSU_NAME"
            ddlBsu.DataValueField = "BSU_ID"
            ddlBsu.DataBind()
            ddlBsu.Items.Insert(0, New ListItem("-Select-", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub BindTemplate()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GET_TEMPLATE_FILES]")
            ddlTemplate.DataSource = ds
            ddlTemplate.DataTextField = "FTM_DESCR"
            ddlTemplate.DataValueField = "FTM_ID"
            ddlTemplate.DataBind()
            ddlTemplate.Items.Insert(0, New ListItem("-Select-", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btn_save_Click(sender As Object, e As EventArgs)
        ImportAdjustmentSheet()
    End Sub




    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        Dim dtXL As New DataTable
        dtXL = CType((Session("dtXL")), DataTable)
        gvExcelImport.DataSource = dtXL
        gvExcelImport.DataBind()
    End Sub

    Private Sub ImportAdjustmentSheet()
        Dim errString As String = ""
        Dim file_title = ""
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload_File.HasFile) Then
                file_title = Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload_File.FileName.ToString()
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload_File.FileName).ToString().ToLower()
                FileName = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile") & "\OnlineExcel\" & file_title
                If strFileType = ".xls" Or strFileType = ".xlsx" Or strFileType = ".csv" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload_File.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar2.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
                If File.Exists(FileName) Then
                    SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                    'Dim ef As New GemBox.Spreadsheet.ExcelFile
                    Dim workbook = ExcelFile.Load(FileName)
                    ' Select active worksheet.
                    Dim worksheet = workbook.Worksheets.ActiveWorksheet
                    Dim dtXL As New DataTable
                    If worksheet.Rows.Count > 1 Then
                        dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                                {
                                 .ColumnHeaders = True,
                                 .StartRow = 0,
                                 .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                                 .NumberOfRows = worksheet.Rows.Count,
                                 .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                                })
                    End If
                    ' File delete first 
                    File.Delete(FileName)
                    If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                        Session("dtXL") = dtXL
                        'gvExcelImport.DataSource = CType((Session("dtXL")), DataTable)
                        'gvExcelImport.DataBind()
                        Dim ResultLog = "0"
                        ResultLog = ValidateAndSave_SqlBulk(dtXL, file_title, ddlTemplate.SelectedValue)
                        If ResultLog = "1" Then
                            btn_save.Visible = False
                            usrMessageBar2.ShowNotification("Data has been upload it Successfully", UserControls_usrMessageBar.WarningType.Success)
                            gvExcelImport.DataSource = CType((Session("dtXL")), DataTable)
                            gvExcelImport.DataBind()
                            DIV_History_View.Visible = False
                            DIV_File_View.Visible = True
                        Else
                            usrMessageBar2.ShowNotification(ResultLog, UserControls_usrMessageBar.WarningType.Danger)
                        End If

                    Else
                        'Me.lblError.Text += "Empty Document!!"
                        usrMessageBar2.ShowNotification("Empty Document!!", UserControls_usrMessageBar.WarningType.Danger)
                    End If

                End If
            Else
                'Me.lblError.Text += "File not Found!!"
                usrMessageBar2.ShowNotification("File not Found!!", UserControls_usrMessageBar.WarningType.Danger)
                Me.FileUpload_File.Focus()
            End If


        Catch ex As Exception
            errString = errString + Err.Description
            usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Function ValidateAndSave_SqlBulk(ByRef DT As DataTable, ByVal file_title As String, ByVal Template_ID As Integer) As String
        Dim ResultLog As String = ""
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim objConn_DMN As New SqlConnection(ConfigurationManager.ConnectionStrings("DATAMIGRATION_NEWConnectionString").ConnectionString)
        Try
            Dim TableName As String = ""
            Dim Columns_Number As String = ""
            Dim reader As SqlDataReader
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@FTM_ID", Template_ID)
            reader = SqlHelper.ExecuteReader(objConn, CommandType.StoredProcedure, "Get_FileTemplateFolder_ByID", param)
            While reader.Read
                TableName = Convert.ToString(reader("FTM_Table_Name"))
                Columns_Number = Convert.ToString(reader("FTM_Columns_Number"))
            End While
            reader.Close()
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If String.IsNullOrEmpty(TableName) = False Then
                Dim DT_Count As Integer = DT.Columns.Count + 2
                If DT_Count = Columns_Number Then
                    Dim FTF_ID = Get_FTF_File(file_title, Template_ID)
                    If FTF_ID <> 0 Then
                        Dim Col As DataColumn = New DataColumn() With {.ColumnName = "FTF_ID",
                                            .DataType = GetType(String),
                                            .DefaultValue = FTF_ID}
                        DT.Columns.Add(Col)
                        Col.SetOrdinal(0)
                        DT.AcceptChanges()
                        If objConn_DMN.State = ConnectionState.Open Then
                            objConn_DMN.Close()
                        End If
                        objConn_DMN.Open()
                        Dim Success As Boolean = True
                        Dim bulk As SqlBulkCopy = New SqlBulkCopy(objConn_DMN)
                        For i As Integer = 0 To DT.Columns.Count - 1
                            bulk.ColumnMappings.Add(DT.Columns(i).ColumnName, (i + 1))
                        Next
                        bulk.DestinationTableName = TableName
                        bulk.WriteToServer(DT)
                        If objConn_DMN.State = ConnectionState.Open Then
                            objConn_DMN.Close()
                        End If
                        DT.Columns.Remove("FTF_ID")
                        DT.AcceptChanges()
                        ResultLog = "1"
                    Else
                        ResultLog = "Error Template Folder did not create!!"
                    End If
                Else
                    ResultLog = "Columns Numbers is not matching between table and file"
                End If
            Else
                ResultLog = "Please check template table name"
            End If
            Return ResultLog
        Catch ex As Exception

            ResultLog = "Data has not been upload it Successfully"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If objConn_DMN.State = ConnectionState.Open Then
                objConn_DMN.Close()
            End If
            Return ResultLog
        End Try

    End Function
    Private Sub ValidateAndSave_single(ByRef DT As DataTable)
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            For Each item As DataRow In DT.Rows
                Try
                    Dim pParms(9) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@FTI_BSU_ID", ddlBsu.SelectedValue)
                    pParms(1) = New SqlClient.SqlParameter("@FTI_FTM_ID", ddlTemplate.SelectedValue)
                    pParms(2) = New SqlClient.SqlParameter("@FTI_UserType", item(0))
                    pParms(3) = New SqlClient.SqlParameter("@FTI_Name", item(1))
                    pParms(4) = New SqlClient.SqlParameter("@FTI_Email", item(2))
                    pParms(5) = New SqlClient.SqlParameter("@FTI_Account_Name", item(3))
                    pParms(6) = New SqlClient.SqlParameter("@FTI_CreateBy", Session("sUsr_name"))
                    pParms(7) = New SqlClient.SqlParameter("@FTI_ModifyBy", "")
                    pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(8).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[dbo].[Save_FileTemplateImport]", pParms)
                    Dim ReturnFlag As Integer = pParms(8).Value
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            Next
            stTrans.Commit()


        Catch ex As Exception
            usrMessageBar2.ShowNotification("Data has not been upload it Successfully", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Function Get_FTF_File(ByVal filename As String, ByVal Template_ID As String) As Integer
        Dim FTF_ID As Integer = 0
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FTF_BSU_ID", ddlBsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@FTF_FTM_ID", Template_ID)
            pParms(2) = New SqlClient.SqlParameter("@FTF_FileTitle", filename)
            pParms(3) = New SqlClient.SqlParameter("@FTF_CreateBy", Session("sUsr_name"))
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[dbo].[Save_FileTemplateFolder]", pParms)
            FTF_ID = pParms(4).Value
            stTrans.Commit()


        Catch ex As Exception
            usrMessageBar2.ShowNotification("Data has not been upload it Successfully", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Return FTF_ID
    End Function
    Function Check_If_Table_EXISTS(ByVal Table_Name As String) As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Table_Name", Table_Name)
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[Check_If_Table_EXISTS]", pParms)
        End Using
        Return Flag
    End Function
    Protected Sub btn_Create_table_Click(sender As Object, e As EventArgs)
        Dim Table_Name As String = System.Text.RegularExpressions.Regex.Replace(txt_table_Name.Text, "[^a-zA-Z0-9]", "_")
        If Check_If_Table_EXISTS(Table_Name) = False Then
            Dim errString As String = ""
            Dim file_title = ""
            Dim ResultLog = "0"
            Try
                lblError.Text = ""
                Dim Table_Count As Integer = 0
                Dim FileName As String = ""
                If (FileUpload_File.HasFile) Then
                    file_title = Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload_File.FileName.ToString()
                    Dim strFileType As String = System.IO.Path.GetExtension(FileUpload_File.FileName).ToString().ToLower()
                    FileName = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile") & "\OnlineExcel\" & file_title
                    If strFileType = ".xls" Or strFileType = ".xlsx" Or strFileType = ".csv" Then
                        If File.Exists(FileName) Then
                            File.Delete(FileName)
                        End If
                        Me.FileUpload_File.SaveAs(FileName)
                    Else
                        'Me.lblError.Text = "Only Excel files are allowed"
                        usrMessageBar2.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Try
                    End If
                    If File.Exists(FileName) Then
                        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                        'Dim ef As New GemBox.Spreadsheet.ExcelFile
                        Dim workbook = ExcelFile.Load(FileName)
                        ' Select active worksheet.
                        Dim worksheet = workbook.Worksheets.ActiveWorksheet
                        Dim dtXL As New DataTable
                        If worksheet.Rows.Count > 1 Then
                            dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                                {
                                 .ColumnHeaders = True,
                                 .StartRow = 0,
                                 .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                                 .NumberOfRows = worksheet.Rows.Count,
                                 .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                                })
                        End If
                        ' File delete first 
                        File.Delete(FileName)
                        If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                            Session("dtXL") = dtXL
                            'gvExcelImport.DataSource = CType((Session("dtXL")), DataTable)
                            'gvExcelImport.DataBind()

                            Table_Count = dtXL.Columns.Count
                            'create table'
                            Dim cmd As New SqlCommand
                            Dim objConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DATAMIGRATION_NEWConnectionString").ConnectionString)
                            If objConn.State = ConnectionState.Open Then
                                objConn.Close()
                            End If
                            objConn.Open()
                            Dim stTrans As SqlTransaction = objConn.BeginTransaction

                            Try
                                Dim SQL_Quary = "CREATE TABLE [dbo].[" + Table_Name + "]([ID] [int] IDENTITY(1,1) NOT NULL,[FTF_ID] [int] NOT NULL,"
                                For i As Integer = 0 To dtXL.Columns.Count - 1
                                    SQL_Quary += System.Text.RegularExpressions.Regex.Replace(dtXL.Columns(i).ColumnName, "[^a-zA-Z0-9]", "_") & " nvarchar(Max) NULL,"
                                Next
                                SQL_Quary += " CONSTRAINT [PK_" + Table_Name + "] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
                                SQL_Quary += ")ON [PRIMARY]"
                                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, SQL_Quary)
                                stTrans.Commit()
                                ResultLog = "1"
                            Catch ex As Exception
                                stTrans.Rollback()
                                ResultLog = ex.Message + "<br> table has been not Created Successfully"
                                usrMessageBar2.ShowNotification(ResultLog, UserControls_usrMessageBar.WarningType.Danger)
                                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

                            Finally
                                If objConn.State = ConnectionState.Open Then
                                    objConn.Close()
                                End If
                            End Try
                            Dim FTM_ID As Integer = 0
                            If ResultLog = "1" Then
                                ResultLog = "0"
                                FTM_ID = Get_FileTemplate_M_ID(txt_Template_name.Text, strFileType.Replace(".", ""), Table_Name, (Table_Count + 2))
                                If FTM_ID <> 0 Then
                                    ResultLog = ValidateAndSave_SqlBulk(dtXL, file_title, FTM_ID)
                                    If ResultLog = "1" Then
                                        btn_save.Visible = False
                                        btn_Create_table.Visible = False
                                        usrMessageBar2.ShowNotification("Data has been upload it Successfully", UserControls_usrMessageBar.WarningType.Success)
                                        gvExcelImport.DataSource = CType((Session("dtXL")), DataTable)
                                        gvExcelImport.DataBind()
                                        DIV_History_View.Visible = False
                                        DIV_File_View.Visible = True
                                    Else
                                        usrMessageBar2.ShowNotification(ResultLog, UserControls_usrMessageBar.WarningType.Danger)
                                    End If
                                End If
                            End If


                        Else
                            'Me.lblError.Text += "Empty Document!!"
                            usrMessageBar2.ShowNotification("Empty Document!!", UserControls_usrMessageBar.WarningType.Danger)
                        End If

                    End If
                Else
                    'Me.lblError.Text += "File not Found!!"
                    usrMessageBar2.ShowNotification("File not Found!!", UserControls_usrMessageBar.WarningType.Danger)
                    Me.FileUpload_File.Focus()
                End If


            Catch ex As Exception
                errString = errString + Err.Description
                usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Else
            usrMessageBar2.ShowNotification("Table is already Exist", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Protected Sub chb_NewTable_CheckedChanged(sender As Object, e As EventArgs)
        If chb_NewTable.Checked Then
            DIV_Create_Table_holder.Visible = True
            btn_Create_table.Visible = True
            btn_save.Visible = False
            DIV_Import_Files_holder.Visible = False
        Else
            DIV_Create_Table_holder.Visible = False
            btn_Create_table.Visible = False
            btn_save.Visible = True
            DIV_Import_Files_holder.Visible = True
        End If
    End Sub
    Function Get_FileTemplate_M_ID(ByVal FTM_DESCR As String, ByVal FTM_FileType As String, ByVal FTM_Table_Name As String, ByVal FTM_Columns_Number As String) As Integer
        Dim FTM_ID As Integer = 0
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FTM_DESCR", FTM_DESCR)
            pParms(1) = New SqlClient.SqlParameter("@FTM_FileType", FTM_FileType)
            pParms(2) = New SqlClient.SqlParameter("@FTM_Table_Name", FTM_Table_Name)
            pParms(3) = New SqlClient.SqlParameter("@FTM_Columns_Number", FTM_Columns_Number)
            pParms(4) = New SqlClient.SqlParameter("@Create_By", Session("sUsr_name"))
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[dbo].[Save_FileTemplate_M]", pParms)
            FTM_ID = pParms(5).Value
            stTrans.Commit()
        Catch ex As Exception
            usrMessageBar2.ShowNotification("Data has not been upload it Successfully", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Return FTM_ID
    End Function
    Protected Sub btn_get_Upload_history_Click(sender As Object, e As EventArgs)
        BindData()
    End Sub
    Protected Sub GV_Upload_history_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Me.GV_Upload_history.PageIndex = e.NewPageIndex
        BindData()
    End Sub
    Protected Sub lbtn_Delete_History_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim chk As CheckBox
            Dim Table_Name As String = String.Empty
            Dim FTF_ID As String = String.Empty

            For Each rowItem As GridViewRow In GV_Upload_history.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect1"), CheckBox)

                Table_Name = DirectCast(rowItem.FindControl("lblTable_Name"), Label).Text
                FTF_ID = DirectCast(rowItem.FindControl("lblFTF_ID"), Label).Text
                If chk.Checked = True Then
                    Dim pParms_Folder(5) As SqlClient.SqlParameter
                    pParms_Folder(0) = New SqlClient.SqlParameter("@Table_Name", Table_Name)
                    pParms_Folder(1) = New SqlClient.SqlParameter("@FTF_ID", FTF_ID)
                    pParms_Folder(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms_Folder(2).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FileTemplate_Upload_history", pParms_Folder)
                    Dim ReturnFlag As Integer = pParms_Folder(2).Value
                End If
            Next
            stTrans.Commit()
            usrMessageBar2.ShowNotification("Row has been Deleted successfully", UserControls_usrMessageBar.WarningType.Success)
            BindData()
        Catch ex As Exception
            stTrans.Rollback()
            usrMessageBar2.ShowNotification("Row has been not Deleted successfully  Please try again  ", UserControls_usrMessageBar.WarningType.Danger)
        Finally
            objConn.Close()
        End Try

    End Sub
    Private Sub BindData()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", ddlBsu.SelectedValue)
            PARAM(1) = New SqlParameter("@FTM_ID", ddlTemplate.SelectedValue)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_FileTemplate_Upload_history", PARAM)
            GV_Upload_history.DataSource = ds
            GV_Upload_history.DataBind()
            DIV_History_View.Visible = True
            DIV_File_View.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
