Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Accounts_AccTreasuryTransfer
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        txtDAmount.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtDAmount.Attributes.Add("onkeydown", " return blockCtrl()")
        txtDAmount.Attributes.Add("onkeyup", " return toWordsUSA('" + txtDAmount.ClientID + "','" + labWords.ClientID + "')")
        txtDAmount.Attributes.Add("onfocusout", " return roundNumber('" + txtDAmount.ClientID + "','3')")
        txtExchangerateF.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtExchangerateT.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtExchangerateF2.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtExchangerateT2.Attributes.Add("onkeypress", " return Numeric_Only()")
        btnSave.Attributes.Add("onclick", " return getWords()")
        If Page.IsPostBack = False Then
            filldrp()
            doClear()
            'If Request.QueryString("viewid") = "" Then
            '    drpBusinessunitF.SelectedValue = Session("sBsuid").ToString()
            '    fillOtherdrp(drpBusinessunitF.SelectedValue.ToString(), drpBankF)
            '    drpBusinessunitF.Enabled = False
            'End If
            'drpBusinessunitF.Enabled = False
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            initialize_components()
            '''''check menu rights
            'If Request.QueryString("editerror") <> "" Then
            '    lblError.Text = "Record already posted/Locked"
            'End If
            'txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
            'txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150077"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A150077" And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("MainMnu_code").Equals("A200351") Then
                    btnAdd.Visible = False
                    btnDelete.Visible = False
                    btnEdit.Visible = False
                    btnPrint.Visible = False

                End If

            End If
            If Request.QueryString("viewid") <> "" Then

                getdata(Request.QueryString("viewid"))

            End If


            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = getErrorMessage("Invalid Editid")
                Exit Sub
            End If

            UtilityObj.beforeLoopingControls(Me.Page)
            getnextdocid()
        End If

    End Sub
    Public Sub getdata(ByVal selectId As String)

        Try
            Dim Query As String
            selectId = selectId.Replace(" ", "+")
            selectId = Encr_decrData.Decrypt(selectId)

            Query = " SELECT * FROM INTERUNIT_TRANSFER WHERE ITF_FYEAR = '" & Session("F_YEAR") & "' AND IRF_ID = " & selectId

            Dim _table As DataTable

            _table = MainObj.ListRecords(Query, "mainDB")
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Invalid Selection ..")

            End If
            h_Editid.Value = _table.Rows(0)("IRF_ID").ToString()
            txtDAmount.Text = _table.Rows(0)("ITF_AMOUNT").ToString()
            txtHNarration.Text = _table.Rows(0)("ITF_NARRATION").ToString()
            txtExchangerateF.Text = _table.Rows(0)("ITF_DREXGRATE1").ToString()
            txtExchangerateT.Text = _table.Rows(0)("ITF_CREXGRATE1").ToString()
            txtExchangerateF2.Text = _table.Rows(0)("ITF_DREXGRATE2").ToString()
            txtExchangerateT2.Text = _table.Rows(0)("ITF_CREXGRATE2").ToString()
            txtDocumentno.Text = _table.Rows(0)("ITF_DOCNO").ToString()

            drpBusinessunitF.SelectedValue = _table.Rows(0)("ITF_CRBSU_ID").ToString()
            drpBusinessunitT.SelectedValue = _table.Rows(0)("ITF_DRBSU_ID").ToString()
            fillOtherdrp(drpBusinessunitF.SelectedValue.ToString(), drpBankF)
            fillOtherdrp(drpBusinessunitT.SelectedValue.ToString(), drpBankT)
            
            drpBankF.SelectedValue = _table.Rows(0)("ITF_CR_BANK_ACT_ID").ToString()
            drpBankT.SelectedValue = _table.Rows(0)("ITF_DR_BANK_ACT_ID").ToString()

            getAccStatus(RptF, drpBankF.SelectedValue.ToString(), drpBusinessunitF.SelectedValue.ToString())
            getAccStatus(RptT, drpBankT.SelectedValue.ToString(), drpBusinessunitT.SelectedValue.ToString())


            If _table.Rows(0)("ITF_bPOSTEDDRBSU").Equals("1") Then
                postedYN.Value = "Y"
            ElseIf _table.Rows(0)("ITF_bPOSTEDCRBSU").Equals("1") Then
                postedYN.Value = "Y"
            Else
                postedYN.Value = "N"
            End If
            Dim ddate As DateTime
            ddate = Convert.ToDateTime(_table.Rows(0)("ITF_DATE").ToString())

            txtHDocdate.Text = ddate.ToString("dd/MMM/yyy")
            labWords.Text = MainObj.SpellNumber(txtDAmount.Text).Replace("AED", drpCurrency.SelectedValue)

            lockUnlok(False)

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)

        drpObj.DataSource = MainObj.ListRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
        

    End Sub
    Public Sub lockUnlok(ByVal locked As Boolean)
        drpBankF.Enabled = locked
        drpBankT.Enabled = locked
        'drpBusinessunitF.Enabled = locked
        drpBusinessunitT.Enabled = locked
        drpCurrency.Enabled = locked
        txtDAmount.Enabled = locked
        txtExchangerateF.Enabled = locked
        txtExchangerateT.Enabled = locked
        txtExchangerateF2.Enabled = locked
        txtExchangerateT2.Enabled = locked
        txtHDocdate.Enabled = locked
        txtHNarration.Enabled = locked
    End Sub

    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "EXEC [GetBsuForTrasuryTransfer] '" & Session("sBsuid") & "', '" & Session("sUsr_name") & "'"
            fillDropdown(drpBusinessunitF, Query, "BSU_NAME", "BSU_ID", "OASISConnectionString", True)
            Query = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_bSHOW = 1 ORDER BY BSU_NAME "
            fillDropdown(drpBusinessunitT, Query, "BSU_NAME", "BSU_ID", "OASISConnectionString", True)
            Query = "SELECT CURRENCY_M.CUR_ID, CURRENCY_M.CUR_ID FROM CURRENCY_M INNER JOIN EXGRATE_S ON CURRENCY_M.CUR_ID = EXGRATE_S.EXG_CUR_ID WHERE EXG_BSU_ID = '" & Session("sBsuid").ToString() & "'"
            fillDropdown(drpCurrency, Query, "CUR_ID", "CUR_ID", "mainDB", False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
        ViewState("str_timestamp") = New Byte()
        ViewState("idJournal") = 0
        h_NextLine.Value = ViewState("idJournal")
       
    End Sub


    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        ' bind_Currency()
        getnextdocid()
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            h_editorview.Value = ""
            If ViewState("datamode") = "edit" Then

            End If
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))

            
        End If
    End Sub

    
    Private Function CheckSave() As Boolean

        labWords.Text = hidwords.Value
        Dim bankBalF As Double = Convert.ToDouble(hidBankFamt.Value.ToString())
        If (drpBusinessunitF.SelectedValue.Equals("0")) Then
            lblError.Text = "Please select Payment Business Unit..!"
            Return False

        ElseIf (drpBusinessunitT.SelectedValue.Equals("0")) Then
            lblError.Text = "Please select Receivable Business Unit..!"
            Return False
        ElseIf IsDate(txtHDocdate.Text.ToString()).Equals(False) Then
            lblError.Text = "Invalid date formate..'DD/MMM/YYYY'..!"
            Return False
        ElseIf (drpBankF.SelectedValue.Equals(drpBankT.SelectedValue)) Then
            lblError.Text = "Both banks can�t be same...!"
            Return False
        ElseIf (drpBankF.SelectedValue.Equals("0")) Then
            lblError.Text = "Please select Payment Bank..!"
            Return False

        ElseIf (drpBankT.SelectedValue.Equals("0")) Then
            lblError.Text = "Please select Receivable Bank..!"
            Return False

        ElseIf (txtDAmount.Text.Equals("") Or txtDAmount.Text.Equals("0")) Then
            lblError.Text = "Please Enter Transfer Amount..!"
            Return False
        ElseIf (IsNumeric(txtDAmount.Text).Equals(False)) Then
            lblError.Text = "Please Enter Numeric Value in Amount!"
            txtDAmount.Text = ""
            txtDAmount.Focus()
            Return False
        ElseIf (txtHNarration.Text.Equals("")) Then
            lblError.Text = "Please Enter Narration..!"
            Return False
        ElseIf (txtExchangerateF.Text.Equals("") Or txtExchangerateF.Text.Equals("0")) Then
            lblError.Text = "Enter FROM Unit Exchange Rate..!"
            Return False
        ElseIf (txtExchangerateF2.Text.Equals("") Or txtExchangerateF2.Text.Equals("0")) Then
            lblError.Text = "Enter FROM Unit Transaction Exchange Rate..!"
            Return False
        ElseIf (txtExchangerateT.Text.Equals("") Or txtExchangerateT.Text.Equals("0")) Then
            lblError.Text = "Enter TO Unit Exchange Rate..!"
            Return False
        ElseIf (txtExchangerateT2.Text.Equals("") Or txtExchangerateT2.Text.Equals("0")) Then
            lblError.Text = "Enter TO Unit Transaction Exchange Rate..!"
            Return False
            'ElseIf (bankBalF < Convert.ToDouble(txtDAmount.Text.ToString())) Then
            '    lblError.Text = "Invalid Amount Please Check From Unit Bank Balance..!"
            '    Return False
        Else
            Return True
        End If
    End Function

    Private Sub doClear()
        txtDAmount.Text = ""
        txtHNarration.Text = ""
        drpBusinessunitF.SelectedValue = "0"
        drpBusinessunitT.SelectedValue = "0"
        txtExchangerateF.Text = "1.00"
        txtExchangerateT.Text = "1.00"
        txtExchangerateF2.Text = "1.00"
        txtExchangerateT2.Text = "1.00"
        hidBankFamt.Value = "0"
        hidBankFamt.Value = "0"
        h_Editid.Value = "0"
        labWords.Text = ""
        'getnextdocid()
        drpBankF.Items.Clear()
        drpBankT.Items.Clear()
        RptF.DataSource = Nothing
        RptF.DataBind()
        RptT.DataSource = Nothing
        RptT.DataBind()
        drpBusinessunitF.Focus()
    End Sub

    Private Function doInsert(ByVal Mode As String) As String
        Dim _parameter As String(,) = New String(28, 1) {}
        _parameter(0, 0) = "@IRF_ID"
        _parameter(0, 1) = h_Editid.Value.ToString()
        _parameter(1, 0) = "@ITF_FYEAR"
        _parameter(1, 1) = Session("F_YEAR").ToString()
        _parameter(2, 0) = "@ITF_DATE"
        _parameter(2, 1) = txtHDocdate.Text.ToString()
        _parameter(3, 0) = "@ITF_BSU_ID"
        _parameter(3, 1) = Session("sBsuid").ToString()
        _parameter(4, 0) = "@ITF_CUR_ID"
        _parameter(4, 1) = drpCurrency.SelectedValue.ToString()
        _parameter(5, 0) = "@ITF_DRBSU_ID"
        _parameter(5, 1) = drpBusinessunitT.SelectedValue.ToString()
        _parameter(6, 0) = "@ITF_CRBSU_ID"
        _parameter(6, 1) = drpBusinessunitF.SelectedValue.ToString()
        _parameter(7, 0) = "@ITF_bPOSTEDDRBSU"
        _parameter(7, 1) = "0"
        _parameter(8, 0) = "@ITF_bPOSTEDCRBSU"
        _parameter(8, 1) = "0"
        _parameter(9, 0) = "@ITF_bDELETED"
        _parameter(9, 1) = "0"
        _parameter(10, 0) = "@ITF_bPOSTED"
        _parameter(10, 1) = "0"
        _parameter(11, 0) = "@ITF_SESSIONID"
        _parameter(11, 1) = Session.SessionID.ToString()
        _parameter(12, 0) = "@ITF_NARRATION"
        _parameter(12, 1) = txtHNarration.Text.Trim().ToString()
        _parameter(13, 0) = "@ITF_DR_BANK_ACT_ID"
        _parameter(13, 1) = drpBankT.SelectedValue.ToString()
        _parameter(14, 0) = "@ITF_CR_BANK_ACT_ID"
        _parameter(14, 1) = drpBankF.SelectedValue.ToString()
        _parameter(15, 0) = "@ITF_DREXGRATE1"
        _parameter(15, 1) = txtExchangerateT.Text.ToString()
        _parameter(16, 0) = "@ITF_CREXGRATE1"
        _parameter(16, 1) = txtExchangerateF.Text.ToString()
        _parameter(17, 0) = "@ITF_DREXGRATE2"
        _parameter(17, 1) = txtExchangerateT2.Text.ToString()
        _parameter(18, 0) = "@ITF_CREXGRATE2"
        _parameter(18, 1) = txtExchangerateF2.Text.ToString()
        _parameter(19, 0) = "@ITF_AMOUNT"
        _parameter(19, 1) = txtDAmount.Text.ToString().Trim()
        _parameter(20, 0) = "@ITF_DR_DOCNO"
        _parameter(20, 1) = ""
        _parameter(21, 0) = "@ITF_CR_DOCNO"
        _parameter(21, 1) = ""
        _parameter(22, 0) = "@MODE"
        _parameter(22, 1) = Mode
        _parameter(23, 0) = "@AUD_WINUSER"
        _parameter(23, 1) = Page.User.Identity.Name.ToString()
        _parameter(24, 0) = "@Aud_form"
        _parameter(24, 1) = Master.MenuName.ToString()
        _parameter(25, 0) = "@Aud_user"
        _parameter(25, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(26, 0) = "@Aud_module"
        _parameter(26, 1) = HttpContext.Current.Session("sModule")
        _parameter(27, 0) = "@newACT_ID"
        _parameter(27, 1) = "0"
        'MainObj.doExcutive("SaveINTERUNIT_TRANSFER", _parameter, "mainDB")
        'lblError.Text = MainObj.MESSAGE
        MainObj.doExcutiveRetvalue("SaveINTERUNIT_TRANSFER", _parameter, "mainDB", "")
        lblError.Text = "Unexpected Error..!"
        If MainObj.SPRETVALUE.Equals(0) Then
            lblError.Text = MainObj.MESSAGE
            doClear()
        End If
        Return MainObj.SPRETVALUE
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CheckSave.Equals(False) Then
                Exit Sub
            End If
            Dim InsertMode As String = "0"
            Dim returnValue As String
            If ViewState("datamode").Equals("edit") Then
                InsertMode = "1"
            End If
            returnValue = doInsert(InsertMode)
            If returnValue = "0" Then
                doClear()
            End If
            lblError.Text = UtilityObj.getErrorMessage(returnValue)
            ViewState("datamode") = "add"
            getnextdocid()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        lockUnlok(True)
        h_editorview.Value = "Edit"
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Session("dtSettle") = DataTables.CreateDataTable_Settle
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If h_Editid.Value.Equals("") Or h_Editid.Value.Equals("0") Then
                Throw New ArgumentException("Could not process your request")

            ElseIf postedYN.Equals("Y") Then
                Throw New ArgumentException("Can not delete!!!Record already Posted..!!!")
            Else
                doInsert("2")
                ViewState("datamode") = "add"
                lockUnlok(True)
                doClear()

                Session("dtSettle") = DataTables.CreateDataTable_Settle
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub getnextdocid() '  generate next document number
        If ViewState("datamode") = "add" Then
            Try
                txtDocumentno.Text = AccountFunctions.GetNextDocId("FTM", "", CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtDocumentno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Private Sub PrintVoucher(ByVal printId As Int32)
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "PrintTreasuryTransfer"
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            cmd.Parameters.Add("@IRFID", SqlDbType.Int).Value = printId
            cmd.Parameters.Add("@BUSID", SqlDbType.VarChar).Value = Session("sBsuid").ToString()

            params("userName") = Session("sUsr_name")
            params("VoucherName") = "FUND TRANSFER MEMO"
            params("reportHeading") = "FUND TRANSFER MEMO"
            repSource.Parameter = params
            repSource.VoucherName = "FUND TRANSFER MEMO"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptTreasuryTransferReport.rpt"
            Session("ReportSource") = repSource
            ' Response.Redirect("../Reports/ASPX Report/RptViewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        PrintVoucher(h_Editid.Value)

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_editorview.Value = ""
        lockUnlok(True)
      
        ViewState("datamode") = "add"
        doClear()
        Session("dtSettle") = DataTables.CreateDataTable_Settle
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        
    End Sub

    Protected Sub drpBusinessunitF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillOtherdrp(drpBusinessunitF.SelectedValue.ToString(), drpBankF)
        RptF.DataSource = Nothing
        RptF.DataBind()
    End Sub

    Protected Sub drpBusinessunitT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillOtherdrp(drpBusinessunitT.SelectedValue.ToString(), drpBankT)
        RptT.DataSource = Nothing
        RptT.DataBind()
    End Sub
    Private Sub fillOtherdrp(ByVal selectId As String, ByVal drpObj As DropDownList)
        If selectId.Equals("0") Then
            drpObj.Items.Clear()
            Exit Sub
        Else
            Dim Sqlstring As String
            Sqlstring = "SELECT ACT_ID ,ACT_NAME FROM ACCOUNTS_M WHERE ACT_BSU_ID Like   '%" & selectId & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False' AND isnull(ACT_BACTIVE,0)=1 ORDER BY ACT_NAME"
            fillDropdown(drpObj, Sqlstring, "ACT_NAME", "ACT_ID", "MainDB", True)
        End If

    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad

    End Sub

    Protected Sub drpBankF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not drpBankF.SelectedValue.Equals("0") Then
            getAccStatus(RptF, drpBankF.SelectedValue.ToString(), drpBusinessunitF.SelectedValue.ToString())
        End If
    End Sub

    Protected Sub drpBankT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not drpBankT.SelectedValue.Equals("0") Then
            getAccStatus(RptT, drpBankT.SelectedValue.ToString(), drpBusinessunitT.SelectedValue.ToString())
        End If
    End Sub
    
    Private Sub getAccStatus(ByVal ObjRepeter As Repeater, ByVal accountId As String, ByVal BsuId As String)
        If IsDate(txtHDocdate.Text.ToString()).Equals(False) Then
            lblError.Text = "Invalid date formate..'DD/MMM/YYYY'..!"
            Exit Sub
        End If

        Dim _parameter As String(,) = New String(3, 1) {}
        _parameter(0, 0) = "@FY_DATE"
        _parameter(0, 1) = txtHDocdate.Text.ToString()
        _parameter(1, 0) = "@BSU_ID"
        _parameter(1, 1) = BsuId
        _parameter(2, 0) = "@ACCOUNT_ID"
        _parameter(2, 1) = accountId
        Dim _table As DataTable
        _table = MainObj.ListRecords("getACCOUNT_STATUS", _parameter, "MainDB")
        ObjRepeter.DataSource = _table
        ObjRepeter.DataBind()
        
        If _table.Rows.Count > 0 Then
            If ObjRepeter.Equals(RptF) Then
                hidBankFamt.Value = _table.Rows(0)("balance").ToString()
            End If
        End If
    End Sub
End Class
