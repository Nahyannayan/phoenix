<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accChqPrint.aspx.vb" Inherits="accChqPrint" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script >
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_report" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="700px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Cheque Print
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" id="tbl" style="cursor: hand">

                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" OnRowDataBound="gvJournal_RowDataBound" CssClass="table table-bordered table-row">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# bind("GUID") %>' __designer:wfdid="w4"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <input id="chkSelectall" onclick="change_chk_state('0')" type="checkbox" __designer:dtid="11540474045136992" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" type="checkbox" />

                                        </ItemTemplate>
                                        <ItemStyle Width="2%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            CHQ. DATE
                                <br />
                                            <asp:TextBox ID="txtChqDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch6" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchControl_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblchqDate" runat="server" Text='<%# Bind("VHD_CHQDT","{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            PAYEE
                                <br />
                                            <asp:TextBox ID="txtPayee" runat="server" ReadOnly="True"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch1" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblDETAILACCOUNT" runat="server" Text='<%# Bind("DETAILACCOUNT") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp;
                                    
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Amount
                                <br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration4" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblamount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHD_AMOUNT")) %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CHQ_DESCR" HeaderText="Format"></asp:BoundField>
                                    <asp:BoundField DataField="CHQ_FILE" HeaderText="RptName"></asp:BoundField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <SelectedRowStyle />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr>
                        <td align="center"> 
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" /></td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_rptfiles" runat="server"></asp:HiddenField>

                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

