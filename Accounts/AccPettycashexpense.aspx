<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccPettycashexpense.aspx.vb"
    Inherits="Accounts_AccPettycashexpense" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function getFilter(NameObj, IdObj, frm, BsuId, DptId) {
            if (frm == 'EMPLOYEE') {

                if (DptId == '') {
                    alert('Please Select Department..!')
                    return false;
                }

                DptId = document.getElementById(DptId).value;
            }

            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 650px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            document.getElementById('<%=hf_IdObj.ClientID%>').value = IdObj;
            document.getElementById('<%=hf_NameObj.ClientID%>').value = NameObj;

            //result = window.showModalDialog("FiltervaluesFind.aspx?FROM=" + frm + "&BSUID=" + BsuId + "&DPTID=" + DptId, "", sFeatures)
            result = radopen("FiltervaluesFind.aspx?FROM=" + frm + "&BSUID=" + BsuId + "&DPTID=" + DptId, "pop_up2")
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('__');
            //document.getElementById(IdObj).value = NameandCode[0];
            //document.getElementById(NameObj).value = NameandCode[1];
            return false;

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                if (document.getElementById('<%=hf_IdObj.ClientID%>')) {
                    var value_id = document.getElementById('<%=hf_IdObj.ClientID%>').value
                    document.getElementById(value_id).value = NameandCode[0];
                }
                if (document.getElementById('<%=hf_NameObj.ClientID%>')) {
                    var name_id = document.getElementById('<%=hf_NameObj.ClientID%>').value
                    document.getElementById(name_id).value = NameandCode[1];
                }


            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function Numeric_Only() {


            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
        function CtrlDisable() {
            if (event.keyCode == 17) {
                alert('This function disabled..!');
                return;
            }
        }
        function Mouse_Move(Obj) {
            document.getElementById(Obj).style.color = "Red";
        }
        function Mouse_Out(Obj) {
            document.getElementById(Obj).style.color = "#1b80b6"
        }
        function getDate(ObjName) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx?dt=" + document.getElementById(ObjName).value, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            document.getElementById(ObjName).value = result;
            return true;
        }

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
        function txtblur() {

            var get_val_txt = "";
            var get_rate_txt = "";
            var get_dist_txt = "";
            var get_val_txt_toal = 0;

            for (var i = 0; i < txtboxRateIDs.length; i++) {

                //for adding here...
                get_dist_txt = parseFloat(document.getElementById(txtboxDistIDs[i]).value);

                get_rate_txt = parseFloat(document.getElementById(txtboxRateIDs[i]).value);

                get_val_txt = get_dist_txt * get_rate_txt

                if (isNaN(get_val_txt)) {
                    document.getElementById(txtboxAmtIDs[i]).value = ""
                }
                else {
                    document.getElementById(txtboxAmtIDs[i]).value = get_val_txt.toFixed(2);
                    get_val_txt_toal += get_val_txt;

                }
                document.getElementById(lblTot).innerText = get_val_txt_toal.toFixed(2);
            }
        }
        function setBlankDates() {
            for (var i = 1; i < txtDateFieldIDs.length; i++) {
                if (document.getElementById(txtDateFieldIDs[i]).value == "") {
                    document.getElementById(txtDateFieldIDs[i]).value = document.getElementById(txtDateFieldIDs[0]).value;
                }
            }
        }

        function funcsum() {
            var table = document.getElementsByTagName("TABLE");
            table = document.getElementById("ctl00_cphMasterpage_gvSubExpenses");
            // document.getElementById("ctl00_cphMasterpage_gvSubExpenses");
            var sum = 0;
            for (var i = 1; i < table.rows.length; i++) {
                if (!isNaN(table.rows[i].cells[5].innerText)) {
                    //    sum=(parseFloat(sum)+parseFloat(table.rows[i].cells[5].getElementById("txtRategv").innerText)).toString();
                    //    alert(sum)
                }
            }

            //alert(sum);
        }
        function AddTotal() {
            var get_amt_txt = "";

            var get_val_txt_toal = 0;

            for (var i = 0; i < txtboxAmtIDs.length; i++) {

                //for adding here...
                get_amt_txt = parseFloat(document.getElementById(txtboxAmtIDs[i]).value);


                if (isNaN(get_amt_txt)) {

                }
                else {
                    get_val_txt_toal += get_amt_txt;

                }
            }

            document.getElementById(lblTot).innerText = get_val_txt_toal.toFixed(2);
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="labHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                HeaderText=" * Marked fields are mandtory"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Document No</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocument" runat="server" TabIndex="2" Enabled="False">
                            </asp:TextBox></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" OnTextChanged="txtHDocdate_TextChanged">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgCalendarH" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" CausesValidation="False" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtHDocdate"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Department</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDepartment" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="ImgDepartment" runat="server" ImageUrl="~/Images/forum_search.gif" CausesValidation="False" /></td>
                        <td align="left" width="20%"><span class="field-label">Expense Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddExpensetype" runat="server" DataTextField="TYP_DESCRIPTION" DataValueField="TYP_ID" AutoPostBack="True" OnSelectedIndexChanged="ddExpensetype_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" CausesValidation="False" /></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span> </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>

                        </td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkForward" runat="server" Text="Forward" Checked="True" CssClass="field-label" />
                        </td>
                    </tr>

                </table>
                <br />
                <asp:Panel ID="pnlOldGrid" runat="server" Visible="false">
                    <table align="center" width="100%">
                        <tr class="title-bg">
                            <td colspan="4" align="left">Details</td>
                        </tr>

                        <tr>
                            <td align="left" width="20%"><span class="field-label">Date</span></td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtDDocdate" runat="server" AutoPostBack="True" OnTextChanged="txtDDocdate_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                    ImageAlign="Middle" CausesValidation="False" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDDocdate"
                                    CssClass="error">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                                <asp:Label ID="labUnite" runat="server" Text="Unite" CssClass="field-label"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtUnit" runat="server" TextMode="MultiLine" TabIndex="1"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUnit"
                                    CssClass="error">*</asp:RequiredFieldValidator></td>
                            <td align="right" width="20%">
                                <asp:Label ID="labDistance" runat="server" Text="Distance"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtDistance" runat="server" TabIndex="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDistance"
                                    CssClass="error">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                                <asp:Label ID="labPurpose" runat="server" Text="Purpose" CssClass="field-label"></asp:Label>
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtPurpose" runat="server" TextMode="MultiLine" TabIndex="3" MaxLength="299"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPurpose"
                                    CssClass="error">*</asp:RequiredFieldValidator></td>

                        </tr>
                        <tr>
                            <td align="left" width="20%">
                                <asp:Label ID="lblSubType" runat="server" Text="Type" CssClass="field-label"></asp:Label>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlsubType1" runat="server"></asp:DropDownList>
                            </td>
                            <td align="center" colspan="2">
                                <asp:Button ID="Button2" runat="server" CausesValidation="False"
                                    Text="Button" />
                                <asp:Button ID="Button3" runat="server" Text="Open Doc" CausesValidation="False" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="20%">
                                <asp:Label ID="labRate" runat="server" Text="Rate" CssClass="field-label"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtRate" runat="server" TabIndex="4"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRate"
                                    CssClass="error">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnInsert" runat="server"
                                    CssClass="button" Text="Add" OnClick="btnInsert_Click" TabIndex="5" />
                                <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" CausesValidation="False" OnClick="btnClear_Click" TabIndex="6" /></td>

                        </tr>
                    </table>


                    <br />
                    <table align="center" width="100%">
                        <tr>
                            <td align="left">

                                <asp:GridView ID="gvJournal" runat="server" Width="100%" CssClass="table table-bordered table-row"
                                    EmptyDataText="No Item Added" AutoGenerateColumns="False" OnRowDataBound="gvJournal_RowDataBound"
                                    ShowFooter="True" Style="cursor: hand" OnRowDeleting="gvJournal_RowDeleting">
                                    <FooterStyle />
                                    <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                    <Columns>
                                        <asp:BoundField DataField="SLNO" HeaderText="Slno">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="DATE" SortExpression="DATE" HeaderText="Date">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UNIT">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PURPOSE">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DISTANCE">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RATE" HeaderText="Rate">
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>

                                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT" HeaderText="Amount">
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>

                                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        </asp:BoundField>
                                        <asp:CommandField ShowDeleteButton="True" DeleteText="Edit" HeaderText="Edit">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:CommandField>
                                    </Columns>
                                    <RowStyle CssClass="griditem"></RowStyle>
                                    <SelectedRowStyle></SelectedRowStyle>
                                    <HeaderStyle></HeaderStyle>
                                    <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">Please enter mandatory fields marked with <font color='red'>*</font>
                            for saving expense details.</td>
                    </tr>
                </table>
                <asp:Panel ID="pnlSubExp" runat="server" Height="100%">
                    <table align="center" width="100%">
                        <tr class="title-bg">
                            <td colspan="4" align="left">Expense Details</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <asp:GridView ID="gvSubExpenses" runat="server" ShowFooter="true" Style="cursor: hand"
                                    AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row">
                                    <RowStyle CssClass="griditem"></RowStyle>
                                    <Columns>
                                        <asp:BoundField DataField="gvRowNumber" HeaderText="SlNo">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Date &lt;font color='red'&gt;*&lt;/font&gt;">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDategv" runat="server" Text=' <%#Eval("P_Date", "{0:dd/MMM/yyyy}")%> ' Width="80px" __designer:wfdid="w8"></asp:TextBox>
                                                <asp:ImageButton ID="imgCalendargv" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" CausesValidation="False" __designer:wfdid="w9"></asp:ImageButton>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" TargetControlID="txtDategv" PopupButtonID="txtDategv" Format="dd/MMM/yyyy" __designer:wfdid="w10">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" TargetControlID="txtDategv" PopupButtonID="imgCalendargv" Format="dd/MMM/yyyy" __designer:wfdid="w11">
                                                </ajaxToolkit:CalendarExtender>
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReasongv" onmouseover="this.title=this.value" runat="server" Text=' <%#Eval("P_Reason")%> ' Width="120px" TextMode="MultiLine" __designer:wfdid="w12" Rows="3"></asp:TextBox>
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtParticularsgv" onmouseover="this.title=this.value" runat="server" Text=' <%#Eval("P_Particulars")%> ' Width="120px" TextMode="MultiLine" __designer:wfdid="w13" Rows="3"></asp:TextBox>
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <FooterTemplate>

                                                <asp:Label ID="lblText" runat="server" Text="Total :"></asp:Label>

                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtInvoicegv" runat="server" Text=' <%#Eval("P_Invoice")%> ' Width="85px"></asp:TextBox>

                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rate &lt;font color='red'&gt;*&lt;/font&gt;">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRategv" runat="server" Text=' <%#Eval("P_Rate")%> '
                                                    Width="80px"></asp:TextBox>

                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount &lt;font color='red'&gt;*&lt;/font&gt;">
                                            <FooterTemplate>

                                                <asp:Label ID="lblTotalAdd" runat="server"></asp:Label>

                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAmountgv" runat="server" Text=' <%#Eval("P_Amount")%> '
                                                    Width="80px"></asp:TextBox>

                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expense Type">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtsubType" runat="server" Text=' <%#Eval("P_ExpType")%> '
                                                    Visible="false" Width="80px"></asp:TextBox>
                                                <asp:DropDownList ID="ddlsubType" runat="server" Width="95px"></asp:DropDownList>

                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Upload Document">
                                            <ItemTemplate>

                                                <asp:FileUpload ID="flUpload" runat="server" Height="22px" Width="150px" />



                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Document">
                                            <FooterTemplate>

                                                <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" Visible="false"
                                                    CausesValidation="False" />

                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDoc" runat="server" AlternateText=' <%#Eval("P_UpFile")%> ' ToolTip="Click To View Document" CommandName="View" CommandArgument=' <%#Eval("P_UpFile")%> ' ImageUrl="../Images/ViewDoc.png" Visible="false" />
                                                &nbsp;
                <asp:HyperLink ID="btnImgDoc" runat="server">View</asp:HyperLink>
                                                <asp:ImageButton ID="imgDelete" runat="server" CommandName="FileDelete" ToolTip="Click To Delete Document" CommandArgument=' <%#Eval("P_UpFile")%> ' ImageUrl="../Images/Oasis_Hr/Images/cross.png" Visible="false" />


                                            </ItemTemplate>

                                            <FooterStyle HorizontalAlign="Right"></FooterStyle>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PCDID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPCDID" runat="server" Text=' <%#Eval("PCD_ID")%> '>
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle />
                                    <SelectedRowStyle></SelectedRowStyle>
                                    <HeaderStyle></HeaderStyle>
                                    <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>
                <br />
                <table width="100%">
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClick="btnDelete_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnPrint_Click" TabIndex="32" Text="Print" /></td>

                    </tr>
                </table>

                <asp:HiddenField ID="viewId" runat="server" Value="0" />
                <asp:HiddenField ID="h_Emp_No" runat="server" Value="0" />
                <asp:HiddenField ID="h_DepartmentId" runat="server" Value="0" />
                <asp:HiddenField ID="h_gvDate" runat="server" Value="0" />

                <asp:HiddenField ID="hf_IdObj" runat="server" />
                <asp:HiddenField ID="hf_NameObj" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

