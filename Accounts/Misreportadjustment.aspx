<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Misreportadjustment.aspx.vb" Inherits="Accounts_Misreportadjustment" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/Numeric.js"></script>
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkedBox) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            //var lstrChk = document.getElementById("chkAL").checked; 
            // alert(checkState);

            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], '');
            } checkedBox.checked = 'checked';
        }


        function ToggleSearch() {//alert('1')//;style.display
            if (document.getElementById('tr_search').style.display == 'none')
                document.getElementById('tr_search').style.display = ''
            else
                document.getElementById('tr_search').style.display = 'none'
        }
        function SearchHide() {
            document.getElementById('tr_search').style.display = 'none'
        }

        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 239px; ";
            sFeatures += "dialogHeight: 264px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            url = "../Accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFromDate.ClientID %>').value;

            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            document.getElementById('<%=txtFromDate.ClientID %>').value = result;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Mis Reports Adjustments
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_SelectedId" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Mis Reports Adjustments </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddMisReports" runat="server" AutoPostBack="True" CssClass="listbox" OnSelectedIndexChanged="ddMisReports_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSublink" runat="server" AutoPostBack="True" CssClass="listbox" Width="350px" OnSelectedIndexChanged="ddlSublink_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 0)" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                            EmptyDataText="No List Added" Width="100%" CssClass="table table-bordered table-row">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:BoundField ReadOnly="True" DataField="Slno" Visible="False" SortExpression="Slno" HeaderText="No">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField ReadOnly="True" DataField="BSU_ID" SortExpression="BSU_ID" HeaderText="Bsu Id">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField ReadOnly="True" DataField="BSU_NAME" SortExpression="BSU_NAME" HeaderText="Business Unit">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" Text='<%# Bind("RMS_AMOUNT") %>'></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" OnClick="btnEdit_Click" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClientClick="return confirm('Are you sure');" OnClick="btnDelete_Click" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClick="btnPrint_Click" TabIndex="32" Text="Print" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
