<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccEditChqBook.aspx.vb" Inherits="AccEditChqBook" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cheque Book Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <input
                    id="h_Chbid" runat="server" type="hidden" />
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%">

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                            <table width="100%">

                                <tr id="tr_ControlAccount" runat="server">
                                    <td width="20%" align="left">Account #</td>

                                    <td align="left">
                                        <asp:TextBox ID="txtControlAcc" runat="server" Style="left: -6px; top: 0px" Width="20%">
                                        </asp:TextBox>&nbsp;<a href="#" onclick="popUp('460','400','BANK','<%=txtControlAcc.ClientId %>','<%=txtControlAccDescr.ClientId %>')"></a>
                                        <asp:TextBox ID="txtControlAccDescr" runat="server" Style="left: 8px; position: relative; top: 0px"
                                            Width="69%"></asp:TextBox>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">Lot No</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtLotNo" runat="server" Style="left: -6px; top: 0px" MaxLength="4" Wrap="False" TabIndex="1"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                                    </td>
                                    <td align="left" width="20%">Prefix</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPrefix" runat="server" Style="left: -2px; top: 0px; position: relative;"
                                            Width="66%" MaxLength="4" Wrap="False" TabIndex="2"></asp:TextBox>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%">From #</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server" Style="left: -2px; position: relative; top: 0px"
                                            Width="40%" MaxLength="4" Wrap="False" TabIndex="2"></asp:TextBox><a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a></td>
                                    <td width="20%" align="left">To #</td>
                                    <td align="left" width="30%">&nbsp;<asp:TextBox ID="txtTo" runat="server" Style="left: -2px; position: relative; top: 0px"
                                        MaxLength="4" Wrap="False" TabIndex="3"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">Next #</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtNext" runat="server" MaxLength="4" Style="left: -2px; position: relative; top: 0px"
                                            Width="10%" Wrap="False" TabIndex="4"></asp:TextBox>

                                    </td>
                                    <td wdth="20%">Cheque Format</td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="drpChequeFormat" runat="server">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%">Previous Cheque Book</td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtLotid" runat="server" Width="20%"></asp:TextBox>
                                        <a href="#" onclick="popUp('460','400','MSTCHQBOOK','<%=txtLotid.ClientId %>','<%=txtControlAcc.ClientId %>','<%=h_Chbid.ClientId %>')">
                                            <img id="Img1" border="0" language="javascript" src="../Images/cal.gif" /></a>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" TabIndex="5" />
                            <asp:Button ID="btnSave" runat="server"
                                Text="SAVE" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server"
                                Text="Cancel" CssClass="button" TabIndex="6" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

