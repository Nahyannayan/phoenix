﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ParentPortalTileSetting.aspx.vb" Inherits="Accounts_ParentPortalTileSetting" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Tile Setting Page for Parent Portal
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
                </div>
                <table id="tblInfoTiles" width="100%">
                    <tbody>
                        <tr>
                            <td width="20%"><span class="field-label">Select Tiles</span>
                            </td>
                            <td width="30%" >
                                <div class="checkbox-list">
                                    <asp:CheckBoxList ID="chkinfolist" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </td>
                            <td width="20%" class="field-label"><span class="field-label">Select BSU Units</span>
                            </td>
                            <td width="30%">
                                <asp:ListBox ID="lstBsuUnit" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </td>

                        </tr>

                        <tr>
                            <td colspan="4" align="center">                                 
                                <asp:Button ID="btnSubmit" Text="Submit" runat="server" OnClick="submit_Click" CssClass="button" />
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click" CssClass="button" />

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>





