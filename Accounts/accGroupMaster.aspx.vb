Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class GroupMaster
    Inherits System.Web.UI.Page 
    Dim MainMnu_code As String 
    Dim Encr_decrData As New Encryption64 


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "accNewGroup.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            GridViewGroupMaster.Attributes.Add("bordercolor", "#1b80b6")

            If Request.QueryString("modified") <> "" Then
                Dim encObj As New Encryption64
                lblError.Text = getErrorMessage(encObj.Decrypt(Request.QueryString("modified").Replace(" ", "+")))
                h_SelectedId.Value = "1"
            End If

            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GridViewGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewGroupMaster.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GridViewGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewGroupMaster.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT * FROM ACCGRP_M WHERE GPM_ID<>'' "
            Dim ds As New DataSet
            Dim str_filter_code, str_filter_name, str_txtCode, str_txtName As String
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If GridViewGroupMaster.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = GridViewGroupMaster.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("GPM_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = GridViewGroupMaster.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("GPM_DESCR", str_Sid_search(0), str_txtName)
                ''column1
                End If
           
            Dim i As Integer
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name)
            GridViewGroupMaster.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                If Request.QueryString("newid") <> "" And h_SelectedId.Value = "1" Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If (ds.Tables(0).Rows(i)("GPM_ID") = Request.QueryString("newid")) Then
                            GridViewGroupMaster.SelectedIndex = i
                            h_SelectedId.Value = "0"
                        End If
                    Next
                Else
                    GridViewGroupMaster.SelectedIndex = -1
                End If
                
                GridViewGroupMaster.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                GridViewGroupMaster.DataBind()
                Dim columnCount As Integer = GridViewGroupMaster.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                GridViewGroupMaster.Rows(0).Cells.Clear()
                GridViewGroupMaster.Rows(0).Cells.Add(New TableCell)
                GridViewGroupMaster.Rows(0).Cells(0).ColumnSpan = columnCount
                GridViewGroupMaster.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridViewGroupMaster.Rows(0).Cells(0).Text = getErrorMessage("259")
            End If

            set_Menu_Img()
            txtSearch = GridViewGroupMaster.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = GridViewGroupMaster.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub GridViewGroupMaster_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridViewGroupMaster.RowCancelingEdit
        GridViewGroupMaster.EditIndex = -1
        gridbind()
        lblError.Text = ""
    End Sub


    Protected Sub GridViewGroupMaster_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewGroupMaster.RowDataBound
        Try
            Dim cmdCol As Integer = GridViewGroupMaster.Columns.Count - 1
            'hlChange lblCurrencyid
            Dim row As GridViewRow = e.Row
            ' Dim hlCButton As New HyperLink
            Dim hlCEdit As New HyperLink
            Dim lblGrpid As New Label
            'hlCButton = TryCast(row.FindControl("hlChange"), HyperLink)
            lblGrpid = TryCast(row.FindControl("lblGroupcode"), Label)
            hlCEdit = TryCast(row.FindControl("hlEdit"), HyperLink)

            If hlCEdit IsNot Nothing And lblGrpid IsNot Nothing Then
                'If hlCButton.ID = "hlChange" Then
                Dim i As New Encryption64
                viewstate("datamode") = Encr_decrData.Encrypt("view")
                 
                hlCEdit.NavigateUrl = "accnewgroup.aspx?editid=" & i.Encrypt(lblGrpid.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub GridViewGroupMaster_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewGroupMaster.RowDeleting
        ' ''Try
        ' ''    lblModifyalert.Text = ""
        ' ''    Dim row As GridViewRow = GridViewGroupMaster.Rows(e.RowIndex)
        ' ''    Dim lblGrpCode As New Label
        ' ''    lblGrpCode = TryCast(row.FindControl("lblGroupcode"), Label)

        ' ''    If Not lblGrpCode Is Nothing Then
        ' ''        Dim str_success As String = fnDoGroupOperations("Delete", lblGrpCode.Text & "", "")
        ' ''        If (str_success = "0") Then
        ' ''            lblModifyalert.Text = getErrorMessage("256")
        ' ''            GridViewGroupMaster.DataBind()

        ' ''        Else
        ' ''            lblModifyalert.Text = getErrorMessage(str_success)
        ' ''        End If
        ' ''    End If

        ' ''    GridViewGroupMaster.EditIndex = -1
        ' ''    gridbind()


        ' ''Catch ex As Exception
        ' ''    Errorlog(ex.Message)
        ' ''End Try
    End Sub 
     

    Protected Sub GridViewGroupMaster_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridViewGroupMaster.RowEditing
        GridViewGroupMaster.EditIndex = e.NewEditIndex
        gridbind()
        lblError.Text = ""
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

     
End Class
