<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccMstAccounts.aspx.vb" Inherits="AccMstAccounts" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function MoveCursorDown() {//alert("ii");
            var c = 16;
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            //alert(c)
            c++;
            if (c <= 0 || c == -1) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == '17';
                c = 16;
            }
            //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);

            //c++;
            //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null;
            var index = 0;
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c < rows.length - 1) {
                rows[c].className = "gridheader";
                if (c % 2 == 0)
                    rows[c - 1].className = "griditem_alternative";
                else
                    rows[c - 1].className = "griditem";
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

            }
            // alert(rows.length-900);

        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Chart of Accounts(GEMS GLOBAL)
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" id="tbl">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label>
                        </td>
                        <td colspan="2"></td>
                        <td align="right">
                            <asp:RadioButton ID="rbActive" runat="server" GroupName="Actives" Text="Active" AutoPostBack="True"
                                CssClass="radiobutton" />
                            <asp:RadioButton ID="rbInactive" runat="server" GroupName="Actives" Text="Inactive"
                                AutoPostBack="True" CssClass="radiobutton" />
                            <asp:RadioButton ID="rbAll" runat="server" GroupName="Actives" Text="All" AutoPostBack="True"
                                CssClass="radiobutton" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="gridheader" CssClass="table table-bordered table-row"
                                Width="100%" DataKeyNames="ACT_ID" EmptyDataText="No Data" AllowPaging="True"
                                 PageSize="20" OnRowDataBound="gvGroup1_RowDataBound">
                                <Columns>
                                    <asp:TemplateField SortExpression="ACT_ID" HeaderText="Account Code">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Account Code
                                <br />
                                            <asp:TextBox ID="txtCode" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ACT_NAME" HeaderText="Account Name">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Account Name
                                <br />
                                            <asp:TextBox ID="txtName" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ACT_TYPE" HeaderText="Account Type">
                                        <HeaderTemplate>
                                            Account Type
                                <br />
                                            <asp:DropDownList ID="DDAccountType" runat="server" AutoPostBack="True"
                                                OnSelectedIndexChanged="DDAccountType_SelectedIndexChanged">
                                                <asp:ListItem>All</asp:ListItem>
                                                <asp:ListItem Value="ASSET">ASSET</asp:ListItem>
                                                <asp:ListItem Value="EXPENSES">EXPENSES</asp:ListItem>
                                                <asp:ListItem Value="INCOME">INCOME</asp:ListItem>
                                                <asp:ListItem>LIABILITY</asp:ListItem>
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccType" runat="server" Text='<%# Bind("ACT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ACT_BANKCASH" HeaderText="Bank Or Cash">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Bank Or Cash<br />
                                            <asp:DropDownList ID="DDBankorCash" runat="server" AutoPostBack="True"
                                                OnSelectedIndexChanged="DDBankorCash_SelectedIndexChanged">
                                                <asp:ListItem>All</asp:ListItem>
                                                <asp:ListItem Value="B">Bank</asp:ListItem>
                                                <asp:ListItem Value="C">Cash</asp:ListItem>
                                                <asp:ListItem Value="N">Normal</asp:ListItem>
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBnkCash" runat="server" Text='<%# Bind("ACT_BANKCASH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ACT_SGP_ID" Visible="False" HeaderText="SubGroupId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGP_ID" runat="server" Text='<%# Bind("ACT_SGP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="SGP_DESCR" Visible="False" HeaderText="SubGroupDescr">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGP_DESCR" runat="server" Text='<%# Bind("SGP_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# Eval("ACT_ID", "AccEditAccount.aspx?editid={0}") %>'
                                                Text="Edit"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField Visible="False" HeaderText="Edit" ShowEditButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField Visible="False" HeaderText="Delete" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                                OnClientClick='return confirm("Are You Sure?");' Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle  />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
</asp:Content>
