<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccApprovalmasterview.aspx.vb" Inherits="Accounts_AccApprovalmasterview" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function Mouse_Move(Obj) {

            document.getElementById(Obj).style.color = "Red";
        }
        function Mouse_Out(Obj) {
            document.getElementById(Obj).style.color = "#1b80b6"
        }
        //function scroll_page()
        //{  
        // document.location.hash ='<%=h_Grid.value %>';
        // }   
        // window.onload = scroll_page;

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Approval Hierarchy
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">
                    <tr>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td align="left" width="10%">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                        <td align="left" width="10%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td align="left">
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <a id='top'></a></td>

                    </tr>

                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="center" width="100%">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">

                                <Columns>
                                    <asp:TemplateField SortExpression="BSU_NAME" HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            Business Unit<br />
                                            <asp:TextBox ID="txtBsuName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBsuName" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" OnClick="btnBsuName_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DOC_NAME" HeaderText="Document Type">
                                        <HeaderTemplate>
                                            Document Type<br />
                                            <asp:TextBox ID="txtDocType" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocumet" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" OnClick="btnDocumet_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label21" runat="server" Text='<%# Bind("DOC_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DPT_DESCR" HeaderText="Document Type">
                                        <HeaderTemplate>
                                            Department<br />
                                            <asp:TextBox ID="txtDepartment" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDepartment" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" OnClick="btnDepartment_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DPT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="APM_ID" Visible="False" HeaderText="APM_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("APM_BSU_ID") %>'></asp:Label>
                                            <asp:Label ID="lblDOC" runat="server" Text='<%# Bind("APM_DOC_ID") %>'></asp:Label>
                                            <asp:Label ID="lblDPT" runat="server" Text='<%# Bind("APM_DPT_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />

            </div>
        </div>
    </div>
</asp:Content>
