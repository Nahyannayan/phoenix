﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccSupplierPaymentRun.aspx.vb" Inherits="Accounts_AccSupplierPaymentRun" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <style type="text/css">
        .autocomplete_highlightedListItem_S1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }

        function getSupplerPaymentHistory(Actid) {
            sFeatures = "dialogWidth: 1350px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var combo = $find('<%= RadStatus.ClientID %>');
            var Seldate = document.getElementById("<%=txtDate.ClientID %>").value;
            var sta = combo.get_selectedItem().get_value();
            var bsu = document.getElementById("<%=h_Sel_BSU_ID.ClientID %>").value;
            var sphid = document.getElementById("<%=h_Sph_Id.ClientID %>").value;

            url = "AccSupplierPaymentHistory.aspx?EntryId=" + Actid + "&date=" + Seldate + "&Status=" + sta + "&bsu=" + bsu + "&sphid=" + sphid;
            result = window.showModalDialog(url, "", sFeatures);
        }

        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Payment Authorization Form
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtDate" PopupButtonID="lnkDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Supplier</span></td>
                                    <td align="left" width="30%">
                                        <telerik:RadComboBox ID="RadSupplier" runat="server" AutoPostBack="true"></telerik:RadComboBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Status</span></td>
                                    <td align="left" width="30%">
                                        <telerik:RadComboBox ID="RadStatus" runat="server" AutoPostBack="true"></telerik:RadComboBox>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                            <asp:Button ID="btnOpen" runat="server" CssClass="button" Visible="false" Text="Open" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnSend" runat="server" CssClass="button" Text="Send" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="GridViewShowDetails" runat="server" AutoGenerateColumns="False"
                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="TRN_ACT_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="BSU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Supplier Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblactid" runat="server" Text='<%# Bind("TRN_ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Supplier">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplier" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Due Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDueAmount" runat="server" Style="text-align: right" Width="100px" Text='<%# Eval("DUEAMOUNT","{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Approved Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblaAmount" runat="server" Style="text-align: right" Width="100px" Text='<%# Bind("[APPROVED AMOUNT]") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkView" runat="server" Text="View"></asp:LinkButton>
                                            <asp:Label ID="lblAct_ID" runat="server" Text='<%# Eval("TRN_ACT_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="h_Sel_BSU_ID" runat="server" />
                            <asp:HiddenField ID="h_Sph_Id" runat="server" />
                            <asp:HiddenField ID="h_Sel_ACT_ID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
