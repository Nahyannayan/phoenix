Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class SubGroupMaster
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "accNewsubGroup.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            If Request.QueryString("modified") <> "" Then
                'lblModifyalert.Text = "The Sub Group is modified successfully"
                Dim encObj As New Encryption64

                lblError.Text = getErrorMessage(encObj.Decrypt(Request.QueryString("modified").Replace(" ", "+")))
                h_SelectedId.Value = "1"
            End If
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

            Dim CurUsr_id As String = Session("sUsr_id")

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'Dim viewstate("menu_rights") As Integer = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))


            gridbind()
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubGroupMaster.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubGroupMaster.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubGroupMaster.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubGroupMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubGroupMaster.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    

    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT * FROM ACCSGRP_M"
            str_Sql = "SELECT " _
            & " gm.GPM_DESCR, sgm.SGP_ID, sgm.SGP_DESCR, gm.GPM_ID," _
            & " sgm.SGP_GPM_ID, sgm.SGP_TYPE SGP_TYPE_CODE," _
            & " CASE sgm.SGP_TYPE " _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash' " _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END SGP_TYPE" _
            & " FROM ACCGRP_M gm,ACCSGRP_M sgm " _
            & " where sgm.SGP_GPM_ID=gm.GPM_ID " _
            & " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            '''''' ''''''''
            Dim str_txtCode, str_filter_code, str_filter_subname, str_filter_type, str_filter_parentname, str_search As String
            str_filter_type = ""
            str_filter_code = ""
            str_filter_subname = ""
            str_filter_parentname = ""
            Dim str_txtParent, str_filter_bankcash, str_txtName, str_filter_name, str_Sid_search() As String
            str_filter_name = ""
            str_filter_bankcash = ""
            str_txtParent = ""
            str_txtName = ""
            str_txtCode = ""
            'str_img = h_selected_menu_1.Value()
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            str_search = str_Sid_search(0)
            Dim txtSearch As New TextBox
            Dim ddbank As New DropDownList
            Dim i_dd_bank As Integer
            If GridViewSubGroupMaster.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("SGP_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SGP_DESCR", str_Sid_search(0), str_txtName)
                ''column1
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtParent")
                str_txtParent = txtSearch.Text.Trim
                str_filter_parentname = set_search_filter("GPM_DESCR", str_Sid_search(0), str_txtParent)

                ddbank = GridViewSubGroupMaster.HeaderRow.FindControl("DropDownListType")
                i_dd_bank = ddbank.SelectedIndex
                If ddbank.SelectedItem.Value <> "A" Then
                    str_filter_type = " AND SGP_TYPE LIKE '%" & ddbank.SelectedItem.Value & "%'"
                End If

            End If


            str_Sql = "SELECT " _
            & " gm.GPM_DESCR, sgm.SGP_ID, sgm.SGP_DESCR, gm.GPM_ID," _
            & " sgm.SGP_GPM_ID, sgm.SGP_TYPE SGP_TYPE_CODE," _
            & " CASE sgm.SGP_TYPE " _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash' " _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END SGP_TYPE" _
            & " FROM ACCGRP_M gm,ACCSGRP_M sgm " _
            & " where sgm.SGP_GPM_ID=gm.GPM_ID " _
            & str_filter_code & str_filter_subname & str_filter_parentname & str_filter_type & str_filter_name _
            & " order by gm.GPM_DESCR "

            Dim i As Integer
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewSubGroupMaster.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                If Request.QueryString("newid") <> "" And h_SelectedId.Value = "1" Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If (ds.Tables(0).Rows(i)("SGP_ID") = Request.QueryString("newid")) Then
                            GridViewSubGroupMaster.SelectedIndex = i
                            h_SelectedId.Value = "0"
                        End If
                    Next
                Else
                    GridViewSubGroupMaster.SelectedIndex = -1
                End If
                GridViewSubGroupMaster.DataBind()
            Else
                GridViewSubGroupMaster.DataBind()
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                GridViewSubGroupMaster.DataBind()
                Dim columnCount As Integer = GridViewSubGroupMaster.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                GridViewSubGroupMaster.Rows(0).Cells.Clear()
                GridViewSubGroupMaster.Rows(0).Cells.Add(New TableCell)
                GridViewSubGroupMaster.Rows(0).Cells(0).ColumnSpan = columnCount
                GridViewSubGroupMaster.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridViewSubGroupMaster.Rows(0).Cells(0).Text = getErrorMessage("268")
                'lblModifyalert.Text = displayMessage("268", 20, "D")
            End If
            set_Menu_Img()
            txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = GridViewSubGroupMaster.HeaderRow.FindControl("txtParent")
            txtSearch.Text = str_txtParent

            ddbank = GridViewSubGroupMaster.HeaderRow.FindControl("DropDownListType")
            ddbank.SelectedIndex = i_dd_bank
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function fnDoGroupOperations(ByVal p_mode As String, ByVal p_SubGroupCode As String, _
      ByVal p_SubGroupName As String, _
      ByVal p_ParentGroupName As String, ByVal p_Type As String) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim cmd As New SqlCommand("sp_SubGroupMaster", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSubGroupCode As New SqlParameter("@SubGroupCode", SqlDbType.VarChar, 10)
            sqlpSubGroupCode.Value = p_SubGroupCode & ""
            cmd.Parameters.Add(sqlpSubGroupCode)

            Dim sqlpSubGroupName As New SqlParameter("@SubGroupName", SqlDbType.VarChar, 100)
            sqlpSubGroupName.Value = p_SubGroupName & ""
            cmd.Parameters.Add(sqlpSubGroupName)

            Dim sqlpParentGroupName As New SqlParameter("@ParentGroupName", SqlDbType.VarChar, 10)
            sqlpParentGroupName.Value = p_ParentGroupName & ""
            cmd.Parameters.Add(sqlpParentGroupName)

            Dim sqlpType As New SqlParameter("@Type", SqlDbType.VarChar, 100)
            sqlpType.Value = p_Type & ""
            cmd.Parameters.Add(sqlpType)

            Dim sqlpModifyOrDelete As New SqlParameter("@ModifyOrDelete", SqlDbType.VarChar, 8)
            sqlpModifyOrDelete.Value = p_mode & ""
            cmd.Parameters.Add(sqlpModifyOrDelete)

            Dim Success As New SqlParameter("@Success", SqlDbType.Int, 8)
            cmd.Parameters.Add(Success)
            cmd.Parameters("@Success").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            'Dim result As String = cmd.Parameters("@ErrorMsg").Value
            Dim str_success As String = cmd.Parameters("@Success").Value

            cmd.Parameters.Clear()
            If str_success = "0" Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            Return str_success

        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Function

    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("0")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ("0")
        End Try
    End Function

    Protected Sub GridViewSubGroupMaster_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridViewSubGroupMaster.RowCancelingEdit
        lbleRROR.Text = ""
        GridViewSubGroupMaster.EditIndex = -1
        gridbind()
    End Sub

    Protected Sub GridViewSubGroupMaster_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewSubGroupMaster.RowDataBound
        Try
            Dim cmdCol As Integer = GridViewSubGroupMaster.Columns.Count - 1
            'hlChange lblCurrencyid
            Dim row As GridViewRow = e.Row
            ' Dim hlCButton As New HyperLink
            Dim hlCEdit As New HyperLink
            Dim lblSGrpid As New Label
            'hlCButton = TryCast(row.FindControl("hlChange"), HyperLink)
            lblSGrpid = TryCast(row.FindControl("lblsubgroupcode"), Label)
            hlCEdit = TryCast(row.FindControl("hlEdit"), HyperLink)

            If hlCEdit IsNot Nothing And lblSGrpid IsNot Nothing Then
                'If hlCButton.ID = "hlChange" Then
                Dim i As New Encryption64
                viewstate("datamode") = Encr_decrData.Encrypt("view")

                hlCEdit.NavigateUrl = "accnewsubgroup.aspx?editid=" & i.Encrypt(lblSGrpid.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")
                'hlCButton.NavigateUrl = "ExchangeRates.aspx?currid=" & lblCurid.Text

                'End If

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub GridViewSubGroupMaster_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewSubGroupMaster.RowDeleting
        lbleRROR.Text = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim str_type As String = ""
        objConn.Open()
        '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim row As GridViewRow = GridViewSubGroupMaster.Rows(e.RowIndex)
            Dim lblSubGrpCode As New Label
            lblSubGrpCode = TryCast(row.FindControl("lblsubgroupcode"), Label)

            If Not lblSubGrpCode Is Nothing Then
                Dim str_success As String = fnDoGroupOperations("Delete", lblSubGrpCode.Text & "", "", "", "")
                If (str_success = "0") Then
                    lbleRROR.Text = getErrorMessage("265")
                    gridbind()
                Else
                    lbleRROR.Text = getErrorMessage(str_success)
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub GridViewSubGroupMaster_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridViewSubGroupMaster.RowEditing
        lbleRROR.Text = ""
        GridViewSubGroupMaster.EditIndex = e.NewEditIndex
        gridbind()
    End Sub

    Protected Sub GridViewSubGroupMaster_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridViewSubGroupMaster.RowUpdating
        Try
            lbleRROR.Text = ""

            Dim row As GridViewRow = GridViewSubGroupMaster.Rows(e.RowIndex)
            Dim txtsubGrpName As New TextBox
            Dim lblSubGrpCode As New Label
            Dim ddParent As New DropDownList
            Dim ddType As New DropDownList
            txtsubGrpName = TryCast(row.FindControl("txtSubName"), TextBox)
            lblSubGrpCode = TryCast(row.FindControl("lblsubgroupcode"), Label)
            ddParent = TryCast(row.FindControl("DropDownListParentedit"), DropDownList)
            ddType = TryCast(row.FindControl("DropDownListType"), DropDownList)

            ''''''''''''DropDownListType DropDownListParentedit


            If Not lblSubGrpCode Is Nothing Then
                Dim str_success As String = fnDoGroupOperations("Update", lblSubGrpCode.Text & "", txtsubGrpName.Text & "", ddParent.SelectedItem.Value & "", ddType.SelectedItem.Value & "")
                If (str_success = "0") Then
                    lbleRROR.Text = getErrorMessage("266")
                    GridViewSubGroupMaster.EditIndex = -1
                    gridbind()


                Else
                    lbleRROR.Text = getErrorMessage(str_success)
                End If
            End If
            '''''''''''''


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub DropDownListType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub GridViewSubGroupMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewSubGroupMaster.PageIndexChanging
        GridViewSubGroupMaster.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

     
End Class
