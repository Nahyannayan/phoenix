Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class SubLedgerMaster
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "accNewSubLedger.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            GridViewSubLedMaster.Attributes.Add("bordercolor", "#1b80b6")

            If Request.QueryString("modified") <> "" Then
                Dim encObj As New Encryption64
                lblError.Text = getErrorMessage(encObj.Decrypt(Request.QueryString("modified").Replace(" ", "+")))
                h_SelectedId.Value = "1"
            End If

            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubLedMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubLedMaster.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GridViewSubLedMaster.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = GridViewSubLedMaster.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT * FROM ACCOUNTS_SUB_ACC_M Where 1=1 "
            Dim ds As New DataSet
            Dim str_filter_code, str_filter_name, str_txtCode, str_txtName As String
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If GridViewSubLedMaster.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = GridViewSubLedMaster.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ASM_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = GridViewSubLedMaster.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ASM_NAME", str_Sid_search(0), str_txtName)
                ''column1
            End If

            Dim i As Integer
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name)
            GridViewSubLedMaster.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                If Request.QueryString("newid") <> "" And h_SelectedId.Value = "1" Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If (ds.Tables(0).Rows(i)("ASM_ID") = Request.QueryString("newid")) Then
                            GridViewSubLedMaster.SelectedIndex = i
                            h_SelectedId.Value = "0"
                        End If
                    Next
                Else
                    GridViewSubLedMaster.SelectedIndex = -1
                End If

                GridViewSubLedMaster.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                GridViewSubLedMaster.DataBind()
                Dim columnCount As Integer = GridViewSubLedMaster.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                GridViewSubLedMaster.Rows(0).Cells.Clear()
                GridViewSubLedMaster.Rows(0).Cells.Add(New TableCell)
                GridViewSubLedMaster.Rows(0).Cells(0).ColumnSpan = columnCount
                GridViewSubLedMaster.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridViewSubLedMaster.Rows(0).Cells(0).Text = getErrorMessage("259")
            End If

            set_Menu_Img()
            txtSearch = GridViewSubLedMaster.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = GridViewSubLedMaster.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub GridViewSubLedMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewSubLedMaster.PageIndexChanging
        GridViewSubLedMaster.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub GridViewSubLedMaster_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridViewSubLedMaster.RowCancelingEdit
        GridViewSubLedMaster.EditIndex = -1
        gridbind()
        lblError.Text = ""
    End Sub


    Protected Sub GridViewSubLedMaster_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewSubLedMaster.RowDataBound
        Try
            Dim cmdCol As Integer = GridViewSubLedMaster.Columns.Count - 1
            'hlChange lblCurrencyid
            Dim row As GridViewRow = e.Row
            ' Dim hlCButton As New HyperLink
            Dim hlCEdit As New HyperLink
            Dim lblGrpid As New Label
            'hlCButton = TryCast(row.FindControl("hlChange"), HyperLink)
            lblGrpid = TryCast(row.FindControl("lblSubLedCode"), Label)
            hlCEdit = TryCast(row.FindControl("hlEdit"), HyperLink)

            If hlCEdit IsNot Nothing And lblGrpid IsNot Nothing Then
                'If hlCButton.ID = "hlChange" Then
                Dim i As New Encryption64
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                hlCEdit.NavigateUrl = "accNewSubLedger.aspx?editid=" & i.Encrypt(lblGrpid.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub GridViewSubLedMaster_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridViewSubLedMaster.RowEditing
        GridViewSubLedMaster.EditIndex = e.NewEditIndex
        gridbind()
        lblError.Text = ""
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function


End Class
