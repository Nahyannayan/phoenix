<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="AccBankR.aspx.vb" Inherits="AccBankR" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

        <script language="javascript" type="text/javascript">
            function expandcollapse(obj, row) {
                var div = document.getElementById(obj);
                var img = document.getElementById('img' + obj);

                if (div.style.display == "none") {
                    div.style.display = "block";
                    if (row == 'alt') {
                        img.src = "../images/Misc/minus.gif";
                    }
                    else {
                        img.src = "../images/Misc/minus.gif";
                    }
                    img.alt = "Close to view other Customers";
                }
                else {
                    div.style.display = "none";
                    if (row == 'alt') {
                        img.src = "../images/Misc/plus.gif";
                    }
                    else {
                        img.src = "../images/Misc/plus.gif";
                    }
                    img.alt = "Expand to show Orders";
                }
            }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {
                var lstrVal;
                var lintScrVal;
                var NameandCode;
                var result;

                document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;


                if (pMode == 'BANK') {
                    result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];

                }
                else if (pMode == 'NOTCC') {
                    if (ctrl2 == '' || ctrl2 == undefined) {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                    } else {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up");
                    }
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];
                    ////document.getElementById(ctrl2).value=lstrVal[2];
                    //// document.getElementById(ctrl3).value=lstrVal[3];

                }
                else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                    QRYSTR = '';
                    if (pMode == 'CASHFLOW_BP')
                        QRYSTR = "?rss=0";
                    if (pMode == 'CASHFLOW_BR')
                        QRYSTR = "?rss=1";

                    result = radopen("acccpShowCashflow.aspx" + QRYSTR, "pop_up");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('__');
                    //document.getElementById(ctrl).value = lstrVal[0];


                }

            }


            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode == 'BANK') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'NOTCC') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                        document.getElementById(ctrl).value = NameandCode[0];
                    }
                }
            }


            function CopyDetails() {
                document.getElementById('<%=txtLineNarrn.ClientID %>').value = document.getElementById('<%=txtNarrn.ClientID %>').value;

            }

            function getBank() {
                popUp('460', '400', 'BANK', '<%=txtBankCode.ClientId %>', '<%=txtBankDescr.ClientId %>');
            }

            function getreceived() {
                popUp('960', '600', 'NOTCC', '<%=Detail_ACT_ID.ClientId %>', '<%=txtPartyDescr.ClientId %>', '<%=ddDCollection.ClientId %>');
            }

            function Settle_Online() {

                var NameandCode;
                var result;
                var pId = 1;//h_NextLine alert(sFeatures)       
                if (pId == 1) {
                    url = "ShowOnlineSettlementDR.aspx?actid=" + document.getElementById('<%=Detail_ACT_ID.ClientID %>').value + "&lineid=" + document.getElementById('<%=h_NextLine.ClientID %>').value + "&docno=" + document.getElementById('<%=txtdocNo.ClientID %>').value + "&dt=" + document.getElementById('<%=txtdocDate.ClientID %>').value;
                }
                result = radopen(url, "pop_up1");
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                document.getElementById('<%=txtLineAmount.ClientID %>').value = result;
                return true;--%>
            }

            function OnClientClose1(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtLineAmount.ClientID %>').value = arg.NameandCode;
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }


            function AddDetails(url) {


                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';

                dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();
                return false;
            }
        </script>

    </telerik:RadScriptBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Bank Receipts
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table cellspacing="0" width="100%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error">
                            </asp:Label>
                    </tr>
                </table>
                <table width="100%" align="center"
                    style="border-collapse: collapse">

                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Doc No [Old Ref No]</span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocNo" runat="server" Width="44%">
                            </asp:TextBox>[
                <asp:TextBox ID="txtOldDocNo" runat="server" Width="35%">
                </asp:TextBox>
                            ]
                        </td>
                        <td align="right" width="20%">
                            <span class="field-label">Doc Date </span><span style="color: red">*</span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server" Width="80%" AutoPostBack="true">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Bank Account <span style="color: red">*</span> </span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtBankCode" runat="server" Width="20%" AutoPostBack="true">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false;" />
                            <asp:TextBox ID="txtBankDescr" runat="server" Width="60%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td width="20%" align="left">
                            <span class="field-label">Collection Type <span style="color: red">*</span></span>
                        </td>
                        <td colspan="3" align="left">
                            <asp:DropDownList ID="ddCollection" runat="server" TabIndex="10">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Currency<span style="color: red">*</span> </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="true" Width="20%">
                            </asp:DropDownList>

                            <asp:TextBox ID="txtExchRate" runat="server" Width="60%">
                            </asp:TextBox>
                        </td>
                        <td width="20%" align="right">
                            <span class="field-label">Group Rate <span style="color: red">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server" Width="80%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">
                            <span class="field-label">Credit Details </span>
                            <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine"
                                Visible="False">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Collection Type<span style="color: red">*</span> </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddDCollection" runat="server" AutoPostBack="true" TabIndex="20" Width="80%">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkAdvance" runat="server" AutoPostBack="True" OnCheckedChanged="chkAdvance_CheckedChanged"
                                Text="Advance"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Credit Account <span style="color: red">*</span></span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%" AutoPostBack="true">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgRcvd" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getreceived();return false;" />
                            <asp:TextBox ID="txtPartyDescr" runat="server" Width="60%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Narration <span style="color: red">*</span></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLineNarrn" runat="server" TextMode="MultiLine" Width="80%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Cheque No <span style="color: red"></span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtChqBook" runat="server" Width="80%">
                            </asp:TextBox>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Cheque Date <span style="color: red"></span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtChqDate" runat="server" Width="80%">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgChq" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Amount <span style="color: red">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLineAmount" runat="server" Width="80%" AutoCompleteType="Disabled">
                            </asp:TextBox>
                            <asp:LinkButton ID="lbSettle" runat="server" OnClientClick="Settle_Online(); return false;">(Settle)</asp:LinkButton>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Cash Flow Line <span style="color: red">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtCashFlow" runat="server" Width="80%">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgCashFlow" runat="server" ImageUrl="~/Images/cal.gif" />
                        </td>
                    </tr>
                    <tr runat="server" id="trTaxType" visible="false">
                        <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddlVATCode" runat="server" ></asp:DropDownList></td>
                        <td width="20%" align="left"></td>
                        <td width="30%" align="left"></td>
                    </tr>

                    <tr runat="server" id="trCostCenter" visible="false">
                        <td width="20%" align="left">
                            <span class="field-label">Cost center <span style="color: red">*</span> </span>
                        </td>
                        <td width="30%" align="left" colspan="3">
                            <asp:DropDownList ID="ddlCostCenter" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceCostCentre0"
                                DataTextField="CCS_DESCR" DataValueField="CCS_ID">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSourceCostCentre0" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                SelectCommand=" SELECT CCS_ID,CCS_DESCR 
            FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                        <td width="20%" align="left">
                            <span class="field-label">Upload Employees </span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:FileUpload ID="fuEmployeeData" runat="server" />
                            <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Cost Allocation </span>
                        </td>
                        <td align="left" colspan="3">
                            <!-- content start -->
                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnFill" runat="server" Text="Add" CssClass="button" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                Width="100%" CellPadding="1">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                    width="9px" border="0" src="../images/Misc/plus.gif" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="true">
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Account Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration">
                                        <ItemStyle Width="20%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLine" HeaderText="CashFlow">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Colln" HeaderText="Colln">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" SkinID="Grid" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChqBookId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqBookId" runat="server" Text='<%# Bind("ChqBookId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ChqBookLot" HeaderText="Cheque Book" Visible="False">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ChqNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("ChqNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChqDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqDate" runat="server" Text='<%# Bind("ChqDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ply" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Costreqd" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostreqd" runat="server" Text='<%# Bind("Costreqd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="lbEdit_Click"> </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteBtn" CommandArgument='<%# Eval("id") %>' CommandName="Delete"
                                                runat="server">
         Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%" align="right">
                                                    <div id="div<%# Eval("id") %>">
                                                        <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False"
                                                            EmptyDataText="Cost Center Not Allocated" CssClass="table table-bordered table-row"
                                                            OnRowDataBound="gvCostchild_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                <asp:BoundField DataField="Costcenter" HeaderText="Costcenter" />
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                    HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtAmt" runat="server" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                            onblur="CheckAmount(this)"></asp:TextBox>
                                                                        <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td colspan="100%" align="right">
                                                                                <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                                                    EmptyDataText="Cost Center Not Allocated"
                                                                                    Width="100%" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                        <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                        <asp:TemplateField HeaderText="Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <span class="field-label">Total </span>
                            <asp:TextBox ID="txtAmount" runat="server" Width="20%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                Visible="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False"
                                Visible="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Visible="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');"
                                Visible="False" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" Visible="False" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Visible="False" />
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <asp:HiddenField ID="hCheqBook" runat="server" />
                <asp:HiddenField ID="h_CostCenterType" runat="server" />
                <asp:HiddenField ID="hPLY" runat="server" />
                <asp:HiddenField ID="hNum" runat="server" Value="1" />
                <asp:HiddenField ID="hCostreqd" runat="server" Value="1" />
                <input id="h_NextLine" runat="server" type="hidden" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calChq" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgChq" TargetControlID="txtChqDate" BehaviorID="ctl01_CalendarExtender2">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
