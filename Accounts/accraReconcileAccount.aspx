<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accraReconcileAccount.aspx.vb" Inherits="Accounts_accraReconcileAccount" Title="Reconcile Account" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text] {
            min-width: 20% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++) {
                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);

                    // Changet(TextBoxDeposit[i], lstrChk);  
                    // Changet(TextBoxPayment[i], lstrChk);
                }
                OncheckChange('');
            }
        }

        function ChangeAllCheckBoxStatesNew(checkState) {
            var chk_state = document.getElementById("Checkbox1").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }

        function ChangeTotal(checkState) {

            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++)
                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);
            }
        }
        function OncheckChangeNew(CC) {
        }

        function OncheckChange(CC) {

            if (CheckboxSelect != null) {
                var psum = 0.0;
                var dsum = 0.0;
                //alert(CheckboxSelect.length); 
                for (var i = 0; i < CheckboxSelect.length; i++) {
                    cb = document.getElementById(CheckboxSelect[i]);
                    if (cb != null) {
                        if (cb.checked == true) {
                            if (document.getElementById(TextBoxDate[i]).value == '') {
                                document.getElementById(TextBoxDate[i]).value = TextDate[i];
                            }

                            psum = psum + parseFloat(document.getElementById(TextBoxPayment[i]).value); //alert(psum);
                            dsum = dsum + parseFloat(document.getElementById(TextBoxDeposit[i]).value);
                        }
                        else {
                            document.getElementById(TextBoxDate[i]).value = '';
                        }
                    } //if                 

                }//for
                document.getElementById('<%=txtDTotalamount.ClientID %>').value = parseFloat(document.getElementById('<%=txtRBalance.ClientID %>').value) + dsum - psum;
            }
        }
        //TextBoxDate

       

        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


            var lstrVal;
            var lintScrVal;

            document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
            document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
            document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var NameandCode;
            var result;
            if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");

                //if (result=='' || result==undefined)
                //{    return false;      } 
                //lstrVal=result.split('||');     
                //document.getElementById(ctrl).value=lstrVal[0];
                //document.getElementById(ctrl1).value=lstrVal[1];              
            }
        }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode=='ALLACC'||pMode=='BANKONLY'||pMode=='CASHONLY'||pMode=='CUSTSUPP'||pMode=='CUSTSUPPnIJV') {
                        document.getElementById(ctrl).value=NameandCode[0];
                        document.getElementById(ctrl1).value=NameandCode[1];
                    }

                }
            }

        function getAccountH() {
            popUp('960', '600', 'BANKONLY', '<%=txtHAccountcode.ClientId %>', '<%=txtHAccountname.ClientId %>');
            return false;
        }
    </script>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Bank Reconciliation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Doc No</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True" Width="60%"></asp:TextBox>
                                        <asp:Button ID="btnReonciliation" runat="server" CssClass="button" Text="Entries not in OASIS" /></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Reconcile Date </span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="btnHdate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="btnHdate" TargetControlID="txtHDocdate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <span class="field-label">BankAccount </span></td>

                                    <td colspan="2" align="left" valign="middle">
                                        <asp:TextBox ID="txtHAccountcode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="btnHaccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccountH(); return false;" />
                                        <asp:TextBox ID="txtHAccountname" runat="server" Width="60%"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Balance as per Bank Book </span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtHTotal" runat="server" AutoPostBack="True">10000</asp:TextBox></td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>

                            <table id="tblDetails" runat="server" visible="false" align="center" with="100%">
                                <tr>
                                    <td align="left">Docno
                            <asp:TextBox ID="txtDocno" runat="server" Width="8%"></asp:TextBox>
                                        Narration
                            <asp:TextBox ID="txtNarration" runat="server" Width="30%"></asp:TextBox>
                                        Chqno
                            <asp:TextBox ID="txtChqNo" runat="server" Width="8%"></asp:TextBox>
                                        Deposit
                            <asp:TextBox ID="txtDeposit" runat="server" Text="0.00" Width="8%" Style="text-align: right"></asp:TextBox>
                                        Payment
                            <asp:TextBox ID="txtPayment" runat="server" Text="0.00" Width="8%" Style="text-align: right"></asp:TextBox>
                                        <asp:Button ID="btnSaveDetails" runat="server" CssClass="button" Text="Save" />
                                    </td>
                                </tr>
                            </table>

                            <table id="Table1" align="center" width="100%">
                                <tr>
                                    <td align="center" width="100%">
                                        <br />
                                        <asp:GridView ID="gvReconcile" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="There is no transactions to reconcile" Width="100%">
                                            <EmptyDataRowStyle Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkSelect" type="checkbox" runat="server" value='<%# Bind("JNL_ID") %>' onclick="OncheckChangeNew(true)" name="RCD_bTemp" />
                                                        <asp:TextBox ID="txtRCDate" runat="server" Text='<%# returndate(Container.DataItem("RCD_RECONDT")) %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <input id="Checkbox1" type="checkbox" onclick="ChangeAllCheckBoxStatesNew(true);" />
                                                        Select
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="JNL_DOCTYPE" HeaderText="Type" SortExpression="JNL_DOCTYPE" />
                                                <asp:BoundField DataField="JNL_DOCNO" HeaderText="Docno" SortExpression="JNL_DOCNO" />
                                                <asp:BoundField DataField="JNL_NARRATION" HeaderText="Narration" SortExpression="JNL_NARRATION" />
                                                <asp:BoundField DataField="JNL_CHQNO" HeaderText="Chqno" SortExpression="JNL_CHQNO" />
                                                <asp:BoundField DataField="JNL_CHQDT" HeaderText="Date" ReadOnly="True" SortExpression="JNL_CHQDT" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" />
                                                <asp:BoundField DataField="OPP_ACT_NAME" HeaderText="Account" SortExpression="OPP_ACT_NAME" />
                                                <asp:TemplateField HeaderText="Deposit" SortExpression="JNL_DEBIT">
                                                    <ItemTemplate>
                                                        <asp:TextBox Style="text-align: right" ID="txtDeposit" runat="server" CssClass="inputbox_rtl" Text='<%# Bind("JNL_DEBIT") %>'></asp:TextBox>
                                                        <asp:HiddenField ID="hdnDocType" runat="server" Value='<%# Bind("JNL_DOCTYPE") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment" SortExpression="JNL_CREDIT">
                                                    <ItemTemplate>
                                                        <asp:TextBox Style="text-align: right" ID="txtPayment" runat="server" CssClass="inputbox_rtl" Text='<%# Bind("JNL_CREDIT") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="JNL_ID" Visible="False" />
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gvHeaderRow" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:TextBox ID="txtRBalance" runat="server" Visible="false">0</asp:TextBox>
                                        <span class="field-label">Balance as per Statement </span>
                                        <asp:TextBox ID="txtDTotalamount" runat="server" ReadOnly="True" Width="20%">0</asp:TextBox>

                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:FileUpload ID="FileUpload1" runat="server" Visible="false" />
                                        <asp:Button ID="btView" runat="server" Text="View Excel" Visible="false" CssClass="button" />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" CausesValidation="False" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnLater" runat="server" CssClass="button" OnClientClick="return confirm('Are you sure');"
                                            Visible="false" Text="Finish Later" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <asp:GridView ID="GridView1" runat="server" Visible="False">
                </asp:GridView>

            </div>
        </div>
    </div>
</asp:Content>

