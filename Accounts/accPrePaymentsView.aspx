<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accPrePaymentsView.aspx.vb" Inherits="accPrePaymentsView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Pre Payment Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                        <td align="left" width="20%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td align="right" colspan="2">
                            <asp:RadioButton ID="rbunpost" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="Actives" Text="Open" />
                            <asp:RadioButton ID="rbpost" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="Actives" Text="Post" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True"
                                CssClass="radiobutton" GroupName="Actives" Text="All" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" width="100%">
                            <table align="left" width="100%">

                                <tr>
                                    <td align="left">
                                        <asp:GridView ID="gvPrePayment" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" Width="100%"
                                            AllowPaging="True" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" OnRowDataBound="gvPrePayment_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Doc No" SortExpression="DocNo" HeaderStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DocNo") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate >
                                                        <asp:Label ID="lblDocNoH" runat="server" Text="Doc No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchDocNo" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchDocNo_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="  Year  " SortExpression="FYear" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("FYear") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" Wrap="True" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("FYear") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PRP_PARTY" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("P_Party") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("P_Party") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Creditor Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("PARTYNAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreditor" runat="server" Text='<%# Bind("PARTYNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblCreditorH" runat="server" Text="Creditor Name"></asp:Label>
                                                        <br />
                                                    
                                                        <asp:TextBox ID="txtCreditor" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchCreditor" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchCreditor_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="P_PreAcc" SortExpression="P_PreAcc" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("P_PreAcc") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("P_PreAcc") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Prepaid Account">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("PREPAYACCNAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrepaid" runat="server" Text='<%# Bind("PREPAYACCNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblprepaidAccount" runat="server" Text="Prepaid Account"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtPrepaid" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchPrepaid" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchPrepaid_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EXPENSEACC" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("EXPENSEACC") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("EXPENSEACC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expense Account   ">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("EXPACCNAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ControlStyle Width="45%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="100%" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30%" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExp" runat="server" Text='<%# Bind("EXPACCNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblExpH" runat="server" Text="Expense Account"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtExp" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchExp" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchExp_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doc Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("DOCDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDateH" runat="server" Text="Doc Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchDate" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchDate_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("Cur") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Cur") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("P_Amt") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("P_Amt")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAmountH" runat="server" Text="Amount"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchAmount" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchAmount_Click" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No. of Inst" HeaderStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("NOOFINST") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblNoInstH" runat="server" Text="Months"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtNoInst" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchNoInst" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchNoInst_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNoInst" runat="server" Text='<%# Bind("NOOFINST") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Posted">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("P_bPost")) %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:LinkButton ID="lbSummary" runat="server" OnClick="lbSummary_Click">Summary</asp:LinkButton>
                                                        <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Guid" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Guid") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGuid" runat="server" Text='<%# Bind("Guid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Print">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <SelectedRowStyle />
                                            <RowStyle CssClass="griditem" />
                                        </asp:GridView>
                                        &nbsp;&nbsp;<br />
                                        <br />
                                        <br />
                                        <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT"
                                            Width="100%" CssClass="table table-bordered table-row">
                                            <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                                            <Columns>
                                                <asp:BoundField DataField="ACT_Name" HeaderText="Account Name" SortExpression="ACT_name" />
                                                <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                                    Visible="False" />
                                                <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                                <asp:BoundField DataField="VDS_descr" HeaderText="Name" SortExpression="VDS_descr" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VDS_AMOUNT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="GRPFIELD" HeaderText="Cost Center" SortExpression="GRPFIELD" />
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_9" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
