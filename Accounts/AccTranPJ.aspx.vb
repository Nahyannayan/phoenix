Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class AccTranPJ
    Inherits System.Web.UI.Page
     
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            Try
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"




                '   --- Lijo's Code ---
                Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (Session("MainMnu_code") <> "A150021") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle
                    Dim url As String
                    Dim datamodeAdd As String = Encr_decrData.Encrypt("add")
                    url = String.Format("AccPJ.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), datamodeAdd)
                    hlAddNew.NavigateUrl = url
                End If
                gridbind()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If

        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
        End If




    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvGroup1.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid("mnu_4_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Private Sub gridbind()
        Dim lstrDocNo As String = String.Empty
        Dim lstrOldDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrBankAC As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltOldDocNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltPartyAC As String = String.Empty
        Dim lstrFiltBankAC As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltCurrency As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim txtSearch As New TextBox
        Dim larrSearchOpr() As String

        If gvGroup1.Rows.Count > 0 Then
            ' --- Initialize The Variables
            lstrDocNo = ""
            lstrDocDate = ""
            lstrPartyAC = ""
            lstrBankAC = ""
            lstrNarration = ""
            lstrCurrency = ""
            lstrAmount = ""

            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "A.PUH_DocNo", lstrDocNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "A.PUH_DocDt", lstrDocDate)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
            lstrBankAC = txtSearch.Text
            If (lstrBankAC <> "") Then lstrFiltBankAC = SetCondn(lstrOpr, "A.HeaderAccount", lstrBankAC)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtOldDocNo")
            lstrOldDocNo = txtSearch.Text
            If (lstrOldDocNo <> "") Then lstrFiltOldDocNo = SetCondn(lstrOpr, "A.PUH_REFNO", lstrOldDocNo)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "A.PUH_Narration", lstrNarration)

            '   -- 6 Amount
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
            lstrAmount = txtSearch.Text
            If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "A.PUH_AMOUNT", lstrAmount)


            '   -- 7  Currency
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
            lstrCurrency = txtSearch.Text
            If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "A.Cur_Descr", lstrCurrency)
        End If




        Dim str_Topfilter As String = ""
        If UsrTopFilter1.FilterCondition <> "All" Then
            str_Topfilter = " top " & UsrTopFilter1.FilterCondition
        End If 


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = " SELECT DISTINCT " & str_Topfilter _
        & " A.GUID,A.PUH_DOCDT,A.PUH_DOCNO as DocNo ,Replace(Left(Convert(VarChar,A.PUH_DOCDT,113),11),' ','/') as DocDate ,A.HeaderAccount ,Max(A.DetailAccount) as DetailAccount," _
        & " MAx(A.PUH_Narration) as Narration,Max(A.CUR_ID) as Currency,Max(isNUll(PUH_REFNO,'')) as OldDocNo," _
        & " (SELECT     SUM(PUD_QTY*PUD_RATE)" _
                & " FROM PURCHASE_D WHERE (PUD_SUB_ID = '" & Session("Sub_ID") & "') " _
                & " AND (PUD_BSU_ID = '" & Session("sBsuid") & "') " _
                & " AND (PUD_DOCNO = A.PUH_DOCNO))  " _
                & " AS Amount  " _
        & " FROM vw_OSA_PURCHASE A WHERE PUH_bDELETED=0 AND PUH_FYEAR = " & Session("F_YEAR")
        str_Sql = str_Sql & lstrFiltDocNo & lstrFiltOldDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltPartyAC & lstrFiltNarration & lstrFiltAmount & lstrFiltCurrency

        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
        End If

        Dim str_ListDoc As String = String.Empty
        If Session("ListDays") IsNot Nothing Then
            'If String.Compare(Session("ListDays"), "all", True) <> 0 Then
            str_ListDoc = " AND A.PUH_DocDt BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
        End If

        If (Session("FetchType") = "0") Then
            str_Sql = str_Sql & " AND PUH_bPOsted='False'"
        ElseIf (Session("FetchType") = "1") Then
            str_Sql = str_Sql & " AND PUH_bPOsted='True'"
            str_Sql += str_ListDoc
        ElseIf (Session("FetchType") = "2") Then
            str_Sql += str_ListDoc
        End If
        ''added by Guru
        str_Sql = str_Sql & " AND BSU_ID='" & Session("sBsuid") & "'"
        ''added by Guru

        str_Sql = str_Sql & " GROUP BY A.GUID,A.PUH_DocNo,A.PUH_DocDt,A.HeaderAccount   ORDER BY 2 Desc,3 DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvGroup1.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup1.DataBind()
            Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count

            gvGroup1.Rows(0).Cells.Clear()
            gvGroup1.Rows(0).Cells.Add(New TableCell)
            gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvGroup1.DataBind()
        End If

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
        txtSearch.Text = lstrDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
        txtSearch.Text = lstrDocDate

        txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
        txtSearch.Text = lstrBankAC

        txtSearch = gvGroup1.HeaderRow.FindControl("txtOldDocNo")
        txtSearch.Text = lstrOldDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
        txtSearch.Text = lstrNarration

        txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
        txtSearch.Text = lstrAmount

        txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
        txtSearch.Text = lstrCurrency

        set_Menu_Img()
    End Sub



    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvGroup1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup1.RowCommand
        If e.CommandName = "View" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvGroup1.Rows(index), GridViewRow)
            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            Dim url As String = String.Empty
            Eid = Encr_decrData.Encrypt(Eid)
            Session("datamode") = "view"
            If (Session("MainMnu_code") = "A150021") Then
                Session("MainMnu_code") = Encr_decrData.Encrypt(Session("MainMnu_code"))
                Session("datamode") = Encr_decrData.Encrypt(Session("datamode"))
                url = String.Format("AccPJ.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Session("MainMnu_code"), Session("datamode"), Eid)
            End If
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub optPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPosted.CheckedChanged
        gridbind()
    End Sub

    Protected Sub optAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub optUnPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optUnPosted.CheckedChanged
        gridbind()
    End Sub

    Protected Sub gvGroup1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim lblGUID As New Label
        lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
        Dim hlview As New HyperLink
        hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
        If hlview IsNot Nothing And lblGUID IsNot Nothing Then
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Dim eid As String = Encr_decrData.Encrypt(lblGUID.Text)
            hlview.NavigateUrl = String.Format("AccPJ.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), ViewState("datamode"), eid)
        End If
    End Sub
    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblDocNo As Label
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    repSource = VoucherReports.PurchaseJournalVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PJ", lblDocNo.Text, Session("HideCC"))
                    Session("ReportSource") = repSource
                    '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                    ReportLoadSelection()
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
