Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class CurrencyMaster
    Inherits System.Web.UI.Page


    Dim MainMnu_code As String 
    Dim Encr_decrData As New Encryption64
 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "accNewCurrency.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")


            Page.Title = OASISConstants.Gemstitle
            GridViewCurrencyMaster.Attributes.Add("bordercolor", "#1b80b6")
            lblError.Text = ""
            If Request.QueryString("modified") <> "" Then
                ' lblModifyalert.Text = "The Currency is modified successfully"
                Dim encObj As New Encryption64
                'encObj.Decrypt(Request.QueryString("modified"))
                lblError.Text = getErrorMessage(encObj.Decrypt(Request.QueryString("modified").Replace(" ", "+")))
                h_SelectedId.Value = "1"
            End If
            gridbind()
        End If
    End Sub


    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ''''
            str_Sql = "SELECT  CM.CUR_ID," _
                & " CM.CUR_DESCR,  CM.CUR_DENOMINATION, " _
                & " ISNULL(ES.EXG_RATE, 0) AS EXG_RATE" _
                & " FROM CURRENCY_M AS CM" _
                & " LEFT OUTER JOIN EXGRATE_S AS ES " _
                & " ON CM.CUR_ID = ES.EXG_CUR_ID " _
                & " AND ES.EXG_BSU_ID = '" & Session("sBsuid") & "'" _
                & " WHERE (ES.EXG_TDATE IS NULL) " _
                & " ORDER BY CUR_ID"

            Dim ds As New DataSet
            Dim i As Integer
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewCurrencyMaster.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                If Request.QueryString("newid") <> "" And h_SelectedId.Value = "1" Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If (ds.Tables(0).Rows(i)("CUR_ID") = Request.QueryString("newid")) Then
                            GridViewCurrencyMaster.SelectedIndex = i
                            h_SelectedId.Value = "0"
                            End If
                    Next
                Else
                    GridViewCurrencyMaster.SelectedIndex = -1
                    End If

            Else
                End If
            GridViewCurrencyMaster.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
   


    Protected Sub GridViewCurrencyMaster_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewCurrencyMaster.RowDataBound
        Try
            Dim cmdCol As Integer = GridViewCurrencyMaster.Columns.Count - 1
            'hlChange lblCurrencyid
            Dim row As GridViewRow = e.Row
            Dim hlCButton As New HyperLink
            Dim hlCEdit As New HyperLink
            Dim lblCurid As New Label
            hlCButton = TryCast(row.FindControl("hlChange"), HyperLink)
            lblCurid = TryCast(row.FindControl("lblCurrencyid"), Label)
            hlCEdit = TryCast(row.FindControl("hlEdit"), HyperLink)

            If hlCButton IsNot Nothing Then
                If hlCButton.ID = "hlChange" Then
                    Dim i As New Encryption64
                    viewstate("datamode") = Encr_decrData.Encrypt("view")

                    hlCButton.NavigateUrl = "accExchangeRates.aspx?currid=" & i.Encrypt(lblCurid.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    'hlCButton.NavigateUrl = "ExchangeRates.aspx?currid=" & lblCurid.Text

                End If

            End If

            If hlCEdit IsNot Nothing And lblCurid IsNot Nothing Then
                'If hlCButton.ID = "hlChange" Then
                Dim i As New Encryption64
                viewstate("datamode") = Encr_decrData.Encrypt("view")
                hlCEdit.NavigateUrl = "accnewcurrency.aspx?editid=" & i.Encrypt(lblCurid.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")
                'hlCButton.NavigateUrl = "ExchangeRates.aspx?currid=" & lblCurid.Text

                'End If

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


     
 

     


End Class
