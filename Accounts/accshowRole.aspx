<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accshowRole.aspx.vb" Inherits="Accounts_accshowRole1" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Role Info</title>
    <base target="_self" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>
    <script>
  function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body onload="listen_window();">

    <form id="form1" runat="server">

        <table align="center" cellpadding="0" cellspacing="0" width="100%">
            <tr>

                <td align="center"
                    valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; color: #0000ff">
                        <tr>
                            <td>
                                <table align="center"
                                    width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <asp:GridView ID="gvEmpInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                DataKeyNames="ID" Width="100%" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Role ID">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>&nbsp;
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                Text="Role ID"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtcode" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchEmpId_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Role Description" ShowHeader="False">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Role Description"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchEmpName_Click" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                OnClick="LinkButton1_Click" Text='<%# Eval("E_Name") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <RowStyle CssClass="griditem" />
                                            </asp:GridView>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"
                    valign="middle">
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                    <input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server"
                        type="hidden" value="=" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
