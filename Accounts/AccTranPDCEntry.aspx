<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccTranPDCEntry.aspx.vb" Inherits="AccTranPDCEntry" Theme="General" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>

        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <asp:Label ID="lblHead2" CssClass="field-label" runat="server"></asp:Label>
                        </td>
                        <td align="left" width="30%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="50%" align="left" colspan="2">
                            <asp:HyperLink ID="hlAddnew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>

                        <td align="right" width="50%">
                            <asp:RadioButton ID="optUnPosted" runat="server" CssClass="field-label" GroupName="optControl" Text="Open"
                                Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="optPosted" runat="server" CssClass="field-label" GroupName="optControl" Text="Posted"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="optAll" runat="server" CssClass="field-label" GroupName="optControl" Text="All" AutoPostBack="True" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td>
                            <asp:GridView ID="gvGroup1" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%"
                                DataKeyNames="GUID" EmptyDataText="No Data" AllowPaging="True" PageSize="50"
                                OnRowDataBound="gvGroup1_RowDataBound" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Old Ref No<br />
                                            <asp:TextBox ID="txtOldDocNo" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnOldDocNo" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodewww" runat="server" Text='<%# Bind("OldDocNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date<br />
                                            <asp:TextBox ID="txtDocDate" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DocDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Bank A/C<br />
                                            <asp:TextBox ID="txtBankAC" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAC" runat="server" Text='<%# Bind("HeaderAccount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Narration<br />
                                            <asp:TextBox ID="txtNarrn" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarrn" runat="server" Text='<%# Bind("Narration") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Auto Voucher">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("VHH_bauto") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAmount" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label><br />
                                            <asp:LinkButton ID="lnkAmount" runat="server" CausesValidation="False" OnClientClick="return false;"
                                                Text='<%# AccountFunctions.Round(Container.DataItem("AMOUNT")) %>'></asp:LinkButton><ajaxToolkit:PopupControlExtender
                                                    ID="PopupControlExtender1" runat="server" DynamicContextKey='<%# Eval("DOCNO") %>'
                                                    DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                                    Position="Right" TargetControlID="lnkAmount">
                                                </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Currency<br />
                                            <asp:TextBox ID="txtCurrency" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>&nbsp;
                                                            
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Currency") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>



                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_8" runat="server" type="hidden" value="=" /><input
                                id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_SelectedId"
                                    runat="server" type="hidden" value="-1" /><input id="h_Selected_menu_6" runat="server"
                                        type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden"
                                            value="=" />
                <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">
                </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>
