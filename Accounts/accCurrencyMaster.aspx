<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accCurrencyMaster.aspx.vb" Inherits="CurrencyMaster" Title="Currency Master" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {

            if (document.getElementById("txtGroupcode").value == '') {
                alert("Kindly enter Group Code");
                return false;
            }
            if (document.getElementById("txtGroupname").value == '') {
                alert("Kindly enter Group Name");
                return false;
            }
            return true;
        }
        function hide(id) {
            var tdid = 'tbl_Message' + id;
            // alert(tdid);
            document.getElementById(tdid).style.display = 'none';
        }
        function help(id) {

            var NameandCode;
            var result;
            var url;
            url = 'ShowError.aspx?id=' + id;
            result = radopen(url, "pop_up");
            return false;
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Currency Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="AutoNumber1" border="0" align="center" style="border-collapse: collapse" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" /></td>
                    </tr>
                </table>
                <table align="center" border="0" style="width: 100%">
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td colspan="4" valign="top">
                                        <asp:GridView ID="GridViewCurrencyMaster" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            DataKeyNames="CUR_ID" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="GUID" HeaderText="GUID" SortExpression="GUID" Visible="False" />
                                                <asp:TemplateField HeaderText="Currency ID" SortExpression="CUR_ID">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblCurrencyid" runat="server" Text='<%# Eval("CUR_ID") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrencyid" runat="server" Text='<%# Bind("CUR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CUR_DESCR" HeaderText="Currency Description" SortExpression="CUR_DESCR">
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Currency Exch. Rate" SortExpression="CUR_EXGRATE">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CUR_EXGRATE") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("EXG_RATE") %>'></asp:Label>
                                                        <asp:HyperLink ID="hlChange" runat="server">(Change)</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CUR_DENOMINATION" HeaderText="Currency Denomination" SortExpression="CUR_DENOMINATION">
                                                    <ItemStyle />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# Eval("CUR_ID", "NewCurrency.aspx?editid={0}") %>'
                                                            Text="View"></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False" Visible="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                                            OnClientClick='return confirm("Are you sure to delete this?");' Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

