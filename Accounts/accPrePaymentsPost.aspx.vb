Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class accPrePaymentsPost
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim MainMnu_code As String = String.Empty
        Dim menu_rights As Integer
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            Dim CurBsUnit As String = Session("sBsuid")

            Dim USR_NAME As String = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'hardcode the menu code
            If USR_NAME = "" Or MainMnu_code <> "A200030" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                gvChild.Attributes.Add("bordercolor", "#1b80b6")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                Call gridbind()
                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"
            End If
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_9.Value.Split("__")
        getid9(str_Sid_img(2))

    End Sub


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvJournal.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
 

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            Dim lbmonth As LinkButton
            If chkPost IsNot Nothing Then
                lbmonth = TryCast(e.Row.FindControl("lbMonthWise"), LinkButton)
                lbmonth.OnClientClick = "javascript:monthDetails('vid=" & lblGUID.Text & "');return false;"
            End If
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If hlCEdit IsNot Nothing And lblGUID IsNot Nothing Then
                        hlview.NavigateUrl = "accPrepaymentsAddEdit.aspx?viewid=" & e.Row.Cells(1).Text
                        hlCEdit.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        h_FirstVoucher.Value = ""
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            PrintVoucher(h_FirstVoucher.Value)
        End If
        gridbind()
    End Sub


    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim str_Sql As String
                str_Sql = "select * FROM PREPAYMENTS_H where GUID='" & p_guid & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTPREPAYMENT", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@PRP_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("PRP_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@PRP_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("PRP_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@PRP_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("PRP_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    'Dim sqlpJHD_DOCTYPE As New SqlParameter("@PRP_DOCTYPE", SqlDbType.VarChar, 20)
                    'sqlpJHD_DOCTYPE.Value = "PP"
                    'cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@PRP_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("PRP_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    'Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                    'sqlpJHD_DOCDT.Value = txtHDocdate.Text & ""
                    'cmd.Parameters.Add(sqlpJHD_DOCDT)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.CommandTimeout = 0
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    If (iReturnvalue = 0) Then
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = p_guid & ""
                        End If
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                    End If
                    If (iReturnvalue = 0) Then
                        lblError.Text = "Pre Payment Voucher Successfully Posted"
                    Else
                        lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
                    End If
                End If
            Catch ex As Exception
                lblError.Text = UtilityObj.getErrorMessage("1000")
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Function


    Private Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_Docno As String = String.Empty
            Dim str_filter_Party As String = String.Empty
            Dim str_filter_year As String = String.Empty
            Dim str_filter_prepay As String = String.Empty
            Dim str_filter_Exp As String = String.Empty
            Dim str_filter_Date As String = String.Empty
            Dim str_filter_Currency As String = String.Empty
            Dim str_filter_Amt As String = String.Empty
            Dim str_filter_NoInst As String = String.Empty

            str_Sql = "Select * from(SELECT PREPAYMENTS_H.Guid as Guid,PREPAYMENTS_H.PRP_ID as DocNo,PREPAYMENTS_H.PRP_FYEAR as FYear, " & _
            " PREPAYMENTS_H.PRP_PARTY as P_Party, ACCOUNTS_M.ACT_NAME as PARTYNAME,PREPAYMENTS_H.PRP_PREPAYACC as P_PreAcc, " & _
            " ACCOUNTS_M_1.ACT_NAME as PREPAYACCNAME, PREPAYMENTS_H.PRP_EXPENSEACC as EXPENSEACC,  ACCOUNTS_M_2.ACT_NAME as EXPACCNAME, " & _
            " PREPAYMENTS_H.PRP_DOCDT as DOCDT, PREPAYMENTS_H.PRP_AMOUNT as P_Amt,PREPAYMENTS_H.PRP_CUR_ID as Cur," & _
            " PREPAYMENTS_H.Prp_NOOFINST as NOOFINST,PREPAYMENTS_H.PRP_bPosted as P_bPost,PREPAYMENTS_H.PRP_bDeleted," & _
            " PREPAYMENTS_H.PRP_SUB_ID,PREPAYMENTS_H.PRP_bPosted,PREPAYMENTS_H.PRP_BSU_ID FROM " & _
            " PREPAYMENTS_H left JOIN ACCOUNTS_M ON PREPAYMENTS_H.PRP_PARTY = ACCOUNTS_M.ACT_ID  INNER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 ON " & _
            " PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID  INNER JOIN ACCOUNTS_M AS ACCOUNTS_M_2 ON " & _
            " PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_2.ACT_ID)a  where a.PRP_bDeleted='false' AND a.PRP_bPosted='False' " & _
            " And   a.PRP_SUB_ID='" & Session("Sub_ID") & "' and a.PRP_BSU_ID='" & Session("sBsuid") & "' AND a.FYEAR = " & Session("F_YEAR")
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_search As String = String.Empty
            Dim str_txtDocNo As String = String.Empty
            Dim str_txtYear As String = String.Empty
            Dim str_txtCreditor As String = String.Empty
            Dim str_txtprepaid As String = String.Empty
            Dim str_txtExp As String = String.Empty
            Dim str_txtDate As String = String.Empty
            Dim str_txtCurrency As String = String.Empty
            Dim str_txtAmount As String = String.Empty
            Dim str_txtNoInst As String = String.Empty

            Dim ds As New DataSet
            If gvJournal.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                str_txtDocNo = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_Docno = " AND a.docno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Docno = " AND a.docno LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Docno = " AND a.docno LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '%" & txtSearch.Text & "'"
                End If 

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
                str_txtCreditor = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Party = "  AND  NOT isnull(a.PARTYNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtprepaid")

                str_txtprepaid = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_prepay = "  AND  NOT a.PREPAYACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtExp")

                str_txtExp = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Exp = " AND a.EXPACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Exp = "  AND  NOT a.EXPACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Exp = " AND a.EXPACCNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Exp = " AND a.EXPACCNAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Exp = " AND a.EXPACCNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Exp = " AND a.EXPACCNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")

                str_txtDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Date = " AND a.DOCDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Date = "  AND  NOT a.DOCDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Date = " AND a.DOCDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Date = " AND a.DOCDT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Date = " AND a.DOCDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Date = " AND a.DOCDT NOT LIKE '%" & txtSearch.Text & "'"
                End If 

                str_Sid_search = h_Selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")

                str_txtAmount = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Amt = " AND a.P_Amt LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Amt = "  AND  NOT a.P_Amt LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Amt = " AND a.P_Amt  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Amt = " AND a.P_Amt  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Amt = " AND a.P_Amt LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Amt = " AND a.P_Amt NOT LIKE '%" & txtSearch.Text & "'"
                End If 

                str_Sid_search = h_Selected_menu_9.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvJournal.HeaderRow.FindControl("txtNoInst")

                str_txtNoInst = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_NoInst = " AND a.NOOFINST LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_NoInst = "  AND  NOT a.NOOFINST LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_NoInst = " AND a.NOOFINST  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_NoInst = " AND a.NOOFINST  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_NoInst = " AND a.NOOFINST LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_NoInst = " AND a.NOOFINST NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_Docno & str_filter_Party & str_filter_year & str_filter_prepay & str_filter_Exp & str_filter_Date & str_filter_Currency & str_filter_Amt & str_filter_NoInst & " order by DOCDT desc, a.DOCNO DESC")

            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
                chkPrint.Visible = True
                gvJournal.SelectedIndex = p_sindex
            Else
                btnPost.Visible = False
                chkPrint.Visible = False
            End If
            gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = str_txtDocNo
            ' txtSearch = gvJournal.HeaderRow.FindControl("txtyear")
            'txtSearch.Text = str_txtYear
            txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
            txtSearch.Text = str_txtCreditor
            txtSearch = gvJournal.HeaderRow.FindControl("txtprepaid")
            txtSearch.Text = str_txtprepaid

            txtSearch = gvJournal.HeaderRow.FindControl("txtExp")
            txtSearch.Text = str_txtExp
            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = str_txtDate
            'txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
            'txtSearch.Text = str_txtCurrency
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = str_txtAmount

            txtSearch = gvJournal.HeaderRow.FindControl("txtNoInst")
            txtSearch.Text = str_txtNoInst

            'set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    
    Protected Sub lbSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True

            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label

                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                Dim i As Integer = sender.parent.parent.RowIndex

                gridbind(i)
                str_Sql = "select * FROM PREPAYMENTS_H where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT     VOUCHER_D_S.VDS_DOCTYPE, VOUCHER_D_S.VDS_DOCNO, ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, " _
                    & " CASE isnull(VOUCHER_D_S.VDS_CODE, '') WHEN '' THEN 'GENERAL' ELSE COSTCENTER_S.CCS_DESCR END AS GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR, VOUCHER_D_S.VDS_CODE,VOUCHER_D_S.vds_descr, VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM  PREPAYMENTS_H INNER JOIN " _
                    & " ACCOUNTS_M ON ACCOUNTS_M.ACT_ID = PREPAYMENTS_H.PRP_EXPENSEACC LEFT OUTER JOIN " _
                    & " VOUCHER_D_S ON PREPAYMENTS_H.PRP_SUB_ID = VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " PREPAYMENTS_H.PRP_BSU_ID = VOUCHER_D_S.VDS_BSU_ID AND PREPAYMENTS_H.PRP_FYEAR = VOUCHER_D_S.VDS_FYEAR AND " _
                    & " PREPAYMENTS_H.PRP_ID = VOUCHER_D_S.VDS_DOCNO and (VOUCHER_D_S.VDS_DOCTYPE = 'PP')  AND (VOUCHER_D_S.VDS_DOCNO = '" & ds.Tables(0).Rows(0)("PRP_ID") & "') AND  (VOUCHER_D_S.VDS_SUB_ID = '" & ds.Tables(0).Rows(0)("PRP_SUB_ID") & "') AND " _
                    & " (VOUCHER_D_S.VDS_BSU_ID = '" & ds.Tables(0).Rows(0)("PRP_BSU_ID") & "') AND (VOUCHER_D_S.VDS_FYEAR = '" & ds.Tables(0).Rows(0)("PRP_FYEAR") & "') " _
                    & "  LEFT OUTER JOIN  COSTCENTER_S ON VOUCHER_D_S.VDS_CCS_ID = COSTCENTER_S.CCS_ID " _
                    & " WHERE ( PREPAYMENTS_H.PRP_ID= '" & ds.Tables(0).Rows(0)("PRP_ID") & "')  ORDER BY GRPFIELD"

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                  
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)

                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    AddHandler helper.GroupSummary, AddressOf helper_GroupSummary
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub helper_GroupSummary(ByVal groupName As String, ByVal values As Object(), ByVal row As GridViewRow)
        Try
            row.Cells(0).HorizontalAlign = HorizontalAlign.Right
            row.Cells(0).Text = "Total Amount:-"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    

    Protected Sub lbview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim MainMnu_code As String = String.Empty
            Dim lblGuid As New Label
            Dim url As String
            Dim viewid As String
            lblGuid = TryCast(sender.FindControl("lblGuid"), Label)
            viewid = lblGuid.Text
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Accounts\accPrepaymentsAddEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub


    Protected Sub btnSearchDocNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchYear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchCreditor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchPrepaid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchExp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchCurrency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchAmount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchNoInst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Sub PrintVoucher(ByVal p_Guid As String)
        Dim repSource As New MyReportClass
        Dim str_Sql, strFilter As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        strFilter = "WHERE  GUID = '" & p_Guid & "'"
        str_Sql = "SELECT * FROM vw_OSA_PREPAYMENT " + strFilter
        Dim sql_DocNo As String = "SELECT DocNo FROM vw_OSA_PREPAYMENT " + strFilter
        Dim p_DocNo As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_DocNo)
        repSource = VoucherReports.PrePaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PP", p_DocNo, p_Guid, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"

    End Sub


End Class


