Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports Encryption64
Partial Class Accounts_AccPasswordReset
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            ViewState("datamode") = "add"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim str_query As String
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050006") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    str_query = " SELECT * FROM USERACCESS_MENU WHERE USM_USR_NAME='" & Session("sUsr_name") & "' AND USM_BSU_ID='" & CurBsUnit & "'"
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    If ds.Tables(0).Rows.Count >= 1 Then
                        ViewState("datamode") = "add"
                        BindBusinessUnit()
                        set_Menu_Img()
                        gridbind()
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        'disable the control buttons based on the rights
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Else
                        Accessrights()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        '  set_Menu_Img()
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim txtSearch As New TextBox
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim strCatFilter As String = ""

            Dim str_query As String = " select USR_ID,USR_NAME,USR_PASSWORD,convert(varchar,USR_EXPDATE,106)as USR_EXPDATE,USR_TRYCOUNT,USR_bDisable from USERS_M where usr_bsu_id='" & ddlBUnit.SelectedValue & "'"
            Dim strFilter As String = ""
            Dim enqSearch As String = ""
            Dim TcSearch As String = ""
            Dim strSearch As String
            Dim selectedDis As String = ""
            Dim strSidsearch As String()
            Dim ds As New DataSet
            Dim ddlgvDisable As New DropDownList
            'Dim selectedDisable As String = ""

            If gvUNITS.Rows.Count > 0 Then

                txtSearch = gvUNITS.HeaderRow.FindControl("txtusrnameSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("USR_NAME", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvUNITS.HeaderRow.FindControl("txtTCSearch")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = strFilter + GetSearchString("USR_TRYCOUNT", txtSearch.Text, strSearch)
                TcSearch = txtSearch.Text

                ddlgvDisable = gvUNITS.HeaderRow.FindControl("ddlgvDisable")
                If ddlgvDisable.Text <> "ALL" Then
                    strFilter += " and USR_bDisable=" + IIf(ddlgvDisable.Text = "True", "'true'", "'false'")
                    selectedDis = ddlgvDisable.Text
                End If
                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If
            End If
       

            str_query += strFilter & " order by USR_NAME, USR_TRYCOUNT desc "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer = 0
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim Encr_decrData As New Encryption64
                    Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("USR_PASSWORD").ToString.Replace(" ", "+"))

                Next

            End If



            gvUNITS.DataSource = ds


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'ds.Tables(0).Rows(0).Item(12) = "true"
                gvUNITS.DataBind()
                Dim columnCount As Integer = gvUNITS.Rows(0).Cells.Count
                gvUNITS.Rows(0).Cells.Clear()
                gvUNITS.Rows(0).Cells.Add(New TableCell)
                gvUNITS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvUNITS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvUNITS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvUNITS.DataBind()
            End If

            If ddlBUnit.SelectedValue <> "" Then
                strCatFilter = " "
            End If

            txtSearch = New TextBox
            txtSearch = gvUNITS.HeaderRow.FindControl("txtusrnameSearch")
            txtSearch.Text = enqSearch

            txtSearch = New TextBox
            txtSearch = gvUNITS.HeaderRow.FindControl("txtTCSearch")
            txtSearch.Text = TcSearch

            ddlgvDisable = gvUNITS.HeaderRow.FindControl("ddlgvDisable")
            ddlgvDisable.Text = selectedDis

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
        ' set_Menu_Img()

    End Sub
    Private Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAME"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()


        If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub ddlgvDisable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gvUNITS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUNITS.PageIndexChanging
        Try
            gvUNITS.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim flag As Integer = 0
            For Each gr As GridViewRow In gvUNITS.Rows
                If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                    flag = flag + 1
                    Dim strSections As String = ""
                    Activate()
                    gridbind()
                End If
                If flag = 0 Then
                    lblError.Text = "Please select a Row"
                End If
            Next
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub Activate()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        Dim USR_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                        str_query = "exec resetpassword '" & USR_ID & "'"
                        Dim stat As Integer
                        stat = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If
                Next
                transaction.Commit()
                lblError.Text = "Account Activated Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    'Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim flag As Integer = 0
    '        For Each gr As GridViewRow In gvUNITS.Rows
    '            If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
    '                flag = flag + 1
    '                Dim strSections As String = ""
    '                ResetData()
    '                gridbind()
    '            End If
    '            If flag = 0 Then
    '                lblError.Text = "Please select the row"
    '            End If
    '        Next
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed"
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub
    Sub ResetData()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pass As String = Encr_decrData.Encrypt("gem2009")
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        Dim USR_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                        str_query = "exec passwordreset '" & USR_ID & "', '" & pass & "'"
                        Dim stat As Integer
                        stat = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If
                Next
                transaction.Commit()
                lblError.Text = "Password Reseted Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    Sub Accessrights()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = " SELECT * FROM USERACCESS_MENU WHERE USM_USR_NAME='" & Session("sUsr_name") & "' AND USM_BSU_ID='" & ddlBUnit.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            btnActivate.Visible = True
            'btnReset.Visible = True
        Else
            btnActivate.Visible = False
            'btnReset.Visible = False
            ddlBUnit.Visible = False
            tabmain.Visible = False
            lblError.Text = "Access denied"
        End If
    End Sub

    Protected Sub gvUNITS_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub btnusrname_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnTC_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Protected Sub gvUNITS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim pass As String = Encr_decrData.Decrypt(DataBinder.Eval(e.Row.DataItem, "USR_PASSWORD").ToString.Replace(" ", "+"))

                ''e.Row.Cells(2).Text = pass
                DirectCast(e.Row.Cells(2).FindControl("lblpasswrd"), Label).Text = pass

            End If
            
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub
End Class