Imports System.Data.DataSet
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Accounts_ShowOnlineSettlement
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Set_Components()
            Page.Title = OASISConstants.Gemstitle
            btnSave.Focus()
        End If
        If h_Close.Value <> "true" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        Else 
        End If
    End Sub



    Private Function Get_Doc() As String
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet

            lstrSQL = "SELECT A.*,isNUll(VHH_CHQ_PDC_ACT_ID,'') as ProvAccount,isNUll(VHH_PROV_ACT_ID,'') as ProvAccount2,REPLACE(CONVERT(VARCHAR(11), A.VHH_DOCDT, 106), ' ', '/') as DocDate ,isNULL(VHH_Amount,0) as Amount,isNULL(A.VHH_REFNO,'') as OldDocNo FROM VOUCHER_H A  WHERE A.GUID='" & Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")) & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)


            If ds.Tables(0).Rows.Count > 0 Then
                ''   --- Initialize The Grid With The Data From The Detail Table
                Return ds.Tables(0).Rows(0)("VHH_DOCNO")
            Else
                lblError.Text = "Record Not Found !!!"
            End If
            Return ""
        Catch ex As Exception
            lblError.Text = "Record Not Found !!! "
            Return ""
        End Try
    End Function



    Public Function returndate(ByVal p_date As String) As String
        Try 'verify date is valid
            Dim dt As Date = CDate(p_date)
            Dim dt_1900 As Date = CDate("01/01/1900")
            If dt = dt_1900 Then
                Return ""
            Else
                Return Format(dt, "dd/MMM/yyyy")
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function



  
    Sub gridbind_settle(ByVal p_actid As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql, str_filter_dt As String


        If IsDate(Request.QueryString("dt")) Then
            str_filter_dt = "AND TRN_DOCDT<='" & Request.QueryString("dt") & "' "
        Else
            str_filter_dt = ""
        End If

        str_Sql = "SELECT " _
                      & " TRN_AMOUNT -ISNULL(A.AMT,0) -TRN_ALLOCAMT  AS BALANCE," _
                      & " ISNULL(A.AMT,0)+TRN_ALLOCAMT AS TRN_ALLOCAMT,*" _
                      & " FROM TRANHDR_D LEFT OUTER JOIN " _
                      & " (SELECT SUM(SOL_SETTLEDAMT) AS AMT , " _
                      & " SOL_REFDOCTYPE, SOL_REFDOCNO, " _
                      & " SOL_REFLINEID, SOL_ACCTCODE" _
                      & " FROM SETTLEONLINE_D " _
                      & " WHERE SOL_ACCTCODE='" & p_actid & "' AND (SOL_DOCNO<>'" & Request.QueryString("docno") & "')" _
                      & " AND SOL_BSU_ID='" & Session("sBsuid") & "'" _
                      & " GROUP BY  SOL_REFDOCTYPE, SOL_REFDOCNO," _
                      & " SOL_REFLINEID, SOL_ACCTCODE) A" _
                      & " ON TRN_DOCNO= A.SOL_REFDOCNO" _
                      & " AND TRN_DOCTYPE= A.SOL_REFDOCTYPE " _
                      & " AND TRN_LINEID=SOL_REFLINEID" _
                      & " WHERE TRN_ACT_ID='" & p_actid & "'" _
                      & " AND TRN_BSU_ID='" & Session("sBsuid") & "' " _
                      & " AND (TRN_SUB_ID = '" & Session("Sub_ID") & "') " _
                      & " AND  TRN_DRCR = 'CR' " _
                      & " AND (TRN_AMOUNT -TRN_ALLOCAMT-ISNULL(A.AMT,0)) >0 " & str_filter_dt

        Dim ds As New DataSet
        ds.Tables.Clear()
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        gvSettle.DataSource = ds.Tables(0)

        gvSettle.DataBind()

        Try
            For Each gvr As GridViewRow In gvSettle.Rows
                Dim txtPayment As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)

                If txtPayment IsNot Nothing Then
                    ClientScript.RegisterArrayDeclaration("TextBoxAllocated", String.Concat("'", txtPayment.ClientID, "'"))
                    'txtRCDate()
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub Set_Components()
        Try
            gridbind_settle(Request.QueryString("actid"))
        Catch ex As Exception
        End Try
    End Sub



    Private Function check_in_ds(ByRef p_dt As DataTable, ByVal p_credit As String, _
    ByVal p_credit_count As Integer, ByVal p_debit As String, ByVal p_debit_count As Integer, _
    ByVal p_searchcontent As String, _
    ByVal p_columnno As Integer) As Boolean
        Try
            p_columnno = p_columnno - 1
            p_debit_count = p_debit_count - 1
            p_credit_count = p_credit_count - 1
            For i As Integer = 0 To p_dt.Rows.Count 'start finding
                If p_dt.Rows(i)(p_columnno).ToString.ToUpper.IndexOf(p_searchcontent.ToUpper) >= 0 And checkempty(p_debit) = checkempty(p_dt.Rows(i)(p_debit_count)) And checkempty(p_credit) = checkempty(p_dt.Rows(i)(p_credit_count)) Then
                    p_dt.Rows(i).Delete()
                    Return True

                End If
            Next
            Return False
        Catch ex As Exception

        End Try

    End Function



    Private Function checkempty(ByVal p_input As Object) As Double
        Try
            If p_input Is System.DBNull.Value Then
                Return 0
            Else
                Return CDbl(p_input)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function


    Private Function checkdate(ByVal p_input As String) As Date
        Try
            If p_input Is System.DBNull.Value Then
                Return CDate("01/jan/1900")
            Else
                Return CDate(p_input)
            End If
        Catch ex As Exception
            Return CDate("01/jan/1900")
        End Try
    End Function



    Protected Sub btnReconcile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Preview_Settle() = True Then
            Save_Offline_Settlement()
            Dim str As String = "window.returnvalue=document.getElementById('" & txtSAlloted.ClientID & "').value;alert(window.returnvalue);window.close();"
            h_Close.Value = "true"
            'Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write("window.returnValue=document.getElementById('" & txtSAlloted.ClientID & "').value;window.close();")
            'Response.Write("} </script>" & vbCrLf)

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & txtSAlloted.Text & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & txtSAlloted.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

         Else
            lblError.Text = "Check allocation"
        End If

    End Sub



    Private Sub Save_Offline_Settlement(Optional ByVal p_temp As Boolean = False)
        Try
            Dim str_err As String = ""
            Dim str_remarks As String = ""
            '''''''
            Dim il As Integer = 0

            While il < Session("dtSettle").Rows.Count
                If Session("dtSettle").rows(il)("Accountid") = Request.QueryString("actid") And _
                   Session("dtSettle").rows(il)("Id") = Request.QueryString("lineid") Then

                    Session("dtSettle").rows(il).delete()
                Else
                    il = il + 1
                End If
            End While
            For Each gvr As GridViewRow In gvSettle.Rows
                Dim txtAllocated As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)
                Dim lblRGuid As Label = CType(gvr.FindControl("lblGuid"), Label)

                If txtAllocated IsNot Nothing And CDbl(txtAllocated.Text) > 0 Then
                    Dim dr As DataRow
                    dr = Session("dtSettle").newrow
                    dr("Id") = Request.QueryString("lineid") 'lineid
                    dr("Accountid") = Request.QueryString("actid")
                    dr("jnlid") = lblRGuid.Text
                    dr("Amount") = AccountFunctions.Round(txtAllocated.Text)
                    Session("dtSettle").rows.add(dr)
                End If
            Next


        Catch ex As Exception
            Errorlog(ex.Message)
        Finally

        End Try
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Preview_Settle()
    End Sub

    Private Function Preview_Settle() As Boolean
        Dim valid As Boolean = True
        Dim DTOTAL As Double = 0
        Dim lblGUID As New Label
        Dim ldblAllocatedSameLine As Decimal
        Dim txtSAllocated As New TextBox

        For Each gvr As GridViewRow In gvSettle.Rows
            ldblAllocatedSameLine = 0
            Dim txtAllocated As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)
            lblGUID = TryCast(gvr.FindControl("lblGUID"), Label)
            If txtAllocated IsNot Nothing Then
                If txtAllocated.Text = "" Then
                    txtAllocated.Text = "0"
                End If
                Try
                    ''''''
                    For il As Integer = 0 To Session("dtSettle").Rows.count - 1
                        'PUTTING AMT INTO TEXTBOX
                        If lblGUID.Text = Session("dtSettle").rows(il)("jnlid") _
                        And Request.QueryString("lineid") = Session("dtSettle").rows(il)("id") Then
                            ldblAllocatedSameLine = Session("dtSettle").rows(il)("Amount")
                        End If
                        'REDUCING AMT

                    Next
                    ''''''
                    If (Convert.ToDecimal(gvr.Cells(7).Text) + ldblAllocatedSameLine) < CDbl(txtAllocated.Text) Then
                        gvr.CssClass = "griditem_hilight"
                        valid = False
                    Else
                        gvr.CssClass = "griditem"
                    End If

                    DTOTAL = DTOTAL + AccountFunctions.Round(txtAllocated.Text)
                Catch ex As Exception
                    gvr.CssClass = "griditem_hilight"
                    'txtAllocated.Text = "0"
                    valid = False
                End Try

            End If
        Next
        txtSAlloted.Text = DTOTAL
        If valid = False Then
            lblError.Text = "Check the Allocations"
        End If
        Preview_Settle = valid
    End Function

    'Protected Sub txtSAllocated_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim TXT As New TextBox
    '    TXT = sender
    '    TXT.Attributes.Add("onfocus", "this.select();")
    '    TXT.Attributes.Add("onBlur", "OnChange();")

    '    'TXT.Attributes.Add("onBlur", "if (this.value=='') this.value='0';")
    '    'OnChange()
    'End Sub

    Protected Sub gvSettle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSettle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim lblGUID As New Label
                Dim txtSAllocated As New TextBox
                lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
                txtSAllocated = TryCast(e.Row.FindControl("txtSAllocated"), TextBox)
                If lblGUID IsNot Nothing Then
                    For il As Integer = 0 To Session("dtSettle").Rows.count
                        'PUTTING AMT INTO TEXTBOX
                        If lblGUID.Text = Session("dtSettle").rows(il)("jnlid") _
                        And Request.QueryString("lineid") = Session("dtSettle").rows(il)("id") Then
                            txtSAllocated.Text = Session("dtSettle").rows(il)("Amount")
                        End If
                        'REDUCING AMT
                        If lblGUID.Text = Session("dtSettle").rows(il)("jnlid") Then
                            e.Row.Cells(7).Text = Convert.ToDecimal(e.Row.Cells(7).Text) - Session("dtSettle").rows(il)("Amount")
                        End If
                    Next
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If

    End Sub

    
End Class
