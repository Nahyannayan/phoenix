Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class iuJournalvoucher
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        If Page.IsPostBack = False Then
            usrCostCenter1.TotalAmountControlName = "txtDAmount"
            Page.Title = OASISConstants.Gemstitle
            txtBSUCR.Attributes.Add("ReadOnly", "ReadOnly")
            txtBSUDR.Attributes.Add("ReadOnly", "ReadOnly")
            txtDAccountName.Attributes.Add("ReadOnly", "ReadOnly")
            'txtHExchRateCR.Attributes.Add("ReadOnly", "ReadOnly")
            txtHGroupRate.Attributes.Add("ReadOnly", "ReadOnly")
            ViewState("datamode") = "none"
            Session("CHECKLAST") = 0
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sBsuid") <> Nothing Then
                txtBSUCR.Text = GetBSUName(Session("sBsuid"))
                h_BSUID_CR.Value = Session("sBsuid")
            End If

            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If

            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            Session("gintGridLine") = 1
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "A150025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                bind_groupPolicy()
                initialize_components()
                bind_Currency()

                If Request.QueryString("viewid") <> "" Then
                    set_viewdata()
                    setViewData()
                    setModifyHeader(Request.QueryString("viewid"))

                    MakeReadOnly(True)

                    btnEdit.Visible = True
                Else
                    ResetViewData()
                    btnUpload.Visible = True
                    UploadDocPhoto.Visible = True
                End If
                gridbind()
                If Request.QueryString("invalidedit") = "1" Then
                    lblError.Text = "Invalid Editid"
                    Exit Sub
                End If
                If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                    txtHDocno.Text = AccountFunctions.GetNextDocId("IJV", "", CType(txtdocDate.Text, Date).Month, CType(txtdocDate.Text, Date).Year)
                    'txtHDocno.Text = AccountFunctions.GetNextDocId("IJV", "", Session("EntryDate").Month, Session("EntryDate").Year)
                    'getnextdocid()
                End If
            End If
            checkFC("CR")
            checkFC("DR")
            'call the  UtilityObj.beforeLoopingControls class to track the initial stage of the page
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
    End Sub

    Private Sub MakeReadOnly(ByVal read As Boolean)
        If read Then
            imgSelectDocDate.Enabled = False
            imgBSUSel.Enabled = False
            txtdocDate.ReadOnly = True
            chkMirrorEntry.Enabled = False
            DDCurrency.Enabled = False
            txtDifferAmt.ReadOnly = True
            txtHGroupRate.ReadOnly = True
            txtHExchRateCR.ReadOnly = True
            txtHExchRateDR.ReadOnly = True
            txtHNarration.ReadOnly = True
            txtDifferAmt.Enabled = False
        Else
            imgSelectDocDate.Enabled = True
            imgBSUSel.Enabled = True
            txtdocDate.ReadOnly = False
            chkMirrorEntry.Enabled = True
            DDCurrency.Enabled = True
            txtDifferAmt.ReadOnly = True
            txtHGroupRate.ReadOnly = False
            txtHExchRateCR.ReadOnly = False
            txtHExchRateDR.ReadOnly = False
            txtHNarration.ReadOnly = False
            txtDifferAmt.Enabled = True
        End If
    End Sub

    Sub initialize_components()
        txtdocDate.Text = GetDiplayDate()
        txtdocDate.Attributes.Add("onBlur", "checkdate(this)")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
        Session("dtJournal") = DataTables.CreateDataTable_IJV()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        ViewState("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEdit.Visible = False
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select *,isnull(case when IJH_CR_BSU_ID='" & Session("SBSUID") & "' then [IJH_bPOSTEDCRBSU] else [IJH_bPOSTEDDRBSU] end,0) IJH_BpostedStatus FROM IJOURNAL_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                bind_Currency()
                set_default_currency(ds.Tables(0).Rows(0)("IJH_CUR_ID"))
                setModifyDetails(p_Modifyid)
                h_Guid.Value = p_Modifyid
                txtdocDate.Text = String.Format("{0:dd/MMM/yyyy}", ds.Tables(0).Rows(0)("IJH_DOCDT"))
                txtHNarration.Text = ds.Tables(0).Rows(0)("IJH_NARRATION")
                txtHDocno.Text = ds.Tables(0).Rows(0)("IJH_DOCNO")
                txtBSUDR.Text = GetBSUName(ds.Tables(0).Rows(0)("IJH_DR_BSU_ID"))
                h_BSUID_DR.Value = ds.Tables(0).Rows(0)("IJH_DR_BSU_ID")
                txtBSUCR.Text = GetBSUName(ds.Tables(0).Rows(0)("IJH_CR_BSU_ID"))
                h_BSUID_CR.Value = ds.Tables(0).Rows(0)("IJH_CR_BSU_ID")

                h_BSUID_DR_Old.Value = ds.Tables(0).Rows(0)("IJH_DR_BSU_ID")
                H_Bposted.Value = ds.Tables(0).Rows(0)("IJH_BpostedStatus")
                h_BSUID_CR_Old.Value = ds.Tables(0).Rows(0)("IJH_CR_BSU_ID")
                txtHExchRateDR.Text = ds.Tables(0).Rows(0)("IJH_DREXGRATE")
                txtHExchRateCR.Text = ds.Tables(0).Rows(0)("IJH_EXGRATE2")
                '''''''''
                'IJH_bPOSTEDCRBSU
                'IJH_bPOSTEDDRBSU
                ViewState("disableheader") = "false"
                If Session("sBsuid") = ds.Tables(0).Rows(0)("IJH_ISS_BSU_ID") Then
                    If ds.Tables(0).Rows(0)("IJH_bPOSTEDCRBSU") = True And ds.Tables(0).Rows(0)("IJH_bPOSTEDDRBSU") = True Then
                        ViewState("canEdit") = "none"
                        ViewState("disableheader") = "true"
                    ElseIf ds.Tables(0).Rows(0)("IJH_bPOSTEDCRBSU") = True And ds.Tables(0).Rows(0)("IJH_bPOSTEDDRBSU") = False Then
                        ViewState("canEdit") = "debit"
                        ViewState("disableheader") = "true"
                    ElseIf ds.Tables(0).Rows(0)("IJH_bPOSTEDCRBSU") = False And ds.Tables(0).Rows(0)("IJH_bPOSTEDDRBSU") = False Then
                        ViewState("canEdit") = "all"
                    ElseIf ds.Tables(0).Rows(0)("IJH_bPOSTEDCRBSU") = False And ds.Tables(0).Rows(0)("IJH_bPOSTEDDRBSU") = True Then
                        ViewState("canEdit") = "credit"
                        ViewState("disableheader") = "true"
                    End If
                Else
                    ViewState("disableheader") = "true"
                    setHeader()
                    If Session("sBsuid") = ds.Tables(0).Rows(0)("IJH_CR_BSU_ID") And ds.Tables(0).Rows(0)("IJH_bPOSTEDCRBSU") = False Then
                        ViewState("canEdit") = "credit"
                    ElseIf Session("sBsuid") = ds.Tables(0).Rows(0)("IJH_DR_BSU_ID") And ds.Tables(0).Rows(0)("IJH_bPOSTEDDRBSU") = False Then
                        ViewState("canEdit") = "debit"
                    Else
                        ViewState("canEdit") = "none"
                    End If
                End If

                '''''''''''
                If ds.Tables(0).Rows(0)("IJH_bMIRROR") = True Then
                    chkMirrorEntry.Checked = True
                    txtDifferAmt.Text = Decimal.Round(ds.Tables(0).Rows(0)("IJH_DIFFAMT"), Session("BSU_ROUNDOFF"))
                Else
                    chkMirrorEntry.Checked = False
                    txtDifferAmt.Text = ""
                    txtDifferAmt.Enabled = False
                End If
                Dim iji_filename As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "select IJI_FILENAME+';'+IJI_CONTENTTYPE+';'+IJI_NAME from IJV_I INNER JOIN IJOURNAL_H on IJH_DOCNO=IJI_DOCNO where IJI_DOCNO='" & txtHDocno.Text & "' and (IJH_CR_BSU_ID=IJI_BSU_ID or IJH_DR_BSU_ID=IJI_BSU_ID)")
                If Not iji_filename Is Nothing Then
                    hdnFileName.Value = iji_filename.Split(";")(0)
                    hdnContentType.Value = iji_filename.Split(";")(1)
                    btnDocumentLink.Text = iji_filename.Split(";")(2)
                    btnDocumentLink.Visible = True
                    btnDocumentDelete.Visible = True
                Else
                    btnUpload.Visible = True
                    UploadDocPhoto.Visible = True
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setHeader()
        chkMirrorEntry.Enabled = False
        imgCRBSUNIT.Enabled = False
        imgSelectDocDate.Enabled = False
        DDCurrency.Enabled = False
        txtdocDate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        'DETAIL_ACT_ID.Attributes.Add("readonly", "readonly")
        'txtHExchRateDR.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub setViewData()
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")
        gvJournal.Columns(6).Visible = False
        gvJournal.Columns(7).Visible = False
        btnAccount.Enabled = False
        imgSelectDocDate.Enabled = False
        DDCurrency.Enabled = False
        txtdocDate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub ResetViewData()
        'tbl_Details.Visible = True
        tbl_Details.Attributes.Add("style", "display:table")

        gvJournal.Columns(6).Visible = True
        gvJournal.Columns(7).Visible = True
        btnAccount.Enabled = True
        imgSelectDocDate.Enabled = True
        DDCurrency.Enabled = True
        txtdocDate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        'imgCRBSUNIT.Attributes.Remove("readonly")
        'imgBSUSel.Attributes.Remove("readonly")
        imgBSUSel.Enabled = True
        imgCRBSUNIT.Enabled = True

        tr_SaveButtons.Visible = True
        btnUpdate.Visible = False
        'tbl_Details.Visible = True
        tbl_Details.Attributes.Add("style", "display:table")
    End Sub

    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM IJOURNAL_D where IJL_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'and IJL_BDELETED='False' "

            str_Sql = "SELECT IJD.GUID, IJH.IJH_CR_BSU_ID, " _
            & " IJH.IJH_DR_BSU_ID, IJH.IJH_DOCNO, " _
            & " IJH.IJH_ISS_BSU_ID, IJD.IJL_ID,  " _
            & " IJD.IJL_FYEAR, IJD.IJL_DOCTYPE,  " _
            & " IJD.IJL_DOCNO, IJD.IJL_DEBIT,  " _
            & " IJD.IJL_ACT_ID, IJD.IJL_CREDIT,  " _
            & " IJD.IJL_SLNO, IJD.IJL_NARRATION " _
            & " FROM IJOURNAL_H AS IJH  " _
            & " INNER JOIN IJOURNAL_D AS IJD  " _
            & " ON IJH.IJH_DOCNO = IJD.IJL_DOCNO  " _
            & " AND IJH.IJH_DOCTYPE = IJD.IJL_DOCTYPE " _
            & " WHERE  (IJD.IJL_DOCNO = '" & ViewState("str_editData").Split("|")(0) & "') " _
            & " AND  IJD.IJL_BDELETED='False'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("IJL_ACT_ID"))
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("IJL_SLNO")

                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("IJL_ACT_ID")
                    rDt("Accountname") = str_actname_cost_mand.Split("|")(0)
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("IJL_NARRATION")

                    rDt("Credit") = ds.Tables(0).Rows(iIndex)("IJL_CREDIT")
                    rDt("Debit") = ds.Tables(0).Rows(iIndex)("IJL_DEBIT")

                    rDt("CostCenter") = str_actname_cost_mand.Split("|")(1)
                    rDt("Required") = Convert.ToBoolean(str_actname_cost_mand.Split("|")(2))
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                    h_NextLine.Value = Session("gintGridLine")
                    Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(Session("Sub_ID"), ds.Tables(0).Rows(iIndex)("IJH_ISS_BSU_ID"), _
                                            "IJV", ViewState("str_editData").Split("|")(0))

                    Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(Session("Sub_ID"), ds.Tables(0).Rows(iIndex)("IJH_ISS_BSU_ID"), _
                    "IJV", ViewState("str_editData").Split("|")(0))
                Next
            Else

            End If
            ViewState("idJournal") = ViewState("idJournal") + 1
            Session("gintGridLine") = ViewState("idJournal")
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function getAccountname(ByVal p_accountid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & p_accountid & "' "

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accountid & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else

            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function

    Sub bind_groupPolicy()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bind_Currency(Optional ByVal p_bsuid As String = "")
        Try '= Session("sBSUID")
            If p_bsuid = "" Then
                p_bsuid = Session("sBSUID")
            End If
            Dim dt As DataTable = MasterFunctions.GetExchangeRates(txtdocDate.Text, p_bsuid, Session("BSU_CURRENCY"))
            DDCurrency.DataSource = dt 'ds.Tables(0)
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()

            If dt.Rows.Count > 0 Then
                If set_default_currency(Session("BSU_CURRENCY")) <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRateCR.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHExchRateDR.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            Else
            End If
            'getnextdocid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency(ByVal currency As String) As Boolean
        Try
            Dim i As Integer = 0
            For Each item As ListItem In DDCurrency.Items
                If String.Compare(item.Text, currency, True) = 0 Then
                    DDCurrency.SelectedIndex = i
                    txtHExchRateCR.Text = item.Value.Split("__")(0).Trim
                    txtHExchRateDR.Text = item.Value.Split("__")(0).Trim
                    txtHGroupRate.Text = item.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
                i += 1
            Next
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub txtdocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = String.Format("{0:dd/MMM/yyyy}", strfDate)
        End If
        bind_Currency()
        getnextdocid()
    End Sub

    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("IJV", "", CType(txtdocDate.Text, Date).Month, CType(txtdocDate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    'btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                'btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSelectDocDate.Click
        bind_Currency()
        getnextdocid()
        txtHNarration.Focus()
    End Sub

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRateCR.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHExchRateDR.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHGroupRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdds.Click
        If ViewState("canEdit") = "none" Then
            lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
            Exit Sub
        End If
        txtDAccountName.Text = AccountFunctions.check_accountid(DETAIL_ACT_ID.Text, Session("sBsuid"))

        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        Try
            Dim rDt As DataRow
            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = CDbl(txtDAmount.Text.Trim)
            If dCrorDb <> 0 Then
                rDt = Session("dtJournal").NewRow
                rDt("Id") = Session("gintGridLine") 'ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = DETAIL_ACT_ID.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                If dCrorDb > 0 Then
                    rDt("Credit") = "0"
                    rDt("Debit") = dCrorDb
                Else
                    rDt("Credit") = dCrorDb * -1
                    rDt("Debit") = "0"
                End If

                rDt("GUID") = System.DBNull.Value
                If ViewState("datamode") = "edit" Then
                    If dCrorDb < 0 Then
                        If ViewState("canEdit") = "credit" Or ViewState("canEdit") = "all" Then
                        Else
                            lblError.Text = "Cannot Edit/Add credit details. The journal is either posted or belong to other business unit"
                            Exit Sub
                        End If
                    Else
                        If ViewState("canEdit") = "debit" Or ViewState("canEdit") = "all" Then
                        Else
                            lblError.Text = "Cannot Edit/Add debit details. The journal is either posted or belong to other business unit"
                            Exit Sub
                        End If
                    End If
                End If

                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                        Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                         Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                         Session("dtJournal").Rows(i)("Credit") = rDt("Credit") And _
                         Session("dtJournal").Rows(i)("Debit") = rDt("Debit") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        Exit Sub
                    End If
                Next
                If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                    CostCenterFunctions.AddCostCenter(Session("gintGridLine"), Session("sBsuid"), _
                                      Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                     Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                     Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                End If
                Session("gintGridLine") = Session("gintGridLine") + 1
                h_NextLine.Value = Session("gintGridLine")
                Session("dtJournal").Rows.Add(rDt)
            End If
            gridbind()
            Clear_Details()
            If gvJournal.Rows.Count = 1 Then
                txtDAmount.Text = AccountFunctions.Round(dCrorDb) * -1
            End If
            btnAccount.Focus()
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try

    End Sub

    Private Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_IJV()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + Session("dtJournal").Rows(i)("Credit")
                        Else
                            dDebit = dDebit + Session("dtJournal").Rows(i)("Debit")
                        End If
                        ' dTotAmount = dTotAmount + session("dtCostChild").Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = AccountFunctions.Round(dDebit)
            txtTotalCredit.Text = AccountFunctions.Round(dCredit)
            Dim difAmt As Double
            If txtDifferAmt.Text <> String.Empty And txtDifferAmt.Text <> "" Then
                difAmt = Convert.ToDouble(txtDifferAmt.Text)
            End If
            If difAmt > 0 Then
                If (dDebit + difAmt) <> dCredit Then
                    'btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Else
                If (dCredit - difAmt) <> dDebit Then
                    'btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtJournal").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(i)("id") & "" = p_id Then
                    Return Session("dtJournal").Rows(i)("Costcenter")
                End If
            Next
        End If
        Return ""
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblReqd As New Label
            Dim lblid As New Label
            Dim imgalloc As New Image
            Dim btnAlloca As New LinkButton

            Dim Label1 As New Label
            Dim Label2 As New Label
            Label1 = e.Row.FindControl("Label1")
            Label2 = e.Row.FindControl("Label2")

            lblReqd = e.Row.FindControl("lblRequired")
            lblid = e.Row.FindControl("lblId")
            imgalloc = e.Row.FindControl("imgAllocate")
            btnAlloca = e.Row.FindControl("btnAlloca")
            If btnAlloca IsNot Nothing Then
                Dim dAmt As Double
                If Label1.Text <> "" Then
                    If CDbl(Label1.Text) > 0 Then
                        dAmt = CDbl(Label1.Text)
                    Else
                        dAmt = CDbl(Label2.Text)
                    End If
                End If
                'btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "" & "&sid=" & e.Row.Cells(8).Text & "')"
                btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.Text) & "');return false;"
            End If
            If lblReqd IsNot Nothing Then
                If lblReqd.Text = "1" Then
                    e.Row.Cells(gvJournal.Columns.Count - 1).BackColor = Drawing.Color.Pink
                    imgalloc.ImageUrl = "images\tick.gif"
                Else
                    imgalloc.ImageUrl = "images\cross.gif"
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        If ViewState("canEdit") = "none" Then
            lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
            Exit Sub
        End If
        Try
            If btnAdds.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                '        Dim lblGrpCode As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        '''''''''
                        If ViewState("datamode") = "edit" Then
                            If Session("dtJournal").Rows(iRemove)("Credit") > 0 Then
                                If ViewState("canEdit") = "credit" Or ViewState("canEdit") = "all" Then
                                Else
                                    lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
                                    Exit Sub
                                End If
                            Else
                                If ViewState("canEdit") = "debit" Or ViewState("canEdit") = "all" Then
                                Else
                                    lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
                                    Exit Sub
                                End If
                            End If
                        End If
                        '''''''''''''
                        If Request.QueryString("viewid") = "" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        'session("dtCostChild").Rows(iRemove).Delete()
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                'gvTransaction.EditIndex = -1
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        If ViewState("canEdit") = "none" Then
            lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
            Exit Sub
        End If
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        Session("gintEditLine") = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then

                If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                    If ViewState("datamode") = "edit" Then
                        If ViewState("canEdit") = "credit" Or ViewState("canEdit") = "all" Then
                            txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Credit")) * -1
                        Else
                            lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
                            Exit Sub
                        End If
                    Else
                        txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Credit")) * -1
                    End If
                Else
                    If ViewState("datamode") = "edit" Then
                        If ViewState("canEdit") = "debit" Or ViewState("canEdit") = "all" Then
                            txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Debit"))
                        Else
                            lblError.Text = "Cannot Edit. The journal is either posted or belong to other business unit"
                            Exit Sub
                        End If
                    Else
                        txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Debit"))
                    End If
                End If

                If CDbl(txtDAmount.Text.Trim) > 0 And h_BSUID_DR.Value <> Session("sBsuid") Then
                    'tr_Allocation.Visible = False
                    tr_Allocation.Attributes.Add("style", "display:none")
                ElseIf CDbl(txtDAmount.Text.Trim) < 0 And h_BSUID_CR.Value <> Session("sBsuid") Then
                    'tr_Allocation.Visible = False
                    tr_Allocation.Attributes.Add("style", "display:none")
                Else
                    'tr_Allocation.Visible = True
                    tr_Allocation.Attributes.Add("style", "display:table-row")
                End If

                DETAIL_ACT_ID.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                btnAdds.Visible = False
                btnUpdate.Visible = True
                btnAccCancel.Visible = True
                gvJournal.SelectedIndex = iIndex
                ClearRadGridandCombo()
                RecreateSsssionDataSource()
                h_NextLine.Value = Session("gintEditLine")
                CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                        Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()
                'gridbind()
                Exit For
            End If
        Next
    End Sub

    Protected Sub txtDAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDAmount.TextChanged
        If CDbl(txtDAmount.Text.Trim) > 0 And h_BSUID_DR.Value <> Session("sBsuid") Then
            'tr_Allocation.Visible = False
            tr_Allocation.Attributes.Add("style", "display:none")
        ElseIf CDbl(txtDAmount.Text.Trim) < 0 And h_BSUID_CR.Value <> Session("sBsuid") Then
            'tr_Allocation.Visible = False
            tr_Allocation.Attributes.Add("style", "display:none")
        Else
            'tr_Allocation.Visible = True
            tr_Allocation.Attributes.Add("style", "display:table-row")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccCancel.Click
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnAccCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = CDbl(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(DETAIL_ACT_ID.Text & "", Session("sBsuid"))
            Dim str_cost_center As String = ""
            '''''FIND ACCOUNT IS THERE
            Dim bool_cost_center_reqired As Boolean = False

            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String

                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                & " AND ACT_ID='" & DETAIL_ACT_ID.Text & "'" _
                & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                    str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                    bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                Else
                    txtDAccountName.Text = ""
                    lblError.Text = getErrorMessage("303") ' account already there
                    Exit Sub
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try

            If dCrordb = 0 Then
                Exit Sub
            End If
            If ViewState("datamode") = "edit" Then
                If dCrordb < 0 Then
                    If ViewState("canEdit") = "credit" Or ViewState("canEdit") = "all" Then
                    Else
                        lblError.Text = "Cannot Edit/Add credit details. The journal is either posted or belong to other business unit"
                        Exit Sub
                    End If
                Else
                    If ViewState("canEdit") = "debit" Or ViewState("canEdit") = "all" Then
                    Else
                        lblError.Text = "Cannot Edit/Add debit details. The journal is either posted or belong to other business unit"
                        Exit Sub
                    End If
                End If
            End If

            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If (Session("dtJournal").Rows(iIndex)("Accountid") <> DETAIL_ACT_ID.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While
                        End If
                        ''updation handle here
                    End If
                    Session("dtJournal").Rows(iIndex)("Accountid") = DETAIL_ACT_ID.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = txtDAccountName.Text.Trim
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Credit") = "0"
                        Session("dtJournal").Rows(iIndex)("Debit") = dCrordb
                    Else
                        Session("dtJournal").Rows(iIndex)("Credit") = dCrordb * -1
                        Session("dtJournal").Rows(iIndex)("Debit") = "0"
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Required") = bool_cost_center_reqired
                    Session("dtJournal").Rows(iIndex)("CostCenter") = str_cost_center
                    RecreateSsssionDataSource()
                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("gintEditLine"), Session("sBsuid"), _
                                          Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If

                    btnAdds.Visible = True
                    btnUpdate.Visible = False
                    btnEdit.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog("UPDATE")
        End Try
    End Sub

    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        gridbind()
        Clear_Details()
        txtHDocno.Text = AccountFunctions.GetNextDocId("IJV", "", Session("EntryDate").Month, Session("EntryDate").Year)
        txtdocDate.Text = GetDiplayDate()
        txtHNarration.Text = ""
        txtDifferAmt.Text = ""
        'txtHGroupRate.Text = ""
        'txtHExchRateCR.Text = ""
        'txtHExchRateDR.Text = ""
        txtBSUDR.Text = ""
    End Sub

    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = ""
        txtDAccountName.Text = ""
        DETAIL_ACT_ID.Text = ""

        imgSelectDocDate.Enabled = True
        txtdocDate.ReadOnly = False
        chkMirrorEntry.Enabled = True
        DDCurrency.Enabled = True
        If chkMirrorEntry.Checked Then
            txtDifferAmt.ReadOnly = False
        Else
            txtDifferAmt.ReadOnly = True
        End If
        txtHGroupRate.ReadOnly = False
        txtHExchRateCR.ReadOnly = False
        txtHExchRateDR.ReadOnly = False
        txtHNarration.ReadOnly = False
        ClearRadGridandCombo()
        'tr_Allocation.Visible = True
        tr_Allocation.Attributes.Add("style", "display:table-row")
    End Sub
    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub
    Private Function check_Totals() As String
        Try
            Dim i As Integer

            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then

                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + Session("dtJournal").Rows(i)("Credit")
                        Else
                            dDebit = dDebit + Session("dtJournal").Rows(i)("Debit")
                        End If
                    Else
                    End If
                Next
            End If
            txtTDotalDebit.Text = AccountFunctions.Round(dDebit)
            txtTotalCredit.Text = AccountFunctions.Round(dCredit)
            Dim difAmt As Double
            If txtDifferAmt.Text <> String.Empty And txtDifferAmt.Text <> "" Then
                difAmt = Convert.ToDouble(txtDifferAmt.Text)
            End If
            If chkMirrorEntry.Checked = True Then
                If difAmt > 0 Then
                    If (dDebit + difAmt) <> dCredit Then

                        Return "Debit and Credit are not Balanced"
                    Else
                        btnSave.Enabled = True
                    End If
                Else
                    If (dCredit - difAmt) <> dDebit Then
                        'btnSave.Enabled = False
                        Return "Debit and Credit are not Balanced"
                    Else
                        btnSave.Enabled = True
                    End If
                End If
            Else
                If h_BSUID_CR.Value = Session("sBsuID") Then
                    If Math.Abs(dCredit) = 0 Then
                        Return "Kindly Enter Atleast One Credit Entry"
                    End If
                ElseIf h_BSUID_DR.Value = Session("sBsuID") Then
                    If Math.Abs(dDebit) = 0 Then
                        Return "Kindly Enter Atleast One Debit Entry"
                    End If
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
        Return ""
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim aud_action As String = String.Empty
            Dim strfDate As String = txtdocDate.Text.Trim

            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtdocDate.Text = String.Format("{0:dd/MMM/yyyy}", strfDate)
            End If

            If h_BSUID_DR.Value = "" Or h_BSUID_CR.Value = "" Then
                lblError.Text = "Please select both CR and DR Bussiness Units "
                Exit Sub
            End If

            If h_BSUID_DR.Value = h_BSUID_CR.Value Then
                lblError.Text = " Both Bussiness Units CR and DR can�t be same...!"
                Exit Sub
            End If

            If (h_BSUID_CR.Value <> Session("sBsuID") And h_BSUID_DR.Value <> Session("sBsuID")) Or _
            (h_BSUID_CR.Value = Session("sBsuID") And h_BSUID_DR.Value = Session("sBsuID")) Then
                lblError.Text = "Bussiness Units are not Valid "
                Exit Sub
            End If

            Dim s As String = check_Totals()
            If s <> "" Then
                lblError.Text = s
                Exit Sub
            End If

            s = check_Errors()
            If s <> "" Then
                lblError.Text = s
                Exit Sub
            End If

            ViewState("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here

                'Adding header info
                Dim cmd As New SqlCommand("SaveIJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpIJH_SUB_ID As New SqlParameter("@IJH_FROM_SUB_ID", SqlDbType.VarChar, 20)
                sqlpIJH_SUB_ID.Value = Session("jid") & ""
                cmd.Parameters.Add(sqlpIJH_SUB_ID)

                Dim sqlpIJH_CR_BSU_ID As New SqlParameter("@IJH_CR_BSU_ID", SqlDbType.VarChar, 20)
                sqlpIJH_CR_BSU_ID.Value = h_BSUID_CR.Value
                cmd.Parameters.Add(sqlpIJH_CR_BSU_ID)

                Dim sqlpIJH_DR_BSU_ID As New SqlParameter("@IJH_DR_BSU_ID", SqlDbType.VarChar, 20)
                sqlpIJH_DR_BSU_ID.Value = h_BSUID_DR.Value
                cmd.Parameters.Add(sqlpIJH_DR_BSU_ID)

                Dim sqlpIJH_FYEAR As New SqlParameter("@IJH_FYEAR", SqlDbType.Int)
                sqlpIJH_FYEAR.Value = Convert.ToInt32(Session("F_YEAR"))
                cmd.Parameters.Add(sqlpIJH_FYEAR)

                Dim sqlpIJH_DOCTYPE As New SqlParameter("@IJH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpIJH_DOCTYPE.Value = "IJV"
                cmd.Parameters.Add(sqlpIJH_DOCTYPE)

                Dim sqlpIJH_DOCNO As New SqlParameter("@IJH_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpIJH_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpIJH_DOCNO.Value = " "
                End If
                'If Request.QueryString("editid") <> "" Then
                '    sqlpIJH_DOCNO.Value = viewstate("str_editData").Split("|")(0) & ""
                'Else
                '    sqlpIJH_DOCNO.Value = "1"
                'End If
                cmd.Parameters.Add(sqlpIJH_DOCNO)

                Dim sqlpIJH_DOCDT As New SqlParameter("@IJH_DOCDT", SqlDbType.DateTime, 30)
                sqlpIJH_DOCDT.Value = String.Format("{0:DD\MMM\yyyy}", txtdocDate.Text)
                cmd.Parameters.Add(sqlpIJH_DOCDT)

                Dim sqlpIJH_CUR_ID As New SqlParameter("@IJH_CUR_ID", SqlDbType.VarChar, 12)
                sqlpIJH_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpIJH_CUR_ID)

                Dim sqlpIJH_EXGRATE1 As New SqlParameter("@IJH_EXGRATE1", SqlDbType.Decimal, 16)
                sqlpIJH_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim  'DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpIJH_EXGRATE1)

                Dim sqlpIJH_EXGRATE2 As New SqlParameter("@IJH_EXGRATE2", SqlDbType.Decimal, 16)
                sqlpIJH_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim 'DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpIJH_EXGRATE2)

                Dim sqlpIJH_DREXGRATE As New SqlParameter("@IJH_DREXGRATE", SqlDbType.Decimal, 16)
                sqlpIJH_DREXGRATE.Value = Convert.ToDecimal(txtHExchRateDR.Text) 'DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpIJH_DREXGRATE)

                Dim sqlpIJH_NARRATION As New SqlParameter("@IJH_NARRATION", SqlDbType.VarChar, 300)
                sqlpIJH_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpIJH_NARRATION)

                Dim sqlpIJH_bMirror As New SqlParameter("@IJH_bMIRROR", SqlDbType.Bit)
                sqlpIJH_bMirror.Value = chkMirrorEntry.Checked
                cmd.Parameters.Add(sqlpIJH_bMirror)

                Dim sqlpIJH_DIFFAMT As New SqlParameter("@IJH_DIFFAMT", SqlDbType.Int, 18)
                If txtDifferAmt.Text <> Nothing And txtDifferAmt.Text <> "" Then
                    sqlpIJH_DIFFAMT.Value = Decimal.Parse(txtDifferAmt.Text) 'DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                Else
                    sqlpIJH_DIFFAMT.Value = 0
                End If
                cmd.Parameters.Add(sqlpIJH_DIFFAMT)

                Dim sqlpIJH_bDELETED As New SqlParameter("@IJH_bDELETED", SqlDbType.Bit)
                sqlpIJH_bDELETED.Value = False
                cmd.Parameters.Add(sqlpIJH_bDELETED) '@IJH_ISS_BSU_ID

                Dim sqlpIJH_ISS_BSU_ID As New SqlParameter("@IJH_ISS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpIJH_ISS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpIJH_ISS_BSU_ID)

                Dim sqlpIJH_bPOSTEDCRBSU As New SqlParameter("@IJH_bPOSTEDCRBSU", SqlDbType.Bit)
                sqlpIJH_bPOSTEDCRBSU.Value = False
                cmd.Parameters.Add(sqlpIJH_bPOSTEDCRBSU)

                Dim sqlpIJH_bPOSTEDDRBSU As New SqlParameter("@IJH_bPOSTEDDRBSU", SqlDbType.Bit)
                sqlpIJH_bPOSTEDDRBSU.Value = False
                cmd.Parameters.Add(sqlpIJH_bPOSTEDDRBSU)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@IJH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpJHD_TIMESTAMP)

                Dim sqlpIJH_SESSIONID As New SqlParameter("@IJH_SESSIONID", SqlDbType.VarChar, 50)
                sqlpIJH_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpIJH_SESSIONID)

                Dim sqlpIJH_LOCK As New SqlParameter("@IJH_LOCK", SqlDbType.VarChar, 20)
                sqlpIJH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpIJH_LOCK)

                Dim sqlpbCalledfromBSU As New SqlParameter("@bCalledfromBSU", SqlDbType.Bit)
                If h_BSUID_DR.Value = Session("sbsuid") Then
                    sqlpbCalledfromBSU.Value = False
                Else
                    sqlpbCalledfromBSU.Value = False
                End If
                cmd.Parameters.Add(sqlpbCalledfromBSU)

                Dim sqlpIJH_CR_BSU_ID_OLD As New SqlParameter("@IJH_CR_BSU_ID_OLD", SqlDbType.VarChar, 20)
                sqlpIJH_CR_BSU_ID_OLD.Value = h_BSUID_CR_Old.Value
                cmd.Parameters.Add(sqlpIJH_CR_BSU_ID_OLD)

                Dim sqlpIJH_DR_BSU_ID_OLD As New SqlParameter("@IJH_DR_BSU_ID_OLD", SqlDbType.VarChar, 20)
                sqlpIJH_DR_BSU_ID_OLD.Value = h_BSUID_DR_Old.Value
                cmd.Parameters.Add(sqlpIJH_DR_BSU_ID_OLD)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopIJH_NEWDOCNO As New SqlParameter("@IJH_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopIJH_NEWDOCNO)
                cmd.Parameters("@IJH_NEWDOCNO").Direction = ParameterDirection.Output
                Dim lstrNewDocNo As String = String.Empty
                'If ViewState("disableheader") & "" <> "true" Then
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                    Exit Sub
                End If
                If (ViewState("datamode") = "add") Then
                    lstrNewDocNo = CStr(cmd.Parameters("@IJH_NEWDOCNO").Value)
                End If

                'End If

                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    If ViewState("datamode") <> "edit" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopIJH_NEWDOCNO.Value)
                    Else
                        iReturnvalue = DeleteVOUCHER_D_S_ALL(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        If iReturnvalue = 0 Then
                            str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        End If

                    End If
                    If str_err = "0" Then
                        'stTrans.Rollback()
                        stTrans.Commit()
                        unlock()
                        'h_editorview.Value = ""
                        clear_All()
                        'btnSave.Enabled = False
                        gvJournal.Enabled = True
                        ''lblError.Text = getErrorMessage("304")
                        txtDNarration.Text = ""
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)

                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage("304")
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            'datamode = Encr_decrData.Encrypt("view")
                            'Response.Redirect("acccrCashReceipt.aspx?viewid=" & Request.QueryString("viewid") & "&edited=yes" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode)
                            lblError.Text = getErrorMessage("305")
                        End If

                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Throw        'Bubble up the exception
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "IJV"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                cmd.Dispose()
                cmd = New SqlCommand("SaveIJOURNAL_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table
                Dim str_crdb As String = "CR"

                If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                    dTotal = Session("dtJournal").Rows(iIndex)("Debit")
                    str_crdb = "DR"
                Else
                    dTotal = Session("dtJournal").Rows(iIndex)("Credit")
                End If

                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtJournal").Rows(iIndex)("id"), _
                          str_crdb, iIndex + 1 - ViewState("iDeleteCount"), Session("dtJournal").Rows(iIndex)("Accountid"), dTotal)
                If str_err <> "0" Then
                    Return str_err
                End If

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                'Dim sqlpJNL_SUB_ID As New SqlParameter("@JNL_SUB_ID", SqlDbType.VarChar, 20)
                'sqlpJNL_SUB_ID.Value = Session("jid")
                'cmd.Parameters.Add(sqlpJNL_SUB_ID)

                Dim sqlpsqlpIJL_FROM_BSU_ID As New SqlParameter("@IJL_FROM_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpIJL_FROM_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpIJL_FROM_BSU_ID)

                Dim sqlpsqlpIJL_TO_BSU_ID As New SqlParameter("@IJL_TO_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpIJL_TO_BSU_ID.Value = h_BSUID_DR.Value ' h_BSUID_DR.Value 'Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpIJL_TO_BSU_ID)

                Dim sqlpIJL_FYEAR As New SqlParameter("@IJL_FYEAR", SqlDbType.Int)
                sqlpIJL_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpIJL_FYEAR)

                Dim sqlpIJL_DOCTYPE As New SqlParameter("@IJL_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpIJL_DOCTYPE.Value = "IJV"
                cmd.Parameters.Add(sqlpIJL_DOCTYPE)

                Dim sqlpIJL_DOCNO As New SqlParameter("@IJL_DOCNO", SqlDbType.VarChar, 20)
                sqlpIJL_DOCNO.Value = p_docno & ""
                cmd.Parameters.Add(sqlpIJL_DOCNO)

                'Dim sqlpIJL_CUR_ID As New SqlParameter("@IJL_CUR_ID", SqlDbType.VarChar, 12)
                'sqlpIJL_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                'cmd.Parameters.Add(sqlpIJL_CUR_ID)

                'Dim sqlpIJL_EXGRATE1 As New SqlParameter("@IJL_EXGRATE1", SqlDbType.Int, 8)
                'sqlpIJL_EXGRATE1.Value = Convert.ToInt32(txtHExchRateCR.Text) 'DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                'cmd.Parameters.Add(sqlpIJL_EXGRATE1)

                'Dim sqlpIJL_EXGRATE2 As New SqlParameter("@IJL_EXGRATE2", SqlDbType.Int, 8)
                'sqlpIJL_EXGRATE2.Value = Convert.ToInt32(txtHGroupRate.Text) 'DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                'cmd.Parameters.Add(sqlpIJL_EXGRATE2)

                'Dim sqlpJNL_DOCDT As New SqlParameter("@JNL_DOCDT", SqlDbType.DateTime, 30)
                'sqlpJNL_DOCDT.Value = txtdocDate.Text & ""
                'cmd.Parameters.Add(sqlpJNL_DOCDT)

                Dim sqlpIJL_ACT_ID As New SqlParameter("@IJL_ACT_ID", SqlDbType.VarChar, 20)
                sqlpIJL_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                cmd.Parameters.Add(sqlpIJL_ACT_ID)

                Dim sqlpIJL_DEBIT As New SqlParameter("@IJL_DEBIT", SqlDbType.Decimal, 8)
                sqlpIJL_DEBIT.Value = Session("dtJournal").Rows(iIndex)("Debit")
                cmd.Parameters.Add(sqlpIJL_DEBIT)

                Dim sqlpIJL_CREDIT As New SqlParameter("@IJL_CREDIT", SqlDbType.Decimal)
                sqlpIJL_CREDIT.Value = Session("dtJournal").Rows(iIndex)("Credit")
                cmd.Parameters.Add(sqlpIJL_CREDIT)

                Dim sqlpIJL_NARRATION As New SqlParameter("@IJL_NARRATION", SqlDbType.VarChar, 300)
                sqlpIJL_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                cmd.Parameters.Add(sqlpIJL_NARRATION)

                Dim sqlpbIJL_SLNO As New SqlParameter("@IJL_SLNO", SqlDbType.Int)
                sqlpbIJL_SLNO.Value = iIndex + 1 - ViewState("iDeleteCount")
                cmd.Parameters.Add(sqlpbIJL_SLNO)

                Dim sqlpbIJL_BDELETED As New SqlParameter("@IJL_BDELETED", SqlDbType.Bit)
                sqlpbIJL_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbIJL_BDELETED)

                Dim sqlpbCalledfromBSU As New SqlParameter("@bCalledfromBSU", SqlDbType.Bit)
                If h_BSUID_DR.Value = Session("sbsuid") Then
                    sqlpbCalledfromBSU.Value = False
                Else
                    sqlpbCalledfromBSU.Value = False
                End If
                cmd.Parameters.Add(sqlpbCalledfromBSU)

                Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                If iIndex = Session("dtJournal").Rows.Count - 1 Then
                    sqlpbLastRec.Value = True
                Else
                    sqlpbLastRec.Value = False
                End If
                cmd.Parameters.Add(sqlpbLastRec)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") <> "edit" Then
                    sqlpbEdit.Value = False
                Else
                    sqlpbEdit.Value = True
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then

                    Exit For
                Else

                End If
                cmd.Parameters.Clear()
            Else
                If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                    cmd = New SqlCommand("DeleteIJOURNAL_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Response.Write(getErrorMessage(iReturnvalue))
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtJournal").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If
        'Adding transaction info       
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
     ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
     ByVal p_voucherid As String, ByVal p_crdr As String, _
     ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), "IJV", _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtdocDate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), "IJV", "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function
    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("Amount")
                End If
            Next
            If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean
        Try
            Dim dTotal As Double = 0

            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM IJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("IJH_DOCDT"), OASISConstants.DateFormat)

                txtHNarration.Text = ds.Tables(0).Rows(0)("IJH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("IJH_DOCNO") & "|" _
                    & ds.Tables(0).Rows(0)("IJH_DOCDT") & "|" _
                    & ds.Tables(0).Rows(0)("IJH_FYEAR") & "|"
                    txtHDocno.Text = ds.Tables(0).Rows(0)("IJH_DOCNO") & ""
                    Dim cmd As New SqlCommand("LockIJOURNAL_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    'If ds.Tables(0).Rows(0)("bPosted") = True Then
                    '    Return 1000
                    'End If
                    Dim sqlpJHD_SUB_ID As New SqlParameter("@IJH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpIJH_CR_BSU_ID As New SqlParameter("@IJH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpIJH_CR_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpIJH_CR_BSU_ID)

                    Dim sqlpIJH_FYEAR As New SqlParameter("@IJH_FYEAR", SqlDbType.Int)
                    sqlpIJH_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpIJH_FYEAR)

                    Dim sqlpIJH_DOCTYPE As New SqlParameter("@IJH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpIJH_DOCTYPE.Value = "IJV"
                    cmd.Parameters.Add(sqlpIJH_DOCTYPE)

                    Dim sqlpIJH_DOCNO As New SqlParameter("@IJH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpIJH_DOCNO.Value = ViewState("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpIJH_DOCNO)

                    Dim sqlpIJH_DOCDT As New SqlParameter("@IJH_DOCDT", SqlDbType.DateTime, 30)
                    sqlpIJH_DOCDT.Value = ds.Tables(0).Rows(0)("IJH_DOCDT") & ""
                    cmd.Parameters.Add(sqlpIJH_DOCDT)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@IJH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopIJH_TIMESTAMP As New SqlParameter("@IJH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopIJH_TIMESTAMP)
                    cmd.Parameters("@IJH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                        Return iReturnvalue
                    End If
                    ViewState("str_timestamp") = sqlopIJH_TIMESTAMP.Value
                    Return iReturnvalue

                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                ViewState("str_editData") = ""
                MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
                ViewState("datamode") = "none"
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&invalidedit=1", MainMnu_code, ViewState("datamode"))
                Response.Redirect(url)
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
            ViewState("datamode") = "none"
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&invalidedit=1", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
            'Return " | | "
        End Try
        Return True
    End Function

    Private Function unlock() As String
        If ViewState("str_editData") <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String

                str_Sql = "SELECT * FROM IJOURNAL_H WHERE" _
                & " IJH_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'"
                '& " order by gm.GPM_DESCR "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtdocDate.Text = Format(ds.Tables(0).Rows(0)("IJH_DOCDT"), OASISConstants.DateFormat)
                    txtHDocno.Text = ViewState("str_editData").Split("|")(0)
                    txtHNarration.Text = ds.Tables(0).Rows(0)("IJH_NARRATION")
                    Dim objConn As New SqlConnection(str_conn)

                    Try
                        objConn.Open()
                        Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                        sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                        cmd.Parameters.Add(sqlpJHD_SUB_ID)

                        Dim sqlpIJH_CR_BSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                        sqlpIJH_CR_BSU_ID.Value = ds.Tables(0).Rows(0)("IJH_CR_BSU_ID") & ""
                        cmd.Parameters.Add(sqlpIJH_CR_BSU_ID)

                        Dim sqlpIJH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                        sqlpIJH_FYEAR.Value = ds.Tables(0).Rows(0)("IJH_FYEAR") & ""
                        cmd.Parameters.Add(sqlpIJH_FYEAR)

                        Dim sqlpIJH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                        sqlpIJH_DOCTYPE.Value = ds.Tables(0).Rows(0)("IJH_DOCTYPE") & ""
                        cmd.Parameters.Add(sqlpIJH_DOCTYPE)

                        Dim sqlpIJH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                        sqlpIJH_DOCNO.Value = ds.Tables(0).Rows(0)("IJH_DOCNO") & ""
                        cmd.Parameters.Add(sqlpIJH_DOCNO)

                        Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                        sqlpJHD_CUR_ID.Value = Session.SessionID
                        cmd.Parameters.Add(sqlpJHD_CUR_ID)

                        Dim sqlpJHD_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                        sqlpJHD_USER.Value = Session("sUsr_name")
                        cmd.Parameters.Add(sqlpJHD_USER)

                        Dim sqlopIJH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                        sqlopIJH_TIMESTAMP.Value = ViewState("str_timestamp")
                        cmd.Parameters.Add(sqlopIJH_TIMESTAMP)

                        Dim iReturnvalue As Integer
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)
                        cmd.ExecuteNonQuery()

                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            lblError.Text = (iReturnvalue)
                        End If
                        Return iReturnvalue
                    Catch ex As Exception
                        Errorlog(ex.Message)
                    Finally
                        objConn.Close()
                    End Try
                Else
                End If
                Return " | | "
            Catch ex As Exception
                Errorlog(ex.Message)
                Return " | | "
            End Try
        End If
        Return True
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            ResetViewData()
            If ViewState("disableheader") = "true" Then
                setHeader()
                imgCRBSUNIT.Enabled = True
            Else
                MakeReadOnly(False)
            End If
            ViewState("datamode") = "edit"
            If chkMirrorEntry.Checked Then
                txtDifferAmt.Enabled = True
                txtDifferAmt.ReadOnly = False
            End If
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            btnSave.Enabled = True
        End If
    End Sub

    Protected Sub btnEditcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        unlock()
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clear_All()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Dim ds As New DataSet

        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        If Request.QueryString("editid") <> "" Then
            str_Sql = "SELECT * FROM IJOURNAL_H WHERE" _
                   & " GUID='" & Request.QueryString("editid") & "'"
        Else
            str_Sql = "SELECT * FROM IJOURNAL_H WHERE" _
                   & " GUID='" & Request.QueryString("viewid") & "'"
        End If

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim cmd As New SqlCommand("DeleteIJOURNAL_H", objConn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("IJH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)

                'Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                'sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
                'cmd.Parameters.Add(sqlpJHD_SUB_ID)

                'Dim sqlpIJH_CR_BSU_ID As New SqlParameter("@IJH_CR_BSU_ID", SqlDbType.VarChar, 20)
                'sqlpIJH_CR_BSU_ID.Value = ds.Tables(0).Rows(0)("IJH_CR_BSU_ID") & ""
                'cmd.Parameters.Add(sqlpIJH_CR_BSU_ID)

                Dim sqlpIJH_FYEAR As New SqlParameter("@IJH_FYEAR", SqlDbType.Int)
                sqlpIJH_FYEAR.Value = ds.Tables(0).Rows(0)("IJH_FYEAR") & ""
                cmd.Parameters.Add(sqlpIJH_FYEAR)

                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "IJV"
                cmd.Parameters.Add(sqlpDOCTYPE)


                'Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                'sqlpJHD_DOCDT.Value = txtdocDate.Text & ""
                'cmd.Parameters.Add(sqlpJHD_DOCDT)


                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                Else
                    Dim aud_action As String = "Delete"
                    Dim aud_remark As String = "data Deleted"

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, aud_action, Session("sUsr_name"), Me.Page)
                    'UtilityObj.operOnAudiTable(USR_NAME, CurBsUnit, aud_module, aud_form, aud_docno, aud_action, aud_remark)
                    If flagAudit <> 0 Then
                        'add to all files
                        Throw New ArgumentException("Could not process your request")

                    End If

                    lblError.Text = getErrorMessage("516")
                    ViewState("datamode") = Encr_decrData.Encrypt("none")
                    Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&deleted=true", Request.QueryString("MainMnu_code"), ViewState("datamode"))
                    Response.Redirect(url)
                    'Response.Redirect("accIUjournalvoucher.aspx?deleted=done")
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try


    End Sub

    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT *,isnull(case when IJH_CR_BSU_ID='" & Session("SBSUID") & "' then [IJH_bPOSTEDCRBSU] else [IJH_bPOSTEDDRBSU] end,0) IJH_BpostedStatus FROM IJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("IJH_DOCDT"), OASISConstants.DateFormat)
                txtHDocno.Text = ds.Tables(0).Rows(0)("IJH_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("IJH_NARRATION")
                ' IJH_ISS_BSU_ID
                H_Bposted.Value = ds.Tables(0).Rows(0)("IJH_BpostedStatus")
                If Session("sBsuid") = ds.Tables(0).Rows(0)("IJH_ISS_BSU_ID") Then
                    ViewState("canEdit") = "all"
                Else
                    ViewState("canEdit") = "all"
                End If

                Try
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("IJH_DOCNO") & "|" _
                    & ds.Tables(0).Rows(0)("IJH_DOCDT") & "|" _
                    & ds.Tables(0).Rows(0)("IJH_FYEAR") & "|"
                    Return ""
                Catch ex As Exception
                    Errorlog(ex.Message)
                End Try
            Else
                ViewState("str_editData") = ""
                MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
                ViewState("datamode") = "none"
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&invalidedit=1", MainMnu_code, ViewState("datamode"))
                Response.Redirect(url)
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
            ViewState("datamode") = "none"
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&invalidedit=1", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        End Try
        Return True
    End Function

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub chkMirrorEntry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMirrorEntry.CheckedChanged
        If chkMirrorEntry.Checked Then
            txtDifferAmt.ReadOnly = False
            txtDifferAmt.Enabled = True
            txtDifferAmt.Text = String.Empty
        Else
            txtDifferAmt.ReadOnly = True
            txtDifferAmt.Text = String.Empty
        End If
        gridbind()
    End Sub

    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Try
            'Adding transaction info
            Dim iIndex As Integer
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                Dim str_manndatory_costcenter As String
                If Convert.ToBoolean(Session("dtJournal").Rows(iIndex)("required")) = True Then
                    str_manndatory_costcenter = Session("dtJournal").Rows(iIndex)("Costcenter")
                Else
                    str_manndatory_costcenter = ""
                End If
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                        str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                        Session("dtJournal").Rows(iIndex)("Credit"), str_manndatory_costcenter)
                    Else
                        str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                        Session("dtJournal").Rows(iIndex)("Debit"), str_manndatory_costcenter)
                    End If

                    If str_Err <> "" Then
                        str_Error = str_Error & "<br/>" & str_Err & " At Line - " & iIndex + 1
                    End If
                End If
            Next
            'Adding transaction info
            Return str_Error
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function

    Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
    Optional ByVal p_mandatory_costcenter As String = "") As String
        Try
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer
            Dim bool_check_other, bool_mandatory_exists As Boolean

            Dim str_err As String = ""
            If p_mandatory_costcenter <> "" Then
                bool_mandatory_exists = False
            Else
                bool_mandatory_exists = True
            End If
            Dim str_balanced As Boolean = True
            bool_check_other = False
            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtCostChild").Rows(iIndex)("costcenter") = p_mandatory_costcenter Then
                        bool_mandatory_exists = True
                    End If
                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                        If str_balanced = False Then
                            str_err = str_err & " <BR> Invalid Allocation for cost center " & AccountFunctions.get_cost_center(Session("dtCostChild").Rows(iIndex)("costcenter"))
                        End If
                    End If
                    str_balanced = True
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
                        iLineid = -1
                        bool_check_other = True
                        str_balanced = check_others(p_voucherid, p_amount)
                        If str_balanced = False Then
                            If str_err = "" Then
                                str_err = "Invalid Allocation for other cost center"
                            Else
                                str_err = str_err & " <BR> Invalid Allocation for other cost centers "
                            End If

                        End If
                    End If
                    iLineid = iLineid + 1
                    'If str_balanced = False Then
                    '    Exit For
                    'End If
                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                End If
            Next
            If bool_mandatory_exists = False Then
                str_err = "<br>Mandatory Cost center - " & AccountFunctions.get_cost_center(p_mandatory_costcenter) & " not allocated"
            End If
            Return str_err
        Catch ex As Exception
            Errorlog(ex.Message, "child")
            Return ""
        End Try
    End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.InterUnitVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "IJV", txtHDocno.Text, H_Bposted.Value, Session("HideCC"))
        Session("ReportSource") = repSource
        ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetBSUName(ByVal BSUID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUID + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub txtDifferAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDifferAmt.TextChanged
        gridbind()
    End Sub

    Protected Sub imgCRBSUNIT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCRBSUNIT.Click
        bind_Currency(h_BSUID_CR.Value)
        checkFC("CR")
    End Sub
    Sub checkFC(ByVal CRDR As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim FC As Boolean
        If CRDR = "CR" Then
            FC = ViewState("canEdit") = "credit" Or ViewState("canEdit") = "all" ' SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select ISNULL(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where BSU_ID='" & h_BSUID_CR.Value & "'")
            If FC Then
                txtHExchRateCR.ReadOnly = False
            Else
                txtHExchRateCR.ReadOnly = True
            End If
        Else
            FC = ViewState("canEdit") = "debit" Or ViewState("canEdit") = "all" ' SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select ISNULL(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where BSU_ID='" & h_BSUID_DR.Value & "'")
            If FC Then
                txtHExchRateDR.ReadOnly = False
            Else
                txtHExchRateDR.ReadOnly = True
            End If
        End If
    End Sub
    Protected Sub imgBSUSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBSUSel.Click
        checkFC("DR")
    End Sub

    'Protected Sub DETAIL_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DETAIL_ACT_ID.TextChanged
    '    chk_DetailsAccount()
    '    ClearRadGridandCombo()
    'End Sub

    Sub chk_DetailsAccount()
        txtDAccountName.Text = AccountFunctions.Validate_Account(DETAIL_ACT_ID.Text, Session("sbsuid"), "NORMAL")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            DETAIL_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtDAmount.Focus()
        End If
    End Sub

    Protected Sub btnAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAccount.Click
        chk_DetailsAccount()
    End Sub
    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub



    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Finance\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) And h_BSUID_CR.Value.Length = 0 Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    If Not IO.File.Exists(str_img & txtHDocno.Text & "_" & h_BSUID_CR.Value & FileExt) Then
                        IO.File.Move(strFilepath, str_img & txtHDocno.Text & "_" & h_BSUID_CR.Value & FileExt)
                    End If
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "insert into IJV_I (IJI_CONTENTTYPE, IJI_DOCNO, IJI_BSU_ID, IJI_NAME, IJI_DATE, IJI_USERNAME, IJI_FILENAME, IJI_DELETE) values ('" & ContentType & "','" & txtHDocno.Text & "','" & Session("sBSUID") & "','" & UploadDocPhoto.FileName & "',getdate(),'" & Session("sUsr_name") & "','" & txtHDocno.Text & "_" & h_BSUID_CR.Value & FileExt & "',0)")
                    hdnFileName.Value = txtHDocno.Text & "_" & h_BSUID_CR.Value & FileExt
                    hdnContentType.Value = ContentType
                    btnDocumentLink.Text = UploadDocPhoto.FileName
                    btnDocumentLink.Visible = True
                    btnDocumentDelete.Visible = True
                    UploadDocPhoto.Visible = False
                    btnUpload.Visible = False
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub

    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function

    Protected Sub btnDocumentDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocumentDelete.Click
        If Not btnSave.Visible Then
            lblError.Text = "Cannot delete file"
        Else
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "delete from ijv_i where iji_docno='" & txtHDocno.Text & "' and iji_bsu_id='" & Session("sBSUID") & "'")
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Finance\" & hdnFileName.Value)
            Try
                fileToDelete.Delete()
                hdnFileName.Value = ""
                hdnContentType.Value = ""
                btnDocumentLink.Visible = False
                btnDocumentDelete.Visible = False
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class
