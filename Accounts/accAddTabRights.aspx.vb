Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Accounts_accAddTabRights
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim MainMnu_code As String
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                If USR_NAME = "" Or (MainMnu_code <> "D050027") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    Response.CacheControl = "no-cache"

                    ' btnAddOpr.Visible = False

                    Using Rolereader As SqlDataReader = AccessRoleUser.GetRoles()
                        While Rolereader.Read
                            ddlRoleCopy.Items.Add(New ListItem(Rolereader("Rol_Descr").ToString, Rolereader("rol_id").ToString))
                            ddlRole.Items.Add(New ListItem(Rolereader("Rol_Descr").ToString, Rolereader("rol_id").ToString))


                        End While
                    End Using
                    Dim ItemTypeCounter As Integer
                    Using BsuReader As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        While BsuReader.Read
                            ddlBsu.Items.Add(New ListItem(BsuReader("bsu_name").ToString, BsuReader("bsu_id").ToString))
                            If ddlBsu.Items(ItemTypeCounter).Value = CurBsUnit Then
                                ddlBsu.SelectedIndex = ItemTypeCounter
                            End If
                            ItemTypeCounter = ItemTypeCounter + 1

                        End While
                    End Using

                    Call callMenuload()


                    btnAddOpr.Visible = False
                    'Panel8.Visible = False
                    maintr.Visible = False
                    'Label1.Visible = False


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub

    Private Sub callMenuload()
        Try

            Dim ds As New DataSet
            Dim busUnit As String = ddlBsu.SelectedValue
            Dim roleId As String
            RoleId = ddlRole.SelectedValue
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Using conn As SqlConnection = New SqlConnection(connStr)
                Dim sql As String = "SELECT  TABS_M.TAB_CODE AS TAB_CODE, TABS_M.TAB_TEXT AS TAB_TEXT, TABS_M.TAB_NAME AS TAB_NAME, TABS_M.TAB_MNU_CODE as TAB_PARENTID, " & _
                     " TABS_M.TAB_MODULE + '_' + CASE WHEN str(TABSRIGHTS_S.TAR_RIGHT) IS NULL THEN '0' ELSE ltrim(str(TABSRIGHTS_S.TAR_RIGHT)) " & _
                     " END AS TAB_MODULE FROM  TABS_M LEFT OUTER JOIN TABSRIGHTS_S ON TABS_M.TAB_CODE = TABSRIGHTS_S.TAB_CODE and TABSRIGHTS_S.TAR_ROL_ID='" & roleId & "' and TABSRIGHTS_S.TAR_BSU_ID='" & busUnit & "'"

                Dim da As SqlDataAdapter = New SqlDataAdapter(sql, conn)

                da.Fill(ds)
                da.Dispose()
            End Using

            ds.DataSetName = "TABRIGHTS"
            ds.Tables(0).TableName = "TABRIGHT"
            Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("TABRIGHT").Columns("TAB_CODE"), ds.Tables("TABRIGHT").Columns("TAB_PARENTID"), True)
            relation.Nested = True
            ds.Relations.Add(relation)

            XmlDataSourceMenu.Data = ds.GetXml()
            tvmenu.DataBind()
            ViewState("toolStatus") = 1
            Session("dt") = CreateDataTable()
            Call callgridBind(roleId, busUnit)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Process could be completed"
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.Double"))
            Dim BusUnit As New DataColumn("BusUnit", System.Type.GetType("System.String"))
            Dim RoleId As New DataColumn("RoleId", System.Type.GetType("System.Double"))
            Dim TABCODE As New DataColumn("TABCODE", System.Type.GetType("System.String"))
            Dim TAB_PARENTID As New DataColumn("TAB_PARENTID", System.Type.GetType("System.String"))
            Dim FormText As New DataColumn("FormText", System.Type.GetType("System.String"))
            Dim OperationId As New DataColumn("OperationId", System.Type.GetType("System.Double"))
            Dim Decription As New DataColumn("Description", System.Type.GetType("System.String"))
            Dim modules As New DataColumn("Modules", System.Type.GetType("System.String"))
            'Dim ParentID As New DataColumn("ParentID", System.Type.GetType("System.String"))
            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(BusUnit)
            dtDt.Columns.Add(RoleId)
            dtDt.Columns.Add(TABCODE)
            dtDt.Columns.Add(TAB_PARENTID)
            dtDt.Columns.Add(FormText)
            dtDt.Columns.Add(OperationId)
            dtDt.Columns.Add(Decription)
            dtDt.Columns.Add(modules)
            ' dtDt.Columns.Add(ParentID)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be inserted"
            Return dtDt
        End Try
    End Function


    Protected Sub btnAddOpr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOpr.Click
        Try
            If tvmenu.SelectedValue = "" Then
                lblErrorMessage.Text = "Select the Form for the access right"
            Else

                Dim rDt As DataRow
                rDt = Session("dt").NewRow
                rDt("Id") = ViewState("idTr")

                rDt("RoleId") = ddlRole.SelectedValue
                rDt("BusUnit") = ddlBsu.SelectedValue
                rDt("TABCODE") = tvmenu.SelectedValue

                If IsDBNull(tvmenu.SelectedNode.Parent.Value) = False Then
                    rDt("TAB_PARENTID") = tvmenu.SelectedNode.Parent.Value
                Else
                    rDt("TAB_PARENTID") = ""
                End If

                Using readerUserDetail As SqlDataReader = AccessRoleUser.GetTabText(tvmenu.SelectedValue)
                    If readerUserDetail.HasRows = True Then

                        While readerUserDetail.Read()
                            rDt("FormText") = Convert.ToString(readerUserDetail("Tab_text"))
                            rDt("modules") = Convert.ToString(readerUserDetail("TAB_MODULE"))
                        End While
                    End If

                End Using


                rDt("OperationId") = ddlOprRight.SelectedValue
                rDt("Description") = ddlOprRight.SelectedItem.Text


                If InfoGridView.Rows.Count = 0 Then
                    Session("dt").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                    gridbind()
                Else
                    Dim recordsStatus As Integer = DupicateRecord(tvmenu.SelectedValue)

                    If recordsStatus = 0 Then
                        Session("dt").Rows.Add(rDt)
                        ViewState("idTr") = ViewState("idTr") + 1
                        gridbind()

                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be inserted"
        End Try
    End Sub
    'check for the duplicate record inserted into the grid view
    Private Function DupicateRecord(ByVal FormId As String) As Integer
        Try
            Dim iIndex As Integer = 0
            For iIndex = 0 To InfoGridView.Rows.Count - 1
                If FormId = InfoGridView.Rows(iIndex).Cells(3).Text Then
                    InfoGridView.DeleteRow(iIndex)

                    InfoGridView.Rows(iIndex).Cells(5).Text = ddlOprRight.SelectedValue()
                    InfoGridView.Rows(iIndex).Cells(6).Text = ddlOprRight.SelectedItem.Text
                    InfoGridView.DataBind()
                    ' Return 1
                    Exit For
                End If
            Next
            Return 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Function
    'check for the parent form is selected or not
    Private Function noparentform() As Integer
        Dim iIndex As Integer
        Dim li As New List(Of String)
        Dim counter1 As Integer

        For iIndex = 0 To InfoGridView.Rows.Count - 1
            If li.Contains(InfoGridView.Rows(iIndex).Cells(3).Text) Then
                counter1 = counter1 + 1
            End If
        Next
        If li.Count = counter1 Then
            Return 1
        Else
            Return -1
        End If

    End Function
    'To delete the record from the grid view
    Protected Sub InfoGridView_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles InfoGridView.RowDeleting
        Try
            Dim row As GridViewRow = InfoGridView.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("idLabel"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("dt").Rows.Count - 1
                If iIndex = Session("dt").Rows(iRemove)(0) Then
                    Session("dt").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be Deleted"
        End Try
    End Sub

    Public Sub callgridBind(ByVal rol_id As String, ByVal bsu_id As String)
        Try
            Session("dt").Rows.Clear()
            ViewState("idTr") = 0
            Using Userreader As SqlDataReader = AccessRoleUser.GetTabRightsOnRole(bsu_id, rol_id)
                While Userreader.Read

                    Dim rDt As DataRow
                    rDt = Session("dt").NewRow
                    rDt("Id") = ViewState("idTr")
                    rDt("RoleId") = ddlRole.SelectedValue    'Convert.ToString(Userreader("MNR_ROL_ID"))
                    rDt("BusUnit") = ddlBsu.SelectedValue '       Session("sBsuid") 'Convert.ToString(Userreader("MNR_BSU_ID"))
                    rDt("TABCODE") = Convert.ToString(Userreader("TAB_CODE"))
                    rDt("TAB_PARENTID") = Convert.ToString(Userreader("TAB_PARENTID"))
                    rDt("FormText") = Convert.ToString(Userreader("TAB_TEXT"))
                    rDt("OperationId") = Convert.ToString(Userreader("TAB_RIGHT"))
                    rDt("Description") = Convert.ToString(Userreader("TAB_Descr"))
                    rDt("modules") = Convert.ToString(Userreader("TAB_MODULE"))
                    Session("dt").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                    gridbind()

                End While
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function gridbind() As String


        Try
            'Dim dtInfo As DataTable = CreateDataTable()
            InfoGridView.DataSource = Session("dt")
            'InfoGridView.Columns(1).Visible = False
            'InfoGridView.Columns(2).Visible = False
            'InfoGridView.Columns(4).Visible = False
            InfoGridView.DataBind()
            Return "1"
        Catch ex As Exception
            Return "1"
        End Try
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Check if the InfoGridView is empty or not to do the transaction

            If InfoGridView.Rows.Count > 0 Then


                Dim str_err As String
                Dim roleid As Integer = ddlRole.SelectedValue
                Dim busunit As String = ddlBsu.SelectedValue      'Session("sBsuid")
                '  Dim modules As String = tvmenu.SelectedNode.ToolTip

                Dim dupflag As Integer = AccessRoleUser.checkduplicateTab_Mod(roleid, busunit)

                'code needs to be modified
                If dupflag = 0 Then
                    Dim sqlstring As String
                    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                        sqlstring = "delete from TABSRIGHTS_S where TAR_ROL_ID='" & roleid & "' and TAR_BSU_ID='" & busunit & "'"

                        Dim command As SqlCommand = New SqlCommand(sqlstring, connection)
                        command.CommandType = Data.CommandType.Text
                        command.ExecuteNonQuery()

                    End Using


                End If
                str_err = CallTransactions()

                If str_err = "0" Then
                    Session("dt").Rows.Clear()
                    gridbind()

                    lblErrorMessage.Text = "Process completed successfully"

                Else
                    lblErrorMessage.Text = "Process can not be completed"
                End If

            Else

                lblErrorMessage.Text = "Currently no form is added with the operation rights"

            End If
            Dim USR_NAME As String = Session("sUsr_name")

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, USR_NAME, "Insert", Page.User.Identity.Name.ToString, Me.Page)

            If flagAudit <> 0 Then

                Throw New ArgumentException("Could not process your request")

            End If
            ' btnAddOpr.Visible = False

            ddlRole.Enabled = False
            ddlBsu.Enabled = False
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Process could not be completed"

        End Try


    End Sub

    Private Function CallTransactions() As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try


                Dim StatusFlag As Integer
                Dim iIndex As Integer

                Dim arrModule As New ArrayList
                Dim TABCODE As String
                Dim OperationId As Integer

                Dim modules As String = String.Empty
                Dim BusUnit As String = String.Empty
                Dim TAB_PARENTID As String = String.Empty
                Dim RoleId As String = String.Empty
                For iIndex = 0 To Session("dt").Rows.Count - 1
                    RoleId = Session("dt").Rows(iIndex)("RoleId")
                    BusUnit = Session("dt").Rows(iIndex)("BusUnit")
                    TABCODE = Session("dt").Rows(iIndex)("TABCODE")
                    TAB_PARENTID = Session("dt").rows(iIndex)("TAB_PARENTID")
                    OperationId = Session("dt").Rows(iIndex)("OperationId")
                    modules = Session("dt").Rows(iIndex)("modules")

                    If arrModule.Contains(modules) = False Then
                        arrModule.Add(modules)
                    End If
                    If OperationId <> 0 Then
                        StatusFlag = AccessRoleUser.InsertTabRights(RoleId, BusUnit, TABCODE, TAB_PARENTID, OperationId, modules, transaction)
                    End If


                    If StatusFlag <> 0 Then
                        Throw New ArgumentException("Error while inserting TAB Rights")
                    End If
                Next
                '  transaction.Commit()

                'For Each i As Object In arrModule

                '    StatusFlag = AccessRoleUser.InsertMissingRights(RoleId, BusUnit, i, transaction)

                '    If StatusFlag <> 0 Then
                '        Throw New ArgumentException("Error while inserting Missing TAB Rights")
                '    End If
                'Next

                transaction.Commit()

                Return "0"
                'Adding transaction info
            Catch myex As ArgumentException
                transaction.Rollback()
                UtilityObj.Errorlog(myex.Message)
                Return "1"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
                Return "1"

            End Try


        End Using
    End Function

    Protected Sub ddlRole_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.PreRender
        If Page.IsPostBack = False Then
            Call BUnit4Role(ddlRole.SelectedValue)

        End If
    End Sub
    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.SelectedIndexChanged
        Try
            Call BUnit4Role(ddlRole.SelectedValue)
            Call callMenuload()
            ddlRoleCopy.SelectedIndex = ddlRole.SelectedIndex
            gridbind()
            'ddlRole.Enabled = False
            'ddlBsu.Enabled = False

        Catch ex As Exception
            UtilityObj.Errorlog("Business Unit Change", "accAddTabRights")

        End Try
    End Sub
    Sub BUnit4Role(ByVal roleid As String)
        Try


            Dim bunit_id As String = ""
            Dim bunit_name As String = ""
            ddlBusUnit.Items.Clear()
            Using readerUserDetail As SqlDataReader = AccessRoleUser.GetBunit4Roles(roleid)
                If readerUserDetail.HasRows = True Then

                    While readerUserDetail.Read()
                        bunit_id = readerUserDetail.GetString(0)
                        bunit_name = readerUserDetail.GetString(1)
                        ddlBusUnit.Items.Add(New ListItem(bunit_name, bunit_id))
                    End While

                Else
                    bunit_id = "0"
                    bunit_name = "No Business Unit"
                    ddlBusUnit.Items.Add(New ListItem(bunit_name, bunit_id))
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddlBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusUnit.SelectedIndexChanged
        'Dim tempBusUnit As String
        '' Dim DT As New DataTable()
        ''ddlCopyRole.Items.Clear()

        'tempBusUnit = ddlBusUnit.SelectedItem.Value

        'Using readerBusUnit As SqlDataReader = AccessRoleUser.getBusUnitRole(tempBusUnit)
        '    Dim di As ListItem
        '    '  ddlCopyRole.Items.Clear()
        '    If readerBusUnit.HasRows = True Then
        '        Panel1.Visible = True
        '        While readerBusUnit.Read
        '            di = New ListItem(readerBusUnit(0), readerBusUnit(1))
        '            '    ddlCopyRole.Items.Add(di)
        '        End While
        '    Else
        '        Panel1.Visible = False
        '    End If

        'End Using

    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete

        If Not tvmenu Is Nothing Then
            If ViewState("toolStatus") = 0 Then
                setstyle(tvmenu.Nodes)
            ElseIf ViewState("toolStatus") = 1 Then

                setstyle2(tvmenu.Nodes)
            End If
        End If

    End Sub

    Private Sub setstyle(ByVal nodeList As TreeNodeCollection)
        Try


            If Not nodeList Is Nothing Then

                For Each nodec As TreeNode In nodeList

                    If nodec.Parent Is Nothing Then
                        'nodec.NavigateUrl = "javascript:javascript:void(0);"
                    End If
                    setstyle(nodec.ChildNodes)
                Next

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub setstyle2(ByVal nodeList As TreeNodeCollection)
        Try


            If Not nodeList Is Nothing Then
                Dim arr1() As String
                For Each nodec As TreeNode In nodeList

                    Dim mnu_color As String = find_color(nodec.Value)
                    If mnu_color = "" Then
                        arr1 = Split(nodec.ToolTip, "_")
                        If arr1.Length > 1 Then
                            mnu_color = arr1(1)
                        Else
                            mnu_color = arr1(0)
                        End If

                    End If

                    Dim str_color As String = "Black"
                    If (mnu_color = "1") Then
                        str_color = "Black"
                    ElseIf (mnu_color = "2") Then
                        str_color = "Green"
                    ElseIf (mnu_color = "0") Then
                        str_color = "Red"
                    End If
                    'nodec.NavigateUrl = "javascript:javascript:void(0);"
                    ''''''''''
                    Dim str1 As String = ""

                    Try
                        If nodec.Text IsNot Nothing Then
                            If nodec.Text.IndexOf(">") > 0 Then
                                Dim i As Integer = 0
                                str1 = nodec.Text
                                i = nodec.Text.IndexOf(">")
                                str1 = nodec.Text.Substring(i + 1, nodec.Text.Length - 1 - i)
                                i = str1.IndexOf("<")
                                str1 = str1.Substring(0, i)
                            End If
                        End If
                    Catch ex As Exception
                        '  nodec.Text = "<font Color='" & str_color & "'>" & nodec.Text & "</font>"
                    End Try

                    '''''''''''
                    If str1 = "" Then
                        str1 = nodec.Text

                    End If
                    nodec.Text = "<font Color='" & str_color & "'>" & str1 & "</font>"
                    nodec.ToolTip = mnu_color
                    If nodec.Parent Is Nothing Then
                        ' nodec.NavigateUrl = "javascript:javascript:void(0);"
                    End If
                    setstyle2(nodec.ChildNodes)
                Next
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try

    End Sub

    Function find_color(ByVal p_mnuid As String) As String
        For i As Integer = 0 To Session("dt").Rows.Count - 1
            If (Session("dt").Rows(i)("TABCODE") = p_mnuid) Then ' 
                Return Session("dt").Rows(i)("OperationId")
            End If
        Next
        Return ""
    End Function

    Protected Sub tvmenu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvmenu.SelectedNodeChanged
        Try
            Dim arr1() As String
            If Not tvmenu Is Nothing Then
                If ViewState("toolStatus") = 0 Then

                ElseIf ViewState("toolStatus") = 1 Then

                    arr1 = Split(tvmenu.SelectedNode.ToolTip, "_")
                    If arr1.Length > 1 Then
                        ddlOprRight.SelectedIndex = arr1(1)
                    Else
                        ddlOprRight.SelectedIndex = tvmenu.SelectedNode.ToolTip
                    End If
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    'Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
    '    Call resetMenu()

    '    dt.Rows.Clear()
    '    gridbind()
    'End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'content = Page.Master.FindControl("cphMasterpage")
        ddlRole.Enabled = False
        ddlBsu.Enabled = False
        ViewState("datamode") = "add"
        btnAddOpr.Visible = True
        'Panel8.Visible = True
        maintr.Visible = True
        'Label1.Visible = True
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click


        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Session("dt").Rows.Clear()
            Call callMenuload()

            gridbind()

            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurBsUnit As String = Session("sBsuid")
            ddlRole.Enabled = True
            ddlBsu.Enabled = True
            'Label1.Visible = False
            btnAddOpr.Visible = False
            'Panel8.Visible = False
            maintr.Visible = False
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Session("dt").Rows.Clear()
            Response.Redirect("~\homePage.aspx")
        End If


    End Sub



    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        Try



            Call callMenuload()
            gridbind()

        Catch ex As Exception
            UtilityObj.Errorlog("Business Unit Change", "accAddTabRights")

        End Try
    End Sub

    Protected Sub ddlRoleCopy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoleCopy.SelectedIndexChanged
        Try
            Call BUnit4Role(ddlRoleCopy.SelectedValue)
            Call callMenuload2()

            gridbind()


        Catch ex As Exception
            UtilityObj.Errorlog("Business Unit Change", "accAddTabRights")

        End Try
    End Sub

    Private Sub callMenuload2()
        Try

            Dim ds As New DataSet
            Dim busUnit As String = ddlBsu.SelectedValue
            Dim roleId As String
            roleId = ddlRoleCopy.SelectedValue
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Using conn As SqlConnection = New SqlConnection(connStr)
                Dim sql As String = "SELECT  TABS_M.TAB_CODE AS TAB_CODE, TABS_M.TAB_TEXT AS TAB_TEXT, TABS_M.TAB_NAME AS TAB_NAME, TABS_M.TAB_MNU_CODE as TAB_PARENTID, " & _
                        " TABS_M.TAB_MODULE + '_' + CASE WHEN str(TABSRIGHTS_S.TAR_RIGHT) IS NULL THEN '0' ELSE ltrim(str(TABSRIGHTS_S.TAR_RIGHT)) " & _
                        " END AS TAB_MODULE FROM  TABS_M LEFT OUTER JOIN TABSRIGHTS_S ON TABS_M.TAB_CODE = TABSRIGHTS_S.TAB_CODE and TABSRIGHTS_S.TAR_ROL_ID='" & roleId & "' and TABSRIGHTS_S.TAR_BSU_ID='" & busUnit & "'"
                Dim da As SqlDataAdapter = New SqlDataAdapter(sql, conn)

                da.Fill(ds)
                da.Dispose()
            End Using

            ds.DataSetName = "TABRIGHTS"
            ds.Tables(0).TableName = "TABRIGHT"
            Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("TABRIGHT").Columns("TAB_CODE"), ds.Tables("TABRIGHT").Columns("TAB_PARENTID"), True)
            relation.Nested = True
            ds.Relations.Add(relation)

            XmlDataSourceMenu.Data = ds.GetXml()
            tvmenu.DataBind()
            ViewState("toolStatus") = 1
            Session("dt") = CreateDataTable()
            Call callgridBind(ddlRoleCopy.SelectedValue, busUnit)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Process could be completed"
        End Try
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Call callMenuCopy()
    End Sub
    Private Sub callMenuCopy()
        Try

            Dim ds As New DataSet
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim BSU_ID As String = ddlBusUnit.SelectedValue
            Dim rol_id As String = ddlRoleCopy.SelectedValue
            Using conn As SqlConnection = New SqlConnection(connStr)
                Dim sql As String = " SELECT  TABS_M.TAB_CODE AS TAB_CODE, TABS_M.TAB_TEXT AS TAB_TEXT, TABS_M.TAB_NAME AS TAB_NAME, TABS_M.TAB_MNU_CODE as TAB_PARENTID, " & _
                         " TABS_M.TAB_MODULE + '_' + CASE WHEN str(TABSRIGHTS_S.TAR_RIGHT) IS NULL THEN '0' ELSE ltrim(str(TABSRIGHTS_S.TAR_RIGHT)) " & _
                    " END AS TAB_MODULE FROM  TABS_M LEFT OUTER JOIN TABSRIGHTS_S ON TABS_M.TAB_CODE = TABSRIGHTS_S.TAB_CODE and TABSRIGHTS_S.TAR_ROL_ID='" & rol_id & "' and TABSRIGHTS_S.TAR_BSU_ID='" & BSU_ID & "'"

                Dim da As SqlDataAdapter = New SqlDataAdapter(sql, conn)

                da.Fill(ds)
                da.Dispose()
            End Using

            ds.DataSetName = "TABRIGHTS"
            ds.Tables(0).TableName = "TABRIGHT"
            Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("TABRIGHT").Columns("TAB_CODE"), ds.Tables("TABRIGHT").Columns("TAB_PARENTID"), True)
            relation.Nested = True
            ds.Relations.Add(relation)

            XmlDataSourceMenu.Data = ds.GetXml()
            tvmenu.DataBind()
            ViewState("toolStatus") = 1
            Call callgridBind(ddlRoleCopy.SelectedValue, ddlBusUnit.SelectedValue)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Process could be completed"
        End Try
    End Sub
End Class
