Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class Accounts_AccPettycashexpenseview
    Inherits System.Web.UI.Page


    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Public viewUrl As String
    'Version            DoneBY              Date                Change
    '1.1                Swapna              31-Jan-11           To change approval screen

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Session("sModule") = "SS" Then
            Me.MasterPageFile = "../mainMasterPageSS.master"
        Else
            Me.MasterPageFile = "../mainMasterPage.master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200356" And ViewState("MainMnu_code") <> "A200357" And ViewState("MainMnu_code") <> "A200358" And ViewState("MainMnu_code") <> "U000024" And ViewState("MainMnu_code") <> "U000044" And ViewState("MainMnu_code") <> "U000046") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
           
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gvStatus.Attributes.Add("bordercolor", "#1b80b6")
            If ViewState("MainMnu_code").Equals("A200357") Or ViewState("MainMnu_code").Equals("U000044") Then
                hlAddNew.Visible = False
                If CheckgApproval().Equals(False) Then
                    ' Exit Sub
                End If
            End If
            If ViewState("MainMnu_code").Equals("A200358") Or ViewState("MainMnu_code").Equals("U000046") Then   ' Menu for printing petty expense voucher by Finance dept.
                hlAddNew.Visible = False
                'No validation required for finance approval----so commented----
                'If CheckgApproval().Equals(False) Then
                '    Exit Sub
                'End If
            End If
            gridbind()

            Dim url As String
            ViewState("datamode") = "add"
            Dim Queryusername As String = Session("sUsr_name")
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            url = "AccPettycashexpense.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & dataModeAdd
            '.aspx
            'AccPettycashexpense
            hlAddNew.NavigateUrl = url


        End If
        
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function





    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
        'gvChild.Visible = False
        'gvDetails.Visible = False
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
           


            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ' hlview.Attributes.Add("onmouseover", "return  fetchId('" & lblGUID.Text & "','" & gvStatus.ClientID & "','" & gvStatus.Rows.Count & "')")
                Dim term As String = ""
                term = lblGUID.Text.ToString

                hlview.Attributes.Add("onmouseover", "return filterTable('" & lblGUID.Text & "','" & gvStatus.ClientID & "',0)")
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If ViewState("MainMnu_code").Equals("A200356") Or ViewState("MainMnu_code").Equals("U000024") Then
                    viewUrl = "AccPettycashexpense.aspx?viewid="
                ElseIf (ViewState("MainMnu_code").Equals("A200357") Or ViewState("MainMnu_code").Equals("A200358") Or ViewState("MainMnu_code").Equals("U000044") Or ViewState("MainMnu_code").Equals("U000046")) Then
                    viewUrl = "AccPettycashexpenseapprove.aspx?viewid="
                End If
                hlview.NavigateUrl = viewUrl & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&viewtype=" & "PC"
               
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If
        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function CheckgApproval() As Boolean
        Dim ds As New DataTable
        Dim sqlQuery As String
        'sqlQuery = " SELECT APM_USR_ID FROM vw_OSO_APPROVAL_M WHERE APM_USR_ID = '" & Session("sUsr_id") & "'  " _
        '& " AND APM_BSU_ID = '" & Session("sBsuid") & "' "

        'V1.1 -Added Session("sUsr_name") in place of  Session("sUsr_id") in below code
        sqlQuery = " SELECT APM_ORDERID  FROM vw_OSO_APPROVAL_M WHERE APM_USR_ID = '" & Session("sUsr_name") & "'" _
                    & " AND APM_BSU_ID = '" & Session("sBsuid") & "' AND APM_DOC_ID = 'PC'"

        ds = MainObj.ListRecords(sqlQuery, "mainDB")

        If ds.Rows.Count.Equals(0) Then
            lblError.Text = " No privilege to Approve..!"
            hlAddNew.Visible = False
            Session("ORDERID") = "0"
            RdForword.Visible = False
            Return False

        End If
        Session("ORDERID") = ds.Rows(0)("APM_ORDERID").ToString()
        Return True
    End Function
    Private Sub statusBind()
        Dim ds As New DataTable
        Dim sqlQuery As String

        sqlQuery = " SELECT APS_DOC_ID,APS_USR_ID,case when V.APS_STATUS<>'N' then REPLACE(CONVERT(VARCHAR(50), APS_DATE, 13), ' ', ' ') else null end AS APS_DATE,APS_REMARKS, V.APS_STATUS,V.APS_ORDERID,APS_DOCTYPE,V.APS_BSU_ID  FROM vw_OSO_APPROVAL_S V " _
                & " where V.APS_DOCTYPE='PC' AND  V.APS_BSU_ID = '" & Session("sBsuid") & "' " _
                & " Union all " _
                & " select PCH_ID as APS_DOC_ID ,PCH_USER_ID as APS_USR_ID,REPLACE(CONVERT(VARCHAR(50), PCH_DATE, 13), ' ', ' ')   as APS_DATE,PCH_REMARKS as APS_REMARKS,PCH_STATUS as APS_STATUS,0,'PC', " _
                & " PCH_BSU_ID as APS_BSU_ID FROM dbo.PETTYEXP_H  H where H.PCH_STATUS='N' and H.PCH_bForward=0 " _
                & " AND  H.PCH_BSU_ID = '" & Session("sBsuid") & "'  order by APS_orderID "
        ds = MainObj.ListRecords(sqlQuery, "mainDB")
        gvStatus.DataSource = ds
        gvStatus.DataBind()

    End Sub
    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrDocno, lstrEmpname, lstrDocDate, lstrNarration, lstrAmount, lstrRemarks As String
            Dim lstrOpr, lstrFiltDocno, lstrFiltreEmpname, lstrFiltDocDate, lstrFiltNarration, lstrFiltAmount, lstrFiltreRemarks As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            Dim ds As New DataTable
            lstrDocno = ""
            lstrEmpname = ""
            lstrDocDate = ""
            lstrNarration = ""
            lstrFiltreEmpname = ""
            lstrAmount = ""
            lstrFiltDocDate = ""
            lstrFiltNarration = ""
            lstrFiltAmount = ""
            lstrFiltDocno = ""
            lstrFiltreRemarks = ""
            lstrRemarks = ""

            statusBind()

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1111   Docno
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                lstrDocno = Trim(txtSearch.Text)

                '   -- 444 Emp
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmploye")
                lstrEmpname = Trim(txtSearch.Text)

                '333---Narattion
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarrationF")
                lstrRemarks = Trim(txtSearch.Text)
                '   -- 2222  DocDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
                lstrDocDate = txtSearch.Text

                '   -- 5555  Cash Type
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtExpensetype")
                lstrNarration = txtSearch.Text

                '- 6666 Amount
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrAmount = txtSearch.Text

            End If
            Dim str_Filter As String = ""
            Dim str_User As String = " AND PCH_BSU_ID = '" & Session("sBsuid") & "' "
            Dim str_ListDoc As String = String.Empty
            If Session("ListDays") IsNot Nothing Then
                'If String.Compare(Session("ListDays"), "all", True) <> 0 Then
                str_ListDoc = " AND PCH_DATE BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
            End If


            Dim strFapprove As String = "0"
            Dim forBit As String = "0"
            If RdApprove.Checked Then
                strFapprove = "A"
            ElseIf RdRejected.Checked Then
                strFapprove = "R"
            ElseIf rbUnposted.Checked Then
                strFapprove = "Open"
            ElseIf rbAll.Checked Then
                strFapprove = "All"
            ElseIf rbPosted.Checked Then
                strFapprove = "Posted"
            ElseIf RdForword.Checked Then
                strFapprove = "Forwarded"
            End If


            If rbAll.Checked = True Then
                str_Filter = str_ListDoc
            End If
            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If
            ' str_Filter += " AND '" & Session("sUsr_id") & "' IN (SELECT APM_USR_ID FROM vw_OSO_APPROVAL_M)"

            Dim sqlStr As String
            Dim param(10) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@user", Session("sUsr_name"), SqlDbType.VarChar)
            param(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@Option", strFapprove, SqlDbType.VarChar)
            param(3) = Mainclass.CreateSqlParameter("@DocNo", lstrDocno, SqlDbType.VarChar)
            param(4) = Mainclass.CreateSqlParameter("@DocDate", lstrDocDate, SqlDbType.VarChar)
            param(5) = Mainclass.CreateSqlParameter("@Narration", lstrRemarks, SqlDbType.VarChar)
            param(6) = Mainclass.CreateSqlParameter("@Employee", lstrEmpname, SqlDbType.VarChar)
            param(7) = Mainclass.CreateSqlParameter("@ExpenseType", lstrNarration, SqlDbType.VarChar)
            param(8) = Mainclass.CreateSqlParameter("@Amount", lstrAmount, SqlDbType.VarChar)


            If ViewState("MainMnu_code").Equals("A200356") Or ViewState("MainMnu_code").Equals("U000024") Then
                RdForword.Visible = True
                sqlStr = "GetPettyExpenseDataOfEmployee"
            Else
                RdForword.Visible = False
                sqlStr = "GetPettyExpenseDataForApproval"
            End If

            ds = Mainclass.getDataTable(sqlStr, param, str_conn)
            gvJournal.DataSource = ds
            If ds.Rows.Count = 0 Then
                ds.Rows.Add()
                'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                PanelView.Visible = False


            Else
                gvJournal.DataBind()
                If gvStatus.Rows.Count > 0 Then
                    PanelView.Visible = True
                End If
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
            txtSearch.Text = lstrDocDate

            txtSearch = gvJournal.HeaderRow.FindControl("txtExpensetype")
            txtSearch.Text = lstrNarration

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrFiltDocno

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmploye")
            txtSearch.Text = lstrEmpname

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrAmount
            gvJournal.SelectedIndex = p_selected_id

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub





    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()

    End Sub


    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        gridbind()

    End Sub


    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        gridbind()

    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvJournal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub

    Public x As Integer = 1
    Private Sub PrintVoucher(ByVal printId As Int32)
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "PrintTreasuryTransfer"
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            cmd.Parameters.Add("@IRFID", SqlDbType.Int).Value = printId
            cmd.Parameters.Add("@BUSID", SqlDbType.VarChar).Value = Session("sBsuid").ToString()

            params("userName") = Session("sUsr_name")
            params("VoucherName") = "FUND TRANSFER MEMO"
            params("reportHeading") = "FUND TRANSFER MEMO"
            repSource.Parameter = params
            repSource.VoucherName = "FUND TRANSFER MEMO"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptTreasuryTransferReport.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/RptViewer.aspx", True)

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvStatus_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        e.Row.ID = "TR" + x.ToString()
        e.Row.Cells(0).ID = "TD" + x.ToString()
        x = x + 1
        If Not e.Row.RowIndex.Equals(-1) Then
            ' e.Row.Cells(1).Text = Convert.ToDateTime(e.Row.Cells(1).Text.ToString()) '.ToString("dd/MMM/yyyy hh:mm tt ")
        End If


    End Sub

    Protected Sub RdForword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Private Function NumberToWords() As String

        '        local stringvar  total ;
        'stringvar amountInWords;
        'stringvar array currencies;
        'total := CStr(Sum ({PrintTreasuryTransfer;1.ITF_AMOUNT}));
        'currencies := Split (total,".");
        'amountInWords :=  UpperCase (Replace (Replace (ToWords (ToNumber(currencies[1])),"and xx / 100" ,""), "-"," "));
        'if ToNumber(currencies[2]) > 0 then 
        'amountInWords + " and " + UpperCase (Replace (Replace (ToWords (ToNumber(currencies[2])),"and xx / 100" ,""), "-"," ")) +" ONLY"
        '        Else
        '            amountInWords(+" ONLY ")

        Return True


    End Function
    Protected Sub RdApprove_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub rbAll_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.DataBinding

    End Sub
End Class
