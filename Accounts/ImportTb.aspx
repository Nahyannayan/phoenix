<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ImportTb.aspx.vb" Inherits="Accounts_ImportTb" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
        function getFile() {

            var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
            document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Import TB
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" />&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtHDocdate"
                                            ErrorMessage="Invalid Date" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="drpBsunit" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">File Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"></asp:FileUpload></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Currency</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="drpCurrency" runat="server">
                                        </asp:DropDownList>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpCurrency"
                                            ErrorMessage="Invalid Currency" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Exchange Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtExchangerate" runat="server" AutoCompleteType="Disabled" MaxLength="18"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtExchangerate"
                                            ErrorMessage="Exchange Rate Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="2">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload Excel" CausesValidation="False"
                                            TabIndex="30" OnClick="btnFind_Click" OnClientClick="getFile()" />
                                        <asp:RadioButton ID="RdCheck" runat="server" AutoPostBack="True" GroupName="rd" OnCheckedChanged="RdCheck_CheckedChanged"
                                            Text="Current View"></asp:RadioButton>
                                        <asp:RadioButton ID="RdExcel" runat="server" AutoPostBack="True"
                                            GroupName="rd" OnCheckedChanged="RdExcel_CheckedChanged"
                                            Text="Excel"></asp:RadioButton>
                                        <asp:RadioButton ID="RdExcandAc" runat="server" AutoPostBack="True"
                                            GroupName="rd" Text="Accounts with Excel" OnCheckedChanged="RdExcandAc_CheckedChanged"></asp:RadioButton>

                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left">
                                        <asp:Label ID="labdetailHead" runat="server" EnableViewState="False">Details</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="matters">
                                        <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                            AutoGenerateColumns="False" OnSelectedIndexChanged="gvExcel_SelectedIndexChanged"
                                            CssClass="table table-bordered table-row" OnRowDataBound="gvExcel_RowDataBound" ShowFooter="True">
                                            <FooterStyle />
                                            <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                            <Columns>
                                                <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="TransDate" SortExpression="TransDate" HeaderText="Date">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField ReadOnly="True" DataField="AccountType" SortExpression="AccountType" HeaderText="Account Type">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AccountNo" SortExpression="AccountNo" HeaderText="Account No">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AccountName" HeaderText="Account Name"></asp:BoundField>
                                                <asp:BoundField DataField="DRAMOUNT" HeaderText="DrAmount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CRAMOUNT" HeaderText="CrAmount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BALANCE" HeaderText="Balance">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle></SelectedRowStyle>
                                            <HeaderStyle></HeaderStyle>

                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table id="Table1" align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4"></td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" CausesValidation="False" TabIndex="24" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26" />

                                        <br />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Editid" runat="server" Value="0" />
                <asp:HiddenField ID="HidUpload" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <script>
 
                </script>

            </div>
        </div>
    </div>

</asp:Content>

