Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class NewCurrency
    Inherits System.Web.UI.Page


    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150001"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                viewstate("datamode") = "none"
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or (MainMnu_code <> "A100015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            txtDate.Text = Format(Now.Date, "dd/MMM/yyyy")
            If Request.QueryString("editid") = "" Then
                'tbl_AddCurrency.Visible = True
                'tbl_ModifyCurrency.Visible = False
                set_view_data()
            Else
                'tbl_AddCurrency.Visible = False
                'tbl_ModifyCurrency.Visible = True
                reset_view_data()
                ViewState("datamode") = "view"
                Dim encObj As New Encryption64
                Dim str_curid As String = encObj.Decrypt(Request.QueryString("editid").Replace(" ", "+"))

                setModifyvalues(str_curid)
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
    End Sub

    'Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    lblError.Text = ""
    '    Dim dblRate As Double
    '    Try
    '        dblRate = CDbl(txtExchangeRate.Text & "")
    '    Catch ex As Exception
    '        lblError.Text = "Invalid Rate"
    '        Exit Sub
    '    End Try
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '    Dim objConn As New SqlConnection(str_conn)
    '    Dim str_type As String = ""
    '    objConn.Open()

    '    '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '    Try

    '        Dim str_success As String = fnDoGroupOperations("Insert", txtCurrencyCode.Text & "", txtCurrencyName.Text & "", txtExchangeRate.Text & "", txtSmallerDenom.Text & "")
    '        If (str_success = "0") Then
    '            Dim encObj As New Encryption64
    '            Response.Redirect("accCurrencyMaster.aspx?modified=" & encObj.Encrypt("274") & "&newid=" & txtCurrencyCode.Text & "")

    '            'lblError.Text = displayMessage("274", 20, "A")

    '            'txtCurrencyCode.Text = ""
    '            'txtCurrencyName.Text = ""
    '            'txtExchangeRate.Text = ""
    '            'txtSmallerDenom.Text = ""
    '        Else
    '            lblError.Text = getErrorMessage(str_success)
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub

    Sub set_view_data()
        txtCurrencyCode.Attributes.Remove("readonly")
        txtCurrencyName.Attributes.Remove("readonly")
        txtDate.Attributes.Remove("readonly")
        txtSmallerDenom.Attributes.Remove("readonly")
    End Sub

    Sub reset_view_data()
        txtCurrencyCode.Attributes.Add("readonly", "readonly")
        txtCurrencyName.Attributes.Add("readonly", "readonly")
        txtDate.Attributes.Add("readonly", "readonly")
        txtSmallerDenom.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "select * FROM CURRENCY_M where CUR_ID='" & p_Modifyid & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                txtCurrencyCode.Text = p_Modifyid
                txtCurrencyName.Text = ds.Tables(0).Rows(0)("CUR_DESCR")
                txtSmallerDenom.Text = ds.Tables(0).Rows(0)("CUR_DENOMINATION")
                'txtExchangeRate.Text = ds.Tables(0).Rows(0)("CUR_EXGRATE")
                'txtMGroupname.Text = ds.Tables(0).Rows(0)("GPM_DESCR")

            Else
                Response.Redirect("accCurrencyMaster.aspx")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub

    'Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
    '    lblMError.Text = ""
    '    Dim dblRate As Double
    '    Try
    '        dblRate = CDbl(txtMExchangeRate.Text & "")
    '    Catch ex As Exception
    '        lblMError.Text = "Invalid Rate"
    '        Exit Sub
    '    End Try
    '    Dim str_success As String = fnDoGroupOperations("Update", txtMCurrencyCode.Text & "", txtMCurrencyName.Text & "", txtMExchangeRate.Text & "", txtMSmallerDenom.Text & "")
    '    If (str_success = "0") Then
    '        'lblMError.Text = displayMessage("258", 20, "D")
    '        Dim encObj As New Encryption64
    '        Response.Redirect("accCurrencyMaster.aspx?modified=" & encObj.Encrypt("270"))
    '    Else
    '        lblMError.Text = getErrorMessage(str_success)
    '    End If
    'End Sub
    Sub clear_all()
        txtCurrencyCode.Text = ""
        txtCurrencyName.Text = ""
        txtDate.Text = ""
        txtSmallerDenom.Text = ""
        'txtExchangeRate.Text = ""
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call clear_all()
        set_view_data()
        viewstate("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        viewstate("datamode") = "edit"
        txtCurrencyCode.Attributes.Add("readonly", "readonly")
        txtSmallerDenom.Attributes.Remove("readonly")
        txtCurrencyName.Attributes.Remove("readonly")
        txtDate.Attributes.Remove("readonly")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If viewstate("datamode") = "add" Then
            Response.Redirect(viewstate("ReferrerUrl"))
        End If

        If viewstate("datamode") = "edit" Then

            Call clear_all()
            'clear the textbox and set the default settings
            Dim str_curid As String = Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+"))

            setModifyvalues(str_curid)
            viewstate("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
        Else
            Response.Redirect(viewstate("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If viewstate("datamode") = "edit" Then
            lblError.Text = ""
            Dim dblRate As Double
            Try
                dblRate = CDbl(txtExchangeRate.Text & "")
            Catch ex As Exception
                lblError.Text = "Invalid Rate"
                Exit Sub
            End Try
            Dim str_success As String = MasterFunctions.fnDoCurrencyOperations("Update", txtCurrencyCode.Text & "", txtCurrencyName.Text & "", txtExchangeRate.Text & "", txtSmallerDenom.Text & "")
            If (str_success = "0") Then
                'lblMError.Text = displayMessage("258", 20, "D")
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtCurrencyCode.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                Dim encObj As New Encryption64

                viewstate("datamode") = Encr_decrData.Encrypt(viewstate("datamode"))
                Response.Redirect("accCurrencyMaster.aspx?modified=" & encObj.Encrypt("270") & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            Else
                lblError.Text = getErrorMessage(str_success)
            End If
        Else
            lblError.Text = ""
            Dim dblRate As Double
            Try
                dblRate = CDbl(txtExchangeRate.Text & "")
            Catch ex As Exception
                lblError.Text = "Invalid Rate"
                Exit Sub
            End Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim str_type As String = ""
            objConn.Open()

            '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                Dim str_success As String = MasterFunctions.fnDoCurrencyOperations("Insert", txtCurrencyCode.Text & "", txtCurrencyName.Text & "", txtExchangeRate.Text & "", txtSmallerDenom.Text & "")
                If (str_success = "0") Then
                    Dim encObj As New Encryption64
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtCurrencyCode.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If

                    Response.Redirect("accCurrencyMaster.aspx?modified=" & encObj.Encrypt("274") & "&newid=" & txtCurrencyCode.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code"))

                    'lblError.Text = displayMessage("274", 20, "A")

                    'txtCurrencyCode.Text = ""
                    'txtCurrencyName.Text = ""
                    'txtExchangeRate.Text = ""
                    'txtSmallerDenom.Text = ""
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub
 
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
            If Not Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) Is Nothing Then
                Dim str_success As String = MasterFunctions.fnDoCurrencyOperations("Delete", Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) & "", "", "0", "")
                If (str_success = "0") Then
                    lblError.Text = getErrorMessage("278")

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtCurrencyCode.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    clear_all()
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
