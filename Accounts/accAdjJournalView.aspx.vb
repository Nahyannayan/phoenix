﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accAdjJournalView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            If Session("sUsr_name") Is Nothing Or Session("sBsuid") Is Nothing Or (ViewState("MainMnu_code").ToString <> "A150100" And ViewState("MainMnu_code").ToString <> "A150110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case Is = "A150100"
                    hlAddNew.NavigateUrl = "accAdjJournalAdd.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    tr_SelBusinessUnit.Visible = True
                Case Is = "A150110"
                    hlAddNew.NavigateUrl = "accAdjJournalDAdd.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    tr_SelBusinessUnit.Visible = False
                    gvJournal.Columns(0).Visible = False
            End Select
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            ddlBusinessunit.DataBind() 
            If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlBusinessunit.SelectedIndex = -1
                ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            End If
            gridbind()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Private Sub gridbind()
        Try
            Dim str_filter As String = String.Empty
            Dim lstrDocNo As String = String.Empty
            Dim lstrDocDate As String = String.Empty
            Dim lstrNarration As String = String.Empty
            Dim lstrOppAccount As String = String.Empty
            Dim lstrAccount As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim lstrChqNo As String = String.Empty
            Dim lstrAmount As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables 
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "JAL_DOCNO", lstrDocNo)
                '   -- 2  DocDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
                lstrChqNo = txtSearch.Text
                If (lstrChqNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "JAL_DEBIT", lstrChqNo)
                '   -- 3  DocDate
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtOPPBSU_NAME")
                lstrDocDate = txtSearch.Text
                If (lstrDocDate <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "OPPBSU_NAME", lstrDocDate)
                '   -- 4  Narration
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
                lstrNarration = txtSearch.Text
                If (lstrNarration <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "JAL_NARRATION", lstrNarration)
                '   -- 5  Amount
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrAmount = txtSearch.Text
                If (lstrAmount <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "JAL_CREDIT", lstrAmount)
                '   -- 6  Account
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAccount")
                lstrAccount = txtSearch.Text
                If (lstrAccount <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "Account", lstrAccount)
                '   -- 7  OppAccount
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtOppAccount")
                lstrOppAccount = txtSearch.Text
                If (lstrOppAccount <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "OppAccount", lstrOppAccount)

            End If
            Dim str_Sql As String = String.Empty
            Select Case ViewState("MainMnu_code").ToString
                Case Is = "A150100"
                    str_filter = str_filter & " AND JAL_bExclude = 0 AND JAL_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "' "
                Case Is = "A150110"
                    str_filter = str_filter & " AND JAL_bExclude = 1 AND JAL_BSU_ID = '" & Session("sBsuid") & "' "
            End Select
            str_Sql = "SELECT * FROM vw_osa_JOURNAL_ADJ_D " _
                & " WHERE 1=1 " & str_filter _
                & " ORDER BY JAL_BSU_ID DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrDocNo
            txtSearch = gvJournal.HeaderRow.FindControl("txtOPPBSU_NAME")
            txtSearch.Text = lstrDocDate
            txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
            txtSearch.Text = lstrChqNo
            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            txtSearch.Text = lstrNarration
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrAmount
            txtSearch = gvJournal.HeaderRow.FindControl("txtAccount")
            txtSearch.Text = lstrAccount
            txtSearch = gvJournal.HeaderRow.FindControl("txtOppAccount")
            txtSearch.Text = lstrOppAccount
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

End Class

