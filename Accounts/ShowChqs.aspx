<%@ Page Language="VB" Theme="General" AutoEventWireup="false" CodeFile="ShowChqs.aspx.vb" Inherits="ShowChqs" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accounts</title>
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">



</head>
<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">

        <table width="98%" id="tbl" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td >
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                </td>
            </tr>

            <tr >
                <td align="center">
                    <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="CHB_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="50" CssClass="table table-bordered table-row">
                        <Columns>

                            <asp:TemplateField SortExpression="CHB_ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblCHBId" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="CHB_LOTNo">
                                <HeaderTemplate>
                                    Cheque Book
                                    <br />
                                    <asp:TextBox ID="txtCHBLot" SkinID="Gridtxt" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblCHBLotNo" runat="server" Text='<%# Bind("CHB_LOTNo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="left" />

                            </asp:TemplateField>
                            <asp:TemplateField Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblChdNo" runat="server" Text='<%# Bind("CHD_No") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Cheque No
                                    <br />
                                    <asp:TextBox ID="txtChqNo" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnOldDocNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblChqNo" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("CHD_No") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem"  />
                        <HeaderStyle CssClass="gridheader_pop"  />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <SelectedRowStyle />
                        <PagerStyle  HorizontalAlign="Left" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
