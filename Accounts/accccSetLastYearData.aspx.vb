Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_SetLastYearData
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "A150088" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            'blnEdit = False
            h_BSUID.Value = IIf(Session("SBSUID") <> "", Session("SBSUID").ToString(), String.Empty)
            ddBusinessunit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSU_ID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedValue = Session("sbsuid")
            lblError.Text = ""
            FillACD()
            FillReports()
            FillGrid()
            GetLastData()
           
        End If
    End Sub
    Private Sub FillACD() 
        Dim DCT As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim param As New SqlParameter("@CUR_FIN_YEAR", SqlDbType.BigInt)
        param.Value = Session("F_Year")
        DCT = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_FINANCIALYEAR_S", param)
        ddlAcademicYear.DataTextField = "FIN_ID"
        ddlAcademicYear.DataValueField = "FIN_ID"
        ddlAcademicYear.DataSource = DCT
        ddlAcademicYear.DataBind()
    End Sub
    Private Sub FillReports()
        Dim strQuery As String
        Dim ds As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        strQuery = "SELECT RSM_TYP,RSM_DESCR FROM RPTSETUP_M"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        ddlReports.DataTextField = "RSM_DESCR"
        ddlReports.DataValueField = "RSM_TYP"
        ddlReports.DataSource = ds
        ddlReports.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim strSql As String 
        Dim dsRpt As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If ddlReports.SelectedValue <> "" Then
            strSql = "SELECT RSS_ID,RSS_TYP,RSS_DESCR FROM RPTSETUP_S WHERE RSS_TYP='" & ddlReports.SelectedValue & "' AND RSS_DESCR<>''"
            dsRpt = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            gvAmountDetails.DataSource = dsRpt
            gvAmountDetails.DataBind()


        End If
    End Sub

    Protected Sub ddlReports_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillGrid()
        GetLastData()

    End Sub

    Protected Sub gvAmountDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAmountDetails.PageIndexChanging
        'gvAmountDetails.PageIndex = e.NewPageIndex
        'FillGrid()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim Trans As SqlTransaction = objConn.BeginTransaction
        Try
            If ddBusinessunit.SelectedValue = "" Then
                lblError.Text = "Specify BusinessUnit"
                Exit Sub
            End If
            If ddlAcademicYear.SelectedValue = "" Then
                lblError.Text = "Specify Academic Year"
                Exit Sub
            End If
            If ddlReports.SelectedValue = "" Then
                lblError.Text = "Specify Report Type"
                Exit Sub
            End If
            
            If gvAmountDetails.Rows.Count > 0 Then
                For i = 0 To gvAmountDetails.Rows.Count - 1
                    Dim ctlAmount As TextBox
                    ctlAmount = gvAmountDetails.Rows(i).FindControl("txtAmount")
                    If IsNumeric(ctlAmount.Text) = False And ctlAmount.Text <> "" Then
                        lblError.Text = "Amount must be Numeric value"
                        Exit Sub
                    End If
                Next

            End If


            Dim cmd1 As SqlCommand
            cmd1 = New SqlCommand("DeleteLASTYEARDATA", objConn, Trans)
            cmd1.CommandType = CommandType.StoredProcedure

            cmd1.Parameters.AddWithValue("@FYR_ID", ddlAcademicYear.SelectedValue)
            cmd1.Parameters.AddWithValue("@FYR_TYPE", ddlReports.SelectedValue)
            cmd1.Parameters.AddWithValue("@FYR_BSU_ID", ddBusinessunit.SelectedValue)
            
            cmd1.ExecuteNonQuery()

            Dim cmd2 As SqlCommand
            For i = 0 To gvAmountDetails.Rows.Count - 1
                
                Dim ctlAmount As TextBox
                Dim lblRSSID As Label
                ctlAmount = gvAmountDetails.Rows(i).FindControl("txtAmount")
                lblRSSID = gvAmountDetails.Rows(i).FindControl("lblID")
                If ctlAmount.Text = "" Then
                    ctlAmount.Text = "0"
                End If
                cmd2 = New SqlCommand("SaveLASTYEARDATA", objConn, Trans)
                cmd2.CommandType = CommandType.StoredProcedure
                cmd2.Parameters.AddWithValue("@FYR_ID", ddlAcademicYear.SelectedValue)
                cmd2.Parameters.AddWithValue("@FYR_TYPE", ddlReports.SelectedValue)
                cmd2.Parameters.AddWithValue("@FYR_BSU_ID", ddBusinessunit.SelectedValue)
                'cmd2.Parameters.AddWithValue("@FYR_RSS_ID", gvAmountDetails.Rows(i).Cells(0).Text)
                cmd2.Parameters.AddWithValue("@FYR_RSS_ID", lblRSSID.Text)
                cmd2.Parameters.AddWithValue("@FYR_AMOUNT", Convert.ToDecimal(ctlAmount.Text))
                cmd2.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd2.ExecuteNonQuery()
                iReturnvalue = CInt(cmd2.Parameters("@ReturnValue").Value)

            Next
            Trans.Commit()
            
        Catch ex As Exception
            Trans.Rollback()
            lblError.Text = "UnExpected Error"
        Finally
            objConn.Close()
        End Try
        If lblError.Text = "" Then
            lblError.Text = "Successfully Saved"
        End If
    End Sub
    Private Sub GetLastData()
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer
        Dim lstrconn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If ddBusinessunit.SelectedValue <> "" And ddlAcademicYear.SelectedValue <> "" And ddlReports.SelectedValue <> "" Then
            If gvAmountDetails.Rows.Count > 0 Then
                For i = 0 To gvAmountDetails.Rows.Count - 1
                    Dim ctlAmount As TextBox
                    Dim lblRSSID As Label
                    lblRSSID = gvAmountDetails.Rows(i).FindControl("lblID")
                    ctlAmount = gvAmountDetails.Rows(i).FindControl("txtAmount")
                    strSql = "SELECT FYR_AMOUNT FROM FYR_OPENING WHERE FYR_ID=" & ddlAcademicYear.SelectedValue & " AND FYR_BSU_ID='" & ddBusinessunit.SelectedValue & "' AND FYR_TYP='" & ddlReports.SelectedValue & "' AND FYR_RSS_ID='" & lblRSSID.Text & "'"
                    ds = SqlHelper.ExecuteDataset(lstrconn, CommandType.Text, strSql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        ctlAmount.Text = IIf(ds.Tables(0).Rows(0)("FYR_AMOUNT") = 0, "", ds.Tables(0).Rows(0)("FYR_AMOUNT"))
                    Else
                        ctlAmount.Text = ""
                    End If
                Next
            End If
        End If

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillGrid()
        GetLastData()
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillGrid()
        GetLastData()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddBusinessunit.SelectedValue <> "" And ddlAcademicYear.SelectedValue <> "" And ddlReports.SelectedValue <> "" Then
            Dim cmd2 As SqlCommand
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim Trans As SqlTransaction = objConn.BeginTransaction
            cmd2 = New SqlCommand("DeleteLASTYEARDATA", objConn, Trans)
            cmd2.CommandType = CommandType.StoredProcedure

            cmd2.Parameters.AddWithValue("@FYR_ID", ddlAcademicYear.SelectedValue)
            cmd2.Parameters.AddWithValue("@FYR_TYPE", ddlReports.SelectedValue)
            cmd2.Parameters.AddWithValue("@FYR_BSU_ID", ddBusinessunit.SelectedValue)
            cmd2.ExecuteNonQuery()
            Trans.Commit()
            lblError.Text = "Successfully Deleted"
        End If
        GetLastData()
    End Sub
End Class
