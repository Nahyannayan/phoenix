<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccTreasuryTransfer.aspx.vb" Inherits="Accounts_AccTreasuryTransfer" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/Numeric.js"></script>
      <style>
        table td input[type=text], table td select,table td textarea {
            min-width: 20% !important;
        }
    </style>
    <script type="text/javascript" language="javascript">


        function getWords() {
            var AmtWords = document.getElementById('<%=labWords.ClientID %>').innerText;
            document.getElementById('<%=hidwords.ClientID %>').innerText = AmtWords;

        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Treasury Transfer
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Document No</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDocumentno" runat="server" AutoPostBack="True"
                                            ReadOnly="True">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Doc Date </span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHNarration" runat="server" TextMode="MultiLine" MaxLength="300" TabIndex="1" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td width="20%" align="left"><span class="field-label">Currency </span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="drpCurrency" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <br />

                            <table align="center" width="100%" class="table table-bordered table-row">
                                <tr >
                                    <th valign="middle" align="center" colspan="2">Transfer From</th>
                                    <th valign="middle" align="center" colspan="2">Received by</th>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label"><span class="field-label">Bsu Unit</span></span></td>
                                    <td valign="middle" align="left" width="30%">
                                        <asp:DropDownList ID="drpBusinessunitF" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpBusinessunitF_SelectedIndexChanged" TabIndex="2"></asp:DropDownList>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpBusinessunitF"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details" InitialValue="0">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%" valign="middle"><span class="field-label">Bsu Unit</span></td>
                                    <td align="left" width="30%" valign="middle">
                                        <asp:DropDownList ID="drpBusinessunitT" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpBusinessunitT_SelectedIndexChanged" TabIndex="4">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator5" runat="server" ControlToValidate="drpBusinessunitT"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details" InitialValue="0">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Bank</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="drpBankF" runat="server" OnSelectedIndexChanged="drpBankF_SelectedIndexChanged" AutoPostBack="True" TabIndex="3"></asp:DropDownList>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="drpBankF"
                                            ErrorMessage="Cash Flow Code Cannot Be Empty" ValidationGroup="Details" InitialValue="0">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"  width="20%"><span class="field-label">Bank</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="drpBankT" runat="server" OnSelectedIndexChanged="drpBankT_SelectedIndexChanged" AutoPostBack="True" TabIndex="5">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator7" runat="server" ControlToValidate="drpBankT"
                                            ErrorMessage="Cash Flow Code Cannot Be Empty" ValidationGroup="Details" InitialValue="0">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" > <span class="field-label">Exg Rate1 </span></td>
                                    <td align="left"  width="30%">
                                        <asp:TextBox  ID="txtExchangerateF" runat="server" AutoCompleteType="Disabled" MaxLength="18" Width="29%"></asp:TextBox>
                                       <strong> Exg Rate2 </strong>
                                       <asp:TextBox ID="txtExchangerateF2" runat="server" AutoCompleteType="Disabled" MaxLength="18" Width="29%"></asp:TextBox></td>
                                    <td align="left"  width="20%"><span class="field-label">Exg Rate1 </span></td>
                                    <td align="left"  width="30%">
                                        <asp:TextBox ID="txtExchangerateT" runat="server" AutoCompleteType="Disabled" MaxLength="18" Width="29%"></asp:TextBox>
                                      <strong>  Exg Rate2  </strong>
                                        <asp:TextBox ID="txtExchangerateT2" runat="server" AutoCompleteType="Disabled" MaxLength="18" Width="29%"> </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Amount </span></td>

                                    <td align="left"  width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" TabIndex="6" AutoCompleteType="Disabled" MaxLength="18"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Amount in Words </span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="labWords" runat="server" CssClass="error"></asp:Label></td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td align="center">

                                        <table width="100%">
                                            <asp:Repeater ID="RptF" runat="server">
                                                <HeaderTemplate>
                                                    <tr class="table table-bordered table-row">
                                                        <th>Bank Balance</th>
                                                        <th>Receivable Next Day</th>
                                                        <th>Payable Next 3 Days</th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="table table-bordered table-row">
                                                        <td><%#Eval("Balance")%>                                    
                                                        </td>
                                                        <td><%#Eval("PDCREcev")%></td>
                                                        <td><%#Eval("PDCPAyable")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <td align="center">
                                        <table width="100%">
                                            <asp:Repeater ID="RptT" runat="server">
                                                <HeaderTemplate>
                                                    <tr class="table table-bordered table-row">
                                                        <th>Bank Balance</th>
                                                        <th>Receivable Next Day</th>
                                                        <th>Payable Next 3 Days</th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="table table-bordered table-row">
                                                        <td><%#Eval("Balance")%>                   
                                                        </td>
                                                        <td><%#Eval("PDCREcev")%></td>
                                                        <td><%#Eval("PDCPAyable")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hidwords" runat="server" Value="0"></asp:HiddenField>
                            <asp:HiddenField ID="hidBankFamt" runat="server" Value="0"></asp:HiddenField>
                            <table id="Table1" align="center" width="100%">
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" CausesValidation="False" TabIndex="24" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');" TabIndex="28" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" TabIndex="30" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="32" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <input id="h_mode" runat="server" type="hidden" />
                <input id="h_Memberids" runat="server" type="hidden" />
                <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                <asp:HiddenField ID="postedYN" runat="server" Value="M" />
            </div>
        </div>
    </div>
</asp:Content>


