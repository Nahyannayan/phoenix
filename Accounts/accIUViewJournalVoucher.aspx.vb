Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class accIUViewJournalVoucher
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            Dim menu_rights As String
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            lblError.Text = ""
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code").ToString <> "A150025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                gvChild.Attributes.Add("bordercolor", "#1b80b6")
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = "The Journal is successfully deleted"
                End If
            End If
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            Dim url As String = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), dataModeAdd)
            hlAddNew.NavigateUrl = url
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Public Function returnpath(ByVal p_posted As Object) As String
        If TypeOf p_posted Is DBNull Then
            Return Nothing
        End If
        If p_posted Then
            Return "~/Images/tick.gif"
        Else
            Return "~/Images/cross.gif"
        End If
    End Function

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then 
                Dim MainMnu_codeEncr As String = Encr_decrData.Encrypt(ViewState("MainMnu_code").ToString)
                hlview.NavigateUrl = String.Format("accIUjournalvoucher.aspx?MainMnu_code={0}&datamode={1}&viewid=" & lblGUID.Text, MainMnu_codeEncr, Encr_decrData.Encrypt("view"))
                hlCEdit.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim lstrDocNo, lstrDocDate, lstrPartyAC, lstrBankAC, lstrNarration, lstrCurrency, lstrAmount As String
            Dim lstrFiltDocNo, lstrFiltDocDate, lstrFiltBankAC, lstrFiltNarration, lstrFiltCurrency As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrFiltDocNo = ""
            lstrFiltDocDate = ""
            lstrFiltBankAC = ""
            lstrFiltNarration = ""
            lstrFiltCurrency = ""
            lstrDocNo = ""
            lstrDocDate = ""
            lstrBankAC = ""
            lstrNarration = ""
            lstrCurrency = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                lstrDocNo = ""
                lstrDocDate = ""
                lstrPartyAC = ""
                lstrBankAC = ""
                lstrNarration = ""
                lstrCurrency = ""
                lstrAmount = ""
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "IJH.IJH_DOCNO", lstrDocNo)

                '   -- 2  DocDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
                lstrDocDate = txtSearch.Text
                If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "IJH.IJH_DOCDT", lstrDocDate)

                '   -- 3  Bank AC
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtBankAC")
                lstrBankAC = txtSearch.Text
                If (lstrBankAC <> "") Then lstrFiltBankAC = SetCondn(lstrOpr, "BUM.BSU_SHORTNAME", lstrBankAC)
                '   -- 5  Narration
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarrn")
                lstrNarration = txtSearch.Text
                If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "IJH.IJH_NARRATION", lstrNarration)
                '   -- 7  Currency
                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
                lstrCurrency = txtSearch.Text
                If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "BUC.BSU_SHORTNAME", lstrCurrency)
            End If

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim str_Filter As String = String.Empty
            Dim str_ListDoc As String = String.Empty

            If Session("ListDays") IsNot Nothing Then
                str_ListDoc = " AND IJH.IJH_DOCDT BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
            End If

            str_Filter = lstrFiltDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltNarration & lstrFiltCurrency
            Dim str_cr, str_dr As String
            str_cr = ""
            str_dr = ""
            If rbPosted.Checked = True Then
                str_cr = " AND IJH.IJH_bPOSTEDCRBSU=1"
                str_dr = " AND IJH.IJH_bPOSTEDDRBSU=1"
            End If
            If rbUnPosted.Checked = True Then
                str_cr = " AND IJH.IJH_bPOSTEDCRBSU=0"
                str_dr = " AND IJH.IJH_bPOSTEDDRBSU=0"
            End If
            If rbAll.Checked = True Then
                str_Filter += str_ListDoc
            End If
            str_Sql = "SELECT IJH.IJH_bMIRROR, IJH.GUID, IJH.IJH_DOCNO, IJH.IJH_DOCDT, IJH.IJH_NARRATION , " _
            & " IJH.IJH_CUR_ID, IJH.IJH_DIFFAMT, case when IJH.IJH_CR_BSU_ID = '" & Session("sBsuid") & "' " _
            & " and IJH.IJH_bPOSTEDCRBSU = 1 then 1 else case when  IJH.IJH_DR_BSU_ID = '" & Session("sBsuid") & "' " _
            & " and IJH.IJH_bPOSTEDDRBSU = 'true' then 1 else 0 end end POSTED ," _
            & " SUM(IJD.IJL_CREDIT) AS CREDITTOTAL, SUM(IJD.IJL_DEBIT) AS DEBITTOTAL," _
            & " BUM.BSU_SHORTNAME AS DR_BSU_NAME, BUC.BSU_SHORTNAME AS CR_BSU_NAME FROM IJOURNAL_H AS IJH INNER JOIN" _
            & " IJOURNAL_D AS IJD ON IJH.IJH_FYEAR = IJD.IJL_FYEAR  AND IJH.IJH_DOCTYPE = IJD.IJL_DOCTYPE " _
            & " AND IJH.IJH_DOCNO = IJD.IJL_DOCNO  LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M AS BUM " _
            & " ON IJH.IJH_DR_BSU_ID = BUM.BSU_ID LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M AS BUC ON IJH.IJH_CR_BSU_ID = BUC.BSU_ID " _
            & " WHERE IJH.IJH_bDELETED = 0 AND IJH.IJH_FYEAR= " & Session("F_YEAR") _
            & " and  ((IJH.IJH_CR_BSU_ID = '" & Session("sBsuid") & "'" _
            & str_cr _
            & " ) OR (IJH.IJH_DR_BSU_ID = '" & Session("sBsuid") & "'" _
            & str_dr _
            & " )) " & str_Filter _
            & " AND (IJH_ISS_BSU_ID='" & Session("sBsuid") & "' OR IJH.IJH_bPOSTEDDRBSU = 1 OR IJH.IJH_bPOSTEDCRBSU=1)" _
            & " GROUP BY IJH.IJH_bMIRROR, BUM.BSU_SHORTNAME, BUC.BSU_SHORTNAME, IJH.GUID, IJH.IJH_CR_BSU_ID, IJH.IJH_DR_BSU_ID, IJH.IJH_FYEAR," _
            & " IJH.IJH_DOCTYPE, IJH.IJH_DOCDT,  IJH.IJH_CUR_ID, IJH.IJH_bPOSTEDCRBSU, IJH.IJH_bPOSTEDDRBSU," _
            & "  IJH.IJH_DIFFAMT, IJH.IJH_NARRATION, IJH.IJH_DOCNO ORDER BY IJH.IJH_DOCDT DESC, IJH.IJH_DOCNO DESC"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                gvJournal.SelectedIndex = -1
                gvDetails.Visible = False
                gvChild.Visible = False
            Else
                gvJournal.DataBind()
                gvJournal.SelectedIndex = p_selected_id
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrDocNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
            txtSearch.Text = lstrDocDate

            txtSearch = gvJournal.HeaderRow.FindControl("txtBankAC")
            txtSearch.Text = lstrBankAC

            txtSearch = gvJournal.HeaderRow.FindControl("txtNarrn")
            txtSearch.Text = lstrNarration

            txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
            txtSearch.Text = lstrCurrency

            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message, "Bind IJV View")
        End Try
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                str_Sql = "select * FROM IJOURNAL_H where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT *, ACCOUNTS_M.ACT_NAME AS ACT_NAME FROM IJOURNAL_D INNER JOIN ACCOUNTS_M" _
                    & " ON IJOURNAL_D.IJL_ACT_ID = ACCOUNTS_M.ACT_ID WHERE IJL_DOCNO='" & ds.Tables(0).Rows(0)("IJH_DOCNO") & "'" _
                    & " AND IJL_FYEAR='" & ds.Tables(0).Rows(0)("IJH_FYEAR") _
                    & "' AND IJL_DOCTYPE='" & ds.Tables(0).Rows(0)("IJH_DOCTYPE") & "'"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.Visible = True
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbViewChild_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                lblSlno = TryCast(sender.FindControl("lblSlno"), Label)
                str_Sql = "select * FROM IJOURNAL_D where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT     *, COSTCENTER_S.CCS_DESCR AS CCS_DESCR FROM VOUCHER_D_S" _
                    & " INNER JOIN COSTCENTER_S ON VOUCHER_D_S.VDS_CCS_ID = COSTCENTER_S.CCS_ID " _
                    & "WHERE VDS_DOCNO='" & ds.Tables(0).Rows(0)("IJL_DOCNO") & "'" _
                    & " AND VDS_DOCTYPE ='" & ds.Tables(0).Rows(0)("IJL_DOCTYPE") & "' AND VDS_FYEAR =" _
                    & ds.Tables(0).Rows(0)("IJL_FYEAR") & " AND VDS_SLNO=" & lblSlno.Text
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    gvChild.Visible = True
                    gvChild.DataBind()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                Throw        'Bubble up the exception
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        gridbind()
        gvChild.DataBind()
        gvDetails.DataBind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblDocNo As Label
                    Dim lblPosted As Label
                    lblPosted = grdRow.FindControl("lblPosted")
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    repSource = VoucherReports.InterUnitVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "IJV", lblDocNo.Text, lblPosted.Text, Session("HideCC"))
                    Session("ReportSource") = repSource
                    ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                    ReportLoadSelection()

                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
