<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ACCGenBR.aspx.vb" Inherits="AccGenBR" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;
            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }


    </script>

    <script language="javascript" type="text/javascript">
        function PopUpCall(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientId %>').value, "pop_up2")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                 document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];


             }
         }
         function autoSizeWithCalendar(oWindow) {
             var iframe = oWindow.get_contentFrame();
             var body = iframe.contentWindow.document.body;
             var height = body.scrollHeight;
             var width = body.scrollWidth;
             var iframeBounds = $telerik.getBounds(iframe);
             var heightDelta = height - iframeBounds.height;
             var widthDelta = width - iframeBounds.width;
             if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
             if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
             oWindow.center();
         }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Generate Collection Receipt
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Doc Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocDate" runat="server" AutoPostBack="True"></asp:TextBox>

                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank Account</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankCode" runat="server"></asp:TextBox>
                            <a href="#" onclick="PopUpCall('460','400','BANK','<%=txtBankCode.ClientId %>','<%=txtBankDescr.ClientId %>')">
                                <img border="0" src="../Images/cal.gif" id="IMG3" language="javascript" /></a>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarration" runat="server" TabIndex="16" TextMode="MultiLine">CASH COLLECTION DEPOSIT</asp:TextBox>
                            <asp:Button ID="btnFill" runat="server" CssClass="button" Text="Fill Narration" />
                        </td>
                    </tr>
                    <tr id="tr_grid" runat="server">
                        <td align="center" width="100%" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" DataKeyNames="VHH_DOCNO" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" OnRowDataBound="gvJournal_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gvJournal_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                            </a>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                            </a>
                                        </AlternatingItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True"
                                        SortExpression="VHH_DOCNO" />
                                    <asp:BoundField DataField="VHH_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc Date"
                                        HtmlEncode="False" SortExpression="VHH_DOCDT">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="VHH_NARRATION" HeaderText="Narration" SortExpression="VHH_NARRATION" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHH_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" runat="server" />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <input type="checkbox" name="chkAL" value="Check All" onclick="ChangeAllCheckBoxStates(true);" />Select 
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="VHH_DOCNO" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DOCNO" SortExpression="DOCNO" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            </td></tr>
                               <tr>
                                   <td colspan="100%">
                                       <div id="div<%# Eval("GUID") %>" style="display: none; position: relative; left: 20px;">
                                           <asp:GridView ID="GridView2" runat="server" Width="80%" CssClass="table table-bordered table-row"
                                               AutoGenerateColumns="false" DataKeyNames="GUID"
                                               EmptyDataText="No orders for this customer.">

                                               <Columns>
                                                   <asp:BoundField DataField="VHD_ACT_ID" HeaderText="Account Id" HtmlEncode="False">
                                                       <ItemStyle HorizontalAlign="Center" />
                                                   </asp:BoundField>
                                                   <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" HtmlEncode="False">
                                                       <ItemStyle HorizontalAlign="Left" />
                                                   </asp:BoundField>
                                                   <asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount">
                                                       <ItemStyle HorizontalAlign="right" />
                                                   </asp:BoundField>
                                               </Columns>
                                               <RowStyle CssClass="griditem" Height="20px" Font-Names="Verdana" />
                                               <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                           </asp:GridView>
                                       </div>
                                   </td>
                               </tr>
                                            <tr>
                                                <td>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" /></td>
                    </tr>
                </table>
                <input id="h_SelectedId" runat="server" type="hidden" />
                <asp:HiddenField ID="h_print" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtDocDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>

</asp:Content>
