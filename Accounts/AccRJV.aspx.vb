Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class AccRJV
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrErrMsg As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("BANKTRAN") = "RJV"
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        LockControls()

        If Page.IsPostBack = False Then
            Try
                '   --- For Checking Rights And Initilize The Edit Variables --
                Session("CHECKLAST") = 0
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'txtNarrn.Attributes.Add("onblur", "javascript:CopyDetails()")
                txtNarrn.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtNarrn.ClientID & "');")
                txtLineNarrn.Attributes.Add("onBlur", "narration_check('" & txtLineNarrn.ClientID & "');")

                Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                If (Session("datamode") = "add") Then

                    Call Clear_Details()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Session("gintGridLine") = 1
                    GridInitialize()
                    Session("gDtlDataMode") = "ADD"

                    txtdocDate.Text = GetDiplayDate()
                    If Session("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString

                    End If
                    bind_Currency()
                    Session("dtDTL") = DataTables.CreateDataTable_RJV()
                    Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                    Session("idCostChild") = 0

                End If

                If Session("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If

                If USR_NAME = "" Or (Session("MainMnu_code") <> "A150022" And Session("MainMnu_code") <> "A200016") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If Session("datamode") = "view" Then
                        '   --- Fill Data ---
                        bind_Currency()
                        FillValues()
                    End If
                End If

                '   --- Checking End  ---

                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        Else
            If (Session("datamode") = "add") Then

                Session("SessDocDate") = txtdocDate.Text
            End If
        End If
        If Session("datamode") <> "add" Then
            btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
            ImageButton1.Enabled = False
        Else
            ImageButton1.Enabled = True
        End If
    End Sub

    Private Sub LockControls()
        txtDocNo.Attributes.Add("readonly", "readonly")
        'txtDocDate.Attributes.Add("readonly", "readonly")
        txtDebit.Attributes.Add("readonly", "readonly")
        txtCredit.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        'txtDebitCode.Attributes.Add("readonly", "readonly")
        txtDebitDescr.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub bind_Currency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            If txtdocDate.Text = "" Then
                txtdocDate.Text = GetDiplayDate()
            End If
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If cmbCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub GridInitialize()
        Dim dtTempDTL As New DataTable
        Dim rDt As DataRow
        dtTempDTL = DataTables.CreateDataTable_RJV()
        rDt = dtTempDTL.NewRow
        dtTempDTL.Rows.Add(rDt)
        gvDTL.DataSource = dtTempDTL
        gvDTL.DataBind()
    End Sub

    Public Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        Dim i, lintIndex As Integer
        Dim lintAmount As Decimal

        Dim ldrNew As DataRow

        If Session("datamode") = "view" Then Exit Sub
        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If Trim(txtdebitCode.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Account Code " & "<br>"
        End If

        If Trim(txtLineNarrn.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Narration" & "<br>"
        End If

        If Trim(txtLineAmount.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Amount " & "<br>"
        End If

        If (IsNumeric(txtLineAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True

            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
        End If

        '   --- (1) END OF VALIDATION DURING ADD
        '''''FIND ACCOUNT IS THERE
        Dim bool_cost_center_reqired As Boolean = False
        Dim str_cost_center As String = ""
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            'str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M" _
            '& " WHERE ACT_ID='" & p_accid & "'" _
            '& " AND ACT_Bctrlac='FALSE'"

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID AND AM.ACT_BANKCASH='N'" _
            & " AND AM.ACT_BACTIVE='TRUE' AND ACT_ID='" & txtdebitCode.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'txtDebitCode.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else
                txtdebitdescr.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtdebitCode.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        '   --- (2) PROCEED TO SAVE IN GRID IF NO ERRORS FOUND
        If (lstrErrMsg = "") Then
            Try
                If Session("gDtlDataMode") = "ADD" Then

                    ldrNew = Session("dtDTL").NewRow
                    ldrNew("Id") = Session("gintGridLine")
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("AccountId") = Trim(txtdebitCode.Text)
                    ldrNew("AccountName") = Trim(txtdebitdescr.Text)
                    ldrNew("Narration") = Trim(txtLineNarrn.Text)

                    ldrNew("Amount") = Math.Abs(Convert.ToDecimal(Trim(txtLineAmount.Text)))
                    lintAmount = Convert.ToDecimal(Trim(txtLineAmount.Text))
                    If (lintAmount > 0) Then
                        ldrNew("Debit") = Math.Abs(lintAmount)
                        ldrNew("Credit") = "0.00"
                    Else
                        ldrNew("Debit") = "0.00"
                        ldrNew("Credit") = Math.Abs(lintAmount)
                    End If

                    ldrNew("Status") = ""
                    ldrNew("GUID") = System.DBNull.Value

                    ldrNew("Ply") = str_cost_center

                    ldrNew("CostReqd") = bool_cost_center_reqired

                    '   --- Check if these details are already there before adding to the grid
                    For i = 0 To Session("dtDTL").Rows.Count - 1
                        If Session("dtDTL").Rows(i)("Accountid") = ldrNew("Accountid") And _
                            Session("dtDTL").Rows(i)("Accountname") = ldrNew("Accountname") And _
                             Session("dtDTL").Rows(i)("Narration") = ldrNew("Narration") And _
                             Session("dtDTL").Rows(i)("Debit") = ldrNew("Debit") And Session("dtDTL").Rows(i)("Credit") = ldrNew("Credit") Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            GridBind()
                            Exit Sub
                        End If
                    Next
                    Session("dtDTL").Rows.Add(ldrNew)

                    GridBind()
                ElseIf (Session("gDtlDataMode") = "UPDATE") Then
                    For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                        If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                            Session("dtDTL").Rows(lintIndex)("AccountId") = txtdebitCode.Text
                            Session("dtDTL").Rows(lintIndex)("AccountName") = txtdebitdescr.Text
                            lintAmount = Convert.ToDecimal(Trim(txtLineAmount.Text))
                            Session("dtDTL").Rows(lintIndex)("Amount") = Math.Abs(lintAmount)
                            If (lintAmount > 0) Then
                                Session("dtDTL").Rows(lintIndex)("Debit") = lintAmount
                                Session("dtDTL").Rows(lintIndex)("Credit") = "0.00"
                            Else
                                Session("dtDTL").Rows(lintIndex)("Debit") = "0.00"
                                Session("dtDTL").Rows(lintIndex)("Credit") = Math.Abs(lintAmount)
                            End If

                            Session("dtDTL").Rows(lintIndex)("Ply") = str_cost_center
                            Session("dtDTL").Rows(lintIndex)("CostReqd") = bool_cost_center_reqired

                            Session("gDtlDataMode") = "ADD"
                            btnFill.Text = "ADD"
                            gvDTL.SelectedIndex = -1
                            Clear_Details()
                            GridBind()
                            Exit For
                        End If
                    Next
                End If

            Catch ex As Exception
                UtilityObj.Errorlog("AccRJV.aspx.vb -" + ex.Message)
            End Try
            CalcuTot()
        End If
        '   --- (2) END OF SAVE IN GRID IF NO ERRORS FOUND
    End Sub

    Private Sub CalcuTot()
        Dim ldblDebit As Decimal
        Dim ldblCredit As Decimal
        Dim i As Integer
        ldblDebit = 0
        ldblCredit = 0
        For i = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                ldblDebit = ldblDebit + Session("dtDTL").Rows(i)("Debit")
                ldblCredit = ldblCredit + Session("dtDTL").Rows(i)("Credit")
            End If
        Next
        txtdebit.Text = AccountFunctions.Round(ldblDebit)
        txtCredit.Text = AccountFunctions.Round(ldblCredit)
    End Sub

    Private Sub GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = DataTables.CreateDataTable_RJV()
        If Session("dtDTL").Rows.Count > 0 Then
            For i = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                        ldrTempNew.Item(j) = Session("dtDTL").Rows(i)(j)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvDTL.DataSource = dtTempDtl
        gvDTL.DataBind()
        If Session("dtDTL").Rows.Count <= 0 Then
            GridInitialize()
        End If

        Clear_Details()
    End Sub

    Private Sub Clear_Details()
        txtdebitCode.Text = ""
        txtdebitdescr.Text = ""
        txtLineNarrn.Text = ""
        txtLineAmount.Text = ""
    End Sub

    Private Sub Clear_Header()
        txtDocNo.Text = ""
        txtOldDocNo.Text = ""

        txtDocDate.Text = ""
        txtDebitCode.Text = ""
        txtDebitDescr.Text = ""
        txtMonths.Text = ""
        txtNarrn.Text = ""
        bind_Currency()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblRowId As New Label
            Dim lintIndex As Integer = 0

            lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
            Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

            For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                    txtdebitCode.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountId"))
                    txtdebitdescr.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountName"))
                    txtLineNarrn.Text = Trim(Session("dtDTL").Rows(lintIndex)("Narration"))

                    If (Convert.ToDecimal(Session("dtDTL").Rows(lintIndex)("Debit") > 0)) Then
                        txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Debit"))
                    Else
                        txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Credit")) * -1
                    End If

                    hPLY.Value = Trim(Session("dtDTL").Rows(lintIndex)("Ply"))
                    hCostreqd.Value = Trim(Session("dtDTL").Rows(lintIndex)("CostReqd"))
                    gvDTL.SelectedIndex = lintIndex
                    gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                    gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                    Session("gDtlDataMode") = "UPDATE"
                    btnFill.Text = "UPDATE"
                    Exit For
                End If
            Next
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub DeleteRecordByID(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim j As Integer
        For iRemove = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iRemove)("id") = pId) Then
                Session("dtDTL").Rows(iRemove)("Status") = "DELETED"
                '   --- Remove The Corresponding Rows From The Detail Grid Also
                'For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                While j < Session("dtCostChild").Rows.Count
                    If (Session("dtCostChild").Rows(j)("VoucherId") = pId) Then
                        Session("dtCostChild").Rows(j)("Status") = "DELETED"
                    Else
                        j = j + 1
                    End If
                End While
            End If
        Next
        GridBind()
    End Sub

    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        Dim lblReqd As New Label
        Dim lblid As New Label
        Dim lblAmount As New Label
        Dim btnAlloca As New LinkButton
        lblid = e.Row.FindControl("lblId")
        lblAmount = e.Row.FindControl("lblAmount")
        btnAlloca = e.Row.FindControl("btnAlloca")
        lblReqd = e.Row.FindControl("lblCostReqd")
        If lblReqd IsNot Nothing Then
            If lblReqd.Text = "True" Then
                e.Row.BackColor = Drawing.Color.Pink
            End If
        End If
        If btnAlloca IsNot Nothing Then
            Dim dAmt As Double
            If CDbl(lblAmount.Text) > 0 Then
                dAmt = CDbl(lblAmount.Text)
            End If
            btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & lblid.Text & "');return false;"
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim l As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
            l.Attributes.Add("onclick", "javascript:return " + "confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "id") + "')")
        End If
    End Sub

    Protected Sub gvDTL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDTL.RowDeleting
        Try
            Dim categoryID As Integer = CInt(gvDTL.DataKeys(e.RowIndex).Value)
            DeleteRecordByID(categoryID)
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM RJOURNAL_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("RJH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("RJH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("RJH_NARRATION")

                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception

                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function

    Private Function rlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM RJOURNAL_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("RJH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("RJH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("RJH_NARRATION")

                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockRJOURNAL_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception

                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function

    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM RJOURNAL_H WHERE" _
           & " GUID='" & Session("Eid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("RJH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("RJH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("RJH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = Session("sBSUId")
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR")
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function

    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        Dim iIndex As Integer
        Dim cIndex As Integer
        Dim lblnFound As Boolean

        lstrErrMsg = ""
        If Trim(txtDocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtDocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If


        If (IsNumeric(txtMonths.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Invalid Installments " & "<br>"
        End If

        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Details " & "<br>"
        End If

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iIndex)("CostReqd") = "True") Then
                lblnFound = False
                For cIndex = 0 To Session("dtCostChild").Rows.Count - 1
                    If Session("dtDTL").Rows(iIndex)("Id") = Session("dtCostChild").Rows(cIndex)("VoucherId") Then
                        lblnFound = True
                    End If
                Next
                If lblnFound = False Then
                    lstrErrMsg = lstrErrMsg & " Enter the mandatory cost center details " & "<br>"
                End If
            End If
        Next
        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            'tr_errLNE.Visible = False
            Return True
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String
        Dim lblnNoErr As Boolean
        '   ----------------- VALIDATIONS --------------------
        Dim strfDate As String = txtDocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtDocDate.Text = strfDate
        End If

        lblnNoErr = SaveValidate()
        If (lblnNoErr = False) Then
            Exit Sub
        End If
        '   ----------------- END OG VALIDATE -----------------

        Try
            Session("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                Dim SqlCmd As New SqlCommand("SaveRJOURNAL_H", objConn, stTrans)
                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                SqlCmd.Parameters.Add(sqlpGUID)
                SqlCmd.Parameters.AddWithValue("@RJH_SUB_ID", Session("SUB_ID"))
                SqlCmd.Parameters.AddWithValue("@RJH_BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@RJH_FYEAR", Session("F_YEAR"))
                SqlCmd.Parameters.AddWithValue("@RJH_DOCTYPE", "RJV")
                SqlCmd.Parameters.AddWithValue("@RJH_DOCNO", Trim(txtDocNo.Text))
                SqlCmd.Parameters.AddWithValue("@RJH_DOCDT", Trim(txtDocDate.Text))
                SqlCmd.Parameters.AddWithValue("@RJH_MONTHS", Convert.ToInt32(Trim(txtMonths.Text)))
                SqlCmd.Parameters.AddWithValue("@RJH_CUR_ID", cmbCurrency.SelectedItem.Text)
                SqlCmd.Parameters.AddWithValue("@RJH_EXGRATE1", txtExchRate.Text)
                SqlCmd.Parameters.AddWithValue("@RJH_EXGRATE2", txtLocalRate.Text)
                SqlCmd.Parameters.AddWithValue("@RJH_NARRATION", txtNarrn.Text)
                SqlCmd.Parameters.AddWithValue("@RJH_bDELETED", False)
                SqlCmd.Parameters.AddWithValue("@RJH_bPOSTED", False)
                SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@RJH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If Session("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
                End If
                SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                SqlCmd.Parameters.AddWithValue("@RJH_SESSIONID", Session.SessionID)
                SqlCmd.Parameters.AddWithValue("@RJH_LOCK", Session("sUsr_name"))
                SqlCmd.Parameters.Add("@RJH_NEWDOCNO", SqlDbType.VarChar, 20)
                SqlCmd.Parameters("@RJH_NEWDOCNO").Direction = ParameterDirection.Output
                If (Session("datamode") = "edit") Then
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                End If

                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (Session("datamode") = "edit") Then
                    lstrNewDocNo = txtDocNo.Text
                Else
                    lstrNewDocNo = CStr(SqlCmd.Parameters("@RJH_NEWDOCNO").Value)
                End If

                'Adding header info
                SqlCmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String
                If (lintRetVal = 0) Then
                    ' stTrans.Commit()
                    If Session("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo)
                    Else
                        str_err = DeleteVOUCHER_D_S_ALL(objConn, stTrans, txtdocNo.Text)
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, txtdocNo.Text)
                        End If
                    End If
                    If str_err.Split("__")(0).Trim = "0" Then
                        h_editorview.Value = ""
                        stTrans.Commit()

                        Call Clear_Header()
                        Call Clear_Details()

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                        Session("gintGridLine") = 1
                        GridInitialize()
                        Session("gDtlDataMode") = "ADD"

                        Session("dtDTL") = DataTables.CreateDataTable_RJV()
                        Session("dtCostChild").Rows.Clear()
                        txtdocDate.Text = GetDiplayDate()
                        If Session("datamode") = "add" Then
                            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                        End If
                        txtdebit.Text = ""
                        txtCredit.Text = ""
                        bind_Currency()
                        If Session("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        End If
                        'tr_errLNE.Visible = True
                        lblError.Text = "Data Successfully Saved..."
                    Else
                        'tr_errLNE.Visible = True
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim & "")
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(lintRetVal & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()
        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "RJV"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If Session("dtDTL").Rows(iIndex)("Status") & "" <> "DELETED" Then
                cmd.Dispose()
                cmd = New SqlCommand("SaveRJOURNAL_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table
                Dim str_crdb As String = "CR"
                If Session("dtDTL").Rows(iIndex)("Debit") > 0 Then
                    dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                    str_crdb = "DR"
                Else
                    dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                    str_crdb = "CR"
                End If
                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtDTL").Rows(iIndex)("id"), _
                str_crdb, iIndex + 1 - Session("iDeleteCount"), Session("dtDTL").Rows(iIndex)("Accountid"), dTotal)
                If str_err <> "0" Then
                    Return str_err
                End If
                '' ''
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                cmd.Parameters.AddWithValue("@RJL_SUB_ID", Session("SUB_ID"))
                cmd.Parameters.AddWithValue("@RJL_BSU_ID", Session("sBsuid"))
                cmd.Parameters.AddWithValue("@RJL_FYEAR", Session("F_YEAR"))
                cmd.Parameters.AddWithValue("@RJL_DOCTYPE", Session("BANKTRAN"))
                cmd.Parameters.AddWithValue("@RJL_DOCNO", p_docno)
                cmd.Parameters.AddWithValue("@RJL_DOCDT", Trim(txtdocDate.Text))
                cmd.Parameters.AddWithValue("@RJL_CUR_ID", cmbCurrency.SelectedItem.Text)
                cmd.Parameters.AddWithValue("@RJL_EXGRATE1", txtExchRate.Text)
                cmd.Parameters.AddWithValue("@RJL_EXGRATE2", txtLocalRate.Text)
                cmd.Parameters.AddWithValue("@RJL_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
                cmd.Parameters.AddWithValue("@RJL_DEBIT", Session("dtDTL").Rows(iIndex)("DEBIT"))
                cmd.Parameters.AddWithValue("@RJL_CREDIT", Session("dtDTL").Rows(iIndex)("CREDIT"))
                cmd.Parameters.AddWithValue("@RJL_NARRATION", Session("dtDTL").Rows(iIndex)("Narration"))
                cmd.Parameters.AddWithValue("@RJL_SLNO", iIndex + 1)
                cmd.Parameters.AddWithValue("@RJL_OPP_ACT_ID", " ")
                Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                If iIndex = Session("dtDTL").Rows.Count - 1 Then
                    sqlpbLastRec.Value = True
                Else
                    sqlpbLastRec.Value = False
                End If
                cmd.Parameters.Add(sqlpbLastRec)


                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If Session("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then
                    Exit For
                Else
                End If
                cmd.Parameters.Clear()
            Else
                If Not Session("dtDTL").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    Session("iDeleteCount") = Session("iDeleteCount") + 1
                    cmd = New SqlCommand("DeleteRJOURNAL_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""
                    If iReturnvalue <> 0 Then
                        Response.Write("DSF")
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtDTL").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String
        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer
        Dim x As Integer = 1

        Dim str_err As String = "0"
        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = "Allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If

                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")

                cmd.Dispose()

                cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                'Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                'sqlpGUID.Value = Session("dtCostChild").Rows(iIndex)("GUID")
                'cmd.Parameters.Add(sqlpGUID)

                cmd.Parameters.AddWithValue("@GUID", Session("dtCostChild").Rows(iIndex)("GUID"))

                Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpJDS_ID.Value = p_slno
                cmd.Parameters.Add(sqlpJDS_ID)

                Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpJDS_DOCTYPE.Value = Session("BANKTRAN")
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpJDS_DOCDT.Value = txtdocDate.Text & ""
                cmd.Parameters.Add(sqlpJDS_DOCDT)

                Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJDS_ACT_ID.Value = p_accountid
                cmd.Parameters.Add(sqlpJDS_ACT_ID)

                Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbJDS_SLNO.Value = p_slno
                cmd.Parameters.Add(sqlpbJDS_SLNO)

                Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpJDS_AMOUNT.Value = Session("dtCostChild").Rows(iIndex)("Amount")
                cmd.Parameters.Add(sqlpJDS_AMOUNT)

                Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                'If (Session("dtCostChild").Rows(iIndex)("costcenter") = "OTH") Then
                '    sqlpJDS_CCS_ID.Value = Session("dtCostChild").Rows(iIndex)("MemberId")
                'Else
                sqlpJDS_CCS_ID.Value = Session("dtCostChild").Rows(iIndex)("costcenter")
                'End If

                cmd.Parameters.Add(sqlpJDS_CCS_ID)

                Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                'If (Session("dtCostChild").Rows(iIndex)("costcenter") = "OTH") Then
                '    sqlpJDS_CODE.Value = System.DBNull.Value
                'Else
                sqlpJDS_CODE.Value = Session("dtCostChild").Rows(iIndex)("Memberid")
                'End If
                cmd.Parameters.Add(sqlpJDS_CODE)


                Dim sqlpJDS_DESCR As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                sqlpJDS_DESCR.Value = Session("dtCostChild").Rows(iIndex)("Name")
                cmd.Parameters.Add(sqlpJDS_DESCR)

                Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbJDS_DRCR.Value = p_crdr
                cmd.Parameters.Add(sqlpbJDS_DRCR)

                Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpJDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJDS_bPOSTED)

                Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbJDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJDS_BDELETED)


                ''--------------- sadhu
                Dim sqlpbVDS_CSS_CSS_ID As New SqlParameter("@VDS_CSS_CSS_ID", SqlDbType.VarChar)
                sqlpbVDS_CSS_CSS_ID.Value = Session("dtCostChild").Rows(iIndex)("SubMemberid")
                cmd.Parameters.Add(sqlpbVDS_CSS_CSS_ID)
                ''---------------

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If Session("datamode") = "add" Then
                    sqlpbEdit.Value = False
                Else
                    sqlpbEdit.Value = True
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""

                If iReturnvalue <> 0 Then
                    Exit For

                Else


                    '' -------------------------added  for insert DEPARTMENT...
                    If (Session("dtCostChild").Rows(iIndex)("costcenter") = Session("CostEMP")) Then

                        Dim vchId As Integer = Session("dtCostChild").Rows(iIndex)("VoucherId")
                        Dim filterExp As String = "costcenter='" & Session("CostEMP") & "' and VoucherId = " & vchId & ""
                        Dim _dtRow() As DataRow = Session("dtCostChild").Select(filterExp)



                        If iIndex = (_dtRow.Length + Convert.ToInt32(Session("CHECKLAST"))) - 1 Then
                            'If iIndex = _dtRow.Length - 1 Then


                            cmd.CommandText = "SaveVOUCHER_D_S_dpt"
                            Dim sqlVDSCCSID As New SqlParameter("@VDSCCSID", SqlDbType.VarChar)
                            sqlVDSCCSID.Value = "0006"
                            cmd.Parameters.Add(sqlVDSCCSID)



                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value
                            success_msg = ""
                            x = 1
                            Session("CHECKLAST") = iIndex + 1
                            If iReturnvalue <> 0 Then
                                Exit For
                            End If

                        End If
                    End If
                    '' -------------------------added Sadhu for insert OTHER COSTCENTER...
                    If (Session("dtCostChild").Rows(iIndex)("costcenter") = Session("CostOTH")) Then

                        Dim vchIdOth As Integer = Session("dtCostChild").Rows(iIndex)("VoucherId")
                        Dim filterExpOth As String = "costcenter='" & Session("CostOTH") & "' and VoucherId = " & vchIdOth & "  AND SubMemberId <> '' "
                        Dim _dtRowOth() As DataRow = Session("dtCostChild").Select(filterExpOth)


                        If iIndex = (_dtRowOth.Length + Convert.ToInt32(Session("CHECKLAST"))) - 1 Then

                            cmd.CommandText = "SaveVOUCHER_D_S_Oth"
                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value
                            success_msg = ""
                            x = 1
                            Session("CHECKLAST") = iIndex + 1
                            If iReturnvalue <> 0 Then
                                Exit For
                            End If

                        End If

                    End If
                    '--------------------------------------------------



                End If
                cmd.Parameters.Clear()
            ElseIf Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" = "DELETED" Then
                If Not Session("dtCostChild").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    cmd = New SqlCommand("DeleteVOUCHER_D_S", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtCostChild").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Response.Write("DFG")
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("costcenter") = p_costid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "DELETED" Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If dTotal = p_total Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form
        Dim str_ As String = rlock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            Session("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            ImageButton1.Enabled = False
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        h_editorview.Value = ""
        Call Clear_Header()
        Call Clear_Details()
        Session("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Session("gintGridLine") = 1
        GridInitialize()
        Session("gDtlDataMode") = "ADD"
        Session("dtDTL") = DataTables.CreateDataTable_RJV()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        txtdocDate.Text = GetDiplayDate()
        If Session("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        ImageButton1.Enabled = True
        bind_Currency()
    End Sub

    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.RJH_DOCDT, 106), ' ', '/') as DocDate  FROM RJOURNAL_H A  " _
                       & " WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtdocNo.Text = ds.Tables(0).Rows(0)("RJH_DOCNO")
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("RJH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("RJH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("RJH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("RJH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("RJH_NARRATION")
                txtMonths.Text = ds.Tables(0).Rows(0)("RJH_MONTHS")





                '   --- Initialize The Grid With The Data From The Detail Table
                lstrSQL2 = "SELECT Convert(VarChar,A.RJL_SLNO) as Id,A.RJL_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.RJL_NARRATION as Narration,A.RJL_DEBIT as Debit,A.RJL_CREDIT as Credit,(A.RJL_DEBIT+A.RJL_CREDIT) as Amount, " _
                            & " '' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM RJOURNAL_D A  " _
                            & " INNER JOIN vw_OSA_ACCOUNTS_M C ON A.RJL_ACT_ID=C.ACT_ID" _
                            & " WHERE A.RJL_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND RJL_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'    AND A.RJL_DOCTYPE='" & Session("BANKTRAN") & "'  AND A.RJL_DOCNO='" & ds.Tables(0).Rows(0)("RJH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("dtDTL") = DataTables.CreateDataTable_RJV()
                Session("dtDTL") = ds2.Tables(0)
                gvDTL.DataSource = ds2
                gvDTL.DataBind()
                lstrSQL2 = "SELECT MAx(RJL_SLNO) as Id FROM RJOURNAL_D A " _
                           & " WHERE A.RJL_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND RJL_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'   AND A.RJL_DOCTYPE='" & Session("BANKTRAN") & "'  AND A.RJL_DOCNO='" & ds.Tables(0).Rows(0)("RJH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1


                '   ----  Initalize the Cost Center Grid
                Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                'lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,VDS_ERN_ID as ERN_ID,A.VDS_AMOUNT as Amount," _
                '            & " '' as Status, VDS_CSS_CSS_ID as SubMemberId ,A.GUID FROM VOUCHER_D_S A" _
                '            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "'  AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("RJH_DOCNO") & "' AND VDS_CODE IS NOT NULL " _
                '            & " UNION ALL SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,'OTH' as Costcenter,A.VDS_CCS_ID as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                '            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                '            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "'  AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("RJH_DOCNO") & "' AND VDS_CODE IS NULL "

                lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,VDS_ERN_ID as ERN_ID,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID, VDS_CSS_CSS_ID as SubMemberId  FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'   " _
                            & " AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "'  AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("RJH_DOCNO") & "' AND VDS_CODE IS NOT NULL   AND VDS_Auto IS NULL "

                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)


                Session("dtCostChild") = ds2.Tables(0)


            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Session("datamode") = "add" Or Session("datamode") = "edit" Then
            unlock()
            h_editorview.Value = ""
            Call Clear_Details()
            Session("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim Status As Integer
        Try
            '   --- CALL A GENERAL FUNCTION TO DELETE BY PASSING THE GUID AND TRAN TYPE ---
            Status = VoucherFunctions.DeleteRJV(Session("BANKTRAN"), Session("sBsuid"), Session("Eid"))
            If Status <> 0 Then
                lblError.Text = (UtilityObj.getErrorMessage(Status))
                Exit Sub
            Else
                Status = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "delete", Page.User.Identity.Name.ToString, Me.Page)
                If Status <> 0 Then
                    Throw New ArgumentException("Could not complete your request")
                End If
                Call Clear_Details()
                lblError.Text = "Record Deleted Successfully"
            End If
        Catch myex As ArgumentException
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(myex.Message, Page.Title)
        Catch ex As Exception
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(ex.Message, Page.Title)

        End Try
        'End Using

        Session("datamode") = "none"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))

        '  If Session("datamode") <> "add" Then
        btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
        '  End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.RecurringJournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "RJV", txtdocNo.Text, Session("HideCC"))
        Session("ReportSource") = repSource
        '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim strfDate As String = txtDocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtDocDate.Text = strfDate
        End If
        bind_Currency()
        If Session("datamode") = "add" Then
            txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
        End If
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        If Session("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString

        End If

    End Sub

    Protected Sub txtDebitCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDebitCode.TextChanged
        chk_DetailsAccount()
    End Sub

    Sub chk_DetailsAccount()
        txtDebitDescr.Text = AccountFunctions.Validate_Account(txtDebitCode.Text, Session("sbsuid"), "NORMAL")
        If txtDebitDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
        Else
            txtLineNarrn.Focus()
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnHAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHAccount.Click
        chk_DetailsAccount()
    End Sub

End Class

