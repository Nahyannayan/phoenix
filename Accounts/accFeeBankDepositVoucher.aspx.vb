Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accFeeBankDepositVoucher
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Session("BANKTRAN") = "QR"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                LockControls()
                bind_Collection()
                txtCashFlow.Text = "363"
                '   --- For Checking Rights And Initilize The Edit Variables --
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                If (ViewState("datamode") = "add") Then
                    Call Clear_Details()
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("gintGridLine") = 1
                    GridInitialize()
                    Session("gDtlDataMode") = "ADD"
                    txtdocDate.Text = GetDiplayDate()
                    If ViewState("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                    End If
                    hCheqBook.Value = 0
                    bind_Currency()
                    Session("dtDTL") = DataTables.CreateDataTable_BR()
                    Session("gdtSub") = CostCenterFunctions.CreateDataTableCostCenter ' DataTables.CreateDataTableCostcenter()
                    Session("gdtSub").rows.clear()
                End If
                txtBankCode.Text = Session("CollectBank")
                txtBankDescr.Text = Session("Collect_name")
                If ViewState("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "A150015" _
                 And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER _
                 And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If ViewState("datamode") = "view" Then
                        '   --- Fill Data ---
                        bind_Currency()
                        FillValues()
                    End If
                End If
                '   --- Checking End  ---
                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        Else
            If (ViewState("datamode") = "add") Then
                'txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
                Session("SessDocDate") = txtdocDate.Text
            End If
        End If
    End Sub

    Private Sub bind_Collection() ' bind combos
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT COL_ID, COL_DESCR FROM COLLECTION_M"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddCollection.Items.Clear()
            ddCollection.DataSource = ds.Tables(0)
            ddCollection.DataTextField = "COL_DESCR"
            ddCollection.DataValueField = "COL_ID"
            ddCollection.DataBind()
            ddCollection.SelectedIndex = 0

            ddDCollection.Items.Clear()
            ddDCollection.DataSource = ds.Tables(0)
            ddDCollection.DataTextField = "COL_DESCR"
            ddDCollection.DataValueField = "COL_ID"
            ddDCollection.DataBind()
            ddDCollection.SelectedIndex = 0
            'getnextdocid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub LockControls()
        txtDocNo.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtPartyDescr.Attributes.Add("readonly", "readonly")
        txtCashFlow.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub bind_Currency()
        If IsDate(txtdocDate.Text) Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                cmbCurrency.Items.Clear()
                cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))

                cmbCurrency.DataTextField = "EXG_CUR_ID"
                cmbCurrency.DataValueField = "RATES"
                cmbCurrency.DataBind()
                If cmbCurrency.Items.Count > 0 Then
                    If set_default_currency() <> True Then
                        cmbCurrency.SelectedIndex = 0
                        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub GridInitialize()
        Dim dtTempDTL As New DataTable
        Dim rDt As DataRow
        dtTempDTL = DataTables.CreateDataTable_BR()
        rDt = dtTempDTL.NewRow
        dtTempDTL.Rows.Add(rDt)
        gvDTL.DataSource = dtTempDTL
        gvDTL.DataBind()
        gvDTL.Columns(15).Visible = False
        gvDTL.Columns(16).Visible = False
        gvDTL.Columns(17).Visible = False
    End Sub

    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = String.Empty
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If

        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If

        Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function

    Public Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        Dim i, lintIndex As Integer
        Dim lstrChqBookId, lstrChqBookLotNo, lstrErrMsg As String
        Dim ldrNew As DataRow
        If ViewState("datamode") = "view" Then Exit Sub
        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If Trim(txtPartyCode.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Party " & "<br>"
        End If

        If Trim(txtChqBook.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the cheque book no " & "<br>"
        End If
        If Trim(txtChqDate.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the cheque date " & "<br>"
        End If
        Dim str_err As String = ""
        Dim strfDate As String = txtChqDate.Text.Trim
        If strfDate <> "" Then
            str_err = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                lstrErrMsg = lstrErrMsg & str_err & "<br>"
            Else
                txtChqDate.Text = strfDate
            End If
        End If

        If Trim(txtLineAmount.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Amount " & "<br>"
        End If

        If (IsNumeric(txtLineAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If Trim(txtCashFlow.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Cash Flow " & "<br>"
        End If

        If txtNarrn.Text = "" Then
            txtNarrn.Text = txtLineNarrn.Text
        End If

        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True

            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
            'usrMessageBar.ShowNotification("Please check the following errrors : " & "<br>" & lstrErrMsg, UserControls_usrMessageBar.WarningType.Danger)
        End If

        ''''COST CENTER VERIFICATION

        '''''FIND ACCOUNT IS THERE
        Dim bool_cost_center_reqired As Boolean = False
        Dim str_cost_center As String = ""
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtPartyCode.Text & "'" _
           & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                txtPartyDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                lblError.Text = getErrorMessage("303") ' account already there
                Exit Sub
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Errorlog(ex.Message)
        End Try
        '''''FIND ACCOUNT IS THERE
        '   --- (1) END OF VALIDATION DURING ADD
        '   --- (2) PROCEED TO SAVE IN GRID IF NO ERRORS FOUND
        If (lstrErrMsg = "") Then
            'tr_errLNE.Visible = False
            Try
                If Session("gDtlDataMode") = "ADD" Then
                    ldrNew = Session("dtDTL").NewRow
                    ldrNew("Id") = Session("gintGridLine")
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("AccountId") = Trim(txtPartyCode.Text)
                    ldrNew("AccountName") = Trim(txtPartyDescr.Text)
                    ldrNew("Narration") = Trim(txtLineNarrn.Text)
                    ldrNew("CLine") = Trim(txtCashFlow.Text)
                    ldrNew("Colln") = Trim(ddDCollection.SelectedItem.Value)
                    'lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False)

                    ' lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                    ' lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                    'lstrChqBookId = lstrChqDet.Split("|")(2)
                    'lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                    lstrChqBookId = 0
                    lstrChqBookLotNo = txtChqBook.Text
                    If (txtChqBook.Text = "") Then
                        lstrChqBookLotNo = " "
                    End If
                    ldrNew("ChqBookId") = lstrChqBookId
                    hCheqBook.Value = Convert.ToInt32(lstrChqBookId)
                    ldrNew("ChqBookLot") = lstrChqBookLotNo
                    ldrNew("ChqNo") = lstrChqBookLotNo

                    If (txtChqDate.Text = "") Then
                        ldrNew("ChqDate") = " "
                    Else
                        ldrNew("ChqDate") = Trim(txtChqDate.Text)
                    End If

                    ldrNew("Amount") = Convert.ToDecimal(Trim(txtLineAmount.Text))
                    ldrNew("Status") = ""
                    ldrNew("GUID") = System.DBNull.Value

                    ldrNew("Ply") = str_cost_center

                    ldrNew("CostReqd") = bool_cost_center_reqired

                    '   --- Check if these details are already there before adding to the grid
                    For i = 0 To Session("dtDTL").Rows.Count - 1
                        If Session("dtDTL").Rows(i)("Accountid") = ldrNew("Accountid") And _
                            Session("dtDTL").Rows(i)("Accountname") = ldrNew("Accountname") And _
                             Session("dtDTL").Rows(i)("Narration") = ldrNew("Narration") And _
                             Session("dtDTL").Rows(i)("CLine") = ldrNew("CLine") And _
                             Session("dtDTL").Rows(i)("Amount") = ldrNew("Amount") Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            GridBind()
                            Exit Sub
                        End If
                    Next
                    Session("dtDTL").Rows.Add(ldrNew)

                    GridBind()
                ElseIf (Session("gDtlDataMode") = "UPDATE") Then
                    For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                        If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                            Session("dtDTL").Rows(lintIndex)("AccountId") = txtPartyCode.Text
                            Session("dtDTL").Rows(lintIndex)("AccountName") = txtPartyDescr.Text
                            Session("dtDTL").Rows(lintIndex)("Narration") = txtLineNarrn.Text
                            Session("dtDTL").Rows(lintIndex)("CLine") = txtCashFlow.Text
                            Session("dtDTL").Rows(lintIndex)("Colln") = ddDCollection.SelectedItem.Value
                            Session("dtDTL").Rows(lintIndex)("ChqBookId") = 0

                            If txtChqBook.Text = "" Then
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = " "
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = " "

                                Session("dtDTL").Rows(lintIndex)("ChqDate") = " "
                            Else
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtChqBook.Text
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = txtChqBook.Text

                                Session("dtDTL").Rows(lintIndex)("ChqDate") = txtChqDate.Text
                            End If

                            Session("dtDTL").Rows(lintIndex)("Amount") = txtLineAmount.Text

                            Session("dtDTL").Rows(lintIndex)("Ply") = str_cost_center
                            Session("dtDTL").Rows(lintIndex)("CostReqd") = bool_cost_center_reqired





                            ToggleCols(True)
                            Session("gDtlDataMode") = "ADD"
                            btnFill.Text = "ADD"
                            gvDTL.SelectedIndex = -1
                            Clear_Details()
                            GridBind()
                            Exit For
                        End If
                    Next
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
            CalcuTot()
        End If
        '   --- (2) END OF SAVE IN GRID IF NO ERRORS FOUND
    End Sub

    Private Sub CalcuTot()
        Dim ldblTot As Decimal
        Dim i As Integer
        ldblTot = 0
        For i = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                ldblTot = ldblTot + Session("dtDTL").Rows(i)("Amount")
            End If
        Next
        txtAmount.Text = AccountFunctions.Round(ldblTot)
    End Sub

    Private Sub GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = DataTables.CreateDataTable_BR()
        If Session("dtDTL").Rows.Count > 0 Then
            For i = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                        ldrTempNew.Item(j) = Session("dtDTL").Rows(i)(j)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvDTL.DataSource = dtTempDtl
        gvDTL.DataBind()
        gvDTL.Columns(15).Visible = True
        gvDTL.Columns(16).Visible = True
        gvDTL.Columns(17).Visible = True
        If Session("dtDTL").Rows.Count <= 0 Then
            GridInitialize()
        End If
        Clear_Details()
    End Sub

    Private Sub Clear_Details()
        txtPartyCode.Text = ""
        txtPartyDescr.Text = ""
        txtLineAmount.Text = ""
        txtChqBook.Text = ""
        If txtChqDate.Text <> txtDocDate.Text Then
            txtChqDate.Text = ""
        End If
    End Sub

    Private Sub Clear_Header()
        txtdocNo.Text = ""
        txtOldDocNo.Text = ""
        'txtdocDate.Text = ""
        txtNarrn.Text = ""
        bind_Currency()
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lintIndex As Integer = 0
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)
        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                txtPartyCode.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountId"))
                txtPartyDescr.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountName"))
                txtLineNarrn.Text = Trim(Session("dtDTL").Rows(lintIndex)("Narration"))
                txtCashFlow.Text = Trim(Session("dtDTL").Rows(lintIndex)("CLine"))
                ddDCollection.SelectedIndex = -1
                ddDCollection.Items.FindByValue(Trim(Session("dtDTL").Rows(lintIndex)("Colln"))).Selected = True
                txtChqBook.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqNo"))
                txtChqDate.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqDate"))
                txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Amount"))
                hPLY.Value = Trim(Session("dtDTL").Rows(lintIndex)("Ply"))
                hCostreqd.Value = Trim(Session("dtDTL").Rows(lintIndex)("CostReqd"))
                gvDTL.SelectedIndex = lintIndex
                gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                Session("gDtlDataMode") = "UPDATE"
                btnFill.Text = "UPDATE"
                ToggleCols(False)
                Exit For
            End If
        Next
    End Sub

    Protected Sub ToggleCols(ByVal pSet As Boolean)
        gvDTL.Columns(15).Visible = pSet
        gvDTL.Columns(16).Visible = pSet
        gvDTL.Columns(17).Visible = pSet
    End Sub

    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        ToggleCols(True)
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub DeleteRecordByID(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim j As Integer
        For iRemove = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iRemove)("id") = pId) Then
                Session("dtDTL").Rows(iRemove)("Status") = "DELETED"
                '   --- Remove The Corresponding Rows From The Detail Grid Also
                'For j As Integer = 0 To Session("gdtSub").Rows.Count - 1
                While j < Session("gdtSub").Rows.Count
                    If (Session("gdtSub").Rows(j)("VoucherId") = pId) Then
                        Session("gdtSub").Rows(j)("Status") = "DELETED"
                    Else
                        j = j + 1
                    End If
                End While
            End If
        Next
        GridBind()
    End Sub

    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        Dim lblReqd As New Label
        lblReqd = e.Row.FindControl("lblCostReqd")
        If lblReqd IsNot Nothing Then
            If lblReqd.Text = "True" Then
                e.Row.BackColor = Drawing.Color.Pink
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim l As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
            l.Attributes.Add("onclick", "javascript:return " + "confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "id") + "')")
        End If
    End Sub

    Protected Sub gvDTL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDTL.RowDeleting
        Try
            Dim categoryID As Integer = CInt(gvDTL.DataKeys(e.RowIndex).Value)
            DeleteRecordByID(categoryID)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
 
    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        Dim iIndex As Integer
        Dim cIndex As Integer
        Dim lblnFound As Boolean
        lstrErrMsg = ""
        If Trim(txtDocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtDocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If

        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If

        If (IsNumeric(txtAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Credit Details " & "<br>"
        End If

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iIndex)("CostReqd") = "True") Then
                lblnFound = False
                For cIndex = 0 To Session("gdtSub").Rows.Count - 1
                    If Session("dtDTL").Rows(iIndex)("Id") = Session("gdtSub").Rows(cIndex)("VoucherId") Then
                        lblnFound = True
                    End If
                Next
                If lblnFound = False Then
                    lstrErrMsg = lstrErrMsg & " Enter the mandatory cost center details - Line " & iIndex + 1 & "<br>"
                End If
            End If
        Next
        If (lstrErrMsg <> "") Then
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.VHH_DOCDT, 106), ' ', '/') as DocDate ,isNULL(VHH_Amount,0) as Amount,B.COL_DESCR, isNULL(A.VHH_REFNO,'') as OldDocNo FROM VOUCHER_H A    INNER JOIN COLLECTION_M B ON A.VHH_COL_ID=B.COL_ID WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("FYear") = ds.Tables(0).Rows(0)("VHH_FYEAR")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtOldDocNo.Text = ds.Tables(0).Rows(0)("OldDocNo")
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                txtBankCode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID")
                txtBankDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ACT_ID"))
                txtAmount.Text = AccountFunctions.Round(ds.Tables(0).Rows(0)("Amount"))
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("VHH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")

                '   --- Initialize The Grid With The Data From The Detail Table
                lstrSQL2 = "SELECT Convert(VarChar,A.VHD_LINEID) as Id,A.VHD_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.VHD_NARRATION as Narration,A.VHD_RSS_ID as CLine,A.VHD_COL_ID as Colln,A.VHD_AMOUNT as Amount,A.VHD_CHQID as ChqBookId,A.VHD_CHQNO as ChqBookLot,A.VHD_CHQNO as ChqNo," _
                           & " REPLACE(CONVERT(VARCHAR(11), A.VHD_CHQDT, 106), ' ', '/') as  ChqDate,'' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM VOUCHER_D A INNER JOIN vw_OSA_ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID" _
                           & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("dtDTL") = DataTables.CreateDataTable_BR()
                Session("dtDTL") = ds2.Tables(0)
                gvDTL.DataSource = ds2
                gvDTL.DataBind()
                lstrSQL2 = "SELECT MAx(VHD_LINEID) as Id,Max(isNull(VHD_CHQID,'')) as CHQID FROM VOUCHER_D A " _
                           & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1
                hCheqBook.Value = ds2.Tables(0).Rows(0)("CHQID")

                '   ----  Initalize the Cost Center Grid
                Session("gdtSub") = CostCenterFunctions.CreateDataTableCostCenter ' DataTables.CreateDataTableCostcenter()
                lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' AND VDS_CODE IS NOT NULL " _
                            & " UNION ALL SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,'OTH' as Costcenter,A.VDS_CCS_ID as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' AND VDS_CODE IS NULL "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gdtSub") = ds2.Tables(0)
            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Record Not Found !!! "
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call Clear_Details()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If chkVoucher.Checked Then
            Session("ReportSource") = FeeDayendProcess.PrintQR(txtdocNo.Text, txtdocDate.Text, Session("sBsuID"), Session("F_YEAR"), "QR")
            Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        Else
            Session("ReportSource") = FeeDayendProcess.PrintDepostSlip(Session("sBsuid"), Session("SUB_ID"), _
            ViewState("FYear"), txtdocNo.Text, Session("sUsr_name"))
            Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        End If
    End Sub

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim strfDate As String = txtDocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtDocDate.Text = strfDate
            If txtChqDate.Text.Trim = "" Then
                txtChqDate.Text = strfDate
            End If
        End If
        bind_Currency()
        If ViewState("datamode") = "add" Then
            txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
        End If
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
            If txtChqDate.Text.Trim = "" Then
                txtChqDate.Text = strfDate
            End If
        End If
        bind_Currency()
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
    End Sub

    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            cmbCurrency.Focus()
        End If
    End Sub

    Protected Sub txtPartyCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartyCode.TextChanged
        txtPartyDescr.Text = AccountFunctions.Validate_Account(txtPartyCode.Text, Session("sbsuid"), "NOTCC")
        If txtPartyDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            txtPartyCode.Focus()
        Else
            lblError.Text = ""
            txtLineNarrn.Focus()
        End If
    End Sub

    Protected Sub ddDCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddDCollection.SelectedIndexChanged
        Dim str_collection As String = AccountFunctions.get_CollectionAccount(ddDCollection.SelectedItem.Value, Session("sBsuid"))
        Try
            txtPartyCode.Text = str_collection.Split("|")(0)
            txtPartyDescr.Text = str_collection.Split("|")(1)
        Catch ex As Exception
        End Try
    End Sub

End Class
