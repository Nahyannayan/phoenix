<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accCheqBounce.aspx.vb" Inherits="Accounts_accCheqClearanceBR" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetDocNo() {

            var result;
            result = radopen("selBankDocNo.aspx", "pop_up");
            <%--if (result != "" && result != undefined) {
                document.getElementById('<%=txtDocNo.ClientID %>').value = result;//NameandCode[0];
                document.getElementById('<%=txtChqNo.ClientID %>').value = "";
                document.getElementById('<%=txtOldBank.ClientID %>').value = "";
                document.getElementById('<%=txtOldChqDate.ClientID %>').value = "";
            }
            return false;--%>
        }

         function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById('<%=txtDocNo.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=txtChqNo.ClientID %>').value = "";
                document.getElementById('<%=txtOldBank.ClientID %>').value = "";
                document.getElementById('<%=txtOldChqDate.ClientID %>').value = "";
                __doPostBack('<%= txtDocNo.ClientID%>', 'TextChanged');
            }
        }

        function GetChqNo() {

            var result;
            var docNo = document.getElementById('<%=txtDocNo.ClientID %>').value;
            if (docNo == "") {
                alert("Please select Document No");
            }
            else {
                result = radopen("selChqNo.aspx?DOCID=" + docNo, "pop_up2");
                <%--if (result != "" && result != undefined) {
                    document.getElementById('<%=txtChqNo.ClientID %>').value = result;//NameandCode[0];
                    return true;
                }
            }
            return false;--%>
            }
        }

         function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById('<%=txtChqNo.ClientID %>').value = arg.NameandCode;//NameandCode[0];
                __doPostBack('<%= txtChqNo.ClientID%>', 'TextChanged');
            }
        }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Cheque Bounce Of Bank Reciept"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="chqBR" />
                <br />

                <table align="center"
                    style="width: 100%;">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Document No</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgDocNo" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetDocNo(); return false;" /></td>
                   
                        <td align="left" width="20%">
                            <span class="field-label">Cheque No </span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgChqNo" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetChqNo(); return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Bank</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtOldBank" runat="server"></asp:TextBox></td>
                  
                        <td align="left" width="20%">
                            <span class="field-label">Cheque Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtOldChqDate" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Amount </span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="chqBR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            <asp:HiddenField ID="h_VHD_ID" runat="server" />
                            <asp:HiddenField ID="h_VHD_ACT_ID" runat="server" />
                            <asp:HiddenField ID="h_VHD_SUB_ID" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

