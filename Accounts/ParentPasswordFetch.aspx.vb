Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration




Imports Encryption64
Imports System.Security.Cryptography
Imports System.IO

Partial Class Accounts_ParentPasswordFetch
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim str_query As String
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'str_query = " SELECT * FROM USERACCESS_MENU WHERE USM_USR_NAME='" & Session("sUsr_name") & "' AND USM_BSU_ID='" & CurBsUnit & "'"
                    'Dim ds As DataSet
                    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    'If ds.Tables(0).Rows.Count >= 1 Then
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    '  btnActivate.Visible = True
                    btnReset.Visible = True
                    ddlBUnit.Visible = True
                    tabmain.Visible = True
                    lblError.Text = ""
                    lblErrors.Text = ""
                    '    Else
                    '    ' btnActivate.Visible = False
                    '    btnReset.Visible = False
                    '    ddlBUnit.Visible = False
                    '    tabmain.Visible = False
                    '    lblError.Text = "Access denied"
                    'End If

                End If
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            set_Menu_Img()
        End If
    End Sub
    Public Sub gridbind()
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""
        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox
        Dim strQuery As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Try
            If gvUNITS.Rows.Count > 0 Then


                txtSearch = gvUNITS.HeaderRow.FindControl("txtFeeIdSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("STU_Fee_Id", txtSearch.Text, strSearch)
                strName = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvUNITS.HeaderRow.FindControl("txtStudNameSearch")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("StudName", txtSearch.Text.Replace("/", " "), strSearch)
                strNo = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvUNITS.HeaderRow.FindControl("txtParentUserNameSearch")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("ParentUserName", txtSearch.Text.Replace("/", " "), strSearch)
                strNo = txtSearch.Text

                If strFilter <> "" Then
                    strQuery = "SELECT CASE B.OLU_bActive WHEN 1 THEN 'Yes' ELSE 'No' END AS OLU_bActive, *  from vw_PARENT_LIST A INNER JOIN Online.Online_Users_M B ON A.STU_SIBLING_ID=B.OLU_DEF_STU_ID WHERE OLU_BSU_ID='" & ddlBUnit.SelectedValue & "'"
                    strQuery += strFilter
                Else
                    strQuery = "SELECT CASE B.OLU_bActive WHEN 1 THEN 'Yes' ELSE 'No' END AS OLU_bActive, *  from vw_PARENT_LIST A INNER JOIN Online.Online_Users_M B ON A.STU_SIBLING_ID=B.OLU_DEF_STU_ID WHERE OLU_BSU_ID='" & ddlBUnit.SelectedValue & "' ORDER BY StudName"
                End If
            Else
                strQuery = "SELECT CASE B.OLU_bActive WHEN 1 THEN 'Yes' ELSE 'No' END AS OLU_bActive, *  from vw_PARENT_LIST A INNER JOIN Online.Online_Users_M B ON A.STU_SIBLING_ID=B.OLU_DEF_STU_ID WHERE OLU_BSU_ID='" & ddlBUnit.SelectedValue & "' ORDER BY StudName"
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Dim i As Integer = 0
            '    For Each dr As DataRow In ds.Tables(0).Rows
            '        Dim Encr_decrData As New Encryption64
            '        Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("OLU_PASSWORD").ToString.Replace(" ", "+"))
            '    Next
            'End If
            gvUNITS.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvUNITS.DataBind()
                Dim columnCount As Integer = gvUNITS.Rows(0).Cells.Count
                gvUNITS.Rows(0).Cells.Clear()
                gvUNITS.Rows(0).Cells.Add(New TableCell)
                gvUNITS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvUNITS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvUNITS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' btnActivate.Enabled = False
                btnReset.Enabled = False
                gvUNITS.HeaderRow.Visible = False
            Else
                ' btnActivate.Enabled = True
                btnReset.Enabled = True
                gvUNITS.DataBind()
            End If
            'set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAME"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()

        If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub gvUNITS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUNITS.PageIndexChanging
        Try
            gvUNITS.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    'Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivate.Click
    '    Try
    '        Dim flag As Integer = 0
    '        If gvUNITS.Rows.Count > 0 Then
    '            For Each gr As GridViewRow In gvUNITS.Rows
    '                If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
    '                    flag = flag + 1
    '                    Dim strSections As String = ""
    '                    Activate()
    '                    gridbind()
    '                End If
    '                If flag = 0 Then
    '                    lblError.Text = "Please select a Row"
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed"
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub
    'Sub Activate()
    '    Dim str_query As String
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim transaction As SqlTransaction
    '    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '        transaction = conn.BeginTransaction("SampleTransaction")
    '        Try
    '            For Each gr As GridViewRow In gvUNITS.Rows
    '                If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
    '                    Dim OLU_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
    '                    str_query = "exec sp_ParentActivatePassword '" & OLU_ID & "'"
    '                    Dim stat As Integer
    '                    stat = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
    '                End If
    '            Next
    '            transaction.Commit()
    '            lblError.Text = "Account Activated Successfully"
    '        Catch myex As ArgumentException
    '            transaction.Rollback()
    '            lblError.Text = myex.Message
    '            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Catch ex As Exception
    '            transaction.Rollback()
    '            lblError.Text = "Record could not be Saved"
    '            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        End Try
    '    End Using
    'End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Dim flag As Integer = 0
            If gvUNITS.Rows.Count > 0 Then
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        flag = flag + 1
                        Dim strSections As String = ""
                        Reset()
                        gridbind()
                    End If
                    If flag = 0 Then
                        lblError.Text = "Please select a Row"
                        lblErrors.Text = "Please select a Row"
                    End If
                Next
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub Reset()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Dim bPasswordUpdate As Boolean = False
        Dim status As Integer
        Dim strParUserName As String

        Dim lstrPWD As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddlBUnit.SelectedItem.Value)
        pParms(1) = New SqlClient.SqlParameter("@ROLE", "PARENT")
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_GLG_DEFP", pParms)
            While reader.Read
                lstrPWD = Convert.ToString(reader("BSU_GLG_DEFP"))
            End While
        End Using

        Using conn As SqlConnection = ConnectionManger.GetGLGConnection
            'transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        Dim OLU_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                        strParUserName = TryCast(gr.FindControl("HiddenField2"), HiddenField).Value.ToString
                        ''commented by nahyan and added new on 28th april 2019
                        ''str_query = "exec sp_ParentResetPassword '" & OLU_ID & "'"
                        Dim hashedpwddefault As String = Encr_decrData.EncryptWithSaltedHash("fHZ2X0GshyOyfUzn6c775A==")
                        str_query = "exec sp_ParentResetPassword_hash '" & OLU_ID & "','fHZ2X0GshyOyfUzn6c775A==','" & hashedpwddefault & "'"

                        Dim stat As Integer
                        stat = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str_query)
                        If stat < 1 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(stat))
                        End If

                        bPasswordUpdate = True
                        If bPasswordUpdate Then
                            ''pasword hashing nahyan on 28th april2019
                            status = VerifyandUpdatePassword(Encr_decrData.Encrypt(lstrPWD), Encr_decrData.EncryptWithSaltedHash(lstrPWD), strParUserName)
                            'Dim vGLGUPDPWD As New com.ChangePWDWebService
                            'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                            'Dim respon As String = vGLGUPDPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            ''comemnted by nahyan on 01june2017
                            ''  Dim respon As String = LDAP_CHANGEPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            ''by nahyan on 01june2017

                            ''commented by nahyan on 3oct2019 AND ADDED DYNAMIC CHECKING  
                            ''  If (ddlBUnit.SelectedItem.Value <> "888888" AndAlso ddlBUnit.SelectedItem.Value <> "888881" AndAlso ddlBUnit.SelectedItem.Value <> "900350" AndAlso ddlBUnit.SelectedItem.Value <> "500610" AndAlso ddlBUnit.SelectedItem.Value <> "900201") Then
                            Dim isAvailableinADfs As Boolean = False
                            isAvailableinADfs = IsAvailableInAD()
                            If isAvailableinADfs Then

                                Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient

                                Dim respon As String = chPWDSVC.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            End If
                            ''by nahyan on 01june2017
                        End If
                    End If
                Next
                'transaction.Commit()

                lblError.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
                lblErrors.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                lblErrors.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                lblErrors.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    Function IsAvailableInAD() As Boolean
        Dim IsAvailableInADFS As Boolean = True
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@bsu_Id", ddlBUnit.SelectedItem.Value)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.IsBSU_NotAvailableInAD", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAvailableInADFS = False
            End If

        End If
        Return IsAvailableInADFS
    End Function
    'VerifyandUpdatePassword
    Public Shared Function VerifyandUpdatePassword(ByVal Encr_Password As String, ByVal hashed_Password As String, _
            ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@HASHED_PWD", hashed_Password)
            pParms(2) = New SqlClient.SqlParameter("@ENCR_PWD", Encr_Password)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            'Added new SP to store the hashed password by Hrushikesh:8-Apr-2019
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD_HASH_ALL", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(3).Value
        End Using
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvUNITS.Rows.Count - 1
            Dim row As GridViewRow = gvUNITS.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        If ddlBUnit.SelectedIndex > -1 Then
            Call gridbind()
        End If
    End Sub
    Protected Sub gvUNITS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUNITS.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim lblParentUserName As String = DataBinder.Eval(e.Row.DataItem, "ParentUserName").ToString()

                Dim hyplink As HyperLink = e.Row.FindControl("hypParentPortal")
                If Session("sroleid") = "204" Then
                    lblParentUserName = lblParentUserName + "$$$" + ConfigurationManager.AppSettings("ParentPortalLoginKey")
                    hyplink.NavigateUrl = ConfigurationManager.AppSettings("ParentPortalLoginURL") + Encrypt_CLEV(lblParentUserName) + "&M=YMWXNS1Z0xo%3D&S="
                Else
                    hyplink.Text = ""
                    '' gvUNITS.Columns(10).Visible = False
                End If
            End If
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

    Public Function Encrypt_CLEV(ByVal stringToEncrypt As String, _
              Optional ByVal SEncryptionKey As String = "!#$a54?CLEV") As String
        Try
            Dim key() As Byte = {}
            Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnStudNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnParentUserName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
End Class
