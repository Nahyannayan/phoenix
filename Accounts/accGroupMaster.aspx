<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accGroupMaster.aspx.vb" Inherits="GroupMaster" Title="Group Master" Theme="General" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {

            if (document.getElementById("txtGroupcode").value == '') {
                alert("Kindly enter Group Code");
                return false;
            }
            if (document.getElementById("txtGroupname").value == '') {
                alert("Kindly enter Group Name");
                return false;
            }
            return true;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Group Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table  width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input type="hidden" id="h_SelectedId" runat="server" value="0" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridViewGroupMaster" runat="server" AutoGenerateColumns="False"
                                Width="100%" CssClass="table table-bordered table-row" >
                                <Columns>
                                    <asp:BoundField DataField="GUID" HeaderText="GUID" SortExpression="GUID" Visible="False" />
                                    <asp:TemplateField HeaderText="Group Code" SortExpression="GRP_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroupcode" runat="server" Text='<%# Bind("GPM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  />
                                        <HeaderTemplate>
                                            Group Code
                                            <br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Name" SortExpression="GRP_DESCR">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGroupname" runat="server" CssClass="inputbox" MaxLength="100"
                                                Text='<%# Bind("GPM_DESCR") %>' Width="98%"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("GPM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Group Name
                                            <br />
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <HeaderStyle  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# Eval("GPM_ID", "NewGroup.aspx?editid={0}") %>'
                                                Text="View"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                                OnClientClick='return confirm("Are You Sure?");' Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Edit" ShowEditButton="True" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

