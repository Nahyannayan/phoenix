Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_ImportTbpost


    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            '''''check menu rights
            viewstate("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200310"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            Page.Title = OASISConstants.Gemstitle
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "A200355" Then 'Or (ViewState("MainMnu_code") <> "A200351") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Page.Title = OASISConstants.Gemstitle

                lblHead.Text = "POST TB"


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub





    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try

            If Not e.Row.RowIndex.Equals(-1) Then
                Dim chkPost As New HtmlInputCheckBox
                chkPost = e.Row.FindControl("chkPosted")
                e.Row.Cells(5).Text = "View"
                e.Row.Cells(5).Attributes.Add("onclick", "return getAccountsdetails('" & chkPost.Value & "','" & e.Row.Cells(1).Text.ToString() & "')")
                e.Row.Cells(5).Style(HtmlTextWriterStyle.Cursor) = "hand"
                e.Row.Cells(5).ToolTip = "Click for accounts details"
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub




    Private Function findPostvalue() As String
        Try


            Dim postId As String = ""
            Dim chk As HtmlInputCheckBox
            For Each grow As GridViewRow In gvJournal.Rows
                chk = DirectCast(grow.FindControl("chkPosted"), HtmlInputCheckBox)
                If chk.Checked = True Then
                    postId = doPost(chk.Value.ToString, grow.Cells(1).Text.ToString())
                End If


            Next
            Return postId
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function





    Private Function doPost(ByVal PostValue As String, ByVal busName As String) As String
        Dim _parameter As String(,) = New String(12, 1) {}


        _parameter(0, 0) = "@MODE"
        _parameter(0, 1) = ""
        _parameter(1, 0) = "@TDH_SUB_ID"
        _parameter(1, 1) = Session("Sub_ID").ToString()
        _parameter(2, 0) = "@TDH_BSU_ID"
        _parameter(2, 1) = Session("sBsuid").ToString()
        _parameter(3, 0) = "@TDH_FYEAR"
        _parameter(3, 1) = Session("F_YEAR").ToString()
        _parameter(4, 0) = "@TDH_SESSIONID"
        _parameter(4, 1) = Session.SessionID.ToString()
        _parameter(5, 0) = "@TDH_ID"
        _parameter(5, 1) = PostValue
        _parameter(6, 0) = "@AUD_WINUSER"
        _parameter(6, 1) = Page.User.Identity.Name.ToString()
        _parameter(7, 0) = "@Aud_form"
        _parameter(7, 1) = Master.MenuName.ToString()
        _parameter(8, 0) = "@Aud_user"
        _parameter(8, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(9, 0) = "@Aud_module"
        _parameter(9, 1) = HttpContext.Current.Session("sModule")
        _parameter(10, 0) = "@ReturnValue"
        _parameter(10, 1) = "0"
        _parameter(11, 0) = "@JHD_NEWDOCNO"
        _parameter(11, 1) = ""
       
        MainObj.doExcutiveRetvalue("postTBDATA_H", _parameter, "mainDB", "")


        If MainObj.SPRETVALUE.Equals(0) Then
            If chkPrint.Checked Then
                PrintVoucher(PostValue, busName)
                chkPrint.Checked = False
            End If
        End If

        Return MainObj.MESSAGE
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            Dim retMsg As String = findPostvalue()
            If retMsg.Equals("") Then
                lblError.Text = "Could not process you are request Please select post Document..!"
                Exit Sub
            End If
            lblError.Text = retMsg
            gridbind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub





    Private Sub gridbind()
        Try
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = ""
            Dim PostYN As String = "0"


            str_Sql = " SELECT TDH_ID," _
                    & " TBDATA_H.TDH_DATE, vw_OSO_BUSINESSUNIT_M.BSU_NAME, " _
                    & " TBDATA_H.TDH_CUR_ID, TBDATA_H.TDH_EXGRATE,  " _
                    & " TBDATA_H.TDH_bPOSTED,TBDATA_H.TDH_DOCNO " _
                    & " FROM TBDATA_H INNER JOIN " _
                    & " vw_OSO_BUSINESSUNIT_M ON TBDATA_H.TDH_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID " _
                    & " WHERE TDH_FYEAR = " & Session("F_YEAR") & " AND TDH_bPOSTED = " & PostYN & " " _
                    & " AND TDH_BSU_ID = '" & Session("SBsuID") & "'  ORDER BY TDH_ID "



            Dim ds As New DataTable
            ds = MainObj.ListRecords(str_Sql, "mainDB")
            gvJournal.DataSource = ds
            If ds.Rows.Count > 0 Then
                btnPost.Visible = True
                chkPrint.Visible = True
            Else
                btnPost.Visible = False
                chkPrint.Visible = False
            End If
            gvJournal.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub PrintVoucher(ByVal printId As String, ByVal bsName As String)
        Try

            Dim SqlQuery As String
            SqlQuery = "SELECT TDH_DATE , ACCOUNTS_M.ACT_ID ,ACCOUNTS_M.ACT_NAME ,  " _
            & " ACCOUNTS_M.ACT_TYPE , TDH_EXGRATE,TDH_CUR_ID,TDH_DOCNO,  " _
            & " SUM(TBDATA_D.TDD_AMOUNT )TDD_AMOUNT,TDH_ID  " _
            & " FROM ACCOUNTS_M LEFT OUTER JOIN   " _
            & " TBDATA_D ON ACCOUNTS_M.ACT_ID =TBDATA_D.TDD_ACT_ID    " _
            & " INNER JOIN TBDATA_H ON TBDATA_H.TDH_ID = TBDATA_D.TDD_TDH_ID    " _
            & " WHERE TDD_TDH_ID = " & printId & " AND TDH_FYEAR = '" & Session("F_YEAR") & "'" _
            & " GROUP BY TDH_EXGRATE,TDH_CUR_ID,TDH_DOCNO,  " _
            & " ACT_ID,ACT_NAME,ACT_TYPE,TDH_DATE,TDH_ID  " _
            & " ORDER BY ACT_TYPE"

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = SqlQuery
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("reportHeading") = "OTHER ACCOUNTS POSTING"
            params("BusinessName") = bsName
            repSource.Parameter = params
            repSource.VoucherName = "OTHER ACCOUNTS POSTING"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptIndiadataExport.rpt"
            Session("ReportSource") = repSource
            h_print.Value = "print"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub





End Class


