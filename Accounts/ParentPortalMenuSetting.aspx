﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ParentPortalMenuSetting.aspx.vb" Inherits="Accounts_ParentPortalMenuSetting" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

  <%--  <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../cssfiles/jquery.ui.timepicker.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ui.timepicker.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <link href="../Scripts/jquery.ui.timepicker.css" rel="stylesheet" />--%>
    
    

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Menu Setting Page for Parent Portal
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="error" />
                </div>
                <br />
                <div id="divcommon" runat="server">
                    <table id="tblcommon" runat="server" width="100%">
                        <tbody>
                            <tr>
                                <td width="20%"><span class="field-label">Select BSU Unit</span>
                                </td>
                                <td width="30%">
                                    <asp:DropDownList ID="lstBsuUnit" runat="server" OnSelectedIndexChanged="lstBsuUnit_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:DropDownList ID="ddlACDYear" runat="server" Visible="false"></asp:DropDownList>
                                </td>
                                <td width="20%">
                                    <span class="field-label">Please Select Parent Menu</span>
                                </td>
                                <td width="30%">
                                    <asp:RadioButtonList ID="radioMenu" runat="server" RepeatDirection="vertical" OnSelectedIndexChanged="radioMenu_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div id="divaddnew" runat="server" visible="false" class="card mb-3">
                                        <div class="card-header letter-space">
                                            <i class="fa fa-users mr-3"></i>
                                           Add new Menu
                                        </div>
                                        <table width="100%">                                            
                                            <tbody>
                                                <tr>
                                                    <td width="20%">Please enter menu name 
                                                    </td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtnewmenuname" runat="server" CssClass="myTextBoxStyleS_CAL" />
                                                    </td>
                                                    <td width="20%">Please enter menu url 
                                                    </td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txturl" runat="server" CssClass="myTextBoxStyleS_CAL" />
                                                    </td>
                                                </tr>
                                             
                                                <tr>
                                                    <td width="20%">Please select grades  </td>
                                                    <td width="30%">
                                                        <asp:CheckBoxList ID="chkgradesnew" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:Button ID="btnsavenew" Text="Save" runat="server" OnClick="btnsavenew_Click1" CssClass="button" />
                                                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click" CssClass="button" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="gridMenu" Width="100%" CssClass="table table-bordered table-row" runat="server" OnRowDataBound="gridMenu_RowDataBound" AutoGenerateColumns="false" OnRowCommand="gridMenu_RowCommand">
                                        <Columns>
                                            <asp:BoundField HeaderText="Main Menu Field" DataField="MENU" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBoxList ID="chkgrades" runat="server" RepeatDirection="Horizontal">
                                                    </asp:CheckBoxList>
                                                    <asp:HiddenField ID="hidoplmid" runat="server" />
                                                    <%-- <asp:LinkButton ID="lnkslctall" runat="server" OnClick="lnkslctall_Click" Text="Select All"></asp:LinkButton>--%>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    Select Grades
                                            <input id="Checkbox1" name="chkAL" onclick="CheckAllGrades(this)" type="checkbox"
                                                value="Check All" />
                                                </HeaderTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Showable">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkshow" runat="server" />
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    Show
                                            <input id="Checkbox1" name="chkAL" onclick="CheckAllMenuToShow(this)" type="checkbox"
                                                value="Check All" />
                                                </HeaderTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField Text="Update menu" CommandName="btnSaveCommand" runat="server" HeaderText="Click To Save" />
                                        </Columns>
                                    </asp:GridView>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnSaveAll" runat="server" Text="Update all menu" OnClick="btnSave_Click" Visible="false" CssClass="button" />
                                    <asp:Button ID="lnkaddnew" runat="server" Text="Add new menu" OnClick="lnkaddnew_Click" Visible="false" CssClass="button"></asp:Button>
                                    <br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function CheckAllGrades(Checkbox) {
            var gvGroup = document.getElementById("<%=gridMenu.ClientID%>");
            for (i = 1; i < gvGroup.rows.length; i++) {
                var checkboxlist = gvGroup.rows[i].cells[1].getElementsByTagName("INPUT");
                for (j = 0; j < checkboxlist.length ; j++) {
                    gvGroup.rows[i].cells[1].getElementsByTagName("INPUT")[j].checked = Checkbox.checked;
                }
            }
        }

        function CheckAllMenuToShow(Checkbox) {
            var gvGroup = document.getElementById("<%=gridMenu.ClientID%>");
            for (i = 1; i < gvGroup.rows.length; i++) {

                gvGroup.rows[i].cells[2].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;

            }
        }

    </script>


</asp:Content>
