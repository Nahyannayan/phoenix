Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_accFreezePeriod
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "A200070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                Session("bApprovalsPending") = False
                bindBusinessunit()
                BindAccountsDetails()
                ddBusinessunit.SelectedIndex = -1
                For Each item As ListItem In ddBusinessunit.Items
                    If item.Value.ToString.Contains(Session("sBSUID")) Then
                        item.Selected = True
                    End If
                Next
                GetPayMonthYear(ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-"))
                txtFrom.Attributes.Add("ReadOnly", "ReadOnly")
                grdViewPendingpost.Attributes.Add("bordercolor", "#b0c4de")
            End If
        End If
    End Sub

    Private Sub BindAccountsDetails()
        Dim ds As New DataSet
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim bunit As String = Session("sBsuid")
        '   Dim userMod As String = moduleDecr.Decrypt(Request.QueryString("userMod").Replace(" ", "+"))
        Dim userRol As String = Session("sroleid")

        Dim userMod As String = Session("sModule")
        Dim sqlString As String
        Dim userSuper As Boolean = Session("sBusper")
        If userSuper = False Then
            sqlString = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                " where mnu_parentid ='A000020' and mnu_code in(select mnr_mnu_id from menurights_s where mnu_module='A0' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='A0' " & _
                " and MNU_CODE in ('A200012', 'A200005', 'A200040', 'A200011', 'A200010', 'A200351') order by mnu_id"
        Else

            sqlString = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                " where mnu_parentid = 'A000020' and mnu_module='" & userMod & "'" & _
                " and MNU_CODE in ('A200012', 'A200005', 'A200040', 'A200011', 'A200010', 'A200351') order by mnu_id "
        End If
        Session("bApprovalsPending") = Nothing
        ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sqlString)
        grdViewPendingpost.DataSource = ds
        grdViewPendingpost.DataBind()
    End Sub

    Sub bindBusinessunit()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     USRBSU.BSU_NAME, LTRIM(RTRIM(USRBSU.BSU_ID)) + '||' +" _
             & " REPLACE(CONVERT(VARCHAR(11), BSU.BSU_DAYCLOSEDT, 113), ' ', '/') AS BSUID" _
             & " FROM dbo.fn_GetBusinessUnits('" & Session("sUsr_name") & "') AS USRBSU INNER JOIN" _
             & " BUSINESSUNIT_M AS BSU ON USRBSU.BSU_ID = BSU.BSU_ID ORDER BY USRBSU.BSU_NAME"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddBusinessunit.Items.Clear()
            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSUID"
            ddBusinessunit.DataBind()
            GetPayMonthYear(ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-"))
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        GetPayMonthYear(ddBusinessunit.SelectedItem.Value.Split("||")(2))
    End Sub

    Private Sub GetPayMonthYear(ByVal data As String) 
        txtFrom.Text = ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-")
        txtFrom.Text = data
        ViewState("T_OLD_DAYCLOSE") = txtFrom.Text
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Not UsrCheckList1.MandatoryFieldsChecked Then
                lblError.Text = "Please select All Mandatory Check List"
                Exit Sub
            End If
            If Not CheckPAYMONTH_YEAR() Then
                Exit Sub
            End If
            If Session("bApprovalsPending") Then
                lblError.Text = "Please clear all the pending approval"
                Exit Sub
            End If
            Dim strBusinessUnit As String
            strBusinessUnit = ddBusinessunit.SelectedItem.Text
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim conn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            conn.Open()
            Dim trans As SqlTransaction = conn.BeginTransaction
            Try
                If Not UsrCheckList1.SaveCheckListDetails(conn, trans, txtNewFreezeDate.Text, ddBusinessunit.SelectedItem.Value.Split("||")(0)) Then
                    lblError.Text = "Please select All Mandatory Check List"
                    trans.Rollback()
                    stTrans.Rollback()
                    Exit Sub
                End If
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = ddBusinessunit.SelectedItem.Value.Split("||")(0)
                pParms(1) = New SqlClient.SqlParameter("@BSU_DAYCLOSEDT", SqlDbType.DateTime)
                pParms(1).Value = CDate(txtNewFreezeDate.Text)

                pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                'Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim retval As Integer

                retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[SaveBSU_FREEZEDAY]", pParms)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "DAY END PROCESS", "DAY END PROCESS", Session("sUsr_name"), Nothing, "-Updated the Colse Day to " & txtNewFreezeDate.Text)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                If pParms(2).Value = "0" Then
                    lblError.Text = getErrorMessage("0")
                    trans.Commit()
                    stTrans.Commit()
                Else
                    lblError.Text = getErrorMessage(pParms(2).Value)
                    stTrans.Rollback()
                    trans.Rollback()
                End If
                bindBusinessunit()
                ddBusinessunit.SelectedIndex = -1
                ddBusinessunit.Items.FindByText(strBusinessUnit).Selected = True
                GetPayMonthYear(ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-"))
                txtNewFreezeDate.Text = ""
            Catch ex As Exception
                stTrans.Rollback()
                trans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
                conn.Close()
            End Try
        Catch ex As Exception
            lblError.Text = "Please check the Date"
        End Try
    End Sub

    Protected Sub grdViewPendingpost_RowDataBound(ByVal sender As Object, _
    ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdViewPendingpost.RowDataBound
        Dim item = e.Row
        Dim lblmnuCode As New Label
        lblmnuCode = TryCast(e.Row.FindControl("lblmnuCode"), Label)
        Dim hlview As New HyperLink
        hlview = TryCast(e.Row.FindControl("hplData"), HyperLink)
        If hlview IsNot Nothing And lblmnuCode IsNot Nothing Then
            Select Case lblmnuCode.Text
                Case "A200012" 'Bank Receipt
                    AccountsDetails.SetBankReceipt(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Bank Receipt")
                Case "A200005" 'Cash Receipt
                    AccountsDetails.SetCashReceipt(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Cash Receipt")
                Case "A200040" 'Credit Card Receipt
                    AccountsDetails.SetCreditCardReceipt(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Credit Card Receipt")
                Case "A200011" 'Bank Payment
                    AccountsDetails.SetBankPayment(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Bank Payment")
                Case "A200010" 'Cash Payment
                    AccountsDetails.SetCashPayment(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Cash Payment")
                Case "A200351" 'TreasuryTransfer
                    AccountsDetails.SetTreasuryTransfer(hlview, True, txtNewFreezeDate.Text)
                    item.Cells(1).Attributes.Add("title", "Cash Payment")
            End Select
            If CInt(hlview.Text) <> 0 Then
                Session("bApprovalsPending") = True
            End If
        End If
    End Sub


    Private Function CheckPAYMONTH_YEAR() As Boolean
        If txtFrom.Text = "" Then
            Return True
        End If
        Dim dtOldPayterm As Date = txtFrom.Text
        Dim dtNewPayterm As Date = txtNewFreezeDate.Text
        If dtOldPayterm >= dtNewPayterm Then
            lblError.Text = "New Freeze period should be greater than the Current"
            Return False
        Else
            Return True
        End If
        Return False
    End Function

    Protected Sub txtNewFreezeDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAccountsDetails()
    End Sub

   
End Class
