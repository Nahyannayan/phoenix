Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccAddNewAccount
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                gvRPTSETUP_M.Attributes.Add("bordercolor", "#1b80b6")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("MainMnu_code") = MainMnu_code
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (MainMnu_code <> "A100010" And MainMnu_code <> "A100002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim i As Integer
                    tr_ControlAccount.Visible = True
                    tr_SubGroup.Visible = False
                    btnSave.Attributes.Add("onclick", "javascript:return Validate();")
                    txtSGroup.Attributes.Add("readonly", "readonly")
                    txtSGroupDescr.Attributes.Add("readonly", "readonly")
                    txtControlAcc.Attributes.Add("readonly", "readonly")
                    txtControlAccDescr.Attributes.Add("readonly", "readonly")
                    txtAccCode.Attributes.Add("readonly", "readonly")
                    set_visible_accounts()
                    bindCurrency()
                    For i = 0 To cmbCurrency.Items.Count - 1
                        If cmbCurrency.Items(i).Value = Session("BSUCurrId") Then
                            cmbCurrency.SelectedIndex = i
                            Exit For
                        End If
                    Next
                    BindType()
                    BindPolicyGrp()
                    BindPymntTrm()
                    BindBusinessUnits()

                    Using CountryReader As SqlDataReader = AccessRoleUser.GetCountry()
                        While CountryReader.Read
                            cmbCountry.Items.Add(New ListItem(CountryReader("CTY_DESCR").ToString, CountryReader("CTY_ID").ToString))
                        End While
                    End Using
                    Using CountryReader As SqlDataReader = AccessRoleUser.GetCity()
                        While CountryReader.Read
                            cmbCity.Items.Add(New ListItem(CountryReader("CIT_DESCR").ToString, CountryReader("CIT_ID").ToString))
                        End While
                    End Using
                    Call radioStatus()
                    Call BankCash()
                    chkActive.Checked = True
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Private Sub BindBusinessUnits()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim Qry As New StringBuilder
        Select Case ViewState("MainMnu_code")
            Case "A100002" ' limits the business units by user's access for GEMS GLOBAL
                Qry.Append("SELECT BSU_ID ,BSU_NAME FROM dbo.BUSINESSUNIT_M AS A WITH(NOLOCK) INNER JOIN ")
                Qry.Append("dbo.USERACCESS_S B WITH(NOLOCK) ON A.BSU_ID=B.USA_BSU_ID INNER JOIN dbo.USERS_M C ")
                Qry.Append("ON B.USA_USR_ID=C.USR_ID WHERE 1=1 AND USR_NAME='" & Session("sUsr_name") & "' ")
            Case Else
                Qry.Append("SELECT BSU_ID ,BSU_NAME FROM dbo.BUSINESSUNIT_M AS A WITH(NOLOCK) ")
        End Select
        Qry.Append(" ORDER BY BSU_NAME")
        Dim busString As String = Qry.ToString

        Dim ds1 As New DataSet
        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, busString)
        Dim row As DataRow
        For Each row In ds1.Tables(0).Rows
            Dim str1 As String = row("BSU_NAME")
            Dim str2 As String = row("BSU_ID")
            chkBusUnit.Items.Add(New ListItem(str1, str2))
        Next
        conn.Close()

        For Each item As ListItem In chkBusUnit.Items
            item.Selected = True
        Next
    End Sub
     
    Private Sub bindCurrency()
        Try
            Dim dt As DataTable
            dt = MasterFunctions.GetExchangeRates(Now(), Session("sBSUID"), Session("Bsu_Currency"))
            cmbCurrency.DataSource = dt
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function set_default_currency() As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As ListItem In cmbCurrency.Items
                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then
                        item.Selected = True
                        Return True
                        Exit For
                    End If
                Next
            Else
            End If
            Return False
        Catch ex As Exception
            'Errorlog(ex.Message)
            Return False
        End Try

    End Function


    Private Sub BindType()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT DISTINCT ACT_TYPE From Accounts_M"
            cmbType.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbType.DataTextField = "ACT_TYPE"
            cmbType.DataValueField = "ACT_TYPE"
            cmbType.DataBind()
            bind_RPTSETUP_M()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub BindPolicyGrp()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT PLY_ID,PLY_DESCR From Policy_M"
            cmbPolicyGrp.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbPolicyGrp.DataTextField = "PLY_DESCR"
            cmbPolicyGrp.DataValueField = "PLY_ID"
            cmbPolicyGrp.DataBind()
        Catch ex As Exception
        End Try
    End Sub


    Private Sub BindPymntTrm()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT PTM_ID,PTM_DESCR From PaymentTerm_M"
            cmbPymntTrm.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbPymntTrm.DataTextField = "PTM_DESCR"
            cmbPymntTrm.DataValueField = "PTM_ID"
            cmbPymntTrm.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Public Function GetNext(ByVal pControlAcc As String, ByVal pChar As String, _
    ByVal pLen As Integer, ByVal pType As String) As String
        Dim lstrRetVal As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim SqlCmd As New SqlCommand("NextAccountNo", objConn)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@pControlAcc", pControlAcc)
            SqlCmd.Parameters.AddWithValue("@pLength", pLen)
            SqlCmd.Parameters.AddWithValue("@pChar", pChar)
            SqlCmd.Parameters.AddWithValue("@Type", pType)
            ' Dim pRetVal As New SqlParameter("@pReturnVal", SqlDbType.VarChar)
            'pRetVal.Direction = ParameterDirection.Output
            'SqlCmd.Parameters.Add(pRetVal)
            'SqlCmd.ExecuteNonQuery()
            lstrRetVal = SqlCmd.ExecuteScalar()
        Catch ex As Exception
        End Try
        Return lstrRetVal
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        Dim lstrFlag As String
        Dim lstrSelOne As String = String.Empty
        Dim flagReportlinkSelected As Boolean = False
        'Dim flagReportType As Boolean = False
        Dim lstrReptype As String
        Dim lstrAllBusUnit As String = String.Empty
        If optCustomer.Checked = True Then
            lstrFlag = "C"
        ElseIf optSupplier.Checked = True Then
            lstrFlag = "S"
        Else
            lstrFlag = "N"
        End If
        '       VALIDATIONS 
        ServerValidate()
        lstrReptype = ""
        For Each item As ListItem In chkBusUnit.Items
            If (item.Selected) Then
                'loop through the each checkbox and insert it
               lstrSelOne = "OK"
            End If
        Next
        If txtControlAcc.Text = "" Then
            txtControlAcc.Text = txtAccCode.Text & txtAccCode2.Text
        End If
        If (optControlAccNo.Checked = True) Then
            'If (txtControlAcc.Text = "06101001") Then
            If optSupplier.Checked Then
                txtAccCode2.Text = Left(txtAccDescr.Text, 1) + GetNext(txtControlAcc.Text, Left(txtAccDescr.Text, 1), 4, "S")
            ElseIf optCustomer.Checked Then
                txtAccCode2.Text = Left(txtAccDescr.Text, 1) + GetNext(txtControlAcc.Text, Left(txtAccDescr.Text, 1), 4, "C")
            Else
                txtAccCode2.Text = GetNext(txtControlAcc.Text, Left(txtAccDescr.Text, 1), 4, "")
            End If
        End If
        If (lstrErrMsg = "") Then 
            '    Proceed to Save 
            If lstrSelOne = "OK" Then
                For Each item As ListItem In chkBusUnit.Items
                    If (item.Selected) Then
                        'loop through the each checkbox and insert it
                        Dim lstrBusUnit As String = item.Value
                        lstrAllBusUnit = lstrAllBusUnit & "," & lstrBusUnit
                    End If
                Next
                lstrAllBusUnit = Mid(lstrAllBusUnit, 2, lstrAllBusUnit.Length)
            Else
                lstrAllBusUnit = String.Empty
            End If

            If ViewState("datamode") = "add" Then 
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim SqlCmd As New SqlCommand("SaveACCOUNTS_M", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    If lstrFlag = "N" Then
                        SqlCmd.Parameters.AddWithValue("@ACT_ID", txtAccCode.Text & txtAccCode2.Text)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_ID", Left(txtAccCode.Text, 3) & txtAccCode2.Text)
                    End If


                    SqlCmd.Parameters.AddWithValue("@ACT_NAME", txtAccDescr.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_SGP_ID", txtAccCode.Text.Trim)
                    SqlCmd.Parameters.AddWithValue("@ACT_TYPE", cmbType.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_BANKCASH", cmbBankCash.SelectedItem.Value)
                    If optControlAccYes.Checked Then
                        SqlCmd.Parameters.AddWithValue("@ACT_CTRLACC", txtAccCode.Text & txtAccCode2.Text)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_CTRLACC", txtControlAcc.Text)
                    End If
                    SqlCmd.Parameters.AddWithValue("@ACT_Bctrlac", optControlAccYes.Checked)
                    SqlCmd.Parameters.AddWithValue("@ACT_FLAG", lstrFlag)
                    SqlCmd.Parameters.AddWithValue("@ACT_bPREPAYMENT", False)
                    SqlCmd.Parameters.AddWithValue("@ACT_BANKACCNO", txtBankAccount.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CREDITDAYS", Val(txtCreditDays.Text))
                    If chkActive.Checked = True Then
                        SqlCmd.Parameters.AddWithValue("@ACT_BACTIVE", True)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_BACTIVE", False)
                    End If
                    If cmbBankCash.SelectedItem.Value = "B" Then
                        SqlCmd.Parameters.AddWithValue("@ACT_FACILITYCEILING", txtFaciCelng.Text)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_FACILITYCEILING", "0")
                    End If

                    SqlCmd.Parameters.AddWithValue("@ACT_PLY_ID", cmbPolicyGrp.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_CUR_ID", cmbCurrency.SelectedItem.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_ADDRESS1", txtAddress1.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_ADDRESS2", txtAddress2.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CITY", cmbCity.SelectedValue)
                    SqlCmd.Parameters.AddWithValue("@ACT_COUNTRY", cmbCountry.SelectedValue)
                    SqlCmd.Parameters.AddWithValue("@ACT_PHONE", txtPhone.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_FAX", txtFax.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_MOBILE", txtMobile.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_EMAIL", txtEmail.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CONTACTPERSON", txtContact.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CONTACTDESIG", txtDesig.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_SALESTAXNO", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_PTM_ID", cmbPymntTrm.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_OPP_ACT_ID", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_RFS_ID", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_RFS_ID_CF", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_OPBAL", "0")
                    SqlCmd.Parameters.AddWithValue("@ACT_CURRBAL", "0")
                    SqlCmd.Parameters.AddWithValue("@ACT_BSU_ID", lstrAllBusUnit)
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)

                    Dim sqlpnewACT_ID As New SqlParameter("@newACT_ID", SqlDbType.VarChar, 50)
                    sqlpnewACT_ID.Direction = ParameterDirection.Output
                    SqlCmd.Parameters.Add(sqlpnewACT_ID)

                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt) '@newACT_ID
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                    Dim strNewactid As String
                    strNewactid = sqlpnewACT_ID.Value & ""
                    '   --- Proceed to Save The BusinessUnits
                    If lintRetVal = 0 Then
                        lstrAllBusUnit = ""

                        For Each item As ListItem In chkBusUnit.Items
                            If (item.Selected) Then
                                'loop through the each checkbox and insert it
                                Dim lstrBusUnit As String = item.Value
                                lstrAllBusUnit = lstrAllBusUnit & "|" & lstrBusUnit
                            End If
                        Next

                        Dim SqlCmd2 As New SqlCommand("InsertUnits", objConn, stTrans)
                        SqlCmd2.CommandType = CommandType.StoredProcedure
                        SqlCmd2.Parameters.AddWithValue("@pActId", txtAccCode.Text & txtAccCode2.Text)
                        SqlCmd2.Parameters.AddWithValue("@IDs", lstrAllBusUnit)
                        SqlCmd2.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        SqlCmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        SqlCmd2.ExecuteNonQuery()
                        lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                        If lintRetVal = 0 Then
                            '''''
                            For Each row As GridViewRow In gvRPTSETUP_M.Rows
                                ' Access the CheckBox
                                Dim lblRSM_TYP As Label = TryCast(row.FindControl("lblRSM_TYP"), Label)
                                Dim hfDescription As HiddenField = TryCast(row.FindControl("hfDescription"), HiddenField)
                                Dim hfDescription1 As HiddenField = TryCast(row.FindControl("hfDescription1"), HiddenField)

                                If hfDescription.Value.EndsWith("|") Then
                                    hfDescription.Value = hfDescription.Value.Substring(0, hfDescription.Value.Length - 1)
                                End If
                                Dim txtDesc As TextBox = TryCast(row.FindControl("txtDesc"), TextBox)
                                Dim txtSub As TextBox = TryCast(row.FindControl("txtCode2"), TextBox)
                                
                                If lblRSM_TYP IsNot Nothing Then
                                    If hfDescription.Value.Split("|").Length > 1 Then

                                        If hfDescription1.Value = "" Then
                                            lblErr.Text = "Please select opposite code"
                                            stTrans.Rollback()
                                            Exit Sub
                                        End If
                                       
                                        If hfDescription.Value.Split("|").Length > 1 Then
                                            lintRetVal = AccountFunctions.SaveRPTSETUP_S_ACC(lblRSM_TYP.Text, hfDescription.Value.Split("|")(0), _
                                            hfDescription1.Value.Split("|")(0), strNewactid, txtSub.Text, stTrans)
                                            flagReportlinkSelected = True
                                            'flagReportType = True
                                        Else
                                            lblErr.Text = "Please select opposite code"
                                            stTrans.Rollback()
                                            Exit Sub
                                        End If
                                    Else

                                        If hfDescription.Value <> "" Then
                                            If CheckSubAccount(lblRSM_TYP.Text, hfDescription.Value) And txtSub.Text = "" Then
                                                lblErr.Text = "Please select Sub Account"
                                                stTrans.Rollback()
                                                Exit Sub
                                            End If

                                            lintRetVal = AccountFunctions.SaveRPTSETUP_S_ACC(lblRSM_TYP.Text, hfDescription.Value, "", strNewactid, txtSub.Text, stTrans)
                                            flagReportlinkSelected = True
                                            'flagReportType = True
                                        Else
                                            If (lblRSM_TYP.Text = "MB") Then
                                                If hfDescription.Value = "" Then
                                                    'flagReportType = False
                                                    lstrReptype = "MB"
                                                    flagReportlinkSelected = False
                                                End If
                                            ElseIf (lblRSM_TYP.Text = "MP") Then
                                                If hfDescription.Value = "" Then
                                                    'flagReportType = False
                                                    lstrReptype = "MP"
                                                    flagReportlinkSelected = False
                                                End If
                                            ElseIf (lblRSM_TYP.Text = "AC") Then
                                                If hfDescription.Value = "" Then
                                                    'flagReportType = False
                                                    lstrReptype = "AC"
                                                    flagReportlinkSelected = False
                                                End If
                                            End If
                                    End If
                                        End If
                                End If
                            Next
                            If lintRetVal <> 0 Or Not flagReportlinkSelected Or lstrReptype <> "" Then
                                'lblErr.Text = "Please Select Proper Report Code/Link Atleast one Report"
                                If lstrReptype = "MB" Then
                                    lblErr.Text = "Please Select Balance Sheet and Cashflow Statements Report links"
                                ElseIf lstrReptype = "MP" Then
                                    lblErr.Text = "Please Select Profit/Loss and Cashflow Statements Report links"
                                Else
                                    lblErr.Text = "Please Select Atleast two Report links"
                                End If
                                stTrans.Rollback()
                                Exit Sub
                            Else

                                stTrans.Commit()
                            End If
                                '''''

                                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtAccCode.Text & txtAccCode2.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                                If flagAudit <> 0 Then
                                    Throw New ArgumentException("Could not process your request")
                                End If
                                ViewState("datamode") = "none"
                                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Call clearall()
                                bind_RPTSETUP_M()
                            lblErr.Text = "Account created & COA code generated...<br>New Account id:-" & strNewactid
                            Else
                                stTrans.Rollback()
                                lblErr.Text = getErrorMessage(lintRetVal)
                            End If
                    Else
                        lblErr.Text = getErrorMessage(lintRetVal)
                    End If
                Catch ex As Exception
                    lblErr.Text = UtilityObj.getErrorMessage("1000")
                    UtilityObj.Errorlog(ex.Message)
                End Try
            End If
        End If
    End Sub


    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If optControlAccYes.Checked = True Then
            If txtSGroup.Text = "" Then
                lstrErrMsg = lstrErrMsg & "Please Select The Subgroup" & "<br>"
            End If
        Else
            If txtControlAcc.Text = "" Then
                lstrErrMsg = lstrErrMsg & "Please Select The Control Account" & "<br>"
            End If
        End If
        If optControlAccYes.Checked = True Then
            If Len(txtAccCode2.Text) < 4 Then
                lstrErrMsg = lstrErrMsg & "Account Code Should Be Eight Characters Long" & "<br>"
            End If
        End If
        If cmbBankCash.SelectedValue = "B" Then
            If (IsNumeric(txtFaciCelng.Text) = False) Then
                lstrErrMsg = lstrErrMsg & "Facility Celing Should Be A Numeric Value" & "<br>"
            End If
        End If
        lblErr.Text = lstrErrMsg
    End Sub


    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("SAVED SUCCESSFULLY...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function


    Protected Sub optControlAccYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccYes.CheckedChanged
        set_visible_accounts()
    End Sub


    Protected Sub optControlAccNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccNo.CheckedChanged
        set_visible_accounts()
    End Sub


    Sub set_visible_accounts()
        If optControlAccYes.Checked = True Then
            tr_ControlAccount.Visible = False
            tr_SubGroup.Visible = True
            txtAccCode.Text = ""
            txtAccCode2.ReadOnly = False
            txtAccCode2.Text = ""
            txtAccDescr.Text = ""
            txtControlAcc.Text = ""
            txtControlAccDescr.Text = ""
            optSupplier.Checked = False
            optCustomer.Checked = False
            optNormal.Checked = True
            optSupplier.Enabled = False
            optCustomer.Enabled = False
            optNormal.Checked = True
            cmbType.SelectedIndex = 0
        Else
             
            optSupplier.Enabled = True
            optCustomer.Enabled = True

            tr_ControlAccount.Visible = True
            tr_SubGroup.Visible = False
            txtAccCode2.ReadOnly = True
            txtAccCode.Attributes.Add("readonly", "readonly")
            txtAccCode.Text = ""
            txtAccCode2.ReadOnly = True
            txtAccCode2.Text = ""
            txtAccDescr.Text = ""
        End If
    End Sub

  
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        set_visible_accounts()
        Call clearall()
        Call radioStatus()
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Sub clearall()
        txtSGroup.Text = ""
        txtSGroupDescr.Text = ""
        'txtControlAcc.Text = ""
        'txtControlAccDescr.Text = ""
        txtAccCode.Text = ""
        txtAccCode2.Text = ""
        txtAccDescr.Text = ""
        txtBankAccount.Text = ""
        txtSalesTax.Text = ""
        txtFaciCelng.Text = ""
        txtContact.Text = ""
        txtDesig.Text = ""
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtFax.Text = ""
        txtCreditDays.Text = 90
        gvRptDetails.Dispose()
    End Sub


    Protected Sub optNormal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optNormal.CheckedChanged
        Call radioStatus()
    End Sub


    Sub radioStatus()
        Dim i As Integer

        If optSupplier.Checked = False Then
            txtControlAcc.Text = ""
            txtControlAccDescr.Text = ""

            tr_Sales.Visible = False
            cmbType.SelectedIndex = 0
            txtAccCode2.Enabled = True
        Else
            Dim STR_CTRLACC_NAME = AccountFunctions.get_CtrlAccount()
            If STR_CTRLACC_NAME <> "" Then
                txtControlAcc.Text = STR_CTRLACC_NAME.SPLIT("|")(0)
                txtControlAccDescr.Text = STR_CTRLACC_NAME.SPLIT("|")(1)
                'GetNext(txtControlAcc.Text, "S", 4)
                txtAccCode.Text = STR_CTRLACC_NAME.SPLIT("|")(2)

            End If

            tr_Sales.Visible = True
            For i = 0 To cmbType.Items.Count - 1
                If cmbType.Items(i).Value = "LIABILITY" Then
                    cmbType.SelectedIndex = i
                    Exit For
                End If
            Next
            txtAccCode2.Enabled = False
        End If
        If optCustomer.Checked Or optSupplier.Checked Then
            trCreditDays.Visible = True
        Else
            trCreditDays.Visible = False
        End If
    End Sub


    Protected Sub optCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optCustomer.CheckedChanged
        Call radioStatus()
    End Sub


    Protected Sub optSupplier_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optSupplier.CheckedChanged
        Call radioStatus()
    End Sub


    Protected Sub cmbBankCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBankCash.SelectedIndexChanged
        Call BankCash()
    End Sub


    Sub BankCash()
        If cmbBankCash.SelectedValue <> "B" Then
            txtFaciCelng.Text = ""
            txtFaciCelng.Enabled = False
            Label2.Enabled = False
        Else
            txtFaciCelng.Enabled = True
            Label2.Enabled = True
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Dim url As String
            Dim mainMnu_code As String = String.Empty
            ViewState("datamode") = "edit"
            'set the rights on the button control based on the user
            mainMnu_code = Request.QueryString("MainMnu_code")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Accounts\AccEditAccount.aspx?MainMnu_code={0}&datamode={1}", mainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed "
        End Try

    End Sub


    Protected Sub cmbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged
        bind_RPTSETUP_M()
    End Sub


    Sub bind_RPTSETUP_M()
        Try
            Dim lstrSql As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            If cmbType.SelectedItem.Value.ToLower = "expenses" Or cmbType.SelectedItem.Value.ToLower = "income" Then
                'lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP = 'p')"
                lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP <> 'B') and (TYP<>'C')"
            Else
                'lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP = 'B')"
                lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP <> 'P') and (TYP<>'C')"
            End If
            gvRPTSETUP_M.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            
            gvRPTSETUP_M.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub LinkButton1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbSet As New LinkButton
        Dim lbSet1 As New LinkButton
        'Dim lbSet2 As New LinkButton
        Dim txtDesc As New TextBox
        Dim txtDesc1 As New TextBox
        ' Dim txtDesc2 As New TextBox
        Dim hfDescription As New HiddenField
        Dim hfDescription1 As New HiddenField
        Dim hfDescription2 As New HiddenField
        txtDesc = sender.parent.findcontrol("txtCode")
        txtDesc1 = sender.parent.findcontrol("txtCode1")
        'txtDesc2 = sender.parent.findcontrol("txtCode2")
        lbSet1 = sender.parent.findcontrol("lbSet1")
        'lbSet2 = sender.parent.findcontrol("LinkButton2")
        txtDesc1.Style.Clear()
        'txtDesc1.Style.Add("",
        hfDescription = sender.parent.findcontrol("hfDescription")
        If hfDescription.Value <> "" Then
            If hfDescription.Value.Substring(0, hfDescription.Value.Length - 1).Split("|").Length = 1 Then
                txtDesc1.Attributes.Add("style", "display:none")
                lbSet1.Attributes.Add("style", "display:none")
            Else
                lbSet1.Style.Clear()
            End If
        Else
            txtDesc1.Attributes.Add("style", "display:none")
            lbSet1.Attributes.Add("style", "display:none")
        End If


        hfDescription = sender.parent.findcontrol("hfDescription")


        hfDescription1 = sender.parent.findcontrol("hfDescription1")
        hfDescription2 = sender.parent.findcontrol("hfDescription2")
        lbSet = sender
        Dim str_clientclick As String = "','" & txtDesc.ClientID & "','" & hfDescription.ClientID & "','" & txtDesc1.ClientID & "')"
        Dim str_clientclick1 As String = "','" & txtDesc1.ClientID & "','" & hfDescription1.ClientID & "','')"
        'Dim str_clientclick2 As String = "','" & txtDesc2.ClientID & "','" & hfDescription2.ClientID & "','')"
        lbSet.OnClientClick = lbSet.OnClientClick.ToString.Replace("')", str_clientclick)
        lbSet1.OnClientClick = lbSet1.OnClientClick.ToString.Replace("')", str_clientclick1)
        'lbSet2.OnClientClick = lbSet2.OnClientClick.ToString.Replace("')", str_clientclick2)
    End Sub


    Sub CheckDRCrEntry()
        For Each row As GridViewRow In gvRPTSETUP_M.Rows
            ' Access the CheckBox
            Dim cb As CheckBox = TryCast(row.FindControl("chkSelect"), CheckBox)
            If cb IsNot Nothing Then
                Dim lstrBusUnit As String = Convert.ToString(gvRPTSETUP_M.DataKeys(row.RowIndex).Value)

            End If
        Next
    End Sub

    
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub


    

    Protected Sub txtAccDescr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccDescr.TextChanged
        gvAccountList.DataSource = AccountFunctions.GetMatchingAccountList(txtAccDescr.Text)
        gvAccountList.DataBind()

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub txtSGroup_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'FillRptDetails()
    End Sub
    Sub FillRptDetails()
        Dim strSql As String
        Dim ds As New DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If txtAccCode.Text <> "" And txtAccCode2.Text <> "" Then
            strSql = "SELECT RPTSETUP_M.RSM_DESCR, RPTSETUP_S.RSS_CODE,RPTSETUP_S.RSS_DESCR " _
                      & " FROM   RPTSETUP_M INNER JOIN " _
                      & "RPTSETUP_S ON RPTSETUP_M.RSM_TYP = RPTSETUP_S.RSS_TYP INNER JOIN " _
                      & " RPTSETUP_S_ACC ON RPTSETUP_S.RSS_TYP = RPTSETUP_S_ACC.RPA_RSS_TYP AND " _
                      & " RPTSETUP_S.RSS_CODE = RPTSETUP_S_ACC.RPA_RSS_CODE " _
                      & " WHERE RPTSETUP_S_ACC.RPA_ACC_ID='" & txtAccCode.Text & txtAccCode2.Text & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            gvRptDetails.DataSource = ds
            gvRptDetails.DataBind()
        End If
    End Sub

    Protected Sub txtControlAcc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub txtControlAccDescr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillRptDetails()
    End Sub

    Protected Sub LinkButton2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbSubAcc As New LinkButton
        Dim txtSub As New TextBox
        Dim hfSubDescription As New HiddenField
        txtSub = sender.parent.findcontrol("txtCode2")
        txtSub.Style.Clear()
        hfSubDescription = sender.parent.findcontrol("hfDescription")
        lbSubAcc = sender

        If hfSubDescription.Value <> "" Then
            If hfSubDescription.Value.Substring(0, hfSubDescription.Value.Length - 1).Split("|").Length = 1 Then
                txtSub.Attributes.Add("style", "display:none")

                'Else
                '    lbSet1.Style.Clear()
            End If
        Else
            txtSub.Attributes.Add("style", "display:none")
            'lbSet1.Attributes.Add("style", "display:none")
        End If
        
        Dim str_clientclick2 As String = "','" & txtSub.ClientID & "','" & hfSubDescription.ClientID & "','')"
        lbSubAcc.OnClientClick = lbSubAcc.OnClientClick.ToString.Replace("')", str_clientclick2)
    End Sub
    Private Function CheckSubAccount(ByVal RssType As String, ByVal RssCode As String) As Boolean
        Dim strSQL As String
        Dim dsSub As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        strSQL = "SELECT RSB_ID,RSB_DESCRIPTION FROM RPTSETUPSUB_S WHERE RSB_RSS_TYP='" & RssType & "' AND RSB_RSS_CODE='" & RssCode & "'"
        dsSub = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsSub.Tables(0).Rows.Count > 0 Then
            CheckSubAccount = True
        Else
            CheckSubAccount = False
        End If
    End Function
End Class
