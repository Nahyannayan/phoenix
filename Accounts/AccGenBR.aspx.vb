Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class AccGenBR
    Inherits System.Web.UI.Page 
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDocDate.Attributes.Add("readonly", "readonly")
        If Page.IsPostBack = False Then
            tr_grid.Visible = False
            '''''check menu rights
            Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or (Session("MainMnu_code") <> OASISConstants.MNU_CASH_DEPOSIT) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            End If
            '''''check menu rights
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
        For Each gvr As GridViewRow In gvJournal.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As CheckBox = CType(gvr.FindControl("chkControl"), CheckBox)
            ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
        Next
        'gridbind()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        RememberOldValues()
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
        RePopulateValues()
    End Sub

    Protected Sub gridbind()
        Dim ds As New DataSet
        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If (txtDocDate.Text = "") Then
            str_Sql = "select getdate()  as CurrDate"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                txtDocDate.Text = String.Format("{0:dd/MMM/yyyy}", ds.Tables(0).Rows(0)("CurrDate"))
            Else
                txtDocDate.Text = String.Format("{0:dd/MMM/yyyy}", txtDocDate.Text)
            End If
        End If
        str_Sql = "SELECT Distinct A.* FROM [VOUCHER_H] A INNER JOIN VOUCHER_D B ON A.VHH_BSU_ID=B.VHD_BSU_ID AND A.VHH_DOCNO=B.VHD_DOCNo" _
        & " WHERE VHH_BSU_Id='" & Session("sBSUID") & "' AND VHH_DOCDt<='" & Convert.ToDateTime(txtDocDate.Text) & "' AND VHH_DOCTYPE='CR'" _
        & " AND isnull(VHH_bPOsted,0)=1 and isnull(VHH_bdeleted,0)=0   AND isNull(VHD_bBANKRECEIPT,0)=0 AND isNull(VHD_bBANKRECNO,'')=''" _
        & " AND isnull(VHH_OWNER_BSU_ID,'')= '' ORDER BY VHH_DOCDT DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        gvJournal.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            tr_grid.Visible = True
        Else
            tr_grid.Visible = False
        End If
    End Sub

    Private Sub RememberOldValues()
        Dim categoryIDList As New ArrayList()
        Dim index As Integer = -1
        For Each row As GridViewRow In gvJournal.Rows
            Dim lstrIndex As String = Convert.ToString(gvJournal.DataKeys(row.RowIndex).Value)
            Dim cb As CheckBox = TryCast(row.FindControl("chkControl"), CheckBox)

            ' Check in the Session
            If Session("CHECKED_ITEMS") IsNot Nothing Then
                categoryIDList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
            End If
            If cb IsNot Nothing AndAlso cb.Checked Then
                If Not categoryIDList.Contains(lstrIndex) Then
                    categoryIDList.Add(lstrIndex)
                End If
            Else
                categoryIDList.Remove(lstrIndex)
            End If
        Next
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            Session("CHECKED_ITEMS") = categoryIDList
        End If
    End Sub

    Private Sub RePopulateValues()
        Dim categoryIDList As ArrayList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            For Each row As GridViewRow In gvJournal.Rows
                Dim lstrIndex As String = Convert.ToString(gvJournal.DataKeys(row.RowIndex).Value)
                If categoryIDList.Contains(lstrIndex) Then
                    Dim myCheckBox As CheckBox = DirectCast(row.FindControl("chkControl"), CheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gv As GridView = e.Row.FindControl("GridView2")
            Dim dbSrc As New SqlDataSource
            dbSrc.ConnectionString = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            dbSrc.SelectCommand = "SELECT A.*,C.ACT_NAME FROM VOUCHER_D A INNER JOIN ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID WHERE VHD_DOCTYPE='CR' AND VHD_DOCNO = '" & _
                e.Row.DataItem("VHH_DOCNO").ToString & "'  AND A.VHD_BSU_ID='" & Session("sBSUID") & "'"
            gv.DataSource = dbSrc
            gv.DataBind()
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If txtNarration.Text.Trim = "" Then
            lblError.Text = "Please enter narration!!!"
            Exit Sub
        End If
        '   --- Initialize The DataTable 
        Dim lstrAllList As String = ""
        Dim i, j As Integer
        Dim str_Sql As String
        Dim lstrDocNo As String
        Dim ldblAmount As Decimal

        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String
        RememberOldValues()
        RePopulateValues()

        Dim categoryIDList As ArrayList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            For i = 0 To categoryIDList.Count - 1
                lstrAllList = lstrAllList & ",'" & categoryIDList.Item(i) & "'"
            Next
        End If

        lstrAllList = Mid(lstrAllList, 2)
        If (lstrAllList = "") Then
            lblError.Text = "Please Select The Voucher(s)!!!"
            Exit Sub
        End If
        '--- GET THE DATES

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        str_Sql = "select Distinct VHH_DOCDT as DocDate FROM VOUCHER_H WHERE VHH_BSU_ID='" & Session("sBSUID") & "' AND VHH_DOCTYPE='CR' AND VHH_DOCNO IN (" & lstrAllList & ")"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Session("dtDATES") = DataTables.CreateDatesTable_CR_Clearance()
        Session("dtDATES").rows.clear()
        Session("dtDATES") = ds.Tables(0)

        '   --- Loop through dtDates and save
        If (Session("dtDATES").Rows.Count > 0) Then
            For i = 0 To Session("dtDATES").Rows.Count - 1

                str_Sql = "select Distinct VHH_DOCNo as DocNo FROM VOUCHER_H WHERE VHH_DOCDT='" & Session("dtDATES").Rows(i)("DocDate") & "' AND VHH_BSU_ID='" & Session("sBSUID") & "' AND VHH_DOCTYPE='CR' AND VHH_DOCNO IN (" & lstrAllList & ")"
                Dim dsDoc As New DataSet
                dsDoc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Session("dtDocNos") = DataTables.CreateDocNosTable_CR_Clearance()
                Session("dtDocNos").rows.clear()
                Session("dtDocNos") = dsDoc.Tables(0)

                ldblAmount = 0
                Session("ldtChqDate") = Session("dtDATES").Rows(i)("DocDate")
                str_Sql = "SELECT Id=count(*),CLS_COL_ACT_ID as AccountId," _
                            & " MAX(VHD_RSS_ID) as CLine,Max(COL_DESCR) as Narration,VHD_COL_ID as Colln, " _
                            & " SUM(VHD_AMOUNT) as Amount,'' as ChqNo,'' as ChqDate,'' as Status,NULL as GUID " _
                            & " FROM VW_OSA_CASHRTOB  WHERE VHH_BSU_ID='" & Session("sBSUID") & "' AND" _
                            & " VHH_DOCDT='" & Session("dtDATES").Rows(i)("DocDate") & "' AND VHD_DOCNO IN (" & lstrAllList & ") " _
                            & " Group BY VHD_COL_ID,CLS_COL_ACT_ID"
                Dim ds2 As New DataSet
                ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Session("dtDTL") = DataTables.CreateDataTable_CR_Clearance()
                Session("dtDTL").rows.clear()

                Session("dtDTL") = ds2.Tables(0)
                For j = 0 To Session("dtDTL").Rows.Count - 1
                    ldblAmount = ldblAmount + Session("dtDTL").Rows(j)("Amount")
                Next
                '   --- Save It ---
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    lstrDocNo = Master.GetNextDocNo("BR", Month(txtDocDate.Text), Year(txtDocDate.Text)).ToString
                    Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                    sqlpGUID.Value = System.DBNull.Value
                    SqlCmd.Parameters.Add(sqlpGUID)
                    SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
                    SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
                    SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
                    SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BR")
                    SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", lstrDocNo)
                    SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "R")
                    SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtDocDate.Text))
                    SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtDocDate.Text))
                    SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
                    SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 1)
                    SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
                    SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
                    SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
                    SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

                    SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

                    SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
                    SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
                    SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", 1)
                    SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarration.Text)
                    SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", Convert.ToInt32(Session("dtDTL").Rows(0)("Colln")))
                    SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", ldblAmount)
                    SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
                    SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
                    SqlCmd.Parameters.AddWithValue("@vhh_bAuto", True)
                    SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
                    Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                    SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                    SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
                    SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
                    SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                    SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                    SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                    If lintRetVal <> 0 Then
                        lblError.Text = getErrorMessage(lintRetVal)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                    lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
                    SqlCmd.Parameters.Clear()

                    If (lintRetVal = 0) Then
                        lintRetVal = DoTransactions(objConn, stTrans, lstrNewDocNo)
                        If lintRetVal = "0" Then
                            '-- Proceed to POST THE TRANSACTION
                            Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                            cmd.CommandType = CommandType.StoredProcedure

                            Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                            sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                            cmd.Parameters.Add(sqlpJHD_SUB_ID)

                            Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                            sqlpsqlpJHD_BSU_ID.Value = Session("sBSUID")
                            cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                            Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                            sqlpJHD_FYEAR.Value = Session("F_YEAR")
                            cmd.Parameters.Add(sqlpJHD_FYEAR)

                            Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                            sqlpJHD_DOCTYPE.Value = "BR"
                            cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                            Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                            sqlpJHD_DOCNO.Value = lstrNewDocNo
                            cmd.Parameters.Add(sqlpJHD_DOCNO)

                            'Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                            'sqlpJHD_DOCDT.Value = txtHDocdate.Text & ""
                            'cmd.Parameters.Add(sqlpJHD_DOCDT)

                            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                            retValParam.Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add(retValParam)
                            cmd.ExecuteNonQuery()
                            lintRetVal = retValParam.Value
                            cmd.Parameters.Clear()

                            If (lintRetVal = 0) Then
                                '   --- END OF POST
                                Dim iIndex As Integer
                                Dim str_refdocnos As String = ""
                                '   --- UPDATE THE RCT NOS ---
                                For iIndex = 0 To Session("dtDocNos").Rows.Count - 1
                                    Dim cmd2 As New SqlCommand("UPDATEVOUCHER", objConn, stTrans)
                                    cmd2.CommandType = CommandType.StoredProcedure
                                    cmd2.Parameters.AddWithValue("@DocType", "CR")
                                    cmd2.Parameters.AddWithValue("@DOC_ID", Session("dtDocNos").Rows(iIndex)("DOCNO"))
                                    str_refdocnos = Session("dtDocNos").Rows(iIndex)("DOCNO") & "," & str_refdocnos
                                    cmd2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                                    cmd2.Parameters.AddWithValue("@RefDocNo", lstrNewDocNo)
                                    cmd2.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                                    cmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                                    cmd2.ExecuteNonQuery()
                                    lintRetVal = CInt(cmd2.Parameters("@ReturnValue").Value)
                                    If lintRetVal <> 0 Then
                                        lblError.Text = getErrorMessage(lintRetVal)
                                        stTrans.Rollback()
                                        Exit Sub
                                    End If
                                    cmd2.Parameters.Clear()
                                    lblError.Text = getErrorMessage(lintRetVal)
                                Next
                                lintRetVal = AccountFunctions.CheckBAnkClearanceData("CR", Session("sBSUID"), lstrNewDocNo, stTrans)
                                If lintRetVal <> 0 Then
                                    lblError.Text = getErrorMessage(lintRetVal)
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "Cash Deposit", Page.User.Identity.Name.ToString, Me.Page, str_refdocnos)
                                If flagAudit <> 0 Then
                                    Throw New ArgumentException("Could not process your request")
                                End If
                                stTrans.Commit()
                                
                                lblError.Text = getErrorMessage(lintRetVal)
                                gridbind()
                                Session("CHECKED_ITEMS") = Nothing
                            Else
                                stTrans.Rollback()
                                lblError.Text = getErrorMessage(lintRetVal)
                            End If
                        Else
                            stTrans.Rollback()
                            lblError.Text = getErrorMessage(lintRetVal)
                        End If
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(lintRetVal)
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    lblError.Text = getErrorMessage("1000")
                    Errorlog(ex.Message, "Gencc")
                Finally
                    objConn.Close()
                End Try
            Next
            gridbind()
        End If
    End Sub 

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
        ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer 
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            cmd.Dispose()
            cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@GUID", Session("dtDTL").Rows(iIndex)("GUID"))
            cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
            cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
            cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "BR")
            cmd.Parameters.AddWithValue("@VHD_DOCNO", Trim(p_docno))
            cmd.Parameters.AddWithValue("@VHD_LINEID", Session("dtDTL").Rows(iIndex)("Id"))
            cmd.Parameters.AddWithValue("@VHD_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
            cmd.Parameters.AddWithValue("@VHD_AMOUNT", Session("dtDTL").Rows(iIndex)("Amount"))
            cmd.Parameters.AddWithValue("@VHD_NARRATION", txtNarration.Text)
            cmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQNO", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtDocDate.Text))
            cmd.Parameters.AddWithValue("@VHD_RSS_ID", Session("dtDTL").Rows(iIndex)("CLine"))
            cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
            cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
            cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
            cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
            cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
            cmd.Parameters.AddWithValue("@VHD_COL_ID", Session("dtDTL").Rows(iIndex)("Colln"))
            cmd.Parameters.AddWithValue("@bEdit", False)

            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()

            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

            Dim success_msg As String = ""
            If iReturnvalue <> 0 Then
                Exit For
            Else
            End If
            cmd.Parameters.Clear()
        Next 
        Return iReturnvalue 
    End Function

    Function GetFill(ByVal p_guid As String) As Boolean
        Dim lstrSQL As String
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        lstrSQL = "SELECT * FROM VOUCHER_H  WHERE A.GUID='" & p_guid & "' "
        ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

        If ds.Tables(0).Rows.Count > 0 Then

        End If
    End Function

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDocDate.TextChanged
        txtDocDate.Text = String.Format("{0:dd/MMM/yyyy}", txtDocDate.Text)
        gridbind()
    End Sub

    Protected Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        Dim str_dates As String = String.Empty
        For Each row As GridViewRow In gvJournal.Rows
            Dim lstrIndex As String = Convert.ToString(gvJournal.DataKeys(row.RowIndex).Value)
            Dim cb As CheckBox = TryCast(row.FindControl("chkControl"), CheckBox)
            If cb IsNot Nothing AndAlso cb.Checked Then
                If str_dates = String.Empty Then
                    str_dates = row.Cells(2).Text
                Else
                    str_dates = str_dates & ", " & row.Cells(2).Text
                End If
            End If
        Next
        txtNarration.Text = "CASH COLLECTION DEPOSIT for " & str_dates
    End Sub

End Class
