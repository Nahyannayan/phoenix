<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="acccnDebitNote.aspx.vb" Inherits="Accounts_acccnDebitNote" Title="Debit Note" EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../images/Misc/minus.gif";
                }
                else {
                    img.src = "../images/Misc/minus.gif";
                }
                img.alt = "Close to view other Customers";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../images/Misc/plus.gif";
                }
                else {
                    img.src = "../images/Misc/plus.gif";
                }
                img.alt = "Expand to show Orders";
            }
        }

        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


            var lstrVal;
            var lintScrVal;

            document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
            document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
            document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var NameandCode;
            var result;
            if (pMode == 'NORMAL') {

                result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                //if (result=='' || result==undefined)
                //{    return false;      } 
                //lstrVal=result.split('||');     
                //document.getElementById(ctrl).value=lstrVal[0];
                //document.getElementById(ctrl1).value=lstrVal[1];
                //document.getElementById(ctrl2).value=lstrVal[2];
                //document.getElementById(ctrl3).value=lstrVal[3];

            }
            else if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");

                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
            var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
            var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (pMode == 'NORMAL') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1]
                }
                else if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                }

            }
        }

        function getAccountH() {
            popUp('960', '600', 'CUSTSUPPnIJV', '<%=txtHAccountcode.ClientId %>', '<%=txtHAccountname.ClientId %>');
            return false;
        }

        function CopyDetails() {
            try {
                if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                    document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
            }
            catch (ex) { }
        }
        function getCashcode() {

            var NameandCode;
            var result;
            result = radopen("acccpShowCashflow.aspx", "pop_up1");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //return false;
        }
        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                return false;

            }
        }
        function getAccount() {
            popUp('960', '600', 'NORMAL', '<%=Detail_ACT_ID.ClientId %>', '<%=txtDAccountName.ClientId %>');
            return false;
        }
        function AddDetails(url) {

            var NameandCode;
            var result;
            var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
            dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
            dates = dates.replace(/[/]/g, '-')
            url_new = url_new + '&dt=' + dates;
            result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "")
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();
            return false;
        }
        function allocate_Header_Cost() {
            if (document.getElementById('<%=txtHamount.ClientID %>').value <= 0) {
                alert('Cannot allocate. Total Amount is Zero');
                return false;
            }
            else {
                AddDetails('vid=hd&amt=' + document.getElementById('<%=txtHamount.ClientID %>').value + '&sid=AST');
                return false;
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Doc No [Old Ref No]</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True" Width="42%"></asp:TextBox>
                                        [
                            <asp:TextBox ID="txtHOldrefno" runat="server" Width="42%" TabIndex="2"></asp:TextBox>
                                        ]
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Doc Date <span style="color: red">*</span></span>
                                    </td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"
                                            Width="80%" TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="5" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Currency </span>
                                    </td>

                                    <td valign="middle" width="30%" align="left">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True"
                                            TabIndex="6" Width="20%">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtHExchRate" runat="server" Width="66%"></asp:TextBox></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Group Rate </span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHLocalRate" runat="server" Width="80%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Account <span style="color: red">*</span> </span>
                                    </td>

                                    <td valign="middle" align="left">
                                        <asp:TextBox ID="txtHAccountcode" runat="server"
                                            AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                runat="server" ControlToValidate="Detail_ACT_ID" ErrorMessage="Account Code Cannot Be Empty"
                                                ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton ID="btnHaccount"
                                                    runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccountH();return false;"
                                                    TabIndex="8" />
                                    </td>
                                    <td valign="middle" align="left" colspan="2">
                                        <asp:TextBox ID="txtHAccountname" runat="server" Style="width: 88% !important"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount <span style="color: red">*</span></span>
                                    </td>

                                    <td valign="middle" align="left" width="30%">
                                        <asp:TextBox ID="txtHamount" runat="server" Width="86%"></asp:TextBox><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txthAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                            ValidationGroup="Details">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator7" runat="server" ControlToValidate="txthAmount" ErrorMessage="Amount Cannot be empty"
                                                ValidationGroup="Details">*</asp:RequiredFieldValidator></td>

                                    <td align="left" width="20%">
                                        <span class="field-label">Narration<span style="color: red">*</span> </span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHNarration" runat="server" Width="80%"
                                            TextMode="MultiLine" MaxLength="300" TabIndex="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Details
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="middle" align="left" width="20%">
                                        <span class="field-label">Account<span style="color: red">*</span> </span>
                                    </td>

                                    <td valign="middle" align="left">

                                        <asp:TextBox ID="Detail_ACT_ID" runat="server" AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="Detail_ACT_ID"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="btnAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount(); return false;"
                                                TabIndex="12" />
                                    </td>
                                    <td valign="middle" align="left" colspan="2">
                                        <asp:TextBox ID="txtDAccountName" runat="server" Style="width: 88% !important"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" width="20%" align="left">
                                        <span class="field-label">Amount <span style="color: red">*</span></span>
                                    </td>
                                    <td valign="middle" align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" Width="86%" TabIndex="14"></asp:TextBox><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                            ValidationGroup="Details">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Narration <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDNarration" runat="server" Width="80%"
                                            TextMode="MultiLine" MaxLength="300" TabIndex="16"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="trTaxType" visible="false">
                                    <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cost Allocation </span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="18" />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="18" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                        <span style="font-size: 10pt; color: #ffffff; font-family: Arial"></span>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Transaction details added yet." Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                                border="0" src="../Images/Misc/plus.gif" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="LinkButton1_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" HeaderText="TEST" Visible="False" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="100%" align="right">
                                                                <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 100%">
                                                                    <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                        CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                            <asp:BoundField DataField="CostCenterTypeName" HeaderText="Costcenter" />
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                            <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                            <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                                HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:TemplateField HeaderText="Amount">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtAmt" runat="server" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                                        onblur="CheckAmount(this)"></asp:TextBox>
                                                                                    <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td colspan="100%" align="right">
                                                                                            <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                                                Width="100%" CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                                    <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                                    <asp:TemplateField HeaderText="Amount">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"><span class="field-label">Total Amount </span>
                                        <asp:TextBox ID="txtDTotalamount" runat="server" Width="20%"> </asp:TextBox>

                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details"
                                            CausesValidation="False" TabIndex="20" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                            TabIndex="22" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="24" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');"
                                            TabIndex="26" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False"
                                            TabIndex="28" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" CausesValidation="False"
                                            TabIndex="30" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <input id="h_mode" runat="server" type="hidden" />
                <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <input id="h_NextLine" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>
