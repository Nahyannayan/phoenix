﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accShowEmpDetailInfo.aspx.vb" Inherits="Accounts_accShowEmpDetailInfo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title></title>
    <base target="_self" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
     <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 

    <script language="javascript" type="text/javascript">
        // function settitle(){
        //alert('fgfg');
        //document.title ='sfsdsd';
        // alert('fgfg');
        //      }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function test(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
             }
             function test1(val) {
                 var path;
                 if (val == 'LI') {
                     path = '../Images/operations/like.gif';
                 } else if (val == 'NLI') {
                     path = '../Images/operations/notlike.gif';
                 } else if (val == 'SW') {
                     path = '../Images/operations/startswith.gif';
                 } else if (val == 'NSW') {
                     path = '../Images/operations/notstartwith.gif';
                 } else if (val == 'EW') {
                     path = '../Images/operations/endswith.gif';
                 } else if (val == 'NEW') {
                     path = '../Images/operations/notendswith.gif';
                 }
                 document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
          }
    </script>
    
</head>
<body onload="listen_window();"> 
    <form id="form1" runat="server">
    <!--1st drop down menu -->                                                   
<div id="dropmenu" class="dropmenudiv" style="width: 110px; left: 153px; top: 1px;display:none;">
<a href="javascript:test('LI');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:test('NLI');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:test('SW');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:test('NSW');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>

<a href="javascript:test('EW');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:test('NEW');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>



<!--2nd drop down menu -->                                                
<div id="dropmenu1" class="dropmenudiv" style="width: 110px; left: 0px; top: -1px;display:none;">
<a href="javascript:test1('LI');"><img class="img_left" alt="Like" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:test1('NLI');"><img class="img_left" alt="Like" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:test1('SW');"><img class="img_left" alt="Like" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:test1('NSW');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>

<a href="javascript:test1('EW');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:test1('NEW');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>                                              
    <div>
        <div>
                        <table  width="100%">
                            <tr>
                                <td class="" colspan="2" valign="top" align="center" style="width: 980px">
                                   <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; color: #0000ff">
                                        <tr>
                                            <td>
                                                <table 
                                                    width="100%">
                                                    <tr>
                                                        <td class="" colspan="4" style="height: 140px" valign="top" align="center">
                                                            <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="ID" CssClass="table table-bordered table-row">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("No") %>'></asp:LinkButton>&nbsp;
                                                                        </ItemTemplate>
                                                                        <HeaderTemplate>
                                                                            <table style="width: 100%; height: 100%">
                                                                                <tr>
                                                                                    <td align="center" style="width: 100px">
                                                                                        <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID" CssClass="gridheader_text"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                       
                                                                                                    <div id="Div1" class="chromestyle" style="display:none;">
                                                                                                        <ul>
                                                                                                            <li><a href="#" rel="dropmenu">
                                                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                                                    style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                              
                                                                                                    <asp:TextBox ID="txtcode" runat="server" Width="92px"></asp:TextBox>
                                                                                              
                                                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                                        OnClick="btnSearchEmpId_Click" />&nbsp;
                                                                                            
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                        <HeaderTemplate>
                                                                            <table style="width: 100%; height: 100%">
                                                                                <tr>
                                                                                    <td align="center"  style="width: 100px; height: 14px;">
                                                                                        <asp:Label ID="lblName" runat="server" Text="Business Unit Name" Width="214px" CssClass="gridheader_text"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                      
                                                                                                    <div id="Div2" class="chromestyle" style="display:none;">
                                                                                                        <ul>
                                                                                                            <li><a href="#" rel="dropmenu1">
                                                                                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                                                    style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                               
                                                                                                    <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                                                                                            
                                                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                                        OnClick="btnSearchEmpName_Click" />&nbsp;
                                                                                            </tr>
                                                                                        </table>
                                                                                 
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                                Text='<%# Eval("E_Name") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            
                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                              
                                                                <RowStyle Height="25px" />
                                                            </asp:GridView>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 12px; width: 511px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="matters" colspan="4" style="width: 567px; height: 28px"
                                    valign="middle">
                        <input type="hidden" id="h_SelectedId" runat="server" value="0" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                            </tr>
                        </table>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
        </table>
    </div>
  <script type="text/javascript">

      cssdropdown.startchrome("Div1")
      cssdropdown.startchrome("Div2")

</script>
                

</form>
    
</body>
</html>
