<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accNewSubGroup.aspx.vb" Inherits="NewSubGroup" Title="New SubGroup" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {

            if (document.getElementById("txtSubGroupcode").value == '') {
                alert("Kindly enter Group Code");
                return false;
            }
            if (document.getElementById("txtSubgroupname").value == '') {
                alert("Kindly enter Sub Group Name");
                return false;
            }
            return true;
        }
        function hide(id) {
            var tdid = 'tbl_Message' + id;
            // alert(tdid);
            document.getElementById(tdid).style.display = 'none';
        }
        function help(id) {
            var NameandCode;
            var result;
            var url;
            url = 'ShowError.aspx?id=' + id;
            result = radopen(url, "pop_up");
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            SubGroup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tbl_AddSubgroup" runat="server" align="center"
                                cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td width="20%"><span class="field-label">Parent Group Code</span></td>

                                    <td width="30%">
                                        <asp:DropDownList ID="DropDownListParentGroup" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td width="20%"><span class="field-label">Sub Group Code</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txtSubGroupcode" runat="server" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%"><span class="field-label">Sub Group Name</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txtSubgroupname" runat="server" MaxLength="100"></asp:TextBox></td>

                                    <td width="20%"> <span class="field-label">Type</span><strong><span style="font-size: 10pt; color: #ffffff; font-family: Arial">e</span></strong></td>

                                    <td width="30%"> 
                                        <asp:RadioButton ID="RbNormal" runat="server" Checked="True" GroupName="type" Text="Normal" CssClass="field-label" />
                                        <asp:RadioButton ID="rbBank" runat="server" GroupName="type" Text="Bank" CssClass="field-label" />
                                        <asp:RadioButton ID="rbCash" runat="server" GroupName="type" Text="Cash" CssClass="field-label" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

