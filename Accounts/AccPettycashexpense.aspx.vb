Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_AccPettycashexpense
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    'Version        Author          Date             Change
    '1.1            Swapna          19-jan-2011      To add sub expense types drop down and file upload

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Page.Master.FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnSave)
            If Page.IsPostBack = False Then
                Session.Remove("Expdatatable")
                txtHDocdate.Text = Date.Now.ToString("dd/MMM/yyyy")
                filldrpDown()
                'getExpSubTypes() 'v1.1
                gvSubExpenses.Columns(9).Visible = False
                For count As Integer = 1 To 10
                    SetInitialRow()
                Next
                CheckCashtype(ddExpensetype.SelectedValue)
                Session("datatable") = New DataTable
                Page.Title = OASISConstants.Gemstitle
                txtEmpNo.Attributes.Add("readonly", "readonly")
                txtRate.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtRate.Attributes.Add("onkeydown", "return CtrlDisable()")
                imgCalendar.Attributes.Add("onclick", "return  getDate('" & txtDDocdate.ClientID & "')")
                imgCalendarH.Attributes.Add("onclick", "return  getDate('" & txtHDocdate.ClientID & "')")
                ImgDepartment.Attributes.Add("onclick", "return  getFilter('" & txtDepartment.ClientID & "','" & h_DepartmentId.ClientID & "','DPT','" & Session("sBsuid") & "'); return false;")
                imgEmployee.Attributes.Add("onclick", "return  getFilter('" & txtEmpNo.ClientID & "','" & h_Emp_No.ClientID & "','EMPLOYEE','" & Session("sBsuid") & "','" & h_DepartmentId.ClientID & "'); return false;")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                clear_All()
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200356" And ViewState("MainMnu_code") <> "A200357" And ViewState("MainMnu_code") <> "U000024") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                If Request.QueryString("viewid") <> "" And ViewState("datamode") = "view" Then
                    'setViewData()   V1.1
                    viewId.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    clearGrid()
                    GetViewExpData()
                    ddExpensetype.Enabled = False
                    gvSubExpenses.Columns(9).Visible = True
                    'pnlSubExp.Enabled = False
                    'UtilityObj.beforeLoopingControls(Me.Page)
                    'Else
                    'ResetViewData()
                Else
                    getLoginDepartment()
                    pnlSubExp.Enabled = True
                    ddExpensetype.Enabled = True
                    GetLoggedInEmployee()
                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                gvSubExpenses.Attributes.Add("bordercolor", "#1b80b6")
            End If

            For Each gvr As GridViewRow In gvSubExpenses.Rows
                Dim txtRategv As TextBox = CType(gvr.FindControl("txtRategv"), TextBox)
                ClientScript.RegisterArrayDeclaration("txtboxRateIDs", String.Concat("'", txtRategv.ClientID, "'"))

                Dim txtInvoicegv As TextBox = CType(gvr.FindControl("txtInvoicegv"), TextBox)
                ClientScript.RegisterArrayDeclaration("txtboxDistIDs", String.Concat("'", txtInvoicegv.ClientID, "'"))

                Dim txtAmountgv As TextBox = CType(gvr.FindControl("txtAmountgv"), TextBox)
                ClientScript.RegisterArrayDeclaration("txtboxAmtIDs", String.Concat("'", txtAmountgv.ClientID, "'"))

                Dim txtDategv As TextBox = CType(gvr.FindControl("txtDategv"), TextBox)
                ClientScript.RegisterArrayDeclaration("txtDateFieldIDs", String.Concat("'", txtDategv.ClientID, "'"))
            Next

            Dim lblTotalAdd As Label = CType(gvSubExpenses.FooterRow.FindControl("lblTotalAdd"), Label)
            ClientScript.RegisterArrayDeclaration("lblTot", String.Concat("'", lblTotalAdd.ClientID, "'"))
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Public Sub GetLoggedInEmployee()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME  FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
         & " ON EM.EMP_ID=USERS_M.USR_EMP_ID" _
         & " WHERE USERS_M.USR_NAME ='" & Session("sUsr_name") & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME")
        h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
    End Sub
    Sub getLoginDepartment()
        Dim _table As DataTable
        Dim SqlQry As String = " SELECT DPT_DESCR,  DPT_ID  "
        SqlQry &= " FROM  EMPLOYEE_M  INNER JOIN  DEPARTMENT_M ON EMP_DPT_ID  =DPT_ID "
        SqlQry &= " WHERE EMP_ID='" & EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")) & "'"
        _table = MainObj.getRecords(SqlQry, "MainDBO")
        If _table.Rows.Count > 0 Then
            txtDepartment.Text = _table.Rows(0)(0).ToString()
            h_DepartmentId.Value = _table.Rows(0)(1).ToString()
        End If

    End Sub
    Private Sub getnextdocid() '  generate next document number
        If ViewState("datamode") = "add" Then
            Try
                'V1.1
                ' txtDocument.Text = AccountFunctions.GetNextDocId("PC", "", CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                txtDocument.Text = AccountFunctions.GetNextDocId("PC", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtDocument.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True

                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
            'DisableScriptManager()
        Catch ex As Exception

        End Try

    End Sub
    Public Sub DisableScriptManager()
        Dim smScriptManager As New ScriptManager
        smScriptManager = Page.Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
#Region "version1.1changes"
    Private Sub getExpSubTypes()     'V1.1
        Dim conn As String = ""
        conn = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet()
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetExpenseSubTypes")
        For Each dr As GridViewRow In gvSubExpenses.Rows
            Dim ddlsubType As DropDownList
            ddlsubType = DirectCast(gvSubExpenses.Rows(dr.RowIndex).Cells(3).FindControl("ddlsubType"), DropDownList)
            ddlsubType.DataSource = ds
            ddlsubType.DataTextField = ds.Tables(0).Columns("PXT_DESCRIPTION").ToString
            ddlsubType.DataValueField = ds.Tables(0).Columns("PXT_ID").ToString
            ddlsubType.DataBind()
            Dim txtSubType As TextBox
            txtSubType = DirectCast(gvSubExpenses.Rows(dr.RowIndex).Cells(3).FindControl("txtsubType"), TextBox)
            If txtSubType.Text <> "" Then
                ddlsubType.SelectedIndex = ddlsubType.Items.IndexOf(ddlsubType.Items.FindByText(txtSubType.Text))
                'txtSubType.Visible = False
                'ddlsubType.Visible = True
            End If
            If ViewState("datamode") = "edit" Then
                txtSubType.Visible = False
                ddlsubType.Visible = True
            End If
        Next

    End Sub
    Private Function SaveDocument(ByVal fuUpload As FileUpload, ByVal con As SqlConnection, ByVal tran As SqlTransaction, ByVal PCD_ID As String) As Boolean
        Try
            Dim filePath As String = fuUpload.PostedFile.FileName
            'Dim fuUpload As FileUpload
            'filePath = System.IO.Path.GetExtension(fuUpload.FileName)
            Dim filename As String = Path.GetFileName(filePath)
            Dim ext As String = Path.GetExtension(filename)
            Dim contenttype As String = String.Empty
            Select Case ext
                Case ".doc"
                    contenttype = "application/vnd.ms-word"
                    Exit Select
                Case ".docx"
                    contenttype = "application/vnd.ms-word"
                    Exit Select
                Case ".xls"
                    contenttype = "application/vnd.ms-excel"
                    Exit Select
                Case ".xlsx"
                    contenttype = "application/vnd.ms-excel"
                    Exit Select
                Case ".jpg"
                    contenttype = "image/jpg"
                    Exit Select
                Case ".png"
                    contenttype = "image/png"
                    Exit Select
                Case ".gif"
                    contenttype = "image/gif"
                    Exit Select
                Case ".pdf"
                    contenttype = "application/pdf"
                    Exit Select
            End Select

            If contenttype <> String.Empty Then

                Dim fs As Stream = fuUpload.PostedFile.InputStream
                Dim br As New BinaryReader(fs)
                Dim bytes As Byte() = br.ReadBytes(fs.Length)


                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", ext)
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bytes)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", Session("sBsuid"))
                Dim bUploadfile As Boolean = InsertUploadedFile(cmd, con, tran)
                If bUploadfile Then
                    lblError.Text = "File Uploaded Successfully"
                    Return True
                Else
                    lblError.Text = UtilityObj.getErrorMessage("991")
                    Return False
                End If

            Else
                lblError.Text = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function
    Public Function InsertUploadedFile(ByVal cmd As SqlCommand, ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Boolean
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        cmd.Transaction = tran
        Try
            'con.Open()
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Response.Write(ex.Message)
            Return False
        Finally
            'con.Close()
            'con.Dispose()
        End Try
    End Function
    Private Sub OpenFileFromDB(ByVal DocId As String)
        Try
            Dim conn As String = ""
            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("OpenDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            Dim myReader As SqlDataReader = cmd.ExecuteReader()
            If myReader.Read Then
                Response.ContentType = myReader("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(myReader("DOC_DOCUMENT"), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.AddHeader("content-disposition", "attachment;filename=" & myReader("DOC_DOCNO").ToString())
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
            myReader.Close()
        Catch ex As Exception
            'lblError.Text = ex.Message
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try

    End Sub
    Private Sub DeleteFileFromDB(ByVal DocId As String)
        Try
            Dim conn As String = ""
            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("DeleteDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            cmd.Parameters.Add("@retVal", SqlDbType.BigInt)
            cmd.Parameters("@retVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            objcon.Close()
            Dim ret As Integer = 0
            ret = cmd.Parameters("@retVal").Value
            If ret <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage("1000")
            End If
        Catch ex As Exception
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try

    End Sub

    Private Function SetInitialRow() As DataTable
        Dim dt As New DataTable()
        Dim dr As DataRow = Nothing
        dt.Columns.Add(New DataColumn("gvRowNumber", GetType(Integer)))
        dt.Columns.Add(New DataColumn("PCD_ID", GetType(Integer)))
        dt.Columns.Add(New DataColumn("P_Date", GetType(String)))
        dt.Columns.Add(New DataColumn("P_Reason", GetType(String)))
        dt.Columns.Add(New DataColumn("P_ExpType", GetType(DropDownList)))
        dt.Columns.Add(New DataColumn("P_Particulars", GetType(String)))
        dt.Columns.Add(New DataColumn("P_Invoice", GetType(String)))
        dt.Columns.Add(New DataColumn("P_Rate", GetType(String)))
        dt.Columns.Add(New DataColumn("P_Amount", GetType(String)))
        dt.Columns.Add(New DataColumn("P_UpFile", GetType(String)))
        Try
            If Not Session("Expdatatable") Is Nothing Then
                dt = Session("Expdatatable")
            End If
            dr = dt.NewRow()
            dr("gvRowNumber") = gvSubExpenses.Rows.Count + 1
            dr("PCD_ID") = 0
            dr("P_Date") = String.Empty
            dr("P_Reason") = String.Empty
            'dr("P_ExpType") = GetType(String)
            dr("P_Particulars") = String.Empty
            dr("P_Invoice") = String.Empty
            dr("P_Amount") = String.Empty
            dr("P_UpFile") = String.Empty
            dt.Rows.Add(dr)
            Session("Expdatatable") = dt
            gvSubExpenses.DataSource = dt
            gvSubExpenses.DataBind()
            getExpSubTypes()
            Dim strDate As String = ""
            For Each grow As GridViewRow In gvSubExpenses.Rows
                Dim txtRategv As TextBox = DirectCast(grow.Cells(5).FindControl("txtRategv"), TextBox)
                Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
                Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                Dim txtExpType As TextBox = DirectCast(grow.Cells(8).FindControl("txtDategv"), TextBox)

                txtExpType.Attributes.Add("readonly", "readonly")
                txtRategv.Attributes.Add("onblur", "txtblur()")
                txtAmountgv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtRategv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtAmountgv.Attributes.Add("onblur", "AddTotal()")
                If grow.RowIndex = 0 Then
                    txtDategv.Attributes.Add("onlostfocus", "setBlankDates()")
                    txtDategv.Attributes.Add(" onblur", "setBlankDates()")
                End If
                Dim imgDoc As Image = DirectCast(grow.Cells(9).FindControl("imgDoc"), Image)
                Dim imgDelete As Image = DirectCast(grow.Cells(9).FindControl("imgDelete"), Image)
                Dim btnImgDoc As HyperLink = DirectCast(grow.Cells(9).FindControl("btnImgDoc"), HyperLink)
                If imgDoc.AlternateText = "" Then
                    If imgDoc IsNot Nothing Then imgDoc.Visible = False
                    If imgDelete IsNot Nothing Then imgDelete.Visible = False
                    If btnImgDoc IsNot Nothing Then btnImgDoc.Visible = False
                End If

                Dim lblPCDID As Label
                lblPCDID = DirectCast(grow.Cells(10).FindControl("lblPCDID"), Label)
                lblPCDID.Text = 0
            Next
            Return dt
        Catch ex As Exception
            lblError.Text = UtilityObj.getErrorMessage("1000")
            Return dt
        End Try
    End Function

    Private Function CreateExpenseDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try

            Dim gvRowNumber As New DataColumn("gvRowNumber", System.Type.GetType("System.Int32"))
            Dim PCD_ID As New DataColumn("PCD_ID", System.Type.GetType("System.String"))
            Dim P_Date As New DataColumn("P_Date", System.Type.GetType("System.String"))
            Dim P_Reason As New DataColumn("P_Reason", System.Type.GetType("System.String"))
            Dim P_ExpType As New DataColumn("P_ExpType", System.Type.GetType("System.String"))
            Dim P_Particulars As New DataColumn("P_Particulars", System.Type.GetType("System.String"))
            Dim P_Invoice As New DataColumn("P_Invoice", System.Type.GetType("System.String"))
            Dim P_Amount As New DataColumn("P_Amount", System.Type.GetType("System.String"))
            Dim P_UpFile As New DataColumn("P_UpFile", System.Type.GetType("System.String")) 'V1.1
            'Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(gvRowNumber)
            dtDt.Columns.Add(PCD_ID)
            dtDt.Columns.Add(P_Date)
            dtDt.Columns.Add(P_Reason)
            dtDt.Columns.Add(P_ExpType)
            dtDt.Columns.Add(P_Particulars)
            dtDt.Columns.Add(P_Invoice)
            dtDt.Columns.Add(P_Amount)
            'dtDt.Columns.Add(cPAY_SCHEDULE) 'V1.1
            dtDt.Columns.Add(P_UpFile)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

#End Region

    Private Sub setViewData() 'setting controls on view/edit
        Dim sqlQry, selectId As String
        Dim _table As New DataTable

        selectId = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        sqlQry = " SELECT EMP_FNAME,PCH_DOCNO," _
                & " PCH_ID,PCH_TYP_ID,PCH_EMP_ID,PCH_STATUS," _
                & " PCH_bForward,PCH_REMARKS,PCD_SLNO as SLNO,PCD_DATE as [DATE]," _
                & " PCD_INVOICENO as DISTANCE,PCD_PURPOSE as PURPOSE,PCD_REASON as UNIT,PCD_RATE as RATE,PCD_AMOUNT as AMOUNT" _
                & " FROM PETTYEXP_H A,PETTYEXP_D B, vw_OSO_EMPLOYEE_M C" _
                & " WHERE A.PCH_ID = B.PCD_PCH_ID AND A.PCH_EMP_ID = C.EMP_ID" _
                & " AND A.PCH_ID = " & selectId & " AND PCH_FYEAR = " & Session("F_YEAR") & " " _
                & " AND PCH_bDELETED = 0 ORDER BY PCD_SLNO "
        _table = MainObj.ListRecords(sqlQry, "MainDB")
        gvJournal.DataSource = _table
        gvJournal.DataBind()
        If _table.Rows.Count > 0 Then
            CheckCashtype(_table.Rows(0)("PCH_TYP_ID").ToString())
            txtRemarks.Text = _table.Rows(0)("PCH_REMARKS").ToString()
            h_Emp_No.Value = _table.Rows(0)("PCH_EMP_ID").ToString()
            txtEmpNo.Text = _table.Rows(0)("EMP_FNAME").ToString()

            ddExpensetype.SelectedValue = _table.Rows(0)("PCH_TYP_ID").ToString()
            txtHDocdate.Text = Convert.ToDateTime(_table.Rows(0)("DATE")).ToString("dd-MMM-yyyy")
            txtDocument.Text = _table.Rows(0)("PCH_DOCNO").ToString()
            viewId.Value = selectId
            If _table.Rows(0)("PCH_bForward") = True Then
                chkForward.Checked = True
            Else
                chkForward.Checked = False
            End If
            gvJournal.FooterRow.Cells(4).Text = "Total"
            gvJournal.FooterRow.Cells(6).Text = Amount.ToString()
            gvJournal.FooterRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
            LockUnlock(False)
        End If
        Session("datatable") = _table
    End Sub
    Private Sub ResetViewData() 'resetting controls on view/edit


    End Sub
    'V1.1 
    Private Sub GetViewExpData()
        Try
            Dim selectId As String
            selectId = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Dim conn As String = ""
            conn = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim ds As New DataSet()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PCH_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = selectId

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetPETTYEXP_Details", pParms)

            txtDocument.Text = ds.Tables(0).Rows(0)("PCH_DOCNO").ToString
            txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_FNAME").ToString
            h_Emp_No.Value = ds.Tables(0).Rows(0)("PCH_EMP_ID").ToString
            h_DepartmentId.Value = ds.Tables(0).Rows(0)("PCH_DPT_ID").ToString
            txtDDocdate.Text = ds.Tables(0).Rows(0)("PCH_DATE").ToString
            ddExpensetype.SelectedValue = ds.Tables(0).Rows(0)("PCH_TYP_ID").ToString
            txtDepartment.Text = ds.Tables(0).Rows(0)("EMP_DEPT_DESCR").ToString
            txtRemarks.Text = ds.Tables(0).Rows(0)("PCH_REMARKS").ToString
            If ds.Tables(0).Rows(0)("PCH_bForward") = True Then
                chkForward.Checked = True
                btnPrint.Visible = True
            Else
                chkForward.Checked = False
                btnPrint.Visible = False
            End If
            LockUnlock(False)
            If ds.Tables(1).Rows.Count < 10 Then
                Dim ro As Integer = ds.Tables(1).Rows.Count
                For i As Integer = ro + 1 To 10
                    ds.Tables(1).Rows.Add()

                Next
            End If
            gvSubExpenses.DataSource = ds.Tables(1)
            gvSubExpenses.DataBind()

            ' gvSubExpenses.Columns(8).Visible = False
            Dim rowNum As Integer = 1
            For Each r1 As GridViewRow In gvSubExpenses.Rows
                r1.Cells(0).Text = rowNum
                rowNum += 1
                'If Not IsNumeric(r1.Cells(10).Text) Then
                '    r1.Cells(10).Text = 0
                'End If
                Dim lblPCDID As Label
                lblPCDID = DirectCast(r1.Cells(10).FindControl("lblPCDID"), Label)
                If Not IsNumeric(lblPCDID.Text) Then
                    lblPCDID.Text = 0
                End If
            Next
            CheckCashtype(ds.Tables(0).Rows(0)("PCH_TYP_ID").ToString())
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub

    Private Sub doInsert(ByVal Mode As String)
        'Mode 1 for edit and 0 for add
        Dim forWrd As String = "0"
        Dim Pslno, Pdate, Pinvoice, Ppurpose, Preason, Prate, Pamount, Pamt, rate, Status, dDate, Papamount As String
        Pslno = ""
        Pinvoice = ""
        Ppurpose = ""
        Preason = ""
        Prate = ""
        Pamount = ""
        Pdate = ""
        Status = "N"
        dDate = ""
        Papamount = ""

        If chkForward.Checked Then
            forWrd = "1"
            Status = "F"
        End If

        'New gridview code
        Dim AmountTotal As Double = 0
        For Each grow As GridViewRow In gvSubExpenses.Rows
            Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
            Dim txtReasongv As TextBox = DirectCast(grow.Cells(2).FindControl("txtReasongv"), TextBox)
            Dim txtParticularsgv As TextBox = DirectCast(grow.Cells(3).FindControl("txtParticularsgv"), TextBox)
            Dim ddlsubType1 As DropDownList = DirectCast(grow.Cells(3).FindControl("ddlsubType1"), DropDownList)
            'Dim box3 As TextBox = DirectCast(grow.Cells(4).FindControl("txtParticularsgv"), TextBox)txtsubType
            Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
            Dim txtRategv As TextBox = DirectCast(grow.Cells(6).FindControl("txtRategv"), TextBox)
            Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
            'Dim box7 As TextBox = DirectCast(grow.Cells(7).FindControl("txtUploadgv"), TextBox)

            If (txtDategv.Text <> "" And txtDategv.Text <> "" And txtInvoicegv.Text <> "" And txtReasongv.Text <> "") Then
                rate = txtRategv.Text.ToString().Trim()
                Pamt = Convert.ToDouble(txtAmountgv.Text.ToString().Trim()).ToString("##0.00")
                If ddExpensetype.SelectedValue.Equals("TR") Then
                    Pamt = (Convert.ToDouble(txtRategv.Text) * Convert.ToDouble(txtInvoicegv.Text)).ToString("##0.00")

                Else
                    Pamt = Convert.ToDouble(txtAmountgv.Text.ToString().Trim()).ToString("##0.00")
                End If
                AmountTotal = AmountTotal + Pamt

                dDate = Convert.ToDateTime(txtDategv.Text.ToString().Trim()).ToString("dd-MMM-yyyy")
                If (rate.Equals("")) Then
                    rate = "0"
                End If
                rate = Convert.ToDouble(rate).ToString("##0.00")
                If Pslno.Equals("") Then
                    Pslno = grow.Cells(0).Text.ToString().Trim()
                    Pdate = dDate
                    Preason = txtReasongv.Text.ToString().Trim().Replace("#", "")
                    Ppurpose = txtParticularsgv.Text.ToString().Trim().Replace("#", "")
                    Pinvoice = txtInvoicegv.Text.ToString().Trim().Replace("#", "")
                    Prate = rate
                    Pamount = Pamt
                    Papamount = "0"

                Else
                    Pslno += "#" & grow.Cells(0).Text.ToString().Trim()
                    Pdate += "#" & dDate
                    Preason += "#" & txtReasongv.Text.ToString().Trim().Replace("#", "")
                    Ppurpose += "#" & txtParticularsgv.Text.ToString().Trim().Replace("#", "")
                    Pinvoice += "#" & txtInvoicegv.Text.ToString().Trim().Replace("#", "")
                    Prate += "#" & rate
                    Pamount += "#" & Pamt
                    Papamount += "#" & "0"
                End If
            End If
        Next

        Dim _parameter As String(,) = New String(26, 1) {}              'V1.1 parameter size increased to add sub expense type
        _parameter(0, 0) = "@PCH_ID"
        _parameter(0, 1) = viewId.Value.ToString()
        _parameter(1, 0) = "@PCH_BSU_ID"
        _parameter(1, 1) = Session("sBsuid").ToString()
        _parameter(2, 0) = "@PCH_FYEAR"
        _parameter(2, 1) = Session("F_YEAR").ToString()
        _parameter(3, 0) = "@PCH_TYP_ID"
        _parameter(3, 1) = ddExpensetype.SelectedValue.ToString()
        _parameter(4, 0) = "@PCH_EMP_ID"
        _parameter(4, 1) = h_Emp_No.Value.ToString()
        _parameter(5, 0) = "@PCH_DATE"
        _parameter(5, 1) = Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(6, 0) = "@PCH_USER_ID"
        _parameter(6, 1) = Session("sUsr_name").ToString()
        _parameter(7, 0) = "@PCH_AMOUNT"
        '_parameter(7, 1) = Convert.ToDouble(gvJournal.FooterRow.Cells(6).Text.ToString()).ToString("##.")
        _parameter(7, 1) = AmountTotal.ToString("##.")
        _parameter(8, 0) = "@PCH_STATUS"
        _parameter(8, 1) = Status
        _parameter(9, 0) = "@PCH_bForward"
        _parameter(9, 1) = forWrd
        _parameter(10, 0) = "@PCH_REMARKS"
        _parameter(10, 1) = txtRemarks.Text.Trim().ToString()
        _parameter(11, 0) = "@MODE"
        _parameter(11, 1) = Mode
        _parameter(12, 0) = "@PCD_SLNO"
        _parameter(12, 1) = Pslno
        _parameter(13, 0) = "@PCD_DATE"
        _parameter(13, 1) = Pdate
        _parameter(14, 0) = "@PCD_INVOICENO"
        _parameter(14, 1) = Pinvoice
        _parameter(15, 0) = "@PCD_PURPOSE"
        _parameter(15, 1) = Ppurpose
        _parameter(16, 0) = "@PCD_REASON"
        _parameter(16, 1) = Preason
        _parameter(17, 0) = "@PCD_RATE"
        _parameter(17, 1) = Prate
        _parameter(18, 0) = "@PCD_AMOUNT"
        _parameter(18, 1) = Pamount
        _parameter(19, 0) = "@AUD_WINUSER"
        _parameter(19, 1) = Page.User.Identity.Name.ToString()
        _parameter(20, 0) = "@Aud_form"
        _parameter(20, 1) = CObj(Page.Master).MenuName.ToString()
        _parameter(21, 0) = "@Aud_user"
        _parameter(21, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(22, 0) = "@Aud_module"
        _parameter(22, 1) = HttpContext.Current.Session("sModule")
        _parameter(23, 0) = "@PCD_APV_AMOUNT"
        _parameter(23, 1) = Papamount
        _parameter(24, 0) = "@PCH_DPT_ID"
        _parameter(24, 1) = h_DepartmentId.Value.ToString()

        'V1.1 parameter addition
        _parameter(25, 0) = "@PCD_PXT_ID"
        _parameter(25, 1) = ddlsubType1.SelectedValue


        MainObj.doExcutive("SavePETTYEXP_H", _parameter, "mainDB", "@v_ReturnMsg")
        lblError.Text = MainObj.MESSAGE
        If MainObj.SPRETVALUE.Equals(0) Then
            clear_All()
        End If


    End Sub



    Private Sub LockUnlock(ByVal locked As Boolean)
        txtDistance.Enabled = locked
        txtEmpNo.Enabled = locked
        txtDDocdate.Enabled = locked
        txtDDocdate.Enabled = locked
        txtPurpose.Enabled = locked
        txtRate.Enabled = locked
        txtRemarks.Enabled = locked
        gvJournal.Enabled = locked
        btnInsert.Enabled = locked
        btnClear.Enabled = locked
        chkForward.Enabled = locked
        ddExpensetype.Enabled = locked
        imgEmployee.Enabled = locked
        txtDepartment.Enabled = locked
        ImgDepartment.Enabled = locked



    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtRemarks.Text.Trim = "" Then
            lblError.Text = "Remarks Cannot be empty..!"
            Exit Sub
        ElseIf h_DepartmentId.Value.Equals("") Then
            lblError.Text = "Invalid Department..!"
            Exit Sub
        End If
        If h_Emp_No.Value.Equals("") Then
            lblError.Text = "Please Select Employee..!"
            Exit Sub
        End If
        Try
            If ViewState("datamode") = "edit" Then
                'ela_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                'doInsert("1")
            Else
                'doInsert("0")
            End If
            ' InsertDocuments()
            SavePettyCashExpenses()
            gvSubExpenses.Columns(9).Visible = False
            LockUnlock(True)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            If ViewState("IsForwarded") <> "True" Then
                btnPrint.Visible = False
            Else
                btnPrint.Visible = True
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub InsertDocuments()
        For Each grow As GridViewRow In gvSubExpenses.Rows
            Dim flpload As FileUpload = DirectCast(grow.Cells(8).FindControl("flUpload"), FileUpload)
            If flpload.HasFile Then
                ' SaveDocument(flpload)
            End If
        Next
    End Sub
    Private Function SavePettyCashExpenses() As Integer
        Dim errorno As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection

            Dim trans As SqlTransaction = conn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted, "SampleTransaction")
            Try
                If errorno = 0 Then errorno = SAVEToPETTYEXP_H(conn, trans)
                If errorno = 0 Then errorno = SAVEToPETTYEXP_D(conn, trans)
                If errorno = 0 Then
                    trans.Commit()
                    If chkForward.Checked = True Then
                        ViewState("IsForwarded") = "True"
                        SendEmailToNextApprover()
                        ViewState("datamode") = "add"
                        btnPrint.Visible = True
                    Else
                        ViewState("IsForwarded") = "False"
                        btnPrint.Visible = False
                    End If
                    'V1.1
                    ' lblError.Text = "Employee Transfer Completed successfully"
                    lblError.Text = getErrorMessage("0")
                    clear_All()
                    clearGrid()
                    'ViewState("datamode") = "view"
                Else
                    trans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(errorno)

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
                Return 1000
                'V1.1

                'lblError.Text = "Error occured while Trnasfering Employee"
            End Try
        End Using
    End Function
    Protected Sub SendEmailToNextApprover()
        Dim Mailstatus As String = ""
        Try

            Dim strStatusDesc = "Approved"
            Dim conn As SqlConnection = ConnectionManger.GetOASISFinConnection

            Dim ds As New DataSet()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@USER_ID", SqlDbType.VarChar, 100)
            pParms(1).Value = Session("sUsr_name").ToString


            pParms(2) = New SqlClient.SqlParameter("@PCH_ID", SqlDbType.Int)
            pParms(2).Value = 0

            Dim strStatus As String = "A"

            pParms(3) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 10)
            pParms(3).Value = strStatus

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getPettyExpMailDetails", pParms)
            'lblError.Text = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            Dim ToEmailId As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                ToEmailId = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            End If
            If ToEmailId = "" Then
                ToEmailId = "swapna.tv@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim MainText As String = ""
            Dim Subject As String = ""
            If ds.Tables(1).Rows.Count > 0 Then

                Subject = "Petty Cash Voucher" + ds.Tables(1).Rows(0).Item("PCH_DOCNO").ToString()

                If strStatus = "A" Then
                    MainText = "Kindly note that a request is pending at your desk for approval."
                End If
                sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Hi,</td></tr>")
                sb.Append("<tr><td ><br /><br /></td></tr>")
                sb.Append("<tr><td><b>Ref.NO:</b> " & ds.Tables(1).Rows(0).Item("PCH_DOCNO").ToString() & "</td></tr>")
                sb.Append("<tr><td><b>Employee NO:</b> " & ds.Tables(1).Rows(0).Item("EMPNO").ToString() & ",<b>Name:</b>" & ds.Tables(1).Rows(0).Item("EMPName").ToString() & "</td></tr>")
                sb.Append("<tr><td><b>Business Unit:</b> " & ds.Tables(1).Rows(0).Item("BSU_NAME").ToString() & "</td></tr>")
                sb.Append("<tr><td><b>Regarding:</b> " & ds.Tables(1).Rows(0).Item("PCH_REMARKS").ToString() & "</td></tr>")
                sb.Append("<tr><td ><br /><br /></td></tr>")

                sb.Append("<tr><td>" & MainText & "</td></tr>")
                If strStatus = "A" Then
                    Dim LoginURL As String
                    LoginURL = WebConfigurationManager.AppSettings.Item("webvirtualURL") & "/login.aspx?ss=1"
                    sb.Append("<tr><td>To view and approve the details, please click on the following link: <a href='" & LoginURL & "'><br />Employee Self Service</a></td></tr>")
                End If
                sb.Append("<tr><td></td></tr>")
                'sb.Append("<tr><td>Please be informed that the above request is " & strStatusDesc & ".</td></tr>")
                sb.Append("<tr><td><b>Remarks:</b> " & ds.Tables(1).Rows(0).Item("UsrRemark").ToString() & "</td></tr>")

                sb.Append("<tr><td ><br /><br />Regards,</td></tr>")
                sb.Append("<tr><td><b>Team OASIS.</b></td></tr>")
                sb.Append("<tr><td></td></tr>")
                sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                sb.Append("<tr></tr><tr></tr>")
                sb.Append("</table>")


            End If
            Dim ds2 As New DataSet
            ds2 = GetCommunicationSettings()

            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""


            If ds2.Tables(0).Rows.Count > 0 Then
                username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            End If
            Try
                'ToEmailId = "shakeel.shakkeer@gemseducation.com"
                Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString, username, password, host, port, 0, False)
            Catch ex As Exception
            Finally
                Mainclass.SaveEmailSendStatus(Session("sBsuid").ToString, "Expense", "", ToEmailId, Subject, sb.ToString, IIf(LCase(Mailstatus).StartsWith("error"), 0, 1), Mailstatus)
            End Try

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)

        End Try
    End Sub

    Public Function GetCommunicationSettings() As DataSet


        Dim ds As DataSet

        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)

        Return ds

    End Function

    Protected Function SAVEToPETTYEXP_H(ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Integer
        Dim Pamt As Double = 0
        Dim iReturnvalue As Integer
        Try
            'Dim cmd As New SqlCommand
            'cmd.Connection = con
            Dim cmd As New SqlCommand("SAVEToPettyExp_H", con, tran)
            cmd.CommandType = CommandType.StoredProcedure
            Dim status As String
            Dim mode As Integer
            Dim forWrd As String = "0"
            status = "N"
            Dim countRow As Integer = 0
            If chkForward.Checked Then
                forWrd = "1"
                status = "F"
            End If
            If ViewState("datamode") = "add" Then
                mode = 0
            ElseIf ViewState("datamode") = "edit" Then
                mode = 1
            Else
                mode = 2
            End If
            If mode <> 2 Then

                For Each grow As GridViewRow In gvSubExpenses.Rows
                    Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                    Dim txtReasongv As TextBox = DirectCast(grow.Cells(2).FindControl("txtReasongv"), TextBox)
                    Dim txtParticularsgv As TextBox = DirectCast(grow.Cells(3).FindControl("txtParticularsgv"), TextBox)
                    Dim ddlsubType1 As DropDownList = DirectCast(grow.Cells(3).FindControl("ddlsubType1"), DropDownList)
                    'Dim box3 As TextBox = DirectCast(grow.Cells(4).FindControl("txtParticularsgv"), TextBox)txtsubType
                    Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
                    Dim txtRategv As TextBox = DirectCast(grow.Cells(6).FindControl("txtRategv"), TextBox)
                    Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
                    'Dim box7 As TextBox = DirectCast(grow.Cells(7).FindControl("txtUploadgv"), TextBox)

                    If (txtDategv.Text <> "" And txtDategv.Text <> "" And txtInvoicegv.Text <> "" And txtReasongv.Text <> "") Then
                        countRow += 1
                        If ddExpensetype.SelectedValue.Equals("TR") Then
                            Pamt += (Convert.ToDouble(txtRategv.Text) * Convert.ToDouble(txtInvoicegv.Text)).ToString("##0.00")

                        Else
                            Pamt += Convert.ToDouble(txtAmountgv.Text.ToString().Trim()).ToString("##0.00")
                        End If
                    End If
                Next
            Else
                Pamt = 0
            End If

            If (countRow > 0 Or mode = 2) Then

                cmd.Parameters.AddWithValue("@PCH_ID", viewId.Value.ToString())
                cmd.Parameters.AddWithValue("@PCH_BSU_ID", Session("sBsuid").ToString())
                cmd.Parameters.AddWithValue("@PCH_FYEAR", Session("F_YEAR").ToString())
                cmd.Parameters.AddWithValue("@PCH_TYP_ID", ddExpensetype.SelectedValue)
                cmd.Parameters.AddWithValue("@PCH_EMP_ID", h_Emp_No.Value.ToString())
                cmd.Parameters.AddWithValue("@PCH_DPT_ID", h_DepartmentId.Value.ToString())
                cmd.Parameters.AddWithValue("@PCH_DATE", Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy"))
                cmd.Parameters.AddWithValue("@PCH_USER_ID", Session("sUsr_name").ToString())
                cmd.Parameters.AddWithValue("@PCH_AMOUNT", Pamt)
                cmd.Parameters.AddWithValue("@PCH_DOCNO", txtDocument.Text)
                cmd.Parameters.AddWithValue("@PCH_STATUS", status)
                cmd.Parameters.AddWithValue("@PCH_bForward", forWrd)
                cmd.Parameters.AddWithValue("@PCH_REMARKS", txtRemarks.Text.Trim().ToString())
                cmd.Parameters.AddWithValue("@MODE ", mode)
                cmd.Parameters.AddWithValue("@Aud_user", HttpContext.Current.Session("sUsr_name").ToString())
                cmd.Parameters.AddWithValue("@Aud_module", HttpContext.Current.Session("sModule"))
                cmd.Parameters.AddWithValue("@Aud_form", CObj(Page.Master).MenuName.ToString())
                cmd.Parameters.AddWithValue("@AUD_WINUSER", Page.User.Identity.Name.ToString())
                cmd.Parameters.Add("@v_ReturnMsg", SqlDbType.VarChar, 100)
                cmd.Parameters("@v_ReturnMsg").Value = ""
                cmd.Parameters("@v_ReturnMsg").Direction = ParameterDirection.Output

                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue

                cmd.ExecuteNonQuery()

                iReturnvalue = cmd.Parameters("@ReturnValue").Value.ToString()

                lblError.Text = cmd.Parameters("@v_ReturnMsg").Value.ToString()

                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                Else
                    Return 0
                End If
            Else
                lblError.Text = "No details available to save"
                Return 990
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function

    Protected Function SAVEToPETTYEXP_D(ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Integer
        Dim Pamt As Double = 0
        Dim iReturnvalue As Integer = 0
        Dim mode As Integer
        If ViewState("datamode") = "add" Then
            mode = 0
        ElseIf ViewState("datamode") = "edit" Then
            mode = 1
        Else
            Return 0
        End If
        Try
            Dim cmd As New SqlCommand("SaveToPETTYEXP_D", con, tran)
            cmd.CommandType = CommandType.StoredProcedure
            For Each grow As GridViewRow In gvSubExpenses.Rows
                Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                Dim txtReasongv As TextBox = DirectCast(grow.Cells(2).FindControl("txtReasongv"), TextBox)
                Dim txtParticularsgv As TextBox = DirectCast(grow.Cells(3).FindControl("txtParticularsgv"), TextBox)
                Dim ddlsubType As DropDownList = DirectCast(grow.Cells(7).FindControl("ddlsubType"), DropDownList)
                'Dim box3 As TextBox = DirectCast(grow.Cells(4).FindControl("txtParticularsgv"), TextBox)txtsubType
                'fOR INVOICE AND DISTANCE:-
                Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
                Dim txtRategv As TextBox = DirectCast(grow.Cells(6).FindControl("txtRategv"), TextBox)
                Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
                Dim filupload As FileUpload = DirectCast(grow.Cells(8).FindControl("flUpload"), FileUpload)
                Dim lblPCDID As Label
                lblPCDID = DirectCast(grow.Cells(10).FindControl("lblPCDID"), Label)
                If Not IsNumeric(lblPCDID.Text) Then
                    lblPCDID.Text = 0
                End If
                'Dim box7 As TextBox = DirectCast(grow.Cells(7).FindControl("txtUploadgv"), TextBox)
                If (txtRategv.Text = "") Then
                    txtRategv.Text = "0"
                End If
                txtRategv.Text = Convert.ToDouble(txtRategv.Text).ToString("0.00")
                If (ddExpensetype.SelectedValue = "TR") Then
                    If txtAmountgv.Text = "" Then
                        txtAmountgv.Text = "0"
                    End If
                End If
                If (txtDategv.Text <> "" And txtRategv.Text <> "" And txtInvoicegv.Text <> "" And txtAmountgv.Text <> "") Then

                    If ddExpensetype.SelectedValue.Equals("TR") Then
                        Pamt = (Convert.ToDouble(txtRategv.Text) * Convert.ToDouble(txtInvoicegv.Text)).ToString("0.00")

                    Else
                        Pamt = Convert.ToDouble(txtAmountgv.Text.ToString().Trim()).ToString("0.00")
                    End If

                    cmd.Parameters.Clear()


                    cmd.Parameters.AddWithValue("@PCD_PCH_ID", viewId.Value.ToString())
                    cmd.Parameters.AddWithValue("@PCD_SLNO", grow.Cells(0).Text.ToString().Trim())
                    cmd.Parameters.AddWithValue("@PCD_ID", lblPCDID.Text)
                    cmd.Parameters.AddWithValue("@PCd_DATE", Convert.ToDateTime(txtDategv.Text()).ToString("dd-MMM-yyyy"))
                    cmd.Parameters.AddWithValue("@PCD_INVOICENO", txtInvoicegv.Text.ToString().Trim())
                    cmd.Parameters.AddWithValue("@PCD_PURPOSE", txtParticularsgv.Text.ToString().Trim())
                    cmd.Parameters.AddWithValue("@PCD_REASON", txtReasongv.Text.ToString().Trim())
                    cmd.Parameters.AddWithValue("@PCD_RATE", txtRategv.Text.ToString().Trim())
                    cmd.Parameters.AddWithValue("@PCD_AMOUNT", Pamt)
                    cmd.Parameters.AddWithValue("@PCD_APV_AMOUNT", Pamt)
                    cmd.Parameters.AddWithValue("@PCD_PXT_ID", ddlsubType.SelectedValue)
                    cmd.Parameters.AddWithValue("@MODE ", mode)
                    cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add("@RetPCD_ID", SqlDbType.BigInt)
                    cmd.Parameters("@RetPCD_ID").Direction = ParameterDirection.Output


                    cmd.ExecuteNonQuery()
                    Dim iPCD_ID As String
                    iReturnvalue = cmd.Parameters("@ReturnValue").Value.ToString()
                    iPCD_ID = cmd.Parameters("@RetPCD_ID").Value.ToString()
                    If iReturnvalue <> 0 Then
                        Return 1000
                    Else
                        'Return 0
                        If filupload.HasFile Then
                            If Not (SaveDocument(filupload, con, tran, Val(iPCD_ID))) Then

                                Return 991
                            End If
                        End If

                    End If
                End If
            Next
            Return 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            'setViewData()
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ViewState("datamode") = "add"
        clear_All()
        clearGrid()
        ddExpensetype.Enabled = True
        gvSubExpenses.Columns(9).Visible = False
        gvSubExpenses.Columns(8).Visible = True
        LockUnlock(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Sub clear_All()
        h_Emp_No.Value = ""
        txtEmpNo.Text = ""
        ddExpensetype.Enabled = True
        txtRemarks.Text = ""
        txtDepartment.Text = ""
        h_DepartmentId.Value = ""
        gvJournal.DataSource = Nothing
        gvJournal.DataBind()
        Dim _table As New DataTable
        Session("datatable") = _table
        clearDetails()
        getnextdocid()
        txtHDocdate.Text = Date.Now.ToString("dd/MMM/yyyy")
        'txtRemarks.Text = ddExpensetype.SelectedItem.Text.ToString() & "..Month of.." & Date.Now.ToString("MMMM")
        txtRemarks.Text = ddExpensetype.SelectedItem.Text.ToString() & "..Month of.." & Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("MMMM")

    End Sub
    Sub clearDetails()
        txtDistance.Text = ""
        txtPurpose.Text = ""
        txtRate.Text = ""
        txtUnit.Text = ""
        txtRate.Text = ""

    End Sub
    Sub clearGrid()
        'V1.1
        For Each grow As GridViewRow In gvSubExpenses.Rows
            Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
            txtDategv.Text = ""
            Dim txtReasongv As TextBox = DirectCast(grow.Cells(2).FindControl("txtReasongv"), TextBox)
            txtReasongv.Text = ""
            Dim txtParticularsgv As TextBox = DirectCast(grow.Cells(3).FindControl("txtParticularsgv"), TextBox)
            txtParticularsgv.Text = ""
            Dim ddlsubType As DropDownList = DirectCast(grow.Cells(7).FindControl("ddlsubType"), DropDownList)
            ddlsubType.Visible = True
            ddlsubType.SelectedIndex = -1
            'fOR INVOICE AND DISTANCE:-
            Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
            txtInvoicegv.Text = ""
            Dim txtRategv As TextBox = DirectCast(grow.Cells(6).FindControl("txtRategv"), TextBox)
            txtRategv.Text = ""
            Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
            txtAmountgv.Text = ""
            Dim txtExpTypegv As TextBox = DirectCast(grow.Cells(7).FindControl("txtsubType"), TextBox)
            txtExpTypegv.Text = ""
            txtExpTypegv.Visible = False
            Dim lblPCDID As Label
            lblPCDID = DirectCast(grow.Cells(10).FindControl("lblPCDID"), Label)
            lblPCDID.Text = 0
            Dim imgDoc As Image = DirectCast(grow.Cells(9).FindControl("imgDoc"), Image)
            Dim imgDelete As Image = DirectCast(grow.Cells(9).FindControl("imgDelete"), Image)
            Dim btnImgDoc As HyperLink = DirectCast(grow.Cells(9).FindControl("btnImgDoc"), HyperLink)
            If imgDoc IsNot Nothing Then imgDoc.Visible = False
            If imgDelete IsNot Nothing Then imgDelete.Visible = False
            If btnImgDoc IsNot Nothing Then btnImgDoc.Visible = False
        Next
        Dim rowF As GridViewRow = gvSubExpenses.FooterRow
        If (rowF.RowType = DataControlRowType.Footer) Then

            Dim lblTot As Label = DirectCast(rowF.FindControl("lblTotalAdd"), Label)
            lblTot.Text = "0.00"
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If (chkForward.Checked) Then
            lblError.Text = "You Can Edit Only Rejected Documents And Documents Which Are Not Forwaded."
            btnPrint.Visible = True
            Exit Sub
        Else
            LockUnlock(True)
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            getExpSubTypes()
            CheckCashtype(ddExpensetype.SelectedValue)
            pnlSubExp.Enabled = True
            btnPrint.Visible = False

            For Each grow As GridViewRow In gvSubExpenses.Rows
                Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                If grow.RowIndex > 0 And txtDategv.Text = "" Then
                    Dim strFirstDate = DirectCast(gvSubExpenses.Rows(0).Cells(1).FindControl("txtDategv"), TextBox).Text
                    If strFirstDate = "" Then
                        strFirstDate = Today.Date
                    End If

                    txtDategv.Text = Convert.ToDateTime(strFirstDate).ToString("dd/MMM/yyyy")
                End If
            Next

        End If
    End Sub
    'V1.1 chnages to change grid header text based on expense type selection
    Private Sub CheckCashtype(ByVal CashType As String)
        Dim dblTotAmt As Double = 0
        Dim row As GridViewRow = gvSubExpenses.HeaderRow
        If CashType.Equals("CH") Then
            labUnite.Text = "Reason" & "<font color='red'>*</font>"
            labPurpose.Text = "Particulars"
            labDistance.Text = "Invoice"
            labRate.Text = "Amount"
            gvJournal.Columns(5).Visible = False
            gvJournal.Columns(2).HeaderText = "Reason"
            gvJournal.Columns(3).HeaderText = "Particulars"
            gvJournal.Columns(4).HeaderText = "Invoice"
            labHead.Text = "Petty Cash Voucher"
            txtDistance.Attributes.Remove("onkeypress")

            gvSubExpenses.Columns(5).Visible = False
            If (row.RowType = DataControlRowType.Header) Then
                Dim cell2 As TableCell = row.Cells(2)
                Dim cell3 As TableCell = row.Cells(3)
                Dim cell4 As TableCell = row.Cells(4)
                Dim cell6 As TableCell = row.Cells(6)

                cell2.Text = "Reason<font color='red'>*</font>"
                cell3.Text = "Particulars"
                cell4.Text = "Invoice<font color='red'>*</font>"
                cell6.Text = "Amount<font color='red'>*</font>"

            End If

            'To make invoice non-numeric
            For Each grow As GridViewRow In gvSubExpenses.Rows
                Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
                txtInvoicegv.Attributes.Remove("onkeypress")
                Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
                Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                txtAmountgv.Attributes.Remove("readonly")
                txtAmountgv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtAmountgv.Attributes.Add("onblur", "AddTotal()")
                txtAmountgv.BackColor = Drawing.Color.White

                If grow.RowIndex = 0 Then
                    txtDategv.Attributes.Add("onlostfocus", "setBlankDates()")
                    txtDategv.Attributes.Add(" onblur", "setBlankDates()")
                End If

                If ViewState("datamode") = "view" Then
                    If IsNumeric(txtAmountgv.Text) Then
                        dblTotAmt += Convert.ToDouble(txtAmountgv.Text.ToString.Trim())
                    End If

                    If (grow.RowType = DataControlRowType.Footer) Then
                        Dim cell6 As TableCell = row.Cells(6)
                        Dim lblTot As Label = DirectCast(cell6.FindControl("lblTotalAdd"), Label)
                        lblTot.Text = dblTotAmt.ToString("0.00")

                    End If
                End If
            Next

        ElseIf CashType.Equals("TR") Then
            labUnite.Text = "Unit"
            labPurpose.Text = "Purpose"
            labDistance.Text = "Distance"
            labRate.Text = "Rate"
            gvJournal.Columns(5).Visible = True
            gvJournal.Columns(2).HeaderText = "Unit"
            gvJournal.Columns(3).HeaderText = "Purpose"
            gvJournal.Columns(4).HeaderText = "Distance<font color='red'>*</font>"
            txtDistance.TextMode = TextBoxMode.SingleLine
            txtDistance.Attributes.Add("onkeypress", "return  Numeric_Only()")
            txtDistance.Attributes.Add("onkeydown", "return CtrlDisable()")
            labHead.Text = "Traval Cash Voucher"
            'If ViewState("datamode") = "add" Then
            '    gvSubExpenses.Columns(6).Visible = False
            'End If
            gvSubExpenses.Columns(5).Visible = True

            If (row.RowType = DataControlRowType.Header) Then
                Dim cell2 As TableCell = row.Cells(2)
                Dim cell3 As TableCell = row.Cells(3)
                Dim cell4 As TableCell = row.Cells(4)
                Dim cell6 As TableCell = row.Cells(6)

                cell2.Text = "Unit"
                cell3.Text = "Purpose"
                cell4.Text = "Distance<font color='red'>*</font>"
                cell6.Text = "Amount"
            End If
            'To make distance numeric only
            For Each grow As GridViewRow In gvSubExpenses.Rows
                Dim txtInvoicegv As TextBox = DirectCast(grow.Cells(4).FindControl("txtInvoicegv"), TextBox)
                txtInvoicegv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                'amount cannot be entered
                Dim txtAmountgv As TextBox = DirectCast(grow.Cells(6).FindControl("txtAmountgv"), TextBox)
                txtAmountgv.Attributes.Add("readonly", "readonly")

                Dim txtRategv As TextBox = DirectCast(grow.Cells(5).FindControl("txtRategv"), TextBox)

                Dim txtDategv As TextBox = DirectCast(grow.Cells(1).FindControl("txtDategv"), TextBox)
                'txtRategv.Attributes.Add("onblur", "funcsum();")
                txtAmountgv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtRategv.Attributes.Add("onkeypress", "return  Numeric_Only()")
                txtRategv.Attributes.Add("onblur", "txtblur()")
                txtAmountgv.Attributes.Remove("onblur")
                txtAmountgv.BackColor = Drawing.Color.LightGray  'To show it is readonly

                Dim ddlsubType As DropDownList
                ddlsubType = DirectCast(grow.Cells(3).FindControl("ddlsubType"), DropDownList)
                Dim txtSubType As TextBox
                txtSubType = DirectCast(grow.Cells(3).FindControl("txtsubType"), TextBox)
                If txtSubType.Text = "" Then
                    ddlsubType.SelectedIndex = 1
                End If


                If grow.RowIndex = 0 Then
                    txtDategv.Attributes.Add("onlostfocus", "setBlankDates()")
                    txtDategv.Attributes.Add(" onblur", "setBlankDates()")
                End If

                If ViewState("datamode") = "view" Then
                    If (IsNumeric(txtRategv.Text) And IsNumeric(txtInvoicegv.Text)) Then
                        dblTotAmt += Convert.ToDouble(txtRategv.Text.ToString.Trim()) * Convert.ToDouble(txtInvoicegv.Text.ToString.Trim())
                    End If

                End If
            Next
        End If
        If ViewState("datamode") = "view" Then
            Dim rowF As GridViewRow = gvSubExpenses.FooterRow
            If (rowF.RowType = DataControlRowType.Footer) Then

                Dim lblTot As Label = DirectCast(rowF.FindControl("lblTotalAdd"), Label)
                lblTot.Text = dblTotAmt.ToString("0.00")
            End If
            gvSubExpenses.Columns(8).Visible = False
        Else
            gvSubExpenses.Columns(8).Visible = True
        End If

        'If (ViewState("datamode") = "add") Then
        '    gvSubExpenses.Columns(9).Visible = False
        'End If
        If ViewState("datamode") = "add" Then txtRemarks.Text = ddExpensetype.SelectedItem.Text.ToString() & "..Month of.." & Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("MMMM")
        'clearDetails()
        'clearGrid() 'V1.1

    End Sub
    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        Dim _table As New DataTable
        _table = Session("datatable")


        If (_table.Columns.Count.Equals(0)) Then
            _table.Columns.Add("SLNO", GetType(String))
            _table.Columns.Add("DATE", GetType(String))
            _table.Columns.Add("UNIT", GetType(String))
            _table.Columns.Add("PURPOSE", GetType(String))
            _table.Columns.Add("DISTANCE", GetType(String))
            _table.Columns.Add("RATE", GetType(String))
            _table.Columns.Add("AMOUNT", GetType(String))
            ddExpensetype.Enabled = False
        End If
        Dim dtRow As DataRow
        Dim Amnt As Double = Convert.ToDouble(txtRate.Text.ToString())
        dtRow = _table.NewRow()
        _table.Rows.Add(dtRow)
        If Amnt.Equals(0) Then
            Amnt = 0
        End If
        _table.Rows(_table.Rows.Count - 1)("SLNO") = _table.Rows.Count.ToString()
        _table.Rows(_table.Rows.Count - 1)("DATE") = txtDDocdate.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("UNIT") = txtUnit.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("PURPOSE") = txtPurpose.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("DISTANCE") = txtDistance.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("RATE") = Amnt.ToString("#,###0.000")
        _table.Rows(_table.Rows.Count - 1)("AMOUNT") = Amnt.ToString("#,###0.000")


        If ddExpensetype.SelectedValue.Equals("TR") Then
            _table.Rows(_table.Rows.Count - 1)("AMOUNT") = (Amnt * Convert.ToDouble(txtDistance.Text)).ToString("#,###0.000")
        End If
        gvJournal.DataSource = _table
        gvJournal.DataBind()
        Session("datatable") = _table
        If gvJournal.Rows.Count > 0 Then
            gvJournal.FooterRow.Cells(4).Text = "Total"
            gvJournal.FooterRow.Cells(6).Text = Amount.ToString("#,###0.000")
            gvJournal.FooterRow.Cells(6).HorizontalAlign = HorizontalAlign.Right

        End If
        clearDetails()
    End Sub
    Public Amount As Double = 0
    Public Slno As Double = 1


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If Not (e.Row.RowIndex.Equals(-1)) Then
            e.Row.Attributes.Add("onmouseover", "return  Mouse_Move('" + e.Row.ClientID + "')")
            e.Row.Attributes.Add("onmouseout", "return  Mouse_Out('" + e.Row.ClientID + "')")
            e.Row.Cells(0).Text = Slno.ToString()
            Amount += Convert.ToDouble(e.Row.Cells(6).Text)
            Slno += 1
        End If
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

        Dim _table As New DataTable
        _table = Session("datatable")
        txtDistance.Text = _table.Rows(e.RowIndex)("DISTANCE")
        txtDDocdate.Text = _table.Rows(e.RowIndex)("DATE")
        txtPurpose.Text = _table.Rows(e.RowIndex)("PURPOSE")
        txtRate.Text = _table.Rows(e.RowIndex)("RATE")
        txtUnit.Text = _table.Rows(e.RowIndex)("UNIT")

        _table.Rows.RemoveAt(e.RowIndex)
        gvJournal.DataSource = _table
        gvJournal.DataBind()
        Session("datatable") = _table
    End Sub

    Protected Sub ddExpensetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddExpensetype.SelectedIndexChanged
        clearGrid()
        CheckCashtype(ddExpensetype.SelectedValue.ToString())
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear_All()
    End Sub

    Sub filldrpDown()
        Dim sqlQuery As String
        sqlQuery = "SELECT TYP_ID,TYP_DESCRIPTION  FROM PETTYEXP_M ORDER BY  TYP_DESCRIPTION"
        ddExpensetype.DataSource = MainObj.ListRecords(sqlQuery, "MainDB")
        ddExpensetype.DataTextField = "TYP_DESCRIPTION"
        ddExpensetype.DataValueField = "TYP_ID"
        ddExpensetype.DataBind()
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If (chkForward.Checked) Then
            lblError.Text = "You Can Delete Only Rejected Documents And Documents Which Are Not Forwaded."
            Exit Sub
        Else
            ViewState("datamode") = "delete"
            Dim conn As SqlConnection = ConnectionManger.GetOASISFinConnection
            Dim trans As SqlTransaction = conn.BeginTransaction("SampleTransaction")
            Dim delStatus = SAVEToPETTYEXP_H(conn, trans)
            If delStatus = 0 Then
                ViewState("datamode") = "add"
                clear_All()
                clearGrid()
                LockUnlock(True)
                trans.Commit()
            Else
                lblError.Text = UtilityObj.getErrorMessage("1000")
                trans.Rollback()
            End If

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        End If
    End Sub
    Protected Sub txtDDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strfDate As String = txtDDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            txtDDocdate.Text = Date.Now.ToString("dd/MMM/yyyy")
            Exit Sub
        Else
            txtDDocdate.Text = strfDate
        End If
    End Sub

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strfDate As String = txtHDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            txtHDocdate.Text = Date.Now.ToString("dd/MMM/yyyy")
        Else

            txtHDocdate.Text = strfDate
        End If
        txtRemarks.Text = ddExpensetype.SelectedItem.Text.ToString() & "..Month of.." & Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("MMMM")
    End Sub
    Private Sub PrintVoucher(ByVal printId As Int32)
        Try
            Dim params As New Hashtable
            params.Add("@IMG_BSU_ID", Session("sbsuid"))
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("@PCH_ID", printId)
            params("reportHeading") = labHead.Text.ToUpper().ToString()

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasisfin"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Reports/RPT_Files/rptPettyExpenseVoucherReport.rpt")
                'End If
            End With
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If viewId.Value.Equals("") Or chkForward.Checked = False Then
            lblError.Text = "Invalid document selection..!"
            Exit Sub
        End If
        PrintVoucher(Int32.Parse(viewId.Value.ToString()))
    End Sub

    Protected Function OpenBrowsedDoc(ByVal PathVirtual As String) As Integer
        Dim strPhysicalPath As String
        Dim objFileInfo As System.IO.FileInfo
        Try
            'strPhysicalPath = Server.MapPath(PathVirtual)
            strPhysicalPath = PathVirtual
            'exit if file does not exist
            If strPhysicalPath <> "" Then
                If Not System.IO.File.Exists(strPhysicalPath) Then
                    lblError.Text = "File could not be generated"
                    Return 1000
                    'Exit Function
                Else
                    objFileInfo = New System.IO.FileInfo(strPhysicalPath)
                    Response.Clear()
                    'Add Headers to enable dialog display
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + "abc.xls")
                    Response.AddHeader("Content-Length", objFileInfo.Length.ToString())
                    'Response.ContentType = "application/octet-stream"
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.WriteFile(objFileInfo.FullName)
                    Response.Flush()
                    'System.IO.File.Delete(strPhysicalPath) 'To delete the file generated in the server
                    'Response.End()
                    'Instead use below code
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Return 0

                End If
            Else
                lblError.Text = "File could not be generated"
                Return 1000
            End If


        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000
        End Try
    End Function

    Protected Sub gvSubExpenses_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubExpenses.RowDataBound
        '  If ViewState("datamode") = "view" Then
        Try
            getExpSubTypes()
            Dim i As Integer = 0
            ' For Each grow As GridViewRow In gvSubExpenses.Rows
            Dim ddlsubType As DropDownList = DirectCast(e.Row.Cells(7).FindControl("ddlsubType"), DropDownList)
            Dim txtsubType As TextBox = DirectCast(e.Row.Cells(7).FindControl("txtsubType"), TextBox)
            If ddlsubType IsNot Nothing And txtsubType IsNot Nothing Then
                If ViewState("datamode") = "view" Then
                    ddlsubType.Visible = False
                    txtsubType.Visible = True
                End If
                ddlsubType.SelectedIndex = ddlsubType.Items.IndexOf(ddlsubType.Items.FindByText(txtsubType.Text))
            End If
            Dim imgDoc As ImageButton = DirectCast(e.Row.Cells(9).FindControl("imgDoc"), ImageButton)
            Dim btnImgDoc As HyperLink = DirectCast(e.Row.FindControl("btnImgDoc"), HyperLink)
            Dim imgDelete As Image = DirectCast(e.Row.Cells(9).FindControl("imgDelete"), Image)
            If btnImgDoc IsNot Nothing Then
                btnImgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(imgDoc.CommandArgument)
                If imgDoc.AlternateText <> "" Then
                    btnImgDoc.Visible = True
                    imgDelete.Visible = True
                Else
                    btnImgDoc.Visible = False
                    imgDelete.Visible = False
                End If
            End If
        Catch ex As Exception

        End Try
        ' Next
        '  End If
    End Sub

    Protected Sub gvSubExpenses_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSubExpenses.RowCommand
        Dim strDocID As String = ""
        strDocID = e.CommandArgument.ToString
        If e.CommandName = "View" Then
            'OpenFileFromDB(strDocID)
        ElseIf e.CommandName = "FileDelete" And ViewState("datamode") = "edit" Then
            DeleteFileFromDB(strDocID)
            GetViewExpData()
        End If
    End Sub
End Class

