<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accCCAllocation.aspx.vb" Inherits="Accounts_accCCAllocation" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        .RadGrid {
            border-radius: 0px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            /*background-color: #00a3e0;*/
        }

        .RadGrid_Office2010Blue th.rgSorted {
            /*background-color: #00a3e0;*/
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCellLeft {
            background-position: 0 0;
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default {
            width: 80% !important;
        }

        .rcbReadOnly td.rcbInputCell.rcbInputCellLeft {
            /* padding: 0px !important; */
            background-color: #ffffff !important;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../images/Misc/minus.gif";
                }
                else {
                    img.src = "../images/Misc/minus.gif";
                }
                img.alt = "Close to view other Customers";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../images/Misc/plus.gif";
                }
                else {
                    img.src = "../images/Misc/plus.gif";
                }
                img.alt = "Expand to show Orders";
            }
        }

        function GetDocNo() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var bsu = document.getElementById('<%= h_BSU_ID.ClientID %>').value;
            var subid = document.getElementById('<%= h_SUB_ID.ClientID %>').value;
            var docType = document.getElementById('<%= ddDocumentType.ClientID %>').value;
            var FYEAR = document.getElementById('<%= h_FYEAR.ClientID %>').value;

            pMode = "CCALLOCJV"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu + "&subid=" + subid + "&doctype=" + docType + "&FYEAR=" + FYEAR;
            result = radopen(url, "pop_up2")
           <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%= h_DocType.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= h_DocNo.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtDocNo.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= h_DocDate.ClientID %>').value = NameandCode[1];--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= h_DocType.ClientID %>').value = NameandCode[2];
                document.getElementById('<%= h_DocNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtDocNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= h_DocDate.ClientID %>').value = NameandCode[1];


            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="CCAlloc" />
                <br />
                <table id="tblMain" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Document Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddDocumentType" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Document No</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgDocNo" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetDocNo(); return false;" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Document Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocDate" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                CssClass="table table-bordered table-row" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemStyle Width="1%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemStyle Width="1%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccCode" runat="server" Text='<%# Bind("JNL_ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide the Cost Center Allocation <%# Eval("id") %>"
                                                    width="9px" border="0" src="../images/Misc/plus.gif" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="JNL_ACT_ID" HeaderText="Account Code"></asp:BoundField>
                                    <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" />
                                    <asp:BoundField DataField="JNL_MISSING" HeaderText="Status" />
                                    <asp:BoundField DataField="JNL_DEBIT" HeaderText="Debit"
                                        SortExpression="JNL_DEBIT">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JNL_CREDIT" HeaderText="Credit"
                                        SortExpression="JNL_CREDIT">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JNL_NARRATION" HeaderText="Narration">
                                        <ItemStyle Width="15%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" OnClick="lbEdit_Click" runat="server" Text="Edit" CausesValidation="false"
                                                CommandName="Edits"> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%" align="right">
                                                    <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 97%">
                                                        <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                            CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                <asp:BoundField DataField="Costcenter" HeaderText="Costcenter" />
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                    HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtAmt" runat="server" CssClass="inputbox" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                            onblur="CheckAmount(this)"></asp:TextBox>
                                                                        <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td colspan="100%" align="right">
                                                                                <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                                    Width="98%" CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                        <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                        <asp:TemplateField HeaderText="Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine" MaxLength="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="chqBR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            <asp:HiddenField ID="h_SUB_ID" runat="server" />
                            <asp:HiddenField ID="h_BSU_ID" runat="server" />
                            <asp:HiddenField ID="h_DocType" runat="server" />
                            <asp:HiddenField ID="h_DocNo" runat="server" />
                            <asp:HiddenField ID="h_NextLine" runat="server" />
                            <asp:HiddenField ID="h_Jnl_id" runat="server" />
                            <asp:HiddenField ID="h_EditID" runat="server" />
                            <br />
                            <asp:HiddenField ID="h_DocDate" runat="server" />
                            <asp:HiddenField ID="h_FYEAR" runat="server" />
                            <asp:TextBox ID="Detail_ACT_ID" runat="server" AutoCompleteType="Disabled" Visible="false" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
