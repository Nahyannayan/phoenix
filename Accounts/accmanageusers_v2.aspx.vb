Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class manageusers_v2
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = String.Format("~/accounts/accAddEditUser.aspx?MainMnu_code={0}&datamode={1}&type={2}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"), Encr_decrData.Encrypt("V2"))

            gvManageUsers.Attributes.Add("bordercolor", "#1b80b6")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050049") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                gridbind()
            End If
        End If
        set_Menu_Img()
    End Sub

    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = String.Empty
            Dim str_filter_User As String = String.Empty
            Dim str_filter_Ename As String = String.Empty
            Dim str_filter_Role As String = String.Empty
            Dim str_filter_BName As String = String.Empty

            str_Sql = " select * from(select users_m.usr_id as usr_id,users_m.usr_name as usr_name ," & _
            " isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as Full_Name, " & _
            " users_m.usr_emp_id,users_m.usr_rol_id, roles_m.ROL_DESCR as DESCR  , businessunit_m.BSU_NAME as BSU_NAME ," & _
            " users_m.usr_bsu_id from users_m join businessunit_m  on users_m.usr_bsu_id=businessunit_m.BSU_ID " & _
            "  join roles_m on users_m.usr_rol_id=roles_m.ROL_ID left outer join vw_OSO_EMPLOYEE_M on " & _
            " users_m.usr_emp_id=vw_OSO_EMPLOYEE_M.EMP_ID)a where  a.usr_id<>'' AND " & _
            " a.USR_BSU_ID IN (	SELECT MNR_BSU_ID FROM dbo.MENURIGHTS_S WHERE 	MNR_ROL_ID=" & Session("sroleid") & " AND MNR_MNU_ID='" & ViewState("MainMnu_code") & "')"

            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_txtusername As String = String.Empty
            Dim str_txtUserRole As String = String.Empty
            Dim str_txtEmpName As String = String.Empty
            Dim str_txtBusName As String = String.Empty
            If gvManageUsers.Rows.Count > 0 Then
                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
                str_txtusername = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_User = " AND a.usr_name LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
                str_txtUserRole = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Role = " AND a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Role = "  AND  NOT a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Role = " AND a.DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Role = " AND a.DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Role = " AND a.DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Role = " AND a.DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
                str_txtEmpName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Ename = " AND a.full_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Ename = "  AND  NOT a.full_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Ename = " AND a.full_name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Ename = " AND a.full_name  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Ename = " AND a.full_name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Ename = " AND a.full_name NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")

                str_txtBusName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_BName = " AND a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_BName = "  AND  NOT a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_BName = " AND a.BSU_NAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_BName = " AND a.BSU_NAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_BName = " AND a.BSU_NAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_BName = " AND a.BSU_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_User & str_filter_Role & str_filter_BName & str_filter_Ename & " order by usr_name")
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
            txtSearch.Text = str_txtusername
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
            txtSearch.Text = str_txtUserRole
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_txtEmpName
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")
            txtSearch.Text = str_txtBusName
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvManageUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvManageUsers.RowCommand
        If e.CommandName = "Select" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvManageUsers.Rows(index), GridViewRow)

            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            Dim url As String


            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~/accounts/accAddEditUser.aspx?MainMnu_code={0}&datamode={1}&Eid={2}&type={3}", ViewState("MainMnu_code"), ViewState("datamode"), Eid, Encr_decrData.Encrypt("V2"))
            Response.Redirect(url, False)

        End If

    End Sub

    Protected Sub btnSearchUserName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchUserRole_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchBusUnit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvManageUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUsers.PageIndexChanging
        gvManageUsers.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

End Class
