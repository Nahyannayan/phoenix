Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections



Imports Encryption64
Imports System.Collections.Generic

Partial Class Accounts_StudentPasswordFetch
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim str_query As String
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050018") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ''commented by nahyan on 24 apr 2019 - prem 
                    'str_query = " SELECT * FROM USERACCESS_MENU WHERE USM_USR_NAME='" & Session("sUsr_name") & "' AND USM_BSU_ID='" & CurBsUnit & "'"
                    'Dim ds As DataSet
                    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    'If ds.Tables(0).Rows.Count >= 1 Then
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    '  btnActivate.Visible = True
                    btnReset.Visible = True
                    ddlBUnit.Visible = True
                    tabmain.Visible = True
                    lblError.Text = ""
                    lblErrors.Text = ""
                    '    Else
                    '    ' btnActivate.Visible = False
                    '    btnReset.Visible = False 'False
                    '    ddlBUnit.Visible = False
                    '    tabmain.Visible = False
                    '    lblError.Text = "Access denied"
                    'End If

                    'btnReset.Visible = False 'Change in Process by removing the checkbox so forcefully making this button visible false 
                    'checkbox reverted back on 25FEB2019, so commented above line
                End If
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            set_Menu_Img()
        End If

        'Dim CheckBoxArray As ArrayList
        'If ViewState("CheckBoxArray") IsNot Nothing Then
        '    CheckBoxArray = DirectCast(ViewState("CheckBoxArray"), ArrayList)
        'Else
        '    CheckBoxArray = New ArrayList()
        'End If
        'If IsPostBack Then
        '    Dim CheckBoxIndex As Integer
        '    Dim CheckAllWasChecked As Boolean = False
        '    Dim chkAll As CheckBox = _
        '     DirectCast(gvUNITS.HeaderRow.Cells(0).FindControl("chkAll"), CheckBox)

        '    Dim checkAllIndex As String = "chkAll-" & gvUNITS.PageIndex
        '    If chkAll.Checked Then
        '        If CheckBoxArray.IndexOf(checkAllIndex) = -1 Then
        '            CheckBoxArray.Add(checkAllIndex)
        '        End If
        '    Else
        '        If CheckBoxArray.IndexOf(checkAllIndex) <> -1 Then
        '            CheckBoxArray.Remove(checkAllIndex)
        '            CheckAllWasChecked = True
        '        End If
        '    End If
        '    For i As Integer = 0 To gvUNITS.Rows.Count - 1
        '        If gvUNITS.Rows(i).RowType = DataControlRowType.DataRow Then
        '            Dim chk As CheckBox = _
        '             DirectCast(gvUNITS.Rows(i).Cells(0) _
        '             .FindControl("chkSelect"), CheckBox)
        '            CheckBoxIndex = gvUNITS.PageSize * gvUNITS.PageIndex + (i + 1)
        '            If chk.Checked Then
        '                If CheckBoxArray.IndexOf(CheckBoxIndex) = -1 And _
        '                 Not CheckAllWasChecked Then
        '                    CheckBoxArray.Add(CheckBoxIndex)
        '                End If
        '            Else
        '                If CheckBoxArray.IndexOf(CheckBoxIndex) <> -1 Or _
        '                  CheckAllWasChecked Then
        '                    CheckBoxArray.Remove(CheckBoxIndex)
        '                End If
        '            End If
        '        End If
        '    Next
        'End If
        'ViewState("CheckBoxArray") = CheckBoxArray
    End Sub
    Public Sub gridbind(Optional ByVal list As List(Of String) = Nothing)
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""
        Dim strName As String = ""
        Dim strUsrName As String = ""
        Dim strFeeId As String = ""
        Dim txtSearch As New TextBox
        Dim strQuery As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ddlSTU_GRDS As DropDownList
        Dim ddlSTU_SCTS As DropDownList
        Dim vSTU_GRD_VAL As String = String.Empty
        Dim vSTU_SCT_VAL As String = String.Empty


        Dim STU_ID As String = ""
        If Not list Is Nothing Then
            For i = 0 To list.Count - 1
                If i = 0 Then
                    'STU_ID = STU_ID & "'" & list.Item(i) & "'"
                    STU_ID = STU_ID & "" & list.Item(i) & ""
                Else
                    'STU_ID = STU_ID & "|'" & list.Item(i) & "'"
                    STU_ID = STU_ID & "|" & list.Item(i) & ""
                End If
            Next
        End If
        Try
            If gvUNITS.Rows.Count > 0 Then

               
                txtSearch = gvUNITS.HeaderRow.FindControl("txtFeeIdSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("STU_Fee_Id", txtSearch.Text, strSearch)
                strFeeId = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvUNITS.HeaderRow.FindControl("txtStudNameSearch")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("StudName", txtSearch.Text.Replace("/", " "), strSearch)
                strName = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvUNITS.HeaderRow.FindControl("txtParentUserNameSearch")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("STU_USR_NAME", txtSearch.Text.Replace("/", " "), strSearch)
                strUsrName = txtSearch.Text

                'Dim vSTU_GRD As String = String.Empty
                'Dim vSTU_SCT As String = String.Empty
           
                Try
                    ddlSTU_GRDS = gvUNITS.HeaderRow.FindControl("ddlgvGrade")
                    'vSTU_GRD = " AND STU_TYPE in (" & ddlSTU_GRDS.SelectedValue & ")"
                    vSTU_GRD_VAL = ddlSTU_GRDS.SelectedValue
                Catch ex As Exception
                    'vSTU_GRD = String.Empty
                End Try


            
                Try
                    ddlSTU_SCTS = gvUNITS.HeaderRow.FindControl("ddlgvSection")
                    'vSTU_SCT = " AND STU_TYPE in (" & ddlSTU_SCTS.SelectedValue & ")"
                    vSTU_SCT_VAL = ddlSTU_SCTS.SelectedValue
                Catch ex As Exception
                    'vSTU_SCT = String.Empty
                End Try

                If strFilter <> "" Then
                    Dim STU_ID_FILTER As String = ""
                    If Not STU_ID = "" Then
                        STU_ID_FILTER = " STU_ID IN (" & STU_ID & ") AND "
                    End If
                    strQuery = "SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE " & STU_ID_FILTER & " STU_BSU_ID='" & ddlBUnit.SelectedValue & "'"
                    'strQuery = "SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE " & STU_ID_FILTER & " STU_BSU_ID='" & ddlBUnit.SelectedValue & "' AND STU_SCT_ID IN (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe ((select GSA_SCT_ID from GRADE_SECTION_ACCESS where GSA_USR_ID='" & Session("sUsr_name") & "' ),'|')) "
                    strQuery += strFilter
                Else
                    Dim STU_ID_FILTER As String = ""
                    If Not STU_ID = "" Then
                        STU_ID_FILTER = " STU_ID IN (" & STU_ID & ") AND "
                    End If
                    strQuery = "SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE " & STU_ID_FILTER & " STU_BSU_ID='" & ddlBUnit.SelectedValue & "' ORDER BY StudName"
                    'strQuery = "SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE " & STU_ID_FILTER & " STU_BSU_ID='" & ddlBUnit.SelectedValue & "' AND STU_SCT_ID IN (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe ((select GSA_SCT_ID from GRADE_SECTION_ACCESS where GSA_USR_ID='" & Session("sUsr_name") & "' ),'|')) ORDER BY StudName"
                End If
            Else
                strQuery = "SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE STU_BSU_ID='" & ddlBUnit.SelectedValue & "' ORDER BY StudName"
                'strQuery = " SELECT *,'' AS RESET_PASSWORD  from vw_PARENT_LIST A WHERE STU_BSU_ID='" & ddlBUnit.SelectedValue & "' AND STU_SCT_ID IN (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe ((select GSA_SCT_ID from GRADE_SECTION_ACCESS where GSA_USR_ID='" & Session("sUsr_name") & "' ),'|')) ORDER BY StudName"
                vSTU_GRD_VAL = "ALL"
                vSTU_SCT_VAL = "ALL"
            End If
            Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BU_CODE", SqlDbType.VarChar)
            pParms(1).Value = ddlBUnit.SelectedValue
            pParms(2) = New SqlClient.SqlParameter("@USER_NAME", SqlDbType.VarChar)
            pParms(2).Value = Session("sUsr_name")
            pParms(3) = New SqlClient.SqlParameter("@GRD_ID", SqlDbType.VarChar)
            pParms(3).Value = vSTU_GRD_VAL
            pParms(4) = New SqlClient.SqlParameter("@SCT_DESCR", SqlDbType.VarChar)
            pParms(4).Value = vSTU_SCT_VAL
            pParms(5) = New SqlClient.SqlParameter("@STU_Fee_Id", SqlDbType.VarChar)
            pParms(5).Value = strFeeId
            pParms(6) = New SqlClient.SqlParameter("@StudName", SqlDbType.VarChar)
            pParms(6).Value = strName
            pParms(7) = New SqlClient.SqlParameter("@STU_USR_NAME", SqlDbType.VarChar)
            pParms(7).Value = strUsrName
            pParms(8) = New SqlClient.SqlParameter("@STU_IDS", SqlDbType.VarChar)
            pParms(8).Value = STU_ID

            'strQuery = "[OASIS].[DBO].[GET_STU_PARENT_LIST] 1,'" & ddlBUnit.SelectedValue & "','" & Session("sUsr_name") & "','" & vSTU_GRD_VAL & "','" & vSTU_SCT_VAL & "','" & strFeeId & "','" & strName & "','" & strUsrName & "'"
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, strQuery)
            strQuery = "[OASIS].[DBO].[GET_STU_PARENT_LIST]"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, strQuery, pParms)

            If Not list Is Nothing Then
                For i = 0 To list.Count - 1
                    Dim OLU_ID As String = list.Item(i)
                    If ds.Tables(0).Rows.Count > 0 Then

                        'For Each dr As DataRow In ds.Tables(0).Rows
                        '    'If dr("STU_ID").ToString = OLU_ID Then
                        '    If dr.Item("STU_ID").ToString = OLU_ID Then
                        '        Dim Encr_decrData As New Encryption64
                        '        'dr.Item("RESET_PASSWORD") = Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("STU_PASSWORD").ToString.Replace(" ", "+"))
                        '        dr.Item("RESET_PASSWORD") = Encr_decrData.Decrypt(dr.Item("STU_PASSWORD").ToString.Replace(" ", "+"))
                        '    End If
                        'Next
                        'if using Lambda 
                        'var test = dt.AsEnumerable().Where(x => x.Field<string>("Year") == "2013" && x.Field<string>("Month") == "2").ToList();

                        'Dim test = (From x In ds.Tables(0).AsEnumerable()
                        '            Where (x.Field(Of String)("STU_ID") = OLU_ID)
                        'Select x)
                        '    'Where (dt.Field(Of String)("EmpName").EndsWith("a")) Select New With {.Name = dt.Field(Of String)("EmpName"), .Location = dt.Field(Of String)("Location")}

                        'test(0)("RESET_PASSWORD") = "2015"

                        'ds.Tables(0).AcceptChanges()

                        For Each dr In ds.Tables(0).Rows
                            If dr.Item("STU_ID").ToString = OLU_ID Then
                                dr.Item("RESET_PASSWORD") = Encr_decrData.Decrypt(dr.Item("STU_PASSWORD").ToString.Replace(" ", "+"))
                            End If
                        Next

                    End If
                Next i
            End If


            'For Each gr As GridViewRow In gvUNITS.Rows
            '    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
            '        Dim OLU_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
            '        If ds.Tables(0).Rows.Count > 0 Then
            '            Dim i As Integer = 0
            '            For Each dr As DataRow In ds.Tables(0).Rows
            '                'If ds.Tables(0).Rows(0)("STU_ID").ToString = OLU_ID Then
            '                '    Dim Encr_decrData As New Encryption64
            '                '    Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("STU_PASSWORD").ToString.Replace(" ", "+"))
            '                'End If

            '                'If dr("STU_ID").ToString = OLU_ID Then
            '                If dr.Item("STU_ID").ToString = OLU_ID Then
            '                    Dim Encr_decrData As New Encryption64
            '                    'dr.Item("RESET_PASSWORD") = Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("STU_PASSWORD").ToString.Replace(" ", "+"))
            '                    dr.Item("RESET_PASSWORD") = Encr_decrData.Decrypt(dr.Item("STU_PASSWORD").ToString.Replace(" ", "+"))
            '                End If
            '            Next


            '        End If
            '    End If
            'Next

            'If ds.Tables(0).Rows.Count > 0 Then
            '    Dim i As Integer = 0
            '    For Each dr As DataRow In ds.Tables(0).Rows
            '        Dim Encr_decrData As New Encryption64
            '        Encr_decrData.Decrypt(ds.Tables(0).Rows(0)("OLU_PASSWORD").ToString.Replace(" ", "+"))
            '    Next
            'End If
            gvUNITS.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvUNITS.DataBind()
                Dim columnCount As Integer = gvUNITS.Rows(0).Cells.Count
                gvUNITS.Rows(0).Cells.Clear()
                gvUNITS.Rows(0).Cells.Add(New TableCell)
                gvUNITS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvUNITS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvUNITS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' btnActivate.Enabled = False
                btnReset.Enabled = False
                gvUNITS.HeaderRow.Visible = False
            Else
                ' btnActivate.Enabled = True
                btnReset.Enabled = True
                gvUNITS.DataBind()
            End If


            If gvUNITS.Rows.Count > 0 Then
                Dim list_grd As List(Of String) = New List(Of String)()
                Dim list_sct As List(Of String) = New List(Of String)()

                ddlSTU_GRDS = gvUNITS.HeaderRow.FindControl("ddlgvGrade")
                ddlSTU_SCTS = gvUNITS.HeaderRow.FindControl("ddlgvSection")

                Dim dr As DataRow

                ddlSTU_GRDS.Items.Clear()
                ddlSTU_GRDS.Items.Add("ALL")


                ddlSTU_SCTS.Items.Clear()
                ddlSTU_SCTS.Items.Add("ALL")


                'list_grd.Add("ALL")
                'list_sct.Add("ALL")

                For Each dr In ds.Tables(0).Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        ''check for duplicate values and add to dropdownlist 
                        If ddlSTU_GRDS.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlSTU_GRDS.Items.Add(.Item(6))
                            list_grd.Add(.Item(6))
                        End If
                        If ddlSTU_SCTS.Items.FindByText(.Item(9)) Is Nothing Then
                            ddlSTU_SCTS.Items.Add(.Item(9))
                            list_sct.Add(.Item(9))
                        End If
                    End With

                Next




                list_grd.Sort()
                list_sct.Sort()
                ddlSTU_GRDS.Items.Clear()
                ddlSTU_SCTS.Items.Clear()
                For Each li As String In list_grd
                    ddlSTU_GRDS.Items.Add(li)
                Next

                For Each li As String In list_sct
                    ddlSTU_SCTS.Items.Add(li)
                Next


                ddlSTU_GRDS.Items.Insert(0, New ListItem("ALL", "ALL"))
                'ddlSTU_GRDS.SelectedIndex = 0

                ddlSTU_SCTS.Items.Insert(0, New ListItem("ALL", "ALL"))
                'ddlSTU_SCTS.SelectedIndex = 0

                If vSTU_GRD_VAL <> "" Then
                    ddlSTU_GRDS.Text = vSTU_GRD_VAL
                End If


                If vSTU_SCT_VAL <> "" Then
                    ddlSTU_SCTS.Text = vSTU_SCT_VAL
                End If


            End If









            'set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Protected Sub SendEmailToParent(ByVal STU_ID As String)
        Dim Mailstatus As String = ""
        Dim APH_SOURCE As String = "0"
        Try

            Dim dt6 As DataTable = GET_STUDENT_DETAILS(STU_ID)

            For Each drow As DataRow In dt6.Rows

                Dim ToEmailId As String = ""
                Dim ToEMPName As String = ""
                ToEmailId = drow.Item("PARENT_EMAIL")
                'ToEmailId = "shakeel.shakkeer@gemseducation.com"
                If ToEmailId = "" Then
                    ToEmailId = "shakeel.shakkeer@gemseducation.com"
                End If

                Dim CLASS_TEACHER As String = ""
                Dim dt4 As DataTable = GET_CLASS_TEACHER(STU_ID)
                For Each row As DataRow In dt4.Rows
                    CLASS_TEACHER = row.Item("CLASS_TEACHER")
                Next


                Dim Subject As String = ""
                Dim dt_et As DataTable = GetEmailText(Session("sBsuid"), "PASSWORD_RESET", "GEMS")
                Dim Email_Text As String = ""
                For Each row As DataRow In dt_et.Rows
                    Email_Text = row.Item("EMAIL_MESSAGE")
                    Subject = row.Item("EMAIL_SUBJECT")
                Next
                Email_Text = Email_Text.Replace("/**PARENT_FIRST_NAME**/", drow.Item("ParentFirstName"))

                Subject = Subject.Replace("/**CHILD_NAME**/", drow.Item("StudName"))
                Email_Text = Email_Text.Replace("/**CHILD_NAME**/", drow.Item("StudName"))
                Email_Text = Email_Text.Replace("/**GRADE_SECTION**/", drow.Item("STU_GRD_ID") & "/" & drow.Item("SCT_DESCR"))
                Email_Text = Email_Text.Replace("/**ACCOUNT_NAME**/", drow.Item("STU_USR_NAME"))
                Email_Text = Email_Text.Replace("/**DATE_TIME**/", DateTime.Now)
                Email_Text = Email_Text.Replace("/**CLASS_OR_ICT_TEACHER_NAME**/", CLASS_TEACHER)
                Email_Text = Email_Text.Replace("/**SCHOOL_NAME**/", drow.Item("SCHOOL_NAME"))
                Try
                    Dim ReturnValue As Integer = InsertIntoEmailSendSchedule(Session("sBsuid"), "PASSWORD_RESET", "SYSTEM", ToEmailId, Subject, Email_Text)
                Catch ex As Exception
                    lblError.Text = ex.Message.ToString
                Finally

                End Try

            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function SendEmailNotification(ByVal OPTIONS As Integer, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal EML_TYPE As String, ByVal COMPANY As String) As Integer
        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 1000)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 100)
        pParms(3).Value = EML_TYPE
        pParms(4) = New SqlClient.SqlParameter("@COMPANY", SqlDbType.VarChar, 50)
        pParms(4).Value = COMPANY

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
         CommandType.StoredProcedure, "OASIS.[DBO].[BULK_EMAIL_INSERT_SCHEDULE_JOB]", pParms)
        Return ReturnFlag

    End Function

    Public Shared Function GET_STUDENT_DETAILS(ByVal STU_ID As String) As DataTable
        'Dim pParms(2) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        'pParms(0).Value = OPTIONS
        'pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 50)
        'pParms(1).Value = STU_ID


        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
        '  CommandType.StoredProcedure, "[dbo].[GET_STUDENT_PARENT_DETAILS_BY_STUID]", pParms)
        'If Not dsData Is Nothing Then
        '    Return dsData.Tables(0)
        'Else
        '    Return Nothing
        'End If

        Dim str_Sql As String = "SELECT * FROM OASIS.[dbo].vw_PARENT_LIST WHERE STU_ID=" & STU_ID

        Dim ds As New DataSet

        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GET_CLASS_TEACHER(ByVal STU_ID As String) As DataTable

        Dim str_Sql As String = "SELECT E.EMP_DISPLAYNAME AS CLASS_TEACHER FROM [OASIS].[dbo].STUDENT_M " &
                " INNER JOIN [OASIS].[dbo].[SECTION_TUTOR_D] S ON  SEC_SCT_ID=STU_SCT_ID AND STU_GRM_ID=SEC_GRM_ID AND STU_BSU_ID=SEC_BSU_ID AND STU_ACD_ID=SEC_ACD_ID " &
                " INNER JOIN [OASIS].[dbo].EMPLOYEE_M E ON EMP_ID=SEC_EMP_ID WHERE STU_ID = " & STU_ID &
                " AND SEC_TODT IS NULL"

        Dim ds As New DataSet

        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GetEmailText(ByVal sBsuid As String, ByVal stu_type As String, ByVal company As String) As DataTable

        Dim str_Sql As String = "SELECT * FROM OASIS.[dbo].[FN_GET_GENERIC_EMAIL_TEMPLATES]('" & sBsuid & "', '" & stu_type & "','" & company & "')"

        Dim ds As New DataSet

        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function
    Public Shared Function InsertIntoEmailSendSchedule(ByVal EML_BSU_ID As String, ByVal EML_TYPE As String,
                                                 ByVal EML_PROFILE_ID As String, ByVal EML_TOEMAIL As String, ByVal EML_SUBJECT As String, ByVal EML_MESSAGE As String) As Integer


        Dim pParms(10) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EML_BSU_ID", SqlDbType.VarChar, 50)
        pParms(0).Value = EML_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 50)
        pParms(1).Value = EML_TYPE
        pParms(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SqlDbType.VarChar, 500)
        pParms(2).Value = EML_PROFILE_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", SqlDbType.VarChar, 500)
        pParms(3).Value = EML_TOEMAIL
        pParms(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SqlDbType.VarChar, 500)
        pParms(4).Value = EML_SUBJECT
        pParms(5) = New SqlClient.SqlParameter("@EML_MESSAGE", SqlDbType.VarChar)
        pParms(5).Value = EML_MESSAGE
        pParms(6) = New SqlClient.SqlParameter("@EML_ID", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.Output

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "OASIS.dbo.InsertIntoEmailSendSchedule", pParms)
        Dim ReturnValue As Integer = pParms(6).Value
        Return ReturnValue

    End Function

    Private Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAME"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()

        If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub gvUNITS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUNITS.PageIndexChanging
        Try
            gvUNITS.PageIndex = e.NewPageIndex
            gridbind()
            'Dim CheckBoxArray As ArrayList = CType(ViewState("CheckBoxArray"), ArrayList)
            'Dim checkAllIndex As String = "chkAll-" & gvUNITS.PageIndex
            'For i As Integer = 0 To gvUNITS.Rows.Count - 1
            '    If gvUNITS.Rows(i).RowType = DataControlRowType.DataRow Then
            '        If CheckBoxArray.IndexOf(checkAllIndex) <> -1 Then
            '            Dim chk As CheckBox = _
            '             DirectCast(gvUNITS.Rows(i).Cells(0) _
            '             .FindControl("chkSelect"), CheckBox)
            '            chk.Checked = True
            '            'gvUNITS.Rows(i).Attributes.Add("style", "background-color:aqua")
            '            gvUNITS.Rows(i).Attributes.Add("style", "background-color:#e0ffba")
            '        Else
            '            Dim CheckBoxIndex As Integer = gvUNITS.PageSize * (gvUNITS.PageIndex) + (i + 1)
            '            If CheckBoxArray.IndexOf(CheckBoxIndex) <> -1 Then
            '                Dim chk As CheckBox = _
            '                 DirectCast(gvUNITS.Rows(i).Cells(0) _
            '                 .FindControl("chkSelect"), CheckBox)
            '                chk.Checked = True
            '                'gvUNITS.Rows(i).Attributes.Add("style", "background-color:aqua")
            '                gvUNITS.Rows(i).Attributes.Add("style", "background-color:#e0ffba")
            '            End If
            '        End If
            '    End If
            'Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        gridbind()
    End Sub
    'Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivate.Click
    '    Try
    '        Dim flag As Integer = 0
    '        If gvUNITS.Rows.Count > 0 Then
    '            For Each gr As GridViewRow In gvUNITS.Rows
    '                If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
    '                    flag = flag + 1
    '                    Dim strSections As String = ""
    '                    Activate()
    '                    gridbind()
    '                End If
    '                If flag = 0 Then
    '                    lblError.Text = "Please select a Row"
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed"
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub
    'Sub Activate()
    '    Dim str_query As String
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim transaction As SqlTransaction
    '    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '        transaction = conn.BeginTransaction("SampleTransaction")
    '        Try
    '            For Each gr As GridViewRow In gvUNITS.Rows
    '                If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
    '                    Dim OLU_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
    '                    str_query = "exec sp_ParentActivatePassword '" & OLU_ID & "'"
    '                    Dim stat As Integer
    '                    stat = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
    '                End If
    '            Next
    '            transaction.Commit()
    '            lblError.Text = "Account Activated Successfully"
    '        Catch myex As ArgumentException
    '            transaction.Rollback()
    '            lblError.Text = myex.Message
    '            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Catch ex As Exception
    '            transaction.Rollback()
    '            lblError.Text = "Record could not be Saved"
    '            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        End Try
    '    End Using
    'End Sub
    Public Shared Function GetStuIDWithSeperator(ByVal p_IDs As String, ByVal separator As String) As String
        Try
            Dim str_appendedString As String = String.Empty
            Dim IDs As String() = p_IDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                str_appendedString += IDs(i) + separator
                i += 1
            Next
            Return str_appendedString
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim row As GridViewRow = (CType((CType(sender, CheckBox)).NamingContainer, GridViewRow))
        Dim index As Integer = row.RowIndex
        Dim cb1 As CheckBox = CType(gvUNITS.Rows(index).FindControl("chkSelect"), CheckBox)

        If cb1.Checked Then
            Dim flag As Integer = 0
            If gvUNITS.Rows.Count > 0 Then
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        flag = flag + 1
                        If flag <= 5 And cb1.Checked = True Then
                            gr.Attributes.Add("BackColor", "#e0ffba")
                            gr.Attributes.Add("style", "background-color:#e0ffba")
                        End If
                    End If
                Next
            End If
            If flag >= 6 Then
                cb1.Checked = False
                lblError.Text = "Maximum 5 students only can update in one attempt"
                lblErrors.Text = "Maximum 5 students only can update in one attempt"
                row.Attributes.Add("BackColor", "#ffffff")
                row.Attributes.Add("style", "background-color:#ffffff")
                'Else
                '    ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "javascript:highlight(this);", True)
            End If
        Else
            row.Attributes.Add("BackColor", "#ffffff")
            row.Attributes.Add("style", "background-color:#ffffff")
        End If


    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Dim flag As Integer = 0
            Dim OLU_ID As String
            Dim list As New List(Of String)
            If gvUNITS.Rows.Count > 0 Then
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                        OLU_ID = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                        list.Add(OLU_ID)
                        flag = flag + 1
                        'Dim strSections As String = ""

                    End If

                Next
            End If
            If flag = 0 Then
                lblError.Text = "Please select a Student"
                lblErrors.Text = "Please select a Student"
            ElseIf flag >= 6 Then
                lblError.Text = "Maximum 5 students only can update in one attempt"
                lblErrors.Text = "Maximum 5 students only can update in one attempt"
            Else
                Reset()
                gridbind(list)

                Dim STU_ID As String = ""
                If Not list Is Nothing Then
                    For i = 0 To list.Count - 1
                        If i = 0 Then
                            STU_ID = STU_ID & list.Item(i)
                        Else
                            STU_ID = STU_ID & "||" & list.Item(i)
                        End If


                    Next i
                End If
                SendEmailNotification(1, STU_ID, Session("sBsuid"), "PASSWORD_RESET", "GEMS")
            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub Reset()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Dim bPasswordUpdate As Boolean = False
        Dim status As Integer
        Dim strParUserName As String


        Dim lstrPWD As String
        Dim pParms(4) As SqlClient.SqlParameter

        'Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_GLG_DEFP", pParms)
        '    While reader.Read
        '        lstrPWD = Convert.ToString(reader("BSU_GLG_DEFP"))
        '    End While
        'End Using


        Using conn As SqlConnection = ConnectionManger.GetGLGConnection
            'transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each gr As GridViewRow In gvUNITS.Rows
                    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then

                        Dim OLU_ID As String = TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                        strParUserName = TryCast(gr.FindControl("HiddenField2"), HiddenField).Value.ToString

                        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddlBUnit.SelectedItem.Value)
                        pParms(1) = New SqlClient.SqlParameter("@ROLE", "STUDENT")
                        pParms(2) = New SqlClient.SqlParameter("@STU_ID", OLU_ID)
                        pParms(3) = New SqlClient.SqlParameter("@STU_USR_NAME", strParUserName)
                        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_GLG_DEFP_v2", pParms)
                            While reader.Read
                                lstrPWD = Convert.ToString(reader("BSU_GLG_DEFP"))
                            End While
                        End Using



                        'Commented on 27FEB2019
                        'str_query = "exec sp_StudentResetPasswordv2 '" & OLU_ID & "'"
                        'Dim stat As Integer
                        'stat = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str_query)
                        'If stat < 1 Then
                        '    Throw New ArgumentException(UtilityObj.getErrorMessage(stat))
                        'End If
                        'Commented on 27FEB2019 Ends Here

                        bPasswordUpdate = True
                        If bPasswordUpdate Then
                            ''commented and added by nahyan on 25jul2020
                            'status = VerifyandUpdatePassword(Encr_decrData.Encrypt(lstrPWD), strParUserName)
                            status = VerifyandUpdatePasswordHash(Encr_decrData.Encrypt(lstrPWD), Encr_decrData.EncryptWithSaltedHash(lstrPWD), strParUserName)
                            ''VerifyandUpdatePasswordHash
                            'Dim vGLGUPDPWD As New com.ChangePWDWebService
                            'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                            'Dim respon As String = vGLGUPDPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            ''comemnted by nahyan 01une2017
                            '' Dim respon As String = LDAP_CHANGEPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            ''by nahyan on 01june2017

                            'Commented on 27FEB2019
                            Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient
                            Dim respon As String = chPWDSVC.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                            'Commented on 27FEB2019 Ends Here
                            ''by nahyan on 01june2017

                        End If
                    End If
                Next
                'transaction.Commit()

                'lblError.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
                'lblErrors.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
            Catch myex As ArgumentException
                'transaction.Rollback()
                lblError.Text = myex.Message
                lblErrors.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                'transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                lblErrors.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub SingleReset(ByVal STU_ID As String, ByVal STU_USR_NAME As String)
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim transaction As SqlTransaction
        Dim bPasswordUpdate As Boolean = False
        Dim status As Integer
        Dim strParUserName As String


        Dim lstrPWD As String
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddlBUnit.SelectedItem.Value)
        pParms(1) = New SqlClient.SqlParameter("@ROLE", "STUDENT")
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        pParms(3) = New SqlClient.SqlParameter("@STU_USR_NAME", STU_USR_NAME)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_GLG_DEFP_v2", pParms)

            While reader.Read
                lstrPWD = Convert.ToString(reader("BSU_GLG_DEFP"))
            End While
        End Using


        Using conn As SqlConnection = ConnectionManger.GetGLGConnection
            'transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'For Each gr As GridViewRow In gvUNITS.Rows
                '    If (TryCast(gr.FindControl("chkSelect"), CheckBox).Checked = True) Then
                Dim OLU_ID As String = STU_ID 'TryCast(gr.FindControl("HiddenField1"), HiddenField).Value.ToString
                strParUserName = STU_USR_NAME 'TryCast(gr.FindControl("HiddenField2"), HiddenField).Value.ToString
                'str_query = "exec sp_StudentResetPassword '" & OLU_ID & "'"

                ''Commented on 27FEB2019
                'Dim stat As Integer
                'stat = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str_query)
                'If stat < 1 Then
                '    Throw New ArgumentException(UtilityObj.getErrorMessage(stat))
                'End If
                'Commented on 27FEB2019 Ends Here

                bPasswordUpdate = True
                If bPasswordUpdate Then
                    status = VerifyandUpdatePassword(Encr_decrData.Encrypt(lstrPWD), strParUserName)
                    'Dim vGLGUPDPWD As New com.ChangePWDWebService
                    'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    'Dim respon As String = vGLGUPDPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                    ''comemnted by nahyan 01une2017
                    '' Dim respon As String = LDAP_CHANGEPWD.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                    ''by nahyan on 01june2017

                    'Commented on 27FEB2019
                    Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient
                    Dim respon As String = chPWDSVC.ChangePassword(strParUserName, lstrPWD, lstrPWD)
                    'Commented on 27FEB2019 Ends Here

                    ''by nahyan on 01june2017
                End If
                '    End If
                'Next
                'transaction.Commit()

                lblError.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
                lblErrors.Text = "Password Reseted Successfully. The new password for " + strParUserName + " is " + lstrPWD
            Catch myex As ArgumentException
                'transaction.Rollback()
                lblError.Text = myex.Message
                lblErrors.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                'transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                lblErrors.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
            ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value
        End Using
    End Function

    'VerifyandUpdatePassword
    Public Shared Function VerifyandUpdatePasswordHash(ByVal Encr_Password As String, ByVal hashed_Password As String, _
            ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@HASHED_PWD", hashed_Password)
            pParms(2) = New SqlClient.SqlParameter("@ENCR_PWD", Encr_Password)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            'Added new SP to store the hashed password by Hrushikesh:8-Apr-2019
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD_HASH_ALL", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(3).Value
        End Using
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvUNITS.Rows.Count - 1
            Dim row As GridViewRow = gvUNITS.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        If ddlBUnit.SelectedIndex > -1 Then
            Call gridbind()
        End If
    End Sub
    Protected Sub gvUNITS_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUNITS.RowCommand
        Try
            If e.CommandName = "resett" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvUNITS.Rows(index), GridViewRow)
                Dim STU_ID As HiddenField = selectedRow.FindControl("HiddenField1")
                Dim STU_USR_NAME As HiddenField = selectedRow.FindControl("HiddenField2")
                SingleReset(STU_ID.Value.ToString, STU_USR_NAME.Value.ToString)
                Dim list As New List(Of String)
                list.Add(STU_ID.Value.ToString)
                gridbind(list)
                'SendEmailToParent(STU_ID.Value.ToString)
                SendEmailNotification(1, STU_ID.Value.ToString, Session("sBsuid"), "PASSWORD_RESET", "GEMS")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    'Protected Sub gvUNITS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUNITS.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim pass As String = Encr_decrData.Decrypt(DataBinder.Eval(e.Row.DataItem, "OLU_PASSWORD").ToString.Replace(" ", "+"))
    '            DirectCast(e.Row.Cells(8).FindControl("lblpasswrd"), Label).Text = pass
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "ERROR WHILE RETREVING DATA"
    '    End Try
    'End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnStudNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnParentUserName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvUNITS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvUNITS.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



End Class
