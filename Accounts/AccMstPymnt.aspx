<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="AccMstPymnt.aspx.vb" Inherits="AccMstPymnt" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Payment Terms
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="gridheader" DataKeyNames="CHB_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="20"
                                CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Bank Account" SortExpression="ACT_NAME">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Payment Term
                                    <br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" SkinID="Grid" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description" SortExpression="ACT_NAME">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Allow PDC
                                    <br />
                                            <asp:DropDownList ID="cmbYesNo" runat="server" AutoPostBack="True" CssClass="listbox"
                                                OnSelectedIndexChanged="cmbYesNo_SelectedIndexChanged">
                                                <asp:ListItem Value="All">All</asp:ListItem>
                                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                                <asp:ListItem Value="False">No</asp:ListItem>

                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" ImageUrl='<%# FormatImageUrl( Eval("Parent")) %>' runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Center" />

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Parent" SortExpression="Parent">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Days
                                    <br />
                                            <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCountrySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCountrySearch_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParent" SkinID="Grid" runat="server" Text='<%# Bind("PTM_DAYS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click1">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CHB_ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCHB_ID" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                          <ItemStyle  HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle />
                                <PagerStyle  />
                            </asp:GridView>

                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <span id="sp_message" class="error" runat="server">
                                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                                <input id="h_Selected_menu_2"
                                    runat="server" type="hidden" value="=" />
                                <input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" />
                                <input id="h_Selected_menu_3" runat="server" type="hidden"
                                    value="=" />
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

