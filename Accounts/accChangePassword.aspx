<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accChangePassword.aspx.vb" Inherits="Accounts_accChangePassword" Title="::::PHOENIX:::: Online Student Administration System::::" %>

<%@ Register Src="../UserControls/ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Change your Password"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <uc1:ChangePassword ID="ChangePassword1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>

