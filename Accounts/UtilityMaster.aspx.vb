﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Accounts_UtilityMaster
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ((ViewState("MainMnu_code") <> "A100090") And (ViewState("MainMnu_code") <> "FD00015")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable
            dt = MainObj.getRecords("select * from utility where UTI_ID=" & p_Modifyid, "MAINDB")

            If dt.Rows.Count > 0 Then
                txtAct_Id.Text = dt.Rows(0)("UTI_ACT_ID").ToString
                h_UTI_ID.Value = dt.Rows(0)("UTI_ID").ToString
                txtBUID.Text = dt.Rows(0)("UTI_BSU_ID").ToString
                ddlDurType.SelectedItem.Text = dt.Rows(0)("UTI_DUR_TYPE").ToString
                txtDocNo.Text = dt.Rows(0)("UTI_DOC_NO").ToString
                TxtActName.Text = Mainclass.getDataValue("select act_name from ACCOUNTS_M where act_ID='" & Trim(txtAct_Id.Text) & "'", "MainDB")
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Trim(txtBUID.Text) & "'", "MainDBO")
                txtDuration.Text = dt.Rows(0)("UTI_DUR").ToString
                txtBillDate.Text = Format(dt.Rows(0)("UTI_START_DATE"), "dd/MMM/yyyy")
                txtPayDate.Text = Format(dt.Rows(0)("UTI_PAY_DATE"), "dd/MMM/yyyy")
                txtAmount.Text = dt.Rows(0)("UTI_AMOUNT").ToString
            Else
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable

        imgICMDescr.Visible = Not mDisable
        txtAct_Id.Enabled = Not mDisable

        txtDocNo.Enabled = Not mDisable
        txtBillDate.Enabled = Not mDisable
        txtPayDate.Enabled = Not mDisable
        ddlDurType.Enabled = Not mDisable
        txtAmount.Enabled = Not mDisable
        txtDuration.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_UTI_ID.Value = 0
        txtAct_Id.Text = ""
        txtBUID.Text = Session("sBsuid")
        txtBUID.ReadOnly = True
        TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
        TxtActName.Text = ""
        txtDocNo.Text = ""
        txtAmount.Text = "0.00"
        txtBillDate.Text = ""
        txtPayDate.Text = ""
        txtDuration.Text = "1"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from journal_ud where JUD_TOTAL_AMOUNT>0 and JUD_READING>0 and jud_uti_id=" & h_UTI_ID.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update utility set uti_bdeleted=1 where uti_id=" & h_UTI_ID.Value)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = "unable to delete, used in transactions"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If txtAct_Id.Text.Trim = "" Then
                lblError.Text = "Account Number cannot be blank"
                Exit Sub
            End If
            If Not IsNumeric(txtDuration.Text) Then
                lblError.Text = "Duration Period"
                Exit Sub
            End If
            If Not IsDate(txtBillDate.Text) Or Not IsDate(txtPayDate.Text) Then
                lblError.Text = "Dates are mandatory"
            End If
            If Not IsNumeric(txtAmount.Text) Then
                lblError.Text = "Enter numbers only"
                Exit Sub
            End If

            Dim pParms(9) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@UTI_ID", h_UTI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@UTI_BSU_ID", txtBUID.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@UTI_ACT_ID", txtAct_Id.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@UTI_DOC_NO", txtDocNo.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@UTI_START_DATE", txtBillDate.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@UTI_PAY_DATE", txtPayDate.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@UTI_DUR_TYPE", ddlDurType.SelectedItem.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@UTI_DUR", txtDuration.Text, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@UTI_DAmount", txtAmount.Text, SqlDbType.Decimal)

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "Save_Utility", pParms)

                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
End Class

