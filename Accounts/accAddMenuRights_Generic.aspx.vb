Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic

Imports Telerik.Web.UI
Partial Class AddMenuRights_Generic
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim MainMnu_code As String
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                If USR_NAME = "" Or (MainMnu_code <> "D050125") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Response.CacheControl = "no-cache"

                    INITIAL_LOAD()
                    initial_State()

                End If
                '  Call TVmenu_Style()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        Format_AccessRights()
    End Sub
    Private Sub initial_State()
        trCopy11.Visible = False
        trCopy12.Visible = False
        trCopy2.Visible = False
        trMenu11.Visible = False
        trMenu12.Visible = False
        trMenu2.Visible = False
        ddlBsu.Enabled = True
        ddlRole.Enabled = True
        btnAddEdit.Visible = True
        ddlOprRight.ClearSelection()
        ddlOprRight.SelectedIndex = 0

    End Sub
    Private Sub After_AddEdit()
        trCopy11.Visible = True
        trCopy12.Visible = True
        trCopy2.Visible = True
        trMenu11.Visible = True
        trMenu12.Visible = True
        trMenu2.Visible = True
        ddlBsu.Enabled = False
        ddlRole.Enabled = False
        btnAddEdit.Visible = False


    End Sub
    Private Sub INITIAL_LOAD()
        BIND_BSU()
        BIND_ROLES()
        BIND_BSU_ROLES()
        BIND_BSU_COPY()

    End Sub
    Private Sub Format_AccessRights()
        ddlOprRight.Items(0).Attributes.Add("style", "color: #f03333;")
        ddlOprRight.Items(1).Attributes.Add("style", "color: #000000;")
        ddlOprRight.Items(2).Attributes.Add("style", "color: #575a12;")
        ddlOprRight.Items(3).Attributes.Add("style", "color: #e05d07;")
        ddlOprRight.Items(4).Attributes.Add("style", "color: #f01da6;")
        ddlOprRight.Items(5).Attributes.Add("style", "color: #0a0bff;")
        ddlOprRight.Items(6).Attributes.Add("style", "color: #09b52b;")
    End Sub
    Private Sub BIND_BSU()
        ddlBsu.Items.Clear()
        ddlBsuCopy.Items.Clear()

        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@INFO_TYPE", "ALL_UNIT")
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "MNU.GETBSU_ROLES_GENERIC", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    ddlBsu.Items.Add(New ListItem(DATAREADER("BSU_NAME"), DATAREADER("BSU_ID")))
                    ddlBsuCopy.Items.Add(New ListItem(DATAREADER("BSU_NAME"), DATAREADER("BSU_ID")))
                End While

            End If
        End Using
        If Not ddlBsu.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBsu.ClearSelection()
            ddlBsu.Items.FindByValue(Session("sBsuid")).Selected = True

            ddlBsuCopy.ClearSelection()
            ddlBsuCopy.Items.FindByValue(Session("sBsuid")).Selected = True
        End If

        ddlBsu.Font.Size = "8.5"
        ddlBsu.Font.Name = "verdana"
        ddlBsuCopy.Font.Size = "8.5"
        ddlBsuCopy.Font.Name = "verdana"
    End Sub
    Private Sub BIND_ROLES()
        ddlRole.Items.Clear()


        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@INFO_TYPE", "BSU_ROLE")
        PARAM(1) = New SqlParameter("@BSU_ID", ddlBsu.SelectedValue)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "MNU.GETBSU_ROLES_GENERIC", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    ddlRole.Items.Add(New ListItem(DATAREADER("ROL_DESCR"), DATAREADER("ROL_ID")))
                End While
            End If
        End Using
        ddlRole.Font.Size = "8.5"
        ddlRole.Font.Name = "verdana"


    End Sub
    Protected Sub ddlBsuCopy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsuCopy.SelectedIndexChanged
        BIND_BSU_ROLES()
    End Sub
    Protected Sub btnAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEdit.Click

        ddlBsu.Enabled = False
        ddlRole.Enabled = False
        After_AddEdit()
        Call TREEVIEWBIND()
        Format_AccessRights()

    End Sub

    Private Sub BIND_BSU_ROLES()
        Try
            ddlRoleCopy.Items.Clear()
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "BSU_ROLE")
            PARAM(1) = New SqlParameter("@BSU_ID", ddlBsu.SelectedValue)
            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "MNU.GETBSU_ROLES_GENERIC", PARAM)
                If DATAREADER.HasRows = True Then
                    While DATAREADER.Read
                        ddlRoleCopy.Items.Add(New ListItem(DATAREADER("ROL_DESCR"), DATAREADER("ROL_ID")))
                    End While
                End If
            End Using

            ddlRoleCopy.Font.Size = "8.5"
            ddlRoleCopy.Font.Name = "verdana"
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BIND_BSU_COPY()
        Try
            chkBSU_Copy.Items.Clear()
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@INFO_TYPE", "ALL_UNIT")
            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "MNU.GETBSU_ROLES_GENERIC", PARAM)
                If DATAREADER.HasRows = True Then
                    chkBSU_Copy.DataSource = DATAREADER
                    chkBSU_Copy.DataTextField = "BSU_NAME"
                    chkBSU_Copy.DataValueField = "BSU_ID"
                    chkBSU_Copy.DataBind()
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TREEVIEWBIND()
        Try
            Session("sMNURIGHTS") = Nothing
            Dim htMenuRights As New Hashtable
            Dim IEnumerableMenuRights As HoldMenuRights
            Dim ds As New DataSet
            Dim EmployeeID As String = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", ddlBsu.SelectedValue)
            PARAM(1) = New SqlParameter("@ROL_ID", ddlRole.SelectedValue)
            PARAM(2) = New SqlParameter("@EMP_ID", EmployeeID)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "MNU.GETMENURIGHTS_TREEBIND_WITHBSU", PARAM)
            rtvMenuRights.DataTextField = "MNU_TEXT"
            rtvMenuRights.DataValueField = "MNU_CODE"
            rtvMenuRights.DataFieldID = "MNU_CODE"
            rtvMenuRights.DataFieldParentID = "MNU_PARENTID"

            rtvMenuRights.DataSource = ds.Tables(0)
            rtvMenuRights.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    IEnumerableMenuRights = New HoldMenuRights
                    IEnumerableMenuRights.MNU_CODE = ds.Tables(0).Rows(iIndex)("MNU_CODE")
                    IEnumerableMenuRights.MNU_PARENTID = Convert.ToString(ds.Tables(0).Rows(iIndex)("MNU_PARENTID"))
                    IEnumerableMenuRights.MNU_CODE_RIGHTS_OLD = ds.Tables(0).Rows(iIndex)("MNU_CODE_RIGHTS")
                    IEnumerableMenuRights.MNU_CODE_RIGHTS_NEW = ds.Tables(0).Rows(iIndex)("MNU_CODE_RIGHTS")
                    IEnumerableMenuRights.MNU_MODULE = ds.Tables(0).Rows(iIndex)("MNU_MODULE")
                    IEnumerableMenuRights.MNU_STATUS = "OLD"
                    htMenuRights(Convert.ToString(ds.Tables(0).Rows(iIndex)("MNU_CODE"))) = IEnumerableMenuRights
                Next
            End If
            Session("sMNURIGHTS") = htMenuRights

            LoopNodes(rtvMenuRights.Nodes, True)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoopNodes(ByVal nodes As RadTreeNodeCollection, ByVal enable As Boolean)
        Dim Red As Integer
        Dim Green As Integer
        Dim Blue As Integer
        Dim EmployeeID As String = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        For Each node As RadTreeNode In nodes

            getColor(node.Value, Red, Green, Blue)
            node.ForeColor = System.Drawing.Color.FromArgb(Red, Green, Blue)
            If (EmployeeID = "16450" Or EmployeeID = "9235" Or EmployeeID = "9158" Or EmployeeID = "18984" Or EmployeeID = "44239") Then
                enable = True
            Else
                If (node.Value = "P000000") Then
                    enable = False
                Else
                    enable = True
                End If
            End If
            node.Enabled = enable
            'If node.Checked = True Then

            '    Str += node.Text & " || "
            'End If
            If node.Nodes.Count > 0 Then
                LoopNodes(node.Nodes, enable)
            End If
        Next

    End Sub

    Private Sub getColor(ByVal Mnu_code As String, ByRef red As Integer, ByRef green As Integer, ByRef blue As Integer)
        Dim htTab As Hashtable = Session("sMNURIGHTS")
        Dim vMNURGT As HoldMenuRights
        Dim accessRight As String = String.Empty
        Dim str_color As String = "#f03333"
        vMNURGT = htTab(Mnu_code)
        If Not vMNURGT Is Nothing Then
            accessRight = vMNURGT.MNU_CODE_RIGHTS_NEW

            If (accessRight = "0") Then
                red = 240
                green = 51
                blue = 51
                'str_color = "#f03333"
            ElseIf (accessRight = "1") Then
                red = 0
                green = 0
                blue = 0
                'str_color = "#000000"
            ElseIf (accessRight = "2") Then
                red = 87
                green = 90
                blue = 18

                ' str_color = "#575a12"
            ElseIf (accessRight = "3") Then

                red = 224
                green = 93
                blue = 7
                ' str_color = "#e05d07"
            ElseIf (accessRight = "4") Then
                red = 240
                green = 29
                blue = 166
                'str_color = "#f01da6"
            ElseIf (accessRight = "5") Then
                red = 10
                green = 11
                blue = 255
                ' str_color = "#0a0bff"
            ElseIf (accessRight = "6") Then
                red = 9
                green = 181
                blue = 43
                ' str_color = "#09b52b"

            End If

        End If


    End Sub



    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            Dim htMenuRights As Hashtable
            Dim vmnuRGT As HoldMenuRights

            If Session("sMNURIGHTS") IsNot Nothing Then
                htMenuRights = Session("sMNURIGHTS")
            Else
                lblErrorMessage.Text = "Please reset and do the process again."
                Exit Sub
            End If
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", ddlBsuCopy.SelectedValue)
            PARAM(1) = New SqlParameter("@ROL_ID", ddlRoleCopy.SelectedValue)

            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "MNU.GETMENURIGHTS_TREEBIND", PARAM)
                While datareader.Read

                    vmnuRGT = htMenuRights(Convert.ToString(datareader("MNU_CODE")))
                    If Not vmnuRGT Is Nothing Then
                        If Convert.ToInt64(datareader("MNU_CODE_RIGHTS")) > vmnuRGT.MNU_CODE_RIGHTS_NEW Then

                            vmnuRGT.MNU_CODE_RIGHTS_NEW = Convert.ToInt64(datareader("MNU_CODE_RIGHTS"))

                            vmnuRGT.MNU_STATUS = "UPDATE"
                            htMenuRights(Convert.ToString(datareader("MNU_CODE"))) = vmnuRGT
                        End If

                    End If

                End While
            End Using
            Session("sMNURIGHTS") = htMenuRights

            LoopNodes(rtvMenuRights.Nodes, True)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be updated"
        End Try

    End Sub



    Protected Sub btnAddOpr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOpr.Click
        Try
            ' Dim checkedNodes As TreeNodeCollection = tvmenu.CheckedNodes
            Dim htMenuRights As Hashtable = Session("sMNURIGHTS")
            Dim vMNURGT As HoldMenuRights
            For Each node As RadTreeNode In rtvMenuRights.GetAllNodes
                vMNURGT = htMenuRights(node.Value)
                If (Not vMNURGT Is Nothing) And (node.Checked = True) Then
                    vMNURGT.MNU_CODE_RIGHTS_NEW = ddlOprRight.SelectedValue

                    vMNURGT.MNU_STATUS = "UPDATE"
                    htMenuRights(node.Value) = vMNURGT

                End If
            Next
            Session("sMNURIGHTS") = htMenuRights


            rtvMenuRights.ClearCheckedNodes()
            LoopNodes(rtvMenuRights.Nodes, True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be inserted"
        End Try
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Session("sMNURIGHTS") = Nothing
        initial_State()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Check if the InfoGridView is empty or not to do the transaction
            If Not Session("sMNURIGHTS") Is Nothing Then
                Dim str_err As String
                Dim errorMessage As String = String.Empty
                str_err = CallTransactions(errorMessage)
                If str_err = "0" Then

                    Session("sMNURIGHTS") = Nothing
                    initial_State()
                    lblErrorMessage.Text = "Menu Rights added successfully"
                Else
                    lblErrorMessage.Text = UtilityObj.getErrorMessage(str_err)
                End If
            Else
                lblErrorMessage.Text = "Please reset and do the process again."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Process could not be completed"
        End Try
    End Sub

    Private Function CallTransactions(ByRef ErrorMessage As String) As String
        Dim str_menu As String = String.Empty
        If Session("sMNURIGHTS") IsNot Nothing Then
            Dim tempHash As Hashtable = Session("sMNURIGHTS")
            For Each vMNURGT As HoldMenuRights In tempHash.Values
                If vMNURGT.MNU_CODE_RIGHTS_NEW <> 0 Then
                    str_menu += String.Format("<MENU MNU_CODE='{0}' MNU_RIGHTS='{1}'  MNU_MODULE='{2}' />", _
                                                  vMNURGT.MNU_CODE, vMNURGT.MNU_CODE_RIGHTS_NEW, vMNURGT.MNU_MODULE)
                End If

            Next
        End If

        If str_menu.Trim <> "" Then

            str_menu = "<MENUS>" + str_menu + "</MENUS>"
        Else
            ErrorMessage = "Please reset and do the process again."
            Return "1000"
        End If




        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim StatusFlag As Integer
                Dim param(10) As SqlParameter

                Dim ADD_BSU_ID As New StringBuilder
                For Each item As ListItem In chkBSU_Copy.Items
                    If (item.Selected) Then
                        ADD_BSU_ID.Append(item.Value)
                        ADD_BSU_ID.Append("|")
                    End If
                Next
                param(0) = New SqlParameter("@STR_MENUS", str_menu)
                param(1) = New SqlParameter("@BSU_ID", ddlBsu.SelectedItem.Value)
                param(2) = New SqlParameter("@ROL_ID", ddlRole.SelectedItem.Value)
                param(3) = New SqlParameter("@BSU_ID_COPY", ADD_BSU_ID.ToString)
                param(4) = New SqlParameter("@USR_ID", Session("sUsr_name"))
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "MNU.SAVEMENU_ACCESS_RIGHTS", param)
                StatusFlag = param(5).Value
                If StatusFlag = 0 Then
                    transaction.Commit()
                Else
                    transaction.Rollback()
                End If
                Return StatusFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
                Return "1000"
            End Try
        End Using
    End Function












    'Protected Sub lbTabrights_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbTabrights.Click
    '    Try
    '        Dim encrData As New Encryption64
    '        Dim url As String = String.Empty
    '        Dim menuText As String = "~/Accounts/accAddTabRights.aspx"
    '        Dim mCode As String = encrData.Encrypt("D050027")
    '        Dim datamode As String = encrData.Encrypt("none")
    '        url = String.Format("{0}?MainMnu_code={1}&datamode={2}", menuText, mCode, datamode)
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub



    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BIND_ROLES()
    End Sub
End Class
Public Class HoldMenuRights

    Dim _MNU_CODE As String
    Dim _MNU_PARENTID As String
    Dim _MNU_TEXT As String
    Dim _MNU_CODE_RIGHTS_OLD As Integer
    Dim _MNU_CODE_RIGHTS_NEW As Integer
    Dim _MNU_MODULE As String
    Dim _MNU_STATUS As String
    Public Property MNU_CODE() As String
        Get
            Return _MNU_CODE
        End Get
        Set(ByVal value As String)
            _MNU_CODE = value
        End Set
    End Property

    Public Property MNU_PARENTID() As String
        Get
            Return _MNU_PARENTID
        End Get
        Set(ByVal value As String)
            _MNU_PARENTID = value
        End Set
    End Property

    Public Property MNU_TEXT() As String
        Get
            Return _MNU_TEXT
        End Get
        Set(ByVal value As String)
            _MNU_TEXT = value
        End Set
    End Property

    Public Property MNU_CODE_RIGHTS_OLD() As Integer
        Get
            Return _MNU_CODE_RIGHTS_OLD
        End Get
        Set(ByVal value As Integer)
            _MNU_CODE_RIGHTS_OLD = value
        End Set
    End Property

    Public Property MNU_CODE_RIGHTS_NEW() As Integer
        Get
            Return _MNU_CODE_RIGHTS_NEW
        End Get
        Set(ByVal value As Integer)
            _MNU_CODE_RIGHTS_NEW = value
        End Set
    End Property
    Public Property MNU_MODULE As String
        Get
            Return _MNU_MODULE
        End Get
        Set(ByVal value As String)
            _MNU_MODULE = value
        End Set
    End Property
    Public Property MNU_STATUS As String
        Get
            Return _MNU_STATUS
        End Get
        Set(ByVal value As String)
            _MNU_STATUS = value
        End Set
    End Property
    'Public Sub New(ByVal MNU_CODE As String, ByVal MNU_PARENTID As String, ByVal MNU_TEXT As String, ByVal MNU_CODE_RIGHTS_OLD As Integer, ByVal MNU_CODE_RIGHTS_NEW As Integer, ByVal MNU_STATUS As String)
    '    _MNU_CODE = MNU_CODE
    '    _MNU_PARENTID=MNU_PARENTID
    '    _MNU_TEXT = MNU_TEXT
    '    _MNU_CODE_RIGHTS_OLD = MNU_CODE_RIGHTS_OLD
    '    _MNU_CODE_RIGHTS_NEW = MNU_CODE_RIGHTS_NEW
    '    _MNU_STATUS = MNU_STATUS
    'End Sub
End Class