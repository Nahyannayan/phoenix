
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Accounts_ShowRPTSETUPsub
    Inherits BasePage
    Dim lstrCheck As String
    Dim lstrType As String
    Dim lstrSQL As String
    Dim lstrCode As String
    Dim lstrField1 As String
    Dim lstrField2 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lstrCheck = Request.QueryString("rss_code")
        'lstrType = Request.QueryString("rss_type")
        lstrType = lstrCheck.Split("|")(0)
        lstrCode = lstrCheck.Split("|")(1)
        lstrField1 = "RSB_ID"
        lstrField2 = "RSB_DESCRIPTION"
        'lstrSQL = " SELECT " _
        '& "  RSS_CODE as Code , RSS_DESCR as Description " _
        '& " FROM RPTSETUP_S WHERE RSS_LNE_TYP NOT IN ('H', 'T') and RSM_TYP='" & lstrCheck & "'"
        'lstrSQL = "SELECT RSS.RSS_CODE AS CODE, RSS.RSS_DESCR AS Description " _
        '& " ,case  RSS_RFS_ID when 'CR' THEN 'CR' WHEN 'DR' THEN  'DR' ELSE '' END RSS_RFS_ID" _
        '& " FROM RPTSETUP_S AS RSS INNER JOIN RPTSETUP_M AS RSM ON RSS.RSS_TYP = RSM.RSM_TYP" _
        '& " WHERE     (RSS.RSS_LNE_TYP NOT IN ('H', 'T')) AND (RSM.RSM_TYP = '" & lstrCheck & "')"
        lstrSQL = "SELECT RSB_ID,RSB_DESCRIPTION FROM RPTSETUPSUB_S WHERE RSB_RSS_TYP='" & lstrType & "' AND RSB_RSS_CODE='" & lstrCode & "'"
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                ' h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        'str_Sid_img = h_Selected_menu_3.Value.Split("__")
        'getid2(str_Sid_img(2))
    End Sub


    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try
            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            str_mode = Request.QueryString("mode")
            Response.Write(str_mode)
            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""

            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND  " & lstrField1 & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND  " & lstrField1 & " NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND  " & lstrField1 & " LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND  " & lstrField1 & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND  " & lstrField1 & " LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND  " & lstrField1 & " NOT LIKE '%" & txtSearch.Text & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_name = " AND  " & lstrField2 & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = " AND  " & lstrField2 & " NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND  " & lstrField2 & " LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND  " & lstrField2 & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND  " & lstrField2 & " LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND  " & lstrField2 & " NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSQL & str_filter_code & str_filter_name & "  ORDER BY RSB_ID")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        Dim lblRSS_RFS_ID As New Label
        lblcode = sender.Parent.FindControl("lblCode")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        lblRSS_RFS_ID = sender.Parent.FindControl("lblRSS_RFS_ID")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRSS_RFS_ID.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")


            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRSS_RFS_ID.Text & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblRSS_RFS_ID.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


End Class

