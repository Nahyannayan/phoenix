<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccTreasurytransferUnposting.aspx.vb" Inherits="Accounts_AccTreasurytransferUnposting" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkedBox) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            //var lstrChk = document.getElementById("chkAL").checked; 
            // alert(checkState);

            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], '');
            } checkedBox.checked = 'checked';
        }


        function ToggleSearch() {//alert('1')//;style.display
            if (document.getElementById('tr_search').style.display == 'none')
                document.getElementById('tr_search').style.display = ''
            else
                document.getElementById('tr_search').style.display = 'none'
        }
        function SearchHide() {
            document.getElementById('tr_search').style.display = 'none'
        }

        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 239px; ";
            sFeatures += "dialogHeight: 264px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            switch (txtControl) {
                case 0:
                    url = "../Accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFromDate.ClientID %>').value;
                    break;
                case 1:
                    url = "../Accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTodate.ClientID %>').value;
                    break;
            }
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            switch (txtControl) {
                case 0:
                    document.getElementById('<%=txtFromDate.ClientID %>').value = result;
                    break;
                case 1:
                    document.getElementById('<%=txtToDate.ClientID %>').value = result;
                    break;
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Unpost Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_SelectedId" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Voucher Type </span>
                                    </td>
                                    <td align="left" width="30%">&nbsp;<asp:DropDownList ID="ddVouchertype" runat="server" AutoPostBack="True" CssClass="listbox">
                                        <asp:ListItem
                                            Selected="True" Value="BP">BANK PAYMENT</asp:ListItem>
                                        <asp:ListItem Value="BR">BANK RECEIPT</asp:ListItem>
                                        <asp:ListItem Value="AP">APPROVE</asp:ListItem>
                                    </asp:DropDownList>
                                        <img src="../Images/search_button_over.gif" border="0" onclick="ToggleSearch();" align="absMiddle"></td>

                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr id="tr_search">
                                    <td align="left" colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="10%"><span class="field-label">Doc No</span> </td>
                                                <td align="left" width="15%">
                                                    <asp:TextBox ID="txtDocno" runat="server"></asp:TextBox></td>
                                                <td align="left" width="10%"><span class="field-label">Date</span></td>
                                                <td align="left" width="15%">
                                                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return getDate(550, 310, 0)" /></td>
                                                <td align="center" width="15%">
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" CssClass="field-label" Text="Between" /></td>
                                                <td align="left" width="10%"><span class="field-label">To Date</span></td>
                                                <td align="left" contenteditable="true" width="15%">
                                                    <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return getDate(550, 310, 1)" /></td>
                                                <td colspan="3" width="10%">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" CausesValidation="False" />
                                                    <img src="../Images/close_red.gif" border="0" onclick="SearchHide();"></td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4" width="100%">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Vouchers for Unposting" Width="100%" AllowPaging="True">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:BoundField DataField="DOCNO" HeaderText="Document Number" ReadOnly="True"
                                                    SortExpression="DOCNO" />
                                                <asp:BoundField DataField="ITF_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Document Date"
                                                    HtmlEncode="False" SortExpression="ITF_DATE">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ITF_NARRATION" HeaderText="Narration" />
                                                <asp:BoundField DataField="ITF_CUR_ID" HeaderText="Currency" SortExpression="ITF_CUR_ID">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("ITF_AMOUNT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UnPost" SortExpression="ITF_bPOSTED">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<input id="chkPosted" runat="server" onclick="ChangeAllCheckBoxStates(this)" checked='<%# Bind("ITF_bPOSTED") %>' type="checkbox"
                                                            value='<%# Bind("IRF_ID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IRF_ID" SortExpression="IRF_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("IRF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:RequiredFieldValidator ID="rfvNarration" runat="server" ErrorMessage="Narration Cannot be Empty" ControlToValidate="txtNarration" CssClass="error"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span>
                                    </td>
                                    <td colspan="3" align="left">
                                        <asp:TextBox ID="txtNarration" runat="server" MaxLength="300" TextMode="MultiLine"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>

                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Unpost" /></td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

