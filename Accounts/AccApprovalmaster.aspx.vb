Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_AccApprovalmaster
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then


            Session("datatable") = New DataTable
            Page.Title = OASISConstants.Gemstitle
            txtBsuName.Attributes.Add("readonly", "readonly")
            'txtBusId.Attributes.Add("readonly", "readonly")
            'txtDoctypeId.Attributes.Add("readonly", "readonly")
            'txtDepartmentId.Attributes.Add("readonly", "readonly")
            txtBusId.Style("display") = "none"
            txtDoctypeId.Style("display") = "none"
            txtDepartmentId.Style("display") = "none"

            txtDocType.Attributes.Add("readonly", "readonly")

            txtEmployee.Attributes.Add("readonly", "readonly")
            txtDepartment.Attributes.Add("readonly", "readonly")

            txtAmount.Attributes.Add("onkeypress", "return  Numeric_Only()")
            txtAmount.Attributes.Add("onkeydown", "return CtrlDisable()")
            imgCalendar.Attributes.Add("onclick", "return  getDate('" & txtDDocdate.ClientID & "')")
            ImgBsu.Attributes.Add("onclick", "  getFilter('" & txtBsuName.ClientID & "','" & txtBusId.ClientID & "','BSUNIT'); return false;")
            ImgDoctype.Attributes.Add("onclick", "  getFilter('" & txtDocType.ClientID & "','" & txtDoctypeId.ClientID & "','DOCTYPE'); return false;")
            ImgUser.Attributes.Add("onclick", "  getFilter('" & txtEmployee.ClientID & "','" & h_Emp_No.ClientID & "','BSUEMP'); return false;")
            ImgDepartment.Attributes.Add("onclick", "  getFilter('" & txtDepartment.ClientID & "','" & txtDepartmentId.ClientID & "','DPT'); return false;")
            'onclick ="getFilter('<%=txtBsuName.ClientID%>','<%=txtBusId.ClientID%>','BSUNIT')" style="cursor: hand" />
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            clear_All()
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A100358") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            If Request.QueryString("viewid") <> "" Then
                setViewData()

                'UtilityObj.beforeLoopingControls(Me.Page)
                'Else
                'ResetViewData()
            End If
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")

        End If
    End Sub





    Private Sub setViewData() 'setting controls on view/edit

        Dim sqlQry, selectId As String
        Dim splitArry As String()
        Dim _table As New DataTable

        'Dim ds1 As Decimal = 45678333.3467
        'Dim mmm As String
        'mmm = ds1.ToString("#,###0.00")

        selectId = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        'selectId = Request.QueryString("viewid")
        splitArry = selectId.ToString().Split("_")




        'sqlQry = " SELECT  " _
        '                   & " vw_OSO_APPROVAL_M.APM_ID,       " _
        '                   & " vw_OSO_APPROVAL_M.APM_USR_ID AS UserName,   " _
        '                   & " vw_OSO_BUSINESSUNIT_M.BSU_NAME,   " _
        '                   & " DOCUMENT_M.DOC_NAME,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_ORDERID AS OrderNo,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_bREQPREVAPPROVAL AS PrvApproval,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_bREQPHIGHAPPROVAL AS HgApproval,  " _
        '                   & " vw_OSO_APPROVAL_M.APM_DOC_ID,  " _
        '                   & " vw_OSO_APPROVAL_M.APM_BSU_ID,  " _
        '                   & " APM_LIMIT AS Amount," _
        '                   & " vw_OSO_APPROVAL_M.APM_USR_ID AS EmpId,   " _
        '                   & " APM_DTFROM AS FromDate " _
        '                   & " FROM " _
        '                   & " vw_OSO_APPROVAL_M INNER JOIN  " _
        '                   & " vw_OSO_BUSINESSUNIT_M ON vw_OSO_APPROVAL_M.APM_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN  " _
        '                   & " DOCUMENT_M ON vw_OSO_APPROVAL_M.APM_DOC_ID = DOCUMENT_M.DOC_ID  " _
        '                   & " WHERE APM_DOC_ID = '" & splitArry(0) & "' AND APM_BSU_ID = '" & splitArry(1) & "'" _
        '                   & " AND APM_DPT_ID = '" & splitArry(2) & "' GROUP BY vw_OSO_APPROVAL_M.APM_ID,       " _
        '                   & " vw_OSO_APPROVAL_M.APM_USR_ID ,   " _
        '                   & " vw_OSO_BUSINESSUNIT_M.BSU_NAME,   " _
        '                   & " DOCUMENT_M.DOC_NAME,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_ORDERID,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_bREQPREVAPPROVAL,   " _
        '                   & " vw_OSO_APPROVAL_M.APM_bREQPHIGHAPPROVAL ,  " _
        '                   & " vw_OSO_APPROVAL_M.APM_DOC_ID,  " _
        '                   & " vw_OSO_APPROVAL_M.APM_BSU_ID,  " _
        '                   & " APM_LIMIT," _
        '                   & " vw_OSO_APPROVAL_M.APM_USR_ID,   " _
        '                   & " APM_DTFROM " _
        '                   & " ORDER BY APM_ORDERID"

        sqlQry = " SELECT vw_OSO_APPROVAL_M.APM_ID, vw_OSO_APPROVAL_M.APM_USR_ID AS UserName,  " _
                & " vw_OSO_BUSINESSUNIT_M.BSU_NAME, DOCUMENT_M.DOC_NAME, vw_OSO_APPROVAL_M.APM_ORDERID AS OrderNo,  " _
                & " vw_OSO_APPROVAL_M.APM_bREQPREVAPPROVAL AS PrvApproval, vw_OSO_APPROVAL_M.APM_bREQPHIGHAPPROVAL AS HgApproval, " _
                & " vw_OSO_APPROVAL_M.APM_DOC_ID, vw_OSO_APPROVAL_M.APM_BSU_ID, vw_OSO_APPROVAL_M.APM_LIMIT AS Amount, " _
                & " vw_OSO_APPROVAL_M.APM_USR_ID AS EmpId, vw_OSO_APPROVAL_M.APM_DTFROM AS FromDate,  " _
                & " vw_OSO_APPROVAL_M.APM_DPT_ID, VW_OSO_DEPARTMENT_M.DPT_DESCR " _
                & " FROM  " _
                & " vw_OSO_APPROVAL_M INNER JOIN " _
                & " vw_OSO_BUSINESSUNIT_M ON vw_OSO_APPROVAL_M.APM_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                & " DOCUMENT_M ON vw_OSO_APPROVAL_M.APM_DOC_ID = DOCUMENT_M.DOC_ID INNER JOIN " _
                & " VW_OSO_DEPARTMENT_M ON vw_OSO_APPROVAL_M.APM_DPT_ID = VW_OSO_DEPARTMENT_M.DPT_ID " _
                & " WHERE  " _
                & " vw_OSO_APPROVAL_M.APM_DOC_ID = '" & splitArry(0) & "' AND vw_OSO_APPROVAL_M.APM_BSU_ID = '" & splitArry(1) & "' AND  " _
                & " vw_OSO_APPROVAL_M.APM_DPT_ID = '" & splitArry(2) & "' " _
                & " GROUP BY vw_OSO_APPROVAL_M.APM_ID, vw_OSO_APPROVAL_M.APM_USR_ID, vw_OSO_BUSINESSUNIT_M.BSU_NAME,  " _
                & " DOCUMENT_M.DOC_NAME, vw_OSO_APPROVAL_M.APM_ORDERID, vw_OSO_APPROVAL_M.APM_bREQPREVAPPROVAL,  " _
                & " vw_OSO_APPROVAL_M.APM_bREQPHIGHAPPROVAL, vw_OSO_APPROVAL_M.APM_DOC_ID, vw_OSO_APPROVAL_M.APM_BSU_ID, " _
                & " vw_OSO_APPROVAL_M.APM_LIMIT, vw_OSO_APPROVAL_M.APM_USR_ID, vw_OSO_APPROVAL_M.APM_DTFROM, " _
                & " vw_OSO_APPROVAL_M.APM_DPT_ID, VW_OSO_DEPARTMENT_M.DPT_DESCR " _
                & " ORDER BY OrderNo "


        _table = MainObj.ListRecords(sqlQry, "MainDB")
        gvJournal.DataSource = _table
        gvJournal.DataBind()
        If _table.Rows.Count > 0 Then

            txtDocType.Text = _table.Rows(0)("DOC_NAME")
            txtDoctypeId.Text = _table.Rows(0)("APM_DOC_ID")
            txtBsuName.Text = _table.Rows(0)("BSU_NAME")
            txtBusId.Text = _table.Rows(0)("APM_BSU_ID")
            txtDepartment.Text = _table.Rows(0)("DPT_DESCR")
            txtDepartmentId.Text = _table.Rows(0)("APM_DPT_ID")



            viewId.Value = selectId
            LockUnlock(False)
        End If

        Session("datatable") = _table


    End Sub


    Private Sub ResetViewData() 'resetting controls on view/edit


    End Sub


    Private Sub doInsert(ByVal Mode As String)


        Dim Pslno, Pdate, EmpId, PappPrv, PappHigh, Pamount, dDate As String
        Pslno = ""
        EmpId = ""
        PappPrv = ""
        PappHigh = ""
        Pamount = ""
        dDate = ""
        Pdate = ""


        Dim _table As DataTable = Session("datatable")


        Dim x As Integer
        For x = 1 To _table.Rows.Count
            dDate = Convert.ToDateTime(_table.Rows(x - 1)("FromDate")).ToString("dd-MMM-yyyy")
            If Pslno.Equals("") Then
                Pslno = x.ToString() ' _table.Rows(x - 1)("OrderNo")
                Pdate = dDate
                PappPrv = _table.Rows(x - 1)("PrvApproval")
                PappHigh = _table.Rows(x - 1)("HgApproval")
                Pamount = _table.Rows(x - 1)("Amount")
                EmpId = _table.Rows(x - 1)("EmpId")

            Else
                Pslno += "#" & x.ToString() '_table.Rows(x - 1)("OrderNo")
                Pdate += "#" & dDate
                PappPrv += "#" & _table.Rows(x - 1)("PrvApproval")
                PappHigh += "#" & _table.Rows(x - 1)("HgApproval")
                Pamount += "#" & _table.Rows(x - 1)("Amount")
                EmpId += "#" & _table.Rows(x - 1)("EmpId")

            End If
        Next


        Dim _parameter As String(,) = New String(11, 1) {}


        _parameter(0, 0) = "@APM_DOC_ID"
        _parameter(0, 1) = txtDoctypeId.Text.Trim()
        _parameter(1, 0) = "@APM_BSU_ID"
        _parameter(1, 1) = txtBusId.Text.Trim()
        _parameter(2, 0) = "@APM_ORDERID"
        _parameter(2, 1) = Pslno
        _parameter(3, 0) = "@APM_PRVAPPROVAL"
        _parameter(3, 1) = PappPrv
        _parameter(4, 0) = "@APM_HIGHAPPROVAL"
        _parameter(4, 1) = PappHigh
        _parameter(5, 0) = "@APM_LIMIT"
        _parameter(5, 1) = Pamount
        _parameter(6, 0) = "@APM_DTFROM"
        _parameter(6, 1) = Pdate
        _parameter(7, 0) = "@EMP_ID"
        _parameter(7, 1) = EmpId
        _parameter(8, 0) = "@SLNO"
        _parameter(8, 1) = Pslno
        _parameter(9, 0) = "@MODE"
        _parameter(9, 1) = Mode
        _parameter(10, 0) = "@APM_DPT_ID"
        _parameter(10, 1) = txtDepartmentId.Text.ToString()




        '_parameter(19, 0) = "@AUD_WINUSER"
        '_parameter(19, 1) = Page.User.Identity.Name.ToString()
        '_parameter(20, 0) = "@Aud_form"
        '_parameter(20, 1) = Master.MenuName.ToString()
        '_parameter(21, 0) = "@Aud_user"
        '_parameter(21, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        '_parameter(22, 0) = "@Aud_module"
        '_parameter(22, 1) = HttpContext.Current.Session("sModule")


        MainObj.doExcutive("SaveAPPROVAL_M_NEW", _parameter, "MainDBO", "@v_ReturnMsg")
        lblError.Text = MainObj.MESSAGE
        If MainObj.SPRETVALUE.Equals(0) Then
            clear_All()
        End If


    End Sub



    Private Sub LockUnlock(ByVal locked As Boolean)


        txtDDocdate.Enabled = locked
        txtDocType.Enabled = locked

        txtEmployee.Enabled = locked
        gvJournal.Enabled = locked
        btnInsert.Enabled = locked
        btnClear.Enabled = locked

        txtBsuName.Enabled = locked
        txtDepartment.Enabled = locked
        ImgDoctype.Enabled = locked
        ImgBsu.Enabled = locked
        ImgDepartment.Enabled = locked
        ImgUser.Enabled = locked



    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click



        If gvJournal.Rows.Count.Equals(0) Then
            lblError.Text = "Please Add the Approval details..!"
            Exit Sub
        End If

        If txtDoctypeId.Text.Equals("") Then
            lblError.Text = "Please Select Document  Type..!"
            Exit Sub
        End If


        If txtBusId.Text.Equals("") Then
            lblError.Text = "Please Select Business Unit..!"
            Exit Sub
        End If

        Try


            If ViewState("datamode") = "edit" Then

                'ela_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                doInsert("1")
            Else

                doInsert("0")
            End If
            ViewState("datamode") = "add"

        Catch ex As Exception
            lblError.Text = ex.Message

        End Try

    End Sub





    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            'setViewData()
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ViewState("datamode") = "add"
        clear_All()

        LockUnlock(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Sub clear_All()

        txtDoctypeId.Text = ""
        txtDocType.Text = ""
        txtBusId.Text = ""
        txtBsuName.Text = ""
        txtDepartment.Text = ""
        txtDepartmentId.Text = ""
        txtAmount.Text = ""
        gvJournal.DataSource = Nothing
        gvJournal.DataBind()
        Dim _table As New DataTable
        Session("datatable") = _table
        clearDetails()
        ImgBsu.Enabled = True
        LockUnlock(True)

    End Sub
    Sub clearDetails()
        ' txtAmount.Text = ""
        txtEmployee.Text = ""
        h_Emp_No.Value = ""
        ChkHigher.Checked = False
        ChkPrevious.Checked = False
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        LockUnlock(True)
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub


    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim _table As New DataTable
        _table = Session("datatable")
        Dim hgApp As Boolean = False
        Dim prApp As Boolean = False

        If (_table.Columns.Count.Equals(0)) Then
            _table.Columns.Add("OrderNo", GetType(String))
            _table.Columns.Add("UserName", GetType(String))
            _table.Columns.Add("PrvApproval", GetType(Boolean))
            _table.Columns.Add("HgApproval", GetType(Boolean))
            _table.Columns.Add("FromDate", GetType(String))
            _table.Columns.Add("Amount", GetType(String))
            _table.Columns.Add("EmpId", GetType(String))


        End If

        If ChkHigher.Checked Then
            hgApp = True
        End If
        If ChkPrevious.Checked Then
            prApp = True
        End If
        'If hgApp.Equals(False) And prApp.Equals(False) Then
        '    lblError.Text = "Should be select Higher OR Previous Approval..!"
        '    Exit Sub
        'End If

        Dim x As Integer
        For x = 1 To _table.Rows.Count
            If txtEmployee.Text.Trim().Equals(_table.Rows(x - 1)("UserName")) Then
                lblError.Text = "Particular User Already Added..!" & txtEmployee.Text
                Exit Sub
            End If
        Next

        Dim dtRow As DataRow
        Dim Amnt As Double = Convert.ToDouble(txtAmount.Text.ToString)
        dtRow = _table.NewRow()
        _table.Rows.Add(dtRow)

        _table.Rows(_table.Rows.Count - 1)("OrderNo") = _table.Rows.Count.ToString()
        _table.Rows(_table.Rows.Count - 1)("UserName") = txtEmployee.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("PrvApproval") = prApp
        _table.Rows(_table.Rows.Count - 1)("HgApproval") = hgApp
        _table.Rows(_table.Rows.Count - 1)("FromDate") = txtDDocdate.Text.ToString()
        _table.Rows(_table.Rows.Count - 1)("Amount") = Amnt.ToString("####0.000")
        _table.Rows(_table.Rows.Count - 1)("EmpId") = h_Emp_No.Value.ToString()




        gvJournal.DataSource = _table
        gvJournal.DataBind()
        Session("datatable") = _table
        ImgBsu.Enabled = False
        clearDetails()
    End Sub
    Public Amount As Double = 0
    Public Slno As Double = 1


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)


        If Not (e.Row.RowIndex.Equals(-1)) Then

            e.Row.Attributes.Add("onmouseover", "return  Mouse_Move('" + e.Row.ClientID + "')")
            e.Row.Attributes.Add("onmouseout", "return  Mouse_Out('" + e.Row.ClientID + "')")
            e.Row.Cells(0).Text = Slno.ToString()
            Slno += 1
        End If
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)


        Dim _table As New DataTable
        _table = Session("datatable")

        txtEmployee.Text = _table.Rows(e.RowIndex)("UserName")
        txtDDocdate.Text = _table.Rows(e.RowIndex)("FromDate")
        txtAmount.Text = _table.Rows(e.RowIndex)("Amount")
        h_Emp_No.Value = _table.Rows(e.RowIndex)("EmpId")
        If _table.Rows(e.RowIndex)("PrvApproval").Equals(True) Then
            ChkPrevious.Checked = True

        Else
            ChkPrevious.Checked = False
        End If
        If _table.Rows(e.RowIndex)("HgApproval").Equals(True) Then
            ChkHigher.Checked = True
        Else
            ChkHigher.Checked = False
        End If

        _table.Rows.RemoveAt(e.RowIndex)
        gvJournal.DataSource = _table
        gvJournal.DataBind()
        Session("datatable") = _table
    End Sub



    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear_All()

    End Sub




    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If (chkForward.Checked) Then
        '    lblError.Text = "You Can Delete Only Rejected Documents And Documents Which Are Not Forwaded."
        '    Exit Sub
        'Else
        doInsert("2")
        ViewState("datamode") = "add"


        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'End If
    End Sub

    Protected Sub txtDDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strfDate As String = txtDDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            txtDDocdate.Text = Date.Now.ToString("dd/MMM/yyyy")
            Exit Sub
        Else
            txtDDocdate.Text = strfDate
        End If
    End Sub




End Class

