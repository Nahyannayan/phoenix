Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj

Partial Class manageusers
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim SessionFlag As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = String.Format("~/accounts/accAddEditUser.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))

            gvManageUsers.Attributes.Add("bordercolor", "#1b80b6")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050001") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                gridbind()
            End If
        End If
        set_Menu_Img()
    End Sub
    Private Sub gridbind()
        Try
             
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_Sql As String = String.Empty
        Dim str_filter_User As String = String.Empty
        Dim str_filter_Ename As String = String.Empty
        Dim str_filter_Role As String = String.Empty
        Dim str_filter_BName As String = String.Empty

        Dim str_filter_EStatus As String = String.Empty
        Dim str_filter_Designation As String = String.Empty
        Dim str_filter_UStatus As String = String.Empty
        Dim str_search As String
        Dim str_txtusername As String = String.Empty
        Dim str_txtUserRole As String = String.Empty
        Dim str_txtEmpName As String = String.Empty
        Dim str_txtBusName As String = String.Empty
            Dim ds As New DataSet
        Dim str_txtEmpStatus As String = String.Empty
        Dim str_txtDesignation As String = String.Empty
            Dim str_txtuserStatus As String = String.Empty
            Dim txtSearch As New TextBox
        If gvManageUsers.Rows.Count > 0 Then
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
                str_txtusername = txtSearch.Text
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
                str_txtUserRole = txtSearch.Text

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
                str_txtEmpName = txtSearch.Text

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")
                str_txtBusName = txtSearch.Text

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpStatus")
                str_txtEmpStatus = txtSearch.Text

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtDesignation")
                str_txtDesignation = txtSearch.Text

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserStatus")
                str_txtuserStatus = txtSearch.Text
            End If
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_name", str_txtusername)
            pParms(1) = New SqlClient.SqlParameter("@DESCR", str_txtUserRole)
            pParms(2) = New SqlClient.SqlParameter("@full_name", str_txtEmpName)
            pParms(3) = New SqlClient.SqlParameter("@BSU_NAME", str_txtBusName)
            pParms(4) = New SqlClient.SqlParameter("@EMP_STATUS", str_txtEmpStatus)
            pParms(5) = New SqlClient.SqlParameter("@DESIGNATION", str_txtDesignation)
            pParms(6) = New SqlClient.SqlParameter("@USER_STATUS", str_txtuserStatus)
            'pParms(7) = New SqlClient.SqlParameter("@Login_BSU_ID", Session("sBsuid"))
            pParms(7) = New SqlClient.SqlParameter("@Login_USR_NAME", Session("sUsr_name"))
            ''
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Bind_USR_LOGIN_LINK_ACCESS", pParms)
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
            txtSearch.Text = str_txtusername
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
            txtSearch.Text = str_txtUserRole
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_txtEmpName
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")
            txtSearch.Text = str_txtBusName

            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpStatus")
            txtSearch.Text = str_txtEmpStatus
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtDesignation")
            txtSearch.Text = str_txtDesignation
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserStatus")
            txtSearch.Text = str_txtuserStatus

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub gridbind_old()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = String.Empty
            Dim str_filter_User As String = String.Empty
            Dim str_filter_Ename As String = String.Empty
            Dim str_filter_Role As String = String.Empty
            Dim str_filter_BName As String = String.Empty

            Dim str_filter_EStatus As String = String.Empty
            Dim str_filter_Designation As String = String.Empty
            Dim str_filter_UStatus As String = String.Empty





            str_Sql = " select * from(select users_m.usr_id as usr_id,users_m.usr_name as usr_name ," & _
            " isnull(employee_m.emp_fname,'')+'  '+isnull(employee_m.emp_mname,'')+'  '+isnull(employee_m.emp_lname,'') as Full_Name, " & _
            " users_m.usr_emp_id,users_m.usr_rol_id, roles_m.ROL_DESCR as DESCR  , businessunit_m.BSU_NAME as BSU_NAME ," & _
            " case when emp_status in(1,2) then 'Yes' else 'No' end  EMP_STATUS, isnull(des_descr,'')  DESIGNATION,case when usr_bdisable=0 then 'Yes' else 'No' end USER_STATUS ," & _
            " users_m.usr_bsu_id from users_m join businessunit_m  on users_m.usr_bsu_id=businessunit_m.BSU_ID " & _
            "  join roles_m on users_m.usr_rol_id=roles_m.ROL_ID left outer join vw_OSO_EMPLOYEE_M on " & _
            " users_m.usr_emp_id=vw_OSO_EMPLOYEE_M.EMP_ID left outer join employee_m on  users_m.usr_emp_id=employee_m.emp_id left outer join EMPDESIGNATION_M on employee_m.emp_des_id=des_id )a where  a.usr_id<>''"
            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_txtusername As String = String.Empty
            Dim str_txtUserRole As String = String.Empty
            Dim str_txtEmpName As String = String.Empty
            Dim str_txtBusName As String = String.Empty

            Dim str_txtEmpStatus As String = String.Empty
            Dim str_txtDesignation As String = String.Empty
            Dim str_txtuserStatus As String = String.Empty



            If gvManageUsers.Rows.Count > 0 Then
                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
                str_txtusername = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_User = " AND a.usr_name LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
                str_txtUserRole = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Role = " AND a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Role = "  AND  NOT a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Role = " AND a.DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Role = " AND a.DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Role = " AND a.DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Role = " AND a.DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
                str_txtEmpName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_Ename = " AND a.full_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Ename = "  AND  NOT a.full_name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Ename = " AND a.full_name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Ename = " AND a.full_name  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Ename = " AND a.full_name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Ename = " AND a.full_name NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")
                str_txtBusName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_BName = " AND a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_BName = "  AND  NOT a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_BName = " AND a.BSU_NAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_BName = " AND a.BSU_NAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_BName = " AND a.BSU_NAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_BName = " AND a.BSU_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If


                'Employee Status
                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpStatus")
                str_txtEmpStatus = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_EStatus = " AND a.EMP_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EStatus = "  AND  NOT a.EMP_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EStatus = " AND a.EMP_STATUS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EStatus = " AND a.EMP_STATUS  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EStatus = " AND a.EMP_STATUS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EStatus = " AND a.EMP_STATUS NOT LIKE '%" & txtSearch.Text & "'"
                End If


                'Employee Designation
                str_Sid_search = h_Selected_menu_7.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtDesignation")
                str_txtDesignation = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_Designation = " AND a.DESIGNATION LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Designation = "  AND  NOT a.DESIGNATION LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Designation = " AND a.DESIGNATION  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Designation = " AND a.DESIGNATION  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Designation = " AND a.DESIGNATION LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Designation = " AND a.DESIGNATION NOT LIKE '%" & txtSearch.Text & "'"
                End If




                'User Status
                str_Sid_search = h_Selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserStatus")
                str_txtuserStatus = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_UStatus = " AND a.USER_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_UStatus = "  AND  NOT a.USER_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_UStatus = " AND a.USER_STATUS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_UStatus = " AND a.USER_STATUS  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_UStatus = " AND a.USER_STATUS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_UStatus = " AND a.USER_STATUS NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_User & str_filter_Role & str_filter_BName & str_filter_Ename & str_filter_EStatus & str_filter_Designation & str_filter_UStatus & " order by usr_name")
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
            txtSearch.Text = str_txtusername
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
            txtSearch.Text = str_txtUserRole
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_txtEmpName
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtBusName")
            txtSearch.Text = str_txtBusName

            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpStatus")
            txtSearch.Text = str_txtEmpStatus
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtDesignation")
            txtSearch.Text = str_txtDesignation
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserStatus")
            txtSearch.Text = str_txtuserStatus

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvManageUsers.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvManageUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvManageUsers.RowCommand
        
        If (e.CommandName = "Login") Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvManageUsers.Rows(index), GridViewRow)

            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            getSelectedUserdetails(Eid)
        End If

        If e.CommandName = "Select" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvManageUsers.Rows(index), GridViewRow)

            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            Dim url As String


            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~/accounts/accAddEditUser.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Eid)
            Response.Redirect(url)

        End If

    End Sub

    Protected Sub btnSearchUserName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchUserRole_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchBusUnit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchEmpStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchDesignation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchUserStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvManageUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUsers.PageIndexChanging
        gvManageUsers.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

    ' Written by: Mahesh Chikhale
    ' Date : 8/4/2019
    ' Use: It will manage to show login link if user is admin other wise hide whole column from user
    Protected Sub OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cell As TableCell = e.Row.Cells(7)
            Dim Status As String = cell.Text

            Dim LoginCell As TableCell = e.Row.Cells(9)

            Dim label As HiddenField = CType(e.Row.FindControl("hdnShowLogin"), HiddenField)
            If label.Value = "0" Then
                LoginCell.Text = ""
            End If
            ''added by nahyan to enable login other users
            Dim isAccess As String = isSuperLoginAccess()
            If (isAccess = "0") Then
                '' If Session("sroleid") <> "204" AndAlso Session("sroleid") <> "1" AndAlso Session("sroleid") <> "487" Then
                'e.Row.Cells(9).Visible = False
                gvManageUsers.Columns(9).Visible = False

            End If
        End If
    End Sub

    Private Function isSuperLoginAccess() As String
        Dim username = Convert.ToString(HttpContext.Current.Session("sUsr_name"))
        Dim parameters As SqlParameter() = New SqlParameter(2) {}
        parameters(0) = New SqlParameter("@usr_name", username)
        Dim usraccess As String = String.Empty
        Try
            usraccess = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.is_SuperLogin_Access", parameters)
            Return usraccess
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    'Written By : Mahesh Chikhale
    'Date : 8/4/2019
    'Use: these 3  function (getSelectedUserdetails,GetUserDetails,getSettings) will fetch session details for user need to log in 
    ' It is same as normal login functionality works but without password

    Public Sub getSelectedUserdetails(Username As String)
        Session.Add("sUsr_nameActual", Convert.ToString(HttpContext.Current.Session("sUsr_name")))
        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Username)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "Get_USR_NAME_ACTIVE", pParms)
            While reader.Read
                Session("sUsr_name") = Username
                Dim lstrActive = Convert.ToString(reader("CURRSTATUS"))
                If lstrActive = "EXPIRED" Then
                    ' Show messge if expired
                    Exit Sub
                End If
            End While
        End Using


        Dim tblusername As String = ""
        Dim tblpassword As String = ""
        Dim tblroleid As Integer = 0
        Dim tblbsuper As Boolean = False
        Dim tblbITSupport As Boolean = False
        Dim tblbsuid As String = ""
        Dim tblbUsr_id As String = ""
        Dim tblDisplay_usr As String = ""
        Dim bCount As Integer = 0
        Dim var_F_Year As String = ""
        Dim var_F_Year_ID As String = ""
        Dim var_F_Date As String = ""
        Dim bUSR_MIS As Integer = 0
        Dim USR_bShowMISonLogin As Integer = 0
        Dim tblModuleAccess As String = ""
        Dim USR_FCM_ID As String = ""
        Dim tblReqRole As String = ""
        Dim tblDays As Integer = 0

        Dim tblBSU_Name As String = ""
        Dim tblEMPID As String = ""
        Dim tblCOSTEMP As String = ""
        Dim tblCOSTSTU As String = ""
        Dim tblCOSTOTH As String = ""
        Dim tbldef_module As String = ""
        Dim tblUSR_ForcePwdChange As Integer = 0
        Dim passwordEncr As New Encryption64
        Try
            Dim dt As New DataTable

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = GetUserDetails(Username, dt, stTrans)
                stTrans.Commit()

                If retval = "0" Then
                    If dt.Rows.Count > 0 Then
                        tblusername = Convert.ToString(dt.Rows(0)("usr_name"))
                        tblDisplay_usr = Convert.ToString(dt.Rows(0)("Display_Name"))
                        If Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")) <> "" Then
                            tblpassword = Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")).Split(New Char() {"."c})(1)
                        Else
                            tblpassword = passwordEncr.Decrypt(Convert.ToString(dt.Rows(0)("usr_password")).Replace(" ", "+"))
                        End If

                        ''  tblpassword = Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")).Split(New Char() {"."c})(1)
                        tblroleid = Convert.ToInt32(dt.Rows(0)("usr_rol_id"))
                        tblbsuper = IIf(IsDBNull(dt.Rows(0)("usr_bsuper")), False, dt.Rows(0)("usr_bsuper"))
                        tblbITSupport = IIf(IsDBNull(dt.Rows(0)("USR_bITSupport")), False, dt.Rows(0)("USR_bITSupport"))
                        tblbsuid = Convert.ToString(dt.Rows(0)("usr_bsu_id"))
                        tblbUsr_id = Convert.ToString(dt.Rows(0)("usr_id"))
                        tblModuleAccess = Convert.ToString(dt.Rows(0)("USR_MODULEACCESS"))
                        tblReqRole = Convert.ToString(dt.Rows(0)("USR_REQROLE"))
                        tblEMPID = Convert.ToString(dt.Rows(0)("USR_EMP_ID"))
                        USR_FCM_ID = Convert.ToString(dt.Rows(0)("USR_FCM_ID"))
                        tblBSU_Name = Convert.ToString(dt.Rows(0)("BSU_NAME"))
                        tblCOSTEMP = dt.Rows(0)("COSTcenterEMP").ToString()
                        tblCOSTSTU = dt.Rows(0)("COSTcenterSTU").ToString()
                        tblCOSTOTH = dt.Rows(0)("COSTcenterOTH").ToString()
                        tbldef_module = dt.Rows(0)("USR_MODULEACCESS").ToString()
                        tblUSR_ForcePwdChange = dt.Rows(0)("USR_ForcePwdChange")
                        If IsDBNull(dt.Rows(0)("USR_MIS")) Then
                            bUSR_MIS = 0
                        Else
                            bUSR_MIS = Convert.ToInt32(dt.Rows(0)("USR_MIS"))
                        End If
                        If IsDBNull(dt.Rows(0)("USR_bShowMISonLogin")) Then
                            USR_bShowMISonLogin = 0
                        Else
                            USR_bShowMISonLogin = Convert.ToInt32(dt.Rows(0)("USR_bShowMISonLogin"))
                        End If

                        tblDays = CInt(dt.Rows(0)("Days"))
                    End If
                ElseIf retval = "560" Then

                    Session("sUsr_name") = Username
                    hfExpired.Value = "expired"
                    'DisableUserLogin()
                    Exit Sub
                ElseIf retval = "2600" Then
                    Dim RedirectUsername As String = Username

                    Dim LogTime As String = Now.ToString("dd-MMM-yyyyHH")
                    Dim RedirectQueryStr As String
                    'RedirectQueryStr = RedirectUsername & "|" & RedirectPassword & "|" & LogTime
                    'RedirectQueryStr = passwordEncr.Encrypt(RedirectQueryStr)
                    'Dim URL As String
                    'URL = WebConfigurationManager.AppSettings("OldOasisURL") & "/loginRedirect.aspx?OASISLogin=" & RedirectQueryStr
                    'Response.Redirect(URL, False)
                Else
                    lblResult.Text = getErrorMessage(retval)

                End If
            Catch ex As Exception
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try


        Catch ex As Exception

        End Try

        Session("sUsr_id") = tblbUsr_id
        'to used for all the page
        Session("sUsr_name") = tblusername
        Session("sUsr_Display_Name") = tblDisplay_usr
        Session("sroleid") = tblroleid
        Session("sBusper") = tblbsuper
        Session("sBsuid") = tblbsuid
        Session("sBITSupport") = tblbITSupport
        Session("BSU_Name") = tblBSU_Name
        Session("sModuleAccess") = tblModuleAccess
        Session("sReqRole") = tblReqRole
        Session("EmployeeId") = tblEMPID
        Session("counterId") = USR_FCM_ID

        Session("CostEMP") = tblCOSTEMP
        Session("CostSTU") = tblCOSTSTU
        Session("CostOTH") = tblCOSTOTH
        Session("sModule") = tbldef_module
        'insert the session id of the cuurently logged in user
        Session("Sub_ID") = "007"

        Using RoleReader As SqlDataReader = AccessRoleUser.GetRoles_modules(tblroleid)
            While RoleReader.Read
                Session("ROL_MODULE_ACCESS") = Convert.ToString(RoleReader("ROL_MODULE_ACCESS"))
            End While
        End Using

        Using collectReader As SqlDataReader = AccessRoleUser.getCollectBank(tblbsuid)
            While collectReader.Read
                Session("CollectBank") = Convert.ToString(collectReader("CollectBank"))
                Session("Collect_name") = Convert.ToString(collectReader("Collect_name"))
            End While
        End Using
        Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M_ID(tblbsuid)
            While CurriculumReader.Read
                Session("CLM") = Convert.ToString(CurriculumReader("BSU_CLM_ID"))
            End While
        End Using
        SessionFlag = AccessRoleUser.UpdateSessionID(tblusername, Session.SessionID)
        If SessionFlag <> 0 Then
            Throw New ArgumentException("Unable to track your request")
        End If

        Using UserreaderFINANCIALYEAR As SqlDataReader = AccessRoleUser.GetFINANCIALYEAR()
            While UserreaderFINANCIALYEAR.Read
                'handle the null value returned from the reader incase  convert.tostring
                var_F_Year_ID = Convert.ToString(UserreaderFINANCIALYEAR("FYR_ID"))
                var_F_Year = Convert.ToString(UserreaderFINANCIALYEAR("FYR_Descr"))
                var_F_Date = Convert.ToString(UserreaderFINANCIALYEAR("FYR_TODT"))
            End While
            'clear of the resource end using
        End Using

        Session("F_YEAR") = var_F_Year_ID
        Session("F_Descr") = var_F_Year
        Session("F_TODT") = Format(var_F_Date, OASISConstants.DateFormat)

        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("UsrName") = Username

        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
        If tblDays <= 0 Then
            hfExpired.Value = "expired"
            Exit Sub
        Else
            hfExpired.Value = ""
        End If

        Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
        If auditFlag <> 0 Then
            Throw New ArgumentException("Unable to track your request")
        End If
        'if super admin allow all businessunit access
        Session("USR_MIS") = bUSR_MIS
        Session("USR_bShowMISonLogin") = USR_bShowMISonLogin

        If USR_bShowMISonLogin = 1 Then
            Response.Redirect("~/Management/empManagementMain.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()

            Exit Sub
        ElseIf USR_bShowMISonLogin = 2 Then
            Response.Redirect("~/Management/empManagementPrincipal.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()

            Exit Sub
        End If
        If tblbsuper = True Then

            ' Response.Redirect("BusinessUnit.aspx")
            Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                While BSUInformation.Read
                    Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                    Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                    Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                    Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                    Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                    Session("BSU_bVATEnabled") = Convert.ToBoolean(BSUInformation("BSU_bVATEnabled"))
                End While
            End Using
            Session("tot_bsu_count") = "1"
            getSettings(Session("sBsuid"))
            Session.Add("isChildLogIn", True)

            Response.Redirect("modulelogin.aspx", False)
            'HttpContext.Current.ApplicationInstance.CompleteRequest()
            'm_bIsTerminating = True
            Exit Sub

        Else
            'if user with single business unit direct the user to module login page else
            'allow the user to select the business unit
            Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                While BUnitreader.Read
                    bCount += 1
                    If bCount = 1 Then
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                        Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Session("F_YEAR"), BUnitreader("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Month(Date.Today), BUnitreader("BSU_PAYMONTH")))
                        Session("BSU_bVATEnabled") = Convert.ToBoolean(BUnitreader("BSU_bVATEnabled"))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BUnitreader("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BUnitreader("BSU_IsHROnDAX"))
                    End If
                End While
            End Using

            Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                While BSUInformation.Read
                    Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                    Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                    Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                    Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                    Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                End While
            End Using
        End If
        Session.Add("isLoggedinChild", True)
        getSettings(Session("sBsuid"))
        Response.Redirect("~/Modulelogin.aspx", False)
        HttpContext.Current.ApplicationInstance.CompleteRequest()

        Exit Sub

    End Sub

    Public Shared Function GetUserDetails(ByVal p_USR_NAME As String, ByRef dtUserData As DataTable, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Try

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "GetUserDetails_byUserName", pParms)

            If pParms(1).Value = "0" Then
                If ds.Tables.Count > 0 Then
                    dtUserData = ds.Tables(0)

                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
        Return GetUserDetails = 1
    End Function


    Private Sub getSettings(ByVal BusUnit As String)
        Try
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(BusUnit)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BSUInformation("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BSUInformation("BSU_IsHROnDAX"))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If
            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub
End Class
