﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAdjJournalView.aspx.vb" Inherits="Accounts_accAdjJournalView" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem, checkbox_checked_status;
            var chk_name = master_box.id.replace("Selectall", "");
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && (curr_elem.id.indexOf(chk_name) > 0))
                    curr_elem.checked = master_box.checked;
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Adjustment Journal
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>

                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr id="tr_SelBusinessUnit" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Select Business Unit </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True"
                                            DataSourceID="sdsBusinessunit" DataTextField="BSU_NAME" DataValueField="BSU_ID"
                                            OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                            SelectCommand="select BSU_ID , BSU_NAME from [fn_GetBusinessUnits]   (@Usr_name) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME
">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="Usr_name" SessionField="sUsr_name" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                            EmptyDataText="No Cash Receipt Vouchers for posting" Width="100%"
                                            AllowPaging="True" CssClass="table table-bordered table-row" PageSize="20">
                                            <Columns>
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="Unit"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Doc. No.">
                                                    <HeaderTemplate>
                                                        Doc. No.<br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("JAL_DOCNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doc Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("JAL_DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document Date">
                                                    <HeaderTemplate>
                                                        To Unit<br />
                                                        <asp:TextBox ID="txtOPPBSU_NAME" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocDateSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("OPPBSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Narration">
                                                    <HeaderTemplate>
                                                        Narration<br />
                                                        <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnNarrationSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("JAL_NARRATION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Chq. No.">
                                                    <HeaderTemplate>
                                                        Debit<br />
                                                        <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnChqNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("JAL_DEBIT", "{0:0.00}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CREDIT">
                                                    <HeaderTemplate>
                                                        Credit<br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAmountSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("JAL_CREDIT", "{0:0.00}") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <HeaderTemplate>
                                                        Account<br />
                                                        <asp:TextBox ID="txtAccount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAccountSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Account") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OppAccount">
                                                    <HeaderTemplate>
                                                        OppAccount<br />
                                                        <asp:TextBox ID="txtOppAccount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnOppAccountSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("OppAccount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>



