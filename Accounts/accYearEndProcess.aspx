<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accYearEndProcess.aspx.vb" Inherits="Accounts_accYearEndProcess" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrCheckList.ascx" TagName="usrCheckList" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            if (mode == 1)
                document.getElementById('<%=txtTo.ClientID %>').value = result;
            return true;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Year End Process
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Business Unit </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Mode </span></td>
                                    <td align="left" width="30%">
                                        <asp:RadioButton ID="rbSoftClose" runat="server" Checked="True" GroupName="Close"
                                            Text="Soft Close" AutoPostBack="True" />
                                        <asp:RadioButton ID="rbHardClose" runat="server" GroupName="Close" Text="Hard Close" AutoPostBack="True" /></td>

                                    <td align="left" width="20%"><span class="field-label">New Freeze Date </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Exchange Rate </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtExgrate" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <uc1:usrCheckList ID="UsrCheckList1" runat="server" ProcessType="YEAREND" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:CheckBox ID="chkCondition1" CssClass="field-label" runat="server" Text="I have passed all necessary vouchers for the current financial year " />
                                        <br />
                                        <asp:CheckBox ID="chkCondition2" CssClass="field-label" runat="server" Text="I have posted all vouchers pending for posting for the current financial year" />
                                        <br />
                                        <asp:CheckBox ID="chkCondition3" CssClass="field-label" runat="server" Text="I have verified all the ledgers and trial balance as on the given date" />
                                        <br />
                                        <asp:CheckBox ID="chkCondition4" CssClass="field-label" runat="server" Text="I understand that I cannot pass any more vouchers in this financial year" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" TabIndex="32" Text="Print" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

