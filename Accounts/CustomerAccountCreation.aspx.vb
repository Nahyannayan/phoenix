﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports Telerik.Web.UI
Partial Class Accounts_CustomerAccountCreation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    'get the data mode from the query string to check if in add or edit mode 
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    'get the menucode to confirm the user is accessing the valid page
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle
                    Clear()
                    GetCountryDtls(ddlCountry)
                    PopulateTree()

                    If Request.QueryString("viewid") Is Nothing Then
                        ViewState("EntryId") = "0"
                    Else
                        ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    End If

                    If ViewState("datamode") = "add" Then
                        hfCus_Act_Code.Value = "0"
                    Else

                        hfCus_Act_Code.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        txtDAXCustmerCode.Text = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))



                        GetCustomerAcctDtls(hfCus_Act_Code.Value)
                        DisabledControls()
                    End If
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Page Load: " + ex.Message, "CustomerAccountCreation")
            End Try
        Else

        End If
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Public Sub GetCountryDtls(ByRef ddl As DropDownList)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "[dbo].[GetCountries]")
            ddl.DataSource = ds.Tables(0)
            ddl.DataTextField = "CTY_NAME"
            ddl.DataValueField = "CTY_ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("SELECT", 0))
            'ddl.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetCountryDtls: " + ex.Message, "CustomerAccountCreation")
        End Try
    End Sub

    Public Sub GetCityDtls(ByRef ddl As DropDownList, ByVal CTY_ID As String)
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CIT_CTY_ID", CTY_ID, SqlDbType.VarChar)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[dbo].[GetCityByCountryID]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                ddl.DataSource = dsData.Tables(0)
                ddl.DataTextField = "CIT_DESCR"
                ddl.DataValueField = "CIT_ID"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("SELECT", 0))
                'ddl.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetCityDtls: " + ex.Message, "CustomerAccountCreation")
        End Try
    End Sub

    Private Function PopulateBusinessUnits() As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", "", SqlDbType.VarChar)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[dbo].[GET_DAX_BUSINESSUNITS]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateBusinessUnits" + ex.Message, "CustomerAccountCreation")
        End Try
    End Function

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSINESSUNIT_M BSU order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub
    Private Sub PopulateNodes(ByVal dt As DataTable,
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            If Not dr("childnodecount") Is Nothing Then
                tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
            End If
        Next
    End Sub
    Private Sub PopulateSubNodes(ByVal dt As DataTable,
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.ExpandAll()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, ByVal parentNode As TreeNode)
        Dim dtTable As DataTable = PopulateBusinessUnits()
        PopulateSubNodes(dtTable, parentNode.ChildNodes)
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If ddlCountry.SelectedItem.Value <> "" AndAlso ddlCountry.SelectedItem.Value <> 0 Then
                If ddlCountry.SelectedItem.Value = "172" Then
                    spanMandatory.Visible = True
                Else spanMandatory.Visible = False
                End If
                GetCityDtls(ddlCity, ddlCountry.SelectedItem.Value)
            Else ddlCity.Items.Clear()
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ddlCountry_SelectedIndexChanged" + ex.Message, "CustomerAccountCreation")
        End Try
    End Sub
    Protected Sub txtDAXCustmerCode_TextChanged(sender As Object, e As EventArgs)
        If txtDAXCustmerCode.Text.Trim() <> "" Then
            GetCustomerAcctDtls(txtDAXCustmerCode.Text.Trim())
        End If
    End Sub

    Public Sub GetCustomerAcctDtls(ByVal CUS_ACT_CODE As String)
        Try
            PopulateTree()
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CUS_ACT_CODE", CUS_ACT_CODE, SqlDbType.VarChar)
            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                Dim sql_query As String = "[DAX].[SP_GET_CUSTOMER_ACCOUNT_DETAILS]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            'Clear()
            If Not dsData Is Nothing Then
                If dsData.Tables(0).Rows.Count > 0 Then
                    txtPhnxAcctCode.Text = dsData.Tables(0).Rows(0)("ACT_ID").ToString().Trim()
                    txtPhnxAcctName.Text = dsData.Tables(0).Rows(0)("ACT_NAME").ToString().Trim()
                    txtEmail.Text = dsData.Tables(0).Rows(0)("ACT_EMAIL").ToString().Trim()
                    txtTelephone.Text = dsData.Tables(0).Rows(0)("ACT_PHONE").ToString().Trim()
                    txtAddress.InnerText = dsData.Tables(0).Rows(0)("ACT_ADDRESS1").ToString().Trim()
                    txtTAXRegNo.Text = dsData.Tables(0).Rows(0)("ACT_TAX_REG").ToString().Trim()
                    txtCreditDays.Text = dsData.Tables(0).Rows(0)("ACT_CREDITDAYS").ToString().Trim()
                    'If Not dsData.Tables(0).Rows(0)("ACT_bINTER_UNIT") Is DBNull.Value Then
                    '    chkbInterUnit.Checked = Convert.ToBoolean(dsData.Tables(0).Rows(0)("ACT_bINTER_UNIT"))
                    'End If
                    chkbInterUnit.Checked = IIf(dsData.Tables(0).Rows(0)("ACT_bINTER_UNIT") Is DBNull.Value, False, dsData.Tables(0).Rows(0)("ACT_bINTER_UNIT"))
                    chkbLeaseCust.Checked = IIf(dsData.Tables(0).Rows(0)("ACT_bLEASER_CUSTOMER") Is DBNull.Value, False, dsData.Tables(0).Rows(0)("ACT_bLEASER_CUSTOMER"))
                    chkbBaseRateList.Checked = IIf(dsData.Tables(0).Rows(0)("ACT_bBASE_RATE_LIST") Is DBNull.Value, False, dsData.Tables(0).Rows(0)("ACT_bBASE_RATE_LIST"))
                    ddlCountry.SelectedValue = dsData.Tables(0).Rows(0)("ACT_COUNTRY").ToString().Trim()
                    If ddlCountry.SelectedItem.Value <> "" AndAlso ddlCountry.SelectedItem.Value <> 0 Then
                        GetCityDtls(ddlCity, ddlCountry.SelectedItem.Value)
                        If (ddlCity.Items.Count > 1) Then
                            ddlCity.SelectedValue = dsData.Tables(0).Rows(0)("ACT_EMR_CODE").ToString().Trim()
                        End If
                    End If
                End If
                If dsData.Tables(1).Rows.Count > 0 Then
                    Dim dt As DataTable = dsData.Tables(1)
                    For Each node As TreeNode In tvBusinessunit.Nodes
                        For Each node2 As TreeNode In node.ChildNodes
                            If node.ChildNodes.Count = dt.Rows.Count Then
                                node.Checked = True
                            End If
                            For Each drow As DataRow In dt.Rows
                                If node2.Value = drow("BSU_ID") Then
                                    node2.Checked = True
                                    tvBusinessunit.ExpandAll()
                                    Exit For
                                Else node2.Checked = False
                                End If
                            Next
                        Next
                    Next
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetCityDtls: " + ex.Message, "CustomerAccountCreation")
        End Try
    End Sub
    Private Sub Clear()
        'txtDAXCustmerCode.Text = ""
        txtPhnxAcctCode.Text = ""
        txtPhnxAcctName.Text = ""
        txtEmail.Text = ""
        txtTelephone.Text = ""
        txtTAXRegNo.Text = ""
        txtAddress.InnerText = ""
        ddlCountry.Items.Clear()
        GetCountryDtls(ddlCountry)
        ddlCity.Items.Clear()
        lblError.Text = ""
        hfCus_Act_Code.Value = "0"
        txtCreditDays.Text = ""
        chkbBaseRateList.Checked = False
        chkbInterUnit.Checked = False
        chkbLeaseCust.Checked = False
    End Sub
    Private Sub DisabledControls()
        txtDAXCustmerCode.Enabled = False
        txtAddress.Disabled = True
        txtPhnxAcctCode.Enabled = False
        txtPhnxAcctName.Enabled = False
        txtEmail.Enabled = False
        txtTelephone.Enabled = False
        txtTAXRegNo.Enabled = False
        ddlCountry.Enabled = False
        ddlCity.Enabled = False
        tvBusinessunit.Enabled = False
        txtCreditDays.Enabled = False
        chkbBaseRateList.Enabled = False
        chkbInterUnit.Enabled = False
        chkbLeaseCust.Enabled = False
    End Sub
    Private Sub EnabledControls()
        txtDAXCustmerCode.Enabled = True
        txtAddress.Disabled = False
        txtPhnxAcctName.Enabled = True
        txtEmail.Enabled = True
        txtTelephone.Enabled = True
        txtTAXRegNo.Enabled = True
        ddlCountry.Enabled = True
        ddlCity.Enabled = True
        tvBusinessunit.Enabled = True
        txtCreditDays.Enabled = True
        chkbBaseRateList.Enabled = True
        chkbInterUnit.Enabled = True
        chkbLeaseCust.Enabled = True
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim validationsPass As Boolean = True
            If ((txtDAXCustmerCode.Text = "" Or txtDAXCustmerCode.Text Is Nothing) Or
                    (txtPhnxAcctName.Text Is Nothing Or txtPhnxAcctName.Text = "")) Then
                ' lblError.Text = "Please fill all mandatory fields"
                '(txtPhnxAcctCode.Text Is Nothing Or txtPhnxAcctCode.Text = "") Or
                Message = getErrorMessage("4029")
                ShowMessage(Message, True)
                validationsPass = False
            ElseIf ddlCountry.SelectedItem.Text = "SELECT" Then
                ' Showing Error Message : Please select Activity Type
                Message = "Please Select Country"
                ShowMessage(Message, True)
                validationsPass = False
            ElseIf ddlCity.SelectedItem.Text = "SELECT" Then
                ' Showing Error Message : Please select Activity Type
                If ddlCountry.SelectedItem.Value = "172" Then
                    Message = "Please Select Emirate/City"
                    ShowMessage(Message, True)
                    validationsPass = False
                End If
            End If

            Dim strBusinessUnits As String = ""
            For Each node As TreeNode In tvBusinessunit.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If node.Value <> "ALL" Then
                    strBusinessUnits += node.Value & ","
                End If
            Next
            If strBusinessUnits = "" Then
                Message = "Please Select Business Units(s)"
                ShowMessage(Message, True)
                validationsPass = False
            End If
            If txtEmail.Text.Trim() <> "" Then
                Dim pattern As String
                pattern = "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"

                If Regex.IsMatch(txtEmail.Text.Trim(), pattern) Then

                Else
                    Message = "Please enter valid Email"
                    ShowMessage(Message, False)
                    validationsPass = False
                End If
            End If
            If validationsPass = True Then
                Dim param(15) As SqlClient.SqlParameter
                strans = objConn.BeginTransaction
                param(0) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar)
                param(0).Value = "CUSTOMER"
                param(1) = New SqlClient.SqlParameter("@BSU_IDS", SqlDbType.VarChar)
                param(1).Value = strBusinessUnits
                param(2) = New SqlClient.SqlParameter("@OASIS_ACT_ID", SqlDbType.VarChar)
                'If txtPhnxAcctCode.Text.Trim() <> "" Then
                param(2).Value = txtPhnxAcctCode.Text.Trim()
                param(2).Direction = ParameterDirection.InputOutput

                'Else param(2).Direction = ParameterDirection.ReturnValue
                'End If

                param(3) = New SqlClient.SqlParameter("@NEW_ACT_DESCR", SqlDbType.VarChar)
                param(3).Value = txtPhnxAcctName.Text.Trim()
                param(4) = New SqlClient.SqlParameter("@AX_CUSTOMER_ACT_CODE", SqlDbType.VarChar)
                param(4).Value = txtDAXCustmerCode.Text.Trim()
                param(5) = New SqlClient.SqlParameter("@EMAIL", SqlDbType.VarChar)
                param(5).Value = txtEmail.Text.Trim()
                param(6) = New SqlClient.SqlParameter("@TRN_NO", SqlDbType.VarChar)
                param(6).Value = txtTAXRegNo.Text.Trim()
                param(7) = New SqlClient.SqlParameter("@ADDRESS", SqlDbType.VarChar)
                param(7).Value = txtAddress.InnerText.Trim()
                param(8) = New SqlClient.SqlParameter("@EMIRATE", SqlDbType.VarChar)
                param(8).Value = ddlCity.SelectedItem.Value
                param(9) = New SqlClient.SqlParameter("@TELEPHONE", SqlDbType.VarChar)
                param(9).Value = txtTelephone.Text.Trim()
                param(10) = New SqlClient.SqlParameter("@COUNTRY_ID", SqlDbType.Int)
                param(10).Value = Convert.ToInt32(ddlCountry.SelectedItem.Value)
                param(11) = New SqlClient.SqlParameter("@INTER_UNIT", SqlDbType.Bit)
                param(11).Value = IIf(chkbInterUnit.Checked = True, True, False)
                param(12) = New SqlClient.SqlParameter("@LEASE_CUSTOMER", SqlDbType.Bit)
                param(12).Value = IIf(chkbLeaseCust.Checked = True, True, False)
                param(13) = New SqlClient.SqlParameter("@BASE_RATE_LIST", SqlDbType.Bit)
                param(13).Value = IIf(chkbBaseRateList.Checked = True, True, False)
                param(14) = New SqlClient.SqlParameter("@CREDITDAYS", SqlDbType.Int)
                Dim creditDay As Integer = 0
                If txtCreditDays.Text <> "" Then
                    creditDay = Convert.ToInt32(txtCreditDays.Text)
                End If
                param(14).Value = creditDay

                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[DAX].[SP_CREATE_CUSTOMER_VENDOR_AND_MAPPING]", param)
                If param(2).Value <> "" Then
                    strans.Commit()
                    If txtPhnxAcctCode.Text = "" Then
                        Message = "Record saved successfully."
                    Else Message = "Record updated successfully."
                    End If
                    txtDAXCustmerCode.Text = ""
                    Clear()
                    ShowMessage(Message, False)
                Else
                    strans.Rollback()
                    Message = getErrorMessage("4000")
                    ShowMessage(Message, True)
                End If
            End If

        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "CustomerAccountCreation")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            txtDAXCustmerCode.Text = ""
            Clear()
            EnabledControls()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            If hfCus_Act_Code.Value = "0" Then
                lblError.Text = "No records to edit"
                Exit Sub
            End If
            EnabledControls()
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Try
            hfCus_Act_Code.Value = "0"
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                Clear()
                DisabledControls()
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
