<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" EnableEventValidation="false"
    CodeFile="acccrCashReceipt.aspx.vb" Inherits="Accounts_acccrCashReceipt" Title="Cash Receipt" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>

    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

        <script language="javascript" type="text/javascript">
            function expandcollapse(obj, row) {
                var div = document.getElementById(obj);
                var img = document.getElementById('img' + obj);

                if (div.style.display == "none") {
                    div.style.display = "block";
                    if (row == 'alt') {
                        img.src = "../images/Misc/minus.gif";
                    }
                    else {
                        img.src = "../images/Misc/minus.gif";
                    }
                    img.alt = "Close to view other Customers";
                }
                else {
                    div.style.display = "none";
                    if (row == 'alt') {
                        img.src = "../images/Misc/plus.gif";
                    }
                    else {
                        img.src = "../images/Misc/plus.gif";
                    }
                    img.alt = "Expand to show Orders";
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function getCashcode() {

                var NameandCode;
                var result;
                result = radopen("acccpShowCashflow.aspx?rss=1", "pop_up");
              <%--  if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=txtDCashflowcode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtDCashflowname.ClientID %>').value = NameandCode[1];

         return false;--%>
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtDCashflowcode.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDCashflowname.ClientID %>').value = NameandCode[1];

                }
            }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

                document.getElementById('<%=hd_ctrl.ClientID%>').value = ctrl;
                document.getElementById('<%=hd_ctrl1.ClientID%>').value = ctrl1;
                document.getElementById('<%=hd_pMode.ClientID()%>').value = pMode;

                var lstrVal;
                var lintScrVal;

                var NameandCode;
                var result;

                if (pMode == 'NOTCC') {
                    if (ctrl2 == '' || ctrl2 == undefined) {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up1");
                    }
                    else {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up1");
                    }
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];
                    ////document.getElementById(ctrl2).value=lstrVal[2];
                    //// document.getElementById(ctrl3).value=lstrVal[3];
                }

            }

            function OnClientClose1(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    var ctrl = document.getElementById('<%=hd_ctrl.ClientID%>').value;
                    var ctrl1 = document.getElementById('<%=hd_ctrl1.ClientID%>').value;
                    var pMode = document.getElementById('<%=hd_pMode.ClientID()%>').value;
                    NameandCode = arg.NameandCode.split('||');
                    if (pMode == 'NOTCC') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }

                }
            }

            function getAccount() {
                popUp('960', '600', 'NOTCC', '<%=Detail_ACT_ID.ClientId %>', '<%=txtDAccountName.ClientId %>', '<%=ddDCollection.ClientId %>');
                return false;
            }

            function CopyDetails() {
                try {
                    if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                        document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
                }
                catch (ex) { }
            }

            function AddDetails(url) {

                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
                //alert(url_new);
                dates = document.getElementById('<%=txtHDocdate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();


         return false;
     }

     function Settle_Online() {

         var NameandCode;
         var result;
         var pId = 1;//h_NextLine alert(sFeatures)       
         if (pId == 1) {
             url = "ShowOnlineSettlementDR.aspx?actid=" + document.getElementById('<%=Detail_ACT_ID.ClientID %>').value + "&lineid=" + document.getElementById('<%=h_NextLine.ClientID %>').value + "&docno=" + document.getElementById('<%=txtHDocno.ClientID %>').value + "&dt=" + document.getElementById('<%=txtHDocdate.ClientID %>').value;
     }
     result = radopen(url, "pop_up2");
                <%-- if (result == '' || result == undefined) {
                 return false;
             }
             document.getElementById('<%=txtDAmount.ClientID %>').value = result;
              return true;--%>
 }

            function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtDAmount.ClientID %>').value = arg.NameandCode;

                }
            }
        </script>

    </telerik:RadScriptBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>

            Cash Receipt
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_pMode" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Cash Receipt
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Doc No [Old Ref No]</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True" Width="40%"></asp:TextBox>[
                            <asp:TextBox ID="txtHOldrefno" runat="server" Width="40%" TabIndex="2"></asp:TextBox>
                                        ]
                                    </td>
                                    <td width="20%" align="left">
                                        <span class="field-label">Doc Date</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"
                                            Width="80%" TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="6" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Currency </span>
                                    </td>
                                    <td valign="middle" align="left" width="30%">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" Width="20%"
                                            TabIndex="8">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtHExchRate" runat="server" ReadOnly="True" Width="61%"></asp:TextBox></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Group Rate </span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHLocalRate" runat="server" Width="80%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td align="left">
                                        <span class="field-label">Collection Account </span>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:DropDownList ID="ddCollection" runat="server" TabIndex="10" Width="40%">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtHNarration" runat="server" MaxLength="300" TabIndex="12" Visible="False" Width="41%"> </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center"
                                width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Collection Account <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddDCollection" runat="server" TabIndex="20" AutoPostBack="True" Width="80%">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:CheckBox ID="chkAdvance" runat="server" AutoPostBack="True" OnCheckedChanged="chkAdvance_CheckedChanged"
                                            Text="Advance"></asp:CheckBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Credit Account <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="Detail_ACT_ID"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:ImageButton
                                            ID="btnAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;"
                                            TabIndex="14" />
                                        <asp:TextBox ID="txtDAccountName" runat="server" Width="60%"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cash Flow <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtDCashflowcode" runat="server" Width="20%"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDCashflowcode"
                                            ErrorMessage="Cash flow Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="imgCashflow" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCashcode(); return false;"
                                                TabIndex="16" />
                                        <asp:TextBox ID="txtDCashflowname" runat="server"
                                            Width="60%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" TabIndex="18"
                                            AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck"
                                            Type="Double" ValidationGroup="Details">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:LinkButton ID="lbSettle" runat="server" OnClientClick="Settle_Online(); return false;">(Settle)</asp:LinkButton>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Narration<span style="color: red">*</span> </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDNarration" runat="server" Width="80%"
                                            MaxLength="300" TextMode="MultiLine" TabIndex="22"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <br />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trTaxType" visible="false">
                                    <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                                    <td width="20%" align="left"><span class="field-label">Upload Employees </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="fuEmployeeData" runat="server" />
                                        <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Cost Allocation </span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <!-- content start -->
                                        <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" ValidationGroup="Details"
                                            TabIndex="24" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="26" />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="28" />
                                    </td>
                                </tr>

                            </table>
                            <table align="center" width="100%"
                                style="border-collapse: collapse">
                                <tr>
                                    <td align="center" width="100%">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                                width="9px" border="0" src="../images/Misc/plus.gif" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Cashflowname" HeaderText="Cash Flow" />
                                                <asp:BoundField DataField="Collection" HeaderText="Collection" />
                                                <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="lbEdit_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField HeaderText="Cost Center" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("CostReqd") %>' Visible="False"></asp:Label>

                                                        <asp:LinkButton ID="lbAllocate" runat="server" Visible="False">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="100%" align="right">
                                                                <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 100%">
                                                                    <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                        CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                            <asp:BoundField DataField="Costcenter" HeaderText="Costcenter" />
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                            <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                            <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                                HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:TemplateField HeaderText="Amount">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtAmt" runat="server" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                                        Width="104px" onblur="CheckAmount(this)"></asp:TextBox>
                                                                                    <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="104px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:TemplateField>
                                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:CommandField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td colspan="100%" align="right">
                                                                                            <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                                                Width="100%" CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                                    <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                                    <asp:TemplateField HeaderText="Amount">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="104px"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    </asp:CommandField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"><span class="field-label">Total Amount  </span>
                                        <asp:TextBox ID="txtDTotalamount" runat="server" ReadOnly="True" Width="20%"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" ValidationGroup="Details" TabIndex="30" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="32" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="34" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');"
                                            TabIndex="36" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="38" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="40" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <input id="h_mode" runat="server" type="hidden" />
                <input id="h_NextLine" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>
