<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accShowEmpPP.aspx.vb" Inherits="ShowEmpDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" Theme="General" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet"/>
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">


        <table width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td class="matters" colspan="4" valign="top" align="center">
                                <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="ID" CssClass="table table-row table-bordered">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                            <EditItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>&nbsp;
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID" CssClass="gridheader_text"></asp:Label><br />
                                                <asp:TextBox ID="txtcode" runat="server"></asp:TextBox></td>
                                                                            <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnSearchEmpId_Click" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblName" runat="server" Text="Business Unit Name" CssClass="gridheader_text"></asp:Label><br />
                                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                                                            <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnSearchEmpName_Click" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                    Text='<%# Eval("E_Name") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID2" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblID2" runat="server" Text='<%# Eval("ID2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID3" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblID3" runat="server" Text='<%# Eval("ID3") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" id="h_SelectedId" runat="server" value="0" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
            </tr>
        </table>

    </form>
</body>
</html>
