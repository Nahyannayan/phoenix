<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccAddCCM.aspx.vb" Inherits="AccAddCCM" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cost Element Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" align="center">
                    <tr>
                        <td>
                            <table width="100%" align="center" border="0">
                                <tbody>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%" align="center">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDescr" runat="server" TextMode="MultiLine"
                                            MaxLength="100"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

