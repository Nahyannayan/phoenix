Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj


Partial Class AccTranRJV
    Inherits System.Web.UI.Page
    
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then

            Try
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

                '   --- Lijo's Code ---

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (Session("MainMnu_code") <> "A150022") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle 
                    Dim Mnu_code As String = Encr_decrData.Encrypt(Session("MainMnu_code"))
                    Dim datamodeView As String = Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = String.Format("AccRJV.aspx?MainMnu_code={0}&datamode={1}", Mnu_code, datamodeView)
                End If
                gridbind()

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If

        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
        End If

    End Sub


    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvGroup1.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function



    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid("mnu_4_img", str_Sid_img(2))
    End Sub



    Private Sub gridbind()
        Dim lstrDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrBankAC As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltPartyAC As String = String.Empty
        Dim lstrFiltBankAC As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltCurrency As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty
        Dim lstrOpr As String = String.Empty

        Dim txtSearch As New TextBox
        Dim larrSearchOpr() As String
        If gvGroup1.Rows.Count > 0 Then
            ' --- Initialize The Variables
            lstrDocNo = ""
            lstrDocDate = ""
            lstrPartyAC = ""
            lstrBankAC = ""
            lstrNarration = ""
            lstrCurrency = ""
            lstrAmount = ""

            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "A.DocNo", lstrDocNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "A.DocDt", lstrDocDate)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "A.RJH_Narration", lstrNarration)

            '   -- 7  Currency
            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
            lstrCurrency = txtSearch.Text
            If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "A.Cur_Descr", lstrCurrency)


        End If
        Dim str_Topfilter As String = ""
        If UsrTopFilter1.FilterCondition <> "All" Then
            str_Topfilter = " top " & UsrTopFilter1.FilterCondition
        End If


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = " SELECT DISTINCT " & str_Topfilter _
        & " A.GUID,A.DocDt,A.DOCNO as DocNo ,Replace(Left(Convert(VarChar,A.DOCDT,113),11),' ','/') as DocDate ," _
        & " MAx(A.RJH_Narration) as Narration,Max(A.Cur_Descr) as Currency " _
        & " FROM vw_OSA_RJOurnal A WHERE  (bDELETED='FALSE') AND BSU_ID='" & Session("SBSUID") & "' AND FYEAR = " & Session("F_YEAR")
        str_Sql = str_Sql & lstrFiltDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltPartyAC & lstrFiltNarration & lstrFiltAmount & lstrFiltCurrency



        If optUnPosted.Checked = True Then
            Session("FetchType") = "0"
        ElseIf optPosted.Checked = True Then
            Session("FetchType") = "1"
        ElseIf optAll.Checked = True Then
            Session("FetchType") = "2"
        End If

        If (Session("FetchType") = "0") Then
            str_Sql = str_Sql & " AND bPOsted='False'"
        ElseIf (Session("FetchType") = "1") Then
            str_Sql = str_Sql & " AND bPOsted='True'"
        End If

        str_Sql = str_Sql & " GROUP BY A.GUID,A.DocNo,A.DocDt   Order By 2 Desc,3 Desc"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvGroup1.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup1.DataBind()
            Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count

            gvGroup1.Rows(0).Cells.Clear()
            gvGroup1.Rows(0).Cells.Add(New TableCell)
            gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvGroup1.DataBind()
        End If

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
        txtSearch.Text = lstrDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
        txtSearch.Text = lstrDocDate



        txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
        txtSearch.Text = lstrNarration



        txtSearch = gvGroup1.HeaderRow.FindControl("txtCurrency")
        txtSearch.Text = lstrCurrency

        set_Menu_Img()
    End Sub



    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    'Protected Sub gvGroup1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup1.RowCommand
    '    If e.CommandName = "View" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim selectedRow As GridViewRow = DirectCast(gvGroup1.Rows(index), GridViewRow)
    '        Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
    '        Dim Eid As String = UserIDLabel.Text
    '        Dim url As String
    '        Eid = Encr_decrData.Encrypt(Eid)
    '        Session("datamode") = "view"
    '        If (Session("MainMnu_code") = "A150022") Then
    '            Session("MainMnu_code") = Encr_decrData.Encrypt(Session("MainMnu_code"))
    '            Session("datamode") = Encr_decrData.Encrypt(Session("datamode"))
    '            url = String.Format("AccRJV.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Session("MainMnu_code"), Session("datamode"), Eid)
    '        End If
    '        Response.Redirect(url)
    '    End If
    'End Sub



    'Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
    '    Try
    '        Dim url As String
    '        Session("datamode") = "add"
    '        If (Session("MainMnu_code") = "A150022") Then
    '            Session("MainMnu_code") = Encr_decrData.Encrypt(Session("MainMnu_code"))
    '            Session("datamode") = Encr_decrData.Encrypt(Session("datamode"))
    '            url = String.Format("AccRJV.aspx?MainMnu_code={0}&datamode={1}", Session("MainMnu_code"), Session("datamode"))
    '        End If
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '    End Try
    'End Sub

    Protected Sub optPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPosted.CheckedChanged
        gridbind()
    End Sub

    Protected Sub optAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub optUnPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optUnPosted.CheckedChanged
        gridbind()
    End Sub

    Protected Sub gvGroup1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim lblGUID As New Label
        lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
        Dim hlview As New HyperLink
        hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
        If hlview IsNot Nothing And lblGUID IsNot Nothing Then
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Dim eid As String = Encr_decrData.Encrypt(lblGUID.Text)
            hlview.NavigateUrl = String.Format("AccRJV.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", Request.QueryString("MainMnu_code"), ViewState("datamode"), eid)
        End If
    End Sub

End Class
