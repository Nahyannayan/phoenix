<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccPasswordReset.aspx.vb" Inherits="Accounts_AccPasswordReset" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>
<script language="javascript" type="text/javascript">
           
                function test(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
                
                 function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
              document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
</script>
     
<div id="dropmenu1" class="dropmenudiv" style="left: 302px; width: 110px; top: 13px">
                <a href="javascript:test('LI');">
                    <img alt="Any where" class="img_left" src="../Images/operations/like.gif" style="text-decoration: underline" />
                    Any Where</a> <a href="javascript:test('NLI');">
                        <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" style="text-decoration: underline" />
                        Not In</a> <a href="javascript:test('SW');">
                            <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif"
                                style="text-decoration: underline" />
                            Starts With</a> <a href="javascript:test('NSW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" style="text-decoration: underline" />
                                Not Start With</a> <a href="javascript:test('EW');">
                                    <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" style="text-decoration: underline" />
                                    Ends With</a> <a href="javascript:test('NEW');">
                                        <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" style="text-decoration: underline" />
                                        Not Ends With</a>
            </div>
            
             
            <div id="dropmenu2" class="dropmenudiv" style="left: 435px; width: 110px; top: 26px">
                <a href="javascript:test1('LI');">
                    <img alt="Any where" class="img_left" src="../Images/operations/like.gif" style="text-decoration: underline" />
                    Any Where</a> <a href="javascript:test1('NLI');">
                        <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" style="text-decoration: underline" />
                        Not In</a> <a href="javascript:test1('SW');">
                            <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif"
                                style="text-decoration: underline" />
                            Starts With</a> <a href="javascript:test1('NSW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" style="text-decoration: underline" />
                                Not Start With</a> <a href="javascript:test1('EW');">
                                    <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" style="text-decoration: underline" />
                                    Ends With</a> <a href="javascript:test1('NEW');">
                                        <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" style="text-decoration: underline" />
                                        Not Ends With</a>
            </div>
     
           
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 465px">
        <tr class="title">
            <td align="left">
                User Activation</td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;<asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                    Height="24px" SkinID="error" style="text-align: center" Width="133px"></asp:Label>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" style="font-weight: bold; height: 93px" valign="top">
                <table id="tabmain" align="center" border="1" bordercolor="#1b80b6" cellpadding="5"
                    cellspacing="0" class="BlueTableView" style="width: 348px" runat=server>
                    <tr class="subheader_img">
                        <td align="left" colspan="9" style="width: 169px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                User Activation</span></font></td>
                    </tr>
                    <tr id="row2">
                        <td align="left" class="matters" style="width: 267px">
                            Bussiness Unit&nbsp;</td>
                        <td align="left" class="matters" style="width: 56px">
                            <asp:DropDownList id="ddlBUnit" runat="server" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="5" style="height: 23px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="6">
                            <asp:GridView id="gvUNITS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" OnPageIndexChanged="gvUNITS_PageIndexChanged" PageSize="30"
                                Width="525px" OnRowDataBound="gvUNITS_RowDataBound">
                                <rowstyle cssclass="griditem" height="25px" />
                                <columns>
<asp:TemplateField HeaderText="Available"><EditItemTemplate>
                       <asp:CheckBox ID="chkSelect" runat="server"  />
                       
</EditItemTemplate>
<HeaderTemplate>
                       <table>
                       <tr>
                      <td align="center">
                     Select </td>
                     </tr>
                       </table>
                       
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" ToolTip="Click here to select" __designer:wfdid="w1"></asp:CheckBox> <asp:HiddenField id="HiddenField1" runat="server" Value='<%# bind("USR_ID") %>' __designer:wfdid="w2"></asp:HiddenField> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="User Name"><HeaderTemplate>
                <table>
                <tr><td align="center">
                <asp:Label ID="lblusrnameHeader" runat="server" CssClass="gridheader_text" Text="User name">
                </asp:Label></td>
                </tr><tr>
                <td ><table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr><td >
                <div id="Div1" class="chromestyle">
                <ul><li><a href="#" rel="dropmenu1">
                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0"
                 src="../Images/operations/like.gif" />
                </a></li></ul>
                </div>
                </td>
                <td >
                <asp:TextBox ID="txtusrnameSearch" runat="server" Width="100px"></asp:TextBox></td>
                <td >
                <td  valign="middle">
                <asp:ImageButton ID="btnusrname_Search" runat="server" ImageAlign="Top" 
                ImageUrl="~/Images/forum_search.gif" OnClick="btnusrname_Search_Click" /></td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                
</HeaderTemplate>
<ItemTemplate>
                                <asp:Label id="lblusrname" runat="server" Text='<%# Bind("USR_NAME") %>'></asp:Label> <asp:HiddenField id="USM_USR_NAME" runat="server" Value='<%# BIND("USR_ID") %>'></asp:HiddenField> 
                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Password"><ItemTemplate>
<asp:Label id="lblpasswrd" runat="server" Width="90px" __designer:wfdid="w3"></asp:Label> <asp:HiddenField id="USR_PASSWORD" runat="server" Value='<%# BIND("USR_PASSWORD") %>' __designer:wfdid="w4"></asp:HiddenField> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Expiry Date"><ItemTemplate>
                                <asp:Label id="lblexpdate" runat="server" Text='<%# Bind("USR_EXPDATE") %>' Width="90px"></asp:Label> <asp:HiddenField id="USR_EXPDATE" runat="server" Value='<%# BIND("USR_ID") %>'></asp:HiddenField>  
                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Try Count"><HeaderTemplate>
                <table>
                <tr><td align="center">
                <asp:Label ID="lblTCHeader" runat="server" CssClass="gridheader_text" Text="Try Count">
                </asp:Label></td>
                </tr><tr>
                <td ><table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr><td >
                <div id="Div2" class="chromestyle">
                <ul><li><a href="#" rel="dropmenu2">
                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0"
                 src="../Images/operations/like.gif" />
                </a></li></ul>
                </div>
                </td>
                <td >
                <asp:TextBox ID="txtTCSearch" runat="server"  Width="50px"></asp:TextBox></td>
                <td >
                <td  valign="middle">
                <asp:ImageButton ID="btnTC_Search" runat="server" ImageAlign="Top" 
                ImageUrl="~/Images/forum_search.gif" OnClick="btnTC_Search_Click" /></td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                
</HeaderTemplate>
<ItemTemplate>
                                <asp:Label id="lbltrycount" runat="server" Text='<%# Bind("USR_TRYCOUNT") %>'></asp:Label> <asp:HiddenField id="USR_TRYCOUNT" runat="server" Value='<%# BIND("USR_ID") %>'></asp:HiddenField>  
                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Disable"><HeaderTemplate>
            <table><tr><td align="center">
            Disable</td></tr><tr>
            <td align="center">
            <asp:DropDownList ID="ddlgvDisable" runat="server" AutoPostBack="True" CssClass="listbox"
            OnSelectedIndexChanged="ddlgvDisable_SelectedIndexChanged" Width="80px">
            <asp:ListItem>ALL</asp:ListItem>
            <asp:ListItem>True</asp:ListItem>
            <asp:ListItem>False</asp:ListItem>
            </asp:DropDownList></td>
            </tr></table>
            
</HeaderTemplate>
<ItemTemplate>
                                <asp:Label id="lbldisable" runat="server" Text='<%# Bind("USR_bDisable") %>'></asp:Label> <asp:HiddenField id="USR_bDisable" runat="server" Value='<%# BIND("USR_ID") %>'></asp:HiddenField>  
                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" height="30px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 29px" valign="bottom">
                &nbsp;
                <asp:Button id="btnActivate" runat="server" CssClass="button" onclick="btnActivate_Click"
                    tabIndex="7" Text="Activate" ValidationGroup="groupM1" Width="70px" />
<%--<asp:Button id="btnReset" runat="server" CausesValidation="False" CssClass="button"
                    tabIndex="8" Text="Reset" UseSubmitBehavior="False" Width="70px" OnClick="btnReset_Click" />--%>
            </td>
            <td><input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
</td>
        </tr>
</table>
    
        <script type="text/javascript">

cssdropdown.startchrome("Div1")
cssdropdown.startchrome("Div2")
        </script>
</asp:Content>

