<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accChequeRegister.aspx.vb" Inherits="Accounts_accChequeRegister" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }
        function fnSelectAll(master_box) {
            var curr_elem, checkbox_checked_status;
            var chk_name = master_box.id.replace("Selectall", "");
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && (curr_elem.id.indexOf(chk_name) > 0))
                    curr_elem.checked = master_box.checked;
            }
        }

        function ShowDetails(bsu, docno) {
            var sFeatures;
            sFeatures = "dialogWidth: 660px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            var url;
            url = 'AccShowBP.aspx?bsu=' + bsu + '&docno=' + docno;
            //result = window.showModalDialog(url, "", sFeatures);
            result = radopen(url, "pop_up2")

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');


            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1200px" Height="620px">
            </telerik:RadWindow>
        </Windows>



    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>

                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="sdsBusinessunit"
                                            DataTextField="BSU_NAME" DataValueField="BSU_ID" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                            SelectCommand="SELECT BSU_ID, BSU_NAME FROM dbo.fn_GetBusinessUnits(@sUsr_name) AS fn_GetBusinessUnits_1 WHERE (ISNULL(BSU_bShow, 1) = 1) UNION SELECT 'ALL' AS BSU_ID, '' AS BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="sUsr_name" SessionField="sUsr_name" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Update Change Status to</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddStatusChange" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Cash Receipt Vouchers for posting"
                                            Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText=" ">
                                                    <HeaderTemplate>
                                                        <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                            value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkControl" runat="server" type="checkbox" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="Unit"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Doc. No.">
                                                    <HeaderTemplate>
                                                        Doc. No.<br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Chq. No.">
                                                    <HeaderTemplate>
                                                        Chq. No<br />
                                                        <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnChqNoSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("VDC_VHD_CHQNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Chq Date.">
                                                    <HeaderTemplate>
                                                        Chq Date.<br />
                                                        <asp:TextBox ID="txtChqDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnChqDateSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChqDate" runat="server" Text='<%# Bind("VHD_CHQDT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document Date">
                                                    <HeaderTemplate>
                                                        Doc Date<br />
                                                        <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocDateSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("VDC_APPROVEDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payee">
                                                    <HeaderTemplate>
                                                        Payee<br />
                                                        <asp:TextBox ID="txtPayee" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnPayeeSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPayee" runat="server" Text='<%# Bind("Payee") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Narration">
                                                    <HeaderTemplate>
                                                        Narration<br />
                                                        <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnNarrationSearch" OnClick="btnSearch_Click" runat="server"
                                                            ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("VDC_REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <HeaderTemplate>
                                                        Bundle<br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAmountSearch" OnClick="btnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBundleNo" runat="server" Text='<%# Bind("VDC_BundleNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount" ReadOnly="True"></asp:BoundField>
                                                <asp:BoundField DataField="VDC_STATUS" HeaderText="Status" ReadOnly="True"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Update Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVDC_ID" runat="server" Text='<%# Bind("VDC_ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblVDC_CST_ID" runat="server" Text='<%# Bind("VDC_CST_ID") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="ddUpdateStatus" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        View Voucher
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbView" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="tr_Narration" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Narration (Only for Rejection)</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtNarrReject" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" id="tr_Post" runat="server">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Process" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>



                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>
