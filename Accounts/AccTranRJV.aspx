<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccTranRJV.aspx.vb" Inherits="AccTranRJV" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            RECURRING JV
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:RadioButton ID="optUnPosted" runat="server" GroupName="optControl" Text="Open" Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="optPosted" runat="server" GroupName="optControl" Text="Posted" AutoPostBack="True" />
                            <asp:RadioButton ID="optAll" runat="server" GroupName="optControl" Text="All" AutoPostBack="True" /></td>
                    </tr>
                </table>
                <table border="0" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td width="80%"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="GUID" AllowPaging="True"
                                PageSize="50" OnRowDataBound="gvGroup1_RowDataBound" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No
                                <br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date  
                                <asp:TextBox ID="txtDocDate" runat="server" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DocDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Narration
                                <br />
                                            <asp:TextBox ID="txtNarrn" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="30%" HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarration" runat="server" Text='<%# Bind("Narration") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Currency
                                             <br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodewww" runat="server" Text='<%# Bind("Currency") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="view">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

