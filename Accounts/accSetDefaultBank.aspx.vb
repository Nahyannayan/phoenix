Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class Accounts_accSetDefaultBank
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtBankPaymentCode.Attributes.Add("readonly", "readonly")
            txtBankPaymentDescr.Attributes.Add("readonly", "readonly")
            txtBankReceiptCode.Attributes.Add("readonly", "readonly")
            txtBankReceiptDescr.Attributes.Add("readonly", "readonly")
            set_BankReceipt()
            set_BankPayment()
        End If
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        Try
            objConn.Open()
            Dim SqlCmd As New SqlCommand("SaveCommonSettings", objConn)
            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@RECEIPT_CODE", txtBankReceiptCode.Text)
            SqlCmd.Parameters.AddWithValue("@PAYMENT_CODE", txtBankPaymentCode.Text)
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If lintRetVal = 0 Then
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtBankReceiptCode.Text & "|" & txtBankPaymentCode.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                lblErr.Text = UtilityObj.getErrorMessage(lintRetVal)
            End If
        Catch myex As ArgumentException
            lblErr.Text = myex.Message
        Catch ex As Exception
            lblErr.Text = UtilityObj.getErrorMessage("1000")
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Sub set_BankReceipt()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_COLLECTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_COLLECTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString)
        If str_bankact_name <> "--" Then
            txtBankReceiptCode.Text = str_bankact_name.Split("|")(0)
            txtBankReceiptDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub

    Sub set_BankPayment()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString)
        If str_bankact_name <> "--" Then
            txtBankPaymentCode.Text = str_bankact_name.Split("|")(0)
            txtBankPaymentDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub
End Class
