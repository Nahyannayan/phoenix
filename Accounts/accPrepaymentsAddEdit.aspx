<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="accPrepaymentsAddEdit.aspx.vb" Inherits="empPrepayments" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        .RadGrid {
            border-radius: 0px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            /*background-color: #00a3e0;*/
        }

        .RadGrid_Office2010Blue th.rgSorted {
            /*background-color: #00a3e0;*/
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCellLeft {
            background-position: 0 0;
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default {
            width: 80% !important;
        }

        .rcbReadOnly td.rcbInputCell.rcbInputCellLeft {
            /* padding: 0px !important; */
            background-color: #ffffff !important;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../images/Misc/minus.gif";
                }
                else {
                    img.src = "../images/Misc/minus.gif";
                }
                img.alt = "Close to view other Customers";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../images/Misc/plus.gif";
                }
                else {
                    img.src = "../images/Misc/plus.gif";
                }
                img.alt = "Expand to show Orders";
            }
        }

        function ViewMonthly() {
            var sFeatures;
            sFeatures = "dialogWidth: 450px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = 'Amount=' + document.getElementById('<%=txtAmount.ClientID %>').value;
            dates1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            dates2 = document.getElementById('<%=txtToDate.ClientID %>').value;
            dates1 = dates1.replace(/[/]/g, '-')
            dates2 = dates2.replace(/[/]/g, '-')
            var url_new
            url_new = url + '&Sdate=' + dates1 + '&Edate=' + dates2;
            //result = window.showModalDialog("accPrepaymentMonthly_Split.aspx?" + url_new, "", sFeatures)
            //return false;
            result = radopen("accPrepaymentMonthly_Split.aspx?" + url_new, "pop_up2")
        }
        function AddDetails() {
            var sFeatures;
            sFeatures = "dialogWidth: 800px; ";
            sFeatures += "dialogHeight: 550px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            var url = 'vid=0&amt=' + document.getElementById('<%=txtAmount.ClientID %>').value + '&sid=' + document.getElementById('<%=hfId2.ClientID %>').value;
            var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
            dates = document.getElementById('<%=txtdocdate.ClientID %>').value;
            dates = dates.replace(/[/]/g, '-')
            url_new = url_new + '&dt=' + dates;
            //result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
            result = radopen("acccpAddDetails.aspx?" + url_new, "pop_up3")

            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=DETAIL_ACT_ID.ClientID %>').focus();
            return false;--%>
        }

        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "calendar.aspx?nofuture=yes";
            if (val == 1) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }
            if (val == 1) {
                document.getElementById('<%=txtdocdate.ClientID %>').value = result;
            }
            else if (val == 2) {
                document.getElementById('<%=txtFromDate.ClientID %>').value = result;
            }
            else if (val == 3) {
                document.getElementById('<%=txtToDate.ClientID %>').value = result;
            }
    return false;
}


function getRoleID(mode) {
    var sFeatures;
    sFeatures = "dialogWidth: 460px; ";
    sFeatures += "dialogHeight: 400px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var url;

    url = 'accShowEmpPP.aspx?id=' + mode;
    document.getElementById('<%=hd_mode.ClientID%>').value = mode;
    result = radopen(url, "pop_up5")
    <%--if (mode == 'CU') {
        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined)
        { return false; }
        //ClearDocno();
        NameandCode = result.split('___');
        document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];

        document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[2];
        document.getElementById("<%=txtPreCode.ClientID %>").value = NameandCode[3];

        document.getElementById("<%=txtExpAcc.ClientID %>").value = NameandCode[4];
        document.getElementById("<%=DETAIL_ACT_ID.ClientID %>").value = NameandCode[5];
    }
    if (mode == 'PP') {
        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined)
        { return false; }
        // ClearDocno();
        NameandCode = result.split('___');

        document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=txtPreCode.ClientID %>").value = NameandCode[1];
    }
    if (mode == 'EA') {
        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');

        document.getElementById("<%=txtExpAcc.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=DETAIL_ACT_ID.ClientID %>").value = NameandCode[1];
    }
    if (mode == 'PA') {
        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined)
        { return false; }

        NameandCode = result.split('___');

        document.getElementById("<%=txtPartyAcc.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=txtPartCode.ClientID %>").value = NameandCode[1];
    }--%>
}
        function GetDocno() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 645px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var docID = document.getElementById("<%=ddDoctype.ClientID %>").value;
            var docNo = document.getElementById("<%=txtHDocno.ClientID %>").value;
            //result = window.showModalDialog("../Common/PopupFormThree.aspx?multiSelect=false&ID=DOCNO&type=" + docID + "&act=" + document.getElementById('<%=txtPreCode.ClientID %>').value + "&dt=" + document.getElementById('<%=txtdocdate.ClientID %>').value + "&docno=" + docNo, "", sFeatures)
            result = radopen("../Common/PopupFormThree.aspx?multiSelect=false&ID=DOCNO&type=" + docID + "&act=" + document.getElementById('<%=txtPreCode.ClientID %>').value + "&dt=" + document.getElementById('<%=txtdocdate.ClientID %>').value + "&docno=" + docNo, "pop_up4")
    <%--if (result != "" && result != "undefined") {
        NameandCode = result.split('___');
        document.getElementById('<%=txtRefdocno.ClientID %>').value = NameandCode[1];
        document.getElementById('<%=txtAmount.ClientID %>').value = NameandCode[0];
        document.getElementById('<%=hfRefid.ClientID %>').value = NameandCode[2];
        document.getElementById('<%=txtAmount.ClientID %>').readOnly = true;
    }
    return false;--%>
        }

        function checkDocno(ss) {
            if (document.getElementById('<%=txtRefdocno.ClientID %>').value != '') {
                alert(' Cannot change the amount \r (Clear the reference document no and try again)!!!');
                ss.blur();
            }
        }
        function ClearDocno() {
            document.getElementById('<%=txtRefdocno.ClientID %>').value = '';
            document.getElementById('<%=txtAmount.ClientID %>').value = '';
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

            }
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=DETAIL_ACT_ID.ClientID %>').focus();
            }
        }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtRefdocno.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtAmount.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=hfRefid.ClientID %>').value = NameandCode[2];
                document.getElementById('<%=txtAmount.ClientID %>').readOnly = true;
            }

        }
        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                var mode = document.getElementById('<%=hd_mode.ClientID%>').value
                //alert( document.getElementById('<%=hd_mode.ClientID%>').value);

                if (mode == 'CU') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];

                    document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[2];
                    document.getElementById("<%=txtPreCode.ClientID %>").value = NameandCode[3];

                    document.getElementById("<%=txtExpAcc.ClientID %>").value = NameandCode[4];
                    document.getElementById("<%=DETAIL_ACT_ID.ClientID %>").value = NameandCode[5];
                }
                if (mode == 'PP') {
                    NameandCode = arg.NameandCode.split('||');

                    document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=txtPreCode.ClientID %>").value = NameandCode[1];
                }
                if (mode == 'EA') {
                    NameandCode = arg.NameandCode.split('||');

                    document.getElementById("<%=txtExpAcc.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=DETAIL_ACT_ID.ClientID %>").value = NameandCode[1];
                }
                if (mode == 'PA') {
                    NameandCode = arg.NameandCode.split('||');

                    document.getElementById("<%=txtPartyAcc.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=txtPartCode.ClientID %>").value = NameandCode[1];
                }
            }

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Pre Payment Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="Group1" CssClass="error"
                                ForeColor="" />
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Doc No</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDocNo" runat="server" ControlToValidate="txtHDocno"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Document No required"
                                            ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Doc Date<span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtdocdate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvDocDate" runat="server" ControlToValidate="txtdocdate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Doc Date required" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revDocDate" runat="server" ControlToValidate="txtdocdate"
                                            EnableViewState="False" ErrorMessage="Enter the Doc Date in given format dd/mmm/yyyy e.g.  21/sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="Group1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDocDate" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Doc Date entered is not a valid date" ValidationGroup="Group1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Currency</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="90"><span class="field-label">Exchange Rate</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtHExchRate" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Group Rate</span>
                                    </td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHGroupRate" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Cost Unit <span class="text-danger">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCostUnit" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnCostunit" runat="server" OnClientClick="getRoleID('CU'); return false;"
                                            ImageUrl="~/Images/cal.gif" />
                                        <asp:RequiredFieldValidator ID="rfvCostUnit" runat="server" ControlToValidate="txtCostUnit"
                                            Display="Dynamic" ErrorMessage="Cost unit required" EnableViewState="False" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Prepaid Account</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPreCode" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnPrePaid" runat="server" OnClientClick="getRoleID('PP'); return false;"
                                            ImageUrl="~/Images/cal.gif" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPreAcc" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPrepaidAccount" runat="server" ControlToValidate="txtPreAcc"
                                            Display="Dynamic" ErrorMessage="Prepaid account required" EnableViewState="False"
                                            ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Ref Document</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddDoctype" runat="server" onChange="ClearDocno();">
                                            <asp:ListItem Value="BP">Bank Payment</asp:ListItem>
                                            <asp:ListItem Value="CP">Cash Payment</asp:ListItem>
                                            <asp:ListItem Value="CN">Credit Note</asp:ListItem>
                                            <asp:ListItem Value="DN">Debit Note</asp:ListItem>
                                            <asp:ListItem Value="IJV">Inter Unit Journal</asp:ListItem>
                                            <asp:ListItem Value="JV">Journal Voucher</asp:ListItem>
                                            <asp:ListItem Value="PJ">Purchase Journal</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtRefdocno" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnDocument" runat="server" OnClientClick="GetDocno();return false;"
                                            ImageUrl="~/Images/cal.gif" />
                                        <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="ClearDocno();return false;">Clear</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Expense Account</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="DETAIL_ACT_ID" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnExpAcc" runat="server" OnClientClick="getRoleID('EA'); return false;"
                                            ImageUrl="~/Images/cal.gif" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtExpAcc" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvExpensive" runat="server" ControlToValidate="txtExpAcc"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Expensive account required"
                                            ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Creditor Account</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPartCode" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnPartyAcc" runat="server" OnClientClick="getRoleID('PA'); return false;"
                                            ImageUrl="~/Images/cal.gif" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPartyAcc" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Invoice No<span class="text-danger">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtInvoiceNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">LPO<span class="text-danger">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtLpo" runat="server" MaxLength="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount<span style="color: red">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAmount" runat="server" onfocus="checkDocno(this)"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Kindly enter valid amount"
                                            ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck"
                                            Type="Double" ValidationGroup="Group1">*</asp:CompareValidator>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date From<span style="color: red">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvDatefrom" runat="server" ControlToValidate="txtFromDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Date From required" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Doc Date in given format dd/mmm/yyyy e.g.  21/sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="Group1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="From Date entered is not a valid date" ValidationGroup="Group1">*</asp:CustomValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Date To<span style="color: red">*</span></span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvTodate" runat="server" ControlToValidate="txtToDate"
                                            EnableViewState="False" ErrorMessage="Date To required" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007 "
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="Group1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="To Date entered is not a valid date and must be one month greater than From date"
                                            ValidationGroup="Group1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="trTaxType" visible="false">
                                    <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration CR<span style="color: red">*</span></span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtNarration1" runat="server" MaxLength="300" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvNarrationCR" runat="server" ControlToValidate="txtNarration1"
                                            EnableViewState="False" ErrorMessage="Narration CR Rerquired" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration DR<span style="color: red">*</span></span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtNarration2" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:ImageButton ID="btnNext" ToolTip="Previous Record" runat="server" ImageAlign="Top"
                                            ImageUrl="~/Images/Misc/LEFT1.gif" CausesValidation="False" OnClick="btnNext_Click"></asp:ImageButton>
                                        <asp:Button ID="btnMonthly" runat="server" CausesValidation="False" CssClass="button"
                                            OnClientClick="ViewMonthly();return false;" Text="Monthly Split-up" />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClientClick="return validate_add();"
                                            Text="Add" CausesValidation="False" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Group1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False"
                                            OnClientClick="return confirm('Are you sure');" />
                                        <asp:Button ID="btnAlloca" runat="server" CssClass="button" Text="Allocate" CausesValidation="False"
                                            OnClientClick="AddDetails();return false;" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Width="50px" />
                                        <asp:ImageButton ID="btnPrevious" ToolTip="Next Record" runat="server" ImageUrl="~/Images/Misc/RIGHT1.gif"
                                            CausesValidation="False" ImageAlign="Top" OnClick="btnPrevious_Click"></asp:ImageButton>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="PRD_PRP_ID" HeaderText="Document No" SortExpression="PRD_PRP_ID" />
                                                <asp:BoundField DataField="PRD_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Document Date"
                                                    HtmlEncode="False" SortExpression="PRD_DOCDT" />
                                                <asp:BoundField DataField="PRD_JVNO" HeaderText="Journal No" />
                                                <asp:BoundField DataField="PRD_ALLOCAMT" HeaderText="Amount" SortExpression="PRD_ALLOCAMTT"
                                                    DataFormatString="{0:#,0.00}" HtmlEncode="False">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="View" ShowHeader="False" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbViewChild" runat="server" CausesValidation="false" Text="View"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfId1" runat="server" />
                <asp:HiddenField ID="hfId2" runat="server" />
                <asp:HiddenField ID="hfId3" runat="server" />
                <asp:HiddenField ID="hfGuid" runat="server" />
                <asp:HiddenField ID="h_BSU_ID" runat="server" Value="" />
                <asp:HiddenField ID="h_SubId" runat="server" Value="" />
                <asp:HiddenField ID="h_fyear" runat="server" Value="" />
                <asp:HiddenField ID="h_editorview" runat="server" Value="" />
                <asp:HiddenField ID="hfRefid" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtdocdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="FromDate" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="ToDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton3" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hd_mode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
