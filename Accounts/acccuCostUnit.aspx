<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="acccuCostUnit.aspx.vb" Inherits="Accounts_acccuCostUnit" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function getRoleID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'accShowEXPP.aspx?id=' + mode;
            document.getElementById('<%=h_mode.ClientID%>').value = mode;
            //result = window.showModalDialog(url, "", sFeatures);
            var result = radopen(url, "pop_up2")
            <%--if (result == '' || result == undefined)
            { return false; }
            if (mode == 'PP') {
                NameandCode = result.split('___');
                document.getElementById("<%=txtPAccountname.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtPAccountcode.ClientID %>").value = NameandCode[1];
            }


            if (mode == 'EA') {
                NameandCode = result.split('___');
                document.getElementById("<%=txtEAccountname.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtEAccountcode.ClientID %>").value = NameandCode[1];

            }--%>
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                //alert(document.getElementById('<%=h_mode.ClientID%>'));
                if (document.getElementById('<%=h_mode.ClientID%>')) {
                    var value_mode = document.getElementById('<%=h_mode.ClientID%>').value
                    //alert(value_mode);
                    if (value_mode == 'PP') {
                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById("<%=txtPAccountname.ClientID %>").value = NameandCode[0];
                        document.getElementById("<%=txtPAccountcode.ClientID %>").value = NameandCode[1];
                    }
                    if (value_mode == 'EA') {
                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById("<%=txtEAccountname.ClientID %>").value = NameandCode[0];
                        document.getElementById("<%=txtEAccountcode.ClientID %>").value = NameandCode[1];
                    }
                }
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Memo JV Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddCurrency" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cost Unit Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddCosttype" runat="server">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="h_editid" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Cost Unit Description</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCostdescr" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCostdescr"
                                            ErrorMessage="Cost Item Description cannot be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cost Unit Item</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCostItem" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCostItem"
                                            ErrorMessage="Cost Unit Description cannot be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Prepaid Account</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPAccountcode" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPAccountcode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="btnHaccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getRoleID('PP');return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPAccountname" runat="server" CssClass="inputbox" Width="264px"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Expense Account</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEAccountcode" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEAccountcode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getRoleID('EA');return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEAccountname" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_mode" runat="server" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Details" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" ValidationGroup="Details" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

