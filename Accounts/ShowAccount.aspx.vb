Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic

Partial Class ShowAccount
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()

    End Sub



    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub



    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Private Sub gridbind()
        Try
            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            'str_mode = Request.QueryString("mode") 'ShowType

            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType")
            Dim STR_COLLECTION As String = Request.QueryString("colid")
            If str_mode = "PARTY" Or str_mode = "PARTY1" Then
                str_filter_custsupp = " AND AM.ACT_FLAG='S'"
                gvGroup.Columns(5).Visible = False
            End If
            If str_mode = "PARTY2" Then
                str_filter_custsupp = " and  AM.ACT_TYPE in ('Liability','Asset')  and  AM.ACT_BANKCASH   in ('N') and  AM.ACT_BCTRLAC<>1"
                gvGroup.Columns(5).Visible = False
            End If

            If str_mode = "CUSTSUPP" Then
                str_filter_custsupp = " AND (AM.ACT_FLAG='S' OR AM.ACT_FLAG='C')"
                gvGroup.Columns(5).Visible = False
            End If
            
            If str_mode = "CUSTSUPPnIJV" Then
                str_filter_custsupp = "AND  ((AM.ACT_FLAG='S' OR AM.ACT_FLAG='C') or ( AM.ACT_SGP_ID='0810')) "
                gvGroup.Columns(5).Visible = False
            End If

            If str_mode = "DEBIT" Or str_mode = "DEBIT_D" Then
                str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                str_filter_debit = " AND AM.ACT_FLAG = 'N'"
                gvGroup.Columns(5).Visible = False
                gvGroup.Columns(4).Visible = False
            End If
            If str_mode = "NORMAL" Then
                str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                gvGroup.Columns(4).Visible = False
            End If
            If str_mode = "NOTCC" Then
                If STR_COLLECTION <> "" And STR_COLLECTION <> "5" Then
                    str_filter_bankcash = " AND (AM.ACT_COL_ID = '" & STR_COLLECTION & "') "
                End If
                str_filter_bankcash = str_filter_bankcash & " AND AM.ACT_BANKCASH<>'CC'"
                gvGroup.Columns(4).Visible = False
            End If
            If str_mode = "BANKONLY" Then
                str_filter_bankcash = " AND AM.ACT_BANKCASH='B'"
                gvGroup.Columns(4).Visible = False
            End If
            If str_mode = "CASHONLY" Then
                str_filter_bankcash = " AND AM.ACT_BANKCASH='C'"
                gvGroup.Columns(4).Visible = False
            End If
            '-----
            If str_mode = "CUSTACC" Then
                str_filter_custsupp = " AND AM.ACT_FLAG='C'"
                gvGroup.Columns(5).Visible = False
            End If
            '-------
            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox

            ''''''''''
            Dim str_CodeorName As String = Request.QueryString("codeorname") & ""

            Dim i As Integer
            Dim iCharcount As Integer = -1
            Dim ch As String
            For i = 1 To Len(str_CodeorName)
                ch = Mid(str_CodeorName, i, 1)
                If IsNumeric(ch) = False Then
                    iCharcount = iCharcount + 1
                    If ch = "-" Then
                        iCharcount = 100
                    End If
                End If
            Next

            If IsNumeric(str_CodeorName) And iCharcount = -1 Then
                str_txtCode = str_CodeorName
                str_filter_code = " AND AM.ACT_ID LIKE '%" & str_CodeorName & "%'"
            Else
                If iCharcount = 0 Then
                    str_txtCode = str_CodeorName
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & str_CodeorName & "%'"
                Else
                    str_txtName = str_CodeorName
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & str_CodeorName & "%'"
                End If
            End If
            'If IsNumeric(str_CodeorName) = True Then

            'End If

            ''''''''
            If gvGroup.Rows.Count > 0 Then
                Try
                    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")

                    ddcust = gvGroup.HeaderRow.FindControl("DDCutomerSupplier")
                    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                    ddbank = gvGroup.HeaderRow.FindControl("DDBankorCash")

                Catch ex As Exception
                End Try

                If ddacctype.SelectedItem.Value <> "All" Then
                    str_filter_acctype = " AND AM.ACT_TYPE='" & ddacctype.SelectedItem.Value & "'"
                End If

                If ddcust.SelectedItem.Value = "C" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='C'"
                ElseIf ddcust.SelectedItem.Value = "S" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='S'"
                ElseIf ddcust.SelectedItem.Value = "N" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='N'"
                End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                If str_mode = "PARTY" Or str_mode = "PARTY1" Then
                    str_filter_custsupp = " AND AM.ACT_FLAG='S'"
                    gvGroup.Columns(5).Visible = False
                End If
                If str_mode = "PARTY2" Then
                    str_filter_custsupp = " AND AM.ACT_TYPE in ('Liability','Asset')  and  AM.ACT_BANKCASH   in ('N') and  AM.ACT_BCTRLAC<>1"
                    gvGroup.Columns(5).Visible = False
                End If
                i_dd_bank = ddbank.SelectedIndex
                i_dd_acctype = ddacctype.SelectedIndex
                i_dd_custsupp = ddcust.SelectedIndex
                If ddbank.SelectedItem.Value = "B" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='B'"
                ElseIf ddbank.SelectedItem.Value = "C" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='C'"
                ElseIf ddbank.SelectedItem.Value = "N" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                End If
                If str_mode = "DEBIT" Or str_mode = "DEBIT_D" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                    str_filter_debit = " AND AM.ACT_FLAG = 'N'"
                    gvGroup.Columns(5).Visible = False
                    gvGroup.Columns(4).Visible = False
                End If
                If str_mode = "CUSTSUPP" Then
                    str_filter_custsupp = " AND (AM.ACT_FLAG='S' OR AM.ACT_FLAG='C')"
                    gvGroup.Columns(5).Visible = False
                End If
                
                If str_mode = "CUSTSUPPnIJV" Then
                    str_filter_custsupp = " AND ((AM.ACT_FLAG='S' OR AM.ACT_FLAG='C') or ( AM.ACT_SGP_ID='0810') )"
                    gvGroup.Columns(5).Visible = False
                End If
                If str_mode = "NORMAL" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                    gvGroup.Columns(4).Visible = False
                End If
                If str_mode = "NOTCC" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH<>'CC'"
                    gvGroup.Columns(4).Visible = False
                End If
                If str_mode = "BANKONLY" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='B'"
                    gvGroup.Columns(4).Visible = False
                End If
                If str_mode = "CASHONLY" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='C'"
                    gvGroup.Columns(4).Visible = False
                End If
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & Trim(txtSearch.Text) & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & Trim(txtSearch.Text) & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "'"
                End If

                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_control = " AND AMC.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_control = " AND AMC.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_control = " AND AMC.ACT_NAME LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_control = " AND AMC.ACT_NAME NOT LIKE '" & Trim(txtSearch.Text) & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_control = " AND AMC.ACT_NAME LIKE '%" & Trim(txtSearch.Text) & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_control = " AND AMC.ACT_NAME NOT LIKE '%" & Trim(txtSearch.Text) & "'"
                End If
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT " _
            & " AM.ACT_ID ,AM.ACT_NAME ,AM.ACT_COL_ID,AM.ACT_SGP_ID ,AM.ACT_TYPE," _
            & " CASE AM.ACT_BANKCASH" _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END " _
            & " ACT_BANKCASH" _
            & " ,AM.ACT_CTRLACC ,AM.ACT_Bctrlac ," _
            & " CASE AM.ACT_FLAG" _
            & " WHEN 'S' THEN 'Supliers'" _
            & " WHEN 'C' THEN 'Customers'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Others' END" _
            & " ACT_FLAG ," _
            & " AMC.ACT_NAME CTRL_NAME,isNULL(AM.PLY_COSTCENTER ,'OTH') as Ply,AM.PLY_BMANDATORY as CostReqd" _
            & " FROM vw_OSA_ACCOUNTS_M AM, ACCOUNTS_M AMC" _
            & " WHERE AM.ACT_Bctrlac='FALSE'" _
            & " AND AMC.ACT_ID=AM.ACT_CTRLACC AND AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%' " _
            & " AND AM.ACT_BACTIVE='TRUE'"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_bankcash & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_filter_custsupp)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            ddcust = gvGroup.HeaderRow.FindControl("DDCutomerSupplier")
            ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
            ddbank = gvGroup.HeaderRow.FindControl("DDBankorCash")

            ddbank.SelectedIndex = i_dd_bank
            ddacctype.SelectedIndex = i_dd_acctype
            ddcust.SelectedIndex = i_dd_custsupp

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

   

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        'set_Menu_Img()
    End Sub



    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton

        Dim lblvarPly As New Label
        Dim lblvarCostReqd As New Label
        lbClose = sender
        Dim lblACT_COL_ID As New Label
        lblcode = sender.Parent.FindControl("lblCode")
        lblcode.Text = lblcode.Text.Replace("___", "||")
        lblvarPly = sender.Parent.FindControl("lblPly")
        lblvarCostReqd = sender.Parent.FindControl("lblCostReqd")
        lblACT_COL_ID = sender.Parent.FindControl("lblACT_COL_ID")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblvarPly.Text & "||" & lblvarCostReqd.Text & "||" & lblACT_COL_ID.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblvarPly.Text & "||" & lblvarCostReqd.Text & "||" & lblACT_COL_ID.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblvarPly.Text & "||" & lblvarCostReqd.Text & "||" & lblACT_COL_ID.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


End Class
