Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accChequeRegisterViewBundle
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A150105") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            hlAddNew.NavigateUrl = "accChequeRegisterBundle.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            ddlBusinessunit.DataBind()
            If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlBusinessunit.ClearSelection()
                ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            End If
            ddChequeStatus.databind()
            gridbind()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        ' Dim i As New Encryption64
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                        'hlCEdit.NavigateUrl = "acccpCashPayments.aspx?editid=" & lblGUID.Text
                        hlview.NavigateUrl = "acccrCashReceipt.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                        hlCEdit.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub  

    Private Sub gridbind()
        Try
            Dim str_filter As String = String.Empty
            Dim lstrDocNo As String = String.Empty
            Dim lstrDocDate As String = String.Empty

            Dim lstrNarration As String = String.Empty
            Dim lstrOpr As String = String.Empty

            Dim lstrChqNo As String = String.Empty
            Dim lstrAmount As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables 
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VHH_DOCNO", lstrDocNo)

                '   -- 2  DocDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
                lstrChqNo = txtSearch.Text
                If (lstrChqNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_VHD_CHQNO", lstrChqNo)

                '   -- 3  DocDate
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
                lstrDocDate = txtSearch.Text
                If (lstrDocDate <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_BundleNo", lstrDocDate)

                '   -- 4  Narration
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
                lstrNarration = txtSearch.Text
                If (lstrNarration <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_REMARKS", lstrNarration)

                '   -- 5  Amount
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrAmount = txtSearch.Text
                If (lstrAmount <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VHD_AMOUNT", lstrAmount)
            End If

            Dim str_Sql As String = String.Empty

            If ddChequeStatus.SelectedItem.Value <> 0 Then
                str_filter = str_filter & " AND VDC_CST_ID=  " & ddChequeStatus.SelectedItem.Value
            End If

            If ddlBusinessunit.SelectedItem.Value <> "ALL" Then
                str_filter = str_filter & " AND VDC_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "'  "
            End If
            str_Sql = "SELECT * FROM VW_OSA_VOUCHER_D_CHEQUES " _
                & " WHERE 1=1 " & str_filter _
                & " ORDER BY VDC_BundleNo DESC,VDC_APPROVEDATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
          
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrDocNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
            txtSearch.Text = lstrDocDate

            txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
            txtSearch.Text = lstrChqNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            txtSearch.Text = lstrNarration

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrAmount

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub  

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddChequeStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddChequeStatus.SelectedIndexChanged
        gridbind()
    End Sub
End Class

