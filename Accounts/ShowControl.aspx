<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowControl.aspx.vb" Inherits="ShowControl" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accounts</title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>

    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>
    <style type="text/css">
        .odd {
            background-color: white;
        }

        .even {
            background-color: gray;
        }
    </style>
     <script type="text/javascript">
      function GetRadWindow() {
          var oWindow = null;
          if (window.radWindow) oWindow = window.radWindow;
          else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
          return oWindow;
      }
    </script>

    <script language="javascript" type="text/javascript">
        //not in use


        function MoveCursorDown() {//alert("ii");
            var c = 16;
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            //alert(c)
         c++;
         if (c <= 0 || c == -1) {
             document.getElementById("<%=h_SelectedId.ClientID %>").value == '17';
            c = 16;
        }
            //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);

            //c++;
            //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null;
            var index = 0;
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c < rows.length - 1) {
                rows[c].className = "gridheader_pop";
                if (c % 2 == 0)
                    rows[c - 1].className = "griditem_alternative";
                else
                    rows[c - 1].className = "griditem";
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

         }
            // alert(rows.length-900);

     }

    </script>
</head>
<body onload="listen_window();" >
    <form id="form1" runat="server">        
        <table width="100%" id="tbl" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3">
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                </td>
            </tr>

            <tr>
                <td></td>
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="ACT_ID" EmptyDataText="No Data" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Code" SortExpression="ACT_ID">
                                <EditItemTemplate>
                                   
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Code
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="ACT_NAME">
                                <EditItemTemplate>
                                   
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Name
                                    <br />
                                    <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("ACT_NAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Type" SortExpression="ACT_TYPE">
                                <HeaderTemplate>
                                    Account Type
                                    <br />
                                    <asp:DropDownList ID="DDAccountType" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="DDAccountType_SelectedIndexChanged">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="ASSET">ASSET</asp:ListItem>
                                        <asp:ListItem Value="EXPENSES">EXPENSES</asp:ListItem>
                                        <asp:ListItem Value="INCOME">INCOME</asp:ListItem>
                                        <asp:ListItem>LIABILITY</asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAccType" runat="server" Text='<%# Bind("ACT_TYPE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bank Or Cash" SortExpression="ACT_BANKCASH">
                                <EditItemTemplate>
                                    
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ACT_BANKCASH") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                     Bank Or Cash<br/>
                                                <asp:DropDownList ID="DDBankorCash" runat="server" AutoPostBack="True" 
                                                    OnSelectedIndexChanged="DDBankorCash_SelectedIndexChanged" >
                                                    <asp:ListItem>All</asp:ListItem>
                                                    <asp:ListItem Value="B">Bank</asp:ListItem>
                                                    <asp:ListItem Value="C">Cash</asp:ListItem>
                                                    <asp:ListItem Value="N">Normal</asp:ListItem>
                                                </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SubGroupId" SortExpression="ACT_SGP_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSGP_ID" runat="server" Text='<%# Bind("ACT_SGP_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SubGroupDescr" SortExpression="SGP_DESCR" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSGP_DESCR" runat="server" Text='<%# Bind("SGP_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem"  />
                        <HeaderStyle CssClass="gridheader_pop"  />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <SelectedRowStyle  />
                    </asp:GridView>
                    
                </td>
                <td></td>
            </tr>           
        </table>

    </form>
</body>
</html>
