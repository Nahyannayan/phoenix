Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GridViewHelper

Partial Class jvViewJournalVoucher
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"

            h_Grid.Value = "top"
            ''''' 
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ((MainMnu_code <> "A150020") And _
            (MainMnu_code <> "A150080") And (MainMnu_code <> "A753010")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Select Case MainMnu_code
                    Case "A150020"
                        lblHeader.Text = "Journal Voucher"
                        ViewState("doctype") = "JV"
                    Case "A150080"
                        lblHeader.Text = "Self Reversing Journal Voucher"
                        ViewState("doctype") = "SJV"
                    Case "A753010"
                        lblHeader.Text = "Year End Adjustments"
                        ViewState("doctype") = "YJV"
                        rbAll.Visible = False
                        rbPosted.Visible = False
                        rbUnposted.Visible = False
                End Select
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
            If Request.QueryString("deleted") <> "" Then
                lblError.Text = "The Journal is successfully deleted"
            End If

            Dim url As String = String.Empty
            Dim datamodeAdd As String = Encr_decrData.Encrypt("add")

            Select Case ViewState("doctype").ToString
                Case "JV"
                    url = "accjvJournalVoucher.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamodeAdd
                Case "SJV"
                    url = "accsjvSJournalVoucher.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamodeAdd
                Case "YJV"
                    url = "accyjvJournalVoucher.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamodeAdd
            End Select
            hlAddNew.NavigateUrl = url
        End If
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If

    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("doctype").ToString
                    Case "JV"
                        hlview.NavigateUrl = "accjvjournalvoucher.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case "SJV"
                        hlview.NavigateUrl = "accsjvSJournalVoucher.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case "YJV"
                        hlview.NavigateUrl = "accyjvJournalVoucher.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select
                'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                hlCEdit.Enabled = True
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = String.Empty 
        Dim lstrRefNo As String = String.Empty
        Dim lstrDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltrefNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim txtSearch As New TextBox
        Dim larrSearchOpr() As String
        If gvJournal.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtrefNo")
            lstrRefNo = Trim(txtSearch.Text)
            If (lstrRefNo <> "") Then lstrFiltrefNo = SetCondn(lstrOpr, "JHD_REFNO", lstrRefNo)
            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtdocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "JHD_DOCNO", lstrDocNo)
            '   --    DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocDATE")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "JHD_DOCDT", lstrDocDate)
            '   --    Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "JHD_Narration", lstrNarration)
            '--   Amount
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            lstrAmount = txtSearch.Text
            If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "TOTAL", lstrAmount)
        End If
        '''''''''''
        Dim str_Filter As String = ""
        Dim str_ListDoc As String = String.Empty

        If Session("ListDays") IsNot Nothing Then
            str_ListDoc = " AND JHD_DOCDT BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
        End If

        If rbPosted.Checked = True Then
            str_Filter = " AND JHD_bPOSTED=1 "
            str_Filter += str_ListDoc
        End If
        If rbUnposted.Checked = True Then
            str_Filter = " AND JHD_bPOSTED=0 "
        End If

        If rbAll.Checked = True Then
            str_Filter = str_ListDoc
        End If
        Dim str_Topfilter As String = ""
        If UsrTopFilter1.FilterCondition <> "All" Then
            str_Topfilter = " top " & UsrTopFilter1.FilterCondition
        End If 
        Select Case ViewState("doctype").ToString
            Case "SJV"
                str_Sql = "SELECT  " & str_Topfilter & " * FROM ( " _
              & " SELECT GUID, SHD_SUB_ID AS JHD_SUB_ID, " _
              & " SHD_BSU_ID AS JHD_BSU_ID, SHD_FYEAR AS JHD_FYEAR,  " _
              & " SHD_DOCTYPE AS JHD_DOCTYPE, SHD_DOCNO AS JHD_DOCNO, " _
              & " SHD_DOCDT AS JHD_DOCDT, SHD_CUR_ID AS JHD_CUR_ID, " _
              & " SHD_EXGRATE1 AS JHD_EXGRATE1, SHD_EXGRATE2 AS JHD_EXGRATE2, " _
              & " SHD_bPOSTED AS JHD_bPOSTED, SHD_bDELETED AS JHD_bDELETED, " _
              & " SHD_REFDOCTYPE AS JHD_REFDOCTYPE, SHD_REFDOCNO AS JHD_REFDOCNO,  " _
              & " SHD_NARRATION AS  JHD_NARRATION, SHD_REFNO AS JHD_REFNO, TOTAL FROM " _
              & " (SELECT GUID, SHD_SUB_ID, SHD_BSU_ID, SHD_FYEAR," _
              & " SHD_DOCTYPE, SHD_DOCNO,  SHD_DOCDT, SHD_CUR_ID,  SHD_EXGRATE1," _
              & " SHD_EXGRATE2, SHD_bPOSTED, SHD_bDELETED,  SHD_REFDOCTYPE, SHD_REFDOCNO," _
              & " SHD_NARRATION,  SHD_REFNO," _
              & " (SELECT SUM(SJL_DEBIT) AS TOTAL FROM SJOURNAL_D AS SJD " _
              & " WHERE ( SJD.SJL_SUB_ID = '" & Session("Sub_ID") & "') AND (SJD.SJL_BSU_ID = '" & Session("sBsuid") & "')  " _
              & " AND (SJD.SJL_DOCTYPE = 'SJV') AND (SJD.SJL_DOCNO = SJH.SHD_DOCNO)) AS TOTAL " _
              & " FROM SJOURNAL_H AS SJH) AS TOTSUB WHERE (SHD_DOCTYPE = 'SJV')  " _
              & " AND (SHD_BSU_ID = '" & Session("sBsuid") & "') AND SHD_FYEAR = " & Session("F_YEAR") _
              & " ) DB WHERE JHD_bDELETED = 0 " & str_Filter _
              & " AND (JHD_DOCTYPE = '" & ViewState("doctype") & "') " _
              & " AND (JHD_BSU_ID = '" & Session("sBsuid") & "') " _
              & lstrFiltrefNo & lstrFiltDocNo & lstrFiltDocDate & lstrFiltNarration & lstrFiltAmount _
              & " ORDER BY JHD_DOCDT DESC, JHD_DOCNO DESC"
            Case "JV"
                str_Sql = "SELECT  " & str_Topfilter & " GUID, JHD_SUB_ID,  JHD_BSU_ID, JHD_FYEAR, " _
               & " JHD_DOCTYPE, JHD_DOCNO,  JHD_DOCDT, JHD_CUR_ID," _
               & " JHD_EXGRATE1, JHD_EXGRATE2, JHD_bPOSTED, JHD_bDELETED," _
               & " JHD_REFDOCTYPE, JHD_REFDOCNO, JHD_NARRATION, JHD_SESSIONID, " _
               & " JHD_LOCK, JHD_TIMESTAMP, JHD_REFNO, TOTAL" _
               & " FROM (SELECT GUID, JHD_SUB_ID, JHD_BSU_ID, JHD_FYEAR," _
               & " JHD_DOCTYPE, JHD_DOCNO, JHD_DOCDT, JHD_CUR_ID, " _
               & " JHD_EXGRATE1, JHD_EXGRATE2, JHD_bPOSTED, JHD_bDELETED, " _
               & " JHD_REFDOCTYPE, JHD_REFDOCNO, JHD_NARRATION, JHD_SESSIONID, " _
               & " JHD_LOCK, JHD_TIMESTAMP, JHD_REFNO, (SELECT SUM(JNL_DEBIT) AS TOTAL" _
               & " FROM JOURNAL_D WHERE (JOURNAL_D.JNL_SUB_ID = '" & Session("Sub_ID") & "') " _
               & " AND (JOURNAL_D.JNL_BSU_ID = '" & Session("sBsuid") & "') " _
               & " AND (JOURNAL_D.JNL_DOCTYPE = '" & ViewState("doctype") & "') " _
               & " AND (JOURNAL_D.JNL_DOCNO = JOURNAL_H.JHD_DOCNO)) AS TOTAL" _
               & " FROM JOURNAL_H) AS TOTSUB WHERE (JHD_bDELETED = 0) AND JHD_FYEAR = " & Session("F_YEAR") & str_Filter _
               & " AND (JHD_DOCTYPE = '" & ViewState("doctype") & "')  AND (JHD_BSU_ID = '" & Session("sBsuid") & "') " _
               & lstrFiltrefNo & lstrFiltDocNo & lstrFiltDocDate & lstrFiltNarration & lstrFiltAmount _
               & " ORDER BY JHD_DOCDT DESC, JHD_DOCNO DESC"
            Case "YJV"
                str_Sql = "SELECT  " & str_Topfilter & " * FROM ( " _
              & " SELECT GUID, YHD_SUB_ID AS JHD_SUB_ID, " _
              & " YHD_BSU_ID AS JHD_BSU_ID, YHD_FYEAR AS JHD_FYEAR,  " _
              & " YHD_DOCTYPE AS JHD_DOCTYPE, YHD_DOCNO AS JHD_DOCNO, " _
              & " YHD_DOCDT AS JHD_DOCDT, YHD_CUR_ID AS JHD_CUR_ID, " _
              & " YHD_EXGRATE1 AS JHD_EXGRATE1, YHD_EXGRATE2 AS JHD_EXGRATE2, " _
              & " YHD_bPOSTED AS JHD_bPOSTED, YHD_bDELETED AS JHD_bDELETED, " _
              & " '' AS JHD_REFDOCTYPE, '' AS JHD_REFDOCNO,  " _
              & " YHD_NARRATION AS  JHD_NARRATION, YHD_REFNO AS JHD_REFNO, TOTAL FROM " _
              & " (SELECT GUID, YHD_SUB_ID, YHD_BSU_ID, YHD_FYEAR," _
              & " YHD_DOCTYPE, YHD_DOCNO,  YHD_DOCDT, YHD_CUR_ID,  YHD_EXGRATE1," _
              & " YHD_EXGRATE2, YHD_bPOSTED, YHD_bDELETED," _
              & " YHD_NARRATION,  YHD_REFNO," _
              & " (SELECT SUM(YJL_DEBIT) AS TOTAL FROM YJOURNAL_D AS SJD " _
              & " WHERE ( SJD.YJL_SUB_ID = '" & Session("Sub_ID") & "') AND (SJD.YJL_BSU_ID = '" & Session("sBsuid") & "')  " _
              & " AND (SJD.YJL_DOCTYPE = 'YJV') AND (SJD.YJL_DOCNO = SJH.YHD_DOCNO)) AS TOTAL " _
              & " FROM YJOURNAL_H AS SJH) AS TOTSUB WHERE (YHD_DOCTYPE = 'YJV')  " _
              & " AND (YHD_BSU_ID = '" & Session("sBsuid") & "') AND YHD_FYEAR = " & Session("F_YEAR") _
              & " ) DB WHERE JHD_bDELETED = 0 " & str_Filter _
              & " AND (JHD_DOCTYPE = '" & ViewState("doctype") & "') " _
              & " AND (JHD_BSU_ID = '" & Session("sBsuid") & "') " _
              & lstrFiltrefNo & lstrFiltDocNo & lstrFiltDocDate & lstrFiltNarration & lstrFiltAmount _
              & " ORDER BY JHD_DOCDT DESC, JHD_DOCNO DESC"
        End Select
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvJournal.DataBind()
            Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

            gvJournal.Rows(0).Cells.Clear()
            gvJournal.Rows(0).Cells.Add(New TableCell)
            gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
            gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvJournal.DataBind()
        End If
        txtSearch = gvJournal.HeaderRow.FindControl("txtrefNo")
        txtSearch.Text = lstrRefNo

        txtSearch = gvJournal.HeaderRow.FindControl("txtDocno")
        txtSearch.Text = lstrDocNo

        txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
        txtSearch.Text = lstrNarration

        txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
        txtSearch.Text = lstrDocDate

        txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
        txtSearch.Text = lstrAmount
        'gvJournal.DataBind()
        gvJournal.SelectedIndex = p_selected_id
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvDetails.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                '        Dim lblGrpCode As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                If ViewState("doctype") = "SJV" Then
                    str_Sql = "select * FROM SJOURNAL_H where GUID='" & lblGUID.Text & "' "
                Else
                    str_Sql = "select * FROM JOURNAL_H where GUID='" & lblGUID.Text & "' "
                End If

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                h_Grid.Value = "detail"

                If ds.Tables(0).Rows.Count > 0 Then
                    If ViewState("doctype") = "SJV" Then
                        str_Sql = "SELECT SJD.GUID, SJD.SJL_SUB_ID AS JNL_SUB_ID," _
                        & " SJD.SJL_BSU_ID AS JNL_BSU_ID, SJD.SJL_FYEAR AS JNL_FYEAR, " _
                        & " SJD.SJL_DOCTYPE AS JNL_DOCTYPE, SJD.SJL_DOCNO AS JNL_DOCNO," _
                        & " SJD.SJL_ACT_ID AS JNL_ACT_ID, SJD.SJL_SLNO AS JNL_SLNO," _
                        & " SJD.SJL_ACT_ID + '-' + AM.ACT_NAME AS ACCOUNT," _
                        & " SJD.SJL_DEBIT AS JNL_DEBIT, SJD.SJL_CREDIT AS JNL_CREDIT, " _
                        & " SJD.SJL_NARRATION AS JNL_NARRATION," _
                        & " AM.ACT_NAME , SJD.SJL_BDELETED" _
                        & " FROM SJOURNAL_D AS SJD INNER JOIN" _
                        & " ACCOUNTS_M AS AM ON SJD.SJL_ACT_ID = AM.ACT_ID AND SJD.SJL_ACT_ID = AM.ACT_ID" _
                        & " WHERE     (SJD.SJL_DOCNO = '" & ds.Tables(0).Rows(0)("SHD_DOCNO") & "') AND (SJD.SJL_SUB_ID = '" & Session("Sub_ID") & "') " _
                        & " AND (SJD.SJL_BSU_ID = '" & Session("sBsuid") & "') AND (SJD.SJL_DOCTYPE = 'SJV')"
                    Else
                        str_Sql = "SELECT JOURNAL_D.GUID, JOURNAL_D.JNL_SUB_ID," _
                              & " JOURNAL_D.JNL_BSU_ID, JOURNAL_D.JNL_FYEAR," _
                              & " JOURNAL_D.JNL_DOCTYPE, JOURNAL_D.JNL_DOCNO," _
                              & " JOURNAL_D.JNL_ACT_ID,JOURNAL_D.JNL_SLNO," _
                              & " JOURNAL_D.JNL_ACT_ID + '-' + ACCOUNTS_M.ACT_NAME" _
                              & " AS ACCOUNT, JOURNAL_D.JNL_DEBIT, " _
                              & " JOURNAL_D.JNL_CREDIT, JOURNAL_D.JNL_NARRATION," _
                              & " ACCOUNTS_M.ACT_NAME, JOURNAL_D.JNL_BDELETED" _
                              & " FROM JOURNAL_D INNER JOIN ACCOUNTS_M" _
                              & " ON JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID " _
                              & " AND JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID" _
                              & " WHERE     (JOURNAL_D.JNL_DOCNO = '" & ds.Tables(0).Rows(0)("JHD_DOCNO") & "') " _
                              & " AND (JOURNAL_D.JNL_SUB_ID = '" & Session("Sub_ID") & "') " _
                              & " AND (JOURNAL_D.JNL_BSU_ID = '" & Session("sBsuid") & "') " _
                              & " AND  (JOURNAL_D.JNL_DOCTYPE = '" & ViewState("doctype") & "')"
                    End If

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub gvChild_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvChild.Sorting

    End Sub

    Protected Sub gvDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvDetails.SelectedIndexChanging
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            gvDetails.SelectedIndex = e.NewSelectedIndex
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label

                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(gvDetails.SelectedRow.FindControl("lblGUID"), Label)
                lblSlno = TryCast(gvDetails.SelectedRow.FindControl("lblSlno"), Label)

                str_Sql = "select * FROM JOURNAL_D where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    str_Sql = "SELECT JOURNAL_D_S.JDS_doctype,JOURNAL_D_S.JDS_DOCNO," _
                    & " JOURNAL_D_S.JDS_DESCR," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, " _
                    & " case isnull(JOURNAL_D_S.JDS_CODE,'') " _
                    & " when '' then 'GENERAL'" _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD," _
                    & " COSTCENTER_S.CCS_DESCR ,JOURNAL_D_S.JDS_SLNO, JOURNAL_D_S.JDS_CODE," _
                    & " JOURNAL_D_S.JDS_AMOUNT, COSTCENTER_S.CCS_QUERY" _
                    & " FROM JOURNAL_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = JOURNAL_D.JNL_ACT_ID " _
                    & " LEFT OUTER JOIN JOURNAL_D_S ON" _
                    & " JOURNAL_D.JNL_SUB_ID=JOURNAL_D_S.JDS_SUB_ID AND" _
                    & " JOURNAL_D.JNL_BSU_ID=   JOURNAL_D_S.JDS_BSU_ID  AND " _
                    & " JOURNAL_D.JNL_FYEAR =JOURNAL_D_S.JDS_FYEAR AND" _
                    & " JOURNAL_D.JNL_DOCTYPE=JOURNAL_D_S.JDS_DOCTYPE AND " _
                    & " JOURNAL_D.JNL_DOCNO=JOURNAL_D_S.JDS_DOCNO  AND " _
                    & " JOURNAL_D.JNL_SLNO=JOURNAL_D_S.JDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  JOURNAL_D_S.JDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  JOURNAL_D_S.JDS_DOCTYPE='" & ViewState("doctype") & "' AND " _
                    & " JOURNAL_D_S.JDS_DOCNO='" & ds.Tables(0).Rows(0)("JNL_DOCNO") & "' AND" _
                    & " JOURNAL_D_S.JDS_SUB_ID='" & Session("Sub_ID") & "' AND" _
                    & " JOURNAL_D_S.JDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " JOURNAL_D_S.JDS_FYEAR='" & Session("F_YEAR") & "'" _
                    & " AND JOURNAL_D_S.JDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)
                    If dsc.Tables(0).Rows.Count > 0 Then
                        h_Grid.Value = "child"
                    Else
                        h_Grid.Value = "detail"
                    End If
                    helper.RegisterSummary("JDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblDocNo As Label
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    repSource = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", lblDocNo.Text, Session("HideCC"))
                    Session("ReportSource") = repSource
                    '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                    ReportLoadSelection()
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
