﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="CustomerAccountMaster.aspx.vb" Inherits="Accounts_CustomerAccountMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Promotions Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>

                        <td align="center">

                            <table id="Table1" runat="server" align="center" width="100%">



                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" width="100%">

                                            <tr>
                                                <td colspan="3" align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3" align="center">
                                                    <asp:GridView ID="gvCusAcctDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCust_Act_Code" runat="server" Text='<%# Bind("CUS_ACT_CODE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Customer Account Code">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblCus_Act_Code" runat="server" Text="Customer Account Code"></asp:Label><br />
                                                                    <asp:TextBox ID="txtCus_Act_Code" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnCus_Act_Code_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnCus_Act_Code_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCUST_ACTT_CODE" runat="server" Text='<%# Bind("CUS_ACT_CODE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Account Code">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblAct_Code" runat="server" Text="Account Code"></asp:Label><br />
                                                                    <asp:TextBox ID="txtAct_Code" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnAct_Code_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnAct_Code_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblACT_ID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Account Name">  
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblAct_Name" runat="server" Text="Account Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtAct_Name" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnAct_Name_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnAct_Name_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblACT_NAME" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                            
                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                        <RowStyle CssClass="griditem" Wrap="False" />
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

