<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accChqCollection.aspx.vb" Inherits="accChqCollection" Title="Untitled Page" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cheque Collection
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValChqDate" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="chkSelectall" type="checkbox" onclick="change_chk_state('0')" /><a href="javascript:change_chk_state('1')">Select All</a>&nbsp;
                        </td>
                    </tr>
                </table>

                <table width="100%" id="tbl">
                    <tr>

                        <td align="center">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="GUID" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" type="checkbox" value='<%# Bind("GUID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch6" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="2%" />
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHD_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            BANK<br />
                                            <asp:TextBox ID="txtBank" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch1" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="2%" />
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblBank" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            CREDITOR<br />
                                            <asp:TextBox ID="txtCreditor" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch2" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditor" runat="server" Text='<%# Bind("PARTY_ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            CHEQUE NO<br />
                                            <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration4" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCheqNo" runat="server" Text='<%# Bind("VHD_CHQNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CHQ. DATE">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtChqDate" runat="server" Width="89px" Text='<%# bind("VHD_CHQDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtChqDate"
                                                ErrorMessage="Enter Proper Date" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                ValidationGroup="ValChqDate">*</asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChqDate"
                                                ErrorMessage="Enter Cheq. Date" ValidationGroup="ValChqDate">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Currency<br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch5" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="1%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("CUR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblamount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHD_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="Center" />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>


                        </td>

                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="ValChqDate" />
                        </td>
                    </tr>

                </table>


            </div>
        </div>
    </div>
</asp:Content>

