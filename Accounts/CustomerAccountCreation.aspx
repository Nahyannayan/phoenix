﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerAccountCreation.aspx.vb" Inherits="Accounts_CustomerAccountCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i>
            <asp:Label Text='Customer Account Creation' ID="lblheading" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div id="divHeading" class="divheadingS_CAL">
                    <div>
                        <asp:HiddenField ID="hidlatestrefid" runat="server" />
                    </div>
                </div>

                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <div>
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            </div>

                        </td>
                    </tr>
                </table>
                <table width="100%" align="center" style="border-collapse: collapse">
                    <tr>
                        <td width="15%" align="left"><span class="field-label">DAX Customer Code</span><span style="color: red">*</span>
                        </td>
                        <td width="35%" align="left">
                            <asp:TextBox ID="txtDAXCustmerCode" runat="server" AutoPostBack="true" OnTextChanged="txtDAXCustmerCode_TextChanged" required="required" autocomplete="off"></asp:TextBox>
                        </td>
                        <td width="15%" align="left"><span class="field-label">Customer Name</span><span style="color: red">*</span>
                        </td>
                        <td width="35%" align="left">
                            <asp:TextBox ID="txtPhnxAcctName" runat="server" required="required" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" align="left"><span class="field-label">Account Code</span>
                        </td>
                        <td width="35%" align="left">
                            <asp:TextBox ID="txtPhnxAcctCode" runat="server" Enabled="false" autocomplete="off"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Tax Registration No.</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTAXRegNo" runat="server" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Credit Days</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCreditDays" runat="server" MaxLength="3" autocomplete="off" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Email</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server" autocomplete="off"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Country</span><span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" required="required"></asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Emirate/City</span><span runat="server" id="spanMandatory" visible="false" style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCity" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Address</span>
                        </td>
                        <td align="left">
                            <%--<asp:TextBox TextMode="MultiLine" Rows="5" ID="txtAddress" runat="server"></asp:TextBox>--%>
                            <textarea aria-multiline="true" id="txtAddress" runat="server"></textarea>
                        </td>
                        <td align="left"><span class="field-label">Telephone</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTelephone" runat="server" MaxLength="12" autocomplete="off" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3" align="left">
                            <asp:CheckBox ID="chkbInterUnit" runat="server" />&nbsp;<span class="field-label">Add As Inter Unit </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkbLeaseCust" runat="server" />&nbsp;<span class="field-label">Add In Lease Customer</span>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkbBaseRateList" runat="server" />&nbsp;<span class="field-label">Add In Base Rate List</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblslctgrade" runat="server" Text="Select Business Unit(s)" CssClass="field-label" /><span style="color: red">*</span>
                        </td>
                        <td colspan="2" align="left">
                            <div class="checkbox-list" runat="server" id="pnlBusinesunits" style="min-height: 250px !important">
                                <asp:TreeView ID="tvBusinessunit" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked();">
                                    <NodeStyle CssClass="treenode" />
                                </asp:TreeView>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="9" />
                            <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save" CausesValidation="true" UseSubmitBehavior="true" OnClick="btnSave_Click" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="10" />
                            <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" CausesValidation="false" data-validate-target="none" UseSubmitBehavior="false" OnClick="btnCancel_Click" />
                            <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="False" Visible="false" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="13" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:HiddenField ID="hfCus_Act_Code" runat="server" Value="0" />
    </div>
    <script type="text/javascript" language="javascript"> 
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
</asp:Content>

