<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccPettycashShow.aspx.vb" Inherits="Accounts_AccPettycashShow" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cash Flow</title>
    <%-- <link rel="stylesheet" type="text/css" href="../cssfiles/title.css" />--%>


    <base target="_self" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />
    <%--<style type="text/css" > 
 .odd{background-color: white;} 
 .even{background-color: gray;} 
</style>--%>
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>




    <script language="javascript" type="text/javascript">
        //not in use
        //function validate_add() {

        //    if (document.getElementById("txtItemname").value == '') {
        //        alert("Kindly enter item Name");
        //        return false;
        //    }


        //    var agree = confirm('Do you really want to Add Item : ' + document.getElementById("txtItemname").value + ' ?');

        //    if (agree)

        //        return true;

        //    else

        //        return false;

        //}
        //function Return(retval) {
        //    window.returnValue = retval;
        //    //alert(retval);
        //    window.close();
        //}
        //function hide(id) {
        //    var tdid = 'tbl_Message' + id;
        //    // alert(tdid);
        //    document.getElementById(tdid).style.display = 'none';
        //}
        //function help(id) {

        //    //var groupcode=


        //    var NameandCode;
        //    var result;
        //    var url;
        //    url = 'ShowError.aspx?id=' + id;
        //    result = radopen(url, "pop_up2");
        //    return false;
        //}
    </script>
    
   <%-- <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> --%>
   
    <script language="javascript" type="text/javascript">
        //not in use
       <%-- function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function validate_add() {

            if (document.getElementById("txtGroupcode").value == '') {
                alert("Kindly enter Group Code");
                return false;
            }
            if (document.getElementById("txtGroupname").value == '') {
                alert("Kindly enter Group Name");
                return false;
            }
            return true;
        }

        function hide(id) {
            var tdid = 'tbl_Message' + id;
            // alert(tdid);
            document.getElementById(tdid).style.display = 'none';
        }

        function help(id) {

            var NameandCode;
            var result;
            var url;
            url = 'ShowError.aspx?id=' + id;
            result = radopen(url, "pop_up");
            return false;
        }


        function MoveCursorDown(e) {
            //alert(event.keyCode);//d=40,u=38
            var start_index = 7;
            var max_index = 19;
            var d = '<%=Request.QueryString("bankcash") %>';
        if (d != '') {
            start_index = 14;
            max_index = 25;
        }
        var c = start_index + 1;
        var i = 1;
        if (event.keyCode == 32 && event.ctrlKey) {
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            if (c <= 0 || c == -1 || c <= start_index) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == (start_index + 1) + '';
                c = start_index;
            }
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");
            var len = rows.length;
            if (len == max_index) len = len - 2;

            if (c < len) {
                var retval;
                cols = rows[c].getElementsByTagName("span");
                //alert('done'+cols[0].innerHTML);
                retval = cols[0].innerText + "___";
                cols = rows[c].getElementsByTagName("a");
                //alert('done'+cols[0].innerHTML);
                retval = retval + cols[0].innerText;
                //
                window.returnValue = retval;
                window.close();
            }
        }

        if (event.keyCode == 40 || event.keyCode == 38) {
            if (event.keyCode == 38) i = -1;

            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            c = c + i;
            if (c <= 0 || c == -1 || c <= start_index) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == (start_index + 1) + '';
                c = start_index;
            }
            selectedRow = null;
            var index = 0;

            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");
            var len = rows.length;
            if (len == max_index) len = len - 2;

            if (c < rows.length) {
                cols1 = rows[c].getElementsByTagName("a");
                if (cols1 != null)
                    if (cols1.length > 0)
                        if (cols1[0].innerText == '1') len = len - 2;
            }
            if (c < len) {
                rows[c].className = "griditem_hilight";
                if (c % 2 == 0)
                { if (c != start_index) rows[c - 1].className = "griditem_alternative"; }
                else
                { if (c != start_index) rows[c - 1].className = "griditem"; }
                if (event.keyCode == 38 && c + 1 < len) {
                    rows[c + 1].className = "griditem";
                }
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

            }
            // alert(rows.length-900);
        }
    }--%>

    </script>
</head>
<body onload="listen_window();" >
    <form id="form1" runat="server">  
        <asp:ScriptManager ID="scriptmanager" runat="server" />     
        <table width="100%" id="tbl">
            <tr>
               
                <td align="center" colspan="5">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Amount" SortExpression="PCH_APV_AMOUNT">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Amount
                                        <br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("PCH_APV_AMOUNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Employee" SortExpression="EMP_FNAME">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Employee
                                        <br />
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("EMP_FNAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ID" Visible="False">

                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("PCH_ID") %>'></asp:Label>
                                    <asp:Label ID="lbRemarks" runat="server" Text='<%# Eval("PCH_REMARKS") %>'></asp:Label>
                                    <asp:Label ID="lbActid" runat="server" Text='<%# Eval("TYP_ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                    </asp:GridView>
                  
                </td>
               
            </tr>
        </table>
        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />


    </form>
</body>
</html>
