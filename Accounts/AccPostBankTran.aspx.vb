Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class AccPostBankTran
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If Page.IsPostBack = False Then
            lblError.Text = ""
            '''''check menu rights
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            Page.Title = OASISConstants.Gemstitle
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200011" And ViewState("MainMnu_code") <> "A200012" And ViewState("MainMnu_code") <> "A200013" And ViewState("MainMnu_code") <> "A200016" And ViewState("MainMnu_code") <> "A200017") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Page.Title = OASISConstants.Gemstitle
                If ViewState("MainMnu_code") = "A200011" Then
                    lblHead.Text = "POST BANK PAYMENTS"
                    ViewState("str_doctype") = "BP"
                ElseIf ViewState("MainMnu_code") = "A200012" Then
                    lblHead.Text = "POST BANK RECEIPTS "
                    ViewState("str_doctype") = "BR"
                ElseIf ViewState("MainMnu_code") = "A200013" Then
                    lblHead.Text = "POST PDC"
                    ViewState("str_doctype") = "BP"
                ElseIf ViewState("MainMnu_code") = "A200016" Then
                    lblHead.Text = "POST RECURRING JV"
                    ViewState("str_doctype") = "RJV"
                ElseIf ViewState("MainMnu_code") = "A200017" Then
                    lblHead.Text = "POST PURCHASE JOURNAL"
                    ViewState("str_doctype") = "PJ"
                End If
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
        If h_SecondTime.Value = "True" Then
            Select Case ViewState("MainMnu_code").ToString
                Case "A200011"
                    Print_BP(h_FirstVoucher.Value)
                Case "A200013"
                    Print_PDC(h_FirstVoucher.Value)
            End Select
            'h_SecondTime.Value = ""
        End If
    End Sub

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        ' Dim i As New Encryption64
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                        If ViewState("MainMnu_code") = "A200011" Then
                            hlview.NavigateUrl = "AccAddBankTran.aspx?Eid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        ElseIf ViewState("MainMnu_code") = "A200012" Then
                            hlview.NavigateUrl = "AccBankR.aspx?Eid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        ElseIf ViewState("MainMnu_code") = "A200013" Then
                            hlview.NavigateUrl = "AccAddPDC.aspx?Eid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        ElseIf ViewState("MainMnu_code") = "A200016" Then
                            hlview.NavigateUrl = "AccRJV.aspx?Eid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        ElseIf ViewState("MainMnu_code") = "A200017" Then
                            hlview.NavigateUrl = "AccPJ.aspx?Eid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        End If
                        hlCEdit.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        h_FirstVoucher.Value = ""
        h_SecondTime.Value = ""
        If (Not chkPrint.Checked) Or (Not ChkPrintNotice.Checked) Then
            h_SecondTime.Value = "True"
        End If
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            Select Case ViewState("MainMnu_code").ToString
                Case "A200011" ' POST BANK PAYMENTS 
                    Print_BP(h_FirstVoucher.Value)
                Case "A200012" ' POST BANK RECEIPTS 
                    Print_BR(h_FirstVoucher.Value)
                Case "A200013" ' POST PDC 
                    Print_PDC(h_FirstVoucher.Value)
                Case "A200017" ' POST PURCHASE JOURNAL 
                    Print_PJ(h_FirstVoucher.Value)
                Case "A200016" ' POST PURCHASE JOURNAL 
                    Print_RJV(h_FirstVoucher.Value)
            End Select
        End If
        If ChkPrintNotice.Checked And (ViewState("MainMnu_code") = "A200011" Or ViewState("MainMnu_code") = "A200013") Then
            PrintChequeFwdLetter(h_FirstVoucher.Value)
        End If
        gridbind()
    End Sub

    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim lstrSUBID, lstrBSUID, lstrFYear, lstrDocType, lstrDocNo As String
            Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim str_Sql As String
                Dim lstrProc As String
                If (ViewState("str_doctype") = "RJV") Then
                    str_Sql = "select RJH_SUB_ID as  VHH_SUB_ID,RJH_BSU_ID as VHH_BSU_ID,RJH_FYEAR as VHH_FYEAR,RJH_DOCNO as VHH_DOCNO FROM RJOURNAL_H where GUID='" & p_guid & "' "
                    lstrProc = "POSTRJOURNAL"
                    lstrSUBID = "@RJH_SUB_ID"
                    lstrBSUID = "@RJH_BSU_ID"
                    lstrFYear = "@RJH_FYEAR"
                    lstrDocType = "@RJH_DOCTYPE"
                    lstrDocNo = "@RJH_DOCNO"

                ElseIf (ViewState("str_doctype") = "PJ") Then
                    str_Sql = "select PUH_SUB_ID as  VHH_SUB_ID,PUH_BSU_ID as VHH_BSU_ID,PUH_FYEAR as VHH_FYEAR,PUH_DOCNO as VHH_DOCNO FROM PURCHASE_H where GUID='" & p_guid & "' "
                    lstrProc = "POSTPURCHASE"
                    lstrSUBID = "@PUH_SUB_ID"
                    lstrBSUID = "@PUH_BSU_ID"
                    lstrFYear = "@PUH_FYEAR"
                    lstrDocType = "@PUH_DOCTYPE"
                    lstrDocNo = "@PUH_DOCNO"
                Else
                    str_Sql = "select * FROM VOUCHER_H where GUID='" & p_guid & "' "
                    lstrProc = "POSTVOUCHER"
                    lstrSUBID = "@VHH_SUB_ID"
                    lstrBSUID = "@VHH_BSU_ID"
                    lstrFYear = "@VHH_FYEAR"
                    lstrDocType = "@VHH_DOCTYPE"
                    lstrDocNo = "@VHH_DOCNO"
                End If

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand(lstrProc, objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter(lstrSUBID, SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("VHH_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter(lstrBSUID, SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter(lstrFYear, SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    If ViewState("str_doctype") <> "RJV" Then
                        Dim sqlpJHD_DOCTYPE As New SqlParameter(lstrDocType, SqlDbType.VarChar, 20)
                        sqlpJHD_DOCTYPE.Value = ViewState("str_doctype")
                        cmd.Parameters.Add(sqlpJHD_DOCTYPE)
                    End If

                    Dim sqlpJHD_DOCNO As New SqlParameter(lstrDocNo, SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    'Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    Dim STATUS As Integer
                    STATUS = retValParam.Value
                    cmd.Parameters.Clear()

                    Dim vDOC_NO As String = ds.Tables(0).Rows(0)("VHH_DOCNO")
                    If ViewState("str_doctype") = "BP" Or ViewState("str_doctype") = "BR" Then
                        Dim vSUB_ID As String = ds.Tables(0).Rows(0)("VHH_SUB_ID")
                        Dim vVHH_CUR_ID As String = ds.Tables(0).Rows(0)("VHH_CUR_ID")
                        Dim vF_YEAR As String = ds.Tables(0).Rows(0)("VHH_FYEAR")
                        Dim vDOC_DATE As String = ds.Tables(0).Rows(0)("VHH_DOCDT")
                        Dim Earn_ded As String = "D"
                        Select Case ViewState("str_doctype").ToString
                            Case "BP"
                                Earn_ded = "D"
                            Case "BR"
                                Earn_ded = "E"
                        End Select
                        If STATUS = 0 Then AccountsDetails.SaveMonthlyData_D(ViewState("str_doctype"), Earn_ded, Session("sBSUID"), vDOC_NO, vSUB_ID, vVHH_CUR_ID, vF_YEAR, vDOC_DATE, objConn, stTrans)
                    End If

                    Dim PRP_BSU_ID As String = ds.Tables(0).Rows(0)("VHH_BSU_ID")
                    Dim PRP_FYEAR As Integer = ds.Tables(0).Rows(0)("VHH_FYEAR")
                    If lblHead.Text = "POST BANK PAYMENTS" Then
                        Dim str_query As String
                        Dim dsCUT As DataSet
                        Dim VHDid As Integer

                        Dim d1, d2 As Date
                        Dim t As TimeSpan
                        Dim PRP_NOOFINST As Integer
                        'Dim PRP_ID As String = ds.Tables(0).Rows(0)("VHH_DOCNO")
                        Dim PRP_SUB_ID As String = ds.Tables(0).Rows(0)("VHH_SUB_ID")
                        Dim PRP_PREPAYACC As String
                        Dim PRP_EXPENSEACC As String
                        Dim PRP_DOCDT As DateTime = Format(Date.Parse(ds.Tables(0).Rows(0)("VHH_DOCDT")), "dd/MMM/yyyy")
                        Dim PRP_FTFROM As DateTime
                        Dim PRP_DTTO As DateTime
                        Dim PRP_AMOUNT As Decimal
                        Dim PRP_CUR_ID As String = ds.Tables(0).Rows(0)("VHH_CUR_ID")
                        Dim PRP_EXGRATE1 As Decimal = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                        Dim PRP_EXGRATE2 As Decimal = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                        Dim PRP_CUT_ID As String

                        Dim PRP_CRNARRATION As String = ""
                        Dim PRP_DRNARRATION As String = ""
                        Dim PRP_TAXCODE As String = ""
                        Dim bEdit As Boolean

                        Dim dsSub As New DataSet
                        Dim j As Integer
                        str_Sql = "SELECT * FROM VOUCHER_D WHERE VHD_BSU_ID='" & PRP_BSU_ID & "' AND VHD_FYEAR=" & PRP_FYEAR & " AND VHD_DOCTYPE='" & ViewState("str_doctype") & "' AND VHD_DOCNO='" & vDOC_NO & "'"
                        dsSub = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
                        If dsSub.Tables(0).Rows.Count > 0 Then
                            For j = 0 To dsSub.Tables(0).Rows.Count - 1
                                VHDid = dsSub.Tables(0).Rows(j)("VHD_ID")
                                PRP_PREPAYACC = dsSub.Tables(0).Rows(j)("VHD_ACT_ID")
                                PRP_CRNARRATION = dsSub.Tables(0).Rows(j)("VHD_NARRATION")
                                PRP_DRNARRATION = dsSub.Tables(0).Rows(j)("VHD_NARRATION")
                                PRP_TAXCODE = dsSub.Tables(0).Rows(j)("VHD_TAX_CODE")
                                str_query = "SELECT * FROM VOUCHER_D_SUB WHERE VSB_VHD_ID=" & VHDid & ""
                                dsCUT = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_query)
                                If dsCUT.Tables(0).Rows.Count > 0 Then
                                    d1 = dsCUT.Tables(0).Rows(0)("VSB_DTFROM")
                                    d2 = dsCUT.Tables(0).Rows(0)("VSB_DTTO")
                                    PRP_FTFROM = Format(Date.Parse(d1), "dd/MMM/yyyy")
                                    PRP_DTTO = Format(Date.Parse(d2), "dd/MMM/yyyy")
                                    t = d2 - d1
                                    PRP_CUT_ID = dsCUT.Tables(0).Rows(0)("VSB_CUT_ID")

                                    Dim totMonth As Integer = DateDiff("m", d1, d2) + 1
                                    Dim totdays As Integer = DateDiff("d", d1, d2) + 1
                                    Dim firstdayLastMonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d2), Date.MinValue)
                                    Dim LastDayofFirstMonth As Date = DateAdd(DateInterval.Second, -3, DateAdd(DateInterval.Month, DateDiff("m", Date.MinValue, d1) + 1, Date.MinValue)).Date
                                    Dim remfirstday As Integer = DateDiff("d", d1, LastDayofFirstMonth) + 1
                                    Dim remLastday As Integer = DateDiff("d", firstdayLastMonth, d2) + 1
                                    Dim totalAmt As Double
                                    Dim PRD_ID As Integer

                                    Dim PRP_NEWDOCNO As String = ""
                                    Dim PRD_DOCDT As String
                                    Dim PRP_ALLOCAMT As String
                                    Dim decFirstMonthAmount As Decimal = 0
                                    Dim Guid As Guid
                                    Dim PRP_JVNO As String = String.Empty
                                    Dim i As Integer

                                    totalAmt = dsSub.Tables(0).Rows(j)("VHD_AMOUNT")
                                    PRP_AMOUNT = dsSub.Tables(0).Rows(j)("VHD_AMOUNT")
                                    Dim copyTotalAmt As Double = totalAmt
                                    bEdit = 0

                                    If totMonth > 2 Then
                                        Dim perDayAmt As Double = totalAmt / totdays
                                        perDayAmt = perDayAmt
                                        Dim firstMonthAmt As Double = remfirstday * perDayAmt
                                        firstMonthAmt = Math.Round(firstMonthAmt, 0)

                                        Dim lastMonthAmt As Double = remLastday * perDayAmt
                                        lastMonthAmt = Math.Round(lastMonthAmt, 0)
                                        Dim total_remAmt4RemMonth As Double
                                        total_remAmt4RemMonth = 0
                                        PRP_NOOFINST = totMonth

                                        PRP_EXPENSEACC = GetAccountId(dsCUT.Tables(0).Rows(0)("VSB_CUT_ID"), stTrans)
                                        STATUS = VoucherFunctions.SavePREPAYMENTS_H("", PRP_SUB_ID, PRP_BSU_ID, _
                                        PRP_FYEAR, "", PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, PRP_DTTO, _
                                        PRP_AMOUNT, "", PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, PRP_EXGRATE2, "", _
                                        PRP_CUT_ID, PRP_CRNARRATION, PRP_DRNARRATION, "", False, True, PRP_NEWDOCNO, _
                                        vDOC_NO, "BP", "", PRP_TAXCODE, stTrans)
                                        UtilityObj.operOnAudiTable(Master.MenuName, PRP_NEWDOCNO, _
                                        "Pre Payment Voucher from BP", Page.User.Identity.Name.ToString, Me.Page)
                                        If STATUS = 0 Then
                                            For ii As Integer = 0 To dsCUT.Tables(0).Rows.Count - 1
                                                STATUS = VoucherFunctions.UpdateVOUCHER_D_SUB(VHDid, PRP_NEWDOCNO, True, stTrans)
                                            Next
                                        End If
                                        Dim dtMonthend As Date
                                        dtMonthend = CDate(GetFREEZEDT(Session("sBsuid")))

                                        PRD_DOCDT = LastDayofFirstMonth
                                        PRP_ALLOCAMT = firstMonthAmt
                                        If LastDayofFirstMonth <= dtMonthend Then
                                            decFirstMonthAmount = decFirstMonthAmount + firstMonthAmt
                                        Else
                                            STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRP_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                        End If
                                        For i = 1 To (totMonth - 2)
                                            Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(i)), Date.MinValue)
                                            Dim lastNextMonth As Date

                                            lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))
                                            Dim remAmt4RemMonth As Double = perDayAmt * Day(lastNextMonth) ' DateDiff("d", nextmonth, lastNextMonth) + 1   ' Dim totalDay_InMonth As Integer = 0
                                            remAmt4RemMonth = Math.Round(remAmt4RemMonth, 0)
                                            total_remAmt4RemMonth = total_remAmt4RemMonth + remAmt4RemMonth
                                            PRD_DOCDT = lastNextMonth

                                            If lastNextMonth <= dtMonthend Then
                                                decFirstMonthAmount = decFirstMonthAmount + remAmt4RemMonth
                                            Else
                                                PRP_ALLOCAMT = remAmt4RemMonth + decFirstMonthAmount
                                                decFirstMonthAmount = 0
                                                STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRP_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                            End If
                                        Next
                                        PRD_DOCDT = DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth))
                                        Dim checkAmtFinal As Double = firstMonthAmt + lastMonthAmt + total_remAmt4RemMonth
                                        Dim AmtDiff As Double = copyTotalAmt - checkAmtFinal
                                        Dim addedToLastMonthAmt As Double = lastMonthAmt + AmtDiff
                                        PRP_ALLOCAMT = addedToLastMonthAmt
                                        STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRP_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                    End If

                                    If STATUS = 0 Then
                                        Dim cmdPost As New SqlCommand("POSTPREPAYMENT", objConn, stTrans)
                                        cmdPost.CommandType = CommandType.StoredProcedure

                                        Dim sqlpJHDSUB_ID As New SqlParameter("@PRP_SUB_ID", SqlDbType.VarChar, 20)
                                        sqlpJHDSUB_ID.Value = PRP_SUB_ID & ""
                                        cmdPost.Parameters.Add(sqlpJHDSUB_ID)

                                        Dim sqlpJHDBSU_ID As New SqlParameter("@PRP_BSU_ID", SqlDbType.VarChar, 20)
                                        sqlpJHDBSU_ID.Value = PRP_BSU_ID & ""
                                        cmdPost.Parameters.Add(sqlpJHDBSU_ID)

                                        Dim sqlpJHDFYEAR As New SqlParameter("@PRP_FYEAR", SqlDbType.Int)
                                        sqlpJHDFYEAR.Value = PRP_FYEAR & ""
                                        cmdPost.Parameters.Add(sqlpJHDFYEAR)

                                        Dim sqlpJHDDOCNO As New SqlParameter("@PRP_DOCNO", SqlDbType.VarChar, 20)
                                        sqlpJHDDOCNO.Value = PRP_NEWDOCNO & ""
                                        cmdPost.Parameters.Add(sqlpJHDDOCNO)

                                        Dim retValue As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                                        retValue.Direction = ParameterDirection.ReturnValue
                                        cmdPost.Parameters.Add(retValue)

                                        cmdPost.ExecuteNonQuery()

                                        STATUS = retValue.Value
                                        cmdPost.Parameters.Clear()
                                    End If
                                End If
                            Next
                        End If
                    End If
                    If (STATUS = 0) Then
                        'stTrans.Rollback()
                        stTrans.Commit()
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                        End If
                        lblError.Text = "Successfully Posted"
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(STATUS)
                    End If
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            'str_Sql = "SELECT  *  FROM  VOUCHER_H" _
            str_Sql = "SELECT     VH.GUID, VH.VHH_SUB_ID, " _
                   & " VH.VHH_BSU_ID, VH.VHH_FYEAR, " _
                   & " VH.VHH_DOCTYPE, VH.VHH_DOCNO," _
                   & " VH.VHH_TYPE, VH.VHH_CHB_ID," _
                   & " VH.VHH_DOCDT, VH.VHH_CHQDT," _
                   & " VH.VHH_ACT_ID, VH.VHH_NOOFINST," _
                   & " VH.VHH_MONTHINTERVEL, VH.VHH_PARTY_ACT_ID, " _
                   & " VH.VHH_INSTAMT, VH.VHH_INTPERCT," _
                   & " VH.VHH_bINTEREST, VH.VHH_CALCTYP," _
                   & " VH.VHH_INT_ACT_ID, VH.VHH_ACRU_INT_ACT_ID, " _
                   & " VH.VHH_CHQ_pdc_ACT_ID, VH.VHH_PROV_ACT_ID," _
                   & " VH.VHH_COL_ACT_ID, VH.VHH_CUR_ID, VH.VHH_EXGRATE1," _
                   & " VH.VHH_EXGRATE2, VH.VHH_NARRATION, VH.VHH_bDELETED," _
                   & " VH.VHH_bPOSTED, VH.VHH_SESSIONID, VH.VHH_LOCK," _
                   & " VH.VHH_TIMESTAMP, VH.VHH_bPDC, VH.VHH_COL_ID," _
                   & " VH.VHH_AMOUNT, VH.VHH_Count, VH.VHH_CRR_ID," _
                   & " VH.VHH_BANKCHARGE, VH.VHH_bImport, VH.VHH_bauto," _
                   & " (VH.VHH_ACT_ID + ' - ' + AM.ACT_NAME) AS ACCOUNT" _
                   & " FROM  VOUCHER_H AS VH INNER JOIN" _
                   & " ACCOUNTS_M AS AM ON VH.VHH_ACT_ID = AM.ACT_ID" _
                   & " where VHH_SUB_ID='" & Session("Sub_ID") & "' and " _
                   & " VHH_BSU_ID='" & Session("sBsuid") & "' AND VH.VHH_FYEAR = " & Session("F_YEAR") _
                   & " AND VHH_bDELETED=0 AND VHH_bPOSTED=0  "
            If (ViewState("MainMnu_code") = "A200011") Then
                str_Sql = str_Sql & " AND VHH_DOCTYPE='BP' AND VHH_bPDC='False' ORDER BY VHH_DOCDT DESC,VHH_DOCNO DESC"
            ElseIf (ViewState("MainMnu_code") = "A200012") Then
                str_Sql = str_Sql & " AND VHH_DOCTYPE='BR' AND VHH_bPDC='False' ORDER BY VHH_DOCDT DESC,VHH_DOCNO DESC"
            ElseIf (ViewState("MainMnu_code") = "A200013") Then
                str_Sql = str_Sql & " AND VHH_DOCTYPE='BP' AND VHH_bPDC='True' ORDER BY VHH_DOCDT DESC,VHH_DOCNO DESC"
            ElseIf ViewState("MainMnu_code") = "A200016" Then
                gvJournal.Columns(3).Visible = False
                'str_Sql = "SELECT  GUID,RJH_DOCTYPE as VHH_DOCTYPE,RJH_DOCNO as VHH_DOCNO,RJH_DOCDT as VHH_DOCDT, " _
                '   & " RJH_NARRATION as VHH_NARRATION,RJH_bPOSTED as VHH_bPosted  FROM  RJOURNAL_H" _
                str_Sql = "SELECT  GUID," _
                & " RJH_DOCTYPE AS VHH_DOCTYPE," _
                & " RJH_DOCNO AS VHH_DOCNO," _
                & " RJH_DOCDT AS VHH_DOCDT," _
                & " RJH_NARRATION AS VHH_NARRATION, " _
                & " RJH_bPOSTED AS VHH_bPosted, " _
                & " RJH_CUR_ID AS VHH_CUR_ID," _
                & " (SELECT     SUM(RJL_DEBIT) AS Expr1" _
                & " FROM RJOURNAL_D" _
                & " WHERE  (RJL_SUB_ID = '" & Session("Sub_ID") & "') " _
                & " AND (RJL_BSU_ID = '" & Session("sBsuid") & "')" _
                & " AND (RJL_DOCNO = RJOURNAL_H.RJH_DOCNO)) AS VHH_AMOUNT,'' AS ACCOUNT" _
                & " FROM         RJOURNAL_H" _
                   & " where RJH_SUB_ID='" & Session("Sub_ID") & "' and " _
                   & " RJH_BSU_ID='" & Session("sBsuid") & "' AND RJH_FYEAR = " & Session("F_YEAR") _
                   & " AND RJH_bDELETED=0 AND RJH_bPOSTED=0  ORDER BY RJH_DOCDT DESC,RJH_DOCNO DESC"
            ElseIf ViewState("MainMnu_code") = "A200017" Then
                str_Sql = "SELECT     PURCHASE_H.GUID," _
                & " PURCHASE_H.PUH_DOCTYPE AS VHH_DOCTYPE, " _
                & " PURCHASE_H.PUH_DOCNO AS VHH_DOCNO," _
                & " PURCHASE_H.PUH_DOCDT AS VHH_DOCDT," _
                & " PURCHASE_H.PUH_NARRATION AS VHH_NARRATION, " _
                & " PURCHASE_H.PUH_bPOSTED AS VHH_bPosted," _
                & " PURCHASE_H.PUH_PARTY_ACT_ID + ' - ' + " _
                & " ACCOUNTS_M.ACT_NAME AS ACCOUNT, " _
                & " PURCHASE_H.PUH_CUR_ID AS VHH_CUR_ID," _
                & " (SELECT     SUM(PUD_QTY*PUD_RATE)" _
                & " FROM PURCHASE_D WHERE (PUD_SUB_ID = '" & Session("Sub_ID") & "') " _
                & " AND (PUD_BSU_ID = '" & Session("sBsuid") & "') " _
                & " AND (PUD_DOCNO = PURCHASE_H.PUH_DOCNO))  " _
                & " AS VHH_AMOUNT FROM PURCHASE_H " _
                & " INNER JOIN ACCOUNTS_M ON " _
                & " PURCHASE_H.PUH_PARTY_ACT_ID = ACCOUNTS_M.ACT_ID" _
               & " where PUH_SUB_ID='" & Session("Sub_ID") & "' and " _
               & " PUH_BSU_ID='" & Session("sBsuid") & "' AND PUH_FYEAR = " & Session("F_YEAR") _
               & " AND PUH_bDELETED=0 AND PUH_bPOSTED=0 ORDER BY PUH_DOCDT DESC,PUH_DOCNO DESC "
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
                chkPrint.Visible = True
                If ViewState("MainMnu_code") = "A200011" Or ViewState("MainMnu_code") = "A200013" Then
                    ChkPrintNotice.Enabled = True
                    ChkPrintNotice.Visible = True
                Else
                    ChkPrintNotice.Enabled = False
                    ChkPrintNotice.Visible = False
                End If
            Else
                btnPost.Visible = False
                chkPrint.Visible = False
                ChkPrintNotice.Enabled = False
                ChkPrintNotice.Visible = False
            End If
            gvJournal.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                str_Sql = "select * FROM VOUCHER_H where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT * FROM VOUCHER_D WHERE VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'" _
                   & " AND VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "' AND VHD_BSU_ID='" _
                   & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "' AND VHD_DOCTYPE='CR'"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbViewChild_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                lblSlno = TryCast(sender.FindControl("lblSlno"), Label)
                str_Sql = "select * FROM VOUCHER_D where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT    VOUCHER_D_S.vds_doctype,VOUCHER_D_S.vds_docno," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME,  " _
                    & " VOUCHER_D_S.VDS_DESCR, " _
                    & " case isnull(VOUCHER_D_S.VDS_CODE,'') " _
                    & " when '' then 'GENERAL' " _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR , VOUCHER_D_S.VDS_CODE, " _
                    & " VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = VOUCHER_D.VHD_ACT_ID " _
                    & " LEFT OUTER JOIN VOUCHER_D_S ON " _
                    & " VOUCHER_D.VHD_SUB_ID=VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " VOUCHER_D.VHD_BSU_ID=   VOUCHER_D_S.VDS_BSU_ID  AND " _
                    & " VOUCHER_D.VHD_FYEAR =VOUCHER_D_S.VDS_FYEAR AND " _
                    & " VOUCHER_D.VHD_DOCTYPE=VOUCHER_D_S.VDS_DOCTYPE AND " _
                    & " VOUCHER_D.VHD_DOCNO=VOUCHER_D_S.VDS_DOCNO  AND " _
                    & " VOUCHER_D.VHD_LINEID = VOUCHER_D_S.VDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  VOUCHER_D_S.VDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  VOUCHER_D_S.VDS_DOCTYPE='CR' AND " _
                    & " VOUCHER_D_S.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHD_DOCNO") & "' AND" _
                    & " VOUCHER_D_S.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHD_SUB_ID") & "' AND" _
                    & " VOUCHER_D_S.VDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " VOUCHER_D_S.VDS_FYEAR = " & Session("F_YEAR") _
                    & "AND VDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub

    Protected Sub Print_PDC(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.PDCVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", p_Docno, False, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub Print_BP(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", p_Docno, False, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Sub Print_PJ(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.PurchaseJournalVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PJ", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub Print_BR(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BR", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub Print_RJV(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.RecurringJournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "RJV", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Private Function GetAccountId(ByVal CUTid As Integer, ByVal strTrans As SqlTransaction) As String
        Dim strSql As String
        Dim strconn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim strAccountId As String
        Dim ds As DataSet
        strSql = "SELECT CUT_EXP_ACT_ID FROM COSTUNIT_M WHERE CUT_ID=" & CUTid & ""
        ds = SqlHelper.ExecuteDataset(strconn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            strAccountId = ds.Tables(0).Rows(0)("CUT_EXP_ACT_ID")
        Else
            strAccountId = ""
        End If
        Return strAccountId
    End Function

    Private Function GetPrepaymentDetails(ByVal BSU_ID As String, ByVal FYear As Integer, ByVal Docdate As DateTime, ByVal Doctype As String, ByVal DocNo As String, ByVal strTrans As SqlTransaction) As Boolean
        Dim str_sql As String
        Dim strconn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As DataSet
        Dim i As Integer
        Dim STATUS As Integer
        Dim strPRPID As String, strBSUID As String, DOCDT As DateTime, AllocAmt As Decimal
        str_sql = "SELECT * FROM VOUCHER_D WHERE VHD_BSU_ID='" & BSU_ID & "' AND VHD_FYEAR=" & FYear & " AND VHD_DOCTYPE='" & Doctype & "' AND VHD_DOCNO='" & DocNo & "'"
        ds = SqlHelper.ExecuteDataset(strconn, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1

                strPRPID = ds.Tables(0).Rows(i)("VHD_DOCNO")
                strBSUID = ds.Tables(0).Rows(i)("VHD_BSU_ID")
                DOCDT = Format(Date.Parse(Docdate), "dd/MMM/yyyy")
                AllocAmt = ds.Tables(0).Rows(i)("VHD_AMOUNT")
                STATUS = VoucherFunctions.SavePREPAYMENTS_D(System.Guid.Empty, 0, strPRPID, strBSUID, FYear, DOCDT, AllocAmt, "", False, strTrans)


            Next
        End If
        If STATUS <> 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function GetDocNo(ByVal BSUID As String, ByVal FYear As Integer, ByVal Doctype As String, ByVal docno As String)
        Dim strSql As String
        Dim strcon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As DataSet
        Dim VhdID As Integer
        strSql = "SELECT VHD_ID FROM VOUCHER_D WHERE VHD_BSU_ID='" & BSUID & "' AND VHD_FYEAR=" & FYear & " AND VHD_DOCTYPE='" & Doctype & "' AND VHD_DOCNO='" & docno & "'"
        ds = SqlHelper.ExecuteDataset(strcon, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            VhdID = ds.Tables(0).Rows(0)("VHD_ID")
        End If
        Return VhdID
    End Function

    Private Function GetFREEZEDT(ByVal BSU_ID As String) As String
        Dim sqlString As String = "SELECT     BSU_FREEZEDT FROM BUSINESSUNIT_M where  BSU_ID='" & BSU_ID & "'"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)

        End Using
        Return CStr(result)
    End Function

    Private Function GetAccountID(ByVal BSUID As String, ByVal FYear As Integer, ByVal Doctype As String, ByVal docno As String) As String
        Dim strQuery As String
        Dim ds1 As DataSet
        Dim strAccount As String = String.Empty
        Dim strcon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        strQuery = "SELECT VHD_ACT_ID FROM VOUCHER_D WHERE VHD_BSU_ID='" & BSUID & "' AND VHD_FYEAR=" & FYear & " AND VHD_DOCTYPE='" & Doctype & "' AND VHD_DOCNO='" & docno & "'"
        ds1 = SqlHelper.ExecuteDataset(strcon, CommandType.Text, strQuery)
        If ds1.Tables(0).Rows.Count > 0 Then
            strAccount = ds1.Tables(0).Rows(0)("VHD_ACT_ID")
        End If
        Return strAccount

    End Function

    Private Sub PrintChequeFwdLetter(ByVal p_Docno As String)
        Dim strSql As String = String.Empty
        Dim strCurID As String = String.Empty
        Dim strCurDesc As String = String.Empty
        Dim strCurDeno As String = String.Empty
        Dim ds As New DataSet
        Dim strCon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim cmd As New SqlCommand
        If Session("sBsuid") <> "" Then
            Dim str_sql As String
            Dim dsCur As DataSet
            str_sql = "SELECT vw_OSO_BUSINESSUNIT_M.BSU_ID, CURRENCY_M.CUR_ID, CURRENCY_M.CUR_DESCR, CURRENCY_M.CUR_DENOMINATION FROM CURRENCY_M CROSS JOIN vw_OSO_BUSINESSUNIT_M where bsu_id='" & Session("sBsuid") & "' AND CUR_ID='AED'"
            dsCur = SqlHelper.ExecuteDataset(strCon, CommandType.Text, str_sql)
            If dsCur.Tables(0).Rows.Count > 0 Then
                strCurID = IIf(IsDBNull(dsCur.Tables(0).Rows(0)("CUR_ID")) = False, dsCur.Tables(0).Rows(0)("CUR_ID"), "")
                strCurDesc = IIf(IsDBNull(dsCur.Tables(0).Rows(0)("CUR_DESCR")) = False, dsCur.Tables(0).Rows(0)("CUR_DESCR"), "")
                strCurDeno = IIf(IsDBNull(dsCur.Tables(0).Rows(0)("CUR_DENOMINATION")) = False, dsCur.Tables(0).Rows(0)("CUR_DENOMINATION"), "")
            End If
        End If
        strSql = "SELECT * FROM VW_OSA_CHEQUEPAYMENTDETAILS WHERE VHH_DOCNO='" & p_Docno & "' and VHH_BSU_ID='" & Session("sbsuid") & "' AND VHH_DOCTYPE='BP' and VHH_FYEAR=" & Session("F_YEAR") & ""
        cmd.CommandText = strSql
        cmd.CommandType = Data.CommandType.Text
        SqlHelper.FillDataset(strCon, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(strCon)
            Dim RepSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("ReportName") = "Sub: Cheque Forwarding Letter"
            RepSource.Parameter = params
            RepSource.Command = cmd
            RepSource.IncludeBSUImage = True
            params("CurID") = strCurID
            params("CurDesc") = strCurDesc
            params("CurDeno") = strCurDeno
            params("SubRepName") = "Settlement Information"
            Dim SUBrEP(1) As MyReportClass
            SUBrEP(0) = GetChequeFwdWithoutSettlement(p_Docno)
            RepSource.SubReport = SUBrEP
            RepSource.ResourceName = "../RPT_Files/ChequeForwardingLetter.rpt"
            Session("ReportSource") = RepSource
        End If
        h_print.Value = "print"
    End Sub

    Protected Sub ChkPrintNotice_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Function GetChequeFwdWithoutSettlement(ByVal p_Docno As String) As MyReportClass
        Dim str_query As String
        Dim ds As New DataSet
        Dim strCon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim cmd As New SqlCommand
        Dim RepSubRepSource As New MyReportClass
        str_query = "SELECT * FROM VW_OSA_CHECKFWDLETTER WHERE VHH_DOCNO='" & p_Docno & "' and VHH_BSU_ID='" & Session("sbsuid") & "' AND VHH_DOCTYPE='BP' and VHH_FYEAR=" & Session("F_YEAR") & " and VHD_CHQNO<>''"
        cmd.Connection = New SqlConnection(strCon)
        cmd.CommandText = str_query
        cmd.CommandType = CommandType.Text
        RepSubRepSource.Command = cmd
        Return RepSubRepSource
    End Function

End Class
