Imports System.Data.DataSet
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Accounts_accposOfflineSettlement
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtHAccountname.Attributes.Add("readonly", "readonly")
            Set_Components()
            btnSave.Enabled = False
            btnPreview.Enabled = False
            Page.Title = OASISConstants.Gemstitle
        End If
    End Sub


    Public Function returndate(ByVal p_date As String) As String
        Try 'verify date is valid
            Dim dt As Date = CDate(p_date)
            Dim dt_1900 As Date = CDate("01/01/1900")
            If dt = dt_1900 Then
                Return ""
            Else
                Return Format(dt, "dd/MMM/yyyy")
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String 'ACT_RECONFORMAT
            str_Sql = "SELECT GUID, TRN_SUB_ID, TRN_BSU_ID," _
             & " TRN_FYEAR, TRN_DOCTYPE, TRN_DOCNO, TRN_LINEID," _
             & " TRN_DOCDT, TRN_ACT_ID, TRN_CUR_ID," _
             & " TRN_EXGRATE1, TRN_EXGRATE2, TRN_DRCR," _
             & " TRN_AMOUNT, TRN_LOCALAMT, TRN_ALLOCAMT," _
             & " TRN_INVNO, TRN_CHQNO, TRN_VOUCHDT," _
             & " TRN_NARRATION ,(TRN_AMOUNT-TRN_ALLOCAMT) AS BALANCE FROM TRANHDR_D" _
             & " WHERE TRN_DRCR='DR' AND TRN_ACT_ID='" & txtHAccountcode.Text & "'" _
             & " AND TRN_BSU_ID='" & Session("sBsuid") & "'" _
             & " AND TRN_SUB_ID='" & Session("Sub_ID") & "'" _
             & " AND (TRN_AMOUNT-TRN_ALLOCAMT>0)"
           
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            gvTrans.DataSource = ds.Tables(0)
            gvTrans.DataBind()
            str_Sql = "SELECT " _
                & " TRN_AMOUNT -ISNULL(A.AMT,0) -TRN_ALLOCAMT  AS BALANCE," _
                & " ISNULL(A.AMT,0)+TRN_ALLOCAMT AS TRN_ALLOCAMT,*" _
                & " FROM TRANHDR_D LEFT OUTER JOIN " _
                & " (SELECT SUM(SOL_SETTLEDAMT) AS AMT , " _
                & " SOL_REFDOCTYPE, SOL_REFDOCNO, " _
                & " SOL_REFLINEID, SOL_ACCTCODE" _
                & " FROM SETTLEONLINE_D " _
                & " WHERE SOL_ACCTCODE='" & txtHAccountcode.Text & "' " _
                & " AND    SOL_BSU_ID='" & Session("sBsuid") & "'" _
                & " GROUP BY  SOL_REFDOCTYPE, SOL_REFDOCNO," _
                & " SOL_REFLINEID, SOL_ACCTCODE) A" _
                & " ON TRN_DOCNO= A.SOL_REFDOCNO" _
                & " AND TRN_DOCTYPE= A.SOL_REFDOCTYPE " _
                & " AND TRN_LINEID=SOL_REFLINEID" _
                & " WHERE TRN_ACT_ID='" & txtHAccountcode.Text & "' " _
                & " AND TRN_BSU_ID='" & Session("sBsuid") & "' " _
                & " AND (TRN_SUB_ID = '" & Session("Sub_ID") & "') " _
                & " AND  TRN_DRCR = 'CR' " _
                & " AND (TRN_AMOUNT -TRN_ALLOCAMT-ISNULL(A.AMT,0)) >0"

            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvSettle.DataSource = ds.Tables(0)
            gvSettle.DataBind()
            Try
                For Each gvr As GridViewRow In gvSettle.Rows
                    Dim txtPayment As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)

                    If txtPayment IsNot Nothing Then
                        ClientScript.RegisterArrayDeclaration("TextBoxAllocated", String.Concat("'", txtPayment.ClientID, "'"))
                        'txtRCDate()
                    End If
                Next
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub Set_Components()
        Try
            gridbind()
            txtHAccountname.Attributes.Add("readonly", "readonly")
            'txtHTotal.Text = AccountFunctions.GetLedgerBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text)
            'txtTBalance.Text = AccountFunctions.GetReconclileBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text)
        Catch ex As Exception
        End Try
    End Sub


    Private Function check_in_ds(ByRef p_dt As DataTable, ByVal p_credit As String, _
    ByVal p_credit_count As Integer, ByVal p_debit As String, ByVal p_debit_count As Integer, _
    ByVal p_searchcontent As String, _
    ByVal p_columnno As Integer) As Boolean
        Try
            p_columnno = p_columnno - 1
            p_debit_count = p_debit_count - 1
            p_credit_count = p_credit_count - 1
            For i As Integer = 0 To p_dt.Rows.Count 'start finding
                If p_dt.Rows(i)(p_columnno).ToString.ToUpper.IndexOf(p_searchcontent.ToUpper) >= 0 And checkempty(p_debit) = checkempty(p_dt.Rows(i)(p_debit_count)) And checkempty(p_credit) = checkempty(p_dt.Rows(i)(p_credit_count)) Then
                    p_dt.Rows(i).Delete()
                    Return True

                End If
            Next
            Return False
        Catch ex As Exception
        End Try

    End Function


    Private Function checkempty(ByVal p_input As Object) As Double
        Try
            If p_input Is System.DBNull.Value Then
                Return 0
            Else
                Return CDbl(p_input)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function


    Private Function checkdate(ByVal p_input As String) As Date
        Try
            If p_input Is System.DBNull.Value Then
                Return CDate("01/jan/1900")
            Else
                Return CDate(p_input)
            End If
        Catch ex As Exception
            Return CDate("01/jan/1900")
        End Try
    End Function


    Protected Sub btnHaccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHaccount.Click
        gvTrans.EditIndex = -1
        Set_Components()
    End Sub


    Protected Sub btnReconcile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Preview_Settle() = True Then
            Save_Offline_Settlement()
        Else
            lblError.Text = "Check allocation"
        End If

    End Sub


    Private Sub Save_Offline_Settlement(Optional ByVal p_temp As Boolean = False)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim str_err As String = ""
            ' Dim rcCount As Integer = 0
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim lblGuid As Label = CType(gvTrans.Rows(gvTrans.EditIndex).FindControl("lblGuid"), Label)
            ''''''
            Dim str_Sql As String
            str_Sql = "SELECT    * FROM TRANHDR_D" _
            & " WHERE GUID='" & lblGuid.Text & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count = 0 Then
                'ds.Tables(0).Rows(0)("TRN_DOCTYPE")
                'ds.Tables(0).Rows(0)("TRN_LINEID")
                'ds.Tables(0).Rows(0)("TRN_EXGRATE1")
                'pParms(10).Value = ds.Tables(0).Rows(0)("TRN_EXGRATE2")
                lblError.Text = "Problem occured"
                Exit Sub
            End If

            Dim str_remarks As String = ""
            For Each gvr As GridViewRow In gvSettle.Rows
                Dim txtAllocated As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)
                Dim lblRGuid As Label = CType(gvr.FindControl("lblGuid"), Label)

                If txtAllocated IsNot Nothing And CDbl(txtAllocated.Text) > 0 Then
                    If str_remarks <> "" Then
                        str_remarks = str_remarks & ", "
                    End If
                    str_remarks = str_remarks & gvr.Cells(0).Text
                    Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
                    str_err = AccountFunctions.SaveSAVESETTLE_D(Session("Sub_ID"), ds.Tables(0).Rows(0)("TRN_LINEID"), _
                    ds.Tables(0).Rows(0)("TRN_DOCTYPE"), ds.Tables(0).Rows(0)("TRN_EXGRATE1"), _
                    ds.Tables(0).Rows(0)("TRN_EXGRATE2"), Session("sBsuid"), _
                    ds.Tables(0).Rows(0)("TRN_FYEAR"), gvTrans.Rows(gvTrans.EditIndex).Cells(0).Text, _
                    txtHAccountcode.Text, txtAllocated.Text, gvTrans.Rows(gvTrans.EditIndex).Cells(2).Text, _
                    ds.Tables(0).Rows(0)("TRN_DOCDT"), lblRGuid.Text, gvr.Cells(0).Text, gvr.Cells(2).Text, stTrans)
                    If str_err <> "0" Then
                        Exit For
                    End If

                End If
            Next
            If str_err = "" Then
                stTrans.Rollback()
                lblError.Text = "Please enter the amount"
            ElseIf str_err = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, gvTrans.Rows(gvTrans.EditIndex).Cells(0).Text, "Insert", Page.User.Identity.Name.ToString, Me.Page, str_remarks)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = getErrorMessage(str_err)
                gvTrans.EditIndex = -1
                Set_Components()

                btnSave.Enabled = False
                txtTBalance.Text = "0"
                btnPreview.Enabled = False

            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(str_err)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        viewstate("datamode") = Encr_decrData.Encrypt("view")
        Response.Redirect("../homepage.aspx")
    End Sub


    Protected Sub gvTrans_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvTrans.RowCancelingEdit
        gvTrans.EditIndex = -1
        gridbind()
        txtTBalance.Text = "0"
        btnSave.Enabled = False
        btnPreview.Enabled = False
    End Sub


    Protected Sub gvTrans_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvTrans.RowEditing
        Dim lblAmount As New Label
        lblAmount = TryCast(gvTrans.Rows(e.NewEditIndex).FindControl("lblBalance"), Label)
        If lblAmount IsNot Nothing Then
            txtTBalance.Text = lblAmount.Text
            btnSave.Enabled = True
            btnPreview.Enabled = True
        End If
        gvTrans.EditIndex = e.NewEditIndex
        gridbind()
    End Sub
    
     
    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Preview_Settle()
    End Sub


    Private Function Preview_Settle() As Boolean
        Dim valid As Boolean = True
        Dim DTOTAL As Double = 0
        For Each gvr As GridViewRow In gvSettle.Rows
            Dim txtAllocated As TextBox = CType(gvr.FindControl("txtSAllocated"), TextBox)
            If txtAllocated IsNot Nothing Then
                If txtAllocated.Text = "" Then
                    txtAllocated.Text = "0"
                End If
                Try
                    If CDbl(gvr.Cells(7).Text) < CDbl(txtAllocated.Text) Then
                        gvr.CssClass = "griditem_hilight"
                        valid = False
                    Else
                        gvr.CssClass = "griditem"
                    End If

                    DTOTAL = DTOTAL + CDbl(txtAllocated.Text)
                Catch ex As Exception
                    gvr.CssClass = "griditem_hilight"
                    'txtAllocated.Text = "0"
                    valid = False
                End Try
            End If
        Next
        txtSAlloted.Text = DTOTAL
        txtBalance.Text = CDbl(txtTBalance.Text) - DTOTAL
        If CDbl(txtSAlloted.Text) > CDbl(txtTBalance.Text) Then
            valid = False
        End If

        If valid = False Then
            lblError.Text = "Check the Allocations"
        End If
        Preview_Settle = valid

    End Function


    Protected Sub txtSAllocated_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim TXT As New TextBox
        TXT = sender
        TXT.Attributes.Add("onfocus", "this.select();")
        TXT.Attributes.Add("onBlur", "OnChange();")
    End Sub


    Protected Sub txtHAccountcode_TextChanged(sender As Object, e As EventArgs)
        gvTrans.EditIndex = -1
        Set_Components()
    End Sub
End Class
