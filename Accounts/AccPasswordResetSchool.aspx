<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccPasswordResetSchool.aspx.vb" Inherits="Accounts_AccPasswordResetSchool" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
           
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="User Activation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tabmain" align="center" border="0" cellpadding="0"
                                cellspacing="0" width="100%" runat="server">

                                <tr id="row2">
                                    <td align="left" width="20%"><span class="field-label">Bussiness Unit</span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBUnit" runat="server" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                                        </asp:DropDownList></td>

                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvUNITS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            OnPageIndexChanged="gvUNITS_PageIndexChanged" PageSize="30"
                                            Width="100%" OnRowDataBound="gvUNITS_RowDataBound">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select 

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click here to select"></asp:CheckBox>
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# bind("USR_Name") %>'></asp:HiddenField>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Staff Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblTCHeader" runat="server" Text="Staff Name">
                                                        </asp:Label><br />
                                                        <asp:TextBox ID="txtTCSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnTC_Search" runat="server" ImageAlign="Middle"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnTC_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltrycount" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                        <asp:HiddenField ID="USR_TRYCOUNT" runat="server" Value='<%# BIND("USR_Name") %>'></asp:HiddenField>

                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="User Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblusrnameHeader" runat="server" Text="User name">
                                                        </asp:Label><br />
                                                        <asp:TextBox ID="txtusrnameSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnusrname_Search" runat="server" ImageAlign="Middle"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnusrname_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblusrname" runat="server" Text='<%# Bind("USR_NAME") %>'></asp:Label>
                                                        <asp:HiddenField ID="USM_USR_NAME" runat="server" Value='<%# BIND("USR_Name") %>'></asp:HiddenField>

                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <%--                <asp:Button id="btnActivate" runat="server" CssClass="button" onclick="btnActivate_Click"
                    tabIndex="7" Text="Activate" ValidationGroup="groupM1" Width="70px" />--%>
                            <asp:Button ID="btnReset" runat="server" CausesValidation="False" CssClass="button"
                                TabIndex="8" Text="Reset" UseSubmitBehavior="False" OnClick="btnReset_Click" />
                        </td>
                        <td>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>



            </div>
        </div>
    </div>

</asp:Content>

