Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_AccGeneralSettings
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim lstrUsedChqNos As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                chkHideCC.Checked = IIf(Session("HideCC") = "True", True, False)
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Values("HideCC") = chkHideCC.Checked
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)
            Session("HideCC") = chkHideCC.Checked
            lblError.Text = "Settings Saved!!!!"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
