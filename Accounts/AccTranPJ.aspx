<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccTranPJ.aspx.vb" Inherits="AccTranPJ" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Purchase Journals
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" id="Table1" border="0">
                    <tr>
                        <td  align="left" width="50%">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td  width="50%" align="right">
                            <asp:RadioButton ID="optUnPosted" runat="server" GroupName="optControl" Text="Open"
                                Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="optPosted" runat="server"
                                    GroupName="optControl" Text="Posted" AutoPostBack="True" />
                            <asp:RadioButton ID="optAll" runat="server" GroupName="optControl" Text="All" AutoPostBack="True" />
                        </td>
                    </tr>
                </table>
                <table  id="tbl" border="0" width="100%" >
                    <tr>
                        <td align="left" width="20%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                DataKeyNames="GUID" EmptyDataText="No Data" AllowPaging="True" PageSize="50"
                                OnRowDataBound="gvGroup1_RowDataBound">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No
                                            <br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle  HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Old Ref No
                                            <br />
                                            <asp:TextBox ID="txtOldDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnOldDocNo" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodewww" runat="server" Text='<%# Bind("OldDocNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date
                                            <br />
                                            <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle  HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DocDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Creditor
                                            <br />
                                            <asp:TextBox ID="txtBankAC" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAC" runat="server" Text='<%# Bind("HeaderAccount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Narration
                                            <br />
                                            <asp:TextBox ID="txtNarrn" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="btnSearchName_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarrn" runat="server" Text='<%# Bind("Narration") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Amount
                                            <br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Currency
                                            <br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Currency") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlview" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="Left" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>

                        </td>                        
                    </tr>
                </table>


                <input id="h_SelectedId" runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input
                                id="h_Selected_menu_5" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_6"
                                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                        type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>
