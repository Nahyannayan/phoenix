<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccAddChqBook.aspx.vb" Inherits="AccAddChqBook" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getbank() {
            popUp('460', '400', 'BANK', '<%=txtControlAcc.ClientId %>', '<%=txtControlAccDescr.ClientId %>');
            document.getElementById("<%=txtLotid.ClientID %>").value = '';
            document.getElementById("<%=h_Chbid.ClientID %>").value = '';
        }

        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var NameandCode;
            var url;
            document.getElementById('<%=hf_pMode.ClientID%>').value = pMode;
            document.getElementById('<%=hf_ctrl.ClientID %>').value = ctrl;
            document.getElementById('<%=hf_ctrl1.ClientID%>').value = ctrl1;
            document.getElementById('<%=hf_ctrl2.ClientID%>').value = ctrl2;
            if (pMode == 'BANK') {
                url = "PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value;
                radopen(url, "pop_up");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'MSTCHQBOOK') {
                    result = radopen("accShowChequebook.aspx?BankCode=" + document.getElementById(ctrl1).value + "", "pop_up");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl2).value = lstrVal[1];
                }
        }

        function OnClientClose(oWnd, args) {

            var pMode = document.getElementById('<%=hf_pMode.ClientID%>').value ;
            var ctrl  = document.getElementById('<%=hf_ctrl.ClientID %>').value ;
            var ctrl1 = document.getElementById('<%=hf_ctrl1.ClientID%>').value;
            var ctrl2 = document.getElementById('<%=hf_ctrl2.ClientID%>').value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (pMode == 'BANK') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                    <%--__doPostBack('<%= txtempname.ClientID %>', 'TextChanged');--%>
                }
                else if (pMode == 'MSTCHQBOOK') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl2).value = NameandCode[1];
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <asp:HiddenField ID="hf_pMode" runat="server" />
    <asp:HiddenField ID="hf_ctrl" runat="server" />
    <asp:HiddenField ID="hf_ctrl1" runat="server" />
    <asp:HiddenField ID="hf_ctrl2" runat="server" />

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cheque Book Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" align="center" border="0">
                    <tbody>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" align="center">


                    <tr id="tr_ControlAccount">
                        <td align="left"><span class="field-label">Account #</span></td>

                        <td align="left" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtControlAcc" runat="server" Width="92%"></asp:TextBox>
                                        <a href="#" onclick="getbank(); return false;">
                                            <img border="0" src="../Images/cal.gif" id="IMG2" language="javascript" /></a>

                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtControlAccDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Lot No</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLotNo" runat="server" MaxLength="4" TabIndex="1"></asp:TextBox>
                            <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                        </td>
                        <td align="left"><span class="field-label">Prefix</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtPrefix" runat="server" MaxLength="4" TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label">From #</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server" MaxLength="10" TabIndex="3"></asp:TextBox>
                            <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                        </td>
                        <td align="left"><span class="field-label">To #</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtTo" runat="server" MaxLength="10" TabIndex="4"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Previous Cheque Book</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtLotid" runat="server"></asp:TextBox>
                            <a href="#" onclick="popUp('460','400','MSTCHQBOOK','<%=txtLotid.ClientId %>','<%=txtControlAcc.ClientId %>','<%=h_Chbid.ClientId %>')">
                                <img border="0" src="../Images/cal.gif" id="Img1" language="javascript" /></a>
                        </td>

                        <td><span class="field-label">Cheque Format</span></td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="drpChequeFormat">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" TabIndex="5" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" TabIndex="6" /></td>
                    </tr>
                </table>
                <input id="h_Chbid" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>

