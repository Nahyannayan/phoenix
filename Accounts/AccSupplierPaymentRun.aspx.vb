﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime

Partial Class Accounts_AccSupplierPaymentRun
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISFINConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200368") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    h_Sel_BSU_ID.Value = Session("sBsuid")
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    h_Sph_Id.Value = "0"
                    saveData()
                Else
                    h_Sph_Id.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                BindDropdowns()
                refreshGrid()
            Else
                If Not Session("Empdata") Is Nothing AndAlso Session("Empdata") = "refresh" Then
                    Session("Empdata") = ""
                    refreshGrid()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub refreshGrid()
        Dim ds As New DataSet
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        selectedSupplier = RadSupplier.SelectedValue
        Dim pParms(7) As SqlParameter
        Try
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", h_Sel_BSU_ID.Value, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(4) = Mainclass.CreateSqlParameter("@SUPPLIER", RadSupplier.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@VIEW", "S", SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@SPH_ID", h_Sph_Id.Value, SqlDbType.Int)
            pParms(7) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "GET_SUPPLIERPAYMENTDETAILS_S", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()

            RadSupplier.DataSource = ds.Tables(1)
            RadSupplier.DataTextField = "ACT_NAME"
            RadSupplier.DataValueField = "ACT_ID"
            RadSupplier.SelectedValue = selectedSupplier
            RadSupplier.DataBind()

            btnSend.Visible = (ds.Tables(0).Rows(0)(0) = 1) 'edit
            btnApprove.Visible = False
            btnReject.Visible = False

            Session("myData") = ds.Tables(0)
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property selectedSupplier() As String
        Get
            Return ViewState("selectedSupplier")
        End Get
        Set(ByVal value As String)
            ViewState("selectedSupplier") = value
        End Set
    End Property

    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property

    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Payment Authorization Form")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTHC_invNo"), Label)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTHireCharges"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@THC_INVOICE", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTHireCharges.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindDropdowns()
        Try
            Dim strsql As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select 'ALL'SCode,'ALL'SDescr union select 'Open'SCode,'Open'SDescr union select 'Approved'SCode,'Approved'SDescr union select 'Rejected'SCode,'Rejected'SDescr "
            RadStatus.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strsql)
            RadStatus.DataTextField = "SCode"
            RadStatus.DataValueField = "SDescr"
            RadStatus.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        saveData()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub RadStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadStatus.SelectedIndexChanged
        refreshGrid()
    End Sub

    Protected Sub RadSupplier_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadSupplier.SelectedIndexChanged
        refreshGrid()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

            Dim ds As New DataSet
            Dim cmd As New SqlCommand
            cmd.CommandText = "GET_SUPPLIERPAYMENTDETAILS_S"

            If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
            Dim pParms(7) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", h_Sel_BSU_ID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(pParms(1))
            pParms(2) = Mainclass.CreateSqlParameter("@TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            cmd.Parameters.Add(pParms(2))
            pParms(4) = Mainclass.CreateSqlParameter("@SUPPLIER", h_Sel_ACT_ID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(pParms(4))
            pParms(5) = Mainclass.CreateSqlParameter("@VIEW", "S", SqlDbType.VarChar)
            cmd.Parameters.Add(pParms(5))
            pParms(6) = Mainclass.CreateSqlParameter("@SPH_ID", h_Sph_Id.Value, SqlDbType.Int)
            cmd.Parameters.Add(pParms(6))
            pParms(7) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
            cmd.Parameters.Add(pParms(7))

            cmd.Connection = New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Connection = New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "Payment Authorization Form"
            params("@TRN_NO") = "1"
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../RPT_Files/SupplierPaymentRun.rpt"

            Dim subRep(0) As MyReportClass
            Dim sqlParam1(1) As SqlParameter
            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        saveData()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Sub saveData()
        '@SPH_ID int, @sph_bsu_id varchar(10), @sph_date smalldatetime, @sph_fyear varchar(4), @sph_user varchar(100)
        Dim iParms(5) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@sph_id", h_Sph_Id.Value, SqlDbType.Int, True)
        iParms(2) = Mainclass.CreateSqlParameter("@sph_bsu_id", Session("sBsuid"), SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@sph_date", txtDate.Text, SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@sph_fyear", Session("F_YEAR"), SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@sph_user", Session("sUsr_name"), SqlDbType.VarChar)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveSPR_H", iParms)
            h_Sph_Id.Value = iParms(1).Value
            stTrans.Commit()

        Catch ex As Exception
            lblError.Text = "Unexpected Error !!!"
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTHCID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  tPTLEASINGINVOICE set TLI_DELETED=1,TLI_USER='" & Session("sUsr_name") & "',TLI_DATE=GETDATE() where TLI_ID=" & lblID.Text)
        refreshGrid()
    End Sub

    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim Remarks As TextBox = e.Row.FindControl("txtRemarks")
                Dim lblActid As Label = e.Row.FindControl("lblactid")
                Dim lnkView As LinkButton = e.Row.FindControl("lnkView")
                lnkView.Attributes.Add("OnClick", "javascript:return getSupplerPaymentHistory('" & lblActid.Text & "')")
                If GridViewShowDetails.ShowFooter Or GridViewShowDetails.EditIndex > -1 Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)

                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class

