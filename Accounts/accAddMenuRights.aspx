<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="accAddMenuRights.aspx.vb" Inherits="AddMenuRights" Title="::::GEMS OASIS:::: Online Student Administration System::::" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

    function ChangeAllCheckBoxStates() {
            var lstrChk = document.getElementById('<%=ChkSelAll.ClientID()%>').checked;
            ChangeCheckBoxState(lstrChk);
    }

        function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBSU_Copy.ClientID%>');
            var options = tableBody.getElementsByTagName('input');            
            for (var i = 0; i < options.length; i++) {               
                var currentTd = options[i];                                
                currentTd.checked = checkState;
            }
        }

        function Disable_Text() {

            alert("Hello")

            // var myVal = document.getElementById('myValidatorClientID');

        }


        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }



    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            Manage Rights
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" cellpadding="0" cellspacing="0" width="99%" border="0">
                    <tr>
                        <td colspan="2" style="height: 6px" align="left" valign="middle">
                            <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>



                    <tr>
                        <td valign="top" align="left">
                            <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="4">
                                        <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="background: url('../Images/AccessRights/red.png'); color: White; width: 130pt; height: 22px;" align="center">Access Denied</td>
                                                <td style="background: url('../Images/AccessRights/black.png') repeat-x 0 0; color: White; width: 130pt;" align="center">View</td>
                                                <td style="background: url('../Images/AccessRights/gray.png'); color: White; width: 130pt;" align="center">View/Print</td>
                                                <td style="background: url('../Images/AccessRights/orange.png'); color: White; width: 130pt;" align="center">View/Print/Add</td>
                                                <td style="background: url('../Images/AccessRights/purple.png'); color: White; width: 130pt;" align="center">View/Print/Add/Edit</td>
                                                <td style="background: url('../Images/AccessRights/blue.png'); color: White; width: 130pt;" align="center">View/Print/Add/Edit/Delete</td>
                                                <td style="background: url('../Images/AccessRights/green.png'); color: White; width: 130pt;" align="center">Full Access</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="title-bg" colspan="4">Select Business Unit/Role Rights To Be Updated</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" colspan="4">
                                        <asp:Button ID="btnAddEdit" runat="server" Text="Add/Edit Access Rights" CssClass="button m-0" /></td>
                                </tr>

                                <tr>
                                    <td valign="top" align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server">
                                        </asp:DropDownList></td>
                                    <td valign="top" align="left" width="20%"><span class="field-label">Roles </span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlRole" runat="server" DataTextField="ROL_DESCR" DataValueField="ROL_ID">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr id="trCopy11" runat="server"
                                    valign="top" align="left" class="title-bg">
                                    <td align="left" colspan="4">Copy Role Rights From</td>
                                </tr>
                                <tr id="trCopy12" runat="server">
                                    <td align="right" colspan="4">
                                        <asp:Button ID="btnCopy" runat="server" Text="Copy Access Rights" CssClass="button" Width="170px" Height="22px" /></td>
                                </tr>
                                <tr id="trCopy2" runat="server">
                                    <td valign="top" align="left"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBsuCopy" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td valign="top" align="left"><span class="field-label">Roles</span></td>
                                    <td>
                                        <asp:DropDownList ID="ddlRoleCopy"
                                            runat="server" DataTextField="ROL_DESCR" DataValueField="ROL_ID">
                                        </asp:DropDownList><font color="maroon" size="3pt" style="font-weight: normal; display: block;">(Only higher role rights will be applied while copying-'no overwrites')</font></td>


                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>

                                <tr id="trMenu11" runat="server">
                                    <td valign="top" align="left" class="title-bg" colspan="4">Menus </td>
                                </tr>
                                <tr id="trMenu12" runat="server">
                                    <td><span class="field-label">Set Operation Rights </span></td>
                                    <td>
                                        <asp:DropDownList ID="ddlOprRight" runat="server">
                                            <asp:ListItem Value="0">Access Denied</asp:ListItem>
                                            <asp:ListItem Value="1">View</asp:ListItem>
                                            <asp:ListItem Value="2">View/Print</asp:ListItem>
                                            <asp:ListItem Value="3">View/Print/Add</asp:ListItem>
                                            <asp:ListItem Value="4">View/Print/Add/Edit</asp:ListItem>
                                            <asp:ListItem Value="5">View/Print/Add/Edit/Delete</asp:ListItem>
                                            <asp:ListItem Value="6">Full Access</asp:ListItem>
                                        </asp:DropDownList>&nbsp;<asp:Button ID="btnAddOpr" runat="server" Text="Update Rights" CssClass="button" />


                                    </td>
                                    <td valign="top" align="left"><span class="field-label">Copy Menu Access rights To </span>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSave"
                                            runat="server" Text="Save" CssClass="button" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr id="trMenu2" runat="server">
                                    <td colspan="2">
                                        <asp:Panel runat="server" ID="Panel2">
                                            <div class="checkbox-list">
                                                <telerik:RadTreeView ID="rtvMenuRights" runat="server"
                                                    CheckBoxes="True"
                                                    CheckChildNodes="true">
                                                    <DataBindings>
                                                        <telerik:RadTreeNodeBinding></telerik:RadTreeNodeBinding>
                                                    </DataBindings>
                                                </telerik:RadTreeView>
                                                <br />
                                            </div>
                                        </asp:Panel>

                                    </td>
                                    <td colspan="2" id="tdChk" runat="server">
                                        <asp:Panel runat="server" ID="Panel3" ScrollBars="Vertical" CssClass="checkbox-list">
                                          <%--<input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);"
                                                title="Select" type="checkbox" value="Check All"  />Select All--%>
                                            <asp:CheckBox ID="ChkSelAll" value="Select All" runat="server" onclick="ChangeAllCheckBoxStates();" />
                                            <asp:CheckBoxList ID="chkBSU_Copy"
                                                runat="server" CellPadding="2" CellSpacing="2"
                                                RepeatColumns="1" RepeatDirection="Horizontal">
                                            </asp:CheckBoxList>
                                        </asp:Panel>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

