<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accjvPostJournalVoucher.aspx.vb" Inherits="jvPostJournalVoucher" Title="Post Journal Voucher" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
  
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
            var curr_elem;
            var countChecked;
            countChecked = 0;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                    if (curr_elem.checked)
                        countChecked = countChecked + 1;
                }
            }
            if (countChecked > 1)
                return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
            else
                return true;
        }
    }
    Sys.Application.add_load(
           function CheckForPrint() {
               if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
            }
        }
                    );
    </script>
    <a id='top'></a>


     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHeader" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" /></td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr valign="TOP">
                                    <td align="center"
                                        width="100%">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                            EmptyDataText="No Journal Vouchers for posting" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="JHD_DOCNO" HeaderText="Doc No." ReadOnly="True"
                                                    SortExpression="JHD_DOCNO" />
                                                <asp:BoundField DataField="JHD_REFNO" HeaderText="Ref No." />
                                                <asp:BoundField DataField="JHD_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Document Date"
                                                    HtmlEncode="False" SortExpression="JHD_DOCDT">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JHD_NARRATION" HeaderText="Narration" />
                                                <asp:BoundField DataField="JHD_CUR_ID" HeaderText="Currency" SortExpression="JHD_CUR_ID">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("TOTAL")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TOTAL" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Post" SortExpression="JHD_bPOSTED">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("JHD_bPOSTED") %>' />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<input id="chkPosted" runat="server" checked='<%# Bind("JHD_bPOSTED") %>' type="checkbox"
                                                            value='<%# Bind("GUID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderTemplate>
                                                        Post<input id="Checkbox2" onclick="fnSelectAll(this)" type="checkbox" />
                                                        &nbsp;
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False" HeaderText="View">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                            OnClick="lbView_Click" Text="Summary"></asp:LinkButton>
                                                        <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False" Visible="False" HeaderText="View">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:HyperLink ID="hlEdit" runat="server" Enabled="False">Edit</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                        <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print Voucher" />

                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" />&nbsp;
    <br />
                                        <br />
                                        <a id='detail'></a>
                                        <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False"
                                            Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="JNL_DOCNO" HeaderText="Document No" SortExpression="JNL_DOCNO" />
                                                <asp:BoundField DataField="JNL_NARRATION" HeaderText="Narration" SortExpression="JNL_NARRATION" />
                                                <asp:BoundField DataField="Account" HeaderText="Account" />
                                                <asp:TemplateField HeaderText="Debit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("JNL_DEBIT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Credit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("JNL_CREDIT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="slno" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("JNL_SLNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="View" SelectText="View" ShowSelectButton="True" />
                                            </Columns>
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <a id='child'></a>
                                        <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT"
                                            Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="ACT_ID" HeaderText="Account No" SortExpression="ACT_ID" />
                                                <asp:BoundField DataField="JDS_SLNO" HeaderText="Slno" SortExpression="JDS_SLNO"
                                                    Visible="False" />
                                                <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                                <asp:BoundField DataField="JDS_DESCR" HeaderText="Cost Object" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("JDS_AMOUNT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="GRPFIELD" HeaderText="GRPFIELD" />
                                                <asp:BoundField DataField="JDS_CODE" HeaderText="CODE" SortExpression="JDS_CODE"
                                                    Visible="False" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <input id="h_FirstVoucher" runat="server" type="hidden" />

            </div>
        </div>
    </div>
</asp:Content>

