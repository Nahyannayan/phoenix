Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class DayendManual
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim qry As String
            qry = "select COUNT(*)SUPERUSR from USERS_M where ISNULL(USR_bITSupport,0)=1 or ISNULL(USR_bSuper,0)=1"
            Dim SU As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry)
            If SU = 0 Then
                Response.Redirect("~\noAccess.aspx")
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "A550023" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                CommandType.Text, "select BSU_ID ,BSU_NAMEwithShort+' ['+BSU_ID+']' BSU_NAME from [fn_GetBusinessUnits]('" & Session("sUsr_name") & "') ORDER BY BSU_NAME")
                DDLBsu.DataSource = ds
                DDLBsu.DataTextField = "BSU_NAME"
                DDLBsu.DataValueField = "BSU_ID"
                DDLBsu.DataBind()
                DDLBsu.SelectedValue = CurBsUnit
                Initialize()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub
    Sub Initialize()
        Dim DayEndDT As String = String.Empty
        Dim MonthEndDT As String = String.Empty
        Me.DTDayend.Text = ""
        Me.DTMonthend.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select BSU_DAYCLOSEDT,BSU_FREEZEDT from BUSINESSUNIT_M where BSU_ID='" & Me.DDLBsu.SelectedValue & "'")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                DayEndDT = CDate(ds.Tables(0).Rows(0)(0)).ToString("dd/MMM/yyyy")
                MonthEndDT = CDate(ds.Tables(0).Rows(0)(1)).ToString("dd/MMM/yyyy")
            End If
        End If
        If DayEndDT.Trim <> "" Then
            DTDayend.Text = DayEndDT
        End If
        If MonthEndDT.Trim <> "" Then
            DTMonthend.Text = MonthEndDT
        End If
        Dim dsLog As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select ISNULL(DME_USR,'')MUSER,DME_MODIDate,DME_OLD_DayDT,DME_OLD_MonthDT,DME_NEW_DayDT,DME_NEW_MonthDT from Day_Month_End_Log where DME_BSU_ID='" & Me.DDLBsu.SelectedValue & "' and isnull(LogType,'')<>'FEE' order by DME_ID desc")
        If dsLog.Tables.Count > 0 Then
            gvAuditLog.DataSource = dsLog
            gvAuditLog.DataBind()
        End If
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim sqlTran As SqlTransaction

        Dim lintRetVal As Integer
        If (lstrErrMsg = "") Then
            objConn.Open()
            sqlTran = objConn.BeginTransaction("SaveDayendDates")
            Try
                Dim SqlCmd As New SqlCommand("SaveDayendDates", objConn, sqlTran)
                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim param(6) As SqlParameter '
                param(0) = Mainclass.CreateSqlParameter("@BSUID", DDLBsu.SelectedItem.Value, SqlDbType.VarChar)
                SqlCmd.Parameters.Add(param(0))
                param(1) = Mainclass.CreateSqlParameter("@DayendDT", CDate(DTDayend.Text), SqlDbType.DateTime)
                SqlCmd.Parameters.Add(param(1))
                param(2) = Mainclass.CreateSqlParameter("@MonthendDT", CDate(DTMonthend.Text), SqlDbType.DateTime)
                SqlCmd.Parameters.Add(param(2))

                param(3) = Mainclass.CreateSqlParameter("@MODI_USER", Session("sUsr_name"), SqlDbType.VarChar)
                SqlCmd.Parameters.Add(param(3))
                param(4) = Mainclass.CreateSqlParameter("@ReturnValue", 0, SqlDbType.Int, True)
                SqlCmd.Parameters.Add(param(4))


                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (lintRetVal = 0) Then
                    sqlTran.Commit()
                    lblError.Text = "Data updated Successfully"
                    Initialize()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ElseIf lintRetVal = 1 Then
                    lblError.Text = "Record not updated, Please contact IT"
                    sqlTran.Rollback()
                Else
                    sqlTran.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(lintRetVal)
                End If
            Catch ex As Exception
                sqlTran.Rollback()
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DDLBsu.SelectedValue = Session("sBsuid")
    End Sub

    Protected Sub btnFee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFee.Click
        'Me.TabPanel2.Enabled = True
    End Sub

    Protected Sub gvAuditLog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuditLog.PageIndexChanging
        gvAuditLog.PageIndex = e.NewPageIndex
        Initialize()
    End Sub

    Protected Sub DDLBsu_SelectedIndexChanged1(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles DDLBsu.SelectedIndexChanged
        Initialize()
    End Sub
End Class
