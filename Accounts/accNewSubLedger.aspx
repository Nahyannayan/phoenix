<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accNewSubLedger.aspx.vb" Inherits="NewSubLedger" Title="Add/Edit Group" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {
            if (document.getElementById('<%=txtSubLedgerCode.ClientID %>').value == '') {
                alert("Kindly enter Sub Ledger Code");
                return false;
            }
            if (document.getElementById('<%=txtSubLedgerName.ClientID %>').value == '') {
                alert("Kindly enter Sub Ledger Name");
                return false;
            }
            return true;
        }

        function hide(id) {
            var tdid = 'tbl_Message' + id;
            // alert(tdid);
            document.getElementById(tdid).style.display = 'none';
        }
        function help(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 150px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'ShowError.aspx?id=' + id;
            result = window.showModalDialog(url, "", sFeatures)
        }

        function GetAccounts() {
            var sFeatures;
            sFeatures = "dialogWidth: 820px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var ActType;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
            //result = window.showModalDialog("accjvshowaccount.aspx?multiSelect=true&forrpt=0", "", sFeatures)
            result = radopen("accjvshowaccount.aspx?multiSelect=true&forrpt=0", "pop_up2")
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = document.getElementById('<%=h_ACTIDs.ClientID %>').value + "||" + result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID%>').value = document.getElementById('<%=h_ACTIDs.ClientID %>').value + "||" + arg.NameandCode;
                document.forms[0].submit();

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Sub Ledger
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tblAddLedger" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Sub Ledger Code</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSubLedgerCode" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Sub Ledger Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSubLedgerName" runat="server" MaxLength="100"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="left" colspan="3">
                                        <asp:CheckBox ID="chkActive" runat="server" Text="Active" CssClass="field-label" /></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Linked Accounts
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtAccNames" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetAccounts(); return false;" /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="grdACTDetails" runat="server" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="False" Width="100%" PageSize="25">
                                            <Columns>
                                                <asp:BoundField DataField="ACT_ID" HeaderText="ACT ID">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="IsDefault">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkACCTIsDefault" runat="server" Checked='<%# Bind("IsDefault") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_ACTIDs" runat="server" />
                                        <asp:HiddenField ID="h_BSUID" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

