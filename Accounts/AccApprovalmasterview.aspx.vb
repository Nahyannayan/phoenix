Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_AccApprovalmasterview
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Public viewUrl As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A100358") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()

            Dim url As String
            ViewState("datamode") = "add"
            Dim Queryusername As String = Session("sUsr_name")
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            url = "AccApprovalmaster.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & dataModeAdd
            '.aspx
            'AccPettycashexpense
            hlAddNew.NavigateUrl = url


        End If

    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Public Sub ExportGridToExcel(ByVal GrdView As GridView, ByVal fileName As String)
        Response.Clear()
        Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", fileName))
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New StringWriter()
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        GrdView.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub



    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
       
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblDPT As New Label
            Dim lblDOC As New Label
            Dim lblBSU As New Label
            Dim hlview As New HyperLink
            Dim Qstring As String
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            lblDPT = TryCast(e.Row.FindControl("lblDPT"), Label)
            lblDOC = TryCast(e.Row.FindControl("lblDOC"), Label)
            lblBSU = TryCast(e.Row.FindControl("lblBSU"), Label)

            If hlview IsNot Nothing And lblDPT IsNot Nothing Then
                Qstring = lblDOC.Text & "_" & lblBSU.Text & "_" & lblDPT.Text
                Qstring = Encr_decrData.Encrypt(Qstring)
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                viewUrl = "AccApprovalmaster.aspx?viewid=" & Qstring

                hlview.NavigateUrl = viewUrl & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If
        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    
    
    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try




            Dim str_Sql, lstrOpr, lstrDocName, lstrFilterDocName, lstrBsName, lstrFilterBsName, lstrDptName As String

            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            Dim ds As New DataTable
            str_Sql = ""
            lstrOpr = ""
            lstrDocName = ""
            lstrFilterDocName = ""
            lstrBsName = ""
            lstrFilterBsName = ""
            lstrDptName = ""




            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)

                '   -- 1111   UserName
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtBsuName")
                lstrBsName = Trim(txtSearch.Text)
                If (lstrBsName <> "") Then lstrFilterBsName = SetCondn(lstrOpr, "vw_OSO_BUSINESSUNIT_M.BSU_NAME", lstrBsName)

                '   -- 222 Emp
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocType")
                lstrDocName = Trim(txtSearch.Text)
                If (lstrDocName <> "") Then lstrFilterDocName = SetCondn(lstrOpr, "DOCUMENT_M.DOC_NAME", lstrDocName)

                '-----333 Narattion
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDepartment")
                lstrDptName = Trim(txtSearch.Text)
                If (lstrDptName <> "") Then lstrFilterDocName = SetCondn(lstrOpr, "VW_OSO_DEPARTMENT_M.DPT_DESCR", lstrDptName)



            End If
            Dim str_Filter As String = ""

            Dim str_ListDoc As String = String.Empty


            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If

            

            str_Sql = " SELECT " & str_Topfilter _
                    & " vw_OSO_BUSINESSUNIT_M.BSU_NAME, DOCUMENT_M.DOC_NAME, VW_OSO_DEPARTMENT_M.DPT_DESCR, " _
                    & " vw_OSO_APPROVAL_M.APM_BSU_ID,vw_OSO_APPROVAL_M.APM_DOC_ID,vw_OSO_APPROVAL_M.APM_DPT_ID  " _
                    & " FROM vw_OSO_APPROVAL_M INNER JOIN  vw_OSO_BUSINESSUNIT_M  " _
                    & " ON vw_OSO_APPROVAL_M.APM_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN  DOCUMENT_M  " _
                    & " ON vw_OSO_APPROVAL_M.APM_DOC_ID = DOCUMENT_M.DOC_ID INNER JOIN  VW_OSO_DEPARTMENT_M  " _
                    & " ON vw_OSO_APPROVAL_M.APM_DPT_ID = VW_OSO_DEPARTMENT_M.DPT_ID   " _
                    & str_Filter & lstrFilterDocName & lstrFilterDocName & lstrFilterBsName _
                    & " GROUP BY vw_OSO_BUSINESSUNIT_M.BSU_NAME, DOCUMENT_M.DOC_NAME, VW_OSO_DEPARTMENT_M.DPT_DESCR,  " _
                    & " vw_OSO_APPROVAL_M.APM_BSU_ID,vw_OSO_APPROVAL_M.APM_DOC_ID,vw_OSO_APPROVAL_M.APM_DPT_ID  " _
                    & " ORDER BY BSU_NAME,DOC_NAME "


            ds = MainObj.ListRecords(str_Sql, "mainDB")
            gvJournal.DataSource = ds
            If ds.Rows.Count = 0 Then
                ds.Rows.Add()
                'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."



            Else
                gvJournal.DataBind()

            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtBsuName")
            txtSearch.Text = lstrBsName

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocType")
            txtSearch.Text = lstrDocName

            txtSearch = gvJournal.HeaderRow.FindControl("txtDepartment")
            txtSearch.Text = lstrDptName

            gvJournal.SelectedIndex = p_selected_id


        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnBsuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnDocumet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnDepartment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
