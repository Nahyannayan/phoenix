﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accInterunitMonthendSetOff
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvIJVDetails.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ddlBusinessunit.DataBind() 
                gvIJVDetails.DataBind()
                If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                    ddlBusinessunit.SelectedIndex = -1
                    ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
                    txtdocDate.Text = UtilityObj.GetDataFromSQL("SELECT REPLACE(CONVERT(VARCHAR(11), BSU_FREEZEDT, 113), ' ', '/')  " & _
                            " FROM BUSINESSUNIT_M WHERE (BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "')", ConnectionManger.GetOASISConnectionString)
                    bindJouranl()
                End If
                If Session("sUsr_name") Is Nothing Or Session("sBsuid") Is Nothing Or (ViewState("MainMnu_code").ToString <> "A210010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code").ToString)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not IsDate(txtdocDate.Text) Then
            lblError.Text = "Please enter a valid date"
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim stTrans As SqlTransaction
        Dim iReturnvalue As Integer
        Dim lblBSU_ID As Label
        objConn.Open()
        stTrans = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand("SaveMONTHEND_ADJ", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            '@MAJ_BSU_ID varchar(20),
            '@MAJ_OPP_BSU_ID varchar(20),
            '@MAJ_ACT_ID varchar(20), 
            '@MAJ_DATE datetime,
            '@MAJ_AMOUNT numeric(18, 3), 
            '@MAJ_USER varchar(50) 
            For Each gvr As GridViewRow In gvIJVDetails.Rows
                If gvr.RowType = DataControlRowType.DataRow Then
                    Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
                    'Dim lblVCD_BNK_ID As Label = CType(gvr.FindControl("lblVCD_BNK_ID"), Label)
                    'Dim lblVCD_EMR_ID As Label = CType(gvr.FindControl("lblVCD_EMR_ID"), Label)
                    lblBSU_ID = CType(gvr.FindControl("lblBSU_ID"), Label)
                    If Not ChkPost Is Nothing And Not lblBSU_ID Is Nothing Then
                        If ChkPost.Checked Then
                            Dim sqlpMAJ_BSU_ID As New SqlParameter("@MAJ_BSU_ID", SqlDbType.VarChar, 20)
                            sqlpMAJ_BSU_ID.Value = ddlBusinessunit.SelectedItem.Value
                            cmd.Parameters.Add(sqlpMAJ_BSU_ID)

                            Dim sqlpMAJ_OPP_BSU_ID As New SqlParameter("@MAJ_OPP_BSU_ID", SqlDbType.VarChar, 20)
                            sqlpMAJ_OPP_BSU_ID.Value = lblBSU_ID.Text
                            cmd.Parameters.Add(sqlpMAJ_OPP_BSU_ID)

                            Dim sqlpMAJ_ACT_ID As New SqlParameter("@MAJ_ACT_ID", SqlDbType.VarChar, 100)
                            sqlpMAJ_ACT_ID.Value = gvr.Cells(0).Text
                            cmd.Parameters.Add(sqlpMAJ_ACT_ID)

                            Dim sqlpMAJ_DATE As New SqlParameter("@MAJ_DATE", SqlDbType.DateTime)
                            sqlpMAJ_DATE.Value = txtdocDate.Text
                            cmd.Parameters.Add(sqlpMAJ_DATE)

                            Dim sqlpMAJ_USER As New SqlParameter("@MAJ_USER", SqlDbType.VarChar, 100)
                            sqlpMAJ_USER.Value = Session("sUsr_name")
                            cmd.Parameters.Add(sqlpMAJ_USER)

                            Dim sqlpMAJ_AMOUNT As New SqlParameter("@MAJ_AMOUNT", SqlDbType.Decimal)
                            sqlpMAJ_AMOUNT.Value = gvr.Cells(2).Text
                            cmd.Parameters.Add(sqlpMAJ_AMOUNT)

                            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                            retValParam.Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add(retValParam)
                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value

                            cmd.Parameters.Clear()
                            If iReturnvalue <> 0 Then Exit For

                        End If
                    End If
                End If
            Next
            If iReturnvalue = 0 Then iReturnvalue = UtilityObj.operOnAudiTable(Master.MenuName, "JNL_ID:", "", Page.User.Identity.Name.ToString, Me.Page, "Jouranl REGISTER")
            If (iReturnvalue = 0) Then
                stTrans.Commit()
                ClearDetails()
            Else
                stTrans.Rollback()
            End If
            lblError.Text = getErrorMessage(iReturnvalue)
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ClearDetails() 
        gvIJVDetails.DataBind() 
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        If Not IsDate(txtdocDate.Text) Then
            txtdocDate.Text = UtilityObj.GetDataFromSQL("SELECT REPLACE(CONVERT(VARCHAR(11), BSU_FREEZEDT, 113), ' ', '/')  " & _
                                       " FROM BUSINESSUNIT_M WHERE (BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "')", ConnectionManger.GetOASISConnectionString)
        End If
        bindJouranl()
    End Sub

    Protected Sub imgBankSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindJouranl()
    End Sub

    Sub bindJouranl()
        Try
            Dim str_sql As String = " exec [dbo].[RptGetInterunitReconForSetoff] '" & ddlBusinessunit.SelectedItem.Value & "','" & txtdocDate.Text & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            gvIJVDetails.DataSource = ds
            gvIJVDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvIJVDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvIJVDetails.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ChkPost As CheckBox
            ChkPost = e.Row.FindControl("ChkPost")
            Dim Amount, OppAmount As String
            Amount = e.Row.Cells(2).Text
            OppAmount = e.Row.Cells(4).Text
            If IsNumeric(Amount) AndAlso IsNumeric(OppAmount) Then
                If Convert.ToDecimal(Amount) = Convert.ToDecimal(OppAmount) * -1 Then
                    ChkPost.Checked = True
                End If
            End If

        End If
    End Sub

    Protected Sub txtdocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        bindJouranl()
    End Sub

End Class
