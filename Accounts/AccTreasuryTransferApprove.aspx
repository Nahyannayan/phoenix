<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccTreasuryTransferApprove.aspx.vb" Inherits="Accounts_AccTreasuryTransferApprove" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }

        Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                   document.getElementById('<%= h_print.ClientID %>').value = '';
                   showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
               }
           }
                    );
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_FirstVoucher" runat="server" type="hidden" /></td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Vouchers for posting" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>

                                    <asp:BoundField DataField="ITF_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc Date"
                                        HtmlEncode="False" SortExpression="ITF_DATE">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ITF_NARRATION" HeaderText="Narration" SortExpression="ITF_NARRATION">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAYEDUNIT" HeaderText="Paid by" ReadOnly="True"
                                        SortExpression="PAYEDUNIT">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RECEIVEDUNIT" HeaderText="Received by" ReadOnly="True"
                                        SortExpression="RECEIVEDUNIT">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20%" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ITF_CUR_ID" HeaderText="Currency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("ITF_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Post" SortExpression="ITF_bPOSTED">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" checked='<%# Bind("ITF_bPOSTED") %>' type="checkbox"
                                                value='<%# Bind("IRF_ID") %>' />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                        <HeaderTemplate>
                                            Post<input id="chkSelectall" type="checkbox" onclick="fnSelectAll(this)" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                Text="Summary" Visible="false"></asp:LinkButton>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("IRF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" Visible="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" Enabled="False">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print Voucher" Checked="True" />
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" />&nbsp;<br />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

