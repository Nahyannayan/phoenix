﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Accounts_CustomerAccountMaster
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "A100359") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    GridBind("", "", "")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCusAcctDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvCusAcctDetails.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvCusAcctDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvCusAcctDetails.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub GridBind(ByVal Cust_Act_Code As String, ByVal Act_Code As String, ByVal Acct_name As String)
        Dim conn As String = ConnectionManger.GetOASISFINConnectionString
        Dim ds As DataSet
        Dim Param(3) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@CUS_ACT_CODE", Cust_Act_Code, SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@ACT_ID", Act_Code, SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@ACT_NAME", Acct_name, SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[DAX].[SP_GET_CUSTOMER_ACCOUNT_DETAILS]", Param)
        gvCusAcctDetails.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvCusAcctDetails.DataBind()
            Dim columnCount As Integer = gvCusAcctDetails.Rows(0).Cells.Count
            gvCusAcctDetails.Rows(0).Cells.Clear()
            gvCusAcctDetails.Rows(0).Cells.Add(New TableCell)
            gvCusAcctDetails.Rows(0).Cells(0).ColumnSpan = columnCount
            gvCusAcctDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvCusAcctDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvCusAcctDetails.DataBind()
        End If
        Dim txtCus_Act_Code As New TextBox
        txtCus_Act_Code = gvCusAcctDetails.HeaderRow.FindControl("txtCus_Act_Code")
        txtCus_Act_Code.Text = Cust_Act_Code
        Dim txtAct_Code As New TextBox
        txtAct_Code = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Code")
        txtAct_Code.Text = Act_Code
        Dim txtAct_Name As New TextBox
        txtAct_Name = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Name")
        txtAct_Name.Text = Acct_name
        set_Menu_Img()
    End Sub
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Accounts\CustomerAccountCreation.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvCusAcctDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCusAcctDetails.PageIndexChanging
        Try
            Dim Cust_Act_Code As String = ""
            Dim Act_Code As String = ""
            Dim Acct_name As String = ""
            If gvCusAcctDetails.Rows.Count > 0 Then
                Dim txtCus_Act_Code As New TextBox
                txtCus_Act_Code = gvCusAcctDetails.HeaderRow.FindControl("txtCus_Act_Code")
                Cust_Act_Code = txtCus_Act_Code.Text
                Dim txtAct_Code As New TextBox
                txtAct_Code = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Code")
                Act_Code = txtAct_Code.Text
                Dim txtAct_Name As New TextBox
                txtAct_Name = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Name")
                Acct_name = txtAct_Name.Text

            End If
            gvCusAcctDetails.PageIndex = e.NewPageIndex
            GridBind(Cust_Act_Code, Act_Code, Acct_name)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub gvCusAcctDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCusAcctDetails.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvCusAcctDetails.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblCust_Act_Code As Label
                With selectedRow
                    lblCust_Act_Code = .Cells(0).FindControl("lblCust_Act_Code")
                End With
                Dim url As String
                url = String.Format("~\Accounts\CustomerAccountCreation.aspx?MainMnu_code={0}&datamode={1}&cus_act_code=" + Encr_decrData.Encrypt(lblCust_Act_Code.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnCus_Act_Code_Search_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim Cust_Act_Code As String = ""
            Dim Act_Code As String = ""
            Dim Acct_name As String = ""
            If gvCusAcctDetails.Rows.Count > 0 Then
                Dim txtCus_Act_Code As New TextBox
                txtCus_Act_Code = gvCusAcctDetails.HeaderRow.FindControl("txtCus_Act_Code")
                Cust_Act_Code = txtCus_Act_Code.Text
                Dim txtAct_Code As New TextBox
                txtAct_Code = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Code")
                Act_Code = txtAct_Code.Text
                Dim txtAct_Name As New TextBox
                txtAct_Name = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Name")
                Acct_name = txtAct_Name.Text
                GridBind(Cust_Act_Code, Act_Code, Acct_name)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAct_Code_Search_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim Cust_Act_Code As String = ""
            Dim Act_Code As String = ""
            Dim Acct_name As String = ""
            If gvCusAcctDetails.Rows.Count > 0 Then
                Dim txtCus_Act_Code As New TextBox
                txtCus_Act_Code = gvCusAcctDetails.HeaderRow.FindControl("txtCus_Act_Code")
                Cust_Act_Code = txtCus_Act_Code.Text
                Dim txtAct_Code As New TextBox
                txtAct_Code = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Code")
                Act_Code = txtAct_Code.Text
                Dim txtAct_Name As New TextBox
                txtAct_Name = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Name")
                Acct_name = txtAct_Name.Text
                GridBind(Cust_Act_Code, Act_Code, Acct_name)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAct_Name_Search_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim Cust_Act_Code As String = ""
            Dim Act_Code As String = ""
            Dim Acct_name As String = ""
            If gvCusAcctDetails.Rows.Count > 0 Then
                Dim txtCus_Act_Code As New TextBox
                txtCus_Act_Code = gvCusAcctDetails.HeaderRow.FindControl("txtCus_Act_Code")
                Cust_Act_Code = txtCus_Act_Code.Text
                Dim txtAct_Code As New TextBox
                txtAct_Code = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Code")
                Act_Code = txtAct_Code.Text
                Dim txtAct_Name As New TextBox
                txtAct_Name = gvCusAcctDetails.HeaderRow.FindControl("txtAct_Name")
                Acct_name = txtAct_Name.Text
                GridBind(Cust_Act_Code, Act_Code, Acct_name)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
