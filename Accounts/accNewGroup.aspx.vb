Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class NewGroup
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150001"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                viewstate("datamode") = "none"
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A100001") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                viewstate("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
            End If
            If Request.QueryString("editid") = "" Then
                If ViewState("datamode") <> "add" Then
                    set_view_data()
                End If
            Else
                'tbl_AddGroup.Visible = False
                'tbl_ModifyGroup.Visible = True
                reset_view_data()
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim encObj As New Encryption64
                setModifyvalues(encObj.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            End If

            UtilityObj.beforeLoopingControls(Me.Page)
        End If
    End Sub


    'Protected Sub btnAddgroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddgroup.Click
    '    lblError.Text = ""
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '    Dim objConn As New SqlConnection(str_conn)
    '    objConn.Open()
    '    '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '    Try
    '        Dim str_success As String = MasterFunctions.fnDoGroupOperations("Insert", txtGroupcode.Text & "", txtGroupname.Text & "")
    '        If (str_success = "0") Then
    '            Dim encObj As New Encryption64

    '            Response.Redirect("accGroupmaster.aspx?modified=" & encObj.Encrypt("254") & "&newid=" & txtGroupcode.Text & "")
    '        Else
    '            lblError.Text = getErrorMessage(str_success)
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub


    'Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
    '    Dim str_success As String = MasterFunctions.fnDoGroupOperations("Update", txtMGroupcode.Text & "", txtMGroupname.Text & "")
    '    If (str_success = "0") Then
    '        'lblMError.Text = displayMessage("258", 20, "D")
    '        Dim encObj As New Encryption64
    '        Response.Redirect("accGroupmaster.aspx?modified=" & encObj.Encrypt("258"))
    '    Else
    '        lblMError.Text = getErrorMessage(str_success)
    '    End If
    'End Sub
    Sub set_view_data()
        txtGroupcode.Attributes.Add("readonly", "readonly")
        txtGroupname.Attributes.Add("readonly", "readonly")
    End Sub

    Sub reset_view_data()
        txtGroupcode.Attributes.Remove("readonly")
        txtGroupname.Attributes.Remove("readonly")
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            'txtGroupcode.Attributes.Remove("readonly")
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ACCGRP_M where GPM_ID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtGroupcode.Text = p_Modifyid
                txtGroupname.Text = ds.Tables(0).Rows(0)("GPM_DESCR")
            Else
                Response.Redirect("accGroupmaster.aspx")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call clear_all()
        reset_view_data()
        viewstate("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
    End Sub


    Sub clear_all()
        txtGroupcode.Text = ""
        txtGroupname.Text = ""
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        viewstate("datamode") = "edit"
        txtGroupcode.Attributes.Add("readonly", "readonly")
        txtGroupname.Attributes.Remove("readonly")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If viewstate("datamode") = "add" Then
            Response.Redirect(viewstate("ReferrerUrl"))
        End If
        If viewstate("datamode") = "edit" Then

            Call clear_all()
            'clear the textbox and set the default settings

            viewstate("datamode") = "none"
            setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
        Else
            Response.Redirect(viewstate("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If viewstate("datamode") = "edit" Then

            Dim str_success As String = MasterFunctions.fnDoGroupOperations("Update", txtGroupcode.Text & "", txtGroupname.Text & "")
            If (str_success = "0") Then
                'lblMError.Text = displayMessage("258", 20, "D")
                Dim encObj As New Encryption64
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtGroupcode.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If


                viewstate("datamode") = Encr_decrData.Encrypt(viewstate("datamode"))
                Response.Redirect("accGroupmaster.aspx?modified=" & encObj.Encrypt("258") & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            Else
                lblError.Text = getErrorMessage(str_success)
            End If
        Else
            lblError.Text = ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim str_success As String = MasterFunctions.fnDoGroupOperations("Insert", txtGroupcode.Text & "", txtGroupname.Text & "")
                If (str_success = "0") Then
                    Dim encObj As New Encryption64
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtGroupcode.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If

                    Response.Redirect("accGroupmaster.aspx?modified=" & encObj.Encrypt("254") & "&newid=" & txtGroupcode.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code"))
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
          


            If Not Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) Is Nothing Then
                Dim str_success As String = MasterFunctions.fnDoGroupOperations("Delete", Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) & "", "")
                If (str_success = "0") Then
                    lblError.Text = getErrorMessage("256")

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtGroupcode.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    clear_all()
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
