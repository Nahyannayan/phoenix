Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_accFreezePeriod
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                bindBusinessunit()
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state 
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((MainMnu_code <> "D050050") And (MainMnu_code <> "A200050")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    trCheckList.Visible = False
                    Select Case MainMnu_code
                        Case "D050050"
                            lblHeader.Text = "Freeze Transactions"
                        Case "A200050"
                            lblHeader.Text = "Do Month End"
                            trCheckList.Visible = True
                    End Select
                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub


    Sub bindBusinessunit()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     USRBSU.BSU_NAME, LTRIM(RTRIM(USRBSU.BSU_ID)) + '||' +" _
             & " REPLACE(CONVERT(VARCHAR(11), BSU.BSU_FREEZEDT, 113), ' ', '/') AS BSUID" _
             & " FROM dbo.fn_GetBusinessUnits('" & Session("sUsr_name") & "') AS USRBSU INNER JOIN" _
             & " BUSINESSUNIT_M AS BSU ON USRBSU.BSU_ID = BSU.BSU_ID ORDER BY USRBSU.BSU_NAME"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddBusinessunit.Items.Clear()
            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSUID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = 0
            ddBusinessunit.SelectedIndex = -1
            For Each item As ListItem In ddBusinessunit.Items
                If item.Value.ToString.Contains(Session("sBSUID")) Then
                    item.Selected = True
                End If
            Next
            txtFrom.Text = ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        txtFrom.Text = ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-")
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dt1, dt2 As Date
            Dim strBusinessUnit As String
            strBusinessUnit = ddBusinessunit.SelectedItem.Text
            dt1 = CDate(txtFrom.Text)
            dt2 = CDate(txtNewFreezeDate.Text)
            If (dt1 > dt2) Then
                lblError.Text = "Choose Date Grteater than Previous Freeze Date"
            End If
            Dim MainMnu_code As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If MainMnu_code = "A200050" Then
                If Not UsrCheckList1.MandatoryFieldsChecked Then
                    lblError.Text = "Please select All Mandatory Check List"
                    Exit Sub
                End If
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            '@DOC_ID = N'dn',
            '		@BSU_ID = N'125016',
            '		@DOS_MONTH = 11,
            '		@DOS_YEAR = 2007,
            '		@NextNO = @NextNO OUTPUT
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Dim conn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            conn.Open()
            Dim trans As SqlTransaction = conn.BeginTransaction

            Try
                'New code inserted by Shijin for check list
                If MainMnu_code = "A200050" Then
                    If Not UsrCheckList1.SaveCheckListDetails(conn, trans, txtNewFreezeDate.Text, ddBusinessunit.SelectedItem.Value.Split("||")(0)) Then
                        lblError.Text = "Please select All Mandatory Check List"
                        trans.Rollback()
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
                '*********

                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = ddBusinessunit.SelectedItem.Value.Split("||")(0)
                pParms(1) = New SqlClient.SqlParameter("@BSU_FREEZEDT", SqlDbType.DateTime)
                pParms(1).Value = dt2

                pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                'Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim retval As Integer
                retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveBSU_FREEZEDT", pParms)
                If pParms(2).Value = "0" Then
                    lblError.Text = getErrorMessage("0")
                    stTrans.Commit()
                    trans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtNewFreezeDate.Text, "Freeze", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    lblError.Text = getErrorMessage(pParms(2).Value)
                    stTrans.Rollback()
                    trans.Rollback()
                End If
                txtNewFreezeDate.Text = ""
                bindBusinessunit()
                ddBusinessunit.SelectedIndex = -1
                ddBusinessunit.Items.FindByText(strBusinessUnit).Selected = True
                txtFrom.Text = ddBusinessunit.SelectedItem.Value.Split("||")(2).Replace(" ", "-")
            Catch ex As Exception
                stTrans.Rollback()
                trans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
                conn.Close()
            End Try
        Catch ex As Exception
            lblError.Text = "Please check the date"
        End Try
    End Sub


End Class
