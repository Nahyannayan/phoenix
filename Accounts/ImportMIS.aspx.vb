Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj



Partial Class Accounts_ImportMIS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Public Property GroupCurr() As String
        Get
            GroupCurr = ViewState("GroupCurr")
        End Get
        Set(ByVal value As String)
            ViewState("GroupCurr") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        txtExchangeRate.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtBSExchangeRate.Attributes.Add("onkeypress", " return Numeric_Only()")
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            doClear()
            initialize_components()
            filldrp()
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim sql As String
            sql = "select top 1 sys_cur_id from sysinfo_s"
            GroupCurr = Mainclass.getDataValue(sql, "maindb")
            lblGroupCurr.Text = GroupCurr
            sql = "SELECT BSU_NAME FROM OASIS.dbo.BUSINESSUNIT_M  WHERE BSU_ID = '" & Session("sBsuid") & "' ORDER BY BSU_NAME "
            lblBSUnit.Text = Mainclass.getDataValue(sql, "maindb")

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                If ViewState("datamode") = "view" Then
                    Dim viewid As String = ""
                    viewid = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    bindData(viewid)
                ElseIf ViewState("datamode") = "add" Then
                    LockYN(False)
                End If


            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200359") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

    Sub bindData(ByVal viewid As String)
        Dim SqlQuery As String = ""
        SqlQuery = "SELECT * FROM MIS_IMPORT_H WHERE MIH_ID = " & viewid
        Dim _table As DataTable = MainObj.getRecords(SqlQuery, "mainDB")
        If _table.Rows.Count > 0 Then
            h_SelectId.Value = viewid
            h_Posted.Value = _table.Rows(0)("MIH_BPosted")

            If Not drpCurrency.Items.FindByText(_table.Rows(0)("MIH_CUR_ID")) Is Nothing Then
                drpCurrency.SelectedValue = drpCurrency.Items.FindByText(_table.Rows(0)("MIH_CUR_ID")).Value
            End If
            txtHDocdate.Text = Convert.ToDateTime(_table.Rows(0)("MIH_DATE").ToString()).ToString("dd/MMM/yyyy")
            txtExchangeRate.Text = _table.Rows(0).Item("MIH_EXG")
            txtBSExchangeRate.Text = _table.Rows(0).Item("MIH_BS_EXG")
            txtRemarks.Text = _table.Rows(0).Item("MIH_REMARKS")
            h_Posted.Value = _table.Rows(0).Item("MIH_Bposted")
            SqlQuery = " select MID_ID,MID_MIH_ID,MID_RSS_ID [ID],MID_RSS_CODE [CODE],MID_RSS_TYP [TYPE],"
            SqlQuery = SqlQuery & " MID_RSS_DESCR [DESCR],MID_AMOUNT [AED] , MID_LOCAL_AMOUNT [LOCALCURRENCY],0 NotValid from MIS_IMPORT_D"
            SqlQuery = SqlQuery & " WHERE MID_MIH_ID = " & viewid & ""
            _table = MainObj.getRecords(SqlQuery, "mainDB")
            gvExcel.DataSource = _table
            gvExcel.DataBind()
            LockYN(False)
        End If
    End Sub
    Sub LockYN(ByVal lockYN As Boolean)
        drpCurrency.Enabled = lockYN
        txtHDocdate.Enabled = lockYN
        txtExchangeRate.Enabled = lockYN
        txtBSExchangeRate.Enabled = lockYN
        txtRemarks.Enabled = lockYN
        gvExcel.Enabled = lockYN
        lblGroupCurr.Enabled = lockYN
    End Sub
    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
    End Sub

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
            txtRemarks.Text = "MIS Data For The Month " & MonthName(CDate(txtHDocdate.Text).Month)
        End If
    End Sub

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        txtExchangeRate.Text = drpCurrency.SelectedValue.ToString()
        txtBSExchangeRate.Text = drpCurrency.SelectedValue.ToString()
        txtRemarks.Text = ""
        h_Editid.Value = "0"
        h_SelectId.Value = "0"
        btnSave.Visible = False
        btnFind.Enabled = True
        btnAdd.Visible = True
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        LockYN(False)
        doClear()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Private Sub UpLoadDBF()
        If uploadFile.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += uploadFile.FileName.ToString().Substring(uploadFile.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If uploadFile.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                uploadFile.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT EXGRATE_S.EXG_RATE, CURRENCY_M.CUR_ID FROM CURRENCY_M INNER JOIN EXGRATE_S ON CURRENCY_M.CUR_ID = EXGRATE_S.EXG_CUR_ID WHERE EXG_BSU_ID = '" & Session("sBsuid").ToString() & "'"
            fillDropdown(drpCurrency, Query, "CUR_ID", "EXG_RATE", "mainDB", True)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function getRSS_TYPE(ByVal Type As String) As String
        Select Case UCase(Type)
            Case "P/L"
                getRSS_TYPE = "MP"
            Case "BS"
                getRSS_TYPE = "MB"
            Case "CASHFLOW"
                getRSS_TYPE = "MC"
            Case Else
                getRSS_TYPE = ""
        End Select
    End Function

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim HDocDate, Currency As String
            HDocDate = Mainclass.ReadValueFromExcel(filePath, 1, 1, 3)
            txtHDocdate.Text = IIf(IsDate(HDocDate), Format(CDate(HDocDate), OASISConstants.DateFormat), Format(CDate(Now.Date), OASISConstants.DateFormat))
            txtRemarks.Text = "MIS Data For The Month " & MonthName(CDate(txtHDocdate.Text).Month)
            Currency = Mainclass.ReadValueFromExcel(filePath, 1, 2, 3)
            Dim lstDrp As New ListItem
            lstDrp = drpCurrency.Items.FindByValue(Currency)
            If lstDrp Is Nothing Then
                lstDrp = drpCurrency.Items.FindByText(Currency)
            End If
            If Not lstDrp Is Nothing Then
                drpCurrency.SelectedValue = lstDrp.Value
            ElseIf drpCurrency.Items.Count > 0 Then
                drpCurrency.SelectedIndex = 0
            End If

            txtExchangeRate.Text = Mainclass.ReadValueFromExcel(filePath, 1, 1, 6)
            txtBSExchangeRate.Text = Mainclass.ReadValueFromExcel(filePath, 1, 2, 6)
            Dim xltable As DataTable
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 3, 8)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()

            Dim RSS_IDs As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString Then
                    RSS_IDs &= IIf(RSS_IDs <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                End If
            Next
            If RSS_IDs = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If
            Dim iTable As DataTable
            Dim query As String
            query = "select RSS_ID,RSS_CODE,RSS_SLNO,RSS_DESCR,RSS_TYP from RPTSETUP_S where RSS_ID in (" & RSS_IDs & ")"
            iTable = MainObj.getRecords(query, "mainDB")
            xltable.Columns.Add("NotValid", System.Type.GetType("System.String"))
            xltable.Columns.Add("ErrorText", System.Type.GetType("System.String"))
            xltable.Columns("NotValid").DefaultValue = "0"
            Dim iRow As DataRow
            Dim iNarr As String = ""
            Dim BSSum As Double
            For Each xlRow In xltable.Rows
                If iTable.Select("RSS_ID=" & xlRow(ColName), "").Length > 0 Then
                    iRow = iTable.Select("RSS_ID=" & xlRow(ColName), "")(0)
                    xlRow("TYPE") = getRSS_TYPE(xlRow("TYPE"))
                    If iRow("RSS_CODE") <> xlRow("CODE") Then
                        xlRow("NotValid") = "1"
                        iNarr = " Invalid Code - " & xlRow("CODE")
                        xlRow("ErrorText") = iNarr
                    End If
                    'If iRow("RSS_SLNO") <> xlRow("SLNO") Then  ' SL No Validation Not Required
                    '    xlRow("NotValid") = 1
                    '    iNarr = " Invalid SLNo - " & xlRow("SLNO")
                    'End If
                    If iRow("RSS_TYP") <> xlRow("TYPE") Then
                        xlRow("NotValid") = "1"
                        iNarr = Trim(IIf(Trim(iNarr) <> "", Environment.NewLine, "") & "Invalid TYPE - " & xlRow("TYPE"))
                        xlRow("ErrorText") = iNarr
                    Else
                        If xlRow("TYPE") = "MB" And IsNumeric(xlRow("AED")) Then
                            BSSum += Val(xlRow("AED"))
                        End If
                    End If
                    If Not IsNumeric(xlRow("Localcurrency")) Then
                        If xlRow("Localcurrency").ToString = "" Then
                            xlRow("Localcurrency") = 0
                        Else
                            xlRow("NotValid") = "1"
                            iNarr = Trim(IIf(Trim(iNarr) <> "", Environment.NewLine, "") & "Invalid Local Currency Amount Entered ")
                            xlRow("ErrorText") = iNarr
                        End If
                    Else
                        xlRow("Localcurrency") = Convert.ToDouble(Convert.ToDouble(xlRow("Localcurrency"))).ToString("####0.00")
                    End If
                    If Not IsNumeric(xlRow("AED")) Then
                        If xlRow("AED").ToString = "" Then
                            xlRow("AED") = 0
                        Else
                            xlRow("NotValid") = "1"
                            iNarr = Trim(IIf(Trim(iNarr) <> "", Environment.NewLine, "") & "Invalid AED Amount Entered")
                            xlRow("ErrorText") = iNarr
                        End If
                    Else
                        xlRow("AED") = Convert.ToDouble(Convert.ToDouble(xlRow("AED"))).ToString("####0.00")
                    End If
                Else
                    xlRow("NotValid") = "1"
                    iNarr = Trim(IIf(Trim(iNarr) <> "", Environment.NewLine, "") & "Invalid " & ColName & " - " & xlRow(ColName))
                    xlRow("ErrorText") = iNarr
                End If
                If iNarr = "" Then
                    xlRow("NotValid") = "0"
                End If
                iNarr = ""
            Next
            xltable.AcceptChanges()
            gvExcel.DataSource = xltable
            gvExcel.DataBind()
            LockYN(True)
            If BSSum <> 0 Then 'Commented
                'lblError.Text = "Balance Sheet Total is wrong !"
            End If

            If xltable.Select("NotValid =1").Length > 0 Then
                For i As Integer = 0 To xltable.Rows.Count - 1
                    If (xltable).Rows(i)("NotValid") = "1" Then
                        gvExcel.Rows(i).BackColor = Drawing.Color.Pink
                        gvExcel.Rows(i).ToolTip = (xltable).Rows(i)("ErrorText")
                    End If
                Next
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CheckSave.Equals(True) Then
                Exit Sub
            End If
            If doInsert() Then
                LockYN(False)
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Function CheckSave() As Boolean
        Dim retValue As Boolean = True
        If txtHDocdate.Text.Equals("") Or Not IsDate(txtHDocdate.Text) Then
            lblError.Text = "Invalid Date...!"
        ElseIf drpCurrency.SelectedItem Is Nothing Or drpCurrency.SelectedValue = "0" Then
            lblError.Text = "Invalid Currency...!"
        ElseIf txtExchangeRate.Text.Equals("") Or Not IsNumeric(txtExchangeRate.Text) Then
            lblError.Text = "Invalid Exchange Rate..!"
        ElseIf txtBSExchangeRate.Text.Equals("") Or Not IsNumeric(txtBSExchangeRate.Text) Then
            lblError.Text = "Invalid BS Exchange Rate..!"
        ElseIf h_Editid.Equals("0") Then
            lblError.Text = "Could not prosess your request Please contact Admin...!"
        ElseIf txtRemarks.Text = "" Then
            lblError.Text = "Narration cannot be empty!"
        Else
            retValue = False
        End If
        Dim grow As GridViewRow
        For Each grow In gvExcel.Rows
            If CType(grow.FindControl("lblNotValid"), Label).Text = "1" Then
                lblError.Text = "Could not prosess your request.Incorrect Data in the file.!"
                Return True
            End If
        Next
        Return retValue
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_Posted.Value = "True" Then
            lblError.Text = "Particular Document Already Posted..!"
            Exit Sub
        End If
        LockYN(True)
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_Posted.Value = "True" Then
            lblError.Text = "Particular Document Already Posted..!"
            Exit Sub
        End If
        If h_SelectId.Value = "0" Or h_SelectId.Value = " " Then
            lblError.Text = "Invalid Document..!"
            Exit Sub
        End If
        doDelete(h_SelectId.Value)
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            doClear()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Sub doDelete(ByVal PostId As String)
        Dim sqlParam(5) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@MIH_ID", PostId, SqlDbType.Int)
        sqlParam(1) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString(), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString(), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
        sqlParam(5) = Mainclass.CreateSqlParameter("@v_ReturnMsg", Session("sModule"), SqlDbType.VarChar, True)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "Delete_MISData", sqlParam)

        If str_success = 0 Then
            lblError.Text = "MIS Data Deleted Successfully"
            doClear()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            btnDelete.Visible = False
            btnEdit.Visible = False
        Else
            lblError.Text = sqlParam(5).Value
        End If

    End Sub
    Private Function doInsert() As Boolean
        Dim sqlParam(12) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            sqlParam(0) = Mainclass.CreateSqlParameter("@MIH_ID", h_SelectId.Value, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@MIH_DATE", Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy"), SqlDbType.DateTime)
            sqlParam(2) = Mainclass.CreateSqlParameter("@MIH_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@MIH_CUR_ID", drpCurrency.SelectedItem.Text.ToString(), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@MIH_GROUP_CUR_ID", GroupCurr, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@MIH_EXG", Val(txtExchangeRate.Text).ToString(), SqlDbType.Float)
            sqlParam(6) = Mainclass.CreateSqlParameter("@MIH_BS_EXG", Val(txtBSExchangeRate.Text).ToString(), SqlDbType.Float)
            sqlParam(7) = Mainclass.CreateSqlParameter("@MIH_REMARKS", txtRemarks.Text.ToString(), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString(), SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString(), SqlDbType.VarChar)
            sqlParam(10) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name").ToString(), SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@v_ReturnMsg", "", SqlDbType.VarChar, True, 200)
            Dim str_success As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveMISDATA_H", sqlParam)
            Dim ErrorMsg As String = ""
            If sqlParam(12).Value <> "" And sqlParam(12).Value <> "0" Then
                ErrorMsg = sqlParam(12).Value
                str_success = "-1"
            End If
            If str_success <> "-1" Then
                Dim iMIH_ID As Int32
                iMIH_ID = sqlParam(0).Value
                ReDim sqlParam(7)
                Dim grow As GridViewRow
                For Each grow In gvExcel.Rows
                    sqlParam(0) = Mainclass.CreateSqlParameter("@MID_MIH_ID", iMIH_ID, SqlDbType.Int)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@MID_RSS_ID", grow.Cells(0).Text.ToString(), SqlDbType.Int)
                    sqlParam(2) = Mainclass.CreateSqlParameter("@MID_RSS_CODE", grow.Cells(1).Text.ToString(), SqlDbType.VarChar)
                    sqlParam(3) = Mainclass.CreateSqlParameter("@MID_RSS_TYP", grow.Cells(4).Text.ToString(), SqlDbType.VarChar)
                    sqlParam(4) = Mainclass.CreateSqlParameter("@MID_RSS_DESCR", grow.Cells(3).Text.ToString(), SqlDbType.VarChar)
                    sqlParam(5) = Mainclass.CreateSqlParameter("@MID_AMOUNT", Val(grow.Cells(7).Text.ToString()).ToString(), SqlDbType.Float)
                    sqlParam(6) = Mainclass.CreateSqlParameter("@MID_LOCAL_AMOUNT", Val(grow.Cells(6).Text.ToString()).ToString(), SqlDbType.Float)
                    sqlParam(7) = Mainclass.CreateSqlParameter("@v_ReturnMsg", "", SqlDbType.NVarChar, True)
                    str_success = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveMISDATA_D", sqlParam)
                    If str_success = "-1" Then
                        Exit For
                    End If
                Next
            End If
            If str_success = "-1" Then
                stTrans.Rollback()
                lblError.Text = ErrorMsg
                doInsert = False
            Else
                stTrans.Commit()
                btnAdd.Visible = True
                btnSave.Visible = False
                btnFind.Enabled = True
                lblError.Text = "Data Saved Successfully"
                doInsert = True
            End If

        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        If (h_Editid.Value = "0") Then
            If Not uploadFile.HasFile Then
                lblError.Text = "Select Excel Sheet...!"
                Exit Sub
            End If
            If Not (uploadFile.FileName.EndsWith("xls") Or uploadFile.FileName.EndsWith("xlsx")) Then
                lblError.Text = "Invalid file type.. Only excel files are alowed.!"
                Exit Sub
            End If
            If txtHDocdate.Text.Equals("") Then
                lblError.Text = "Invalid Date..."
                Exit Sub
            End If
            UpLoadDBF()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            lblError.Text = "Data already retrieved "
        End If
    End Sub
End Class

