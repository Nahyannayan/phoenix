<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FiltervaluesFind.aspx.vb" Inherits="Accounts_FiltervaluesFind" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../cssfiles/sb-admin.css" />
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.css" />
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <%--<link rel="stylesheet" type="text/css" href="../cssfiles/title.css" />--%>
    <base target="_self" />
    <%--<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> --%>
    <style type="text/css">
        .odd {
            background-color: white;
        }

        .even {
            background-color: gray;
        }
    </style>






</head>
<body>

    <script type="text/javascript">

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

    <script type="text/javascript">

        function Mouse_Move(Obj)
        { document.getElementById(Obj).style.color = "Red"; }
        function Mouse_Out(Obj)
        { document.getElementById(Obj).style.color = "#1b80b6" }

        function getValueIn(CodeVal, NameVal, cl) {
            window.returnValue = CodeVal + "__" + NameVal
            window.close();
        }

        function listen_window(lblcode, lbClose, lblID) {
            var oArg = new Object();
            //alert(lblcode);
            oArg.NameandCode = lblcode + "||" + lbClose + "||" + lblID;
            var oWnd = GetRadWindow("'" + lblcode + "||" + lbClose + "||" + lblID + "'");
            oWnd.close(oArg);
        }


        function FilterName(control, cel, trow, tcol, txtHid) {
            var rowInd = document.getElementById(txtHid).value;
            //alert(rowInd);
            var rowId = Number(rowInd) - Number(trow);
            //alert(rowId) 
            var ColId = "gvJournal"
            if (event.keyCode == 38 || event.keyCode == 40)
            { return false; }

            var RowId;
            var CodeId;
            var NameId;

            var xy = document.getElementById("txtcount").value
            //alert(xy)
            if (xy != 0) {
                var StyleName = "griditem";

                var Mde = xy % 2;
                if (Mde == 0)
                { StyleName = "griditem_alternative" }
                //alert((Number(xy) -1))
                RowId = ColId + "_TR" + (Number(xy) - 1)
                CodeId = RowId + "_CODE"
                NameId = RowId + "_NAME"
                document.getElementById("txtcount").value = 0;
                document.getElementById(CodeId).className = StyleName;
                document.getElementById(NameId).className = StyleName;
            }
            var p = 0;
            var m = 0;
            var SS = "";
            var q = 0;
            var SString = "";
            var tr = rowInd;//document.getElementById(trow).value;
            var colom = document.getElementById(tcol).value;
            //alert(tr); 
            var name = document.getElementById(control).value.toLowerCase();

            var nameArr = new Array(tr);
            var arr = new Array(tr);

            for (i = rowId; i < tr; i++) {

                RowId = ColId + "_TR" + i
                CodeId = RowId + "_CODE"
                NameId = RowId + "_NAME"

                //alert(CodeId);
                for (j = 0; j < colom; j++) {
                    if (j > 1) {

                        SS = SS + "$" + document.getElementById("Cell" + i + j).value;
                        //alert(SS);
                    }

                }
                if (cel == "Code") {

                    SString = document.getElementById(CodeId).innerText + "$" + document.getElementById(NameId).innerText + SS;

                }
                else {

                    SString = document.getElementById(NameId).innerText + "$" + document.getElementById(CodeId).innerText + SS;
                }

                SS = "";

                if (SString.toLowerCase().indexOf(name) == 0) {
                    nameArr[p] = SString;
                    p = p + 1;
                }
                else {
                    arr[m] = SString;
                    m = m + 1;
                }

            }

            nameArr.sort();

            arr.sort();

            var dup_p = p;
            var flag = 0;

            for (i = 0; i < p; i++) {
                var _namecode = "";
                var _namcod = ""
                var RowId1 = ColId + "_TR" + rowId
                var CodeId1 = RowId1 + "_CODE"
                var NameId1 = RowId1 + "_NAME"
                // alert(CodeId1)
                rowId++;
                if (flag == 0) {
                    _namecode = nameArr[i];
                    _namcod = _namecode.split("$")
                }
                else {
                    _namecode = arr[m];
                    _namcod = _namecode.split("$");
                    m++;
                }
                for (j = 0; j < colom; j++) {
                    if (cel == "Code") {
                        document.getElementById(CodeId1).innerText = _namcod[0];
                        document.getElementById(NameId1).innerText = _namcod[1];
                    }
                    else {

                        document.getElementById(NameId1).innerText = _namcod[0];
                        document.getElementById(CodeId1).innerText = _namcod[1];

                    }
                    if (j > 1) {

                        document.getElementById("Cell" + i + j).value = _namcod[j];
                    }
                }

                if (i == dup_p - 1) {
                    p = m + p;
                    flag = 1;
                    m = 0;
                }
            }

        }





        //For selecting the items


        function selectName(trows, NId, CId, txtHid) {
            var rowInd = document.getElementById(txtHid).value;
            //alert(rowInd);
            var rowId = Number(rowInd) - Number(trows);
            var ColId = "gvJournal"
            var key = event.keyCode;
            //var trows=document.getElementById(Trow).value-2;
            var i = document.getElementById("txtcount").value;
            //var controlArray=Controls.split("/");

            if (i == 0)
                i = rowId;
            //alert(i +'..'+rowId+'..'+rowInd);
            var RowId = ColId + "_TR" + i
            var CodeId = RowId + "_CODE"
            var NameId = RowId + "_NAME"
            var StyleName = "griditem";

            var Mde = i % 2;
            if (Mde == 0)
            { StyleName = "griditem_alternative" }

            if (key == 40)//for Down arrow
            {


                if (i == rowInd) {
                    return false;
                }


                document.getElementById(CodeId).className = "griditem_hilight";

                document.getElementById(NameId).className = "griditem_hilight";

                document.getElementById(CId).value = document.getElementById(CodeId).innerText;
                document.getElementById(NId).value = document.getElementById(NameId).innerText;


                document.getElementById("txtcount").value = parseInt(i) + 1;

                if (i > rowId) {
                    RowId = ColId + "_TR" + (i - 1)
                    CodeId = RowId + "_CODE"
                    NameId = RowId + "_NAME"

                    document.getElementById(CodeId).className = StyleName;
                    document.getElementById(NameId).className = StyleName;

                }


            }
            else if (key == 38)//for UP arrow
            {

                i = i - 1;
                if (i < 0)
                    return false;

                RowId = ColId + "_TR" + i;
                CodeId = RowId + "_CODE"
                NameId = RowId + "_NAME"

                document.getElementById(CodeId).className = StyleName;
                document.getElementById(NameId).className = StyleName;

                document.getElementById("txtcount").value = parseInt(i);

                if (i != 0) {
                    RowId = ColId + "_TR" + (i - 1)
                    CodeId = RowId + "_CODE"
                    NameId = RowId + "_NAME"


                    document.getElementById(CodeId).className = "griditem_hilight";
                    document.getElementById(NameId).className = "griditem_hilight";

                    document.getElementById(CId).value = document.getElementById(CodeId).innerText;
                    document.getElementById(NId).value = document.getElementById(NameId).innerText;

                }


            }
        }
        function postItem(CodeObj, NameObj) {
            if (event.keyCode == 13) {
                var CodeVal = document.getElementById(CodeObj).value;
                var NameVal = document.getElementById(NameObj).value;
                getValueIn(CodeVal, NameVal);
            }
        }

        function sortList(trow, tcol, txtHid) {

            var rowInd = document.getElementById(txtHid).value;
            var rowId = Number(rowInd) - Number(trow);
            var ColId = "gvJournal"
            var p = 0;
            var SortString;
            var SS = "";
            var colom = document.getElementById(tcol).value;
            var tr = rowInd;//document.getElementById(trow).value;
            var sortArray = new Array(tr);
            for (i = rowId; i < tr - 2; i++) {
                for (j = 0; j < colom; j++) {
                    if (j > 1) {
                        SS = SS + "$" + document.getElementById("Cell" + i + j).value;
                    }

                }
                var RowId = ColId + "_TR" + i
                var CodeId = RowId + "_CODE"
                var NameId = RowId + "_NAME"

                SString = document.getElementById(CodeId).innerText + "$" + document.getElementById(NameId).innerText + SS;
                SS = "";
                sortArray[i] = SString;
            }

            sortArray.sort();
            //sortArray.length + rowId
            alert(sortArray.length + '....' + rowInd)
            for (i = rowId; i < rowInd - 2 ; i++) {
                var SortString = sortArray[i];
                var _sortcod = SortString.split("$");

                RowId = ColId + "_TR" + i
                CodeId = RowId + "_CODE"
                NameId = RowId + "_NAME"

                document.getElementById(CodeId).innerText = _sortcod[0];
                document.getElementById(NameId).innerText = _sortcod[1];

                for (j = 0; j < colom - 1; j++) {
                    if (j > 1) {
                        document.getElementById("Cell" + i + j).value = _sortcod[j + 1];
                    }

                }
            }
        }

    </script>
    <form id="form1" runat="server">
        <div align="center">
            <br />
            <table style="cursor: hand" width="100%">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="labHead" runat="server" Text=""></asp:Label></td>
                                <td align="center">Find..<asp:TextBox ID="txtFind" runat="server" TabIndex="2"></asp:TextBox>
                                    <asp:ImageButton ID="btnFind" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>

                        <asp:GridView ID="gvJournal" runat="server" Width="100%" CssClass="table table-bordered table-row" AllowPaging="True"
                            EmptyDataText="No Item Display" PageSize="21">

                            <EmptyDataRowStyle Wrap="True" />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <PagerStyle />
                            <EditRowStyle />


                        </asp:GridView>

                    </td>
                </tr>
            </table>

            <asp:Label runat="server" ID="MsgLable" CssClass="error"></asp:Label>&nbsp;<br />
            <asp:TextBox ID="txtRow" runat="server" Style="display: none"></asp:TextBox>
            <input id="txtcount" type="hidden" value="0" />
            <asp:TextBox ID="txtColom" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtControl" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidden" runat="server" Style="display: none">0</asp:TextBox>
            <asp:HiddenField ID="hidFrom" runat="server" />


       
            <br />

        </div>
    </form>
</body>
</html>
