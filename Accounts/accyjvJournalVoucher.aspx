<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accyjvJournalVoucher.aspx.vb" Inherits="Accounts_accyjvJournalVoucher" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function checkOldref() {
            if (document.getElementById('<%=txtHOldrefno.ClientID %>').value == '') {
                return (confirm('The Old Ref No. is Empty. Do You Want to Continue?'));
            }
            else {
                return true;
            }
        }
        function getAccount() {
            //popUp('960', '600', 'NORMAL', '<%=txtDAccountCode.ClientId %>', '<%=txtDAccountName.ClientId %>');
            var result = radopen("ShowAccount.aspx?ShowType=NORMAL&codeorname=" + document.getElementById('<%=txtDAccountCode.ClientID%>').value, "pop_up2")
            //result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            
        }
        //function CallPopup() {
          
              
            
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //}

        function CopyDetails() {
            try {
                if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                    document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
            }
            catch (ex) { }
        }

        function AddDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 800px; ";
            sFeatures += "dialogHeight: 550px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            //var result;
            var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
            //alert(url_new);
            dates = document.getElementById('<%=txtHDocdate.ClientID %>').value;
            dates = dates.replace(/[/]/g, '-')
            url_new = url_new + '&dt=' + dates;
            //result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
            var result = radopen("acccpAddDetails.aspx?" + url_new, "pop_up3")
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=txtDAccountCode.ClientID %>').focus();--%>
            //return false;
        }

        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            if (mode == 1) {
                result = window.showModalDialog("calendar.aspx?dt=" + document.getElementById('<%=txtHDocdate.ClientID %>').value, "", sFeatures)
            }
            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            if (mode == 1) {
                document.getElementById('<%=txtHDocdate.ClientID %>').value = result;
            }

            return true;
        }
        function getOther() {
            document.getElementById('<%=h_mode.ClientID %>').value = 'others';
            var sFeatures;
            sFeatures = "dialogWidth: 529px; ";
            sFeatures += "dialogHeight: 634px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: yes; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            sFeatures = "";
            var url = "jvPickDetails.aspx?ccsmode=others";
            // alert(url);
            result = window.showModalDialog(url, window, sFeatures)
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtDAccountCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtDAccountName.ClientID%>').value = NameandCode[1];              

            }
        }
        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
           <%-- var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtDAccountCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtDAccountName.ClientID%>').value = NameandCode[1];

            }--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Year End Adjustments
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Doc No [Old Ref No]</span></td>
                                    <td align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="50%">
                                                    <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td align="left" width="50%">[
                                                    <asp:TextBox ID="txtHOldrefno" runat="server" TabIndex="2"></asp:TextBox>
                                                    ]
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Doc Date</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(1);" TabIndex="6" /></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Currency</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" TabIndex="8">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="35%">
                                                    <asp:TextBox ID="txtHExchRate" runat="server"></asp:TextBox></td>
                                                <td align="left" width="30%">
                                                    <span class="field-label">Group Rate</span></td>
                                                <td align="left" width="35%">
                                                    <asp:TextBox ID="txtHLocalRate" runat="server"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Narration</span></td>
                                    <td colspan="3" align="left">
                                        <asp:TextBox ID="txtHNarration" runat="server" TextMode="MultiLine" TabIndex="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Details
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Account</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtDAccountCode" runat="server" AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDAccountCode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="btnHAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" getAccount(); return false;" TabIndex="12" />&nbsp;
                                    </td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtDAccountName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Amount</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" TabIndex="14"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                            ValidationGroup="Details">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Narration</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDNarration" runat="server" TextMode="MultiLine" TabIndex="16"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:Button ID="btnAdds" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" TabIndex="18" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="20" />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="22" />
                                        <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:Label ID="lblAddError" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table id="Table1" align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4" width="100%">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                            EmptyDataText="No Transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="Debit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDebit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("debit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Credit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("credit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="LinkButton1_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" HeaderText="TEST" Visible="False" />
                                                <asp:TemplateField HeaderText="Cost Center" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("Required") %>' Visible="False"></asp:Label>
                                                        &nbsp;
                                                <asp:LinkButton ID="lbAllocate" runat="server" Visible="False" OnClick="lbAllocate_Click">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <SelectedRowStyle />
                                            <EmptyDataRowStyle Wrap="True" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <table>
                                            <tr>
                                                <td align="left" width="10%"><span class="field-label">Debit Total</span></td>
                                                <td align="left" width="25%">
                                                    <asp:TextBox ID="txtTDotalDebit" runat="server" ReadOnly="True"></asp:TextBox></td>
                                                <td align="left" width="10%"><span class="field-label">Credit Total</span></td>
                                                <td align="left" width="25%">
                                                    <asp:TextBox ID="txtTotalCredit"
                                                        runat="server" ReadOnly="True"></asp:TextBox></td>
                                                <td align="left" width="10%"><span class="field-label">Difference</span></td>
                                                <td align="left" width="20%">
                                                    <asp:TextBox ID="txtDifference" runat="server"
                                                        ReadOnly="True"></asp:TextBox></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" ValidationGroup="Details" TabIndex="22" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return checkOldref();" TabIndex="24" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" TabIndex="28" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="30" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="32" />
                                        <br />
                                        <input id="h_Memberids" runat="server" type="hidden" />
                                        <input id="h_mode" runat="server" type="hidden" />
                                        <input id="h_editorview" runat="server" type="hidden" value="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

