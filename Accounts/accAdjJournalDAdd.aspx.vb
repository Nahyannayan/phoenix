﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accAdjJournalDAdd
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            initialize_components()
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")
            ddlReportType.DataBind()
            BindReport()
            '''''check menu rights
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or (MainMnu_code <> "A150110" And MainMnu_code <> "A200015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                btnAdd.Visible = True
                btnEdit.Visible = True
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()
            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = "Invalid Editid"
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                getnextdocid()
            End If
            ''call the object during the page load to check the initial stage
            UtilityObj.beforeLoopingControls(Me.Page)
        End If

    End Sub

    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
        ViewState("str_timestamp") = New Byte()
        txtHExchRate.Attributes.Add("readonly", "readonly")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        txtDAccountName.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        Session("dtJournal") = CreateDataTable_ADJ()
        ViewState("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
    End Sub

    Public Shared Function CreateDataTable_ADJ() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)
            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cDebit)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Private Sub setViewData()
        tbl_Details.Visible = False
        gvJournal.Columns(6).Visible = False
        gvJournal.Columns(7).Visible = False
        btnHAccount.Enabled = False
        imgCalendar.Enabled = False
        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub ResetViewData()
        tbl_Details.Visible = True
        gvJournal.Columns(6).Visible = True
        gvJournal.Columns(7).Visible = True

        btnHAccount.Enabled = True
        imgCalendar.Enabled = True
        DDCurrency.Enabled = True
        txtHDocdate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM JOURNAL_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("JHD_DOCDT")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")

                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim IntJALID As Integer
            str_Sql = "select * FROM JOURNAL_D where JAL_DOCNO='" _
            & ViewState("str_editData").Split("|")(0) & "'and JAL_BDELETED='False' " _
            & " AND (JAL_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (JAL_SUB_ID = '" & Session("Sub_ID") & "') ORDER BY JAL_ID "
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'JAL_DEBIT, JAL_CREDIT, JAL_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    Dim str_actname_cost_mand As String = AccountFunctions.getAccountname(ds.Tables(0).Rows(iIndex)("JAL_ACT_ID"), Session("sBsuid"))
                    IntJALID = ds.Tables(0).Rows(iIndex)("JAL_ID")
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("JAL_SLNO")
                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("JAL_ACT_ID")
                    rDt("Accountname") = str_actname_cost_mand.Split("|")(0)
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("JAL_NARRATION")
                    rDt("Credit") = ds.Tables(0).Rows(iIndex)("JAL_CREDIT")
                    rDt("Debit") = ds.Tables(0).Rows(iIndex)("JAL_DEBIT")
                    rDt("Status") = "Normal"
                    If IntJALID > 0 Then
                        str_Sql = "SELECT VS.VSB_DTFROM as FromDate,VS.VSB_DTTO as ToDate,CU.CUT_ID as CostUnitID,CU.CUT_EXP_ACT_ID as ExpAcc,AM.ACT_NAME as ExpAccName FROM VOUCHER_D_SUB AS VS INNER JOIN COSTUNIT_M AS CU ON VS.VSB_CUT_ID=CU.CUT_ID" _
                                        & " INNER JOIN ACCOUNTS_M AS AM ON CU.CUT_EXP_ACT_ID=AM.ACT_ID WHERE VS.VSB_VHD_ID=" & IntJALID & ""
                        ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                        If ds2.Tables(0).Rows.Count > 0 Then
                            rDt("FromDate") = IIf(IsDBNull(ds2.Tables(0).Rows(0)("FromDate")) = False, ds2.Tables(0).Rows(0)("FromDate"), "")
                            rDt("ToDate") = IIf(IsDBNull(ds2.Tables(0).Rows(0)("ToDate")) = False, ds2.Tables(0).Rows(0)("ToDate"), "")
                            rDt("CostUnit") = IIf(IsDBNull(ds2.Tables(0).Rows(0)("CostUnitID")) = False, ds2.Tables(0).Rows(0)("CostUnitID"), "")
                            rDt("ExpAcc") = IIf(IsDBNull(ds2.Tables(0).Rows(0)("ExpAcc")) = False, ds2.Tables(0).Rows(0)("ExpAcc"), "")
                            rDt("ExpAccName") = IIf(IsDBNull(ds2.Tables(0).Rows(0)("ExpAccName")) = False, ds2.Tables(0).Rows(0)("ExpAccName"), "")
                        Else
                            rDt("FromDate") = ""
                            rDt("ToDate") = ""
                            rDt("CostUnit") = ""
                            rDt("ExpAcc") = ""
                            rDt("ExpAccName") = ""
                        End If
                    End If
                    Session("dtJournal").Rows.Add(rDt)
                Next
            End If
            ViewState("idJournal") = ViewState("idJournal") + 1
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("ADJ", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtHDocdate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdds.Click
        txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
           & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        Try
            Dim rDt As DataRow
            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = AccountFunctions.Round(txtDAmount.Text.Trim)
            If dCrorDb <> 0 Then
                rDt = Session("dtJournal").NewRow
                rDt("Id") = ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = txtDAccountCode.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                If dCrorDb > 0 Then
                    rDt("Credit") = "0"
                    rDt("Debit") = dCrorDb
                Else
                    rDt("Credit") = dCrorDb * -1
                    rDt("Debit") = "0"

                End If
                rDt("GUID") = System.DBNull.Value
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtJournal").Rows(i)("Credit") = rDt("Credit") And _
                          Session("dtJournal").Rows(i)("Debit") = rDt("Debit") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                Session("dtJournal").Rows.Add(rDt)
            End If
            gridbind()
            Clear_Details()
            If gvJournal.Rows.Count = 1 Then
                txtDAmount.Text = AccountFunctions.Round(dCrorDb) * -1
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try
    End Sub

    Private Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_JV()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = AccountFunctions.Round(dCredit + Session("dtJournal").Rows(i)("Credit"))
                        Else
                            dDebit = AccountFunctions.Round(dDebit + Session("dtJournal").Rows(i)("Debit"))
                        End If
                        dDebit = AccountFunctions.Round2(dDebit, 2)
                        dCredit = AccountFunctions.Round2(dCredit, 2)
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = AccountFunctions.Round2(dDebit, 2)
            txtTotalCredit.Text = AccountFunctions.Round2(dCredit, 2)
            txtDifference.Text = AccountFunctions.Round(dDebit - dCredit)
            If dDebit <> dCredit Or dCredit = 0 Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdds.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        If ViewState("datamode") <> "edit" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        Dim IntCUTid As Integer = 0
        str_Search = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDAccountCode.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Credit")) * -1
                Else
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Debit")) 
                End If
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                btnAdds.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                gvJournal.SelectedIndex = iIndex 
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = AccountFunctions.Round(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))

            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                    & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                    & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                    & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                    & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
                    & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                    & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                '& " order by gm.GPM_DESCR "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                Else
                    txtDAccountName.Text = ""
                    lblError.Text = getErrorMessage("303")
                    Exit Sub
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try

            '''''
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then

                    Session("dtJournal").Rows(iIndex)("Accountid") = txtDAccountCode.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = txtDAccountName.Text.Trim
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Credit") = "0"
                        Session("dtJournal").Rows(iIndex)("Debit") = dCrordb
                    Else
                        Session("dtJournal").Rows(iIndex)("Credit") = dCrordb * -1
                        Session("dtJournal").Rows(iIndex)("Debit") = "0"
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim

                    btnAdds.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog("UPDATE")
        End Try
    End Sub

    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        gridbind()
        Clear_Details()
        getnextdocid()
        txtHNarration.Text = ""
        txtDNarration.Text = ""
    End Sub

    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        txtDAccountCode.Text = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strfDate As String = txtHDocdate.Text.Trim
            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtHDocdate.Text = strfDate
            End If

            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            End If
            ViewState("iDeleteCount") = 0
            Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Dim iReturnvalue As Integer
            Try
                Dim iIndex As Integer = 0
                ' ''FIND OPPOSITE ACCOUNT
                Dim str_DR_LargestAccount, str_CR_LargestAccount As String
                str_DR_LargestAccount = ""
                str_CR_LargestAccount = ""
                Dim d_cr_largest As Double = 0
                Dim d_dr_largest As Double = 0
                For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                        If Session("dtJournal").Rows(iIndex)("Debit") > d_dr_largest Then
                            str_DR_LargestAccount = Session("dtJournal").Rows(iIndex)("Accountid")
                            d_dr_largest = Session("dtJournal").Rows(iIndex)("Debit")
                        End If
                    Else
                        If Session("dtJournal").Rows(iIndex)("Credit") > d_cr_largest Then
                            str_CR_LargestAccount = Session("dtJournal").Rows(iIndex)("Accountid")
                            d_cr_largest = Session("dtJournal").Rows(iIndex)("Credit")
                        End If
                    End If
                Next
                'Adding transaction info
                Dim cmd As New SqlCommand
                Dim str_docno As String = ""
                Dim dTotal As Double = 0
                For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                        cmd.Dispose()
                        cmd = New SqlCommand("SaveJOURNAL_ADJ_D_Direct", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure
                        '' ''Handle sub table
                        Dim str_crdb As String
                        Dim str_BSU_ID As String
                        Dim str_OPPBSU_ID As String

                        If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                            dTotal = Session("dtJournal").Rows(iIndex)("Debit")
                            str_crdb = "DR"
                            str_BSU_ID = ddlBusinessunit.SelectedItem.Value
                            str_OPPBSU_ID = Session("sBSUID")
                        Else
                            dTotal = Session("dtJournal").Rows(iIndex)("Credit")
                            str_crdb = "CR"
                            str_BSU_ID = Session("sBSUID")
                            str_OPPBSU_ID = ddlBusinessunit.SelectedItem.Value
                        End If
                        '@JAL_ID bigint,  
                        '@JAL_bExclude bit, 
                        '@JAL_BSU_ID varchar(20),  
                        '@JAL_OPP_BSU_ID varchar(20), 
                        Dim sqlpJAL_ID As New SqlParameter("@JAL_ID", SqlDbType.BigInt)
                        sqlpJAL_ID.Value = 0
                        cmd.Parameters.Add(sqlpJAL_ID)

                        Dim sqlpJAL_SUB_ID As New SqlParameter("@JAL_bExclude", SqlDbType.Bit)
                        sqlpJAL_SUB_ID.Value = True
                        cmd.Parameters.Add(sqlpJAL_SUB_ID)

                        Dim sqlpsqlpJAL_BSU_ID As New SqlParameter("@JAL_BSU_ID", SqlDbType.VarChar, 20)
                        sqlpsqlpJAL_BSU_ID.Value = str_BSU_ID
                        cmd.Parameters.Add(sqlpsqlpJAL_BSU_ID)

                        Dim sqlpJAL_OPP_BSU_ID As New SqlParameter("@JAL_OPP_BSU_ID", SqlDbType.VarChar, 20)
                        sqlpJAL_OPP_BSU_ID.Value = str_OPPBSU_ID
                        cmd.Parameters.Add(sqlpJAL_OPP_BSU_ID)
                        '@JAL_FYEAR varchar(20), 
                        '@JAL_DOCTYPE varchar(20), 
                        '@JAL_DOCNO varchar(20), 
                        Dim sqlpJAL_FYEAR As New SqlParameter("@JAL_FYEAR", SqlDbType.Int)
                        sqlpJAL_FYEAR.Value = Session("F_YEAR") & ""
                        cmd.Parameters.Add(sqlpJAL_FYEAR)

                        Dim sqlpJAL_DOCTYPE As New SqlParameter("@JAL_DOCTYPE", SqlDbType.VarChar, 20)
                        sqlpJAL_DOCTYPE.Value = "ADJ"
                        cmd.Parameters.Add(sqlpJAL_DOCTYPE)

                        Dim sqlpJAL_DOCNO As New SqlParameter("@JAL_DOCNO", SqlDbType.VarChar, 20)
                        sqlpJAL_DOCNO.Value = str_docno
                        cmd.Parameters.Add(sqlpJAL_DOCNO)
                        '@JAL_CUR_ID varchar(20), 
                        '@JAL_EXGRATE1 numeric(18,3), 
                        '@JAL_EXGRATE2 numeric(18,3),
                        '@JAL_DOCDT datetime, 
                        '@JAL_ACT_ID varchar(20), 
                        Dim sqlpJAL_CUR_ID As New SqlParameter("@JAL_CUR_ID", SqlDbType.VarChar, 12)
                        sqlpJAL_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                        cmd.Parameters.Add(sqlpJAL_CUR_ID)

                        Dim sqlpJAL_EXGRATE1 As New SqlParameter("@JAL_EXGRATE1", SqlDbType.Decimal, 8)
                        sqlpJAL_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                        cmd.Parameters.Add(sqlpJAL_EXGRATE1)

                        Dim sqlpJAL_EXGRATE2 As New SqlParameter("@JAL_EXGRATE2", SqlDbType.Decimal, 8)
                        sqlpJAL_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                        cmd.Parameters.Add(sqlpJAL_EXGRATE2)

                        Dim sqlpJAL_DOCDT As New SqlParameter("@JAL_DOCDT", SqlDbType.DateTime, 30)
                        sqlpJAL_DOCDT.Value = txtHDocdate.Text & ""
                        cmd.Parameters.Add(sqlpJAL_DOCDT)

                        Dim sqlpJAL_ACT_ID As New SqlParameter("@JAL_ACT_ID", SqlDbType.VarChar, 20)
                        sqlpJAL_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                        cmd.Parameters.Add(sqlpJAL_ACT_ID)
                        '@JAL_DEBIT numeric(18,3), 
                        '@JAL_CREDIT numeric(18,3), 
                        '@JAL_NARRATION varchar(20), 
                        '@JAL_BDELETED bit=0, 
                        '@JAL_SLNO int, 
                        Dim sqlpJAL_CREDIT As New SqlParameter("@JAL_CREDIT", SqlDbType.Decimal)
                        sqlpJAL_CREDIT.Value = Session("dtJournal").Rows(iIndex)("Credit")
                        cmd.Parameters.Add(sqlpJAL_CREDIT)

                        Dim sqlpJAL_DEBIT As New SqlParameter("@JAL_DEBIT", SqlDbType.Decimal, 8)
                        sqlpJAL_DEBIT.Value = Session("dtJournal").Rows(iIndex)("Debit")
                        cmd.Parameters.Add(sqlpJAL_DEBIT)

                        Dim sqlpJAL_NARRATION As New SqlParameter("@JAL_NARRATION", SqlDbType.VarChar, 300)
                        sqlpJAL_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                        cmd.Parameters.Add(sqlpJAL_NARRATION)

                        Dim sqlpbJAL_BDELETED As New SqlParameter("@JAL_BDELETED", SqlDbType.Bit)
                        sqlpbJAL_BDELETED.Value = False
                        cmd.Parameters.Add(sqlpbJAL_BDELETED)

                        Dim sqlpbJAL_SLNO As New SqlParameter("@JAL_SLNO", SqlDbType.Int)
                        sqlpbJAL_SLNO.Value = iIndex + 1 - ViewState("iDeleteCount")
                        cmd.Parameters.Add(sqlpbJAL_SLNO)
                        '@JAL_RSS_ID varchar(20), 
                        '@JAL_OPP_ACT_ID varchar(20), 
                        '@JAL_RSS_TYP varchar(20)
                        Dim sqlpJAL_RSS_ID As New SqlParameter("@JAL_RSS_ID", SqlDbType.VarChar, 20)
                        sqlpJAL_RSS_ID.Value = ddlReprotlist.SelectedItem.Value
                        cmd.Parameters.Add(sqlpJAL_RSS_ID)

                        Dim sqlpJAL_OPP_ACT_ID As New SqlParameter("@JAL_OPP_ACT_ID", SqlDbType.VarChar, 10)
                        If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                            sqlpJAL_OPP_ACT_ID.Value = str_CR_LargestAccount
                        Else
                            sqlpJAL_OPP_ACT_ID.Value = str_DR_LargestAccount
                        End If
                        cmd.Parameters.Add(sqlpJAL_OPP_ACT_ID)

                        Dim sqlpJAL_RSS_TYP As New SqlParameter("@JAL_RSS_TYP", SqlDbType.VarChar, 20)
                        sqlpJAL_RSS_TYP.Value = ddlReportType.SelectedItem.Value
                        cmd.Parameters.Add(sqlpJAL_RSS_TYP)
                        'Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                        'If iIndex = Session("dtJournal").Rows.Count - 1 Then
                        '    sqlpbLastRec.Value = True
                        'Else
                        '    sqlpbLastRec.Value = False
                        'End If
                        'cmd.Parameters.Add(sqlpbLastRec)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        Dim sqlopJHD_NEWDOCNO As New SqlParameter("@JAL_NEWDOCNO", SqlDbType.VarChar, 20)
                        cmd.Parameters.Add(sqlopJHD_NEWDOCNO)
                        cmd.Parameters("@JAL_NEWDOCNO").Direction = ParameterDirection.Output

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value

                        If iReturnvalue <> 0 Then Exit For
                        str_docno = sqlopJHD_NEWDOCNO.Value
                        cmd.Parameters.Clear()
                        'Else
                        '    If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                        '        ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                        '        cmd = New SqlCommand("DeleteJOURNAL_D", objConn, stTrans)
                        '        cmd.CommandType = CommandType.StoredProcedure

                        '        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        '        sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                        '        cmd.Parameters.Add(sqlpGUID)

                        '        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        '        retValParam.Direction = ParameterDirection.ReturnValue
                        '        cmd.Parameters.Add(retValParam)


                        '        cmd.ExecuteNonQuery()
                        '        iReturnvalue = retValParam.Value
                        '        Dim success_msg As String = ""

                        '        If iReturnvalue <> 0 Then
                        '            lblError.Text = getErrorMessage(iReturnvalue)
                        '        End If
                        '        cmd.Parameters.Clear()
                        'End If
                    End If
                Next

                If (iReturnvalue = 0) Then
                    stTrans.Commit()
                    btnSave.Enabled = False
                    gvJournal.Enabled = True
                    txtDNarration.Text = ""
                    If ViewState("datamode") <> "edit" Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_docno, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = getErrorMessage("304")
                    Else
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = getErrorMessage("305")
                    End If
                    clear_All()
                Else
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub 

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        tbl_Details.Visible = False
        ResetViewData()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnSave.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" And ViewState("ReferrerUrl") Is Nothing Then
            Response.Redirect(Session("ReferrerUrl"))
        ElseIf ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" ' _
            If Request.QueryString("BSUID") Is Nothing Then
                str_Sql += " AND JHD_BSU_ID='" & Session("sBsuid") & "'"
            Else
                str_Sql += " AND JHD_BSU_ID='" & Request.QueryString("BSUID") & "'"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")

                ViewState("str_editData") = ds.Tables(0).Rows(0)("JHD_DOCNO") & "|" _
               & ds.Tables(0).Rows(0)("JHD_SUB_ID") & "|" _
               & ds.Tables(0).Rows(0)("JHD_DOCDT") & "|" _
               & ds.Tables(0).Rows(0)("JHD_FYEAR") & "|"
                Return ""
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
        End Try
        Return True
    End Function

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        clear_All()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub imgCaledar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReport()
    End Sub

    Sub BindReport()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "exec GetRPTSETUP_S '" & ddlReportType.SelectedValue & "'")
        ddlReprotlist.DataSource = ds
        ddlReprotlist.DataBind()
    End Sub
     
End Class

