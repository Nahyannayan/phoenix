Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_Misreportadjustment
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            ''''' 
            Page.Title = OASISConstants.Gemstitle

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A150365") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnDelete.Visible = False
                btnEdit.Visible = False
                btnPrint.Visible = False

            End If
           
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")

            txtFromDate.Text = DateTime.Now.ToString("dd/MMM/yyyy")
            filldrp()

        End If
    End Sub

   


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
           
            Dim txtAmount As New TextBox
           
            txtAmount = e.Row.FindControl("txtAmount")
            If txtAmount IsNot Nothing Then
                txtAmount.Attributes.Add("onkeypress", " return Numeric_Only()")
            End If

        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub filldrp()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim Query As String
        Query = "SELECT RSM_TYP,RSM_DESCR FROM RPTSETUP_M  ORDER BY RSM_DESCR"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
        ddMisReports.DataSource = ds
        ddMisReports.DataTextField = "RSM_DESCR"
        ddMisReports.DataValueField = "RSM_TYP"
        ddMisReports.DataBind()

        ddMisReports.Items.Insert(0, " ")
        ddMisReports.Items(0).Value = "0"
        ddMisReports.SelectedValue = "0"


    End Sub

    Private Sub fillSublink(ByVal rssType As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim Query As String
        Query = "SELECT RSS_CODE, RSS_DESCR FROM RPTSETUP_S WHERE RSS_TYP ='" & rssType & "'  AND RSS_LNE_TYP NOT IN ('T','S') " _
        & " AND RSS_DESCR <> '' order by RSS_DESCR"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
        ddlSublink.DataSource = ds
        ddlSublink.DataTextField = "RSS_DESCR"
        ddlSublink.DataValueField = "RSS_CODE"
        ddlSublink.DataBind()



    End Sub

    Private Sub gridbind(ByVal rssType As String, ByVal rssCode As String)
        Try
            If ddMisReports.SelectedIndex = -1 Then
                ddMisReports.SelectedIndex = 0
            End If
            'txtFromDate.Text = DateTime.Now.ToString("dd/MMM/yyyy")
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim Query As String
            Query = " SELECT row_number() over(order by BSU_NAME) as Slno,vw_OSO_BUSINESSUNIT_M.BSU_ID, vw_OSO_BUSINESSUNIT_M.BSU_NAME, " _
                    & " ISNULL(RPTMISDATA_D.RMS_AMOUNT,0)RMS_AMOUNT, RPTMISDATA_D.RMS_DATE,RMS_RSS_CODE " _
                    & " FROM RPTMISDATA_D RIGHT OUTER JOIN " _
                    & " vw_OSO_BUSINESSUNIT_M ON RPTMISDATA_D.RMS_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID " _
                    & " WHERE RMS_RSS_TYP = '" & rssType & "' AND RMS_RSS_CODE = '" & rssCode & "' " _
                    & " AND RPTMISDATA_D.RMS_FYEAR = " & Session("F_YEAR") & " " _
                    & " AND MONTH (RPTMISDATA_D.RMS_DATE) = " & Month(Convert.ToDateTime(txtFromDate.Text)) & "  " _
                    & " AND RMS_OWN_BSU_ID = '" & Session("sBsuid") & "' " _
                    & " ORDER BY BUS_BSG_ID, BSU_ID"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)


            If ds.Tables(0).Rows.Count = 0 Then
                Query = "SELECT row_number() over(order by BSU_NAME) as Slno, vw_OSO_BUSINESSUNIT_M.BSU_ID, vw_OSO_BUSINESSUNIT_M.BSU_NAME, 0.00 RMS_AMOUNT, NULL RMS_DATE " _
                        & " FROM vw_OSO_BUSINESSUNIT_M ORDER BY BUS_BSG_ID, BSU_ID "
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
                btnEdit.Visible = False
                btnSave.Visible = True
                btnDelete.Visible = False
                btnPrint.Visible = False
                doLockTF(True)
            Else
                txtFromDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0)("RMS_DATE")).ToString("dd/MMM/yyyy")
                ''ViewState("datamode") = "edit"

                btnEdit.Visible = True
                btnSave.Visible = False
                btnDelete.Visible = True
                btnPrint.Visible = True
                doLockTF(False)
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            gvJournal.DataSource = ds
            gvJournal.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub doLockTF(ByVal lockTF As Boolean)

        gvJournal.Enabled = lockTF
        btnSave.Enabled = lockTF
        txtFromDate.Enabled = lockTF
    End Sub

    Private Sub doSave(ByVal Mode As String)
        Dim MainObj As Mainclass = New Mainclass()
        Dim StrAmount, StrBsuId, StrSlno
        Dim x As Integer = 1
        Dim txtAmount As New TextBox
        Dim _parameter As String(,) = New String(13, 1) {}


        For Each grow As GridViewRow In gvJournal.Rows
            txtAmount = grow.FindControl("txtAmount")
            If x = 1 Then
                StrSlno = x.ToString()
                StrBsuId = grow.Cells(1).Text.ToString()
                StrAmount = txtAmount.Text.ToString()
            Else
                StrSlno += "#" & x.ToString()
                StrBsuId += "#" & grow.Cells(1).Text.ToString()
                StrAmount += "#" & txtAmount.Text.ToString()
            End If
            x = x + 1
        Next



        _parameter(0, 0) = "@RMS_RSS_TYP"
        _parameter(0, 1) = ddMisReports.SelectedValue.ToString()
        _parameter(1, 0) = "@RMS_DATE"
        _parameter(1, 1) = txtFromDate.Text.ToString()
        _parameter(2, 0) = "@RMS_FYEAR"
        _parameter(2, 1) = Session("F_YEAR").ToString()
        _parameter(3, 0) = "@RMS_OWN_BSU_ID"
        _parameter(3, 1) = Session("sBsuid").ToString()

        _parameter(4, 0) = "@RMS_BSU_ID"
        _parameter(4, 1) = StrBsuId
        _parameter(5, 0) = "@RMS_AMOUNT"
        _parameter(5, 1) = StrAmount
        _parameter(6, 0) = "@MODE"
        _parameter(6, 1) = Mode
        _parameter(7, 0) = "@SLNO"
        _parameter(7, 1) = StrSlno

        _parameter(8, 0) = "@AUD_WINUSER"
        _parameter(8, 1) = Page.User.Identity.Name.ToString()
        _parameter(9, 0) = "@Aud_form"
        _parameter(9, 1) = Master.MenuName.ToString()
        _parameter(10, 0) = "@Aud_user"
        _parameter(10, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(11, 0) = "@Aud_module"
        _parameter(11, 1) = HttpContext.Current.Session("sModule")
        _parameter(12, 0) = "@RMS_RSS_CODE"
        _parameter(12, 1) = ddlSublink.SelectedValue.ToString()


        MainObj.doExcutive("SaveRPTMISDATA_D", _parameter, "mainDB", "@v_ReturnMsg")
        lblError.Text = MainObj.MESSAGE
        If MainObj.SPRETVALUE.Equals(0) Then
            gridbind(ddMisReports.SelectedValue.ToString(), ddlSublink.SelectedValue.ToString())
        End If



    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddMisReports.SelectedValue.Equals("0") Then
                Throw New Exception("Please select Mis Report Type...!")
            End If
            If ViewState("datamode") = "edit" Then
                doSave("1")
            Else
                doSave("0")
            End If


        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub ddMisReports_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        fillSublink(ddMisReports.SelectedValue.ToString())
        gvJournal.DataSource = Nothing
        gvJournal.DataBind()

    End Sub

    

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
       
        ViewState("datamode") = "edit"
        doLockTF(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doSave("2")
        btnDelete.Visible = False
    End Sub

    
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PrintVoucher()
    End Sub
    Private Sub PrintVoucher()
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "ListRPTMISDATA_D"
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            cmd.Parameters.Add("@RMS_RSS_TYP", SqlDbType.VarChar).Value = ddMisReports.SelectedValue.ToString()
            cmd.Parameters.Add("@RMS_DATE", SqlDbType.DateTime).Value = Convert.ToDateTime(txtFromDate.Text.ToString())
            cmd.Parameters.Add("@RMS_FYEAR", SqlDbType.Int).Value = Session("F_YEAR").ToString()
            cmd.Parameters.Add("@RMS_RSS_CODE", SqlDbType.VarChar).Value = ddlSublink.SelectedValue.ToString()


            params("userName") = Session("sUsr_name")

            params("ReportHeading") = "MIS REPORTS ADJUSTMENTS"
            repSource.Parameter = params
            repSource.VoucherName = "MIS REPORTS ADJUSTMENTS"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptMisreportsAdjustment.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/RptViewer.aspx", True)

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlSublink_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind(ddMisReports.SelectedValue.ToString(), ddlSublink.SelectedValue.ToString())
    End Sub
End Class
