<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ImportTbpost.aspx.vb"
    Inherits="Accounts_ImportTbpost" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }
        Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                       document.getElementById('<%= h_print.ClientID %>').value = '';
                       showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                   }
               }
                    );

               function getAccountsdetails(getId, BsuName) {
                   alert(getId + '...' + BsuName)
                   var sFeatures;
                   sFeatures = "dialogWidth: 700px; ";
                   sFeatures += "dialogHeight: 500px; ";
                   sFeatures += "help: no; ";
                   sFeatures += "resizable: no; ";
                   sFeatures += "scroll: yes; ";
                   sFeatures += "status: no; ";
                   sFeatures += "unadorned: no; ";
                   var NameandCode;
                   var result;
                   //window.open("IndiadataExportview.aspx?viewId="+getId+"&BSU="+BsuName,"",sFeatures)

                   result = window.showModalDialog("ImportTbview.aspx?viewId=" + getId + "&BSU=" + BsuName, "", sFeatures)
                   if (result == '' || result == undefined) {
                       return false;
                   }
                   else
                       return true;

               }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_FirstVoucher" runat="server" type="hidden" /></td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Vouchers for posting" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="TDH_DATE" SortExpression="TDH_DATE" HeaderText="Doc Date">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" SortExpression="BSU_NAME" HeaderText="Bisuness Unit">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TDH_CUR_ID" HeaderText="Currency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField ReadOnly="True" DataField="TDH_EXGRATE" SortExpression="TDH_EXGRATE" HeaderText="Exchange Rate">
                                        <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField SortExpression="TDH_bPOSTED" HeaderText="Post">
                                        <EditItemTemplate>
                                            &nbsp;
                                
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Post<input id="chkSelectall" type="checkbox" onclick="fnSelectAll(this)" />

                                        </HeaderTemplate>

                                        <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" checked='<%# Bind("TDH_bPOSTED") %>' type="checkbox"
                                                value='<%# Bind("TDH_ID") %>' />


                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField NullDisplayText="View" HeaderText="..">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>


                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print Voucher" Checked="True" />
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>

