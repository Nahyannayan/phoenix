Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class AccMstPymnt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim MainMnu_code As String = String.Empty
        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = String.Format("~/accounts/AccAddPymnt.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "A100020" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    gvGroup1.Attributes.Add("bordercolor", "#1b80b6")

                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
            set_Menu_Img()
        End If




    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvGroup1.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvGroup1.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

  
    
    Private Sub gridbind()
        Try
            Dim str_filter_pdc, str_filter_bankcash, str_filter_custsupp As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_country As String
            Dim str_txtCode, str_txtName, str_txtCountry As String
            ' str_mode = Request.QueryString("mode")
            'Response.Write(str_mode)
            Dim i_dd_bank As Integer = 0
            Dim i_cmbYesNo As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_pdc = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_country = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtCountry = ""

            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddYesNo As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup1.Rows.Count > 0 Then

                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND AM.PTM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM.PTM_DESCR NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM.PTM_DESCR LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM.PTM_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM.PTM_DESCR LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM.PTM_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)


                txtSearch = gvGroup1.HeaderRow.FindControl("txtCountry")
                str_txtCountry = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_country = " AND AM.PTM_DAYS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_country = " AND AM.PTM_DAYS NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_country = " AND AM.PTM_DAYS LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_country = " AND AM.PTM_DAYS NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_country = " AND AM.PTM_DAYS LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_country = " AND AM.PTM_DAYS NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                ddYesNo = gvGroup1.HeaderRow.FindControl("cmbYesNo")
                If ddYesNo.SelectedItem.Value <> "All" Then
                    str_filter_pdc = " AND AM.PTM_bPDC='" & ddYesNo.SelectedItem.Value & "'"
                End If
                i_cmbYesNo = ddYesNo.SelectedIndex

            End If


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT PTM_ID as CHB_ID,PTM_DESCR as ACT_NAME,PTM_bPDC as Parent,PTM_DAYS" _
            & " FROM PAYMENTTERM_M AM WHERE PTM_ID<>''"



            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_country & str_filter_pdc)
            gvGroup1.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup1.DataBind()
                Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup1.Rows(0).Cells.Clear()
                gvGroup1.Rows(0).Cells.Add(New TableCell)
                gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup1.DataBind()
                sp_message.InnerHtml = ""
            End If
            '  gvGroup.Columns(0).Visible = False
            'ddcust = gvGroup1.HeaderRow.FindControl("DDCutomerSupplier")

            ddYesNo = gvGroup1.HeaderRow.FindControl("cmbYesNo")
            ddYesNo.SelectedIndex = i_cmbYesNo

            txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode

            txtSearch = gvGroup1.HeaderRow.FindControl("txtCountry")
            txtSearch.Text = str_txtCountry
            'txtSearch = gvGroup1.HeaderRow.FindControl("txtControl")
            'txtSearch.Text = str_txtControl

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Function FormatImageURL(ByVal pPDC As Boolean)
        Dim lstrImgURL As String
        If pPDC = True Then
            lstrImgURL = "~/Images/tick.gif"
        Else
            lstrImgURL = "~/Images/cross.gif"
        End If
        Return lstrImgURL
    End Function

    

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

     

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub

    Protected Sub cmbYesNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        'set_Menu_Img()
    End Sub

     
    Public Function GetNext(ByVal pControlAcc As String) As String
        Dim lstrRetVal As String


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()

        Try
            Dim SqlCmd As New SqlCommand("NextAccountNo", objConn)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@pControlAcc", pControlAcc)
            SqlCmd.Parameters.AddWithValue("@pLength", 5)

            ' Dim pRetVal As New SqlParameter("@pReturnVal", SqlDbType.VarChar)
            'pRetVal.Direction = ParameterDirection.Output
            'SqlCmd.Parameters.Add(pRetVal)
            'SqlCmd.ExecuteNonQuery()
            lstrRetVal = SqlCmd.ExecuteScalar()
        Catch ex As Exception

        End Try

        Return lstrRetVal
    End Function

     
    



    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnCountrySearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            Dim lblGuid As New Label
            Dim url As String
            Dim viewid As String
            Dim mainMnu_code As String = String.Empty
            lblGuid = TryCast(sender.FindControl("lblCHB_ID"), Label)
            viewid = lblGuid.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            mainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Accounts\AccEditPymnt.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", mainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
