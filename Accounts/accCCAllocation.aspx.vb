Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj

Partial Class Accounts_accCCAllocation
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            Else
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("../noAccess.aspx")
        End If
        Dim MainMnu_code As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        Page.Title = OASISConstants.Gemstitle

        UtilityObj.beforeLoopingControls(Me.Page)

        If Not Page.IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            txtDocNo.Attributes.Add("ReadOnly", "ReadOnly")
            lblrptCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            h_BSU_ID.Value = Session("sBsuid")
            h_FYEAR.Value = Session("F_YEAR")
            h_SUB_ID.Value = "007"
            Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
            Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
            filldrp()
        End If

        If ViewState("datamode") = "add" Or txtDocNo.Text <> "" Then
            h_Jnl_id.Value = 0
        Else
            h_Jnl_id.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Dim myValues() As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "select JHD_SUB_ID+';'+JHD_BSU_ID+';'+JHD_DOCTYPE+';'+JHD_DOCNO+';'+replace(convert(varchar(30),JHD_DOCDT,106),' ','/') from journal_h where jhd_id=" & h_Jnl_id.Value).ToString.Split(";")

            h_SUB_ID.Value = myValues(0)
            h_BSU_ID.Value = myValues(1)
            ddDocumentType.SelectedValue = myValues(2)
            h_DocType.Value = myValues(2)
            h_DocNo.Value = myValues(3)
            txtDocNo.Text = myValues(3)
            h_DocDate.Value = myValues(4)
            txtDocDate.Text = myValues(4)
            h_Jnl_id.Value = 0
            FillValues()
        End If

    End Sub

    Private Sub FillValues()
        Try
            lblError.Text = ""
            Dim dtDTL As DataTable = ViewJVForCCAllocation(h_SUB_ID.Value, h_BSU_ID.Value, h_DocType.Value, h_DocNo.Value, h_Jnl_id.Value)
            If h_Jnl_id.Value > 0 Then
                h_SUB_ID.Value = dtDTL.Rows(0)("JNL_SUB_ID")
                h_BSU_ID.Value = dtDTL.Rows(0)("JNL_BSU_ID")
                h_DocType.Value = dtDTL.Rows(0)("JNL_DOCTYPE")
                h_DocNo.Value = dtDTL.Rows(0)("JNL_DOCNO")
            End If
            Session("gintGridLine") = dtDTL.Compute("max(ID)", "") + 1
            h_NextLine.Value = Session("gintGridLine")

            Session("dtCostChild") = ViewCCDetailsForAllocation(h_SUB_ID.Value, h_BSU_ID.Value, h_DocType.Value, h_DocNo.Value)
            Session("CostAllocation") = ViewCCDetailsForSubAllocation(h_SUB_ID.Value, h_BSU_ID.Value, h_DocType.Value, h_DocNo.Value)
            Session("dtDTL") = dtDTL
            gvDTL.DataSource = dtDTL
            gvDTL.DataBind()
            RecreateSsssionDataSource()
            ClearRadGridandCombo()
            Dim sql As String
            sql = "select distinct JDS_REMARKS from journal_d_s WHERE JDS_DOCNO = '" & h_DocNo.Value & "' AND JDS_BSU_ID = '" & h_BSU_ID.Value & "'" & _
                       " AND JDS_SUB_ID = '" & h_SUB_ID.Value & "' AND JDS_DOCTYPE = '" & h_DocType.Value & "' "
            Dim remDT As New DataTable
            remDT = Mainclass.getDataTable(sql, ConnectionManger.GetOASISFINConnectionString)
            Dim mRow As DataRow
            txtNarrn.Text = ""
            For Each mRow In remDT.Rows
                txtNarrn.Text &= IIf(txtNarrn.Text = "", "", "<br>") & mRow("JDS_REMARKS")
            Next
            txtDocDate.Text = h_DocDate.Value
        Catch ex As Exception
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub
    Private Sub ResetGridRowBackColor()
        Try
            Dim mGrdVRow As GridViewRow
            For Each mGrdVRow In gvDTL.Rows
                mGrdVRow.BackColor = Nothing
            Next
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        If lblRowId IsNot Nothing Then
            Dim lblAccCode As Label
            lblAccCode = TryCast(sender.parent.FindControl("lblAccCode"), Label)
            Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)
            h_EditID.Value = Session("gintEditLine")
            btnFillCancel.Visible = True
            btnUpdate.Visible = True
            Detail_ACT_ID.Text = lblAccCode.Text
            RecreateSsssionDataSource()
            ClearRadGridandCombo()

            CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
            usrCostCenter1.BindCostCenter()

            ResetGridRowBackColor()
            CType(sender.parent.parent, GridViewRow).BackColor = Drawing.Color.Pink
        End If
    End Sub
    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub
    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub
    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        h_NextLine.Value = Session("gintGridLine")
        Session("gDtlDataMode") = "ADD"
        btnUpdate.Visible = False
        btnFillCancel.Visible = False
        gvDTL.SelectedIndex = -1
        btnFillCancel.Visible = False
        ClearRadGridandCombo()
        ResetGridRowBackColor()
    End Sub
    Public Shared Function ViewJVForCCAllocation(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String, ByVal JNL_ID As Integer) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        pParms(4) = New SqlClient.SqlParameter("@JNL_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = JNL_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewJVForCCAllocation", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function ViewCCDetailsForAllocation(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, _
       ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String) As DataTable
        'exec ViewVoucherDetails @VHD_SUB_ID='007',@VHD_BSU_ID='131001',@VHD_DOCTYPE='BP',@VHD_DOCNO ='10BP-0000414'  
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewCCDetailsForAllocation", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ViewCCDetailsForSubAllocation(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, _
        ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String) As DataTable
        'exec ViewVoucherDetails @VHD_SUB_ID='007',@VHD_BSU_ID='131001',@VHD_DOCTYPE='BP',@VHD_DOCNO ='10BP-0000414'  
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewCCDetailsForSubAllocation", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT ID,DESCR FROM VW_MST_CC_DocumentTypes "
            fillDropdown(ddDocumentType, Query, "DESCR", "ID", "OASISConnectionString", False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If Not Request.UrlReferrer Is Nothing Then
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgDocNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDocNo.Click
        FillValues()
    End Sub

    
    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                CostCenterFunctions.AddCostCenter(h_EditID.Value, Session("sBsuid"), _
                                  Session("CostOTH"), h_DocDate.Value, Session("idCostChild"), _
                                 Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                 Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                btnUpdate.Visible = False
                btnFillCancel.Visible = False
                ClearRadGridandCombo()
                ResetGridRowBackColor()
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim str_err As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                str_err = BACKUP_COSTCENTER_ALLOC(objConn, stTrans, h_DocNo.Value, h_DocType.Value)
                If str_err = 0 Then
                    str_err = DeleteJOURNAL_D_S_ALL(objConn, stTrans, h_DocNo.Value, h_DocType.Value)
                    If str_err = 0 Then
                        str_err = DoTransactions(objConn, stTrans, h_DocNo.Value, h_DocType.Value)
                    End If
                    If str_err = 0 Then
                        str_err = UPDATE_CC_ALLOC_REMARKS(objConn, stTrans, h_DocNo.Value, h_DocType.Value, txtNarrn.Text)
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()

                        Dim iReturnvalue As Integer
                        Dim cmd = New SqlCommand("COSTCENTER_UPDATE", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpJDS_SUB_ID As New SqlParameter("@JDS_SUB_ID", SqlDbType.VarChar, 20)
                        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                        cmd.Parameters.Add(sqlpJDS_SUB_ID)

                        Dim sqlpJDS_BSU_ID As New SqlParameter("@JDS_BSU_ID", SqlDbType.VarChar, 20)
                        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                        cmd.Parameters.Add(sqlpJDS_BSU_ID)

                        Dim sqlpJDS_FYEAR As New SqlParameter("@JDS_FYEAR", SqlDbType.Int)
                        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                        cmd.Parameters.Add(sqlpJDS_FYEAR)

                        Dim sqlpJDS_DOCTYPE As New SqlParameter("@JDS_DOCTYPE", SqlDbType.VarChar, 10)
                        sqlpJDS_DOCTYPE.Value = h_DocType.Value
                        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                        Dim sqlpJDS_DOCNO As New SqlParameter("@JDS_DOCNO", SqlDbType.VarChar, 20)
                        sqlpJDS_DOCNO.Value = h_DocNo.Value
                        cmd.Parameters.Add(sqlpJDS_DOCNO)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value

                        btnFillCancel.Visible = False
                        btnUpdate.Visible = False
                        FillValues()
                        lblError.Text = getErrorMessage(0)
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                    End If
                End If
            Catch ex As Exception
                stTrans.Rollback()
            End Try
            ResetGridRowBackColor()
        Catch ex As Exception

        End Try
    End Sub
    Private Function BACKUP_COSTCENTER_ALLOC(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String, ByVal p_doctype As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()
        cmd = New SqlCommand("BACKUP_COSTCENTER_ALLOC", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpJDS_SUB_ID As New SqlParameter("@JDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@JDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@JDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@JDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = p_doctype
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@JDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function
    Private Function UPDATE_CC_ALLOC_REMARKS(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String, ByVal p_doctype As String, ByVal p_Remarks As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()
        cmd = New SqlCommand("UPDATE_CC_ALLOC_REMARKS", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpJDS_SUB_ID As New SqlParameter("@JDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@JDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@JDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@JDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = p_doctype
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@JDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim sqlpJDS_REMARKS As New SqlParameter("@JDS_REMARKS", SqlDbType.VarChar, 300)
        sqlpJDS_REMARKS.Value = p_Remarks
        cmd.Parameters.Add(sqlpJDS_REMARKS)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DeleteJOURNAL_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String, ByVal p_doctype As String) As String
        Dim iIndex As Integer
        Dim iReturnvalue As Integer
        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If Session("dtDTL").Rows(iIndex)("JNL_MISSING") <> "MATCH" Then
                Dim cmd As New SqlCommand
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                cmd.Dispose()
                cmd = New SqlCommand("DeleteJOURNAL_D_S_ALL", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                Dim sqlpJDS_SUB_ID As New SqlParameter("@JDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@JDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@JDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@JDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpJDS_DOCTYPE.Value = p_doctype
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@JDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlpJDS_SLNO As New SqlParameter("@JDS_SLNO", SqlDbType.VarChar, 20)
                sqlpJDS_SLNO.Value = Session("dtDTL").Rows(iIndex)("ID")
                cmd.Parameters.Add(sqlpJDS_SLNO)
                cmd.ExecuteNonQuery()
                iReturnvalue = iReturnvalue + retValParam.Value
            End If
        Next        
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String, ByVal p_doctype As String) As String
        Dim iReturnvalue As Integer
        Try
            Dim iIndex As Integer = 0
            ' ''FIND OPPOSITE ACCOUNT
            Dim str_DR_LargestAccount, str_CR_LargestAccount As String
            str_DR_LargestAccount = ""
            str_CR_LargestAccount = ""
            Dim d_cr_largest As Double = 0
            Dim d_dr_largest As Double = 0
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                If Session("dtDTL").Rows(iIndex)("JNL_DEBIT") > 0 Then
                    If Session("dtDTL").Rows(iIndex)("JNL_DEBIT") > d_dr_largest Then
                        str_DR_LargestAccount = Session("dtDTL").Rows(iIndex)("JNL_ACT_ID")
                        d_dr_largest = Session("dtDTL").Rows(iIndex)("JNL_DEBIT")
                    End If
                Else
                    If Session("dtDTL").Rows(iIndex)("JNL_CREDIT") > d_cr_largest Then
                        str_CR_LargestAccount = Session("dtDTL").Rows(iIndex)("JNL_ACT_ID")
                        d_cr_largest = Session("dtDTL").Rows(iIndex)("JNL_CREDIT")
                    End If
                End If
            Next
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                '' ''Handle sub table
                Dim str_crdb As String = "CR"
                If Session("dtDTL").Rows(iIndex)("JNL_DEBIT") > 0 Then
                    dTotal = Session("dtDTL").Rows(iIndex)("JNL_DEBIT")
                    str_crdb = "DR"
                Else
                    dTotal = Session("dtDTL").Rows(iIndex)("JNL_CREDIT")
                End If
                If Session("dtDTL").Rows(iIndex)("JNL_MISSING") <> "MATCH" Then
                    iReturnvalue = DoTransactions_Sub_Table(objConn, stTrans, p_docno, p_doctype, Session("dtDTL").Rows(iIndex)("id"), _
                    str_crdb, Session("dtDTL").Rows(iIndex)("id"), Session("dtDTL").Rows(iIndex)("JNL_ACT_ID"), dTotal)
                    If iReturnvalue <> "0" Then
                        Return iReturnvalue
                    End If
                End If
            Next
            If iIndex <= Session("dtDTL").Rows.Count - 1 Then
                Return iReturnvalue
            Else
                Return iReturnvalue
            End If
            'Adding transaction info
        Catch ex As Exception
            Return "1000"
        End Try
    End Function
    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
     ByVal stTrans As SqlTransaction, ByVal p_docno As String, ByVal p_doctype As String, _
     ByVal p_voucherid As String, ByVal p_crdr As String, _
     ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                'iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _  'swapna commented,added below code as per shakeel's email 30 oct 2012

                iReturnvalue = CostCenterFunctions.SaveJOURNAL_D_S_REALLOCATE(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), p_doctype, _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                h_DocDate.Value, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        'iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _  'swapna commented
                        'added below code instead
                        iReturnvalue = CostCenterFunctions.SaveJOURNAL_D_SUB_REALLOCATE(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), p_doctype, "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function

    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmdCol As Integer = gvDTL.Columns.Count - 1
                'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
                Dim lblReqd As New Label
                Dim lblid As New Label
                Dim lblDebit As New Label
                Dim lblCredit As New Label
                Dim gvCostchild As New GridView
                lblid = e.Row.FindControl("lblId")
                gvCostchild = e.Row.FindControl("gvCostchild")
                gvCostchild.Attributes.Add("bordercolor", "#fc7f03")
                If Not Session("dtCostChild") Is Nothing Then
                    Dim dv As New DataView(Session("dtCostChild"))
                    dv.RowFilter = "VoucherId='" & lblid.Text & "'"
                    dv.Sort = "MemberId"
                    gvCostchild.DataSource = dv.ToTable
                    gvCostchild.DataBind()
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub
End Class
