<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccApprovalmaster.aspx.vb" Inherits="Accounts_AccApprovalmaster" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function getFilter(NameObj, IdObj, frm) {
            var bsuId = "";
            if (frm == 'BSUEMP') {
                document.getElementById('<%=hd_usr1.ClientID%>').value = NameObj;
                document.getElementById('<%=hd_usr2.ClientID%>').value = IdObj;

                bsuId = document.getElementById('<%=txtBusId.ClientID %>').value
                if (bsuId == '') {
                    alert('Please Select Business Unit..!');
                    return;
                }


            }
            if (frm == 'BSUNIT') {
                document.getElementById('<%=txtEmployee.ClientID %>').value = "";
                document.getElementById('<%=hd_bsu1.ClientID%>').value = NameObj;
                document.getElementById('<%=hd_bsu2.ClientID%>').value = IdObj;

            }
            if (frm == 'DOCTYPE') {
                document.getElementById('<%=hd_type1.ClientID%>').value = NameObj;
                document.getElementById('<%=hd_type2.ClientID%>').value = IdObj;
            }

            if (frm == 'DPT') {
                document.getElementById('<%=hd_dept1.ClientID%>').value = NameObj;
                document.getElementById('<%=hd_dept2.ClientID%>').value = IdObj;
            }
            document.getElementById('<%=hd_frm.ClientID%>').value = frm;


            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 650px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("FiltervaluesFind.aspx?FROM=" + frm + "&BSUID=" + bsuId, "", sFeatures)
            //////result = window.showModalDialog("Fillterany.aspx?FROM="+frm+"&BSUID="+bsuId,"", sFeatures)
            var result = radopen("FiltervaluesFind.aspx?FROM=" + frm + "&BSUID=" + bsuId, "pop_up2")

            //Fillterany.aspx
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('__');
            //document.getElementById(IdObj).value = NameandCode[0];
            //document.getElementById(NameObj).value = NameandCode[1];
            //return false;
        }

        function getInvisible() {
            var BsuId = document.getElementById('<%=txtBusId.ClientID %>').value
            if (BsuId == "")
                document.getElementById("ImgBsuName").style.display = 'Inline'
            else
                document.getElementById("ImgBsuName").style.display = 'none'
        }

        function Numeric_Only() {
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function CtrlDisable() {
            if (event.keyCode == 17) {
                alert('This function disabled..!');
                return;
            }
        }
        function Mouse_Move(Obj) {
            document.getElementById(Obj).style.color = "Red";
        }
        function Mouse_Out(Obj) {
            document.getElementById(Obj).style.color = "#1b80b6"
        }
        function getDate(ObjName) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx?dt=" + document.getElementById(ObjName).value, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            document.getElementById(ObjName).value = result;
            return true;
        }

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmployee.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                var frm_id = document.getElementById('<%=hd_frm.ClientID%>').value
                //alert( document.getElementById('<%=hd_frm.ClientID%>').value);
                if (frm_id == 'BSUNIT') {
                    //alert('BSUNIT' + frm_id);
                    var bsuvalue_id = document.getElementById('<%=hd_bsu1.ClientID%>').value
                    var bsuvalue_id2 = document.getElementById('<%=hd_bsu2.ClientID%>').value
                    document.getElementById(bsuvalue_id).value = NameandCode[1];
                    document.getElementById(bsuvalue_id2).value = NameandCode[0];
                    //alert(bsuvalue_id);
                }
                if (frm_id == 'BSUEMP') {
                    //alert('BSUEMP' + frm_id);
                    var usrvalue_id = document.getElementById('<%=hd_usr1.ClientID%>').value
                    var usrvalue_id2 = document.getElementById('<%=hd_usr2.ClientID%>').value
                    document.getElementById(usrvalue_id).value = NameandCode[1];
                    document.getElementById(usrvalue_id2).value = NameandCode[0];
                    //alert(usrvalue_id);
                }
                if (frm_id == 'DOCTYPE') {
                    //alert('DOCTYPE' + frm_id);
                    var typevalue_id = document.getElementById('<%=hd_type1.ClientID%>').value
                    var typevalue_id2 = document.getElementById('<%=hd_type2.ClientID%>').value
                    document.getElementById(typevalue_id).value = NameandCode[1];
                    document.getElementById(typevalue_id2).value = NameandCode[0];
                    //alert(typevalue_id);
                }
                if (frm_id == 'DPT') {
                    //alert('DPT' + frm_id);
                    var dptvalue_id = document.getElementById('<%=hd_dept1.ClientID%>').value
                    var dptvalue_id2 = document.getElementById('<%=hd_dept2.ClientID%>').value
                    document.getElementById(dptvalue_id).value = NameandCode[1];
                    document.getElementById(dptvalue_id2).value = NameandCode[0];
                }
                <%--document.getElementById('<%=txtBankPaymentCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankPaymentDescr.ClientID%>').value = NameandCode[1];--%>
                //document.getElementById(IdObj).value = NameandCode[0];
                //document.getElementById(NameObj).value = NameandCode[1];
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Approval Hierarchy Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" align="center" id="MainTable">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                HeaderText=" * Marked fields are mandatory"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtBsuName" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgBsu" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif" AccessKey="B"></asp:ImageButton>
                            <asp:TextBox ID="txtBusId" runat="server"></asp:TextBox><span style="font-size: 6pt; color: #808080">(Alt+B)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBusId"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Document Type</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDocType" runat="server"></asp:TextBox>

                            <asp:ImageButton ID="ImgDoctype" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif" AccessKey="T"></asp:ImageButton>
                            <asp:TextBox ID="txtDoctypeId" runat="server"></asp:TextBox>&nbsp;<span style="font-size: 8px; color: gray;">(Alt+T)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDoctypeId"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Department</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDepartment" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="ImgDepartment" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif" AccessKey="P"></asp:ImageButton>
                            <asp:TextBox ID="txtDepartmentId" runat="server"></asp:TextBox>
                            <span style="font-size: 8px; color: gray;">(Alt+P)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDepartmentId"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">User Name</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmployee" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgUser" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif" AccessKey="U"></asp:ImageButton>&nbsp;<span style="font-size: 8px; color: gray;">(Alt+U)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmployee"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                        <td align="left" width="20%"><span class="field-label">Amount Limit</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server" TabIndex="4"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAmount"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date From</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDDocdate" runat="server" AutoPostBack="True" OnTextChanged="txtDDocdate_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                ImageAlign="Middle" CausesValidation="False" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDDocdate" PopupButtonID="imgCalendar">
                            </ajaxToolkit:CalendarExtender>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDDocdate"
                                CssClass="error">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%">
                            <asp:CheckBox ID="ChkPrevious" runat="server" Text="Previous Approval" CssClass="field-label"></asp:CheckBox></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="ChkHigher" runat="server" Text="Higher Approval" CssClass="field-label"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnInsert" runat="server" CssClass="button" Text="Add" OnClick="btnInsert_Click" TabIndex="5" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" CausesValidation="False" OnClick="btnClear_Click" TabIndex="6" />
                        </td>
                    </tr>
                </table>


                <br />
                <table align="center" width="100%">
                    <tr>
                        <td>

                            <asp:GridView ID="gvJournal" runat="server" Width="100%" EmptyDataText="No Item Added" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" OnRowDataBound="gvJournal_RowDataBound" ShowFooter="False" Style="cursor: hand" OnRowDeleting="gvJournal_RowDeleting">
                                <FooterStyle />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True"></EmptyDataRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="OrderNo" HeaderText="Order No">
                                        <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserName" HeaderText="User Name">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PrvApproval" HeaderText="Prv Approval">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HgApproval" HeaderText="Hg Approval">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="FromDate" SortExpression="FromDate" HeaderText=" From Date">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Limit Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True" DeleteText="Edit" HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:BoundField DataField="EmpId" Visible="False" SortExpression="EmpId" HeaderText="EmpId"></asp:BoundField>
                                </Columns>
                                <RowStyle CssClass="griditem"></RowStyle>
                                <SelectedRowStyle></SelectedRowStyle>
                                <HeaderStyle></HeaderStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                <br />
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="viewId" runat="server" Value="0" />
                <asp:HiddenField ID="h_Emp_No" runat="server" Value="0" />


                <asp:HiddenField ID="hd_bsu1" runat="server" />
                <asp:HiddenField ID="hd_bsu2" runat="server" />
                <asp:HiddenField ID="hd_type1" runat="server" />
                <asp:HiddenField ID="hd_type2" runat="server" />
                <asp:HiddenField ID="hd_dept1" runat="server" />
                <asp:HiddenField ID="hd_dept2" runat="server" />
                <asp:HiddenField ID="hd_usr1" runat="server" />
                <asp:HiddenField ID="hd_usr2" runat="server" />
                <asp:HiddenField ID="hd_frm" runat="server" />

                <script type="text/javascript">
                    getInvisible();
                </script>

            </div>
        </div>
    </div>
</asp:Content>


