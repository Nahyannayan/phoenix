<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accIUJournalvoucher.aspx.vb" Inherits="iuJournalvoucher" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
      <style>
        table td input[type=text], table td select, div select{
            min-width: 20% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../images/Misc/minus.gif";
                }
                else {
                    img.src = "../images/Misc/minus.gif";
                }
                img.alt = "Close to view other Customers";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../images/Misc/plus.gif";
                }
                else {
                    img.src = "../images/Misc/plus.gif";
                }
                img.alt = "Expand to show Orders";
            }
        }

        function showDocument() {

            contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
            filename = document.getElementById('<%=hdnFileName.ClientID %>').value;
            //result = window.showModalDialog("IFrame.aspx?id=" + id + "&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            result = radopen("../Inventory/IFrameNew.aspx?id=0&path=finance&filename=" + filename + "&contenttype=" + contenttype, "pop_up")
            return false;
        }

        function getAccount() {
            popUp('960', '600', 'NORMAL', '<%=DETAIL_ACT_ID.ClientId %>', '<%=txtDAccountname.ClientId %>');
            return false;
        }

         function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

              document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                    document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                    document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var lstrVal;
            var lintScrVal;


            var NameandCode;
            var result;
            if (pMode == 'NORMAL') {
                document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                    document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                    document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up1");
                    //if (result=='' || result==undefined)
                    //{    return false;      } 
                    //lstrVal=result.split('||');     
                    //document.getElementById(ctrl).value=lstrVal[0];
                    //document.getElementById(ctrl1).value=lstrVal[1];
                    //document.getElementById(ctrl2).value=lstrVal[2];
                    //document.getElementById(ctrl3).value=lstrVal[3];

                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function OnClientClose1(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode == 'NORMAL') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1]
                    }

                }
            }


    function AddDetails(url) {

        var NameandCode;
        var result;
        var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
       dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
       dates = dates.replace(/[/]/g, '-')
       url_new = url_new + '&dt=' + dates;
       result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
       if (result == '' || result == undefined) {
           return false;
       }
       NameandCode = result.split('___');
       document.getElementById('<%=DETAIL_ACT_ID.ClientID %>').focus();


        return false;
    }

    function CopyDetails() {
        try {
            document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
        }
        catch (ex) { }
    }

        function GetBSUName(id) {
            document.getElementById('<%=hd_id.ClientID%>').value = id;
            var NameandCode;
            var result;
            result = radopen("selBussinessUnit.aspx?multiSelect=false&getall=true", "pop_up2")
            <%--  if (result == '' || result == undefined) {
            return false;
        }
        else {
            switch (id) {
                case 0:
                    NameandCode = result.split('___');
                    document.getElementById('<%=txtBSUCR.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_BSUID_CR.ClientID %>').value = NameandCode[0];
                return true;
                break;
            case 1:
                NameandCode = result.split('___');
                document.getElementById('<%=txtBSUDR.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_BSUID_DR.ClientID %>').value = NameandCode[0];
                return true;
                break;
        }--%>
            //}
            //return false;

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            var id = document.getElementById('<%=hd_id.ClientID%>').value;
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (id == 0) {                    
                    document.getElementById('<%=txtBSUCR.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=h_BSUID_CR.ClientID %>').value = NameandCode[0];
                    return true;
                }
                else if (id == 1) {                    
                    document.getElementById('<%=txtBSUDR.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=h_BSUID_DR.ClientID %>').value = NameandCode[0];
                    return true;
                }
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Interunit Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                 <asp:HiddenField ID="hd_id" runat="server" />
                <table align="center" width="100%" >
                    <tr valign="top">
                        <td align="left">
                            <asp:ValidationSummary ID="MainError" runat="server" ValidationGroup="dateval" CssClass="error" />
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <table align="center" width="100%">

                                <tr>
                                    <td width="20%">
                                        <span class="field-label"><span class="field-label">Doc No</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True" TabIndex="-1"></asp:TextBox><br />
                                        <asp:CheckBox ID="chkMirrorEntry" runat="server" Text="Mirror Entry" AutoPostBack="True"
                                            TabIndex="2" />
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label"><span class="field-label">Doc Date </span></span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"
                                            CausesValidation="True" ValidationGroup="dateval" TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgSelectDocDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="6" />
                                        <asp:RegularExpressionValidator ID="revTodate" runat="server" ControlToValidate="txtdocDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="dateval" EnableTheming="True">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Currency </span>
                                    </td>
                                    <td  align="left">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True"  width="15%"
                                            TabIndex="8">
                                        </asp:DropDownList>
                                      <strong >  Differ Amount </strong>
                            <asp:TextBox ID="txtDifferAmt" runat="server"  ReadOnly="True" TabIndex="10" Width="30%"
                                AutoPostBack="True"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                    runat="server" ControlToValidate="txtDifferAmt" Display="Dynamic" ErrorMessage="Amount Should be Valid"
                                   ValidationExpression="^(-)?\d+(\.\d\d)?$" ValidationGroup="Details"
                                   >*</asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Group Rate </span>
                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtHGroupRate" runat="server" ReadOnly="True"
                                             TabIndex="-1"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Business Unit (CR) </span>
                                    </td>
                                    <td  align="left">
                                        <asp:TextBox ID="txtBSUCR" runat="server" TabIndex="-1"></asp:TextBox>
                                        <asp:ImageButton ID="imgCRBSUNIT" runat="server" ImageUrl="../Images/forum_search.gif"
                                            OnClientClick="GetBSUName(0); return false;" TabIndex="12" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Business Unit (DR) </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBSUDR" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBSUSel" runat="server" ImageUrl="../Images/forum_search.gif"
                                            OnClientClick="GetBSUName(1); return false;" TabIndex="12" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Exchange Rate(CR) </span>
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox ID="txtHExchRateCR" runat="server"
                                             TabIndex="-1"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Exchange Rate(DR) </span>
                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtHExchRateDR" runat="server" 
                                            TabIndex="14"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtHExchRateDR"
                                            ErrorMessage="Exchange Rate(DR) Required" ValidationGroup="dateval"
                                           >*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Narration </
                                    </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtHNarration" runat="server" 
                                            MaxLength="300" TextMode="MultiLine"  TabIndex="16"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="dateval">*</asp:RequiredFieldValidator>
                                        <span class="field-label">Supporting Docs </span>
                                        <asp:LinkButton ID="btnDocumentLink" Text="view" Visible='false' runat="server" OnClientClick="showDocument();return true;"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDocumentDelete" Text='x' Visible='false' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                        <asp:FileUpload ID="UploadDocPhoto" runat="server" 
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' Visible="false" />
                                        <asp:HiddenField ID="hdnFileName" runat="server" />
                                        <asp:HiddenField ID="hdnContentType" runat="server" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" class="BlueTable" cellspacing="0"
                                width="100%" cellpadding="5">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Account Details
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label" >Account </span>
                                    </td>
                                    <td valign="middle" align="left" colspan="2">
                                        <asp:TextBox ID="DETAIL_ACT_ID" runat="server" Width="20%"
                                            AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ControlToValidate="DETAIL_ACT_ID" ErrorMessage="Account Code Cannot Be Empty"
                                                ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton ID="btnAccount"
                                                    runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;"
                                                    TabIndex="18" />
                                        <asp:TextBox ID="txtDAccountName" runat="server" Width="69%"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Amount </span>
                                    </td>
                                    <td valign="middle" align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" 
                                            TabIndex="20" AutoPostBack="True"></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck" Type="Double"
                                            ValidationGroup="Details" Display="Dynamic">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                
                                    <td valign="middle" width="20%">
                                        <span class="field-label">Narration </span>
                                    </td>
                                    <td align="left" valign="middle" width="30%">
                                        <asp:TextBox ID="txtDNarration" runat="server"
                                             MaxLength="300" TextMode="MultiLine"  TabIndex="22"></asp:TextBox><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                                ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                   
                                </tr>
                                <tr id="tr_Allocation" runat="server">
                                    <td valign="middle" width="20%">
                                        <span class="field-label">Cost Allocation </span>
                                    </td>
                                    <td align="left" valign="middle" colspan="3">
                                        <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" colspan="4">
                                        <asp:Button ID="btnAdds" runat="server" CssClass="button" Text="Add" ValidationGroup="Details"
                                            TabIndex="24" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="24" />
                                        <asp:Button ID="btnAccCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="26" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                            EmptyDataText="No Transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="id">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="DEBIT">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Debit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CREDIT">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("credit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="LinkButton1_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" HeaderText="TEST" Visible="False" />
                                                <asp:TemplateField HeaderText="Cost Center" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("Required") %>' Visible="False"></asp:Label>
                                                        <asp:Image ID="imgAllocate" runat="server" Visible="False" />
                                                        <asp:LinkButton ID="lbAllocate" runat="server" Visible="False">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><span class="field-label">Debit Total </span>
                            <asp:TextBox ID="txtTDotalDebit" runat="server" ReadOnly="True" Width="20%"></asp:TextBox>
                                     <span class="field-label">   Credit Total </span>
                            <asp:TextBox ID="txtTotalCredit" runat="server" Width="20%"
                                ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" ValidationGroup="Details" TabIndex="28" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="30" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="32"
                                            ValidationGroup="dateval" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" TabIndex="34" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="38"
                                            UseSubmitBehavior="False" CausesValidation="False" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="36" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_editorview" runat="server" />
                <asp:HiddenField ID="h_Guid" runat="server" />
                <input id="h_Memberids" runat="server" type="hidden" />
                <asp:HiddenField ID="h_BSUID_DR" runat="server" />
                <asp:HiddenField ID="H_Bposted" runat="server" />
                <asp:HiddenField ID="h_BSUID_DR_Old" runat="server" />
                <asp:HiddenField ID="h_BSUID_CR" runat="server" />
                <asp:HiddenField ID="h_BSUID_CR_Old" runat="server" />
                <input id="h_mode" runat="server" type="hidden" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgSelectDocDate" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <input id="h_NextLine" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>
