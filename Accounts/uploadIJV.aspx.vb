﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports GemBox.Spreadsheet

Partial Class Accounts_UploadIJV
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptUpload As New ScriptManager
            smScriptUpload = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptUpload.RegisterPostBackControl(btnUploadPrf)
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200367") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If ViewState("EntryId") = "0" Then
                    SetDataMode("add")
                    setModifyvalues(0)
                Else
                    SetDataMode("view")
                    setModifyvalues(ViewState("EntryId"))
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub setDataMode(ByVal Mode As String)
        h_Mode.Value = Mode
    End Sub

    Private Sub setModifyvalues(ByVal entryId As Integer) 'setting header data on view/edit
        Try
            h_EntryId.Value = entryId
            fillDataGrid()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnUploadPrf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadPrf.Click
        If UploadFile.HasFile Then
            Dim str_tempfilename As String = UploadFile.FileName 'Session("sUsr_name") & Session.SessionID & UploadFile.FileName
            Dim strFilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & str_tempfilename
            Dim FileError As Boolean = False
            UploadFile.PostedFile.SaveAs(strFilepath)

            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            ElseIf txtNarration.Text.Trim.Length < 10 Then
                lblError.Text = "Enter Narration!!!!"
                Exit Sub
            Else
                If IO.File.Exists(strFilepath) Then
                    Dim filename As String = Path.GetFileName(strFilepath)
                    Dim ext As String = Path.GetExtension(strFilepath)
                    lblError.Text = ""
                    If ext = ".xls" Or ext = ".xlsx" Then
                        Try
                            Dim xlTable As DataTable, resTable As DataTable
                            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                            '  Dim ef As ExcelFile = New ExcelFile
                            Dim ef = ExcelFile.Load(strFilepath)
                            '  ef.Save(strFilepath)
                            Dim mRowObj As ExcelRow
                            mRowObj = ef.Worksheets(0).Rows(1)
                            Dim allocatedcolumns As Integer = mRowObj.AllocatedCells.Count() - 1
                            xlTable = Mainclass.FetchFromExcelIntoDataTable(strFilepath, 1, 1, 2)

                            'xlTable = Mainclass.FetchFromExcel("select * from [TableName]", strFilepath)
                            If xlTable.Rows.Count.Equals(0) Then
                                lblError.Text = "Could not process the request.Invalid data in excel..!"
                            End If
                            File.Delete(strFilepath)

                            Dim strFields As String = ""
                            uploadFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select '' BSU, 0.00 Amount, '' BSU_NAME where 1=0").Tables(0)
                            For cols As Int16 = 0 To xlTable.Columns.Count - 1
                                strFields &= xlTable.Columns(cols).ColumnName
                            Next
                            If strFields.Contains("BSUAmount") Then
                                Dim objConn As New SqlConnection(connectionString)
                                objConn.Open()
                                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                                Dim iParms(7) As SqlClient.SqlParameter
                                iParms(1) = Mainclass.CreateSqlParameter("@strtransfer", DataTableToString(xlTable, "transfer"), SqlDbType.VarChar)
                                iParms(2) = Mainclass.CreateSqlParameter("@struserid", Session("sUsr_name"), SqlDbType.VarChar)
                                iParms(3) = Mainclass.CreateSqlParameter("@strbsuid", Session("sBSUID"), SqlDbType.VarChar)
                                iParms(4) = Mainclass.CreateSqlParameter("@strnarration", txtNarration.Text, SqlDbType.VarChar)
                                iParms(5) = Mainclass.CreateSqlParameter("@strMode", h_Mode.Value, SqlDbType.VarChar)
                                iParms(6) = Mainclass.CreateSqlParameter("@strid", h_EntryId.Value, SqlDbType.Int, True)
                                iParms(7) = Mainclass.CreateSqlParameter("@strFYear", Session("F_YEAR"), SqlDbType.VarChar)
                                Try
                                    resTable = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "importIJVUpload", iParms).Tables(0)
                                    stTrans.Commit()
                                    If h_EntryId.Value = 0 Then h_EntryId.Value = iParms(6).Value

                                    If resTable.Rows.Count > 0 And h_EntryId.Value > 0 Then
                                        For Each dr As DataRow In resTable.Rows
                                            Dim mrow As DataRow
                                            mrow = uploadFooter.NewRow
                                            mrow("BSU") = dr("BSU").ToString()
                                            mrow("BSU_NAME") = dr("BSU_NAME").ToString()
                                            mrow("Amount") = dr("Amount").ToString()
                                            uploadFooter.Rows.Add(mrow)
                                        Next
                                        'txtTotal.Text = Convert.ToDouble(uploadFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")

                                        gvUpload.EditIndex = -1
                                        gvUpload.DataSource = uploadFooter
                                        gvUpload.DataBind()
                                        btnUpdate.Visible = True
                                        'showNoRecordsFound()
                                    Else
                                        If resTable.Rows.Count = 0 Then
                                            lblError.Text = "Error: nothing to import"
                                        Else
                                            lblError.Text = "Error:" & iParms(4).Value
                                        End If
                                    End If
                                Catch ex As Exception
                                    stTrans.Rollback()
                                End Try
                            Else
                                lblError.Text = "Excel file format invalid"
                            End If

                        Catch ex As Exception
                            lblError.Text = "Error:" & ex.Message
                            Exit Sub
                        End Try
                    End If
                Else
                    lblError.Text = "upload only Excel File"
                End If
            End If
            If FileError And lblError.Text.Length = 0 Then lblError.Text = "Problems encountered while uploading file, please try again"
        Else
            lblError.Text = "File not selected !!!"
        End If

    End Sub

    Private Function DataTableToString(ByRef dt As DataTable, ByVal strRootElementName As String) As String
        Dim sb As New StringBuilder(), addElement As Boolean = True, rowStr As String = ""
        If strRootElementName = "" Then
            strRootElementName = Convert.ToString(dt.TableName)
        End If
        For Each dr As DataRow In dt.Rows            
            rowStr = ""
            For Each dc As DataColumn In dt.Columns
                If dr(dc).ToString().Replace("’", "`") <> "" Then
                    addElement = True
                    If dc.ColumnName.ToLower.Contains("date") AndAlso dr(dc).ToString().Length > 0 Then
                        rowStr &= ("<" & dc.ColumnName & ">" & Format(dr(dc), "dd-MMM-yyyy hh:mm:ss") & "</") & dc.ColumnName & ">"
                    Else
                        rowStr &= ("<" + dc.ColumnName & ">" & dr(dc).ToString().Replace("’", "`") & "</") + dc.ColumnName & ">"
                    End If
                Else
                    addElement = False
                End If
            Next
            If addElement Then
                sb.Append("<" & strRootElementName & ">")
                sb.Append(rowStr)
                sb.Append("</" & strRootElementName & ">")
            End If
        Next
        Return sb.ToString()
    End Function

    Private Property uploadFooter() As DataTable
        Get
            Return ViewState("uploadFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("uploadFooter") = value
        End Set
    End Property

    Private Sub fillDataGrid()
        Dim iParms(7) As SqlClient.SqlParameter, uploadDs As DataSet
        iParms(1) = Mainclass.CreateSqlParameter("@strtransfer", "", SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@struserid", "", SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@strbsuid", Session("sBSUID"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@strnarration", "", SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@strMode", "", SqlDbType.VarChar)
        iParms(6) = Mainclass.CreateSqlParameter("@strid", h_EntryId.Value, SqlDbType.Int, True)
        iParms(7) = Mainclass.CreateSqlParameter("@strFYear", Session("F_YEAR"), SqlDbType.VarChar)
        uploadDs = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "importIJVUpload", iParms)
        txtNarration.Text = uploadDs.Tables(1).Rows(0)("UJV_NARRATION")
        btnUpdate.Visible = (uploadDs.Tables(1).Rows(0)("UJV_STATUS") = "U")
        btnDelete.Visible = btnUpdate.Visible
        trUpload.Visible = Not (uploadDs.Tables(1).Rows(0)("UJV_STATUS") = "P")

        gvUpload.EditIndex = -1
        gvUpload.DataSource = uploadDs.Tables(0)
        gvUpload.DataBind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim pParms(4) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", h_EntryId.Value, SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@DT", txtUPDDate.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@ACT_ID", txtAccount.Text, SqlDbType.VarChar)
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "importIJVPost", pParms)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "unable to post IJV, due to errors in upload file"
        End Try
        If pParms(4).Value = 1 Then
            lblError.Text = "unable to post IJV, due to errors in upload file"
        Else
            Response.Redirect(ViewState("ReferrerUrl"), False)
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim sqlStr As String = "update uploadIJV set UJV_DELETED=1 where UJV_ID=@ID"
        Dim pParms(1) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", h_EntryId.Value, SqlDbType.Int)
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
        End Try
        Response.Redirect(ViewState("ReferrerUrl"), False)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"), False)
    End Sub
End Class

