Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccEditCCM
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("editid") <> "" Then
                Session("EditId") = Request.QueryString("editid")
                FillValues()
            End If
        End If
    End Sub
    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL As String
            Dim ds As New DataSet

            lstrSQL = "SELECT * FROM COSTCENTER_M WHERE CCT_ID='" & Session("EditId") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtDescr.Text = ds.Tables(0).Rows(0)("CCT_DESCR")

            End If
        Catch ex As Exception


        End Try

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        ServerValidate()

        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            objConn.Open()
            Try
                Dim SqlCmd As New SqlCommand("SaveCOSTCENTER_M", objConn)
                SqlCmd.CommandType = CommandType.StoredProcedure
                SqlCmd.Parameters.AddWithValue("@CCT_ID", Session("EditId"))
                SqlCmd.Parameters.AddWithValue("@CCT_DESCR", Trim(txtDescr.Text))
                SqlCmd.Parameters.AddWithValue("@bEdit", True)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (lintRetVal <> 0) Then
                    lblErr.Text = getErrorMessage(lintRetVal)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub ServerValidate()
        lstrErrMsg = ""

        If ((Trim(txtDescr.Text) = "") Or (Master.StringWithSpace(txtDescr.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "From # Should Be A String Value" & "<br>"
        End If

        lblErr.Text = lstrErrMsg
    End Sub
    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("Saved Successfully...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
