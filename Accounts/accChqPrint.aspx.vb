Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class accChqPrint
    Inherits System.Web.UI.Page

    Shared MainMnu_code As String
    Shared menu_rights As Integer
    Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64
    Shared content As ContentPlaceHolder

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                datamode = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Then 'Or MainMnu_code <> "A200014" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

            Response.CacheControl = "no-cache"
            'calling pageright class to get the access rights
            menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
            Call AccessRight.setpage(content, menu_rights, datamode)

            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")

            gridbind()
        End If

    End Sub

    ' For the Search Controls

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub


    Private Sub gridbind()

        Dim lstrDocNo As String = String.Empty
        Dim lstrBank As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrCreditor As String = String.Empty
        Dim lstrChqNo As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrFiltCHQDATE As String = String.Empty
        Dim lstrFiltPAYEE As String = String.Empty
        Dim lstrFiltAMOUNT As String = String.Empty
        Dim lstrOpr As String = String.Empty

        Dim txtSearch As New TextBox
        Dim larrSearchOpr() As String
        If gvJournal.Rows.Count > 0 Then
            ' --- Initialize The Variables           

            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   CHQ Date
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtChqDate")
            If txtSearch IsNot Nothing Then
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then lstrFiltCHQDATE = SetCondn(lstrOpr, "VHD_CHQDT", lstrDocNo)
            End If
            '   -- 2  Payee
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtPayee")
            If txtSearch IsNot Nothing Then
                lstrBank = txtSearch.Text
                If (lstrBank <> "") Then lstrFiltPAYEE = SetCondn(lstrOpr, "DETAILACCOUNT", lstrBank)
            End If
            '   -- 3  Amount
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            If txtSearch IsNot Nothing Then
                lstrCreditor = txtSearch.Text
                If (lstrCreditor <> "") Then lstrFiltAMOUNT = SetCondn(lstrOpr, "VHD_AMOUNT", lstrCreditor)
            End If

        End If
        Dim str_Filter As String = lstrFiltPAYEE & lstrFiltCHQDATE & lstrFiltAMOUNT

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim repSource As MyReportClass = DirectCast(Session("ReportSource"), MyReportClass)
        If repSource IsNot Nothing Then
            str_Sql = repSource.Command.CommandText.Replace("<%FILTER%>", str_Filter)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
        End If

        If gvJournal.Rows.Count > 0 Then

            txtSearch = gvJournal.HeaderRow.FindControl("txtChqDate")
            If txtSearch IsNot Nothing Then
                txtSearch.Text = lstrDocNo
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtPayee")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrBank

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrCreditor

            set_Menu_Img()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Private Function Find_Checked(ByVal ctr As Control, ByRef strGUID As StringBuilder, ByVal isPDC As Boolean) As StringBuilder
        Dim comma As String = String.Empty
        If isPDC Then
            For Each ctrl As Control In ctr.Controls
                If TypeOf ctrl Is GridViewRow Then
                    Dim gvrow As GridViewRow = ctrl
                    If TypeOf gvrow.FindControl("chkPosted") Is HtmlInputCheckBox Then
                        Dim chk As HtmlInputCheckBox = CType(gvrow.FindControl("chkPosted"), HtmlInputCheckBox)
                        If chk.Checked = True And chk.Disabled = False Then
                            If TypeOf gvrow.FindControl("lblGUID") Is Label Then
                                Dim lblGUID As Label = gvrow.FindControl("lblGUID")
                                strGUID.Append(comma & "'" & lblGUID.Text & "'")
                                comma = ","
                            End If
                            h_rptfiles.Value = gvrow.Cells(gvrow.Cells.Count - 1).Text
                        End If
                    End If
                Else
                    If ctrl.Controls.Count > 0 Then
                        Find_Checked(ctrl, strGUID, isPDC)
                    End If
                End If
            Next
         Else
            comma = " WHERE "
            For Each ctrl As Control In ctr.Controls
                If TypeOf ctrl Is GridViewRow Then
                    Dim gvrow As GridViewRow = ctrl
                    If TypeOf gvrow.FindControl("chkPosted") Is HtmlInputCheckBox Then
                        Dim chk As HtmlInputCheckBox = CType(gvrow.FindControl("chkPosted"), HtmlInputCheckBox)
                        If chk.Checked = True And chk.Disabled = False Then
                            If TypeOf gvrow.FindControl("lblchqDate") Is Label And _
                            TypeOf gvrow.FindControl("lblDETAILACCOUNT") Is Label Then
                                Dim lblchqDate As Label = gvrow.FindControl("lblchqDate")
                                Dim lblDETAILACCOUNT As Label = gvrow.FindControl("lblDETAILACCOUNT")


                                strGUID.Append(comma & "(VHD_CHQDT ='" & lblchqDate.Text & "' AND DETAILACCOUNT = '" & lblDETAILACCOUNT.Text.Replace("'", "''") & "')")
                                comma = " AND "
                            End If
                            h_rptfiles.Value = gvrow.Cells(gvrow.Cells.Count - 1).Text
                        End If
                    End If
                Else
                    If ctrl.Controls.Count > 0 Then
                        Find_Checked(ctrl, strGUID, isPDC)
                    End If
                End If
            Next
        End If
        Return strGUID
    End Function

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If MultiFormat() = True Then
            lblError.Text = "Multiple Cheque Format Not Allowed..!"
            Exit Sub
        End If

        Dim repSource As MyReportClass = DirectCast(Session("ReportSource"), MyReportClass)
        If repSource IsNot Nothing Then
            Dim strGUID As New StringBuilder
            Dim ChequePrint As String = IIf(Request.QueryString("ChequePrint") Is Nothing, String.Empty, Request.QueryString("ChequePrint"))
            Dim isPDC As Boolean = False
            If ChequePrint = "PDC" Then
                isPDC = True
                strGUID.Append("AND VOUCHER_D.GUID in (")
                Find_Checked(gvJournal, strGUID, isPDC)
                strGUID.Append(")")
            Else
                Find_Checked(gvJournal, strGUID, isPDC)
            End If
            Dim params As New Hashtable
            params("CASHCHQ") = False
            If strGUID.Length > 0 Then
                repSource.Command.CommandText = repSource.Command.CommandText.Replace("<%FILTER%>", strGUID.ToString()) ' & " AND GUID in(" & strGUID.ToString() & ")"
                Dim conn As SqlConnection = repSource.Command.Connection
                Dim drReader As SqlDataReader
                Try
                    drReader = SqlHelper.ExecuteReader(repSource.Command.Connection, CommandType.Text, repSource.Command.CommandText)
                    While (drReader.Read())
                        If drReader("DETAILACCOUNT").ToString().ToUpper().Trim() = "CASH" Then
                            params("CASHCHQ") = True
                        End If
                    End While
                Catch ex As Exception
                    drReader.Close()
                    conn.Close()
                Finally
                    drReader.Close()
                    conn.Close()
                End Try

                'Select Case ChequePrint
                '    Case "BP"
                '        Try
                '         Case "PDC"
                '            repSource.Command.CommandText = repSource.Command.CommandText & " AND VOUCHER_D.GUID in(" & strGUID.ToString() & ") order by VHD_CHQDT"
                '    End Select
                'Session("PrintReport") = repSource
                'Response.Redirect("../Reports/ASPX Report/printreport.aspx", True)
                repSource.Parameter = params
                repSource.ResourceName = "../RPT_Files/" & h_rptfiles.Value
                Session("ReportSource") = repSource
                ReportLoadSelection()
                'reg_clientScript()
                'Response.Write("<script>showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '','dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;');</script>")
                'Response.Redirect("../Reports/ASPX Report/RptViewer.aspx", True)
            End If
        End If
        gridbind()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    'Sub reg_clientScript()
    '    Dim scr As String = "<script>" & _
    '    "radopen('../Reports/ASPX Report/RptViewerModal.aspx', " & _
    '    "'pop_report');" & _
    '    "</script>"
    '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Done", scr)
    'End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub
    Private Function MultiFormat() As Boolean
        Dim rptName As String = ""
        Dim RetFormat As Boolean = False
        Dim chkPosted As New HtmlInputCheckBox
        Dim x As Integer = 0

        For Each gRow As GridViewRow In gvJournal.Rows

            If gRow.RowType = DataControlRowType.DataRow Then
                chkPosted = gRow.FindControl("chkPosted")
                If chkPosted.Checked Then
                    rptName = gvJournal.Rows(x).Cells(gvJournal.Rows(x).Cells.Count - 1).Text
                    If rptName <> gRow.Cells(gRow.Cells.Count - 1).Text.ToString() Then
                        RetFormat = True
                        Exit Function
                    End If
                End If
            End If
            If rptName = "" Then
                x = x + 1
            End If
        Next
        Return RetFormat
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        e.Row.Cells(e.Row.Cells.Count - 1).Style("display") = "none"
    End Sub
End Class
