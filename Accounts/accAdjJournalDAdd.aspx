﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAdjJournalDAdd.aspx.vb" Inherits="Accounts_accAdjJournalDAdd" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getAccount() {
            //popUp('960', '600', 'NORMAL', '<%=txtDAccountCode.ClientId %>', '<%=txtDAccountName.ClientId %>');
            //result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];
            var result = radopen("ShowAccount.aspx?ShowType=NORMAL&codeorname=" + document.getElementById('<%=txtDAccountCode.ClientID%>').value, "pop_up2")
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtDAccountCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtDAccountName.ClientID%>').value = NameandCode[1];
            }
        }

        function CopyDetails() {
            try {
                if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                    document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
            }
            catch (ex) { }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>



    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Adjustment Journal Direct
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Doc No</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Doc Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="6" /></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Currency</span></td>
                                    <td align="left">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" width="75%">
                                                    <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" TabIndex="8">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="25%">
                                                    <asp:TextBox ID="txtHExchRate" runat="server"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Group Rate</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHLocalRate" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Report</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportType" runat="server"
                                            DataSourceID="sdsReportType" DataTextField="RSM_DESCR"
                                            DataValueField="RSM_TYP" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsReportType" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                            SelectCommand="SELECT [RSM_TYP], [RSM_DESCR] FROM [RPTSETUP_M]  "></asp:SqlDataSource>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Report</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReprotlist" runat="server" DataTextField="RSS_DESCR"
                                            DataValueField="RSS_ID">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtHNarration" runat="server" TextMode="MultiLine" TabIndex="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">To Businessunit (CR)</span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="sdsBusinessunit"
                                            DataTextField="BSU_NAME" DataValueField="BSU_ID">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                            SelectCommand="select BSU_ID , BSU_NAME from [fn_GetBusinessUnits]   (@Usr_name) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME
">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="Usr_name" SessionField="sUsr_name" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Account</span> </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAccountCode" runat="server" AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDAccountCode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="btnHAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;" TabIndex="12" />&nbsp;
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAccountName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount</span> </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" CssClass="inputbox" Width="184px" TabIndex="14" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck"
                                            Type="Double" ValidationGroup="Details">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDNarration" runat="server" TextMode="MultiLine" TabIndex="16"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdds" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" TabIndex="18" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="20" />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="22" />&nbsp;
                                <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                            EmptyDataText="No Transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="Debit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDebit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("debit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Credit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("credit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="LinkButton1_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="15%"><span class="field-label">Debit Total</span> </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtTDotalDebit" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" width="15%"><span class="field-label">Credit Total</span></td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtTotalCredit" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" width="10%"><span class="field-label">Difference</span> </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtDifference" runat="server" ReadOnly="True"></asp:TextBox></td>

                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" ValidationGroup="Details" TabIndex="22" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="24" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" TabIndex="28" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="30" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>


