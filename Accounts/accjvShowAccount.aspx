<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accjvShowAccount.aspx.vb" Inherits="ShowAccount" Theme="General" %>
<%@ OutputCache Duration = "1" Location="None" VaryByParam = "none"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Accounts</title>
    <%--<link rel="stylesheet" type="text/css" href="../cssfiles/title.css" />
     <base target="_self" />
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> --%>
    
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
   
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

    <script language="javascript" type="text/javascript">
 
    function Return(retval)
    {
    window.returnValue = retval;
    //alert(retval);
    window.close();    
    } 
       
                
        function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }                           
    </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" >
    <form id="form1" runat="server">

      <table width="100%">            
                <tr> 
                    <td align="center">
                        <asp:GridView ID="gvGroup" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" DataKeyNames="ACT_ID" EmptyDataText="No Data" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                    <ItemTemplate>
                                        &nbsp; &nbsp;
                                        <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ACT_ID") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Select
                                        <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                            value="Check All" />&nbsp;
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Code" SortExpression="ACT_ID">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                        Account Code<br />
                                        <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                        <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Name" SortExpression="ACT_NAME">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <ItemStyle Width="150px" />
                                    <HeaderTemplate>
                                        Account Name<br />
                                        <asp:TextBox ID="txtName" runat="server" Width="75%"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("ACT_NAME") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Name">
                                    <ItemStyle />
                                    <HeaderTemplate>
                                        Account Name<br />
                                        <asp:TextBox ID="txtActName" runat="server" Width="75%"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchACTName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Type" SortExpression="ACT_TYPE">
                                    <HeaderTemplate>
                                        Account Type<br />
                                                    <asp:DropDownList ID="DDAccountType" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="DDAccountType_SelectedIndexChanged" Width="80%" >
                                                        <asp:ListItem>All</asp:ListItem>
                                                        <asp:ListItem Value="ASSET">ASSET</asp:ListItem>
                                                        <asp:ListItem Value="EXPENSES">EXPENSES</asp:ListItem>
                                                        <asp:ListItem Value="INCOME">INCOME</asp:ListItem>
                                                        <asp:ListItem>LIABILITY</asp:ListItem>
                                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("ACT_TYPE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Control Account" SortExpression="ACT_CTRLACC">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CTRL_NAME") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                       Control Account<br />
                                       <asp:TextBox ID="txtControl" runat="server" Width="75%"></asp:TextBox>
                                       <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("CTRL_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bank Or Cash" SortExpression="ACT_BANKCASH">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("ACT_BANKCASH") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Bank Or Cash<br />
                                                    <asp:DropDownList ID="DDBankorCash" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="DDBankorCash_SelectedIndexChanged" Width="80%" >
                                                        <asp:ListItem>All</asp:ListItem>
                                                        <asp:ListItem Value="B">Bank</asp:ListItem>
                                                        <asp:ListItem Value="C">Cash</asp:ListItem>
                                                        <asp:ListItem Value="N">Normal</asp:ListItem>
                                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" SortExpression="ACT_FLAG">
                                    <HeaderTemplate>
                                        Type<br />
                                        <asp:DropDownList ID="DDCutomerSupplier" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="DDCutomerSupplier_SelectedIndexChanged" Width="80%" >
                                                        <asp:ListItem>All</asp:ListItem>
                                                        <asp:ListItem Value="C">Customer</asp:ListItem>
                                                        <asp:ListItem Value="S">Supplier</asp:ListItem>
                                                        <asp:ListItem Value="N">Normal</asp:ListItem>
                                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("ACT_FLAG") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns> 
                        </asp:GridView> 
                    </td> 
                </tr>
          <tr>
              <td align="left" ><asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True" Text="Select All" CssClass="radiobutton" /></td>
          </tr>
                <tr> 
                    <td align="center" colspan="4">
                        <asp:Button ID="Button2" runat="server" CssClass="button" Text="Finish" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                        <input id="h_selected_menu_4" runat="server" type="hidden" value="=" /></td>
                </tr>                 
            </table>  
  
    </form>
</body>
</html>
