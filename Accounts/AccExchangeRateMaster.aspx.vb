﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Accounts_AccExchangeRateMaster
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                txtBUID.Visible = False
                bindlist()
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A100095" And ViewState("MainMnu_code") <> "A550024") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If


                If Request.QueryString("viewid") <> "" Then
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    SetDataMode("view")
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub bindlist()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "select * FROM CURRENCY_M"
            'ds = 
            ddlCurrencytype.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCurrencytype.DataTextField = "CUR_ID"
            ddlCurrencytype.DataValueField = "CUR_DESCR"
            ddlCurrencytype.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable
            dt = MainObj.getRecords("select * from EXGRATE_TRN where ETR_ID=" & p_Modifyid, "MAINDB")

            If dt.Rows.Count > 0 Then
                h_Etr_Id.Value = p_Modifyid
                h_Approve.Value = dt.Rows(0)("etr_approve")
                ddlCurrencytype.SelectedItem.Text = dt.Rows(0)("ETR_CUR_ID").ToString
                txtBUID.Text = dt.Rows(0)("ETR_BSU_ID").ToString
                txtExchRate.Text = dt.Rows(0)("ETR_RATE").ToString
                txtFromDate.Text = IIf(IsDBNull(dt.Rows(0)("ETR_FDATE")), "", Format(dt.Rows(0)("ETR_FDATE"), "dd-MMM-yyyy"))
                If IsDBNull(dt.Rows(0)("ETR_TDATE")) = True Then
                    txtToDate.Text = DBNull.Value.ToString
                Else
                    txtToDate.Text = Format(dt.Rows(0)("ETR_TDATE"), "dd-MMM-yyyy")
                End If
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
            Else
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            ddlCurrencytype.Enabled = False
            txtExchRate.Enabled = False
            txtFromDate.Enabled = False
            txtToDate.Enabled = False

        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
            txtExchRate.Enabled = True
            ddlCurrencytype.Enabled = True
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            ddlCurrencytype.Enabled = False
            If Not txtToDate.Text = "" Then
                txtExchRate.Enabled = False
                txtFromDate.Enabled = False
                txtToDate.Enabled = False
            Else
                txtExchRate.Enabled = True
                txtFromDate.Enabled = True
                txtToDate.Enabled = True
            End If
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable

        If ViewState("MainMnu_code") = "A550024" Then
            btnAdd.Visible = False

            If h_Approve.Value = False Then
                btnApprove.Visible = True
            Else
                btnApprove.Visible = False
                btnSave.Visible = False
                btnDelete.Visible = False
                btnEdit.Visible = False
            End If
        Else
            btnApprove.Visible = False
        End If

    End Sub

    Sub clear_All()
        h_Etr_Id.Value = 0
        txtBUID.Text = Session("sBsuid")
        txtBUID.ReadOnly = True
        TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
        txtExchRate.Text = "0.00"
        txtFromDate.Text = ""
        txtToDate.Text = ""

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If h_Approve.Value = False Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from EXGRATE_TRN where ETR_ID=" & h_Etr_Id.Value & "and ETR_BSU_ID='" & txtBUID.Text & "'")
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = "unable to delete, " & ddlCurrencytype.SelectedItem.Text & " used in transactions"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""

            If txtFromDate.Text = "" Then
                lblError.Text = "Empty from date is not allowed."
                Exit Sub
            End If
            
            If Not txtFromDate.Text = "" And Not txtToDate.Text = "" Then
                If (txtToDate.Text > txtFromDate.Text) Then
                    lblError.Text = "Invalid Date.from date should be less ToDate"
                    Exit Sub
                End If
            End If
            If Not IsNumeric(txtExchRate.Text) Then
                lblError.Text = "Enter numbers only"
                Exit Sub
            End If


            Dim pParms(12) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@ETR_ID", h_Etr_Id.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@ETR_CUR_ID", ddlCurrencytype.SelectedItem.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@ETR_BSU_ID", txtBUID.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@ETR_RATE", txtExchRate.Text, SqlDbType.Decimal)
            pParms(5) = Mainclass.CreateSqlParameter("@ETR_FDATE", txtFromDate.Text, SqlDbType.DateTime)
            pParms(6) = Mainclass.CreateSqlParameter("@ETR_TDATE", IIf(txtToDate.Text = "", DBNull.Value, txtToDate.Text), SqlDbType.DateTime)
            pParms(7) = Mainclass.CreateSqlParameter("@ETR_APPROVE", 0, SqlDbType.Decimal)
            pParms(8) = Mainclass.CreateSqlParameter("@ETR_APPROVEDBY", "", SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@ETR_APPROVEDDATE", DBNull.Value, SqlDbType.DateTime)
            pParms(10) = Mainclass.CreateSqlParameter("@ETR_EXG_ID", DBNull.Value, SqlDbType.Int)
            pParms(11) = Mainclass.CreateSqlParameter("@ETR_CREATEDBY", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@ETR_CREATEDDATE", Now.Date, SqlDbType.DateTime)

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveExchRateMaster_Trn", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim pParms(5) As SqlParameter
        Dim str As String

        pParms(1) = Mainclass.CreateSqlParameter("@EXG_ID", 0, SqlDbType.Int)
        str = pParms(1).Value
        pParms(2) = Mainclass.CreateSqlParameter("@ETR_APPROVE", 1, SqlDbType.Decimal)
        str = str & pParms(2).Value
        pParms(3) = Mainclass.CreateSqlParameter("@ETR_APPROVEDBY", Session("sUsr_name"), SqlDbType.VarChar)
        str = str & pParms(3).Value
        pParms(4) = Mainclass.CreateSqlParameter("@ETR_APPROVEDDATE", Now.Date, SqlDbType.DateTime)
        str = str & pParms(4).Value
        pParms(5) = Mainclass.CreateSqlParameter("@ETR_ID", h_Etr_Id.Value, SqlDbType.Int)
        str = str & pParms(5).Value

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveExchRateMaster_Trn_APPROVE", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                ViewState("EntryId") = pParms(1).Value
                lblError.Text = "Data Saved Successfully !!!"
                setModifyHeader(ViewState("EntryId"))
                SetDataMode("view")
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try

    End Sub
End Class

