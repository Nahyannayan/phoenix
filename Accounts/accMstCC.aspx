<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accMstCC.aspx.vb" Inherits="accMstCC" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                       
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvCommonCC" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" DataKeyNames="ID" Width="100%" PageSize="20">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" SortExpression="GRP_ID">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblHCode" runat="server">ID</asp:Label><br />
                                                        <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnCodeSearch_Click" />

                                                    </HeaderTemplate>
                                                    <ItemStyle />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcode" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" SortExpression="DESCR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGroupname" runat="server" CssClass="inputbox" MaxLength="100"
                                                            Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblName" runat="server">Description</asp:Label><br />
                                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchpar_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("View") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click1">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                </table>
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server"
                        type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>

