Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccEditAccount
    Inherits System.Web.UI.Page

    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (MainMnu_code <> "A100010" And MainMnu_code <> "A100002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    ViewState("MainMnu_code") = MainMnu_code


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim i As Integer


                    tr_ControlAccount.Visible = True
                    tr_SubGroup.Visible = False

                    btnSave.Attributes.Add("onclick", "javascript:return Validate();")
                    txtSGroup.Attributes.Add("readonly", "readonly")
                    txtSGroupDescr.Attributes.Add("readonly", "readonly")
                    txtControlAcc.Attributes.Add("readonly", "readonly")
                    txtControlAccDescr.Attributes.Add("readonly", "readonly")
                    txtAccCode.Attributes.Add("readonly", "readonly")
                    txtAccCode2.Attributes.Add("readonly", "readonly")
                    txtAccDescr.Attributes.Add("readonly", "readonly")
                    set_visible_accounts()


                    bindCurrency()
                    For i = 0 To cmbCurrency.Items.Count - 1
                        If cmbCurrency.Items(i).Value = Session("BSUCurrId") Then
                            cmbCurrency.SelectedIndex = i
                            Exit For
                        End If
                    Next
                    BindType()
                    BindPolicyGrp()
                    BindPymntTrm()
                    Call radioStatus()
                    Call BankCash()
                    '  gridbind()
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))




                        FillValues()
                        FillRptDetails()
                        FillLinkedAccounts()
                        bind_RPTSETUP_M()
                        If optCustomer.Checked Or optSupplier.Checked Then
                            trCreditDays.Visible = True
                        Else
                            trCreditDays.Visible = False
                        End If
                    End If
                End If



                UtilityObj.beforeLoopingControls(Me.Page)

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub


    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL As String
            Dim lstrSQL2 As String
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer  

            lstrSQL = "SELECT * FROM vw_OSA_ACCOUNTS_M WHERE ACT_ID='" & ViewState("viewid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("ACT_Bctrlac") = True) Then
                    optControlAccNo.Checked = False
                    optControlAccYes.Checked = True
                    set_visible_accounts()
                End If
                txtSGroup.Text = ds.Tables(0).Rows(0)("ACT_SGP_ID")
                txtSGroupDescr.Text = ds.Tables(0).Rows(0)("SGP_DESCR")
                txtControlAcc.Text = ds.Tables(0).Rows(0)("ACT_CTRLACC")
                txtControlAccDescr.Text = ds.Tables(0).Rows(0)("ControlAcc")
                txtAccCode.Text = Left(ds.Tables(0).Rows(0)("ACT_ID"), ds.Tables(0).Rows(0)("ACT_SGP_ID").ToString.Length)
                txtAccCode2.Text = Right(ds.Tables(0).Rows(0)("ACT_ID"), ds.Tables(0).Rows(0)("ACT_ID").ToString.Length - ds.Tables(0).Rows(0)("ACT_SGP_ID").ToString.Length)
                txtAccDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")

                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("ACT_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                cmbType.SelectedIndex = -1
                cmbType.Items.FindByValue(ds.Tables(0).Rows(0)("ACT_TYPE")).Selected = True
                
                cmbBankCash.SelectedIndex = -1
                For i = 0 To cmbBankCash.Items.Count - 1
                    If cmbBankCash.Items(i).Value = ds.Tables(0).Rows(0)("ACT_BANKCASH") Then
                        cmbBankCash.SelectedIndex = i
                        Exit For
                    End If
                Next

                txtBankAccount.Text = ds.Tables(0).Rows(0)("ACT_BANKACCNO")
                txtFaciCelng.Text = ds.Tables(0).Rows(0)("ACT_FACILITYCEILING")
                txtSalesTax.Text = ds.Tables(0).Rows(0)("ACT_SALESTAXNO")
                txtCreditDays.Text = Val(ds.Tables(0).Rows(0)("ACT_CREDITDAYS").ToString)
                cmbPymntTrm.SelectedIndex = -1
                For i = 0 To cmbPymntTrm.Items.Count - 1
                    If cmbPymntTrm.Items(i).Value = ds.Tables(0).Rows(0)("ACT_PTM_ID") Then
                        cmbPymntTrm.SelectedIndex = i
                        Exit For
                    End If
                Next

                If (ds.Tables(0).Rows(0)("ACT_FLAG") = "C") Then
                    optCustomer.Checked = True
                ElseIf (ds.Tables(0).Rows(0)("ACT_FLAG") = "S") Then
                    optSupplier.Checked = True
                Else
                    optNormal.Checked = True
                End If

                For i = 0 To cmbPolicyGrp.Items.Count - 1
                    If cmbPolicyGrp.Items(i).Value = ds.Tables(0).Rows(0)("ACT_PLY_ID") Then
                        cmbPolicyGrp.SelectedIndex = i
                        Exit For
                    End If
                Next

                Dim s1 As Boolean = ds.Tables(0).Rows(0)("ACT_BACTIVE")
                If s1 = True Then
                    chkActive.Checked = True
                Else
                    chkActive.Checked = False
                End If

                Dim ItemTypeCounter As Integer
                Dim temp As String = ds.Tables(0).Rows(0)("ACT_COUNTRY") & ""

                Using CountryReader As SqlDataReader = AccessRoleUser.GetCountry()
                    While CountryReader.Read

                        cmbCountry.Items.Add(New ListItem(CountryReader("CTY_DESCR").ToString, CountryReader("CTY_ID").ToString))

                        If cmbCountry.Items(ItemTypeCounter).Value = temp Then
                            cmbCountry.SelectedIndex = ItemTypeCounter
                        End If
                        ItemTypeCounter = ItemTypeCounter + 1

                    End While
                End Using
                ItemTypeCounter = 0
                temp = ds.Tables(0).Rows(0)("ACT_CITY") & ""
                Using CountryReader As SqlDataReader = AccessRoleUser.GetCity()
                    While CountryReader.Read
                        cmbCity.Items.Add(New ListItem(CountryReader("CIT_DESCR").ToString, CountryReader("CIT_ID").ToString))
                        If cmbCity.Items(ItemTypeCounter).Value = temp Then
                            cmbCity.SelectedIndex = ItemTypeCounter
                        End If
                        ItemTypeCounter = ItemTypeCounter + 1

                    End While
                End Using

                ''account mis


                For Each cur_row As GridViewRow In gvRPTSETUP_M.Rows
                    ' Access the CheckBox
                    Dim lblRSM_TYP As Label = TryCast(cur_row.FindControl("lblRSM_TYP"), Label)
                    Dim hfDescription As HiddenField = TryCast(cur_row.FindControl("hfDescription"), HiddenField)
                    Dim hfDescription1 As HiddenField = TryCast(cur_row.FindControl("hfDescription1"), HiddenField)

                    If hfDescription.Value.EndsWith("|") Then
                        hfDescription.Value = hfDescription.Value.Substring(0, hfDescription.Value.Length - 1)
                    End If
                    Dim txtDesc As TextBox = TryCast(cur_row.FindControl("txtCode"), TextBox)

                    Dim txtDesc1 As TextBox = TryCast(cur_row.FindControl("txtCode1"), TextBox)
                    If lblRSM_TYP IsNot Nothing Then


                        Dim str_sql As String = "SELECT RSS.RSS_DESCR, RSS.RSS_CODE, RPA.RPA_ACC_ID, RSS.RSS_TYP," _
                        & " CASE RSS.RSS_RFS_ID WHEN 'CR' THEN 'CR' WHEN 'DR' THEN 'DR' ELSE '' END AS  RSS_RFS_ID" _
                        & " FROM RPTSETUP_S AS RSS INNER JOIN RPTSETUP_M AS RSM ON RSS.RSS_TYP = RSM.RSM_TYP " _
                        & " INNER JOIN RPTSETUP_S_ACC AS RPA ON RSS.RSS_CODE = RPA.RPA_RSS_CODE" _
                        & " WHERE (RSS.RSS_LNE_TYP NOT IN ('H', 'T')) AND (RSM.RSM_TYP = '" & lblRSM_TYP.Text & "') AND RPA.RPA_ACC_ID='" & ViewState("viewid") & "'"
                        Dim ds_ As DataSet
                        ds_ = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, str_sql)
                        If ds_.Tables(0).Rows.Count > 0 Then
                            If ds_.Tables(0).Rows(0)("RSS_RFS_ID").ToString = "" Then
                                hfDescription.Value = ds_.Tables(0).Rows(0)("RSS_CODE").ToString & "|"
                                txtDesc.Text = ds_.Tables(0).Rows(0)("RSS_DESCR").ToString
                            Else
                                hfDescription.Value = ds_.Tables(0).Rows(0)("RSS_CODE").ToString & "|" & ds_.Tables(0).Rows(0)("RSS_RFS_ID").ToString
                                txtDesc.Text = ds_.Tables(0).Rows(0)("RSS_DESCR").ToString

                                hfDescription1.Value = ds_.Tables(0).Rows(1)("RSS_CODE").ToString
                                txtDesc1.Text = ds_.Tables(0).Rows(1)("RSS_DESCR").ToString
                            End If
                        End If
                    End If
                Next

                ''''mis


                txtContact.Text = ds.Tables(0).Rows(0)("ACT_CONTACTPERSON")
                txtDesig.Text = ds.Tables(0).Rows(0)("ACT_CONTACTDESIG")
                txtAddress1.Text = ds.Tables(0).Rows(0)("ACT_ADDRESS1")
                txtAddress2.Text = ds.Tables(0).Rows(0)("ACT_ADDRESS2")
                txtPhone.Text = ds.Tables(0).Rows(0)("ACT_PHONE")
                txtMobile.Text = ds.Tables(0).Rows(0)("ACT_MOBILE")
                txtFax.Text = ds.Tables(0).Rows(0)("ACT_FAX")
                txtEmail.Text = ds.Tables(0).Rows(0)("ACT_EMAIL")



                '   --- Check The CheckBoxes In The Grid

                Dim ds1 As New DataSet
                Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim Qry As New StringBuilder
                Select Case ViewState("MainMnu_code")
                    Case "A100002" ' limits the business units by user's access for GEMS GLOBAL
                        Qry.Append("SELECT BSU_ID ,BSU_NAME FROM dbo.BUSINESSUNIT_M AS A WITH(NOLOCK) INNER JOIN ")
                        Qry.Append("dbo.USERACCESS_S B WITH(NOLOCK) ON A.BSU_ID=B.USA_BSU_ID INNER JOIN dbo.USERS_M C ")
                        Qry.Append("ON B.USA_USR_ID=C.USR_ID WHERE 1=1 AND USR_NAME='" & Session("sUsr_name") & "' ")
                    Case Else
                        Qry.Append("SELECT BSU_ID ,BSU_NAME FROM dbo.BUSINESSUNIT_M AS A WITH(NOLOCK) ")
                End Select
                Qry.Append(" ORDER BY BSU_NAME")
                Dim busString As String = Qry.ToString

                ' lstrSQL2 = "SELECT ACS_BSU_ID FROM ACCOUNTS_S WHERE ACS_ACT_ID='" & ViewState("viewid") & "' "
                Dim ACT_BSU_IDS As String
                lstrSQL2 = "SELECT ISNULL(ACT_BSU_ID,'')ACT_BSU_ID FROM ACCOUNTS_M WHERE ACT_ID='" & ViewState("viewid") & "'"
                ACT_BSU_IDS = Mainclass.getDataValue(lstrSQL2, "mainDB")
                lstrSQL2 = "SELECT ID ACS_BSU_ID FROM OASIS..FNSPLITME('" & ACT_BSU_IDS & "',',')"


                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, busString)
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Dim row As DataRow
                For Each row In ds1.Tables(0).Rows
                    Dim str1 As String = row("bsu_name")
                    Dim str2 As String = row("bsu_id")
                    chkBusUnit.Items.Add(New ListItem(str1, str2))
                    Dim row2 As DataRow
                    For Each row2 In ds2.Tables(0).Rows
                        Dim str3 As String = row2("ACS_BSU_ID")
                        If str2 = str3 Then
                            chkBusUnit.Items.FindByText(str1).Selected = True
                        End If
                    Next
                Next
            Else
            End If
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Private Sub bindCurrency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String



            str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
               & " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
               & " (ltrim(B.EXG_RATE)+'__'+" _
               & " ltrim(a.EXG_RATE ))+'__'+" _
               & " ltrim(a.EXG_ID) as RATES " _
                & " FROM EXGRATE_S A,EXGRATE_S B" _
                           & " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
                           & " AND GETDATE() BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
                           & " AND " _
                           & " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
                           & " AND " _
                           & " B.EXG_CUR_ID='" & Session("BSU_CURRENCY") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            cmbCurrency.DataSource = ds.Tables(0)
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then

                If set_default_currency() <> True Then

                    cmbCurrency.SelectedIndex = 0



                End If

            Else

            End If  
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function set_default_currency() As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As ListItem In cmbCurrency.Items
                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then
                        item.Selected = True
                        Return True
                        Exit For
                    End If
                Next
            Else
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function


    Private Sub BindType()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT DISTINCT ACT_TYPE From Accounts_M"

            cmbType.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbType.DataTextField = "ACT_TYPE"
            cmbType.DataValueField = "ACT_TYPE"
            cmbType.DataBind()
            bind_RPTSETUP_M()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub BindPolicyGrp()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT PLY_ID,PLY_DESCR From Policy_M"

            cmbPolicyGrp.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbPolicyGrp.DataTextField = "PLY_DESCR"
            cmbPolicyGrp.DataValueField = "PLY_ID"
            cmbPolicyGrp.DataBind()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub BindPymntTrm()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSql As String = "SELECT PTM_ID,PTM_DESCR From PaymentTerm_M"
            cmbPymntTrm.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            cmbPymntTrm.DataTextField = "PTM_DESCR"
            cmbPymntTrm.DataValueField = "PTM_ID"
            cmbPymntTrm.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        Dim lstrReptype As String
        Dim lstrFlag As String = String.Empty
        Dim lstrSelOne As String = String.Empty
        Dim lstrAllBusUnit As String = String.Empty
        Dim flagReportlinkSelected As Boolean = False
        If optCustomer.Checked = True Then
            lstrFlag = "C"
        ElseIf optSupplier.Checked = True Then
            lstrFlag = "S"
        Else
            lstrFlag = "N"
        End If
        '       VALIDATIONS 
        ServerValidate()
        lstrReptype = ""
        For Each item As ListItem In chkBusUnit.Items
            If (item.Selected) Then
                'loop through the each checkbox and insert it
                lstrSelOne = "OK"
            End If
        Next

        If txtControlAcc.Text = "" Then
            txtControlAcc.Text = txtAccCode.Text & txtAccCode2.Text
        End If

        If gvRptDetails.Rows.Count > 0 Then
            flagReportlinkSelected = True
        End If

        If (lstrErrMsg = "") Then
            '    Proceed to Save
            If lstrSelOne = "OK" Then
                For Each item As ListItem In chkBusUnit.Items
                    If (item.Selected) Then
                        'loop through the each checkbox and insert it
                        Dim lstrBusUnit As String = item.Value
                        lstrAllBusUnit = lstrAllBusUnit & "," & lstrBusUnit
                    End If
                Next
                lstrAllBusUnit = Mid(lstrAllBusUnit, 2, lstrAllBusUnit.Length)
            Else
                lstrAllBusUnit = String.Empty
            End If
            If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim SqlCmd As New SqlCommand("SaveACCOUNTS_M", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    SqlCmd.Parameters.AddWithValue("@ACT_ID", ViewState("viewid"))
                    SqlCmd.Parameters.AddWithValue("@ACT_NAME", txtAccDescr.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_SGP_ID", Left(txtControlAcc.Text, 3))
                    SqlCmd.Parameters.AddWithValue("@ACT_TYPE", cmbType.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_BANKCASH", cmbBankCash.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_CTRLACC", txtControlAcc.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_Bctrlac", optControlAccYes.Checked)
                    SqlCmd.Parameters.AddWithValue("@ACT_FLAG", lstrFlag)
                    SqlCmd.Parameters.AddWithValue("@ACT_bPREPAYMENT", False)
                    SqlCmd.Parameters.AddWithValue("@ACT_BANKACCNO", txtBankAccount.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CREDITDAYS", Val(txtCreditDays.Text))

                    If chkActive.Checked = True Then
                        SqlCmd.Parameters.AddWithValue("@ACT_BACTIVE", True)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_BACTIVE", False)
                    End If
                    If cmbBankCash.SelectedItem.Value = "B" Then
                        SqlCmd.Parameters.AddWithValue("@ACT_FACILITYCEILING", txtFaciCelng.Text)
                    Else
                        SqlCmd.Parameters.AddWithValue("@ACT_FACILITYCEILING", "0")
                    End If
                    SqlCmd.Parameters.AddWithValue("@ACT_PLY_ID", cmbPolicyGrp.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_CUR_ID", cmbCurrency.SelectedItem.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_ADDRESS1", txtAddress1.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_ADDRESS2", txtAddress2.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CITY", cmbCity.SelectedValue)
                    SqlCmd.Parameters.AddWithValue("@ACT_COUNTRY", cmbCountry.SelectedValue)
                    SqlCmd.Parameters.AddWithValue("@ACT_PHONE", txtPhone.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_FAX", txtFax.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_MOBILE", txtMobile.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_EMAIL", txtEmail.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CONTACTPERSON", txtContact.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_CONTACTDESIG", txtDesig.Text)
                    SqlCmd.Parameters.AddWithValue("@ACT_SALESTAXNO", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_PTM_ID", cmbPymntTrm.SelectedItem.Value)
                    SqlCmd.Parameters.AddWithValue("@ACT_OPP_ACT_ID", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_RFS_ID", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_RFS_ID_CF", "")
                    SqlCmd.Parameters.AddWithValue("@ACT_OPBAL", "0")
                    SqlCmd.Parameters.AddWithValue("@ACT_CURRBAL", "0")
                    SqlCmd.Parameters.AddWithValue("@ACT_BSU_ID", lstrAllBusUnit)
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)

                    '   --- Proceed to Save The BusinessUnits
                    If lintRetVal = 0 Then
                        lstrAllBusUnit = ""

                        For Each item As ListItem In chkBusUnit.Items
                            If (item.Selected) Then
                                'loop through the each checkbox and insert it
                                Dim lstrBusUnit As String = item.Value
                                lstrAllBusUnit = lstrAllBusUnit & "|" & lstrBusUnit
                            End If
                        Next

                        Dim SqlCmd2 As New SqlCommand("InsertUnits", objConn, stTrans)
                        SqlCmd2.CommandType = CommandType.StoredProcedure
                        SqlCmd2.Parameters.AddWithValue("@pActId", ViewState("viewid"))
                        SqlCmd2.Parameters.AddWithValue("@IDs", lstrAllBusUnit)
                        SqlCmd2.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        SqlCmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        SqlCmd2.ExecuteNonQuery()
                        lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)



                        If lintRetVal = 0 Then
                            '''''
                            For Each row As GridViewRow In gvRPTSETUP_M.Rows
                                ' Access the CheckBox
                                Dim lblRSM_TYP As Label = TryCast(row.FindControl("lblRSM_TYP"), Label)
                                Dim hfDescription As HiddenField = TryCast(row.FindControl("hfDescription"), HiddenField)
                                Dim hfDescription1 As HiddenField = TryCast(row.FindControl("hfDescription1"), HiddenField)

                                If hfDescription.Value.EndsWith("|") Then
                                    hfDescription.Value = hfDescription.Value.Substring(0, hfDescription.Value.Length - 1)
                                End If
                                Dim txtDesc As TextBox = TryCast(row.FindControl("txtDesc"), TextBox)
                                Dim txtSub As TextBox = TryCast(row.FindControl("txtCode2"), TextBox)

                                If lblRSM_TYP IsNot Nothing Then
                                    If hfDescription.Value.Split("|").Length > 1 Then

                                        If hfDescription1.Value = "" Then
                                            lblErr.Text = "Please select opposite code"
                                            stTrans.Rollback()
                                            Exit Sub

                                        End If
                                        If hfDescription.Value.Split("|").Length > 1 Then
                                            lintRetVal = AccountFunctions.SaveRPTSETUP_S_ACC(lblRSM_TYP.Text, hfDescription.Value.Split("|")(0), _
                                            hfDescription1.Value.Split("|")(0), ViewState("viewid"), txtSub.Text, stTrans)
                                            flagReportlinkSelected = True
                                        Else
                                            lblErr.Text = "Please select opposite code"
                                            stTrans.Rollback()
                                            Exit Sub
                                        End If
                                    Else
                                        If hfDescription.Value <> "" Then

                                            If CheckSubAccount(lblRSM_TYP.Text, hfDescription.Value) And txtSub.Text = "" Then
                                                lblErr.Text = "Please select Sub Account"
                                                stTrans.Rollback()
                                                Exit Sub
                                            End If

                                            lintRetVal = AccountFunctions.SaveRPTSETUP_S_ACC(lblRSM_TYP.Text, hfDescription.Value, "", ViewState("viewid"), txtSub.Text, stTrans)
                                            flagReportlinkSelected = True
                                        Else
                                            If gvRptDetails.Rows.Count < 2 Then
                                                If (lblRSM_TYP.Text = "MB") Then
                                                    If hfDescription.Value = "" Then
                                                        'flagReportType = False
                                                        lstrReptype = "MB"
                                                        flagReportlinkSelected = False
                                                    End If
                                                ElseIf (lblRSM_TYP.Text = "MP") Then
                                                    If hfDescription.Value = "" Then
                                                        'flagReportType = False
                                                        lstrReptype = "MP"
                                                        flagReportlinkSelected = False
                                                    End If
                                                ElseIf (lblRSM_TYP.Text = "AC") Then
                                                    If hfDescription.Value = "" Then
                                                        'flagReportType = False
                                                        lstrReptype = "AC"
                                                        flagReportlinkSelected = False
                                                    End If
                                                End If
                                            End If
                                    End If
                                        End If
                                End If
                            Next
                            If lintRetVal <> 0 Or Not flagReportlinkSelected Or lstrReptype <> "" Then
                                If lstrReptype = "MB" Then
                                    lblErr.Text = "Please Select Balance Sheet and Cashflow Statements Report links"
                                ElseIf lstrReptype = "MP" Then
                                    lblErr.Text = "Please Select Profit/Loss and Cashflow Statements Report links"
                                Else
                                    lblErr.Text = "Please Select Atleast two Report links"
                                End If
                                stTrans.Rollback()
                                'lblErr.Text = "Please Select Proper Report Code/Link Atleast one Report"
                                Exit Sub
                            Else
                                stTrans.Commit()
                            End If
                            '''''

                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtAccCode.Text & txtAccCode2.Text, "Edit", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If 
                            ViewState("datamode") = "none"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            Call clearall()
                            lblErr.Text = "Data Successfully Saved..."
                        Else
                            stTrans.Rollback()
                            lblErr.Text = getErrorMessage(lintRetVal)
                        End If
                    Else
                        lblErr.Text = getErrorMessage(lintRetVal)
                    End If
               
                Catch ex As Exception
                    stTrans.Rollback()
                    lblErr.Text = UtilityObj.getErrorMessage("1000")
                    UtilityObj.Errorlog(ex.Message)
                End Try
            End If
        End If
    End Sub


    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If optControlAccYes.Checked = True Then
            If txtSGroup.Text = "" Then
                lstrErrMsg = lstrErrMsg & "Please Select The Subgroup" & "<br>"
            End If
        Else
            If txtControlAcc.Text = "" Then
                lstrErrMsg = lstrErrMsg & "Please Select The Control Account" & "<br>"
            End If
        End If
        If cmbBankCash.SelectedValue = "B" Then
            If (IsNumeric(txtFaciCelng.Text) = False) Then
                lstrErrMsg = lstrErrMsg & "Facility Celing Should Be A Numeric Value" & "<br>"
            End If
        End If

        lblErr.Text = lstrErrMsg
    End Sub


    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("SAVED SUCCESSFULLY...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function


    Protected Sub optControlAccYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccYes.CheckedChanged
        set_visible_accounts()
    End Sub


    Protected Sub optControlAccNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccNo.CheckedChanged
        set_visible_accounts()
    End Sub


    Sub set_visible_accounts()
        If optControlAccYes.Checked = True Then
            tr_ControlAccount.Visible = False
            tr_SubGroup.Visible = True
            txtAccCode.Text = ""
            txtAccCode2.ReadOnly = False
            txtAccCode2.Text = ""
            txtAccDescr.Text = ""
            txtControlAcc.Text = ""
            txtControlAccDescr.Text = ""
            optSupplier.Checked = False
            optNormal.Checked = True
            cmbType.SelectedIndex = 0
        Else
            tr_ControlAccount.Visible = True
            tr_SubGroup.Visible = False
            txtAccCode2.ReadOnly = True
            txtAccCode.Attributes.Add("readonly", "readonly")
            txtAccCode.Text = ""
            txtAccCode2.ReadOnly = True
            txtAccCode2.Text = ""
            txtAccDescr.Text = ""
        End If
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
            ' Response.Redirect("~\Accounts\AccMstAccounts.aspx")
        End If

    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        Dim Eid As String
        'Delete  the  user
        Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                Eid = ViewState("viewid")
                Status = AccessRoleUser.DeleteChartAcc(Eid, transaction)
                If Status = -1 Then
                    Throw New ArgumentException("Record do not exits")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record can not be Deleted")
                Else
                    Status = UtilityObj.operOnAudiTable(Master.MenuName, Eid, "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        Throw New ArgumentException("Could not complete your request")
                    End If
                    ViewState("datamode") = "none"

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearall()
                    lblErr.Text = "Record Deleted Successfully"
                End If
            Catch myex As ArgumentException
                transaction.Rollback()
                lblErr.Text = myex.Message
            Catch ex As Exception
                transaction.Rollback()
                lblErr.Text = "Record could not be Deleted"
            End Try
        End Using



       




    End Sub


    Sub clearall()
        txtSGroup.Text = ""
        txtSGroupDescr.Text = ""
        txtControlAcc.Text = ""
        txtControlAccDescr.Text = ""
        txtAccCode.Text = ""
        txtAccCode2.Text = ""
        txtAccDescr.Text = ""
        txtBankAccount.Text = ""
        txtSalesTax.Text = ""
        txtFaciCelng.Text = ""
        txtContact.Text = ""
        txtDesig.Text = ""
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtFax.Text = ""
        txtCreditDays.Text = 0
        gvRptDetails.DataSource = Nothing
        gvLinkedAcc.DataSource = Nothing
        gvLinkedAcc.DataBind()
        gvRptDetails.DataBind()
    End Sub


    Protected Sub optSupplier_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optSupplier.CheckedChanged
        Call radioStatus()
    End Sub


    Sub BankCash()
        If cmbBankCash.SelectedValue <> "B" Then
            txtFaciCelng.Text = ""
            txtFaciCelng.Enabled = False
            Label2.Enabled = False
            'lblDot3.Enabled = False
        Else
            txtFaciCelng.Enabled = True
            Label2.Enabled = True
            'lblDot3.Enabled = True
        End If
    End Sub


    Sub radioStatus()
        Dim i As Integer
        If optSupplier.Checked = False Then

            tr_Sales.Visible = False
            cmbType.SelectedIndex = 0
            txtAccCode2.Enabled = True
        Else
            tr_Sales.Visible = True
            For i = 0 To cmbType.Items.Count - 1
                If cmbType.Items(i).Value = "LIABILITY" Then
                    cmbType.SelectedIndex = i
                    Exit For
                End If
            Next
            txtAccCode2.Enabled = False
        End If
        If optCustomer.Checked Or optSupplier.Checked Then
            trCreditDays.Visible = True
        Else
            trCreditDays.Visible = False
        End If
    End Sub


    Protected Sub optCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optCustomer.CheckedChanged
        Call radioStatus()
    End Sub


    Protected Sub optNormal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optNormal.CheckedChanged
        Call radioStatus()
    End Sub


    Protected Sub cmbBankCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBankCash.SelectedIndexChanged
        Call BankCash()
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim url As String
            ViewState("datamode") = "add"
            Dim MainMnu_code As String
            MainMnu_code = Request.QueryString("MainMnu_code")

            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))


            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~/accounts/AccAddNewAccount.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))

            Response.Redirect(url)
        Catch ex As Exception
            '  lblErrorMessage.Text = "Request could not be processed "
        End Try
    End Sub


    Protected Sub LinkButton1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbSet As New LinkButton
        Dim lbSet1 As New LinkButton
        Dim txtDesc As New TextBox
        Dim txtDesc1 As New TextBox
        Dim hfDescription As New HiddenField
        Dim hfDescription1 As New HiddenField
        txtDesc = sender.parent.findcontrol("txtCode")
        txtDesc1 = sender.parent.findcontrol("txtCode1")
        lbSet1 = sender.parent.findcontrol("lbSet1")
        txtDesc1.Style.Clear()
        'txtDesc1.Style.Add("",
        hfDescription = sender.parent.findcontrol("hfDescription")
        If hfDescription.Value <> "" Then
            If hfDescription.Value.Substring(0, hfDescription.Value.Length - 1).Split("|").Length = 1 Then
                txtDesc1.Attributes.Add("style", "display:none")
                lbSet1.Attributes.Add("style", "display:none")
            Else
                lbSet1.Style.Clear()
            End If
        Else
            txtDesc1.Attributes.Add("style", "display:none")
            lbSet1.Attributes.Add("style", "display:none")
        End If


        hfDescription = sender.parent.findcontrol("hfDescription")

        hfDescription1 = sender.parent.findcontrol("hfDescription1")
        lbSet = sender
        Dim str_clientclick As String = "','" & txtDesc.ClientID & "','" & hfDescription.ClientID & "','" & txtDesc1.ClientID & "')"
        Dim str_clientclick1 As String = "','" & txtDesc1.ClientID & "','" & hfDescription1.ClientID & "','')"

        lbSet.OnClientClick = lbSet.OnClientClick.ToString.Replace("')", str_clientclick)
        lbSet1.OnClientClick = lbSet1.OnClientClick.ToString.Replace("')", str_clientclick1)

    End Sub


    Protected Sub cmbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged
        bind_RPTSETUP_M()
        FillRptDetails()
    End Sub


    Sub bind_RPTSETUP_M()
        Try
            Dim lstrSql As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            If cmbType.SelectedItem.Value.ToLower = "expenses" Or cmbType.SelectedItem.Value.ToLower = "income" Then
                'lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP = 'p')"
                lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP <> 'B' AND TYP<>'C')"
            Else
                'lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP = 'B')"
                lstrSql = "SELECT     RSM_ID, RSM_TYP, RSM_DESCR, TYP FROM RPTSETUP_M WHERE (TYP <> 'P' AND TYP<>'C')"
            End If
            gvRPTSETUP_M.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)

            gvRPTSETUP_M.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Sub FillRptDetails()
        Dim strSql As String
        Dim ds As New DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If txtAccCode.Text <> "" And txtAccCode2.Text <> "" Then
            strSql = "SELECT RPTSETUP_M.RSM_DESCR, RPTSETUP_S.RSS_CODE,RPTSETUP_S.RSS_DESCR " _
                      & " FROM   RPTSETUP_M INNER JOIN " _
                      & "RPTSETUP_S ON RPTSETUP_M.RSM_TYP = RPTSETUP_S.RSS_TYP INNER JOIN " _
                      & " RPTSETUP_S_ACC ON RPTSETUP_S.RSS_TYP = RPTSETUP_S_ACC.RPA_RSS_TYP AND " _
                      & " RPTSETUP_S.RSS_CODE = RPTSETUP_S_ACC.RPA_RSS_CODE " _
                      & " WHERE RPTSETUP_S_ACC.RPA_ACC_ID='" & txtAccCode.Text & txtAccCode2.Text & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvRptDetails.DataSource = ds
                gvRptDetails.DataBind()
            Else
                gvRptDetails.DataSource = Nothing
                gvRptDetails.DataBind()
            End If

        End If
    End Sub
    Sub FillLinkedAccounts()
        Dim strSql As String
        Dim ds As New DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        If txtAccCode.Text <> "" And txtAccCode2.Text <> "" Then
            strSql = " select a.ASM_ID,a.ASM_NAME ,case when sl.ASL_bDefault=1 then 'YES' else 'NO' end IsDefault "
            strSql = strSql & " from   ACCOUNTS_SUB_ACC_M a INNER JOIN ACCOUNTS_SUB_LINK sl"
            strSql = strSql & " on a.ASM_ID=sl.ASL_ASM_ID and sl.ASL_ACT_ID='" & txtAccCode.Text & txtAccCode2.Text & "'"
            strSql = strSql & " order by sl.ASL_bDefault desc "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvlinkedAcc.DataSource = ds
                gvlinkedAcc.DataBind()
            Else
                gvlinkedAcc.DataSource = Nothing
                gvlinkedAcc.DataBind()
            End If

        End If
    End Sub

    Protected Sub txtAccDescr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillRptDetails()
    End Sub

    Protected Sub LinkButton2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbSubAcc As New LinkButton
        Dim txtSub As New TextBox
        Dim hfSubDescription As New HiddenField
        txtSub = sender.parent.findcontrol("txtCode2")
        txtSub.Style.Clear()
        hfSubDescription = sender.parent.findcontrol("hfDescription")
        lbSubAcc = sender

        If hfSubDescription.Value <> "" Then
            If hfSubDescription.Value.Substring(0, hfSubDescription.Value.Length - 1).Split("|").Length = 1 Then
                txtSub.Attributes.Add("style", "display:none")

                'Else
                '    lbSet1.Style.Clear()
            End If
        Else
            txtSub.Attributes.Add("style", "display:none")
            'lbSet1.Attributes.Add("style", "display:none")
        End If
        
        Dim str_clientclick2 As String = "','" & txtSub.ClientID & "','" & hfSubDescription.ClientID & "','')"
        lbSubAcc.OnClientClick = lbSubAcc.OnClientClick.ToString.Replace("')", str_clientclick2)
    End Sub
    Private Function CheckSubAccount(ByVal RssType As String, ByVal RssCode As String) As Boolean
        Dim strSQL As String
        Dim dsSub As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        strSQL = "SELECT RSB_ID,RSB_DESCRIPTION FROM RPTSETUPSUB_S WHERE RSB_RSS_TYP='" & RssType & "' AND RSB_RSS_CODE='" & RssCode & "'"
        dsSub = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsSub.Tables(0).Rows.Count > 0 Then
            CheckSubAccount = True
        Else
            CheckSubAccount = False
        End If
    End Function

    Protected Sub gvLinkedAcc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLinkedAcc.RowDataBound
        Try
            Dim cmdCol As Integer = gvLinkedAcc.Columns.Count - 1
            Dim row As GridViewRow = e.Row
            Dim hlCEdit As New HyperLink
            Dim lblGrpid As New Label
            lblGrpid = TryCast(row.FindControl("lblASMID"), Label)
            hlCEdit = TryCast(row.FindControl("hlEdit"), HyperLink)
            If hlCEdit IsNot Nothing And lblGrpid IsNot Nothing Then
                'If hlCButton.ID = "hlChange" Then
                Dim i As New Encryption64
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlCEdit.NavigateUrl = "accNewSubLedger.aspx?editid=" & i.Encrypt(lblGrpid.Text) & "&MainMnu_code=" & i.Encrypt("A100085") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
