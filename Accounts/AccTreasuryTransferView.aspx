<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccTreasuryTransferView.aspx.vb" Inherits="Accounts_AccTreasuryTransferView" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        Sys.Application.add_load(
                       function CheckForPrint() {
                           if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                               document.getElementById('<%= h_print.ClientID %>').value = '';
                               radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
                           }
                       }
                    );

                       function autoSizeWithCalendar(oWindow) {
                           var iframe = oWindow.get_contentFrame();
                           var body = iframe.contentWindow.document.body;
                           var height = body.scrollHeight;
                           var width = body.scrollWidth;
                           var iframeBounds = $telerik.getBounds(iframe);
                           var heightDelta = height - iframeBounds.height;
                           var widthDelta = width - iframeBounds.width;
                           if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                           if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                           oWindow.center();
                       }

        <%--function getpdfView()
 {
    alert('ddd');
   var hidPrint =document.getElementById('<%=h_print%>').value
   alert(hidPrint);
   if (hidPrint == "print")
  
  radopen('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
 //showModelessDialog
 } --%>


   
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="700px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Treasury transfer
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0"
                    width="100%">
                    <tr>
                        <td valign="top" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <a id='top'></a></td>
                        <td align="right">
                            <asp:RadioButton ID="rbUnposted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Open" Checked="True" />
                            <asp:RadioButton ID="rbPosted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Posted" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="All" /><asp:LinkButton ID="LinkPDF" runat="server" OnClick="LinkPDF_Click">PDF View</asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" valign="middle" width="20%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td width="80%"></td>
                    </tr>
                    <tr valign="top">
                        <td align="center" width="100%" colspan="2">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row"
                                OnSelectedIndexChanged="gvJournal_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField SortExpression="ITF_DOCNO" HeaderText="Document No">
                                        <HeaderTemplate>
                                            Document No
                                <br />
                                            <asp:TextBox ID="txtDocno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocno" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label22" runat="server" Text='<%# Bind("ITF_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ITF_DATE" HeaderText="Document Date">
                                        <HeaderTemplate>
                                            Date
                                <br />
                                            <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("ITF_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ITF_NARRATION" HeaderText="Narration">
                                        <HeaderTemplate>
                                            Narration
                                <br />
                                            <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("ITF_NARRATION") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payed Unit">
                                        <HeaderTemplate>
                                            Paid by
                                <br />
                                            <asp:TextBox ID="txtPayedunit" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PAYEDUNIT") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Received Unit">
                                        <HeaderTemplate>
                                            Received by<br />
                                            <asp:TextBox ID="txtReceivedunite" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("RECEIVEDUNIT") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount
                                         <br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("ITF_AMOUNT")) %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ITF_bPOSTED" HeaderText="Posted">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("ITF_bPOSTED")) %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>

                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="IRF_ID" Visible="False" HeaderText="IRF_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("IRF_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
