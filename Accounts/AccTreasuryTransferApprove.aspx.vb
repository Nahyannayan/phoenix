Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_AccTreasuryTransferApprove
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            '''''check menu rights
            viewstate("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200310"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            Page.Title = OASISConstants.Gemstitle
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351") Then 'Or (ViewState("MainMnu_code") <> "A200351") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Page.Title = OASISConstants.Gemstitle
                If ViewState("MainMnu_code") = "A200310" Then
                    lblHead.Text = "APPROVE TREASURY TRANSFER"
                ElseIf ViewState("MainMnu_code") = "A200351" Then
                    lblHead.Text = "POSTING TREASURY TRANSFER"
                End If

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
                '''''check menu rights

                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            End If
    End Sub


    


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            'hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            'If chkPost IsNot Nothing Then
            'If chkPost.Checked = True Then
            'chkPost.Disabled = True
            'Else
            If lblGUID IsNot Nothing Then
                ' Dim i As New Encryption64
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If ViewState("MainMnu_code") = "A200310" Then
                    hlview.NavigateUrl = "AccTreasuryTransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                ElseIf ViewState("MainMnu_code") = "A200351" Then
                    hlview.NavigateUrl = "AccTreasuryTransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")

                End If
                'hlCEdit.Enabled = True
            End If
            'End If
            'End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    

    Private Function findPostvalue() As String
        Dim postId As String = ""
        Dim chk As HtmlInputCheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            chk = DirectCast(grow.FindControl("chkPosted"), HtmlInputCheckBox)
            If chk.Checked = True Then 'And chk.Disabled = False Then


                If ViewState("MainMnu_code").Equals("A200310") Then
                    postId = doPost(chk.Value.ToString, "APPROVE")
                    'postId = doPostNext(chk.Value.ToString, "APPROVE")
                ElseIf ViewState("MainMnu_code").Equals("A200351") Then
                    postId = doPost(chk.Value.ToString, "POST")
                    'postId = doPostNext(chk.Value.ToString, "POST")
                End If

            End If


        Next
        Return postId
    End Function

    Private Function doPostNext(ByVal PostValue As String, ByVal Mode As String) As String
       

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "postINTERUNIT_TRANSFER"
        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection.Open()
        Try

            cmd.Parameters.Add("@MODE", SqlDbType.VarChar).Value = Mode
            cmd.Parameters.Add("@VHH_SUB_ID", SqlDbType.VarChar).Value = Session("Sub_ID").ToString()
            cmd.Parameters.Add("@VHH_BSU_ID", SqlDbType.VarChar).Value = Session("sBsuid").ToString()
            cmd.Parameters.Add("@VHH_FYEAR", SqlDbType.Int).Value = Int32.Parse(Session("F_YEAR").ToString())
            cmd.Parameters.Add("@VHH_SESSIONID", SqlDbType.VarChar).Value = Session.SessionID.ToString()
            cmd.Parameters.Add("@IRF_ID", SqlDbType.Int).Value = Int32.Parse(PostValue.ToString())
            cmd.Parameters.Add("@AUD_WINUSER", SqlDbType.VarChar).Value = Page.User.Identity.Name.ToString()
            cmd.Parameters.Add("@Aud_form", SqlDbType.VarChar).Value = Master.MenuName.ToString()
            cmd.Parameters.Add("@Aud_user", SqlDbType.VarChar).Value = HttpContext.Current.Session("sUsr_name").ToString()
            cmd.Parameters.Add("@Aud_module", SqlDbType.VarChar).Value = HttpContext.Current.Session("sModule")
            cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Value = 0
            cmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar).Value = "0"
            cmd.Parameters.Add("@v_ReturnMsg", SqlDbType.VarChar, 1000).Value = "0"
            'cmd.Parameters("@ReturnValue").Direction = ParameterDirection.Output
            cmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            cmd.Parameters("@v_ReturnMsg").Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()


            Return cmd.Parameters("@v_ReturnMsg").Value
            cmd.Connection.Close()
            cmd.Connection.Dispose()
        Catch ex As Exception

            cmd.Connection.Close()
            cmd.Connection.Dispose()
            Return ex.Message
        End Try
        'objConn.Close()


        Return cmd.Parameters("@v_ReturnMsg").Value
    End Function

   

    Private Function doPost(ByVal PostValue As String, ByVal Mode As String) As String
        Dim MnuName = Master.MenuName
        If MnuName = Nothing Then
            MnuName = ViewState("MainMnu_code")
        End If
        Dim _parameter As String(,) = New String(12, 1) {}
        _parameter(0, 0) = "@MODE"
        _parameter(0, 1) = Mode
        _parameter(1, 0) = "@VHH_SUB_ID"
        _parameter(1, 1) = Session("Sub_ID").ToString()
        _parameter(2, 0) = "@VHH_BSU_ID"
        _parameter(2, 1) = Session("sBsuid").ToString()
        _parameter(3, 0) = "@VHH_FYEAR"
        _parameter(3, 1) = Session("F_YEAR").ToString()
        _parameter(4, 0) = "@VHH_SESSIONID"
        _parameter(4, 1) = Session.SessionID.ToString()
        _parameter(5, 0) = "@IRF_ID"
        _parameter(5, 1) = PostValue
        _parameter(6, 0) = "@AUD_WINUSER"
        _parameter(6, 1) = Page.User.Identity.Name.ToString()
        _parameter(7, 0) = "@Aud_form"
        _parameter(7, 1) = MnuName 'ViewState("MainMnu_code") 'Master.MenuName.ToString()
        _parameter(8, 0) = "@Aud_user"
        _parameter(8, 1) = HttpContext.Current.Session("sUsr_name").ToString()
        _parameter(9, 0) = "@Aud_module"
        _parameter(9, 1) = HttpContext.Current.Session("sModule")
        _parameter(10, 0) = "@ReturnValue"
        _parameter(10, 1) = "0"
        _parameter(11, 0) = "@VHH_NEWDOCNO"
        _parameter(11, 1) = ""
        ' _parameter(12, 0) = "@v_ReturnMsg"
        ' _parameter(12, 1) = ""


        MainObj.doExcutive("postINTERUNIT_TRANSFER", _parameter, "mainDB", "@v_ReturnMsg")
        'MainObj.doExcutive("postINTERUNIT_TRANSFER", _parameter, "mainDB")
        If chkPrint.Checked Then
            If ViewState("MainMnu_code") = "A200351" Then
                CheckprintType(Int32.Parse(PostValue))
            Else
                PrintVoucher(Int32.Parse(PostValue))
            End If

            chkPrint.Checked = False
        End If
        Return MainObj.MESSAGE
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            Dim retMsg As String = findPostvalue()
            lblError.Text = retMsg
            gridbind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub gridbind()
        Try
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = ""

            If (ViewState("MainMnu_code") = "A200310") Then ' for Approve
                str_Sql = " SELECT " _
          & " INTERUNIT_TRANSFER.IRF_ID, " _
          & " INTERUNIT_TRANSFER.ITF_DATE, " _
          & " vw_OSO_BUSINESSUNIT_M.BSU_NAME AS RECEIVEDUNIT, " _
          & " vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS PAYEDUNIT," _
          & " INTERUNIT_TRANSFER.ITF_NARRATION, " _
          & " INTERUNIT_TRANSFER.ITF_AMOUNT, " _
          & " INTERUNIT_TRANSFER.ITF_bPOSTED,INTERUNIT_TRANSFER.ITF_CUR_ID" _
          & " FROM " _
          & " INTERUNIT_TRANSFER INNER JOIN " _
          & " vw_OSO_BUSINESSUNIT_M ON INTERUNIT_TRANSFER.ITF_DRBSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
          & " vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON  " _
          & " INTERUNIT_TRANSFER.ITF_CRBSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID WHERE INTERUNIT_TRANSFER.ITF_bPOSTED = 0 " _
          & " AND ITF_FYEAR = " & Session("F_YEAR") & " AND ITF_bDELETED = 0 AND ITF_BSU_ID = '" & Session("sBsuid") & "' ORDER BY ITF_DATE DESC "

            ElseIf (ViewState("MainMnu_code") = "A200351") Then ' fro transfer posting
                str_Sql = " SELECT " _
         & " INTERUNIT_TRANSFER.IRF_ID, " _
         & " INTERUNIT_TRANSFER.ITF_DATE, " _
         & " vw_OSO_BUSINESSUNIT_M.BSU_NAME AS RECEIVEDUNIT, " _
         & " vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS PAYEDUNIT," _
         & " INTERUNIT_TRANSFER.ITF_NARRATION, " _
         & " INTERUNIT_TRANSFER.ITF_AMOUNT, " _
         & " INTERUNIT_TRANSFER.ITF_bPOSTED,INTERUNIT_TRANSFER.ITF_CUR_ID" _
         & " FROM " _
         & " INTERUNIT_TRANSFER INNER JOIN " _
         & " vw_OSO_BUSINESSUNIT_M ON INTERUNIT_TRANSFER.ITF_DRBSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
         & " vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON  " _
         & " INTERUNIT_TRANSFER.ITF_CRBSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID " _
         & " WHERE INTERUNIT_TRANSFER.ITF_bPOSTED = 1 AND ITF_FYEAR = " & Session("F_YEAR") & " AND ITF_bDELETED = 0 " _
         & " AND ((INTERUNIT_TRANSFER.ITF_DRBSU_ID =  '" & Session("sBsuid") & "' AND INTERUNIT_TRANSFER.ITF_bPOSTEDDRBSU = 0 )" _
         & " OR (INTERUNIT_TRANSFER.ITF_CRBSU_ID =  '" & Session("sBsuid") & "' AND INTERUNIT_TRANSFER.ITF_bPOSTEDCRBSU = 0)) " _
         & " ORDER BY ITF_DATE DESC "
            End If
            Dim ds As New DataTable
            ds = MainObj.ListRecords(str_Sql, "mainDB")
            gvJournal.DataSource = ds
            If ds.Rows.Count > 0 Then
                btnPost.Visible = True
                chkPrint.Visible = True
            Else
                btnPost.Visible = False
                chkPrint.Visible = False
            End If
            gvJournal.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub PrintVoucher(ByVal printId As Int32)
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "PrintTreasuryTransfer"
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            cmd.Parameters.Add("@IRFID", SqlDbType.Int).Value = printId
            cmd.Parameters.Add("@BUSID", SqlDbType.VarChar).Value = Session("sBsuid").ToString()

            params("userName") = Session("sUsr_name")
            params("VoucherName") = "FUND TRANSFER MEMO"
            params("reportHeading") = "FUND TRANSFER MEMO"
            repSource.Parameter = params
            repSource.VoucherName = "FUND TRANSFER MEMO"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptTreasuryTransferReport.rpt"
            Session("ReportSource") = repSource
            h_print.Value = "print"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Public Sub CheckprintType(ByVal printId As Int32)
        Dim sqlQuery As String
        Dim _table As DataTable
        sqlQuery = " SELECT CASE  WHEN ITF_DRBSU_ID = '" & Session("sBsuid") & "' THEN   'BR' " _
        & " ELSE   'BP' END AS VHHDOCTYPE, CASE  WHEN ITF_DRBSU_ID = '" & Session("sBsuid") & "' THEN  ITF_DR_DOCNO " _
        & " ELSE  ITF_CR_DOCNO END AS VHHDOCNO FROM INTERUNIT_TRANSFER WHERE IRF_ID = " & printId
        _table = MainObj.ListRecords(sqlQuery, "MainDB")
        If _table.Rows.Count > 0 Then
            If (_table.Rows(0)("VHHDOCTYPE").Equals("BP")) Then
                Print_BP(_table.Rows(0)("VHHDOCNO").ToString())
            Else
                Print_BR(_table.Rows(0)("VHHDOCNO").ToString())
            End If
        End If

    End Sub
    Protected Sub Print_BR(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BR", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub
    Protected Sub Print_BP(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", p_Docno, False, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub
End Class


