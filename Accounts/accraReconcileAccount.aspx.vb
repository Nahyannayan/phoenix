Imports System.Data.DataSet
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class Accounts_accraReconcileAccount
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("viewid") Is Nothing Then h_entryId = Request.QueryString("viewid")
        If Not Request.QueryString("RCD_DOCNO") Is Nothing Then h_entryId = Request.QueryString("viewid")
        If Page.IsPostBack = False Then
            If h_entryId Is Nothing Then
                Session("liUserList") = New List(Of String)
                txtHDocdate.Text = GetDiplayDate()
                txtHDocno.Text = AccountFunctions.GetNextDocId("REC", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                'txtHAccountcode.Attributes.Add("readonly", "readonly")
                bind_Check_date_AndBind()
                Page.Title = OASISConstants.Gemstitle
                btnAdd.Visible = False
                btnDelete.Visible = False
            Else
                Page.Title = OASISConstants.Gemstitle
                'tblDetails.Visible = True
                btnHaccount.Visible = False
                btnHdate.Visible = False
                txtHDocdate.Enabled = False
                txtHAccountcode.Enabled = False
                txtHAccountname.Enabled = False
                btnAdd.Visible = False
                gridbind1(Encr_decrData.Decrypt(h_entryId.Replace(" ", "+")))
                txtHTotal.Text = AccountFunctions.Round(AccountFunctions.GetLedgerBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text))
                txtRBalance.Text = AccountFunctions.Round(AccountFunctions.GetReconclileBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text))
            End If
        End If
    End Sub

    Public Function returnpath(ByVal p_Date As String) As String
        Try
            If p_Date <> "" Then
                Dim STR_DATE As String = String.Format("{0:dd/MMM/yyyy}", CDate(p_Date))
                If STR_DATE = "01/Jan/1900" Then
                    Return ""
                Else
                    Return STR_DATE
                End If
            End If
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function returndate(ByVal p_date As String) As String
        Try
            Dim dt As Date = CDate(p_date)
            Dim dt_1900 As Date = CDate("01/01/1900")
            If dt = dt_1900 Then
                Return ""
            Else
                Return Format(dt, "dd/MMM/yyyy")
            End If

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Sub gridbind(Optional ByVal p_date As String = "")
        Try
            If p_date = "" Then
                p_date = txtHDocdate.Text
            End If
            If txtHAccountcode.Text = "" Then Exit Sub
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String 'ACT_RECONFORMAT

            str_Sql = "SELECT * FROM (SELECT JOURNAL_D.GUID," _
           & " JOURNAL_D.JNL_ID, JOURNAL_D.JNL_SUB_ID, " _
           & " JOURNAL_D.JNL_BSU_ID, JOURNAL_D.JNL_FYEAR," _
           & " JOURNAL_D.JNL_DOCTYPE, JOURNAL_D.JNL_DOCNO," _
           & " JOURNAL_D.JNL_CUR_ID, JOURNAL_D.JNL_EXGRATE1," _
           & " JOURNAL_D.JNL_EXGRATE2, JOURNAL_D.JNL_DOCDT," _
           & " JOURNAL_D.JNL_ACT_ID, cast(case when isnull(RCD_JNL_DEBIT,0)<>0 then JOURNAL_D.JNL_DEBIT-isnull(RCD_JNL_DEBIT,JOURNAL_D.JNL_DEBIT) * JOURNAL_D.JNL_EXGRATE1 else isnull(RCD_JNL_DEBIT,JOURNAL_D.JNL_DEBIT) * JOURNAL_D.JNL_EXGRATE1 end as decimal(12,3)) JNL_DEBIT," _
           & " cast(case when isnull(RCD_JNL_CREDIT,0)<>0 then JOURNAL_D.JNL_CREDIT-isnull(RCD_JNL_CREDIT,JOURNAL_D.JNL_CREDIT) * JOURNAL_D.JNL_EXGRATE1 else isnull(RCD_JNL_CREDIT,JOURNAL_D.JNL_CREDIT) * JOURNAL_D.JNL_EXGRATE1 end as decimal(12,3)) JNL_CREDIT, JOURNAL_D.JNL_NARRATION, " _
           & " JOURNAL_D.JNL_RECONDT, JOURNAL_D.JNL_CHQNO," _
           & " JOURNAL_D.JNL_REFDOCTYPE, JOURNAL_D.JNL_REFDOCNO," _
           & " JOURNAL_D.JNL_BDISCOUNTED, JOURNAL_D.JNL_BDELETED, " _
           & " JOURNAL_D.JNL_SLNO, JOURNAL_D.JNL_RSS_ID, JOURNAL_D.JNL_OPP_ACT_ID," _
           & " '" & txtHDocdate.Text & "' AS RCD_RECONDT, " _
           & " CASE WHEN " _
           & " ISNULL(JOURNAL_D.JNL_CHQDT, '1/JAN/1900') = '1/JAN/1900'" _
           & " THEN JOURNAL_D.JNL_DOCDT" _
           & " ELSE JOURNAL_D.JNL_CHQDT END" _
           & " AS JNL_CHQDT, RCD_JNL_ID," _
           & " 0 RCD_bTemp," _
           & " (ACCOUNTS_M_1.ACT_ID+' - '+ ACCOUNTS_M_1.ACT_NAME)" _
           & " AS OPP_ACT_NAME" _
           & " FROM  JOURNAL_D INNER JOIN ACCOUNTS_M " _
           & " ON JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID " _
           & " LEFT OUTER JOIN (select sum(RCD_JNL_DEBIT) RCD_JNL_DEBIT, sum(RCD_JNL_CREDIT) RCD_JNL_CREDIT, RCD_JNL_ID from RECONCILIATION_D group by RCD_JNL_ID) RCD ON" _
           & " JOURNAL_D.JNL_ID = RCD_JNL_ID " _
           & " LEFT OUTER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 " _
            & " ON JOURNAL_D.JNL_OPP_ACT_ID = ACCOUNTS_M_1.ACT_ID" _
            & " WHERE (ACCOUNTS_M.ACT_BANKCASH = 'B')" _
            & " AND (JOURNAL_D.JNL_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND ((ISNULL(JOURNAL_D.JNL_RECONDT, '') = '') or JOURNAL_D.JNL_DEBIT <> RCD_JNL_DEBIT or  JOURNAL_D.JNL_CREDIT <> RCD_JNL_CREDIT) " _
            & " AND (JOURNAL_D.JNL_ACT_ID = '" & txtHAccountcode.Text & "') " _
            & " AND (JOURNAL_D.JNL_DOCDT <= '" & p_date & "')" _
            & " ) A where (JNL_DEBIT+JNL_CREDIT)>0 order by JNL_DOCNO, JNL_CHQDT "

            '& " UNION ALL " _
            '& " select null, RCN_ID, null, RCN_BSU_ID, null, 'BS', RCN_DOCNO, '', 1, 1, RCN_DATE, RCN_ACT_ID, RCN_DEPOSIT, RCN_PAYMENT, RCN_NARRATION, " _
            '& " NULL,RCN_CHQNO,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'" & txtHDocdate.Text & "',RCN_DATE,NULL,NULL,NULL FROM RECONCILIATION_N " _
            '& " where RCN_DELETED=0 and RCN_BSU_ID='" & Session("sBsuid") & "' and RCN_ACT_ID='" & txtHAccountcode.Text & "' and RCN_RECONDT is null " _

            '&"AND ISNULL(RECONCILIATION_D.RCD_bTemp,0)=1"
                '& " AND ((ISNULL(JOURNAL_D.JNL_RECONDT, '') = '') 
            recoTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql).Tables(0)
            gvReconcile.DataSource = recoTable
            gvReconcile.DataBind()
            getBankBalance()
            Try
                For Each gvr As GridViewRow In gvReconcile.Rows
                    Dim txtPayment As TextBox = CType(gvr.FindControl("txtPayment"), TextBox)
                    Dim txtDeposit As TextBox = CType(gvr.FindControl("txtDeposit"), TextBox)
                    Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
                    Dim chkSelect As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)

                    If txtPayment IsNot Nothing Then
                        ClientScript.RegisterArrayDeclaration("TextBoxPayment", String.Concat("'", txtPayment.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextBoxDeposit", String.Concat("'", txtDeposit.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("CheckboxSelect", String.Concat("'", chkSelect.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextBoxDate", String.Concat("'", txtRcdt.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextDate", String.Concat("'", gvr.Cells(5).Text, "'"))
                        'txtRCDate()
                    End If
                Next
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Property h_entryId() As String
        Get
            Return ViewState("h_entryId")
        End Get
        Set(ByVal value As String)
            ViewState("h_entryId") = value
        End Set
    End Property

    Private Property recoTable() As DataTable
        Get
            Return ViewState("recoTable")
        End Get
        Set(ByVal value As DataTable)
            ViewState("recoTable") = value
        End Set
    End Property

    Sub gridbind1(Optional ByVal RCDDOCNO As String = "")
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            txtHDocno.Text = RCDDOCNO
            Dim returnValues() As String = Mainclass.getDataValue("select top 1 replace(convert(varchar(30),rcd_docdt,106),' ','/')+';'+RCD_JNL_ACT_ID+';'+act_name from RECONCILIATION_D inner join accounts_m on act_id=RCD_JNL_ACT_ID where RCD_BSU_ID='" & Session("sBsuid") & "' and RCD_DOCNO ='" & RCDDOCNO & "'", "MainDB").Split(";")
            txtHDocdate.Text = returnValues(0)
            txtHAccountcode.Text = returnValues(1) ' Mainclass.getDataValue(" select top 1 RCD_JNL_ACT_ID from RECONCILIATION_D  where RCD_DOCNO ='" & RCDDOCNO & "'", "MainDB")
            txtHAccountname.Text = returnValues(2) ' Mainclass.getDataValue("select act_name from ACCOUNTS_M where act_ID='" & Trim(txtHAccountcode.Text) & "'", "MainDB")

            Dim str_Sql As String
            str_Sql = "select RECONCILIATION_D.guid,rcd_jnl_id as JNL_ID,RCD_JNL_SUB_ID,RCD_JNL_BSU_ID,RCD_JNL_FYEAR,RCD_JNL_DOCTYPE as JNL_DOCTYPE,RCD_JNL_DOCNO as JNL_DOCNO,RCD_JNL_CUR_ID,RCD_JNL_EXGRATE1,RCD_JNL_EXGRATE2 " _
           & ",RCD_JNL_DOCDT,RCD_JNL_ACT_ID,RCD_JNL_DEBIT as JNL_DEBIT,RCD_JNL_CREDIT as JNL_CREDIT,RCD_JNL_NARRATION as JNL_NARRATION,RCD_RECONDT,RCD_JNL_CHQNO as JNL_CHQNO,'' as JNL_REFDOCTYPE,'' as JNL_REFDOCNO," _
           & "0,0 ,RCD_JNL_SLNO,'' as JNL_RSS_ID,'' as JNL_OPP_ACT_ID,ISNULL(RECONCILIATION_D.RCD_RECONDT, '') AS RCD_RECONDT," _
           & "CASE WHEN ISNULL(RCD_jnL_CHQDT, '1/JAN/1900') = '1/JAN/1900' THEN RCD_JNL_DOCDt ELSE RCD_JNL_CHQDT END " _
           & "AS JNL_CHQDT, RCD_JNL_ID,ISNULL(RCD_bTemp, 0) AS RCD_bTemp,ACT_NAME AS OPP_ACT_NAME  from RECONCILIATION_D " _
           & "INNER JOIN JOURNAL_D on JNL_ID = RCD_JNL_ID LEFT OUTER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 ON JNL_OPP_ACT_ID = ACCOUNTS_M_1.ACT_ID " _
           & " WHERE RCD_BSU_ID='" & Session("sBsuid") & "' and RCD_DOCNO= '" & RCDDOCNO & "'"

            '& " UNION ALL " _
            '& " select null, RCN_ID, null, RCN_BSU_ID, null, 'BS', RCN_DOCNO, '', 1, 1, isnull(RCN_RECONDT,RCN_DATE), RCN_ACT_ID, RCN_DEPOSIT, RCN_PAYMENT, RCN_NARRATION, " _
            '& " RCN_DATE,RCN_CHQNO,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1900-01-01 00:00:00.000',RCN_DATE,NULL,0,'' FROM RECONCILIATION_N " _
            '& " where RCN_DELETED=0 and RCN_BSU_ID='" & Session("sBsuid") & "' and RCN_ACT_ID='" & txtHAccountcode.Text & "' " _
            '& " and (RCN_RCD_DOCNO='" & RCDDOCNO & "' or isnull(RCN_RCD_DOCNO_RECON,'')='" & RCDDOCNO & "') "

            'str_Sql = "select RECONCILIATION_D.guid,rcd_jnl_id as JNL_ID,RCD_JNL_SUB_ID,RCD_JNL_BSU_ID,RCD_JNL_FYEAR,RCD_JNL_DOCTYPE as JNL_DOCTYPE,RCD_JNL_DOCNO as JNL_DOCNO,RCD_JNL_CUR_ID,RCD_JNL_EXGRATE1,RCD_JNL_EXGRATE2 " _
            '& ",RCD_JNL_DOCDT,RCD_JNL_ACT_ID,RCD_JNL_DEBIT as JNL_DEBIT,RCD_JNL_CREDIT as JNL_CREDIT,RCD_JNL_NARRATION as JNL_NARRATION,RCD_RECONDT,RCD_JNL_CHQNO as JNL_CHQNO,'' as JNL_REFDOCTYPE,'' as JNL_REFDOCNO," _
            '& "0,0 ,RCD_JNL_SLNO,'' as JNL_RSS_ID,'' as JNL_OPP_ACT_ID,ISNULL(RECONCILIATION_D.RCD_RECONDT, '') AS RCD_RECONDT," _
            '& "CASE WHEN ISNULL(RCD_jnL_CHQDT, '1/JAN/1900') = '1/JAN/1900' THEN RCD_JNL_DOCDt ELSE RCD_JNL_CHQDT END " _
            '& "AS JNL_CHQDT, RCD_JNL_ID,ISNULL(RCD_bTemp, 0) AS RCD_bTemp,ACT_NAME AS OPP_ACT_NAME  from RECONCILIATION_D " _
            '& "INNER JOIN JOURNAL_D on JNL_ID = RCD_JNL_ID LEFT OUTER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 ON JNL_OPP_ACT_ID = ACCOUNTS_M_1.ACT_ID " _
            '& " WHERE RCD_BSU_ID='" & Session("sBsuid") & "' and RCD_DOCNO= '" & RCDDOCNO & "'"
            recoTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql).Tables(0)
            gvReconcile.DataSource = recoTable
            gvReconcile.DataBind()
            getBankBalance()
            Try
                For Each gvr As GridViewRow In gvReconcile.Rows
                    Dim txtPayment As TextBox = CType(gvr.FindControl("txtPayment"), TextBox)
                    Dim txtDeposit As TextBox = CType(gvr.FindControl("txtDeposit"), TextBox)
                    Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
                    Dim chkSelect As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)

                    If txtPayment IsNot Nothing Then
                        ClientScript.RegisterArrayDeclaration("TextBoxPayment", String.Concat("'", txtPayment.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextBoxDeposit", String.Concat("'", txtDeposit.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("CheckboxSelect", String.Concat("'", chkSelect.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextBoxDate", String.Concat("'", txtRcdt.ClientID, "'"))
                        ClientScript.RegisterArrayDeclaration("TextDate", String.Concat("'", gvr.Cells(5).Text, "'"))
                    End If
                Next
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btView.Click
        Dim intFileNameLength As Integer
        Dim strFileNamePath As String = ""
        Dim strFileNameOnly As String = ""

        If Not (FileUpload1.PostedFile Is Nothing) Then
            strFileNamePath = FileUpload1.PostedFile.FileName

            intFileNameLength = InStr(1, StrReverse(strFileNamePath), "\")

            strFileNameOnly = Mid(strFileNamePath, (Len(strFileNamePath) - intFileNameLength) + 2)
            Dim paths As String = Server.MapPath("") & "\Uploads\Excel"

            paths = paths & "\"

            'If File.Exists(paths & strFileNameOnly) Then
            'lblMessage.Text = "Image of Similar name already Exist,Choose other name"
            'Else
            If FileUpload1.PostedFile.ContentLength > 40000 Then
                lblError.Text = "The Size of file is greater than 4 MB"
            ElseIf strFileNameOnly = "" Then
                Exit Sub
            Else
                strFileNameOnly = Session("sUsr_name") & "-" & Session("sBsuid") & "-" & Format(Date.Today, "dd/MMM/yyyy").Replace("/", "-") & "-" & AccountFunctions.GetRandomString() & ".xls"
                FileUpload1.PostedFile.SaveAs(paths & strFileNameOnly)
                lblError.Text = "File Upload Success."
                Session("Img") = strFileNameOnly
            End If
        End If
        'End If

        Dim myDataset As New DataSet()
        Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & Server.MapPath("") & "\Uploads\Excel\" & strFileNameOnly & ";" & _
        "Extended Properties=Excel 8.0;"

        ''You must use the $ after the object you reference in the spreadsheet
        Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("") & "\Uploads\Excel\" & strFileNameOnly) & "]", strConn)
        myData.TableMappings.Add("Table", "ExcelTest")
        myData.Fill(myDataset)

        GridView1.DataSource = myDataset.Tables(0)

        Dim i As Integer = 0
        ''''''''''''''
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String 'ACT_RECONFORMAT
        str_Sql = " SELECT " _
          & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME," _
          & " ACCOUNTS_M.ACT_RECONFORMAT, ACCOUNTSFORMAT_S.AFT_INST_NO," _
          & " ACCOUNTSFORMAT_S.AFT_NARRATION, ACCOUNTSFORMAT_S.AFT_DATE," _
          & " ACCOUNTSFORMAT_S.AFT_DEBIT, ACCOUNTSFORMAT_S.AFT_CREDIT" _
          & " FROM ACCOUNTS_M INNER JOIN ACCOUNTSFORMAT_S" _
          & " ON ACCOUNTS_M.ACT_RECONFORMAT = ACCOUNTSFORMAT_S.AFT_FORMAT" _
          & " WHERE ACCOUNTS_M.ACT_ID='" & txtHAccountcode.Text & "'"

        '& " order by gm.GPM_DESCR "
        Dim ds As New DataSet
        Dim i_chqno, i_narration, i_debit, i_credit, i_date As Integer
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then ' get the fields to map
            i_chqno = ds.Tables(0).Rows(0)("AFT_INST_NO")
            i_narration = ds.Tables(0).Rows(0)("AFT_NARRATION")
            i_debit = ds.Tables(0).Rows(0)("AFT_DEBIT")
            i_credit = ds.Tables(0).Rows(0)("AFT_CREDIT")
            i_date = ds.Tables(0).Rows(0)("AFT_DATE")
        Else
            GridView1.DataBind()
            Exit Sub
        End If
        gvReconcile.DataSource = ds.Tables(0)

        'gvReconcile.DataBind()

        For Each gvr As GridViewRow In gvReconcile.Rows
            'need chqno narration amount(dep/pay)
            Dim str_chqno As String = gvr.Cells(4).Text.Replace("&nbsp;", "")
            Dim str_narration As String = gvr.Cells(3).Text.Replace("&nbsp;", "")
            Dim str_date As String = gvr.Cells(5).Text.Replace("&nbsp;", "")
            Dim txtPayment As TextBox = CType(gvr.FindControl("txtPayment"), TextBox)
            Dim txtDeposit As TextBox = CType(gvr.FindControl("txtDeposit"), TextBox)

            Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
            Dim chkSelect As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)
            'chkSelect.Checked = True
            If txtPayment IsNot Nothing Then
                Dim bExists As Boolean = False

                'For k As Integer = 0 To .Rows.Count 'start finding
                If (str_chqno <> "") Then
                    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, _
                    txtDeposit.Text, i_debit, str_chqno, i_chqno)
                End If
                If bExists = False And str_narration <> "" Then
                    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, _
                    txtDeposit.Text, i_debit, str_narration, i_narration)
                End If 'str_date <> ""
                If bExists = False And str_date <> "" Then
                    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, _
                    txtDeposit.Text, i_debit, txtPayment.Text, i_credit)
                    'Else
                    '    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, txtDeposit.Text, i_debit, txtDeposit.Text, i_debit)
                End If
                If bExists = False And CInt(txtPayment.Text) > 0 Then
                    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, _
                    txtDeposit.Text, i_debit, txtPayment.Text, i_credit)
                    'Else
                    '    bExists = check_in_ds(myDataset.Tables(0), txtPayment.Text, i_credit, txtDeposit.Text, i_debit, txtDeposit.Text, i_debit)
                End If
                If bExists = True Then
                    chkSelect.Checked = True
                    txtRcdt.Text = gvr.Cells(5).Text & ""
                End If

                'Next
            End If
        Next

        ''''''''''''''

    End Sub

    Private Function check_in_ds(ByRef p_dt As DataTable, ByVal p_credit As String, _
    ByVal p_credit_count As Integer, ByVal p_debit As String, ByVal p_debit_count As Integer, _
    ByVal p_searchcontent As String, _
    ByVal p_columnno As Integer) As Boolean
        Try
            p_columnno = p_columnno - 1
            p_debit_count = p_debit_count - 1
            p_credit_count = p_credit_count - 1
            For i As Integer = 0 To p_dt.Rows.Count 'start finding
                If p_dt.Rows(i)(p_columnno).ToString.ToUpper.IndexOf(p_searchcontent.ToUpper) >= 0 And checkempty(p_debit) = checkempty(p_dt.Rows(i)(p_debit_count)) And checkempty(p_credit) = checkempty(p_dt.Rows(i)(p_credit_count)) Then
                    p_dt.Rows(i).Delete()
                    Return True

                End If
            Next
            Return False
        Catch ex As Exception

        End Try

    End Function

    Private Function checkempty(ByVal p_input As Object) As Double
        Try
            If p_input Is System.DBNull.Value Then
                Return 0
            Else
                Return CDbl(p_input)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function


    Private Function checkdate(ByVal p_input As String) As Date
        Try
            If p_input Is System.DBNull.Value Then
                Return CDate("01/jan/1900")
            Else
                Return CDate(p_input)
            End If
        Catch ex As Exception
            Return CDate("01/jan/1900")
        End Try
    End Function

    Protected Sub btnHaccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHaccount.Click
        bind_Check_date_AndBind()
        'tblDetails.Visible = True
    End Sub

    Private Sub bind_Check_date_AndBind()
        Try
            Dim strfDate As String = txtHDocdate.Text.Trim

            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                gridbind("01/01/1900")
                Exit Sub
            Else
                txtHDocdate.Text = strfDate

                gridbind()
                txtHAccountname.Attributes.Add("readonly", "readonly")
                txtHTotal.Text = AccountFunctions.Round(AccountFunctions.GetLedgerBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text))
                txtRBalance.Text = AccountFunctions.Round(AccountFunctions.GetReconclileBalance(Session("sBsuid"), txtHAccountcode.Text, txtHDocdate.Text))

            End If

        Catch ex As Exception
            lblError.Text = "The Date Selected is invalid"
        End Try
    End Sub


    Protected Sub btnReconcile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim strfDate As String = txtHDocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)


        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        If Encr_decrData.Decrypt(Request.QueryString("datamode")) = "add" Then
            Save_Reconcile_Account()
        Else
            Update_Reconcile_Account()
        End If

    End Sub
    Private Sub Update_Reconcile_Account(Optional ByVal p_temp As Boolean = False)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Try
            Dim rcCount As Integer = 0
            Dim docno As String = ""
            Dim str_err As String = ""
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            For Each gvr As GridViewRow In gvReconcile.Rows
                Dim chkSelect As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)
                If chkSelect IsNot Nothing Then
                    If chkSelect.Checked = True Then
                        rcCount = rcCount + 1
                        Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
                        Dim txtDepositFtr As TextBox = CType(gvr.FindControl("txtDeposit"), TextBox)
                        Dim txtPaymentFtr As TextBox = CType(gvr.FindControl("txtPayment"), TextBox)
                        Dim hdnDocType As HiddenField = CType(gvr.FindControl("hdnDocType"), HiddenField)
                        Dim txtJNLID As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)
                        Dim strRecDate As String = txtRcdt.Text
                        If strRecDate = "" Then strRecDate = txtHDocdate.Text
                        If hdnDocType.Value = "BS" Then
                            str_err = "0"
                            saveReonciliationN(txtJNLID.Value, txtDocno.Text, txtNarration.Text, txtChqNo.Text, txtDepositFtr.Text, txtPaymentFtr.Text)
                        Else
                            str_err = UpdateReconclile_D(Encr_decrData.Decrypt(h_entryId.Replace(" ", "+")), txtJNLID.Value, strRecDate, txtDepositFtr.Text, txtPaymentFtr.Text, stTrans)
                        End If

                        If str_err <> "0" Then
                            Exit For
                        End If
                    End If
                End If
            Next
            If str_err = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage(str_err)
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                'Response.Redirect("accraViewReconcileAccount.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"), True)
                Response.Redirect("../common/ListReportView.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"), True)
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(str_err)
            End If
            'gridbind()
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub

    Private Sub Save_Reconcile_Account(Optional ByVal p_temp As Boolean = False)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            If Session("liUserList").Count > 0 Then
                Session("liUserList").Clear()
            End If
            Dim rcCount As Integer = 0
            Dim docno As String = ""
            Dim str_err As String = ""
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            For Each gvr As GridViewRow In gvReconcile.Rows
                Dim chkSelect As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)
                If chkSelect IsNot Nothing Then
                    If chkSelect.Checked = True Then
                        rcCount = rcCount + 1
                        Dim txtRcdt As TextBox = CType(gvr.FindControl("txtRCDate"), TextBox)
                        Dim txtDeposit As TextBox = CType(gvr.FindControl("txtDeposit"), TextBox)
                        Dim txtPayment As TextBox = CType(gvr.FindControl("txtPayment"), TextBox)
                        Dim strRecDate As String = txtRcdt.Text
                        Dim hdnDocType As HiddenField = CType(gvr.FindControl("hdnDocType"), HiddenField)
                        Dim txtJNLID As HtmlInputCheckBox = CType(gvr.FindControl("chkSelect"), HtmlInputCheckBox)
                        If strRecDate = "" Then strRecDate = txtHDocdate.Text

                        If hdnDocType.Value = "BS" Then
                            If rcCount = 1 Then
                                str_err = "0|0"
                            Else
                                str_err = "0"
                            End If
                            saveReonciliationN(txtJNLID.Value, txtDocno.Text, txtNarration.Text, txtChqNo.Text, txtDeposit.Text, txtPayment.Text)
                        Else
                            str_err = SaveReconclile_D(Session("sBsuid"), _
                            txtHAccountcode.Text, txtHDocdate.Text, _
                            docno, chkSelect.Value, rcCount, strRecDate, txtPayment.Text, txtDeposit.Text, stTrans, p_temp)
                        End If

                        If rcCount = 1 Then
                            Dim str_doc_err As String()
                            str_doc_err = str_err.Split("|")
                            If str_doc_err.Length = 2 Then
                                If str_doc_err(1) = "0" Then
                                    docno = str_doc_err(0)
                                    str_err = str_doc_err(1)
                                Else
                                    str_err = str_doc_err(1)
                                    Exit For
                                End If
                            Else
                                str_err = "1000"
                                Exit For
                            End If
                        Else
                            If str_err <> "0" Then
                                Exit For
                            End If
                        End If

                    End If
                End If
            Next
            If str_err = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage(str_err)
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                'Response.Redirect("accraViewReconcileAccount.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"), True)
                Response.Redirect("../common/ListReportView.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"), True)
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(str_err)
            End If
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            gridbind()

        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Function SaveReconclile_D(ByVal p_Bsu_id As String, _
   ByVal p_Accountid As String, ByVal p_Docdate As String, ByVal p_DocNO As String, _
   ByVal p_Jnl_id As String, ByVal p_Slno As Integer, ByVal p_Recdate As String, ByVal p_Payment As Decimal, ByVal p_Deposit As Decimal, _
   ByVal stTrans As SqlTransaction, Optional ByVal p_temp As Boolean = False) As String
        '      @RCD_BSU_ID = N'125016',
        '@RCD_JNL_BSU_ID = N'125016',
        '@RCD_JNL_ACT_ID = N'24601006',
        '@RCD_RECONDT = N'2007-10-27'
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Try

            Dim str_Sql As String
            str_Sql = "SELECT     GUID, JNL_ID, JNL_SUB_ID, " _
            & " JNL_BSU_ID, JNL_FYEAR, JNL_DOCTYPE, " _
            & " JNL_DOCNO, JNL_CUR_ID, JNL_EXGRATE1," _
            & " JNL_EXGRATE2, JNL_DOCDT, JNL_ACT_ID," _
            & " JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, " _
            & " JNL_RECONDT, ISNULL(JNL_CHQNO,' ') AS JNL_CHQNO, JNL_CHQDT, " _
            & " JNL_REFDOCTYPE, JNL_REFDOCNO, " _
            & " JNL_BDISCOUNTED, JNL_BDELETED," _
            & " JNL_SLNO, JNL_RSS_ID, JNL_OPP_ACT_ID, " _
            & " JNL_ACTVOUCHERDT, JNL_PARTY_ACT_ID" _
            & " FROM JOURNAL_D " _
            & " where JNL_ID='" & p_Jnl_id & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ' ''		@GUID = NULL,
                ' ''		@RCD_DOCNO = '123',
                ' ''        @RCD_RECONDT='2007-12-17',
                ' ''		@RCD_DOCDT = N'2007-12-17',
                ' ''		@RCD_BSU_ID = N'125016',
                Dim pParms(28) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@GUID", SqlDbType.VarChar, 50)
                pParms(0).Value = System.DBNull.Value
                pParms(1) = New SqlClient.SqlParameter("@RCD_DOCNO", SqlDbType.VarChar, 20)
                pParms(1).Value = p_DocNO
                pParms(2) = New SqlClient.SqlParameter("@RCD_DOCDT", SqlDbType.DateTime)
                pParms(2).Value = p_Docdate
                pParms(3) = New SqlClient.SqlParameter("@RCD_BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = p_Bsu_id

                ' ''		@RCD_FYEAR = 2007,
                ' ''		@RCD_JNL_ID = 40470,
                ' ''		@RCD_JNL_SUB_ID = N'007',
                ' ''		@RCD_JNL_BSU_ID = N'125016',
                ' ''		@RCD_JNL_FYEAR = 2007,

                pParms(4) = New SqlClient.SqlParameter("@RCD_FYEAR", SqlDbType.Int)
                pParms(4).Value = Session("F_YEAR")
                pParms(5) = New SqlClient.SqlParameter("@RCD_JNL_ID", SqlDbType.VarChar, 20)
                pParms(5).Value = p_Jnl_id
                pParms(6) = New SqlClient.SqlParameter("@RCD_JNL_SUB_ID", SqlDbType.VarChar, 20)
                pParms(6).Value = ds.Tables(0).Rows(0)("JNL_SUB_ID")
                pParms(7) = New SqlClient.SqlParameter("@RCD_JNL_BSU_ID", SqlDbType.VarChar, 20)
                pParms(7).Value = p_Bsu_id
                pParms(8) = New SqlClient.SqlParameter("@RCD_JNL_FYEAR", SqlDbType.Int)
                pParms(8).Value = ds.Tables(0).Rows(0)("JNL_FYEAR")
                ' ''		@RCD_JNL_DOCTYPE = N'BP',
                ' ''		@RCD_JNL_DOCNO = N'0709BP-00100',
                ' ''		@RCD_JNL_DOCDT = N'2007-12-17',
                ' ''		@RCD_JNL_ACT_ID = N'24601006',
                ' ''		@RCD_JNL_DEBIT = 0,

                'JNL_ID JNL_BSU_ID JNL_SUB_ID JNL_FYEAR
                'JNL_DOCTYPE JNL_DOCNO JNL_CUR_ID
                'JNL_EXGRATE1 JNL_EXGRATE2 JNL_DOCDT
                'JNL_ACT_ID JNL_DEBIT JNL_CREDIT
                'JNL_NARRATION JNL_RECONDT JNL_CHQNO
                'JNL_CHQDT JNL_REFDOCTYPE JNL_REFDOCNO
                'JNL_BDISCOUNTED JNL_BDELETED JNL_ACTVOUCHERDT
                'JNL_SLNO JNL_RSS_ID JNL_OPP_ACT_ID    
                pParms(9) = New SqlClient.SqlParameter("@RCD_JNL_DOCTYPE", SqlDbType.VarChar, 10)
                pParms(9).Value = "REC"
                pParms(10) = New SqlClient.SqlParameter("@RCD_JNL_DOCNO", SqlDbType.VarChar, 20)
                pParms(10).Value = ds.Tables(0).Rows(0)("JNL_DOCNO")
                pParms(11) = New SqlClient.SqlParameter("@RCD_JNL_DOCDT", SqlDbType.DateTime)
                pParms(11).Value = ds.Tables(0).Rows(0)("JNL_DOCDT")
                pParms(12) = New SqlClient.SqlParameter("@RCD_JNL_ACT_ID", SqlDbType.VarChar, 20)
                pParms(12).Value = ds.Tables(0).Rows(0)("JNL_ACT_ID")
                pParms(13) = New SqlClient.SqlParameter("@RCD_JNL_DEBIT", SqlDbType.Decimal, 21)
                pParms(13).Value = p_Deposit 'ds.Tables(0).Rows(0)("JNL_DEBIT")

                ' ''		@RCD_JNL_CREDIT = 8160,
                ' ''		@RCD_JNL_CUR_ID = N'DHS',
                ' ''		@RCD_JNL_EXGRATE1 = 1,
                ' ''		@RCD_JNL_EXGRATE2 = 1,
                ' ''		@RCD_JNL_NARRATION = N'BOOKPLUS PDC',
                pParms(14) = New SqlClient.SqlParameter("@RCD_JNL_CREDIT", SqlDbType.Decimal, 21)
                pParms(14).Value = p_Payment 'ds.Tables(0).Rows(0)("JNL_CREDIT")
                pParms(15) = New SqlClient.SqlParameter("@RCD_JNL_CUR_ID", SqlDbType.VarChar, 20)
                pParms(15).Value = ds.Tables(0).Rows(0)("JNL_CUR_ID")
                pParms(16) = New SqlClient.SqlParameter("@RCD_JNL_EXGRATE1", SqlDbType.Decimal, 20)
                pParms(16).Value = ds.Tables(0).Rows(0)("JNL_EXGRATE1")
                pParms(17) = New SqlClient.SqlParameter("@RCD_JNL_EXGRATE2", SqlDbType.Decimal, 20)
                pParms(17).Value = ds.Tables(0).Rows(0)("JNL_EXGRATE2")
                pParms(18) = New SqlClient.SqlParameter("@RCD_JNL_NARRATION", SqlDbType.VarChar, 200)
                pParms(18).Value = ds.Tables(0).Rows(0)("JNL_NARRATION")

                ' ''		@RCD_JNL_CHQNO = N'19',
                ' ''		@RCD_JNL_CHQDT = N'2007-12-17',
                ' ''		@RCD_bTemp = 0,
                ' ''		@RCD_bDeleted = 0,
                ' ''		@RCD_SLNO = 0
                ''@RCD_NEWDOCNO
                pParms(19) = New SqlClient.SqlParameter("@RCD_JNL_CHQNO", SqlDbType.VarChar, 20)
                pParms(19).Value = ds.Tables(0).Rows(0)("JNL_CHQNO")
                pParms(20) = New SqlClient.SqlParameter("@RCD_JNL_CHQDT", SqlDbType.DateTime)
                pParms(20).Value = ds.Tables(0).Rows(0)("JNL_CHQDT")
                pParms(21) = New SqlClient.SqlParameter("@RCD_bTemp", SqlDbType.Bit)
                pParms(21).Value = p_temp
                pParms(22) = New SqlClient.SqlParameter("@RCD_bDeleted", SqlDbType.Bit)
                pParms(22).Value = False
                pParms(23) = New SqlClient.SqlParameter("@RCD_JNL_SLNO", SqlDbType.Int)
                pParms(23).Value = ds.Tables(0).Rows(0)("JNL_SLNO")
                pParms(24) = New SqlClient.SqlParameter("@RCD_SLNO", SqlDbType.Int)
                pParms(24).Value = p_Slno
                pParms(25) = New SqlClient.SqlParameter("@RCD_RECONDT", SqlDbType.DateTime)
                pParms(25).Value = p_Recdate
                pParms(26) = New SqlClient.SqlParameter("@RCD_NEWDOCNO", SqlDbType.VarChar, 20)
                pParms(26).Direction = ParameterDirection.Output

                pParms(27) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(27).Direction = ParameterDirection.ReturnValue
                'Dim stTrans As SqlTransaction = objConn.BeginTransaction

                SaveReconclile_D = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveRECONCILIATION_D", pParms)
                If pParms(27).Value = "0" Then

                    If p_Slno = 1 Then
                        SaveReconclile_D = pParms(26).Value & "|" & "0"
                    Else
                        SaveReconclile_D = 0
                    End If
                Else
                    SaveReconclile_D = pParms(27).Value
                End If
            Else
                'jnl not found
                SaveReconclile_D = "1000"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            SaveReconclile_D = 1000
        Finally

        End Try

    End Function

    Public Shared Function UpdateReconclile_D(ByVal RCD_DocNO As String, ByVal p_Jnl_id As String, ByVal RCD_RECONDT As String, ByVal p_Deposit As Decimal, ByVal p_Payment As Decimal, ByVal stTrans As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@RCD_DOCNO", SqlDbType.VarChar, 20)
            pParms(1).Value = RCD_DocNO
            pParms(2) = New SqlClient.SqlParameter("@RCD_JNL_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_Jnl_id
            pParms(3) = New SqlClient.SqlParameter("@RCD_RECONDT", SqlDbType.DateTime)
            pParms(3).Value = RCD_RECONDT
            pParms(4) = New SqlClient.SqlParameter("@RCD_JNL_DEBIT", SqlDbType.Decimal, 21)
            pParms(4).Value = p_Deposit
            pParms(5) = New SqlClient.SqlParameter("@RCD_JNL_CREDIT", SqlDbType.Decimal, 21)
            pParms(5).Value = p_Payment
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue

            UpdateReconclile_D = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "UpdateRECONCILIATION_D", pParms)
            If pParms(6).Value = "0" Then
                UpdateReconclile_D = 0
            Else
                UpdateReconclile_D = pParms(6).Value
            End If
            'Else
            'UpdateReconclile_D = "1000"
            'End If

        Catch ex As Exception
            Errorlog(ex.Message)
            UpdateReconclile_D = 1000
        Finally

        End Try

    End Function

    Private Sub list_add(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
        Else
            Session("liUserList").Add(p_userid)
        End If
    End Sub

    Protected Sub btnHdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHdate.Click
        bind_Check_date_AndBind()
    End Sub

    Protected Sub btnLater_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLater.Click
        Save_Reconcile_Account(True)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ViewState("datamode") = Encr_decrData.Encrypt("view")
        Response.Redirect("../Common/ListReportView.aspx?MainMnu_code=vknaKTP5oWw=&datamode=Zo4HhpVNpXc=")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim pParms(2) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@RCD_DOCNO", txtHDocno.Text, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "deleteRECONCILIATION_D", pParms)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
        End Try
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        'Response.Redirect("accraViewReconcileAccount.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"), True)
        Response.Redirect("../Common/ListReportView.aspx?MainMnu_code=vknaKTP5oWw=&datamode=Zo4HhpVNpXc=")
    End Sub

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        bind_Check_date_AndBind()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim url As String
        Dim datamodeAdd As String = Encr_decrData.Encrypt("add")
        url = "accraReconcileAccount.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamodeAdd
        Response.Redirect(url)
    End Sub

    Protected Sub btnSaveDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDetails.Click
        Dim RReason As String = ""
        If txtDocno.Text = "" Then
            RReason = "Doc no,"
        End If
        If txtNarration.Text = "" Then
            RReason &= "Narration,"
        End If
        If txtChqNo.Text = "" Then
            RReason &= "Chq.No,"
        End If
        If Not (IsNumeric(txtDeposit.Text) And IsNumeric(txtPayment.Text)) Or Val(txtDeposit.Text) + Val(txtPayment.Text) = 0 Then
            RReason &= "Amount,"
        End If
        If RReason <> "" Then
            lblError.Text = RReason.Substring(0, RReason.Length - 1) & " mandatory"
            Exit Sub
        End If
        saveReonciliationN(0, txtDocno.Text, txtNarration.Text, txtChqNo.Text, txtDeposit.Text, txtPayment.Text)

        If h_entryId Is Nothing Then
            bind_Check_date_AndBind()
        Else
            gridbind1(Encr_decrData.Decrypt(h_entryId.Replace(" ", "+")))
        End If
    End Sub

    Private Sub saveReonciliationN(ByVal RCN_ID As Integer, ByVal RCN_DOCNO As String, ByVal RCN_NARRATION As String, ByVal RCN_CHQNO As String, ByVal RCN_DEPOSIT As Decimal, ByVal RCN_PAYMENT As Decimal)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        Dim kParms(10) As SqlClient.SqlParameter
        kParms(1) = Mainclass.CreateSqlParameter("@RCN_ID", RCN_ID, SqlDbType.Int)
        kParms(2) = Mainclass.CreateSqlParameter("@RCN_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        kParms(3) = Mainclass.CreateSqlParameter("@RCN_ACT_ID", txtHAccountcode.Text, SqlDbType.VarChar)
        kParms(4) = Mainclass.CreateSqlParameter("@RCN_DATE", txtHDocdate.Text, SqlDbType.VarChar)
        kParms(5) = Mainclass.CreateSqlParameter("@RCN_DOCNO", RCN_DOCNO, SqlDbType.VarChar)
        kParms(6) = Mainclass.CreateSqlParameter("@RCN_NARRATION", RCN_NARRATION, SqlDbType.VarChar)
        kParms(7) = Mainclass.CreateSqlParameter("@RCN_CHQNO", RCN_CHQNO, SqlDbType.VarChar)
        kParms(8) = Mainclass.CreateSqlParameter("@RCN_DEPOSIT", RCN_DEPOSIT, SqlDbType.Decimal)
        kParms(9) = Mainclass.CreateSqlParameter("@RCN_PAYMENT", RCN_PAYMENT, SqlDbType.Decimal)
        kParms(10) = Mainclass.CreateSqlParameter("@RCN_RCD_DOCNO", txtHDocno.Text, SqlDbType.VarChar)

        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveRECONCILIATION_N", kParms)
            stTrans.Commit()
            txtDocno.Text = ""
            txtNarration.Text = ""
            txtChqNo.Text = ""
            txtDeposit.Text = "0.00"
            txtPayment.Text = "0.00"
            lblError.Text = "Data saved successfully"

        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        End Try
    End Sub

    Private Sub getBankBalance()
        Dim rTable As DataTable
        Dim kParms(3) As SqlClient.SqlParameter
        kParms(1) = Mainclass.CreateSqlParameter("@DOCDT", txtHDocdate.Text, SqlDbType.VarChar)
        kParms(2) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
        kParms(3) = Mainclass.CreateSqlParameter("@ACT_ID", txtHAccountcode.Text, SqlDbType.VarChar)
        rTable = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString, CommandType.StoredProcedure, "RPTRECONCILIATIONSTMT", kParms).Tables(0)
        txtHTotal.Text = rTable.Compute("sum(AMount)", "typ=0")
        txtDTotalamount.Text = rTable.Compute("sum(AMount)", "")
    End Sub

    Protected Sub txtHAccountcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHAccountcode.TextChanged
        txtHAccountname.Text = AccountFunctions.Validate_Account(txtHAccountcode.Text, Session("sbsuid"), "BANK")
        If txtHAccountname.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtHAccountcode.Focus()
        Else
            lblError.Text = ""
            bind_Check_date_AndBind()
        End If
    End Sub

    Protected Sub btnReonciliation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReonciliation.Click
        Response.Redirect("BankReconciliation.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
    End Sub
End Class
