﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Partial Class Accounts_ParentPortalTileSetting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    DataBindInfoList()
                    DataBindBsuUnits()
                End If
            Catch ex As Exception
                lblError.Text = "Updation got failed."
            End Try
        End If
    End Sub
    Protected Sub submit_Click(sender As Object, e As EventArgs)
        Try
            For Each ListBox As ListItem In lstBsuUnit.Items
                If (ListBox.Selected = True) Then
                    For Each chklistitem As ListItem In chkinfolist.Items
                        If (chklistitem.Selected = True) Then
                            UpdateInfo_s(chklistitem.Value, ListBox.Value, True)
                        Else
                            UpdateInfo_s(chklistitem.Value, ListBox.Value, False)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            lblError.Text = "Updation got failed."
        End Try
    End Sub
    Protected Sub UpdateInfo_s(ByVal InfoTileUnit As String, ByVal BSU_Unit As String, ByVal UPDATE As Boolean)
        Dim strans As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        strans = objConn.BeginTransaction
        Dim param(2) As SqlParameter
        Try            
            param(0) = Mainclass.CreateSqlParameter("@TILE_UNIT", Convert.ToInt32(InfoTileUnit), SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@BSU_UNIT", BSU_Unit, SqlDbType.VarChar)
            If (UPDATE = True) Then
                param(2) = Mainclass.CreateSqlParameter("@UPDATE", 1, SqlDbType.Bit)
            Else
                param(2) = Mainclass.CreateSqlParameter("@UPDATE", 0, SqlDbType.Bit)
            End If
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[INSERT_UPDATE_ONLINE_INFO_S]", param)
            lblError.Text = "Successfully updated selected schools with tile details."
            strans.Commit()
        Catch ex As Exception
            lblError.Text = "Updation got failed."
            strans.Rollback()
        End Try
    End Sub
    Protected Sub DataBindInfoList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsInfoTiles As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ONLINEINFO_TILES]")
        chkinfolist.DataSource = dsInfoTiles.Tables(0)
        chkinfolist.DataTextField = "inf_type"
        chkinfolist.DataValueField = "inf_id"
        chkinfolist.DataBind()
    End Sub
    Protected Sub DataBindBsuUnits()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsbsu As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_BSU_UNITS]")
        lstBsuUnit.DataSource = dsbsu.Tables(0)
        lstBsuUnit.DataTextField = "bsu_name"
        lstBsuUnit.DataValueField = "bsu_id"
        lstBsuUnit.DataBind()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Try
            Response.Redirect("../Homepage.aspx")
        Catch ex As Exception

        End Try

    End Sub
End Class
