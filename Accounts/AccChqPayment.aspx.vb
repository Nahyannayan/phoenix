Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Partial Class Accounts_AccChqPayment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrErrMsg As String
    Dim lstrUsedChqNos As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("BANKTRAN") = "QP"
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        LockControls()
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnView)
        If Page.IsPostBack = False Then
            Try
                '   --- For Checking Rights And Initilize The Edit Variables --
                Dim CurBsUnit As String = Session("sBsuid")
                Session("dtSettle") = DataTables.CreateDataTable_Settle
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                btnSettle.Visible = False
                lnkCostUnit.Enabled = False
                lnkCostUnit.Visible = False
                Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")) 
                If (Session("datamode") = "add") Then

                    Call Clear_Details()
                    Call Clear_Header()
                    set_bankaccount()
                    Session("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Session("gintGridLine") = 1
                    GridInitialize()
                    Session("gDtlDataMode") = "ADD"
                    Session("dtDTL") = DataTables.CreateDataTable_PDC()
                    Session("gdtSub") = CostCenterFunctions.CreateDataTableCostCenter()

                    txtdocDate.Text = GetDiplayDate()
                    txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString

                    bind_Currency()
                    'FillLotNo()

                End If

                If Session("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If

                If USR_NAME = "" Or (Session("MainMnu_code") <> "A150013" _
                And Session("MainMnu_code") <> OASISConstants.MNU_CHEQUE_PAYMENT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If Session("datamode") = "view" Then
                        '   --- Fill Data ---
                        bind_Currency()
                        FillValues()
                    End If
                End If
                '   --- Checking End  ---

                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                Errorlog(ex.Message, "pageload")
            End Try
        Else
            If (Session("datamode") = "add") Then
                '    txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
                Session("SessDocDate") = txtdocDate.Text
            End If
        End If
        If Session("datamode") <> "add" Then
            btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
            imgDocdate.Enabled = False
        Else
            imgDocdate.Enabled = True
        End If

    End Sub

    Private Sub FillLotNo()
        If txtBankCode.Text <> "" Then
            Dim str_alloted_nos As String = String.Empty
            If Not Session("chqNos") Is Nothing Then
                str_alloted_nos = GetAllotedChequeNos(Session("chqNos"))
            End If

            Dim str_sql As String = " SELECT TOP 1 ISNULL(CHQBOOK_M.CHB_LOTNO,'') AS LOT_NO, " & _
            " CHB_ID, ISNULL(MIN(ISNULL(CHD_NO,'')),'') AS CHD_NO FROM CHQBOOK_M INNER JOIN " & _
            " CHQBOOK_D ON CHQBOOK_M.CHB_ID = CHQBOOK_D.CHD_CHB_ID WHERE " & _
            " (CHQBOOK_D.CHD_ALLOTED = 0 OR CHD_DOCNO = '" & txtdocNo.Text & "') AND (CHQBOOK_M.CHB_ACT_ID = '" & txtBankCode.Text & "')" & _
            " AND CHQBOOK_M.CHB_BSU_ID = '" & Session("sBSUID") & "'"
            If Not Session("chqNos") Is Nothing Then
                str_sql += " AND CHD_NO NOT IN (" & str_alloted_nos & ")"
            End If
            str_sql += "GROUP BY CHB_LOTNO, CHB_ID "
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            While (dr.Read())
                hCheqBook.Value = dr("CHB_ID")
                txtChqNo.Text = dr("CHD_NO")
                If dr("LOT_NO") <> 0 Then
                    txtChqBook.Text = dr("LOT_NO")
                Else
                    txtChqBook.Text = ""
                End If
            End While

        End If
    End Sub

    Private Function GetAllotedChequeNos(ByVal arrList As Hashtable) As String
        Dim str_chqNos As String = String.Empty
        Dim comma As String = String.Empty
        Dim ienum As IDictionaryEnumerator = arrList.GetEnumerator
        While (ienum.MoveNext())
            str_chqNos += comma & "'" & ienum.Value & "'"
            comma = ","
        End While
        Return str_chqNos
    End Function

    Private Sub LockControls()
        txtNewChqBook.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtPaidDescr.Attributes.Add("readonly", "readonly")
        txtIntrDescr.Attributes.Add("readonly", "readonly")
        txtAcrdDescr.Attributes.Add("readonly", "readonly")
        txtPrepdDescr.Attributes.Add("readonly", "readonly")
        txtChqissDescr.Attributes.Add("readonly", "readonly")
        txtMInst.Attributes.Add("readonly", "readonly")
        tr_Update.Visible = False
        If optControlAccNo.Checked = True Then
            txtIntRate.Text = "0"
        End If
    End Sub


    Private Sub bind_Currency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            If txtdocDate.Text = "" Then
                txtdocDate.Text = GetDiplayDate()
            End If
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If cmbCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString)
        If str_bankact_name <> "" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If

    End Sub


    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function


    Private Sub GridInitialize()
        Dim dtTempDTL As New DataTable
        Dim rDt As DataRow
        dtTempDTL = DataTables.CreateDataTable_PDC()
        rDt = dtTempDTL.NewRow
        dtTempDTL.Rows.Add(rDt)
        gvDTL.DataSource = dtTempDTL
        gvDTL.DataBind()
    End Sub


    Private Sub Clear_Header()
        txtdocNo.Text = ""
        txtOldDocNo.Text = ""
        txtdocDate.Text = ""
        txtNarrn.Text = ""
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtPaidto.Text = ""
        txtPaidDescr.Text = ""
        txtIntr.Text = ""
        txtIntrDescr.Text = ""
        txtAcrd.Text = ""
        txtAcrdDescr.Text = ""
        txtPrepd.Text = ""
        txtPrepdDescr.Text = ""
        txtChqiss.Text = ""
        txtChqissDescr.Text = ""
        txtNewPartycode.Text = ""
        txtNewPartyname.Text = ""
        txtNarrn.Text = ""
        bind_Currency()
        set_bankaccount()
    End Sub


    Private Sub Clear_Details()
        txtAmount.Text = ""
        txtIntRate.Text = ""
        txtMnths.Text = ""
        txtMIntr.Text = ""
        txtMInst.Text = ""
        txtChqBook.Text = ""
        txtChqDate.Text = ""
        txtMInst.Text = ""
        txtGridInterest.Text = ""
        txtBankTotal.Text = ""
        txtAdj.Text = ""
        'txtFrom.Text = ""
        'txtTo.Text = ""
        'txtExpCode.Text = ""
        'txtPreAcc.Text = ""
        'lnkCostUnit.Enabled = False
        'lnkCostUnit.Visible = False
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblRowId As New Label
            Dim lintIndex As Integer = 0

            lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
            Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

            For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                    tr_Add.Visible = False

                    tr_Update.Visible = True
                    hCheqBook.Value = Trim(Session("dtDTL").Rows(lintIndex)("ChqBookId"))
                    txtNewChqBook.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqBookLot"))


                    txtNewChqNo.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqNo"))

                    gvDTL.SelectedIndex = lintIndex
                    gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                    gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                    Session("gDtlDataMode") = "UPDATE"


                    Exit For
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub ToggleCols(ByVal pSet As Boolean)
        gvDTL.Columns(9).Visible = pSet
        gvDTL.Columns(10).Visible = pSet
        gvDTL.Columns(11).Visible = pSet
    End Sub


    Protected Function GetNavigateUrl(ByVal pId As String, ByVal pAmount As String) As String
        'Return String.Format("javascript:var popup = window.showModalDialog('ShowAlloc.aspx?code={0}', 'Popup', 'toolbar=no');", pId)
        Return String.Format("javascript:var popup = window.showModalDialog('TestAlloc.aspx?Id={0}&pAmount={1}', '','dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:no;');", pId, pAmount)
    End Function


    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ToggleCols(True)
        Session("gDtlDataMode") = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
    End Sub


    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")


                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
           & " GUID='" & Session("Eid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = Session("sBSUId")
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR")
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        lstrErrMsg = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If

        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If

        If Trim(txtPaidto.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Party " & "<br>"
        End If

        If (optControlAccYes.Checked = True) Then
            If Trim(txtIntr.Text = "") Then
                lstrErrMsg = lstrErrMsg & "Invalid Interest Account " & "<br>"
            End If

            If Trim(txtAcrd.Text = "") Then
                lstrErrMsg = lstrErrMsg & "Invalid Accrued Account " & "<br>"
            End If

            If (IsNumeric(txtMIntr.Text) = False) Then
                lstrErrMsg = lstrErrMsg & "Enter the Month Interval" & "<br>"
            End If
        End If

        If Trim(txtPrepd.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Prepaid Account " & "<br>"
        End If

        If Trim(txtChqiss.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Provision Account " & "<br>"
        End If

        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Details " & "<br>"
        End If


        If (hCostReqd.Value = "True") Then
            If Session("gdtSub").Rows.Count = 0 Then
                lstrErrMsg = lstrErrMsg & " Enter the mandatory cost center details " & "<br>"
            End If
        End If

        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            'tr_errLNE.Visible = False
            Return True
        End If


    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String
        Dim lblnNoErr, isSupplier As Boolean
        '   ----------------- VALIDATIONS --------------------
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            'tr_errLNE.Visible = False
            txtdocDate.Text = strfDate
        End If
        lblnNoErr = SaveValidate()
        If (lblnNoErr = False) Then
            Exit Sub
        End If

        Dim acttype As String = AccountFunctions.check_accounttype(txtPaidto.Text, Session("sBsuid"))
        If acttype = "S" Then
            isSupplier = True
        Else
            isSupplier = False
        End If

        txtNewPartyname.Text = AccountFunctions.Validate_Account(txtNewPartycode.Text, Session("sbsuid"), "NOTCC")
        If txtNewPartyname.Text = "" Then
            lblError.Text = "Invalid Account Selected (Party)"
            txtNewPartyname.Focus()
            Exit Sub
        End If

        txtPaidDescr.Text = AccountFunctions.Validate_Account(txtPaidto.Text, Session("sbsuid"), "NOTCC")
        If txtPaidDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected (Paid to)"
            txtPaidto.Focus()
            Exit Sub
        End If

        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
            Exit Sub
        End If
        If optControlAccYes.Checked Or txtIntr.Text.Trim <> "" Then
            txtIntrDescr.Text = AccountFunctions.Validate_Account(txtIntr.Text, Session("sbsuid"), "INTRAC")
            If txtIntrDescr.Text = "" Then
                lblError.Text = "Invalid Interest Account Selected"
                txtIntr.Focus()
                Exit Sub
            End If
        End If

        If optControlAccYes.Checked Or txtAcrd.Text.Trim <> "" Then
            txtAcrdDescr.Text = AccountFunctions.Validate_Account(txtAcrd.Text, Session("sbsuid"), "ACRDAC")
            If txtAcrdDescr.Text = "" Then
                lblError.Text = "Invalid Interest Account Selected"
                txtAcrd.Focus()
                Exit Sub
            End If
        End If


        txtPrepdDescr.Text = AccountFunctions.Validate_Account(txtPrepd.Text, Session("sbsuid"), "PREPDAC")
        If txtPrepdDescr.Text = "" Then
            lblError.Text = "Invalid Interest Account Selected"
            txtPrepd.Focus()
            Exit Sub
        End If

        txtChqissDescr.Text = AccountFunctions.Validate_Account(txtChqiss.Text, Session("sbsuid"), "CHQISSAC_PDC")
        If txtChqissDescr.Text = "" Then
            lblError.Text = "Invalid Check Issue account Selected"
            txtChqiss.Focus()
            Exit Sub
        End If
        '   ----------------- END OG VALIDATE -----------------
        Try
            Session("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                If (Session("datamode") = "edit") Then


                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@GUID", SqlDbType.VarChar, 50)
                    pParms(0).Value = Session("Eid")


                    pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(1).Direction = ParameterDirection.ReturnValue

                    Dim retval As Integer

                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "DeleteVOUCHER_PDC_DETAILS", pParms)
                    If pParms(1).Value <> "0" Then
                        Errorlog("PROBLEM PDC")
                        lblError.Text = "PLEASE TRY AGAIN..."
                        Exit Sub
                    End If
                End If


                '   --- END DELETE   ---
                Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)



                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                SqlCmd.Parameters.Add(sqlpGUID)
                SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
                SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", Session("BANKTRAN"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", Trim(txtdocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_REFNO", Trim(txtOldDocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", hCheqBook.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtdocDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChqDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", txtMnths.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", txtMIntr.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", txtPaidto.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", Convert.ToDecimal(txtMInst.Text))
                If (optControlAccYes.Checked = True) Then
                    SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", Convert.ToDecimal(txtIntRate.Text))
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", 0)
                End If
                SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", optControlAccYes.Checked)
                SqlCmd.Parameters.AddWithValue("@VHH_bAuto", False)
                SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", cmbCalcu.SelectedItem.Value)
                If (optControlAccYes.Checked = True) Then
                    SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", txtIntr.Text)
                    SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", txtAcrd.Text)
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
                    SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
                End If

                SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", txtChqiss.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", txtPrepd.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

                SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", cmbCurrency.SelectedItem.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", txtExchRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", txtLocalRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txtReceivedBy.Text)

                SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
                SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
                SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True) '@VHH_ISSUEDTO
                SqlCmd.Parameters.AddWithValue("@VHH_ISSUEDTO", txtNewPartycode.Text.Trim)
                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If Session("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
                End If
                SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
                SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
                SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                If (Session("datamode") = "edit") Then
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                End If
                SqlCmd.Parameters.AddWithValue("@VHH_bPDC", True)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (Session("datamode") = "edit") Then
                    lstrNewDocNo = txtdocNo.Text
                Else
                    lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
                End If

                'Adding header info
                SqlCmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String
                If (lintRetVal = 0) Then
                    If chkAdvance.Checked Then
                        isSupplier = False
                    End If
                    If Session("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo, isSupplier)
                    Else
                        str_err = DoTransactions(objConn, stTrans, txtdocNo.Text, isSupplier)
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()

                        Call Clear_Header()
                        Call Clear_Details()
                        Session("datamode") = "add"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                        Session("gintGridLine") = 1
                        GridInitialize()
                        Session("gDtlDataMode") = "ADD"
                        Session("dtDTL") = DataTables.CreateDataTable_PDC()
                        Session("gdtSub").Rows.Clear()
                        Session("dtSettle").rows.clear()
                        txtdocDate.Text = GetDiplayDate()
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString

                        bind_Currency()
                        If Session("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        End If
                        'Dim new_guid As String = VoucherFunctions.get_Voucher_Guid(lstrNewDocNo, Session("sBsuid"), Session("SUB_ID"), Session("BANKTRAN"), Session("F_YEAR"))

                        'If new_guid <> "" Then
                        '    Response.Redirect("AccAddPDC.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view") & "&Eid=" & Encr_decrData.Encrypt(new_guid), False)
                        'End If
                        'tr_errLNE.Visible = True
                        lblError.Text = getErrorMessage(0)
                    Else
                        'tr_errLNE.Visible = True
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                    End If
                Else
                    'tr_errLNE.Visible = True
                    lblError.Text = getErrorMessage(lintRetVal)
                    stTrans.Rollback()
                End If

            Catch ex As Exception
                lblError.Text = getErrorMessage(lintRetVal)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception

        End Try
    End Sub


    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
    ByVal p_isSupplier As Boolean) As String
        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            Dim str_err As String = ""
            Dim dTotal As Double = 0

            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, "1", _
                  "DR", iIndex + 1, txtPaidto.Text, txtMInst.Text, txtdocDate.Text)
                If str_err <> "0" Then
                    Return str_err
                End If

                cmd.Dispose()
                cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
                cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
                cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
                cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
                cmd.Parameters.AddWithValue("@VHD_DOCTYPE", Session("BANKTRAN"))
                cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
                cmd.Parameters.AddWithValue("@VHD_LINEID", Session("dtDTL").Rows(iIndex)("Id"))
                cmd.Parameters.AddWithValue("@VHD_ACT_ID", txtPaidto.Text)

                cmd.Parameters.AddWithValue("@VHD_AMOUNT", Session("dtDTL").Rows(iIndex)("PayAmount"))
                cmd.Parameters.AddWithValue("@VHD_NARRATION", Trim(txtNarrn.Text))
                cmd.Parameters.AddWithValue("@VHD_CHQID", Session("dtDTL").Rows(iIndex)("ChqBookId"))
                cmd.Parameters.AddWithValue("@VHD_CHQNO", Session("dtDTL").Rows(iIndex)("ChqNo"))
                cmd.Parameters.AddWithValue("@VHD_CHQDT", Session("dtDTL").Rows(iIndex)("ChqDate"))
                cmd.Parameters.AddWithValue("@VHD_RSS_ID", "83")

                cmd.Parameters.AddWithValue("@VHD_OPBAL", Session("dtDTL").Rows(iIndex)("BalDue"))
                cmd.Parameters.AddWithValue("@VHD_INTEREST", Session("dtDTL").Rows(iIndex)("Interest"))

                cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
                cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
                cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
                cmd.Parameters.AddWithValue("@VHD_bCHEQUE", True)
                cmd.Parameters.AddWithValue("@VHD_COL_ID", 0)
                If (Session("datamode") = "edit") Then
                    cmd.Parameters.AddWithValue("@bEdit", False)
                Else
                    cmd.Parameters.AddWithValue("@bEdit", False)
                End If
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()

                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                Dim success_msg As String = ""


                ''''''''

                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                    Exit For
                Else
                    AccountFunctions.SAVETRANHDRONLINE_D("", Session("SUB_ID"), _
                    Session("dtDTL").Rows(iIndex)("Id"), Session("BANKTRAN"), cmbCurrency.SelectedItem.Text, _
                    txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), Session("F_YEAR"), _
                    p_docno, txtPaidto.Text, "DR", Session("dtDTL").Rows(iIndex)("PayAmount"), _
                    txtdocDate.Text, "", Session("dtDTL").Rows(iIndex)("ChqDate"), _
                    Session("dtDTL").Rows(iIndex)("ChqNo"), False, Trim(txtNarrn.Text), stTrans)
                    Dim dblAmtAlloc As Decimal = 0
                    For il As Integer = 0 To Session("dtSettle").Rows.count - 1
                        If Session("dtDTL").Rows(iIndex)("Id") = Session("dtSettle").rows(il)("Id") And _
                        txtPaidto.Text.Trim = Session("dtSettle").rows(il)("Accountid") Then
                            Dim SOL_DOCDT As DateTime

                            If CDate(txtdocDate.Text) > CDate(Session("dtDTL").Rows(iIndex)("ChqDate")) Then
                                SOL_DOCDT = txtdocDate.Text
                            Else
                                SOL_DOCDT = Session("dtDTL").Rows(iIndex)("ChqDate")
                            End If

                            str_err = AccountFunctions.SAVESETTLEONLINE_D(Session("Sub_ID"), Session("dtDTL").Rows(iIndex)("Id"), _
                                  Session("BANKTRAN"), txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), _
                                  Session("F_YEAR"), p_docno, _
                                  txtPaidto.Text.Trim, Session("dtSettle").rows(il)("Amount"), cmbCurrency.SelectedItem.Text, _
                                 SOL_DOCDT, Session("dtSettle").rows(il)("jnlid"), stTrans)
                            dblAmtAlloc = dblAmtAlloc + Convert.ToDecimal(Session("dtSettle").rows(il)("Amount"))
                        End If

                    Next
                    If dblAmtAlloc <> Session("dtDTL").Rows(iIndex)("PayAmount") And p_isSupplier = True Then
                        Return "535"
                    End If
                End If
                cmd.Parameters.Clear()
            Next

            Return iReturnvalue

        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function



    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, _
      ByVal p_amount As String, ByVal p_date As String) As String

        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_err As String = "0"
        Dim str_balanced As Boolean = True
        For iIndex = 0 To Session("gdtSub").Rows.Count - 1
            If Session("gdtSub").Rows(iIndex)("VoucherId") = p_voucherid And Session("gdtSub").Rows(iIndex)("Status") & "" <> "Deleted" Then

                If str_prev_cost_center <> Session("gdtSub").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("gdtSub").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    lblError.Text = "Allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If

                str_prev_cost_center = Session("gdtSub").Rows(iIndex)("costcenter")

                cmd.Dispose()

                cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                sqlpGUID.Value = Session("gdtSub").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpJDS_ID.Value = p_slno
                cmd.Parameters.Add(sqlpJDS_ID)

                Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpJDS_DOCTYPE.Value = Session("BANKTRAN")
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpJDS_DOCDT.Value = txtdocDate.Text & ""
                cmd.Parameters.Add(sqlpJDS_DOCDT)

                Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJDS_ACT_ID.Value = p_accountid
                cmd.Parameters.Add(sqlpJDS_ACT_ID)

                Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbJDS_SLNO.Value = p_slno
                cmd.Parameters.Add(sqlpbJDS_SLNO)

                Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpJDS_AMOUNT.Value = Session("gdtSub").Rows(iIndex)("Amount")
                cmd.Parameters.Add(sqlpJDS_AMOUNT)

                Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                If (Session("gdtSub").Rows(iIndex)("costcenter") = "OTH") Then
                    sqlpJDS_CCS_ID.Value = Session("gdtSub").Rows(iIndex)("MemberId")
                Else
                    sqlpJDS_CCS_ID.Value = Session("gdtSub").Rows(iIndex)("costcenter")
                End If

                cmd.Parameters.Add(sqlpJDS_CCS_ID)

                Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                If (Session("gdtSub").Rows(iIndex)("costcenter") = "OTH") Then
                    sqlpJDS_CODE.Value = System.DBNull.Value
                Else
                    sqlpJDS_CODE.Value = Session("gdtSub").Rows(iIndex)("Memberid")
                End If

                cmd.Parameters.Add(sqlpJDS_CODE)

                Dim sqlpJDS_Descr As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                sqlpJDS_Descr.Value = Session("gdtSub").Rows(iIndex)("Name")
                cmd.Parameters.Add(sqlpJDS_Descr)

                Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbJDS_DRCR.Value = p_crdr
                cmd.Parameters.Add(sqlpbJDS_DRCR)

                Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpJDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJDS_bPOSTED)

                Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbJDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJDS_BDELETED)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If Session("datamode") = "add" Then
                    sqlpbEdit.Value = False
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""

                If iReturnvalue <> 0 Then
                    Exit For
                End If
                cmd.Parameters.Clear()
            End If
        Next
        Return iReturnvalue
    End Function


    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("gdtSub").Rows.Count - 1
            If Session("gdtSub").Rows(iIndex)("voucherid") = p_voucherid And Session("gdtSub").Rows(iIndex)("costcenter") = p_costid And Session("gdtSub").Rows(iIndex)("Status") & "" <> "Deleted" Then
                dTotal = dTotal + Session("gdtSub").Rows(iIndex)("amount")
            End If
        Next
        If dTotal = p_total Then
            Return True
        Else
            Return False
        End If

    End Function


    Protected Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        If IsNumeric(txtMonthInterestStart.Text) = False Then
            txtMonthInterestStart.Text = "0"
        End If
        If (txtPaidto.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Select The Supplier" & "<br>"
        End If

        If (IsNumeric(txtAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If (optControlAccYes.Checked = True) Then
            If (txtIntRate.Text = "") Then
                lstrErrMsg = lstrErrMsg & "Enter The Interest Rate" & "<br>"
            End If

        End If

        If (IsNumeric(txtMnths.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Months" & "<br>"
        End If

        If (IsNumeric(txtMIntr.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Month Interval" & "<br>"
        End If

        If (txtChqBook.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter The Cheque No" & "<br>"
        End If

        If Trim(txtChqDate.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the cheque date " & "<br>"
        End If

        'If GetAccountCUDetails(txtPaidto.Text) Then
        '    If txtExpCode.Text = "" Then
        '        lstrErrMsg = "Please Specify CostUnit/Expense Details"

        '    End If
        '    If txtTo.Text = "" Then
        '        lstrErrMsg = "Please Specify CostUnit ToDate"

        '    End If
        '    If DateDiff("m", Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text)) < 2 Then
        '        lstrErrMsg = "CostUnit ToDate must be 2 months greater than FromDate"

        '    End If
        '    If IsDate(txtTo.Text) = False Then
        '        lstrErrMsg = "Invalid CostUnit ToDate"

        '    End If
        'End If
        ''''
        Dim strfDate As String = txtChqDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lstrErrMsg = lstrErrMsg & str_err & "<br>"
        Else
            txtChqDate.Text = strfDate
        End If
        ''''
        If (lstrErrMsg <> "") Then
            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
        End If
        If (lstrErrMsg <> "") Then Exit Sub
        CalcuInst()

    End Sub


    Private Sub CalcuInst()
        If Session("datamode") = "view" Then Exit Sub
        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If (txtPaidto.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Select The Supplier" & "<br>"
        End If
        If IsNumeric(txtMonthInterestStart.Text) = False Then
            txtMonthInterestStart.Text = "0"
        End If
        If (IsNumeric(txtAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If
        If (optControlAccYes.Checked = True) Then
            If (txtIntRate.Text = "") Then
                lstrErrMsg = lstrErrMsg & "Enter The Interest Rate" & "<br>"
            End If

        End If
        If (IsNumeric(txtMnths.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Months" & "<br>"
        End If
        If (IsNumeric(txtMIntr.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Month Interval" & "<br>"
        End If
        If (txtChqBook.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter The Cheque No" & "<br>"
        End If
        If Trim(txtChqDate.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the cheque date " & "<br>"
        End If
        If (lstrErrMsg <> "") Then Exit Sub
        ''''COST CENTER VERIFICATION  '''''FIND ACCOUNT IS THERE

        Dim bool_cost_center_reqired As Boolean = False
        Dim str_cost_center As String = ""
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtNewPartycode.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                hCostReqd.Value = bool_cost_center_reqired
            End If
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
        '''''FIND ACCOUNT IS THERE

        Dim ldrNew As DataRow
        Dim ldblAmount, ldblTotPayAmount, ldblBalDue As Decimal
        Dim ldblTotalInterest, ldblMonthlyInterest As Decimal
        Dim ldblMonthlyAmount As Decimal
        Dim ldblAdjustInterest As Decimal = 0

        Dim lstrChqDet, lstrChqBookId, lstrChqBookLotNo As String
        Dim lintMonths, lintChqs, lintChqCount, lintMaxChqs As Integer
        Dim iCountRemMonth, i, lintMonthAfter As Integer
        Dim ldblSuminterest As Decimal = 0
        Dim dt As Date
        Dim ldblAdjustrate As Decimal = 0
        ldblAmount = Convert.ToDecimal(txtAmount.Text)
        lintMonthAfter = CInt(txtMonthInterestStart.Text)
        lintMonths = Convert.ToInt32(txtMnths.Text)

        If optControlAccYes.Checked Then
            ldblTotalInterest = Convert.ToDecimal(txtIntRate.Text)
            ldblTotalInterest = ((ldblAmount * ldblTotalInterest) / 100) / 12 * (lintMonths - lintMonthAfter) * CInt(txtMIntr.Text) 'monthly
            ldblMonthlyInterest = Math.Round(ldblTotalInterest / lintMonths, 9)

        ElseIf optControlAccYes.Checked = False Then
            If cmbCalcu.SelectedItem.Value = "F" Then
                ldblTotalInterest = 0
                lintMonthAfter = 0
                txtMonthInterestStart.Text = "0"
                ldblMonthlyInterest = 0
            End If
        End If
        iCountRemMonth = lintMonthAfter

        If cmbCalcu.SelectedItem.Value = "F" Then
            ldblMonthlyAmount = Math.Round(ldblAmount / lintMonths + ldblMonthlyInterest, 9)
            If rbEqual.Checked Or rbHigherRO.Checked Then
                txtMInst.Text = Math.Ceiling(ldblMonthlyAmount)
                ldblAdjustrate = Math.Ceiling(ldblMonthlyAmount) - ldblMonthlyAmount
            Else
                txtMInst.Text = Math.Floor(ldblMonthlyAmount)
                ldblAdjustrate = Math.Floor(ldblMonthlyAmount) - ldblMonthlyAmount
            End If
        Else
            ''''Diminsihing balance
            ldblMonthlyAmount = Math.Round(-1 * Microsoft.VisualBasic.Pmt(Convert.ToDecimal(txtIntRate.Text) / 12 / 100, lintMonths * CInt(txtMIntr.Text), ldblAmount), 9)
            If rbHigherRO.Checked Or rbEqual.Checked Then
                txtMInst.Text = Math.Ceiling(ldblMonthlyAmount)
            Else
                txtMInst.Text = Math.Floor(ldblMonthlyAmount)
            End If
        End If

        ldblTotPayAmount = ldblAmount + ldblTotalInterest
        ldblBalDue = ldblAmount
        Session("dtDTL") = DataTables.CreateDataTable_PDC()

        lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False)
        lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
        lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
        lstrChqBookId = lstrChqDet.Split("|")(2)
        lstrChqBookLotNo = lstrChqDet.Split("|")(3)
        lintChqCount = 0

        For i = 1 To lintMonths
            lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), i, False)
            If lstrChqDet = "" Then
                lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), i, True)
            ElseIf lstrChqDet = "INSUFFICIENT LOT" Then
                'tr_errLNE.Visible = True
                'tr_errLNE.Visible = True
                lblError.Text = "Insufficient ChqBook Lot(s)"
                Exit Sub
            End If
            If lstrChqDet = "" Or lstrChqDet = "INSUFFICIENT LOT" Then
                lblError.Text = "Insufficient ChqBook Lot(s)"
                Exit Sub
            End If

            ldrNew = Session("dtDTL").NewRow
            ldrNew("Id") = i
            If cmbCalcu.SelectedItem.Value = "F" Then

                If rbEqual.Checked Or rbHigherRO.Checked Then
                    ldrNew("PayAmount") = Math.Ceiling(ldblMonthlyAmount)
                    If i = lintMonths Then
                        If Not rbEqual.Checked Then
                            ldrNew("PayAmount") = Math.Ceiling(ldblMonthlyAmount) - ldblAdjustrate * lintMonths
                        End If
                    End If
                Else
                    ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount)
                    If i = lintMonths Then
                        ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount) - ldblAdjustrate * lintMonths
                    End If
                End If

            Else
                '''' diminishing 
                If rbHigherRO.Checked Or rbEqual.Checked Then
                    ldrNew("PayAmount") = Math.Ceiling(ldblMonthlyAmount)
                Else
                    ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount)
                End If

            End If
            'If GetAccountCUDetails(txtPaidto.Text) Then
            '    ldrNew("CostUnit") = hfId1.Value
            '    ldrNew("FromDate") = txtFrom.Text
            '    ldrNew("ToDate") = txtTo.Text
            '    ldrNew("ExpAcc") = txtExpCode.Text
            '    ldrNew("ExpAccName") = txtPreAcc.Text
            'Else

            '    ldrNew("CostUnit") = ""
            '    ldrNew("FromDate") = ""
            '    ldrNew("ToDate") = ""
            '    ldrNew("ExpAcc") = ""
            '    ldrNew("ExpAccName") = ""
            'End If


            ldrNew("ChqBookId") = lstrChqDet.Split("|")(0)
            ldrNew("ChqBookLot") = lstrChqDet.Split("|")(1)
            ldrNew("ChqNo") = lstrChqDet.Split("|")(2)
            If Session("chqNos") Is Nothing Then
                Session("chqNos") = New Hashtable
            End If
            Session("chqNos")(CInt(ldrNew("Id"))) = txtChqNo.Text

            dt = DateAdd(DateInterval.Month, Convert.ToInt32(txtMIntr.Text) * (i - 1), Convert.ToDateTime(txtChqDate.Text))
            ldrNew("ChqDate") = dt.ToString("dd/MMM/yyyy")
            ldrNew("Ply") = str_cost_center

            ldrNew("CostReqd") = bool_cost_center_reqired
            If cmbCalcu.SelectedItem.Value = "F" Then
                If optControlAccYes.Checked Then
                    If iCountRemMonth > 0 Then
                        ldrNew("Interest") = 0
                        iCountRemMonth = iCountRemMonth - 1
                    Else
                        ldrNew("Interest") = Math.Round(ldblTotalInterest / (lintMonths - lintMonthAfter), 9) + ldblAdjustrate
                        If ldrNew("Interest") < 0 Then
                            ldrNew("Interest") = 0
                        End If
                    End If
                Else
                    ldrNew("Interest") = 0
                End If

                If i = lintMonths Then
                    If optControlAccYes.Checked Then
                        If rbEqual.Checked Then
                            txtAdj.Text = ldblAdjustrate * lintMonths
                            'ldrNew("Interest") = ldrNew("Interest") - ldblAdjustrate * lintMonths
                        Else
                            ldrNew("Interest") = ldrNew("Interest") - ldblAdjustrate * lintMonths
                        End If
                    Else
                        ldrNew("Interest") = 0
                    End If

                End If
                ldrNew("AmtWOInterest") = ldblMonthlyAmount - ldrNew("Interest")
                ldblBalDue = ldblBalDue - ldrNew("AmtWOInterest")
                ldrNew("BalDue") = Math.Round(ldblBalDue, 9)
                If ldrNew("Interest") < 0 Then
                    ldrNew("Interest") = 0
                End If

                ldblSuminterest = ldblSuminterest + ldrNew("Interest")
                txtGridInterest.Text = ldblSuminterest
                'If i = lintMonths And rbEqual.Checked Then
                '    ldrNew("Interest") = ldblAdjustrate * lintMonths + ldrNew("Interest")
                'End If
            Else
                ''diminishing
                If iCountRemMonth > 0 Then
                    ldrNew("Interest") = 0
                    iCountRemMonth = iCountRemMonth - 1
                Else
                    ldrNew("Interest") = Math.Round(ldblBalDue * Convert.ToDecimal(txtIntRate.Text) / 100 / 12, 9)
                End If
                If ldrNew("Interest") < 0 Then
                    ldrNew("Interest") = 0
                End If
                ldrNew("AmtWOInterest") = ldblMonthlyAmount - ldrNew("Interest")
                If (i = lintMonths) Then
                    If rbHigherRO.Checked Or rbEqual.Checked Then
                        Dim ldblRoundBalance = lintMonths * (Math.Ceiling(ldblMonthlyAmount) - ldblMonthlyAmount)
                    Else
                        Dim ldblRoundBalance = lintMonths * (Math.Floor(ldblMonthlyAmount) - ldblMonthlyAmount)
                    End If
                End If
                'ldrNew("Interest") = Math.Round(ldrNew("Interest"), 2)
                ldblSuminterest = ldblSuminterest + ldrNew("Interest")
                ldblBalDue = ldblBalDue - ldrNew("AmtWOInterest")
                ldrNew("BalDue") = Math.Round(ldblBalDue, 9)
                ldblAdjustInterest = ldblAdjustInterest + (ldrNew("Interest") - Math.Truncate(ldrNew("Interest") * 100) / 100)

                If i = lintMonths Then
                    Dim ldblBalanceInterest As Decimal
                    ldblBalanceInterest = ((ldrNew("PayAmount") * lintMonths) - ldblAmount) - ldblSuminterest
                    If rbEqual.Checked = False Then
                        If ldblBalanceInterest > 0 Then
                            ldrNew("PayAmount") = Math.Ceiling(ldrNew("PayAmount") - ldblBalanceInterest)
                        Else
                            ldrNew("PayAmount") = Math.Ceiling(ldrNew("PayAmount") - ldblBalanceInterest)
                        End If
                    Else
                        txtAdj.Text = Math.Round(ldblBalanceInterest, 2)
                        ldrNew("Interest") = ldrNew("Interest") + ldblBalanceInterest
                        ldrNew("AmtWOInterest") = ldrNew("AmtWOInterest") - ldblBalanceInterest
                    End If
                    ldrNew("Interest") = ldblAdjustInterest + ldrNew("Interest")
                End If
            End If

            Session("dtDTL").Rows.Add(ldrNew)
            FillLotNo()
        Next
        txtGridInterest.Text = Math.Ceiling(ldblSuminterest)
        txtBankTotal.Text = Math.Ceiling(ldblSuminterest) + ldblAmount
        gvDTL.DataSource = Session("dtDTL")
        gvDTL.DataBind()
    End Sub


    Protected Function GetNextChqNo(ByVal pId As Integer, ByVal pItr As Integer, ByVal pGetNext As Boolean) As String
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim lstrChqNo As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "SELECT CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If

        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_PREV_CHB_ID='" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If
        If pId = 0 Then
            Return "INSUFFICIENT LOT"
        End If



        Dim str_Sql As String

        If pItr = 1 Then
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 ORDER BY CHD_NO"
        Else
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 AND CHD_NO NOT IN (" & Mid(lstrUsedChqNos, 2) & ") ORDER BY CHD_NO"
        End If

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lstrChqBookId = ds.Tables(0).Rows(0)(0)
            hCheqBook.Value = lstrChqBookId
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(1)
            lstrChqNo = ds.Tables(0).Rows(0)(2)
            lstrUsedChqNos = lstrUsedChqNos & "," & lstrChqNo
            Return Convert.ToString(lstrChqBookId) & "|" & Convert.ToString(lstrChqBookLotNo) & "|" & lstrChqNo
        Else
            Return ""
        End If



    End Function


    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If
        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If
        Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                'tr_errLNE.Visible = True
                lblError.Text = getErrorMessage(str_)
            Else
                'tr_errLNE.Visible = True
                lblError.Text = "Did not get lock"
            End If
        Else
            Session("dtSettle") = DataTables.CreateDataTable_Settle
            Session("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            imgDocdate.Enabled = False
        End If
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        Call Clear_Header()
        Call Clear_Details()
        Session("datamode") = "add"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Session("gintGridLine") = 1
        GridInitialize()
        Session("gDtlDataMode") = "ADD"
        Session("dtDTL") = DataTables.CreateDataTable_PDC()

        Session("dtSettle") = DataTables.CreateDataTable_Settle
        Session("gdtSub").rows.clear()
        txtdocDate.Text = Format(Session("EntryDate"), "dd/MMM/yyyy")
        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        imgDocdate.Enabled = True
        bind_Currency()
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Session("datamode") = "add" Or Session("datamode") = "edit" Then
            unlock()
            Call Clear_Details()
            Session("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim Status As Integer
        Try
            '   --- CALL A GENERAL FUNCTION TO DELETE BY PASSING THE GUID AND TRAN TYPE ---
            Status = VoucherFunctions.DeleteVOUCHER(Session("BANKTRAN"), Session("sBsuid"), Session("Sub_ID"), Session("sUsr_name"), Session("Eid"))
            If Status <> 0 Then
                lblError.Text = (UtilityObj.getErrorMessage(Status))
                Exit Sub
            Else
                Status = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "delete", Page.User.Identity.Name.ToString, Me.Page)
                If Status <> 0 Then
                    Throw New ArgumentException("Could not complete your request")
                End If
                Call Clear_Details()
                lblError.Text = "Record Deleted Successfully"
            End If
        Catch myex As ArgumentException
            lblError.Text = "Record could not be Deleted"
            Errorlog(myex.Message)
        Catch ex As Exception
            lblError.Text = "Record could not be Deleted"
            Errorlog(ex.Message, Page.Title)
        End Try
        Session("datamode") = "none"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
    End Sub


    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer
            Dim IntVHDID As Integer

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.VHH_DOCDT, 106), ' ', '/') as DocDate ,B.CHB_LOTNO,isNULL(A.VHH_REFNO,'') as OldDocNo FROM VOUCHER_H A LEFT OUTER JOIN CHQBOOK_M B ON A.VHH_BSU_ID=CHB_BSU_ID AND " _
                       & " A.VHH_CHB_ID=B.CHB_ID WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)
            'btnSettle.Visible = True
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")

                txtOldDocNo.Text = ds.Tables(0).Rows(0)("OldDocNo")
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("VHH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtReceivedBy.Text = ds.Tables(0).Rows(0)("VHH_RECEIVEDBY").ToString
                optControlAccYes.Checked = ds.Tables(0).Rows(0)("VHH_bINTEREST")
                txtExchRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                txtBankCode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID") & ""
                txtBankDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ACT_ID")) & ""
                txtPaidto.Text = ds.Tables(0).Rows(0)("VHH_PARTY_ACT_ID") & ""
                txtPaidDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_PARTY_ACT_ID")) & ""
                If (optControlAccYes.Checked = True) Then
                    txtIntr.Text = ds.Tables(0).Rows(0)("VHH_INT_ACT_ID") & ""
                    txtIntrDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_INT_ACT_ID") & "")
                    txtAcrd.Text = ds.Tables(0).Rows(0)("VHH_ACRU_INT_ACT_ID") & ""
                    txtAcrdDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ACRU_INT_ACT_ID") & "")
                End If

                txtPrepd.Text = ds.Tables(0).Rows(0)("VHH_PROV_ACT_ID") & ""
                txtPrepdDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_PROV_ACT_ID") & "")
                txtChqiss.Text = ds.Tables(0).Rows(0)("VHH_CHQ_pdc_ACT_ID") & ""
                txtChqissDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_CHQ_pdc_ACT_ID") & "")

                txtNewPartycode.Text = ds.Tables(0).Rows(0)("VHH_ISSUEDTO") & ""
                If ds.Tables(0).Rows(0)("VHH_ISSUEDTO") & "" <> "" Then
                    txtNewPartyname.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ISSUEDTO") & "")

                End If


                txtIntRate.Text = ds.Tables(0).Rows(0)("VHH_INTPERCT") & ""
                For i = 0 To cmbCalcu.Items.Count - 1
                    If cmbCalcu.Items(i).Value = ds.Tables(0).Rows(0)("VHH_CALCTYP") & "" Then
                        cmbCalcu.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtMnths.Text = ds.Tables(0).Rows(0)("VHH_NOOFINST") & ""
                txtMIntr.Text = ds.Tables(0).Rows(0)("VHH_MONTHINTERVEL") & ""
                txtChqBook.Text = ds.Tables(0).Rows(0)("CHB_LOTNO") & ""
                hCheqBook.Value = ds.Tables(0).Rows(0)("VHH_CHB_ID") & ""

                ' txtChqDate.Text = Format(ds.Tables(0).Rows(0)("VHH_CHQDT") & "", "dd/MMM/yyyy")
                txtAmount.Text = AccountFunctions.Round(ds.Tables(0).Rows(0)("VHH_Amount"))

                'If GetAccountCUDetails(txtPaidto.Text) Then
                '    lnkCostUnit.Enabled = True
                '    lnkCostUnit.Visible = True
                'Else
                '    lnkCostUnit.Enabled = False
                '    lnkCostUnit.Visible = False
                'End If
                '   --- Initialize The Grid With The Data From The Detail Table
                lstrSQL2 = "SELECT A.VHD_ID,A.VHD_LINEID as Id,A.VHD_AMOUNT as PayAmount,(VHD_AMOUNT-VHD_INTEREST) as AmtWOInterest, VHD_INTEREST as Interest,A.VHD_OPBAL as BalDue,A.VHD_CHQID as ChqBookId,B.CHB_LOTNO as ChqBookLot,A.VHD_CHQNO as ChqNo," _
                            & " REPLACE(CONVERT(VARCHAR(11), A.VHD_CHQDT, 106), ' ', '/') as  ChqDate,A.GUID,'' as DELETED,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM VOUCHER_D A LEFT OUTER JOIN CHQBOOK_M B ON A.VHD_BSU_ID=CHB_BSU_ID AND " _
                            & " A.VHD_CHQID=B.CHB_ID INNER JOIN vw_OSA_ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID" _
                            & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' order by vhd_id "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("dtDTL") = DataTables.CreateDataTable_PDC()
                If ds2.Tables(0).Rows.Count > 0 Then
                    For iIndex As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        Dim rDt As DataRow
                        IntVHDID = ds2.Tables(0).Rows(iIndex)("VHD_ID")
                        rDt = Session("dtDTL").NewRow
                        rDt("Id") = ds2.Tables(0).Rows(iIndex)("Id")
                        rDt("PayAmount") = ds2.Tables(0).Rows(iIndex)("PayAmount")
                        rDt("BalDue") = ds2.Tables(0).Rows(iIndex)("BalDue")
                        rDt("ChqBookId") = ds2.Tables(0).Rows(iIndex)("ChqBookId")
                        rDt("ChqBookLot") = ds2.Tables(0).Rows(iIndex)("ChqBookId")
                        rDt("ChqNo") = ds2.Tables(0).Rows(iIndex)("ChqNo")
                        rDt("ChqDate") = ds2.Tables(0).Rows(iIndex)("ChqDate")
                        rDt("GUID") = ds2.Tables(0).Rows(iIndex)("GUID")
                        rDt("DELETED") = ""
                        rDt("Ply") = ds2.Tables(0).Rows(iIndex)("Ply")
                        rDt("CostReqd") = ds2.Tables(0).Rows(iIndex)("CostReqd")
                        rDt("AmtWOInterest") = ds2.Tables(0).Rows(iIndex)("AmtWOInterest")
                        rDt("Interest") = ds2.Tables(0).Rows(iIndex)("Interest")

                        'If IntVHDID > 0 Then
                        '    Dim strSql As String
                        '    Dim ds3 As DataSet
                        '    strSql = "SELECT VS.VSB_DTFROM as FromDate,VS.VSB_DTTO as ToDate,CU.CUT_DESCR as CostUnit,CU.CUT_EXP_ACT_ID as ExpAcc,AM.ACT_NAME as ExpAccName FROM VOUCHER_D_SUB AS VS INNER JOIN COSTUNIT_M AS CU ON VS.VSB_CUT_ID=CU.CUT_ID" _
                        '                    & " INNER JOIN ACCOUNTS_M AS AM ON CU.CUT_EXP_ACT_ID=AM.ACT_ID WHERE VS.VSB_VHD_ID=" & IntVHDID & ""
                        '    ds3 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, strSql)
                        '    If ds3.Tables(0).Rows.Count > 0 Then
                        '        rDt("FromDate") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("FromDate")) = False, ds3.Tables(0).Rows(0)("FromDate"), "")
                        '        rDt("ToDate") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ToDate")) = False, ds3.Tables(0).Rows(0)("ToDate"), "")
                        '        rDt("CostUnit") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("CostUnit")) = False, ds3.Tables(0).Rows(0)("CostUnit"), "")
                        '        rDt("ExpAcc") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ExpAcc")) = False, ds3.Tables(0).Rows(0)("ExpAcc"), "")
                        '        rDt("ExpAccName") = IIf(IsDBNull(ds3.Tables(0).Rows(0)("ExpAccName")) = False, ds3.Tables(0).Rows(0)("ExpAccName"), "")

                        '    Else
                        '        rDt("FromDate") = ""
                        '        rDt("ToDate") = ""
                        '        rDt("CostUnit") = ""
                        '        rDt("ExpAcc") = ""
                        '        rDt("ExpAccName") = ""

                        '    End If

                        'End If
                        Session("dtDTL").Rows.Add(rDt)
                    Next
                End If
                'Session("dtDTL") = ds2.Tables(0)
                'gvDTL.DataSource = ds2
                gvDTL.DataSource = Session("dtDTL")
                gvDTL.DataBind()
                Dim ppalAmt As Decimal = 0
                Dim totInterest As Decimal = 0

                For iIndex As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                    ppalAmt = ppalAmt + ds2.Tables(0).Rows(iIndex)("AmtWOInterest") 'Interest
                    totInterest = totInterest + ds2.Tables(0).Rows(iIndex)("Interest")
                Next
                txtGridInterest.Text = Math.Ceiling(totInterest)
                txtBankTotal.Text = Math.Round(ppalAmt + totInterest, 0)
                txtAmount.Text = Math.Round(ppalAmt, 0)
                lstrSQL2 = "SELECT VHD_AMOUNT as Amount,VHD_CHQID as CHQID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM VOUCHER_D A INNER JOIN vw_OSA_ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID" _
                         & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                hCheqBook.Value = ds2.Tables(0).Rows(0)("CHQID") & ""
                hPLY.Value = ds2.Tables(0).Rows(0)("Ply") & ""
                hCostReqd.Value = ds2.Tables(0).Rows(0)("CostReqd") & ""
                txtMInst.Text = AccountFunctions.Round(ds2.Tables(0).Rows(0)("Amount"))

                ' - - - - Initalize the Cost Center Grid
                Session("gdtSub") = CostCenterFunctions.CreateDataTableCostCenter()
                lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status ,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' AND VDS_CODE IS NOT NULL " _
                            & " UNION ALL SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,'OTH' as Costcenter,A.VDS_CCS_ID as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' AND VDS_CODE IS NULL "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("gdtSub") = ds2.Tables(0)

            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & Session("F_YEAR") & " and VHH_DOCTYPE = '" & Session("BANKTRAN") & "' and VHH_DOCNO = '" & txtdocNo.Text _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtdocDate.Text)) & "' and VHH_BSU_ID in('" & Session("sBsuid") & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            '"' as DETAILACCOUNT ,VHD_AMOUNT,VHD_CHQDT  from VOUCHER_D" _
            If chkPrintChq.Checked Then
                cmd.CommandText = "Select VHD_CHQNO VHD_CHQID,case when  isnull( VHH_RECEIVEDBY ,'')='' then ACT_NAME else    VHH_RECEIVEDBY end as DETAILACCOUNT,  VOUCHER_D.GUID, VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_CHQDT, " _
                & "CURRENCY_M.CUR_DENOMINATION FROM VOUCHER_D INNER JOIN " _
                & "VOUCHER_H ON VOUCHER_D.VHD_SUB_ID = VOUCHER_H.VHH_SUB_ID AND" _
                & " VOUCHER_D.VHD_BSU_ID = VOUCHER_H.VHH_BSU_ID AND " _
                & "VOUCHER_D.VHD_FYEAR = VOUCHER_H.VHH_FYEAR AND " _
                & "VOUCHER_D.VHD_DOCTYPE = VOUCHER_H.VHH_DOCTYPE AND " _
                & "VOUCHER_D.VHD_DOCNO = VOUCHER_H.VHH_DOCNO INNER JOIN " _
                & "CURRENCY_M ON VOUCHER_H.VHH_CUR_ID = CURRENCY_M.CUR_ID INNER JOIN ACCOUNTS_M ON VHD_ACT_ID=ACT_ID" _
                & " WHERE VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & _
                "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & _
                "'   AND VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND VHD_DOCNO='" _
                & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' <%FILTER%>"

                str_Sql = "SELECT ACT_RECONFORMAT FROM ACCOUNTS_M WHERE  ACT_ID = '" & txtBankCode.Text & "'"
                Dim str_bank As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
                Select Case str_bank.ToUpper()
                    Case "NBD"
                        repSource.Command = cmd
                        repSource.ResourceName = "../RPT_Files/BankPaymentChq_NBD.rpt"
                        Session("ReportSource") = repSource
                        Response.Redirect("accChqPrint.aspx?ChequePrint=PDC", True)
                        'Response.Redirect("../Reports/ASPX Report/printreport.aspx", True)
                    Case "MSRQ"
                        repSource.Command = cmd
                        repSource.ResourceName = "../RPT_Files/BankPaymentChq_MASHREQ.rpt"
                        Session("ReportSource") = repSource
                        Response.Redirect("accChqPrint.aspx?ChequePrint=PDC", True)
                        'Response.Redirect("../Reports/ASPX Report/printreport.aspx", True)
                    Case Else
                        Response.Write("<script language='javascript'>alert('Cheque Format not supported for printing')</script>")
                End Select
            Else
                repSource = VoucherReports.PDCVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "QP", txtdocNo.Text, False, Session("HideCC"))
                Session("ReportSource") = repSource
                '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Protected Sub optControlAccYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccYes.CheckedChanged
        CalcuInst()
    End Sub


    Protected Sub optControlAccNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optControlAccNo.CheckedChanged
        CalcuInst()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim lintIndex As Integer
        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtNewChqBook.Text And Session("dtDTL").Rows(lintIndex)("ChqNo") = txtNewChqNo.Text) Then
                'tr_errLNE.Visible = True
                lblError.Text = "Cannot Update.Cheque No Already In Use"
                Exit Sub
            End If
        Next
        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                Session("dtDTL").Rows(lintIndex)("ChqBookId") = hCheqBook.Value
                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtNewChqBook.Text
                Session("dtDTL").Rows(lintIndex)("ChqNo") = txtNewChqNo.Text
                txtChqBook.Text = txtNewChqBook.Text
                txtChqNo.Text = txtNewChqNo.Text
                tr_Add.Visible = True
                tr_Update.Visible = False
                If Session("chqNos") Is Nothing Then
                    Session("chqNos") = New Hashtable
                End If
                Session("chqNos")(CInt(Session("gintEditLine"))) = txtChqNo.Text

                Exit For

            End If
        Next
        gvDTL.DataSource = Session("dtDTL")
        gvDTL.DataBind()
        gvDTL.SelectedIndex = -1
    End Sub


    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCancel.Click
        tr_Add.Visible = True
        tr_Update.Visible = False
        gvDTL.SelectedIndex = -1
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDocdate.Click
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()

        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
    End Sub


    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
    End Sub


    Protected Sub cmbCalcu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCalcu.SelectedIndexChanged
        If cmbCalcu.SelectedItem.Value = "R" Then
            optControlAccNo.Checked = False
            optControlAccYes.Checked = True
            optControlAccNo.Enabled = False
            optControlAccYes.Enabled = False
        Else
            optControlAccNo.Enabled = True
            optControlAccYes.Enabled = True
        End If
        CalcuInst()
    End Sub


    Protected Sub btnSettle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSettle.Click
        Response.Redirect("accposOnlineSettlement.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&eid=" & Request.QueryString("Eid"))
    End Sub


    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
        FillLotNo()
    End Sub


    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub


    Protected Sub txtNewPartycode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewPartycode.TextChanged
        chk_newPartycode()
    End Sub


    Protected Sub imgParty_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgParty.Click
        chk_newPartycode()
    End Sub


    Protected Sub txtPaidto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPaidto.TextChanged
        chk_Paidto()
        Dim acttype As String = AccountFunctions.check_accounttype(txtPaidto.Text, Session("sBsuid"))
        If acttype = "S" Then
            set_AdvanceControls()
        Else
            gvDTL.Columns(14).Visible = False
        End If
        'If GetAccountCUDetails(txtPaidto.Text) Then
        '    lnkCostUnit.Enabled = True
        '    lnkCostUnit.Visible = True
        '    txtFrom.Text = txtdocDate.Text
        'Else
        '    lnkCostUnit.Enabled = False
        '    lnkCostUnit.Visible = False
        'End If

    End Sub


    Protected Sub imgPaidto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPaidto.Click
        chk_Paidto()
        Dim acttype As String = AccountFunctions.check_accounttype(txtPaidto.Text, Session("sBsuid"))
        If acttype = "S" Then
            set_AdvanceControls()
        Else
            gvDTL.Columns(14).Visible = False
        End If

    End Sub


    Protected Sub txtIntr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIntr.TextChanged
        chk_Interest()
    End Sub


    Protected Sub imgInterest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgInterest.Click
        chk_Interest()
        Dim acttype As String = AccountFunctions.check_accounttype(txtPaidto.Text, Session("sBsuid"))
        If acttype = "S" Then
            set_AdvanceControls()
        Else

        End If

    End Sub


    Protected Sub txtAcrd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAcrd.TextChanged
        chk_AcrdInterest()
    End Sub


    Protected Sub imgAcrdint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAcrdint.Click
        chk_AcrdInterest()
    End Sub


    Protected Sub txtPrepd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrepd.TextChanged
        chk_Prepaid()
    End Sub


    Protected Sub imgPrepaid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrepaid.Click
        chk_Prepaid()
    End Sub


    Protected Sub txtChqiss_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtChqiss.TextChanged
        chk_Chqiss()
    End Sub


    Protected Sub imgChqiss_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgChqiss.Click
        chk_Chqiss()
    End Sub


    Sub chk_newPartycode()
        txtNewPartyname.Text = AccountFunctions.Validate_Account(txtNewPartycode.Text, Session("sbsuid"), "NOTCC")
        If txtNewPartyname.Text = "" Then
            lblError.Text = "Invalid Account Selected (Party)"
            txtNewPartyname.Focus()
        Else
            lblError.Text = ""
            txtAmount.Focus()
        End If
    End Sub


    Sub chk_Paidto()
        lblPaymentTerm.Text = ""
        txtPaidDescr.Text = AccountFunctions.Validate_Account(txtPaidto.Text, Session("sbsuid"), "NOTCC")
        If txtPaidDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected (Paid to)"
            txtPaidto.Focus()
        Else
            Dim strPayTerm As String = AccountFunctions.GetPaymentTerm(txtPaidto.Text)
            If strPayTerm <> "" Then
                lblPaymentTerm.Text = "Payment Term : " & strPayTerm
                trPayTerm.Visible = True
            Else
                trPayTerm.Visible = False
            End If
            lblError.Text = ""
            txtIntr.Focus()
        End If
    End Sub


    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            txtPaidto.Focus()
        End If
    End Sub


    Sub chk_Interest()
        txtIntrDescr.Text = AccountFunctions.Validate_Account(txtIntr.Text, Session("sbsuid"), "INTRAC")
        If txtIntrDescr.Text = "" Then
            lblError.Text = "Invalid Interest Account Selected"
            txtIntr.Focus()
        Else
            lblError.Text = ""
            txtAcrd.Focus()
        End If
    End Sub


    Sub chk_AcrdInterest()
        txtAcrdDescr.Text = AccountFunctions.Validate_Account(txtAcrd.Text, Session("sbsuid"), "ACRDAC")
        If txtAcrdDescr.Text = "" Then
            lblError.Text = "Invalid Interest Account Selected"
            txtAcrd.Focus()
        Else
            lblError.Text = ""
            txtPrepd.Focus()
        End If
    End Sub


    Sub chk_Prepaid()
        txtPrepdDescr.Text = AccountFunctions.Validate_Account(txtPrepd.Text, Session("sbsuid"), "PREPDAC")
        If txtPrepdDescr.Text = "" Then
            lblError.Text = "Invalid Interest Account Selected"
            txtPrepd.Focus()
        Else
            lblError.Text = ""
            txtChqiss.Focus()
        End If
    End Sub


    Sub chk_Chqiss()
        txtChqissDescr.Text = AccountFunctions.Validate_Account(txtChqiss.Text, Session("sbsuid"), "CHQISSAC_PDC")
        If txtChqissDescr.Text = "" Then
            lblError.Text = "Invalid Check Issue account Selected"
            txtChqiss.Focus()
        Else
            lblError.Text = ""
            txtNewPartycode.Focus()
        End If
    End Sub


    Protected Sub chkAdvance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdvance.CheckedChanged
        set_AdvanceControls()
    End Sub


    Sub set_AdvanceControls()
        If chkAdvance.Checked = True Then
            gvDTL.Columns(14).Visible = False
            Session("dtSettle").Rows.Clear()
        Else
            gvDTL.Columns(14).Visible = True
        End If
    End Sub


    Protected Sub rbHigherRO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CalcuInst()
    End Sub


    Protected Sub rbLowerRO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CalcuInst()
    End Sub


    Protected Sub rbEqual_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CalcuInst()
    End Sub


    Protected Sub lbFillAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbFillAccounts.Click
        Dim str_sql As String = "SELECT  VHS_ID, VHS_DOCTYPE, VHS_DESCRIPTION, VHS_ACR_INT_ACT_ID, " _
        & " VHS_INT_ACT_ID, VHS_PREP_EXP_ACT_ID, VHS_CHQ_ISS_ACT_ID " _
        & " FROM VOUCHERSETUP_S where VHS_ID='" & hfAccountList.Value & "'"
        Dim ds As New DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtIntr.Text = ds.Tables(0).Rows(0)("VHS_INT_ACT_ID").ToString
            chk_Interest()
            txtAcrd.Text = ds.Tables(0).Rows(0)("VHS_ACR_INT_ACT_ID").ToString
            chk_AcrdInterest()
            txtChqiss.Text = ds.Tables(0).Rows(0)("VHS_CHQ_ISS_ACT_ID").ToString
            chk_Chqiss()
            txtPrepd.Text = ds.Tables(0).Rows(0)("VHS_PREP_EXP_ACT_ID").ToString
            chk_Prepaid()
        Else

        End If

    End Sub


    Protected Sub btView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        If Not Directory.Exists(str_img & "\temp") Then
            Directory.CreateDirectory(str_img & "\temp")
        End If
        Dim strFilepath As String = str_img & "\temp\"
        Dim intFileNameLength As Integer
        Dim strFileNamePath As String = ""
        Dim strFileNameOnly As String = ""

        If Not (FileUpload1.PostedFile Is Nothing) Then
            strFileNamePath = FileUpload1.PostedFile.FileName

            intFileNameLength = InStr(1, StrReverse(strFileNamePath), "\")

            strFileNameOnly = Mid(strFileNamePath, (Len(strFileNamePath) - intFileNameLength) + 2)


            'If File.Exists(paths & strFileNameOnly) Then
            'lblMessage.Text = "Image of Similar name already Exist,Choose other name"
            'Else
            If FileUpload1.PostedFile.ContentLength > 3340000 Then
                lblError.Text = "The Size of file is greater than 4 MB"
            ElseIf strFileNameOnly = "" Then
                Exit Sub
            Else
                strFileNameOnly = Session("sUsr_name") & "-" & Session("sBsuid") & "-" & Format(Date.Today, "dd/MMM/yyyy").Replace("/", "-") & "-" & AccountFunctions.GetRandomString() & ".xls"
                FileUpload1.PostedFile.SaveAs(strFilepath & strFileNameOnly)
                lblError.Text = "File Upload Success."
                Session("Img") = strFileNameOnly
            End If
        End If

        Dim excelDataset As New DataSet()


        Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & strFilepath & strFileNameOnly & ";" & _
        "Extended Properties=Excel 8.0;"

        ''You must use the $ after the object you reference in the spreadsheet
        Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(strFilepath & strFileNameOnly) & "]", strConn)
        myData.TableMappings.Add("Table", "ExcelTest")
        myData.Fill(excelDataset)


        CalcuInst_Excel(excelDataset.Tables(0))
    End Sub

    Private Sub CalcuInst_Excel(ByVal exceltable As DataTable)
        If Session("datamode") = "view" Then Exit Sub
        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If (txtPaidto.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Select The Supplier" & "<br>"
        End If
        If IsNumeric(txtMonthInterestStart.Text) = False Then
            txtMonthInterestStart.Text = "0"
        End If
        'If (IsNumeric(txtAmount.Text) = False) Then
        '    lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        'End If
        If (optControlAccYes.Checked = True) Then
            If (txtIntRate.Text = "") Then
                lstrErrMsg = lstrErrMsg & "Enter The Interest Rate" & "<br>"
            End If

        End If
        If (IsNumeric(txtMnths.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Months" & "<br>"
        End If
        If (IsNumeric(txtMIntr.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Month Interval" & "<br>"
        End If
        If (txtChqBook.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter The Cheque No" & "<br>"
        End If
        'If Trim(txtChqDate.Text) = "" Then
        '    lstrErrMsg = lstrErrMsg & "Enter the cheque date " & "<br>"
        'End If
        lblError.Text = lstrErrMsg
        If (lstrErrMsg <> "") Then Exit Sub
        ''''COST CENTER VERIFICATION  '''''FIND ACCOUNT IS THERE

        Dim bool_cost_center_reqired As Boolean = False
        Dim str_cost_center As String = ""
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtNewPartycode.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                hCostReqd.Value = bool_cost_center_reqired
            End If
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
        '''''FIND ACCOUNT IS THERE

        Dim ldrNew As DataRow
        'Dim ldblAmount, ldblTotPayAmount, ldblBalDue As Decimal
        'Dim ldblTotalInterest, ldblMonthlyInterest As Decimal
        'Dim ldblMonthlyAmount As Decimal
        'Dim ldblAdjustInterest As Decimal = 0

        Dim lstrChqDet, lstrChqBookId, lstrChqBookLotNo, lintChqs, lintChqCount, lintMaxChqs As String
        Dim i As Integer
        'Dim iCountRemMonth, lintMonths, lintMonthAfter As Integer
        Dim ldblSuminterest As Decimal = 0
        'Dim dt As Date
        'Dim ldblAdjustrate As Decimal = 0
        'ldblAmount = Convert.ToDecimal(txtAmount.Text)
        'lintMonthAfter = CInt(txtMonthInterestStart.Text)
        'lintMonths = Convert.ToInt32(txtMnths.Text)

        'If optControlAccYes.Checked Then
        '    ldblTotalInterest = Convert.ToDecimal(txtIntRate.Text)
        '    ldblTotalInterest = ((ldblAmount * ldblTotalInterest) / 100) / 12 * (lintMonths - lintMonthAfter) * CInt(txtMIntr.Text) 'monthly
        '    ldblMonthlyInterest = Math.Round(ldblTotalInterest / lintMonths, 9)

        'ElseIf optControlAccYes.Checked = False Then
        '    If cmbCalcu.SelectedItem.Value = "F" Then
        '        ldblTotalInterest = 0
        '        lintMonthAfter = 0
        '        txtMonthInterestStart.Text = "0"
        '        ldblMonthlyInterest = 0
        '    End If
        'End If
        'iCountRemMonth = lintMonthAfter

        'If cmbCalcu.SelectedItem.Value = "F" Then
        '    ldblMonthlyAmount = Math.Round(ldblAmount / lintMonths + ldblMonthlyInterest, 9)
        '    If rbEqual.Checked Or rbHigherRO.Checked Then
        '        txtMInst.Text = Math.Ceiling(ldblMonthlyAmount)
        '        ldblAdjustrate = Math.Ceiling(ldblMonthlyAmount) - ldblMonthlyAmount
        '    Else
        '        txtMInst.Text = Math.Floor(ldblMonthlyAmount)
        '        ldblAdjustrate = Math.Floor(ldblMonthlyAmount) - ldblMonthlyAmount
        '    End If
        'Else
        '    ''''Diminsihing balance
        '    ldblMonthlyAmount = Math.Round(-1 * Microsoft.VisualBasic.Pmt(Convert.ToDecimal(txtIntRate.Text) / 12 / 100, lintMonths * CInt(txtMIntr.Text), ldblAmount), 9)
        '    If rbHigherRO.Checked Or rbEqual.Checked Then
        '        txtMInst.Text = Math.Ceiling(ldblMonthlyAmount)
        '    Else
        '        txtMInst.Text = Math.Floor(ldblMonthlyAmount)
        '    End If
        'End If

        'ldblTotPayAmount = ldblAmount + ldblTotalInterest
        'ldblBalDue = ldblAmount
        Session("dtDTL") = DataTables.CreateDataTable_PDC()

        lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False)
        lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
        lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
        lstrChqBookId = lstrChqDet.Split("|")(2)
        lstrChqBookLotNo = lstrChqDet.Split("|")(3)
        lintChqCount = 0

        For i = 1 To exceltable.Rows.Count - 1

            If IsDate(exceltable.Rows(i - 1)(1)) Then

                If i = 1 Then
                    txtMInst.Text = exceltable.Rows(i - 1)(6)
                    txtAmount.Text = exceltable.Rows(i - 1)(3)
                    txtChqDate.Text = CDate(exceltable.Rows(i - 1)(1)).ToString("dd/MMM/yyyy")
                End If
                lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), i, False)
                If lstrChqDet = "" Then
                    lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), i, True)
                ElseIf lstrChqDet = "INSUFFICIENT LOT" Then
                    'tr_errLNE.Visible = True
                    'tr_errLNE.Visible = True
                    lblError.Text = "Insufficient ChqBook Lot(s)"
                    Exit Sub
                End If
                If lstrChqDet = "" Or lstrChqDet = "INSUFFICIENT LOT" Then
                    lblError.Text = "Insufficient ChqBook Lot(s)"
                    Exit Sub
                End If

                ldrNew = Session("dtDTL").NewRow
                ldrNew("Id") = i
                'If cmbCalcu.SelectedItem.Value = "F" Then

                '    If rbEqual.Checked Or rbHigherRO.Checked Then
                '        ldrNew("PayAmount") = Math.Ceiling(ldblMonthlyAmount)
                '        If i = lintMonths Then
                '            If Not rbEqual.Checked Then
                '                ldrNew("PayAmount") = Math.Ceiling(ldblMonthlyAmount) - ldblAdjustrate * lintMonths
                '            End If
                '        End If
                '    Else
                '        ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount)
                '        If i = lintMonths Then
                '            ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount) - ldblAdjustrate * lintMonths
                '        End If
                '    End If

                'Else
                '''' diminishing 
                'If rbHigherRO.Checked Or rbEqual.Checked Then
                ldrNew("PayAmount") = Math.Ceiling(exceltable.Rows(i - 1)(6))
                'Else
                '    ldrNew("PayAmount") = Math.Floor(ldblMonthlyAmount)
                'End If

                'End If

                ldrNew("ChqBookId") = lstrChqDet.Split("|")(0)
                ldrNew("ChqBookLot") = lstrChqDet.Split("|")(1)
                ldrNew("ChqNo") = lstrChqDet.Split("|")(2)
                'dt = DateAdd(DateInterval.Month, Convert.ToInt32(txtMIntr.Text) * (i - 1), Convert.ToDateTime(txtChqDate.Text))
                ldrNew("ChqDate") = CDate(exceltable.Rows(i - 1)(1)).ToString("dd/MMM/yyyy")
                ldrNew("Ply") = str_cost_center

                ldrNew("CostReqd") = bool_cost_center_reqired
                'If cmbCalcu.SelectedItem.Value = "F" Then
                '    'If optControlAccYes.Checked Then
                '    '    If iCountRemMonth > 0 Then
                '    '        ldrNew("Interest") = 0
                '    '        iCountRemMonth = iCountRemMonth - 1
                '    '    Else
                If IsNumeric(exceltable.Rows(i - 1)(4)) Then

                    ldrNew("Interest") = exceltable.Rows(i - 1)(4)
                Else

                    'If ldrNew("Interest") < 0 Then
                    ldrNew("Interest") = 0
                End If
                '    End If
                'Else
                '    ldrNew("Interest") = 0
                'End If

                'If i = lintMonths Then
                '    If optControlAccYes.Checked Then
                '        If rbEqual.Checked Then
                '            txtAdj.Text = ldblAdjustrate * lintMonths
                '            'ldrNew("Interest") = ldrNew("Interest") - ldblAdjustrate * lintMonths
                '        Else
                '            ldrNew("Interest") = ldrNew("Interest") - ldblAdjustrate * lintMonths
                '        End If
                '    Else
                '        ldrNew("Interest") = 0
                '    End If

                'End If
                ldrNew("AmtWOInterest") = exceltable.Rows(i - 1)(5)
                'ldblBalDue = ldblBalDue - ldrNew("AmtWOInterest")
                ldrNew("BalDue") = exceltable.Rows(i - 1)(7)
                'If ldrNew("Interest") < 0 Then
                '    ldrNew("Interest") = 0
                'End If

                ldblSuminterest = ldblSuminterest + ldrNew("Interest")
                'txtGridInterest.Text = ldblSuminterest
                'If i = lintMonths And rbEqual.Checked Then
                '    ldrNew("Interest") = ldblAdjustrate * lintMonths + ldrNew("Interest")
                'End If
                'Else
                ' ''diminishing
                'If iCountRemMonth > 0 Then
                '    ldrNew("Interest") = 0
                '    iCountRemMonth = iCountRemMonth - 1
                'Else
                '    ldrNew("Interest") = Math.Round(ldblBalDue * Convert.ToDecimal(txtIntRate.Text) / 100 / 12, 9)
                'End If
                'If ldrNew("Interest") < 0 Then
                '    ldrNew("Interest") = 0
                'End If
                'ldrNew("AmtWOInterest") = ldblMonthlyAmount - ldrNew("Interest")
                'If (i = lintMonths) Then
                '    If rbHigherRO.Checked Or rbEqual.Checked Then
                '        Dim ldblRoundBalance = lintMonths * (Math.Ceiling(ldblMonthlyAmount) - ldblMonthlyAmount)
                '    Else
                '        Dim ldblRoundBalance = lintMonths * (Math.Floor(ldblMonthlyAmount) - ldblMonthlyAmount)
                '    End If
                '    'End If
                '    ''ldrNew("Interest") = Math.Round(ldrNew("Interest"), 2)
                '    'ldblSuminterest = ldblSuminterest + ldrNew("Interest")
                '    'ldblBalDue = ldblBalDue - ldrNew("AmtWOInterest")
                '    'ldrNew("BalDue") = Math.Round(ldblBalDue, 9)
                '    'ldblAdjustInterest = ldblAdjustInterest + (ldrNew("Interest") - Math.Truncate(ldrNew("Interest") * 100) / 100)

                '    If i = lintMonths Then
                '        Dim ldblBalanceInterest As Decimal
                '        ldblBalanceInterest = ((ldrNew("PayAmount") * lintMonths) - ldblAmount) - ldblSuminterest
                '        If rbEqual.Checked = False Then
                '            If ldblBalanceInterest > 0 Then
                '                ldrNew("PayAmount") = Math.Ceiling(ldrNew("PayAmount") - ldblBalanceInterest)
                '            Else
                '                ldrNew("PayAmount") = Math.Ceiling(ldrNew("PayAmount") - ldblBalanceInterest)
                '            End If
                '        Else
                '            txtAdj.Text = Math.Round(ldblBalanceInterest, 2)
                '            ldrNew("Interest") = ldrNew("Interest") + ldblBalanceInterest
                '            ldrNew("AmtWOInterest") = ldrNew("AmtWOInterest") - ldblBalanceInterest
                '        End If
                '        ldrNew("Interest") = ldblAdjustInterest + ldrNew("Interest")
                '    End If
                'End If

                Session("dtDTL").Rows.Add(ldrNew)
            End If

        Next
        txtMnths.Text = Session("dtDTL").Rows.Count
        txtGridInterest.Text = Math.Ceiling(ldblSuminterest)
        txtBankTotal.Text = Math.Ceiling(ldblSuminterest) + CDbl(txtAmount.Text)
        gvDTL.DataSource = Session("dtDTL")
        gvDTL.DataBind()
    End Sub
    'Private Function GetAccountCUDetails(ByVal ACTID As String) As Boolean
    '    Dim strQuery As String
    '    Dim ds As DataSet
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '    Dim blnStatus As Boolean
    '    blnStatus = False
    '    strQuery = "SELECT * FROM ACCOUNTS_M WHERE ACT_ID='" & ACTID & "'"
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        If ds.Tables(0).Rows(0)("act_bprepayment") = True Then

    '            blnStatus = True
    '        End If

    '    End If
    '    Return blnStatus
    'End Function

    'Protected Sub txtTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim strfDate As String = txtTo.Text.Trim
    '    Dim str_err As String = DateFunctions.checkdate(strfDate)
    '    If str_err <> "" Then
    '        lblError.Text = str_err
    '        Exit Sub
    '    Else
    '        txtTo.Text = strfDate
    '    End If
    'End Sub

    'Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim strfDate As String = txtFrom.Text.Trim
    '    Dim str_err As String = DateFunctions.checkdate(strfDate)
    '    If str_err <> "" Then
    '        lblError.Text = str_err
    '        Exit Sub
    '    Else
    '        txtFrom.Text = strfDate
    '    End If
    'End Sub

End Class
