<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmStreamAllocationApproval.aspx.vb" Inherits="Curriculum_clmStreamAllocationApproval"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Student Stream Allocation</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">

                    <tr>
                        <td align="left" class="title">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table >

                                <tr class="matters" >


                                    <td style="color:blue;">REQUESTED :
                                    </td>
                                    <td align="left"   valign="top">
                                        <asp:DataList ID="dlRStreams" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" HeaderStyle-ForeColor="Blue">
                                            <ItemTemplate>
                                                <table>
                                                    <tr  >
                                                        <td class="matters"  >
                                                            <asp:LinkButton Style="text-decoration: none;  " ID="lnkRStream" runat="server"
                                                                Text='<%# Bind("STM_DESCR") %>'></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Visible="false" ID="lblRStmID" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="Div1" runat="server" class="panel-cover">
                                                    <div class="title-bg">
                                                        Subjects
                                                    </div>
                                                    <asp:Panel ID="PopupMenu1" runat="server" Height="25%" ScrollBars="Vertical"
                                                        Width="100%">
                                                        <asp:Literal ID="ltRProcess" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                </div>
                                                <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="Server" HoverCssClass="popupHover"
                                                    OffsetX="0" OffsetY="20" PopDelay="50" PopupControlID="Div1" PopupPosition="Center"
                                                    TargetControlID="lnkRStream">
                                                </ajaxToolkit:HoverMenuExtender>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>

                                    <td class="matters" style="color: Green">ALLOTED :
                                    </td>
                                    <td align="left" class="matters"   valign="top">
                                        <asp:DataList ID="dlStreams" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td class="matters">
                                                            <asp:LinkButton Style="text-decoration: none; color: Green" ID="lnkStream" runat="server"
                                                                Text='<%# Bind("STM_DESCR") %>'></asp:LinkButton>
                                                        </td>
                                                        <td style="text-decoration: none; color: Green">:
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Style="text-decoration: none; color: Green" ID="lblStAlloted"
                                                                Text='<%# Bind("STM_COUNT") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Visible="false" ID="lblStmID" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="popup" runat="server" class="panel-cover">
                                                    <div class="title-bg">
                                                        Subjects
                                                    </div>
                                                    <asp:Panel ID="PopupMenu" runat="server" CssClass="modalPopup1" Height="25%" ScrollBars="Vertical"
                                                        Width="100%">
                                                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                </div>
                                                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                                    OffsetX="200" OffsetY="20" PopDelay="50" PopupControlID="popup" PopupPosition="Left"
                                                    TargetControlID="lnkStream">
                                                </ajaxToolkit:HoverMenuExtender>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters"   valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnSearch">
                                <table id="tblTC" runat="server" width="100%">
                                    <tr>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Student ID</span>
                                        </td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:TextBox ID="txtStuNo" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Student Name</span>
                                        </td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:TextBox ID="txtName" runat="server">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Section</span>
                                        </td>
                                        <td align="left"  >
                                            <asp:DropDownList ID="ddlSection" runat="server">
                                            </asp:DropDownList>
                                        </td>

                                        <td align="left" class="matters"><span class="field-label">Science </span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtScience" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                     
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Maths </span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtMaths" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" class="matters"><span class="field-label">CGPA</span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtCGPA" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">English </span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtEnglish" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" class="matters"><span class="field-label">Social</span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtSocial" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">OverAll</span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtOverAll" runat="server"></asp:TextBox>
                                        </td>

                                        <td align="left" class="matters"><span class="field-label">Choice 1</span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlStream1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream1_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions1" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-row" OnRowDataBound="gvOptions_RowDataBound">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice1"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice1Option" runat="server">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Choice&nbsp; 2</span>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlStream2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream2_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions2" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-row" OnRowDataBound="gvOptions_RowDataBound">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice2"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice2Option" runat="server">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                        <td align="left" class="matters"><span class="field-label">Choice 3</span>
                                        </td>
                                        <td   align="left">
                                            <asp:DropDownList ID="ddlStream3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream3_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions3" runat="server" AutoGenerateColumns="False"   CssClass="table table-bordered table-row" OnRowDataBound="gvOptions_RowDataBound">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice3"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice3Option" runat="server">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                        <td align="left" class="matters"  ><span class="field-label">Show Records</span>
                        </td>
                        <td align="left" class="matters"  >
                            <asp:TextBox ID="txtRecords" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" class="matters"  >
                            <table border="0">
                                <tr>
                                    <td class="matters">
                                        <asp:RadioButton ID="rdAll" runat="server" Text="All" GroupName="g1" CssClass="field-label"></asp:RadioButton>
                                    </td>
                                    <td class="matters">
                                        <asp:RadioButton ID="rdAllocated" runat="server" Text="Allotted" GroupName="g1" CssClass="field-label"></asp:RadioButton>
                                    </td>
                                    <td class="matters">
                                        <asp:RadioButton ID="rdNotAllocated" runat="server" GroupName="g1" Text="Not Allotted" CssClass="field-label"
                                            Checked="True"></asp:RadioButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="matters"  >
                                <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4"   align="left">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <table>
                                <tr>
                                    <td class="matters"><span class="field-label">Allocate to Stream &amp; Option</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAllocatedStream" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAllocatedStream_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2">
                                        <asp:GridView ID="gvAllocatedOption" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" OnRowDataBound="gvAllocatedOptions_RowDataBound">
                                            <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Option">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlAllocatedOption" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OptId" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4" valign="top" style="text-align:center">
                            <asp:CheckBox ID="chkMarkSort" runat="server" Text="Sort By Marks" Visible="False"></asp:CheckBox>
                            <asp:CheckBox ID="chkChoiceSort" runat="server" Text="Sort By Choice" Visible="False"></asp:CheckBox>
                            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                               PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Available">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                                        Select<br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("STU_SCT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SL.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNoView() %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate >
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Science Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSciencemark" runat="server" Text='<%# Bind("SCIENCE_MARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Science Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScience" runat="server" Text='<%# Bind("SCIENCE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maths Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMathsmarks" runat="server" Text='<%# Bind("MATHS_MARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maths Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMaths" runat="server" Text='<%# Bind("MATHS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="English Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnglishmarks" runat="server" Text='<%# Bind("ENGLISH_MARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="English Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnglish" runat="server" Text='<%# Bind("ENGLISH")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Social Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSocialmarks" runat="server" Text='<%# Bind("SOCIAL_MARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Social Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSocial" runat="server" Text='<%# Bind("SOCIAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CGPA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCGPA" runat="server" Text='<%# Bind("CGPA") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OverAll">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOverall" runat="server" Text='<%# Bind("OVERALL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Choice 1">

                                        <ItemTemplate>
                                            <asp:Label ID="lblChoice1" runat="server" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"c1color").ToString()) %>' Text='<%# Bind("Choice1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Choice 2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChoice2" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"c2color").ToString()) %>' runat="server" Text='<%# Bind("Choice2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Choice 3">

                                        <ItemTemplate>
                                            <asp:Label ID="lblChoice3" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"c3color").ToString()) %>' runat="server" Text='<%# Bind("Choice3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alloted Stream">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAllotedStream" runat="server" Text='<%# Bind("AllotedStream") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle   CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4"  valign="top">
                            <asp:Button ID="btnAllot" runat="server" Text="Allot" CssClass="button" TabIndex="4" />
                            <asp:Button ID="btnDeAllot" runat="server" Text="De Allot" CssClass="button" TabIndex="4" />
                            <asp:Button ID="btnExport" runat="server" CssClass="button"  
                                TabIndex="4" Text="Export to Excel" />
                        </td>
                    </tr>
                </table>
                </asp:Panel>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
