﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="clmNonPhoenixFileUpload.aspx.vb" Inherits="Curriculum_clmNonPhoenixFileUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .error {
            font-family: Raleway,sans-serif;
            font-size: 7pt;
            font-weight: normal;
        }

        #btnOtherCurriculumSubmit {
            width: 17% !important;
        }


        .field-validation-error {
            color: red;
            font-size: 12px;
        }

        .padding-0 {
            padding: 0px !important;
        }
    </style>
    <script type="text/javascript">
        var fileSizeInMB = 5;

        var validationErrorMessage = {
            FileSelect: 'Please select the audio/video file.',
            FileSize: 'File size can&#39;t exceed ' + fileSizeInMB + 'MB.',
            FileType: "Allowed extensions are {0}",
            FileSubmit: 'Error! A problem has been occurred while submitting your data.',
            TechnicalError: 'Techinal Error! Some problem occured while submitting your data.'
        };


        $(document).ready(function () {          
            var isSuccess = getURLParameter('success');
            if (isSuccess != undefined) {               
                removeQueryStringFromURL('success');
            }
        });

        function validateAndSaveFileToCollection(uploader) {
            if (uploader == undefined) {
                uploader = document.getElementById('otherCurriculumFileUpload');
            }
            var file = uploader.files[0];
            if (file == undefined) {
                showFileError(validationErrorMessage.FileSelect);
                return false;
            }

            var fileSize = file.size;
            fileSize = (fileSize / (1024 * 1024)).toFixed(2);
            if (fileSize > fileSizeInMB) {
                showFileError(validationErrorMessage.FileSize);
                return false;
            }

            var lblMsg = $("#lblUploadedMsg");

            var invalidMessage = validateFileType(file.name);
            if (invalidMessage == undefined || invalidMessage == '') {
                lblMsg.text("");
            }
            else {
                showFileError(invalidMessage);
                $(uploader).val("");
                return false;
            }

            return true;
        };

        function validateFileType(fileName) {
            var validExtensions = ['pdf'];
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
            if ($.inArray(fileNameExt, validExtensions) == -1) {
                return validationErrorMessage.FileType.replace("{0}", validExtensions.join(', '));
            }
        };

        function getURLParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        };

        //Function used to remove querystring
        function removeQueryStringFromURL(key) {
            var urlValue = document.location.href;

            //Get query string value
            var searchUrl = location.search;

            if (key != "") {
                oldValue = getURLParameter(key);
                removeVal = key + "=" + oldValue;
                if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
                    urlValue = urlValue.replace('?' + removeVal + '&', '?');
                }
                else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
                    urlValue = urlValue.replace('&' + removeVal + '&', '&');
                }
                else if (searchUrl.indexOf('?' + removeVal) != "-1") {
                    urlValue = urlValue.replace('?' + removeVal, '');
                }
                else if (searchUrl.indexOf('&' + removeVal) != "-1") {
                    urlValue = urlValue.replace('&' + removeVal, '');
                }
            }
            else {
                searchUrl = location.search;
                urlValue = urlValue.replace(searchUrl, '');
            }
            history.pushState({ state: 1, rand: Math.random() }, '', urlValue);
        };

        function showFileError(message) {
            $("#lblUploadedMsg").html(message)
            $("#lblUploadedMsg").addClass("field-validation-error");
        };

        function showTechinalError(failureMessage) {
            console.log(failureMessage);
            alert(validationErrorMessage.TechnicalError);
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <div class="card mb-3 form-horizontal">
                <div class="card-header letter-space">
                    <i class="fa fa-file mr-3"></i>Manage Other Curriculum Report Cards
                </div>
                <div class="card-body">
                    <div id="divSuccess" runat="server" class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Success!</strong> Your data has been updated successfully.
                    </div>
                    <div id="divError" runat="server" class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> A problem has been occurred while submitting your data.
                    </div>
                    <table width="100%">
                        <tr>
                            <td align="left" width="15%">
                                <span class="field-label">Academic Year</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td width="40%" colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAcademicYear"
                                    Display="Dynamic" ErrorMessage="Academic Year required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Report Source</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReportSource" runat="server" CssClass="form-field" AutoComplete="off" EnableViewState="true" AutoPostBack="True" OnSelectedIndexChanged="ddlReportSource_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="SIMS" Value="SIMS"></asp:ListItem>
                                    <asp:ListItem Text="ISAMS" Value="ISAMS"></asp:ListItem>
                                    <asp:ListItem Text="Power School" Value="POWERSCHOOL"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlReportSource"
                                    Display="Dynamic" ErrorMessage="Report Source required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>

                            <td align="left">
                                <span class="field-label">Report Card</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReportCardSetup" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="True" OnSelectedIndexChanged="ddlReportCardSetup_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlReportCardSetup"
                                    Display="Dynamic" ErrorMessage="Report Card required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>

                            <td align="left">
                                <span class="field-label">Report Schedule</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReportSchedule" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlReportSchedule"
                                    Display="Dynamic" ErrorMessage="Report Schedule required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>

                            <td align="left">
                                <span class="field-label">Grade</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReportGrade" runat="server" CssClass="form-field" AutoComplete="off">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlReportGrade"
                                    Display="Dynamic" ErrorMessage="Report Grade required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <%--<tr>
                    <td>
                        <asp:FileUpload ID="btnUpload" ClientIDMode="Static" runat="server" CssClass="button" Text="Upload File" />
                    </td>
                    <td>
                        <asp:Button ID="btnSubmit" Text="Save" runat="server" CssClass="button" CausesValidation="true" ValidationGroup="UploadValidation" OnClick="btnSubmit_Click" />
                    </td>
                    <td colspan="2"></td>
                </tr>--%>
                        <tr>
                            <td align="left">
                                <span class="field-label">Upload Report File</span>
                            </td>
                            <td>
                                <div class="fileuploader">
                                    <asp:FileUpload runat="server" ID="otherCurriculumFileUpload" enctype="multipart/form-data" ClientIDMode="Static" onchange="return validateAndSaveFileToCollection(this);" CssClass="button" />
                                    <asp:Button ID="btnOtherCurriculumSubmit" ClientIDMode="Static" Text="Submit" runat="server" CssClass="button" CausesValidation="true" ValidationGroup="UploadValidation"  OnClick="btnSubmit_Click" />
                                </div>

                            </td>
                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="otherCurriculumFileUpload"
                                    Display="Dynamic" ErrorMessage="Report File required" ValidationGroup="UploadValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <span id="lblUploadedMsg" class="col-md-12 padding-0"></span>

                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnOtherCurriculumSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

