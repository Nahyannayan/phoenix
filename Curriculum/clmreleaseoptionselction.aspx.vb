﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmreleaseoptionselction
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")

            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


           

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                lblacid.Text = getacyear()
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100550") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'set_Menu_Img()

                    GridBind()
                    gvOption.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_query As String = "select grm_id,GRM_GRD_ID,isnull(opt_bopen,0) as tag  from GRADE_BSU_M  inner join GRADE_M on GRD_ID=grm_grd_id " _
                              & " left outer join  OPL.ONLINEOPTION_SETTING  on GRm_grd_id=opt_grd_id and grm_acd_id=opt_acd_id WHERE GRM_BSU_ID='" + Session("sbsuid") + "' " _
                              & " and GRM_ACY_ID='" + Session("Current_ACY_ID") + "' and GRD_DISPLAYORDER not in (1,2,3,16) order by GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvOption.DataBind()
            Dim columnCount As Integer = gvOption.Rows(0).Cells.Count
            gvOption.Rows(0).Cells.Clear()
            gvOption.Rows(0).Cells.Add(New TableCell)
            gvOption.Rows(0).Cells(0).ColumnSpan = columnCount
            gvOption.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvOption.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvOption.DataBind()
        End If

        

    End Sub
    Private Function getacyear() As String
        Dim ds As String


        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT ACY_DESCR from ACADEMICYEAR_M where ACY_ID='" + Session("Current_ACY_ID") + "' "

        ds = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return ds


    End Function
    'Protected Sub gvOption_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOption.RowCommand

    '    Dim s As String = "", r As String = ""
    '    Try
    '        '    Dim url As String
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim selectedRow As GridViewRow = DirectCast(gvOption.Rows(index), GridViewRow)

    '        If e.CommandName = "Save" Then
    '            Dim lbl As Label
    '            'Dim lbl1 As Label
    '            Dim chkb As CheckBox
    '            Dim t As Integer
    '            'lbl = selectedRow.FindControl("lblOptId")
    '            lbl = selectedRow.FindControl("lblOption")
    '            chkb = selectedRow.FindControl("chkAll")

    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '            Dim str_query As String
    '            If chkb.Checked Then
    '                t = 1
    '                s = lbl.Text
    '                ' r = lbl1.Text
    '            Else
    '                t = 0
    '            End If

    '            str_query = "exec OPL.SAVEPARENTLOGIN_MENU '" + Session("sbsuid") + "' , '" + s + "'"
    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    '            str_query = "exec OPL.SAVEONLINEOPTION_SETTING '" + Session("sbsuid") + "' , '" + Session("Current_ACY_ID") + "','" + s + "'," + t.ToString + ""
    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    '            lblError.Text = "Updated ! ... "


    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    Protected Sub gvOption_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOption.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim chkisdefault As CheckBox = DirectCast(e.Row.FindControl("chkAll"), CheckBox)

            If chkisdefault.Text = "False" Then
                chkisdefault.Checked = False
                chkisdefault.Text = ""
            Else
                chkisdefault.Checked = True
                chkisdefault.Text = ""
            End If
        End If

    End Sub
    Protected Function GetSelectedRecords() As String
        Dim gr As String = ""
        For Each row As GridViewRow In gvOption.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkAll"), CheckBox)
                If chkRow.Checked Then
                    If gr = "" Then
                        gr = TryCast(row.Cells(1).FindControl("lblOption"), Label).Text
                    Else
                        gr = gr + "|" + TryCast(row.Cells(1).FindControl("lblOption"), Label).Text
                    End If
                End If
            End If
        Next
        Return gr
    End Function

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        Dim s As String
        s = GetSelectedRecords()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            str_query = "exec OPL.SAVEPARENTLOGIN_MENU '" + Session("sbsuid") + "' , '" + s + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            str_query = "exec OPL.SAVEONLINEOPTION_SETTING '" + Session("sbsuid") + "' , '" + Session("Current_ACY_ID") + "','" + s + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblError.Text = "Updated ! ... "
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"

        End Try
        
    End Sub
End Class
