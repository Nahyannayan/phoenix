Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSubjectSyllabus_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("dtSubject") = SetDataTable()

                    BindCurriculum()
                    PopulateSubjects()
                    ViewState("SelectedRow") = -1
                    btnSave.Visible = False
                    btnCancel.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(2)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbm_id"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbm_descr"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbs_boardcode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)
        keys(1) = column


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "clm"
        dt.Columns.Add(column)
        keys(2) = column

        dt.PrimaryKey = keys
        Return dt
    End Function
    Private Sub PopulateSubjects()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_query As String = "SELECT SBM_ID,SBM_DESCR FROM SUBJECT_M WHERE SBM_ID" _
                                     & " NOT IN(SELECT SBS_SBM_ID FROM SYLLABUS_M WHERE SBS_CLM_ID=" + ddlCurriculum.SelectedValue.ToString + ")" _
                                     & " ORDER BY SBM_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "SBM_DESCR"
            ddlSubject.DataValueField = "SBM_ID"
            ddlSubject.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PopulateSubjects")
        End Try
    End Sub
    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT CLM_ID,CLM_DESCR FROM CURRICULUM_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurriculum.DataSource = ds
        ddlCurriculum.DataTextField = "CLM_DESCR"
        ddlCurriculum.DataValueField = "CLM_ID"
        ddlCurriculum.DataBind()
    End Sub
    Sub AddRows()
        Dim dt As New DataTable
        Dim dr As DataRow
        dt = Session("dtSubject")
        Dim keys As Object()
        ReDim keys(2)
        keys(0) = ddlSubject.SelectedValue.ToString
        keys(1) = "add"
        keys(2) = ddlCurriculum.SelectedValue.ToString

        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This subject is already added"
            Exit Sub
        End If
        dr = dt.NewRow
        dr.Item(0) = ddlSubject.SelectedValue.ToString
        dr.Item(1) = ddlSubject.SelectedItem.Text
        dr.Item(2) = txtBoardCode.Text.ToString.Trim
        dr.Item(3) = "add"
        dr.Item(4) = ddlCurriculum.SelectedValue.ToString
        dt.Rows.Add(dr)

        Session("dtSubject") = dt
        GridBind(dt)
    End Sub
    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(3) <> "delete" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvSubject.DataSource = dtTemp
        gvSubject.DataBind()
    End Sub


    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            txtBoardCode.Text = ""
            btnSave.Visible = True
            btnCancel.Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtSubject")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(2) = txtBoardCode.Text.ToString.Trim
            End With

            Session("dtSubject") = dt

            GridBind(dt)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim sbsAddIds As String = ""
        Dim Id As String = ""
        Dim dt As DataTable
        dt = Session("dtSubject")
        Dim dr As DataRow
        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If dr.Item(3) = "add" Then
                            str_query = "exec saveSYLLABUS_M 0," _
                                       & .Item(0).ToString + "," _
                                       & "'" + .Item(2).Trim + "'," _
                                       & .Item(4) + "," _
                                       & "'add'"
                            Id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                            If sbsAddIds <> "" Then
                                sbsAddIds += ","
                            End If
                            sbsAddIds += Id
                        End If
                    End With
                Next

                Dim flagAudit As Integer
                If sbsAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SBS_ID(" + sbsAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function


#End Region
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If SaveData() = True Then
                ddlSubject.ClearSelection()
                ddlSubject.SelectedIndex = 0
                txtBoardCode.Text = ""
                Session("dtSubject") = SetDataTable()
                gvSubject.DataSource = Session("dtSubject")
                gvSubject.DataBind()
                PopulateSubjects()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvSubject_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSubject.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvSubject.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtSubject")


            Dim lblBoard As Label
            Dim lblSubId As Label
            Dim lblSubject As Label
            Dim lblclm As Label

            lblBoard = selectedRow.FindControl("lblBoard")
            lblSubId = selectedRow.FindControl("lblSubId")
            lblSubject = selectedRow.FindControl("lblSubject")
            lblclm = selectedRow.FindControl("lblclm")

            ReDim keys(2)

            keys(0) = lblSubId.Text
            keys(1) = "add"
            keys(2) = lblclm.Text

            index = dt.Rows.IndexOf(dt.Rows.Find(keys))

            If e.CommandName = "edit" Then
                ViewState("SelectedRow") = index
                btnAddNew.Text = "Update"
                Dim li As New ListItem
                li.Text = lblSubject.Text
                li.Value = lblSubId.Text
                ddlSubject.ClearSelection()
                ddlSubject.Items(ddlSubject.Items.IndexOf(li)).Selected = True
                txtBoardCode.Text = lblBoard.Text
            ElseIf e.CommandName = "delete" Then
                dt.Rows(index).Item(3) = "delete"
                Session("dtSubject") = dt
                GridBind(dt)
                If gvSubject.Rows.Count = 0 Then
                    btnSave.Visible = False
                    btnCancel.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvSubject_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSubject.RowDeleting

    End Sub

    Protected Sub gvSubject_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSubject.RowEditing

    End Sub

 
    Protected Sub ddlCurriculum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        Try
            PopulateSubjects()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "add" Then
                    Session("dtOption") = SetDataTable()
                    gvSubject.DataSource = Session("dtSubject")
                    gvSubject.DataBind()
                End If
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
End Class
