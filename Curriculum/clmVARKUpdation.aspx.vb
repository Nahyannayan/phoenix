﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Data.OleDb
Partial Class Curriculum_clmVARKUpdation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100355") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    FillGrade()
                    FillSection()
                    gridbind()

                End If

                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '  lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " SELECT DISTINCT GRM_DISPLAY,grm_GRD_ID,GRD_DISPLAYORDER  FROM GRADE_BSU_M  INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID  WHERE " _
                                 & " GRM_BSU_ID ='" + Session("SBSUID") + "' AND GRM_ACD_ID= '" + ddlAca_Year.SelectedValue + "' ORDER BY GRD_DISPLAYORDER "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= '" + ddlAca_Year.SelectedValue + "'  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedItem.Value + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        FillSection()
        gridbind()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        FillGrade()
    End Sub
    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            Dim txtSearch As New TextBox
            Dim optSearch As String
            Dim strFilter As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String
            Dim strFilter1 As String = ""




            str_Sql = "SELECT STU_ID,STU_NO,stu_NAME=ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,''), " _
                      & " isnull(vk_type,'--') vk_type " _
                      & "   FROM STUDENT_M " _
                       & " left outer join VARKSCORES on STU_ID =vk_stu_id " _
                       & " WHERE STu_ACD_ID=" & ddlAca_Year.SelectedValue & " and STU_GRD_ID ='" & ddlGrade.SelectedValue & "' "


            If ddlSection.SelectedValue <> "" Then

                str_Sql += " AND Stu_SCT_ID='" + ddlSection.SelectedValue + "'"
            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch = gvComments.HeaderRow.FindControl("txtOption")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch.Text <> "" Then
                    strFilter = " and stu_no  like '" & txtSearch.Text & "%' "
                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If
            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch1.Text <> "" Then
                    strFilter1 = " and ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')  like '" & txtSearch1.Text & "%' "
                    optSearch1 = txtSearch1.Text



                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch

            txtSearch1 = New TextBox
            txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
            txtSearch1.Text = optSearch1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnEmpid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnstuname_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        UpLoadExcelFiletoXml()
    End Sub
    Private Sub UpLoadExcelFiletoXml()
        Dim strFileNameOnly As String

        lblerror3.Text = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "_") & Session("sBsuid") & "_" & ddlGrade.SelectedItem.Value & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly & ";" & _
                "Extended Properties=Excel 8.0;"

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim s As New MemoryStream()

                dt.WriteXml(s, True)

                'Retrieve the text from the stream

                s.Seek(0, SeekOrigin.Begin)

                Dim sr As New StreamReader(s)

                Dim xmlString As String

                xmlString = sr.ReadToEnd()

                sr.Close()

                sr.Dispose()
                Dim cmd As New SqlCommand
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("dbo.saveVARKScoreXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@vk_ACD_ID", ddlAca_Year.SelectedValue)
                cmd.Parameters.AddWithValue("@vk_GRD_ID", ddlGrade.SelectedItem.Value)

                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()

                lblerror3.Text = "Records Saved Sucessfully"
                gridbind()

            Catch ex As Exception
                lblerror3.Text = "Error: " & ex.Message.ToString
            End Try
        Else
            lblerror3.Text = "Please select a file to upload."
        End If


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""

            For Each row As GridViewRow In gvComments.Rows
                Dim lblid As New Label
                Dim ddlPrefer As New DropDownList
            

                lblid = row.FindControl("cmtId")
                ddlPrefer = row.FindControl("ddlPrefer")
          
                If ddlPrefer.SelectedItem.Value <> "--" Then

                    str_query = "exec dbo.saveVARKSCORES " & ddlAca_Year.SelectedValue & " ,'" & ddlGrade.SelectedValue & "'," & lblid.Text & ",'" _
                                 & "" & ddlPrefer.SelectedItem.Text & "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    lblError.Text = "Saved..."
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
