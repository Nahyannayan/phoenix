﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCASEStudentPerformanceReport.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCASEStudentPerformanceReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblText" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left"  valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center"  cellpadding="7" cellspacing="0" width="100%">                               
                                <tr id="trBsu" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlBusinessUnit"  runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trAcd1" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Section</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                               

                                <tr id="trSave" runat="server">
                                    <td align="center"  colspan="4" valign="top">
                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" TabIndex="4"
                                             />
                                        <asp:Button ID="btnDownload" runat="server" Text="Download Report in PDF" CssClass="button"
                                            TabIndex="4"  />
                                        <asp:HiddenField ID="hfbDownload" runat="server" />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

