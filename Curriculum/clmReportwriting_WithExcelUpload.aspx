﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportwriting_WithExcelUpload.aspx.vb" Inherits="Curriculum_clmReportwriting_WithExcelUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }

        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
</script>
  
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Report Writing-With Excel Upload
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left"   valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblTC" runat="server" align="center" width="100%" cellpadding="7" cellspacing="0">


                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span>
                        </td>
                        
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" Width="68px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Subject</span>
                        </td>
                        
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlSubject" SkinID="smallcmb" AutoPostBack="true" runat="server" >
                            </asp:DropDownList>
                        </td>
                      
                    </tr>
                    <tr>
                          <td align="left" width="20%" ><span class="field-label">Group</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGroup" SkinID="smallcmb" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td  align="left" width="20%"><span class="field-label">Report Card Schedule</span></td>
                        
                        <td  align="left" width="30%">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                       
                    </tr>
                    <tr>
                         <td align="left" width="20%">
                            <span class="field-label">Headers</span>
                        </td>
                        
                        <td width="30%" align="left">
                            <asp:TreeView ID="tvHeaders" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked();">
                                <NodeStyle CssClass="treenode" />
                            </asp:TreeView>
                        </td>
                        <td width="20%" align="left" >
                            <asp:LinkButton ID="btnDownloadTemplate" runat="server" Text="Download Excel Template" CssClass="button" TabIndex="4"/>
                            </td>
                        <td width="30%" align="left">
                            <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"/>
                        </td>
                    </tr>
                    <tr>
                         <td colspan="4" align="center">                              
                            <asp:Button ID="btnUpload" runat="server" Text="Upload Excel Data" CssClass="button" TabIndex="4"  />                           
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                <asp:GridView ID="gvSubj" Visible="false" CssClass="table table-bordered table-row" runat="server" HorizontalAlign="Center">
                    <RowStyle HorizontalAlign="Center" />
                    <AlternatingRowStyle />
                </asp:GridView>

               </div>
        </div>
     </div>

</asp:Content>

