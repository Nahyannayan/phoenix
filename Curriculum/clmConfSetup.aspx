﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConfSetup.aspx.vb" Inherits="Curriculum_ConfigSetup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" >

<script type="text/javascript"></script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> 
            <asp:Label ID="Label2" runat="server" Text="Configuration"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
         <table align="center"  style="border-collapse: collapse" cellpadding="4" cellspacing="0" width="100%">
               <tr>
                    <td>
                        <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                    <asp:HiddenField  ID ="hdf_rsmid" runat="server" />
                                    <asp:HiddenField ID ="hdf_selGuid" runat="server" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                                    <span style="  color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                <tr>
             <td colspan="6">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"></asp:UpdatePanel>
                <div style="overflow: auto">
                    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        
                        <tr class="gridheader_pop" runat="server" >
                            <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">
                                Report Type
                            </td>
                        </tr>
                        <tr runat="server">
                           <td runat="server"></td>
                        </tr>
                        <tr runat="server">
                             <td align="left" width="20%" runat="server">
                                  <span class="field-label"> Academic Year*</span>
                             </td>
                             <td align="left" runat="server" >
                                 <asp:DropDownList ID="DrpAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                     OnSelectedIndexChanged="DrpAcademicYear_SelectedIndexChanged">
                                 </asp:DropDownList>
                             <br />
                             </td>
                             <td align="left" valign="middle" width="20%" runat="server">
                                 <span class="field-label"> Description*</span>
                             </td>
                             <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                 <asp:TextBox runat="server"  ID="txtRptDescription" AutoPostBack="True" ></asp:TextBox>
                                   <br />
                             </td>
                        </tr>
                        <tr runat="server">
                            <td align="left" valign="middle" width="20%" runat="server">
                                    <span class="field-label"> Grade*</span>
                            </td>
                            <td align ="left" runat="server">
                                 <div ID="PlChkLst" runat="server" class="checkbox-list">
                                    <asp:CheckBoxList ID="ChkGradeList" runat="server"  DataTextField="GRM_DISPLAY" AutoPostBack ="false"></asp:CheckBoxList>
                                     <br />                             
                                 </div>
                            </td>
                        </tr>
                        <tr runat="server" >
                            <td colspan ="4" runat="server"> 
                                <asp:Checkbox ID ="ChkFinalReports" text ="Final reports" runat="server"  cssclass ="field-label" AutoPostBack="True"></asp:Checkbox>
                                <asp:Checkbox ID ="ChkAolPremium" text ="AOL Premium" runat="server"  cssclass ="field-label"></asp:Checkbox>
                            </td>
                         </tr>
                        <tr runat="server">
                                <td align="left" valign="middle" width="20%" runat="server">
                                    <span class="field-label"> Report Type</span>
                                </td>
                                <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                    <asp:TextBox runat="server" ID ="TxtReportType" ></asp:TextBox>
                                      <br />
                                 </td>
                                <td align="left" valign="middle" width="20%" runat="server">
                                    <span class="field-label"> Report Format</span>
                                </td>
                                <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                     <asp:TextBox runat="server" ID ="TxtReportFormat" ValidateRequestMode="Enabled"></asp:TextBox>
                              <br />
                                </td>
                          </tr>
                        <tr runat="server"><td runat="server"></td></tr>
                          <tr class="gridheader_pop" runat="server" >
                                 <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">
                                     Report Headers
                                 </td>
                          </tr>
                          <tr runat="server">
                              <td runat="server" id ="tdblank1"></td>
                          </tr>
                          <tr runat="server">
                               <td align="left" width="20%" runat="server" id ="tdheader">
                                    <span class="field-label"> Header*</span>
                               </td>
                               <td align="left" runat="server" >
                                   <asp:TextBox runat="server" ID ="TxtRptHeader" ></asp:TextBox>
                                   <br />
     
                                   </td>
                               <td align="left" valign="middle" width="20%" runat="server" id ="tdDisplaytype">
                                   <span class="field-label"> Display Type</span></td>
                               <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                   <asp:DropDownList ID="DrpDisplayType" runat="server" OnSelectedIndexChanged="DrpDisplayType_SelectedIndexChanged">
                                  </asp:DropDownList>
                                  <br />
                               </td>
                           </tr>
                           <tr runat="server">
                                <td colspan ="4" runat="server"> 
                                    <asp:Checkbox ID ="ChkDirectEntry" text ="Direct Entry" cssclass ="field-label" runat="server" ></asp:Checkbox>
                                    <asp:Checkbox ID ="ChkAllSubject" text ="All Subject" runat="server" cssclass ="field-label" ></asp:Checkbox>
                                    <asp:Checkbox ID ="ChkRuleReq" text ="Rule Required" runat="server"  cssclass ="field-label" ></asp:Checkbox>
                                    <asp:Checkbox ID ="ChkPerReq" text ="Perfomance Required" runat="server"  cssclass ="field-label"></asp:Checkbox>
                                    <asp:Checkbox ID ="ChkGraphReq" text ="Graph" runat="server"  cssclass ="field-label"></asp:Checkbox>
                                </td>    
                            </tr>
                            <tr runat="server">
                                <td align="left" valign="middle" width="20%" runat="server" id ="tdcssclass">
                                    <span class="field-label"> CSS Class</span>
                                </td>
                                <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                    <asp:DropDownList ID="DrpCCSClass" runat="server" OnSelectedIndexChanged="DrpCCSClass_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </td> 
                   
                                <td align="left" valign="middle" width="20%" runat="server" id ="tdresult">
                                    <span class="field-label"> Result</span>
                                </td>

                                <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                    <asp:DropDownList ID="DrpResult" runat="server" OnSelectedIndexChanged="DrpResult_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </td> 
                            </tr>
                            <tr runat="server" align="left" colspan="1" valign="middle" width="30%" >
                                <td colspan="2" align="right" runat="server">
                                </td>
                                <td colspan ="4" align ="right" runat="server" width="3%">
                                  <asp:LinkButton ID="btnAddNew_Group2" runat="server" CssClass="button" Text="Add New" />
                                </td>
                            </tr>
                           <tr runat="server"> 
                           <td align="center" colspan="4" runat="server" > 
                                      <asp:GridView ID="Grd_Group2" runat="server" Width="100%" AutoGenerateColumns="False"
                                     CssClass="table table-bordered table-row"  EmptyDataText="Nothing to display here."
                                       PageSize="5"   >
                                     <Columns>
                                          <asp:TemplateField HeaderText ="Slno."  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                         <ItemTemplate>
                                             <asp:Label runat="server"  id="lblSrno"  Text ='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField Visible="false" HeaderText ="Id" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                             <ItemTemplate>
                                                 <asp:Label runat ="server" Width ="100px"  ID ="lblrpt_HeaderID" Text ='<%# Bind("Rpt_ID")%>' ></asp:Label>
                                             </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-HorizontalAlign ="left" ItemStyle-HorizontalAlign ="left">
                                           <HeaderTemplate>Report Header</HeaderTemplate>
                                          <ItemTemplate>
                                            <asp:Label ID="lblRpt_HeaderName" Width ="200px" runat="server"  text='<%# Bind("Rpt_HeaderName")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Display Type" HeaderStyle-HorizontalAlign ="left" ItemStyle-HorizontalAlign ="left">
                                          <ItemTemplate>
                                            <asp:Label ID="lblRpt_DisplayType" Width="120px" runat="server" text='<%# Bind("Rpt_DisplayType")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField >
                                       <asp:TemplateField HeaderText="Direct Entry" HeaderStyle-HorizontalAlign ="left" ItemStyle-HorizontalAlign ="left" >
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_DirectEntry" runat="server" text='<%# Bind("Rpt_DirectEntry")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                      <asp:TemplateField HeaderText="All Subjects"  HeaderStyle-HorizontalAlign ="left" ItemStyle-HorizontalAlign ="left">
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_AllSubject" runat="server" text='<%# Bind("Rpt_AllSubject") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Rule Required" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_RuleReq" runat="server" text='<%# Bind("Rpt_RuleReq")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Perfomance Required" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center" >
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_PerfoReq" runat="server" text='<%# Bind("Rpt_PerfoReq")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Graph" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center" >
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_Graph" runat="server" text='<%# Bind("Rpt_Graph")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="CSS Class"  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_CssClass" runat="server" text='<%# Bind("Rpt_CssClass")%>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Result"  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                          <ItemTemplate>
                                          <asp:Label ID="lblRpt_Result" runat="server" text='<%# Bind("Rpt_Result") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                      <asp:ButtonField CommandName="edit" Text="Edit" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                       </asp:ButtonField>
                                   <asp:ButtonField CommandName="delete" Text="Delete"  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center" >
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                       </asp:ButtonField>
                                       </Columns>  
                                     <HeaderStyle  CssClass="gridheader_pop" />
                                          <RowStyle CssClass="griditem"  />
                                          <SelectedRowStyle CssClass="Green" />
                                          <AlternatingRowStyle CssClass="griditem_alternative" />
                                     </asp:GridView>  
                                    </td>
                           </tr>
                            <tr class="gridheader_pop" runat="server" >
                                 <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">
                                     Report Names
                                   </td>
                            </tr>
                            <tr runat="server">
                                 <td runat="server"></td>
                            </tr>
                            <tr runat="server">
                                 <td align="left" valign="middle" width="20%" runat="server" id ="tdReportNames">
                                    <span class="field-label"> Report Name*</span>
                                 </td>
                                 <td align="left" colspan="1" valign="middle" width="30%" runat="server">
                                      <asp:TextBox runat="server" ID ="TxtRptName" ></asp:TextBox>
                                    <br />
                                 </td>
                                <td colspan ="4" align ="right" runat="server" >
                                  <asp:linkbutton ID="btnAddNew_Group3" runat="server" CssClass="button" Text="Add New"   />
                                </td>
                            </tr>
                        <tr runat="server" id ="isr" >
                            <td runat="server" align="center" colspan="4">
                                <asp:GridView ID="Grd_Group3" runat="server" AutoGenerateColumns="False"  height="15px"
                                CssClass="table table-bordered table-row"  EmptyDataText="Nothing to display here." PageSize="5"   >
                                    <Columns>
                                        <asp:TemplateField HeaderText ="Slno" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                         <ItemTemplate>
                                             <asp:Label runat="server" id="lblSrno_rptname" Width="70px"  Text ='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible =" false" HeaderText ="Id" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center"> 
                                             <ItemTemplate>
                                                 <asp:Label runat ="server" ID ="lblrpt_ReportID" Text ='<%# Bind("ReportName_ID")%>'></asp:Label>
                                             </ItemTemplate>
                                       </asp:TemplateField>
                                        <asp:TemplateField  Headertext ="Report Name" HeaderStyle-HorizontalAlign ="left" ItemStyle-HorizontalAlign ="left">
                                            <ItemTemplate>
                                                <asp:label ID="lblRpt_name" Width="1000px"  runat="server" Text='<%# Bind("Report_Name") %>'></asp:label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:ButtonField CommandName="edit" Text="Edit"  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center"  HeaderStyle-Width ="40px">
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                             <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                         </asp:ButtonField>
                                         <asp:ButtonField CommandName="delete" Text="Delete"  HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center" Headerstyle-width="30px" >
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                              <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                         </asp:ButtonField>
                                      </Columns>  
                                    <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                         <RowStyle CssClass="griditem" Height="25px" />
                                         <SelectedRowStyle CssClass="Green" />
                                         <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr runat="server" >
                            <td colspan ="4" align ="center" runat="server">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            </td>
                         </tr>

                    </table>
                </div>      
                 </td>
             </tr>
        </table>
       </div>
   </div>
</div>
</asp:Content>

