﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmLessonStudentCriterias_Popup.aspx.vb"
    Inherits="Curriculum_clmLessonStudentCriterias_Popup" MaintainScrollPositionOnPostback="true" Title="Lesson Criteria" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>Untitled Page</title>

    <script type="text/javascript">



        function change_ddl_state(ddlThis, ddlObj) {

            ddlThis.setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;


                if (currentid.indexOf(ddlObj) != -1) {

                    if (ddlThis.selectedIndex != -1) {


                        document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex;
                        document.forms[0].elements[i].setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))


                    }
                }
            }
        }



        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function autoWidth(mySelect) {
            var maxlength = 0;
            for (var i = 0; i < mySelect.options.length; i++) {
                if (mySelect[i].text.length > maxlength) {
                    maxlength = mySelect[i].text.length;
                }
            }
            mySelect.style.width = maxlength * 5;
        }

        function setWidth(mySelect) {
            mySelect.style.width = 35;
        }

        function setColor(ddlThis) {
            ddlThis.setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))

        }
        function ClosePopup() {
            parent.$.fancybox.close()
            parent.ViewClick()

        }
    </script>
    <style type="text/css">
        /*.field-label {
            font-weight: 600 !important;
            font-size: 16px !important;
        }*/
        .min-Rowwidth {
            min-width: 110px !important;
            vertical-align: top !important;
            max-width: 110px !important;
        }

        table td span.field-value {
            padding: 9px !important;
            min-height: 34px !important;
        }


        table th, table td {
            padding: 4px !important;
        }

            table td span.field-label, .card span.field-label {
                font-size: 1.1rem !important;
                color: rgba(114,114,114,1.00) !important;
                font-weight: 600 !important;
            }

        .grid_alternate {
            background-color: #f5f5f5;
        }

        .img-thumbnail {
            border-radius: 50% !important;
            box-shadow: 1px 2px 10px rgba(0,0,0,0.12) !important;
        }

        .popup-name {
            font-size: 24px;
            color: #333;
            line-height: 60px;
        }

        .popup-grade {
            font-size: 20px;
            color: #656565;
        }

        div.RadUpload .ruBrowse {
            background-position: 62px 79px;
            width: 120px;
            height: 28px;
            color: #fff;
            background: rgb(165,224,103) !important;
            background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%) !important;
            background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
            background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }

        div.RadUpload_Default .ruFileWrap .ruButtonHover {
            background-position: 100% -23px !important;
        }

        .labeltolink {
            text-decoration: none;
            cursor: pointer;
            color: #016b05;
            font-weight: bold;
        }

        .higlight {
            background-color: #e5e5e5;
            padding: 4px;
            border: 1px solid rgba(0, 0, 0, 0.15);
            display: block;
            margin: 6px 0;
            border-radius: 6px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </ajaxToolkit:ToolkitScriptManager>
        <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <script src='<%= ResolveUrl("~/vendor/jquery/jquery.min.js")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js?1=2")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js?1=2")%>'></script>
        <link type="text/css" href='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css?1=2")%>' rel="stylesheet" />
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>


                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row mt-2 border-bottom">
                            <div class="col-md-2 col-sm-2 text-center">
                                <asp:Image ID="imgStud" runat="server" class="img-thumbnail" ImageUrl="~/Images/Photos/no_image.gif" Width="100px" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <asp:Label ID="lblName" CssClass="popup-name" runat="server"></asp:Label>
                                <br />
                                <span class="popup-grade">Grade </span>
                                <asp:Label ID="lblGrade" CssClass="popup-grade" runat="server"></asp:Label>
                                <br />
                                <asp:Label ID="lblObjective" runat="server" CssClass="popup-grade"></asp:Label>
                            </div>
                            <div class="col-md-4 col-sm-4 text-right">
                                <span style="float: right">
                                    <asp:LinkButton ID="lnkSearch" runat="server" Visible="true"></asp:LinkButton></span>
                            </div>
                        </div>

                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Style="text-align: left"></asp:Label>

                    </div>
                    <asp:Panel ID="pnlFilters" runat="server">
                        <div class="card-body">
                            <div class="table-responsive m-auto">

                                <table id="Table2" runat="server" width="100%">
                                    <%--            <tr id="trError" runat="server">
                <td  class="title-bg" colspan="5">
                   LESSON CRITERIA
                </td>
            </tr>--%>
                                    <tr>
                                        <td align="left" width="32%">
                                            <span class="field-label">Academic Year</span>
                                            <br />
                                            <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                        <td align="left" width="32%">
                                            <span class="field-label">Student ID</span>
                                            <br />
                                            <asp:Label ID="lblStudentID" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                        <%-- <td rowspan="3" align="center" id="tdphoto" runat="server">
                                            <asp:Image ID="imgStud" runat="server" Height="150px" ImageUrl="~/Images/Photos/no_image.gif" Width="150px" />
                                        </td>--%>

                                        <td align="left" width="32%">
                                            <span class="field-label">Subject</span><br />
                                            <asp:Label ID="lblSubject" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <span class="field-label">Group</span>
                                            <br />
                                            <asp:Label ID="lblGroup" runat="server" CssClass="field-value"></asp:Label>
                                        </td>

                                        <td align="left">
                                            <span class="field-label">Topic</span>
                                            <br />
                                            <asp:Label ID="lblTopic" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <span class="field-label">Grades</span>
                                            <br />
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdAgeBand" runat="server" visible="false">
                                             <span class="field-label">Age Band</span>
                                            <br />
                                             <asp:DropDownList ID="ddlAgeBand" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlAgeBand_SelectedIndexChanged">
                                                </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <div>
                                    <div class="higlight" id="div_status" runat="server" visible="false">
                                        <asp:Label ID="lbl_BlockStatus" runat="server" class="field-label" Style="color: red !important;"></asp:Label>
                                    </div>
                                    <br />
                                    <asp:GridView ID="gvObjectives" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" BorderStyle="None" Width="100%">
                                        <RowStyle Wrap="True" />
                                        <AlternatingRowStyle CssClass="grid_alternate" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="NC Level" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("SYC_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("STC_STATUS") %>'></asp:Label>
                                                    <asp:Label ID="lblSydId" runat="server" Text='<%# Bind("SYC_SYD_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Topic">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTopic" runat="server" Text='<%# Bind("Topic")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Objective">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjective" runat="server" Text='<%# Bind("SYC_DESCR") %>' CssClass="labeltolink"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descriptor">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" onclick="javascript:setColor(this);">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    Status<br />
                                                    <asp:DropDownList ID="ddlAll" runat="server" onclick="javascript:change_ddl_state(this,'ddlStatus')">
                                                    </asp:DropDownList>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="min-Rowwidth"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                                <HeaderTemplate>
                                                    Upload Evidence
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <telerik:RadAsyncUpload ID="uploadFile" RenderMode="Lightweight" MaxFileInputsCount="1" HideFileInput="true"
                                                        Width="100%" runat="server" MultipleFileSelection="Disabled" DropZones=".FileUploadZone">
                                                    </telerik:RadAsyncUpload>
                                                    <asp:LinkButton ID="lblFileName" runat="server" OnClick="lblFileName_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("SYC_STARTDT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Bind("STC_UPDATEDATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EditRowStyle Wrap="True" />
                                        <EmptyDataRowStyle Wrap="True" />
                                        <HeaderStyle CssClass="gridheader_pop" Wrap="True" />
                                        <SelectedRowStyle CssClass="Green" Wrap="True" />
                                    </asp:GridView>
                                    <asp:GridView ID="gvObjectives_Students" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" BorderStyle="None">
                                        <RowStyle Wrap="True" />
                                        <AlternatingRowStyle CssClass="grid_alternate" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="NC Level" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStudentId" runat="server" Text='<%# Bind("STU_ID")%>'></asp:Label>
                                                    <asp:Label ID="lblStatus_Student" runat="server" Text='<%# Bind("STC_STATUS") %>'></asp:Label>
                                                    <asp:Label ID="lblSydId_Student" runat="server" Text='<%# Bind("STC_SYD_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Student Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStudents" runat="server" Text='<%# Bind("STU_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <HeaderTemplate>
                                                    Status<br />
                                                    <asp:DropDownList ID="ddlAll" runat="server" onclick="javascript:change_ddl_state(this,'ddlStatus')">
                                                    </asp:DropDownList>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlStatus_Student" runat="server" onclick="javascript:setColor(this);">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                                <HeaderTemplate>
                                                    Upload Evidence
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <telerik:RadAsyncUpload ID="uploadFile" RenderMode="Lightweight" MaxFileInputsCount="1" HideFileInput="true" Width="100%" runat="server" MultipleFileSelection="Disabled" DropZones=".FileUploadZone"></telerik:RadAsyncUpload>
                                                    <asp:LinkButton ID="lblFileName" runat="server" OnClick="lblFileName_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Bind("STC_UPDATEDATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EditRowStyle Wrap="True" />
                                        <EmptyDataRowStyle Wrap="True" />
                                        <HeaderStyle CssClass="gridheader_pop" Wrap="True" />
                                        <SelectedRowStyle CssClass="Green" Wrap="True" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <%--           <tr>
                            <td align="left">
                                <span class="field-label">Completion Date</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox><asp:ImageButton
                                    ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtDate" PopupPosition="TopRight">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                            <td align="center" colspan="3">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="7" />
                            </td>
                        </tr>--%>


                        <tr>
                            <td width="200px">
                                <span class="field-label">Completion Date : </span>
                            </td>
                            <td width="250px">
                                <asp:TextBox ID="txtDate" runat="server"> </asp:TextBox><asp:ImageButton
                                    ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtDate" PopupPosition="TopRight">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                            <td colspan="3" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="7" />
                            </td>
                        </tr>



                    </table>
                    <asp:HiddenField ID="hSTU_ID" runat="server" />
                    <asp:HiddenField ID="hACD_ID" runat="server" />
                    <asp:HiddenField ID="hSBM_ID" runat="server" />
                    <asp:HiddenField ID="hSGR_ID" runat="server" />
                    <asp:HiddenField ID="hSBG_ID" runat="server" />
                    <asp:HiddenField ID="hGRD_ID" runat="server" />
                    <asp:HiddenField ID="hTopic_ID" runat="server" />
                    <asp:HiddenField ID="hObj_ID" runat="server" />
                    <asp:HiddenField ID="hStuPhotoPath" runat="server" />
                    <asp:HiddenField ID="hTSM_ID" runat="server" />
                </div>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlFilters"
                    CollapseControlID="lnkSearch" ExpandControlID="lnkSearch" TextLabelID="lnkSearch"
                    CollapsedText="Show Details" ExpandedText="Hide Details" AutoCollapse="false" Collapsed="true">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
<script type="text/javascript">
    function HoverPopUp(_obj_id) {
        PageMethods.ObjDescr(_obj_id, onSuccessNM, OnFailedNM);
    }
    function onSuccessNM(result) {
        $.fancybox({
            'showCloseButton': false,
            'content': result
        })

    }

    function OnFailedNM(result) {
        alert(result);
    }
</script>
</html>
