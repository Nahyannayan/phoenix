﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmHomeworkDets
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100356") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindGrade()
                    BindSubject()
                    BindGroup()
                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        'ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAcademicYear.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        If bSuperUsr Then
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER  " & _
            "  FROM VW_GRADE_BSU_M INNER JOIN oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID  " & _
             " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcademicYear.SelectedValue & "') " & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER" & _
            "  FROM VW_GRADE_BSU_M inner join oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " & _
            " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcademicYear.SelectedValue & "') " & _
            " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EmployeeID") & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrade.DataSource = ds


        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grd_id"
        ddlGrade.DataBind()

    End Sub
    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

    End Sub
    Sub BindGroup()
     
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String()


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "'"
        End If
        If ddlSubject.SelectedValue <> "" Then
            strCondition += " AND SGR_SBG_ID='" & ddlSubject.SelectedValue & "'"
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL  AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue + "'"

            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue + "'"
            'AND SGR_GRD_ID='" & strGRD_IDs & "' AND SGR_SBG_ID=" & vSBM_IDs & " ORDER BY SGR_ID "
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "DESCR2"
        ddlGroup.DataValueField = "ID"
        ddlGroup.DataBind()
      
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim grade As String()

            grade = ddlGrade.SelectedValue.Split("|")


            str_Sql = "SELECT HW_ID, HW_GRD_ID, SBG_DESCR ,SGR_DESCR , HW_CDATE, HW_SDATE, HW_TIME, HW_DESC FROM HOMEWORKDETS" _
                      & " inner join SUBJECTS_GRADE_S on HW_SBG_ID=SBG_ID " _
                      & " inner join GROUPS_M  on HW_SGR_ID=SGR_ID " _
                      & " WHERE HW_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND HW_GRD_ID='" + grade(0) + "'" _
                      & " AND HW_SBG_ID=" + ddlSubject.SelectedValue + " AND HW_SGR_ID=" + ddlGroup.SelectedValue + " order by HW_CDATE desc"



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdType.DataSource = ds.Tables(0)
                grdType.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdType.DataSource = ds.Tables(0)
                Try
                    grdType.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdType.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdType.Rows(0).Cells.Clear()
                grdType.Rows(0).Cells.Add(New TableCell)
                grdType.Rows(0).Cells(0).ColumnSpan = columnCount
                grdType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdType.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub grdType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            grdType.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubject()
        BindGroup()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        BindGroup()
        gridbind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
        gridbind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String()
        Dim str_query As String = ""

        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("dbo.SAVE_HOMEWORKDETS", objConn)
            grade = ddlGrade.SelectedValue.Split("|")

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@HW_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            cmd.Parameters.AddWithValue("@HW_GRD_ID", grade(0))
            cmd.Parameters.AddWithValue("@HW_SBG_ID", ddlSubject.SelectedValue.ToString)
            cmd.Parameters.AddWithValue("@HW_SGR_ID", ddlGroup.SelectedValue)
            cmd.Parameters.AddWithValue("@HW_CDATE", CDate(txtCDate.Text))
            cmd.Parameters.AddWithValue("@HW_SDATE", CDate(txtSDate.Text))
            cmd.Parameters.AddWithValue("@HW_TIME", Val(txtTime.Text).ToString)
            cmd.Parameters.AddWithValue("@HW_DESC", txtDesc.Text)
            cmd.Parameters.AddWithValue("@HW_ONLINE", cbOnline.Checked)
            If ViewState("datamode") = "edit" Then
                cmd.Parameters.AddWithValue("@ID", H_CAT_ID.Value)
            Else
                cmd.Parameters.AddWithValue("@ID", 0)
            End If

            cmd.ExecuteNonQuery()

            
            lblError.Text = "Record Saved Successfully"
            clear()
            gridbind()
            btnSave.Text = "Save"
            ViewState("datamode") = "add"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub
    Sub clear()
        txtCDate.Text = ""
        txtDesc.Text = ""
        txtSDate.Text = ""
        txtTime.Text = ""
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub grdType_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdType.RowDeleting
        Try
            grdType.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdType.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("tyId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM dbo.HOMEWORKDETS WHERE HW_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdType_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdType.RowEditing
        grdType.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = grdType.Rows(e.NewEditIndex)

        Dim lblID As New Label
        lblID = TryCast(row.FindControl("tyId"), Label)

        If lblID.Text <> "" Then
            Displayitems(lblID.Text)
            H_CAT_ID.Value = lblID.Text
        End If



        btnSave.Text = "Update"
    End Sub
    Private Sub Displayitems(ByVal ID As Int64)

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "SELECT  HW_ACD_ID,  grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID, HW_SBG_ID ,HW_SGR_ID , HW_CDATE, HW_SDATE, HW_TIME, HW_DESC,HW_ONLINE FROM HOMEWORKDETS" _
                        & " inner join VW_GRADE_BSU_M  on grm_grd_id=HW_GRD_ID INNER JOIN oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID where HW_ID=" & ID

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then
                    ddlAcademicYear.SelectedValue = ds.Tables(0).Rows(0).Item("HW_ACD_ID")
                    ddlGrade.SelectedValue = ds.Tables(0).Rows(0).Item("GRD_ID")
                    ddlSubject.SelectedValue = ds.Tables(0).Rows(0).Item("HW_SBG_ID")
                    ddlGroup.SelectedValue = ds.Tables(0).Rows(0).Item("HW_SGR_ID")
                    txtCDate.Text = ds.Tables(0).Rows(0).Item("HW_CDATE")
                    txtSDate.Text = ds.Tables(0).Rows(0).Item("HW_SDATE")
                    txtTime.Text = ds.Tables(0).Rows(0).Item("HW_TIME")
                    txtDesc.Text = ds.Tables(0).Rows(0).Item("HW_DESC")
                    cbOnline.Checked = ds.Tables(0).Rows(0).Item("HW_ONLINE")
                    ViewState("datamode") = "edit"
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    'Protected Sub lnkDets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDets.Click
    '    Try
    '        Dim url As String
    '        'define the datamode to Add if Add New is clicked
    '        ViewState("datamode") = "add"
    '        'Encrypt the data that needs to be send through Query String
    '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
    '        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
    '        url = String.Format("~\Curriculum\clmHomeworkdets_D.aspx?Acdid=" + ddlAcademicYear.SelectedValue + "&MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub
    Protected Sub lnkShowdetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowdetails.Click
        pnlsetup.Visible = True
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        IfRefClose.Attributes.Add("src", "clmHomeworkdets_D.aspx?Acdid=" + ddlAcademicYear.SelectedValue + "&Grdid=" + ddlGrade.SelectedValue)

    End Sub
    Protected Sub lbPnlJobClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPnlJobClose.Click
        pnlsetup.Visible = False

    End Sub
End Class
