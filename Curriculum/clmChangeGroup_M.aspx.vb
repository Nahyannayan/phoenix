Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmChangeGroup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateTeacher()
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                    PopulateGradeStream()
                    BindSubjects()
                    updateSubjectURL()
                    hfSGR_ID.Value = "0"
                    ViewState("stumode") = "add"
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()

                    tblGroup.Rows(4).Visible = False
                    tblGroup.Rows(5).Visible = False
                    tblGroup.Rows(6).Visible = False
                    tblGroup.Rows(7).Visible = False

                    If Session("multistream") = 0 And Session("multishift") = 0 Then
                        tblGroup.Rows(2).Visible = False

                    End If
                    gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                    gvNewGroup.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
    End Sub

    Protected Sub btnStuNo1_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName1_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSection1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStuNo2_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName2_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSection2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#Region "Private Methods"

    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
            & "emp_id FROM employee_m WHERE emp_ect_id=1 and emp_bsu_id='" + Session("sBsuid") + "'  order by emp_fname,emp_mname,emp_lname"
        Else
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                   & "emp_id FROM employee_m WHERE emp_id=" + empId.ToString

        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()
        If Session("CurrSuperUser") = "Y" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = 0
            ddlTeacher.Items.Insert(0, li)
        End If

    End Sub

    Sub PopulateMoveTeacher()

        ddlMTeacher.Items.Clear()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "select sgr_sbg_id from groups_m where sgr_id='" + ddlGroup.SelectedValue.ToString + "'"
        Dim sbg_id As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_query = "SELECT distinct ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
        & " emp_id FROM employee_m as a inner join groups_teacher_s as b on a.emp_id=b.sgs_emp_id " _
        & " inner join groups_m as c on b.sgs_sgr_id=c.sgr_id " _
        & " WHERE emp_ect_id=1 and emp_bsu_id='" + Session("sBsuid") + "'" _
        & " and sgr_sbg_id=" + sbg_id.ToString + " and sgs_todate is null and emp_status<>4" _
        & " order by emp_fname,emp_mname,emp_lname"




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMTeacher.DataSource = ds
        ddlMTeacher.DataTextField = "emp_name"
        ddlMTeacher.DataValueField = "emp_id"
        ddlMTeacher.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlMTeacher.Items.Insert(0, li)

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub PopulateGradeStream()

        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "' and grm_shf_id=" + ddlShift.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If

        

    End Sub

    'Sub PopulateGradeSubjects()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT A.SBG_ID AS SBG_ID,SUBJECT=CASE A.SBG_PARENT_ID WHEN 0 THEN A.SBG_DESCR" _
    '                             & " ELSE A.SBG_DESCR+'-'+B.SBG_SHORTCODE END FROM " _
    '                             & " SUBJECTS_GRADE_S AS A LEFT OUTER JOIN SUBJECTS_GRADE_S AS B " _
    '                             & " ON A.SBG_PARENT_ID=B.SBG_SBM_ID" _
    '                             & " AND B.SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND B.SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND B.SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
    '                             & " WHERE A.SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND A.SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND A.SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
    '                             & " ORDER BY SUBJECT"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlSubject.DataSource = ds
    '    ddlSubject.DataTextField = "SUBJECT"
    '    ddlSubject.DataValueField = "SBG_ID"
    '    ddlSubject.DataBind()
    'End Sub

    Sub PopulateGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlTeacher.SelectedItem.Text = "ALL" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE " _
                        & " SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                        & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                        & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString _
                        & " AND SGR_SBG_ID=" + ddlSubjects.SelectedItem.Value.ToString
        Else
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                      & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID" _
                      & " WHERE SGS_EMP_ID=" + ddlTeacher.SelectedValue.ToString _
                      & " AND ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01'" _
                      & " AND '" + Format(Now.Date, "yyyy-MM-dd") + "'" _
                      & " AND SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                      & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                      & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                      & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString _
                      & " AND SGR_SBG_ID=" + ddlSubjects.SelectedItem.Value.ToString.ToString

        End If
        If str_query <> "" Then
            str_query += "ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub
    Sub PopulateNewGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " LEFT OUTER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                                & " WHERE SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString _
                                & " AND SGR_SBG_ID=" + ddlSubjects.SelectedItem.Value.ToString _
                                & " AND SGR_ID<>" + ddlGroup.SelectedValue.ToString
        If ddlMTeacher.SelectedValue <> "" Then
            str_query += " AND SGS_EMP_ID=" + ddlMTeacher.SelectedValue.ToString _
                                & " AND SGS_TODATE IS NULL "
        End If
        If str_query <> "" Then
            str_query += " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlNewGroup.DataSource = ds
        ddlNewGroup.DataTextField = "SGR_DESCR"
        ddlNewGroup.DataValueField = "SGR_ID"
        ddlNewGroup.DataBind()
    End Sub
    Sub GridBind(ByVal gvGrid As GridView, ByVal sgr_id As String)

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String


        strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                     & " SCT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                                     & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                                     & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                                     & " WHERE STU_bACTIVE='TRUE'" _
                                     & " AND SSD_SGR_ID=" + sgr_id _
                                     & " ORDER BY STU_NAME "




        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvHouse As New DropDownList
        Dim selectedHouse As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""

        If gvGrid.Rows.Count > 0 Then

            ddlgvHouse = gvGrid.HeaderRow.FindControl("ddlgvHouse")

            txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
            End If


            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvGrid.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGrid.DataBind()
            Dim columnCount As Integer = gvGrid.Rows(0).Cells.Count
            gvGrid.Rows(0).Cells.Clear()
            gvGrid.Rows(0).Cells.Add(New TableCell)
            gvGrid.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGrid.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGrid.Rows(0).Cells(0).Text = "No records"
        Else
            gvGrid.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvGrid.Rows.Count > 0 Then


            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            Dim dr As DataRow


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")


            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(4))
                    End If
                End With

            Next
            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If


        End If

    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvNewGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvNewGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvNewGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim lblSsdId As Label
        Dim i As Integer
        For i = 0 To gvGroup.Rows.Count - 1
            chkSelect = gvGroup.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSsdId = gvGroup.Rows(i).FindControl("lblSsdid")
                str_query = "exec saveCHANGEGROUP_M " + ddlNewGroup.SelectedValue.ToString + "," + lblSsdId.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next
    End Sub

    Sub updateSubjectURL()
        hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + ddlGrade.SelectedValue + "&stmid=" + ddlStream.SelectedValue.ToString
    End Sub

#End Region

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            PopulateGroup()
            PopulateMoveTeacher()
            If ddlGroup.Items.Count <> 0 And ddlMTeacher.Items.Count <> 0 Then
                PopulateNewGroup()
                GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
                btnMove.Enabled = True
            Else
                gvGroup.DataBind()
                btnMove.Enabled = False
            End If
            If ddlNewGroup.Items.Count <> 0 Then
                GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
                btnMove.Enabled = True
            Else
                gvNewGroup.DataBind()
                btnMove.Enabled = False
            End If
            tblGroup.Rows(4).Visible = True
            tblGroup.Rows(5).Visible = True
            tblGroup.Rows(6).Visible = True
            tblGroup.Rows(7).Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            PopulateNewGroup()
            GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
            If ddlNewGroup.Items.Count <> 0 Then
                GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            PopulateGradeStream()
            BindSubjects()
            updateSubjectURL()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            PopulateGradeStream()
            BindSubjects()
            updateSubjectURL()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            PopulateGradeStream()
            BindSubjects()
            updateSubjectURL()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            BindSubjects()
            updateSubjectURL()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlNewGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNewGroup.SelectedIndexChanged
        Try
            GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMove.Click
        Try
            SaveData()
            GridBind(gvGroup, ddlGroup.SelectedValue.ToString)
            GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub imgSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSub.Click
        'PopulateGroup()
    End Sub

    Protected Sub ddlMTeacher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMTeacher.SelectedIndexChanged
        Try
            PopulateNewGroup()
            If ddlNewGroup.Items.Count <> 0 Then
                GridBind(gvNewGroup, ddlNewGroup.SelectedValue.ToString)
                btnMove.Enabled = True
            Else
                btnMove.Enabled = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub BindSubjects()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        'Dim grade As String()
        'grade = ddlGrade.SelectedValue.Split("|")
        str_query = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                      & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + ddlAcademicYear.SelectedItem.Value _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedItem.Value + "'"


        'If grade(1) <> "0" And grade(1) <> "" Then
        '    str_query += " AND SBG_STM_ID=" + grade(1)
        'End If

        If str_query <> "" Then
            str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlSubjects.DataSource = ds
                ddlSubjects.DataTextField = "SBG_DESCR"
                ddlSubjects.DataValueField = "SBG_ID"
                ddlSubjects.DataBind()
            End If
        End If
    End Sub

End Class
