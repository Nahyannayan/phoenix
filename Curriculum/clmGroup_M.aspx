<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGroup_M.aspx.vb" Inherits="Curriculum_clmGroup_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <script language="javascript" type="text/javascript">

function getSubject() 
{        
var sFeatures;
sFeatures="dialogWidth: 445px; ";
sFeatures+="dialogHeight: 310px; ";
sFeatures+="help: no; ";
sFeatures+="resizable: no; ";
sFeatures+="scroll: yes; ";
sFeatures+="status: no; ";
sFeatures+="unadorned: no; ";
var NameandCode;
var result;
var url;
// url='../Students/ShowAcademicInfo.aspx?id='+mode;

url=document.getElementById("<%=hfSubjectURL.ClientID %>").value

result = window.showModalDialog(url,"", sFeatures);
     
if (result=='' || result==undefined)
{    return false; 
}   

NameandCode = result.split('|');  
document.getElementById("<%=txtSubject.ClientID %>").value=NameandCode[0];
document.getElementById("<%=hfSBG_ID.ClientID %>").value=NameandCode[1];
var Subj;



if (document.getElementById("<%=hfAddNew.ClientID %>").value=='add')
{
Subj=document.getElementById("<%=hfDescr.ClientID %>").value;
document.getElementById("<%=txtDescr.ClientID %>").value=Subj+NameandCode[2];
}

}





function confirm_delete()
{

if (confirm("You are about to remove the selected students from this group.Do you want to proceed?")==true)
return true;
else
return false;

}




var color = ''; 
function highlightView(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvView.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}

function highlightAdd(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvAdd.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}

function change_chk_state(chkThis)
{
var chk_state= ! chkThis.checked ;
for(i=0; i<document.forms[0].elements.length; i++)
{
var currentid =document.forms[0].elements[i].id; 
if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
{
//if (document.forms[0].elements[i].type=='checkbox' )
//if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
document.forms[0].elements[i].checked=chk_state;
document.forms[0].elements[i].click();//fire the click event of the child element
}
}
}           

    </script>



        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Group Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM2" style="text-align: left" />&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" >
                <asp:Label ID="lblError1" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
       <%-- <tr>
            <td align="left">
                <asp:LinkButton id="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>--%>
        <tr>
            <td width="100%" >
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    
                    <tr>
                        <td align="left" width="20%">
                            <asp:Label ID="lblAccText"  class="field-label" runat="server" Text="Select Academic Year ">
                            </asp:Label></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:Label ID="lblStream"  class="field-label" runat="server" Text="Stream"></asp:Label></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trshift" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblGrade"  class="field-label" runat="server" Text="Select Grade"></asp:Label></td>
                        
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:Label ID="lblShift"  class="field-label" runat="server" Text="Shift"></asp:Label></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlShift" runat="server">
                            </asp:DropDownList></td>
                     
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lbl1"  class="field-label" runat="server" Text="Subject "></asp:Label></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtSubject" runat="server" TabIndex="2" MaxLength="100"
                                Enabled="False" Visible="false">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgSub" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return getSubject();" TabIndex="18" Visible="FALSE"/>
                                <asp:DropDownList ID="ddlSubjects" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span  class="field-label">Group Name</span></td>
                        
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtDescr" runat="server" TabIndex="2" MaxLength="100">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trUntis" runat="server">
                        <td align="left">
                            <span  class="field-label">Untis Group Name</span></td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtUntisGroup" runat="server" TabIndex="2" MaxLength="100">
                            </asp:TextBox></td>

                       <td align="left" colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table id="TB7" width="100%" runat="server">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lbl15"  class="field-label" runat="server" Text="Teacher"></asp:Label></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTeacher" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"  width="20%">
                                        <span  class="field-label">From Date</span>
                                    </td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled" >
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /> <%--ImageAlign="right" --%>
                                    </td>
                                    <td align="left">
                                        <asp:Button id="btnAddNew" tabIndex="7" runat="server" Text="Add Teacher" CssClass="button"
                                             ValidationGroup="groupM2"></asp:Button></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span  class="field-label">Schedule</span></td>
                                   
                                    <td align="left">
                                        <asp:TextBox ID="txtSchedule" runat="server" TabIndex="2" MaxLength="100">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <span  class="field-label">Room No</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtRoomNo" runat="server" TabIndex="2" MaxLength="100">
                                        </asp:TextBox></td>
                                    <td align="left">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:GridView ID="gvTeacher" PageSize="2" runat="server" AutoGenerateColumns="false"
                                            EmptyDataText="No Teachers for this group" Width="100%" cssclass="table table-bordered table-row">
                                            <rowstyle/>
                                            <columns>

        <asp:TemplateField HeaderText="eid" Visible="False"><ItemTemplate>
        <asp:Label ID="lblEmpId" runat="server" text='<%# Bind("EMP_ID") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Teacher" ><ItemTemplate>
        <asp:Label ID="lblTeacher" runat="server"  text='<%# Bind("EMP_NAME") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
         <asp:TemplateField HeaderText="Schedule" ><ItemTemplate>
        <asp:Label ID="lblSchedule" runat="server"  text='<%# Bind("SGS_SCHEDULE") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
         <asp:TemplateField HeaderText="Room No." ><ItemTemplate>
        <asp:Label ID="lblRoomNo" runat="server"  text='<%# Bind("SGS_ROOMNO") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        

        <asp:TemplateField HeaderText="From Date"><ItemTemplate>
        <asp:Label ID="lblFrom" runat="server"   text='<%# Bind("FROMDATE") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:ButtonField CommandName="edit" HeaderText="edit" Text="edit"  >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:ButtonField>
        

        <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete"  >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:ButtonField>

        </columns>
                                            <selectedrowstyle />
                                            <headerstyle  />
                                            <alternatingrowstyle  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <tr>
                            <td align="center"  valign="bottom" colspan="6">
                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" ValidationGroup="groupM1"
                                    TabIndex="7" />
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                    TabIndex="7" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                        </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="7" style="border-bottom: 1px solid #dee2e6;">
                <asp:Menu id="mnuMaster" Orientation="Horizontal" Runat="server" OnMenuItemClick="mnuMaster_MenuItemClick" CssClass="menu_a">
                    <items  >
          
            <asp:MenuItem Text="View Students" Value="0" Selected="True" > </asp:MenuItem> <%--ImageUrl="~/Images/tabmenu/btnViewStudents2.jpg"--%>
            <asp:MenuItem Text="Allocate Students" Value="1"  > </asp:MenuItem><%--ImageUrl="~/Images/tabmenu/btnAllocateStudents1.jpg"--%>
          </items>
                 <StaticMenuItemStyle CssClass="menuItem" />
<StaticSelectedStyle CssClass="selectedItem" />
<StaticHoverStyle CssClass="hoverItem" />
                </asp:Menu>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="7">
                <asp:RadioButton id="rdAll" runat="server" AutoPostBack="True" Checked="True" GroupName="g1"
                    Text="All">
                </asp:RadioButton>
                <asp:RadioButton id="rdMuslim" runat="server" AutoPostBack="True" GroupName="g1"
                    Text="Muslim">
                </asp:RadioButton>
                <asp:RadioButton id="rdNonMuslim" runat="server" AutoPostBack="True" GroupName="g1"
                    Text="Non Muslim">
                </asp:RadioButton></td>
        </tr>
        <tr>
            <td colspan="7" align="left">
                <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table id="tb1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="gvView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="No Records Found" PageSize="20" Width="100%">
                                        <rowstyle  />
                                        <columns>
        
        
        <asp:TemplateField HeaderText="Available"><EditItemTemplate>
        <asp:CheckBox ID="chkSelect" runat="server"  />
        </EditItemTemplate>
        <HeaderTemplate>
        
        Select 
            <br />
        <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
        ToolTip="Click here to select/deselect all rows" />
        </HeaderTemplate>
        <ItemTemplate>
        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlightView(this);" />
        </ItemTemplate>
        <HeaderStyle Wrap="False"></HeaderStyle>
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="STU_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblStuId" runat="server" text='<%# Bind("STU_ID") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="STU_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblSctId" runat="server" text='<%# Bind("STU_SCT_ID") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="SSD_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblSsdId" runat="server" text='<%# Bind("SSD_ID") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="SL.No"><ItemTemplate>
        <asp:Label ID="lblSlNo" runat="server" text='<%# getSerialNoView() %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        
        <asp:TemplateField HeaderText="Stud No."><HeaderTemplate>
        
        <asp:Label ID="lblh1" runat="server" Text="Stud. No"></asp:Label>
            <br />
        
        <asp:TextBox ID="txtStuNo" runat="server" ></asp:TextBox>
        <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />

        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Student Name"><HeaderTemplate>
        
        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label>
            <br />
        <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
        
        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblStuName" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Section"><HeaderTemplate>
        
        Section<br />
        <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged" >
        </asp:DropDownList>

        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        
          <asp:TemplateField HeaderText="Option"><HeaderTemplate>
        
        Option
            <br />
          <asp:DropDownList ID="ddlgvOption" runat="server" AutoPostBack="True" 
        OnSelectedIndexChanged="ddlgvOption_SelectedIndexChanged" >
        </asp:DropDownList>

        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblOption" runat="server" text='<%# Bind("OPT_DESCR") %>'></asp:Label>

        </ItemTemplate>
        </asp:TemplateField>
        
        
        </columns>
                                        <selectedrowstyle />
                                        <headerstyle  />
                                        <alternatingrowstyle  />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="Remove from Group"
                                        OnClientClick="javascript:return confirm_delete();" ValidationGroup="groupM1"
                                        TabIndex="7" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table id="Table1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="gvAdd" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="No Records Found" PageSize="20" Width="100%">
                                        <rowstyle  />
                                        <columns>
        <asp:TemplateField HeaderText="Available"><EditItemTemplate>
        <asp:CheckBox ID="chkSelect" runat="server"  />

        </EditItemTemplate>
        <HeaderTemplate>
        
        Select 
            <br />
        <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
        ToolTip="Click here to select/deselect all rows" />
        </HeaderTemplate>
        <ItemTemplate>
        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlightAdd(this);" />
        </ItemTemplate>
        <HeaderStyle Wrap="False"></HeaderStyle>
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
        </asp:TemplateField>
        
             <asp:TemplateField HeaderText="STU_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblStuId" runat="server" text='<%# Bind("STU_ID") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="STU_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblSctId" runat="server" text='<%# Bind("STU_SCT_ID") %>'></asp:Label>

        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="SSD_ID" Visible="False"><ItemTemplate>
        <asp:Label ID="lblSsdId" runat="server" text='<%# Bind("SSD_ID") %>'></asp:Label>
        </ItemTemplate>
        
        </asp:TemplateField>
        <asp:TemplateField HeaderText="SL.No"><ItemTemplate>
        <asp:Label ID="lblSlNo" runat="server" text='<%# getSerialNoAdd() %>'></asp:Label>

        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Stud No."><HeaderTemplate>
        
        <asp:Label ID="lblh1" runat="server" Text="Stud. No"></asp:Label>
            <br />
        
        <asp:TextBox ID="txtStuNo" runat="server" Width="100px"></asp:TextBox>
        <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
        
        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Student Name"><HeaderTemplate>
        
        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label>
            <br />
        <asp:TextBox ID="txtStudName" runat="server" ></asp:TextBox>
        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
        
        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblStuName" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Section"><HeaderTemplate>
        
        Section
            <br />
          <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
        </asp:DropDownList>

        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>

        </ItemTemplate>
        </asp:TemplateField>
        
        
        
        
        <asp:TemplateField HeaderText="Option"><HeaderTemplate>
        <table>
        <tr>
        <td align="center">
        Option</td>
        </tr>
        <tr>
        <td align="center">
          <asp:DropDownList ID="ddlgvOption" runat="server" AutoPostBack="True" 
        OnSelectedIndexChanged="ddlgvOption_SelectedIndexChanged" >
        </asp:DropDownList></td>
        </tr>
        </table>
        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="lblOption" runat="server" text='<%# Bind("OPT_DESCR") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        
        
        
        </columns>
                                        <selectedrowstyle  />
                                        <headerstyle  />
                                        <alternatingrowstyle  />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Button ID="btnAllocate" runat="server" CssClass="button" Text="Add to Group"
                                        ValidationGroup="groupM1" TabIndex="7"  />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView></td>
        </tr>
        <tr>
            <td >
                <asp:HiddenField ID="hfSGR_ID" runat="server" />
                <asp:HiddenField ID="hfBOptional" runat="server" />
                <asp:HiddenField ID="hfGroupName" runat="server" />
                <asp:HiddenField ID="hfSubjectURL" runat="server" />
                <asp:HiddenField ID="hfSBG_ID" runat="server" /><asp:HiddenField ID="hfGROUP" runat="server" />
                <asp:HiddenField ID="hfSBG_PARENT_ID" runat="server" /><asp:HiddenField ID="hfAddNew" runat="server" />
                
              <%--  <ajaxToolkit:AlwaysVisibleControlExtender
                        ID="AlwaysVisibleControlExtender1" runat="server" HorizontalSide="Center" TargetControlID="lblError1" 
                        VerticalSide="Middle">
                    </ajaxToolkit:AlwaysVisibleControlExtender>--%>
                <asp:HiddenField ID="hfDescr" runat="server" />
                <asp:RequiredFieldValidator ID="rfDescr" runat="server" ErrorMessage="Please enter the field Description"
                    ControlToValidate="txtDescr" Display="None" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rffrom" runat="server" ErrorMessage="Please enter the field From Date"
                    ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM2">
                </asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="txtFrom"
                    TargetControlID="txtFrom" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
   
</asp:Content>
