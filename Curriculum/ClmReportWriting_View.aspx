﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ClmReportWriting_View.aspx.vb" Inherits="Curriculum_ClmReportWriting_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="Literal1" runat="server" Text=" Report Writing"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">

                            <table id="Table1" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="3" align="center">

                                        <table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">



                                            <tr>
                                                <td colspan="3" align="center">
                                                    <asp:GridView ID="gvReport" runat="server"
                                                        AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        PageSize="20" AllowPaging="false" Width="100%">
                                                        <Columns>


                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("RPF_DESCR") %>', 'one');">
                                                                        <img id="imgdiv<%# Eval("RPF_DESCR") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                                    </a>
                                                                </ItemTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("RPF_DESCR") %>', 'alt');">
                                                                        <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                                    </a>
                                                                </AlternatingItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Report">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("rpf_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Grades" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrades" runat="server" Text='<%# Bind("Grades") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    </td></tr>
             <tr>
                 <td colspan="100%">
                     <div id="div<%# Eval("rpf_descr") %>" style="display: none; position: relative; left: 20px;">
                         <asp:GridView ID="gvGroup" runat="server" Width="98%" CssClass="table table-bordered table-row"
                             AutoGenerateColumns="false" EmptyDataText="No groups for this teacher.">

                             <Columns>

                                 <asp:TemplateField HeaderText="Grade">
                                     <ItemTemplate>
                                         <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Subject">
                                     <ItemTemplate>
                                         <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Group">
                                     <ItemTemplate>
                                         <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="SGR_ID" Visible="False">
                                     <ItemTemplate>
                                         <asp:Label ID="lblSgr_ID" runat="server" Text='<%# Bind("SGR_ID") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                     <ItemTemplate>
                                         <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="RPF_DESCR" Visible="False">
                                     <ItemTemplate>
                                         <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="SBG_ID" Visible="False">
                                     <ItemTemplate>
                                         <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>


                                 <asp:TemplateField>
                                     <HeaderTemplate>
                                         <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                     </HeaderTemplate>
                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                     <ItemTemplate>
                                         <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit Report"></asp:LinkButton>
                                     </ItemTemplate>
                                 </asp:TemplateField>



                             </Columns>
                             <RowStyle CssClass="griditem" Font-Names="Verdana" />
                             <HeaderStyle CssClass="gridheader_pop" />
                         </asp:GridView>
                     </div>
                 </td>
             </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" Font-Names="Verdana" />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <PagerStyle HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /></td>
                    </tr>

                </table>


            </div>
        </div>
    </div>
</asp:Content>

