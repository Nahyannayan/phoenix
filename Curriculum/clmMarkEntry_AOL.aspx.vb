﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml
Partial Class Curriculum_clmMarkEntry_AOL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    lblActivity.Text = Encr_decrData.Decrypt(Request.QueryString("activity").Replace(" ", "+"))
                    lblSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    lblGroup.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    lblDate.Text = Encr_decrData.Decrypt(Request.QueryString("date").Replace(" ", "+"))
                    hfCAS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("casid").Replace(" ", "+"))
                    hfMEntered.Value = Encr_decrData.Decrypt(Request.QueryString("menterd").Replace(" ", "+"))
                    hfEntryType.Value = Encr_decrData.Decrypt(Request.QueryString("entry").Replace(" ", "+"))
                    hfGradeSlab.Value = Encr_decrData.Decrypt(Request.QueryString("gradeslab").Replace(" ", "+"))
                    hfMinMarks.Value = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfMaxMarks.Value = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMaxmarks.Text = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMinmarks.Text = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfWithoutSkills.Value = Encr_decrData.Decrypt(Request.QueryString("ws").Replace(" ", "+"))
                    getSubject()
                    GridBind()
                    'gvStud.Attributes.Add("bordercolor", "#1b80b6")
                    '  tblact.Rows(3).Visible = False
                    '  tblact.Rows(4).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


#Region "Private methods"
    Sub getSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAS_SBG_ID,ISNULL(CAS_AOL_KU_MAXMARK,10),ISNULL(CAS_AOL_APP_MAXMARK,10),ISNULL(CAS_AOL_COMM_MAXMARK,10),ISNULL(CAS_AOL_HOTS_MAXMARK,10),ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10) FROM ACT.ACTIVITY_SCHEDULE WHERE CAS_ID='" + hfCAS_ID.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        With ds.Tables(0).Rows(0)
            Session("SBGID") = .Item(0).ToString
            hfKUMaxMarks.Value = Replace(.Item(1).ToString, ".0", "")
            hfAPPLMaxMarks.Value = Replace(.Item(2).ToString, ".0", "")
            hfCOMMMaxMarks.Value = Replace(.Item(3).ToString, ".0", "")
            hfHOTSMaxMarks.Value = Replace(.Item(4).ToString, ".0", "")
            hfWSMaxMarks.Value = Replace(.Item(5).ToString, ".0", "")
        End With


    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvStud.PageSize * gvStud.PageIndex)
    End Function
    Sub GridBind()
        ViewState("slno") = 0
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STA_ID,STU_ID,STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                 & " +' '+ISNULL(STU_LASTNAME,'')),STU_NO,CASE  WHEN STA_MARK IS NULL THEN '' " _
                                 & " ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STA_MARK) AS VARCHAR),'.00','') END AS MARKS,STA_GRADE," _
                                 & " MAXMARKS=" + hfMaxMarks.Value + ",MINMARKS=" + hfMinMarks.Value _
                                 & " ,ERR='Marks Should be between 0 and " + hfMaxMarks.Value + "'" _
                                 & " ,CASE  WHEN STA_bATTENDED='P' THEN 'TRUE' WHEN STA_bATTENDED IS NULL THEN 'TRUE' ELSE 'FALSE' END AS ENABLE, " _
                                 & " CASE  WHEN STS_KU_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_KU_M) AS VARCHAR),'.00','') END AS KU_MARKS,STS_KU_G," _
                                 & " CASE  WHEN STS_APPL_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_APPL_M) AS VARCHAR),'.00','') END AS APP_MARKS,STS_APPL_G," _
                                 & " CASE  WHEN STS_COMM_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_COMM_M) AS VARCHAR),'.00','') END AS COMM_MARKS,STS_COMM_G," _
                                 & " CASE  WHEN STS_HOTS_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_HOTS_M) AS VARCHAR),'.00','') END AS HOTS_MARKS,STS_HOTS_G," _
                                 & " CASE  WHEN STS_WITHOUTSKILLS_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_WITHOUTSKILLS_M) AS VARCHAR),'.00','') END AS WITHOUTSKILLS_MARKS,STS_WITHOUTSKILLS_G," _
                                 & " STS_CHAPTER,STS_FEEDBACK,STS_WOT,STS_TARGET," _
                                 & " KU_MAXMARKS=ISNULL(CAS_AOL_KU_MAXMARK,10),APP_MAXMARKS=ISNULL(CAS_AOL_APP_MAXMARK,10),COMM_MAXMARKS=ISNULL(CAS_AOL_COMM_MAXMARK,10),HOTS_MAXMARKS=ISNULL(CAS_AOL_HOTS_MAXMARK,10),WITHOUTSKILLS_MAXMARKS=ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10)" _
                                 & " ,ERR_KU='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_KU_MAXMARK,10))" _
                                 & " ,ERR_APPL='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_APP_MAXMARK,10))" _
                                 & " ,ERR_COMM='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_COMM_MAXMARK,10))" _
                                 & " ,ERR_HOTS='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_HOTS_MAXMARK,10))" _
                                 & " ,ERR_WITHOUTSKILLS='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10))" _
                                 & " ,STA_bATTENDED" _
                                 & ", CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END AS STU_STATUS" _
                                 & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B" _
                                 & " ON A.STU_ID=B.STA_STU_ID" _
                                 & " LEFT OUTER JOIN ACT.STUDENT_ACTIVITY_AOL AS C ON B.STA_ID=C.STS_STA_ID" _
                                 & " LEFT OUTER JOIN ACT.ACTIVITY_SCHEDULE AS D ON B.STA_CAS_ID=D.CAS_ID" _
                                 & " WHERE STA_CAS_ID=" + hfCAS_ID.Value _
                                 & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()


        Dim lKUMarks As Label

        lKUMarks = gvStud.HeaderRow.FindControl("lKUMarks")
        If hfKUMaxMarks.Value <> "0" Then
            lKUMarks.Text = "(" + hfKUMaxMarks.Value + ")"
        End If


        Dim lAPPLMarks As Label

        lAPPLMarks = gvStud.HeaderRow.FindControl("lAPPLMarks")
        If hfAPPLMaxMarks.Value <> "0" Then
            lAPPLMarks.Text = "(" + hfAPPLMaxMarks.Value + ")"
        End If


        Dim lCOMMMarks As Label

        lCOMMMarks = gvStud.HeaderRow.FindControl("lCOMMMarks")
        If hfCOMMMaxMarks.Value <> "0" Then
            lCOMMMarks.Text = "(" + hfCOMMMaxMarks.Value + ")"
        End If

        Dim lHOTSMarks As Label

        lHOTSMarks = gvStud.HeaderRow.FindControl("lHOTSMarks")
        If hfHOTSMaxMarks.Value <> "0" Then
            lHOTSMarks.Text = "(" + hfHOTSMaxMarks.Value + ")"
        End If

        Dim lWSMarks As Label

        lWSMarks = gvStud.HeaderRow.FindControl("lWSMarks")
        If hfWSMaxMarks.Value <> "0" Then
            lWSMarks.Text = "(" + hfWSMaxMarks.Value + ")"
        End If

        If hfWithoutSkills.Value.ToLower = "true" Then
            gvStud.Columns(6).Visible = False
            gvStud.Columns(7).Visible = False
            gvStud.Columns(8).Visible = False
            gvStud.Columns(9).Visible = False
        Else
            gvStud.Columns(10).Visible = False
        End If




    End Sub


    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim lblStaId As Label
        Dim txtKUMarks As TextBox
        Dim txtAPPMarks As TextBox
        Dim txtCOMMMarks As TextBox
        Dim txtHOTSMarks As TextBox
        Dim txtWSMarks As TextBox
        Dim txtChapter As TextBox
        Dim txtFeedBack As TextBox
        Dim txtWOT As TextBox
        Dim txtTarget As TextBox


        Dim grade As String
        Dim ddlGrade As DropDownList
        Dim cmd As SqlCommand
        Dim AuditID As Int32

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                cmd = New SqlCommand("[ACT].[SaveAUDITDATA]", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 50)
                sqlpBSU_ID.Value = Session("SBsuid")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDOCTYPE As New SqlParameter("@AUD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpDOCNO As New SqlParameter("@AUD_DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpPROCEDURE As New SqlParameter("@AUD_PROCEDURE", SqlDbType.VarChar, 200)
                sqlpPROCEDURE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpPROCEDURE)

                Dim sqlpLOGDT As New SqlParameter("@AUD_LOGDT", SqlDbType.DateTime)
                sqlpLOGDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpLOGDT)

                Dim sqlpACTION As New SqlParameter("@AUD_ACTION", SqlDbType.VarChar, 50)
                sqlpACTION.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpACTION)

                Dim sqlpDT As New SqlParameter("@AUD_DT", SqlDbType.DateTime)
                sqlpDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpDT)

                Dim sqlpUSER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
                sqlpUSER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpUSER)

                Dim sqlpREMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 200)
                sqlpREMARKS.Value = "Marks"
                cmd.Parameters.Add(sqlpREMARKS)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                AuditID = retValParam.Value
                If AuditID <> 0 Then
                    For i = 0 To gvStud.Rows.Count - 1

                        lblStaId = gvStud.Rows(i).FindControl("lblStaId")
                        txtKUMarks = gvStud.Rows(i).FindControl("txtKUMarks")
                        txtAPPMarks = gvStud.Rows(i).FindControl("txtAPPMarks")
                        txtCOMMMarks = gvStud.Rows(i).FindControl("txtCOMMMarks")
                        txtHOTSMarks = gvStud.Rows(i).FindControl("txtHOTSMarks")
                        txtWSMarks = gvStud.Rows(i).FindControl("txtWSMarks")

                        txtChapter = gvStud.Rows(i).FindControl("txtChapter")
                        txtFeedBack = gvStud.Rows(i).FindControl("txtFeedBack")
                        txtWOT = gvStud.Rows(i).FindControl("txtWOTs")
                        txtTarget = gvStud.Rows(i).FindControl("txtTarget")


                        ddlGrade = gvStud.Rows(i).FindControl("ddlGrade")
                        If txtKUMarks.Enabled = True Then
                            str_query = "ACT.saveMARKENTRY_AOL " _
                                           & lblStaId.Text + "," _
                                           & IIf(txtKUMarks.Text = "", "NULL", txtKUMarks.Text.ToString) + "," _
                                           & IIf(txtAPPMarks.Text = "", "NULL", txtAPPMarks.Text.ToString) + "," _
                                           & IIf(txtCOMMMarks.Text = "", "NULL", txtCOMMMarks.Text.ToString) + "," _
                                           & IIf(txtHOTSMarks.Text = "", "NULL", txtHOTSMarks.Text.ToString) + "," _
                                           & IIf(txtWSMarks.Text = "", "NULL", txtWSMarks.Text.ToString) + "," _
                                           & "'" + txtChapter.Text.Replace("'", "''") + "'," _
                                           & "'" + txtFeedBack.Text.Replace("'", "''") + "'," _
                                           & "'" + txtWOT.Text.Replace("'", "''") + "'," _
                                           & "'" + txtTarget.Text.Replace("'", "''") + "'," _
                                           & "'" + hfWithoutSkills.Value + "'," _
                                           & CStr(AuditID) + "," _
                                           & "'" + Session("SUSR_nAME") + "'"

                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If
                        lblError.Text = "Record saved successfully"

                    Next
                End If
                str_query = "ACT.updateBMARKENTERD " + hfCAS_ID.Value
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub


    Function checkPublishActiviyEnable(ByVal stu_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
                                 & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS B ON A.STA_CAS_ID=B.CAS_ID" _
                                 & " INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON B.CAS_CAD_ID=C.RRD_CAD_ID" _
                                 & " INNER JOIN RPT.REPORT_RULE_M AS E ON C.RRD_RRM_ID=E.RRM_ID" _
                                 & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS F ON A.STA_STU_ID=F.RPP_STU_ID " _
                                 & " AND F.RPP_RPF_ID=E.RRM_RPF_ID AND F.RPP_TRM_ID=E.RRM_TRM_ID" _
                                 & " AND F.RPP_ACD_ID=E.RRM_ACD_ID AND F.RPP_GRD_ID=E.RRM_GRD_ID" _
                                 & " WHERE RPP_bPUBLISH='TRUE' AND RPP_STU_ID=" + stu_id _
                                 & " AND STA_CAS_ID=" + hfCAS_ID.Value
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub CopyHeader(ByVal header As String)
        Dim txt As TextBox
        Dim i As Integer
        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)
                txt = .FindControl(header)
                txt.Text = txtHeader.Text
            End With
        Next
    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        'Select Case ddlHeader.SelectedValue
        '    Case "CH"
        '        CopyHeader("txtChapter")
        '    Case "FB"
        '        CopyHeader("txtFeedBack")
        '    Case "WOT"
        '        CopyHeader("txtWOT")
        '    Case "TG"
        '        CopyHeader("txtTarget")
        'End Select
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim imgBtn As ImageButton
            Dim lblStuId As Label
            Dim lblAtt As Label
            Dim lblStatus As Label

            Dim txtKUMarks As TextBox
            Dim txtAPPMarks As TextBox
            Dim txtCOMMMarks As TextBox
            Dim txtHOTSMarks As TextBox
            Dim txtWSMarks As TextBox


            lblStatus = e.Row.FindControl("lblStatus")

            If lblStatus.Text <> "" Then
                lblStatus.ForeColor = Drawing.Color.Red
            End If


            Dim txtFeedBack As TextBox
            txtFeedBack = e.Row.FindControl("txtFeedBack")

            Dim txtChapter As TextBox
            txtChapter = e.Row.FindControl("txtChapter")

            ClientScript.RegisterArrayDeclaration("txtCH", String.Concat("'", txtChapter.ClientID, "'"))
            ClientScript.RegisterArrayDeclaration("txtFB", String.Concat("'", txtFeedBack.ClientID, "'"))

            lblStuId = e.Row.FindControl("lblStuId")
            imgBtn = e.Row.FindControl("imgBtn")
            lblAtt = e.Row.FindControl("lblAtt")

            txtKUMarks = e.Row.FindControl("txtKUMarks")
            txtAPPMarks = e.Row.FindControl("txtAPPMarks")
            txtCOMMMarks = e.Row.FindControl("txtCOMMMarks")
            txtHOTSMarks = e.Row.FindControl("txtHOTSMarks")
            txtWSMarks = e.Row.FindControl("txtWSMarks")

            If lblAtt.Text.ToLower = "a" Or lblAtt.Text.ToLower = "l" Then
                txtKUMarks.BackColor = Drawing.Color.Gray
                txtAPPMarks.BackColor = Drawing.Color.Gray
                txtCOMMMarks.BackColor = Drawing.Color.Gray
                txtHOTSMarks.BackColor = Drawing.Color.Gray
                txtWSMarks.BackColor = Drawing.Color.Gray

                txtKUMarks.ToolTip = "Absent"
                txtAPPMarks.ToolTip = "Absent"
                txtCOMMMarks.ToolTip = "Absent"
                txtHOTSMarks.ToolTip = "Absent"
                txtWSMarks.ToolTip = "Absent"
            End If


            If Not imgBtn Is Nothing Then
                imgBtn.OnClientClick = "javascript:getcomments('" + txtFeedBack.ClientID + "','ALLCMTS','AOL_CMTS','" + lblStuId.Text + "','0'); return false;"
            End If

            Try

                Dim bt As Boolean = checkPublishActiviyEnable(lblStuId.Text)
                If bt = False Then
                    e.Row.BackColor = Drawing.Color.FromArgb(Convert.ToInt32("deffb4", 16)) 'ffe5dc 'efffdb

                    txtKUMarks.Enabled = False
                    txtAPPMarks.Enabled = False
                    txtCOMMMarks.Enabled = False
                    txtHOTSMarks.Enabled = False
                    txtChapter.Enabled = False
                    txtWSMarks.Enabled = False
                End If
            Catch ex As Exception
            End Try

        End If
    End Sub
End Class
