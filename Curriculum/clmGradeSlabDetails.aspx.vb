Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.web.Configuration
Imports CURRICULUM

Partial Class CUR_GradeSlabDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_GRADESLAB_DETAILS Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            btnDelete.Visible = False
            gvGradeSlabDetails.Attributes.Add("bordercolor", "#1b80b6")
            Session("sCUR_GRD_SLAB_DET") = Nothing
            PopulateGradeSlabMaster()
            If ViewState("datamode") = "view" Then
                Dim GSD_GSM_SLAB_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("GSM_SLAB_ID").Replace(" ", "+"))
                Dim vHTCUR_GRD_SLAB_DET As Hashtable = GRADESLABDETAILS.GETGRADESLABDETAILS(GSD_GSM_SLAB_ID)
                Session("sCUR_GRD_SLAB_DET") = vHTCUR_GRD_SLAB_DET
                Dim strSelectedVal As String
                For Each lstItm As ListItem In ddlGradeSlab.Items
                    If lstItm.Value.StartsWith(GSD_GSM_SLAB_ID & "__") Then
                        strSelectedVal = lstItm.Value
                    End If
                Next
                ddlGradeSlab.Items.FindByValue(strSelectedVal).Selected = True
                GridBindGradeSlabDetails()
                DissableAllControls(True)
            End If
        End If
    End Sub

    Private Sub PopulateGradeSlabMaster()
        ddlGradeSlab.DataSource = GRADESLABDETAILS.PopulateGradeSlabMaster(Session("sBSUID"))
        ddlGradeSlab.DataTextField = "GSM_DESC"
        ddlGradeSlab.DataValueField = "SLB_MARK"
        ddlGradeSlab.DataBind()
    End Sub

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        ddlGradeSlab.Enabled = Not dissble
        txtSlabName.ReadOnly = dissble
        txtMinMark.ReadOnly = dissble
        txtMaxMark.ReadOnly = dissble
        gvGradeSlabDetails.Columns(5).Visible = Not dissble
    End Sub

    Private Sub GridBindGradeSlabDetails()
        Dim vHTGRD_SLB_DET As Hashtable
        If Not Session("sCUR_GRD_SLAB_DET") Is Nothing Then
            vHTGRD_SLB_DET = Session("sCUR_GRD_SLAB_DET")
            gvGradeSlabDetails.DataSource = GRADESLABDETAILS.GetSubDetailsAsDataTable(vHTGRD_SLB_DET)
            gvGradeSlabDetails.DataBind()
        Else
            gvGradeSlabDetails.DataSource = Nothing
            gvGradeSlabDetails.DataBind()
        End If
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vHTGRD_SLB_DET As New Hashtable
        If Not Session("sCUR_GRD_SLAB_DET") Is Nothing Then
            vHTGRD_SLB_DET = Session("sCUR_GRD_SLAB_DET")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vGRD_SLB_DET As GRADESLABDETAILS = vHTGRD_SLB_DET(Convert.ToInt64(lblFEE_ID.Text))
            If Not vGRD_SLB_DET Is Nothing Then
                vGRD_SLB_DET.Delete = True
            End If
        End If
        Session("sCUR_GRD_SLAB_DET") = vHTGRD_SLB_DET
        GridBindGradeSlabDetails()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction
        If Not Session("sCUR_GRD_SLAB_DET") Is Nothing Then
            Dim SLAB_ID As Integer = ddlGradeSlab.SelectedValue.Split("__")(0)
            Dim retVal As Integer = GRADESLABDETAILS.SaveDetails(Session("sCUR_GRD_SLAB_DET"), SLAB_ID, conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(retVal)
            Else
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") = "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = 0 ' UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    UtilityObj.Errorlog("Cannot write into Audit trial")
                    ' Throw New ArgumentException("Could not process your request")
                End If
                trans.Commit()
                lblError.Text = "Data updated Successfully"
                ClearDetails()
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Private Sub ClearDetails()
        ClearSubDetails()
        ddlGradeSlab.ClearSelection()
        Session("sCUR_GRD_SLAB_DET") = Nothing
        GridBindGradeSlabDetails()
    End Sub

    Private Sub ClearSubDetails()
        txtSlabName.Text = ""
        txtMinMark.Text = ""
        txtMaxMark.Text = ""
        btnDetAdd.Text = "Add"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableAllControls(False)
        gvGradeSlabDetails.Columns(3).Visible = True
        ViewState("datamode") = "add"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        Dim vGRD_SLB_DET As New Hashtable
        If Not Session("sCUR_GRD_SLAB_DET") Is Nothing Then
            vGRD_SLB_DET = Session("sCUR_GRD_SLAB_DET")
        End If
        vGRD_SLB_DET = AddDetails(vGRD_SLB_DET)
        Session("sCUR_GRD_SLAB_DET") = vGRD_SLB_DET
        GridBindGradeSlabDetails()
    End Sub

    Private Function AddDetails(ByVal htSLB_DET As Hashtable) As Hashtable
        Dim vSLB_DET As New GRADESLABDETAILS
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vSLB_DET = htSLB_DET(Convert.ToInt64(ViewState("Eid")))
        Else
            vSLB_DET.GSD_ID = GetNextMaxID(vSLB_DET)
        End If
        lblError.Text = ""
        vSLB_DET.GSD_DESC = txtSlabName.Text
        vSLB_DET.MAX_MARK = txtMaxMark.Text
        vSLB_DET.MIN_MARK = txtMinMark.Text
        htSLB_DET(Convert.ToInt64(vSLB_DET.GSD_ID)) = vSLB_DET
        ClearSubDetails()
        Return htSLB_DET
    End Function

    Private Function GetNextMaxID(ByVal vSLB_DET As GRADESLABDETAILS) As Integer
        If ViewState("NEXT_ID") Is Nothing Then
            ViewState("NEXT_ID") = GRADESLABDETAILS.GetNextID()
        Else
            ViewState("NEXT_ID") = ViewState("NEXT_ID") + 1
        End If
        vSLB_DET.NEWLY_ADDED = True
        Return ViewState("NEXT_ID")
    End Function

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vSLB_DET As New Hashtable
        If Not Session("sCUR_GRD_SLAB_DET") Is Nothing Then
            vSLB_DET = Session("sCUR_GRD_SLAB_DET")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vGRD_SLB_DET As GRADESLABDETAILS = vSLB_DET(Convert.ToInt64(lblFEE_ID.Text))
            If Not vGRD_SLB_DET Is Nothing Then
                txtSlabName.Text = vGRD_SLB_DET.GSD_DESC
                txtMinMark.Text = Math.Round(vGRD_SLB_DET.MIN_MARK, 2)
                txtMaxMark.Text = Math.Round(vGRD_SLB_DET.MAX_MARK, 2)
                ViewState("Eid") = CInt(lblFEE_ID.Text)
                btnDetAdd.Text = "Save"
            End If
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        DissableAllControls(False)
        ddlGradeSlab.Enabled = False
        gvGradeSlabDetails.Columns(3).Visible = True
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'If Session("sCUR_GRD_SLAB_DET") Is Nothing Then Return
        'Dim vFEE_ADJ As GRADESLABDETAILS = Session("sCUR_GRD_SLAB_DET")

        'For Each vFEE_DET As GRADESLABDETAILS_S In vFEE_ADJ.FEE_ADJ_DET.Values
        '    vFEE_DET.bDelete = True
        '    vFEE_DET.bEdit = True
        'Next
        'Session("sCUR_GRD_SLAB_DET") = vFEE_ADJ
        'btnSave_Click(sender, e)
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sCUR_GRD_SLAB_DET") = Nothing
        GridBindGradeSlabDetails()
    End Sub

End Class
