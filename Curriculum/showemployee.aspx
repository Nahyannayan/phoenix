<%@ Page Language="VB" AutoEventWireup="false" CodeFile="showemployee.aspx.vb" Inherits="Curriculum_showemployee" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
   
  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <base target="_self" />
    <title>Untitled Page</title> 
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>

<body onload="listen_window();">
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvempname" runat="server" AllowPaging="True"
            CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="False" PageSize="12">
            <Columns>
                <asp:TemplateField HeaderText="Employee Id" Visible="false">
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        &nbsp;<asp:Label ID="lblempid" runat="server"  Text='<%# Bind("EMP_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Id">
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        &nbsp;<asp:Label ID="lblempno" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Name">
                    <EditItemTemplate>
                        &nbsp;
                    </EditItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="lblHDESCR2" Text="Employee Name" runat="server"  
                             ></asp:Label>
                        <br />
                        <asp:TextBox ID="txtDESCR2" runat="server" ></asp:TextBox>
                        <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch2_Click" />&nbsp;
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkempname" runat="server" CommandName="Select" Text='<%# Bind("EMPname") %>'></asp:LinkButton>&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle />
            <HeaderStyle   />
            <AlternatingRowStyle  />
        </asp:GridView>
        <asp:HiddenField ID="h_SelectedId" runat="server" />
     <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
    </div>
    </form>
</body>
</html>
