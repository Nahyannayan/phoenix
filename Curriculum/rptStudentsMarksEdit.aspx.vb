Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_rptStudentsMarksEdit
    Inherits System.Web.UI.Page
    Dim studClass As New studClass
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Session("StuMarks") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330003") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then
                        btnAdd.Text = "Add"
                        BindGrid()
                        ddlAcdID = ActivityFunctions.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sBsuid"))
                    Else

                        H_ACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("AccId").Replace(" ", "+"))
                        H_GRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("GradeId").Replace(" ", "+"))
                        H_SBJ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SubjID").Replace(" ", "+"))
                        H_RPT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("MarksId").Replace(" ", "+"))
                        ddlAcdID = studClass.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sbsuid"))

                        DisplayRecordForEdit()
                        BindMarkEdit()
                    End If

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    btnDelete.Visible = True
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Functions"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function BindGrid()
        Dim dtTempDtl As New DataTable
        dtTempDtl = Session("StuMarks")
        If dtTempDtl.Rows.Count = 0 Then
            gvMarks.DataSource = dtTempDtl
            dtTempDtl.Rows.Add(dtTempDtl.NewRow())
            gvMarks.DataBind()
            Dim columnCount As Integer = gvMarks.Columns.Count
            gvMarks.Rows(0).Cells.Clear()
            gvMarks.Rows(0).Cells.Add(New TableCell)
            gvMarks.Rows(0).Cells(0).ColumnSpan = columnCount
            gvMarks.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvMarks.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            If gvMarks.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords." Then
                dtTempDtl.Rows(0).Delete()
            End If
            gvMarks.DataSource = dtTempDtl
            gvMarks.DataBind()
        End If
    End Function

    Private Function BindMarkEdit()
        Dim dtSetupD As DataTable
        Dim drSetupd As DataRow
        dtSetupD = New DataTable

        Try
            Dim RSDID As New DataColumn("RSDID", System.Type.GetType("System.String"))
            Dim RSDDESC As New DataColumn("RSDDESC", System.Type.GetType("System.String"))
            Dim RSDVALUE As New DataColumn("RSDVALUE", System.Type.GetType("System.String"))

            dtSetupD.Columns.Add(RSDID)
            dtSetupD.Columns.Add(RSDDESC)
            dtSetupD.Columns.Add(RSDVALUE)

            'Add Records Into DataTable
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            Dim strSQL As String = "SELECT RSD_ID,RSD_RESULT,RSD_CSSCLASS,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RPT_ID.Value & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' "
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            With dsHeader.Tables(0)
                For intCnt As Integer = 0 To .Rows.Count - 1
                    drSetupd = .NewRow()
                    drSetupd.Item("RSDID") = .Rows(intCnt).Item("RSD_ID")
                    drSetupd.Item("RSDDESC") = .Rows(intCnt).Item("RSD_HEADER")
                    drSetupd.Item("RSDVALUE") = ""
                    .Rows.Add(drSetupd)
                Next
            End With

            'Bind Grid Wirh Marks
            gvMarks.DataSource = dsHeader.Tables(0)
            gvMarks.DataBind()

        Catch ex As Exception

        End Try
    End Function

    Private Function GetReportHeader() As String
        Try


   
        Catch ex As Exception

        End Try

    End Function


    Private Function AddRecord()
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        Try
            If ViewState("datamode") = "add" Then
                ldrNew = Session("StuMarks").NewRow
                ldrNew("Id") = gvMarks.Rows.Count + 1
                ldrNew("RSD_ID") = H_SETUP.Value
                ldrNew("AcdId") = ddlAcdID.SelectedValue
                ldrNew("AcdYear") = ddlAcdID.SelectedItem.Text
                ldrNew("GrdId") = H_GRD_ID.Value
                ldrNew("Grade") = txtGrade.Text
                ldrNew("RPfID") = H_RPF_ID.Value 'H_RPT_ID
                ldrNew("Report") = txtReportId.Text
                ldrNew("GrdSbjId") = H_SBJ_GRD_ID.Value
                ldrNew("GrdSbj") = txtGrdSubject.Text
                ldrNew("GroupId") = H_GRP_ID.Value
                ldrNew("Group") = txtGroup.Text
                ldrNew("TypeLevel") = ddlLevel.SelectedValue
                ldrNew("SBJID") = H_SBJ_ID.Value
                ldrNew("Subject") = txtSubject.Text
                ldrNew("RptSchId") = H_SCH_ID.Value
                ldrNew("rptSchedule") = txtRptSchedule.Text
                ldrNew("studId") = H_STU_ID.Value
                ldrNew("StudName") = txtStuName.Text
                ldrNew("Mark") = txtMarks.Text
                ldrNew("MGrade") = txtGrdLevel.Text
                ldrNew("Comments") = txtComments.Text
                Session("StuMarks").Rows.Add(ldrNew)
            End If

            If ViewState("datamode") = "edit" Then
                If Not Session("StuMarks") Is Nothing Then
                    For lintIndex = 0 To Session("StuMarks").Rows.Count - 1
                        If (Session("StuMarks").Rows(lintIndex)("ID") = Session("gintGridLine")) Then

                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Function

    Private Sub SaveRecord()

        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            For iIndex = 0 To Session("StuMarks").Rows.Count - 1
                cmd = New SqlCommand("[RPT].[SaveREPORT_STUDENT_S]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                With Session("StuMarks")
                    If ViewState("datamode") = "add" Then
                        cmd.Parameters.AddWithValue("@RST_ID", 0)
                    End If
                    If ViewState("datamode") = "edit" Then
                        cmd.Parameters.AddWithValue("@RST_ID", .Rows(iIndex)("id"))
                    End If

                    cmd.Parameters.AddWithValue("@RST_RPF_ID", .Rows(iIndex)("RPfID"))
                    cmd.Parameters.AddWithValue("@RST_RSD_ID", .Rows(iIndex)("RSD_ID")) '-- Report Setup
                    cmd.Parameters.AddWithValue("@RST_ACD_ID", .Rows(iIndex)("AcdId"))
                    cmd.Parameters.AddWithValue("@RST_GRD_ID", .Rows(iIndex)("GrdId"))
                    cmd.Parameters.AddWithValue("@RST_STU_ID", .Rows(iIndex)("studId"))
                    cmd.Parameters.AddWithValue("@RST_RSS_ID", .Rows(iIndex)("RptSchId"))
                    cmd.Parameters.AddWithValue("@RST_SGR_ID", .Rows(iIndex)("GrdSbjId"))
                    cmd.Parameters.AddWithValue("@RST_SBG_ID", .Rows(iIndex)("SBJID"))
                    cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", .Rows(iIndex)("TypeLevel"))
                    cmd.Parameters.AddWithValue("@RST_MARK", .Rows(iIndex)("Mark"))
                    cmd.Parameters.AddWithValue("@RST_COMMENTS", .Rows(iIndex)("Comments"))
                    cmd.Parameters.AddWithValue("@RST_GRADING", .Rows(iIndex)("MGrade"))

                End With
                If ViewState("datamode") = "add" Then
                    cmd.Parameters.AddWithValue("@bEdit", 0)
                End If
                If ViewState("datamode") = "edit" Then
                    cmd.Parameters.AddWithValue("@bEdit", 1)
                End If
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            Next
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                Exit Sub
            End If
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateMarksentry()
        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            If ViewState("datamode") = "edit" Then
                cmd = New SqlCommand("[RPT].[UpdateREPORT_STUDENT_S]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@RST_ID", H_ID.Value)
                cmd.Parameters.AddWithValue("@RST_MARK", txtMarks.Text)
                cmd.Parameters.AddWithValue("@RST_COMMENTS", txtComments.Text)
                cmd.Parameters.AddWithValue("@RST_GRADING", txtGrdLevel.Text)

                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            End If

            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                Exit Sub
            End If
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ClearComments()
        txtComments.Text = ""
        txtCategory.Text = ""
        H_CMT_CAT_ID.Value = 0
        btnAdd.Text = "Add"
    End Sub

    Private Sub ClearEntireScreen()
        txtComments.Text = ""
        txtCategory.Text = ""
        H_CMT_CAT_ID.Value = 0
        btnAdd.Text = "Add"
    End Sub

    Private Sub DisplayGridRecords(ByVal RowIndx As Integer)
        Try
            If Session("StuMarks").rows.count >= 1 Then
                With Session("StuMarks").rows(RowIndx)
                    ddlAcdID.SelectedItem.Text = .Item("AcdYear")
                    ddlAcdID.SelectedValue = .Item("AcdId")
                    txtGrade.Text = .Item("Grade")
                    H_GRD_ID.Value = .Item("GrdId")
                    txtReportId.Text = .Item("Report")
                    H_RPT_ID.Value = .Item("RPfID")
                    txtGrdSubject.Text = .Item("GrdSbj")
                    H_SBJ_GRD_ID.Value = .Item("GrdSbjId")
                    txtGroup.Text = .Item("GroupId")
                    H_GRP_ID.Value = .Item("Group")
                    ddlLevel.SelectedValue = .Item("TypeLevel")
                    txtSubject.Text = .Item("Subject")
                    H_SBJ_ID.Value = .Item("SBJID")
                    txtRptSchedule.Text = .Item("rptSchedule")
                    H_SCH_ID.Value = .Item("RptSchId")
                    txtStuName.Text = .Item("StudName")
                    H_STU_ID.Value = .Item("studId")
                    txtMarks.Text = .Item("Mark")
                    txtGrdLevel.Text = .Item("MGrade")
                    txtComments.Text = .Item("Comments")

                    ViewState("datamode") = "edit"
                End With
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub DisplayRecordForEdit()

        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsMarks As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'strCriteria =  " AND RST_GRD_ID='" & ddlGrade.SelectedValue & "' "
        'strCriteria += " AND RST_SBG_ID=" & ddlSubject.SelectedValue & " "
        'strCriteria += " AND SGR_DESCR='" & ddlGroup.SelectedItem.Text & "' "
        'strCriteria += " AND RST_STU_ID=" & H_STU_ID.Value & " "
        Try

            strSql = "SELECT DISTINCT RST_ID,RSD.RSD_HEADER,RSM.RSM_DESCR,RST_STU_ID,RST_GRD_ID,RST_SBG_ID,RST_STU_ID,RST_ACD_ID,M.ACY_DESCR, " & _
                     "ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS STUNAME,STU_NO, " & _
                     "SBG_DESCR, SGR_DESCR, RST_TYPE_LEVEL, RST_COMMENTS, RST_GRADING,RST_MARK,RP.RPF_DESCR " & _
                     "FROM RPT.REPORT_STUDENT_S AS  A " & _
                     "INNER JOIN " & _
                     "SUBJECTS_GRADE_S AS B ON A.RST_SBG_ID=B.SBG_ID " & _
                     "INNER JOIN GROUPS_M AS C ON A.RST_SGR_ID=C.SGR_ID " & _
                     "INNER JOIN VW_ACADEMICYEAR_D D ON D.ACD_ID=RST_ACD_ID " & _
                     "INNER JOIN VW_ACADEMICYEAR_M M ON M.ACY_ID=D.ACD_ACY_ID " & _
                     "INNER JOIN VW_STUDENT_M STU ON STU.STU_ID=A.RST_STU_ID " & _
                     "INNER JOIN RPT.REPORT_SETUP_D RSD ON RSD.RSD_ID=A.RST_RSD_ID " & _
                     "INNER JOIN RPT.REPORT_SETUP_M RSM ON RSM.RSM_ID=RSD.RSD_RSM_ID " & _
                     "INNER JOIN RPT.REPORT_PRINTEDFOR_M RP ON RP.RPF_ID=A.RST_RPF_ID " & _
                     "WHERE SGR_ACD_ID=" & H_ACD_ID.Value & " AND RST_ID=" & H_RPT_ID.Value & ""

            ' "INNER JOIN RPT.REPORT_PRINTEDFOR_M RPF ON RPF.RPF_ID=A.RST_RPF_ID " & _

            dsMarks = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            With dsMarks.Tables(0)
                If .Rows.Count >= 1 Then
                    txtStuName.Text = .Rows(0).Item("STUNAME")
                    txtStuNo.Text = .Rows(0).Item("STU_NO")
                    txtMarks.Text = .Rows(0).Item("RST_MARK")
                    txtGrdLevel.Text = IIf(IsDBNull(.Rows(0).Item("RST_GRADING")), "", .Rows(0).Item("RST_GRADING"))
                    ddlLevel.SelectedValue = .Rows(0).Item("RST_TYPE_LEVEL")
                    txtComments.Text = IIf(IsDBNull(.Rows(0).Item("RST_COMMENTS")), "", .Rows(0).Item("RST_COMMENTS"))
                    ddlAcdID.SelectedItem.Text = .Rows(0).Item("ACY_DESCR")
                    ddlAcdID.SelectedValue = .Rows(0).Item("RST_ACD_ID")
                    txtGrade.Text = .Rows(0).Item("RST_GRD_ID")
                    txtGroup.Text = .Rows(0).Item("RST_GRD_ID")
                    H_GRD_ID.Value = .Rows(0).Item("RST_GRD_ID")
                    txtGrdSubject.Text = .Rows(0).Item("SBG_DESCR")
                    txtGrdSubject.Text = .Rows(0).Item("SGR_DESCR")
                    txtRptSchedule.Text = .Rows(0).Item("RPF_DESCR")
                    txtHeader.Text = .Rows(0).Item("RSD_HEADER")
                    txtSubject.Text = .Rows(0).Item("SBG_DESCR")
                    txtReportId.Text = .Rows(0).Item("RSM_DESCR")
                    btnDetAdd.Enabled = False
                    btnSubCancel.Enabled = False
                    H_ID.Value = .Rows(0).Item("RST_ID")
                    H_STU_ID.Value = .Rows(0).Item("RST_STU_ID")
                    btnSave.Text = "Update"
                    ddlAcdID.Enabled = False
                    imgReport.Enabled = False
                    imgGrade.Enabled = False
                    imgSbjGrade.Enabled = False
                    imgGroup.Enabled = False
                    ddlLevel.Enabled = False
                    imgSubject.Enabled = False
                    imgRptSchedule.Enabled = False
                    imgStuNo.Enabled = False
                    imgHeader.Enabled = False

                    ViewState("datamode") = "edit"
                End If
            End With

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DeleteGridRow(ByVal RowIndx As Integer)
        Try
            If Not Session("StuMarks") Is Nothing Then
                Session("StuMarks").rows(RowIndx).Delete()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateGridRow(ByVal RowIndx As Integer)
        Try
            If Not Session("StuMarks") Is Nothing Then
                With Session("StuMarks").rows(RowIndx)

                    .Item("AcdYear") = ddlAcdID.SelectedItem.Text
                    .Item("AcdId") = ddlAcdID.SelectedValue
                    .Item("Grade") = txtGrade.Text
                    .Item("GrdId") = H_GRD_ID.Value
                    .Item("Report") = txtReportId.Text
                    .Item("RPfID") = H_RPT_ID.Value
                    .Item("GrdSbj") = txtGrdSubject.Text
                    .Item("GrdSbjId") = H_SBJ_GRD_ID.Value
                    .Item("GroupId") = txtGroup.Text
                    .Item("Group") = H_GRP_ID.Value
                    .Item("TypeLevel") = ddlLevel.SelectedValue
                    .Item("Subject") = txtSubject.Text
                    .Item("SBJID") = H_SBJ_ID.Value
                    .Item("rptSchedule") = txtRptSchedule.Text
                    .Item("RptSchId") = H_SCH_ID.Value
                    .Item("StudName") = txtStuName.Text
                    .Item("studId") = H_STU_ID.Value
                    .Item("Mark") = txtMarks.Text
                    .Item("MGrade") = txtGrdLevel.Text
                    .Item("Comments") = txtComments.Text
                    Session("StuMarks").Update()
                End With
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function GetStudentMarks()
        Try

        Catch ex As Exception

        End Try
    End Function

#End Region

#Region "Pick Up Values"

    Protected Sub imgAcdId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            'If H_ACD_ID.Value <> "" AndAlso H_ACD_ID.Value.Contains("___") Then
            '    Dim ACD_ID As String = H_ACD_ID.Value.Split("___")(0)
            '    txtAccYear.Text = H_ACD_ID.Value.Split("___")(3)
            '    H_ACD_ID.Value = ACD_ID
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_GRD_ID.Value <> "" AndAlso H_GRD_ID.Value.Contains("___") Then
                Dim GRD_ID As String = H_GRD_ID.Value.Split("___")(0)
                txtGrade.Text = H_GRD_ID.Value.Split("___")(3)
                H_GRD_ID.Value = GRD_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgGroup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            txtGroup.Text = ""
            If H_GRP_ID.Value <> "" AndAlso H_GRP_ID.Value.Contains("___") Then
                Dim GRP_ID As String = H_GRP_ID.Value.Split("___")(0)

                If H_GRP_ID.Value.Split("___").Length > 3 Then
                    ' Split the String If multipple Uder score is found
                    Dim Strsplit As String()
                    Strsplit = H_GRP_ID.Value.Split("___")
                    For inti As Integer = 3 To Strsplit.Length - 2
                        txtGroup.Text += Strsplit(inti).ToString() + "_"
                    Next
                    txtGroup.Text += Strsplit(Strsplit.Length - 1).ToString()
                Else
                    txtGroup.Text = H_GRP_ID.Value.Split("___")(3)
                End If
                H_GRP_ID.Value = GRP_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SBJ_GRD_ID.Value <> "" AndAlso H_SBJ_GRD_ID.Value.Contains("___") Then
                Dim SBG_ID As String = H_SBJ_GRD_ID.Value.Split("___")(0)
                txtSubject.Text = H_SBJ_GRD_ID.Value.Split("___")(6)
                H_SBJ_GRD_ID.Value = SBG_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_RPT_ID.Value <> "" AndAlso H_RPT_ID.Value.Contains("___") Then
                Dim RPT_ID As String = H_RPT_ID.Value.Split("___")(0)
                txtReportId.Text = H_RPT_ID.Value.Split("___")(3)
                H_RPT_ID.Value = RPT_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgSbjGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SBJ_ID.Value <> "" AndAlso H_SBJ_ID.Value.Contains("___") Then
                Dim SBGRGD_ID As String = H_SBJ_ID.Value.Split("___")(0)
                txtGrdSubject.Text = H_SBJ_ID.Value.Split("___")(3)
                H_SBJ_ID.Value = SBGRGD_ID
                ReportFunctions.PopulateSubjectLevel(ddlLevel, SBGRGD_ID)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgRptSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SCH_ID.Value <> "" AndAlso H_SCH_ID.Value.Contains("___") Then
                Dim SCH_ID As String = H_SCH_ID.Value.Split("___")(0)
                txtRptSchedule.Text = H_SCH_ID.Value.Split("___")(3)
                H_SCH_ID.Value = SCH_ID
                'H_RPF_ID.Value = H_SCH_ID.Value.Split("___")(3) 'ReportFunctions.GetReportPrintedFor(H_SCH_ID.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_STU_ID.Value <> "" AndAlso H_STU_ID.Value.Contains("___") Then
                Dim STU_ID As String = H_STU_ID.Value.Split("___")(0)
                txtStuName.Text = H_STU_ID.Value.Split("___")(3)
                txtStuNo.Text = H_STU_ID.Value.Split("___")(6)
                H_STU_ID.Value = STU_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgCategory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_CMT_CAT_ID.Value <> "" AndAlso H_CMT_CAT_ID.Value.Contains("___") Then
                Dim CAT_ID As String = H_CMT_CAT_ID.Value.Split("___")(0)
                txtCategory.Text = H_CMT_CAT_ID.Value.Split("___")(3)
                H_CMT_CAT_ID.Value = CAT_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgComments_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_CMT_ID.Value <> "" AndAlso H_CMT_ID.Value.Contains("___") Then
                Dim CMT_ID As String = H_CMT_ID.Value.Split("___")(0)
                txtComments.Text = H_CMT_ID.Value.Split("___")(3)
                H_CMT_ID.Value = CMT_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgHeader_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SETUP.Value <> "" AndAlso H_SETUP.Value.Contains("___") Then
                Dim _ID As String = H_SETUP.Value.Split("___")(0)
                txtHeader.Text = H_SETUP.Value.Split("___")(6)
                H_RPF_ID.Value = ReportFunctions.GetReportPrintedFor(H_SETUP.Value.Split("___")(3))
                H_SETUP.Value = _ID

            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region " Button event Handlings"

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'To Edit the Grid Row While Add Mode
            If ViewState("datamode") = "add" Then
                AddRecord()
                BindGrid()
                ClearComments()
                'To edit the Grid row While Editing
            ElseIf ViewState("datamode") = "edit" Then
                UpdateGridRow(Session("iEdit"))
                BindGrid()
                ClearComments()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuMarks") Is Nothing Then
                    SaveRecord()
                End If
            End If
            'Dim url As String
            'ViewState("datamode") = "add"
            'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'url = String.Format("~\Curriculum\rptStudentMarks_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'Response.Redirect(url)

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Sub gvMarks_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvMarks.RowDeleting
        Try
            gvMarks.SelectedIndex = e.RowIndex
            Dim row As GridViewRow = gvMarks.Rows(e.RowIndex)

            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblId"), Label)
            If lblID.Text <> "" And gvMarks.SelectedIndex >= 0 Then
                DeleteGridRow(e.RowIndex)
            End If
            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvMarks_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvMarks.RowEditing
        Try
            gvMarks.SelectedIndex = e.NewEditIndex
            Dim row As GridViewRow = gvMarks.Rows(e.NewEditIndex)

            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblId"), Label)

            If lblID.Text <> "" And gvMarks.SelectedIndex >= 0 Then
                DisplayGridRecords(gvMarks.SelectedIndex)
                Session("iEdit") = gvMarks.SelectedIndex
            End If
            btnSave.Visible = True
            btnAdd.Text = "Update"

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            'ViewState("datamode") = "add"
            'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvMarks.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentsMarksEdit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentMarks_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If txtStuName.Text <> "" And txtStuNo.Text <> "" Then
            If H_ID.Value <> 0 Then

                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    strSql = "DELETE FROM RPT.REPORT_STUDENT_S WHERE RST_ID='" & H_ID.Value & "' AND " _
                                & "RST_ACD_ID='" & ddlAcdID.SelectedValue & "' AND RST_STU_ID='" & H_STU_ID.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSql)
                    stTrans.Commit()
                    lblError.Text = "Successfully Deleted"
                    ClearComments()
                Catch ex As Exception
                    stTrans.Rollback()
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed"
                End Try
            End If
        End If
    End Sub

End Class
