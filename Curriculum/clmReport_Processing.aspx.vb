Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Partial Class Students_studAtt_Registration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_PROCESSING_REPORT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call GetEmpID()

                    callYEAR_DESCRBind()

                    btnCancel.Visible = False
                    'Session("dt_ATT") = CreateDataTable()
                    'Session("dt_ATT").Rows.Clear()
                    'ddlSGR_ID.Attributes.Add("onmouseover", "javascript:showToolTip();return false;")



                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    
   
    Sub DisableControls()
        For j As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(j)
            Dim G_Stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
            If Session("Astud_ID").Contains(G_Stud_ID) Then

                DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False

                DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
            End If
        Next
    End Sub
 
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'If ddlAcdYear.SelectedIndex = -1 Then
        '    lblError.Text = "Academic Year not selected"

        'ElseIf ddlGRD_ID.SelectedIndex = -1 Then
        '    lblError.Text = "Grade not selected"
        'ElseIf ddlSection.SelectedIndex = -1 Then
        '    lblError.Text = "Section not selected"
        'ElseIf ddlStream.SelectedIndex = -1 Then
        '    lblError.Text = "Stream not selected"
        'ElseIf ddlShift.SelectedIndex = -1 Then
        '    lblError.Text = "Shift not selected"

        'Else

        '    If ViewState("ATT_OPEN") = True Then
        '        If ValidateDate() = "0" Then
        '            If AuthorizedUser_State() > 0 Then
        '                Call Edit_clicked()
        '                Call backGround()
        '            Else
        '                lblError.Text = "Not authorized for attendance for the selected Grade or Date "
        '            End If
        '        End If
        '    Else
        '        If AuthorizedUser_State() > 0 Then
        '            Call Edit_clicked()
        '            Call backGround()
        '        Else
        '            lblError.Text = "Not authorized for attendance for the selected Grade or Date "
        '        End If
        '    End If
        'End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = calltransaction(errorMessage)
        If str_err = "0" Then

            lblError.Text = "Record Saved Successfully"

        Else
            lblError.Text = errorMessage

        End If

    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim status As Integer
        Dim ACD_ID As String = ddlAcd_Year.SelectedValue
        Dim GRD_ID As String = String.Empty
        Dim TRM_ID As String = String.Empty
        Dim SBG_ID As String = String.Empty
        Dim SGR_ID As String = String.Empty
        Dim RSM_ID As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim RSD_ID As String = String.Empty
        Dim RPT_XML As String = getRPT_XML()
        If ddlGRD_ID.SelectedIndex <> -1 Then
            GRD_ID = ddlGRD_ID.SelectedValue
        End If
        If ddlTrm_id.SelectedIndex <> -1 Then
            If ddlTrm_id.SelectedValue = 0 Then
                TRM_ID = Nothing
            Else
                TRM_ID = ddlTrm_id.SelectedValue
            End If


        End If

        If ddlSBG_ID.SelectedIndex <> -1 Then
            SBG_ID = ddlSBG_ID.SelectedValue
        End If
        If ddlSGR_ID.SelectedIndex <> -1 Then
            SGR_ID = ddlSGR_ID.SelectedValue
        End If
        If ddlRSM_ID.SelectedIndex <> -1 Then
            RSM_ID = ddlRSM_ID.SelectedValue
        End If
        If ddlRPF_ID.SelectedIndex <> -1 Then
            RPF_ID = ddlRPF_ID.SelectedValue
        End If
        If ddlRSD_ID.SelectedIndex <> -1 Then
            RSD_ID = ddlRSD_ID.SelectedValue
        End If

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                bEdit = False
                status = ACTIVITYMASTER.SaveREPORT_STUDENT_S(RPT_XML, RPF_ID, ACD_ID, GRD_ID, SGR_ID, SBG_ID, RSD_ID, transaction)

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error in inserting new record", "calltransaction")
                    Return "1"
                End If
                ViewState("viewid") = "0"
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "calltransaction")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
    Function getRPT_XML()
        Dim str As String = String.Empty

        Dim STU_ID As String = String.Empty
        Dim MARK As String = String.Empty
        Dim GRADE As String = String.Empty
        Dim TYPE_LEVEL As String = String.Empty
        Dim RRM_ID As String = String.Empty


        Dim iCount As Integer = 0



        For Each rowItem As GridViewRow In gvInfo.Rows

            STU_ID = gvInfo.DataKeys(rowItem.RowIndex)("STU_ID").ToString()
            MARK = DirectCast(rowItem.FindControl("txtMARK"), TextBox).Text
            GRADE = DirectCast(rowItem.FindControl("txtGRADE"), TextBox).Text
            TYPE_LEVEL = DirectCast(rowItem.FindControl("lblType_Level"), Label).Text
            RRM_ID = DirectCast(rowItem.FindControl("lblRRM_ID"), Label).Text

            str += String.Format("<STUDENT STU_ID='{0}'><STU_DETAIL MARK='{1}' GRADE='{2}' TYPE_LEVEL='{3}' RRM_ID='{4}' /></STUDENT>", STU_ID, MARK, GRADE, TYPE_LEVEL, RRM_ID)

        Next

        If str <> "" Then
            str = "<STUDENTS>" + str + "</STUDENTS>"
        End If

        Return str
    End Function



   
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        If ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    'Sub setControl()
    '    ddlAcdYear.Enabled = False
    '    ddlStream.Enabled = False
    '    ddlShift.Enabled = False
    '    ddlGRD_ID.Enabled = False
    '    ddlSection.Enabled = False


    '    ddlAttType.Enabled = False
    'End Sub
    'Sub ResetControl()
    '    ddlAcdYear.Enabled = True
    '    ddlStream.Enabled = True
    '    ddlShift.Enabled = True
    '    ddlGRD_ID.Enabled = True
    '    ddlSection.Enabled = True
    '    If ViewState("ATT_OPEN") = True Then
    '        txtDate.Enabled = True
    '        imgCalendar.Enabled = True
    '        rfvDate.Enabled = True
    '    Else
    '        ddlAttDate.Enabled = True
    '    End If

    '    ddlAttType.Enabled = True
    'End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
        smScriptManager.Services.Add(New ServiceReference("~/WebService/ProcessRule.asmx"))
        smScriptManager.Scripts.Add(New ScriptReference("WebServiceMethodError.js"))

    End Sub
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
" FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
" WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcd_Year.Items.Clear()
            ddlAcd_Year.DataSource = ds.Tables(0)
            ddlAcd_Year.DataTextField = "ACY_DESCR"
            ddlAcd_Year.DataValueField = "ACD_ID"
            ddlAcd_Year.DataBind()
            If Not ddlAcd_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAcd_Year.ClearSelection()
                ddlAcd_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            ddlAcd_Year_SelectedIndexChanged(ddlAcd_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLTERM_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            str_Sql = " SELECT    TRM_ID, TRM_DESCRIPTION FROM  VW_TRM_M WHERE TRM_ACD_ID= '" & ACD_ID & "' ORDER BY TRM_DESCRIPTION "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlTrm_id.Items.Clear()
            ddlTrm_id.DataSource = ds.Tables(0)
            ddlTrm_id.DataTextField = "TRM_DESCRIPTION"
            ddlTrm_id.DataValueField = "TRM_ID"
            ddlTrm_id.DataBind()
            ddlTrm_id.Items.Add(New ListItem("FINAL TERM", "0"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_CardBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            str_Sql = " SELECT     RSM_ID,RSM_DESCR FROM  RPT.REPORT_SETUP_M WHERE RSM_ACD_ID='" & ACD_ID & "' ORDER BY RSM_DISPLAYORDER"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRSM_ID.Items.Clear()
            ddlRSM_ID.DataSource = ds.Tables(0)
            ddlRSM_ID.DataTextField = "RSM_DESCR"
            ddlRSM_ID.DataValueField = "RSM_ID"
            ddlRSM_ID.DataBind()
            ddlRSM_ID_SelectedIndexChanged(ddlRSM_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_PrintForBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim RSM_ID As String = ddlRSM_ID.SelectedItem.Value
            str_Sql = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" & RSM_ID & "' ORDER BY RPF_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRPF_ID.Items.Clear()
            ddlRPF_ID.DataSource = ds.Tables(0)
            ddlRPF_ID.DataTextField = "RPF_DESCR"
            ddlRPF_ID.DataValueField = "RPF_ID"
            ddlRPF_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_HeaderBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim RSM_ID As String = ddlRSM_ID.SelectedItem.Value
            str_Sql = "SELECT    RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D where RSD_RSM_ID='" & RSM_ID & "'  and RSD_bDIRECTENTRY=0 ORDER BY RSD_DISPLAYORDER"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRSD_ID.Items.Clear()
            ddlRSD_ID.DataSource = ds.Tables(0)
            ddlRSD_ID.DataTextField = "RSD_HEADER"
            ddlRSD_ID.DataValueField = "RSD_ID"
            ddlRSD_ID.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            Dim TRM_ID As String = String.Empty
            If ddlTrm_id.SelectedIndex <> -1 Then
                If ddlTrm_id.SelectedValue = 0 Then
                    TRM_ID = "  RPT.REPORT_RULE_M.RRM_TRM_ID IS NULL "
                Else
                    TRM_ID = "  RPT.REPORT_RULE_M.RRM_TRM_ID = '" & ddlTrm_id.SelectedValue & "'"
                End If


            End If
            Dim RSM_ID As String = String.Empty
            If ddlRSM_ID.SelectedIndex <> -1 Then
                RSM_ID = ddlRSM_ID.SelectedValue
            End If

            Dim RPF_ID As String = String.Empty
            If ddlRPF_ID.SelectedIndex <> -1 Then
                RPF_ID = ddlRPF_ID.SelectedValue
            End If
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, VW_GRADE_M.GRD_DISPLAYORDER " & _
" FROM   VW_GRADE_BSU_M INNER JOIN    VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
" RPT.REPORT_RULE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORT_RULE_M.RRM_GRD_ID AND " & _
" VW_GRADE_BSU_M.GRM_ACD_ID = RPT.REPORT_RULE_M.RRM_ACD_ID WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') " & _
" and RPT.REPORT_RULE_M.RRM_RSM_ID = '" & RSM_ID & "' and  " & TRM_ID & " and RPT.REPORT_RULE_M.RRM_RPF_ID  = '" & RPF_ID & "'" & _
" ORDER BY VW_GRADE_M.GRD_DISPLAYORDER "



            '" SELECT   DISTINCT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY AS  GRM_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " & _
            '            " FROM  VW_GRADE_BSU_M INNER JOIN  VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " & _
            '            " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' ORDER BY VW_GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGRD_ID.Items.Clear()
            ddlGRD_ID.DataSource = ds.Tables(0)
            ddlGRD_ID.DataTextField = "GRM_DISPLAY"
            ddlGRD_ID.DataValueField = "GRD_ID"
            ddlGRD_ID.DataBind()
            ddlGRD_ID_SelectedIndexChanged(ddlGRD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Stream()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            Dim GRD_ID As String = ddlGRD_ID.SelectedValue
            Dim ds As New DataSet
            str_Sql = " SELECT  DISTINCT VW_STREAM_M.STM_ID, VW_STREAM_M.STM_DESCR FROM  VW_STREAM_M INNER JOIN " & _
" VW_GRADE_BSU_M ON VW_STREAM_M.STM_ID = VW_GRADE_BSU_M.GRM_STM_ID " & _
" where VW_GRADE_BSU_M.GRM_GRD_ID='" & GRD_ID & "' and VW_GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "'  order by VW_STREAM_M.STM_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlStream.Items.Clear()
            ddlStream.DataSource = ds.Tables(0)
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            If Not ddlStream.Items.FindByValue("1") Is Nothing Then
                ddlStream.Items.FindByValue("1").Selected = True
            End If

            ddlStream_SelectedIndexChanged(ddlStream, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindGrade_Subject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            Dim GRD_ID As String = ddlGRD_ID.SelectedValue
            Dim STM_ID As String = ddlStream.SelectedValue
            Dim ds As New DataSet
            str_Sql = "SELECT      SBG_ID, SBG_DESCR + case when isnull(SBG_Parents_SHORT,'NA') ='NA' then '' else '_'+SBG_Parents_SHORT end  as SBG_DESCR FROM  SUBJECTS_GRADE_S WHERE SBG_ACD_ID = '" & ACD_ID & "' AND SBG_GRD_ID = '" & GRD_ID & "' AND SBG_STM_ID = '" & STM_ID & "' order by  SBG_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSBG_ID.Items.Clear()
            ddlSBG_ID.DataSource = ds.Tables(0)
            ddlSBG_ID.DataTextField = "SBG_DESCR"
            ddlSBG_ID.DataValueField = "SBG_ID"
            ddlSBG_ID.DataBind()
            ddlSBG_ID_SelectedIndexChanged(ddlSBG_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindSUBJECT_GROUP()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = String.Empty
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            Dim GRD_ID As String = ddlGRD_ID.SelectedValue
            Dim SBG_ID As String = ddlSBG_ID.SelectedValue

            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim ds As New DataSet
            If Not Session("CurrSuperUser") Is Nothing Then

                If Session("CurrSuperUser") = "Y" Then
                    '    str_Sql = "SELECT  SGR_ID ,SGR_DESCR FROM GROUPS_M WHERE (SGR_ACD_ID = '" & ACD_ID & "') AND (SGR_SBG_ID='" & SBG_ID & "') AND   (SGR_GRD_ID = '" & GRD_ID & "') AND " & _
                    ' " SGR_ID IN(SELECT  SGS_SGR_ID FROM GROUPS_TEACHER_S where SGS_TODATE IS NULL ) ORDER BY SGR_DESCR"
                    'code modified by dhanya
                    str_Sql = "SELECT  SGR_ID ,SGR_DESCR FROM GROUPS_M WHERE (SGR_ACD_ID = '" & ACD_ID & "') AND (SGR_SBG_ID='" & SBG_ID & "') AND   (SGR_GRD_ID = '" & GRD_ID & "')  " & _
                 "  ORDER BY SGR_DESCR"
                Else
                    str_Sql = "SELECT  SGR_ID ,SGR_DESCR FROM GROUPS_M WHERE (SGR_ACD_ID = '" & ACD_ID & "') AND (SGR_SBG_ID='" & SBG_ID & "') AND   (SGR_GRD_ID = '" & GRD_ID & "') AND " & _
                 " SGR_ID IN(SELECT  SGS_SGR_ID FROM GROUPS_TEACHER_S where SGS_TODATE IS NULL AND SGS_EMP_ID='" & EMP_ID & "') ORDER BY SGR_DESCR"
                End If

            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSGR_ID.Items.Clear()
            ddlSGR_ID.DataSource = ds.Tables(0)
            ddlSGR_ID.DataTextField = "SGR_DESCR"
            ddlSGR_ID.DataValueField = "SGR_ID"
            ddlSGR_ID.DataBind()

            If ddlSBG_ID.SelectedIndex <> -1 Then
                lbtnDetail.Visible = True
                ProcessRule()
            Else
                lbtnDetail.Visible = False
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcd_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        CALLTERM_DESCRBind()
        CALLReport_CardBind()
        bindAcademic_Grade()
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False

    End Sub

    Protected Sub ddlTrm_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLReport_CardBind()
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
    End Sub

    Protected Sub ddlRSM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLReport_PrintForBind()
        CALLReport_HeaderBind()
        bindAcademic_Grade()
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False

    End Sub

    Protected Sub ddlGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindAcademic_Stream()
        bindGrade_Subject()
        bindSUBJECT_GROUP()
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False

    End Sub

    Protected Sub ddlSBG_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindSUBJECT_GROUP()
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False

    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvInfo.Visible = True

        Dim ACD_ID As String = ddlAcd_Year.SelectedValue
        Dim GRD_ID As String = String.Empty
        Dim TRM_ID As String = String.Empty
        Dim SBG_ID As String = String.Empty
        Dim SGR_ID As String = String.Empty
        Dim RSM_ID As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim RSD_ID As String = String.Empty

        If ddlGRD_ID.SelectedIndex <> -1 Then
            GRD_ID = ddlGRD_ID.SelectedValue
        End If

        If ddlTrm_id.SelectedIndex <> -1 Then
            If ddlTrm_id.SelectedValue = 0 Then
                TRM_ID = Nothing
            Else
                TRM_ID = ddlTrm_id.SelectedValue
            End If


        End If

        If ddlTrm_id.SelectedIndex <> -1 Then
            TRM_ID = ddlTrm_id.SelectedValue
        End If

        If ddlSBG_ID.SelectedIndex <> -1 Then
            SBG_ID = ddlSBG_ID.SelectedValue
        End If
        If ddlSGR_ID.SelectedIndex <> -1 Then
            SGR_ID = ddlSGR_ID.SelectedValue
        End If
        If ddlRSM_ID.SelectedIndex <> -1 Then
            RSM_ID = ddlRSM_ID.SelectedValue
        End If
        If ddlRPF_ID.SelectedIndex <> -1 Then
            RPF_ID = ddlRPF_ID.SelectedValue
        End If
        If ddlRSD_ID.SelectedIndex <> -1 Then
            RSD_ID = ddlRSD_ID.SelectedValue
        End If
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_App As String = String.Empty




            Dim ds As New DataSet
            '('5','40','KG1','1','4','131515','56','1') 
            str_Sql = "SELECT ROW_NUMBER() OVER(ORDER BY SNAME) AS 'SRNO',STU_ID,STU_NO,SNAME,MARK,TYPE_LEVEL,RRM_ID,GRADE,ACD_ID ,SGR_ID ,SBG_ID " & _
" FROM [RPT].[fn_REPORT_STUDENT_S]('" & SBG_ID & "','" & ACD_ID & "','" & GRD_ID & "','" & RPF_ID & "','" & RSD_ID & "'," & TRM_ID & ",'" & SGR_ID & "','" & RSM_ID & "') ORDER BY SNAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            If ds.Tables(0).Rows.Count > 0 Then

                gvInfo.DataSource = ds.Tables(0)
                gvInfo.DataBind()
                btnSave.Visible = True
                btnCancel.Visible = True

            Else
                btnSave.Visible = False
                btnCancel.Visible = False
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())


                gvInfo.DataSource = ds.Tables(0)
                Try
                    gvInfo.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvInfo.Rows(0).Cells.Clear()
                gvInfo.Rows(0).Cells.Add(New TableCell)
                gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlRSD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlGRD_ID_SelectedIndexChanged(ddlGRD_ID, Nothing)
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
        If ddlSBG_ID.SelectedIndex <> -1 Then
            lbtnDetail.Visible = True
            ProcessRule()
        Else
            lbtnDetail.Visible = False
        End If
    End Sub
    '<WebMethod()> _
    '    Public Function GetContent(ByVal contextKey As String) As String
    '    ' Create a random color 
    '    Dim color As String = (New Random()).[Next](&HFFFFFF).ToString("x6")
    '    ' Use the style specified by the page author 
    '    Dim style As String = contextKey
    '    ' Display the current time 
    '    Dim time As String = DateTime.Now.ToLongTimeString()
    '    ' Compose the content to return 
    '    Return (("<span style='color:#" & color & "; ") + style & "'>") + time & "</span> "
    'End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
 Public Shared Function GetContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "|"
        Dim vSTA_STU_ACD_SGR_SBG As String = contextKey
        Dim drReader As SqlDataReader
        Dim ACD_ID As String = String.Empty
        Dim SBG_ID As String = String.Empty
        Dim SGR_ID As String = String.Empty
        Dim STU_ID As String = String.Empty


        arInfo = vSTA_STU_ACD_SGR_SBG.Split(splitter)
        If arInfo.Length > 2 Then
            STU_ID = arInfo(0)
            ACD_ID = arInfo(1)
            SGR_ID = arInfo(2)
            SBG_ID = arInfo(3)
        End If



        Try
            Dim str_Sql As String = "SELECT  S.CAS_DESC ,ISNULL(A.STA_MARK,0) AS STA_MARK, A.STA_MAX_MARK, ISNULL(A.STA_GRADE,' ') AS STA_GRADE," & _
 " A.STA_REMARKS, A.STA_ATT_DATE, S.CAS_DATE  FROM ACT.STUDENT_ACTIVITY as A " & _
" INNER JOIN ACT.ACTIVITY_SCHEDULE AS S  ON A.STA_CAS_ID = S.CAS_ID INNER JOIN " & _
" ACT.ACTIVITY_D AS D ON S.CAS_CAD_ID = D.CAD_ID where A.STA_STU_ID='" & STU_ID & "'" & _
 " and A.STA_ACD_ID='" & ACD_ID & "' AND A.STA_SBG_ID='" & SBG_ID & "' AND A.STA_SGR_ID='" & SGR_ID & "' " & _
" AND A.STA_bCOUNTED=1 ORDER BY S.CAS_DATE DESC"



            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='table table-bordered table-row panel-cover' >")
            sTemp.Append("<tr style='background-color:#ffffff;'>")
            sTemp.Append("<td colspan=5 align='center'><b>Activity Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr style='background-color:#ffffff;'>")
            sTemp.Append("<td><b>ACTIVITY</b></td>")
            sTemp.Append("<td><b>MARK OBTAINED</b></td>")

            '***CODE ADDED BY DHANYA 
            sTemp.Append("<td><b>GRADE</b></td>")
            '*****


            sTemp.Append("<td><b>MAX. MARK</b></td>")
            sTemp.Append("<td><b>ACTIVITY DATE</b></td>")
            sTemp.Append("</tr>")
            Dim CAS_DATE As String = String.Empty
            While (drReader.Read())
                CAS_DATE = ""
                sTemp.Append("<tr style='background-color:#ffffff;'>")
                sTemp.Append("<td>" & drReader("CAS_DESC").ToString & "</td>")
                sTemp.Append("<td>" & drReader("STA_MARK").ToString & "</td>")

                '***CODE ADDED BY DHANYA
                sTemp.Append("<td>" & drReader("STA_GRADE").ToString & "</td>")
                '******


                sTemp.Append("<td>" & drReader("STA_MAX_MARK").ToString & "</td>")
                If IsDate(drReader("CAS_DATE")) = True Then
                    CAS_DATE = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((drReader("CAS_DATE"))))
                End If
                sTemp.Append("<td>" & CAS_DATE & "</td>")
                sTemp.Append("</tr>")
            End While

        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function
   
    Protected Sub ddlSGR_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False


        If ddlSBG_ID.SelectedIndex <> -1 Then
            lbtnDetail.Visible = True
            ProcessRule()
        Else
            lbtnDetail.Visible = False
        End If

    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
        bindGrade_Subject()

        If ddlSBG_ID.SelectedIndex <> -1 Then
            lbtnDetail.Visible = True
            ProcessRule()
        Else
            lbtnDetail.Visible = False
        End If
    End Sub
    Sub ProcessRule()
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim SBG_ID As String = String.Empty
        Dim ACD_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim RSD_ID As String = String.Empty
        Dim TRM_ID As String = String.Empty
        If ddlAcd_Year.SelectedIndex <> -1 Then
            ACD_ID = ddlAcd_Year.SelectedValue
        Else
            ACD_ID = ""
        End If
        If ddlSBG_ID.SelectedIndex <> -1 Then
            SBG_ID = ddlSBG_ID.SelectedValue
        Else
            SBG_ID = ""
        End If
        If ddlGRD_ID.SelectedIndex <> -1 Then
            GRD_ID = ddlGRD_ID.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddlRPF_ID.SelectedIndex <> -1 Then
            RPF_ID = ddlRPF_ID.SelectedValue
        Else
            RPF_ID = ""
        End If
        If ddlRSD_ID.SelectedIndex <> -1 Then
            RSD_ID = ddlRSD_ID.SelectedValue
        Else
            RSD_ID = ""
        End If
        If ddlTrm_id.SelectedIndex <> -1 Then
            TRM_ID = ddlTrm_id.SelectedValue
        Else
            TRM_ID = ""
        End If

        Dim sqlstr As String = ""

        Try

            sqlstr = " SELECT CAM_ID,GRADE_SLAB,RULES,OPT,WEIGHT FROM (SELECT RSS_CAM_ID AS CAM_ID," & _
 " GRADE_SLAB=(SELECT GSM_DESC FROM ACT.GRADING_SLAB_M WHERE GSM_SLAB_ID=(SELECT RRM_GSM_SLAB_ID FROM RPT.REPORT_RULE_M WHERE RRM_ID=A.RSS_RRM_ID ))," & _
                     " RULES=RSS_OPRT_DISPLAY+'('+(select STUFF((SELECT ',' + CASE A.RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN " & _
                     " convert(varchar(100),RRD_WEIGHTAGE)+'% of '+convert(varchar(100),CAD_DESC) ELSE convert(varchar(100),CAD_DESC) END " & _
                     " from RPT.REPORT_SCHEDULE_D AS P INNER JOIN ACT.ACTIVITY_D AS Q ON P.RRD_CAD_ID=Q.CAD_ID " & _
                     " where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))+')', " & _
                     " OPT=CASE RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN 'WT' ELSE RSS_OPRT_DISPLAY END, " & _
" WEIGHT=RSS_WEIGHTAGE   FROM RPT.REPORT_SCHEDULE_M AS A WHERE RSS_RRM_ID= (SELECT     RRM_ID FROM  RPT.REPORT_RULE_M " & _
 " where RRM_SBG_ID='" & SBG_ID & "' and RRM_ACD_ID='" & ACD_ID & "' and RRM_GRD_ID='" & GRD_ID & "' and  RRM_RPF_ID='" & RPF_ID & "' and RRM_RSD_ID='" & RSD_ID & "' AND " & _
 " RRM_TRM_ID='" & TRM_ID & "'))A  ORDER BY CAM_ID"

            dsProcessRule = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstr)

            Dim i As Integer
            Dim OutStr As String = String.Empty
            Dim SumOf As String = String.Empty
            If dsProcessRule.Tables(0).Rows.Count > 0 Then

                For I = 0 To dsProcessRule.Tables(0).Rows.Count - 1

                    OutStr = OutStr & "<div style='text-align:left; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>Rule : " & i + 1 & "<div><font color='#1b80b6'>Operation : </font>" & dsProcessRule.Tables(0).Rows(i).Item("RULES") & "</div> <div><font color='#1b80b6'>Grade Slab :</font> " & dsProcessRule.Tables(0).Rows(i).Item("GRADE_SLAB") & " </div><div><font color='#1b80b6'> Weight : </font> " & dsProcessRule.Tables(0).Rows(i).Item("WEIGHT") & " </DIV></div>"
                    SumOf = SumOf + " (Rule : " & i + 1 & ") +"

                Next
                'If Len(SumOf) > 0 Then
                '    SumOf = Mid(SumOf, 1, Len(SumOf) - 1)
                'End If
                ltProcessRule.Text = "<DIV style='text-align:left; font-size: 10px; color: #800000;font-weight: bold;'>Rule Applied For " & ddlSGR_ID.SelectedItem.Text & "</DIV>" & OutStr & "<DIV  style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #800000;'>Final result is the sum of " & SumOf.TrimEnd("+") & "</Div>"

            Else
                ltProcessRule.Text = "<div style='text-align:left;font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>No Rule Available !!!</div>"
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub

   

    
    
    Protected Sub ddlRPF_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvInfo.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
        bindAcademic_Grade()
        If ddlSBG_ID.SelectedIndex <> -1 Then
            lbtnDetail.Visible = True
            ProcessRule()
        Else
            lbtnDetail.Visible = False
        End If
    End Sub

    Protected Sub gvInfo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfo.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)
            ' Set the BehaviorID 
            Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
            pce.BehaviorID = behaviorID
            ' Programmatically reference the Image control 
            Dim i As LinkButton = DirectCast(e.Row.Cells(1).FindControl("lbtnSNAME"), LinkButton)
            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)
            'i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onmouseout", OnMouseOutScript)
        End If
    End Sub

End Class
