﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCASEQuestions_M.aspx.vb" Inherits="Curriculum_clmCASEQuestions_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space ">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="QUESTIONS MASTER"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%"  >
                    <tr>
                        <td     >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left" valign="top" colspan="4">
                                        <asp:LinkButton ID="lnkAddNew" runat="server"  >Add New</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Academic Year</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Grade</span></td>
                                    <td  align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Subject</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Description</span></td>

                                    <td  align="left">
                                        <asp:TextBox ID="txtDescription" runat="server"  ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Total Marks</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtTotalMarks" runat="server"  ></asp:TextBox>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Total Questions</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTotalQuestions" runat="server"  ></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr id="trGrid1" runat="server">
                                    <td colspan="4" align="left">&nbsp;</td>
                                </tr>
                                <tr id="trGrid2" runat="server">
                                    <td colspan="4" style="margin-left: 80px" align="center">
                                        <asp:GridView ID="gvQuestions" runat="server" AllowPaging="false" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords." PageSize="20"  >
                                            <RowStyle CssClass="griditem"   Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQsdId" runat="server" Text='<%# Bind("QSD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSklId" runat="server" Text='<%# Bind("QSD_SKL_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("QSD_SKS_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbuId" runat="server" Text='<%# Bind("QSD_SBU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Question"  HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQuestion" runat="server"   Text='<%# Bind("QSD_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Max Mark"  HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMax" runat="server"   Text='<%# Bind("QSD_MAXMARK") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit" HeaderStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlUnit" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="50%" >
                                                    <ItemTemplate >
                                                                    <asp:DropDownList ID="ddlSkill" OnSelectedIndexChanged="ddlSkill_SelectedIndexChanged" runat="server" AutoPostBack="true" >
                                                                    </asp:DropDownList><br />
                                                                    <asp:DropDownList ID="ddlSubSkills" runat="server" Visible="false"></asp:DropDownList>
                                                     
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false"  >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete" Visible="false"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle   CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trGrid3" runat="server">
                                    <td colspan="7" align="center">
                                        <asp:HiddenField ID="hfQSM_ID" runat="server" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

