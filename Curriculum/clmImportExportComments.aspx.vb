﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Data.OleDb


Partial Class Curriculum_clmImportExportComments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                 Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330355") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then
                        H_CMT_ID.Value = 0
                    Else

                    End If
                    Try
                        ddlReport = PopulateReports(ddlReport)
                        If ddlReport.SelectedValue <> "" Then
                            FillGrade()
                            FillSubjects()
                            gridbind()
                            H_CAT_ID.Value = 1
                            GetReportHeader(chkHeader)
                        End If
                        PopulateGrades(ddlGrade0)
                        chkCatByGrade.Visible = False
                        ddlGrade0.Visible = False
                        BindCategory()

                        If radCatBySubject.Checked = True Then
                            trcatGrade.Visible = False
                            chkGrade.Checked = True
                        Else
                            trcatGrade.Visible = True
                            chkGrade.Checked = False
                        End If
                        'disable the control buttons based on the rights

                        gvComments.Attributes.Add("bordercolor", "#1b80b6")

                    Catch ex As Exception
                    End Try


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYearExport = studClass.PopulateAcademicYear(ddlAcademicYearExport, Session("clm"), Session("sbsuid"))
                    ddlAcademicYearUpload = studClass.PopulateAcademicYear(ddlAcademicYearUpload, Session("clm"), Session("sbsuid"))
                    BindReportCard(ddlReportCardExport, ddlAcademicYearExport.SelectedValue.ToString)
                    BindReportCard(ddlReportcardUpload, ddlAcademicYearUpload.SelectedValue.ToString)
                    BindHeader(ddlHeaderExport, ddlReportCardExport.SelectedValue.ToString, 1)
                    BindHeader(ddlHeaderUpload, ddlReportcardUpload.SelectedValue.ToString, 0)
                    PopulateGrades(ddlGradeExport, ddlReportCardExport.SelectedValue.ToString)
                    PopulateGrades(ddlGradeUpload, ddlReportcardUpload.SelectedValue.ToString)
                    PopulateSubjects(ddlSubjectExport, ddlAcademicYearExport.SelectedValue.ToString, ddlGradeExport.SelectedValue.ToString)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '  lblError.Text = "Request could not be processed"
            End Try
            
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub PopulateGrades(ByVal ddlGrade As DropDownList, ByVal rsm_id As String)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_ID  , GRD_DISPLAY  " _
                       & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                       & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                       & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & rsm_id & "' " _
                       & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String, ByVal grd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                & " WHERE SBG_ACD_ID='" + acd_id + "'"



        str_query += " AND SBG_GRD_ID='" + grd_id + "'"

        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)
    End Sub


    Sub BindReportCard(ByVal ddlReportCard As DropDownList, ByVal acd_id As String)


        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & acd_id & " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

  

    Sub BindHeader(ByVal ddlReportHeader As DropDownList, ByVal rsm_id As String, ByVal k As Integer)
        Dim strStubjects As String = ""

        Dim i As Integer
        If k = 1 Then
            For i = 0 To ddlSubjectExport.Items.Count - 1
                If ddlSubjectExport.Items(i).Selected = True Then
                    If strStubjects <> "" Then
                        strStubjects += ","
                    End If
                    strStubjects += ddlSubjectExport.Items(i).Value
                End If
            Next
        End If
        If strStubjects = "" Then
            strStubjects = "0"
        End If
        ddlReportHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_RSM_ID,RSD_RESULT,RSD_CSSCLASS,RSD_HEADER  FROM RPT.REPORT_SETUP_D " & _
                                     " WHERE RSD_RSM_ID=" & rsm_id & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " & _
                                     " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID IN('" + strStubjects + "'))" & _
                                     " AND RSD_CSSCLASS='textboxmulti' ORDER BY RSD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportHeader.DataSource = ds
        ddlReportHeader.DataTextField = "RSD_HEADER"
        ddlReportHeader.DataValueField = "RSD_ID"
        ddlReportHeader.DataBind()

    End Sub
    Sub ExportComments()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        If (isgeneral1()) Then
            str_query = "SELECT SBG_DESCR as Subject,CAT_DESC as Category,CMT_COMMENTS as Comments FROM ACT.COMMENTS_M" _
                                    & " INNER JOIN SUBJECTS_GRADE_S ON SBG_ID=CMT_SBG_ID" _
                                    & " INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID" _
                                    & " WHERE CMT_RSm_ID=" + ddlReportCardExport.SelectedValue.ToString _
                                    & " AND CMT_GRD_ID='" + ddlGradeExport.SelectedValue.ToString + "'"
            If ddlSubjectExport.SelectedValue.ToString <> "0" Then
                str_query += " AND CMT_SBG_ID=" + ddlSubjectExport.SelectedValue.ToString
            End If
        Else
            str_query = "SELECT CAT_DESC as Category,CMT_COMMENTS as Comments FROM ACT.COMMENTS_M" _
                                       & " INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID" _
                                       & " WHERE CMT_RSm_ID=" + ddlReportCardExport.SelectedValue.ToString _
                                       & " AND CMT_GRD_ID='" + ddlGradeExport.SelectedValue.ToString + "' AND CMT_SBG_ID=0 "

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable
        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)

        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))

        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        System.IO.File.Delete(tempFileName)
        lblerror3.Text = "Export Completed"
    End Sub

#End Region

    Protected Sub ddlAcademicYearExport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYearExport.SelectedIndexChanged


        BindReportCard(ddlReportCardExport, ddlAcademicYearExport.SelectedValue.ToString)
        PopulateGrades(ddlGradeExport, ddlReportCardExport.SelectedValue.ToString)
        PopulateSubjects(ddlSubjectExport, ddlAcademicYearExport.SelectedValue.ToString, ddlGradeExport.SelectedValue.ToString)

        BindHeader(ddlHeaderExport, ddlReportCardExport.SelectedValue.ToString, 1)
    End Sub

    Protected Sub ddlGradeUpload_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGradeUpload.SelectedIndexChanged

        'PopulateGrades(ddlGradeUpload, ddlReportcardUpload.SelectedValue.ToString)
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportComments()
    End Sub
    Private Function PopulateReports(ByVal ddlReports As DropDownList)
        ddlReports.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & Session("CURRENT_ACD_ID") & " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReports.DataSource = ds
        ddlReports.DataTextField = "RSM_DESCR"
        ddlReports.DataValueField = "RSM_ID"
        ddlReports.DataBind()

        Return ddlReports
    End Function
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY  " _
                       & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                       & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                       & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & ddlReport.SelectedValue & "' " _
                       & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "DESCR1"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count >= 1 Then
            ddlGrade.SelectedIndex = 0
            Try
                FillSubjects()
                gridbind()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Sub FillSubjects()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + Session("Current_ACD_ID") + "'"



        str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"

        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub
    Private Function isCommentsExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select count(CMT_ID) from ACT.COMMENTS_M WHERE CMT_COMMENTS='" + txtComments.Text + "' AND CMT_ID<>" + H_CMT_ID.Value.ToString
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If sbm = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
            Dim strSubject As String = ""
            Dim strHeader As String = ""
            Dim txtSearch As New TextBox
            Dim optSearch As String
            Dim strFilter As String = ""

            If radGeneralComments.Checked = False Then
                If ddlGrade.Items.Count >= 1 Then
                    Dim GRD_ID As String = ddlGrade.SelectedItem.Value
                    str_Sql = " SELECT DISTINCT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CAT_DESC,RSD_HEADER,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
                              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
                              " INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID" & _
                              " INNER JOIN RPT.REPORT_SETUP_D ON CMT_RSD_ID=RSD_ID AND RSD_RSM_ID=" + ddlReport.SelectedValue.ToString & _
                              " WHERE CMT_GRD_ID='" & GRD_ID & "'"
                    strSubject = getSubjects()
                    If strSubject <> "" Then
                        str_Sql += "AND CMT_SBG_ID IN (" & strSubject & ")"
                    End If

                    strHeader = getHeaders()

                    If strHeader <> "" Then
                        str_Sql += " AND CMT_RSD_ID IN(" & strHeader & ")"
                    End If
                    gvComments.Columns(4).Visible = True
                    gvComments.Columns(5).Visible = True
                Else
                    str_Sql = " SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
                              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
                              " WHERE CMT_GRD_ID='0'"
                End If
            Else

                str_Sql = " SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CAT_DESC,'' AS RSD_HEADER,CMT_COMMENTS,'' AS SBG_DESCR,'' AS SBG_GRD_ID " & _
                          " FROM ACT.COMMENTS_M CMT INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID WHERE CMT_bGENCOMMENTS=1 "
                gvComments.Columns(2).Visible = False
                gvComments.Columns(3).Visible = False
                gvComments.Columns(4).Visible = False
                gvComments.Columns(5).Visible = False
            End If

            If ddlCategory.SelectedValue.ToString <> "0" Then
                str_Sql += " AND CMT_CAT_ID=" + ddlCategory.SelectedValue.ToString
            End If
            'GetSelectedGrades

            If gvComments.Rows.Count > 0 Then

                txtSearch = gvComments.HeaderRow.FindControl("txtOption")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch.Text <> "" Then
                    strFilter = " and CMT_COMMENTS  like '" & txtSearch.Text & "%' "
                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " ORDER BY 1")

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Function GetReportHeader(ByVal ddlHeader As CheckBoxList) As CheckBoxList
        Try
            Dim strHeader As String = ""
            Dim strStubjects As String = ""

            Dim i As Integer
            For i = 0 To ddlSubject.Items.Count - 1
                If ddlSubject.Items(i).Selected = True Then
                    If strStubjects <> "" Then
                        strStubjects += ","
                    End If
                    strStubjects += ddlSubject.Items(i).Value
                End If
            Next

            If strStubjects = "" Then
                strStubjects = "0"
            End If

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            Dim strSQL As String = "SELECT RSD_ID,RSD_RSM_ID,RSD_RESULT,RSD_CSSCLASS,RSD_HEADER  FROM RPT.REPORT_SETUP_D " & _
                                   " WHERE RSD_RSM_ID=" & ddlReport.SelectedValue & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " & _
                                   " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID IN('" + strStubjects + "'))" & _
                                   " AND RSD_CSSCLASS='textboxmulti' ORDER BY RSD_DISPLAYORDER "
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

            ddlHeader.DataSource = dsHeader
            ddlHeader.DataTextField = "RSD_HEADER"
            ddlHeader.DataValueField = "RSD_ID"
            ddlHeader.DataBind()


            Return ddlHeader
        Catch ex As Exception

        End Try

    End Function
    Private Sub PopulateGrades(ByVal ddlGrade As DropDownList)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'If isCommentsExists() = False Then
            SaveData()
            ' Else
            'lblError.Text = "This subject already exists"
            ' End If
            clearControls()
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Dim IntMode As Integer
        Dim INTrESULT As Integer
        Dim intGenComments As Integer

        If (ViewState("datamode") = "add") Then
            IntMode = 0
            H_CMT_ID.Value = 0
        ElseIf (ViewState("datamode") = "edit") Then
            IntMode = 1
        Else
            IntMode = 2
        End If

        If ddlCategory.SelectedValue.ToString <> "0" Then
            H_CAT_ID.Value = ddlCategory.SelectedValue.ToString
        End If
        If radGeneralComments.Checked = True Then
            'If General Comments
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    If ViewState("datamode") = "edit" Then
                        'UtilityObj.InsertAuditdetails(transaction, "edit", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                    ElseIf ViewState("datamode") = "delete" Then
                        'UtilityObj.InsertAuditdetails(transaction, "delete", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                    End If
                    'Dim str_query As String = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",'" + txtComments.Text.ToString + "'," & _
                    '                          "" + chkGen.Checked.ToString + "," & IntMode.ToString & "" & _
                    '                          ""
                    Dim str_query As String
                    If chkAOLcomment.Checked Then
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,-1,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    ElseIf radGeneralComments.Checked Then
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade0.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,0,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    Else
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,0,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    End If
                    INTrESULT = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CMT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using
        Else ' If Not General Comments
            For Each lstItem As ListItem In ddlSubject.Items
                If lstItem.Selected Then ' 

                    For Each lstHeaderItem As ListItem In chkHeader.Items
                        If lstHeaderItem.Selected Then

                            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    If ViewState("datamode") = "edit" Then
                                        'UtilityObj.InsertAuditdetails(transaction, "edit", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                                    ElseIf ViewState("datamode") = "delete" Then
                                        'UtilityObj.InsertAuditdetails(transaction, "delete", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                                    End If

                                    Dim str_query As String
                                    If chkAOLcomment.Checked Then
                                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + "," & lstItem.Value & ",'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",'" + txtComments.Text.ToString + "'," & _
                                        "" + ddlReport.SelectedValue + ",0,-1,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                                    Else
                                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + "," & lstItem.Value & ",'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.ToString + "'," & _
                                                      "" + ddlReport.SelectedValue + ",0," + lstHeaderItem.Value + ",'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & "" & _
                                                      ""
                                    End If
                                    INTrESULT = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CMT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                                    If flagAudit <> 0 Then
                                        Throw New ArgumentException("Could not process your request")
                                    End If
                                    transaction.Commit()
                                    lblError.Text = "Record Saved Successfully"

                                Catch myex As ArgumentException
                                    transaction.Rollback()
                                    lblError.Text = myex.Message
                                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                                Catch ex As Exception
                                    transaction.Rollback()
                                    lblError.Text = "Record could not be Saved"
                                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                                End Try
                            End Using

                        End If ' Heder Item ..If Ending 

                    Next '' Header Item Loop Ending 
                End If
            Next  'Subjects Loop Ending 
        End If

    End Sub
    Private Sub clearControls()
        txtComments.Text = ""
        btnSave.Text = "Save"
    End Sub
    Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strSubject As String = ""
        Dim ds As DataSet
        If radCatBySubject.Checked = True Then

            strSubject = getSubjects()

            Select Case Session("sbsuid")
                Case "131001", "131002", "121012", "121013", "121014", "121009", "115002", "123004", "141001", "151001"
                    str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                      & " ( CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "')" _
                      & " AND CAT_BSU_ID='" + Session("SBSUID") + "'"
                Case Else
                    str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                      & " ( CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' OR CAT_GRD_ID IS NULL)" _
                      & " AND CAT_BSU_ID='" + Session("SBSUID") + "'"
                    If strSubject <> "" Then
                        str_query += " AND ( CAT_ID IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_SBG_ID IN(" + strSubject + ") " _
                                  & " AND CMT_RSM_ID=" + ddlReport.SelectedValue.ToString + ") "

                        'INCLUDE NEW CATEGORIES ALSO
                        str_query += " OR CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_BSU_ID='" + Session("SBSUID") + "')) "
                    Else
                        str_query += " AND (CAT_ID IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE  " _
                       & " CMT_RSM_ID=" + ddlReport.SelectedValue.ToString + ")"

                        str_query += " OR CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_BSU_ID='" + Session("SBSUID") + "')) "
                    End If
            End Select
            str_query += " ORDER BY CAT_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "CAT_DESC"
            ddlCategory.DataValueField = "CAT_ID"
            ddlCategory.DataBind()

        Else
            str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                    & " (CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' OR CAT_GRD_ID IS NULL)"
            str_query += " AND CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE  " _
            & " ISNULL(CMT_bGEnCOMMENTS,0)=0 AND CMT_BSU_ID='" + Session("SBSUID") + "') AND CAT_BSU_ID='" + Session("SBSUID") + "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "CAT_DESC"
            ddlCategory.DataValueField = "CAT_ID"
            ddlCategory.DataBind()
        End If

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)
    End Sub
    Function getSubjects() As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To ddlSubject.Items.Count - 1
            If ddlSubject.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += ddlSubject.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            H_CMT_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvComments.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Function getHeaders() As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To chkHeader.Items.Count - 1
            If chkHeader.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += chkHeader.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FillGrade()
            FillSubjects()
            BindCategory()
            If ddlReport.SelectedValue <> "" Then
                GetReportHeader(chkHeader)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSubjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Try
                BindCategory()
                gridbind()
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub chkHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FillSubjects()
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnempid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlReportCardExport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCardExport.SelectedIndexChanged

        PopulateGrades(ddlGradeExport, ddlReportCardExport.SelectedValue.ToString)
        PopulateSubjects(ddlSubjectExport, ddlAcademicYearExport.SelectedValue.ToString, ddlGradeExport.SelectedValue.ToString)

        BindHeader(ddlHeaderExport, ddlReportCardExport.SelectedValue.ToString, 1)
    End Sub

    Protected Sub ddlReportcardUpload_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportcardUpload.SelectedIndexChanged
        PopulateGrades(ddlGradeUpload, ddlReportcardUpload.SelectedValue.ToString)
        BindHeader(ddlHeaderUpload, ddlReportcardUpload.SelectedValue.ToString, 0)
    End Sub

    Protected Sub ddlSubjectExport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubjectExport.SelectedIndexChanged
        'PopulateSubjects(ddlSubjectExport, ddlAcademicYearExport.SelectedValue.ToString, ddlGradeExport.SelectedValue.ToString)
    End Sub

    Protected Sub ddlGradeExport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGradeExport.SelectedIndexChanged
        PopulateSubjects(ddlSubjectExport, ddlAcademicYearExport.SelectedValue.ToString, ddlGradeExport.SelectedValue.ToString)
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        'UpLoadExcelFile()
        UpLoadExcelFiletoXml()

    End Sub
    Private Sub UpLoadExcelFile()

        If uploadFile.HasFile Then
            Dim tempDir As String = HttpContext.Current.Server.MapPath("~/Curriculum/ReportDownloads/")
            ' Dim tempDir As String = "~/Curriculum/ReportDownloads/"

            '"C:\Users\rajesh.kumar\Desktop\New Microsoft Excel Worksheet.xls"
            Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls" ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."

            Dim tempFileNameUsed As String = tempDir + tempFileName
            If uploadFile.HasFile Then
                If File.Exists(tempFileNameUsed) Then
                    File.Delete(tempFileNameUsed)
                End If
                uploadFile.SaveAs(tempFileNameUsed)
                Try
                    getdataExcel(tempFileNameUsed)
                    File.Delete(tempFileNameUsed)
                    lblError.Text = ""
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed"
                End Try
            End If
        End If
    End Sub

    Sub getdataExcel(ByVal filename As String)
        Dim subject As String = "", cate As String = "", comment As String = "", suj As String = ""
        Dim str_query As String = ""
        Dim transaction As SqlTransaction
        Dim transaction1 As SqlTransaction
        Dim IntMode As Integer, i As Integer
        Dim INTrESULT As Integer
        Dim m As Integer

        i = 0
        IntMode = ddlUploadType.SelectedIndex


        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim mObj As ExcelRowCollection

        Dim iRowRead As Boolean
        iRowRead = True

        ef.Save(filename)
        Dim mRowObj As ExcelRow
        mObj = ef.Worksheets(0).Rows
        Dim iRow As Integer = 0



        Dim category As New ArrayList
        While iRowRead
            mRowObj = mObj(iRow)
            If mRowObj.Cells(0).Value Is Nothing Then
                Exit While
            End If
            If mRowObj.Cells(0).Value.ToString = "" Then
                Exit While
            End If

            If (isgeneral()) Then
                subject = mRowObj.Cells(0).Value.ToString()
                cate = mRowObj.Cells(1).Value.ToString()
                comment = mRowObj.Cells(2).Value.ToString()
                m = 0
            Else
                subject = "0"
                cate = mRowObj.Cells(0).Value.ToString()
                comment = mRowObj.Cells(1).Value.ToString()
                m = 1
            End If


            If IntMode = 2 Then
                If ((suj <> subject) And (i = 1)) Then i = 0
                If i = 0 Then
                    Using conn1 As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                        transaction1 = conn1.BeginTransaction("SampleTransaction")
                        Try
                            str_query = "exec act.delCOMMENTS_Mfromexcel '" + subject + "','" + ddlGradeUpload.SelectedValue.ToString + "'," & _
                                                           " " & ddlReportcardUpload.SelectedValue.ToString + "," + ddlHeaderUpload.SelectedValue.ToString + ",'" + Session("sBsuid") + "'"

                            INTrESULT = SqlHelper.ExecuteNonQuery(transaction1, CommandType.Text, str_query)
                            transaction1.Commit()
                            i = 1
                            suj = subject
                        Catch ex As Exception
                            transaction1.Rollback()
                        End Try
                    End Using
                End If

            End If


            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try


                    str_query = "exec act.saveCOMMENTS_Mfromexcel '" + subject + "','" + ddlGradeUpload.SelectedValue.ToString + "','" + cate + "','" + comment + "'," & _
                                             " " & ddlReportcardUpload.SelectedValue.ToString + ",0," + ddlHeaderUpload.SelectedValue.ToString + ",'" + Session("sBsuid") + "'," + m.ToString + ""


                    INTrESULT = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    transaction.Commit()
                Catch ex As Exception
                    transaction.Rollback()
                    lblerror2.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using

            iRow += 1


        End While
        lblerror2.Text = "Records Saved Sucessfully"




    End Sub
    Private Function isgeneral() As Boolean
        Dim ds As Boolean

       
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT RSD_bALLSUBJECTS  FROM RPT.REPORT_SETUP_D where RSD_RSM_ID='" + ddlReportcardUpload.SelectedValue.ToString + "' AND rsd_ID='" + ddlHeaderUpload.SelectedValue.ToString + "'"

        ds = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return ds


    End Function

    Private Function isgeneral1() As Boolean
        Dim ds As Boolean


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT RSD_bALLSUBJECTS  FROM RPT.REPORT_SETUP_D where RSD_RSM_ID='" + ddlReportCardExport.SelectedValue.ToString + "' AND rsd_ID='" + ddlHeaderExport.SelectedValue.ToString + "'"

        ds = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return ds


    End Function

    Private Sub UpLoadExcelFiletoXml()
        Dim strFileNameOnly As String, IntMode As Integer, m As Integer
        IntMode = ddlUploadType.SelectedIndex
        lblError.Text = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "-") & Session("sBsuid") & "_" & ddlGradeUpload.SelectedItem.Value & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly & ";" & _
                "Extended Properties=Excel 8.0;"

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("~/Curriculum/ReportDownloads/") & "/" & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim s As New MemoryStream()

                dt.WriteXml(s, True)



                'Retrieve the text from the stream

                s.Seek(0, SeekOrigin.Begin)

                Dim sr As New StreamReader(s)

                Dim xmlString As String

                xmlString = sr.ReadToEnd()



                'close 

                sr.Close()

                sr.Dispose()
                Dim cmd As New SqlCommand
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("act.ProcessommentXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ACD_ID", ddlAcademicYearUpload.SelectedValue.ToString)
                cmd.Parameters.AddWithValue("@CMT_GRD_ID", ddlGradeUpload.SelectedItem.Value)
                cmd.Parameters.AddWithValue("@CMT_RSM_ID", ddlReportcardUpload.SelectedValue.ToString)
                cmd.Parameters.AddWithValue("@CMT_RSD_ID", ddlHeaderUpload.SelectedValue.ToString)
                cmd.Parameters.AddWithValue("@CMT_BSU_ID ", Session("sBsuid"))
                If (isgeneral()) Then
                    m = 0
                Else
                    m = 1
                End If
                cmd.Parameters.AddWithValue("@CMT_GEN", m)
                cmd.Parameters.AddWithValue("@edit", IntMode)
                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()

                lblerror2.Text = "Records Saved Sucessfully"


            Catch ex As Exception
                lblerror2.Text = "Error: " & ex.Message.ToString
            End Try
        Else
            lblerror2.Text = "Please select a file to upload."
        End If


    End Sub

    
End Class
