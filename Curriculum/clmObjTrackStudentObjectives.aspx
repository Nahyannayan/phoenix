﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmObjTrackStudentObjectives.aspx.vb" Inherits="Curriculum_clmObjTrackStudentObjectives" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<style>
    /*table th{
        vertical-align :bottom !important;
    }*/
     .min-field-width {
         min-width:80px !important; 
         max-width:90px !important;
    }
    
</style>
<script>
  //function change_ddl_state(ddlThis,ddlObj)
  //      {                 
          
  //        for(i=0; i<document.forms[0].elements.length; i++)
  //             {
  //             var currentid =document.forms[0].elements[i].id; 
               
              
  //             if( currentid.indexOf(ddlObj)!=-1)
  //           {
           
  //               if (ddlThis.selectedIndex!=0)
  //               {
                   
  //                  document.forms[0].elements[i].selectedIndex=ddlThis.selectedIndex-1;
                    
  //                }                    
                     
  //               }
  //            }
  //        }           
    function change_ddl_state(ddlThis, ddlObj) {



        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;


            if (currentid.indexOf(ddlObj) != -1) {

                if (ddlThis.selectedIndex != 0) {

                    document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex - 1;



                    if (document.forms[0].elements[i].selectedIndex == 0) {
                        ddlThis.style.backgroundColor = "green";
                        document.forms[0].elements[i].style.backgroundColor = "green";
                    }
                    else if (document.forms[0].elements[i].selectedIndex == 1) {
                        ddlThis.style.backgroundColor = "yellow";
                        document.forms[0].elements[i].style.backgroundColor = "yellow";
                    }
                    else if (document.forms[0].elements[i].selectedIndex == 2) {
                        ddlThis.style.backgroundColor = "red";
                        document.forms[0].elements[i].style.backgroundColor = "red";
                    }


                }


            }
        }
    }
           
 function GetClientId(strid)
{
     var count=document.forms[0].length;
     var i=0;
     var eleName; 
     for (i=0; i < count; i++ )
     {
       eleName=document.forms[0].elements[i].id; 
       pos=eleName.indexOf(strid);
       if(pos>=0)  break;            
     }
    return eleName;
}     
          
    function autoWidth(mySelect)
    {
        var maxlength = 0;
       
     //   var mySelect=document.getElementById(GetClientId(ddl));
        
              
        for (var i=0; i<mySelect.options.length;i++)
        {
            if (mySelect[i].text.length > maxlength)
            {
                maxlength = mySelect[i].text.length;
            }
        }
        mySelect.style.width = maxlength * 5;
    }
    
    function setWidth(mySelect)
    {
          //var mySelect=document.getElementById(GetClientId(ddl));
        mySelect.style.width = 35;
    }

    function setBackColor(ddlThis) {

        if (ddlThis.selectedIndex == 0) {
            ddlThis.style.backgroundColor = "green";
        }
        else if (ddlThis.selectedIndex == 1) {
            ddlThis.style.backgroundColor = "yellow";
        }
        else if (ddlThis.selectedIndex == 2) {
            ddlThis.style.backgroundColor = "red";
        }
    }
</script>
     

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_dialog" runat="server" Behaviors="Close,Move"  
                Width="900px" Height="620px" >
            </telerik:RadWindow>
        </Windows>        
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           Student Objectives
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkSearch" runat="server" Visible="false">Search</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:Panel ID="pnlSearch" runat="server">
                                <table id="Table2" runat="server" align="left"
                                    cellpadding="5" cellspacing="0"  width="100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>

                                        <td align="left">
                                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Grade</span> </td>

                                        <td align="left" >
                                            <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Subject</span> </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Group</span> </td>
                                        <td  align="left">
                                            <asp:DropDownList ID="ddlGroup" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Level</span> </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlLevel"
                                                runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Objective Category</span> </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCategory" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="rdObj1" runat="server" Checked="True" GroupName="g1"
                                                            Text="Objectives 1 to 25" CssClass ="field-label"  />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj2" runat="server" GroupName="g1"
                                                            Text="Objectives 26 to 50" CssClass ="field-label" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj3" runat="server" GroupName="g1"
                                                            Text="Objectives 51 to 75"  CssClass ="field-label"/>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj4" runat="server" GroupName="g1"
                                                            Text="Objectives 76 to 100" CssClass ="field-label" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Student ID</span> </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Student Name</span> </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnView" runat="server" CssClass="button" TabIndex="7"
                                                Text="View Students" ValidationGroup="groupM1" />
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                        </td>
                    
                    </tr>

                     </table>
            </div>
        </div>
    </div>

    <div class="card mb-3" id="DivstudObjGrid" runat="server">
        <div class="card-header letter-space">
            <%--<i class="fa fa-book mr-3"></i>--%>
           Student Objectives
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" align="center">
                    <tr id="trGrid" runat="server">
                        <td align="left">
                            <table style="border-collapse: collapse" width="100%">
                                <tr>
                                    <td align="left">
                                        <div id="div_gridholder">
                                            <asp:GridView ID="gvStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False" 
                                                 EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." CssClass="table table-bordered table-row"
                                                PageSize="20" UseAccessibleHeader="True" BorderStyle="None" >
                                                <RowStyle  CssClass="griditem" />
                                                <SelectedRowStyle CssClass="Green" />
                                                <HeaderStyle CssClass="FreezeHeader" VerticalAlign="Top" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <EmptyDataRowStyle />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Stuid" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="First Name">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lnkFirstNameH" runat="server" Text="First Name"
                                                                OnClick="lnkFirstNameH_Click"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblFirstName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="FrozenCell"></ItemStyle>
                                                        <HeaderStyle />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Last Name">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lnkLastNameH" runat="server" Text="Last Name"
                                                                Font-Underline="false" OnClick="lnkLastNameH_Click"></asp:Label>
                                                        </HeaderTemplate>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("STU_LASTNAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="FrozenCell"></ItemStyle>
                                                        <HeaderStyle />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Current Level">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrentLevel" runat="server" Text='<%# Bind("SCL_CURRENTLEVEL") %>'></asp:Label>
                                                            <asp:Label ID="lblCurrentGrade" runat="server" Text='<%# Bind("SCL_GRADE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Objectives Completed">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblObjCount" runat="server" Text='<%# Bind("OBJCOUNT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Stuno" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj1H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId1" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH1" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj1Id');"  >
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background-color: green;" />
                                                                <asp:ListItem Text="I" Value="I" style="background-color: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background-color: red;" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        
                                                        <ItemTemplate>

                                                            <asp:DropDownList ID="ddlObj1Id" runat="server" OnChange="Javascript:setBackColor(this);" >
                                                                <asp:ListItem Text="Y" Value="Y" Style="background-color : green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" Style="background-color: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" Style="background-color: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" CssClass="min-field-width" />
                                                        <HeaderStyle Wrap="true" VerticalAlign="Bottom" />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>                                                                                                                               
                                                            <asp:Label ID="lblObj2H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId2" runat="server" Visible="false"></asp:Label>
                                                                        <br />
                                                                         <asp:DropDownList ID="ddlObjH2" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj2Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>                                                                                                                                                                                                                                                                                                  
                                                            <%--<table width="100%" height="100%">
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj2Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                        <HeaderStyle Wrap="true" VerticalAlign="Bottom" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj3H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId3" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH3" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj3Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj3Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                        <HeaderStyle Wrap="true" VerticalAlign="Bottom" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj4H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId4" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH4" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj4Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj4Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle VerticalAlign="Bottom" />
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj5H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId5" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH5" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj5Id');" >
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj5Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj6H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId6" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH6" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj6Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj6Id" runat="server" onChange="Javascript:setBackColor(this);"> 
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj7H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId7" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH7" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj7Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj7Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj8H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId8" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH8" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj8Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj8Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj9H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId9" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH9" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj9Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--   <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                     
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj9Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj10H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId10" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH10" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj10Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>

                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj10Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj11H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId11" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH11" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj11Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj11Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj12H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId12" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH12" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj12');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj12Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj13H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId13" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH13" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj13Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj13Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj14H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId14" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH14" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj14Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--  <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj14Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj15H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId15" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH15" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj15Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj15Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj16H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId16" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH16" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj16Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                      
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj16Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj17H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId17" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH17" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj17Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj17Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj18H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId18" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH18" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj18Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj18Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj19H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId19" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH19" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj19Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj19Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj20H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId20" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH20" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj20Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj20Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj21H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId21" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH21" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj21Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj21Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj22H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId22" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH22" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj22Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj22Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj23H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId23" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH23" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj23Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                            </asp:DropDownList>
                                                            <%-- <table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj23Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj24H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId24" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH24" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj24Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj24Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj25H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId25" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH25" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj25Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red" />
                                                            </asp:DropDownList>
                                                            <%--<table>
                                                                <tr style="vertical-align: top">
                                                                    <td>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj25Id" runat="server" onChange="Javascript:setBackColor(this);">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="min-field-width" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                        <asp:HiddenField ID="h_LVL_ID" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    

 </asp:Content>

