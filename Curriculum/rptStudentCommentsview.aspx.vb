Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security

Partial Class Curriculum_rptStudentCommentsview
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            'Session("StuComments") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ViewState("datamode") = "add"

                    If ViewState("datamode") = "add" Then

                        'ddlAcdID = ActivityFunctions.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sBsuid"))
                        FillAcd()
                    Else

                        H_ACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("AccId").Replace(" ", "+"))
                        H_GRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("GradeId").Replace(" ", "+"))
                        'H_SBJ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SubjID").Replace(" ", "+"))
                        H_RPT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("MarksId").Replace(" ", "+"))
                        'DisplayRecordForEdit()
                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Functions"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    'Private Sub SaveRecord()

    '    Dim iReturnvalue As Integer
    '    Dim iIndex As Integer
    '    Dim cmd As New SqlCommand
    '    Dim lblStudId As New Label
    '    Dim lblStudName As New Label
    '    Dim txtComments As New TextBox


    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim objConn As New SqlConnection(str_conn)
    '        objConn.Open()
    '        Dim stTrans As SqlTransaction = objConn.BeginTransaction

    '        For iIndex = 0 To gvStudents.Rows.Count - 1
    '            lblStudId = TryCast(gvStudents.Rows(iIndex).FindControl("lblStudId"), Label)
    '            lblStudName = TryCast(gvStudents.Rows(iIndex).FindControl("lblStudName"), Label)
    '            txtComments = TryCast(gvStudents.Rows(iIndex).FindControl("txtComments"), TextBox)
    '            If Not lblStudId Is Nothing Then
    '                cmd = New SqlCommand("[RPT].[SaveREPORT_STUDCOMMENTS_S]", objConn, stTrans)
    '                cmd.CommandType = CommandType.StoredProcedure

    '                'With Session("StuComments")
    '                If ViewState("datamode") = "add" Then
    '                    cmd.Parameters.AddWithValue("@RST_ID", 0)
    '                End If
    '                If ViewState("datamode") = "edit" Then
    '                    ' cmd.Parameters.AddWithValue("@RST_ID", .Rows(iIndex)("id"))
    '                End If

    '                cmd.Parameters.AddWithValue("@RST_RPF_ID", 0)
    '                cmd.Parameters.AddWithValue("@RST_RSD_ID", Convert.ToInt32(H_RPT_ID.Value)) '-- Report Setup
    '                cmd.Parameters.AddWithValue("@RST_ACD_ID", Convert.ToInt32(H_ACD_ID.Value))
    '                cmd.Parameters.AddWithValue("@RST_GRD_ID", H_GRD_ID.Value)
    '                cmd.Parameters.AddWithValue("@RST_STU_ID", Convert.ToInt32(lblStudId.Text))
    '                cmd.Parameters.AddWithValue("@RST_RSS_ID", 0)
    '                cmd.Parameters.AddWithValue("@RST_SGR_ID", 0)
    '                cmd.Parameters.AddWithValue("@RST_SBG_ID", 0)
    '                cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", "")
    '                cmd.Parameters.AddWithValue("@RST_MARK", 0)
    '                cmd.Parameters.AddWithValue("@RST_COMMENTS", txtComments.Text)
    '                cmd.Parameters.AddWithValue("@RST_GRADING", "")

    '                'End With
    '                If ViewState("datamode") = "add" Then
    '                    cmd.Parameters.AddWithValue("@bEdit", 0)
    '                End If
    '                If ViewState("datamode") = "edit" Then
    '                    cmd.Parameters.AddWithValue("@bEdit", 1)
    '                End If
    '                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
    '                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
    '                cmd.ExecuteNonQuery()
    '                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
    '            End If
    '        Next
    '        If iReturnvalue <> 0 Then
    '            stTrans.Rollback()
    '            lblError.Text = "Unexpected error"
    '            Exit Sub
    '        End If
    '        stTrans.Commit()
    '        lblError.Text = "Successfully Saved"
    '    Catch ex As Exception

    '    End Try
    'End Sub



    Private Sub ClearComments()
       
        H_CMT_CAT_ID.Value = 0

    End Sub

    Private Sub ClearEntireScreen()
       
        H_CMT_CAT_ID.Value = 0

    End Sub


    Private Function GetStudentMarks()
        Try

        Catch ex As Exception

        End Try
    End Function

    Private Function BindStudents()
        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        gvStudents.DataSourceID = ""
        Dim tmpClmCount As Integer = 0
        Dim txtSearch As New TextBox
        Dim str_search As String
        Dim str_filter_StudentNo As String = String.Empty
        Dim str_filter_StudName As String = String.Empty
        Dim str_STUD_NO As String = String.Empty
        Dim str_STUD_NAME As String = String.Empty
        Dim ds As DataSet

        Try
            If txtRepHeader.Text = "" Then
                lblError.Text = "Specify Report Header"
                Exit Function
            End If
            If H_GRD_ID.Value <> "" Then
                strCriteria = " AND STU_GRD_ID='" & H_GRD_ID.Value & "' "
            End If
            If H_ACD_ID.Value <> "" Then
                strCriteria += " AND RST_ACD_ID=" & H_ACD_ID.Value & " "
            End If
            'If H_SCH_ID.Value <> "" Then
            '    strCriteria += " AND RST_RPF_ID=" & H_SCH_ID.Value & " "
            'End If
            
            '" '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, ('View Comments '+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview" & _

            If strCriteria <> "" Then
                strSql = "select rst_stu_id as StudentId,isnull(stu_firstname,'')+' '+isnull(stu_midname,'')+''+isnull(stu_lastname,'') as StudentName,rst_comments as comments,rst_id,stu_no as StudentNo from rpt.report_student_s inner join rpt.report_setup_d on report_setup_d.rsd_id=report_student_s.rst_rsd_id  inner join vw_student_m on report_student_s.rst_stu_id= vw_student_m.stu_id where report_student_s.rst_rsd_id='" & H_SETUP.Value & "' and report_student_s.rst_comments<>''" + strCriteria

            Else
                strSql = "select rst_stu_id as StudentId,isnull(stu_firstname,'')+' '+isnull(stu_midname,'')+''+isnull(stu_lastname,'') as StudentName,rst_comments as comments,rst_id,stu_no as StudentNo from rpt.report_student_s inner join rpt.report_setup_d on report_setup_d.rsd_id=report_student_s.rst_rsd_id  inner join vw_student_m on report_student_s.rst_stu_id= vw_student_m.stu_id where report_student_s.rst_rsd_id='" & H_SETUP.Value & "'and report_student_s.rst_comments<>''"

            End If



            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If dsStudents.Tables(0).Rows.Count > 0 Then

                'Else

                '    gvStudents.DataSource = dsStudents.Tables(0)
                '    gvStudents.DataBind()

                '    'gvStudents.Columns(1).Visible = False
                '    gvStudents.Columns(1).HeaderStyle.Width = 0
                '    gvStudents.Columns(1).HeaderStyle.Width = 0
                '    'GetComments()
                If gvStudents.Rows.Count > 0 Then

                    txtSearch = TryCast(gvStudents.HeaderRow.FindControl("txtStud_No"), TextBox)
                    If Not txtSearch Is Nothing Then
                        str_STUD_NO = txtSearch.Text
                        If str_STUD_NO <> "" Then
                            str_filter_StudentNo = " AND STU_NO LIKE '%" & txtSearch.Text & "%'"
                        End If
                    End If
                    txtSearch = TryCast(gvStudents.HeaderRow.FindControl("txtStud_Name"), TextBox)
                    str_STUD_NAME = txtSearch.Text
                    If str_STUD_NAME <> "" Then
                        str_filter_StudName = " AND  isnull(stu_firstname,'')+' '+isnull(stu_midname,'')+''+isnull(stu_lastname,'') LIKE '%" & txtSearch.Text & "%'"
                    End If
                End If
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql & str_filter_StudentNo & str_filter_StudName & "ORDER BY StudentName")
                gvStudents.DataSource = ds
                gvStudents.DataBind()
                SetCommentControls()
            Else
                gvStudents.DataSource = Nothing
                gvStudents.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function GetReportHeader() As String
        Try
            Dim NVCReportSetup As New NameValueCollection

            Dim strHeader As String = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            Dim strSQL As String = "SELECT CAST(RSD_ID AS VARCHAR) + '_' + CAST(RSD_RSM_ID AS VARCHAR) AS Value ,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=2 AND RSD_BDIRECTENTRY=1"
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            With dsHeader.Tables(0)
                For intCnt As Integer = 0 To .Rows.Count - 1
                    strHeader += "'' AS '" + .Rows(intCnt).Item("RSD_HEADER").ToString + "' ,"
                    NVCReportSetup.Add(.Rows(intCnt).Item("RSD_HEADER").ToString, .Rows(intCnt).Item("Value").ToString)
                Next
                If strHeader.Length > 1 Then
                    'strHeader = strHeader.Substring(0, strHeader.Length - 1)
                End If
            End With
            Session("ReportHeader") = NVCReportSetup
            Return strHeader
        Catch ex As Exception

        End Try

    End Function

#End Region

#Region "Pick Up Values"

    Protected Sub imgAcdId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            'If H_ACD_ID.Value <> "" AndAlso H_ACD_ID.Value.Contains("___") Then
            '    Dim ACD_ID As String = H_ACD_ID.Value.Split("___")(0)
            '    txtAccYear.Text = H_ACD_ID.Value.Split("___")(3)
            '    H_ACD_ID.Value = ACD_ID
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_GRD_ID.Value <> "" AndAlso H_GRD_ID.Value.Contains("___") Then
                Dim GRD_ID As String = H_GRD_ID.Value.Split("___")(0)
                txtGrade.Text = H_GRD_ID.Value.Split("___")(3)
                H_GRD_ID.Value = GRD_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub imgGroup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        txtGroup.Text = ""
    '        If H_GRP_ID.Value <> "" AndAlso H_GRP_ID.Value.Contains("___") Then
    '            Dim GRP_ID As String = H_GRP_ID.Value.Split("___")(0)

    '            If H_GRP_ID.Value.Split("___").Length > 3 Then
    '                ' Split the String If multipple Uder score is found
    '                Dim Strsplit As String()
    '                Strsplit = H_GRP_ID.Value.Split("___")
    '                For inti As Integer = 3 To Strsplit.Length - 2
    '                    txtGroup.Text += Strsplit(inti).ToString() + "_"
    '                Next
    '                txtGroup.Text += Strsplit(Strsplit.Length - 1).ToString()
    '            Else
    '                txtGroup.Text = H_GRP_ID.Value.Split("___")(3)
    '            End If
    '            H_GRP_ID.Value = GRP_ID
    '            H_SBJ_GRD_ID.Value = GRP_ID
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        If H_SBJ_GRD_ID.Value <> "" AndAlso H_SBJ_GRD_ID.Value.Contains("___") Then
    '            Dim SBG_ID As String = H_SBJ_GRD_ID.Value.Split("___")(0)
    '            'txtSubject.Text = H_SBJ_GRD_ID.Value.Split("___")(6)
    '            H_SBJ_GRD_ID.Value = SBG_ID
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub imgReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_RPT_ID.Value <> "" AndAlso H_RPT_ID.Value.Contains("___") Then
                Dim RPT_ID As String = H_RPT_ID.Value.Split("___")(0)
                txtReportId.Text = H_RPT_ID.Value.Split("___")(3)
                H_RPT_ID.Value = RPT_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub imgSbjGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        If H_SBJ_ID.Value <> "" AndAlso H_SBJ_ID.Value.Contains("___") Then
    '            Dim SBGRGD_ID As String = H_SBJ_ID.Value.Split("___")(0)
    '            txtGrdSubject.Text = H_SBJ_ID.Value.Split("___")(3)
    '            H_SBJ_ID.Value = SBGRGD_ID
    '            ReportFunctions.PopulateSubjectLevel(ddlLevel, SBGRGD_ID)
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    

#End Region

#Region " Button event Handlings"






#End Region



    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            BindStudents()

        Catch ex As Exception

        End Try
    End Sub


    Sub FillAcd()

        ddlAcdID.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcdID.DataTextField = "ACY_DESCR"
        ddlAcdID.DataValueField = "ACD_ID"
        ddlAcdID.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcdID.Items(ddlAcdID.Items.IndexOf(li)).Selected = True
        H_ACD_ID.Value = ddlAcdID.SelectedItem.Value
        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub



    'Sub GetComments()
    '    Dim i As Integer
    '    Dim txtComments As New TextBox
    '    Dim lblStudId As New Label
    '    Dim strSql As String
    '    Dim ds As DataSet
    '    Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Try
    '        If gvStudents.Rows.Count > 0 Then
    '            For i = 0 To gvStudents.Rows.Count - 1
    '                txtComments = TryCast(gvStudents.Rows(i).FindControl("txtComments"), TextBox)
    '                lblStudId = TryCast(gvStudents.Rows(i).FindControl("lblStudId"), Label)
    '                If Not lblStudId Is Nothing Then
    '                    strSql = "SELECT RST_COMMENTS FROM RPT.REPORT_STUDENT_S WHERE " _
    '                            & " RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
    '                            & " RST_GRD_ID='" & H_GRD_ID.Value & "' AND " _
    '                            & " RST_STU_ID='" & lblStudId.Text & "' AND " _
    '                            & " RST_RSD_ID='" & H_RPT_ID.Value & "'"
    '                    ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
    '                    If ds.Tables(0).Rows.Count > 0 Then
    '                        txtComments.Text = IIf(IsDBNull(ds.Tables(0).Rows(i)("RST_COMMENTS")), "", ds.Tables(0).Rows(i)("RST_COMMENTS"))
    '                    End If
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    

    Protected Sub ImgRepHeader_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SETUP.Value <> "" AndAlso H_SETUP.Value.Contains("___") Then
                Dim SETUP_ID As String = H_SETUP.Value.Split("___")(0)
                txtRepHeader.Text = H_SETUP.Value.Split("___")(6)
                H_SETUP.Value = SETUP_ID
                'H_RPF_ID.Value = H_SCH_ID.Value.Split("___")(3) 'ReportFunctions.GetReportPrintedFor(H_SCH_ID.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub

    

    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If
    End Sub

    Protected Sub btnSearchStud_No_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()

    End Sub

    Protected Sub btnSearchStud_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()
    End Sub
    Private Function SetCommentControls() As String
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim PnlComment As New Panel
        Dim lblComments As New Label
        Dim txtCmntsShort As New TextBox
        Dim strClass As String
        Try
            If gvStudents.Rows.Count > 0 Then
                strSql = "SELECT RSD_CSSCLASS FROM RPT.REPORT_SETUP_D WHERE RSD_ID='" & H_SETUP.Value & "'"
                ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    strClass = ds.Tables(0).Rows(0)("RSD_CSSCLASS")


                    If strClass = "textboxsmall" Then
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            PnlComment.Visible = False
                            lblComments.Visible = False
                            txtCmntsShort.Visible = True
                        Next
                    ElseIf strClass = "Null" Or strClass = "textboxmulti" Then
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            PnlComment.Visible = True
                            lblComments.Visible = True
                            txtCmntsShort.Visible = False
                        Next
                    Else
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            PnlComment.Visible = False
                            lblComments.Visible = False
                            txtCmntsShort.Visible = True
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function
End Class
