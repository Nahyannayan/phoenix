﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectCategory.aspx.vb" Inherits="Curriculum_clmSubjectCategory" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Categories
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

 <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr>
            <td align="left" valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="error" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%" >                    
                    <tr>
                        <td align="left" width="15%">
                            <span class="field-label">Select Academic Year
                                </span> 
                        </td>                      
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="true">
                            </asp:DropDownList></td>

                        <td align="left" width="15%">
                            <span class="field-label">Select Grade</span> 
                        </td>
                    
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                        <td  align="left" width="15%">
                            <span class="field-label">Select Subject</span> 
                        </td>
                      
                        <td  align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>               
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="gvCategory" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                               EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" CssClass="table table-bordered table-row">
                                <RowStyle   Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCatId" runat="server" Text='<%# Bind("SBT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCategory" runat="server" Text='<%# Bind("SBT_DESCR") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Short Name">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtShortname" runat="server" Text='<%# Bind("SBT_SHORTNAME") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Display Order">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server"  Text='<%# Bind("SBT_ORDER") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle Wrap="False" />
                                <HeaderStyle   Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle  Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
 
                </div>
            </div>
        </div>

</asp:Content>

