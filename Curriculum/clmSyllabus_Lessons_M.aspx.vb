Imports System.Data.SqlClient
Imports CURRICULUM
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports system
Imports ActivityFunctions

Partial Class Curriculum_clmSyllabus_Lessons_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            

             If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100029") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'ddlAcademicYear = ActivityFunctions.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    If ViewState("datamode") = "add" Then

                        ddlacdyearbind()
                        ClearControls()

                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gvLesson.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Sub ddlacdyearbind()
        ActivityFunctions.PopulateAcademicYear(ddlacadamicyear, Session("CLM"), Session("sBsuid"))
        h_ACD_ID.Value = ddlacadamicyear.SelectedValue
        ddltermbind()
        ddlgradebind()
        ddlsubjectbind()
        ddlgroupbind()
        BindLessonSetup()
    End Sub

    Sub ddltermbind()
        Dim reader As SqlDataReader = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlacadamicyear.SelectedValue)
        ddlterm.DataSource = reader
        ddlterm.DataTextField = "TRM_DESCRIPTION"
        ddlterm.DataValueField = "TRM_ID"
        ddlterm.DataBind()
        If Not ddlterm.SelectedIndex = -1 Then
            h_TRM_ID.Value = ddlterm.SelectedValue
        End If
    End Sub

    Sub ddlgradebind()
        ddlgrade = PopulateGrade(ddlgrade, ddlacadamicyear.SelectedValue.ToString)
        If Not ddlgrade.SelectedIndex = -1 Then
            h_GRD_ID.Value = ddlgrade.SelectedItem.Value
        End If
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
     
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + acdid
                str_query += " order by grd_displayorder"
            Else

                str_query = "SELECT DISTINCT " & _
"CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY + '-' + STM_DESCR END AS GRM_DISPLAY,   " & _
" GRADE_BSU_M.GRM_GRD_ID + '|' + CONVERT(VARCHAR(100), STREAM_M.STM_ID) AS GRM_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
" STREAM_M.STM_ID FROM GROUPS_TEACHER_S INNER JOIN " & _
" GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID  " & _
" AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
"GRADE_BSU_M INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
" STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID ON GROUPS_M.SGR_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
" GROUPS_M.SGR_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND GROUPS_M.SGR_STM_ID = GRADE_BSU_M.GRM_STM_ID AND " & _
" GROUPS_M.SGR_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & " WHERE " _
      & "  grm_acd_id=" + ddlacadamicyear.SelectedValue.ToString & " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId")

                str_query += " order by grd_displayorder"

            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub ddlgroupbind()
        Dim currFns As New currFunctions
        Dim grade As String()
        grade = ddlgrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            ddlgroup = PopulateGroups(ddlgroup, ddlacadamicyear.SelectedValue.ToString, grade(0), ddlsubject.SelectedValue.ToString)
        Else
            ddlgroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlgroup, ddlacadamicyear.SelectedValue.ToString, grade(0), ddlsubject.SelectedValue.ToString)
        End If
        h_GRP_ID.Value = ddlgroup.SelectedValue.ToString
    End Sub


    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")

        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + acd_id

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function


    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Sub ddlsubjectbind()
        If Session("CurrSuperUser") = "Y" Then
            ddlsubject = PopulateSubjects(ddlsubject, ddlacadamicyear.SelectedValue.ToString)

        Else
            ddlsubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlsubject, ddlacadamicyear.SelectedValue.ToString)
        End If
        
        If Not ddlsubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlsubject.SelectedItem.Value
        End If
    End Sub

    

    Protected Sub ddlacadamicyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearPartialDetails()
        h_ACD_ID.Value = 0
        If Not ddlacadamicyear.SelectedIndex = -1 Then
            h_ACD_ID.Value = ddlacadamicyear.SelectedValue
        End If
        ddltermbind()
        ddlgradebind()
        ddlsubjectbind()
        ddlgroupbind()
    End Sub

    Protected Sub ddlgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ClearPartialDetails()
        If Not ddlgrade.SelectedIndex = -1 Then
            h_GRD_ID.Value = ddlgrade.SelectedValue
        End If
        ddlsubjectbind()
        ddlgroupbind()
        BindLessonSetup()
        'ddlsyllabusbind()
    End Sub

    Protected Sub ddlsubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLessonSetup()
        ClearPartialDetails()
        If Not ddlsubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlsubject.SelectedValue
        End If
        ddlgroupbind()
    End Sub

    Protected Sub ddlterm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearPartialDetails()
        h_TRM_ID.Value = 0
        If Not ddlterm.SelectedIndex = -1 Then
            h_TRM_ID.Value = ddlterm.SelectedValue
        End If
        ddlgradebind()
        ddlsubjectbind()
        ddlgroupbind()
        'ddlsyllabusbind()
    End Sub

    

    Protected Sub ddlgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ClearPartialDetails()
        If Not ddlgroup.SelectedIndex = -1 Then
            h_GRP_ID.Value = ddlgroup.SelectedValue
        End If
    End Sub
 
  

    Protected Sub btnadddetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i, j As Integer
        If IsDate(txtFromDate.Text) = False Then
            lblError.Text = "Invalid FromDate"
            Exit Sub
        End If
        If IsDate(txtToDate.Text) = False Then
            lblError.Text = "Invalid ToDate"
            Exit Sub
        End If
        If Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
            lblError.Text = "FromDate must be less than ToDate"
            Exit Sub
        End If
        If txtempname.Text = "" Then
            lblError.Text = "Select Employee"
            Exit Sub
        End If
        If ViewState("datamode") = "add" Then
            If CheckTopicExists() = True Then
                lblError.Text = "Topic already allocated"
                Exit Sub
            End If
            Dim ldrNew As DataRow
            Session("gDtlDataMode") = "Add"
            If Session("gDtlDataMode") = "Add" Then
                ldrNew = Session("dtLessonTeac").NewRow
                Session("gintGridLine") = Session("gintGridLine") + 1
                ldrNew("Id") = Session("gintGridLine")
                ldrNew("TOPICID") = h_TopicID.Value
                ldrNew("TOPICNAME") = txtTopic.Text
                ldrNew("EMPID") = h_EMP_ID.Value
                ldrNew("EMPNAME") = txtempname.Text
                ldrNew("GROUPID") = h_GRP_ID.Value
                ldrNew("LESFROMDT") = txtFromDate.Text
                ldrNew("LESTODT") = txtToDate.Text
                ldrNew("LESHRSTK") = txtHrs.Text
                ldrNew("LESOBJECTIVE") = txtobjective.Text
                ldrNew("LesSYTId") = 0
                Session("dtLessonTeac").Rows.Add(ldrNew)
            End If
        Else

            If ViewState("datamode") = "Edit" Then
                If Not Session("dtLessonTeac") Is Nothing Then
                    For i = 0 To Session("dtLessonTeac").Rows.Count - 1
                        If (Session("dtLessonTeac").Rows(i)("Id") = Session("gintGridLine")) Then

                            Session("dtLessonTeac").Rows(i)("TOPICID") = h_TopicID.Value
                            Session("dtLessonTeac").Rows(i)("EMPID") = h_EMP_ID.Value
                            Session("dtLessonTeac").Rows(i)("EMPNAME") = txtempname.Text
                            Session("dtLessonTeac").Rows(i)("GROUPID") = ddlgroup.SelectedValue
                            Session("dtLessonTeac").Rows(i)("LESFROMDT") = txtFromDate.Text
                            Session("dtLessonTeac").Rows(i)("LESTODT") = txtToDate.Text
                            Session("dtLessonTeac").Rows(i)("LESHRSTK") = txtHrs.Text
                            Session("dtLessonTeac").Rows(i)("LESOBJECTIVE") = txtobjective.Text
                            Session("dtLessonTeac").Rows(i)("TOPICNAME") = txtTopic.Text

                        End If
                    Next
                End If
            End If

            ' btnadddetails.Text = "Add"
        End If

    
        txtTopic.Text = ""
        txtobjective.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtHrs.Text = ""
        txtempname.Text = ""
    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearControls()

    End Sub
    Private Sub ClearControls()
        txtempname.Text = ""
        txtHrs.Text = ""
        txtobjective.Text = ""
        h_EMP_ID.Value = Session("EmployeeId")
        h_TopicID.Value = 0
        GetEmpName()
        txtTopic.Text = ""
        txtToDate.Text = ""
        txtFromDate.Text = ""
        Session("dtLessonTeac") = Nothing
        Session("gintGridLine") = 0
        h_SYT_ID.Value = 0
        h_Completed.Value = 0
        txtPlannedStDate.Text = ""
        txtPlannedEndDate.Text = ""


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Try
            Dim ckBxselect As New CheckBox
            Dim ckBxComplete As New CheckBox
            SaveSyllabus()

            '  ClearControls()
            GetLessonDetails()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Sub SaveSyllabus()
        Dim iReturnvalue As Integer
        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_M]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@SYT_ID", SqlDbType.Int)
            cmd.Parameters("@SYT_ID").Direction = ParameterDirection.InputOutput
            If h_SYT_ID.Value <> 0 Then
                cmd.Parameters("@SYT_ID").Value = Val(h_SYT_ID.Value).ToString
            Else
                cmd.Parameters("@SYT_ID").Value = 0
            End If
            cmd.Parameters.AddWithValue("@SYT_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@SYT_SYD_ID", h_TopicID.Value)
            cmd.Parameters.AddWithValue("@SYT_EMP_ID", h_EMP_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_SGR_ID", h_GRP_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_SBG_ID", h_SUBJ_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_STDT", txtFromDate.Text)
            cmd.Parameters.AddWithValue("@SYT_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@SYT_HRS", txtHrs.Text)
            cmd.Parameters.AddWithValue("@SYT_REMARKS", 0)
            cmd.Parameters.AddWithValue("@SYT_OBJECTIVE", txtobjective.Text)
            cmd.Parameters.AddWithValue("@SYL_DESCR", txtTopic.Text)
            cmd.Parameters.AddWithValue("@SYL_RESOURCE", "")
            cmd.Parameters.AddWithValue("@SYT_COMPLETED", h_Completed.Value)
            If ViewState("datamode") = "add" Then
                cmd.Parameters.AddWithValue("@bEdit", 0)
            Else
                cmd.Parameters.AddWithValue("@bEdit", 1)
            End If
            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            If h_SYT_ID.Value = 0 Then
                h_SYT_ID.Value = cmd.Parameters("@SYT_ID").Value
            End If
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                objConn.Close()
                Exit Sub
            End If

            Dim i As Integer
            Dim lblLesId As Label
            Dim txtComments As TextBox
            Dim lblSylId As Label
            For i = 0 To gvLesson.Rows.Count - 1
                lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
                txtComments = gvLesson.Rows(i).FindControl("txtComments")
                lblSylId = gvLesson.Rows(i).FindControl("lblSylId")

                cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_D]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@SYL_ID", Val(lblSylId.Text).ToString)

                cmd.Parameters.AddWithValue("@SYL_SYT_ID", h_SYT_ID.Value)
                cmd.Parameters.AddWithValue("@SYL_SYD_ID", h_TopicID.Value)
                cmd.Parameters.AddWithValue("@SYL_LES_ID", lblLesId.Text)
                cmd.Parameters.AddWithValue("@SYL_DESCR", txtComments.Text)
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = "Unexpected error"
                    objConn.Close()
                    Exit Sub
                End If
            Next
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Unexpected error"
            objConn.Close()
            Exit Sub
        End Try

        stTrans.Commit()
        lblError.Text = "Lesson Allocated IS Successfully Saved"
        objConn.Close()
    End Sub



    Protected Sub gvallocation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)

    End Sub
    Sub GetLessonDetails()
        Dim strSQL As String
           Dim ds As DataSet
        Dim dsSub As DataSet
        Dim IntCmplt As Integer = 0
        Dim i, j As Integer
        Dim ldrNew As DataRow
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        strSQL = "SELECT SYT_ID,SYT_BSU_ID,SYT_SYD_ID,SYT_EMP_ID,SYT_SGR_ID,SYT_SBG_ID,SYT_STDT," _
                & " SYT_ENDDT, SYT_HRS, SYT_REMARKS,isnull(SYT_OBJECTIVE,'') SYT_OBJECTIVE, SYT_bCOMPLETE" _
                & " FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_SGR_ID=" + ddlgroup.SelectedValue.ToString _
                & " AND SYT_SYD_ID=" + h_TopicID.Value + " AND SYT_EMP_ID=" + h_EMP_ID.Value

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            h_SYT_ID.Value = ds.Tables(0).Rows(0)("SYT_ID")
            txtFromDate.Text = Format(ds.Tables(0).Rows(0)("SYT_STDT"), "dd/MMM/yyyy")
            txtToDate.Text = Format(ds.Tables(0).Rows(0)("SYT_ENDDT"), "dd/MMM/yyyy")
            txtobjective.Text = ds.Tables(0).Rows(0)("SYT_OBJECTIVE")
            txtHrs.Text = ds.Tables(0).Rows(0)("SYT_HRS")
        End If

        strSQL = "SELECT SYL_ID,SYL_LES_ID,ISNULL(SYL_DESCR,'') SYL_DESCR FROM SYL.SYLLABUS_TEACHER_D WHERE SYL_SYT_ID=" + h_SYT_ID.Value
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        Dim lblLesId As Label
        Dim txtComments As TextBox
        Dim lblSylId As Label
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To gvLesson.Rows.Count - 1
                lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
                txtComments = gvLesson.Rows(i).FindControl("txtComments")
                lblSylId = gvLesson.Rows(i).FindControl("lblSylId")
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If lblLesId.Text = ds.Tables(0).Rows(j)("SYL_LES_ID") Then
                        txtComments.Text = ds.Tables(0).Rows(j)("SYL_DESCR")
                        lblSylId.Text = ds.Tables(0).Rows(j)("SYL_ID")
                    End If
                Next
            Next
        End If
    End Sub

    Sub BindLessonSetup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LES_ID,'&nbsp;'+LES_DESCRIPTION LES_DESCRIPTION FROM SYL.LESSONSETUP_M WHERE LES_SBG_ID='" + ddlsubject.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvLesson.DataSource = ds
        gvLesson.DataBind()
        If gvLesson.Rows.Count > 0 Then
            gvLesson.HeaderRow.Visible = False
        End If
    End Sub
    Protected Sub txtSyllabus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GetLessonDetails()
    End Sub

     Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

      Private Function CheckTopicExists() As Boolean
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        strSql = "SELECT SYT_ID FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_BSU_ID='" & Session("SBsuid") & "' and " _
                & " SYT_SYD_ID='" & h_TopicID.Value & "' and " _
                & " SYT_SGR_ID='" & h_GRP_ID.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            CheckTopicExists = True
        Else
            CheckTopicExists = False
        End If

    End Function
    Sub GetEmpName()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'strSql = "SELECT EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME AS EMPNAME FROM VW_EMPLOYEE_M " _
        '            & " WHERE EMP_BSU_ID='" & Session("SBsuid") & "' and EMP_ID='" & h_EMP_ID.Value & "'"

        strSql = "SELECT VW_EMPLOYEE_M.EMP_ID, VW_EMPLOYEE_M.EMP_FNAME + ' ' + VW_EMPLOYEE_M.EMP_MNAME + ' ' + VW_EMPLOYEE_M.EMP_LNAME as empname, " _
                    & " VW_EMPLOYEE_M.EMPNO  FROM GROUPS_TEACHER_S INNER JOIN  VW_EMPLOYEE_M ON GROUPS_TEACHER_S.SGS_EMP_ID = VW_EMPLOYEE_M.EMP_ID " _
                    & " WHERE GROUPS_TEACHER_S.SGS_SGR_ID='" & h_GRP_ID.Value & "' AND " _
                    & " VW_EMPLOYEE_M.EMP_BSU_ID='" & Session("SBsuid") & "' AND " _
                    & " VW_EMPLOYEE_M.EMP_ID='" & h_EMP_ID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtempname.Text = ds.Tables(0).Rows(0)("EMPNAME")
        End If
    End Sub

    Protected Sub txtTopic_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GetPlannedDates()
        If txtempname.Text <> "" Then
            GetLessonDetails()
        End If

    End Sub
    Sub GetPlannedDates()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        If txtTopic.Text <> "" And h_TopicID.Value <> "" Then
            strSql = "SELECT SYD_STDT,SYD_ENDDT,SYD_TOT_HRS,ISNULL(SYD_OBJECTIVE,'') SYD_OBJECTIVE FROM SYL.SYLLABUS_D WHERE SYD_ID='" & h_TopicID.Value & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPlannedStDate.Text = Format(ds.Tables(0).Rows(0)("SYD_STDT"), "dd/MMM/yyyy")
                txtPlannedEndDate.Text = Format(ds.Tables(0).Rows(0)("SYD_ENDDT"), "dd/MMM/yyyy")
                txtobjective.Text = ds.Tables(0).Rows(0)("SYD_OBJECTIVE")
            End If
        End If
    End Sub

    Protected Sub lnkSubTopics_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim url As String
            Dim lblSytId As New Label
            Dim strSytId As String

            lblSytId = TryCast(sender.FindControl("lblSYTId"), Label)
            If Not lblSytId Is Nothing Then
                strSytId = lblSytId.Text
                ViewState("datamode") = "add"
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                url = String.Format("~\Curriculum\clmSyllabus_ScheduleAllocate.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") & "&ParentId=" & Encr_decrData.Encrypt(h_TopicID.Value) & "&SyllabusId=" & Encr_decrData.Encrypt(h_SyllabusId.Value) & "&SytID=" & strSytId)
                Response.Redirect(url)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub ClearPartialDetails()
        h_GRP_ID.Value = 0
        h_SyllabusId.Value = 0
        h_TopicID.Value = 0
        txtSyllabus.Text = ""
        txtTopic.Text = ""
        txtPlannedStDate.Text = ""
        txtPlannedEndDate.Text = ""
       
    End Sub

    Protected Sub txtempname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempname.TextChanged
        GetLessonDetails()
    End Sub
End Class