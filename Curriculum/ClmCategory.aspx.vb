Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Partial Class Curriculum_ClmCategory
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            ViewState("datamode") = "add"
            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ViewState("datamode") = "add"
                ViewState("grade") = Request.QueryString("Grade").ToString

                'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                If ViewState("datamode") = "add" Then
                    H_CMT_ID.Value = 0
                Else
                End If
                'disable the control buttons based on the rights
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
            Response.Write("window.close();")
            Response.Write("} </script>")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Dim IntMode As Integer
        If (ViewState("datamode") = "add") Then
            IntMode = 0
            H_CMT_ID.Value = 0
        ElseIf (ViewState("datamode") = "edit") Then
            IntMode = 1
        Else
            IntMode = 2
        End If

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "CATEGORY_M", "CAT_ID", "CAT_ID", "CAT_ID=" + H_CMT_ID.Value.ToString)
                ElseIf ViewState("datamode") = "delete" Then
                    UtilityObj.InsertAuditdetails(transaction, "delete", "CATEGORY_M", "CAT_ID", "CAT_ID", "CAT_ID=" + H_CMT_ID.Value.ToString)
                End If
                If chkGrade.Checked = True Then
                    'ViewState("grade")
                    Dim str_query As String = "exec ACT.saveCATEGORY_M " + H_CMT_ID.Value + ",'" + txtCategory.Text + "','" + Session("sBsuid") + "','" + ViewState("grade") + "'," + IntMode.ToString + ""
                    H_CMT_ID.Value = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                Else
                    Dim str_query As String = "exec ACT.saveCATEGORY_M " + H_CMT_ID.Value + ",'" + txtCategory.Text + "','" + Session("sBsuid") + "',''," + IntMode.ToString + ""
                    H_CMT_ID.Value = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                End If

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CAT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Write("<script language='javascript'> {")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        Response.Write("window.close();")
        Response.Write("} </script>")
        Me.Dispose()
    End Sub
End Class
