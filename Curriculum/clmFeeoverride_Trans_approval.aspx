﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmFeeoverride_Trans_approval.aspx.vb" Inherits="Fees_Feeoverride_approval" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript"> 
         function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
          function test2(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
                
                function test3(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid3()%>").src = path;
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                }
                function test4(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid4()%>").src = path;
                document.getElementById("<%=h_selected_menu_4.ClientID %>").value=val+'__'+path;
                }
    </script>
    
    
    <table style="width:60%">
        <tr>
            <td colspan="2">                     
                     <asp:GridView ID="gvStud" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" PageSize="20" SkinID="GridViewView">
                          <Columns>                    
                    <asp:TemplateField HeaderText="Student No">
                        <HeaderTemplate>
                    <table>
                   <tr>
                    <td align="center">
                  <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label></td>
                  </tr>
                   <tr>
                   <td align="left" style="width: 196px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                  <td>
                  <div id="Div1" class="chromestyle">
                <ul>
                <li><a href="#" rel="dropmenu1"></a>
               <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" 
                        src="../Images/operations/like.gif" /><span
                style="font-size: 12pt; color: #0000ff; text-decoration: none"> </span></li>
                </ul>
                </div>
                </td>
                <td style="height: 12px">
                <asp:TextBox ID="txtStuNo" runat="server" Width="100px"></asp:TextBox></td>
                <td style="height: 12px" valign="middle">
                <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Top" 
                        ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click"/>
                </td>
                </tr>
                </table>
                </td>
               </tr>
               </table>
              
</HeaderTemplate>
<ItemTemplate>
               <asp:Label ID="lblStuNo" Width="70px" runat="server" 
                   Text='<%# Bind("Stu_No") %>'></asp:Label>
                   <asp:HiddenField ID="HF_stu_id" runat="server" value='<%# Bind("Stu_ID") %>'/>
                    <asp:HiddenField ID="HF_ROR_ID" runat="server" value='<%# Bind("ROR_ID") %>'/>
               
</ItemTemplate>

<ItemStyle Width="60px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
    <HeaderTemplate>
                <table style="width: 124px;">
                <tr>
                <td align="center" style="width: 196px; height: 18px;">
                <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label></td>
                </tr>
                 <tr>
                <td align="left" style="width: 196px;">
               <table border="0" cellpadding="0" cellspacing="0">
              <tr>
            <td style="height: 12px">
           <div id="Div2" class="chromestyle">
           <ul>
         <li><a href="#" rel="dropmenu2"></a>
        <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" 
                 src="../Images/operations/like.gif" /><span
        style="font-size: 12pt; color: #0000ff; text-decoration: none"> </span></li>
          </ul>
            </div>
            </td>
           <td style="height: 12px">
           <asp:TextBox ID="txtStuName" runat="server" Width="134px"></asp:TextBox></td>
           <td style="height: 12px" valign="middle">
            <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Top" 
                   ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click"/>
           </td>
           </tr>
           </table>
           </td>
           </tr>
            </table>
             
</HeaderTemplate>
<ItemTemplate>
            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
            
</ItemTemplate>

<ItemStyle Width="180px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Grade">
    <HeaderTemplate>
              <table><tr>
              <td align="center">
              <asp:Label ID="lblH12" runat="server"  Text="Grade"></asp:Label></td>
              </tr><tr><td >
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr><td ><div id="Div3" 
                      class="chromestyle">
              <ul><li><a href="#" rel="dropmenu3"></a>
              <img id="mnu_3_img" runat="server" align="middle" alt="Menu" border="0" 
                      src="../Images/operations/like.gif" />
              </li></ul></div></td>
              <td >
              <asp:TextBox ID="txtGrade" runat="server" Width="100px"></asp:TextBox></td> 
                  <td >
              <td  valign="middle">
              <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" 
                      ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" /></td>
              </tr></table></td></tr>
              </table>
              
</HeaderTemplate>
<ItemTemplate>
                 <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
             
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Section">
    <HeaderTemplate>
              <table><tr>
              <td align="center">
              <asp:Label ID="lblH123" runat="server"  Text="Section"></asp:Label></td>
              </tr><tr><td >
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr><td ><div id="Div4" 
                      class="chromestyle">
              <ul><li><a href="#" rel="dropmenu4"></a>
              <img id="mnu_4_img" runat="server" align="middle" alt="Menu" border="0" 
                      src="../Images/operations/like.gif" />
              </li></ul></div></td>
              <td >
              <asp:TextBox ID="txtSection" runat="server" Width="100px"></asp:TextBox></td> 
                  <td >
              <td  valign="middle">
              <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Top" 
                      ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" /></td>
              </tr></table></td></tr>
              </table>
              
</HeaderTemplate>
<ItemTemplate>
                 <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
             
</ItemTemplate>
</asp:TemplateField>




                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_select" runat="server" onclick="lnk_select_Click">Select</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>                    
</Columns>                           
                         <HeaderStyle Height="30px" />
                        
                     </asp:GridView>
                     
                          </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
                     
                                                <asp:Label ID="lblerror" runat="server" CssClass="error" 
                                                    EnableViewState="False"></asp:Label>
                     
                          <asp:Label ID="lblpopup" runat="server"></asp:Label>
                     
                          <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server" 
                                                  BackgroundCssClass="modalBackground"  CancelControlID="btncancel" PopupControlID="Panel1" TargetControlID="lblpopup">
                                                </ajaxToolkit:ModalPopupExtender>  
                                    <asp:Panel ID="Panel1" runat="server" Width="400px" BackColor="LightYellow" CssClass="modalPopup"  >
               
                                    <table style="width: 100%" class="BlueTable">
                                        <tr class="matters">
                                            <td class="subheader_img" colspan="3" >
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td align="left">
                                                            Approve/Reject</td>
                                                        <td align="right">
                                                            <asp:ImageButton ID="btncancel" runat="server" 
                                                                ImageUrl="~/Images/close_red1.png" Height="18px"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="matters">
                                            <td align="left">
                                                Student No</td>
                                            <td style="width:1px">
                                                :
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblStudentno" runat="server"></asp:Label>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr class="matters">
                                            <td align="left">
                                                Remarks</td>
                                            <td style="width:1px">
                                                :</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtRemarks" runat="server" ValidationGroup="usrname" 
                                                    Height="73px" TextMode="MultiLine" Width="249px" EnableTheming="False"></asp:TextBox>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                    ControlToValidate="txtRemarks" ErrorMessage="Remarks Required" 
                                                    ValidationGroup="gr"></asp:RequiredFieldValidator>
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                        <tr class="matters">
                                            <td align="center" colspan="3">
                                                <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" 
                                                    ValidationGroup="gr" Width="75px" />
                                                &nbsp;<asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" 
                                                    Width="75px" ValidationGroup="gr" />
                                                <br />
                                                <asp:HiddenField ID="HFROR_ID" runat="server" />
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                
                                
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="0" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                                   
   <div id="dropmenu1" class="dropmenudiv" style="left: 6px; width: 110px; top: 7px">
                                    <a href="javascript:test1('LI');">
                                        <img alt="Like" class="img_left" src="../Images/operations/like.gif" />
                                        Any Where</a> <a href="javascript:test1('NLI');">
                                            <img alt="Like" class="img_left" src="../Images/operations/notlike.gif" />
                                            Not In</a> <a href="javascript:test1('SW');">
                                                <img alt="Like" class="img_left" src="../Images/operations/startswith.gif" />
                                                Starts With</a> <a href="javascript:test1('NSW');">
                                                    <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />
                                                    Not Start With</a> <a href="javascript:test1('EW');">
                                                        <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />
                                                        Ends With</a> <a href="javascript:test1('NEW');">
                                                            <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />
                                                            Not Ends With</a>
                                </div>
                                
  <div id="dropmenu2" class="dropmenudiv" style="left: 153px; width: 110px; top: 1px">
  <a href="javascript:test2('LI');">
                                        <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />
                                        Any Where</a> <a href="javascript:test2('NLI');">
                                            <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />
                                            Not In</a> <a href="javascript:test2('SW');">
                                                <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />
                                                Starts With</a> <a href="javascript:test2('NSW');">
                                                    <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />
                                                    Not Start With</a> <a href="javascript:test2('EW');">
                                                        <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />
                                                        Ends With</a> <a href="javascript:test2('NEW');">
                                                            <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />
                                                            Not Ends With</a>
                                                            
                                                            </div>
                                                            
     <div id="dropmenu3" class="dropmenudiv" style="left: 450px; width: 110px; top: 0px">
                                        <a href="javascript:test3('LI');">
                                            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />
                                            Any Where</a> <a href="javascript:test3('NLI');">
                                                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />
                                                Not In</a> <a href="javascript:test3('SW');">
                                                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />
                                                    Starts With</a> <a href="javascript:test3('NSW');">
                                                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />
                                                        Not Start With</a> <a href="javascript:test3('EW');">
                                                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />
                                                            Ends With</a> <a href="javascript:test3('NEW');">
                                                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />
                                                                Not Ends With</a></div>
                                                                
  <div id="dropmenu4" class="dropmenudiv" style="left: 300px; width: 110px; top: 1px">
      <a href="javascript:test4('LI');">
       <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />
                                                Any Where</a> <a href="javascript:test4('NLI');">
                                                    <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />
                                                    Not In</a> <a href="javascript:test4('SW');">
                                                        <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />
                                                        Starts With</a> <a href="javascript:test4('NSW');">
                                                            <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />
                                                            Not Start With</a> <a href="javascript:test4('EW');">
                                                                <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />
                                                                Ends With</a> <a href="javascript:test4('NEW');">
                                                                    <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />
                                                                    Not Ends With</a>
                                        
                                    </div>
                               
<script type="text/javascript">
    cssdropdown.startchrome("Div1");
    cssdropdown.startchrome("Div2");
    cssdropdown.startchrome("Div3");
    cssdropdown.startchrome("Div4");
   </script>
</asp:Content>

