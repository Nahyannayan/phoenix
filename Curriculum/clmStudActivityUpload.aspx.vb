Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Imports ICSharpCode.SharpZipLib

Partial Class Curriculum_clmStudActivityUpload
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        ' ScriptManager.GetCurrent(Page).RegisterPostBackControl(mdlPopup)
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"

                'check for the usr_name and the menucode are valid otherwise redirect to login page


                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100250") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    GETPRE_DEFINEDSIZE()
                    btnCloseedit.Attributes.Add("onClick", "pagereload();")
                    ltNote.Text = "<div><b>Note:</b><i>Upload zip folder type extension <b>zip</b> with size less than 3MB.The zip folder must contain only image file type extension <b>jpg</b> with each file size less than 100KB </i></div>" ' and the image file name can be student no or Fee id and also maintain each image max width " & ViewState("IMG_WIDTH") & " and max height " & ViewState("IMG_HEIGHT") & " pixels.</i></div>"
                    Call bindAcademic_Year()

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
        gridbind()
    End Sub
    Sub GETPRE_DEFINEDSIZE()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim STR As String = " select FUM_IMG_WIDTH,FUM_IMG_HEIGHT from CURR.FILEUPLOAD_M where FUM_APPLIED_FOR='CURR' AND FUM_BSU_ID='" & Session("sBsuid") & "'"
        Using IMG_DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR)
            If IMG_DATAREADER.HasRows Then
                While IMG_DATAREADER.Read
                    ViewState("IMG_WIDTH") = Convert.ToString(IMG_DATAREADER("FUM_IMG_WIDTH"))
                    ViewState("IMG_HEIGHT") = Convert.ToString(IMG_DATAREADER("FUM_IMG_HEIGHT"))
                End While
            Else
                ViewState("IMG_WIDTH") = "380"
                ViewState("IMG_HEIGHT") = "510"
            End If
        End Using
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlAcdID.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdID.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()
            ddlAcdID.ClearSelection()
            ddlAcdID.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdID_SelectedIndexChanged(ddlAcdID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetGrades()
        Try
            Dim RPF_ID As String = String.Empty
            Dim RSM_ID As String = String.Empty
            Dim SCT_ID As String = String.Empty
            Dim bSuperUsr As Boolean = False
            If Session("CurrSuperUser") = "Y" Then
                bSuperUsr = True
            End If

            If ddlRptSchedule.SelectedIndex = -1 Then
                RPF_ID = "0"
                lblError.Text = "Report Schedule not selected !!!"
                Exit Sub
            Else
                RPF_ID = ddlRptSchedule.SelectedValue
            End If
            If ddlReportId.SelectedIndex = -1 Then
                RSM_ID = "0"
                lblError.Text = "Report type not selected !!!"
                Exit Sub
            Else
                RSM_ID = ddlReportId.SelectedValue
            End If

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String


            If bSuperUsr Then
                str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
                " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
                " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
                " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
                " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
                 " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcdID.SelectedValue & "') " & _
                 " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & RSM_ID & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & RPF_ID & "'" & _
                  " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            Else
                str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
                " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
                " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
                " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
                " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
                " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
               " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcdID.SelectedValue & "') " & _
                " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & RSM_ID & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & RPF_ID & "'" & _
                  " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EmployeeID") & _
                  " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            End If





            ddlGrade.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlGrade.Items.Add(New ListItem(reader("GRM_DISPLAY"), reader("GRD_ID")))
            End While
            reader.Close()
            Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            Session("Comm_Coll") = ddlAcdID.SelectedValue & "|" & ddlGrade.SelectedValue & "|" & ddlRptSchedule.SelectedValue

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindSection()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = "  SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_DESCR<>'TEMP' AND SCT_GRD_ID='" & ddlGrade.SelectedValue & "' AND SCT_ACD_ID='" & ddlAcdID.SelectedValue & "' ORDER BY SCT_DESCR "


            ddlsct_id.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlsct_id.Items.Add(New ListItem(reader("SCT_DESCR"), reader("SCT_ID")))
            End While
            reader.Close()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    

    Sub PopulateReports()
        ddlReportId.Items.Clear()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                       & "  AND RSM_ACD_ID=" & ddlAcdID.SelectedValue & " ORDER BY RSM_DISPLAYORDER"


            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlReportId.Items.Add(New ListItem(reader("RSM_DESCR"), reader("RSM_ID")))
            End While

            reader.Close()
            ddlReportId_SelectedIndexChanged(ddlReportId, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub PopulateReportSchedule()
        ddlRptSchedule.Items.Clear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


            Dim str_Sql As String = "SELECT RPF_ID ,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M "
            If ddlReportId.SelectedValue <> "" Then
                str_Sql += " WHERE RPF_RSM_ID=" & ddlReportId.SelectedValue & ""
            End If
            str_Sql += " ORDER BY RPF_DISPLAYORDER "


            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlRptSchedule.Items.Add(New ListItem(reader("RPF_DESCR"), reader("RPF_ID")))
            End While

            reader.Close()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdID.SelectedIndexChanged
        PopulateReports()

    End Sub

    Protected Sub ddlReportId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportId.SelectedIndexChanged
        PopulateReportSchedule()
        GetGrades()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        BindSection()
        gridbind()
    End Sub
    Protected Sub btnViewStud_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewStud.Click
        gridbind()
    End Sub
    Sub gridbind()
        Dim strCondition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strSql As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim SCT_ID As String = String.Empty
        If ddlRptSchedule.SelectedIndex = -1 Then
            RPF_ID = "0"
            lblError.Text = "Report Schedule not selected !!!"
            Exit Sub
        Else
            RPF_ID = ddlRptSchedule.SelectedValue
        End If

        If ddlsct_id.SelectedIndex = -1 Then
            SCT_ID = "0"
            lblError.Text = "Section not selected !!!"
            Exit Sub
        Else
            SCT_ID = ddlsct_id.SelectedValue
        End If

        Dim dsStudents As DataSet
        Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString

        Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"

        Dim fpath As String = str_imgvirtual & Session("Folder_Name")
        Session("Comm_Coll") = ddlAcdID.SelectedValue & "|" & ddlGrade.SelectedValue & "|" & ddlRptSchedule.SelectedValue

        strSql = " SELECT  ROW_NUMBER() OVER (ORDER BY SNAME) AS R1,STU_NO,STU_ID,SNAME,SCT_DESCR, SFU_FILEPATH FROM " & _
" (SELECT DISTINCT      S.STU_NO, S.STU_ID, S.STU_NAME AS  SNAME, S.SCT_DESCR, CASE WHEN ISNULL(U.SFU_FILEPATH, '') " & _
 " = '' THEN '~/Images/Photos/no_image1.gif'  ELSE '" & fpath & "' + U.SFU_FILEPATH +'?'+  '" & DateTime.Now.Ticks.ToString() & "'  END AS SFU_FILEPATH FROM  vw_STUDENT_DETAILS AS S LEFT OUTER JOIN " & _
 " CURR.STUDENT_FILEUPLOAD AS U ON S.STU_ID=U.SFU_STU_ID AND  U.SFU_RPF_ID = '" & RPF_ID & "' WHERE  (S.STU_SCT_ID='" & SCT_ID & "') AND (S.STU_GRD_ID = '" & ddlGrade.SelectedValue & "') AND (S.STU_ACD_ID = '" & ddlAcdID.SelectedValue & "') AND S.STU_CURRSTATUS <> 'CN'  AND (convert(datetime,S.STU_LEAVEDATE) >= convert(datetime,GETDATE()) " & _
" OR convert(datetime,S.STU_LEAVEDATE) is null) AND S.STU_DOJ <= convert(datetime,GETDATE()) " & _
" )A ORDER BY SCT_DESCR,SNAME  "
        dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If dsStudents.Tables(0).Rows.Count > 0 Then
            gvInfo.DataSource = dsStudents.Tables(0)
            gvInfo.DataBind()
        Else
            dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
            'start the count from 1 no matter gridcolumn is visible or not
            dsStudents.Tables(0).Rows(0)(4) = True
            gvInfo.DataSource = dsStudents.Tables(0)
            Try
                gvInfo.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
            gvInfo.Rows(0).Cells.Clear()
            gvInfo.Rows(0).Cells.Add(New TableCell)
            gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
            gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvInfo.Rows(0).Cells(0).Text = "Not record available."
        End If
        deleteOld_dir()
    End Sub
    Sub deleteOld_dir()
        Try


            Dim str_phyPath As String = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")
            'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa
            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            If Directory.Exists(UploadPath) Then
                Dim Dirlist As String() = Directory.GetDirectories(UploadPath)
                For Each vdir As String In Dirlist
                    Dim dt As DateTime = DateAdd(DateInterval.Hour, 1, File.GetCreationTime(vdir))
                    If dt < Now() Then
                        Directory.Delete(vdir, True)
                    End If
                Next



            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub imgThumb_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblSname As New Label
        Dim imgthumb As New ImageButton
        imgthumb = sender
           lblSname = TryCast(sender.FindControl("lblSname"), Label)

        ltHeader.Text = lblSname.Text
        imgLarge.ImageUrl = imgthumb.ImageUrl
        mdlPopup.Show()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

   

    Protected Sub imgbtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim acd_year As String = ddlAcdID.SelectedItem.Text
        Dim grd As String = ddlGrade.SelectedItem.Text
        Dim Report As String = ddlReportId.SelectedItem.Text
        Dim reportSchedule As String = ddlRptSchedule.SelectedItem.Text
        Dim imgthumb As New ImageButton
        Dim lblstu_id As New Label
        Dim lblSname As New Label
        Dim lblSCT_DESCR As New Label

        imgthumb = TryCast(sender.FindControl("imgThumb"), ImageButton)

        lblstu_id = TryCast(sender.FindControl("lblstu_id"), Label)
        lblSname = TryCast(sender.FindControl("lblSname"), Label)
        lblSCT_DESCR = TryCast(sender.FindControl("lblSCT_DESCR"), Label)
        ltName.Text = lblSname.Text

        Session("Upload_display_data") = lblSname.Text & "|" & acd_year & "|" & grd & " & " & lblSCT_DESCR.Text & "|" & Report & "|" & reportSchedule
        Session("Upload_data") = lblstu_id.Text & "|" & ddlRptSchedule.SelectedValue & "|" & ddlAcdID.SelectedValue

        mdlPopUpEdit.Show()


    End Sub
    
    Protected Sub btnClose_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mdlPopup.Hide()
        'gridbind()
    End Sub
    Protected Sub btnCloseedit_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        mdlPopUpEdit.Hide()
        ' gridbind()
        ' ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> window.location.href=window.location.href</script>", False)
    End Sub
    'Sub ListZipContent(ByVal sFile As String)
    '    Dim zip As New ZipFile(File.OpenRead(sFile))
    '    Dim str1 As String = String.Empty
    '    For Each entry As ZipEntry In zip
    '        str1 = str1 + entry.Name + ","
    '    Next
    '    lblError.Text = str1
    'End Sub


    'Sub UncompressZip(ByVal sFile As String)
    '    Dim zipin As New ZipInputStream(File.OpenRead(sFile))

    '    Dim entry As ZipEntry
    '    For Each entry In zipin.GetNextEntry()


    '    Next
    '    While (entry = zipin.GetNextEntry())
    '        Dim streamWriter As FileStream = File.Create("C:\Temp\" & entry.Name)
    '        Dim size As Long = entry.Size
    '        Dim data As Byte() = New Byte(size - 1) {}
    '        While (True)
    '            size = zipin.Read(data, 0, data.Length)
    '            If size > 0 Then
    '                streamWriter.Write(data, 0, CInt(size))
    '            Else
    '                Exit While
    '            End If
    '        End While
    '        streamWriter.Close()
    '    End While

    'End Sub

    ''    private static void UncompressZip(string sFile)
    ''{
    ''    ZipInputStream zipIn = new ZipInputStream(File.OpenRead(sFile));
    ''    ZipEntry entry;
    ''    while ((entry = zipIn.GetNextEntry()) != null)
    ''    {
    '        FileStream streamWriter = File.Create(@"C:\Temp\" + entry.Name);
    '        long size = entry.Size;
    '        byte[] data = new byte[size];
    '        while (true)
    '        {
    '            size = zipIn.Read(data, 0, data.Length);
    '            if (size > 0) streamWriter.Write(data, 0, (int) size);
    '            else break;
    '        }
    '        streamWriter.Close();
    ''    }
    ''    Console.WriteLine("Done!!");
    '}  

    '    ZipFile zip = new ZipFile(ZipPath); 
    '//List compressed files 
    'foreach(ZipEntry compfile in zip) { 
    'if (compfile.IsFile) { 
    '//call compfile.Name to ge the file name Notice it includes relative path 
    '} 

    'Protected Sub gvInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInfo.SelectedIndexChanged
    '    Dim lblSname As New Label

    '    Dim gvInfo As GridView = CType(sender, GridView)
    '    Dim row As GridViewRow = gvInfo.SelectedRow


    '    ltHeader.Text = row.Cells(2).Text
    'End Sub

    Protected Sub ddlsct_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlsct_id.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub ddlRptSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRptSchedule.SelectedIndexChanged
        gridbind()
    End Sub
End Class
