﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabusLessonSetup_M.aspx.vb" Inherits="Curriculum_clmSyllabusLessonSetup_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Lesson Setup Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">AcademicYear</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Grade</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" 
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Subject</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server" 
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>

                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" style="text-align: center">
                                        <asp:GridView ID="gvLesson" runat="server" AllowPaging="false" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. 
                                 Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20"  >
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLesId" runat="server" Text='<%# Bind("LES_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Criteria" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCriteria" runat="server"  Width="100%"  Text='<%# Bind("LES_DESCRIPTION") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Comments" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtComments" TextMode="multiLine" Width="100%" SkinID="MultiText" runat="server"  Text='<%# Bind("LES_COMMENTS") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Display Order" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder" runat="server"   Text='<%# Bind("LES_ORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

