<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmFeeOverideReq_M.aspx.vb" Inherits="Curriculum_clmFeeOverideReq_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Request for Fee Override
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" />
                &nbsp;
            </td>
        </tr>
        <tr valign="bottom">
            <td align="left" valign="bottom" style="height: 5px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                      
                    
                    <tr>
                            <td align="left">
                                <span class="field-label">Student ID</span></td>
              
                        <td align="left">
                         <asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                        
                       
                 <td align="left">
                         <span class="field-label">Student Name</span></td>
                    
                 <td align="left">
                         <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                        
                       
            </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Academic Year</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" 
                             Width="100px">
                            </asp:DropDownList></td>

                        <td align="left">
                                <span class="field-label">Select Report Card</span></td>
                 <td align="left">
                         <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                         </asp:DropDownList>
                            </td>
                    </tr>
                  
                    
                    <tr>
                 <td align="left">
                                <span class="field-label">Select Report Schedule</span></td>
                     
                 <td align="left" colspan="2">
                         <asp:DropDownList ID="ddlReportSchedule" runat="server">
                         </asp:DropDownList>
                            </td>        
                   </tr>
                  
                    
                    <tr>
                        <td align="left">
                           <span class="field-label"> Remarks</span></td>
                        
                        <td align="left" colspan="2">
                      <asp:TextBox id="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                      </td>        
                  
                       </tr>
                  
                   
                                        </table>
               </td>
        </tr>
       
        <tr>
            <td align="center">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" 
                    CausesValidation="False" />
                </td>
        </tr>
        <tr>
            <td  valign="bottom">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"
                    Width="23px"></asp:RequiredFieldValidator>&nbsp;
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" /><asp:HiddenField ID="hfMode" runat="server" />
                &nbsp;&nbsp;
           </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>

