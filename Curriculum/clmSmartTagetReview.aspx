﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSmartTagetReview.aspx.vb" Inherits="Curriculum_clmSmartTagetReview" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>SMART Target Review
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">

                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">
                                <tr id="trAcd1" runat="server">
                                    <td align="left">
                                        <span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Set Up</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSetup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left">
                                        <span class="field-label">Grade</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Group</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlgroup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="Tr1" runat="server">
                                    <td id="Td1" align="center" runat="server" colspan="12">
                                        <asp:GridView ID="grdStud" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            Width="100%" AllowPaging="true" PageSize="20" OnPageIndexChanging="grdStud_PageIndexChanging">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="atd" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AtdId" runat="server" Text='<%# bind("TSD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Width="200px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmark" runat="server" Text='<%# Bind("TSD_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cb1" runat="server" Text='<%# Bind("Approved") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Review">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cb2" runat="server" Text='<%# Bind("Review") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>


                            </table>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

