﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Imports ICSharpCode.SharpZipLib
Partial Class Students_Stud_upload_frame
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Private Sub UpLoadPhoto()

        If fileload.FileName <> "" Then
            lblErrorLoad.Text = ""
            If Not fileload.PostedFile.ContentType.Contains("image") Then
                ''Throw New Exception
                lblErrorLoad.Text = "Select Image Only"
                Exit Sub
            End If

            If fileload.HasFile Then


            End If
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim DirectoryName As String = ""
            If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\Temp\") Then
                Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\Temp\")
            Else
                Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\Temp\")

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fi
                        f.Delete()
                    Next
                End If
            End If

            Dim str_tempfilename As String = fileload.FileName
            Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\Temp\" & "STUPHOTO" & fs.Extension
            'ImgHeightnWidth(strFilepath)
            fileload.PostedFile.SaveAs(strFilepath)
            Try
                If Not fileload.PostedFile.ContentType.Contains("image") Then
                    Throw New Exception
                End If

                ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\Temp\" & "STUPHOTO" & fs.Extension
                ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
            Catch ex As Exception
                File.Delete(strFilepath)

                UtilityObj.Errorlog("No Image found")

            End Try
        End If
    End Sub
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
            Dim DirName As String = "fPath_" & System.Guid.NewGuid().ToString() 'create the unique folder fpath_f2131dsa
            Dim strPostedFileName As String = fs.Name 'get the file name

            If intDocFileLength > 4096000 Then '4MB
                lblErrorLoad.Text = "Image file size exceeds the limit of 4Mb."
                Exit Sub
            End If

            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".zip") Then ' if zip file processed

                    Dim savePath As String = String.Empty
                    savePath = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString & "\" & DirName
                    'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa

                    If Not Directory.Exists(savePath) Then

                        fileload.PostedFile.SaveAs(savePath & System.IO.Path.GetFileName(strPostedFileName))
                        Dim extr As New Zip.FastZip
                        extr.ExtractZip(savePath & System.IO.Path.GetFileName(strPostedFileName), savePath, String.Empty)
                        createStudent_file(savePath)
                        File.Delete(savePath & System.IO.Path.GetFileName(strPostedFileName))
                        Directory.Delete(savePath, True)

                        ' hf_reload.Value = "1"
                        ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                        lblErrorLoad.Text = "File uploaded successfully..."
                    End If

                Else
                    lblErrorLoad.Text = "Please upload a valid Document with the extenion in : .zip"

                End If
            Else
                lblErrorLoad.Text = "There is no file to Upload."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub
    Private Sub createStudent_file(ByVal savePath As String)
        Dim filepath As String = Session("Folder_Name")
        ' Dim strFilter As String = "*.jpg"
        ' Dim formatarray As String() = {"*.jpg"} 'strFilter.Split(";")
        Dim formatarray As String() = {"*.jpg"}
        ' formatarray(0) = "*.jpg"
        Dim str_output As New StringWriter
        For Each FileFormat As String In formatarray
            Dim filelist As String() = Directory.GetFiles(savePath, FileFormat, SearchOption.AllDirectories)
            For Each File As String In filelist
                Dim filename As New FileInfo(File.ToString())
                Dim fee_id As String = GetValid_FeeId(filename.Name.Split(".")(0))
                Dim WriteStatus As String = String.Empty


                If fee_id <> "" Then
                    WriteStatus = CREATEFILE(filename.FullName, fee_id, filename.Name.Split(".")(1))

                    If WriteStatus <> "" Then
                        str_output.WriteLine(WriteStatus)
                    End If

                Else
                    str_output.WriteLine("Student record doesn't exist with " & filename.Name)

                End If

            Next
        Next

        lblErrorLoad.Text = Server.HtmlEncode(str_output.ToString).Replace(Environment.NewLine, "<br/>")

    End Sub
    Private Function GetValid_FeeId(ByVal filename As String) As String
        Try
            Dim temp As String = Session("Comm_Coll") 'acd_id|GRD_ID| RPF_ID
            Dim GRD_ID As String = temp.Split("|")(1)
            Dim fee_id As String = String.Empty
            Dim sql_Connection As String = ConnectionManger.GetOASISConnectionString
            Dim Sql_Query As String = "SELECT TOP 1 STU_FEE_ID FROM STUDENT_M WHERE(STU_GRD_ID='" & GRD_ID & "') AND (STU_FEE_ID='" & filename.Trim & "' OR STU_NO ='" & filename.Trim & "') AND  STU_BSU_ID='" & Session("sBsuid") & "'"
            fee_id = CStr(SqlHelper.ExecuteScalar(sql_Connection, CommandType.Text, Sql_Query))

            Return fee_id
        Catch ex As Exception

            Return ""
        End Try
    End Function

    Private Function CREATEFILE(ByVal OldFile As String, ByVal NewFile As String, ByVal extFile As String) As String
        Try
            Dim status As String = ""

            Dim str_phyPath As String = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString

            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")
            Dim DatabasePath As String = Session("Folder_Name")
            If Directory.Exists(UploadPath) Then

                File.Copy(OldFile, UploadPath & NewFile & "." & extFile, True)
                status = Update_database(NewFile & "." & extFile, NewFile)
            Else
                Directory.CreateDirectory(UploadPath)
                File.Copy(OldFile, UploadPath & NewFile & "." & extFile, True)
                status = Update_database(NewFile & "." & extFile, NewFile)
            End If
            If status = "0" Then
                Return ""
            Else
                Return "Error while saving " & OldFile & " to the destination folder"
            End If

        Catch ex As Exception
            Return "Error while saving " & OldFile & " to the destination folder"
        End Try
    End Function

    Private Function Update_database(ByVal file As String, ByVal fee_id As String) As String
        Try
            Dim temp As String = Session("Comm_Coll") 'acd_id|GRD_ID| RPF_ID
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim pParms(9) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", temp.Split("|")(0))
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
            pParms(2) = New SqlClient.SqlParameter("@GRD_ID", temp.Split("|")(1))
            pParms(3) = New SqlClient.SqlParameter("@RPF_ID", temp.Split("|")(2))
            pParms(4) = New SqlClient.SqlParameter("@FEE_ID", fee_id)
            pParms(5) = New SqlClient.SqlParameter("@FILEPATH", file)
            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVESTUDENT_FILEUPLOAD", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag

        Catch ex As Exception
            Return -1
        End Try
       
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    End Sub
End Class
'machine.config.comments file for maxRequestLength of httpRuntime tag.  This number is in KB. 

'<httpRuntime
'executionTimeout = "110" [in Seconds][number
'maxRequestLength = "4096" [number]
'requestLengthDiskThreshold = "80" [number]
'useFullyQualifiedRedirectUrl = "false" [true|false]
'minFreeThreads = "8" [number]
'minLocalRequestFreeThreads = "4" [number]
'appRequestQueueLimit = "5000" [number]
'enableKernelOutputCache = "true" [true|false]
'enableVersionHeader = "true" [true|false]
'apartmentThreading = "false" [true|false]
'requireRootedSaveAsPath = "true" [true|false]
'enable = "true" [true|false]
'sendCacheControlHeader = "true" [true|false]
'shutdownTimeout = "90" [in Seconds][number]
'delayNotificationTimeout = "5" [in Seconds][number]
'waitChangeNotification = "0" [number]
'maxWaitChangeNotification = "0" [number]
'enableHeaderChecking = "true" [true|false]
'/>