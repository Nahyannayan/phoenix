<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmDefaultBank_View.aspx.vb" Inherits="Curriculum_clmDefaultBank_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  Set Default Values
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
            
                <tr >    <td align="left" colspan="4">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                </tr>
                <tr>
                <td align="center" colspan="4">

                <table id="Table1"  runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                        <td align="left" class="matters" colspan="3">
                            <asp:LinkButton id="lnkAddNew" runat="server">Add New</asp:LinkButton></td>
                    </tr>
                <tr><td  colspan="3" align="center" class="matters" >
                <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                <tr><td colspan="3" align="center" >

                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                 PageSize="20" Width="100%"  BorderStyle="None">
                <RowStyle   CssClass="griditem"  />
                <EmptyDataRowStyle  />
                <Columns>
                
                <asp:TemplateField HeaderText="sbm_id" Visible="False"><ItemTemplate>
                <asp:Label ID="lblRdmId" runat="server" Text='<%# Bind("RDM_ID") %>'></asp:Label>
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=""><ItemTemplate>
                <asp:Label ID="lblDefault" runat="server" text='<%# Bind("RDM_DESCR") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

            <asp:ButtonField CommandName="view" Text="View" HeaderText="View">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                            </asp:ButtonField>


                </Columns>  
                <SelectedRowStyle  />
                <HeaderStyle  />
                <EditRowStyle />
                <AlternatingRowStyle   CssClass="griditem_alternative"/>
                </asp:GridView>
                </td></tr>
                </table>
<input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
runat="server" type="hidden" value="=" />

</td>
</tr>
</table>
    <asp:HiddenField id="hfRSM_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="hfACD_ID" runat="server"></asp:HiddenField>
</td></tr>

</table>

            </div>
        </div>
    </div>


</asp:Content>

