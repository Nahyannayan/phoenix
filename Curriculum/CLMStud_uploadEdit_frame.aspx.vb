﻿Imports System
Imports System.Web
Imports System.Web.UI
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Curriculum_CLMStud_uploadEdit_frame
    Inherits BasePage
    Protected Sub btnCrop_Click(ByVal sender As Object, ByVal e As EventArgs)
        If ViewState("ImgPath").trim <> "~/Images/Photos/no_image1.gif" Then
            lblCroppedImage.Text = ""
            Dim X1 As Integer = Convert.ToInt32(Request.Form("x1"))
            Dim Y1 As Integer = Convert.ToInt32(Request("y1"))
            Dim X2 As Integer = Convert.ToInt32(Request.Form("x2"))
            Dim Y2 As Integer = Convert.ToInt32(Request.Form("y2"))
            Dim X As Integer = System.Math.Min(X1, X2)
            Dim Y As Integer = System.Math.Min(Y1, Y2)
            Dim w As Integer = Convert.ToInt32(Request.Form("w"))
            Dim h As Integer = Convert.ToInt32(Request.Form("h"))
            ' That can be any image type (jpg,jpeg,png,gif) from any where in the local server
            Dim str_PhyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim str_imgvirtual As String = ConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString
            Dim subFilePath As String = Session("Folder_Name")
            Dim fpath As String = str_PhyPath & subFilePath & ViewState("ImgPath")
            Dim tempPath As String = "CroppedPath_" & System.Guid.NewGuid().ToString()

            ViewState("DEL_PATH") = str_PhyPath & subFilePath & tempPath

            Dim originalFile As String = str_PhyPath & subFilePath & ViewState("ImgPath") ' "C:\inetpub\wwwroot\CURR\125017\PK_10_50\71104.jpg" 'Server.MapPath(fpath)
            Using img As Image = Image.FromFile(originalFile)
                Using _bitmap As New System.Drawing.Bitmap(w, h)
                    _bitmap.SetResolution(img.HorizontalResolution, img.VerticalResolution)
                    Using _graphic As Graphics = Graphics.FromImage(_bitmap)
                        _graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
                        _graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality
                        _graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality
                        _graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality
                        _graphic.DrawImage(img, 0, 0, w, h)
                        _graphic.DrawImage(img, New Rectangle(0, 0, w, h), X, Y, w, h, _
                        GraphicsUnit.Pixel)

                        Dim extension As String = Path.GetExtension(originalFile)
                        Dim croppedFileName As String = ViewState("FEE_ID") 'Guid.NewGuid().ToString()
                        'Dim strPath As String = "C:/inetpub/wwwroot/Curr/" & croppedFileName + extension
                        Dim strPath As String = str_imgvirtual & subFilePath & "/" & tempPath & "/" & croppedFileName + extension
                        'Dim path__1 As String = Server.MapPath("~/Curriculum/cropped/")
                        Dim path__1 As String = str_PhyPath & subFilePath & "/" & tempPath & "/"

                        ' If the image is a gif file, change it into png
                        If extension.EndsWith("gif", StringComparison.OrdinalIgnoreCase) Then
                            extension = ".png"
                        End If

                        Dim newFullPathName As String = String.Concat(path__1, croppedFileName, extension)
                        ViewState("oldPath") = newFullPathName
                        ViewState("Extension") = extension
                        Using encoderParameters As New EncoderParameters(1)
                            encoderParameters.Param(0) = New EncoderParameter(Encoder.Quality, 100L)

                            Dim UploadPath As String = str_PhyPath & subFilePath & tempPath

                            If Directory.Exists(UploadPath) Then

                                _bitmap.Save(newFullPathName, GetImageCodec(extension), encoderParameters)
                            Else
                                Directory.CreateDirectory(UploadPath)
                                _bitmap.Save(newFullPathName, GetImageCodec(extension), encoderParameters)
                            End If



                        End Using

                        lblCroppedImage.Text = String.Format("<img src='{0}' alt='Cropped image'>", strPath)
                    End Using
                End Using
            End Using
        Else
            lblErrorLoad.Text = "Not a valid image !!!"
            Exit Sub
        End If
    End Sub

    Protected Sub btnReduce_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReduce.Click
        If ViewState("ImgPath").trim <> "~/Images/Photos/no_image1.gif" Then
            Dim str_PhyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim str_imgvirtual As String = ConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString
            Dim subFilePath As String = Session("Folder_Name")
            Dim fpath As String = str_PhyPath & subFilePath & ViewState("ImgPath")
            lblCroppedImage.Text = ""
            Dim WIDTH As Integer = ViewState("IMG_WIDTH")
            Dim HEIGHT As Integer = ViewState("IMG_HEIGHT")
            Dim oBitmap As Bitmap
            oBitmap = New Bitmap(fpath)

            ' Here create a new bitmap object of the same height and width of the image.
            Dim bmpNew As Bitmap = New Bitmap(WIDTH, HEIGHT)
           
            'oGraphic = Graphics.FromImage(bmpNew)
            'oGraphic.DrawImage(oBitmap, New Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width, oBitmap.Height, GraphicsUnit.Pixel)


            If bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format1bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format4bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format8bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Undefined Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.DontCare Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppArgb1555 Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppGrayScale Then
                Throw New NotSupportedException("Pixel format of the image is not supported.")
            End If

            Dim oGraphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bmpNew)
            oGraphic.SmoothingMode = Drawing.Drawing2D.SmoothingMode.HighQuality
            oGraphic.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
            oGraphic.DrawImage(oBitmap, New Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width, oBitmap.Height, GraphicsUnit.Pixel)
            oGraphic.Dispose()
            'File.Copy(OldFile, UploadPath & NewFile & extFile, True)
            ' Release the lock on the image file. Of course,
            ' image from the image file is existing in Graphics object
            oBitmap.Dispose()
            oBitmap = bmpNew

            Dim oBrush As New SolidBrush(Color.Black)
            Dim ofont As New Font("Arial", 8)
            oGraphic.Dispose()
            ofont.Dispose()
            oBrush.Dispose()

            Dim DirName As String = "fCopy_" & ViewState("FEE_ID")

            Dim savePath As String = String.Empty
            savePath = str_PhyPath & subFilePath & DirName
            Dim strVir_Path As String = str_imgvirtual & subFilePath & DirName & "\" & ViewState("ImgPath")
            'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa
            ViewState("ReduceFPath") = savePath
            If Not Directory.Exists(savePath) Then
                Directory.CreateDirectory(savePath)
                oBitmap.Save(savePath & "\" & ViewState("ImgPath"), ImageFormat.Jpeg)
                lblCroppedImage.Text = String.Format("<img src='{0}' alt='Cropped image'>", strVir_Path)
                oBitmap.Dispose()
            Else

                File.Delete(savePath & "\" & ViewState("ImgPath"))

                oBitmap.Save(savePath & "\" & ViewState("ImgPath"), ImageFormat.Jpeg)
                lblCroppedImage.Text = String.Format("<img src='{0}' alt='Cropped image'>", strVir_Path)
                oBitmap.Dispose()

            End If





            'If Directory.Exists(ViewState("DEL_PATH")) Then
            '    Directory.Delete(ViewState("DEL_PATH"), True)
            'End If


        Else

            lblErrorLoad.Text = "Not a valid image !!!"
            Exit Sub
        End If
        'bp.Save("C:\inetpub\wwwroot\Curr\mage.jpg", System.Drawing.Imaging.ImageFormat.Jpeg)
    End Sub

    ''' <summary>
    ''' Find the right codec
    ''' </summary>
    ''' <param name="extension"></param>
    ''' <returns></returns>
    Public Shared Function GetImageCodec(ByVal extension As String) As ImageCodecInfo
        extension = extension.ToUpperInvariant()
        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
        For Each codec As ImageCodecInfo In codecs
            If codec.FilenameExtension.Contains(extension) Then
                Return codec
            End If
        Next
        Return codecs(1)
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Not Page.IsPostBack Then
            Dim display_data() As String = New String(7) {}
            Dim data() As String = New String(4) {}

            Dim splitter As Char = "|"
            Dim str_display_data As String = String.Empty
            rbCrop.Checked = True
            btnReduce.Visible = False
            treditImg.Visible = True
            trUploadImg.Visible = False
            ViewState("ImgPath") = "~/Images/Photos/no_image1.gif"
            ViewState("STU_ID") = "0"
            ViewState("RPF_ID") = "0"

            If Not Session("Upload_display_data") Is Nothing Then
                str_display_data = Session("Upload_display_data")

                ' data = Session("Upload_data").Split(splitter)
                display_data = str_display_data.Split(splitter)
                'ltName.Text = display_data(0)
                'ltYear.Text = display_data(1)
                'ltGrade.Text = display_data(2)
                ltACY.Text = "<font color='black'>" & display_data(1) & "</font>"
                ltGRD_SCT.Text = "<font color='black'>" & display_data(2) & "</font>"
                ltReport.Text = "<font color='black'>" & display_data(3) & "</font>"
                ltSchedule.Text = "<font color='black'>" & display_data(4) & "</font>"
            End If
            GETPRE_DEFINEDSIZE()
            If Not Session("Upload_data") Is Nothing Then
                data = Session("Upload_data").Split(splitter)
                ViewState("STU_ID") = data(0)
                ViewState("RPF_ID") = data(1)
                ViewState("ACD_ID") = data(2)
            End If
            BINDIMAGE()

            ' Session("Upload_data") = lblstu_id.Text & "|" & ddlRptSchedule.SelectedValue & "|" & imgthumb.ImageUrl


            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
        End If

    End Sub
    Sub GETPRE_DEFINEDSIZE()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim STR As String = " select FUM_IMG_WIDTH,FUM_IMG_HEIGHT from CURR.FILEUPLOAD_M where FUM_APPLIED_FOR='CURR' AND FUM_BSU_ID='" & Session("sBsuid") & "'"
        Using IMG_DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR)
            If IMG_DATAREADER.HasRows Then
                While IMG_DATAREADER.Read
                    ViewState("IMG_WIDTH") = Convert.ToString(IMG_DATAREADER("FUM_IMG_WIDTH"))
                    ViewState("IMG_HEIGHT") = Convert.ToString(IMG_DATAREADER("FUM_IMG_HEIGHT"))
                End While
            Else
                ViewState("IMG_WIDTH") = "380"
                ViewState("IMG_HEIGHT") = "510"
            End If
        End Using
    End Sub
    Sub BINDIMAGE()
        originalImage.Src = ""
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_PhyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
        Dim str_imgvirtual As String = ConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString
        Dim subFilePath As String = Session("Folder_Name")
        Dim fpath As String = str_imgvirtual & subFilePath

        Dim STR As String = " SELECT S.STU_FEE_ID AS FEE_ID, CASE WHEN ISNULL(U.SFU_FILEPATH, '')  = '' THEN '~/Images/Photos/no_image1.gif' ELSE   U.SFU_FILEPATH  END AS SFU_FILEPATH FROM " & _
     "  vw_STUDENT_DETAILS AS S LEFT OUTER JOIN  CURR.STUDENT_FILEUPLOAD AS U ON  S.STU_ID=U.SFU_STU_ID AND U.SFU_RPF_ID ='" & ViewState("RPF_ID") & "' WHERE(S.STU_ID = '" & ViewState("STU_ID") & "')"
        Using IMGREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR)
            If IMGREADER.HasRows Then
                While IMGREADER.Read
                    ViewState("ImgPath") = Convert.ToString(IMGREADER("SFU_FILEPATH"))
                    ViewState("FEE_ID") = Convert.ToString(IMGREADER("FEE_ID"))
                End While
            End If
        End Using

        If ViewState("ImgPath").trim <> "~/Images/Photos/no_image1.gif" Then
            originalImage.Src = fpath + ViewState("ImgPath") + "?" + DateTime.Now.Ticks.ToString()
        Else
            originalImage.Src = "~/images/Photos/no_image1.gif"

        End If

    End Sub
    Protected Sub rbCrop_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCrop.CheckedChanged
        treditImg.Visible = True
        trUploadImg.Visible = False
        btnReduce.Visible = False
        btnCrop.Visible = True
        btnSaveCrop.Visible = True
        spOth.InnerText = "Cropped image"
        BINDIMAGE()
        lblCroppedImage.Text = ""
    End Sub
    Protected Sub rbUpload_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUpload.CheckedChanged
        treditImg.Visible = False

        trUploadImg.Visible = True
        ' BINDIMAGE()
        ltNote.Text = " <font color='#800000' size='2' style='padding:2px;'><b>Note:</b><i>Upload image file type extension <b>jpg</b> with size less than 100KB.Maintain max image width " & ViewState("IMG_WIDTH") & "  and max height  " & ViewState("IMG_HEIGHT") & " pixels.</i></font>"
        lblCroppedImage.Text = ""
    End Sub
    Protected Sub rbReduce_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbReduce.CheckedChanged
        treditImg.Visible = True
        trUploadImg.Visible = False
        btnCrop.Visible = False
        btnReduce.Visible = True
        btnSaveCrop.Visible = True
        spOth.InnerText = "Reduced image"
        BINDIMAGE()
        lblCroppedImage.Text = ""
    End Sub
    Protected Sub btnSaveCrop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCrop.Click
        Try

            Dim status As String = String.Empty
            Dim str_PhyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim str_imgvirtual As String = ConfigurationManager.ConnectionStrings("STU_REPORT_VIRTUAL").ConnectionString
            Dim subFilePath As String = Session("Folder_Name")

            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            If rbCrop.Checked Then
                status = CREATEFILE(ViewState("oldPath"), ViewState("FEE_ID"), ViewState("Extension"))
                If status = "" Then
                    Directory.Delete(ViewState("DEL_PATH"), True)
                    BINDIMAGE()
                    lblCroppedImage.Text = ""
                    lblErrorLoad.Text = "<font color='green'>File uploaded successfully...</font>"
                End If

            ElseIf rbReduce.Checked Then


                Dim d As New DirectoryInfo(ViewState("ReduceFPath"))
                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fi
                        File.Copy(f.FullName, str_PhyPath & subFilePath & ViewState("FEE_ID") & f.Extension, True)
                    Next
                    Directory.Delete(ViewState("ReduceFPath"), True)

                    BINDIMAGE()
                    lblCroppedImage.Text = ""
                    lblErrorLoad.Text = "<font color='green'>File uploaded successfully...</font>"
                Else

                    lblErrorLoad.Text = "File does not exist!!!.Reduce the orginal file and then save."

                End If



            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Save button")

            lblErrorLoad.Text = "File could not be saved."

        End Try

    End Sub
    Private Function CREATEFILE(ByVal OldFile As String, ByVal NewFile As String, ByVal extFile As String) As String
        Try
            Dim status As String = ""

            Dim str_phyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString

            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")

            If Directory.Exists(UploadPath) Then

                File.Copy(OldFile, UploadPath & NewFile & extFile, True)
                status = Update_database(NewFile & extFile, NewFile)
            Else

                Directory.CreateDirectory(UploadPath)
                File.Copy(OldFile, UploadPath & NewFile & extFile, True)
                status = Update_database(NewFile & extFile, NewFile)
            End If
            If status = "0" Then
                Return ""
            Else
                Return "Error while saving the destination folder"
            End If

        Catch ex As Exception
            Return "Error while saving  the destination folder"
        End Try
    End Function
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click

        Try
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength
            If fileload.HasFile = False Then
                lblErrorLoad.Text = "Select  image file to upload !!!"
                Exit Sub
                ' ElseIf intDocFileLength > 50500 Then '4MB
            ElseIf intDocFileLength > 102400 Then '4MB
                lblErrorLoad.Text = "Image file size exceeds the limit of 100kb !!!"
                Exit Sub
            End If
            'Dim fs As New FileInfo(fileload.PostedFile.FileName)

            ' get the file size
            Dim strPostedFileName As String = fileload.PostedFile.FileName       'get the file name
            Dim str_phyPath As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")
            Dim status As String = String.Empty



            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".jpg") Then ' if zip file processed Or (strExtn = ".png") Or (strExtn = ".bmp")


                    'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa

                    If Not Directory.Exists(UploadPath) Then
                        Directory.CreateDirectory(UploadPath)
                        fileload.PostedFile.SaveAs(UploadPath & ViewState("FEE_ID") & System.IO.Path.GetExtension(strPostedFileName))
                        status = Update_database(ViewState("FEE_ID") & System.IO.Path.GetExtension(strPostedFileName), ViewState("FEE_ID"))

                        If status <> "0" Then
                            lblErrorLoad.Text = "Error while saving " & fileload.PostedFile.FileName & " to the destination folder"
                        End If
                    Else
                        fileload.PostedFile.SaveAs(UploadPath & ViewState("FEE_ID") & System.IO.Path.GetExtension(strPostedFileName))
                        status = Update_database(ViewState("FEE_ID") & System.IO.Path.GetExtension(strPostedFileName), ViewState("FEE_ID"))

                        If status <> "0" Then
                            lblErrorLoad.Text = "Error while saving " & fileload.PostedFile.FileName & " to the destination folder"
                            Exit Sub
                        End If


                    End If
                    BINDIMAGE()
                    lblCroppedImage.Text = ""
                    lblErrorLoad.Text = "<font color='green'>File uploaded successfully...</font>"
                Else
                    lblErrorLoad.Text = "Upload image file type extension jpg only !!!"

                End If
            Else
                lblErrorLoad.Text = "There is no file to upload !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub
    Private Function Update_database(ByVal file As String, ByVal fee_id As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("ACD_ID"))
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", ViewState("STU_ID"))
            pParms(2) = New SqlClient.SqlParameter("@RPF_ID", ViewState("RPF_ID"))
            pParms(3) = New SqlClient.SqlParameter("@FEE_ID", fee_id)
            pParms(4) = New SqlClient.SqlParameter("@FILEPATH", file)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVEFILEUPLOAD_BYSTUD", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag

        Catch ex As Exception
            Return -1
        End Try

    End Function
    'Public Shared Function ResizeImageFile(ByVal imageFile As Byte(), ByVal targetSize As Integer) As Byte()
    '    Dim original As Image = Image.FromStream(New MemoryStream(imageFile))
    '    Dim targetH As Integer, targetW As Integer
    '    If original.Height > original.Width Then
    '        targetH = targetSize
    '        targetW = CInt((original.Width * (CSng(targetSize) / CSng(original.Height))))
    '    Else
    '        targetW = targetSize
    '        targetH = CInt((original.Height * (CSng(targetSize) / CSng(original.Width))))
    '    End If
    '    Dim imgPhoto As Image = Image.FromStream(New MemoryStream(imageFile))
    '    ' Create a new blank canvas. The resized image will be drawn on this canvas.
    '    Dim bmPhoto As New Bitmap(targetW, targetH, PixelFormat.Format24bppRgb)
    '    bmPhoto.SetResolution(72, 72)
    '    Dim grPhoto As Graphics = Graphics.FromImage(bmPhoto)
    '    grPhoto.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
    '    grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic
    '    grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality
    '    grPhoto.DrawImage(imgPhoto, New Rectangle(0, 0, targetW, targetH), 0, 0, original.Width, original.Height, _
    '    GraphicsUnit.Pixel)
    '    ' Save out to memory and then to a file. We dispose of all objects to make sure the files don't stay locked.
    '    Dim mm As New MemoryStream()
    '    bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg)
    '    original.Dispose()
    '    imgPhoto.Dispose()
    '    bmPhoto.Dispose()
    '    grPhoto.Dispose()
    '    Return mm.GetBuffer()
    'End Function
End Class