Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReportCardSetup_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                MaintainScrollPositionOnPostBack = True
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100130") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CStr(Session("sBsuid")), ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    '----Active control 
                    TabContainer_ReportCardSetup.ActiveTabIndex = 0
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
                    DrpAcID_RptLinking = studClass.PopulateAcademicYear(DrpAcID_RptLinking, Session("clm"), CStr(Session("sBsuid")))
                    ddlacd_rptDesignlinking = studClass.PopulateAcademicYear(ddlacd_rptDesignlinking, Session("clm"), CStr(Session("sBsuid")))
                    GridBind()
                    gvReport.Attributes.Add("bordercolor", "#1b80b6")

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub TabContainer_ReportCardSetup_ActiveTabChanged(sender As Object, e As EventArgs) Handles TabContainer_ReportCardSetup.ActiveTabChanged
        If TabContainer_ReportCardSetup.ActiveTabIndex = 0 Then   'Tab : Setup
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
            GridBind()
            gvReport.Attributes.Add("bordercolor", "#1b80b6")
            ddlAcademicYear.Focus()
            MaintainScrollPositionOnPostBack = True
        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 1 Then 'Tab : Linking
            DrpAcID_RptLinking = studClass.PopulateAcademicYear(DrpAcID_RptLinking, Session("clm"), CStr(Session("sBsuid")))
            Call Get_RsmID_FromAcdID(DrpRpt_RsmID_RptLinking)
            DrpAcID_RptLinking.Focus()
            MaintainScrollPositionOnPostBack = True
        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 2 Then  'Tab : Grade Descriptor
            DrpAcID_From_GradeDescriptor = studClass.PopulateAcademicYear(DrpAcID_From_GradeDescriptor, Session("clm"), CStr(Session("sBsuid")))
            DrpAcID_To_GradeDescriptor = studClass.PopulateAcademicYear(DrpAcID_To_GradeDescriptor, Session("clm"), CStr(Session("sBsuid")))
            Call Load_GradeDescriptor_List()
            DrpAcID_To_GradeDescriptor.Focus()
            MaintainScrollPositionOnPostBack = True
        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 3 Then  'Tab : Absentee Settings
            DrpAcID_AbsenteeSettings = studClass.PopulateAcademicYear(DrpAcID_AbsenteeSettings, Session("clm"), CStr(Session("sBsuid")))
            Call Load_AsbenteeSettings_List()
        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 4 Then  'Tab :Rule Deletion
            ddlruledeleteAcdYr = studClass.PopulateAcademicYear(ddlruledeleteAcdYr, Session("clm"), CStr(Session("sBsuid")))
            Call SelectFields()
            ddlGrade = PopulateGrade(ddlGrade, ddlruledeleteAcdYr.SelectedValue.ToString)
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGrade.Items.Insert(0, li)

            BindTerm()
            BindSubjects()

            updateSubjectURL()

            If (Not Session("accYear_rule") Is Nothing) Or _
            (Not Session("term_rule") Is Nothing) Or _
            (Not Session("grade_rule") Is Nothing) Or _
            (Not Session("subj_rule") Is Nothing) Or _
              (Not Session("schedule_rule") Is Nothing) Or (Not Session("header_rule") Is Nothing) Then
                SelectFields()
                GridBindRule()
            Else
                tblClm.Rows(4).Visible = False
                tblClm.Rows(5).Visible = False
            End If
            'DrpAcID_From_Subjectcopy = studClass.PopulateAcademicYear(DrpAcID_From_Subjectcopy, Session("clm"), CStr(Session("sBsuid")))
            'DrpAcID_To_Subjectcopy = studClass.PopulateAcademicYear(DrpAcID_To_Subjectcopy, Session("clm"), CStr(Session("sBsuid")))
            ' Call Load_GradeDescriptor_List()
            ' DrpAcID_To_GradeDescriptor.Focus()
            MaintainScrollPositionOnPostBack = True

        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 5 Then  'Tab : Subject copy
            DrpAcID_From_Subjectcopy = studClass.PopulateAcademicYear(DrpAcID_From_Subjectcopy, Session("clm"), CStr(Session("sBsuid")))
            DrpAcID_To_Subjectcopy = studClass.PopulateAcademicYear(DrpAcID_To_Subjectcopy, Session("clm"), CStr(Session("sBsuid")))
            ' Call Load_GradeDescriptor_List()
            ' DrpAcID_To_GradeDescriptor.Focus()
            MaintainScrollPositionOnPostBack = True

        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 6 Then  'Tab : Option copy
            ddloptionscopyfromYR = studClass.PopulateAcademicYear(ddloptionscopyfromYR, Session("clm"), CStr(Session("sBsuid")))
            ddloptionscopyToYR = studClass.PopulateAcademicYear(ddloptionscopyToYR, Session("clm"), CStr(Session("sBsuid")))
            ' Call Load_GradeDescriptor_List()
            ' DrpAcID_To_GradeDescriptor.Focus()
            MaintainScrollPositionOnPostBack = True

        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 7 Then  'Tab : Unpublished Report Card
            ddlunpubliAcadyr = studClass.PopulateAcademicYear(ddlunpubliAcadyr, Session("clm"), CStr(Session("sBsuid")))
            BindReportCard()
            BindTermUnpublish()
            BindGradeUnpublish()
            txtRelease.Text = Format(Now.Date, "dd/MMM/yyyy")
            'ddloptionscopyToYR = studClass.PopulateAcademicYear(ddloptionscopyToYR, Session("clm"), CStr(Session("sBsuid")))
            ' Call Load_GradeDescriptor_List()
            ' DrpAcID_To_GradeDescriptor.Focus()
            MaintainScrollPositionOnPostBack = True

        ElseIf TabContainer_ReportCardSetup.ActiveTabIndex = 8 Then  'Tab : Report Design Linking 
            ddlacd_rptDesignlinking = studClass.PopulateAcademicYear(ddlacd_rptDesignlinking, Session("clm"), CStr(Session("sBsuid")))
            Call Get_RsmID_FromAcdIDRPTDSG(ddlRptDLnk_RPTNAME)
            BIND_DESIGNNAME()
            DrpAcID_RptLinking.Focus()
            MaintainScrollPositionOnPostBack = True
        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT DISTINCT  RSM_ID  ,MAX(RSM_DESCR) AS RSM_DESCR FROM RPT.REPORT_SETUP_M  WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedItem.Value.ToString + " GROUP BY RSM_ID ORDER BY RSM_ID  DESC "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvReport.DataSource = ds
            gvReport.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
#End Region
    Protected Sub gvReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReport.PageIndexChanging
        gvReport.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
    Protected Sub gvReport_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReport.RowCommand

        Try
            If e.CommandName = "view" Then

                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvReport.Rows(index), GridViewRow)
                Dim lblRsmId As Label
                Dim lblReport As Label
                lblRsmId = selectedRow.FindControl("lblRsmId")
                lblReport = selectedRow.FindControl("lblReport")

                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                ViewState("rsmid") = Encr_decrData.Encrypt(lblRsmId.Text)

                Dim url_view As String
                url_view = String.Format("~\Curriculum\clmConfSetup.aspx?MainMnu_code={0}&datamode={1}&rsmid=" + Encr_decrData.Encrypt(lblRsmId.Text) _
                     & "&report=" + Encr_decrData.Encrypt(lblReport.Text) _
                     & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url_view)
            End If

            If e.CommandName = "edit" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvReport.Rows(index), GridViewRow)
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt("edit")

                Dim lblRsmId As Label
                Dim lblReport As Label
                lblRsmId = selectedRow.FindControl("lblRsmId")
                lblReport = selectedRow.FindControl("lblReport")

                Dim url_edit As String
                url_edit = String.Format("~\Curriculum\clmConfSetup.aspx?MainMnu_code={0}&datamode={1}&rsmid=" + Encr_decrData.Encrypt(lblRsmId.Text) _
                     & "&report=" + Encr_decrData.Encrypt(lblReport.Text) _
                     & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url_edit)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub
    Protected Sub btnAddNew_Click(sender As Object, e As EventArgs) Handles btnAddNew.Click
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        Dim url As String
        url = String.Format("~\Curriculum\clmConfSetup.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    '-------Tab 2 Linking----------
    Protected Sub DrpAcID_RptLinking_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_RptLinking.SelectedIndexChanged
        DrpRpt_RsmID_RptLinking.Items.Clear()
        DrpRpt_RsmID_RptLinking.Focus()
        Chklst_RsmID_Rpt_linking.Items.Clear()
        Call Get_RsmID_FromAcdID(DrpRpt_RsmID_RptLinking)
    End Sub
    Protected Sub Get_RsmID_FromAcdID(DrpDwnName As DropDownList)
        DrpDwnName.Items.Clear()
        Chklst_RsmID_Rpt_linking.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Try
            Dim conn As New SqlConnection(str_conn)
            If conn.State = ConnectionState.Closed Then conn.Open()
            Using conn
                Using cmd As New SqlCommand("ACT.usp_GetAllRsmID_From_AcdId", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@intAcdID", SqlDbType.VarChar).Value = DrpAcID_RptLinking.SelectedValue.ToString
                    cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
                    Dim Sqlreader As SqlDataReader = cmd.ExecuteReader()
                    If Sqlreader.HasRows Then
                        With DrpDwnName
                            .DataSource = Sqlreader
                            .DataTextField = "RSM_DESCR"
                            .DataValueField = "RSM_ID"
                            .Items.Insert(0, "Please select...")
                            .SelectedIndex = 0
                            .DataBind()
                        End With
                    Else
                        DrpDwnName.Items.Insert(0, "Nothing to display here...")
                        DrpDwnName.SelectedIndex = 0
                    End If
                End Using
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        End Try
    End Sub
    Protected Sub DrpRpt_RsmID_RptLinking_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpRpt_RsmID_RptLinking.SelectedIndexChanged
        If DrpRpt_RsmID_RptLinking.SelectedItem.Text = "Please select..." Then
            Chklst_RsmID_Rpt_linking.Items.Clear()
        Else
            hdf_SelRsmDescr.Value = DrpRpt_RsmID_RptLinking.SelectedItem.ToString
            Get_To_RsmID()
        End If
    End Sub
    Sub Get_To_RsmID()
        Chklst_RsmID_Rpt_linking.Items.Clear()
        Try
            Dim conn As New SqlConnection(ConnectionManger.GetOASIS_CURRICULUMConnectionString)
            If conn.State = ConnectionState.Closed Then conn.Open()
            Using conn
                Using cmd As New SqlCommand("ACT.usp_GetAllRsmID_From_AcdId", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@intAcdID", SqlDbType.VarChar).Value = DrpAcID_RptLinking.SelectedValue.ToString
                    cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
                    Dim Sqlreader As SqlDataReader = cmd.ExecuteReader()
                    While Sqlreader.Read()
                        If Sqlreader.HasRows Then
                            With Chklst_RsmID_Rpt_linking
                                .DataSource = Sqlreader
                                .DataTextField = "RSM_DESCR"
                                .DataValueField = "RSM_ID"
                                .DataBind()
                            End With
                        End If
                    End While
                End Using
            End Using
            conn.Close()
            ''----Remove the item name sa selected in rsmID From
            For Each item As ListItem In Chklst_RsmID_Rpt_linking.Items
                If item.Text = hdf_SelRsmDescr.Value Then
                    item.Enabled = False
                    item.Attributes.Add("style", "display:none")
                End If
            Next
            Chklst_RsmID_Rpt_linking.Focus()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        End Try
    End Sub
    Protected Sub btnCancel_RptLinking_Click(sender As Object, e As EventArgs) Handles btnCancel_RptLinking.Click
        DrpRpt_RsmID_RptLinking.Items.Clear()
        Chklst_RsmID_Rpt_linking.Items.Clear()
        ddlAcademicYear = studClass.PopulateAcademicYear(DrpAcID_RptLinking, Session("clm"), CStr(Session("sBsuid")))
        Call Get_RsmID_FromAcdID(DrpRpt_RsmID_RptLinking)
    End Sub
    Protected Sub btnAssign_RptLinking_Click(sender As Object, e As EventArgs) Handles btnAssign_RptLinking.Click
        Dim Selitem As String = vbNullString
        For Each item As ListItem In Chklst_RsmID_Rpt_linking.Items
            Selitem += If(item.Selected, item.Value & "|", "")
        Next

        If Selitem = vbNullString Or DrpRpt_RsmID_RptLinking.Text = vbNullString Or DrpRpt_RsmID_RptLinking.Text = "Please Select..." Or DrpRpt_RsmID_RptLinking.Text = "Nothing to display here..." Or DrpAcID_RptLinking.Text = vbNullString Then
            lblError.Text = "*Fields Marked with (*) are mandatory"
        Else
            Try
                Dim conn As New SqlConnection(ConnectionManger.GetOASIS_CURRICULUMConnectionString)
                If conn.State = ConnectionState.Closed Then conn.Open()
                Dim sqlComm01 As New SqlCommand("ACT.usp_UpdateReportRSMLinking", conn)
                sqlComm01.CommandType = CommandType.StoredProcedure
                With sqlComm01
                    .Parameters.AddWithValue("@intAcdID", DrpAcID_RptLinking.SelectedValue)
                    .Parameters.AddWithValue("@strBsuID", CStr(Session("sBsuid")))
                    .Parameters.AddWithValue("@strRSMId", DrpRpt_RsmID_RptLinking.SelectedValue)
                    .Parameters.AddWithValue("@strSelectedRSMID", Selitem.ToString)
                    .ExecuteNonQuery()
                End With
                lblError.Text = "Record(s) updated sucessfully.."
                conn.Close()
                Call Get_RsmID_FromAcdID(DrpRpt_RsmID_RptLinking)
                MaintainScrollPositionOnPostBack = True
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed" & ex.Message
            End Try
        End If
    End Sub


    '---------Tab 3 Grade Descriptor---------------------
    Protected Sub Load_GradeDescriptor_List()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim conn As New SqlConnection(str_conn)
        If conn.State = ConnectionState.Closed Then conn.Open()
        Try
            Using cmd As New SqlCommand("ACT.usp_GradeDescriptor_List", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = CStr(Session("sBsuid").ToString)
                cmd.Parameters.Add("@strAcdID", SqlDbType.Int).Value = DrpAcID_From_GradeDescriptor.SelectedValue.ToString
                Using adapter As New SqlDataAdapter(cmd)
                    Dim ds2 As New DataSet()
                    adapter.Fill(ds2)
                    With Grd_GradeLst_GradeDescriptor
                        .DataSource = ds2.Tables(0)
                        .DataBind()
                    End With
                End Using
            End Using
            Grd_GradeLst_GradeDescriptor.Attributes.Add("bordercolor", "#1b80b6")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        Finally
            conn.Close()
        End Try

    End Sub
    Protected Sub DrpAcID_From_GradeDescriptor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_From_GradeDescriptor.SelectedIndexChanged
        DrpAcID_To_GradeDescriptor.Focus()
        MaintainScrollPositionOnPostBack = True
    End Sub
    Protected Sub DrpAcID_To_GradeDescriptor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_To_GradeDescriptor.SelectedIndexChanged
        btnCopy_GradeDescriptor.Focus()
        MaintainScrollPositionOnPostBack = True
    End Sub
    Protected Sub btnAddUpdate_GradeDescriptor_Click(sender As Object, e As EventArgs) Handles btnAddUpdate_GradeDescriptor.Click
        'ViewState("datamode") = Encr_decrData.Encrypt(Trim$(btnAddUpdate_GradeDescriptor.Text))
        Call ValidateAndSaveData()
        btnAddUpdate_GradeDescriptor.Text = "Add New"
    End Sub
    Protected Sub ValidateAndSaveData()

        If txtGradeid_GradeDescriptor.Text = vbNullString Or
            txtGradeheader1_GradeDescriptor.Text = vbNullString Or
            txtGradeheader2_GradeDescriptor.Text = vbNullString Then
            lblError.Text = "Fields Marked with (*) are mandatory"
        Else
            Dim strMode As String
            If Trim$(btnAddUpdate_GradeDescriptor.Text) = "Update" Then
                strMode = hdf_SelDataMode.Value
            ElseIf Trim$(btnAddUpdate_GradeDescriptor.Text) = "Add new" Then
                strMode = 0
            End If


            Dim sp_name As String = "ACT.usp_GradeDescriptor_Entry"
            Dim Trans As SqlTransaction = Nothing
            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim objconn As New SqlConnection(str_conn)
                objconn.Open()
                Trans = objconn.BeginTransaction
                Dim param(8) As SqlParameter
                param(0) = Mainclass.CreateSqlParameter("@strGrhid", strMode, SqlDbType.Text)
                param(1) = Mainclass.CreateSqlParameter("@strGrhGradeID", Trim$(txtGradeid_GradeDescriptor.Text.ToString), SqlDbType.Text)
                param(2) = Mainclass.CreateSqlParameter("@strGradeHeading1", Trim$(txtGradeheader1_GradeDescriptor.Text.ToString), SqlDbType.Text)
                param(3) = Mainclass.CreateSqlParameter("@strGradeHeading2", Trim$(txtGradeheader2_GradeDescriptor.Text.ToString), SqlDbType.Text)
                param(4) = Mainclass.CreateSqlParameter("@intACD_CopyFrom", Trim$(DrpAcID_From_GradeDescriptor.SelectedValue), SqlDbType.Int)
                param(5) = Mainclass.CreateSqlParameter("@intACD_CopyTo", Trim$(DrpAcID_To_GradeDescriptor.SelectedValue), SqlDbType.Int)
                param(6) = Mainclass.CreateSqlParameter("@strBSU_CopyFrom", CStr(Session("sBsuid").ToString), SqlDbType.Text)
                param(7) = Mainclass.CreateSqlParameter("@strBSU_Copyto", CStr(Session("sBsuid").ToString), SqlDbType.Text)
                param(8) = Mainclass.CreateSqlParameter("@strDataMode", Encr_decrData.Decrypt(ViewState("datamode")), SqlDbType.Text)
                SqlHelper.ExecuteScalar(Trans, sp_name, param)
                Trans.Commit()
                objconn.Close()
                lblError.Text = "Record(s) saved sucessfully!..."

                Call ClearFields_GradeDescriptor()
                Call Load_GradeDescriptor_List()

            Catch ex As SqlException
                Trans.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed | " & ex.Message
            End Try
        End If
    End Sub
    Protected Sub ClearFields_GradeDescriptor()
        DrpAcID_From_GradeDescriptor = studClass.PopulateAcademicYear(DrpAcID_From_GradeDescriptor, Session("clm"), CStr(Session("sBsuid")))
        DrpAcID_To_GradeDescriptor = studClass.PopulateAcademicYear(DrpAcID_To_GradeDescriptor, Session("clm"), CStr(Session("sBsuid")))
        txtGradeid_GradeDescriptor.Text = Nothing
        txtGradeheader1_GradeDescriptor.Text = Nothing
        txtGradeheader2_GradeDescriptor.Text = Nothing
    End Sub
    Protected Sub Grd_GradeLst_GradeDescriptor_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles Grd_GradeLst_GradeDescriptor.PageIndexChanging
        Grd_GradeLst_GradeDescriptor.PageIndex = e.NewPageIndex
        Call Load_GradeDescriptor_List()
    End Sub
    Protected Sub Grd_GradeLst_GradeDescriptor_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Grd_GradeLst_GradeDescriptor.RowCommand
        If e.CommandName = "Edit_GradeDescriptor" Then
            MaintainScrollPositionOnPostBack = True
            Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = Grd_GradeLst_GradeDescriptor.Rows(rowIndex)
            txtGradeid_GradeDescriptor.Text = TryCast(row.FindControl("lblGrh_GrdId_GradeDescriptor"), Label).Text
            txtGradeheader1_GradeDescriptor.Text = IIf(TryCast(row.FindControl("lblGrh_Header1_GradeDescriptor"), Label).Text = vbNullString, "", TryCast(row.FindControl("lblGrh_Header1_GradeDescriptor"), Label).Text)
            txtGradeheader2_GradeDescriptor.Text = IIf(TryCast(row.FindControl("lblGrh_Header2_GradeDescriptor"), Label).Text = vbNullString, "", TryCast(row.FindControl("lblGrh_Header2_GradeDescriptor"), Label).Text)
            hdf_SelDataMode.Value = TryCast(row.FindControl("lblGrh_Id_GradeDescriptor"), Label).Text
            ViewState("datamode") = Encr_decrData.Encrypt("Update")
            btnAddUpdate_GradeDescriptor.Text = "Update"
        End If
    End Sub

    '----------Tab 4 Absentee Settings
    Protected Sub Load_AsbenteeSettings_List()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim conn As New SqlConnection(str_conn)
        If conn.State = ConnectionState.Closed Then conn.Open()
        Try
            Using cmd As New SqlCommand("ACT.usp_AbsenteeSettings_List", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = CStr(Session("sBsuid").ToString)
                cmd.Parameters.Add("@strAcdID", SqlDbType.Int).Value = DrpAcID_AbsenteeSettings.SelectedValue.ToString
                Using adapter As New SqlDataAdapter(cmd)
                    Dim ds_AbsenteeSettings As New DataSet()
                    adapter.Fill(ds_AbsenteeSettings)
                    With Grd_AdsenteeSettings
                        .DataSource = ds_AbsenteeSettings.Tables(0)
                        .DataBind()
                    End With
                End Using
            End Using
            Grd_AdsenteeSettings.Attributes.Add("bordercolor", "#1b80b6")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        Finally
            conn.Close()
        End Try
    End Sub
    Protected Sub DrpAcID_AbsenteeSettings_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_AbsenteeSettings.SelectedIndexChanged
        Call Load_AsbenteeSettings_List()
        DrpAcID_To_GradeDescriptor.Focus()
        MaintainScrollPositionOnPostBack = True
    End Sub

    'Subject copy

    Protected Sub DrpAcID_From_Subjectcopy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_From_Subjectcopy.SelectedIndexChanged
        DrpAcID_From_Subjectcopy.Focus()
        ' chklist_gradefrom.Items.Clear()
        Call Get_Subjectcopyfrom_grade()
        MaintainScrollPositionOnPostBack = True
    End Sub

    Protected Sub DrpAcID_To_Subjectcopy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpAcID_To_Subjectcopy.SelectedIndexChanged
        DrpAcID_To_Subjectcopy.Focus()
        MaintainScrollPositionOnPostBack = True
    End Sub

    Protected Sub btnShow_Subjectcopy_Click(sender As Object, e As EventArgs) Handles btnShow_Subjectcopy.Click
        'ViewState("datamode") = Encr_decrData.Encrypt(Trim$(btnAddUpdate_GradeDescriptor.Text))
        'Call ValidateAndSaveData()
        'btnAddUpdate_GradeDescriptor.Text = "Add New"
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim stransgrade As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Param(2) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid").ToString(), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_COPYFROM", DrpAcID_From_Subjectcopy.SelectedValue, SqlDbType.BigInt)
            Param(2) = Mainclass.CreateSqlParameter("@ACD_COPYTO", DrpAcID_To_Subjectcopy.SelectedValue, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[saveSUBJECTCOPY]", Param)
            strans.Commit()
            ' Dim i As Integer = 0
           

        Catch ex As Exception
            strans.Rollback()
            ' stransgrade.Rollback()
        End Try







    End Sub

    Sub Get_Subjectcopyfrom_grade()
        chklist_gradefrom.Items.Clear()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid").ToString(), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", DrpAcID_From_Subjectcopy.SelectedValue, SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GRADES_FROM_ACDID]", Param)
            Dim dt As DataTable = DS.Tables(0)
            chklist_gradefrom.DataSource = dt
            chklist_gradefrom.DataTextField = "GRM_GRD_ID"
            chklist_gradefrom.DataValueField = "GRM_GRD_ID"
            chklist_gradefrom.DataBind()
            '   getsections()
            ''----Remove the item name sa selected in grade From
            For Each item As ListItem In chklist_gradefrom.Items
                Dim grade As String = item.Text + ","
                If item.Text = hdf_SelRsmDescr.Value Then
                    item.Enabled = False
                    item.Attributes.Add("style", "display:none")
                End If
            Next
            chklist_gradefrom.Focus()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        End Try
    End Sub

    'Ruledeletion
    Protected Sub lblMarks_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
            End With
            Dim url As String
            url = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                               & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                               & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Sub SelectFields()
        If (Not Session("accYear_rule") Is Nothing) Then
            If Not ddlruledeleteAcdYr.Items.FindByValue(Session("accYear_rule")) Is Nothing Then
                ddlruledeleteAcdYr.ClearSelection()
                ddlruledeleteAcdYr.Items.FindByValue(Session("accYear_rule")).Selected = True
            End If
        End If
        If (Not Session("term_rule") Is Nothing) Then
            If Not ddlTerm.Items.FindByValue(Session("term_rule")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("term_rule")).Selected = True
            End If
        End If
        If (Not Session("grade_rule") Is Nothing) Then
            If Not ddlGrade.Items.FindByValue(Session("grade_rule")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grade_rule")).Selected = True
            End If
        End If
    End Sub

    Protected Sub lblAttendance_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label

            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
            End With
            Dim url As String
            url = String.Format("~\Curriculum\clmATTEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub ddlgvAttendance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBindRule()
    End Sub


    Protected Sub ddlgvMarks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBindRule()
    End Sub

    Protected Sub ddlgvHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBindRule()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBindRule()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"

    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlruledeleteAcdYr.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlTerm.Items.Insert(0, li)
        li = New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub


    'Private Function isPageExpired() As Boolean

    '    If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
    '        Return False
    '    ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Sub updateSubjectURL()
        If ddlGrade.SelectedValue = "" Then
            hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlruledeleteAcdYr.SelectedValue.ToString _
                              & "&grdid=0&stmid=0"
        Else
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            If grade.Length > 1 Then
                hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlruledeleteAcdYr.SelectedValue.ToString _
                                             & "&grdid=" + grade(0) + "&stmid=" + grade(1)
            Else
                hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                                                            & "&grdid=" + ddlGrade.SelectedValue.ToString + "&stmid=1"
            End If
        End If
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub GridBindRule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strFilter As String = ""

        'str_query = "SELECT RRM_ID,RRM_DESCR,RRM_SBG_ID,RRM_ACD_ID,RRM_GRD_ID," _
        '           & " SBG_DESCR, GRM_DISPLAY, RRM_TRM_ID, TRM_DESCRIPTION" _
        '           & " FROM RPT.REPORT_RULE_M  AS A INNER JOIN OASIS..GRADE_BSU_M AS B" _
        '           & " ON A.RRM_GRD_ID=B.GRM_GRD_ID AND A.RRM_ACD_ID=B.GRM_ACD_ID " _
        '           & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.RRM_SBG_ID=C.SBG_ID" _
        '           & " INNER JOIN VW_TRM_M AS D ON A.RRM_TRM_ID=D.TRM_ID"

        If ddlTerm.SelectedValue <> "" And ddlTerm.SelectedValue <> "0" Then
            str_query = "SELECT DISTINCT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR,RSD_ID,RSD_HEADER, " _
                            & " SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_GRD_ID,GRM_DISPLAY,TRM_ID,TRM_DESCRIPTION," _
                            & " RRM_TYPE, RRM_DESCR,RRM_ID FROM RPT.REPORT_RULE_M AS A " _
                            & " INNER JOIN RPT.REPORT_SETUP_M AS D WITH(NOLOCK) ON A.RRM_RSM_ID=D.RSM_ID" _
                            & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS E WITH(NOLOCK) ON A.RRM_RPF_ID=E.RPF_ID" _
                            & " INNER JOIN RPT.REPORT_SETUP_D AS F WITH(NOLOCK) ON A.RRM_RSD_ID=F.RSD_ID" _
                            & " INNER JOIN OASIS..GRADE_BSU_M AS G WITH(NOLOCK) ON A.RRM_GRD_ID=G.GRM_GRD_ID AND A.RRM_ACD_ID=G.GRM_ACD_ID " _
                            & " INNER JOIN SUBJECTS_GRADE_S AS H WITH(NOLOCK) ON A.RRM_SBG_ID=H.SBG_ID " _
                            & " INNER JOIN VW_TRM_M AS I WITH(NOLOCK) ON A.RRM_TRM_ID=I.TRM_ID " _
                            & " WHERE RRM_ACD_ID=" + ddlruledeleteAcdYr.SelectedValue.ToString
        Else
            str_query = "SELECT * FROM (SELECT DISTINCT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR,RSD_ID,RSD_HEADER, " _
                           & " SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_GRD_ID,GRM_DISPLAY," _
                           & " CASE WHEN TRM_ID IS NULL THEN '0' ELSE TRM_ID END AS TRM_ID  ,CASE WHEN TRM_DESCRIPTION IS NULL THEN 'TERM FINAL' ELSE TRM_DESCRIPTION END AS TRM_DESCRIPTION ," _
                           & " RRM_TYPE, RRM_DESCR,RRM_ID,RRM_GRD_ID,RRM_SBG_ID,SBG_STM_ID FROM RPT.REPORT_RULE_M AS A " _
                           & " INNER JOIN RPT.REPORT_SETUP_M AS D WITH(NOLOCK) ON A.RRM_RSM_ID=D.RSM_ID" _
                           & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS E WITH(NOLOCK) ON A.RRM_RPF_ID=E.RPF_ID" _
                           & " INNER JOIN RPT.REPORT_SETUP_D AS F WITH(NOLOCK) ON A.RRM_RSD_ID=F.RSD_ID" _
                           & " INNER JOIN OASIS..GRADE_BSU_M AS G WITH(NOLOCK) ON A.RRM_GRD_ID=G.GRM_GRD_ID AND A.RRM_ACD_ID=G.GRM_ACD_ID " _
                           & " INNER JOIN SUBJECTS_GRADE_S AS H WITH(NOLOCK) ON A.RRM_SBG_ID=H.SBG_ID " _
                           & " LEFT OUTER JOIN VW_TRM_M AS I WITH(NOLOCK) ON A.RRM_TRM_ID=I.TRM_ID " _
                           & " WHERE RRM_ACD_ID=" + ddlruledeleteAcdYr.SelectedValue.ToString + ") P WHERE RRM_ID<>0 "

        End If

        If (ddlGrade.SelectedValue <> "") Then
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND RRM_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        If ddlSubjects.SelectedValue <> "" Then
            str_query += " AND SBG_ID=" + ddlSubjects.SelectedValue
        End If


        If ddlTerm.SelectedValue <> "" Then
            str_query += " AND TRM_ID=" + ddlTerm.SelectedValue.ToString
        End If

        Dim ddlgvSchedule As New DropDownList
        Dim ddlgvHeader As New DropDownList

        Dim selectedHeader As String = ""
        Dim selectedSchedule As String = ""

        If gvStud.Rows.Count > 0 Then

            ddlgvSchedule = gvStud.HeaderRow.FindControl("ddlgvSchedule")
            If ddlgvSchedule.Text <> "ALL" And ddlgvSchedule.Text <> "" Then
                strFilter += " AND RPF_DESCR='" + ddlgvSchedule.Text + "'"
                Session("schedule_rule") = ddlgvSchedule.Text
                selectedSchedule = ddlgvSchedule.Text
            Else
                Session("schedule_rule") = "ALL"
                selectedSchedule = "ALL"
            End If

            ddlgvHeader = gvStud.HeaderRow.FindControl("ddlgvHeader")
            If ddlgvHeader.Text <> "ALL" And ddlgvHeader.Text <> "" Then
                strFilter += " AND rsd_header='" + ddlgvHeader.Text + "'"
                Session("header_rule") = ddlgvHeader.Text
                selectedHeader = ddlgvHeader.Text
            Else
                Session("header_rule") = "ALL"
                selectedHeader = "ALL"
            End If

            str_query += strFilter
        End If

        If Session("schedule_rule") <> "ALL" And Session("schedule_rule") <> "" Then
            str_query += " AND RPF_DESCR='" + Session("schedule_rule") + "'"
            selectedSchedule = Session("schedule_rule")
        End If
        If Session("header_rule") <> "ALL" And Session("header_rule") <> "" Then
            str_query += " AND rsd_header='" + Session("header_rule") + "'"
            selectedHeader = Session("header_rule")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.Visible = True
            gvStud.DataBind()
        End If

        Dim dr As DataRow
        Dim dt As DataTable
        dt = ds.Tables(0)

        If gvStud.Rows.Count > 0 Then
            ddlgvSchedule = gvStud.HeaderRow.FindControl("ddlgvSchedule")
            ddlgvHeader = gvStud.HeaderRow.FindControl("ddlgvHeader")
            ddlgvSchedule.Items.Clear()
            ddlgvSchedule.Items.Add("ALL")


            ddlgvHeader.Items.Clear()
            ddlgvHeader.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                    If ddlgvSchedule.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvSchedule.Items.Add(.Item(3))
                    End If

                    If ddlgvHeader.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvHeader.Items.Add(.Item(5))
                    End If


                End With

            Next

            ddlgvSchedule.Text = selectedSchedule
            ddlgvHeader.Text = selectedHeader
        End If
    End Sub

    Sub DeleteRule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblRrmId As Label
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblRrmId = gvStud.Rows(i).FindControl("lblRrmId")
                str_query = "EXEC RPT.deleteRULEconfi " + lblRrmId.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next
    End Sub
    Function isMarkProcessed(ByVal rrmid As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RST_RRM_ID) FROM RPT.REPORT_STUDENT_S WHERE RST_RRM_ID='" + rrmid + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If count = 0 Then
        '    Return False
        'Else
        Return False
        'End If
    End Function

#End Region

    'Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
    '    BindTerm()
    '    ddlGrade = PopulateGrade(ddlGrade, ddlruledeleteAcdYr.SelectedValue.ToString)
    '    Dim li As New ListItem
    '    li.Text = "ALL"
    '    li.Value = ""
    '    ddlGrade.Items.Insert(0, li)
    '    BindSubjects()
    '    updateSubjectURL()
    'End Sub




    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblClm.Rows(4).Visible = True
        tblClm.Rows(5).Visible = True

        Session("schedule_rule") = "ALL"
        Session("header_rule") = "ALL"


        GridBindRule()

        Session("accYear_rule") = ddlruledeleteAcdYr.SelectedValue.ToString
        Session("term_rule") = ddlTerm.SelectedValue.ToString
        Session("grade_rule") = ddlGrade.SelectedValue.ToString
        Session("subj_rule") = ddlSubjects.SelectedItem.Value

    End Sub

    Sub SelectSearches(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("accYear_rule") Is Nothing Then
            If Not ddlAcademicYear.Items.FindByValue(Session("accYear_rule")) Is Nothing Then
                ddlruledeleteAcdYr.ClearSelection()
                ddlruledeleteAcdYr.Items.FindByValue(Session("accYear_rule")).Selected = True
                ddlruledeleteAcdYr_SelectedIndexChanged(sender, e)    '''test
            End If
        End If

        If Not Session("term_rule") Is Nothing Then
            If Not ddlTerm.Items.FindByValue(Session("term_rule")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("term_rule")).Selected = True
            End If
        End If

        If Not Session("grade_rule") Is Nothing Then
            If Not ddlGrade.Items.FindByValue(Session("grade_rule")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grade_rule")).Selected = True
                ddlGrade_SelectedIndexChanged(sender, e)
            End If
        End If

        If Not Session("subj_rule") Is Nothing Then
            hfSBG_ID.Value = Session("subj_rule")
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ID='" + hfSBG_ID.Value + "'"
            txtSubject.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        updateSubjectURL()
        GridBindRule()
    End Sub

    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\clmReportRule_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBindRule()
    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt("edit")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblRrmId As Label
                With selectedRow
                    lblRrmId = .FindControl("lblRrmId")
                End With
                Dim url As String
                url = String.Format("~\Curriculum\clmReportRule_M.aspx?MainMnu_code={0}&datamode={1}&rrmid=" + Encr_decrData.Encrypt(lblRrmId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelect As CheckBox
            Dim lblRrmId As Label
            Dim lblRule As LinkButton = e.Row.FindControl("lblRule")
            Dim lblSubject As Label = e.Row.FindControl("lblSubject")
            Dim lblPrinted As Label = e.Row.FindControl("lblPrinted")
            Dim lblHeader As Label = e.Row.FindControl("lblHeader")

            chkSelect = e.Row.FindControl("chkSelect")
            lblRrmId = e.Row.FindControl("lblRrmId")
            If isMarkProcessed(lblRrmId.Text) = True Then
                chkSelect.Enabled = False
            End If
            Dim strLink As String = "clmReportRule_Popup.aspx?rrm_id=" + lblRrmId.Text + "&subject=" + lblSubject.Text + "&report=" + lblPrinted.Text + "&header=" + lblHeader.Text
            lblRule.Attributes.Add("onclick", "ShowWindowWithClose('" + strLink + "','search', '35%', '85%');")

        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteRule()
        GridBindRule()
    End Sub

    Sub BindSubjects()

        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            If ddlGrade.SelectedValue <> "" Then
                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")
                str_query = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                          & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                          & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + ddlAcademicYear.SelectedValue _
                          & " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1)
            Else
                ddlSubjects.Items.Clear()
                ddlSubjects.Items.Insert(0, New ListItem("All", ""))
            End If

            If str_query <> "" Then
                str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlSubjects.DataSource = ds
                    ddlSubjects.DataTextField = "SBG_DESCR"
                    ddlSubjects.DataValueField = "SBG_ID"
                    ddlSubjects.DataBind()
                End If

                ddlSubjects.Items.Insert(0, New ListItem("All", ""))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try


    End Sub

    Protected Sub ddlruledeleteAcdYr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlruledeleteAcdYr.SelectedIndexChanged
        BindTerm()
        ddlGrade = PopulateGrade(ddlGrade, ddlruledeleteAcdYr.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)
        BindSubjects()
        updateSubjectURL()
    End Sub


    'Optioncopy tab

 
    Sub Get_Optioncopyfrom_grade()
        chklist_gradefrom.Items.Clear()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid").ToString(), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddloptionscopyfromYR.SelectedValue, SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GRADES_FROM_ACDID]", Param)
            Dim dt As DataTable = DS.Tables(0)
            chklist_gradefrom.DataSource = dt
            chklist_gradefrom.DataTextField = "GRM_GRD_ID"
            chklist_gradefrom.DataValueField = "GRM_GRD_ID"
            chklist_gradefrom.DataBind()
            '   getsections()
            ''----Remove the item name sa selected in grade From
            For Each item As ListItem In chklist_gradefrom.Items
                Dim grade As String = item.Text + ","
                If item.Text = hdf_SelRsmDescr.Value Then
                    item.Enabled = False
                    item.Attributes.Add("style", "display:none")
                End If
            Next
            chklist_gradefrom.Focus()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        End Try
    End Sub

    Protected Sub ddloptionscopyfromYR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddloptionscopyfromYR.SelectedIndexChanged
        ddloptionscopyfromYR.Focus()
        ' chklist_gradefrom.Items.Clear()
        Call Get_Optioncopyfrom_grade()
        MaintainScrollPositionOnPostBack = True
    End Sub

    Protected Sub ddloptionscopyToYR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddloptionscopyToYR.SelectedIndexChanged
        ddloptionscopyToYR.Focus()
        MaintainScrollPositionOnPostBack = True
    End Sub

    Protected Sub btncopy_Click(sender As Object, e As EventArgs) Handles btncopy.Click
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim stransgrade As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Param(2) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid").ToString(), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_COPYFROM", ddloptionscopyfromYR.SelectedValue, SqlDbType.BigInt)
            Param(2) = Mainclass.CreateSqlParameter("@ACD_COPYTO", ddloptionscopyToYR.SelectedValue, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[saveSUBJECTCOPY]", Param)
            strans.Commit()

            Dim gradeselectednext As String = ""
            Dim gradeselected As String = ""
            Dim selectindex As Int16 = 0
            Dim count As Integer = chklist_gradefrom.Items.Count
            If (chklist_gradefrom.Items.Count > 0) Then
                For Each item As ListItem In chklist_gradefrom.Items
                    If (item.Selected = True) Then
                        If (gradeselected = "") Then
                            gradeselected = item.Text
                        Else
                            gradeselected = gradeselected + "," + item.Text
                        End If
                    End If
                Next
            End If
            If (chklist_gradefrom.Items.Count > 0) Then
               


                selectindex = chklist_gradefrom.SelectedIndex

                For i As Integer = selectindex To count - chklist_gradefrom.SelectedIndex

                    If (gradeselectednext = "") Then
                        gradeselectednext = chklist_gradefrom.Items(i + 1).Text
                    Else

                        gradeselectednext = gradeselectednext + "," + chklist_gradefrom.Items(i + 1).Text

                   
                    End If
                Next

            End If
            objConn.Close()

            Dim str_conn1 As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


            Dim objConn1 As New SqlConnection(str_conn1)
            objConn1.Open()
            stransgrade = objConn1.BeginTransaction
            Dim Parame(3) As SqlParameter
            Parame(0) = Mainclass.CreateSqlParameter("@grade", gradeselected, SqlDbType.VarChar)
            Parame(1) = Mainclass.CreateSqlParameter("@ACD_COPYFROM", ddloptionscopyfromYR.SelectedValue, SqlDbType.BigInt)
            Parame(2) = Mainclass.CreateSqlParameter("@ACD_COPYTO", ddloptionscopyToYR.SelectedValue, SqlDbType.BigInt)
            Parame(3) = Mainclass.CreateSqlParameter("@gradenext", gradeselectednext, SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(str_conn1, "[dbo].[saveSubjectcopyGrade]", Parame)
            stransgrade.Commit()

            chklist_gradefrom.Items.Clear()
        Catch ex As Exception
            strans.Rollback()
            stransgrade.Rollback()
        End Try





    End Sub

    Protected Sub chklist_gradefrom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chklist_gradefrom.SelectedIndexChanged
        Dim count As Integer = 0
        count = count + 1
    End Sub


    'unpublished report

    Sub GetHeaderCount()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If
        Dim str_query As String = "SELECT COUNT(RSD_ID) FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + RSM_ID _
                                 & " AND RSD_bDIRECTENTRY='FALSE'"
        hfHeaderCount.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub


    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + RSM_ID _
                                 & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub BindTermUnpublish()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlUnpubliTerm.DataSource = ds
        ddlUnpubliTerm.DataTextField = "TRM_DESCRIPTION"
        ddlUnpubliTerm.DataValueField = "TRM_ID"
        ddlUnpubliTerm.DataBind()
        Dim li As New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlUnpubliTerm.Items.Add(li)
    End Sub
    Sub BindGradeUnpublish()
        ddlunpubliGrade.Items.Clear()
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + RSM_ID

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If


        str_query += " ORDER BY RSG_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlunpubliGrade.DataSource = ds
        ddlunpubliGrade.DataTextField = "GRM_DISPLAY"
        ddlunpubliGrade.DataValueField = "GRM_GRD_ID"
        ddlunpubliGrade.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlunpubliGrade.Items.Insert(0, li)

    End Sub

    Sub GridBindUnpublish()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,SCT_DESCR,SCT_ID FROM OASIS..GRADE_BSU_M AS A" _
        '                       & " INNER JOIN OASIS..SECTION_M AS B ON A.GRM_ID=B.SCT_GRM_ID " _
        '                       & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID " _
        '                       & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.GRM_GRD_ID=D.SBG_GRD_ID AND " _
        '                       & " A.GRM_STM_ID=D.SBG_STM_ID AND SBG_bREPCRD_DISPLAY='TRUE'" _
        '                       & " WHERE (SELECT COUNT(DISTINCT RST_ID) FROM RPT.REPORT_STUDENT_S WHERE RST_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
        '                       & " AND RST_SBG_ID=D.SBG_ID)=" + hfHeaderCount.Value


        Dim RSM_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If

        If ddlunpubliGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlunpubliGrade.SelectedValue.ToString
        Else
            GRD_ID = "0"
        End If


        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,SCT_DESCR,SCT_ID,GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..SECTION_M AS B ON A.GRM_ID=B.SCT_GRM_ID " _
                        & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID " _
                        & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.GRM_GRD_ID=D.SBG_GRD_ID " _
                        & " INNER JOIN OASIS..GRADE_M AS E ON A.GRM_GRD_ID=E.GRD_ID " _
                        & " WHERE  rtrim(ltrim(upper(SCT_DESCR)))<>'TEMP' AND GRM_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString _
                        & " AND RSG_RSM_ID=" + RSM_ID

        If ddlunpubliGrade.SelectedValue <> "" Then
            str_query += " AND GRM_GRD_ID='" + GRD_ID + "'"
        End If

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND SCT_ID IN(select DISTINCT SCT_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlunpubliAcadyr.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If



        str_query += " ORDER BY GRD_DISPLAYORDER,SCT_DESCR"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGrade.DataSource = ds
        gvGrade.DataBind()

    End Sub

    Sub SaveDataUnpublish()
        Dim i As Integer
        Dim str_query As String
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim lblSctId As Label
        Dim lblGrdId As Label
        Dim txtDate As TextBox

        Dim transaction As SqlTransaction

        For i = 0 To gvGrade.Rows.Count - 1
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    chkPublish = gvGrade.Rows(i).FindControl("chkPublish")
                    chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                    lblGrdId = gvGrade.Rows(i).FindControl("lblGrdId")
                    lblSctId = gvGrade.Rows(i).FindControl("lblSctId")
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")

                    If chkPublish.Checked = True Or chkRelease.Checked = True Then
                        str_query = "EXEC RPT.savePUBLISHRELEASESECTIONWISE " _
                                             & ddlReportCard.SelectedValue.ToString + "," _
                                             & ddlPrintedFor.SelectedValue.ToString + "," _
                                             & ddlunpubliAcadyr.SelectedValue.ToString + "," _
                                             & ddlUnpubliTerm.SelectedValue.ToString + "," _
                                             & "'" + lblGrdId.Text + "'," _
                                             & lblSctId.Text + "," _
                                             & chkPublish.Checked.ToString + "," _
                                             & chkRelease.Checked.ToString + "," _
                                             & IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'") + "," _
                                             & "'" + Session("sUsr_name") + "'"

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"
                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using

        Next


    End Sub
    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGradeUnpublish()
        GridBindUnpublish()
    End Sub

    


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveDataUnpublish()
        GridBindUnpublish()
    End Sub

    Sub CheckUncheck(ByVal gRow As GridViewRow)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim lblGrdId As Label
        Dim lblSctId As Label

        lblGrdId = gRow.FindControl("lblGrdId")
        lblSctId = gRow.FindControl("lblSctId")
        chkPublish = gRow.FindControl("chkPublish")
        chkRelease = gRow.FindControl("chkrelease")


        Dim count As Integer

        str_query = "SELECT COUNT(RPP_ID) FROM RPT.REPORT_STUDENTS_PUBLISH WHERE " _
                  & " RPP_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                  & " AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                  & " AND RPP_SCT_ID=" + lblSctId.Text + " AND RPP_bPUBLISH='TRUE'"
        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        ''If count = 0 Then
        ''    chkPublish.Checked = False
        ''Else
        ''    chkPublish.Checked = True
        ''    'chkPublish.Enabled = False
        ''End If
        'chkPublish.Enabled = False
        If count = 0 Then
            chkPublish.Checked = False
        Else
            chkPublish.Checked = True
            ' chkPublish.Enabled = False
        End If


        str_query = "SELECT COUNT(RPP_ID) FROM RPT.REPORT_STUDENTS_PUBLISH WHERE " _
                          & " RPP_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                          & " AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                          & " AND RPP_SCT_ID=" + lblSctId.Text + " AND RPP_bRELEASEONLINE='TRUE'"
        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            chkRelease.Checked = False
        Else
            chkRelease.Checked = True
        End If


    End Sub

    Protected Sub gvGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrade.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGrade.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblGrdId As Label
                Dim lblSctId As Label
                Dim lblGrade As Label
                Dim lblSection As Label

                With selectedRow
                    lblGrdId = .FindControl("lblGrdId")
                    lblSctId = .FindControl("lblSctId")
                    lblGrade = .FindControl("lblGrade")
                    lblSection = .FindControl("lblSection")
                End With

                Dim url As String
                url = String.Format("~\Curriculum\clmPublishReleaseStudents.aspx?" _
                                   & "&acdid=" + Encr_decrData.Encrypt(ddlunpubliAcadyr.SelectedValue.ToString) _
                                   & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                   & "&sctid=" + Encr_decrData.Encrypt(lblSctId.Text) _
                                   & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                   & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                   & "&trmid=" + Encr_decrData.Encrypt(ddlUnpubliTerm.SelectedValue.ToString) _
                                   & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                                   & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                                   & "&reportcard=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                   & "&schedule=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text) _
                                  & "&MainMnu_code={0}&datamode={1}&viewid=", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGrade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGrade.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            CheckUncheck(e.Row)
        End If
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim txtDate As TextBox
            Dim chkPublish As CheckBox
            Dim chkRelease As CheckBox
            For i = 0 To gvGrade.Rows.Count - 1
                chkPublish = gvGrade.Rows(i).FindControl("chkPublish")
                chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                If chkPublish.Checked = True Or chkRelease.Checked = True Then
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtRelease.Text
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        GridBindUnpublish()
    End Sub


    Protected Sub ddlunpubliAcadyr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlunpubliAcadyr.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGradeUnpublish()
        BindTermUnpublish()
        GridBindUnpublish()
    End Sub

    Protected Sub ddlunpubliGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlunpubliGrade.SelectedIndexChanged
        GridBindUnpublish()
    End Sub



    'TAB: Report Design Linking

    Protected Sub Get_RsmID_FromAcdIDRPTDSG(DrpDwnName As DropDownList)
        DrpDwnName.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Try
            Dim conn As New SqlConnection(str_conn)
            If conn.State = ConnectionState.Closed Then conn.Open()
            Using conn
                Using cmd As New SqlCommand("ACT.usp_GetAllRsmID_From_AcdId", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@intAcdID", SqlDbType.VarChar).Value = ddlacd_rptDesignlinking.SelectedValue.ToString
                    cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
                    Dim Sqlreader As SqlDataReader = cmd.ExecuteReader()
                    If Sqlreader.HasRows Then
                        With DrpDwnName
                            .DataSource = Sqlreader
                            .DataTextField = "RSM_DESCR"
                            .DataValueField = "RSM_ID"
                            .Items.Insert(0, "Please select...")
                            .SelectedIndex = 0
                            .DataBind()
                        End With
                    Else
                        DrpDwnName.Items.Insert(0, "Nothing to display here...")
                        DrpDwnName.SelectedIndex = 0
                    End If
                End Using
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed | " & ex.Message
        End Try
    End Sub

    Private Sub BIND_DESIGNNAME()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim conn As New SqlConnection(str_conn)
            'Dim sqlCon_img As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim Param_img(0) As SqlParameter
            Param_img(0) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(conn, "[OASIS_CURRICULUM].dbo.[RPTDSGLINKING_DESIGNNAME]", Param_img)
            dt = ds.Tables(0)
            '  If dt.Rows.Count > 0 Then
            ddlRptDLnk_DSGNAME.Items.Clear()
            ddlRptDLnk_DSGNAME.DataSource = ds.Tables(0)
            ddlRptDLnk_DSGNAME.DataTextField = "DEV_REPORT_NAME"
            ddlRptDLnk_DSGNAME.DataValueField = "DEV_ID"
            ddlRptDLnk_DSGNAME.DataBind()
            Dim li As New ListItem
            li.Text = ""
            li.Value = ""
            ddlRptDLnk_DSGNAME.Items.Insert(0, li)
        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
    Protected Sub ddlacd_rptDesignlinking_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlacd_rptDesignlinking.SelectedIndexChanged
        ddlRptDLnk_RPTNAME.Items.Clear()
        ddlRptDLnk_RPTNAME.Focus()

        Call Get_RsmID_FromAcdIDRPTDSG(ddlRptDLnk_RPTNAME)
    End Sub
    Private Sub ClearRptLINKING()
        ddlRptDLnk_DSGNAME.Items.Clear()
        BIND_DESIGNNAME()
        ddlRptDLnk_RPTNAME.Items.Clear()
        Call Get_RsmID_FromAcdIDRPTDSG(ddlRptDLnk_RPTNAME)
    End Sub

    Protected Sub btnRPTDSGLNK_SAVE_Click(sender As Object, e As EventArgs) Handles btnRPTDSGLNK_SAVE.Click
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim Param(3) As SqlParameter
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        strans = objConn.BeginTransaction
        Param(0) = Mainclass.CreateSqlParameter("@RSM_DEV_ID", Convert.ToInt32(ddlRptDLnk_DSGNAME.SelectedValue), SqlDbType.Int)
        Param(1) = Mainclass.CreateSqlParameter("@RSM_ID", Convert.ToInt32(ddlRptDLnk_RPTNAME.SelectedValue), SqlDbType.Int)
        Param(2) = Mainclass.CreateSqlParameter("@RSM_BSU_ID", CStr(Session("sBsuid")), SqlDbType.VarChar)
        Param(3) = Mainclass.CreateSqlParameter("@RSM_ACD_ID", Convert.ToInt32(ddlacd_rptDesignlinking.SelectedValue), SqlDbType.VarChar)
        SqlHelper.ExecuteNonQuery(str_conn, "[OASIS_CURRICULUM].dbo.[RPTDSGLINKING_UPD_REPORT_SETUP_M]", Param)
        strans.Commit()
        lblError.Text = "Updated Successfully"
        Call ClearRptLINKING()
    End Sub
End Class
