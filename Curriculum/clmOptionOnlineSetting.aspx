﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmOptionOnlineSetting.aspx.vb" Inherits="Curriculum_clmOptionOnlineSetting"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>  Option Online Setting
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" 
        cellpadding="0" cellspacing="0" width="100%">
      
        <tr >
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" >
                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                   <%-- <tr>
                     <td  class="title-bg" colspan="6">
                            OPTION ONLINE SETTING
                        </td></tr>--%>
                    <tr>
                        <td align="left" width="20%">
                        <span class="field-label">Academic Year </span>                 
                        </td>
                      
                        <td >
                          <asp:DropDownList ID="ddlAcademicYear"  AutoPostBack="true" runat="server">
                            </asp:DropDownList> 
                        </td>

                        <td colspan="2" ></td>
                        </tr>
                    <tr>
                        <td  align="left">
                        <span class="field-label">Grade </span>                 
                        </td>
                        
                        <td align="left">
                          <asp:DropDownList ID="ddlGrade"  AutoPostBack="true" runat="server">
                            </asp:DropDownList> 
                        </td>
                        <td class="matters" align="left">
                            <span class="field-label">Option</span>
                        </td>
                        <td align="left">
                          <asp:DropDownList ID="ddlOption" AutoPostBack="true" runat="server">
                            </asp:DropDownList> 
                        </td>
                    </tr>

                    <tr><td colspan="4"></td></tr>
                  <tr >
                        <td colspan="4" align="center" >
                            <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                 PageSize="20" Width="100%" BorderStyle="None">
                                <RowStyle  CssClass="griditem"  />
                                <EmptyDataRowStyle  />
                                <Columns>
                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAlId" runat="server" Text='<%# Bind("SGO_ALLOCATEDTO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Subject">
                                     <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Allocated To">
                                    <ItemStyle HorizontalAlign="Center" />
                                     <ItemTemplate>
                                         <asp:DropDownList ID="ddlAllocated" runat="server" >
                                         <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                         <asp:ListItem Text="BOYS" Value="BOYS"></asp:ListItem>
                                         <asp:ListItem Text="GIRLS" Value="GIRLS"></asp:ListItem>
                                         </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <SelectedRowStyle  />
                                <HeaderStyle    />
                                <EditRowStyle  />
                                <AlternatingRowStyle   CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                  </tr>
                          
                    </tr>
                    <tr><td colspan="4" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" /></td></tr>
                </table>
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>
