<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentCommentsview.aspx.vb" Inherits="Curriculum_rptStudentCommentsview" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
    function confirm_delete()
    {
      if (confirm("You are about to delete this record.Do you want to proceed?")==true)
        return true;
      else
        return false;
     }//GRADE
    function GetPopUp(id)
       {     
            //alert(id);
            var sFeatures;
            sFeatures="dialogWidth: 429px; ";
            sFeatures+="dialogHeight: 345px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           if(id==1)//Accadamic Year
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=ACCYEAR","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_ACD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            
          else if(id==2)//Grade
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=GRADE","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_GRD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            
          
            
          
           else if(id==6)//Report Setup
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUP","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_RPT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            else if(id==7)//Report Schedule
           {
                var RPTId
                RPTId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSCH&RPTID="+RPTId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SCH_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==8)//Student Info
           {
                var GradeId
                GradeId=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs="+GradeId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==9)//Comment Category Info
           {
               
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_CMT_CAT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==10)//Comments List
           {
                var CmtCatId
                CmtCatId=document.getElementById('<%=H_CMT_CAT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTS&CAT_ID="+CmtCatId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_CMT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==11)//Report SetUp_D List
           {
                var RSetupId
                RSetupId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUPD&SETUP_ID="+RSetupId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SETUP.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            else if(id==12)
            {
                var RSetupId
                var AllSubjects
                RSetupId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                AllSubjects=0;
                //RSetupRsdId=document.getElementById('<%=H_SCH_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUPHEADER&SETUP_ID="+RSetupId+"&ALL_SUB="+AllSubjects+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SETUP.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            else if(id==13)
            {
                var GrdId
                var AcdId
                
                GrdId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                AcdId=document.getElementById('<%=H_ACD_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=GROUP_GRADE&GRADE_ID="+GrdId+"&ACD_ID="+AcdId+"","", sFeatures)
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
        }//CMTSCAT
        
        
      
  function GetStudentValues()
    {
    alert("test");
    //alert(document.getElementById(txt1[0]).value);
       for(i=0;i<txt1.length;i++)
        {
         if(document.getElementById(txt1[i]).value !=='')
          {  
           alert(document.getElementById(txt1[i]).value);
          }
        }
    }
    
        
    </script>


    <table align="center" cellpadding="5" cellspacing="0" class="BlueTable">
        <tr class="title" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                View Report Comments</td>
        </tr>
        <tr valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                </asp:Label><asp:ValidationSummary id="ValidationSummary1" runat="server">
                </asp:ValidationSummary></td>
        </tr>
        <tr class="subheader_img" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblHeader" runat="server" Text="View Report Comments"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Academic Year</td>
            <td align="left" class="matters">
                :</td>
            <td align="left" class="matters" colspan="3"><asp:DropDownList id="ddlAcdID" runat="server" Width="174px">
            </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Grade</td>
            <td align="left" class="matters" colspan="1">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtGrade" runat="server" Enabled="False" Width="164px"></asp:TextBox>&nbsp;
                <asp:ImageButton id="imgGrade" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(2)" OnClick="imgGrade_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Report</td>
            <td align="left" class="matters">
                :</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox id="txtReportId" runat="server" Enabled="False" Width="169px"></asp:TextBox>
                <asp:ImageButton id="imgReport" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(6)" OnClick="imgReport_Click">
                </asp:ImageButton></td>
            <td align="left" class="matters" colspan="1">
                Report Header</td>
            <td align="left" class="matters" colspan="1">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtRepHeader" runat="server" Enabled="False" Width="169px">
                </asp:TextBox>
                <asp:ImageButton id="ImgRepHeader" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(12)" OnClick="ImgRepHeader_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="8">
                &nbsp;<asp:Button id="btnView" runat="server" CssClass="button" onclick="btnView_Click1"
                    Text="View Students Comments" Width="164px" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="height: 23px">
                
                <asp:GridView id="gvStudents" runat="server"
                    EmptyDataText="No Data Found for selected critirea" Width="598px" AutoGenerateColumns="False" EnableTheming="True"  CssClass="gridstyle" OnRowDataBound="gvStudents_RowDataBound">
                    <columns>
<asp:TemplateField HeaderText="StudentNo"><HeaderTemplate>
<TABLE style="WIDTH: 61%"><TBODY><TR><TD align=center><asp:Label id="lblStud_N0" runat="server" Text="StudentNo" CssClass="gridheader_text" __designer:wfdid="w70"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px"><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD style="WIDTH: 82px"><DIV id="Div2" class="chromestyle"><UL><LI><A href="#" rel="dropmenu2"><IMG id="mnu_2_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></A> </LI></UL></DIV></TD><TD style="WIDTH: 100px"><asp:TextBox id="txtStud_No" runat="server" Width="100px" __designer:wfdid="w71"></asp:TextBox></TD><TD style="WIDTH: 100px" vAlign=middle><asp:ImageButton id="btnSearchStud_No" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w72" OnClick="btnSearchStud_No_Click"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblStudNo" runat="server" Text='<%# Bind("StudentNo") %>' __designer:wfdid="w193"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="StudentID"><ItemTemplate>
<asp:Label id="lblStudId" runat="server" Text='<%# Bind("StudentId") %>' __designer:wfdid="w144"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="StudentName"><HeaderTemplate>
<TABLE style="WIDTH: 61%"><TBODY><TR><TD align=center><asp:Label id="lblStud_Name" runat="server" Text="StudentName" CssClass="gridheader_text" __designer:wfdid="w74"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px"><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD style="WIDTH: 82px"><DIV id="Div1" class="chromestyle"><UL><LI><A href="#" rel="dropmenu2"><IMG id="mnu_3_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></A> </LI></UL></DIV></TD><TD style="WIDTH: 100px"><asp:TextBox id="txtStud_Name" runat="server" Width="100px" __designer:wfdid="w75"></asp:TextBox></TD><TD style="WIDTH: 100px" vAlign=middle><asp:ImageButton id="btnSearchStud_Name" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w76" OnClick="btnSearchStud_Name_Click"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblStudName" runat="server" Text='<%# Bind("StudentName") %>' __designer:wfdid="w73"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Comments"><ItemTemplate>
<asp:PlaceHolder id="ph1" runat="server" __designer:wfdid="w186"></asp:PlaceHolder> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Comments"><ItemTemplate>
<asp:Label id="lblComments" runat="server" Text="View" ForeColor="Red" Width="45px" __designer:wfdid="w194"></asp:Label> <asp:Panel id="Panel1" runat="server" __designer:wfdid="w195"><TABLE style="HEIGHT: 151px" class="BlueTable" cellSpacing=0 cellPadding=5 align=center><TBODY><TR><TD class="subheader_img" align=left colSpan=6></TD></TR><TR><TD class="matters" align=left colSpan=6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD class="matters" align=left colSpan=6><asp:TextBox id="txtComments" runat="server" Text='<%# Bind("COMMENTS") %>' SkinID="MultiText_Large" Width="311px" Height="80px" __designer:wfdid="w196" ReadOnly="True"></asp:TextBox></TD></TR></TBODY></TABLE></asp:Panel> <TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:TextBox id="txtCmntsShort" runat="server" Text='<%# Bind("COMMENTS") %>' __designer:wfdid="w192"></asp:TextBox></TD></TR></TBODY></TABLE><ajaxToolkit:CollapsiblePanelExtender id="Extend1" runat="server" __designer:wfdid="w198" TextLabelID="lblComments" TargetControlID="Panel1" ScrollContents="true" ExpandedText="Hide Comments" ExpandedSize="240" ExpandControlID="lblComments" CollapsedText="View Comments" CollapsedSize="0" Collapsed="true" CollapseControlID="lblComments" AutoExpand="False" AutoCollapse="False"></ajaxToolkit:CollapsiblePanelExtender> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <headerstyle cssclass="gridheader_pop" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView>
                
                
                
            </td>
        </tr>
        <tr>
    </table>
    <asp:HiddenField id="H_ACD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_GRD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_GRP_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_RPT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SCH_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_STU_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_CMT_CAT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SBJ_GRD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_CMT_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_SETUP" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_RPF_ID" runat="server">
    </asp:HiddenField>
</asp:Content>
       

