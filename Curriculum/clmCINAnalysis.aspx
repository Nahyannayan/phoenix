﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCINAnalysis.aspx.vb" Inherits="Curriculum_clmCINAnalysis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">




    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"> <span class="field-label">Academic Year</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" />
                        </td>
                        <td align="left" valign="middle" width="20%"> <span class="field-label">Grade</span>
                        </td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>

                    <tr>
                        <td align="left" valign="middle"> <span class="field-label">Section</span>
                        </td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="105px">
                            </asp:DropDownList>
                        </td>

                        <td align="left" valign="middle"> <span class="field-label">Subject</span>
                        </td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSub" runat="server" AutoPostBack="True"
                                Width="172px">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">Target</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTarget" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left"> <span class="field-label">Selection</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTargetSetup" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">Prediction</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPrediction" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left"> <span class="field-label">Selection</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPredictionSetup" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>

                    <tr>
                        <td align="left"> <span class="field-label">Report Card</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left"> <span class="field-label">Selection</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlReportCardSetup" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>




                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            &nbsp;
                <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download in PDF" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>

        </div>
    </div>
</asp:Content>
