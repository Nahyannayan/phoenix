<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGradeSlabDetails.aspx.vb" Inherits="CUR_GradeSlabDetails" Theme="General" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
    
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Grade Slab Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

         <table cellpadding="0" cellspacing="0">
        <tr runat="server" >
        <td align="left">
         <asp:Label id="lblError" runat="server" EnableViewState="False"
        SkinID="LabelError"></asp:Label>
    <asp:ValidationSummary id="ValidationSummary1" runat="server" ValidationGroup="SUBERROR">
    </asp:ValidationSummary>
    <asp:ValidationSummary id="ValidationSummary2" runat="server" ValidationGroup="MAINERROR">
    </asp:ValidationSummary>
         </td></tr>
        </table>
        <table align="center" border="0"  cellpadding="0" cellspacing="0" width ="100%">
        <%-- <tr class="subheader_img" valign="top">
            <td align="left" colspan="8" class="title-bg">
                <asp:Label id="lblHeader" runat="server" Text="Grade Slab Details"></asp:Label></td>
        </tr>--%>
       <tr>
           <td align="left"  width="20%">
               <span class="field-label">Grade Slab</span>
           </td>
          
           <td align="left" class="matters" colspan="3">
               <asp:DropDownList id="ddlGradeSlab" runat="server">
               </asp:DropDownList>
               <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Grade Slab"
                   ValidationGroup="MAINERROR" ControlToValidate="ddlGradeSlab">*</asp:RequiredFieldValidator></td>
       </tr>
       <%--<tr>
           <td align="left" class="title-bg" colspan="8"> Grade Slab Details</td>
       </tr>--%>
       <tr>
           <td align="left" Width="20%">
                <span class="field-label">Slab Name</span></td>
          
           <td align="left" colspan="3">
               <asp:TextBox id="txtSlabName" runat="server"></asp:TextBox>
               <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Slab name required"
                   ValidationGroup="SUBERROR" ControlToValidate="txtSlabName">*</asp:RequiredFieldValidator></td>
       </tr>
        <tr>
            <td align="left" Width="20%">
                <span class="field-label">Min. Mark</span></td>
           
            <td align="left">
                <asp:TextBox id="txtMinMark" runat="server" CssClass="inputbox" ></asp:TextBox><asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server"
                    ControlToValidate="txtMinMark" ErrorMessage="Min. Mark Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
            <td align="left" >
                <span class="field-label">Max. Mark</span></td>
           
            <td align="left">
                <asp:TextBox id="txtMaxMark" runat="server" CssClass="inputbox"></asp:TextBox><asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtMaxMark" ErrorMessage="Max. Mark Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
        </tr>
       <tr>
           <td align="center" class="matters" colspan="4">
               <asp:Button id="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
               <asp:Button id="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
           </td>
       </tr>
            <tr>
                <td align="left" class="matters" colspan="4">
            <asp:GridView id="gvGradeSlabDetails" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" >
                <columns>
<asp:TemplateField Visible="False" HeaderText="slabId">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
                <asp:Label id="lblFEE_ID" runat="server" Text='<%# Bind("GSD_ID") %>'></asp:Label> 
                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Slab Name">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
                <asp:Label id="lblFeeType" runat="server" Text='<%# bind("GSD_DESC") %>'></asp:Label>
                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Min. Mark">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
<ItemTemplate>
                <asp:Label id="lblAmount" runat="server" Text='<%# bind("GSD_MIN_MARK") %>'></asp:Label>
                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Max. Mark"><ItemTemplate>
                        <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("GSD_MAX_MARK") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Edit">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
                <asp:LinkButton id="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
                <asp:LinkButton id="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton> 
                
</ItemTemplate>
</asp:TemplateField>
</columns>
            </asp:GridView></td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="4">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" OnClick="btnAdd_Click" />
                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="MAINERROR" />
                    <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" /></td>
            </tr>
        </table> 

                </div>
            </div>
    </div>


</asp:Content>

