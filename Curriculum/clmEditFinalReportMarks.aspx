<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmEditFinalReportMarks.aspx.vb" Inherits="Curriculum_clmEditFinalReportMarks" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            var hv = document.getElementById('h_' + obj);



            if (div.style.display == "none") {
                div.style.display = "inline";
                hv.value = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                hv.value = "none"
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";

            }
        }

        function showDiv(obj) {
            var div = document.getElementById(obj);
            /*div.style.display = "inline";
            var img = document.getElementById('img' + obj);
            img.src="../Images/Expand_Button_white_Down.jpg" 
            img.alt = "Click to close";*/



            var hv = document.getElementById('h_' + obj);
            hv.value = obj;
            var display;

            if (hv.value == "inline") {
                //div.style.display = "inline";
                display = "display.inline";
            }
            else {
                //div.style.display = "none";
                display = "display.none;"
            }
            return display;
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Edit Final Report Marks"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%" align="center" cellpadding="0" cellspacing="0">

                    <tr>

                        <td align="left" class="title">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblTC" runat="server" align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Report Card</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Select Report Schedule</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                </tr>

                                <tr>

                                    <td align="left"><span class="field-label">Select Grade</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left"><span class="field-label">Select Section</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Student Name</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>

                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>



                                    <td align="center" colspan="4" valign="top">


                                        <asp:GridView ID="gvStud" Width="100%" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="5" AllowPaging="true">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("STU_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'alt');">
                                                            <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%--         </td></tr>--%>
                                                        <tr>
                                                            <td colspan="100%">

                                                                <div id="div<%# Eval("STU_ID") %>" style="display: inline-block; width: 100%;">

                                                                    <asp:GridView ID="gvSubjects" CssClass="table table-bordered" runat="server" Width="100%"
                                                                        AutoGenerateColumns="false" EmptyDataText="No subjects for this grade."
                                                                        OnRowDataBound="gvSubjects_RowDataBound">

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblStuIdSub" runat="server" Text='<%# Bind("ssd_Stu_ID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("Sbg_ID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblEntryType" runat="server" Text='<%# Bind("Sbg_Entrytype") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Subject" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Sbg_descr") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>



                                                                            <asp:TemplateField HeaderStyle-Width="70%" ItemStyle-Width="70%">
                                                                                <HeaderTemplate>
                                                                                    <asp:DataList runat="server" ID="dlHeader" Width="100%" RepeatDirection="Horizontal" RepeatLayout="table">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# Bind("RSD_Header") %>' ID="lblHeader" Width="100%" />
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:DataList runat="server" ID="dlMarks" Enabled="FALSE" Width="100%" RepeatDirection="Horizontal" RepeatLayout="table">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox runat="server" Text='<%# Bind("Marks") %>' ID="txtMark" Width="100%"></asp:TextBox>
                                                                                            <asp:Label runat="server" Text='<%# Bind("Rsd_id") %>' ID="lblRsdId" Visible="False" Width="100%" />
                                                                                            <asp:Label runat="server" Text='<%# Bind("Marks") %>' ID="lblMark" Visible="False" Width="100%" />
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                                                                </HeaderTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>
                                                                        <RowStyle CssClass="griditem" />
                                                                        <HeaderStyle />
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>



                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                            <input id="h_DivStyle" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>


</asp:Content>

