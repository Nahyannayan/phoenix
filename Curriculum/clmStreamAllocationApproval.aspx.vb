Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Curriculum_clmStreamAllocationApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
       
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330129") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    tblTC.Rows(8).Visible = False
                    tblTC.Rows(9).Visible = False
                    tblTC.Rows(10).Visible = False
                    'tblTC.Rows(6).Visible = False
                    'tblTC.Rows(7).Visible = False

                    BindSection()

                    BindStream(ddlStream1)
                    BindStream(ddlStream2)
                    BindStream(ddlStream3)

                    BindOptions(ddlStream1.SelectedValue.ToString, gvOptions1)
                    BindOptions(ddlStream2.SelectedValue.ToString, gvOptions2)
                    BindOptions(ddlStream3.SelectedValue.ToString, gvOptions3)

                    BindAlloted()
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)

    End Sub



#Region "Private Methods"
    Private Sub BindStream(ByVal ddlStream As DropDownList)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT STM_ID, STM_DESCR FROM VW_STREAM_M WHERE STM_ID IN(2,3,4)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlStream.Items.Insert(0, li)
    End Sub
    Protected Sub ddlStream1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream1.SelectedValue.ToString, gvOptions1)
    End Sub

    Protected Sub ddlStream2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream2.SelectedValue.ToString, gvOptions2)
    End Sub

    Protected Sub ddlStream3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream3.SelectedValue.ToString, gvOptions3)
    End Sub
    Protected Sub ddlAllocatedStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlAllocatedStream.SelectedValue.ToString, gvAllocatedOption)
    End Sub
    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblChoice As Label
            Dim lblOptId As Label
            lblOptId = e.Row.FindControl("lblOptId")
            lblChoice = e.Row.FindControl("lblChoice")
            If lblChoice.Text = "choice1" Then
                Dim ddlChoice1Option As DropDownList
                ddlChoice1Option = e.Row.FindControl("ddlChoice1Option")
                BindSubjects(lblOptId.Text, ddlChoice1Option, ddlStream1.SelectedValue.ToString)
            ElseIf lblChoice.Text = "choice2" Then
                Dim ddlChoice2Option As DropDownList
                ddlChoice2Option = e.Row.FindControl("ddlChoice2Option")
                BindSubjects(lblOptId.Text, ddlChoice2Option, ddlStream2.SelectedValue.ToString)
            ElseIf lblChoice.Text = "choice3" Then
                Dim ddlChoice3Option As DropDownList
                ddlChoice3Option = e.Row.FindControl("ddlChoice3Option")
                BindSubjects(lblOptId.Text, ddlChoice3Option, ddlStream3.SelectedValue.ToString)
            End If
        End If
    End Sub
    Protected Sub gvAllocatedOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblOptId As Label
            lblOptId = e.Row.FindControl("lblOptId")

            Dim ddlAllocatedOption As DropDownList
            ddlAllocatedOption = e.Row.FindControl("ddlAllocatedOption")
            BindSubjects(lblOptId.Text, ddlAllocatedOption, ddlAllocatedStream.SelectedValue.ToString)

        End If
    End Sub
    Sub BindOptions(ByVal stm_id As String, ByVal gvOptions As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='11'" _
                                & " AND SBG_STM_ID=" + stm_id _
                                & " AND SBG_ACD_ID=" + Session("next_acd_id") _
                                & " ORDER BY OPT_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvOptions.DataSource = ds
        gvOptions.DataBind()
        gvOptions.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

    Sub BindSubjects(ByVal OptId As String, ByVal ddlSubject As DropDownList, ByVal stm_id As String)

        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,SBG_DESCR FROM " _
                      & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                      & " SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                      & " C.SBG_ID WHERE SBG_GRD_ID='11'" _
                      & " AND SBG_STM_ID=" + stm_id _
                      & " AND SBG_ACD_ID=" + Session("NEXT_ACD_ID") _
                      & " AND SGO_OPT_ID=" + OptId

        str_query += " ORDER BY SBG_DESCR "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

    End Sub

    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        Dim sChoice1 As String = ""
        Dim sChoice2 As String = ""
        Dim sChoice3 As String = ""
        ViewState("slno") = 0





        If txtRecords.Text <> "" Then
            str_query = "SELECT TOP " + txtRecords.Text + " STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR,STU_BSU_ID,SCIENCE,MATHS,ENGLISH,SOCIAL,CGPA,OVERALL," _
                     & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,SCIENCE_MARKS,MATHS_MARKS,ENGLISH_MARKS,SOCIAL_MARKS," _
                     & " CASE  WHEN CHOICE1 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE1 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C1COLOR,   " _
                     & " CASE  WHEN CHOICE2 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE2 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C2COLOR,   " _
                     & " CASE  WHEN CHOICE3 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE3 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C3COLOR   " _
                     & " FROM [STREAM].[FN_GetStudentStreamAllocationDetails]('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + "," + Session("Next_ACD_ID") + ",'10')" _
                     & " WHERE 1=1"
        Else
            str_query = "SELECT  STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR,STU_BSU_ID,SCIENCE,MATHS,ENGLISH,SOCIAL,CGPA,OVERALL," _
                   & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,SCIENCE_MARKS,MATHS_MARKS,ENGLISH_MARKS,SOCIAL_MARKS," _
                   & " CASE  WHEN CHOICE1 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE1 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C1COLOR,   " _
                   & " CASE  WHEN CHOICE2 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE2 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C2COLOR,   " _
                   & " CASE  WHEN CHOICE3 LIKE 'SCIENCE%' THEN '#975AAD' WHEN CHOICE3 LIKE 'COMMERCE%' THEN '#5C7C48' ELSE '#E9627A' END AS C3COLOR   " _
                   & " FROM [STREAM].[FN_GetStudentStreamAllocationDetails]('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + "," + Session("Next_ACD_ID") + ",'10')" _
                   & " WHERE 1=1"
        End If



        If txtName.Text <> "" Then
            str_query += " AND STU_NAME LIKE '%" + txtName.Text + "%'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If

        If ddlSection.SelectedValue <> "" Then
            str_query += " AND SCT_DESCR='" + ddlSection.SelectedItem.Text + "'"
        End If

        If txtScience.Text <> "" Then
            str_query += " AND SCIENCE_MARKS>='" + txtScience.Text + "'"
        End If


        If txtMaths.Text <> "" Then
            str_query += " AND MATHS_MARKS>='" + txtMaths.Text + "'"
        End If

        If txtEnglish.Text <> "" Then
            str_query += " AND ENGLISH_MARKS>='" + txtEnglish.Text + "'"
        End If

        If txtSocial.Text <> "" Then
            str_query += " AND SOCIAL_MARKS>='" + txtSocial.Text + "'"
        End If

        If txtCGPA.Text <> "" Then
            str_query += " AND CGPA>=" + txtCGPA.Text
        End If

        If txtOverAll.Text <> "" Then
            str_query += " AND OVERALL>=" + txtOverAll.Text
        End If

        If rdAllocated.Checked = True Then
            str_query += " and ISNULL(ALLOTEDSTREAM,'')<>''"
        End If

        If rdNotAllocated.Checked = True Then
            str_query += " and ISNULL(ALLOTEDSTREAM,'')=''"
        End If


        str_query += getSearchOptions(gvOptions1, "choice1", ddlStream1)
        str_query += getSearchOptions(gvOptions2, "choice2", ddlStream2)
        str_query += getSearchOptions(gvOptions3, "choice3", ddlStream3)


        str_query += " ORDER BY OVERALL DESC,CGPA DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()

        End If




    End Sub

    Function getSearchOptions(ByVal gvOption As GridView, ByVal choice As String, ByVal ddlStream As DropDownList) As String
        Dim i As Integer
        Dim ddlSubject As DropDownList
        Dim lblOptId As Label
        Dim strSql As String = ""
        Dim strSreach As String = ""
        Dim searchCount As String = 0

        For i = 0 To gvOption.Rows.Count - 1
            lblOptId = gvOption.Rows(i).FindControl("lblOptId")
            If choice = "choice1" Then
                ddlSubject = gvOption.Rows(i).FindControl("ddlChoice1Option")
            ElseIf choice = "choice2" Then
                ddlSubject = gvOption.Rows(i).FindControl("ddlChoice2Option")
            ElseIf choice = "choice3" Then
                ddlSubject = gvOption.Rows(i).FindControl("ddlChoice3Option")
            End If

            If ddlSubject.SelectedValue <> "0" Then
                If strSreach <> "" Then
                    strSreach += ","
                End If
                strSreach += "'" + lblOptId.Text + "-" + ddlSubject.SelectedValue.ToString + "'"
                searchCount += 1
            End If
        Next
        If strSreach <> "" Then
            strSql += " AND STU_ID IN(SELECT SRO_STU_ID FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_CHOICE='" + choice + "'  " _
                       & " AND SRO_STM_ID=" + ddlStream.SelectedValue.ToString + " AND " _
                       & " CONVERT(VARCHAR(100),SRO_OPT_ID)+'-'+CONVERT(VARCHAR(100),SRO_SBG_ID) IN(" + strSreach + ")" _
                       & " GROUP BY SRO_STU_ID HAVING COUNT(SRO_STU_ID)>=" + searchCount.ToString + ")"


        ElseIf ddlStream.SelectedValue.ToString <> "0" Then
            strSql += " AND STU_ID IN(SELECT SRO_STU_ID FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_CHOICE='" + choice + "'  " _
                     & " AND SRO_STM_ID=" + ddlStream.SelectedValue.ToString + ")"
        End If

        Return strSql
    End Function

    Function getCheckedLists(ByVal lst As CheckBoxList) As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To lst.Items.Count - 1
            If lst.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += "'" + lst.Items(i).Value + "'"
            End If
        Next

        Return str
    End Function

    Sub GetSubjectKeysAlloted(ByVal ltprocess As Literal, ByVal stmId As String)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder
        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try
            Dim strSQL As String = "SELECT SBG_DESCR,OPT_DESCR,COUNT(STO_ID) FROM SUBJECTS_GRADE_S AS A " _
                              & "  INNER JOIN STREAM.STREAM_OPTION_ALLOTED AS B ON A.SBG_ID=B.STO_SBG_ID" _
                              & " INNER JOIN SUBJECTGRADE_OPTIONS_S ON SBG_ID=SGO_SBG_ID AND STO_OPT_ID=SGO_OPT_ID " _
                              & " INNER JOIN OPTIONS_M ON SGO_OPT_ID=OPT_ID" _
                              & " WHERE SBG_ACD_ID=" + Session("NEXT_ACD_ID") + " AND SBG_GRD_ID='11' AND SBG_STM_ID=" + stmId _
                              & " GROUP BY SBG_DESCR,OPT_DESCR "
            reader = SqlHelper.ExecuteReader(conn, CommandType.Text, strSQL)
            sb.AppendLine("<table class=""BlueTableView"" border=1 bordercolor=#1B80B6>")
            sb.AppendLine("<tr ><td class=""matters"">Subjects</td><td class=""matters"">Option</td><td class=""matters"">Allotted</td></tr>")
            While reader.Read
                sb.AppendLine("<tr ><td class=""matters"">" + reader.GetString(0) + "</td><td class=""matters"">" + reader.GetString(1) + "</td><td class=""matters"" align=center>" + reader.GetValue(2).ToString + "</td></tr>")
            End While
            reader.Close()
            sb.AppendLine("</table>")
            ltprocess.Text = sb.ToString

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetSubjectKeysRequested(ByVal ltrprocess As Literal, ByVal stream As String)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder
        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Dim choice1 As Integer = 0
        Dim choice2 As Integer = 0
        Dim choice3 As Integer = 0

        Try
            Dim str_query As String = "SELECT SBG_DESCR,OPT_DESCR,(SELECT COUNT(SRO_ID) FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_SBG_ID=A.SBG_ID AND SRO_OPT_ID=C.OPT_ID AND SRO_CHOICE='CHOICE1') AS CHOICE1," _
                                  & "(SELECT COUNT(SRO_ID) FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_SBG_ID=A.SBG_ID AND SRO_OPT_ID=C.OPT_ID AND SRO_CHOICE='CHOICE2') AS CHOICE2, " _
                                  & "(SELECT COUNT(SRO_ID) FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_SBG_ID=A.SBG_ID AND SRO_OPT_ID=C.OPT_ID AND SRO_CHOICE='CHOICE3') AS CHOICE3 " _
                                  & " FROM SUBJECTS_GRADE_S AS A  INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON SBG_ID=SGO_SBG_ID " _
                                  & " INNER JOIN OPTIONS_M AS C ON OPT_ID=SGO_OPT_ID " _
                                  & " INNER JOIN VW_STREAM_M ON SBG_STM_ID=STM_ID WHERE SBG_ACD_ID=" + Session("NEXT_ACD_ID") + " AND SBG_GRD_ID='11' AND STM_DESCR='" + stream + "'"


            reader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_query)

            sb.AppendLine("<table class=""BlueTableView"" border=1 bordercolor=#1B80B6>")
            sb.AppendLine("<tr ><td class=""matters"">Subject</td><td class=""matters"">Option</td><td class=""matters"">Choice 1</td><td class=""matters"">Choice 2</td><td class=""matters"">Choice 3</td></tr>")
            While reader.Read
                sb.AppendLine("<tr><td class=""matters"" >" + reader.GetString(0) + "</td><td class=""matters"" >" + reader.GetString(1) + "</td><td class=""matters"" align=center>" + reader.GetValue(2).ToString + "</td><td class=""matters"" align=center>" + reader.GetValue(3).ToString + "</td><td class=""matters"" align=center>" + reader.GetValue(4).ToString + "</td></tr>")
                choice1 += reader.GetValue(2)
                choice2 += reader.GetValue(3)
                choice3 += reader.GetValue(4)
            End While
            reader.Close()
            sb.AppendLine("<tr><td class=""matters"">Total</td><td>&nbsp;</td><td class=""matters"" align=center>" + choice1.ToString + "</td><td class=""matters"" align=center>" + choice2.ToString + "</td><td class=""matters"" align=center>" + choice3.ToString + "</td></tr>")

            sb.AppendLine("</table>")
            ltrprocess.Text = sb.ToString
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + Session("CURRENT_ACD_ID") _
                   & " AND GRM_GRD_ID='10'"


        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Insert(0, li)
    End Sub

    Function BindOptions(ByVal lst As CheckBoxList) As CheckBoxList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        lst.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lst.DataSource = ds
        lst.DataTextField = "SGM_DESCR"
        lst.DataValueField = "SGM_ID"
        lst.DataBind()
        Return lst
    End Function

    Sub BindAlloted()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID,(SELECT COUNT(SAA_ID) FROM  " _
                                     & " STREAM.STREAM_ALLOTED WHERE SAA_STM_ID=A.STM_ID AND SAA_ACD_ID=" + Session("NEXT_ACD_ID") _
                                     & " ) AS STM_COUNT" _
                                     & " FROM OASIS..STREAM_M AS A" _
                                     & " INNER JOIN OASIS..GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID" _
                                     & " WHERE GRM_GRD_ID='11' AND GRM_ACD_ID='" + Session("NEXT_ACD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlStreams.DataSource = ds
        dlStreams.DataBind()


        dlRStreams.DataSource = ds
        dlRStreams.DataBind()
    End Sub

    Function BindGridOption(ByVal ddlChoice As DropDownList) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlChoice.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11' "
        str_query += " UNION ALL "

        str_query += "SELECT SGM_ID,SGM_DESCR+'('+SBG_SHORTCODE+')' AS SGM_DESCR FROM SUBJECTOPTION_GROUP_M  AS A " _
                    & " INNER JOIN SUBJECTOPTION_GROUP_S AS B ON A.SGM_ID=B.SGS_SGM_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGS_SBG_ID=C.SBG_ID" _
                    & " WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                    & " AND SGM_GRD_ID='11' AND SGS_bOPTIONAL=1"

        str_query = "SELECT * FROM (" + str_query + ") P ORDER BY SGM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlChoice.DataSource = ds
        ddlChoice.DataTextField = "SGM_DESCR"
        ddlChoice.DataValueField = "SGM_DESCR"
        ddlChoice.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlChoice.Items.Insert(0, li)
        Return ddlChoice
    End Function

    Sub SaveData(ByVal mode As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim lblStuId As Label
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim j As Integer
        Dim ddlAllocatedSubject As DropDownList
        Dim lblOptId As Label


        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            If chkSelect.Checked = True Then

                str_query = "EXEC STREAM.saveSTREAMALLOCATION_ALLOTMENT " _
                          & "@SAA_STU_ID=" + lblStuId.Text + "," _
                          & "@SAA_ACD_ID=" + Session("NEXT_ACD_ID") + "," _
                          & "@SAA_STM_ID=" + ddlAllocatedStream.SelectedValue.ToString + "," _
                          & "@MODE='" + mode + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                For j = 0 To gvAllocatedOption.Rows.Count - 1
                    ddlAllocatedSubject = gvAllocatedOption.Rows(j).FindControl("ddlAllocatedOption")
                    lblOptId = gvAllocatedOption.Rows(j).FindControl("lblOptId")
                    If ddlAllocatedSubject.SelectedValue <> "0" Then
                        str_query = "EXEC STREAM.saveSTREAMOPTIONALLOTMENT " _
                                  & "@STO_STU_ID=" + lblStuId.Text + "," _
                                  & "@STO_ACD_ID=" + Session("Next_ACD_ID") + "," _
                                  & "@STO_STM_ID=" + ddlAllocatedStream.SelectedValue.ToString + "," _
                                  & "@STO_SBG_ID=" + ddlAllocatedSubject.SelectedValue.ToString + "," _
                                  & "@STO_OPT_ID=" + lblOptId.Text + "," _
                                  & "@MODE='" + mode + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next
            End If

        Next
    End Sub
    Sub ExportExcel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If txtRecords.Text <> "" Then
            str_query = "SELECT TOP " + txtRecords.Text + " STU_NO AS STUDENTID,STU_NAME as NAME,SCT_DESCR as SECTION,SCIENCE_MARKS,SCIENCE SCIENCE_GRADE,MATHS_MARKS,MATHS MATHS_GRADE,ENGLISH_MARKS,ENGLISH ENGLISH_GRADE,SOCIAL_MARKS,SOCIAL SOCIAL_GRADE,CGPA,OVERALL," _
                     & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM FROM  " _
                     & " [STREAM].[FN_GetStudentStreamAllocationDetails]('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + "," + Session("Next_ACD_ID") + ",'10')" _
                     & " WHERE 1=1"
        Else
            str_query = "SELECT STU_NO AS STUDENTID,STU_NAME as NAME,SCT_DESCR as SECTION,SCIENCE_MARKS,SCIENCE SCIENCE_GRADE,MATHS_MARKS,MATHS MATHS_GRADE,ENGLISH_MARKS,ENGLISH ENGLISH_GRADE,SOCIAL_MARKS,SOCIAL SOCIAL_GRADE,CGPA,OVERALL," _
                   & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM FROM  " _
                   & " [STREAM].[FN_GetStudentStreamAllocationDetails]('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + "," + Session("Next_ACD_ID") + ",'10')" _
                   & " WHERE 1=1"
        End If



        If txtName.Text <> "" Then
            str_query += " AND STU_NAME LIKE '%" + txtName.Text + "%'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If

        If ddlSection.SelectedValue <> "" Then
            str_query += " AND SCT_DESCR='" + ddlSection.SelectedItem.Text + "'"
        End If

        If txtScience.Text <> "" Then
            str_query += " AND SCIENCE='" + txtScience.Text + "'"
        End If


        If txtMaths.Text <> "" Then
            str_query += " AND MATHS='" + txtMaths.Text + "'"
        End If

        If txtEnglish.Text <> "" Then
            str_query += " AND ENGLISH='" + txtEnglish.Text + "'"
        End If

        If txtSocial.Text <> "" Then
            str_query += " AND SOCIAL='" + txtSocial.Text + "'"
        End If

        If txtCGPA.Text <> "" Then
            str_query += " AND CGPA>=" + txtCGPA.Text
        End If

        If txtOverAll.Text <> "" Then
            str_query += " AND OVERALL>=" + txtOverAll.Text
        End If

        str_query += getSearchOptions(gvOptions1, "choice1", ddlStream1)
        str_query += getSearchOptions(gvOptions2, "choice2", ddlStream2)
        str_query += getSearchOptions(gvOptions3, "choice3", ddlStream3)


        str_query += " ORDER BY OVERALL DESC,CGPA DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)




        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        'Dim ef As ExcelFile = New ExcelFile

        'Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        'ws.InsertDataTable(dt, "A1", True)

        'ef.SaveXls(tempFileName)
        'Dim bytes() As Byte = File.ReadAllBytes(tempFileName)

        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)

        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        System.IO.File.Delete(tempFileName)
    End Sub

#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ViewState("slno") = 0

        tblTC.Rows(8).Visible = True
        tblTC.Rows(9).Visible = True
        tblTC.Rows(10).Visible = True
        'tblTC.Rows(6).Visible = True
        'tblTC.Rows(7).Visible = True

        BindStream(ddlAllocatedStream)
        BindOptions(ddlAllocatedStream.SelectedValue.ToString, gvAllocatedOption)
        GridBind()
    End Sub


    Protected Sub btnAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllot.Click
        SaveData("Add")
        GridBind()
        BindAlloted()
    End Sub

    Protected Sub btnDeAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeAllot.Click
        SaveData("Delete")
        GridBind()
        BindAlloted()
    End Sub

    Protected Sub dlStreams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlStreams.ItemDataBound
        Dim lblStmId As Label
        Dim ltProcess As Literal
        lblStmId = e.Item.FindControl("lblStmId")
        ltProcess = e.Item.FindControl("ltProcess")
        GetSubjectKeysAlloted(ltProcess, lblStmId.Text)
    End Sub



    Protected Sub dlRStreams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlRStreams.ItemDataBound
        Dim lnkRStream As LinkButton
        Dim ltProcess As Literal
        lnkRStream = e.Item.FindControl("lnkRStream")
        ltProcess = e.Item.FindControl("ltRProcess")
        GetSubjectKeysRequested(ltProcess, lnkRStream.Text)
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportExcel()
    End Sub
End Class

