﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmOptionOnlineSetting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C1000140") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindOption()
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                   & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                   & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                   & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                   & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                   & " ORDER BY GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindOption()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT OPT_ID,OPT_DESCR FROM OPTIONS_M WHERE OPT_BSU_ID='" + Session("SBSUID") + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOption.DataSource = ds
        ddlOption.DataTextField = "OPT_DESCR"
        ddlOption.DataValueField = "OPT_ID"
        ddlOption.DataBind()
    End Sub

    Sub GridBind()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT SBG_ID,SBG_DESCR,ISNULL(SGO_ALLOCATEDTO,'ALL') SGO_ALLOCATEDTO FROM SUBJECTS_GRADE_S AS A " _
                 & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID " _
                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND SBG_GRD_ID='" + grade(0) + "'" _
                 & " AND SGO_OPT_ID=" + ddlOption.SelectedValue.ToString _
                 & " AND SBG_bOPTIONAL=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        gvOption.DataBind()

        Dim i As Integer
        Dim lblAlId As Label
        Dim ddlAllocated As DropDownList

        For i = 0 To gvOption.Rows.Count - 1
            lblAlId = gvOption.Rows(i).FindControl("lblAlId")
            ddlAllocated = gvOption.Rows(i).FindControl("ddlAllocated")

            If Not ddlAllocated.Items.FindByValue(lblAlId.Text) Is Nothing Then
                ddlAllocated.Items.FindByValue(lblAlId.Text).Selected = True
            End If

        Next


    End Sub
   
    Sub SaveData()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim i As Integer

        Dim lblSbgId As Label
        Dim ddlAllocated As DropDownList


        For i = 0 To gvOption.Rows.Count - 1
            With gvOption.Rows(i)
                lblSbgId = .FindControl("lblSbgId")
                ddlAllocated = .FindControl("ddlAllocated")
            End With
            str_query = "EXEC saveOPTIONONLINESETTING" _
                                    & " @OPT_ID =" + ddlOption.SelectedValue.ToString + "," _
                                    & " @SBG_ID =" + lblSbgId.Text + "," _
                                    & " @SGO_ALLOCATEDTO='" + ddlAllocated.SelectedValue + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindOption()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOption.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub
End Class
