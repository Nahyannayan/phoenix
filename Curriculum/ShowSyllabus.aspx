<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowSyllabus.aspx.vb" Inherits="Curriculum_ShowSyllabus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title>Show Topics</title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">        
            <asp:GridView ID="gvempname" runat="server" AllowPaging="True"
                CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="False" PageSize="12">
                <Columns>
                    <asp:TemplateField HeaderText="Syllabus Id">
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="lblsylid" runat="server" Text='<%# Bind("SYM_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Syllabus Name">
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <HeaderTemplate>
                            Syllabus Name
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnksyllabusname" runat="server" CommandName="Select" Text='<%# Bind("SYM_DESCR") %>'></asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <HeaderStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
            <asp:HiddenField ID="h_SelectedId" runat="server" />        
    </form>
</body>
</html>
