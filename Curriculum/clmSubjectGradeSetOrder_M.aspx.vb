Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSubjectGradeSetOrder_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("SelectedRow") = -1
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblacademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                     PopulateSubjectTree()
                    ' BindSubjectList()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub PopulateSubjectTree()
        Dim ds As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT A.SBG_ID AS SBG_ID,A.SBG_DESCR AS SBG_DESCR ,A.SBG_SHORTCODE AS SBG_SHORTCODE,A.SBG_PARENT_ID AS PARENT,CASE A.SBG_PARENT_ID " _
                                  & " WHEN 0 THEN NULL ELSE B.SBG_ID END AS PARENT_ID,A.SBG_ORDER  FROM SUBJECTS_GRADE_S AS A " _
                                  & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON A.SBG_PARENT_ID=B.SBG_ID AND A.SBG_GRD_ID=B.SBG_GRD_ID " _
                                  & " AND A.SBG_STM_ID=B.SBG_STM_ID AND A.SBG_ACD_ID=B.SBG_ACD_ID " _
                                  & " WHERE A.SBG_GRD_ID='" + hfGRD_ID.Value + "' AND A.SBG_STM_ID =" + hfSTM_ID.Value + " AND A.SBG_ACD_ID=" + hfACD_ID.Value _
                                  & " ORDER BY A.SBG_ORDER"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ds.DataSetName = "SUBJECTS"
        ds.Tables(0).TableName = "SUBJECT"
        Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("SUBJECT").Columns("SBG_ID"), ds.Tables("SUBJECT").Columns("PARENT_ID"), True)
        relation.Nested = True
        ds.Relations.Add(relation)
        XmlSubjects.Data = ds.GetXml()
        tvSubjects.DataBind()
        '  tvSubjects.Nodes(0).Expand()

    End Sub
    'Sub BindSubjectList()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  WHERE " _
    '                           & " SBG_GRD_ID='" + hfGRD_ID.Value + "' AND SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_STM_ID=" + hfSTM_ID.Value
    '    If ddlSubject.SelectedValue = 0 Then
    '        str_query += " AND SBG_PARENT_ID=0"
    '    Else
    '        str_query += " AND SBG_PARENT_ID=" + ddlSubject.SelectedValue.ToString
    '    End If
    '    str_query += " ORDER BY SBG_ORDER"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    lstSubjects.DataSource = ds
    '    lstSubjects.DataTextField = "SBG_DESCR"
    '    lstSubjects.DataValueField = "SBG_ID"
    '    lstSubjects.DataBind()
    'End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim strValues As String()
        Dim strValue As String()


        If lstValues.Value.Length <> 0 Then
            strValues = lstValues.Value.Split("|")
            For i = 0 To lstSubjects.Items.Count - 1
                strValue = strValues(i).Split("$")
                str_query = "exec saveSUBJECTGRADE_SETORDER " + strValue(1) + "," + (i + 1).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
        Else
            For i = 0 To lstSubjects.Items.Count - 1
                str_query = "exec saveSUBJECTGRADE_SETORDER " + lstSubjects.Items(i).Value + "," + (i + 1).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
        End If
    End Sub

    Sub BindSubjectList(ByVal id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  WHERE " _
                               & " SBG_GRD_ID='" + hfGRD_ID.Value + "' AND SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_STM_ID=" + hfSTM_ID.Value
        If id = "0" Then
            str_query += " AND SBG_PARENT_ID=0"
        Else
            str_query += " AND SBG_PARENT_ID=" + id
        End If
        str_query += " ORDER BY SBG_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSubjects.DataSource = ds
        lstSubjects.DataTextField = "SBG_DESCR"
        lstSubjects.DataValueField = "SBG_ID"
        lstSubjects.DataBind()
    End Sub

#End Region

    

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If lstSubjects.Items.Count <> 0 Then
                SaveData()
                BindSubjectList(hfParent.Value)
                PopulateSubjectTree()
            Else
                lblError.Text = "No subjects to sort"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub tvSubjects_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvSubjects.SelectedNodeChanged
        Try
            Dim node As TreeNode = tvSubjects.SelectedNode
            If node.Parent Is Nothing Then
                BindSubjectList(0)
                hfParent.Value = 0
            Else
                BindSubjectList(node.Value)
                hfParent.Value = node.Value
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

   
    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
End Class
