<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_ScheduleAllocate.aspx.vb" Inherits="Curriculum_clmSyllabus_ScheduleAllocate" title="Untitled Page" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script type= "text/javascript">    
    
     
    </script>
    
         <table cellpadding="0" cellspacing="0" style="width: 100%">
             <tr id="Tr1" runat="server" class="title">
                 <td align="left" style="width: 486px">
                     <asp:Literal id="ltHeader" runat="server" Text="SubTopics Allocation"></asp:Literal></td>
             </tr>
        <tr id="Tr2" runat="server" >
        <td align="left" style="width: 486px">
         <asp:Label id="lblError" runat="server" EnableViewState="False"
        SkinID="LabelError"></asp:Label>
    <asp:ValidationSummary id="ValidationSummary1" runat="server" ValidationGroup="SUBERROR">
    </asp:ValidationSummary>&nbsp;
         </td></tr>
         </table>
        <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 91%">
            <tr  valign="top" style="display:none">
                <td align="left" colspan="8" style="height: 18px">
                </td>
            </tr>
         <tr class="subheader_img" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblHeader" runat="server" Text="SubTopics Allocation"></asp:Label></td>
        </tr>
       <tr>
           <td align="left" class="matters" style="width: 66px">
                Academic Year</td>
           <td align="left" class="matters" style="font-size: 9pt">
               :</td>
           <td align="left" class="matters" colspan="3" style="width: 159px"><asp:TextBox id="txtAccYear" runat="server" Width="135px" Enabled="False">
           </asp:TextBox></td>
           <td align="left" class="matters" colspan="1">
               Term</td>
           <td align="left" class="matters" colspan="1" style="width: 14px">
               :</td>
           <td align="left" class="matters" colspan="1" style="width: 175px"><asp:TextBox id="txtTerm" runat="server" Width="156px" Enabled="False">
           </asp:TextBox></td>
       </tr>
            <tr>
                <td align="left" class="matters" style="width: 66px">
                Grade</td>
                <td align="left" class="matters" style="font-size: 9pt">
                    :</td>
                <td align="left" class="matters" colspan="3" style="width: 159px"><asp:TextBox id="txtGrade" runat="server" Width="135px" Enabled="False">
                </asp:TextBox></td>
                <td align="left" class="matters" colspan="1">
                Subject</td>
                <td align="left" class="matters" colspan="1" style="width: 14px">
                    :</td>
                <td align="left" class="matters" colspan="1" style="width: 175px">
                    <asp:TextBox id="txtSubject" runat="server" Width="156px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 66px">
                    Syllabus</td>
                <td align="left" class="matters" style="font-size: 9pt">
                    :</td>
                <td align="left" class="matters" colspan="3" style="width: 159px">
                    <asp:TextBox id="txtSyllabus" runat="server" Width="135px" Enabled="False">
                    </asp:TextBox></td>
                <td align="left" class="matters" colspan="1">
                    Group</td>
                <td align="left" class="matters" colspan="1" style="width: 14px">
                    :</td>
                <td align="left" class="matters" colspan="1" style="width: 175px">
                    &nbsp;<asp:TextBox id="txtGroup" runat="server">
                    </asp:TextBox></td>
            </tr>
       <tr style="display:none">
           <td align="left" colspan="8" class="subheader_img"> <asp:Label id="Label1" runat="server" Text="Details.." Width="105px"></asp:Label></td>
       </tr>
            <tr style="display:none">
                <td align="left" class="matters" style="width: 66px">
                    Parent Topic</td>
                <td align="left" class="matters">
                    :</td>
                <td align="left" class="matters" colspan="3" style="width: 159px">
                    &nbsp;</td>
                <td align="left" class="matters" colspan="1">
                Topic Name<asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label>
            </td>
                <td align="left" class="matters" colspan="1" style="width: 14px">
                    :</td>
                <td align="left" class="matters" colspan="1" style="width: 175px">
                    &nbsp;</td>
            </tr>
        <tr style="display:none">
            <td align="left" class="matters" style="width: 66px; height: 18px;">
                    From Date</td>
            <td align="left" class="matters" style="height: 18px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px; height: 18px;" >
                &nbsp;</td>
            <td align="left" class="matters" colspan="1" style="height: 18px">
                    To Date</td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 18px;">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 18px; width: 175px;">
                    </td>
        </tr>
            <tr style="display:none">
                <td align="left" class="matters" style="height: 24px; width: 66px;">
                Total Hrs</td>
                <td align="left" class="matters" style="height: 24px">
                    :</td>
                <td align="left" class="matters" colspan="3" style="height: 24px; width: 159px;">
                    &nbsp;</td>
                <td align="left" class="matters" colspan="1" style="height: 24px">
                    </td>
                <td align="left" class="matters" colspan="1" style="height: 24px; width: 14px;">
                    </td>
                <td align="left" class="matters" colspan="1" style="height: 24px; width: 175px;">
                    &nbsp;</td>
            </tr>
       <tr style="display:none">
           <td align="left" class="matters" colspan="8" style="text-align: right">
               <asp:Button id="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR"  Width="48px" />
               <asp:Button id="btnSubCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnSubCancel_Click" />
           </td>
       </tr>
            <tr align=center>
                <td align="center" class="subheader_img" colspan="8">
                    <asp:Label id="Label2" runat="server" Text="Topic Details" Font-Bold="True" Font-Size="Larger" ForeColor="White" Width="185px" Height="13px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" class="matters" colspan="8" style="text-align: right">
            <asp:GridView id="gvSyllabus" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" SkinID="GridViewNormal" Width="100%" AllowPaging="True" OnPageIndexChanging="gvSyllabus_PageIndexChanging">
                <columns>
<asp:TemplateField HeaderText="SydId"><ItemTemplate>
<asp:Label id="lblSydId" runat="server" Text='<%# Bind("SydId") %>' __designer:wfdid="w13"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Topics">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblSylDescr" runat="server" Text='<%# Bind("SYLDescr") %>' __designer:wfdid="w17"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Planned StartDate">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblPlnStDate" runat="server" Text='<%# Bind("PlnStdate","{0:dd/MMM/yyyy}") %>' __designer:wfdid="w18"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Planned EndDate">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblPlnEndDate" runat="server" Text='<%# Bind("PlnEndDate","{0:dd/MMM/yyyy}") %>' __designer:wfdid="w19"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Planned Hrs">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblPlnHrs" runat="server" Text='<%# Bind("PlnHrs") %>' __designer:wfdid="w20"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Actual StartDate">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD></TD><TD style="WIDTH: 100px"><asp:TextBox id="txtActStartDate" runat="server" Width="89px" __designer:wfdid="w17"></asp:TextBox></TD><TD align=left><asp:ImageButton id="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" __designer:dtid="27021597764223030" __designer:wfdid="w18">
                    </asp:ImageButton></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE><ajaxToolkit:CalendarExtender id="Cl1" runat="server" __designer:dtid="27021597764223071" __designer:wfdid="w19" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar" TargetControlID="txtActStartDate"></ajaxToolkit:CalendarExtender> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Actual EndDate">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD></TD><TD style="WIDTH: 100px"><asp:TextBox id="txtActEndDate" runat="server" Width="89px" __designer:wfdid="w20"></asp:TextBox></TD><TD align=left><asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" __designer:dtid="27021597764223030" __designer:wfdid="w21"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE><ajaxToolkit:CalendarExtender id="Cl2" runat="server" __designer:dtid="27021597764223072" __designer:wfdid="w22" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtActEndDate"></ajaxToolkit:CalendarExtender> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Actual Hrs">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:TextBox id="txtActHrs" runat="server" Width="64px" __designer:wfdid="w23"></asp:TextBox> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Completed">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
<asp:CheckBox id="chkComplete" runat="server" __designer:wfdid="w25" OnCheckedChanged="chkComplete_CheckedChanged" AutoPostBack="True"></asp:CheckBox>&nbsp; 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete"><ItemTemplate>
<asp:LinkButton id="lnkDelete" onclick="lnkDelete_Click" runat="server" __designer:wfdid="w14">Delete</asp:LinkButton> <ajaxToolkit:ConfirmButtonExtender id="ConfirmButtonExtender1" runat="server" __designer:wfdid="w15" TargetControlID="lnkDelete" ConfirmText="Do you want to delete this SubTopic Allocation?"></ajaxToolkit:ConfirmButtonExtender>
</ItemTemplate>
</asp:TemplateField>
</columns>
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="8">
                    &nbsp;&nbsp;
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="MAINERROR" Width="46px" />&nbsp;
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table> 
    <asp:HiddenField id="h_TERMID" runat="server" />
    <asp:HiddenField ID="h_ACCID" runat="server" />     
   <asp:HiddenField id="h_SUBJID" runat="server" /><asp:HiddenField id="h_SyllabusId" runat="server" />
    <asp:HiddenField id="h_GRDID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_GRPID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SYDID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_ParentId" runat="server">
    </asp:HiddenField>
    &nbsp;<asp:HiddenField id="h_SYTID" runat="server"></asp:HiddenField>
    &nbsp; &nbsp;
    &nbsp; &nbsp;
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />
    &nbsp; &nbsp;&nbsp;
</asp:Content>

