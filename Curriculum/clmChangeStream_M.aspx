<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeStream_M.aspx.vb" Inherits="Curriculum_clmChangeStream_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i> Change Stream Approval
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                 HeaderText="You must enter a value in the following fields:" 
                                ValidationGroup="groupM1" Style="text-align: left" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" ><asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td  valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <%--  <tr class="title-bg">
                                    <td align="left" colspan="9" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                CHANGE STREAM APPROVAL</span></font></td>
                                </tr>--%>
                                <tr>

                                    <td align="left" >
                                        <asp:Label ID="lblStu" runat="server" Text="Student No" class="field-label"></asp:Label></td>
                                 
                                    <td align="left" >
                                        <asp:Label ID="lblStuNo" runat="server" class="field-value"></asp:Label></td>

                                    <td align="left" >
                                        <asp:Label ID="lblNtext" runat="server" Text="Student Name" class="field-label"></asp:Label></td>
                            
                                    <td align="left" >
                                        <asp:Label ID="lblStuName" runat="server"  class="field-value"></asp:Label></td>

                                </tr>

                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="lblGr" runat="server" Text="Stream" class="field-label"></asp:Label></td>
                               
                                    <td align="left" >
                                        <asp:Label ID="lblGrade" runat="server"  class="field-value"></asp:Label></td>

                                    <td align="left" >
                                        <asp:Label ID="lblStre" runat="server" Text="Old Stream" class="field-label"></asp:Label></td>
                              
                                    <td align="left" >
                                        <asp:Label ID="lblStream" runat="server"  class="field-value"></asp:Label></td>
                                </tr>



                                <tr >

                                    <td align="left" >
                                        <asp:Label ID="lblstm" runat="server" Text="Requested Stream"  class="field-label"></asp:Label></td>
                               
                                    <td align="left" >
                                        <asp:Label ID="lblNewStream" runat="server"  class="field-value"></asp:Label></td>

                                    <td align="left" >
                                        <asp:Label ID="Label2" runat="server" Text="Requested Subjects"  class="field-label" ></asp:Label></td>
                                  
                                    <td align="left">
                                        <asp:Label ID="lblSubjects" runat="server"  class="field-value"></asp:Label></td>


                                </tr>



                                <tr >

                                    <td align="left" >
                                        <asp:Label ID="lbl23" runat="server" Text="Remarks"  class="field-label"></asp:Label></td>
                 
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblRemarks" runat="server"  class="field-value" Text="-"></asp:Label></td>


                                </tr>

                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label1" runat="server" Text="Select new section"   class="field-label"></asp:Label></td>
              
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSection" runat="server" ></asp:DropDownList></td>

                                    <td align="left" >   <span class="field-label">Approval Date </span></td>
                  
                                    <td align="left" >
                                        <asp:TextBox ID="txtDate" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                    </td>

                                </tr>

                                 <tr>
                            <td colspan="4" class="mt-5">&nbsp;</td>
                                     </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="groupM1" TabIndex="7" />&nbsp;
                    <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" TabIndex="7"  CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="7" CausesValidation="False" /></td>
                                </tr>

                            </table>




                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCS_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTM_NEWID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSHF_ID" runat="server"></asp:HiddenField>


                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                PopupButtonID="txtDate" TargetControlID="txtDate" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgDate" TargetControlID="txtDate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfDate" runat="server" ControlToValidate="txtDate"
                                Display="None" ErrorMessage="Please enter the field Approval Date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>

