﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CLMStud_uploadEdit_frame.aspx.vb" Inherits="Curriculum_CLMStud_uploadEdit_frame" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Upload Frame</title>


   <%-- <link href="../cssfiles/jcrop.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/ControlStyle.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js" type="text/javascript"></script>

    <script src="js/jquery.Jcrop.js" type="text/javascript"></script>

    <script src="js/crop.js" type="text/javascript"></script>--%>

    <!-- Bootstrap core JavaScript-->
                
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="js/crop.js" type="text/javascript"></script>
    
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet"/>


    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->
     
<script language="javascript" type="text/javascript">


 //Trim the input text
  function Trim(input)
  {
    var lre = /^\s*/; 
    var rre = /\s*$/; 
    input = input.replace(lre, ""); 
    input = input.replace(rre, ""); 
    return input; 
   }
 
   // filter the files before Uploading for text file only  
   function CheckUploadedFile() 
   {
        var file = document.getElementById('<%=fileload.ClientID%>');
        var fileName=file.value;    
        //var objFSO = new ActiveXObject("Scripting.FileSystemObject");
//            var files;
//            var size;
//             var path = file.value;    
        //Checking for file browsed or not 
        if (Trim(fileName) =='' )
        {
            alert("Please select a file to upload!!!");
            file.focus();
            return false;
        }
 
       //Setting the extension array for diff. type of text files 
         
       var extType= new Array(".jpg");

       //getting the file name
       while (fileName.indexOf("\\") != -1)
         fileName = fileName.slice(fileName.indexOf("\\") + 1);
 
 
       //Getting the file extension                     
       var ext = fileName.slice(fileName.lastIndexOf(".")).toLowerCase();
 
       //matching extension with our given extensions.
    for (var i = 0; i < extType.length; i++) 
        {

         if (extType[i] == ext) 
         { 
           return true;
         }
         
           }  

//        //alert(path);

//        files = objFSO.getFile(path);
//      

//        size = files.size ; // This size will be in Bytes

//  // We are converting it to KB as below

//        alert('File Size is : ' + files.size /1024 +' KB'); 

         alert("Please only upload files that end in types:  "+(extType.join("  ").toUpperCase())  
           +  "\nPlease select a new "
           + "file to upload and submit again.");
           file.focus();
           return false;                
   }  
 
    </script>
</head>
<body onload="Init();">
    <form id="form1" runat="server">
        <table style="margin-top: -1px;" width="100%">
            <tr>
                <td align="left">
                    <table align="center" cellpadding="2" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td align="left" colspan="4">
                                <br />
                                <asp:Label ID="lblErrorLoad" runat="server" CssClass="error"
                                    EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Academic Year</span> </td>
                            <td align="left">
                               <span class="field-value"><asp:Literal ID="ltACY" runat="server"></asp:Literal></span>
                            </td>
                            <td align="left" ><span class="field-label">Grade &amp; Section</span> </td>
                            <td align="left" >
                                <span class="field-value"><asp:Literal ID="ltGRD_SCT" runat="server"></asp:Literal></span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Report</span> </td>
                            <td align="left">
                                <span class="field-value"><asp:Literal ID="ltReport" runat="server"></asp:Literal></span> 
                            </td>
                            <td align="left" ><span class="field-label">Report Schedule</span></td>
                            <td align="left" >
                               <span class="field-value"> <asp:Literal ID="ltSchedule" runat="server"></asp:Literal></span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <asp:RadioButton ID="rbCrop" runat="server" GroupName="curr" CssClass="field-label"
                                    Text="Crop the Original Image" AutoPostBack="True" ToolTip="Crop image" />
                                <asp:RadioButton ID="rbReduce" runat="server" GroupName="curr" CssClass="field-label"
                                    Text="Reduce image size" AutoPostBack="True"
                                    ToolTip="Reduce image to the predefined file size" />
                                <asp:RadioButton ID="rbUpload" runat="server" AutoPostBack="True" CssClass="field-label"
                                    GroupName="curr" Text="Upload new image"
                                    ToolTip="Upload image with following file type extension jpg only" />
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr align="center" id="treditImg" runat="server">
                <td align="center">
                    <table cellpadding="1" cellspacing="0" style="margin-top: -1px; text-align: center;" width="100%">
                        <tr align="center">
                            <td width="10%">
                                <span class="field-label">X1</span><br />
                                
                                    <input type="text" size="4" id="x1" name="x1" class="coor" style="border: solid 1px  gray; color: Gray;" readonly="readonly" /><br />
                                 <span class="field-label">Y1</span><br />
                               
                                    <input type="text" size="4" id="y1" name="y1" style="border: solid 1px  gray; color: Gray;"
                                        class="coor" readonly="readonly" /><br />
                                 <span class="field-label">X2</span><br />
                               
                                    <input type="text" size="4" id="x2" name="x2" style="border: solid 1px  gray; color: Gray;"
                                        class="coor" readonly="readonly" /><br />
                                 <span class="field-label">Y2</span><br />
                               
                                    <input type="text" size="4" id="y2"
                                        name="y2" style="border: solid 1px  gray; color: Gray;" class="coor" readonly="readonly" /><br />
                                 <span class="field-label">W</span><br />
                                
                                    <input type="text" size="4" id="w" name="w"
                                        class="coor" style="border: solid 1px  gray; color: Gray;" readonly="readonly" /><br />
                                 <span class="field-label">H</span><br />
                               
                                    <input type="text" size="4" id="h" name="h"
                                        class="coor" style="border: solid 1px  gray; color: Gray;" readonly="readonly" />
                            </td>
                            <td style="vertical-align: top;" width="50%">
                                <div style="overflow: visible; border-color:gray ; border-style: solid; border-width: 1px; ">
                                    <div class="msg_header" style="height: 23px; width:100%; margin-top: 0px; vertical-align: middle; background-color: White;">
                                        <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: center;">
                                            <span id="spOrg" runat="server">Original image</span>
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 380px; overflow: auto; padding: 5px; margin-bottom: 5px;"
                                        id="imgContainer">
                                        <img src="" alt="My File" title="Report File" id="originalImage" runat="server" style="left:0 !important;" />
                                    </div>
                                </div>

                            </td>

                            <td width="40%">
                                <div style="overflow: visible; border-color: gray; border-style: solid; border-width: 1px; width:100%;">
                                    <div class="msg_header" width="100%">
                                        <div class="msg" style="text-align: center;">
                                            <span id="spOth" runat="server">Cropped image</span>
                                        </div>
                                    </div>

                                    <div style="width: 100%; height: 380px; overflow: auto; padding: 5px; margin-bottom: 5px;">
                                        <asp:Literal runat="server" ID="lblCroppedImage"></asp:Literal>
                                    </div>
                                </div> 
                            </td>

                        </tr>
                        <tr align="center">
                            <td colspan="4" align="center" style="vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button runat="server" ID="btnCrop" OnClick="btnCrop_Click" Text="Crop"
                         OnClientClick="return ValidateSelected();" Width="97px"
                         CssClass="button" />
                                <asp:Button ID="btnReduce" runat="server" CssClass="button"
                                    Text="Reduce Size" Width="97px" />
                                <asp:Button ID="btnSaveCrop" runat="server" CssClass="button" Text="Save"
                                    Width="97px" />

                            </td>
                        </tr>
                    </table>

                </td>


            </tr>
            <tr id="trUploadImg" runat="server" align="center" >
                <td align="left" >
                    <%--<div  style="overflow: visible; border-color: gray; border-style:solid;border-width:1px;width:380px;">
         <div class="msg_header" style="height:23px;width:100%;margin-top:1px;vertical-align:middle;background-color:White;"> <div class="msg"  style="margin-top:0px;vertical-align:middle; text-align:center;">
           Upload Image</div></div>--%>
                    <asp:FileUpload ID="fileload" runat="server" CssClass="filebutton" Width="350px"
                        Height="20px" />
                    &nbsp;&nbsp;
      <asp:Button ID="btnProcess" runat="server"
          Text="Process file to Upload" CssClass="button" /><br />
                    <div style="padding-top: 6px;">
                        <asp:Literal ID="ltNote" runat="server"></asp:Literal>
                    </div>

                    <%-- </div>--%>

                </td>


            </tr>


        </table>







    </form>
</body>
</html>
