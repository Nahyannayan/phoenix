<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmEditFinalReportComments.aspx.vb" Inherits="Curriculum_clmEditFinalReportComments" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            var hv = document.getElementById('h_' + obj);



            if (div.style.display == "none") {
                div.style.display = "inline";
                hv.value = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                hv.value = "none"
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";

            }
        }

        function showDiv(obj) {
            var div = document.getElementById(obj);
            /*div.style.display = "inline";
            var img = document.getElementById('img' + obj);
            img.src="../Images/Expand_Button_white_Down.jpg" 
            img.alt = "Click to close";*/



            var hv = document.getElementById('h_' + obj);
            hv.value = obj;
            var display;

            if (hv.value == "inline") {
                //div.style.display = "inline";
                display = "display.inline";
            }
            else {
                //div.style.display = "none";
                display = "display.none;"
            }
            return display;
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Edit Final Report Comments
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <table id="tblTC" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="7" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="1"><span style="font-family: Verdana">
                                EDIT FINAL REPORT COMMENTS</span></font></td>
                    </tr>
                                --%>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td colspan="2">&nbsp;</td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select ReportCard</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Report Schedule</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%">
                                        <span class="field-label">Select Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left" width="20%">
                                        <span class="field-label">Select Section</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>


                                <tr>

                                    <td align="left" width="20%">
                                        <span class="field-label">Select Subject</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left" width="20%">
                                        <span class="field-label">Select Group</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGroup" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>


                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>

                                <tr>
                                    <td colspan="4"></td>
                                </tr>

                                <tr>



                                    <td align="center" colspan="4">


                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="5" AllowPaging="true" Width="100%">
                                            <RowStyle />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("STU_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'alt');">
                                                            <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        </td>
                            </tr>
                        <tr>
                            <td colspan="6">

                                <div id="div<%# Eval("STU_ID") %>" style="display: inline-block; width: 100%">

                                    <asp:GridView ID="gvSubjects" runat="server" Width="100%" CssClass="table table-bordered table-row"
                                        AutoGenerateColumns="false" EmptyDataText="No subjects for this grade."
                                        OnRowDataBound="gvSubjects_RowDataBound">

                                        <Columns>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStuIdSub" runat="server" Text='<%# Bind("ssd_Stu_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("Sbg_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSgrId" runat="server" Text='<%# Bind("SSD_SGR_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEntryType" runat="server" Text='<%# Bind("Sbg_Entrytype") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Subject">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Sbg_descr") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Teacher">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTeacher" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Grades">
                                                <ItemTemplate>
                                                    <asp:DataList runat="server" ID="dlGrades" RepeatColumns="1">
                                                        <ItemTemplate>
                                                            <table border="1" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label runat="server" Text='<%# Bind("Rsd_Header") %>' ID="lblHeader"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label runat="server" Text='<%# Bind("COMMENTS") %>' ID="lblGrade"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:DataList runat="server" ID="dlHeader" RepeatColumns="10">
                                                        <ItemTemplate>
                                                            <table border="1">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label runat="server" Text='<%# Bind("RSD_Header") %>' ID="lblHeader" />

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DataList runat="server" ID="dlMarks" RepeatColumns="10" Enabled="FALSE">
                                                        <ItemTemplate>
                                                            <table border="1">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:TextBox runat="server" Text='<%# Bind("COMMENTS") %>' TextMode="MultiLine" SkinID="MultiText_Large" ID="txtCmt" Width="400PX"></asp:TextBox>
                                                                        <asp:Label runat="server" Text='<%# Bind("Rsd_id") %>' ID="lblRsdId" Visible="False" />
                                                                        <asp:Label runat="server" Text='<%# Bind("COMMENTS") %>' ID="lblCmt" Visible="False" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle />
                                        <HeaderStyle />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                <input id="h_DivStyle" runat="server" type="hidden" value="=" />


            </div>
        </div>
    </div>


</asp:Content>

