Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system

Partial Class Curriculum_clmChangeOption_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim subjs As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))

                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblStuName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                    GetRequestInfo()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub


#Region "private Methods"

    Sub GetRequestInfo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim newSubjects As String = ""
        Dim oldSubjects As String = ""

        Dim oldSub As String()
        Dim newSub As String()

        Dim changeOldSub As String = ""
        Dim changeNewSub As String = ""

        Dim str_query As String = "SELECT SCS_ID,SCS_SUBJECTS,subjects=(SELECT ISNULL(STUFF((SELECT ','+SBG_DESCR FROM SUBJECTS_GRADE_S AS D" _
                                & " INNER JOIN  STUDENT_GROUPS_S AS G ON G.SSD_SBG_ID=D.SBG_ID " _
                                & " AND G.SSD_STU_ID=" + hfSTU_ID.Value + " AND D.SBG_bOPTIONAL='TRUE'  AND G.SSD_ACD_ID=" + hfACD_ID.Value _
                                 & " for xml path('')),1,1,'') ,'')) FROM" _
                                 & " STUDENT_CHANGESTREAMREQ_S WHERE SCS_STU_ID=" + hfSTU_ID.Value _
                                 & " AND SCS_ACD_ID=" + hfACD_ID.Value + " AND SCS_APPROVE=0 AND SCS_TYPE='CO'"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        hfSCS_ID.Value = 0
        While reader.Read
            hfSCS_ID.Value = reader.GetValue(0).ToString
            newSubjects = reader.GetString(1)
            oldSubjects = reader.GetString(2)
        End While
        reader.Close()


        'str_query = " as subjects )A"

        'reader = SqlHelper.ExecuteReader(str_query, CommandType.Text)
        'While reader.Read
        '    oldSubjects += reader.GetString(0)
        'End While
        'reader.Close()
        If Not (oldSubjects = "" And newSubjects = "") Then
            oldSub = oldSubjects.Split(",")
            newSub = newSubjects.Split(",")
            Dim i As Integer
            Dim j As Integer
            Dim flag As Boolean = False
            For i = 0 To newSub.Length - 1
                For j = 0 To oldSub.Length - 1
                    If newSub(i).Trim <> oldSub(j).Trim Then
                        flag = False
                    Else
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    If changeNewSub <> "" Then
                        changeNewSub += ","
                    End If
                    changeNewSub += newSub(i)
                End If
                flag = False
            Next

            For i = 0 To oldSub.Length - 1
                For j = 0 To newSub.Length - 1
                    If oldSub(i).Trim <> newSub(j).Trim Then
                        flag = False
                    Else
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    If changeOldSub <> "" Then
                        changeOldSub += ","
                    End If
                    changeOldSub += oldSub(i)
                End If
                flag = False
            Next
            lblSubjects.Text = changeOldSub + " has been changed to " + changeNewSub
        End If
        

        If hfSCS_ID.Value <> 0 Then
            ViewState("datamode") = "edit"
        Else
            ViewState("datamode") = "add"
        End If
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

   
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function SaveData(ByVal approve As Integer) As Boolean
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If approve = 1 Then
                    UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_GROUPS_S", "SSD_ID", "SSD_STU_ID", "SSD_STU_ID=" + hfSTU_ID.Value + " AND SSD_ACD_ID=" + hfACD_ID.Value + " AND SSD_OPT_ID IS NOT NULL")
                    UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_PROMO_S", "STP_ID", "STP_STU_ID", "STP_STU_ID=" + hfSTU_ID.Value + " AND STP_ACD_ID=" + hfACD_ID.Value)
                End If

                Dim str_query As String = "exec saveCHANGEOPTIONAPPROVAL " _
                                         & hfACD_ID.Value + "," _
                                         & hfSTU_ID.Value + "," _
                                         & "'" + hfGRD_ID.Value + "'," _
                                         & hfSCS_ID.Value + "," _
                                         & approve.ToString + "," _
                                         & "'" + IIf(txtDate.Text = "", "01/01/1900", Format(Date.Parse(txtDate.Text), "MM/dd/yyyy")) + "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + hfSTU_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function

#End Region


    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If (txtDate.Text = "") Then
                lblError.Text = "Please select a valid date"
            Else
                If SaveData(1) = True Then
                    lblError.Text = "The optional subjects of  student " + lblStuName.Text + " has been successfully updated"
                    btnReject.Visible = False
                    btnApprove.Visible = False
                    btnCancel.Text = "Close"
                    'btnApprove.Enabled = False
                    'btnReject.Enabled = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            If (txtDate.Text = "") Then
                lblError.Text = "Please select a valid date"
            Else
                If SaveData(2) = True Then
                    lblError.Text = "The request for option change is rejected"
                    btnReject.Visible = False
                    btnApprove.Visible = False
                    btnCancel.Text = "Close"
                    ' btnReject.Enabled = False
                    'btnApprove.Enabled = False
                End If
            End If
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
