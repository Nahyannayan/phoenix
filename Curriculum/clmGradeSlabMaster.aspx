<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGradeSlabMaster.aspx.vb" Inherits="CUR_GRADESLABMASTER" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">   
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="GRADE_SLAB" />
    
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label ID="Label1" runat="server" Text="Grade Slab Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        
        <tr>
            <td align="left" >
                <span class="field-label">Grade Slab Description</span></td>
            
            <td align="left" >
                <asp:TextBox ID="txtGradeSlabDescr" runat="server" CssClass="inputbox" Width="222px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtGradeSlabDescr"
                    ErrorMessage="Grade Slab Description Required" ValidationGroup="GRADE_SLAB">*</asp:RequiredFieldValidator></td>
       
            <td align="left" >
                <span class="field-label">Grade Total Marks</span></td>
           
            <td align="left" >
                <asp:TextBox ID="txtGradeTotalMark" runat="server" CssClass="inputbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGradeTotalMark"
                    ErrorMessage="Total Mark Required" ValidationGroup="GRADE_SLAB">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td class="matters" colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnAdd_Click" Text="Add" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnEdit_Click" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                    Text="Save" ValidationGroup="GRADE_SLAB" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnCancel_Click" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField id="h_GRADE_SLAB_ID" runat="server">
    </asp:HiddenField>

            </div>
        </div>
     </div>
</asp:Content>

