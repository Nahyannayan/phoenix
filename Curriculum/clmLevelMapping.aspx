﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmLevelMapping.aspx.vb" Inherits="Curriculum_clmLevelMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Level Mapping
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" width="100%">

                                <tr>
                                    <td align="left" width="100%" colspan="4">
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" class="matters" width="20%"><span class="field-label">Level</span>
                                    </td>

                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtLevel" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Order</span>
                                    </td>

                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                                    </td>

                                    <td align="left" class="matters"><span class="field-label">Value</span>
                                    </td>

                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4" class="matters">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCopy" runat="server" Text="Copy From Previous Year" CssClass="button" ValidationGroup="groupM1" />
                                    </td>
                                </tr>



                                <tr id="trGrid" runat="server">
                                    <td align="left" colspan="4" class="matters">
                                        <table id="Table4" runat="server" width="100%">
                                            <tr>
                                                <td align="center" class="matters" style="text-align:center">
                                                    <asp:GridView ID="gvLevels" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No records to edit."
                                                        PageSize="20" BorderStyle="None" Width="100%">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Level" Visible="False">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("LMM_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ACDID" Visible="false">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblACDID" runat="server" Text='<%# Bind("LMM_ACD_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Order">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("LML_ORDER")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Level">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("LML_LVL")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Value">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblValue" runat="server" Text='<%# Bind("LML_VALUE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:ButtonField CommandName="edit" HeaderText="edit" Text="edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:TemplateField HeaderText="delete" ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("UNIQUEID") %>'
                                                                        CommandName="delete" Text="delete"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Details will be deleted permanently.Are you sure you want to continue?"
                                                                        runat="server">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                        <RowStyle CssClass="griditem" Wrap="False" />
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfLMM_ID" runat="server" Value="0" />
            </div>
        </div>
    </div>
</asp:Content>

