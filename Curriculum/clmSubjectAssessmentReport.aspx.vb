﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Curriculum_Reports_Aspx_clmSubjectAssessmentReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100193") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindSection()
                    BindReportSchedule()
                    Session("CurrSuperUser") = "Y"
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    ViewState("SelectedRow") = -1

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownReport)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownGraoh)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSection()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ddlGrade.SelectedValue <> "" Then
            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"
            str_query += " ORDER BY SCT_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Insert(0, New ListItem("All", "0"))

    End Sub

    Sub BindReportCard()

        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If Session("sbsuid") = "123004" And Session("clm") = 3 Then
            str_query += " AND (RSM_DESCR LIKE '%KG1%' OR RSM_DESCR LIKE '%KG2%')"
        End If
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()

    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Sub BindReportSchedule()

        Dim grades As String()
        Dim grade As String
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str As String = ""
        Dim i As Integer
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade + "' AND " _
                                & " RSM_ID=" + ddlReportCard.SelectedValue + " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cblReportSchedule.DataSource = ds
        cblReportSchedule.DataTextField = "RPF_DESCR"
        cblReportSchedule.DataValueField = "RPF_ID"
        cblReportSchedule.DataBind()

    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        Dim strRPF As String = ""


        Dim i As Integer
        For i = 0 To cblReportSchedule.Items.Count - 1
            If cblReportSchedule.Items(i).Selected = True Then
                If strRPF <> "" Then
                    strRPF += "|"
                End If
                strRPF += cblReportSchedule.Items(i).Value
            End If
        Next

        Dim strSection As String = ""
        If ddlSection.SelectedIndex = 0 Then
            For i = 1 To ddlSection.Items.Count - 1
                If strSection <> "" Then
                    strSection += "|"
                End If
                strSection += ddlSection.Items(i).Value
            Next
        Else
            strSection = ddlSection.SelectedValue.ToString
        End If

        Dim grades As String()
        Dim grade As String = ""
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue)
        param.Add("@RPF_ID", strRPF)
        param.Add("@GRD_ID", grade)
        param.Add("@SCT_ID", strSection)
        param.Add("@STU_ID", "")
        param.Add("@SBG_ID", ddlSubject.SelectedValue)
        param.Add("ACY_DESCR", ddlAcademicYear.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptSUBJECT_ASSESSMENT_REPORT.rpt")
        End With

        If hfDownloadReport.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub CallReportGraph()
        Dim param As New Hashtable
        Dim strRPF As String = ""


        Dim i As Integer
        For i = 0 To cblReportSchedule.Items.Count - 1
            If cblReportSchedule.Items(i).Selected = True Then
                If strRPF <> "" Then
                    strRPF += "|"
                End If
                strRPF += cblReportSchedule.Items(i).Value
            End If
        Next

        Dim strSection As String = ""
        If ddlSection.SelectedIndex = 0 Then
            For i = 1 To ddlSection.Items.Count - 1
                If strSection <> "" Then
                    strSection += "|"
                End If
                strSection += ddlSection.Items(i).Value
            Next
        Else
            strSection = ddlSection.SelectedValue.ToString
        End If

        Dim grades As String()
        Dim grade As String = ""
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue)
        param.Add("@RPF_ID", strRPF)
        param.Add("@GRD_ID", grade)
        param.Add("@SCT_ID", strSection)
        param.Add("@STU_ID", "")
        param.Add("@SBG_ID", ddlSubject.SelectedValue)
        param.Add("ACY_DESCR", ddlAcademicYear.SelectedItem.Text)

       
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptSUBJECT_ASSESSMENT_REPORT_GRAPHWISE.rpt")
        End With

        If hfDownloadGraph.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If

    End Sub

   

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindReportSchedule()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        If ddlReportCard.Items.Count > 0 Then
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            BindReportSchedule()
            Session("CurrSuperUser") = "Y"
            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindReportSchedule()
        Session("CurrSuperUser") = "Y"
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection()
    End Sub

    Protected Sub btnGetData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        Try
            hfDownloadReport.Value = "0"
            CallReport()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        End Try
       
    End Sub

    Protected Sub btnGenGraph_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenGraph.Click
        Try
            hfDownloadGraph.Value = "0"
            CallReportGraph()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnDownReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownReport.Click
        Try
            hfDownloadReport.Value = "1"
            CallReport()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnDownGraoh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownGraoh.Click
        Try
            hfDownloadGraph.Value = "1"
            CallReportGraph()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
