<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOptions_View.aspx.vb" Inherits="Curriculum_clmOptions_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Option Masters
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">

                            <table id="Table1" runat="server" cellpadding="0" border="0" cellspacing="0" width="100%">


                                <tr>
                                    <td colspan="4" align="center" class="matters">

                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td colspan="4" align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3" align="center">
                                                    <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <RowStyle  CssClass="griditem" />
                                                        <EmptyDataRowStyle  />

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOptId" runat="server" Text='<%# Bind("OPT_ID") %>'></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>




                                                            <asp:TemplateField HeaderText="Option Name">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblopt" runat="server" Text="Option Name"></asp:Label><br />


                                                                    <asp:TextBox ID="txtOption" runat="server" ></asp:TextBox>

                                                                    <asp:ImageButton ID="btnOption_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnOption_Search_Click" />



                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOption" runat="server" Text='<%# Bind("OPT_DESCR") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <SelectedRowStyle  />
                                                        <HeaderStyle  />
                                                        <EditRowStyle  />
                                                        <AlternatingRowStyle  CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>




</asp:Content>

