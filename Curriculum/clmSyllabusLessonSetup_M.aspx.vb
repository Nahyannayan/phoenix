﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSyllabusLessonSetup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100059") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade()
                    PopulateSubject()

                    GridBind()
                    gvLesson.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"

    Sub PopulateGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                  & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                  & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                  & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                  & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub


    Public Sub PopulateSubject()
        Dim grade() As String = ddlGrade.SelectedValue.ToString.Split("|")
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = ""
        str_sql = "SELECT SBG_ID,CASE WHEN SBG_PARENT_ID=0 THEN SBG_DESCR ELSE SBG_PARENTS+'-'+SBG_SHORTCODE END SBG_DESCR FROM SUBJECTS_GRADE_S WHERE" _
                & " SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_GRD_ID='" + grade(0) + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "LES_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "LES_DESCRIPTION"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "LES_ORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "LES_COMMENTS"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LES_ID,LES_DESCRIPTION,LES_ORDER,ISNULL(LES_COMMENTS,'')  LES_COMMENTS FROM SYL.LESSONSETUP_M WHERE " _
                             & " LES_SBG_ID='" + ddlSubject.SelectedValue.ToString + "' ORDER BY LES_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = "edit"
                dr.Item(3) = i.ToString
                dr.Item(4) = .Item(2)
                dr.Item(5) = .Item(3)
                dt.Rows.Add(dr)
            End With
        Next

        'add empty rows to show 50 rows
        For i = 0 To 15 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = "add"
            dr.Item(3) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(4) = ""
            dr.Item(5) = ""
            dt.Rows.Add(dr)
        Next

        Session("dtLesson") = dt
        gvLesson.DataSource = dt
        gvLesson.DataBind()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim lblLesId As Label
        Dim txtCriteria As TextBox
        Dim txtComments As TextBox
        Dim txtOrder As TextBox
        Dim lblDelete As Label
        Dim mode As String = ""
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        For i = 0 To gvLesson.Rows.Count - 1
            lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
            txtCriteria = gvLesson.Rows(i).FindControl("txtCriteria")
            txtOrder = gvLesson.Rows(i).FindControl("txtOrder")
            lblDelete = gvLesson.Rows(i).FindControl("lblDelete")
            txtComments = gvLesson.Rows(i).FindControl("txtComments")

            If lblDelete.Text = "1" Then
                mode = "delete"
            ElseIf lblLesId.Text = "0" And txtCriteria.Text <> "" Then
                mode = "add"
            ElseIf txtCriteria.Text <> "" Then
                mode = "edit"
            Else
                mode = ""
            End If
            str_query = "exec SYL.saveLESSONSETUP_M " _
                      & " @LES_ID=" + Val(lblLesId.Text).ToString + "," _
                      & " @LES_BSU_ID='" + Session("SBSUID") + "'," _
                      & " @LES_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                      & " @LES_SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                      & " @LES_GRD_ID='" + grade(0) + "'," _
                      & " @LES_DESCRIPTION=N'" + txtCriteria.Text + "'," _
                      & " @LES_COMMENTS=N'" + txtComments.Text + "'," _
                      & " @LES_ORDER=" + Val(txtOrder.Text).ToString + "," _
                      & " @MODE='" + mode + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvLesson.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade()
        PopulateSubject()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSubject()
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub
End Class
