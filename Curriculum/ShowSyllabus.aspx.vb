
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Partial Class Curriculum_ShowSyllabus
    Inherits System.Web.UI.Page
    Sub gridbind()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT SYM_ID,SYM_DESCR FROM SYL.SYLLABUS_M WHERE SYM_BSU_ID ='" & Session("sBsuid").ToString() & "' AND " _
                  & " SYM_ACD_ID=" & ViewState("SearchAcdId") & " and " _
                  & " SYM_TRM_ID=" & ViewState("SearchTrmId") & " and " _
                  & " SYM_GRD_ID=" & ViewState("SearchGrdId") & "and " _
                  & " SYM_SBG_ID=" & ViewState("SearchSubjId") & ""

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvempname.DataSource = ds
        gvempname.DataBind()
        ViewState("SearchAcdId") = ""
        ViewState("SearchTrmId") = ""
        ViewState("SearchGrdId") = ""
        ViewState("SearchSubjId") = ""

    End Sub

    Protected Sub gvempname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvempname.SelectedIndexChanged
        Dim lblSylId As Label
        Dim lnkSyllabusname As LinkButton

        lblSylId = gvempname.SelectedRow.FindControl("lblSylId")
        lnkSyllabusname = gvempname.SelectedRow.FindControl("lnkSyllabusname")
        
        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & lnkSyllabusname.Text + "||" + lblSylId.Text & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Employee ='" & lnkSyllabusname.Text + "||" + lblSylId.Text & "';")
        Response.Write("var oWnd = GetRadWindow('" & lnkSyllabusname.Text + "||" + lblSylId.Text & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"
        'Dim javaSr As String
        'javaSr = "<script language='javascript'> function listen_window(){"
        'javaSr += "window.returnValue = '" & lnkempname.Text & "||" & lblempId.Text.Replace("'", "\'") & "';"
        'javaSr += "window.close();"
        'javaSr += "} </script>"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        'h_SelectedId.Value = "Close"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("SearchAcdId") = Request.QueryString("AcdID")
            ViewState("SearchTrmId") = Request.QueryString("TrmID")
            ViewState("SearchGrdId") = Request.QueryString("GrdID")
            ViewState("SearchSubjId") = Request.QueryString("SubjID")

            gridbind()
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvempname_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvempname.PageIndexChanging
        gvempname.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
End Class

