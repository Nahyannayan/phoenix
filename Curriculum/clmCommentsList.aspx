<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmCommentsList.aspx.vb" Inherits="Curriculum_clmCommentsList" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
     <script language="javascript" type="text/javascript">
         //function listen_window() { }
         function GetRadWindow() {
             var oWindow = null;
             if (window.radWindow) oWindow = window.radWindow;
             else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
             return oWindow;
         }
    </script>
</head>
<body onload="listen_window();">

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">

    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr valign="top" align="center" class="title">
             <td> 
                 <span class="field-label">Select Value</span> </td>
         </tr>
            <tr id="trBSU" runat="server">
                <td align="left" >
                    <span class="field-label">School :</span> <asp:DropDownList ID="ddlBSUUnit" runat="server"  AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblGrade" class="field-label" runat="server" Text="Grade :"></asp:Label>
                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <span class="field-label">Category</span> 
                    <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
            <asp:GridView ID="gvcmtname" runat="server" AllowPaging="True"  
CssClass="table table-bordered table-row" AutoGenerateColumns="False" PageSize="20"  UseAccessibleHeader="False">
            <Columns>
                <asp:TemplateField HeaderText="Comment Id" Visible="False">
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        &nbsp;<asp:Label ID="lblcmtid" runat="server" Text='<%# Bind("CMT_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                
                <asp:TemplateField HeaderText="Comment Name">
                    <EditItemTemplate>
                      &nbsp;
                    </EditItemTemplate>
                    <HeaderTemplate>
                        Comments 
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkcmtname" runat="server" CommandName="Select" Text='<%# Bind("CMT_COMMENTS") %>' OnClick="lnkcmtname_Click" ></asp:LinkButton>&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                
            </Columns>
            <RowStyle />
            <HeaderStyle  />
            <AlternatingRowStyle  ForeColor="White" />
        </asp:GridView>
                </td>
            </tr>
            </table>
        
        <asp:HiddenField ID="h_SelectedId" runat="server" />
    
        <asp:HiddenField ID="h_SBGID" runat="server" />
    
    </div>
    </form>
</body>
</html>
