Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmShowStudents
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hfACD_ID.Value = Request.QueryString("acdid")
            hfGRD_ID.Value = Request.QueryString("grdid")
            hfSTM_ID.Value = Request.QueryString("stmid")
            hfSCT_ID.Value = Request.QueryString("sctid")

            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            GridBind()
         
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID

                s = gvSubjects.HeaderRow.FindControl("mnu_3_img")

                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfACD_ID.Value = Session("Current_ACD_ID") Then

            str_query = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,STU_ID FROM " _
                      & " STUDENT_M WHERE STU_ACD_ID=" + hfACD_ID.Value
        Else
            str_query = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,STU_ID FROM " _
                   & " VW_STUDENT_DETAILS_PREVYEARS WHERE STU_ACD_ID=" + hfACD_ID.Value
        End If

        If hfGRD_ID.Value <> "" And hfGRD_ID.Value <> "0" Then
            str_query += " AND STU_GRD_ID='" + hfGRD_ID.Value + "'"
        End If

        If hfSTM_ID.Value <> "" And hfSTM_ID.Value <> "0" Then
            str_query += " AND STU_STM_ID=" + hfSTM_ID.Value
        End If


        If hfSCT_ID.Value <> "" And hfSCT_ID.Value <> "0" Then
            str_query += " AND STU_SCT_ID=" + hfSCT_ID.Value
        End If



        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim stuNo As String = ""
        Dim stuName As String = ""

        Dim txtSearch As New TextBox
        Dim ddlgvOpt As New DropDownList
        Dim selectedOpt As String = ""

        If gvSubjects.Rows.Count > 0 Then

            txtSearch = gvSubjects.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stuNo = txtSearch.Text



            txtSearch = gvSubjects.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')", txtSearch.Text, strSearch)
            stuName = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If
        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSubjects.DataBind()
            Dim columnCount As Integer = gvSubjects.Rows(0).Cells.Count
            gvSubjects.Rows(0).Cells.Clear()
            gvSubjects.Rows(0).Cells.Add(New TableCell)
            gvSubjects.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSubjects.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSubjects.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSubjects.DataBind()
        End If

        txtSearch = gvSubjects.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stuNo

        txtSearch = gvSubjects.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuName


    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Protected Sub gvSubjects_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjects.PageIndexChanging
        gvSubjects.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub btnStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
 
    Protected Sub gvSubjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSubjects.SelectedIndexChanged
        Dim lblStuId As Label
        Dim lnkName As LinkButton
        Dim lnkStuNo As LinkButton

        Dim subject As String

        lblStuId = gvSubjects.SelectedRow.FindControl("lblStuId")
        lnkName = gvSubjects.SelectedRow.FindControl("lnkName")
        lnkStuNo = gvSubjects.SelectedRow.FindControl("lnkStuNo")



        Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & lblStuId.Text + "|" + lnkStuNo.Text + "|" + lnkName.Text.Replace("'", "") & "';")
        'Response.Write("window.close();")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Student ='" & lblStuId.Text + "||" + lnkName.Text + "||" + lnkStuNo.Text & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & lnkName.Text + "||" + lblStuId.Text & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"




    End Sub


End Class
