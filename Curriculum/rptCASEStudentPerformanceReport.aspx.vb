﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_Reports_Aspx_rptCASEStudentPerformanceReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330445" And ViewState("MainMnu_code") <> "C330447" And ViewState("MainMnu_code") <> "C330450" And ViewState("MainMnu_code") <> "C330455") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                BindBuisnessUnit()

                If Session("sbsuid") <> "111001" Then
                    trBsu.Visible = False
                End If
                BindAcademicYear()
                BindGrade()
                BindSubject()
                BindSection()

                Select Case ViewState("MainMnu_code")
                    Case "C330445"
                        lblText.Text = "STUDENT PERFORMANCE REPORT"
                    Case "C330447"
                        lblText.Text = "STUDENT MARKS"
                    Case "C330450"
                        lblText.Text = "STUDENT MARKS"
                    Case "C330455"
                        lblText.Text = "STUDENT PERFORMANCE GRAPH"
                End Select
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"

    Private Sub BindBuisnessUnit()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "SELECT BSU_SHORTNAME,BSU_ID FROM BUSINESSUNIT_M " _
                                 & " WHERE  BSU_SHORTNAME IN('OOD','OOW','OOS','OOB','OIS','OOF','OOL','OOA','KGS','TMS','GMS')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        ddlBusinessUnit.DataSource = ds
        ddlBusinessUnit.DataTextField = "BSU_SHORTNAME"
        ddlBusinessUnit.DataValueField = "BSU_ID"
        ddlBusinessUnit.DataBind()

        If Not ddlBusinessUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBusinessUnit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACY_ID FROM ACADEMICYEAR_M WHERE ACY_ID>=23"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACD_ACY_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                              & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=" + Session("CLM")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.Items.FindByValue(ds.Tables(0).Rows(0).Item(0)).Selected = True


    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID  IN('04','06','08') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                               & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M " _
                               & " INNER JOIN ACADEMICYEAR_D ON SCT_ACD_ID=ACD_ID" _
                               & " WHERE SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND ACD_ACY_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND ACD_BSU_ID='" + Session("SBSUID") + "' ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

    Sub CallReport()
        Dim param As New Hashtable

        param.Add("@IMG_TYPE", "LOGO")
        If Session("sbsuid") = "111001" Then
            param.Add("@IMG_BSU_ID", ddlBusinessUnit.SelectedValue.ToString)
            param.Add("@BSU_ID", ddlBusinessUnit.SelectedValue.ToString)
        Else
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@BSU_ID", Session("SBSUID"))
        End If

        param.Add("@ACY_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("section", ddlSection.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case ViewState("MainMnu_code")
                Case "C330445"
                    .reportPath = Server.MapPath("../Rpt/CASE/rptCASEStudentPerformance.rpt")
                Case "C330447"
                    .reportPath = Server.MapPath("../Rpt/CASE/rptCASEStudentMarks.rpt")
                Case "C330450"
                    .reportPath = Server.MapPath("../Rpt/CASE/rptCASEStudentPerformance_BySkill.rpt")
                Case "C330455"
                    .reportPath = Server.MapPath("../Rpt/CASE/rptCASEStudentPerformancebySkill_Graph.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub
End Class
