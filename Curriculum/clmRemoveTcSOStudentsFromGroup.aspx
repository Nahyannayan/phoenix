<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmRemoveTcSOStudentsFromGroup.aspx.vb" Inherits="Curriculum_clmRemoveTcSOStudentsFromGroup"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {

    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            if (document.forms[0].elements[i].disabled == false) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
            if (chkstate = true) {

            }
        }
    }
}


    </script>

    <div class="card mb-3">
        <div class="card-header  letter-space">
            <i class="fa fa-book mr-3"></i>Remove TC/SO Students From All Groups
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center"  cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center"  
                                 cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    
                                    <td align="left" class="matters" >
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Select Section</span></td>
                                   
                                    <td align="left" class="matters" colspan="2">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Student ID</span></td>
                                    
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left" class="matters"><span class="field-label">Student Name</span></td>
                                    
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtName" runat="server">
                                        </asp:TextBox></td>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="8" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Available" HeaderStyle-HorizontalAlign="Center" >
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>
                                                       
                                                                    <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label><br />
                                                                    <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />
                                                               

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label><br />
                                                                    <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label><br />
                                                                    <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>






                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="8" valign="top">
                                        <asp:Button ID="btnRemove" runat="server" Text="Remove From All Groups" CssClass="button"
                                            TabIndex="4" />
                                         <asp:LinkButton ID="btnHidden" runat="server" CssClass="link"></asp:LinkButton>
                <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnHidden"
                    PopupControlID="pnlEmail" OkControlID="btnClose" CancelControlID="btnCancel"
                    DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                </ajaxToolkit:ModalPopupExtender>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>


                    <asp:Panel ID="pnlEmail" Style="display: none;" runat="server">
        <div style="min-width: 400px;  border-radius: 8px; " class="pb-4 bg-white border">
         
                  <div class="row"> 
                  <div class=" col-12 griditem_alternative_dark text-right">
                  
                        <asp:Button ID="btnClose" CssClass="button_small" runat="server" Text="X" />
                    </div>
        </div>
                <div class="row"> 
                  <div class=" col-12 subHeader text-center font-weight-bold">
                   Confirmation!!!
                     </div>
        </div>
                 <div class="row"> 
                  <div class=" col-12 text-center">
                        <asp:Label ID="lblMessage" CssClass="error_password" runat="server" Text="Are you agree you proceed?"></asp:Label>
                     </div>
        </div>
            
            <div class="text-center">
                <asp:Button ID="btnOkay" CssClass="button" runat="server" Text="Yes" />    
                <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />
              
            </div> 
        </div>
    </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>
