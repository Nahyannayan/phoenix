﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmProcessCBSEUpgrade.aspx.vb" Inherits="Curriculum_clmProcessCBSEUpgrade"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Process CBSE Grade Upgradation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%" >
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"  ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tbgroup" runat="server" align="center" width="100%">
                                <tr id="trReport" runat="server">
                                    <td  align="left" width="20%"><span class="field-label">Report Card</span>
                                    </td>
                                    
                                    <td  align="left">
                                        <asp:DropDownList ID="ddlReportcard" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td  align="left" width="20%"><span class="field-label">Report Schedule</span>
                                    </td>
                                    
                                    <td  align="left">
                                        <asp:DropDownList ID="ddlReportSchedule" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="left"><span class="field-label">Section</span>
                                    </td>
                                    
                                    <td  align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td  align="center">
                                        <asp:Button ID="btnProcess" runat="server" CssClass="button" Text="Process" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                    <td  align="center">
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print Details" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:GridView ID="gvUpgrade" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                              PageSize="20"   SkinID="GridViewView" CssClass="table table-bordered table-row">
                                            <RowStyle  Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="NC Level" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       
                                                           Select
                                                           <br />
                                                           <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Co.Scholastic Average">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAverage" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Marks">
                                                    <ItemStyle   HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:DataList runat="server" ID="dlUpgrade" RepeatColumns="10">
                                                            <ItemStyle HorizontalAlign="center" />
                                                            <ItemTemplate>
                                                                <table border="1" class="BlueTable">
                                                                    <tr>
                                                                        <td colspan="2"   >
                                                                            <asp:Label runat="server" Text='<%# Bind("SBG_DESCR") %>' ID="lblSubject"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >Actual</td>
                                                                        <td >Upgraded</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >
                                                                            <asp:Label runat="server" Text='<%# Bind("FGU_GRADE_ACTUAL") %>' ID="lblActual"></asp:Label>
                                                                        </td>
                                                                        <td >
                                                                            <asp:Label runat="server" Text='<%# Bind("FGU_GRADE_UPGRADE") %>' ID="lblUpgrade"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfBrownBookDate" runat="server" Visible="False" Value="0"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>
