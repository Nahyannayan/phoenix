﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGradeNineMarkEntry.aspx.vb" Inherits="Curriculum_clmGradeNineMarkEntry"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Grade 9 Final Mark Entry</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                   
                    <tr>
                        <td align="center"  style="font-weight: bold;"
                            valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center"  cellpadding="0" cellspacing="0"  width="100%">
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Select Section</span>
                                    </td>
                                   
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"  ><span class="field-label">Select Subject</span>
                                    </td>
                                   
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlSubject" SkinID="smallcmb" runat="server"
                                           AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4"
                                             />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" >
                                            <RowStyle   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo"  runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FA-MARK">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFAMark"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FA-GRADE">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlFAGrade" runat="server">
                                                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                            <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                            <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                            <asp:ListItem Text="B1" Value="B1"></asp:ListItem>
                                                            <asp:ListItem Text="B2" Value="B2"></asp:ListItem>
                                                            <asp:ListItem Text="C1" Value="C1"></asp:ListItem>
                                                            <asp:ListItem Text="C2" Value="C2"></asp:ListItem>
                                                            <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="E1" Value="E1"></asp:ListItem>
                                                            <asp:ListItem Text="E2" Value="E2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SA-MARK">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSAMark" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SA-GRADE">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlSAGrade" runat="server">
                                                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                            <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                            <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                            <asp:ListItem Text="B1" Value="B1"></asp:ListItem>
                                                            <asp:ListItem Text="B2" Value="B2"></asp:ListItem>
                                                            <asp:ListItem Text="C1" Value="C1"></asp:ListItem>
                                                            <asp:ListItem Text="C2" Value="C2"></asp:ListItem>
                                                            <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="E1" Value="E1"></asp:ListItem>
                                                            <asp:ListItem Text="E2" Value="E2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OVERALL-MARK">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTotalMark"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OVERALL-GRADE">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlTotalGrade" runat="server">
                                                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                            <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                            <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                            <asp:ListItem Text="B1" Value="B1"></asp:ListItem>
                                                            <asp:ListItem Text="B2" Value="B2"></asp:ListItem>
                                                            <asp:ListItem Text="C1" Value="C1"></asp:ListItem>
                                                            <asp:ListItem Text="C2" Value="C2"></asp:ListItem>
                                                            <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="E1" Value="E1"></asp:ListItem>
                                                            <asp:ListItem Text="E2" Value="E2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" >
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="hfRSM_ID" runat="server" />
                <asp:HiddenField ID="hfRPF_ID" runat="server" />
                <asp:HiddenField ID="hfRSD_SA" runat="server" />
                <asp:HiddenField ID="hfRSD_TOTAL" runat="server" />
                <asp:HiddenField ID="hfRSD_FA" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
