<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="clmSubjectGrade_M.aspx.vb" Inherits="Curriculum_clmSubjectGrade_M" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function EnableOption(chk) {
            if (chk.checked == true) {
                document.getElementById("<%=lstOptions.ClientId %>").disabled = false;
            }
            else {
                document.getElementById("<%=lstOptions.ClientId %>").disabled = true;
            }
        }

        function UncheckOtherNodes(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var oTree = document.getElementById("<%=tvParentSubjects.ClientId %>");
                childChkBoxes = oTree.getElementsByTagName("input");
                var childChkBoxCount = childChkBoxes.length;
                for (var i = 0; i < childChkBoxCount; i++) {
                    if (childChkBoxes[i].id != src.id) {
                        childChkBoxes[i].checked = false;
                    }
                }
            }

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>GradeWise Subject Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                </table>
                <table id="tbl_ShowScreen" runat="server" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table id="Table1" runat="server" align="center" cellpadding="5"
                                cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" colspan="4">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblAccText" class="field-label" runat="server" Text="Academic Year"></asp:Label>
                                                </td>

                                                <td align="left" width="25%">
                                                    <asp:Label ID="lblacademicYear" class="field-value" runat="server"></asp:Label></td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblgradeText" class="field-label" runat="server" Text="Grade"></asp:Label>
                                                </td>

                                                <td align="left" width="25%">
                                                    <asp:Label ID="lblGrade" class="field-value" runat="server" Text=""></asp:Label></td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblStreamText" class="field-label" runat="server" Text="Stream"></asp:Label>
                                                </td>

                                                <td align="left" width="20%">
                                                    <asp:Label ID="lblStream" class="field-value" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" valign="middle" class="title-bg">
                                        <span class="field-label">Details</span>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td align="left" colspan="4">
                                        <br />
                                        <table id="Table3" runat="server" align="left" cellpadding="0"
                                            cellspacing="0" width="100%">
                                            <tr>--%>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label1" class="field-label" runat="server" Text="Copy Details From Grade"></asp:Label></td>

                                    <td align="left">
                                   
                                               <asp:DropDownList ID="ddlCopyStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>

                                    <td align="left">
                                      <asp:DropDownList ID="ddlCopyGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" ValidationGroup="groupM2" />
                                    </td>
                                    <%--</tr>

                                        </table>
                                    </td>--%>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4" valign="middle">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lbl1" class="field-label" runat="server" Text="Subject"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lbl2" class="field-label" runat="server" Text="Description "></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDescr" runat="server" CausesValidation="True" ValidationGroup="groupM1" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label7" class="field-label" runat="server" Text="Short Code "></asp:Label>

                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtShortCode" runat="server" CausesValidation="True" ValidationGroup="groupM1" TabIndex="2" MaxLength="15"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label2" class="field-label" runat="server" Text="Department"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDPT" runat="server" TabIndex="3">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" rowspan="2" width="20%">
                                        <asp:Label ID="Label4" class="field-label" runat="server" Text="Parent Subject"></asp:Label>

                                    </td>
                                    <td align="left" rowspan="2">
                                        <div class="checkbox-list">
                                            <asp:TreeView ID="tvParentSubjects" runat="server"
                                                DataSourceID="XmlSubjects" ExpandDepth="0" NodeIndent="5" onclick="return UncheckOtherNodes(event);"
                                                ShowCheckBoxes="All" Style="overflow: auto; text-align: left" TabIndex="4">
                                                <ParentNodeStyle Font-Bold="True" />
                                                <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Underline="False"
                                                    HorizontalPadding="3px" VerticalPadding="1px" />
                                                <DataBindings>
                                                    <asp:TreeNodeBinding DataMember="menuItem" TextField="Text" ToolTipField="ToolTip" ValueField="valuefield"></asp:TreeNodeBinding>
                                                </DataBindings>

                                            </asp:TreeView>
                                        </div>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Optional</span>

                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkOpt" runat="server" AutoPostBack="True" TabIndex="11" />

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label3" runat="server" class="field-label" Text="Option Name"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstOptions" runat="server" RepeatLayout="Flow" TabIndex="12">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">
                                                        <asp:CheckBox ID="chkTC" Text="Display Subject in TC" runat="server" TabIndex="5" /></span></td>
                                                <td align="left">
                                                    <span class="field-label">
                                                        <asp:CheckBox ID="chkSub" Text="Display Subject in Report Card" runat="server" TabIndex="6" /></span></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">
                                                        <asp:RadioButton ID="rdMarks" runat="server" GroupName="gr" Text="Display marks in report card" TabIndex="7" AutoPostBack="True" Checked="True"></asp:RadioButton></span></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:RadioButton ID="rdGrade" runat="server" GroupName="gr" Text="Display grade in report card" TabIndex="8" AutoPostBack="True"></asp:RadioButton></span></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:RadioButton ID="rdEMark" runat="server" Checked="True" GroupName="g5" Text="Enter Marks" TabIndex="9"></asp:RadioButton></span></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:RadioButton ID="rdEgrade" runat="server" GroupName="g5" Text="Enter Grade" TabIndex="10"></asp:RadioButton></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label class="field-label" ID="Label6" runat="server" Text="Core And Extended">
                                        </asp:Label></td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkCE" runat="server" AutoPostBack="True" TabIndex="13" />

                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Subject Type</span>

                                    </td>
                                    <td align="left">
                                        <asp:DropDownList runat="server" ID="ddlMajor" TabIndex="14">
                                            <asp:ListItem Value="1">Major</asp:ListItem>
                                            <asp:ListItem Value="0">Minor</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trCredit" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Credits</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCredit" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Weight</span>

                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtWeight" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trCount" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Count in Hrs</span>

                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlHr" runat="server">
                                            <asp:ListItem Value="YES">Yes</asp:ListItem>
                                            <asp:ListItem Value="NO">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Count in GPA</span>

                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGpa" runat="server">
                                            <asp:ListItem Value="YES">Yes</asp:ListItem>
                                            <asp:ListItem Value="NO">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trLength" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Length</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLength" runat="server">
                                            <asp:ListItem Value="ALL">Full Year</asp:ListItem>
                                            <asp:ListItem Value="SEM">Semester</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label9" class="field-label" runat="server" Text="Auto. Allocate Group">
                                        </asp:Label></td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkGroup" runat="server" AutoPostBack="True" TabIndex="15" />

                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="16" />
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table id="Table5" runat="server" align="center" border="0" cellpadding="5" width="100%"
                                            cellspacing="0">
                                            <tr>
                                                <td align="left" colspan="8">
                                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">
                                                        <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False"
                                                            CssClass="table table-bordered table-row" EmptyDataText="No Records"
                                                            HeaderStyle-Height="30" PageSize="20">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblsbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblsbmId" runat="server" Text='<%# Bind("SBG_SBM_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblsbgParentId" runat="server" Text='<%# Bind("SBG_PARENT_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:BoundField DataField="SBG_DESCR" HeaderText="Subject" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_SHORTCODE" HeaderText="Short Code" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_bOPTIONAL" HeaderText="Optional" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="OPT_DESCR" HeaderText="Option" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DPT_DESCR" HeaderText="Department" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:TemplateField HeaderText="Parent">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblsbgParent" runat="server" Text='<%# Bind("SBG_PARENT") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="SBG_bTC_DISPLAY" HeaderText="Display TC" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_bREPCRD_DISPLAY" HeaderText="Display Subject" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_bMRK_DISPLAY" HeaderText="Display Marks" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_bCOREEXT" HeaderText="Core & Extended" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>


                                                                <asp:BoundField DataField="SBG_bAUTOGROUP" HeaderText="Auto.Allocate Group" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_bMAJOR" HeaderText="Major Subject" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_ENTRYTYPE" HeaderText="Entry Type" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>


                                                                <asp:BoundField DataField="SBG_CREDITS" HeaderText="Credits" Visible="False" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_WT" HeaderText="Weight" Visible="False" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_LEN" HeaderText="Length" Visible="False" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>


                                                                <asp:BoundField DataField="SBG_HRS" HeaderText="Count in Hrs" Visible="False" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="SBG_GPA" HeaderText="Count in GPA" Visible="False" HtmlEncode="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                                                    <HeaderStyle Wrap="True" />
                                                                </asp:BoundField>



                                                                <asp:ButtonField CommandName="view" HeaderText="Edit" Text="Edit">
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:ButtonField>

                                                                <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete">
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:ButtonField>

                                                            </Columns>
                                                            <HeaderStyle />
                                                            <RowStyle />
                                                            <SelectedRowStyle />
                                                            <AlternatingRowStyle />
                                                        </asp:GridView>
                                                    </asp:Panel>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom"
                                        colspan="4" align="center">
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" UseSubmitBehavior="False" />

                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                            ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:RequiredFieldValidator ID="rfSub" runat="server" ControlToValidate="txtDescr"
                                Display="None" ErrorMessage="Please enter data in the field description" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rhShort" runat="server" ControlToValidate="txtShortCode"
                                Display="None" ErrorMessage="Please enter data in the field Short Code" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfACD_ID" runat="server" />

                            <asp:XmlDataSource ID="XmlSubjects" runat="server" EnableCaching="False" TransformFile="~/Curriculum/parentsubjectXSLT.xsl"
                                XPath="MenuItems/MenuItem"></asp:XmlDataSource>
                            <asp:HiddenField ID="hfGRD_ID" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfSTM_ID" runat="server" />
                            <asp:HiddenField ID="hfCopy" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

