﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmCASEUnits_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                  Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C410050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    BindAcademicYear()

                    BindGrade()

                    BindSubject()

                    GridBind()

                    gvUnit.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACY_ID FROM ACADEMICYEAR_M WHERE ACY_ID>=23"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ACY_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                              & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=" + Session("CLM")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID NOT IN('PN1','PN2','KG1','KG2','PK') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                               & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBU_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBU_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBU_ORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBU_SHORTNAME"
        dt.Columns.Add(column)
        Return dt
    End Function

    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBU_ID,ISNULL(SBU_DESCR,'') SBU_DESCR,ISNULL(SBU_ORDER,0) SBT_ORDER,ISNULL(SBU_SHORTNAME,'') SBU_SHORTNAME FROM " _
                                & " CE.SUBJECTS_UNITS_M WHERE " _
                                & " SBU_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                                & " AND SBU_ACY_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " ORDER BY SBU_ORDER"

        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = "edit"
                dr.Item(3) = i.ToString
                dr.Item(4) = .Item(2)
                dr.Item(5) = .Item(3)
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 50 rows
        For i = 0 To 15 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = "add"
            dr.Item(3) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(4) = ""
            dr.Item(5) = ""
            dt.Rows.Add(dr)
        Next

        Session("dtUnit") = dt



        gvUnit.DataSource = dt
        gvUnit.DataBind()


    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblSbuId As Label
        Dim txtUnit As TextBox
        Dim txtOrder As TextBox
        Dim txtShort As TextBox
        Dim lblDelete As Label
        Dim mode As String = ""

        For i = 0 To gvUnit.Rows.Count - 1
            With gvUnit.Rows(i)
                lblSbuId = .FindControl("lblSbuId")
                txtUnit = .FindControl("txtUnit")
                txtOrder = .FindControl("txtOrder")
                lblDelete = .FindControl("lblDelete")
                txtShort = .FindControl("txtShort")
                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblSbuId.Text = "0" And txtUnit.Text <> "" Then
                    mode = "add"
                ElseIf txtUnit.Text <> "" Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then
                    str_query = "exec CE.saveSUBJECTS_UNIT_M " _
                   & " @SBU_ID=" + lblSbuId.Text + "," _
                   & " @SBU_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                   & " @SBU_ACY_ID='" + ddlAcademicYear.SelectedValue.ToString + "'," _
                   & " @SBU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                   & " @SBU_DESCR='" + txtUnit.Text + "'," _
                   & " @SBU_SHORTNAME='" + txtShort.Text + "'," _
                   & " @SBU_ORDER='" + Val(txtOrder.Text).ToString + "'," _
                   & " @MODE='" + mode + "'"


                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvUnit.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#End Region

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GridBind()
    End Sub
End Class
