<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmReportCardDefaults_M.aspx.vb" Inherits="Curriculum_clmReportCardDefaults_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }
    </script>
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Report Card Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr >
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" >
                <table id="Table1" runat="server" align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="3" align="center" class="matters">
                            <table id="tbReport" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="subheader_img">
                                    <%--<td align="left" valign="middle" colspan="3">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                            SET DEFAULT VALUES</span></font>
                                    </td>--%>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label"> Report Card</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:Label ID="lblReportCard" runat="server" Text="Label" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                       <span class="field-label">  Header</span>
                                    </td>
                                 
                                    <td align="left" >
                                        <asp:Label ID="lblHeader" runat="server" Text="Label" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label"> Select Grade</span>
                                    </td>
                                   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                      <span class="field-label">   Select Subject</span>
                                    </td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label"> Add Default Values</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnAddNew" runat="server" CssClass="button" TabIndex="7" Text="Add"
                                            ValidationGroup="groupM1" />
                                        <asp:Label ID="lblDefaultBank" runat="server" Font-Underline="True" Style="cursor: hand">Default Bank</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="2" width="100%">
                                        <asp:GridView ID="gvDefault" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem"  />
                                            <EmptyDataRowStyle  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Value">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDefault"  runat="server" Text='<%# Bind("RSP_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Order">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder"  runat="server" Text='<%# Bind("RSP_DISPLAYORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="edit" Text="Delete" HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <EditRowStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" >
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="2" class="title-bg">
                                        Copy default values to subjects
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" >
                                        <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem"  />
                                            <EmptyDataRowStyle  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <HeaderTemplate>
                                                        
                                                                    Select<br />
                                                                
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle />
                                            <EditRowStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative"/>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" >
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" class="title-bg">
                                        Copy default values to headers
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" >
                                        <asp:GridView ID="gvHeaders" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem"  />
                                            <EmptyDataRowStyle  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCRsdId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Header">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCHeader" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <EditRowStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" >
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="7" Text="Save"
                                            ValidationGroup="groupM1" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRSD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSBM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfAllSubjects" runat="server"></asp:HiddenField>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlPopup" runat="server" CssClass="panel-cover" >
        <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
             PageSize="20" Width="373px" BorderStyle="None">
            <RowStyle CssClass="griditem"  />
            <EmptyDataRowStyle  />
            <Columns>
                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblRdmId" runat="server" Text='<%# Bind("RDM_ID") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:Label ID="lblDefault" runat="server" Text='<%# Bind("RDM_DESCR") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="view" Text="Select">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
                </asp:ButtonField>
            </Columns>
            <SelectedRowStyle  />
            <EditRowStyle  />
        </asp:GridView>
        <asp:Button ID="btnDefCancel" runat="server" CausesValidation="False"
                                                CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False" />
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="pnlPopup" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="lblDefaultBank">
    </ajaxToolkit:ModalPopupExtender>

    
            </div>
        </div>
    </div>

</asp:Content>
