﻿Imports GemBox.Spreadsheet
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Curriculum_clmTransscript_TWS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                'If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C102619")) Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else
                'calling pageright class to get the access rights

                lblReportCaption.Text = Mainclass.GetMenuCaption(ViewState("MainMnu_code"))
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'New Code Added By Nikunj - 13/Jan/2020
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                'End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed."
            End Try
        Else
            If Session("QData") IsNot Nothing Then
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If FileUpload1.HasFile Then
            Call UploadStudentData()
        Else
            lblError.ForeColor = System.Drawing.Color.Red
            lblError.Text = "Please select file."
        End If
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        Call GetDataByParameters()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
    End Sub
    'Added By Nikunj - 20/Jan/2020
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = " |0"
        ddl.Items.Insert(0, li)
        Return ddl
    End Function
    'Added By Nikunj - 20/Jan/2020
    Sub GetDataByParameters()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@GRADE", grade(0))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStudentDataByAcdID_GradeID", pParms)

            If Not ds Is Nothing Then
                If ds.Tables(0) Is Nothing OrElse ds.Tables(0).Rows.Count > 0 Then
                    Dim dtEXCEL As New DataTable
                    dtEXCEL = ds.Tables(0)
                    ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                    '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                    SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                    Dim ef As ExcelFile = New ExcelFile
                    If dtEXCEL.Rows.Count > 0 Then
                        Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_STUDENT_DATA_EXPORT")
                        ws.Columns(0).Hidden = True
                        ws.Columns(1).Hidden = True
                        ws.Columns(2).Hidden = True

                        ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                        ws.Columns(3).AutoFit()
                        ws.Columns(4).AutoFit()
                        ws.Columns(5).AutoFit()
                        ws.Columns(6).AutoFit()
                        ws.Columns(7).AutoFit()
                        ws.Columns(8).AutoFit()
                        ws.Columns(9).AutoFit()
                        ws.Columns(10).AutoFit()

                        Response.ContentType = "application/vnd.ms-excel"
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_STUDENT_DATA_EXPORT.xlsx")
                        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                        Dim pathSave As String
                        pathSave = "Student Data" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                        ef.Save(cvVirtualPath & pathSave)
                        Dim path = cvVirtualPath & pathSave

                        Dim bytes() As Byte = File.ReadAllBytes(path)
                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/octect-stream"
                        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()
                    End If
                Else
                    lblError.Text = "No Records To display with this filter condition....!!!"
                    lblError.Focus()
                End If
            End If

            Session("QData") = ds.Tables(0)
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed."
        End Try
    End Sub
    'Added By Nikunj - 20/Jan/2020
    Sub UploadStudentData()
        Try
            '-----------  Check file size
            Dim MaxSize As Integer = FileUpload1.PostedFile.ContentLength
            If MaxSize > "2097152" Then
                lblError.ForeColor = System.Drawing.Color.Red
                lblError.Text = "The file size cannot exceed 2 MB."
                btnGenerateReport.Focus()
            End If

            '-----------  Check file extension
            Dim validFileTypes As String() = {"xlsx", "xls"}
            Dim ext As String = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim isValidFile As Boolean = False
            For i As Integer = 0 To validFileTypes.Length - 1
                If ext = "." & validFileTypes(i) Then
                    isValidFile = True
                    Exit For
                End If
            Next

            If Not isValidFile Then
                lblError.ForeColor = System.Drawing.Color.Red
                lblError.Text = "Invalid File. Please upload a File with extension " &
                String.Join(", ", validFileTypes)
            Else
                '-----------  Copy file
                Dim folderPath As String = Server.MapPath("~/Files/")
                If Not Directory.Exists(folderPath) Then
                    Directory.CreateDirectory(folderPath)
                End If

                If File.Exists(folderPath & Path.GetFileName(FileUpload1.FileName)) Then
                    File.Delete(folderPath & Path.GetFileName(FileUpload1.FileName))
                End If
                FileUpload1.SaveAs(folderPath & Path.GetFileName(FileUpload1.FileName))

                '-----------  Convert data xl to datatable
                Dim excelQuery As String = ""
                excelQuery = " SELECT * FROM  [TableName]"
                Dim xltable As DataTable
                xltable = Mainclass.FetchFromExcel(excelQuery, folderPath & FileUpload1.PostedFile.FileName)
                If xltable.Rows.Count.Equals(0) Then
                    Throw New Exception("Could not process the request.Invalid data in excel..!")
                End If

                File.Delete(folderPath & FileUpload1.PostedFile.FileName)

                '-----------  Set datatable layout
                xltable.Columns.Remove("StudentId")
                xltable.Columns.Remove("Name")
                xltable.Columns.Remove("Subject")
                xltable.Columns.Remove("Board")
                xltable.Columns("STU_ID").ColumnName = "FTM_STU_ID"
                xltable.Columns("ACD_ID").ColumnName = "FTM_ACD_ID"
                xltable.Columns("SBG_ID").ColumnName = "FTM_SBG_ID"
                xltable.Columns("Grade").ColumnName = "FTM_GRADE"
                xltable.Columns("MaxMark").ColumnName = "FTM_MAXMARK"
                xltable.Columns("MinMark").ColumnName = "FTM_MINMARK"
                xltable.Columns("Result").ColumnName = "FTM_RESULT"
                xltable.AcceptChanges()

                '-----------  Bulk copy to sync table
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Using cn As New SqlConnection(str_conn)
                    cn.Open()
                    Using copy As New SqlBulkCopy(cn)
                        copy.DestinationTableName = "[RPT].[FINAL_TOTALMARKS_S_Sync]"
                        copy.WriteToServer(xltable)
                    End Using
                End Using

                '-----------  Sync opration
                Dim pParms(0) As SqlClient.SqlParameter
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RPT].[rptSync_FINAL_TOTALMARKS_S]", pParms)

                lblError.ForeColor = System.Drawing.Color.Green
                lblError.Text = "File uploaded successfully."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed."
        End Try
    End Sub
End Class
