<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmPathfinderProcess_GradeSection.aspx.vb" Inherits="Curriculum_clmPathfinderProcess_GradeSection" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Path Finder Processing"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                   
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom">
                            <table width="100%" >
                                <tr>
                                    <td align="left"  width="25%" ><span class="field-label">Select Section</span></td>
                                    <td align="left" colspan="4" >
                                        <asp:CheckBoxList ID="lstSection" runat="server" RepeatLayout="Flow" RepeatColumns="22" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                        <asp:Label ID="lblNote" runat="server" ></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center"   valign="bottom">

                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom">
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

