<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmShowStudents.aspx.vb" Inherits="Curriculum_clmShowStudents" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Students</title>
  <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script> 
    <script>
        function GetRadWindow() {
           
              var oWindow = null;
              if (window.radWindow) oWindow = window.radWindow;
              else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
              return oWindow;
          }
    </script>    
</head>
<body onload="listen_window();"> 
    
    <form id="form1" runat="server">   
      <div>        
          <table align="center" cellpadding="0" cellspacing="0" width="100%">
              <%--<tr>              
                <td align="left"  colspan="2" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">--%>
              <tr>
                  <%--<td>
                      <table align="center" cellpadding="0" cellspacing="0" width="100%">
                          <tr>--%>
                              <td align="left" colspan="4" style="" valign="top">
                                  <asp:GridView ID="gvSubjects" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                      Width="90%" CssClass="table table-bordered table-row">
                                      <Columns>

                                          <asp:TemplateField HeaderText="stu_id" Visible="False">
                                              <ItemTemplate>
                                                  <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                              </ItemTemplate>
                                              <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                          </asp:TemplateField>


                                          <asp:TemplateField HeaderText="Student ID">
                                              <EditItemTemplate>
                                                  <asp:Label ID="Label1" runat="server" Text='<%# Eval("STU_NO") %>'></asp:Label>
                                              </EditItemTemplate>
                                              <ItemTemplate>
                                                  <asp:LinkButton ID="lnkStuNo" runat="server" CommandName="Select" Text='<%# Bind("STU_NO") %>'></asp:LinkButton>&nbsp;
                                              </ItemTemplate>
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblID2" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                      Text="Student ID"></asp:Label>
                                                  <br />
                                                  <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                  <asp:ImageButton ID="btnStuNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                      OnClick="btnStuNo_Click" />&nbsp;
                                              </HeaderTemplate>
                                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                          </asp:TemplateField>

                                          <asp:TemplateField HeaderText="GRADE">
                                              <EditItemTemplate>
                                                  <asp:Label ID="Label1" runat="server" Text='<%# Eval("STU_NAME") %>'></asp:Label>
                                              </EditItemTemplate>
                                              <ItemTemplate>
                                                  <asp:LinkButton ID="lnkName" runat="server" CommandName="Select" Text='<%# Bind("STU_NAME") %>'></asp:LinkButton>&nbsp;
                                              </ItemTemplate>
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblID1" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                      Text="Student Name"></asp:Label>
                                                  <br />
                                                  <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                                  <asp:ImageButton ID="btnGrade" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                      OnClick="btnStuName_Click" />&nbsp;
                                              </HeaderTemplate>
                                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                          </asp:TemplateField>
                                      </Columns>
                                      <HeaderStyle />
                                      <AlternatingRowStyle />
                                      <RowStyle />
                                  </asp:GridView>
                              </td>
                          <%--</tr>
                      </table>
                  </td>--%>
              </tr>
              <%-- <tr>
                            <td align="center"></td>
                        </tr>--%>
              <%--</table>
                </td>
            </tr>--%>
              <tr>
                  <td align="center" colspan="4" valign="middle">
                      <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                          runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                              type="hidden" value="=" /><input id="h_selected_menu_3" runat="server"
                                  type="hidden" value="=" />
                      &nbsp;
                  </td>
              </tr>
          </table>
    
    </div>
    
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" /><asp:HiddenField ID="hfSCT_ID" runat="server" />
    </form>
</body>
</html>
