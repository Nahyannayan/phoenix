﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmFeeoverride_Feeapproval.aspx.vb" Inherits="Fees_Feeoverride_approval" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-dollar"></i>Fee Override Approval to Release Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 60%" align="center" border="1" border-color="green">
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" PageSize="20" SkinID="GridViewView" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblStu_NoH" runat="server">Student No
                                                <br />
                                                            <asp:TextBox ID="txtStuNo" runat="server" Width="70%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Top"
                                                                ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />
                                                        </asp:Label></td>
                                                </tr>
                                            </table>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" Width="70px" runat="server"
                                                Text='<%# Bind("Stu_No") %>'></asp:Label>
                                            <asp:HiddenField ID="HF_stu_id" runat="server" Value='<%# Bind("Stu_ID") %>' />
                                            <asp:HiddenField ID="HF_ROR_ID" runat="server" Value='<%# Bind("ROR_ID") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100%;">
                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuName" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Top"
                                                            ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />
                                                    </td>
                                                </tr>
                                            </table>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server" Width="70%"> </asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />

                                                    </td>
                                                </tr>
                                            </table>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblH123" runat="server" Text="Section"></asp:Label><br />
                                                        <asp:TextBox ID="txtSection" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Top"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />
                                                    </td>
                                                </tr>
                                            </table>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_select" runat="server" OnClick="lnk_select_Click">Select</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>

                        </td>
                    </tr>                 
                </table>

                <asp:Label ID="lblerror" runat="server" CssClass="error"
                    EnableViewState="False"></asp:Label>

                <asp:Label ID="lblpopup" runat="server"></asp:Label>

                <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server"
                    BackgroundCssClass="modalBackground" CancelControlID="btncancel" PopupControlID="Panel1" TargetControlID="lblpopup">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" Width="400px" BackColor="LightYellow" CssClass="modalPopup">

                    <table style="width: 100%" class="BlueTable" border="1" border-color="green">
                        <tr class="matters">
                            <td class="subheader_img" colspan="2">
                                <table style="width: 100%">
                                    <tr>
                                        <td align="left">Approve/Reject</td>
                                        <td align="right">
                                            <asp:ImageButton ID="btncancel" runat="server"
                                                ImageUrl="~/Images/close_red1.png" Height="18px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="matters">
                            <td align="left">Student No</td>
                          
                            <td align="left">
                                <asp:Label ID="lblStudentno" runat="server"></asp:Label>
                                <br />
                            </td>
                        </tr>
                        <tr class="matters">
                            <td align="left">Remarks</td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtRemarks" runat="server" ValidationGroup="usrname"
                                    Height="73px" TextMode="MultiLine" Width="249px" EnableTheming="False"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="txtRemarks" ErrorMessage="Remarks Required"
                                    ValidationGroup="gr"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="matters">
                            <td align="center" colspan="2">
                                <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"
                                    ValidationGroup="gr" Width="75px" />
                                &nbsp;<asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject"
                                    Width="75px" ValidationGroup="gr" />
                                <br />
                                <asp:HiddenField ID="HFROR_ID" runat="server" />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="0" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
                                     
                               

</asp:Content>

