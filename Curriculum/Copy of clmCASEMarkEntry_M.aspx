<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Copy of clmCASEMarkEntry_M.aspx.vb" Inherits="Curriculum_clmCASEMarkEntry_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="center" class="matters" style="font-weight: bold;" valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center" border="1" class="BlueTableView"
                    bordercolor="#1b80b6" cellpadding="7" cellspacing="0" style="width: 90%">
                    <tr id="trAcd1" runat="server">
                        <td align="left" class="matters">
                            Academic Year
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters" colspan="5">
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="108px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd2" runat="server">
                        <td align="left" class="matters">
                            Grade
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="68px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters">
                            Section
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters" colspan="2">
                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" Width="71px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd3" runat="server">
                        <td align="left" class="matters">
                            Subject
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" Width="303px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters">
                            Question Bank
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters" colspan="2">
                            <asp:DropDownList ID="ddlQuestions" runat="server" AutoPostBack="True" Width="303px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd4" runat="server">
                        <td align="left" class="matters">
                            Student ID
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtStuNo" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left" class="matters">
                            Student Name
                        </td>
                        <td class="matters">
                            :
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtName" runat="server">
                            </asp:TextBox>
                        </td>
                        <td style="height: 10px">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr id="trStudent" runat="server">
                        <td align="center" class="matters" colspan="7" valign="top">
                            <table>
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvStudent" runat="server" CssClass="gridstyle" AutoGenerateColumns="False"
                                            AllowPaging="True" AllowSorting="True" PageSize="1" Width="669px" BackImageUrl="~/Images/bg-menu-main.png">
                                            <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                Position="Top" PageButtonCount="100" />
                                            <RowStyle CssClass="griditem" Height="20px" />
                                            <PagerStyle Font-Bold="True" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                Font-Size="Small" ForeColor="White" HorizontalAlign="Right" Height="20px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuID" runat="server" Text='<%# Eval("Stu_ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Eval("Section") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("Stu_No") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Eval("Stu_Name") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancelSearch" runat="server" Text="Cancel Search" CssClass="button"
                                            TabIndex="4" Width="100px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trMarks" runat="server">
                        <td align="center" class="matters" colspan="7" valign="top">
                            <asp:GridView ID="gvQuestions" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords." HeaderStyle-Height="30" PageSize="20"
                                Width="373px">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQsdId" runat="server" Text='<%# Bind("QSD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMaxMark" runat="server" Text='<%# Bind("QSD_MAXMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Question">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuestion" runat="server" Width="50px" Text='<%# Bind("QSD_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mark">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtMark" Width="50px" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMark"
                                                            ErrorMessage='<%# Bind("ERR") %>' Display="Dynamic" Type="Double" MaximumValue='<%# Bind("QSD_MAXMARK") %>'
                                                            MinimumValue="0">
                                                        </asp:RangeValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSave" runat="server">
                        <td align="center" class="matters" colspan="7" valign="top">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"
                                Width="51px" />
                            <asp:Button ID="btnSaveNext" runat="server" Text="Save and Next" CssClass="button"
                                TabIndex="4" Width="116px" />
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfTeacherGrades" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfACY_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfQSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSBM_ID" runat="server"></asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>
</asp:Content>
