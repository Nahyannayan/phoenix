﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_LessonDocuments.aspx.vb" Inherits="Curriculum_clmSyllabus_LessonDocuments" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Documents Updation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <tr id="trAcd2" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Grade</span> 
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"  width="20%" ><span class="field-label">Subject</span> 
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>
                               
                                <tr>
                                    <td align="left"  width="20%" ><span class="field-label">Description</span> 
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine"
                                            SkinID="MultiText_Large"></asp:TextBox>
                                    </td>
                                    <td align="left"  width="20%" ><span class="field-label">Select a File</span> 
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="fp" runat="server" />

                                    </td>
                                </tr>
                              
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                             PageSize="20" CssClass="table table-bordered table-row">
                                            <RowStyle Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("sd_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Description">
                                                    <HeaderTemplate>
                                                       <div align="center">
                                                                    <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Description"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnEmpid_Search_Click" />
                                                                </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("sd_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="File Name">
                                                    <HeaderTemplate>
                                                       <div align="center">
                                                                    <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="File Name"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtOption1" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnuid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnuid_Search_Click" />
                                                                </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:ImageButton
                                                                        ID="imgF" runat="server" OnClick="imgF_Click" CommandArgument='<%# Bind("sd_id") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="txtfile" runat="server"
                                                                        Text='<%# Bind("sd_filename") %>' CommandName="View" CommandArgument='<%# Bind("sd_id") %>'
                                                                        OnClick="txtfile_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Show">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbShow" runat="server" Checked='<%# Bind("sd_bshow") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete"
                                                            Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="lnkDelete" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle  Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>                                                                                            
</asp:Content>

