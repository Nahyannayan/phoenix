Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports SKILLSCHEDULE

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page


                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_ACTIVITY_SCHEDULE And ViewState("MainMnu_code") <> "C312015") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else
                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else


                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                'Populate Academic Year
                Dim studCl As New studClass
                studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                'Populate Term 
                BindTerm()
                'Populate Activity
                BindActivity()
                'Populate Grade
                GetAllGradeForActivity()
                'Bind Time Details
                BindTime()
                'Populate Grade Slab Details
                BindGradeSlabDetails()
                imgQuick.Visible = False
                cpeQuick.Enabled = False
                pnlQuickDetail.Visible = False

                txtSubject.Attributes.Add("ReadOnly", "ReadOnly")
                If ViewState("datamode") = "view" Then
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    PopulateDetails(ViewState("viewid"))
                    Enable_disable_control(False, True)
                ElseIf ViewState("datamode") = "retest" Then
                    ltLabel.Text = "Retest Assessment Schedule"
                    imgQuick.Visible = True
                    cpeQuick.Enabled = True
                    pnlQuickDetail.Visible = True
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    PopulateDetails(ViewState("viewid"), True)
                    h_PARENT_ID.Value = ViewState("viewid")
                    ViewState("datamode") = "add"
                    h_CAS_ID.Value = ""
                    ViewState("retest") = True
                    Enable_disable_control(False, True)
                    Enable_disable_control(True, False)
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtDescription.Text = ddlACTIVITY.SelectedItem.Text
                ElseIf ViewState("datamode") = "add" Then
                    txtDescription.Text = ddlACTIVITY.SelectedItem.Text
                    SetAOLVisible()
                    txtDuration.Text = "0"
                    chkbAttendanceMandatory.Checked = True
                    chkAutoAllocate.Checked = True
                    'End If
                End If

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
                ' h_GRD_IDs.Value = grade(0)
        h_STM_ID.Value = grade(1)
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        End If
    End Sub

    Private Sub PopulateDetails(ByVal vCAS_ID As String, Optional ByVal bRetest As Boolean = False)
        Dim vACT_SCH As New SKILLSCHEDULE.SKILLSCHEDULE
        vACT_SCH = SKILLSCHEDULE.SKILLSCHEDULE.GetScheduleDetails(vCAS_ID)
        ddlAca_Year.SelectedIndex = -1
        ddlAca_Year.Items.FindByValue(vACT_SCH.ACD_ID).Selected = True
        ddlTerm.SelectedIndex = -1
        ddlTerm.Items.FindByValue(vACT_SCH.TRM_ID).Selected = True
        BindActivity()
        ddlACTIVITY.SelectedIndex = -1
        ddlACTIVITY.Items.FindByValue(vACT_SCH.ACTIVITY_ID).Selected = True
        '    ddlGrade.Items.FindByValue(vACT_SCH.Grade).Selected = True
        txtSubject.Text = vACT_SCH.SUBJECT
        h_SBG_ID.Value = getSBG_ID(vACT_SCH.CAS_ID)
        h_GRD_IDs.Value = vACT_SCH.Grade
        ddlGrade.SelectedItem.Text = h_GRD_IDs.Value
        h_PARENT_ID.Value = vACT_SCH.PARENT_ID
        Dim bCORE_EXT As Boolean = False
        If vACT_SCH.TYPE_LEVEL <> "NORMAL" Then
            bCORE_EXT = True
        End If
        h_SBM_ID.Value = vACT_SCH.SUBJECT_ID & "___" & CInt(bCORE_EXT) & "___" & vACT_SCH.SUBJECT

        imgSubject_Click(Nothing, Nothing)
        FillSelectedDetails(lstSubjectGroups.Items, vACT_SCH.SUBJECT_GROUP_ID)
        HideShowDurationDetails()
        If bRetest Then

            lblDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
            'lblGradeSlab.Text = ddlGradeSlab.Items.FindByValue(vACT_SCH.GRADE_SLAB_ID).Text
            lblDuration.Text = vACT_SCH.DURATION & " Hrs"
            Dim strTime As String
            strTime = IIf(vACT_SCH.ACTIVITY_TIME.Hour > 12, vACT_SCH.ACTIVITY_TIME.Hour - 12, vACT_SCH.ACTIVITY_TIME.Hour) & " : "
            strTime += Format(vACT_SCH.ACTIVITY_TIME, "mm")
            If vACT_SCH.ACTIVITY_TIME.Hour < 12 Then
                strTime += " AM"
            Else
                strTime += " PM"
            End If
            lblTime.Text = strTime
            lblFromDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
            lblToDate.Text = Format(vACT_SCH.ACTIVTY_TO_DATE, OASISConstants.DateFormat)
            lblMinMark.Text = Math.Round(vACT_SCH.MIN_MARK, 2)
            lblMaxMark.Text = Math.Round(vACT_SCH.MAX_MARK, 2)
            lblDescription.Text = vACT_SCH.DESCRIPTION
            lblRemarks.Text = vACT_SCH.REMARKS
            chkviewbmandatory.Checked = vACT_SCH.bATTEND_COMPULSORY
        Else
            h_CAS_ID.Value = vACT_SCH.CAS_ID
            txtFromDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
            txtToDate.Text = Format(vACT_SCH.ACTIVTY_TO_DATE, OASISConstants.DateFormat)
            txtDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
            Dim hour As String = Format(IIf(vACT_SCH.ACTIVITY_TIME.Hour > 12, vACT_SCH.ACTIVITY_TIME.AddHours(-12), vACT_SCH.ACTIVITY_TIME), "HH")
            If hour <> 0 Then
                ddlTimeH.SelectedIndex = -1
                ddlTimeH.Items.FindByValue(hour).Selected = True
            End If

            'If vACT_SCH.ACTIVITY_TIME.Hour < 12 Then
            '    ddlTimeAMPM.SelectedIndex = 0
            'Else
            '    ddlTimeAMPM.SelectedIndex = 1
            'End If
            If vACT_SCH.ACTIVITY_TIME.Minute <> 0 Then
                ddlTimeM.SelectedIndex = -1
                ddlTimeM.Items.FindByValue(vACT_SCH.ACTIVITY_TIME.Minute).Selected = True
            End If

            txtDuration.Text = vACT_SCH.DURATION
            FillGradeSlab(ddlGradeSlab, vACT_SCH.GRADE_SLAB_ID)
            txtMinMark.Text = Math.Round(vACT_SCH.MIN_MARK, 2)
            txtMaxMark.Text = Math.Round(vACT_SCH.MAX_MARK, 2)
            txtDescription.Text = vACT_SCH.DESCRIPTION
            txtremarks.Text = vACT_SCH.REMARKS
            chkbAttendanceMandatory.Checked = vACT_SCH.bATTEND_COMPULSORY
        End If
        trAOLMarks.Visible = (vACT_SCH.HasAOLMARKS And vACT_SCH.WithSkills <> True)
        If vACT_SCH.HasAOLMARKS And vACT_SCH.WithSkills <> True Then
            txtAOLKU.Text = vACT_SCH.MAX_MARK_AOL_KU
            txtAOLAPP.Text = vACT_SCH.MAX_MARK_AOL_APP
            txtAOLCOMM.Text = vACT_SCH.MAX_MARK_AOL_COMM
            txtAOLHOTS.Text = vACT_SCH.MAX_MARK_AOL_HOTS
            'txtAOLKU.Text = vACT_SCH.SkillsMaxMarksDt.Rows(0).Item(0)
            'txtAOLAPP.Text = vACT_SCH.SkillsMaxMarksDt.Rows(1).Item(0)
            'txtAOLCOMM.Text = vACT_SCH.SkillsMaxMarksDt.Rows(2).Item(0)
            'txtAOLHOTS.Text = vACT_SCH.SkillsMaxMarksDt.Rows(3).Item(0)
        End If
        trSkill.Visible = vACT_SCH.WithSkills

        If vACT_SCH.WithSkills Then
            Dim textControlName As String
            Dim dt As DataTable = getSkillCaption(vACT_SCH.CAS_ID)
            For i As Integer = 0 To 9
                textControlName = "txtSkill" + CType(i + 1, String)
                Dim MainContent As ContentPlaceHolder = CType(Page.Master.FindControl("cphMasterpage"), ContentPlaceHolder)
                Dim txtSkills As TextBox = CType(MainContent.FindControl(textControlName), TextBox)
                txtSkills.Text = vACT_SCH.SkillsMaxMarksDt.Rows(i).Item(0).ToString()
                If (vACT_SCH.SkillsMaxMarksDt.Rows(i).Item(0).ToString() = "0") Then
                    txtSkills.Enabled = False
                End If
            Next
            
            For j As Integer = 0 To dt.Rows.Count - 1
                Dim labelControlName As String
                Dim MainContent As ContentPlaceHolder = CType(Page.Master.FindControl("cphMasterpage"), ContentPlaceHolder)
                labelControlName = "lblSkill" + CType(j + 1, String)
                Dim lblSkills As Label = CType(MainContent.FindControl(labelControlName), Label)
                lblSkills.Text = dt.Rows(j).Item(1).ToString()
            Next
        End If
        trWS.Visible = vACT_SCH.WithoutSkills
        txtWS.Text = vACT_SCH.MAX_MARK_AOL_WITHOUTSKILLS
    End Sub

    Private Sub FillGradeSlab(ByRef ddlGSlab As DropDownList, ByVal SLAB_ID As String)
        ddlGSlab.ClearSelection()
        For Each itm As ListItem In ddlGSlab.Items
            If itm.Value.Split("___")(0) = SLAB_ID Then
                ddlGSlab.SelectedIndex = -1
                itm.Selected = True
                Exit Sub
            End If
        Next
    End Sub

    Private Sub FillSelectedDetails(ByRef lstitems As ListItemCollection, ByVal selVal As Hashtable)
        Dim ienum As IDictionaryEnumerator = selVal.GetEnumerator
        While ienum.MoveNext 
            lstitems.FindByValue(ienum.Value).Selected = True
        End While
    End Sub

    Private Sub BindTime()
        Dim dt As New DateTime(0)
        Dim arrList As New ArrayList
        For i As Integer = 1 To 12
            arrList.Add(Format(dt.AddHours(i), "hh"))
        Next
        ddlTimeH.DataSource = arrList
        ddlTimeH.DataBind()

        arrList = New ArrayList
        For i As Integer = 0 To 59
            arrList.Add(Format(dt.AddMinutes(i), "mm"))
        Next
        ddlTimeM.DataSource = arrList
        ddlTimeM.DataBind()
    End Sub

    Private Sub BindGradeSlabDetails()
        ddlGradeSlab.DataSource = SKILLSCHEDULE.SKILLSCHEDULE.GetGradeSlab(Session("sBSUID"))
        ddlGradeSlab.DataTextField = "GSM_DESC"
        ddlGradeSlab.DataValueField = "GSM_SLAB_ID"
        ddlGradeSlab.DataBind()
        ddlGradeSlab.Items.Add(New ListItem("--", 0))
        ddlGradeSlab.SelectedIndex = -1
        ddlGradeSlab.Items.FindByText("--").Selected = True
    End Sub

    Private Sub BindActivity()
        ddlACTIVITY.DataSource = SKILLSCHEDULE.SKILLSCHEDULE.GetActivityDetails(ddlAca_Year.SelectedValue, ddlTerm.SelectedValue)
        ddlACTIVITY.DataTextField = "CAD_DESC"
        ddlACTIVITY.DataValueField = "CAD_ID"
        ddlACTIVITY.DataBind()
        HideShowDurationDetails()
    End Sub

    Sub GetAllGradeForActivity()
        'ddlGrade.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBSUID"), ddlAca_Year.SelectedValue)
        'ddlGrade.DataTextField = "GRM_GRD_ID"
        'ddlGrade.DataValueField = "GRM_DISPLAY"
        'ddlGrade.DataBind()
        PopulateGrade(ddlGrade, ddlAca_Year.SelectedValue)
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If ddlTerm.SelectedIndex <> -1 Then
            BindActivity()
        Else
            ddlACTIVITY.Items.Clear()
        End If
    End Sub


    Sub SetAOLVisible()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(CAD_bAOL,'false'),isnull(CAD_bWITHOUTSKILLS,'false'),isnull(CAD_bSKILLS,'false') FROM ACT.ACTIVITY_D WHERE CAD_ID=" + ddlACTIVITY.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        trAOLMarks.Visible = ds.Tables(0).Rows(0).Item(0)
        trWS.Visible = ds.Tables(0).Rows(0).Item(1)
        If trWS.Visible = True Then
            trAOLMarks.Visible = False
        End If
    End Sub
    'Sub Student_Detail_Leaves()

    '    Using readerSTUD_Detail_Leaves As SqlDataReader = AccessStudentClass.GetStudent_Detail_Leaves(ViewState("viewid"))
    '        While readerSTUD_Detail_Leaves.Read


    '            ViewState("STU_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STU_ID"))
    '            ViewState("ACD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("ACD_ID"))
    '            ViewState("GRD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("GRD_ID"))
    '            ViewState("SCT_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SCT_ID"))
    '            ViewState("STM_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STM_ID"))
    '            ViewState("SHF_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SHF_ID"))



    '        End While
    '    End Using
    'End Sub
    Sub Enable_disable_control(ByVal bEnable As Boolean, ByVal bAll As Boolean)
        If bAll Then
            ddlAca_Year.Enabled = bEnable
            ddlTerm.Enabled = bEnable
            ddlGrade.Enabled = bEnable
            imgSubject.Enabled = bEnable
            lstSubjectGroups.Enabled = bEnable
            chkAutoAllocate.Enabled = bEnable
            radCore.Enabled = bEnable
            radExtended.Enabled = bEnable
            ddlACTIVITY.Enabled = bEnable
            chkAutoAllocate.Enabled = bEnable
        End If
        imgDate.Enabled = bEnable
        calDate1.Enabled = bEnable
        calDate2.Enabled = bEnable
        ddlTimeH.Enabled = bEnable
        ddlTimeM.Enabled = bEnable
        ddlTimeAMPM.Enabled = bEnable
        txtDuration.Enabled = bEnable
        ddlGradeSlab.Enabled = bEnable
        txtMinMark.Enabled = bEnable
        txtMaxMark.Enabled = bEnable
        txtDescription.Enabled = bEnable
        txtremarks.Enabled = bEnable
        chkbAttendanceMandatory.Enabled = bEnable

        txtAOLKU.Enabled = bEnable
        txtAOLAPP.Enabled = bEnable
        txtAOLCOMM.Enabled = bEnable
        txtAOLHOTS.Enabled = bEnable
        txtWS.Enabled = bEnable
    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlAca_Year.Enabled = False
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnSave.Enabled = True
        'Dim idRow As New Label
        'idRow = TryCast(row.FindControl("lblID"), Label)
        'Dim iEdit As Integer = 0
        'Dim iIndex As Integer = 0
        'iIndex = CInt(idRow.Text)
        ''loop through the data table row  for the selected rowindex item in the grid view
        ''For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        ''Next
        'For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        '    If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") = ddlGRD_ID_EDIT.SelectedItem.Value
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("GRD_DESC") = ddlGRD_ID_EDIT.SelectedItem.Text
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT") = txtCAD_DESC_Edit.Text
        '        Exit For
        '    End If
        'Next
    End Sub

    Private Function ValidPage() As Boolean
        lblError.Text = ""
        If GetSubjectGroups().Count <= 0 Then
            lblError.Text += "<br>Please Select atleast one group"
        End If
        If trPeriod.Visible Then
            If txtFromDate.Text = "" OrElse Not IsDate(txtFromDate.Text) Then
                lblError.Text += "<br>Please Enter valid From Date"
            End If
            If txtToDate.Text = "" OrElse Not IsDate(txtToDate.Text) Then
                lblError.Text += "<br>Please Enter valid To Date"
            End If
        Else
            If txtDate.Text = "" OrElse Not IsDate(txtDate.Text) Then
                lblError.Text += "<br>Please Enter valid Date"
            End If
            If txtDuration.Text = "" Then
                lblError.Text += "<br>Please Enter Activity Duration"
            End If
        End If

        If txtMinMark.Text = "" Then
            lblError.Text += "<br>Please Enter Minimum Pass Mark"
        End If
        If txtMaxMark.Text = "" Then
            lblError.Text += "<br>Please Enter Maximum Mark"
        End If
        If lblError.Text = "" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ValidPage() Then
            Exit Sub
        End If
        Dim ACT_SCH As New SKILLSCHEDULE.SKILLSCHEDULE

        ACT_SCH.ACD_ID = ddlAca_Year.SelectedValue
        ACT_SCH.TRM_ID = ddlTerm.SelectedValue
        ACT_SCH.ACTIVITY_ID = ddlACTIVITY.SelectedValue
        ACT_SCH.GRADE_ID = GetSelectedGrades()
        ACT_SCH.SUBJECT_ID = h_SBM_ID.Value
        ACT_SCH.SUBJECT_GROUP_ID = GetSubjectGroups()

        If trPeriod.Visible Then
            ACT_SCH.ACTIVTY_DATE = CDate(txtFromDate.Text)
            ACT_SCH.ACTIVTY_TO_DATE = CDate(txtToDate.Text)
            ACT_SCH.ACTIVITY_TIME = CDate(txtToDate.Text)
            ACT_SCH.DURATION = 0
        Else
            ACT_SCH.ACTIVTY_DATE = CDate(txtDate.Text)
            ACT_SCH.ACTIVTY_TO_DATE = CDate(txtDate.Text)
            ACT_SCH.ACTIVITY_TIME = GetTime()
            ACT_SCH.DURATION = txtDuration.Text
        End If

        ACT_SCH.GRADE_SLAB_ID = ddlGradeSlab.SelectedValue.Split("___")(0)
        ACT_SCH.MIN_MARK = txtMinMark.Text
        ACT_SCH.MAX_MARK = txtMaxMark.Text
        ACT_SCH.DESCRIPTION = txtDescription.Text
        ACT_SCH.REMARKS = txtremarks.Text
        ACT_SCH.TYPE_LEVEL = GetTypeLevel()
        ACT_SCH.bATTEND_COMPULSORY = chkbAttendanceMandatory.Checked
        ACT_SCH.bAUTOALLOCATE = chkAutoAllocate.Checked
        ACT_SCH.PARENT_ID = IIf(h_PARENT_ID.Value <> "", h_PARENT_ID.Value, 0)
        ACT_SCH.CAS_ID = IIf(h_CAS_ID.Value <> "", h_CAS_ID.Value, 0)

        ACT_SCH.HasAOLMARKS = trAOLMarks.Visible
        If ACT_SCH.HasAOLMARKS Then
            ACT_SCH.MAX_MARK_AOL_KU = Val(txtAOLKU.Text)
            ACT_SCH.MAX_MARK_AOL_APP = Val(txtAOLAPP.Text)
            ACT_SCH.MAX_MARK_AOL_COMM = Val(txtAOLCOMM.Text)
            ACT_SCH.MAX_MARK_AOL_HOTS = Val(txtAOLHOTS.Text)
        End If

        ACT_SCH.WithoutSkills = trWS.Visible
        If trWS.Visible = True Then
            ACT_SCH.HasAOLMARKS = True
            ACT_SCH.MAX_MARK_AOL_WITHOUTSKILLS = Val(txtWS.Text)
        End If
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'End If
        If trSkill.Visible = True Then
            Dim dt As DataTable = getSkillCaption(ACT_SCH.CAS_ID)
            ACT_SCH.Skills = New Hashtable
            Dim textControlName = String.Empty
            For i As Integer = 0 To 9
                textControlName = "txtSkill" + CType(i + 1, String)
                Dim MainContent As ContentPlaceHolder = CType(Page.Master.FindControl("cphMasterpage"), ContentPlaceHolder)
                Dim txtSkills As TextBox = CType(MainContent.FindControl(textControlName), TextBox)
                ACT_SCH.Skills(i) = txtSkills.Text
            Next
        End If
        GetSkillCount()

        Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = SKILLSCHEDULE.SKILLSCHEDULE.SaveDetails(ACT_SCH, objConn, stTrans)
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'errNo = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            If errNo = 0 Then
                stTrans.Commit()
                ClearAllFields()
                lblError.Text = "Details saved sucessfully..."
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(errNo)
            End If
        Catch
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(-1)
        Finally
            objConn.Close()
        End Try


        Enable_disable_control(False, True)
        'Dim str_err As String = String.Empty
        'Dim errorMessage As String = String.Empty
        'str_err = CallTransaction(errorMessage)
        'If str_err = "0" Then
        '    disable_control()
        '    lblError.Text = "Record updated successfully"
        'Else
        '    lblError.Text = errorMessage
        ' End If
    End Sub

    Private Sub ClearAllFields(Optional ByVal bClearTerm As Boolean = True)
        If bClearTerm Then
            ddlTerm.ClearSelection()
        End If
        ddlGrade.ClearSelection()
        lstSubjectGroups.ClearSelection()
        chkAutoAllocate.Checked = False
        trCoreExt.Visible = False
        radNormal.Checked = True
        ddlACTIVITY.ClearSelection()
        chkAutoAllocate.Checked = False
        ddlTimeH.ClearSelection()
        ddlTimeM.ClearSelection()
        ddlTimeAMPM.ClearSelection()
        txtDuration.Text = ""
        txtDate.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ddlGradeSlab.ClearSelection()
        txtMinMark.Text = ""
        txtMaxMark.Text = ""
        txtDescription.Text = ""
        txtremarks.Text = ""
        chkbAttendanceMandatory.Checked = False
        h_SBM_ID.Value = ""
        txtSubject.Text = ""
        lstSubjectGroups.Items.Clear()        
    End Sub

    Private Function GetTime() As DateTime
        Dim time As DateTime
        Dim examDate As Date = CDate(txtDate.Text)
        time = New DateTime(examDate.Year, examDate.Month, examDate.Day, _
        IIf(ddlTimeAMPM.SelectedValue = "PM", ddlTimeH.SelectedValue + 12, ddlTimeH.SelectedValue) _
        , ddlTimeM.SelectedValue, 0)
        Return time
    End Function

    Private Function GetTypeLevel() As String
        If radNormal.Checked Then
            Return "NORMAL"
        ElseIf radCore.Checked Then
            Return "CORE"
        ElseIf radExtended.Checked Then
            Return "EXTENDED"
        End If
        Return ""
    End Function

    Private Function GetSubjectGroups() As Hashtable
        Dim httab As New Hashtable
        For Each lstItem As ListItem In lstSubjectGroups.Items
            If lstItem.Selected Then
                httab(lstItem.Value) = lstItem.Value
            End If
        Next
        Return httab
    End Function

    Private Function GetSelectedGrades() As Hashtable
        Return Nothing
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Enable_disable_control(True, False)
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            imgQuick.Visible = False            
            pnlQuickDetail.Visible = False
            Enable_disable_control(True, True)
            ClearAllFields()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAllFields()
        BindTerm()

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ' h_GRD_IDs.Value = grade(0)
        h_STM_ID.Value = grade(1)
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_SBM_ID.Value <> "" AndAlso h_SBM_ID.Value.Contains("___") Then
            Dim vSBM_ID As String = h_SBM_ID.Value.Split("___")(0)
            Dim bCORE_EXT As Boolean = h_SBM_ID.Value.Split("___")(3)
            If bCORE_EXT Then
                trCoreExt.Visible = True
                radCore.Checked = True
                chkAutoAllocate.Checked = False
                chkAutoAllocate.Enabled = False
            Else
                radNormal.Checked = True
                trCoreExt.Visible = False
                chkAutoAllocate.Enabled = True
            End If
            txtSubject.Text = h_SBM_ID.Value.Split("___")(6)
            PopulateSubjectGroupDetails(vSBM_ID)
            h_SBM_ID.Value = vSBM_ID
        End If
    End Sub

    Private Sub PopulateSubjectGroupDetails(ByVal vSBM_ID As String)
        lstSubjectGroups.DataSource = SKILLSCHEDULE.SKILLSCHEDULE.GetSubjectGroups(Session("sBSUID"), ddlAca_Year.SelectedValue, 0, vSBM_ID, Session("CurrSuperUser"), Session("EmployeeId"))
        lstSubjectGroups.DataTextField = "SGR_DESCR"
        lstSubjectGroups.DataValueField = "SGR_ID"
        lstSubjectGroups.DataBind()
        If lstSubjectGroups.Items.Count = 1 Then
            lstSubjectGroups.SelectedIndex = 0
        End If
    End Sub

    'Protected Sub chkGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim strSelectedGrades As String = String.Empty
    '    Dim strSeperator As String = String.Empty
    '    For Each lstItem As ListItem In ddlGrade.Items
    '        If lstItem.Selected Then
    '            strSelectedGrades += strSeperator + lstItem.Value
    '            strSeperator = "___"
    '        End If
    '    Next
    '    h_GRD_IDs.Value = strSelectedGrades
    'End Sub

    'Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ViewState("datamode") = "add"
    '    h_PARENT_ID.Value = ""
    '    imgQuick.Visible = False
    '    cpeQuick.Enabled = False
    '    pnlQuickDetail.Visible = False
    '    ViewState("reset") = Nothing
    '    ltLabel.Text = "Activity Schedule"
    '    ClearAllFields()
    '    Enable_disable_control(True, True)
    '    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    'End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAllFields(False)
        BindActivity()
    End Sub

    Protected Sub ddlACTIVITY_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtDescription.Text = ddlACTIVITY.SelectedItem.Text
        HideShowDurationDetails()
    End Sub

    Sub HideShowDurationDetails()
        trPeriod.Visible = False
        trDate.Visible = False
        trTime.Visible = False

        trResetDuration.Visible = False
        trResetPeriod.Visible = False
        If ACTIVITYSCHEDULE.HasPeriod(ddlACTIVITY.SelectedValue) Then
            trResetPeriod.Visible = True
            trPeriod.Visible = True
        Else
            trDate.Visible = True
            trTime.Visible = True
            trResetDuration.Visible = True
        End If
    End Sub


    Protected Sub ddlGradeSlab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGradeSlab.SelectedIndexChanged
        Dim vGradeSlabID As String = ddlGradeSlab.SelectedValue.Split("___")(0)
        txtMaxMark.Text = SKILLSCHEDULE.SKILLSCHEDULE.FillMaxMarkDetails(vGradeSlabID, Session("sBSUID"))
        txtMinMark.Text = "0"
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'h_GRD_IDs.Value = grade(0)
        h_STM_ID.Value = grade(1)
    End Sub

    Protected Sub ddlACTIVITY_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACTIVITY.SelectedIndexChanged
        SetAOLVisible()
    End Sub

    Function getSkillCaption(CAS_ID As String) As DataTable

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SKL_ID,SKL_DESCR FROM SKIL.SKILLS_M INNER JOIN ( " _
                                    & "SELECT     Split.a.value('.', 'NVARCHAR(200)') AS ID   " _
                                    & "FROM  (SELECT   " _
                                    & "CAST ('<M>' + REPLACE([CAS_SKL_ID], '|', '</M><M>') + '</M>' AS XML) AS String  " _
                                    & "FROM  ACT.ACTIVITY_SCHEDULE WHERE CAS_ID='" + CAS_ID + "') AS A CROSS APPLY String.nodes ('/M') AS Split(a) " _
                                    & ")A ON SKL_ID=ID "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = ds.Tables(0)

        Return dt
    End Function
    Public Sub GetSkillCount()
        Dim str_sql As String = String.Empty

        str_sql = "SELECT " & _
                    "(	Select Count(*) " & _
                    "FROM SKIL.SKILLS_M " & _
                    "WHERE SKL_GRD_ID = " + h_GRD_IDs.Value + " And " & _
                    "SKL_ACD_ID = " + ddlAca_Year.SelectedItem.Value + " And " & _
                    "SKL_SBG_ID = " + h_SBG_ID.Value + " " & _
                    ") subcount , SKL_ID,SKL_DESCR " & _
                    "FROM SKIL.SKILLS_M " & _
                    "WHERE " & _
                    "SKL_GRD_ID = " + h_GRD_IDs.Value + "    AND  " & _
                    "SKL_ACD_ID = " + ddlAca_Year.SelectedItem.Value + "	AND  " & _
                    "SKL_SBG_ID = '" + h_SBG_ID.Value + "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Session("CountSkills") = ds.Tables(0).Rows(0).Item(0)
            Session("SkillIds") = ds.Tables(0)
        End If
    End Sub

    Function getSBG_ID(CAS_ID As String) As String

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAS_SBG_ID FROM ACT.ACTIVITY_SCHEDULE WHERE CAS_ID='" + CAS_ID + "'"

        Dim SBG_ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return SBG_ID
    End Function
  
End Class
