Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Partial Class Curriculam_showTopics_Term
    Inherits System.Web.UI.Page

    Protected Sub tvCostcenter_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvCostcenter.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
      ByVal parentNode As TreeNode)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String

        'str_Sql = "SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S  WHERE CCS_PARENT_CCS_ID = '" & parentid & "' " _
        '& " GROUP BY CCS_ID,CCS_DESCR ORDER BY CCS_ID "
        str_Sql = "SELECT SYD_ID,SYD_DESCR FROM SYL.SYLLABUS_D WHERE SYD_PARENT_ID=" + parentid + " GROUP BY SYD_ID,SYD_DESCR ORDER BY SYD_ID "

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub

    Private Sub PopulateRootLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        tvCostcenter.Nodes.Clear()

        str_Sql = "SELECT SYD_ID,SYD_DESCR FROM SYL.SYLLABUS_D " _
                  & " INNER JOIN SYL.SYLLABUS_M ON SYD_SYM_ID=SYM_ID " _
         & " WHERE  SYD_PARENT_ID=0  and SYM_SBG_ID = " + h_SbgId.Value
        If h_TermId.Value <> "0" And h_TermId.Value <> "" Then
            str_Sql += " AND SYM_TRM_ID=" + h_TermId.Value
        End If

        str_Sql += " GROUP BY SYD_ID,SYD_DESCR ORDER BY SYD_ID "


        'str_Sql = "SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S  WHERE (/*CCS_PARENT_CCS_ID ='0010' OR*/ CCS_ID = '0010') GROUP BY CCS_ID,CCS_DESCR ORDER BY CCS_ID"

        'str_Sql = " SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S  WHERE CCS_PARENT_CCS_ID IS NULL  " _
        '        & " AND  CCS_ID IN (SELECT CCS_PARENT_CCS_ID FROM COSTCENTER_S )GROUP BY CCS_ID,CCS_DESCR  " _
        '        & " ORDER BY CCS_ID"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvCostcenter.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("SYD_DESCR").ToString()
            tn.Value = dr("SYD_ID").ToString().Trim()
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = True
            'tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
            ' PopulateSubLevel(dr("CCS_ID").ToString(), tn)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        tvCostcenter.Nodes.Clear()
        PopulateRootLevel()
        tvCostcenter.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            h_TermId.Value = Request.QueryString("TermId")
            h_SbgId.Value = Request.QueryString("SbgId")
            PopulateTree()
        End If
    End Sub

    Private Sub CloseWndow()
        Dim javaSr As String
        'Response.Write("<script language='javascript'> function listen_window(){")
        ''javaSr += "window.returnValue = '" & tvCostcenter.SelectedNode.Value & "||" & tvCostcenter.SelectedNode.Text.Replace("'", "\'") & "';"
        '' javaSr += "window.close();"
        'Response.Write("var oArg = new Object();")
        'Response.Write("oArg.Topic ='" & tvCostcenter.SelectedNode.Value & "||" & tvCostcenter.SelectedNode.Text.Replace("'", "\'") & "';")
        'Response.Write("var oWnd = GetRadWindow('" & tvCostcenter.SelectedNode.Text + "||" + tvCostcenter.SelectedNode.Value & "');")
        'Response.Write("oWnd.close(oArg);")
        'Response.Write("} </script>")
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)



        javaSr = "<script language='javascript'> function listen_window(){"
        javaSr += " var oArg = new Object();"
        javaSr += "oArg.Topic ='" & tvCostcenter.SelectedNode.Value & "||" & tvCostcenter.SelectedNode.Text.Replace("'", "\'") & "' ; "
        javaSr += "var oWnd = GetRadWindow('" & tvCostcenter.SelectedNode.Value & "||" & tvCostcenter.SelectedNode.Text.Replace("'", "\'") & "');"
        javaSr += "oWnd.close(oArg);"
        javaSr += "} </script>"
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        

    End Sub

    Protected Sub tvCostcenter_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CloseWndow()
    End Sub
End Class
