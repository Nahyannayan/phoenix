<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmStreamAllocationRequest.aspx.vb" Inherits="clmStreamAllocationRequest" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
         Stream Allocation For Grade
                            <asp:Label ID="lblGrade" runat="server"></asp:Label>
                                        &nbsp;(Academic Year :
                            <asp:Label ID="lblAcademicYear" runat="server"></asp:Label>
                                        )<asp:Label ID="Label1" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="Table1"  width="100%">
                    <tr  >
                        <td align="left" class="title"  >
                            <span style="display: block; float: left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    ></asp:Label>
                            </span>
                        </td>
                    </tr>
                </table>
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td class="matters" valign="bottom">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Section</span>
                                    </td>   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Student ID</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStud_ID" runat="server"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student Name</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStud_Name" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                            Text="List"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" CssClass="table  table-bordered table-row "
                                            EmptyDataText="No Records Found"  PageSize="20"  
                                            Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CGPA" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCGPA" Text='<%# Bind("CGPA") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Science Grade" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScience" Text='<%# Bind("SCIENCE") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Maths Grade" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMaths" Text='<%# Bind("MATHS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Choice 1">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice1" runat="server" Text='<%# Bind("CHOICE1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Choice 2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice2" runat="server" Text='<%# Bind("CHOICE2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Choice 3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice3" runat="server" Text='<%# Bind("CHOICE3") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" CommandName="Edit" runat="server" Text="Edit" OnClick="lblEdit_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop"  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_Row" type="hidden" runat="server" />
                <asp:Panel ID="pnlPopup" runat="server" CssClass="panel-cover"  >

                    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0" >

                        <tr class="title-bg-small">
                            <td colspan="3">Select Choice
                            </td>
                        </tr>
                        <tr align="left">
                            <td  ><span class="field-label">Student Id</span> </td>                         
                            <td>
                                <asp:Label ID="lblstudNo" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                        </tr>
                        <tr class="matters" align="left">
                            <td  ><span class="field-label">Name </span> </td>                         
                            <td>
                                <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblerror1" runat="server" CssClass="error" EnableViewState="False" ></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td ><span class="field-label">Choice 1</span> </td>
                                        <td ><span class="field-label">Choice 2 </span></td>
                                        <td ><span class="field-label">Choice 3</span> </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <asp:DropDownList ID="ddlStream1" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlStream1_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions1" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row"
                                                OnRowDataBound="gvOptions_RowDataBound" Width="100%">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice1"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice1Option" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlChoice1Option_SelectedIndexChanged"  >
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlStream2" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlStream2_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions2" runat="server" AutoGenerateColumns="False"
                                                BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" CssClass="matters"
                                                OnRowDataBound="gvOptions_RowDataBound" Width="100%">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice2"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice2Option" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlChoice2Option_SelectedIndexChanged" >
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlStream3" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlStream3_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:GridView ID="gvOptions3" runat="server" AutoGenerateColumns="False"
                                                BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" CssClass="matters"
                                                OnRowDataBound="gvOptions_RowDataBound" Width="100%">
                                                <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Option" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice" runat="server" Text="choice3"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Option">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlChoice3Option" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlChoice3Option_SelectedIndexChanged"  >
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OptId" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("OPT_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <tr >
                                <td align="center" colspan="11">
                                    <table style="border-right: 1px; border-top: 1px; border-left: 1px; border-bottom: 1px; border-collapse: collapse">
                                        <tbody>
                                            <tr>
                                                <td colspan="3">
                                                    <table>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="matters" align="center">
                                                <td   colspan="3">
                                                    <asp:Button ID="btnStudSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Popup"></asp:Button>
                                                    <asp:Button ID="btnClose" runat="server" CausesValidation="false" Text="Close" CssClass="button"></asp:Button>
                                                    <asp:HiddenField ID="hfSTU_ID" runat="server" />
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Popup"></asp:ValidationSummary>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <%--</tbody>--%>
                    </table>

                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPSetOption" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="pnlPopup" RepositionMode="RepositionOnWindowResizeAndScroll"
                    TargetControlID="Label1">
                </ajaxToolkit:ModalPopupExtender>
            </div>
    </div>
    </div>
</asp:Content>
