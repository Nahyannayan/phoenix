﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM

Partial Class Curriculum_clmCASEStudentAttendance
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")

            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))



                    BindGrade()
                    FillSection()
                    BindSubject()


                    gridbind()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= '" + ddlAcademicYear.SelectedValue + "'  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedItem.Value + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID  IN('04','06','08') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                         & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"
        Else
            str_query = "SELECT DISTINCT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                       & " INNER JOIN SUBJECTS_GRADE_S ON SBG_SBM_ID=SBM_ID" _
                       & " INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID" _
                       & " INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID AND SGS_TODATE IS NULL" _
                       & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC') AND SGS_EMP_ID=" + Session("EmployeeID") _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                       & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub
    Sub getACYID()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        hfACY_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        FillSection()
        BindSubject()
        gridbind()
        GridBindStudents(0)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        FillSection()
        BindSubject()
        gridbind()
        GridBindStudents(0)
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
            getACYID()

            str_Sql = " select SSA_ID,(select STU_NO   from " _
                       & " VW_STUDENT_M where STU_id=SSA_STU_ID) as stu_no,(select stu_NAME=ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')  from " _
                      & " VW_STUDENT_M where STU_id=SSA_STU_ID) as stu_NAME, " _
                       & " (select SCT_DESCR from VW_STUDENT_M inner join oasis.dbo.SECTION_M on STU_SCT_ID =sct_id and STU_id=SSA_STU_ID) as stu_sect " _
                       & " from CE.STUDENT_ABSENTEES where SSA_BSU_ID =" & Session("sBSUID")


            If ddlAcademicYear.SelectedValue <> "0" Then
                str_Sql += " AND SSA_ACY_ID='" + hfACY_ID.Value + "'"
            End If

            If ddlGrade.SelectedValue <> "0" Then
                str_Sql += " AND SSA_GRD_ID ='" + ddlGrade.SelectedValue.ToString + "'"
            End If
            
            If ddlSubject.SelectedValue <> "0" Then
                str_Sql += " AND SSA_SBM_ID ='" + ddlSubject.SelectedValue.ToString + "'"
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvGroup.DataSource = ds.Tables(0)
                gvGroup.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataSource = ds.Tables(0)
                Try
                    gvGroup.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        gridbind()
        GridBindStudents(0)
    End Sub
    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click
        Try


            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""


            For Each row As GridViewRow In grdStudent.Rows
                Dim lb As Label = row.FindControl("lbstu_no")
                str_query = "exec CE.insert_STUDENT_ABSENTEES  '" & lb.Text & "','" & Session("sBsuid") & "'," & hfACY_ID.Value & ",'" & ddlGrade.SelectedValue & "'," & ddlSubject.SelectedValue
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
            lblError.Text = "Saved...."
            gridbind()
            GridBindStudents(0)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvGroup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvGroup.RowDeleting

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Try
            gvGroup.SelectedIndex = e.RowIndex
            'Dim ID As Integer = CInt(gvComments.DataKeys(e.RowIndex).Value)

            Dim row As GridViewRow = DirectCast(gvGroup.Rows(e.RowIndex), GridViewRow)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblID"), Label)

            If lblID.Text <> "" Then
                str_query = "exec ce.delete_STUDENT_ABSENTEES   " & Val(lblID.Text)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
End Class
