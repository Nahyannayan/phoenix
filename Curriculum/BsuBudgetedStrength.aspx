<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BsuBudgetedStrength.aspx.vb" Inherits="Students_BsuBudgetedStrength" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
        <tr class="title">
            <td align="left">
                School Strength</td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;<asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                    Height="24px" SkinID="error" style="text-align: center" Width="133px"></asp:Label>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" style="font-weight: bold; height: 93px" valign="top">
                <table id="tabmain" align="center" border="1" bordercolor="#1b80b6" cellpadding="5"
                    cellspacing="0" class="BlueTableView" style="width: 348px">
                    <tr class="subheader_img">
                        <td align="left" colspan="9" style="width: 169px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                School Strength</span></font></td>
                    </tr>
                    <tr id="row2">
                        <td align="left" class="matters" style="width: 267px">
                            Accadamic Year&nbsp;</td>
                        <td align="left" class="matters" style="width: 46px">
                            <asp:DropDownList id="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged"
                                Width="169px">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="5">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="6">
                            <asp:GridView id="gvUNITS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" OnPageIndexChanged="gvUNITS_PageIndexChanged" PageSize="20"
                                Width="525px">
                                <rowstyle cssclass="griditem" height="25px" />
                                <columns>
<asp:BoundField DataField="BSU_ID" HeaderText="Business Unit">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BSU_NAME" HeaderText="Unit Name"></asp:BoundField>
<asp:TemplateField HeaderText="Strength"><ItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Width="86px"></asp:TextBox>
</ItemTemplate>
</asp:TemplateField>
</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" height="30px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 29px" valign="bottom">
                &nbsp;
                <asp:Button id="btnSave" runat="server" CssClass="button" onclick="btnSave_Click"
                    tabIndex="7" Text="Save" ValidationGroup="groupM1" Width="45px" />
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    tabIndex="8" Text="Cancel" UseSubmitBehavior="False" Width="54px" />
            </td>
        </tr>
    </table>
</asp:Content>

