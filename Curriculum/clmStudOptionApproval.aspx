﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmStudOptionApproval.aspx.vb" Inherits="Curriculum_clmStudOptionApproval"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

  var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvStudent.ClientID%>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
    rowObject.style.backgroundColor = '#deffb4'; //#f6deb2 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}



    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1 && document.forms[0].elements[i].disabled==false)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )  
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }           
                
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Student Option Approval
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="font-weight: bold;" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" cellpadding="3" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Grade</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:DataList ID="dlOptions" runat="server" RepeatColumns="10" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lblOption" runat="server" Text='<%# Bind("OPT_DESCR") %>' Width="100%"
                                                                ></asp:LinkButton>
                                                            <asp:Label ID="lblOptID" runat="server" Text='<%# Bind("OPT_ID") %>' Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="false" CssClass="table table-row table-bordered bg-white" Width="50%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="SBG_DESCR" HeaderText="Subjects">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REQ_COUNT" HeaderText="Requested">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ALT_COUNT" HeaderText="Alloted">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                                                 OffsetY="20" PopDelay="50" PopupControlID="gvOptions" PopupPosition="center"
                                                                TargetControlID="lblOption">
                                                            </ajaxToolkit:HoverMenuExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student No</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Student Name</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:RadioButton ID="rdAllOption" runat="server" BorderStyle="None" Checked="True"
                                            GroupName="G1" Text="All Options" AutoPostBack="true" />
                                        <asp:RadioButton ID="rdSubject" runat="server" GroupName="G1" Text="Filter By Subject"
                                            AutoPostBack="true" />
                                        <asp:RadioButton ID="rdOption" runat="server" GroupName="G1" Text="Filter By Option"
                                            AutoPostBack="true" />
                                    </td>
                                    <td align="left"  width="20%">
                                        <span class="field-label" >Section</span>
                                    </td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="trSubject" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject1" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Option</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlOption1" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trOption" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Option</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlOption2" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject2" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:RadioButton ID="rdAllStudents" runat="server" BorderStyle="None" GroupName="G2"
                                            Text="All Students" />
                                        <asp:RadioButton ID="rdAlloted" runat="server" GroupName="G2" Text="Alloted" />
                                        <asp:RadioButton ID="rdNotAlloted" runat="server" GroupName="G2" Text="Not Alloted"
                                            Checked="True" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Not Requested Students" />
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Show Records</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtRecords" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="title-bg-light">
                                    <td align="left" colspan="4" valign="middle" width="20%">
                                        <span class="field-label">Academic Performance</span>
                                    </td>
                                </tr>
                                <tr id="tr1" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Select AcademicYear</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear2" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%" align="left"><span class="field-label">Select ReportCard</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="trApr1" runat="server" align="left">
                                    <td colspan="4">
                                        <asp:Button ID="btnApprove1" runat="server" Text="Approve" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="trGrd" runat="server">
                                    <td colspan="4" align="left">
                                        <asp:Label ID="lblGrdError" runat="server" CssClass="error_password" SkinID="Error"></asp:Label>
                                        <asp:GridView ID="gvStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" BorderStyle="None">
                                            <RowStyle Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <span class="field-label">Select</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="stu_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNoView() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblName" runat="server" Text='<%# Bind("STU_NAME") %>' Style="text-decoration: none;"
                                                            ForeColor="#1B80B6"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Level/Grade">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltLevel" runat="server"></asp:Literal>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H1" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H1" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt1" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption1" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H2" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H2" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt2" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption2" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H3" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H3" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt3" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption3" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H4" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H4" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt4" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption4" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H5" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H5" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt5" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption5" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H6" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H6" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt6" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption6" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H7" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H7" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt7" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption7" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H8" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H8" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt8" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption8" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H9" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H9" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt9" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption9" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H10" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H10" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt10" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption10" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H11" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H11" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt11" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption11" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H12" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H13" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt12" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption12" Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit Request"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle></HeaderStyle>
                                        </asp:GridView>
                                        <asp:HiddenField ID="hfTotalOptions" runat="server" />
                                        <asp:HiddenField ID="hfGRD_ID" runat="server" />
                                        <asp:HiddenField ID="hfOptionFilter" runat="server" />
                                        <br />
                                        <asp:HiddenField ID="hfSubjectFilter" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trApr2" runat="server" visible="true">
                                    <td colspan="4">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                    HorizontalSide="Center" TargetControlID="lblGrdError" VerticalSide="Middle">
                </ajaxToolkit:AlwaysVisibleControlExtender>

            </div>
        </div>
    </div>
</asp:Content>
