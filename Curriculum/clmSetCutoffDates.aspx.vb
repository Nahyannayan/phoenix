Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class clmActivity_D_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("datamode") = "add"
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call callYEAR_DESCRBind()
                    Call BindReportType()
                    Call BindReportPrintedFor()
                    PopulateDetails(ddlReportPrintedFor.SelectedValue)

                    'If ViewState("datamode") = "view" Then
                    '    h_Row.Value = "1"
                    '    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    '    DISABLE_CONTROL()
                    'ElseIf ViewState("datamode") = "add" Then
                    '    h_Row.Value = "2"
                    '    ViewState("SRNO") = 1
                    'End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub DISABLE_CONTROL()
        ddlAca_Year.Enabled = False
        ddlCAD_CAM_ID.Enabled = False
    End Sub
    Sub ENABLE_CONTROL()
        ddlAca_Year.Enabled = True
        ddlCAD_CAM_ID.Enabled = True
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        btnSave.Enabled = True

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        str_err = CallTransaction(errorMessage)
        If str_err = "0" Then
            'DISABLE_CONTROL()
            lblError.Text = "Record updated successfully"
        Else
            lblError.Text = errorMessage
        End If
    End Sub

    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim ReturnFlag As Integer = 0
        Dim RecordSTATUS As String = String.Empty
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ATT_STARTDATE", GetData(usrDPAttStDt.SelectedText()))
                pParms(1) = New SqlClient.SqlParameter("@ATT_ENDDATE", GetData(usrDPAttEndDt.SelectedText()))
                pParms(2) = New SqlClient.SqlParameter("@FEE_CUTOFF_DATE", GetData(usrDPFeeCutoff.SelectedText()))
                pParms(3) = New SqlClient.SqlParameter("@FEE_CUTOFF_AMOUNT", GetData(txtAmount.Text))
                pParms(4) = New SqlClient.SqlParameter("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVECUTOFF_DETAILS", pParms)
                ReturnFlag = pParms(5).Value
            Catch ex As Exception
                errorMessage = "Record could not be Updated"
            Finally
                If ReturnFlag <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using
        Return ReturnFlag
    End Function
    
    Public Function GetData(ByVal vDATA As Object) As Object
        If vDATA.ToString = "" Then
            Return DBNull.Value
        End If
        Return vDATA
    End Function

    Private Function GetDateWithFormat(ByVal dt As String) As DateTime

    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        ENABLE_CONTROL()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ENABLE_CONTROL()
        ddlAca_Year.Enabled = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            DISABLE_CONTROL()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
            " FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
            " WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindReportType()
        Try
            Dim ds As DataSet = ReportFunctions.GetReportPrinetdID(Session("sBSUID"), ddlAca_Year.SelectedValue)
            ddlCAD_CAM_ID.DataSource = ds.Tables(0)
            ddlCAD_CAM_ID.DataTextField = "RSM_DESCR"
            ddlCAD_CAM_ID.DataValueField = "RSM_ID"
            ddlCAD_CAM_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindReportPrintedFor()
        Try
            Dim ds As DataSet = ReportFunctions.GetReportPrintedFor_ALL(ddlCAD_CAM_ID.SelectedValue, False)
            ddlReportPrintedFor.DataSource = ds.Tables(0)
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlCAD_CAM_ID.ClearSelection()
        ddlCAD_CAM_ID.SelectedIndex = 0
    End Sub

    Protected Sub ddlCAD_CAM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCAD_CAM_ID.SelectedIndexChanged
        BindReportPrintedFor()
        PopulateDetails(ddlReportPrintedFor.SelectedValue)
    End Sub

    Protected Sub ddlReportPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportPrintedFor.SelectedIndexChanged
        PopulateDetails(ddlReportPrintedFor.SelectedValue)
    End Sub

    Sub PopulateDetails(ByVal RPF_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim dr As SqlDataReader
            str_Sql = " SELECT RPF_ID, RPF_DATE, RPF_FEE_CUTTOFF_AMT, " & _
            "RPF_ATT_STARTDATE, RPF_ATT_ENDDATE FROM RPT.REPORT_PRINTEDFOR_M " & _
            " WHERE RPF_ID = " & RPF_ID
            dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

            While (dr.Read())
                txtAmount.Text = DoNullCheck(dr("RPF_FEE_CUTTOFF_AMT"), "NUMBER")
                usrDPFeeCutoff.SetDate = DoNullCheck(dr("RPF_DATE"), "datetime")
                usrDPAttStDt.SetDate = DoNullCheck(dr("RPF_ATT_STARTDATE"), "datetime")
                usrDPAttEndDt.SetDate = DoNullCheck(dr("RPF_ATT_ENDDATE"), "datetime")
            End While

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Function DoNullCheck(ByVal obj As Object, ByVal sqlType As String) As String
        If obj Is DBNull.Value Then
            Return ""
        Else
            Select Case sqlType.ToUpper
                Case "STRING"
                    Return obj.ToString
                Case "DATETIME"
                    Return Format(obj, OASISConstants.DateFormat)
                Case "NUMBER"
                    Return String.Format("{0:0.00}", obj)
            End Select
        End If
    End Function

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        Call BindReportType()
        Call BindReportPrintedFor()
        PopulateDetails(ddlReportPrintedFor.SelectedValue)
    End Sub
End Class
