Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class clmStreamAllocationRequest
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            ' Try
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'if query string returns Eid  if datamode is view state
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330127") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                'disable the control buttons based on the rights
                ViewState("datamode") = "add"
                lblAcademicYear.Text = GetCurrentACY_DESCR()
                lblGrade.Text = " 10 "
                BindSection()
             

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message)
            'End Try

        End If
    End Sub


    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.Integer"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STU_NAME As New DataColumn("STU_NAME", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NAME)
            dtDt.Columns.Add(STU_NAME)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click

        GridBind()
        gvStud.Attributes.Add("bordercolor", "#1b80b6")
        Session("OptionalSubjects") = Nothing
        Session("OptionGroup") = Nothing



        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
       
      
    End Sub

    Private Sub GridBind()
        Dim conn_str As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [STREAM].[GETSTUDENTSTREAMREPOUESTDETAILS] " _
                                  & " @BSU_ID='" + Session("sbsuid") + "'," _
                                  & " @ACD_ID=" + Session("current_acd_id") + "," _
                                  & " @ACD_NEXT=" + Session("next_acd_id") + "," _
                                  & " @SCT_ID=" + ddlSection.SelectedValue.ToString + "," _
                                  & " @STU_NO='" + txtStud_ID.Text + "'," _
                                  & " @STU_NAME='" + txtStud_Name.Text + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_query)



        gvStud.DataSource = ds
        gvStud.DataBind()


    End Sub

    Sub BindOptions(ByVal stm_id As String, ByVal gvOptions As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='11'" _
                                & " AND SBG_STM_ID=" + stm_id _
                                & " AND SBG_ACD_ID=" + Session("next_acd_id") _
                                & " ORDER BY OPT_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvOptions.DataSource = ds
        gvOptions.DataBind()
        gvOptions.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

    Sub BindSubjects(ByVal OptId As String, ByVal ddlSubject As DropDownList, ByVal stm_id As String)

        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,SBG_DESCR FROM " _
                      & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                      & " SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                      & " C.SBG_ID WHERE SBG_GRD_ID='11'" _
                      & " AND SBG_STM_ID=" + stm_id _
                      & " AND SBG_ACD_ID=" + Session("NEXT_ACD_ID") _
                      & " AND SGO_OPT_ID=" + OptId

        str_query += " ORDER BY SBG_DESCR "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

    End Sub

    Sub getData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SRA_CHOICE1_STM_ID,SRA_CHOICE2_STM_ID,SRA_CHOICE3_STM_ID FROM STREAM.STREAM_REQUEST " _
                                & " WHERE SRA_STU_ID=" + hfSTU_ID.Value + " AND SRA_ACD_ID=" + Session("NEXT_ACD_ID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                If Not ddlStream1.Items.FindByValue(.Item("SRA_CHOICE1_STM_ID")) Is Nothing Then
                    ddlStream1.ClearSelection()
                    ddlStream1.Items.FindByValue(.Item("SRA_CHOICE1_STM_ID")).Selected = True
                End If
                If Not ddlStream2.Items.FindByValue(.Item("SRA_CHOICE2_STM_ID")) Is Nothing Then
                    ddlStream2.ClearSelection()
                    ddlStream2.Items.FindByValue(.Item("SRA_CHOICE2_STM_ID")).Selected = True
                End If
                If Not ddlStream3.Items.FindByValue(.Item("SRA_CHOICE3_STM_ID")) Is Nothing Then
                    ddlStream3.ClearSelection()
                    ddlStream3.Items.FindByValue(.Item("SRA_CHOICE3_STM_ID")).Selected = True
                End If
            End With
        End If

    End Sub

    Sub GetOptions(ByVal ddlSubject As DropDownList, ByVal choice As String, ByVal optid As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SRO_SBG_ID FROM STREAM.STREAM_OPTION_REQUEST WHERE SRO_STU_ID=" + hfSTU_ID.Value _
                                & " AND SRO_ACD_ID=" + Session("NEXT_ACD_ID") + " AND SRO_CHOICE='" + choice + "'" _
                                & " AND SRO_OPT_ID=" + optid
        Dim sbg_id As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If Not ddlSubject.Items.FindByValue(sbg_id) Is Nothing Then
            ddlSubject.Items.FindByValue(sbg_id).Selected = True
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC STREAM.saveSTREAMALLOCATION_REQUEST" _
                                & " @SRA_STU_ID =" + hfSTU_ID.Value + "," _
                                & " @SRA_ACD_ID =" + Session("next_acD_id") + "," _
                                & " @SRA_CHOICE1_STM_ID =" + ddlStream1.SelectedValue.ToString + "," _
                                & " @SRA_CHOICE2_STM_ID =" + ddlStream2.SelectedValue.ToString + "," _
                                & " @SRA_CHOICE3_STM_ID =" + ddlStream3.SelectedValue.ToString

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub SaveOptions(ByVal gvOption As GridView, ByVal choice As String)
        Dim i As Integer
        Dim ddlOption As DropDownList
        Dim lblOptId As Label
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim stream As String
        Dim str_query As String
        For i = 0 To gvOption.Rows.Count - 1
            With gvOption.Rows(i)
                lblOptId = .FindControl("lblOptId")
                If choice = "CHOICE1" Then
                    ddlOption = .FindControl("ddlChoice1Option")
                    stream = ddlStream1.SelectedValue.ToString
                ElseIf choice = "CHOICE2" Then
                    ddlOption = .FindControl("ddlChoice2Option")
                    stream = ddlStream2.SelectedValue.ToString
                ElseIf choice = "CHOICE3" Then
                    ddlOption = .FindControl("ddlChoice3Option")
                    stream = ddlStream3.SelectedValue.ToString
                End If
            End With

            str_query = "exec Stream.saveSTREAMOPTIONREQUEST " _
                     & " @SRO_STU_ID =" + hfSTU_ID.Value + "," _
                     & " @SRO_ACD_ID =" + Session("next_acd_id") + "," _
                     & " @SRO_STM_ID =" + stream + "," _
                     & " @SRO_SBG_ID =" + ddlOption.SelectedValue.ToString + "," _
                     & " @SRO_OPT_ID =" + lblOptId.Text + "," _
                     & " @SRO_CHOICE ='" + choice + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next

    End Sub
    Function checkUniqueOption(ByVal gvOption As GridView, ByVal choice As String) As Boolean
        Dim i As Integer
        Dim ddlOption As DropDownList
        Dim strSbgid As String = "-1"
        For i = 0 To gvOption.Rows.Count - 1
            With gvOption.Rows(i)
                If choice = "choice1" Then
                    ddlOption = .FindControl("ddlChoice1Option")
                ElseIf choice = "choice2" Then
                    ddlOption = .FindControl("ddlChoice2Option")
                ElseIf choice = "choice3" Then
                    ddlOption = .FindControl("ddlChoice3Option")
                End If
            End With
            If ddlOption.SelectedValue.ToString = strSbgid Then
                Return False
            End If
            strSbgid = ddlOption.SelectedValue.ToString
        Next
        Return True
    End Function


    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblChoice As Label
            Dim lblOptId As Label
            lblOptId = e.Row.FindControl("lblOptId")
            lblChoice = e.Row.FindControl("lblChoice")
            If lblChoice.Text = "choice1" Then
                Dim ddlChoice1Option As DropDownList
                ddlChoice1Option = e.Row.FindControl("ddlChoice1Option")
                BindSubjects(lblOptId.Text, ddlChoice1Option, ddlStream1.SelectedValue.ToString)
                GetOptions(ddlChoice1Option, "CHOICE1", lblOptId.Text)
            ElseIf lblChoice.Text = "choice2" Then
                Dim ddlChoice2Option As DropDownList
                ddlChoice2Option = e.Row.FindControl("ddlChoice2Option")
                BindSubjects(lblOptId.Text, ddlChoice2Option, ddlStream2.SelectedValue.ToString)
                GetOptions(ddlChoice2Option, "CHOICE2", lblOptId.Text)
            ElseIf lblChoice.Text = "choice3" Then
                Dim ddlChoice3Option As DropDownList
                ddlChoice3Option = e.Row.FindControl("ddlChoice3Option")
                BindSubjects(lblOptId.Text, ddlChoice3Option, ddlStream3.SelectedValue.ToString)
                GetOptions(ddlChoice3Option, "CHOICE3", lblOptId.Text)
            End If

            ' BindSubjects(lblOptId.Text, ddlChoice1)
            ' GetRequestedSubjects(lblOptId.Text, ddlChoice1)
        End If
    End Sub


    Private Sub BindStream(ByVal ddlStream As DropDownList)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT STM_ID, STM_DESCR FROM VW_STREAM_M WHERE STM_ID IN(2,3,4)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlStream.Items.Insert(0, li)
    End Sub

   
  
 
    

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim panel As UpdatePanel = Master.FindControl("UpdatePanel1")
        panel.UpdateMode = UpdatePanelUpdateMode.Conditional
        panel.ChildrenAsTriggers = True
    End Sub

    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub



    Private Function GetCurrentACY_DESCR() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & Session("next_ACD_ID") & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Sub BindSection()
        If Session("CurrSuperUser") = "Y" Then
            ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), Session("Current_ACD_ID"), "10")
        Else
            ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), Session("Current_ACD_ID"), "10", Session("EmployeeID"), False)
        End If
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
       
    End Sub

    Protected Sub ddlStream1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream1.SelectedValue.ToString, gvOptions1)
        MPSetOption.Show()
    End Sub

    Protected Sub ddlStream2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream2.SelectedValue.ToString, gvOptions2)
        MPSetOption.Show()
    End Sub

    Protected Sub ddlStream3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptions(ddlStream3.SelectedValue.ToString, gvOptions3)
        MPSetOption.Show()
    End Sub

    Protected Sub ddlChoice1Option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        MPSetOption.Show()

    End Sub

    Protected Sub ddlChoice2Option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        MPSetOption.Show()

    End Sub

    Protected Sub ddlChoice3Option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        MPSetOption.Show()

    End Sub



    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        'Try
        '    If (e.Row.RowType = DataControlRowType.DataRow) Then
        '        Dim ddlChoice1, ddlChoice2, ddlChoice3 As New DropDownList
        '        ddlChoice1 = e.Row.FindControl("ddlChoice1")
        '        ddlChoice2 = e.Row.FindControl("ddlChoice2")
        '        ddlChoice3 = e.Row.FindControl("ddlChoice3")
        '        If Not ddlChoice1 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice1)
        '            dllChoice1_SelectedIndexChanged(ddlChoice1, e)
        '        End If
        '        If Not ddlChoice2 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice2)
        '            dllChoice2_SelectedIndexChanged(ddlChoice2, e)
        '        End If
        '        If Not ddlChoice3 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice3)
        '            dllChoice3_SelectedIndexChanged(ddlChoice3, e)
        '        End If
        '    End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try

    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand

        '    Try
        '        Dim index As String = e.CommandArgument
        '        Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)

        '        If (selectedRow.RowType = DataControlRowType.DataRow) Then
        '            Dim ddlChoice1, ddlChoice2, ddlChoice3 As New DropDownList
        '            ddlChoice1 = selectedRow.FindControl("ddlChoice1")
        '            ddlChoice2 = selectedRow.FindControl("ddlChoice2")
        '            ddlChoice3 = selectedRow.FindControl("ddlChoice3")
        '            If Not ddlChoice1 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice1)
        '                dllChoice1_SelectedIndexChanged(ddlChoice1, e)
        '            End If
        '            If Not ddlChoice2 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice2)
        '                dllChoice2_SelectedIndexChanged(ddlChoice2, e)
        '            End If
        '            If Not ddlChoice3 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice3)
        '                dllChoice3_SelectedIndexChanged(ddlChoice3, e)
        '            End If
        '        End If
        '    Catch ex As Exception
        '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '        lblError.Text = "Request could not be processed"
        '    End Try

        'End If

    End Sub

    Protected Sub gvStud_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStud.RowEditing
        Dim index As Integer = e.NewEditIndex
        Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
        Dim lblStuID, lblStuNo, lblStuName, lblCGPA, lblScience, lblMaths As New Label
        lblStuID = selectedRow.FindControl("lblStuID")
        hfSTU_ID.Value = lblStuID.Text
        lblStuNo = selectedRow.FindControl("lblStuNo")
        lblStuName = selectedRow.FindControl("lblStuName")
        lblCGPA = selectedRow.FindControl("lblCGPA")
        lblScience = selectedRow.FindControl("lblScience")
        lblMaths = selectedRow.FindControl("lblMaths")
        lblstudNo.Text = lblStuNo.Text
        lblStudName.Text = lblStuName.Text

        BindStream(ddlStream1)
        BindStream(ddlStream2)
        BindStream(ddlStream3)

        getData()

        BindOptions(ddlStream1.SelectedValue.ToString, gvOptions1)
        BindOptions(ddlStream2.SelectedValue.ToString, gvOptions2)
        BindOptions(ddlStream3.SelectedValue.ToString, gvOptions3)


    End Sub


    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        MPSetOption.Show()
    End Sub
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MPSetOption.Hide()
        MPSetOption.Dispose()
        MPSetOption = Nothing
    End Sub

    Protected Sub btnStudSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudSave.Click
        If checkUniqueOption(gvOptions1, "choice1") = False Then
            lblerror1.Text = "Subjects should be unique in  choice 1"
            MPSetOption.Show()
            Exit Sub
        ElseIf checkUniqueOption(gvOptions2, "choice2") = False Then
            lblerror1.Text = "Subjects should be unique in  choice 2"
            MPSetOption.Show()
            Exit Sub
        ElseIf checkUniqueOption(gvOptions3, "choice3") = False Then
            lblerror1.Text = "Subjects should be unique in  choice 3"
            MPSetOption.Show()
            Exit Sub
        End If
        SaveData()
        SaveOptions(gvOptions1, "CHOICE1")
        SaveOptions(gvOptions2, "CHOICE2")
        SaveOptions(gvOptions3, "CHOICE3")
        GridBind()
    End Sub







End Class
