﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGradeNineMarkEntryPartB.aspx.vb" Inherits="Curriculum_clmGradeNineMarkEntryPartB"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Grade 9 Final Mark Entry
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <%--<tr >
            <td width="50%" align="left" class="title" style="height: 50px">
                <asp:Label ID="lblTitle" runat="server">GRADE 9 FINAL MARK ENTRY</asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                     <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" width="20%">
                              <span class="field-label">  Select Section </span>
                            </td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%">
                               <span class="field-label"> Select Subject </span>
                            </td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" >
                            </td>
                        </tr>
                         <tr>
                          <td colspan="4" align="center" >
                                <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                            </td></tr>
                        <tr>
                            <td align="center" colspan="4" >
                                <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    PageSize="20" Width="100%">
                                    <RowStyle />
                                    <Columns>
                                        <asp:TemplateField HeaderText="HideID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        
                                        <asp:TemplateField HeaderText="Student No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStuNo"  runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle ></ItemStyle>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="OVERALL-GRADE" >
                                        <ItemStyle  HorizontalAlign="Center" />
                                            <ItemTemplate>
                                            
                                                 <asp:DropDownList ID="ddlTotalGrade" runat="server">
                                                <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                <asp:ListItem Text="A+" Value="A+"></asp:ListItem>
                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                 <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                  <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                    </asp:DropDownList> 
                                            </ItemTemplate>
                                                </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle />
                                    <HeaderStyle />
                                    <AlternatingRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                          <td colspan="4" align="center" >
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"/>
                             
                            </td>
                        </tr>
                    </table>
               
            </td>
        </tr>
    </table>
    
       <asp:HiddenField ID="hfRSM_ID" runat="server" />
                                <asp:HiddenField ID="hfRPF_ID" runat="server" />
                                <asp:HiddenField ID="hfRSD_TOTAL" runat="server" />
                                <asp:HiddenField ID="hfACD_ID" runat="server" />

                  </div>
        </div>
    </div>
</asp:Content>
