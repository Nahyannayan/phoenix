﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_showDocument
    Inherits BasePage

   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Session("liUserList") = Nothing
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
                Session("liUserList") = Nothing
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        ViewState("multiSel") = IIf(Request.QueryString("multiSelect") = "true", True, False)
        Dim strMultiSel As String = Request.QueryString("multiSelect")
        
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"


            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            gridbind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub
    Protected Sub btnuid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_DOCSConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim strFilter As String = ""

            Dim strSearch As String = ""

            Dim txtSearch As New TextBox
            Dim optSearch As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String = ""
            Dim strFilter1 As String = ""
            Dim str_search As String = ""



            str_Sql = "select SD_ID ID,SD_DESCR DESCR,SD_FILENAME from SYLLABUS_DOC " _
                      & " WHERE SD_bSHOW=1 and SD_GRD_ID='" + Request.QueryString("GRD_IDs") + "' AND SD_BSU_ID='" + Session("sBsuid") + "' AND SD_SBM_DESCR='" + Request.QueryString("SBM_IDs") + "'"

            If gvGroup.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtOption")

                If txtSearch.Text <> "" Then
                    strFilter = set_search_filter("SD_DESCR", str_search, txtSearch.Text)
                    optSearch = txtSearch.Text
                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If
                txtSearch1 = gvGroup.HeaderRow.FindControl("txtOption1")
                If txtSearch1.Text <> "" Then
                    strFilter1 = set_search_filter("SD_FILENAME", str_search, txtSearch1.Text)
                    optSearch1 = txtSearch1.Text
                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If




            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvGroup.DataSource = ds.Tables(0)
                gvGroup.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataSource = ds.Tables(0)
                Try
                    gvGroup.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvGroup.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch

            txtSearch1 = New TextBox
            txtSearch1 = gvGroup.HeaderRow.FindControl("txtOption1")
            txtSearch1.Text = optSearch1

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("sd_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("sd_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("sd_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

  

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim img As ImageButton = e.Row.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".xlsx" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".docx" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.png"
                Else
                    img.ImageUrl = "~/Curriculum/images/img.png"
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYL.SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub


    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYL.SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        If chkSelAll.Checked Then
          
            
     
          
            Dim str_query_header As String = "select SD_ID ID,SD_DESCR DESCR,SD_FILENAME from SYL.SYLLABUS_DOC " _
                      & " WHERE SD_GRD_ID='" + Request.QueryString("GRD_IDs") + "' AND SD_BSU_ID='" + Session("sBsuid") + "' AND SD_SBM_DESCR='" + Request.QueryString("SBM_IDs") + "'"

          

            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_query_header)
            If drReader.HasRows = True Then
                While (drReader.Read())
                    Session("liUserList").Remove(drReader(0))
                    Session("liUserList").Add(drReader(0))
                End While
            End If
        Else
            SetChk(Me.Page)
            h_SelectedId.Value = ""
        End If
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "___"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Employee =document.getElementById('h_SelectedId').value; ")
        Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

    End Sub
End Class
