﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Public Class HifzTracker_HifzTracker
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Public Overrides Sub Validate(ByVal validationGroup As String)
        If validationGroup.Contains(",") Then
            Dim validationGroups As String() = validationGroup.Split(",".ToCharArray())

            For Each group As String In validationGroups
                Page.Validate(group)
            Next
        End If

        MyBase.Validate(validationGroup)
    End Sub

    Protected Sub gv_HifzTracker_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        Dim isAccess As String = isHifzAccess()
        If (isAccess = "1") Then
            BindHifzTrackerGrid()
        Else
            lblError.Text = "Sorry! You have no access to this page."
            imgAddNew.Enabled = False
            gv_HifzTracker.Visible = False
        End If

    End Sub

    Private Sub BindHifzTrackerGrid()
        Try
            gv_HifzTracker.DataSource = GetHifzTrackerData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetHifzTrackerData() As DataSet
        Dim schoolId = Convert.ToString(HttpContext.Current.Session("sBsuid"))
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@BSU_ID", schoolId)
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetHifzTrackerData", parameters)
    End Function

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Try
            Dim btnDelete As ImageButton = (TryCast(sender, ImageButton))
            Dim trackerId = btnDelete.CommandArgument
            Dim result = DeleteHifzTrackerData(trackerId)

            If result Then
                BindHifzTrackerGrid()
                gv_HifzTracker.Databind()
            Else
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function DeleteHifzTrackerData(ByVal id As String) As Boolean
        Dim parameters As SqlParameter() = New SqlParameter(9) {}
        parameters(1) = New SqlParameter("@HifzTrackerId", id)
        parameters(2) = New SqlParameter("@TransMode", "D"c)
        parameters(3) = New SqlParameter("@output", SqlDbType.Int)
        parameters(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.UpdateHifzTrackerData", parameters)
        Return CInt(parameters(3).Value) > 0
    End Function

    Private Function isHifzAccess() As String
        Dim username = Convert.ToString(HttpContext.Current.Session("sUsr_name"))
        Dim parameters As SqlParameter() = New SqlParameter(2) {}
        parameters(0) = New SqlParameter("@usr_name", username)
        Dim usraccess As String = String.Empty
        Try
            usraccess = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.Hifz_Usr_Access", parameters)
            Return usraccess
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
End Class
