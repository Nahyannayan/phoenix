<%@ Page Language="VB"  AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="clmActivity_Schedule.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
   <script language="javascript" type="text/javascript">
      function GetSUBJECT()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            var STM_ID =document.getElementById('<%=h_STM_ID.ClientID %>').value;
             
            var AcdId
             AcdId=document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select Grade')
                return false;
            }
            result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBJECT&GRD_IDs=" + GRD_IDs+"&ACD_ID="+AcdId+"&STM_ID="+STM_ID  ,"", sFeatures)            
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_SBM_ID.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
        }
        
   </script>
<%--  <head runat="server">--%>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
<%--</head>--%>
<%--<form runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:updatepanel id="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Allocate Students
        </div>
        <div class="card-body">
            <div class="table-responsive">                         
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="5"
        cellspacing="0" width="100%">
        <tr>
            <td align="left"  colspan="9">
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        
        <tr>
            <td   valign="bottom">
                <table align="center" cellpadding="4" cellspacing="0" Width="100%">
                    <tr class="title-bg">
                        <td valign="middle" class="subheader_img" colspan="8" >
            <asp:Panel ID="pnlQuickHeader" runat="server" CssClass="" Width="100%"  >
                        <div style="width:100%; height:100%; cursor: pointer;">
                        <asp:ImageButton ID="imgQuick" style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)"/>
                            <asp:label id="ltLabel" runat="server" Text="Assessment Schedule"></asp:label> 
                            </div> 
                            </asp:Panel>
                            </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="padding:0px">
            <asp:Panel ID="pnlQuickDetail" runat="server" Css Width="100%"> 
                <table>
                    <tr runat ="server" id ="trResetDuration">
                        <td><span class="field-label">Date</span>
                        </td>
                        
                        <td>
                            <asp:Label runat="server" id="lblDate"></asp:Label>
                        </td>
                        <td><span class="field-label">Time</span>
                        </td>
                        
                        <td>
                            <asp:Label runat="server" id="lblTime"></asp:Label>
                        </td>
                        <td><span class="field-label">Duration</span>
                        </td>
                        
                        <td>
                            <asp:Label runat="server" id="lblDuration"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id ="trResetPeriod">
                        <td>
                            <span class="field-label">From Date</span></td>
                        
                        <td>
                            <asp:Label ID="lblFromDate" runat="server"></asp:Label></td>
                        <td>
                            <span class="field-label">To Date</span></td>
                        
                        <td colspan="2">
                            <asp:Label ID="lblToDate" runat="server"></asp:Label></td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Grade Slab</span>
                        </td>
                        
                        <td colspan ="7">
                            <asp:Label id="lblGradeSlab" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Min Pass Mark</span>
                        </td>
                        
                        <td>
                            <asp:Label id="lblMinMark" runat="server"></asp:Label>
                        </td>
                         <td>
                            <span class="field-label">Max Mark</span>
                        </td>
                        
                        <td colspan ="4">
                            <asp:Label id="lblMaxMark" runat="server"></asp:Label>
                        </td>
                   </tr>
                    <tr>
                        <td>
                            <span class="field-label">Description</span></td>
                       
                        <td colspan="7">
                            <asp:Label id="lblDescription" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Remarks</span></td>
                       
                        <td colspan="7">
                            <asp:Label id="lblRemarks" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <asp:CheckBox id="chkviewbmandatory" text = "Attendance Mandatory" runat="server" Enabled="False">
                            </asp:CheckBox></td>
                    </tr>
                </table>
            </asp:Panel>
        <ajaxToolkit:CollapsiblePanelExtender ID="cpeQuick" runat="server" CollapseControlID="pnlQuickHeader"
                        Collapsed="true" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Original Test Details...)"
                        ExpandControlID="pnlQuickHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
                        ImageControlID="imgQuick" TargetControlID="pnlQuickDetail">
        </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left">
                            <span class="field-label">Term</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList id="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Assessment</span></td>
                        
                        <td align="left" colspan="6">
                            <asp:DropDownList id="ddlACTIVITY" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACTIVITY_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id ="20" visible="false" >
                        <td align="left" valign="top">
                            <span class="field-label">Grade</span></td>
                        
                        <td align="left" colspan="6" valign="top">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id ="21" >
                        <td align="left">
                            <span class="field-label">Subject</span></td>
                       
                        <td align="left" colspan="6">
                            <asp:TextBox id="txtSubject" runat="server" ></asp:TextBox>
                            <asp:ImageButton id="imgSubject" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSUBJECT();" OnClick="imgSubject_Click">
                            </asp:ImageButton></td>
                    </tr>
                    <tr id ="trCoreExt" runat ="server" visible ="false" >
                        <td align="left" colspan="8">
                            <asp:RadioButton id="radCore" runat="server" Checked="True" GroupName="LEVEL" Text="Core">
                            </asp:RadioButton>&nbsp;<asp:RadioButton id="radExtended" runat="server" GroupName="LEVEL"
                                Text="Extended"></asp:RadioButton>
                            <asp:RadioButton id="radNormal" runat="server" GroupName="LEVEL"
                                Text="Normal" Visible="False">
                            </asp:RadioButton></td>
                    </tr>
                    <tr >
                        <td align="left" valign="top">
                            <span class="field-label">Groups</span></td>
                       
                        <td align="left" colspan="6">
                            <asp:ListBox id="lstSubjectGroups" runat="server"  SelectionMode="Multiple"></asp:ListBox></td>
                    </tr>
                    <tr runat ="server" id="trPeriod">
                        <td align="left">
                            <span class="field-label">From Date</span></td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" ></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /></td>
                        <td align="left" colspan="3">
                            <span class="field-label">To Date</span></td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox" ></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /></td>
                    </tr>
                    <tr runat="server" id ="trDate">
                        <td align="left">
                            <span class="field-label">Date</span></td>
                        
                        <td align="left" colspan="6">
                            <asp:TextBox id="txtDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton id="imgDate" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton></td>
                            </tr>
                        <tr runat ="server" id="trTime">
                        <td align="left">
                            <span class="field-label">Time</span></td>
                       
                        <td align="left">
                            <asp:DropDownList id="ddlTimeH" runat="server" >
                            </asp:DropDownList>&nbsp;<asp:DropDownList id="ddlTimeM" runat="server" ></asp:DropDownList>&nbsp;<asp:DropDownList
                                id="ddlTimeAMPM" runat="server"><asp:ListItem Selected="True">AM</asp:ListItem>
                                <asp:ListItem>PM</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" colspan="3">
                           <span class="field-label"> Duration</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtDuration" runat="server" ></asp:TextBox>
                            <span class="field-label">Hrs</span></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Grade Slab</span></td>
                        
                        <td align="left" colspan="6">
                            <asp:DropDownList id="ddlGradeSlab" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label"> Min. Pass Mark</span></td>
                       
                        <td align="left">
                            <asp:TextBox id="txtMinMark" runat="server" ></asp:TextBox></td>
                        <td align="left">
                           <span class="field-label"> Max Mark</span></td>
                        
                        <td align="left" colspan="3">
                            &nbsp;<asp:TextBox id="txtMaxMark" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr runat = "server" id ="trAOLMarks">
                       <td align="left" colspan="8" >
                         <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 100%" >
                         <tr class="gridheader_pop">
                        <td align="center"  colspan="4" >
                            <span class="field-label">Maximum Mark AOL Exams</span></td>
                    </tr>                                       
                         <tr align="center">
                        <td  >
                           <span class="field-label"> KU</span></td>
                        <td  >
                            <span class="field-label">APP</span></td>
                        <td  >
                           <span class="field-label"> COMM</span></td>
                        <td  >
                              <span class="field-label"> HOTS</span></td>          
                    </tr>                                       
                         <tr align="center">
                        <td  >
                            <asp:TextBox ID="txtAOLKU"  runat="server"></asp:TextBox></td>
                        <td  >
                            <asp:TextBox ID="txtAOLAPP" runat="server"></asp:TextBox></td>
                        <td  >
                            <asp:TextBox ID="txtAOLCOMM"  runat="server"></asp:TextBox></td>
                        <td  >
                               <asp:TextBox ID="txtAOLHOTS"  runat="server"></asp:TextBox></td>          
                    </tr>                                       
                      </table>
                       </td>
                    </tr>
                   <tr id="trWS" runat="server" >
                        <td align="left">
                           <span class="field-label"> Max Marks Without Skills</span> </td>
                        
                        <td align="left" colspan="6">
                            <asp:TextBox id="txtWS" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr >
                        <td align="left">
                            <span class="field-label">Description</span> </td>
                        
                        <td align="left" colspan="6">
                            <asp:TextBox id="txtDescription" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                           <span class="field-label"> Remarks</span></td>
                        
                        <td align="left" colspan="6">
                            <asp:TextBox id="txtremarks" runat="server" TextMode="MultiLine"  SkinID="MultiText" ></asp:TextBox></td>
                    </tr>
                    <tr >
                        <td align="left" colspan="8">
                            <asp:CheckBox id="chkbAttendanceMandatory" runat="server" Text="Attendance Compulsory">
                            </asp:CheckBox><br />
                            <asp:CheckBox id="chkAutoAllocate" runat="server" Text="Auto Allocate Student">
                            </asp:CheckBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td  valign="bottom">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" />&nbsp;<asp:Button id="btnEdit" runat="server"
                        CausesValidation="False" CssClass="button" onclick="btnEdit_Click" Text="Edit" />
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" /><asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
                 </div>
            </div>
            </div>
    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField  id="h_SBM_ID" runat="server"/>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_PARENT_ID" runat="server"/>
    <asp:HiddenField id="h_CAS_ID" runat="server"/>
    <asp:HiddenField ID="h_" runat="server" />
    <asp:HiddenField id="h_STM_ID" runat="server"/>
                       <%--   </ContentTemplate>
                    </asp:updatepanel>
</form>--%>
</asp:Content>