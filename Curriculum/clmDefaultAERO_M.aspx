<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmDefaultAERO_M.aspx.vb" Inherits="Curriculum_clmDefaultAERO_M" Title="Untitled Page" %>



<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }
        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }
        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }



        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }





        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
        }



        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
            background-position: 0 !important;
        }



        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }



        html body .RadInput_Default .riEmpty, html body .RadInput_Empty_Default, html body .RadInput_Default .riTextBox, html body .RadInputMgr_Default {
            padding: 10px;
        }



        .RadColorPicker {
            width: 80% !important;
        }



            .RadColorPicker .rcpMillionColorsPageView .rcpInputsWrapper li {
                margin-right: 20px !important;
            }

        /*.RadColorPicker ul, .RadColorPicker ul li{

          display:inline-flex !important;

      }*/
    </style>

    <div class="card mb-3">

        <div class="card-header letter-space">

            <i class="fa fa-book mr-3"></i>Pupil Tracker Settings

        </div>

        <div class="card-body">

            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" TabIndex="7">

                    <asp:TabPanel ID="TabPanel0" runat="server">

                        <HeaderTemplate>Assessment Term</HeaderTemplate>

                        <ContentTemplate>

                            <table width="100%">

                                <tr>

                                    <td width="20%"><span class="field-label">Term</span></td>

                                    <td width="30%">
                                        <asp:DropDownList ID="ddlTermMaster" runat="server"></asp:DropDownList>
                                    </td>

                                    <td width="20%"><span class="field-label">Assessment Term</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txtSubTerm" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr>

                                    <td width="20%"><span class="field-label">Display Order</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txt_term_order" runat="server"></asp:TextBox></td>

                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnSubTermAdd" CssClass="button" runat="server" Text="Add" OnClick="btnSubTermAdd_Click" />
                                    </td>



                                </tr>

                                <tr>

                                    <td colspan="2">

                                        <asp:GridView ID="gv_SubTerm" runat="server" CssClass="table table-bordered table-row" DataKeyNames="TSM_ID" OnRowDeleting="gv_SubTerm_RowDeleting"   AutoGenerateDeleteButton="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Expectations has been set for the selected subject.">

                                            <Columns>

                                                 <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                          <asp:LinkButton ID="lnkBtnEdit"  runat="server" align="center"  text="Edit" OnClick="lnkBtnEdit_Click"></asp:LinkButton>  <%--onclick="lnkBtnView_Click"--%>                                                                                                            
                                                   </ItemTemplate>                                                     
                                                </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lock">
                                                    <ItemTemplate>
                                          <asp:LinkButton ID="lnkBtnLock"  runat="server" align="center"  text='<%# IIf(Not Eval("TSM_BBLOCKED"), "Lock", "Unlock")%>' 
                                              OnClick="lnkBtnLock_Click" ></asp:LinkButton>  <%--onclick="lnkBtnView_Click"--%>                                                                                                            
                                                   </ItemTemplate>                                                     
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Term">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblTermMaster" runat="server" Text='<%# Bind("TRM_DESCRIPTION")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Assessment Term">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblSubTerm" runat="server" Text='<%# Bind("TSM_DESCRIPTION")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Display Order">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblTermOrder" runat="server" Text='<%# Bind("TSM_DISPLAY_ORDER")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                            </Columns>

                                        </asp:GridView>

                                    </td>

                                    <td></td>

                          <td></td>

                                </tr>
                                <%--<tr><asp:TextBox ID="txt" runat="server"></asp:TextBox></tr>--%>
                            </table>

                        </ContentTemplate>

                    </asp:TabPanel>

                    <asp:TabPanel ID="TabPanel1" runat="server">

                        <HeaderTemplate>Assessment Descriptors</HeaderTemplate>

                        <ContentTemplate>

                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">



                                <tr>

                                    <td colspan="4" align="center">

                                        <table id="tbReport" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">



                                            <tr>

                                                <td align="left" width="20%"><span class="field-label">Descriptor Type</span></td>



                                                <td align="left">

                                                    <asp:DropDownList ID="ddlDescriptorType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDescriptorType_SelectedIndexChanged">

                                                        <asp:ListItem Text="Attainment Descriptor" Value="AD"></asp:ListItem>

                                                        <asp:ListItem Text="Progress Descriptor" Value="PD"></asp:ListItem>

                                                    </asp:DropDownList>

                                                    <%--<asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>--%>

 

                                                </td>



                                                <td align="left"><span class="field-label">Descriptor Name</span></td>



                                                <td align="left">

                                                    <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>

                                                </td>





                                            </tr>

                                            <tr>

                                                <td align="left"><span class="field-label">Grade</span></td>

                                                <td>

                                                    <telerik:RadComboBox RenderMode="Lightweight" ID="rcb_Grade" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                                        Width="100%" EmptyMessage="Select Grade(s)..." OnSelectedIndexChanged="rcb_Grade_SelectedIndexChanged">
                                                    </telerik:RadComboBox>

                                                </td>

                                                <td align="left" colspan="2">



                                                    <asp:Button ID="btnAddNew" runat="server" CssClass="button" TabIndex="7" Text="Add"
                                                        ValidationGroup="groupM1" /></td>

                                            </tr>



                                            <tr>

                                                <td align="center" colspan="4">



                                                    <asp:GridView ID="gvDefault" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">

                                                        <RowStyle CssClass="griditem" />

                                                        <EmptyDataRowStyle />

                                                        <Columns>



                                                            <asp:TemplateField HeaderText="Code" HeaderStyle-Width="30%">

                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("DAD_CODE")%>'> </asp:TextBox>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="30%">

                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtDefault" runat="server" Text='<%# Bind("DAD_DESCR")%>'> </asp:TextBox>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Display Order" HeaderStyle-Width="10%">

                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtOrder" runat="server" Text='<%# Bind("DAD_ORDER") %>'></asp:TextBox>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Choose Color">

                                                                <ItemTemplate>

                                                                    <telerik:RadColorPicker RenderMode="Lightweight" runat="server" ShowIcon="true" SelectedColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("DAD_COLOR_CODE"))%>' ID="RadColorPicker1" PaletteModes="HSV" Columns="5">
                                                                    </telerik:RadColorPicker>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Value" HeaderStyle-Width="10%">

                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtValue" runat="server" Text='<%# Bind("DAD_VALUE")%>'></asp:TextBox>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>





                                                            <asp:ButtonField CommandName="Delete" Text="Delete" HeaderText="Delete" HeaderStyle-Width="5%">

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                                            </asp:ButtonField>

                                                                

                                                        </Columns>



                                                        <SelectedRowStyle />

                                                        <HeaderStyle />

                                                        <EditRowStyle />

                                                        <AlternatingRowStyle CssClass="griditem_alternative" />

                                                    </asp:GridView>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="center" colspan="4">

                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="7" Text="Save"
                                                        ValidationGroup="groupM1" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                            CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False" /></td>

                                            </tr>

                                        </table>

                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />



                                    </td>

                                </tr>

                            </table>

                        </ContentTemplate>

                    </asp:TabPanel>

                    <asp:TabPanel ID="TabPanel2" runat="server">

                        <HeaderTemplate>Expectations</HeaderTemplate>

                        <ContentTemplate>

                            <table width="100%">

                                <tr>

                                    <td width="20%"><span class="field-label">Subject</span></td>

                                    <td width="30%">

                                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddlSubject" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                            Width="100%" EmptyMessage="Select Subject(s)..." OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </telerik:RadComboBox>

                                        <%--      <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged"></asp:DropDownList>--%>

 

                                    </td>

                                    <td width="20%"></td>

                                    <td width="30%"></td>

                                </tr>

                                <tr class="title-bg">

                                    <td colspan="4">End Of Year Expectations

                                    </td>

                                </tr>

                                <tr>

                                    <td width="20%"><span class="field-label">Descriptors</span></td>

                                    <td width="30%">

                                        <asp:TextBox ID="txtDescriptors" runat="server"></asp:TextBox></td>

                                    <td width="20%"><span class="field-label">Color Code</span></td>

                                    <td width="30%">

                                        <telerik:RadColorPicker RenderMode="Lightweight" runat="server" ShowIcon="true" ID="Expectation_Color_picker" PaletteModes="HSV" Columns="5"></telerik:RadColorPicker>

                                    </td>

                                </tr>

                                <tr>

                                    <td><span class="field-label">From Range</span></td>

                                    <td>

                                        <asp:TextBox ID="txtFromMarks" runat="server"></asp:TextBox></td>

                                    <td><span class="field-label">To Range</span></td>

                                    <td>

                                        <asp:TextBox ID="txtToMarks" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr align="center">

                                    <td colspan="4">

                                        <asp:Button ID="btnMarkAdd" runat="server" Text="Add Descriptor" CssClass="button" OnClick="btnMarkAdd_Click" />

                                    </td>

                                </tr>

                                <tr>

                                    <td colspan="2">

                                        <asp:GridView ID="gv_EOY" runat="server" CssClass="table table-bordered table-row" DataKeyNames="AEE_ID" OnRowDeleting="gv_EOY_RowDeleting" AutoGenerateColumns="false"
                                            AutoGenerateDeleteButton="true" EmptyDataText="No Expectations has been set for the selected subject.">

                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                      
                                                               
<asp:LinkButton ID="lnkBtnEditgv_EOY"  runat="server" align="center"  text="Edit" OnClick="lnkBtnEditgv_EOY_Click"></asp:LinkButton>  <%--onclick="lnkBtnView_Click"--%>
                                                                                                                                                                     
                                                   </ItemTemplate>                                                     
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_Subject" runat="server" Text='<%# Bind("SBM_DESCR")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Descriptor">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_descriptor" runat="server" Text='<%# Bind("AEE_DESCR")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Color">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblColor" runat="server" Text="__________" BackColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("AEE_COLOR_CODE"))%>'
                                                            ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("AEE_COLOR_CODE"))%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="From Range">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_mark_from" runat="server" Text='<%# Bind("AEE_MARK_FROM")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="To Range">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_mark_to" runat="server" Text='<%# Bind("AEE_MARK_TO")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>

                                    </td>

                                    <td colspan="2"></td>

                                </tr>

                                <tr class="title-bg">

                                    <td colspan="4">Termly Expectations

                                    </td>

                                </tr>

                                <tr>

                                    <td><span class="field-label">Term</span></td>

                                    <td>

                                        <asp:DropDownList ID="ddlTerm" runat="server"></asp:DropDownList></td>

                                    <td><span class="field-label">Expectation</span></td>

                                    <td>

                                        <asp:TextBox ID="txtTermExpectation" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr align="center">

                                    <td colspan="4">

                                        <asp:Button ID="btnTermkAdd" runat="server" Text="Add Expectation" CssClass="button" OnClick="btnTermkAdd_Click" />

                                    </td>

                                </tr>

                                <tr>

                                    <td colspan="2">

                                        <asp:GridView ID="gv_Term" runat="server" CssClass="table table-bordered table-row" DataKeyNames="AET_ID" OnRowDeleting="gv_Term_RowDeleting" AutoGenerateDeleteButton="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Expectations has been set for the selected subject.">

                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                      
                                                               
<asp:LinkButton ID="lnkBtnEditgv_Term"  runat="server" align="center"  text="Edit" OnClick="lnkBtnEditgv_Term_Click"></asp:LinkButton>  <%--onclick="lnkBtnView_Click"--%>
                                                                                                                                                                     
                                                   </ItemTemplate>                                                     
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Term">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblTerm" runat="server" Text='<%# Bind("TSM_DESCRIPTION")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Expectation(%)">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lblExpectation" runat="server" Text='<%# Bind("AET_EXPECTATION_PERC")%>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>

                                    </td>

                                    <td colspan="2"></td>

                                </tr>

                            </table>

                        </ContentTemplate>

                    </asp:TabPanel>

                </asp:TabContainer>

                <asp:HiddenField ID="hfRDM_ID" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hf_acd_id" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfAllSubjects" runat="server"></asp:HiddenField>

            </div>

        </div>

    </div>

</asp:Content>
