﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmObjTrackLevel_M.aspx.vb" Inherits="Curriculum_clmObjTrackLevel_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Levels
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%"  >
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%" >
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label" >Academic Year</span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                    <td class="matters" align="left" width="20%"><span class="field-label" >Subject</span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true"  >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="groupM1" TabIndex="7" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align:center">
                                        <asp:GridView ID="gvLevel" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20"   CssClass="table table-bordered table-row">
                                            <RowStyle  Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="NC Level" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("NCL_LVL_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="NC Level" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("NCL_DISPLAYORDER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="NC Level" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValue" runat="server" Text='<%# Bind("NCL_VALUE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="NC Level">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNCLevel" runat="server" Text='<%# Bind("NCM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Level">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlLevel" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Weightage">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWeightage" Text='<%# Bind("NCL_WEIGHTAGE") %>'   runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="matters" colspan="4"  ><span class="field-label">Copy default values to subjects </span></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" style="text-align:center" >

                                        <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False"  Width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                              PageSize="20" >
                                            <RowStyle CssClass="griditem"   Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                Select<br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("SBG_SBM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="groupM1" TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
