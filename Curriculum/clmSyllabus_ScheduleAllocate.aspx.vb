

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_clmSyllabus_ScheduleAllocate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            'Session("SyllabusSchedule") = SyllabusMaster.CreateDT_Syllabus_Schedule()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") = "C100025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_SyllabusId.Value = Encr_decrData.Decrypt(Request.QueryString("SyllabusId").Replace(" ", "+")) '"SYLLABUSID"
                    'h_SYDID.Value = Encr_decrData.Decrypt(Request.QueryString("SYDId").Replace(" ", "+")) '"SYDID"
                    h_ParentId.Value = Encr_decrData.Decrypt(Request.QueryString("ParentId").Replace(" ", "+")) '"ParentId"
                    'ClearPartialDetails()
                    h_SYTID.Value = Request.QueryString("SytId")
                    PopulateParentSyllabus()
                    'ddlgroupbind()
                    GetGroup()
                    GetSyllabusSchedule()
                    GetActualDetails()

                    If ViewState("datamode") = "add" Then

                        Session("gintGridLine") = 0
                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    End If
                    'Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then
            'SaveSyllabus()
            SaveSchedule()
        End If
    End Sub
    

#End Region

   

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            'ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                'Response.Redirect(ViewState("ReferrerUrl"))
                ' ViewState("datamode") = "none"
                'Response.Redirect(ViewState("datamode"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub ClearControls()
        txtSubject.Text = ""
        txtSubject.Text = ""
        'txtSubTopic.Text = ""
        'txtToDate.Text = ""
        'txtFromDate.Text = ""
        'txtParentTopic.Text = ""
        ' txtHrs.Text = ""

        gvSyllabus.DataSource = Nothing
        gvSyllabus.DataBind()
        Session("gintGridLine") = 0
        Session("SyllabusSchedule") = Nothing
        'h_SYD_ID.Value = 0

    End Sub
    Sub FillAcd()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Sub ClearPartialDetails()
        ClearControls()
    End Sub
    
    Sub PopulateParentSyllabus()
        Dim ds As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT     SYL.SYLLABUS_M.SYM_ACD_ID as AcdId, VW_ACADEMICYEAR_M.ACY_DESCR as AcdYear, SYL.SYLLABUS_M.SYM_TRM_ID as TrmId, VW_TRM_M.TRM_DESCRIPTION as Term, " _
                            & " SYL.SYLLABUS_M.SYM_GRD_ID as GrdId, VW_GRADE_M.GRD_DISPLAY as Grade, SYL.SYLLABUS_M.SYM_SBG_ID as SubjId, SUBJECTS_GRADE_S.SBG_DESCR as Subject, " _
                            & " SYL.SYLLABUS_M.SYM_DESCR as Syllabus,SYL.SYLLABUS_M.SYM_ID as SylId " _
                            & " FROM SYL.SYLLABUS_M INNER JOIN " _
                            & " VW_BUSINESSUNIT_M ON SYL.SYLLABUS_M.SYM_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                            & " vw_ACADEMICYEAR_D ON SYL.SYLLABUS_M.SYM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " _
                            & " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID INNER JOIN " _
                            & " VW_TRM_M ON SYL.SYLLABUS_M.SYM_TRM_ID = VW_TRM_M.TRM_ID INNER JOIN " _
                            & " VW_GRADE_M ON SYL.SYLLABUS_M.SYM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " _
                            & " SUBJECTS_GRADE_S ON SYL.SYLLABUS_M.SYM_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " _
                            & " WHERE SYL.SYLLABUS_M.SYM_ID='" & h_SyllabusId.Value & "' AND SYL.SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "'"
        '& " WHERE (SYL.SYLLABUS_M.SYM_ACD_ID = '" & h_ACCID.Value & "') AND (SYL.SYLLABUS_M.SYM_TRM_ID = '" & h_TERMID.Value & "') AND (SYL.SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "') AND " _
        '& " SYL.SYLLABUS_M.SYM_GRD_ID='" & h_GRDID.Value & "' AND SYL.SYLLABUS_M.SYM_ID='" & h_SyllabusId.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count >= 1 Then
            txtAccYear.Text = ds.Tables(0).Rows(0)("AcdYear")
            txtTerm.Text = ds.Tables(0).Rows(0)("Term")
            txtGrade.Text = ds.Tables(0).Rows(0)("Grade")
            txtSubject.Text = ds.Tables(0).Rows(0)("Subject")
            txtSyllabus.Text = ds.Tables(0).Rows(0)("Syllabus")
            h_ACCID.Value = ds.Tables(0).Rows(0)("AcdId")
            h_TERMID.Value = ds.Tables(0).Rows(0)("TrmId")
            h_GRDID.Value = ds.Tables(0).Rows(0)("GrdId")
            h_SUBJID.Value = ds.Tables(0).Rows(0)("SubjId")
            'txtsy = ds.Tables(0).Rows(i)("Syllabus")
        End If

    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearControls()
    End Sub

    Sub GetSyllabusSchedule()
        Dim strSql As String
        Dim strSub As String
        Dim ds As DataSet
        Dim i As Integer
        Dim ldrNew As DataRow
        Try
            ' Session("SyllabusSchedule") = SyllabusMaster.CreateDT_Syllabus_Schedule()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            'strSql = "SELECT SYL.SYLLABUS_M.SYM_ID, SYL.SYLLABUS_D.SYD_ID, SYL.SYLLABUS_TEACHER_M.SYT_ID as SYTId, SYL.SYLLABUS_TEACHER_D.SYL_ID as SYLId, " _
            '          & " SYL.SYLLABUS_TEACHER_D.SYL_DESCR as SYLDescr, SYL.SYLLABUS_TEACHER_D.SYL_PLN_STDT as PlnStDate, SYL.SYLLABUS_TEACHER_D.SYL_PLN_ENDDT as PlnEndDate, " _
            '          & " SYL.SYLLABUS_TEACHER_D.SYL_PLN_HRS as PlnHrs, ISNULL(SYL.SYLLABUS_TEACHER_D.SYL_ACT_STDT,'') as ActStDate, ISNULL(SYL.SYLLABUS_TEACHER_D.SYL_ACT_ENDDT,'') as ActEndDate, " _
            '          & " ISNULL(SYL.SYLLABUS_TEACHER_D.SYL_HRS,0) as ActHrs, SYL.SYLLABUS_TEACHER_D.SYL_bCOMPLETE as Complete " _
            '          & " FROM SYL.SYLLABUS_TEACHER_M INNER JOIN " _
            '          & " SYL.SYLLABUS_TEACHER_D ON SYL.SYLLABUS_TEACHER_M.SYT_ID = SYL.SYLLABUS_TEACHER_D.SYL_SYT_ID INNER JOIN " _
            '          & " SYL.SYLLABUS_D ON SYL.SYLLABUS_TEACHER_M.SYT_SYD_ID = SYL.SYLLABUS_D.SYD_ID AND " _
            '          & " SYL.SYLLABUS_TEACHER_D.SYL_SYD_ID = SYL.SYLLABUS_D.SYD_ID INNER J,OIN " _
            '          & " SYL.SYLLABUS_M ON SYL.SYLLABUS_D.SYD_SYM_ID = SYL.SYLLABUS_M.SYM_ID " _
            '          & " WHERE SYL.SYLLABUS_M.SYM_ID='" & h_SyllabusId.Value & "' and SYL.SYLLABUS_D.SYD_ID='" & h_SYDID.Value & "' and SYL.SYLLABUS_M.SYM_GRD_ID='" & h_GRDID.Value & "' order by SYL.SYLLABUS_TEACHER_M.SYT_ID"
            strSql = "SELECT     SYL.SYLLABUS_D.SYD_ID as SydId, SYL.SYLLABUS_D.SYD_DESCR as SYLDescr, SYL.SYLLABUS_D.SYD_STDT as PlnStdate, SYL.SYLLABUS_D.SYD_ENDDT as PlnEndDate, " _
                    & " SYL.SYLLABUS_D.SYD_TOT_HRS as PlnHrs" _
                    & " FROM SYL.SYLLABUS_M INNER JOIN " _
                    & "SYL.SYLLABUS_D ON SYL.SYLLABUS_M.SYM_ID = SYL.SYLLABUS_D.SYD_SYM_ID AND " _
                    & " SYL.SYLLABUS_M.SYM_ID = SYL.SYLLABUS_D.SYD_SYM_ID where SYL.SYLLABUS_D.SYD_PARENT_ID='" & h_ParentId.Value & "' and SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SyllabusId.Value & "' AND " _
                    & " SYL.SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "' order by SYL.SYLLABUS_D.SYD_STDT,SYL.SYLLABUS_D.SYD_ENDDT"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                GetSubTopic(ds, str_conn)
                gvSyllabus.DataSource = ds
                gvSyllabus.DataBind()
                gvSyllabus.Columns(0).Visible = False
                'gvSyllabus.Columns(1).Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    
    Sub SaveSchedule()
        'Dim lblSYTId As Label
        'Dim lblSYLId As Label
        Dim lblSydId As New Label
        'Dim lblSylDescr As New Label

        Dim txtActStDt As TextBox
        Dim txtActEndDate As TextBox
        Dim txtActHrs As TextBox
        Dim chkCmplt As CheckBox
        Dim i, j As Integer
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim blnComplete As Boolean = False
        Dim strSQL As String
        Dim ds As DataSet
        Try
            If gvSyllabus.Rows.Count > 0 Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                

                For i = 0 To gvSyllabus.Rows.Count - 1
                    'lblSYLId = TryCast(gvSyllabus.Rows(i).FindControl("lblSylId"), Label)
                    'lblSYTId = TryCast(gvSyllabus.Rows(i).FindControl("lblSytId"), Label)
                    lblSydId = TryCast(gvSyllabus.Rows(i).FindControl("lblSydId"), Label)
                    txtActStDt = TryCast(gvSyllabus.Rows(i).FindControl("txtActStartDate"), TextBox)
                    txtActEndDate = TryCast(gvSyllabus.Rows(i).FindControl("txtActEndDate"), TextBox)
                    If txtActStDt.Text <> "" And txtActEndDate.Text <> "" Then
                        If IsDate(txtActStDt.Text) = False Then
                            stTrans.Rollback()
                            lblError.Text = "Invalid Actual StartDate in Row No:" + (i + 1).ToString()
                            Exit Sub
                        End If
                        If IsDate(txtActEndDate.Text) = False Then
                            stTrans.Rollback()
                            lblError.Text = "Invalid Actual EndDate in Row No:" + (i + 1).ToString()
                            Exit Sub
                        End If
                        If Convert.ToDateTime(txtActStDt.Text) > Convert.ToDateTime(txtActEndDate.Text) Then
                            stTrans.Rollback()
                            lblError.Text = "Actual StartDate must be less than EndDate in Row No:" + (i + 1).ToString()
                            Exit Sub
                        End If

                        txtActHrs = TryCast(gvSyllabus.Rows(i).FindControl("txtActHrs"), TextBox)
                        If Not txtActHrs Is Nothing Then
                            If Trim(txtActHrs.Text) = "" Or IsNumeric(txtActHrs.Text) = False Then
                                stTrans.Rollback()
                                lblError.Text = "Specify Actual Hours taken in Row No:" + (i + 1).ToString()
                                Exit Sub
                            End If
                        End If
                        chkCmplt = TryCast(gvSyllabus.Rows(i).FindControl("chkComplete"), CheckBox)
                        'If Not lblSYLId Is Nothing And lblSYTId.Text <> "0" Then
                        If Not lblSydId Is Nothing Then
                            Try
                               
                                cmd = New SqlCommand("[SYL].[DeleteSYLLABUS_TEACHER_D]", objConn, stTrans)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.AddWithValue("@SYL_ID", 0)
                                cmd.Parameters.AddWithValue("@SYL_SYT_ID", h_SYTID.Value)
                                cmd.Parameters.AddWithValue("@SYL_SYD_ID", Convert.ToInt64(lblSydId.Text))
                                cmd.ExecuteNonQuery()

                                cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_D]", objConn, stTrans)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.AddWithValue("@SYL_ID", 0)
                                cmd.Parameters.AddWithValue("@SYL_SYT_ID", h_SYTID.Value)
                                cmd.Parameters.AddWithValue("@SYL_SYD_ID", Convert.ToInt64(lblSydId.Text))
                                cmd.Parameters.AddWithValue("@SYL_ACT_STDT", CDate(txtActStDt.Text))
                                cmd.Parameters.AddWithValue("@SYL_ACT_ENDDT", CDate(txtActEndDate.Text))
                                cmd.Parameters.AddWithValue("@SYL_ACT_HRS", Convert.ToDecimal(txtActHrs.Text))
                                cmd.Parameters.AddWithValue("@SYL_BComplete", IIf(chkCmplt.Checked = True, 1, 0))
                                cmd.Parameters.AddWithValue("@SYL_RESOURCE", "")
                                cmd.Parameters.AddWithValue("@bEdit", 0)
                                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                                cmd.ExecuteNonQuery()
                                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                                If iReturnvalue <> 0 Then
                                    stTrans.Rollback()
                                    lblError.Text = "Request could not be processed"
                                    objConn.Close()
                                    Exit Sub
                                End If



                            Catch ex As Exception
                                stTrans.Rollback()
                                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                                lblError.Text = "Request could not be processed"
                                objConn.Close()
                                Exit Sub
                            End Try

                        End If
                    End If
                    
                Next

                stTrans.Commit()
                lblError.Text = "Schedule Successfully Saved"
                objConn.Close()
                GetSyllabusSchedule()
                GetActualDetails()

                'Updating Status to Master table
                Dim chkComplt As New CheckBox
                For j = 0 To gvSyllabus.Rows.Count - 1
                    chkComplt = TryCast(gvSyllabus.Rows(j).FindControl("chkComplete"), CheckBox)
                    If chkComplt.Checked = False Then
                        blnComplete = False
                        Exit For
                    Else
                        blnComplete = True
                    End If
                Next
                Dim objCon As New SqlConnection(str_conn)
                objCon.Open()
                Dim Trans As SqlTransaction = objCon.BeginTransaction
                'UPdate Main Topic to be Completed

                cmd = New SqlCommand("[SYL].[UpdateSYLLABUS_TEACHER_M]", objCon, Trans)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SYT_ID", h_SYTID.Value)
                cmd.Parameters.AddWithValue("@SYT_BSU_ID", Session("SBsuid"))
                If blnComplete = True Then
                    cmd.Parameters.AddWithValue("@SYT_Completed", 1)
                Else
                    cmd.Parameters.AddWithValue("@SYT_Completed", 0)
                End If
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                If iReturnvalue <> 0 Then
                    Trans.Rollback()
                    lblError.Text = "Request could not be processed"
                    objCon.Close()
                    Exit Sub
                End If
                Trans.Commit()
                objCon.Close()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub GetActualDetails()
        'Dim lblSYTId As Label
        'Dim lblSYLId As Label
        Dim lblsydID As New Label
        Dim txtActStDt As TextBox
        Dim txtActEndDate As TextBox
        Dim txtActHrs As TextBox
        Dim chkCmplt As CheckBox
        Dim img1 As New ImageButton
        Dim img2 As New ImageButton
        Dim i As Integer
        Dim strSQL As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Try
            If gvSyllabus.Rows.Count > 0 Then
                For i = 0 To gvSyllabus.Rows.Count - 1
                    'lblSYLId = TryCast(gvSyllabus.Rows(i).FindControl("lblSylId"), Label)
                    'lblSYTId = TryCast(gvSyllabus.Rows(i).FindControl("lblSytId"), Label)
                    lblsydID = TryCast(gvSyllabus.Rows(i).FindControl("lblSydId"), Label)
                    txtActStDt = TryCast(gvSyllabus.Rows(i).FindControl("txtActStartDate"), TextBox)
                    txtActEndDate = TryCast(gvSyllabus.Rows(i).FindControl("txtActEndDate"), TextBox)
                    txtActHrs = TryCast(gvSyllabus.Rows(i).FindControl("txtActHrs"), TextBox)
                    chkCmplt = TryCast(gvSyllabus.Rows(i).FindControl("chkComplete"), CheckBox)
                    img1 = TryCast(gvSyllabus.Rows(i).FindControl("imgCalendar"), ImageButton)
                    img2 = TryCast(gvSyllabus.Rows(i).FindControl("ImageButton1"), ImageButton)
                    'If Not lblSYLId Is Nothing And lblSYTId.Text <> "0" Then
                    If Not lblsydID Is Nothing Then
                        strSQL = "SELECT SYL_ACT_STDT,SYL_ACT_ENDDT,SYL_HRS,SYL_BCOMPLETE FROM SYL.SYLLABUS_TEACHER_D " _
                                & " WHERE SYL_SYD_ID='" & lblsydID.Text & "' and SYL_SYT_ID=" & h_SYTID.Value & ""
                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not IsDBNull(ds.Tables(0).Rows(0)("SYL_ACT_STDT")) Then
                                txtActStDt.Text = Format(ds.Tables(0).Rows(0)("SYL_ACT_STDT"), "dd/MMM/yyyy")
                            Else
                                txtActStDt.Text = ""
                            End If
                            If Not IsDBNull(ds.Tables(0).Rows(0)("SYL_ACT_ENDDT")) Then
                                txtActEndDate.Text = Format(ds.Tables(0).Rows(0)("SYL_ACT_ENDDT"), "dd/MMM/yyyy")
                            Else
                                txtActEndDate.Text = ""
                            End If
                            If Not IsDBNull(ds.Tables(0).Rows(0)("SYL_HRS")) Then
                                txtActHrs.Text = ds.Tables(0).Rows(0)("SYL_HRS")
                            Else
                                txtActHrs.Text = ""
                            End If
                            If ds.Tables(0).Rows(0)("SYL_BCOMPLETE") = 0 Then
                                chkCmplt.Checked = False
                                gvSyllabus.Rows(i).Enabled = True
                                txtActStDt.Enabled = True
                                txtActEndDate.Enabled = True
                                txtActHrs.Enabled = True
                                img1.Enabled = True
                                img2.Enabled = True
                            Else
                                chkCmplt.Checked = True
                                txtActStDt.Enabled = False
                                txtActEndDate.Enabled = False
                                txtActHrs.Enabled = False
                                img1.Enabled = False
                                img2.Enabled = False
                            End If
                        End If
                    End If

                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub GetSubTopic(ByVal ds As DataSet, ByVal strconn As String)
        Dim strSub As String
        Dim Sqldr As SqlDataReader
        Dim DSTemp As DataSet = ds.Clone
        DSTemp = ds.Copy()
        Try
            For Each dr As DataRow In DSTemp.Tables(0).Rows

                strSub = "SELECT     SYL.SYLLABUS_D.SYD_ID as SydId, SYL.SYLLABUS_D.SYD_DESCR as SYLDescr, SYL.SYLLABUS_D.SYD_STDT as PlnStdate, SYL.SYLLABUS_D.SYD_ENDDT as PlnEndDate, " _
                         & " SYL.SYLLABUS_D.SYD_TOT_HRS as PlnHrs" _
                         & " FROM SYL.SYLLABUS_M INNER JOIN " _
                          & "SYL.SYLLABUS_D ON SYL.SYLLABUS_M.SYM_ID = SYL.SYLLABUS_D.SYD_SYM_ID AND " _
                        & " SYL.SYLLABUS_M.SYM_ID = SYL.SYLLABUS_D.SYD_SYM_ID where SYL.SYLLABUS_D.SYD_PARENT_ID='" & dr("SydId") & "' and SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SyllabusId.Value & "'"

                Sqldr = SqlHelper.ExecuteReader(strconn, CommandType.Text, strSub)
                While Sqldr.Read()
                    Dim dr1 As DataRow = ds.Tables(0).NewRow
                    dr1.Item("SydId") = Sqldr.GetValue(0)
                    dr1.Item("SYLDescr") = Sqldr.GetValue(1)
                    dr1.Item("PlnStdate") = Sqldr.GetValue(2)
                    dr1.Item("PlnEndDate") = Sqldr.GetValue(3)
                    dr1.Item("PlnHrs") = Sqldr.GetValue(4)
                    ds.Tables(0).Rows.Add(dr1)
                End While

                'dsSub.Tables(0).Clear()

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSydId As New Label
        Dim txtActFromDate As New TextBox
        Dim strSql As String

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        lblSydId = TryCast(sender.FindControl("lblSydId"), Label)
        txtActFromDate = TryCast(sender.FindControl("txtActStartDate"), TextBox)
        Try
            If txtActFromDate.Text <> "" Then
                If Not lblSydId Is Nothing Then
                    strSql = "DELETE FROM SYL.SYLLABUS_TEACHER_D WHERE SYL_SYD_ID='" & lblSydId.Text & "' and SYL_SYT_ID='" & h_SYTID.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSql)
                End If
                lblError.Text = "Successfully Deleted the Selected Allocation"
            End If
            stTrans.Commit()
            GetSyllabusSchedule()
            GetActualDetails()


        Catch ex As Exception
            stTrans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        Finally
            objConn.Close()
        End Try
    End Sub
    Sub GetGroup()
        Dim strSQL As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        strSQL = "SELECT     GROUPS_M.SGR_DESCR, SYL.SYLLABUS_TEACHER_M.SYT_SGR_ID " _
                    & " FROM SYL.SYLLABUS_TEACHER_M INNER JOIN " _
                    & " GROUPS_M ON SYL.SYLLABUS_TEACHER_M.SYT_SGR_ID = GROUPS_M.SGR_ID WHERE SYL.SYLLABUS_TEACHER_M.SYT_ID='" & h_SYTID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            h_GRPID.Value = IIf(IsDBNull(ds.Tables(0).Rows(0)("SYT_SGR_ID")), 0, ds.Tables(0).Rows(0)("SYT_SGR_ID"))
            txtGroup.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("SGR_DESCR")), "", ds.Tables(0).Rows(0)("SGR_DESCR"))
            txtGroup.Enabled = False
        End If

    End Sub

    Protected Sub gvSyllabus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvSyllabus.PageIndex = e.NewPageIndex
            GetSyllabusSchedule()
            GetActualDetails()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub chkComplete_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtActStDate As New TextBox
        Dim txtActEndDate As New TextBox
        Dim txtActHrs As New TextBox
        Dim chkCmplt As New CheckBox
        Dim img1 As New ImageButton
        Dim img2 As New ImageButton

        txtActStDate = TryCast(sender.FindControl("txtActStartDate"), TextBox)
        txtActEndDate = TryCast(sender.FindControl("txtActEndDate"), TextBox)
        txtActHrs = TryCast(sender.FindControl("txtActHrs"), TextBox)
        img1 = TryCast(sender.FindControl("imgCalendar"), ImageButton)
        img2 = TryCast(sender.FindControl("Imagebutton1"), ImageButton)
        chkCmplt = TryCast(sender.FindControl("chkComplete"), CheckBox)

        If chkCmplt.Checked = True Then
            txtActStDate.Enabled = False
            txtActEndDate.Enabled = False
            txtActHrs.Enabled = False
            img1.Enabled = False
            img2.Enabled = False
        Else
            txtActStDate.Enabled = True
            txtActEndDate.Enabled = True
            txtActHrs.Enabled = True
            img1.Enabled = True
            img2.Enabled = True
        End If


    End Sub
   
End Class

