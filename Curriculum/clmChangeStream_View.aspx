<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeStream_View.aspx.vb" Inherits="Curriculum_clmChangeStream_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" Text="Change Stream Approval" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table id="studTable" runat="server" width="100%">
                                <tr>
                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblAccText" class="field-label" runat="server" Text="Select Academic Year"></asp:Label></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>


                                <tr id="tr2" style="text-align:center">

                                    <td colspan="4" style="vertical-align: top" align="center">

                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" AllowPaging="True" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStmId" runat="server" Text='<%# Bind("STU_STM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShfId" runat="server" Text='<%# Bind("STU_SHF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Stud No.">
                                                    <HeaderTemplate>
                                                                    <asp:Label ID="lblh1" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>
                                                                    <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                                    Grade<br />
                                                                    <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                                    Section<br />
                                                                    <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>


                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:HiddenField ID="hfGRM_ID" runat="server"></asp:HiddenField>
                            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

</asp:Content>

