﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmLessonPlannerStudentCriterias_New.aspx.vb" Inherits="Curriculum_clmLessonPlannerStudentCriterias"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>

                tr.FreezeCol{
            background-color:white;
            width:177px;
        }
        #div_gridholder select {
            padding: 4px !important;
            font-size: 13px !important;
            width: 96% !important;
            margin: 6px 0;
        }

        .hideSteps {
            overflow: hidden;
            position: relative;
            line-height: 1.2em;
            max-height: 3.6em;
            text-align: left;
            padding-right: 0.6em;
            cursor: pointer;
            color: #016b05;
            font-weight: 800;
            text-align: center;
        }

        .hr-margin {
            margin-top: 0.2rem !important;
            margin-bottom: 0.2rem !important;
        }

        .hideText {
            overflow: hidden;
            position: relative;
            line-height: 1.2em;
            max-height: 3.6em;
            text-align: left;
            padding-right: 0.6em;
            cursor: pointer;
            color: #016b05;
        }

            .hideText:before {
                content: '...';
                position: absolute;
                right: 2px;
                bottom: 0px;
            }

            .hideText:after {
                content: '';
                position: absolute;
                right: 0;
                width: 1em;
                height: 1em;
                margin-top: 0.2em;
                background: #f5f5f5;
            }

        .table {
            width: 0;
        }

        .min-Rowwidth {
            min-width: 110px !important;
            vertical-align: bottom !important;
            max-width: 110px !important;
        }

        .labeltolink {
            text-decoration: none;
            cursor: pointer;
            color: #016b05;
            font-weight: bold;
        }

        .card-body {
            padding: 0 1.25rem !important;
        }

        table th, table td {
            padding: 0px !important;
        }

            table td span.field-label, .card span.field-label {
                font-size: 1.1rem !important;
                color: rgba(114,114,114,1.00) !important;
                font-weight: 600 !important;
            }

            table td span.Topic, .card span.Topic {
                font-size: 13px !important;
                color: rgba(114,114,114,1.00) !important;
                font-weight: 600 !important;
            }

        .higlight {
            background-color: #e5e5e5;
            padding: 4px;
            border: 1px solid rgba(0, 0, 0, 0.15);
            display: block;
            margin: 6px 0;
            border-radius: 6px;
        }

        .grid_alternate {
            background-color: #f5f5f5;
        }
    </style>
    <script>
        function change_ddl_state(ddlThis, ddlObj) {

            ddlThis.setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;


                if (currentid.indexOf(ddlObj) != -1) {

                    if (ddlThis.selectedIndex != -1) {
                        document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex;
                        document.forms[0].elements[i].setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))
                    }
                }
            }
        }

        function setColor(ddlThis) {
            ddlThis.setAttribute('style', ddlThis.options[ddlThis.selectedIndex].getAttribute('style'))
        }

        function GetSubjects() {

            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var trmId;
            var sbgId;
            var trmId;
            trmId = document.getElementById('<%=h_TRM_ID.ClientID%>').value;
            sbgId = document.getElementById('<%=ddlSubject.ClientID %>').value.split("|");
            // result = window.showModalDialog("showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "", sFeatures)

            var oWnd = radopen("showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "pop_topic");

            <%--if (result != "" && result != "undefined") {

                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];

            }
            return false;--%>
        }
        function OnClientClose3(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function ViewClick() {
            document.getElementById('<%=btnView.ClientID%>').click()
        }
    </script>
    <script src='<%= ResolveUrl("~/js/GridScroll.js")%>'></script>
       <script type="text/javascript">
           function FreezeGrid() {
               var gridViewScroll = new GridViewScroll({
                   elementID: '<%=gvStudent.ClientID%>',//"ctl00_cphMasterpage_", // Target element id
                freezeHeaderRowCount: 1,
                freezeColumn: true, // Boolean
                freezeColumnCount: 2,
                freezeColumnCssClass: "FreezeCol", // String
                width: "100%", // Integer or String(Percentage)
                height: 600, // Integer or String(Percentage)// Integer
            });
            gridViewScroll.enhance();
        }
</script>

    <%--  <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js">
    </script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js">
    </script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css" rel="stylesheet" />--%>
    <script>
        function PopupBig(url) {

            $.fancybox({
                'width': '90%',
                'height': '100%',
                'autoScale': true,
                'fitToView': false,
                'autoSize': false,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
            <span style="float: right">

                <asp:LinkButton ID="lnkSearch" runat="server" Visible="true"></asp:LinkButton>
            </span>
        </div>
        <asp:Panel ID="pnlFilters" runat="server">
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table id="tbl_AddGroup" runat="server" border="0" cellpadding="0" cellspacing="0"
                        style="vertical-align: top;" width="100%">
                        <tr>
                            <td align="left" valign="bottom">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Panel ID="pnlSearch" runat="server">
                                    <table id="Table2" runat="server" cellpadding="5"
                                        cellspacing="0" style="border-collapse: collapse" width="100%">
                                        <tr>
                                            <td align="left" width="32%"><span class="field-label">Academic Year          </span>
                                                <br />
                                                <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td width="32%"><span class="field-label">Grade   </span>
                                                <br />

                                                <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>

                                            <td align="left" width="32%" id="tdhide" runat="server"><span class="field-label">Age Band  </span>
                                                <br />
                                                <asp:DropDownList ID="ddlAgeBand" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="32%"><span class="field-label">Subject  </span>
                                                <br />
                                                <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left"><span class="field-label">Group   </span>
                                                <br />
                                                <asp:DropDownList ID="ddlGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>

                                            <td align="left"><span class="field-label">Term      </span>
                                                <br />
                                                <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged"></asp:DropDownList>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Topic</span><br />
                                                <asp:TextBox ID="txtTopic" runat="server" AutoPostBack="True" Enabled="False" OnTextChanged="txtTopic_TextChanged"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSubjects(); return false;"></asp:ImageButton>
                                            </td>
                                            <td align="left" id="tdSteps" runat="server" visible="false"><span class="field-label">Steps</span><br />
                                                <asp:DropDownList ID="ddlSteps" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" id="tdStudId" runat="server"><span class="field-label">Student ID</span><br />
                                                <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="left"><span class="field-label">Student Name</span><br />
                                                <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                            </td>


                                        </tr>
                                          <tr>
                                            <td colspan="3">
                                                <table width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:RadioButton ID="rdObj1" runat="server" Checked="True" GroupName="g1" Text="Criterias 1 to 25" CssClass="field-label" />

                                                            <asp:RadioButton ID="rdObj2" runat="server" GroupName="g1" Text="Criterias 26 to 50" CssClass="field-label" />

                                                            <asp:RadioButton ID="rdObj3" runat="server" GroupName="g1" Text="Criterias 51 to 75" CssClass="field-label" />

                                                            <asp:RadioButton ID="rdObj4" Visible="false" runat="server" GroupName="g1" Text="Criterias 76 to 100" CssClass="field-label" />
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="vertical-align: bottom;" colspan="3">
                                                <asp:Button ID="btnView" runat="server" CssClass="button" TabIndex="8" Text="View Students"
                                                    ValidationGroup="groupM1" />
                                                <asp:Button ID="btnPrev" runat="server" CssClass="button" Text="Previous Entry" OnClick="btnPrev_Click" />
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>

    <div class="card mb-3">
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" align="center">
                    <tr id="trGrid" runat="server">
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" colspan="4">
                                        <div id="div_gridholder">
                                            <div class="higlight" id="div_status" runat="server" visible="false">
                                                <asp:Label ID="lbl_BlockStatus" runat="server" class="field-label" Style="color: red !important;"></asp:Label>
                                            </div>
                                            <div class="higlight" id="divselectedTopic" runat="server" visible="false">
                                                <span class="field-label">Selected Topic(s)</span><br />
                                                <asp:Literal ID="ltTopics" runat="server"></asp:Literal>
                                            </div>

                                            <asp:Literal ID="ltObjectives" runat="server"></asp:Literal>
                                            <asp:GridView ID="gvStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                HeaderStyle-Height="30" PageSize="20" UseAccessibleHeader="True"
                                                BorderStyle="Solid">
                                                <AlternatingRowStyle CssClass="grid_alternate" Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Stuid" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle HorizontalAlign="Center" Width="1px"></HeaderStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lnkFirstNameH" runat="server" Text="Name" OnClick="lnkFirstNameH_Click"
                                                                Font-Underline="false"></asp:LinkButton>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="img_graph" runat="server" ImageUrl="~/Images/progress-bar.png" Width="30px" Height="30px" />
                                                            <asp:Label ID="lblFirstName" Font-Underline="false" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>' CssClass="labeltolink"></asp:Label>
                                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("STU_LASTNAME") %>' CssClass="labeltolink"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="180px" Height="133px" BackColor="#f5f5f5"></HeaderStyle>
                                                        <ItemStyle Wrap="false"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Current Level" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrentLevel" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell"
                                                            Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Objectives Completed" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblObjCount" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell"
                                                            Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Stuno" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" Width="10%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <%--      <div class="hideText">
                                                                        <asp:Label ID="lblTopic" runat="server" Text="Plants - Adaptations for seed dispersal"></asp:Label>
                                                             </div>--%>
                                                            <div id="divStep1" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep1" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">
                                                                <asp:Label ID="lblObj1H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId1" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId1" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt1" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH1" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj1Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj1Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep2" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep2" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj2H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId2" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId2" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt2" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH2" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj2Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj2Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep3" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep3" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj3H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId3" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId3" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt3" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH3" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj3Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj3Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep4" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep4" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj4H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId4" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId4" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt4" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH4" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj4Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj4Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep5" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep5" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj5H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId5" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId5" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt5" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH5" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj5Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj5Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep6" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep6" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj6H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId6" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId6" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt6" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH6" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj6Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj6Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep7" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep7" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj7H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId7" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId7" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt7" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH7" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj7Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj7Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep8" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep8" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj8H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId8" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId8" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt8" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH8" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj8Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj8Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep9" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep9" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj9H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId9" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId9" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt9" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH9" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj9Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj9Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep10" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep10" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj10H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId10" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId10" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt10" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH10" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj10Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj10Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep11" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep11" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj11H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId11" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId11" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt11" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH11" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj11Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj11Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep12" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep12" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj12H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId12" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId12" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt12" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH12" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj12Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj12Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep13" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep13" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj13H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId13" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId13" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt13" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH13" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj13Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj13Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep14" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep14" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj14H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId14" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId14" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt14" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH14" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj14Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj14Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep15" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep15" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj15H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId15" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId15" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt15" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH15" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj15Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj15Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep16" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep16" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj16H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId16" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId16" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt16" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH16" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj16Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj16Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep17" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep17" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj17H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId17" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId17" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt17" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH17" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj17Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj17Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep18" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep18" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj18H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId18" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId18" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt18" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH18" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj18Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj18Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep19" runat="server">

                                                                <div class="hideSteps19">
                                                                    <asp:Label ID="lblStep19" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj19H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId19" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId19" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt19" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH19" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj19Id');">
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj19Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep20" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep20" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj20H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId20" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId20" runat="server" Visible="false"></asp:Label>
                                                            </div>

                                                            <asp:Label ID="lblObjDt20" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH20" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj20Id');">
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj20Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep21" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep21" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj21H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId21" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId21" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt21" runat="server"></asp:Label>
                                                            <br />

                                                            <asp:DropDownList ID="ddlObjH21" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj21Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj21Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep22" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep22" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj22H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId22" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId22" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt22" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH22" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj22Id');">
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj22Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep23" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep23" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj23H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId23" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId23" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt23" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH23" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj23Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj23Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep24" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep24" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj24H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId24" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId24" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt24" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH24" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj24Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj24Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <div id="divStep25" runat="server">

                                                                <div class="hideSteps">
                                                                    <asp:Label ID="lblStep25" runat="server"></asp:Label>
                                                                </div>
                                                                <hr class="hr-margin" />
                                                            </div>
                                                            <div class="hideText">

                                                                <asp:Label ID="lblObj25H" runat="server"></asp:Label>
                                                                <asp:Label ID="lblObjId25" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSydId25" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <asp:Label ID="lblObjDt25" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH25" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj25Id');">
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj25Id" runat="server" OnChange="Javascript:setColor(this);">
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td width="200px">
                                        <span class="field-label">Completion Date : </span>
                                    </td>
                                    <td width="250px">
                                        <asp:TextBox ID="txtDate" runat="server"> </asp:TextBox><asp:ImageButton
                                            ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtDate" PopupPosition="TopRight">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td colspan="20">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_LVL_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_TopicID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TRM_ID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlFilters"
        CollapseControlID="lnkSearch" ExpandControlID="lnkSearch" TextLabelID="lnkSearch"
        CollapsedText="Show Filters" ExpandedText="Hide Filters" AutoCollapse="false" Collapsed="false">
    </ajaxToolkit:CollapsiblePanelExtender>
</asp:Content>
