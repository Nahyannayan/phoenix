
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmRemoveTcSOStudentsFromGroup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100140") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)


                    PopulateSection()


                    tblTC.Rows(3).Visible = False
                    tblTC.Rows(4).Visible = False
                    tblTC.Rows(5).Visible = False
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Private Sub PopulateSection()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"


        ddlSection.Items.Clear()
        If ddlGrade.SelectedValue = "All" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View' " _
                    & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                    & " WHERE STU_CURRSTATUS<>'EN' " _
                    & "  AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                    & " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SSD_SGR_ID IS NOT NULL)"
                   

        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If

        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection

        set_Menu_Img()

        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'GridBind()
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)


        PopulateSection()
        If ViewState("norecord") = "1" Then
            tblTC.Rows(3).Visible = False
            tblTC.Rows(4).Visible = False
        End If
    End Sub

    Protected Sub gvStud_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStud.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If


        Dim chk As CheckBox
        Dim stuId As String = String.Empty
        For Each rowItem As GridViewRow In gvStud.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            stuId = DirectCast(rowItem.FindControl("lblStuId"), Label).Text
            If hash.ContainsValue(stuId) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub


    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If


            Dim chk As CheckBox
            Dim stuId As String = String.Empty
            For Each rowItem As GridViewRow In gvStud.Rows
                ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
                'DirectCast(row.FindControl("lblEnqId"), Label).Text)
                stuId = DirectCast(rowItem.FindControl("lblStuId"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(stuId) = False Then
                        hash.Add(stuId, DirectCast(rowItem.FindControl("lblStuId"), Label).Text)
                    End If
                Else
                    If hash.Contains(stuId) = True Then
                        hash.Remove(stuId)
                    End If
                End If
            Next

            Session("hashCheck") = hash
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub




    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            PopulateSection()
            If ViewState("norecord") = "1" Then
                tblTC.Rows(3).Visible = False
                tblTC.Rows(4).Visible = False
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Session("liUserList") = New List(Of String)
            Session("hashCheck") = Nothing
            tblTC.Rows(3).Visible = True
            tblTC.Rows(4).Visible = True
            tblTC.Rows(5).Visible = True
              hfACD_ID.Value = ddlAcademicYear.SelectedValue
            hfGRD_ID.Value = ddlGrade.SelectedValue
            hfSCT_ID.Value = ddlSection.SelectedValue
            hfSTUNO.Value = txtStuNo.Text
            hfNAME.Value = txtName.Text
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub




    Sub PrintAdmitSlip()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("principal", GetEmpName("PRINCIPAL"))
        param.Add("registrar", GetEmpName("REGISTRAR"))
        'param.Add("@STU_XML", ViewState("stuIds"))

        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim stuIds As String = ""


        'For i = 0 To gvStud.Rows.Count - 1
        '    chkSelect = gvStud.Rows(i).FindControl("chkSelect")
        '    If chkSelect.Checked = True Then
        '        lblStuId = gvStud.Rows(i).FindControl("lblStuId")
        '        stuIds += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID></ID>"
        '    End If
        'Next

        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                stuIds += "<ID><STU_ID>" + hashloop.Value + "</STU_ID></ID>"
            Next
        End If

        If stuIds = "" Then
            lblError.Text = "No records selected"
            Exit Sub
        End If
        stuIds = "<IDS>" + stuIds + "</IDS>"
        param.Add("@STU_XML", stuIds)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptAdmitSlip.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                str_query = "EXEC saveSTUDENTTCSOGROUPSDELETE " _
                            & hashloop.Value + "," _
                            & hfACD_ID.Value
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
        End If

    End Sub
    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If emp Is Nothing Then
            emp = ""
        End If
        Return emp
    End Function
    Protected Sub btnOkay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkay.Click
        Dim ChkState As Boolean = False

        Dim chk As CheckBox
        Dim stuId As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvStud.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
            stuId = DirectCast(rowItem.FindControl("lblstuId"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(stuId) = False Then
                    hash.Add(stuId, DirectCast(rowItem.FindControl("lblstuId"), Label).Text)
                End If
            Else
                If hash.Contains(stuId) = True Then
                    hash.Remove(stuId)
                End If
            End If
        Next

        Session("hashCheck") = hash
        Dim selectedcount As Integer = Session("hashCheck").Count

        'If Not Session("hashCheck") Is Nothing Then
        If selectedcount > 0 Then
            SaveData()
            lblError.Text = "Records Saved Successfully"
        Else
            lblError.Text = "No records selected"
        End If
        Session("liUserList") = Nothing
        Session("hashCheck") = Nothing
        GridBind()
    End Sub
    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Me.MPE1.Show()
    End Sub
End Class

