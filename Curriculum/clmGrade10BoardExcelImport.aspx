<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGrade10BoardExcelImport.aspx.vb" Inherits="Curriculum_clmGrade10BoardExcelImport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                        <td align="left" width="20%">
                            <span class="field-label">Report Card</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            <span class="field-label">Report Schedule</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList></td>
                        <td align="center" colspan="2">
                            <asp:RadioButton ID="rdSubject" runat="server" GroupName="g1" Text="Subject" Checked="True" AutoPostBack="true" class="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdSkill" runat="server" Text="Skill" GroupName="g1" AutoPostBack="true" class="field-label"></asp:RadioButton></td>

                    </tr>
                    <%--    <tr>
                        <td align="left" colspan="4" valign="middle">
                           </td>
                    </tr>--%>
                    <tr>
                        <td align="left" valign="middle">
                            <span class="field-label">Report Header</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlHeader" runat="server">
                            </asp:DropDownList></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>



                    <tr id="trSubject" runat="server">
                        <td align="left" valign="middle">
                            <span class="field-label">Subject</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                            </asp:DropDownList>&nbsp;
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4" >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

