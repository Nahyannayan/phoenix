﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Data.OleDb
Partial Class Curriculum_clmProgressTest
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100852") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))

                    FillGrade()
                    FillSection()
                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '  lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " SELECT DISTINCT GRM_DISPLAY,grm_GRD_ID,GRD_DISPLAYORDER  FROM GRADE_BSU_M  INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID  WHERE " _
                                 & " GRM_BSU_ID ='" + Session("SBSUID") + "' AND GRM_ACD_ID= '" + ddlAca_Year.SelectedValue + "' ORDER BY GRD_DISPLAYORDER "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()



    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= '" + ddlAca_Year.SelectedValue + "'  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedItem.Value + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        FillSection()
        gridbind()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        FillGrade()
        FillSection()
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            Dim txtSearch As New TextBox
            Dim optSearch As String
            Dim strFilter As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String
            Dim strFilter1 As String = ""


            Dim currAcd As String = Session("Current_acd_id")

            If ddlAca_Year.SelectedValue = currAcd Then
                str_Sql = "SELECT DISTINCT STU_ID,STU_NO,stu_NAME=ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,''), " _
                     & " PT_MS,PT_MT,PT_ES,PT_ET,PT_SS,PT_ST " _
                     & "   FROM STUDENT_M " _
                     & " LEFT OUTER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID " _
                      & " left outer join PROGRESSTESTDETAILS on STU_ID =pt_stu_id AND pt_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                      & " WHERE STU_ACD_ID=" & ddlAca_Year.SelectedValue & " and STU_GRD_ID ='" & ddlGrade.SelectedValue & "' "
                '& " AND AD_GRD_ID='" & ddlGrade.SelectedValue & "'"


            Else
                str_Sql = "SELECT DISTINCT STU_ID,STU_NO,stu_NAME=ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,''), " _
                      & " PT_MS,PT_MT,PT_ES,PT_ET,PT_SS,PT_ST " _
                      & "   FROM STUDENT_M " _
                      & " LEFT OUTER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID " _
                       & " left outer join PROGRESSTESTDETAILS on STU_ID =pt_stu_id AND pt_aCD_ID= " + ddlAca_Year.SelectedValue.ToString _
                       & " WHERE STP_ACD_ID=" & ddlAca_Year.SelectedValue & " and STP_GRD_ID ='" & ddlGrade.SelectedValue & "' "
                '& " AND AD_GRD_ID='" & ddlGrade.SelectedValue & "'"
            End If




            If ddlSection.SelectedValue <> "" Then
                If ddlAca_Year.SelectedValue = currAcd Then
                    str_Sql += " AND STu_SCT_ID='" + ddlSection.SelectedValue + "'"
                Else
                    str_Sql += " AND STP_SCT_ID='" + ddlSection.SelectedValue + "'"
                End If

            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch = gvComments.HeaderRow.FindControl("txtOption")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch.Text <> "" Then
                    strFilter = " and stu_no  like '" & txtSearch.Text & "%' "
                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If
            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch1.Text <> "" Then
                    strFilter1 = " and ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')  like '" & txtSearch1.Text & "%' "
                    optSearch1 = txtSearch1.Text



                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If
            End If
            str_Sql += " " + "order by ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch

            txtSearch1 = New TextBox
            txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
            txtSearch1.Text = optSearch1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub btnEmpid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnstuname_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpLoadExcelFiletoXml()
        ' UploadExcel()
    End Sub

    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Private Sub UpLoadExcelFiletoXml()
        Dim strFileNameOnly As String

        lblerror3.Text = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "_") & Session("sBsuid") & "_" & ddlGrade.SelectedItem.Value & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly & ";" & _
                "Extended Properties=Excel 8.0;"

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim icount As Integer = dt.Rows.Count
                Dim xmlString As String = ""

                'Dim StudentID As String
                'Dim StudentName As String
                'Dim MathScore As Integer
                'Dim MathPercentile As Integer
                'Dim EnglishScore As Integer
                'Dim EnglishPercentile As Integer
                'Dim ScienceScore As Integer
                'Dim SciencePercentile As Integer
                Dim strCon As String = ConnectionManger.GetOASISConnectionString
                Dim stu_id As String = ""
                Dim dsStu As DataSet
                Dim str_query As String = ""
                Dim count As Integer
                For count = 0 To dt.Rows.Count - 1


                    xmlString += "<ID><StudentID>" + dt.Rows(count).Item(0).ToString + "</StudentID>"
                    xmlString += "<StudentName>" + dt.Rows(count).Item(1).ToString + "</StudentName>"
                    xmlString += "<MathScore>" + dt.Rows(count).Item(2).ToString + "</MathScore>"
                    xmlString += "<MathStanine>" + dt.Rows(count).Item(3).ToString + "</MathStanine>"
                    xmlString += "<EnglishScore>" + dt.Rows(count).Item(4).ToString + "</EnglishScore>"
                    xmlString += "<EnglishStanine>" + dt.Rows(count).Item(5).ToString + "</EnglishStanine>"
                    xmlString += "<ScienceScore>" + dt.Rows(count).Item(6).ToString + "</ScienceScore>"
                    xmlString += "<ScienceStanine>" + dt.Rows(count).Item(7).ToString + "</ScienceStanine></ID>"
                Next

                xmlString = "<IDS>" + xmlString + "</IDS>"



                'commented by Minu as Upload is throwing Error
                'Dim s As New MemoryStream()

                'dt.WriteXml(s, True)

                'Retrieve the text from the stream

                's.Seek(0, SeekOrigin.Begin)

                'Dim sr As New StreamReader(s)

                'Dim xmlString As String

                'xmlString = sr.ReadToEnd()

                'sr.Close()

                'sr.Dispose()
                Dim cmd As New SqlCommand
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("dbo.savePROGRESSTESTDETAILSXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@AD_ACD_ID", ddlAca_Year.SelectedValue)
                cmd.Parameters.AddWithValue("@AD_GRD_ID", ddlGrade.SelectedItem.Value)

                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()

                lblerror3.Text = "Records Saved Sucessfully"
                gridbind()

                'Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                'Dim filename As String = uploadFile.FileName.ToString()
                'Dim savepath As String = Path + "SMS/" + Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
                'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                'Dim mObj As ExcelRowCollection
                'Dim iRowRead As Boolean
                'Dim ef As ExcelFile = New ExcelFile
                'Dim mRowObj As ExcelRow
                'Dim iRow As Integer = 1
                'Dim dataTable As New DataTable
                'Dim dr As DataRow

                'iRowRead = True

                'dataTable.Columns.Add("ID", GetType(Int64))
                'dataTable.Columns.Add("MobileNo", GetType(String))
                'dataTable.Columns.Add("Name", GetType(String))

                'ef.LoadXls(savepath)
                'mObj = ef.Worksheets(0).Rows


                ''Dim ws As ExcelWorksheet = ef.Worksheets(0)
                ''ws.ExtractToDataTable(dataTable, ws.Rows.Count, ExtractDataOptions.StopAtFirstEmptyRow, ws.Rows(0), ws.Columns(0))

                'While iRowRead
                '    mRowObj = mObj(iRow)
                '    If mRowObj.Cells(0).Value Is Nothing Then
                '        Exit While
                '    End If
                '    If mRowObj.Cells(0).Value.ToString = "" Then
                '        Exit While
                '    End If

                '    If Not mRowObj.Cells(1).Value Is Nothing Then
                '        dr = dataTable.NewRow
                '        dr.Item(0) = Convert.ToInt64(mRowObj.Cells(0).Value)
                '        dr.Item(1) = mRowObj.Cells(1).Value.ToString
                '        'dr.Item(2) = mRowObj.Cells(0).Value.ToString
                '        'dr.Item(3) = mRowObj.Cells(1).Value.ToString
                '        'dr.Item(4) = "add"
                '        'dr.Item(5) = iRow
                '        'dr.Item(6) = "0"
                '        'dr.Item(7) = "false"
                '        dataTable.Rows.Add(dr)
                '    End If
                '    iRow += 1
                'End While

                'myDataset.Tables.Add(dataTable)
                'Dim i = 0

                'Dim student_id As String
                'Dim mobilenumber As String
                'Dim errorflag As Boolean
                'Dim str_query As String
                'Dim name As String


                'For i = 0 To myDataset.Tables(0).Rows.Count - 1

                '    student_id = myDataset.Tables(0).Rows(i).Item("ID").ToString().Trim()
                '    name = myDataset.Tables(0).Rows(i).Item("Name").ToString().Replace("'", "")

                '    mobilenumber = myDataset.Tables(0).Rows(i).Item("MobileNo").ToString().Trim()
                '    mobilenumber = mobilenumber.Replace("-", "").Trim()
                '    If Session("sbsuid") <> "315888" Then
                '        mobilenumber = "971" & Right(mobilenumber, 9)
                '    Else
                '        mobilenumber = "012" & Right(mobilenumber, 7)
                '    End If

                '    If student_id <> "" And mobilenumber <> "" Then
                '        If mobilenumber.Length < 10 Then
                '            errorflag = False
                '        Else
                '            errorflag = True
                '        End If

                '        str_query = "insert into  COM_EXCEL_SMS_DATA_LOG (LOG_ID,ID,MOBILE_NO,NAME,NO_ERROR_FLAG) values('" & Logid & "','" & student_id & "','" & mobilenumber & "','" & name & "','" & errorflag & "') "
                '        SqlHelper.ExecuteNonQuery(Transaction, CommandType.Text, str_query)
                '    End If


                'Next



            Catch ex As Exception
                lblerror3.Text = "Error: " & ex.Message.ToString
            End Try
        Else
            lblerror3.Text = "Please select a file to upload."
        End If


    End Sub

    Sub UploadExcel()
        Dim strFileNameOnly As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "_") & Session("sBsuid") & "_" & ddlGrade.SelectedItem.Value & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                '  Dim ef As ExcelFile = New ExcelFile

                Dim mObj As ExcelRowCollection

                Dim iRowRead As Boolean
                iRowRead = True
                Dim xmlString As String = ""

                Dim ef = ExcelFile.Load(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                '  ef.LoadXls(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim mRowObj As ExcelRow
                mObj = ef.Worksheets(0).Rows
                Dim iRow As Integer = 1


                While iRowRead
                    mRowObj = mObj(iRow)
                    If mRowObj.Cells(0).Value Is Nothing Then
                        Exit While
                    End If
                    If mRowObj.Cells(0).Value.ToString = "" Then
                        Exit While
                    End If
                    xmlString += "<ID><StudentID>" + mRowObj.Cells(0).Value.ToString + "</StudentID>"
                    xmlString += "<StudentName>" + mRowObj.Cells(1).Value.ToString + "</StudentName>"
                    xmlString += "<MathScore>" + mRowObj.Cells(2).Value.ToString + "</MathScore>"
                    xmlString += "<MathStanine>" + mRowObj.Cells(3).Value.ToString + "</MathStanine>"
                    xmlString += "<EnglishScore>" + mRowObj.Cells(4).Value.ToString + "</EnglishScore>"
                    xmlString += "<EnglishStanine>" + mRowObj.Cells(5).Value.ToString + "</EnglishStanine>"
                    xmlString += "<ScienceScore>" + mRowObj.Cells(6).Value.ToString + "</ScienceScore>"
                    xmlString += "<ScienceStanine>" + mRowObj.Cells(7).Value.ToString + "</ScienceStanine></ID>"
                    iRow += 1
                End While


                xmlString = "<IDS>" + xmlString + "</IDS>"

                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("dbo.savePROGRESSTESTDETAILSXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@AD_ACD_ID", ddlAca_Year.SelectedValue)
                cmd.Parameters.AddWithValue("@AD_GRD_ID", ddlGrade.SelectedItem.Value)

                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()


                'str_query = " saveASSETDETAILSXml" _
                '            & " @AD_ACD_ID=" + ddlAca_Year.SelectedValue.ToString + "," _
                '            & " @AD_GRD_ID='" + ddlGrade.SelectedItem.Value.ToString + "'," _
                '            & " @data='" + xmlString + "'"

                'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                lblerror3.Text = "Records Saved Sucessfully"
                gridbind()
            Catch ex As Exception
                lblerror3.Text = ex.Message
            End Try
        End If
    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""

            For Each row As GridViewRow In gvComments.Rows
                Dim lblid As New Label
                Dim txtn As New TextBox
                Dim txtv As New TextBox
                Dim txtq As New TextBox
                Dim txts As New TextBox
                Dim txtm As New TextBox
                Dim txtSc As New TextBox

                lblid = row.FindControl("cmtId")
                txtn = row.FindControl("txtnv")
                txtv = row.FindControl("txtv")
                txtq = row.FindControl("txtqu")
                txts = row.FindControl("txtsp")
                txtm = row.FindControl("txtmc")
                txtSc = row.FindControl("txtscp")
                If txtn.Text = "" Then txtn.Text = "0"
                If txtv.Text = "" Then txtv.Text = "0"
                If txtq.Text = "" Then txtq.Text = "0"
                If txts.Text = "" Then txts.Text = "0"
                If txtm.Text = "" Then txtm.Text = "0"
                If txtSc.Text = "" Then txtSc.Text = "0"

                str_query = "exec dbo.savePROGRESSTESTDETAILS " & ddlAca_Year.SelectedValue & " ,'" & ddlGrade.SelectedValue & "'," & lblid.Text & "," _
                             & "" & txtn.Text & "," & txtv.Text & "," & txtq.Text & "," & txts.Text & "," & txtm.Text & "," & txtSc.Text

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                lblError.Text = "Saved..."

            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

End Class
