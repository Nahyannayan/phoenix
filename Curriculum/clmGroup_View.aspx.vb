Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroup_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    GridBind()
                    gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")

            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGroup_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSubject_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnOption_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        'if the login user is a teacher then display only the groups for that teacher
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "  SELECT DISTINCT SGR_ID,SGR_SBG_ID,SGR_GRD_ID,SGR_SHF_ID,SGR_STM_ID," _
                                     & " SGR_DESCR, GRM_DISPLAY, SHF_DESCR, STM_DESCR, " _
                                     & " SBG_DESCR,SBG_PARENTS,ISNULL(SGR_UNTIS_GRP,'') SGR_UNTIS_GRP FROM GROUPS_M AS A" _
                                     & " INNER JOIN GRADE_BSU_M AS B ON A.SGR_GRD_ID=B.GRM_GRD_ID AND " _
                                     & " A.SGR_ACD_ID=B.GRM_ACD_ID AND A.SGR_STM_ID=B.GRM_STM_ID AND A.SGR_SHF_ID=B.GRM_SHF_ID " _
                                     & " INNER JOIN STREAM_M AS C ON A.SGR_STM_ID=C.STM_ID" _
                                     & " INNER JOIN SHIFTS_M AS D ON A.SGR_SHF_ID=D.SHF_ID" _
                                     & " INNER JOIN SUBJECTS_GRADE_S AS E ON A.SGR_SBG_ID=E.SBG_ID" _
                                     & " WHERE SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString
        ElseIf studClass.isEmpTeacher(empId) = True Then

            str_query = "  SELECT DISTINCT SGR_ID,SGR_SBG_ID,SGR_GRD_ID,SGR_SHF_ID,SGR_STM_ID," _
                                & " SGR_DESCR, GRM_DISPLAY, SHF_DESCR, STM_DESCR, " _
                                & " SBG_DESCR,SBG_PARENTS,,ISNULL(SGR_UNTIS_GRP,'') SGR_UNTIS_GRP FROM GROUPS_M AS A" _
                                & " INNER JOIN GRADE_BSU_M AS B ON A.SGR_GRD_ID=B.GRM_GRD_ID AND " _
                                & " A.SGR_ACD_ID=B.GRM_ACD_ID AND A.SGR_STM_ID=B.GRM_STM_ID AND A.SGR_SHF_ID=B.GRM_SHF_ID " _
                                & " INNER JOIN STREAM_M AS C ON A.SGR_STM_ID=C.STM_ID" _
                                & " INNER JOIN SHIFTS_M AS D ON A.SGR_SHF_ID=D.SHF_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS E ON A.SGR_SBG_ID=E.SBG_ID" _
                                & " INNER JOIN GROUPS_TEACHER_S AS F ON A.SGR_ID=F.SGS_SGR_ID AND F.SGS_TODATE IS NULL " _
                                & " WHERE SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " AND SGS_EMP_ID=" + empId.ToString

        End If

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim txtSearch As New TextBox
        Dim grpSearch As String = ""
        Dim subSearch As String = ""

        Dim selectedGrade As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvStream As New DropDownList
        Dim i As Integer

        If gvGroup.Rows.Count > 0 Then

            txtSearch = gvGroup.HeaderRow.FindControl("txtGroup")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SGR_DESCR", txtSearch.Text, strSearch)
            grpSearch = txtSearch.Text

            txtSearch = gvGroup.HeaderRow.FindControl("txtSubject")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("E.SBG_DESCR", txtSearch.Text, strSearch)
            subSearch = txtSearch.Text


            ddlgvGrade = gvGroup.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
                strFilter += " and grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvShift = gvGroup.HeaderRow.FindControl("ddlgvShift")
            If ddlgvShift.Text <> "ALL" And ddlgvShift.Text <> "" Then
                strFilter = strFilter + " and  shf_descr='" + ddlgvShift.Text + "'"
                selectedShift = ddlgvShift.Text
            End If


            ddlgvStream = gvGroup.HeaderRow.FindControl("ddlgvStream")
            If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then
                strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                selectedStream = ddlgvStream.Text
            End If

            If strFilter.Trim <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " order by sgr_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGroup.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup.DataBind()
            Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
            gvGroup.Rows(0).Cells.Clear()
            gvGroup.Rows(0).Cells.Add(New TableCell)
            gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvGroup.DataBind()
        End If

        txtSearch = New TextBox
        txtSearch = gvGroup.HeaderRow.FindControl("txtGroup")
        txtSearch.Text = grpSearch

        txtSearch = New TextBox
        txtSearch = gvGroup.HeaderRow.FindControl("txtSubject")
        txtSearch.Text = subSearch

        Dim dt As DataTable = ds.Tables(0)

        If gvGroup.Rows.Count > 0 Then
            ddlgvGrade = gvGroup.HeaderRow.FindControl("ddlgvGrade")
            ddlgvShift = gvGroup.HeaderRow.FindControl("ddlgvShift")
            ddlgvStream = gvGroup.HeaderRow.FindControl("ddlgvStream")

            Dim dr As DataRow

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")

            ddlgvShift.Items.Clear()
            ddlgvShift.Items.Add("ALL")

            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                    If ddlgvGrade.Items.FindByText(.Item(6)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(6))
                    End If

                    If ddlgvShift.Items.FindByText(.Item(7)) Is Nothing Then
                        ddlgvShift.Items.Add(.Item(7))
                    End If

                    If ddlgvStream.Items.FindByText(.Item(8)) Is Nothing Then
                        ddlgvStream.Items.Add(.Item(8))
                    End If

                End With
            Next

            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If

            If selectedShift <> "" Then
                ddlgvShift.Text = selectedShift
            End If
            If selectedGrade <> "" Then
                ddlgvShift.Text = selectedShift
            End If
        End If


        set_Menu_Img()

        If Session("multistream") = 0 Then
            gvGroup.Columns(11).Visible = False
        End If
        If Session("multishift") = 0 Then
            gvGroup.Columns(10).Visible = False
        End If


    End Sub

#End Region

    Protected Sub lbAuto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAuto.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Curriculum\clmGroupAutoAllocate_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        Try
            gvGroup.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            gvGroup.DataBind()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Curriculum\clmGroup_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand
        Try

            Dim url As String


            If e.CommandName = "View" Then
                Dim lblGrade As New Label
                Dim lblGroup As New Label
                Dim lblStream As Label
                Dim lblShift As Label
                Dim lblSubject As Label
                Dim lblSgrId As Label
                Dim lblGrdId As Label
                Dim lblShfId As Label
                Dim lblStmId As Label
                Dim lblSbgId As Label
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGroup.Rows(index), GridViewRow)
                Dim lblUntisGroup As Label

                ViewState("datamode") = Encr_decrData.Encrypt("view")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

                lblGrade = selectedRow.FindControl("lblGrade")
                lblStream = selectedRow.FindControl("lblStream")
                lblShift = selectedRow.FindControl("lblShift")
                lblSubject = selectedRow.FindControl("lblSubject")
                lblGroup = selectedRow.FindControl("lblGroup")
                lblSgrId = selectedRow.FindControl("lblSgrId")
                lblGrdId = selectedRow.FindControl("lblGrdId")
                lblShfId = selectedRow.FindControl("lblShfId")
                lblStmId = selectedRow.FindControl("lblStmId")
                lblSbgId = selectedRow.FindControl("lblSbgId")
                lblUntisGroup = selectedRow.FindControl("lblUntisGroup")

                url = String.Format("~\Curriculum\clmGroup_M.aspx?MainMnu_code={0}&datamode={1}&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                 & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                 & "&shift=" + Encr_decrData.Encrypt(lblShift.Text) + "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                 & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) + "&sgrid=" + Encr_decrData.Encrypt(lblSgrId.Text) _
                 & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) + "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) _
                 & "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text) + "&sbgid=" + Encr_decrData.Encrypt(lblSbgId.Text) _
                 & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                 & "&untisgroup=" + Encr_decrData.Encrypt(lblUntisGroup.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
