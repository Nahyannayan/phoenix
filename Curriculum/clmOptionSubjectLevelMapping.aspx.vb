﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Collections.Generic
Imports System.Collections
Partial Class Curriculum_clmOptionSubjectLevelMapping
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100450") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights




                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

                    If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
                        GetPrevGrade(ddlGrade.SelectedValue.ToString)
                    Else
                        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
                    End If

                    BindSubject()
                    BindPrevYearSubject()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try


        End If



    End Sub
#Region "Private Methods"

    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm _
                                  & " AND ACD_ID IN(" + Session("CURRENT_ACD_ID") + "," + Session("NEXT_ACD_ID") + ")" _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()


        ddlAcademicYear.Items.FindByValue(Session("NEXT_ACD_ID")).Selected = True
        Return ddlAcademicYear
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                  & " FROM SUBJECTS_GRADE_S AS A" _
                  & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                  & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                  & " AND SBG_bOPTIONAL=1" _
                  & " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindPrevYearSubject()
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                  & " FROM SUBJECTS_GRADE_S AS A" _
                  & " WHERE SBG_ACD_ID=" + ddlAcademicYear.Items(0).Value.ToString _
                  & " AND SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
                  & " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubject.DataSource = ds
        gvSubject.DataBind()

        BindMappedSubjects()
        '& " AND SBG_bOPTIONAL=1" 
    End Sub

    Public Sub GetPrevGrade(ByVal vGRD_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER - 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfGRD_ID.Value = ds.Tables(0).Rows(0).Item("GRD_ID")
    End Sub

    Sub BindLevels(ByVal ddlLevel As DropDownList, ByVal sbg_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RGP_GRADE,RGP_VALUE FROM " _
                               & " RPT.REPORT_GRADEMAPPING WHERE RGP_SBG_ID=" + sbg_id + " ORDER BY RGP_GRADE"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "RGP_GRADE"
        ddlLevel.DataValueField = "RGP_VALUE"
        ddlLevel.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlLevel.Items.Insert(0, li)
    End Sub


    Sub BindMappedSubjects()
        If gvSubject.Rows.Count > 0 Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT SSM_MINLEVEL,SSM_MINLEVELVALUE,SSM_MAPPED_SBG_ID FROM " _
                                     & " OPTION_SUBJECTMAPPING_S WHERE SSM_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"

            Dim i, j As Integer
            Dim lblSbgId As Label
            Dim ddlLevel As DropDownList

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To gvSubject.Rows.Count - 1
                    For j = 0 To ds.Tables(0).Rows.Count - 1
                        lblSbgId = gvSubject.Rows(i).FindControl("lblSbgId")
                        With ds.Tables(0).Rows(j)
                            If .Item(2) = lblSbgId.Text Then
                                ddlLevel = gvSubject.Rows(i).FindControl("ddlLevel")
                                If Not ddlLevel.Items.FindByText(.Item(0)) Is Nothing Then
                                    ddlLevel.Items.FindByText(.Item(0)).Selected = True
                                End If
                            End If
                        End With
                    Next
                Next
            End If
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer

        Dim lblSbgId As Label
        Dim ddlLevel As DropDownList

        For i = 0 To gvSubject.Rows.Count - 1
            lblSbgId = gvSubject.Rows(i).FindControl("lblSbgId")
            ddlLevel = gvSubject.Rows(i).FindControl("ddlLevel")

            str_query = "EXEC [dbo].[saveOPTION_SUBJECTMAPPING_S] " _
                      & "@SSM_SBG_ID =" + ddlSubject.SelectedValue.ToString + "," _
                      & "@SSM_MAPPED_SBG_ID = " + lblSbgId.Text + "," _
                      & "@SSM_MINLEVEL = '" + ddlLevel.SelectedItem.Text + "'," _
                      & "@SSM_MINLEVELVALUE = " + ddlLevel.SelectedValue.ToString
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub

#End Region

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindPrevYearSubject()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If
        BindSubject()
        BindPrevYearSubject()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If
        BindSubject()
        BindPrevYearSubject()
    End Sub

    Protected Sub gvSubject_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubject.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblSbgId As Label
            Dim ddlLevel As DropDownList

            lblSbgId = e.Row.FindControl("lblSbgId")
            ddlLevel = e.Row.FindControl("ddlLevel")
            BindLevels(ddlLevel, lblSbgId.Text)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        lblError.Text = "Record saved successfully"
    End Sub
End Class
