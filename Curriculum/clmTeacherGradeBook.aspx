﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmTeacherGradeBook.aspx.vb" Inherits="Curriculum_clmTeacherGradeBook"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">
function checkNum(x,evt)
{
var s_len=x.value.length ;
 var s_charcode = 0;
 for (var s_i=0;s_i<s_len;s_i++)
 {
     s_charcode = x.value.charCodeAt(s_i);
     
 if(!((s_charcode>=48 && s_charcode<=57)))
 {
     if (s_charcode == 46) {return true;
     }
     else if (x.value == "AB" || x.value == "ab" || x.value == "L" || x.value == "l") {
         return true;
     }
     else {
         alert("Only Numeric Values,AB or L Allowed");
         x.value = '';
         x.focus();
         return false;
     }
}
}
return true;
}


function onchangetxt(hdr_mrk, rowid, colid) {

    strId = "ctl00_cphMasterpage_gvMarks_ctl0" + (rowid).toString() + "_txtMarks" + colid;
    ErrID = "ctl00_cphMasterpage_gvMarks_ctl0" + (rowid).toString() + "_lblErrGrid" + colid;
    var txt = document.getElementById(strId);
    var Err = document.getElementById(ErrID);
    if(txt.value > hdr_mrk)
    {
        // alert("Mark Entered is higher");
        Err.style.display = "block";
        txt.value = "";
    }
    else {
        Err.style.display = "none";
    }
}

function ScrollDown(rowid, colid, evt) {
    // var txt = document.getElementById("ctl00_cphMasterpage_gvMarks_ctl27_txtMarks3");
    var strId;
    var charCode = (evt.which) ? evt.which : event.keyCode;

    //scrolldown or enter key
    if (charCode == 40 || charCode == 13) {
        if (rowid < 9) {
            strId = "ctl00_cphMasterpage_gvMarks_ctl0" + (rowid + 1).toString() + "_txtMarks" + colid
        }
        else {
            strId = "ctl00_cphMasterpage_gvMarks_ctl" + (rowid + 1).toString() + "_txtMarks" + colid
        }
        var txt = document.getElementById(strId);
        if (txt != null) {
            txt.focus();
        }

    }

    //scrollup
    if (charCode == 38) {
        if (rowid <= 10) {
            strId = "ctl00_cphMasterpage_gvMarks_ctl0" + (rowid - 1).toString() + "_txtMarks" + colid
        }
        else {
            strId = "ctl00_cphMasterpage_gvMarks_ctl" + (rowid - 1).toString() + "_txtMarks" + colid
        }
        var txt = document.getElementById(strId);
        if (txt != null) {
            txt.focus();
        }

    }

    //scrollright
    if (charCode == 39) {
        if (rowid < 10) {
            strId = "ctl00_cphMasterpage_gvMarks_ctl0" + rowid + "_txtMarks" + (colid + 1).toString();
        }
        else {
            strId = "ctl00_cphMasterpage_gvMarks_ctl" + rowid + "_txtMarks" + (colid + 1).toString();
        }
        var txt = document.getElementById(strId);
        if (txt != null) {
            txt.focus();
        }

    }


    //scrollleft
    if (charCode == 37) {
        if (rowid < 10) {
            strId = "ctl00_cphMasterpage_gvMarks_ctl0" + rowid + "_txtMarks" + (colid - 1).toString();
        }
        else {
            strId = "ctl00_cphMasterpage_gvMarks_ctl" + rowid + "_txtMarks" + (colid - 1).toString();
        }
        var txt = document.getElementById(strId);
        if (txt != null) {
            txt.focus();
        }

    }

}

</script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Teacher Gradebook
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                 <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label>
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <table id="Table2" runat="server" width="100%" >
                                <tr >
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlTerm" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left">
                                        <asp:DataList ID="dlGroups" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" 
                                            Width="95" CssClass="menu_a">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSgrId" runat="server" Text='<% #Bind("SGR_ID")%>' Visible="false" />
                                                
                                                            <asp:Button ID="btnGroup" runat="server" Text='<% #Bind("SGR_DESCR")%>' 
                                                                CssClass="button" OnClick="btnGroup_Click" />
                                            </ItemTemplate>

                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr id="trDetails" runat="server">
                                    <td>
                                        <asp:Label ID="lblDetails" runat="server" CssClass="field-label"/>
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton Text="Assessments" ID="lnkAssmt" runat="server"> </asp:LinkButton>
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton Text="Copy Assessment to Mark Entry" ID="lnkCopy" runat="server"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="trGrid" runat="server">
                                    <td colspan="3" align="center">
                                        <table  width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvMarks" runat="server" HorizontalAlign="Center" CssClass="table table-bordered table-row" 
                                                          Width="100%" OnRowDataBound="gvMarks_RowDataBound">
                                                        <RowStyle HorizontalAlign="Center"  />
                                                        <AlternatingRowStyle />
                                                        <HeaderStyle  />
                                                    </asp:GridView>

                                                </td>
                                                <tr id="trSave" runat="server">
                                                    <td colspan="2" align="center">
                                                        <asp:Button ID="btnSaveMarks" Text="Save All" runat="server" CssClass="button" />
                                                        <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="button" />
                                                    </td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlAssmt" runat="server" BackColor="white" CssClass="panel-cover"  >
                    <table  width="100%">
                        <tr>
                            <td>
                                <div style="overflow: scroll;height:400px;" width="70%">
                                    <asp:GridView ID="gvAssmt" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords." PageSize="20">
                                        <RowStyle Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="objid" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGbmId" runat="server" Text='<%# Bind("GBM_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Assessment">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDescr" runat="server" TextMode="multiline" SkinID="multitext"  Text='<%# Bind("GBM_DESCR") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Short Name">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtShort" runat="server" Text='<%# Bind("GBM_SHORT") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Maxmark">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtMax" runat="server" Text='<%# Bind("GBM_MAXMARK") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDate" runat="server"  Text='<%# Bind("GBM_DATE") %>'></asp:TextBox>
                                                    &nbsp;<asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
                                                        TargetControlID="txtDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lock">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBlock" runat="server" Checked='<%# Bind("GBM_bLOCK") %>'></asp:CheckBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="objid" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="index" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblEditH" runat="server" Text="delete"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="delete"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="lnkDelete"
                                                        ConfirmText="Assessment will be deleted permanently.Are you sure you want to continue? " runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle   />
                                        <HeaderStyle   />
                                        <EditRowStyle  />
                                        <AlternatingRowStyle   />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnSaveAssmt" Text="Save" runat="server" CssClass="button" />
                                <asp:Button ID="btnCancelAssmt" Text="Cancel" runat="server" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>


                <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="pnlAssmt" CancelControlID="btnCancelAssmt"
                    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="ddlAcademicYear">
                </ajaxToolkit:ModalPopupExtender>


                <asp:HiddenField ID="hfEMP_ID" runat="server" />
                <asp:HiddenField ID="hfSBM_ID" runat="server" />
                <asp:HiddenField ID="hfSBG_ID" runat="server" />
                <asp:HiddenField ID="hfSGR_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfGroup" runat="server" />
                <asp:HiddenField ID="hfSubject" runat="server" />
                <asp:HiddenField ID="hfACY_DESCR" runat="server" />
             
            </div>
        </div>
    </div>
</asp:Content>
