<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" MaintainScrollPositionOnPostback ="true" 
    CodeFile="clmReportCardSetup_View.aspx.vb" Inherits="Curriculum_clmReportCardSetup_View" Title="Untitled Page" %>

<asp:Content ID="content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    
    <script language="javascript" type="text/javascript">

        function clearSubject() {
            document.getElementById("<%=hfSBG_ID.ClientID %>").value = "";
            document.getElementById("<%=txtSubject.ClientID %>").value = "";
            return false;
        }


        function getSubject() {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfSubjectURL.ClientID %>").value

            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('|');
            document.getElementById("<%=txtSubject.ClientID %>").value = NameandCode[0];
    document.getElementById("<%=hfSBG_ID.ClientID %>").value = NameandCode[1];



        }



        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
    if (color == '') {
        color = getRowColor();
    }
    if (obj.checked) {
        rowObject.style.backgroundColor = '#f6deb2';
    }
    else {
        rowObject.style.backgroundColor = '';
        color = '';
    }
    // private method

    function getRowColor() {
        if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
        else return rowObject.style.backgroundColor;
    }
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

        //Unpublished Rpt Card
var color = '';
function highlight1(obj) {
    var rowObject = getParentRow1(obj);
    var parentTable = document.getElementById("<%=gvGrade.ClientID %>");
            if (color == '') {
                color = getRowColor1();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#deffb4'; //#f6deb2
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor1() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow1(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        //// This method returns the parent row of the object
        //function getParentRow(obj) {
        //    do {
        //        obj = obj.parentElement;
        //    }
        //    while (obj.tagName != "TR")
        //    return obj;
        //}

        function change_chk_state2(chkThis) {       //1
            var chk_state = !chkThis.checked;
            //alert(chk_state);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkPublish") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    //if (document.forms[0].elements[i].disabled == false) {      '16/5/2019
                    document.forms[0].elements[i].checked = chk_state;
                    
                       document.forms[0].elements[i].click();//fire the click event of the child element
                   // }
                   
                }
            }


        }


        function change_chk_state1(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkRelease") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }

            }
        }

        //-------unpublish Report card





    </script>


    <script language="javascript" type="text/javascript">
        function confirm_remove() {

            if (confirm("Are you sure you want to Delete the selected rules?") == true)
                return true;
            else
                return false;

        }

    </script>
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Configurations
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table align="center"  style="border-collapse: collapse" cellpadding="4" cellspacing="0" width="100%">
                  <tr>
                    <td>
                        <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                    <asp:HiddenField  ID ="hdf_SelRsmDescr" runat="server" />
                                    <asp:HiddenField  ID ="hdf_SelDataMode" runat="server" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                                    <span style="  color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                <tr>
                    <td colspan="6">
                        <asp:UpdatePanel ID="Upnl_Main" runat="server" UpdateMode="Conditional"></asp:UpdatePanel>
                    </td>
                 </tr>
                <tr>
<td>
   <ajaxToolkit:TabContainer ID="TabContainer_ReportCardSetup" runat="server" ActiveTabIndex="3" width="100%" AutoPostBack ="true">
        <%--Tab # 1 :Setup--%>
        <ajaxToolkit:TabPanel ID="Tpnl_ReportSetup" runat="server" HeaderText="Setup" >
            <ContentTemplate>
                 <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr class="gridheader_pop" runat="server" >
                          <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Setup</td>
                      </tr>
                 </table>   
                   
              <table id="tbl_Rptheader_setup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr runat="server">
                    <td align="center" runat="server">
                        <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr runat="server">
                                <td align="center" class="matters" runat="server">
                                    <table id="Tbl_Setup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr runat="server">
                                            <td align="left" width="20%" runat="server">
                                               <span class="field-label"> Select&nbsp; Academic Year </span>
                                            </td>
                                            <td width="20%" runat="server">
                                                <asp:DropDownList id="ddlAcademicYear" runat="server"  AutoPostBack="True" >
                                                </asp:DropDownList>
                                            </td>
                                             <td colspan ="0" align ="left" runat="server">
                                                <asp:Button runat="server" Text="Add New"  CssClass ="button" ID ="btnAddNew" Visible="true"/>
                                            </td>
                                          </tr>
                                          <tr runat="server">
                                              <td> <br /> </td>
                                          </tr>
                                          <tr runat="server">
                                              <td colspan="3" align="center" style="width: 338px" runat="server">
                                                  <asp:GridView ID="gvReport" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                                      CssClass="table table-bordered table-row" 
                                                      EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." Width="100%" >
                                                      <rowstyle CssClass="griditem"  />
                                                        <columns>
                                                            <asp:TemplateField HeaderText =" Slno.">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server"  id="lblSrno" Text ='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                            </ItemTemplate>
                                                                <HeaderStyle Width="70px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False"><ItemTemplate>
                                                              <asp:Label ID="lblRsmId" runat="server" Text='<%# Bind("RSM_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Report Card" >
                                                            <ItemTemplate>
                                                              <asp:Label ID="lblReport" runat="server" width="300px" text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                                  <HeaderStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="edit" Text="Edit" >
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="view" Text="View" >
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            </asp:ButtonField>
                                                        </columns>
                                                      <alternatingrowstyle CssClass="griditem_alternative" />
                                                  </asp:GridView>
                                              </td>
                                          </tr>
                                </table>
                                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                                 <input id="h_Selected_menu_2" runat="server" type="hidden" value="="/>
                              </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
                                 <hr />
       </ContentTemplate>
    </ajaxToolkit:TabPanel>

        <%--Tab # 2 :Linking--%>
        <ajaxToolkit:TabPanel ID="Tpnl_Linking" runat="server" HeaderText="Linking">
               <ContentTemplate>
                      <table id="tbl_header_Linking" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Report Linking</td>
                            </tr>
                      </table>   
                          <br /> 
                        <table id="tbl_AcID_Linking" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            
                              <tr runat="server" colspan="4">
                                  <td runat="server"  width="18%">
                                    <span class="field-label">Select Academic Year*</span> 
                                  </td>
                                  <td runat="server" width="20%">
                                      <asp:DropDownList ID="DrpAcID_RptLinking" runat="server" AutoPostBack="True" 
                                          OnSelectedIndexChanged="DrpAcID_RptLinking_SelectedIndexChanged" SkinID="smallcmb" width="20%">
                                      </asp:DropDownList>
                                  </td>
                                  <td><hr /></td>
                              </tr>
                            </table>
                           <hr />
                        <table id="tbl_Selection_Linking" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr runat="server" colspan="4">
                               <td runat="server" width="18%">
                                   <span class="field-label">Report&nbsp;Name&nbsp;(&nbsp;From&nbsp;)* </span>
                               </td>
                               <td runat="server" width ="20%">
                                   <asp:DropDownList id="DrpRpt_RsmID_RptLinking" runat="server"  AppendDataBoundItems="True"  AutoPostBack="True" Width ="130%"  DataValueField ="RSM_ID"></asp:DropDownList>
                               </td>
                               <td runat="server" align="right" width="10%">&#8594;</td>
                               <td  width="10%" runat="server" align="left">
                                   <span class="field-label">Report&nbsp;Name&nbsp;(&nbsp;To&nbsp;)* </span>
                               </td>
                                <td align ="left" runat="server" width ="40%">
                                   <div id="PlChkLst_Linking" runat="server" class="checkbox-list">
                                      <asp:CheckBoxList ID="Chklst_RsmID_Rpt_linking" runat="server" DataTextField="RSM_ID"></asp:CheckBoxList>
                                   </div>
                               </td>
                            </tr>
                      </table>
                   <hr />
                         <table id="Table3" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                             <tr runat="server" width ="100%"  align="center">
                              <td align ="center" runat="server">
                                <asp:Button runat="server" Text="Assign"  CssClass ="button" ID ="btnAssign_RptLinking" />
                                <asp:Button runat="server" Text="Cancel" CssClass ="button" ID ="btnCancel_RptLinking" />
                              </td>
                          </tr>
                        </table>
              </ContentTemplate>
        </ajaxToolkit:TabPanel>

        <%--Tab # 3 :Grade Description--%>
        <ajaxToolkit:TabPanel ID="Tpnl_GradeDescriptor" runat="server" HeaderText="Grade Descriptor">
             <ContentTemplate>      
                 <table id="tbl_Header_GradeDescriptor" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="gridheader_pop" runat="server" >
                            <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Report Grade Descriptor</td>
                        </tr>
                  </table>   
                       <br />                
                 <table id="Tbl_GradeHeader_GradeDescriptor" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr runat="server">
                          <td runat="server" > 
                              <span class="field-label">Select  Academic Year*&nbsp;(&nbsp;From&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                              <asp:DropDownList ID="DrpAcID_From_GradeDescriptor" SkinID="smallcmb" runat="server" AutoPostBack="false" ></asp:DropDownList>
                          </td>
                          <td runat="server">
                            <span class="field-label"> Select  Academic Year*&nbsp;(&nbsp;To&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                            <asp:DropDownList ID="DrpAcID_To_GradeDescriptor" runat="server" AutoPostBack="false" SkinID="smallcmb">
                            </asp:DropDownList>
                         </td>
                         <td  runat="server" align ="right">
                            <asp:Button ID="btnCopy_GradeDescriptor" runat="server" CssClass="button" Text="&nbsp;&nbsp;&nbsp;&nbsp;Copy&nbsp;&nbsp;&nbsp;&nbsp;"/>
                         </td>
                      </tr>
                </table>
                 <hr />
                <table id="Tbl_Entry_GradeDescriptor" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                       <tr>
                           <td>
                               <span class="field-label">Grade ID*</span>
                           </td>
                           <td width="20%" runat="server">
                               <asp:TextBox runat="server" ID ="txtGradeid_GradeDescriptor" MaxLength ="500"></asp:TextBox>
                           </td>
                           <td>
                               <span class="field-label">Grade Heading 1*</span>
                           </td>
                           <td>
                               <asp:TextBox runat="server" ID ="txtGradeheader1_GradeDescriptor" MaxLength ="3000"></asp:TextBox>
                           </td>
                           <td>
                               <span class="field-label">Grade Heading 2*</span>
                           </td>
                           <td>
                               <asp:TextBox runat="server" ID ="txtGradeheader2_GradeDescriptor" MaxLength ="3000"></asp:TextBox>
                           </td>
                           <td runat="server" align ="right">
                               <asp:Button runat="server" Text="Add New" ID="btnAddUpdate_GradeDescriptor"  CssClass ="button" />
                           </td>
                       </tr>               
                </table>
                 <hr />
                <table id="Tbl_Grid_GradeDescriptor" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr runat="server">
                           <td colspan="3" align="center" style="width: 338px" runat="server">
                              <asp:GridView ID="Grd_GradeLst_GradeDescriptor" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                  CssClass="table table-bordered table-row" 
                                  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." Width="100%" >
                                  <rowstyle CssClass="griditem"  />
                                <columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign ="Center" HeaderStyle-HorizontalAlign ="Center"   HeaderStyle-Width ="70px" HeaderText =" Slno.">
                                    <ItemTemplate>
                                        <asp:Label runat="server"  id="lblSrno_GradeDescriptor" Text ='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrh_Id_GradeDescriptor" runat="server" Text='<%# Bind("GRH_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade Grid ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrh_GrdId_GradeDescriptor" runat="server" Text='<%# Bind("GRH_GRD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade Header 1" HeaderStyle-HorizontalAlign ="left" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrh_Header1_GradeDescriptor" runat="server" width="300px" text='<%# Bind("GRH_HEADER1")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade Header 2" HeaderStyle-HorizontalAlign ="left" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrh_Header2_GradeDescriptor" runat="server" width="300px" text='<%# Bind("GRH_HEADER1")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:ButtonField CommandName="Edit_GradeDescriptor" Text="Edit" 
                                          HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                       </asp:ButtonField>

<%--                                    <asp:ButtonField CommandName="view" HeaderText="" Text="View"  HeaderStyle-Width ="30px" >
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:ButtonField>--%>
                                </columns>
                              <alternatingrowstyle CssClass="griditem_alternative" />
                          </asp:GridView>
                       </td>
                    </tr>
                </table>
             </ContentTemplate>
         </ajaxToolkit:TabPanel>



                <%--Tab # 4 :Absense Settings--%>
                 <ajaxToolkit:TabPanel ID="Tpnl_Absentee" runat="server" HeaderText="Absentee Settings">
                    <ContentTemplate>      
                        <table id="tbl_Header_Absentee" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Absentee Settings</td>
                            </tr>
                        </table> 
                          <br />   
                        <table id="Tbl_details_Absentee" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr runat="server">
                                <td align="left" width="21.5%" runat="server">
                                   <span class="field-label"> Select&nbsp; Academic Year </span>
                                </td>
                                <td width="20%" runat="server" align="left">
                                    <asp:DropDownList id="DrpAcID_AbsenteeSettings" runat="server"  AutoPostBack="True" >
                                    </asp:DropDownList>
                                </td>
                                 <td colspan ="0" align ="left" runat="server">
                                </td>
                            </tr>
                         </table>

                         <table id="Tbl_details1_Absentee" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                             <tr>
                                 <td runat="server" > 
                                     <span class="field-label">Absent Description : </span>
                                 </td>
                                 <td>
                                     <asp:TextBox runat="server" ID ="txtAbsentEntry_AbsenteeSettings" MaxLength ="20"></asp:TextBox>
                                 </td>
                                 <td align ="left" runat="server">
                                </td>
                                 <td>
                                     <span class="field-label">Present Description : </span>
                                 </td>
                                 <td>
                                     <asp:TextBox runat="server" ID ="txtPresentEntry_AbsenteeSettings" MaxLength ="20"></asp:TextBox>
                                 </td>
                                 <td>
                                     <asp:Button runat="server" ID ="btnAddUpdate_AbsenteeSettings"  Text ="Add New" CssClass="button" />
                                 </td>
                             </tr>
                             </table>
                                         <hr />
                        <table id="Tbl_details2_Absentee" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                <asp:GridView ID="Grd_AdsenteeSettings" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                  CssClass="table table-bordered table-row" 
                                  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." Width="100%" >
                                  <rowstyle CssClass="griditem"  />
                                    <columns>
                                     <asp:TemplateField HeaderText =" Slno." HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                         <ItemTemplate>
                                             <asp:Label runat="server"  id="lblSrno" Text ='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                         </ItemTemplate>
                                             <HeaderStyle Width="70px" />
                                         <ItemStyle HorizontalAlign="Center" />
                                     </asp:TemplateField>
                                    <asp:TemplateField HeaderText ="Rat ID" Visible ="false" HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                        <ItemTemplate>
                                            <asp:Label runat="server"  id="lblRatAbsent_AbsenteeSettings" Text ='<%# Bind("RAT_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText ="Absent Description" HeaderStyle-HorizontalAlign ="Left" ItemStyle-HorizontalAlign ="Left">
                                        <ItemTemplate>
                                            <asp:Label runat="server"  id="lblRatAbsent_AbsenteeSettings" Text ='<%# Bind("RAT_ABSENT")%>'></asp:Label>
                                        </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText ="Present Description" HeaderStyle-HorizontalAlign ="Left" ItemStyle-HorizontalAlign ="Left">
                                        <ItemTemplate>
                                            <asp:Label runat="server"  id="lblRatPrsent_AbsenteeSettings" Text ='<%# Bind("RAT_APRLEAVE")%>'></asp:Label>
                                        </ItemTemplate>
                                     </asp:TemplateField>

                                     <asp:ButtonField CommandName="Edit_AbsenteeSettings" Text="Edit" 
                                          HeaderStyle-HorizontalAlign ="center" ItemStyle-HorizontalAlign ="center">
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                       </asp:ButtonField>
                                    </columns>
                                </asp:GridView>
                                </td>
                            </tr>
                        </table>
                      </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <%--Tab # 4 :Rule Deletion--%>
                <ajaxToolkit:TabPanel ID="Tpal_RuleDeletion" runat="server" HeaderText=" Rule Deletion  ">
                    <ContentTemplate>  
                            <table id="Tbl_Header_RuleDeletion" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Rule Deletion</td>
                            </tr>
                      </table>  
                          <table id="Tbl_details_RuleDeletion" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                             <tr>
                        <td align="center" style="font-weight: bold; height: 215px" valign="top">

                            <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlruledeleteAcdYr" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Term</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span></td>

                                    <td align="left" width="30%">
                                        
                                                    <asp:TextBox ID="txtSubject" Visible="false" runat="server" Enabled="False" MaxLength="100" TabIndex="2"></asp:TextBox>
                                               
                                                    <asp:ImageButton ID="imgSub" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getSubject();" TabIndex="18"></asp:ImageButton>
                                                    <asp:DropDownList ID="ddlSubjects" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:LinkButton Visible="false" ID="lnkClear" runat="server" OnClientClick="return clearSubject();">(Clear)</asp:LinkButton></td>
                                           
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="height: 16px" align="left">
                                        <asp:LinkButton ID="lnkAddNew" runat="server" Visible="false">Add New</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRrmId" runat="server" Text='<%# Bind("RRM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Rule">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblRule" runat="server" Text='<%# Bind("RRM_DESCR") %>'></asp:LinkButton>


                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Report Card">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Report Schedule">
                                                    <HeaderTemplate>
                                                       Report Schedule<br />
                                                                    <asp:DropDownList ID="ddlgvSchedule" runat="server" AutoPostBack="True" 
                                                                        OnSelectedIndexChanged="ddlgvSchedule_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrinted" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Header">
                                                    <HeaderTemplate>
                                                       Header<br />
                                                                    <asp:DropDownList ID="ddlgvHeader" runat="server" AutoPostBack="True" 
                                                                        OnSelectedIndexChanged="ddlgvHeader_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeader" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Term">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTerm" runat="server" Text='<%# Bind("TRM_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Exam Level">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("RRM_TYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:Button ID="btnDelete" OnClientClick="javascipt:return confirm_remove();" runat="server" Text="Delete" CssClass="button" TabIndex="4" /></td>
                                </tr>

                            </table>
                            <input id="Hidden1" runat="server" type="hidden" value="=" /><input id="Hidden2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                            <asp:HiddenField ID="hfSBG_ID" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfSubjectURL" runat="server" />
                            &nbsp;
                         
                        </td>
                    </tr>
                           </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <%--Tab # 5 :Subject Copy--%>
                <ajaxToolkit:TabPanel ID="Tpal_SubjectDeletion" runat="server" HeaderText=" Subject Copy  ">
                    <ContentTemplate>  
                            <table id="tbl_header_SubDeletion" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Subject Deletion</td>
                            </tr>
                      </table>  
                          <table id="tbl_details_SubDeletion" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                           <%--   <tr>
                                  <td>
                                        Details here

                                  </td>
                              </tr>--%>

                              <tr runat="server">
                          <td runat="server" > 
                              <span class="field-label">Select  Academic Year*&nbsp;(&nbsp;From&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                              <asp:DropDownList ID="DrpAcID_From_Subjectcopy" SkinID="smallcmb" runat="server" AutoPostBack="true" ></asp:DropDownList>
                          </td>
                          <td runat="server">
                            <span class="field-label"> Select  Academic Year*&nbsp;(&nbsp;To&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                            <asp:DropDownList ID="DrpAcID_To_Subjectcopy" runat="server" AutoPostBack="true" SkinID="smallcmb" >
                            </asp:DropDownList>
                         </td>
                         <td  runat="server" align ="right">
                            <asp:Button ID="btnShow_Subjectcopy" runat="server" CssClass="button" Text="&nbsp;&nbsp;&nbsp;&nbsp;Copy&nbsp;&nbsp;&nbsp;&nbsp;"/>
                         </td>
                      </tr>
                              
                             
                           </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>


                <%--Tab # 6 :Options Copy--%>
                <ajaxToolkit:TabPanel ID="TabPanel7" runat="server" HeaderText=" Option Copy  ">
                    <ContentTemplate>  
                            <table id="tbl_header_OptionCopy" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Option Copy</td>
                            </tr>
                      </table>  
                          <table id="tbl_details_OptionCopy" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr runat="server">
                          <td runat="server" > 
                              <span class="field-label">Select  Academic Year*&nbsp;(&nbsp;From&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                              <asp:DropDownList ID="ddloptionscopyfromYR" SkinID="smallcmb" runat="server" AutoPostBack="true" ></asp:DropDownList>
                          </td>
                          <td runat="server">
                            <span class="field-label"> Select  Academic Year*&nbsp;(&nbsp;To&nbsp;)</span>
                          </td>
                          <td width="20%" runat="server">
                            <asp:DropDownList ID="ddloptionscopyToYR" runat="server" AutoPostBack="true" SkinID="smallcmb" >
                            </asp:DropDownList>
                         </td>
                         <td  runat="server" align ="right">
                            <asp:Button ID="btncopy" runat="server" CssClass="button" Text="&nbsp;&nbsp;&nbsp;&nbsp;Copy&nbsp;&nbsp;&nbsp;&nbsp;"/>
                         </td>
                      </tr>
                              
                             <tr >
                                 
                                 <td>
                                  <div id="Div1" runat="server" class="checkbox-list"  >
                                      <asp:CheckBoxList ID="chklist_gradefrom" runat="server" DataTextField="GRM_GRD_ID"></asp:CheckBoxList>
                                   </div>
                                     </td>
                                 </tr>
                           </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <%--Tab # 7 :Un published Reportsy--%>
                <ajaxToolkit:TabPanel ID="Tpnl_UnpublisedRpts" runat="server" HeaderText=" Unpublished Reports  ">
                   <ContentTemplate>  
                            <table id="tbl_header_UnpublishedRpt" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Un published Reports</td>
                            </tr>
                      </table>  
                          <table id="tbl_details_UnpublishedRpt" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                   <tr align="left">
                        <td><asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="False"
                             SkinID="error" Style="text-align: center"  ></asp:Label></td>
                    </tr>




                    <tr align="left">
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%"  >

                                <tr>
                                    <td align="left" ><span class="field-label">Academic Year</span></td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlunpubliAcadyr" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Select Report Card</span></td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="left" ><span class="field-label">Select Report Schedule</span></td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                     <td align="left" ><span class="field-label">Select Term</span></td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlUnpubliTerm" runat="server"  >
                                        </asp:DropDownList></td>
                                </tr>
                                
                                <tr>
                                    <td align="left" ><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlunpubliGrade" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Release Date</span></td>
                                    
                                    <td align="left" >
                                        <asp:TextBox runat="server" ID="txtRelease">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgRelease" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />&nbsp;
                            </td>
                                </tr>
                                <tr><td colspan="4" align="center"><asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply to All"   /></td></tr>
                                <tr>

                                    <td align="center"  colspan="4">

                                        <asp:GridView ID="gvGrade" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row"  >
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>


                                                <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# BIND("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Publish" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        Publish <br />
                                                                    <asp:CheckBox ID="chkAll2" runat="server" onclick="javascript:change_chk_state2(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                            
                                                    </HeaderTemplate>

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkPublish" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkPublish" onclick="javascript:highlight1(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Release Online" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        Release <br />
                                                                    <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_state1(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />

                                                    </HeaderTemplate>

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkRelease" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRelease" onclick="javascript:highlight1(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Release Date">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        
                                                                    <asp:TextBox ID="txtDate" runat="server" CausesValidation="true" ></asp:TextBox>
                                                                    <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                                                    <asp:Label ID="lblErr" runat="server" Text="*" ForeColor="RED" Visible="false"></asp:Label>
                                                                
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" PopupButtonID="imgDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15%" />
                                                </asp:TemplateField>


                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View Students" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>

                                            <HeaderStyle CssClass="gridheader_pop"  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />



                                        </asp:GridView>



                                    </td>
                                </tr>





                                <tr>
                                    <td align="center"  colspan="5">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"   OnClick="btnSave_Click" />&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4"
                            valign="middle">
                            <asp:HiddenField ID="hfHeaderCount" runat="server"></asp:HiddenField>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRelease" PopupPosition="BottomLeft" TargetControlID="txtRelease">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="Hidden3"
                        runat="server" type="hidden" value="=" /><input id="Hidden4" runat="server"
                            type="hidden" value="=" />

                        </td>
                    </tr>
                           </table>
                    </ContentTemplate>

                </ajaxToolkit:TabPanel>
         <%--Tab # 8 :Report Design Linking--%>
       <ajaxToolkit:TabPanel ID="Tp_Rpt_Dsg_LNK" runat="server" HeaderText=" Report Design Linking  ">
                    <ContentTemplate>  
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="gridheader_pop" runat="server" >
                                <td align="left" colspan="4" valign="middle" class="title-bg" runat="server">Report Design Linking</td>
                            </tr>
                      </table>  
                         <br /> 
                        <table id="Table6" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            
                              <tr runat="server" colspan="4">
                                  <td runat="server"  width="20%">
                                    <span class="field-label">Select Academic Year*</span> 
                                  </td>
                                  <td runat="server" width="30%">
                                      <asp:DropDownList ID="ddlacd_rptDesignlinking" runat="server" AutoPostBack="True" 
                                           SkinID="smallcmb" width="20%">    <%--OnSelectedIndexChanged="DrpAcID_RptLinking_SelectedIndexChanged"--%>
                                      </asp:DropDownList>
                                  </td>
                                  <td width="20%"></td>
                                  <td width="30%"></td>
                              </tr>
                            </table>
                           <hr />
                          <table id="Table5" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr runat="server">
                           <td runat="server" width="20%">
                                   <span class="field-label">Report&nbsp;Name&nbsp;* </span>
                               </td>
                               <td runat="server" width ="30%">
                                   <asp:DropDownList id="ddlRptDLnk_RPTNAME" runat="server"  AppendDataBoundItems="True"  AutoPostBack="True"  DataValueField ="RSM_ID"></asp:DropDownList>
                               </td>
                          <td runat="server" width="20%">
                            <span class="field-label"> Design Name</span>
                          </td>
                          <td width="30%" runat="server">
                            <asp:DropDownList ID="ddlRptDLnk_DSGNAME" runat="server" AutoPostBack="true" SkinID="smallcmb" >
                            </asp:DropDownList>
                         </td>
                         
                      </tr>
                              
                             <tr>
                                 
                                 <td colspan="4" class="text-center">
                                <asp:Button ID="btnRPTDSGLNK_SAVE" runat="server" CssClass="button" Text="Save"/>
                                     </td>
                                 </tr>
                           </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

    </ajaxToolkit:TabContainer>
                    </td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
