Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security

Partial Class Curriculum_rptStudentsMarks
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Private NVCStudentPage As NameValueCollection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Session.Timeout = 60

        If Page.IsPostBack = False Then

            Session("StuMarks") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                lbtnKeys.Visible = False
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330333" And ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then

                        btnSave.Enabled = False

                        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

                        lblGroup.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                        lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        lblSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                        H_ACD_ID.Value = Session("CURRENT_ACD_ID")
                        H_GRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("GRDID").Replace(" ", "+"))
                        H_GRP_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grpid").Replace(" ", "+"))
                        H_SBJ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))


                        If ViewState("MainMnu_code") = "C320001" Then
                            BindReportCard()
                            ddlReportcard.Visible = True
                            lblReport.Visible = False
                        Else
                            lblReport.Text = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
                            GetReportID()
                            ddlReportcard.Visible = False
                            lblReport.Visible = True
                        End If
                        BindCategory()

                        If GetReportHeader().Length >= 2 Then
                            GetStudentsList() ' This Function will Execute only one time in a group
                            BindStudents()
                            Session("SBGID") = H_SBJ_ID.Value
                            Session("Grade") = H_GRD_ID.Value
                            btnSave.Enabled = True

                            GetHeaderKeys()
                        Else
                            lblError.Text = "Mark entry columns are not available! "
                        End If
                    End If

                    gvStudents.Attributes.Add("bordercolor", "#1b80b6")
                    'Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBT_ID,SBT_DESCR FROM SUBJECT_REPORT_CATEGORY WHERE " _
                               & " SBT_SBG_ID=" + H_SBJ_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "SBT_DESCR"
        ddlCategory.DataValueField = "SBT_ID"
        ddlCategory.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)

        If ddlCategory.Items.Count = 1 Then
            trCat.Visible = False
        Else
            trCat.Visible = True
        End If

    End Sub
#Region "Private Functions"

    Sub GetReportID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_ID,RPF_ID,RSM_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                            & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON A.RSM_ID=B.RSG_RSM_ID" _
                            & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON A.RSM_ID=C.RPF_RSM_ID" _
                            & " WHERE RSM_ACD_ID=" + H_ACD_ID.Value + " AND RSG_GRD_ID='" + H_GRD_ID.Value + "'" _
                            & " AND RPF_DESCR='" + lblReport.Text + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                H_RSM_ID.Value = .Item(0)
                H_RPF_ID.Value = .Item(1)
                H_RSM_DESCR.Value = .Item(2)
            End With
        Next

    End Sub

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CONVERT(VARCHAR(100),RSM_ID)+'|'+CONVERT(VARCHAR(100),RPF_ID)+'|'+RSM_DESCR AS RPF_ID,RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                                  & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON A.RSM_ID=B.RSG_RSM_ID" _
                                  & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON A.RSM_ID=C.RPF_RSM_ID" _
                                  & " WHERE RSM_ACD_ID=" + H_ACD_ID.Value + " AND RSG_GRD_ID='" + H_GRD_ID.Value + "'" _
                                  & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportcard.DataSource = ds
        ddlReportcard.DataTextField = "RPF_DESCR"
        ddlReportcard.DataValueField = "RPF_ID"
        ddlReportcard.DataBind()

        Dim strRpf As String() = ddlReportcard.SelectedValue.Split("|")
        H_RSM_ID.Value = strRpf(0)
        H_RPF_ID.Value = strRpf(1)
        H_RSM_DESCR.Value = strRpf(2)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function AddRecord()
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        Dim GvHeader As NameValueCollection
        Dim txtCtrl As TextBox
        Dim strVArray() As String
        Dim strColArray() As String
        Dim IntRow As Integer = 0
        Dim IntCols As Integer = 0
        Try
            If ViewState("datamode") = "add" And Not Session("ReportHeader") Is Nothing Then ' Session("ReportHeader")
                GvHeader = Session("ReportHeader")
                'To keep the Item Values In an Array
                strVArray = H_CommentCOLS.Value.Split("|")

                For Each gvRow As GridViewRow In gvStudents.Rows
                    strColArray = strVArray(IntRow).Split(",")
                    IntRow = IntRow + 1
                    If CheckArray(strColArray) = True Then
                        For IntCol As Integer = 4 To gvStudents.Columns.Count - 1
                            If strColArray(IntCol - 4).ToString <> "" Then
                                ldrNew = Session("StuMarks").NewRow
                                ldrNew("Id") = gvStudents.Rows.Count + 1
                                ldrNew("RSD_ID") = H_SETUP.Value + GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(0)
                                ldrNew("AcdId") = H_ACD_ID.Value
                                ldrNew("AcdYear") = H_ACD_ID.Value
                                ldrNew("GrdId") = H_GRD_ID.Value
                                ldrNew("Grade") = lblGrade.Text
                                ldrNew("RPfID") = H_RPF_ID.Value + GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(1)
                                'ldrNew("Report") = txtReportId.Text
                                ldrNew("GrdSbjId") = H_SBJ_GRD_ID.Value
                                ldrNew("GrdSbj") = lblSubject.Text
                                ldrNew("GroupId") = H_GRP_ID.Value
                                ldrNew("Group") = lblGroup.Text
                                ldrNew("TypeLevel") = "NORMAL"
                                ldrNew("SBJID") = H_SBJ_ID.Value
                                ldrNew("Subject") = lblSubject.Text
                                ldrNew("RptSchId") = H_RPF_ID.Value
                                ldrNew("rptSchedule") = lblReport.Text
                                ldrNew("studId") = gvRow.Cells(1).Text
                                ldrNew("StudName") = gvRow.Cells(2).Text

                                'To assign the Values Baed on the Setup entered
                                If GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) <> "" Then
                                    If GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) = "C" Then
                                        ldrNew("Comments") = strColArray(IntCol - 4).ToString.Replace("~", ",")
                                    ElseIf GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) = "G" Then
                                        ldrNew("MGrade") = strColArray(IntCol - 4).ToString
                                    ElseIf GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) = "D" Then
                                        ldrNew("Comments") = strColArray(IntCol - 4).ToString
                                    Else
                                        ldrNew("Mark") = strColArray(IntCol - 4).ToString
                                    End If
                                End If
                                'End the Category wiase Value Setup

                                Session("StuMarks").Rows.Add(ldrNew)
                            Else
                                'To Delete The Single Col Value, If its Already Entred and Now its Blank Then
                                DeleteHeaderComments(gvRow.Cells(1).Text, H_SBJ_ID.Value, H_ACD_ID.Value, H_GRD_ID.Value, H_RPF_ID.Value, _
                                                     GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(0))
                            End If
                        Next
                        txtCtrl = gvRow.FindControl("txtEffort")
                    Else
                        'To Delete all the Values for a student, If Its Already Entered and Now Its Blank Then.....
                        DeleteStudentComments(gvRow.Cells(1).Text, H_SBJ_ID.Value, H_ACD_ID.Value, H_GRD_ID.Value, H_RPF_ID.Value)
                        'Deleted the Blank Values If Already Entred....
                    End If
                Next
            End If

            If ViewState("datamode") = "edit" Then
                If Not Session("StuMarks") Is Nothing Then
                    For lintIndex = 0 To Session("StuMarks").Rows.Count - 1
                        If (Session("StuMarks").Rows(lintIndex)("ID") = Session("gintGridLine")) Then

                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'Session("SaveTrue") = "Request could not be processed"
        End Try

    End Function

    Private Function DeleteStudentComments(ByVal STUID As Integer, ByVal SBG_ID As Integer, ByVal ACD_ID As Integer, ByVal GRDID As String, ByVal RPFID As Integer)
        Try
            'Dim strSQL As String
            'strSQL = "DELETE FROM RPT.REPORT_STUDENT_S WHERE RST_STU_ID=" & STUID & " " & _
            '         " AND RST_SBG_ID=" & SBG_ID & " AND RST_ACD_ID=" & ACD_ID & " AND RST_GRD_ID='" & GRDID & "' AND RST_RPF_ID=" & RPFID & " "

            'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            'Dim IntResult As Integer
            'IntResult = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSQL)
            Dim str_query As String = "exec [RPT].[DELETE_REPORT_STUDENT_S] " _
                                                       & "@RST_RPF_ID=" + RPFID.ToString + "," _
                                                       & "@RST_ACD_ID=" + ACD_ID.ToString + "," _
                                                       & "@RST_GRD_ID='" + GRDID.ToString + "'," _
                                                       & "@RST_STU_ID=" + STUID.ToString + "," _
                                                       & "@RST_SBG_ID=" + SBG_ID.ToString + ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Function

    Private Function DeleteHeaderComments(ByVal STUID As Integer, ByVal SBG_ID As Integer, ByVal ACD_ID As Integer, ByVal GRDID As String, ByVal RPFID As Integer, ByVal RSDID As Integer)
        Try
            'Dim strSQL As String
            'strSQL = "DELETE FROM RPT.REPORT_STUDENT_S WHERE RST_STU_ID=" & STUID & " " & _
            '         " AND RST_SBG_ID=" & SBG_ID & " AND RST_ACD_ID=" & ACD_ID & " AND RST_GRD_ID='" & GRDID & "' AND RST_RPF_ID=" & RPFID & " AND RST_RSD_ID=" & RSDID & ""

            'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            'Dim IntResult As Integer
            'IntResult = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSQL)


            Dim str_query As String = "exec [RPT].[DELETE_REPORT_STUDENT_S] " _
                                                      & "@RST_RPF_ID=" + RPFID.ToString + "," _
                                                      & "@RST_ACD_ID=" + ACD_ID.ToString + "," _
                                                      & "@RST_GRD_ID='" + GRDID.ToString + "'," _
                                                      & "@RST_STU_ID=" + STUID.ToString + "," _
                                                      & "@RST_SBG_ID=" + SBG_ID.ToString + ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Function

    Private Function CheckArray(ByVal StrArr() As String) As Boolean
        Dim blnReturn As Boolean = False
        For Each Str As String In StrArr
            If Str <> "" And Str <> "--" Then
                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function

    Private Sub SaveRecord()

        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand
        Dim stTrans As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            objConn.Open()
            stTrans = objConn.BeginTransaction

            For iIndex = 0 To Session("StuMarks").Rows.Count - 1
                If IsDBNull(Session("StuMarks").Rows(iIndex)("studId")) = False Then
                    If CStr(Session("StuMarks").Rows(iIndex)("RSD_ID")) <> "-1" Then

                        cmd = New SqlCommand("[RPT].[SaveREPORT_STUDENT_S]", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        With Session("StuMarks")
                            If ViewState("datamode") = "add" Then
                                cmd.Parameters.AddWithValue("@RST_ID", 0)
                            End If
                            If ViewState("datamode") = "edit" Then
                                cmd.Parameters.AddWithValue("@RST_ID", .Rows(iIndex)("id"))
                            End If

                            cmd.Parameters.AddWithValue("@RST_RPF_ID", .Rows(iIndex)("RptSchId"))
                            cmd.Parameters.AddWithValue("@RST_RSD_ID", .Rows(iIndex)("RSD_ID")) '-- Report Setup
                            cmd.Parameters.AddWithValue("@RST_ACD_ID", .Rows(iIndex)("AcdId"))
                            cmd.Parameters.AddWithValue("@RST_GRD_ID", .Rows(iIndex)("GrdId"))
                            cmd.Parameters.AddWithValue("@RST_STU_ID", .Rows(iIndex)("studId"))
                            cmd.Parameters.AddWithValue("@RST_RSS_ID", .Rows(iIndex)("RptSchId"))
                            cmd.Parameters.AddWithValue("@RST_SGR_ID", .Rows(iIndex)("GrdSbjId"))
                            cmd.Parameters.AddWithValue("@RST_SBG_ID", .Rows(iIndex)("SBJID"))
                            cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", .Rows(iIndex)("TypeLevel"))
                            If IsDBNull(.Rows(iIndex)("Mark")) = True Then
                                .Rows(iIndex)("Mark") = "0"
                            Else
                                If IsNumeric(.Rows(iIndex)("Mark")) = False Then
                                    .Rows(iIndex)("Mark") = "0"
                                End If
                            End If
                            cmd.Parameters.AddWithValue("@RST_MARK", .Rows(iIndex)("Mark"))
                            cmd.Parameters.AddWithValue("@RST_COMMENTS", .Rows(iIndex)("Comments"))
                            cmd.Parameters.AddWithValue("@RST_GRADING", .Rows(iIndex)("MGrade"))
                            cmd.Parameters.AddWithValue("@RST_USER", Session("sUsr_name"))
                        End With
                        If ViewState("datamode") = "add" Then
                            cmd.Parameters.AddWithValue("@bEdit", 0)
                        End If
                        If ViewState("datamode") = "edit" Then
                            cmd.Parameters.AddWithValue("@bEdit", 1)
                        End If
                        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        cmd.ExecuteNonQuery()
                        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                    End If
                End If
            Next
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                Session("StuMarks").rows.clear()
                Exit Sub
            End If
            stTrans.Commit()
            Session("StuMarks").rows.clear()
            lblError.Text = "Successfully Saved"
            'Session("SaveTrue") = "Successfully Saved"
        Catch ex As Exception
            stTrans.Rollback()
            Session("StuMarks").rows.clear()
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Function GetStudentMarks(ByVal STUID As String, ByVal ColHeader As String) As DataSet
        Try

            Dim strSQL As String
            'strSQL = "SELECT RST_ID,RST_RSD_ID,RST_RPF_ID,RSD_RESULT,RST_SBG_ID,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER ,RST_MARK,RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
            '         " FROM RPT.REPORT_STUDENT_S S " & _
            '         " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
            '         " WHERE RST_STU_ID =" & STUID & " And RST_RPF_ID =" &H_RPF_ID.value & " And RST_ACD_ID = " & H_ACD_ID.value & " " & _
            '         " AND RST_SBG_ID=" & H_SBJ_ID.Value & " " & _
            '         " AND RSD_RSM_ID= " & H_RSM_ID.Value & " AND D.RSD_HEADER='" & ColHeader & "' ORDER BY RST_ID"

            strSQL = "SELECT RST_ID,RST_RSD_ID,RST_RPF_ID,RSD_RESULT,RST_SBG_ID,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER ,RST_MARK,RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
                  " FROM RPT.REPORT_STUDENT_S S " & _
                  " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
                  " WHERE RST_STU_ID =" & STUID & " And RST_RPF_ID =" & H_RPF_ID.Value & " And RST_ACD_ID = " & H_ACD_ID.Value & " " & _
                  " AND RST_SBG_ID=" & H_SBJ_ID.Value & " " & _
                  " AND RSD_RSM_ID= " & H_RSM_ID.Value & " AND D.RSD_bDIRECTENTRY='TRUE'" & _
                  " AND D.RSD_bALLSUBJECTS='TRUE' AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID='" + H_SBJ_ID.Value + "') " 'ORDER BY RST_ID"

            If Session("SBSUID") <> "125017" And Session("SBSUID") <> "114003" Then
                strSQL += " union all "

                If H_RSM_DESCR.Value.Contains("FINAL REPORT") = True Then
                    strSQL += "SELECT RST_ID,-1 AS RST_RSD_ID,RST_RPF_ID,'L' AS RSD_RESULT,RST_SBG_ID,ISNULL(RPF_SHORTNAME,REPLACE(SUBSTRING(RPF_DESCR,1,3),'.','')) AS RSD_HEADER ,RST_MARK,RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
                 " FROM RPT.REPORT_STUDENT_S S " & _
                 " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
                 " INNER JOIN RPT.REPORT_SETUP_M AS E ON RSD_RSM_ID=RSM_ID AND RSM_ACD_ID=" + H_ACD_ID.Value.ToString & _
                 " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSM_ID=RPF_RSM_ID AND RST_RPF_ID=RPF_ID " & _
                 " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID AND RSG_GRD_ID='" + H_GRD_ID.Value.ToString + "'" & _
                 " WHERE RST_STU_ID =" & STUID & "  And RST_ACD_ID = " & H_ACD_ID.Value & " " & _
                 " AND RST_SBG_ID=" & H_SBJ_ID.Value & " " & _
                 " AND RSD_bPERFORMANCE_INDICATOR=1 " & _
                 " AND RPF_DISPLAYORDER<(SELECT RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_ID=" + H_RPF_ID.Value.ToString + ")"
                Else
                    strSQL += "SELECT RST_ID,-1 AS RST_RSD_ID,RST_RPF_ID,'L' AS RSD_RESULT,RST_SBG_ID,ISNULL(RPF_SHORTNAME,REPLACE(SUBSTRING(RPF_DESCR,1,3),'.','')) AS RSD_HEADER ,RST_MARK,RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
                 " FROM RPT.REPORT_STUDENT_S S " & _
                 " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
                 " INNER JOIN RPT.REPORT_SETUP_M AS E ON RSD_RSM_ID=RSM_ID " & _
                 " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSM_ID=RPF_RSM_ID AND RST_RPF_ID=RPF_ID " & _
                 " WHERE RST_STU_ID =" & STUID & "  And RST_ACD_ID = " & H_ACD_ID.Value & " " & _
                 " AND RST_SBG_ID=" & H_SBJ_ID.Value & " " & _
                 " AND RSD_RSM_ID= " & H_RSM_ID.Value & _
                 " AND RSD_bPERFORMANCE_INDICATOR=1 " & _
                 " AND RPF_DISPLAYORDER<(SELECT RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_ID=" + H_RPF_ID.Value.ToString + ")"
                End If
            End If

            If Session("SBSUID") = "123016" And lblReport.Text.Contains("STRATEGIES") = True Then
                strSQL += "union all SELECT RST_ID,-1 AS RST_RSD_ID,RST_RPF_ID,'L' AS RSD_RESULT,RST_SBG_ID,RSD_HEADER ,RST_MARK,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
                                            " FROM RPT.REPORT_STUDENT_S S " & _
                                            " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
                                            " INNER JOIN RPT.REPORT_SETUP_M AS E ON RSD_RSM_ID=RSM_ID " & _
                                            " WHERE RST_STU_ID =" & STUID & "  And RST_ACD_ID = " & H_ACD_ID.Value & " " & _
                                            " AND RST_SBG_ID=" & H_SBJ_ID.Value & _
                                            "  And RSM_ACD_ID = " & H_ACD_ID.Value & " " & _
                                            " AND RSD_HEADER IN('Target Grade Term2','Achieved Grade','Target Grade Next Year') "
            ElseIf Session("SBSUID") = "123016" Then
                strSQL += "union all SELECT RST_ID,-1 AS RST_RSD_ID,RST_RPF_ID,'L' AS RSD_RESULT,RST_SBG_ID,RSD_HEADER ,RST_MARK,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,RST_GRADING,RST_RRM_ID " & _
               " FROM RPT.REPORT_STUDENT_S S " & _
               " INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID " & _
               " INNER JOIN RPT.REPORT_SETUP_M AS E ON RSD_RSM_ID=RSM_ID " & _
               " WHERE RST_STU_ID =" & STUID & "  And RST_ACD_ID = " & H_ACD_ID.Value & " " & _
               " AND RSD_RSM_ID= " & H_RSM_ID.Value & _
               " AND RST_RPF_ID=" & H_RPF_ID.Value.ToString & _
               " AND RST_SBG_ID=" & H_SBJ_ID.Value & _
               " AND RSD_BBENCHMARK=1 "

            End If

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet

            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

            Return dsHeader

        Catch ex As Exception

        End Try
    End Function

    Private Function PopulateReports(ByVal ddlReports As DropDownList, ByVal ddlACDYear As DropDownList)
        ddlReports.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & ddlACDYear.SelectedValue

        If Session("sbsuid") = "121013" Then
            str_Sql += " AND RSM_DESCR='BROWN BOOK KG1-KG2'"
        End If

        str_Sql += " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReports.DataSource = ds
        ddlReports.DataTextField = "RSM_DESCR"
        ddlReports.DataValueField = "RSM_ID"
        ddlReports.DataBind()

        Return ddlReports
    End Function

    Private Function PopulateReportSchedule(ByVal ddlReportSChedule As DropDownList, ByVal ddlReportsM As DropDownList)
        ddlReportSChedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_Sql As String = "SELECT RPF_ID ,RPF_ID ,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M "
        If ddlReportsM.SelectedValue <> "" Then
            str_Sql += " WHERE RPF_RSM_ID=" & ddlReportsM.SelectedValue & ""
        End If
        str_Sql += " ORDER BY RPF_DISPLAYORDER "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReportSChedule.DataSource = ds
        ddlReportSChedule.DataTextField = "RPF_DESCR"
        ddlReportSChedule.DataValueField = "RPF_ID"
        ddlReportSChedule.DataBind()

        Return ddlReportSChedule
    End Function

    Private Function PopulateGrades(ByVal ddlGrades As DropDownList, ByVal ddlReportsM As DropDownList)
        ddlGrades.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""

        If ddlReportsM.SelectedValue <> "" Then

            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
                str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER,GRM_DISPLAY " _
                              & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
                              & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
                              & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSG_GRD_ID=GRD_ID AND RSG_RSM_ID='" + ddlReportsM.SelectedValue & " ' " _
                              & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & Session("Current_ACD_ID") & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' AND GRM_ACD_ID=" + H_ACD_ID.Value.ToString _
                              & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            Else
                str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_BSU_M.GRM_DISPLAY,GRD_DISPLAYORDER  " _
                        & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                        & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                        & " INNER JOIN VW_GRADE_BSU_M ON GRD_ID=GRM_GRD_ID AND GRM_ACD_ID=" + H_ACD_ID.Value.ToString _
                        & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & ddlReportsM.SelectedValue & "' "

                If ViewState("GRD_ACCESS") > 0 Then
                    str_Sql += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                             & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
                End If
                str_Sql += " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            End If

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrades.DataSource = ds
        ddlGrades.DataTextField = "GRM_DISPLAY"
        ddlGrades.DataValueField = "ID"
        ddlGrades.DataBind()

        Return ddlGrades
    End Function

    Private Function BindStudents()
        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        gvStudents.DataSourceID = ""
        Dim tmpClmCount As Integer = 0
        Dim GvHeader As NameValueCollection
        Dim strCondition As String = ""
        Dim strStuIDs As String = ""
        Dim cArr As New ArrayList
        Try

            If txtStudName.Text.Trim <> "" Then
                strCriteria += " AND STU_ID=" & H_STU_ID.Value & " "
            Else
                If Not ViewState("StuPages") Is Nothing Then
                    NVCStudentPage = ViewState("StuPages")
                    If Not NVCStudentPage.GetValues(ViewState("PageNo").ToString) Is Nothing Then
                        Dim x As String() = NVCStudentPage.GetValues(ViewState("PageNo").ToString)
                        For Each x1 As String In x
                            strStuIDs = strStuIDs + x1 + ","
                        Next
                        If strStuIDs.Contains(",") = True Then
                            strStuIDs = strStuIDs.Substring(0, Len(strStuIDs) - 1)
                        End If
                        strCriteria += " AND STU_ID IN (" & strStuIDs & ")"
                    End If
                End If
            End If


            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                strCondition += " INNER JOIN GROUPS_TEACHER_S ON SGS_SGR_ID=SGR_ID "
                strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & " "
            End If

            strSql = "SELECT DISTINCT(STU_NO),STU_ID,STU_NAME + ' '+ CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END as STU_NAME,SCT_DESCR AS Section, " & GetReportHeader() & " 'Test Values' COMMENTS " & _
                    " FROM VW_STUDENT_DETAILS " & _
                    " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " & _
                    " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                    " " & strCondition & " " & _
                    " WHERE STU_GRD_ID='" & H_GRD_ID.Value & "' AND STU_ACD_ID=" & H_ACD_ID.Value & " AND SGR_ID=" & H_GRP_ID.Value & " " & _
                    "" & strCriteria

            If Session("sbuid") = "125002" Then
                strSql += "  ORDER BY STU_NAME "
            Else
                strSql += "  ORDER BY SCT_DESCR,STU_NAME "
            End If


            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            GvHeader = Session("ReportHeader")

            If dsStudents.Tables(0).Rows.Count = 0 Then
                gvStudents.DataSource = dsStudents.Tables(0)
                dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
                gvStudents.DataBind()
                Dim columnCount As Integer = gvStudents.Columns.Count
                gvStudents.Rows(0).Cells.Clear()
                gvStudents.Rows(0).Cells.Add(New TableCell)
                gvStudents.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudents.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudents.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                Dim Col As DataControlField
                Dim clmCnt As Integer = 0
                'Dim cl As Integer = 0

                For Each Clm As DataColumn In dsStudents.Tables(0).Columns
                    If Clm.ColumnName <> "COMMENTS" Then
                        If clmCnt < 4 Then
                            Dim Field As New BoundField
                            Field.DataField = Clm.ColumnName
                            Field.HeaderText = Clm.ColumnName
                            Col = Field
                            gvStudents.Columns.Insert(clmCnt, Col)
                        ElseIf clmCnt >= 4 Then
                            tmpClmCount = tmpClmCount + 1
                            Dim TField As New TemplateField
                            Dim DTField As DynamicTemplate = New DynamicTemplate(ListItemType.Item)
                            Dim TxtItem As New TextBox()
                            Dim HField As DynamicTemplate = New DynamicTemplate(ListItemType.Item)

                            TxtItem.ID = "txt" + tmpClmCount.ToString 'Clm.ColumnName.ToString().Replace(" ", "_")

                            If GvHeader.Item(Clm.ColumnName).Split("_")(3) <> "" Then
                                TxtItem.CssClass = GvHeader.Item(Clm.ColumnName).Split("_")(3)
                                If TxtItem.CssClass = "textboxmulti" Then
                                    TxtItem.TextMode = TextBoxMode.MultiLine
                                End If
                                cArr.Add(TxtItem.ID)
                            End If
                            If GvHeader.Item(Clm.ColumnName).Split("_")(2) = "C" And TxtItem.CssClass = "textboxmulti" Then
                                'TxtItem.SkinID = "MultiText"
                                TxtItem.EnableTheming = False
                                TxtItem.Visible = True
                                TxtItem.Text = ""

                                '-----------------------------------------
                                Dim imgbtn As New ImageButton
                                imgbtn.ID = "img" + tmpClmCount.ToString
                                imgbtn.ImageUrl = "../Images/Comment.JPG"
                                imgbtn.Width = 20
                                imgbtn.Height = 20
                                imgbtn.ToolTip = "Press Alt + Z "
                                imgbtn.AccessKey = "Z"
                                DTField.AddControl(TxtItem, "Text", Clm.ColumnName)
                                DTField.AddControl(imgbtn, "ToolTip", Clm.ColumnName)
                                cArr.Add(TxtItem.ID)
                            ElseIf GvHeader.Item(Clm.ColumnName).Split("_")(2) = "D" Then
                                Dim ddlItem As New DropDownList
                                ddlItem = GetHeaderValues(ddlItem, GvHeader.Item(Clm.ColumnName).Split("_")(0))

                                If ddlItem.Items.Count > 1 Then
                                    ddlItem.ID = "txt" + tmpClmCount.ToString
                                    DTField.AddControl(ddlItem, "Text", Clm.ColumnName)

                                Else
                                    TxtItem.TextMode = TextBoxMode.SingleLine
                                    DTField.AddControl(TxtItem, "Text", Clm.ColumnName)
                                End If
                            Else
                                'TxtItem.CssClass = "inputboxnor" 'inputbox_multi
                                TxtItem.TextMode = TextBoxMode.SingleLine
                                TxtItem.Visible = True
                                TxtItem.Text = ""
                                '------------------------------------------
                                DTField.AddControl(TxtItem, "Text", Clm.ColumnName)
                                If GvHeader.Item(Clm.ColumnName).Split("_")(2) = "L" Then
                                    TxtItem.Width = 20
                                    TxtItem.Enabled = False
                                End If


                            End If


                            TField.ItemTemplate = DTField
                            TField.HeaderText = Clm.ColumnName
                            Dim tStyle As TableItemStyle

                            Dim lblText As New LinkButton
                            lblText.ID = "lbl" + tmpClmCount.ToString
                            lblText.Text = Clm.ColumnName

                            Dim lblText2 As New Label
                            lblText2.ID = "lblT" + tmpClmCount.ToString
                            lblText2.Text = GvHeader.Item(Clm.ColumnName).Split("_")(4)

                            HField.AddControl(lblText, "Text", Clm.ColumnName)
                            HField.AddControl(lblText2, "Text", Clm.ColumnName)
                            TField.HeaderTemplate = HField

                            gvStudents.Columns.Insert(clmCnt, TField)

                        End If
                        clmCnt = clmCnt + 1
                    End If

                Next




                H_CommentCOLS.Value = tmpClmCount
                Session("ColummnCnt") = tmpClmCount

                gvStudents.DataSource = dsStudents.Tables(0)
                gvStudents.DataBind()

                gvStudents.Columns(1).HeaderStyle.Width = 0
                gvStudents.Columns(1).ItemStyle.Width = 0

            End If


            Dim lblText1 As New LinkButton
            Dim lblText3 As New Label
            Dim i As Integer
            For i = 0 To gvStudents.Columns.Count - 1
                lblText1 = gvStudents.HeaderRow.FindControl("lbl" + i.ToString)
                lblText3 = gvStudents.HeaderRow.FindControl("lblT" + i.ToString)
                If Not lblText1 Is Nothing Then
                    If lblText3.Text <> "" And lblText3.Text <> "-" Then
                        lblText1.Attributes.Add("title", lblText3.Text)
                    End If
                    lblText1.Attributes.Add("style", "text-decoration:none")
                    lblText1.Attributes.Add("OnClick", "javascript:return false;")
                    lblText3.Visible = False
                End If

            Next

            Dim cAr As String()
            ' ReDim cAr(cArr.Count * gvStudents.Rows.Count)
            Dim k As Integer
            Dim p As Integer
            Dim t As Integer = 0
            Dim cst As String = ""
            Dim txtItem1 As TextBox
            Dim cs As ClientScriptManager

            Dim LENGTH_TEXT As Integer

            Dim lengthFunction As String
            Dim lengthFunction1 As String


            Dim strQuery As String = "SELECT ISNULL(SBG_CMTMAXLENGTH,3000) FROM SUBJECTS_GRADE_S WHERE SBG_ID=" + H_SBJ_ID.Value
            LENGTH_TEXT = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
            'If Session("sbsuid") = "125017" Then
            '    'LENGTH_TEXT = 250
            '    Select Case txtGrdSubject.Text
            '        Case "Islamic Education"
            '            LENGTH_TEXT = 120
            '        Case "Language A (English)"
            '            LENGTH_TEXT = 250
            '        Case "Language B (Arabic)"
            '            LENGTH_TEXT = 250
            '        Case "Mathematics"
            '            LENGTH_TEXT = 310
            '        Case "Music"
            '            LENGTH_TEXT = 180
            '        Case "Physical Education & Swimming"
            '            LENGTH_TEXT = 180
            '        Case "Programme of Inquiry"
            '            LENGTH_TEXT = 300
            '        Case Else
            '            LENGTH_TEXT = 180
            '    End Select
            'Else
            '    LENGTH_TEXT = 1500
            'End If
            lengthFunction = "function isMaxLength(txtBox,evt) {"
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"
            lengthFunction += " var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"

            'lengthFunction1 = "function isMaxLength1(txtBox) {"
            'lengthFunction1 += " if(txtBox) { "
            '' lengthFunction1 += "alert(txtBox.value.length);"
            ''lengthFunction1 += " var charCode = (evt.which) ? evt.which : event.keyCode ;"
            ''lengthFunction1 += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            'lengthFunction1 += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            'lengthFunction1 += "{ return true;} else {alert('The comment length has exceeded then maximum allowed length.Please use back space to remove extra characters');return false;}"
            ''            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            'lengthFunction1 += " }"
            'lengthFunction1 += "}"

            lengthFunction += "function isMaxLength1(txtBox,evt) {"
            lengthFunction += "txtBox.style.height = 60; "
            lengthFunction += "txtBox.style.width = 200+ ""px""; "
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"
            lengthFunction += "var str=txtBox.value; var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {alert('The comment length has exceeded then maximum permissible length of " + LENGTH_TEXT.ToString + " characters.The remaining text will be truncated.');txtBox.value=str.substring(0," + (LENGTH_TEXT - 1).ToString + ");return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"

            lengthFunction += "function textHeightIncrease(txtBox,evt) {"
            lengthFunction += "txtBox.style.height = 200+ ""px""; "
            lengthFunction += "txtBox.style.width = 400+ ""px""; "
            lengthFunction += "return true;"
            lengthFunction += "}"

            For p = 0 To gvStudents.Rows.Count - 1
                For k = 0 To cArr.Count - 1
                    If cst <> "" Then
                        cst += "|"
                    End If
                    cst += gvStudents.Rows(p).FindControl(cArr.Item(k).ToString).ClientID
                    Try
                        txtItem1 = gvStudents.Rows(p).FindControl(cArr.Item(k).ToString)

                        If txtItem1.TextMode = TextBoxMode.MultiLine Then
                            txtItem1.Attributes.Add("onkeydown", "return isMaxLength(this,event);")
                            txtItem1.Attributes.Add("onmousedown", "return textHeightIncrease(this,event);")
                            txtItem1.Attributes.Add("onblur", "return isMaxLength1(this,event);")
                        End If
                        ' txtItem1.Attributes.Add("onblur", "return isMaxLength1(this);")
                        ClientScript.RegisterClientScriptBlock(Page.GetType(), txtItem1.ClientID, lengthFunction, True)
                        'ClientScript.RegisterClientScriptBlock(Page.GetType(), txtItem1.ClientID, lengthFunction1, True)
                    Catch ex As Exception
                    End Try
                Next
            Next

            If cst <> "" Then
                cAr = cst.Split("|")
                RadSpell1.ControlsToCheck = cAr
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function BindBlankRow()

        Dim strSql As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        gvStudents.DataSourceID = ""
        Try
            strSql = "SELECT DISTINCT(STU_NO) " & _
                    " FROM VW_STUDENT_DETAILS " & _
                    " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " & _
                    " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                    " INNER JOIN GROUPS_TEACHER_S ON SGS_SGR_ID=SGR_ID " & _
                    " AND SGS_EMP_ID=" & Session("EmployeeId") & " " & _
                    " WHERE STU_GRD_ID='" & H_GRD_ID.Value & "' AND STU_ACD_ID=" & H_ACD_ID.Value & " AND SGR_ID=" & H_GRP_ID.Value & " " & _
                    " AND STU_ID=1 "
            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If dsStudents.Tables(0).Rows.Count = 0 Then
                gvStudents.DataSource = dsStudents.Tables(0)
                dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
                gvStudents.DataBind()
                gvStudents.Columns.Clear()
                gvStudents.DataSource = Nothing
                gvStudents.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function GetHeaderValues(ByVal ddlList As DropDownList, ByVal RSDID As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsHeader As DataSet
        Dim strSQL As String
        'If Session("sbsuId") = "114003" Then
        '    strSQL = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RSM_ID.Value & " AND RSP_SBG_ID=" & H_SBJ_ID.Value & " AND RSP_RSD_ID=" & RSDID _
        '           & " ORDER BY RSP_DISPLAYORDER"
        'ElseIf Session("sbsuid") = "125017" Then
        '    strSQL = "SELECT RSP_DESCR FROM(SELECT RSP_DESCR,RSP_DISPLAYORDER FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RSM_ID.Value & " AND                               RSP_SBG_ID=" & H_SBJ_ID.Value & " AND RSP_RSD_ID=" & RSDID _
        '    & " UNION ALL SELECT '--',-1" _
        '            & " )PP ORDER BY RSP_DISPLAYORDER"
        'Else
        '    strSQL = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RSM_ID.Value & " AND RSP_SBG_ID=" & H_SBJ_ID.Value & " AND RSP_RSD_ID=" & RSDID & " UNION SELECT '--' as RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S "
        'End If

        strSQL = "SELECT RSP_DESCR FROM(SELECT RSP_DESCR,RSP_DISPLAYORDER FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RSM_ID.Value & " AND                               RSP_SBG_ID=" & H_SBJ_ID.Value & " AND RSP_RSD_ID=" & RSDID _
       & " UNION ALL SELECT '--',-1" _
               & " )PP ORDER BY RSP_DISPLAYORDER"


        dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        ddlList.DataSource = dsHeader
        ddlList.DataTextField = "RSP_DESCR"
        ddlList.DataValueField = "RSP_DESCR"
        ddlList.DataBind()

        Session("ddlListSelValue") = ddlList.Items(0).Value.ToString
        ddlList.ClearSelection()
        Return ddlList
    End Function




    Private Function GetReportHeader() As String
        Dim NVCReportSetup As New NameValueCollection

        Dim strHeader As String = ""
        Dim strSQL As String
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            If Session("SBSUID") <> "125017" And Session("SBSUID") <> "114003" Then
                If H_RSM_DESCR.Value.Contains("FINAL REPORT") = True Then
                    strSQL = "SELECT '-1' + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + 'L' + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,ISNULL(RPF_SHORTNAME,REPLACE(SUBSTRING(RPF_DESCR,1,3),'.','')) AS RSD_HEADER " _
                                     & " FROM RPT.REPORT_SETUP_D AS A" _
                                     & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RSD_RSM_ID=B.RSM_ID AND RSM_ACD_ID=" + H_ACD_ID.Value.ToString _
                                     & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON B.RSM_ID=C.RPF_RSM_ID" _
                                     & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS D ON B.RSM_ID=D.RSG_RSM_ID AND RSG_GRD_ID='" + H_GRD_ID.Value.ToString + "'" _
                                     & " WHERE RSD_bPERFORMANCE_INDICATOR=1 AND RSD_bALLSUBJECTS='True' " _
                                     & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID=" + H_SBJ_ID.Value + ")" _
                                     & " AND RPF_DISPLAYORDER<(SELECT RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_ID=" + H_RPF_ID.Value.ToString + ")" _
                                     & " AND RSD_RESULT<>'M'  AND ISNULL(RSD_bSUPRESS,0)=0"

                Else
                    strSQL = "SELECT '-1' + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + 'L' + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,ISNULL(RPF_SHORTNAME,REPLACE(SUBSTRING(RPF_DESCR,1,3),'.','')) AS RSD_HEADER " _
                                          & " FROM RPT.REPORT_SETUP_D AS A" _
                                          & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RSD_RSM_ID=B.RSM_ID" _
                                          & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON B.RSM_ID=C.RPF_RSM_ID" _
                                          & " WHERE RSD_RSM_ID = " & H_RSM_ID.Value _
                                          & " AND RSD_bPERFORMANCE_INDICATOR=1 AND RSD_bALLSUBJECTS='True' " _
                                          & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID=" + H_SBJ_ID.Value + ")" _
                                          & " AND RPF_DISPLAYORDER<(SELECT RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_ID=" + H_RPF_ID.Value.ToString + ")" _
                                          & " AND RSD_RESULT<>'M'  AND ISNULL(RSD_bSUPRESS,0)=0"
                End If

            

                If ddlCategory.SelectedValue <> "0" Then
                    strSQL += " AND RSD_SBG_CATEGORY_ID=" + ddlCategory.SelectedValue.ToString
                End If


                strSQL += " ORDER BY RPF_DISPLAYORDER "
                dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
                With dsHeader.Tables(0)
                    For intCnt As Integer = 0 To .Rows.Count - 1
                        strHeader += "'' AS '" + .Rows(intCnt).Item("RSD_HEADER").ToString + "' ,"
                        NVCReportSetup.Add(.Rows(intCnt).Item("RSD_HEADER").ToString, .Rows(intCnt).Item("Value").ToString)
                    Next
                    If strHeader.Length > 1 Then
                        'strHeader = strHeader.Substring(0, strHeader.Length - 1)
                    End If
                End With
            End If
            'Session("ReportHeader") = NVCReportSetup
            'Return strHeader
        Catch ex As Exception

        End Try


        Try
            '  Dim NVCReportSetup As New NameValueCollection

            '   Dim strHeader As String = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            'strSQL = "SELECT CAST(RSD_ID AS VARCHAR) + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + RSD_RESULT + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER " _
            '                      & " FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RSM_ID.Value _
            '                      & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " _
            '                      & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID=" + H_SBJ_ID.Value + ")" _
            '                      & " ORDER BY RSD_DISPLAYORDER "


            strSQL = "SELECT * FROM (SELECT CAST(RSD_ID AS VARCHAR) + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + RSD_RESULT + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER,RSD_DISPLAYORDER " _
                                 & " FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RSM_ID.Value _
                                 & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " _
                                 & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID=" + H_SBJ_ID.Value + ")  AND ISNULL(RSD_bSUPRESS,0)=0 "

            If Session("SBSUID") = "125017" Then
                If Not (lblSubject.Text.ToUpper.Contains("ARABIC") Or lblSubject.Text.ToUpper.Contains("ISLAMIC")) Then
                    strSQL += " AND ISNULL(RSD_bCOMMON,0)=0"
                End If
            Else
                strSQL += " AND ISNULL(RSD_bCOMMON,0)=0"
            End If
            If ddlCategory.SelectedValue <> "0" Then
                strSQL += " AND RSD_SBG_CATEGORY_ID=" + ddlCategory.SelectedValue.ToString
            End If

            If Session("SBSUID") = "123016" And lblReport.Text.Contains("STRATEGIES") = True Then
                strSQL += "union all SELECT '-1' + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + 'L' + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,RSD_HEADER ,0 RPF_DISPLAYORDER" _
                          & " FROM RPT.REPORT_SETUP_D AS A" _
                          & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RSD_RSM_ID=B.RSM_ID" _
                          & " WHERE RSM_ACD_ID = " & H_ACD_ID.Value _
                          & " AND RSD_HEADER IN('Target Grade Term2','Achieved Grade','Target Grade Next Year') "
            ElseIf Session("SBSUID") = "123016" Then
                strSQL += "union all SELECT '-1' + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + 'L' + '_' + RSD_CSSCLASS + '_'+ ISNULL(RSD_SUB_DESC,'-') AS Value  ,RSD_HEADER,RSD_DISPLAYORDER " _
                        & " FROM RPT.REPORT_SETUP_D AS A" _
                        & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RSD_RSM_ID=B.RSM_ID" _
                        & " WHERE RSD_RSM_ID = " & H_RSM_ID.Value _
                        & " AND (RSD_bBenchmark=1)  AND ISNULL(RSD_bSUPRESS,0)=0 "
            End If

            strSQL += " )P ORDER BY RSD_DISPLAYORDER "


            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            With dsHeader.Tables(0)
                For intCnt As Integer = 0 To .Rows.Count - 1
                    strHeader += "'' AS '" + .Rows(intCnt).Item("RSD_HEADER").ToString + "' ,"
                    NVCReportSetup.Add(.Rows(intCnt).Item("RSD_HEADER").ToString, .Rows(intCnt).Item("Value").ToString)
                Next
                If strHeader.Length > 1 Then
                    'strHeader = strHeader.Substring(0, strHeader.Length - 1)
                End If
            End With

            Session("ReportHeader") = NVCReportSetup
            Return strHeader
        Catch ex As Exception

        End Try

    End Function


    Sub GetHeaderKeys()
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        Dim grade As String() = H_GRD_ID.Value.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try

            Dim strSQL As String = "SELECT ISNULL(RSD_SUB_DESC,'-') AS RSD_SUB_DESC ,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER " _
                                   & " FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RSM_ID.Value _
                                   & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " _
                                   & " AND (RSD_SBG_ID=" + H_SBJ_ID.Value + ")" _
                                   & " ORDER BY RSD_DISPLAYORDER "

            reader = SqlHelper.ExecuteReader(conn, CommandType.Text, strSQL)

            While reader.Read
                sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>" + reader.GetString(1) + " - " + reader.GetString(0) + "</Div>")
            End While
            reader.Close()
            'If sb.ToString = "" Then
            '    sb.Append("<div style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>No Rule Available !!!</div>")
            'End If
            ltProcess.Text = sb.ToString
            If sb.ToString = "" Then
                lbtnKeys.Visible = False
            Else
                lbtnKeys.Visible = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Function checkPublishActiviyEnable(ByVal stu_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RPP_STU_ID) from RPT.REPORT_STUDENTS_PUBLISH " _
                                & " WHERE RPP_STU_ID=" + stu_id + " AND RPP_RPF_ID=" + H_RPF_ID.Value.ToString _
                                & " AND RPP_bPUBLISH=1"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

#End Region



#Region " Button event Handlings"

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvStudents.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentsMarks.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentsMarks.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
            lblPages.Text = ""

        Catch ex As Exception

        End Try
    End Sub

   

    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try


      
        If GetReportHeader().Length >= 2 Then
            GetStudentsList() ' This Function will Execute only one time in a group
            BindStudents()
            Session("SBGID") = H_SBJ_ID.Value
            Session("Grade") = H_GRD_ID.Value
               btnSave.Enabled = True

            GetHeaderKeys()
        Else
            lblError.Text = "Mark entry columns are not available! "
        End If

        '   Catch ex As Exception
        'UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'lblError.Text = "Request could not be processed"
        'End Try

    End Sub

#End Region

    Protected Sub lblPageNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblPageNo As LinkButton
        lblPageNo = TryCast(sender.FindControl("lblPageNo"), LinkButton)
        ViewState("PageNo") = CInt(lblPageNo.Text)
        lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")
        gvStudents.Columns.Clear()
        BindStudents()
    End Sub
    Private Sub GetStudentsList()


        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim intRowCnt As Integer = 0
        Dim dsRowsNum As DataSet
        'Try
        'Instantiate a New Hash Table And Keeps into a ViewState
        NVCStudentPage = New NameValueCollection
        ViewState("StuPages") = NVCStudentPage
        ViewState("PageNo") = "0"

        If txtStudName.Text.Trim <> "" Then
            strCriteria += " AND STU_ID=" & H_STU_ID.Value & " "
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " INNER JOIN GROUPS_TEACHER_S ON SGS_SGR_ID=SGR_ID "
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & " AND SGS_TODATE IS NULL "
        End If

        strSql = "SELECT STU_NO,STU_ID,STU_NAME + ' '+ CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END as STU_NAME,SCT_DESCR as Section, " & GetReportHeader() & " 'Test Values' COMMENTS " & _
                " FROM VW_STUDENT_DETAILS " & _
                " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " & _
                " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                " " & strCondition & " " & _
                " WHERE STU_GRD_ID='" & H_GRD_ID.Value & "' AND STU_ACD_ID=" & H_ACD_ID.Value & " AND SGR_ID=" & H_GRP_ID.Value & " " & _
                "" & strCriteria

        If Session("sbsuid") = "125002" Then
            strSql += "  ORDER BY STU_NAME "
        Else
            strSql += "  ORDER BY SCT_DESCR,STU_NAME "
        End If


        dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

        ''-- To Get the Maximum Number of Rows 
        'strSql = "SELECT CMT_ROWID,CMT_SBG_ID,CMT_ROWNUM FROM RPT.COMMENTS_ROWNUM WHERE CMT_SBG_ID=" & H_SBJ_GRD_ID.Value & ""
        'dsRowsNum = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        'Dim TotRows As Integer = CInt(dsRowsNum.Tables(0).Rows(0).Item("CMT_ROWNUM"))
      If Session("sbsuid") = "125017" Then
            If dsStudents.Tables(0).Rows.Count >= 1 Then
                For Each drRow As DataRow In dsStudents.Tables(0).Rows
                    intRowCnt += 1
                    If intRowCnt < 11 Then
                        NVCStudentPage.Add(1, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 11 And intRowCnt < 21 Then
                        NVCStudentPage.Add(2, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 21 And intRowCnt < 31 Then
                        NVCStudentPage.Add(3, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 31 And intRowCnt < 41 Then
                        NVCStudentPage.Add(4, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 41 And intRowCnt < 51 Then
                        NVCStudentPage.Add(5, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 51 And intRowCnt < 61 Then
                        NVCStudentPage.Add(6, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 61 And intRowCnt < 71 Then
                        NVCStudentPage.Add(7, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 71 And intRowCnt < 81 Then
                        NVCStudentPage.Add(8, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 81 And intRowCnt < 91 Then
                        NVCStudentPage.Add(9, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 91 And intRowCnt < 101 Then
                        NVCStudentPage.Add(10, drRow.Item("STU_ID"))
                    Else
                        NVCStudentPage.Add(11, drRow.Item("STU_ID"))
                    End If
                Next
            End If
        ElseIf Session("sbsuid") = "135010" Or Session("sbsuid") = "123016" Then
            If dsStudents.Tables(0).Rows.Count >= 1 Then
                For Each drRow As DataRow In dsStudents.Tables(0).Rows
                    intRowCnt += 1
                    If intRowCnt < 16 Then
                        NVCStudentPage.Add(1, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 16 And intRowCnt < 31 Then
                        NVCStudentPage.Add(2, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 31 And intRowCnt < 46 Then
                        NVCStudentPage.Add(3, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 46 And intRowCnt < 61 Then
                        NVCStudentPage.Add(4, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 61 And intRowCnt < 76 Then
                        NVCStudentPage.Add(5, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 76 And intRowCnt < 81 Then
                        NVCStudentPage.Add(8, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 81 And intRowCnt < 91 Then
                        NVCStudentPage.Add(9, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 91 And intRowCnt < 101 Then
                        NVCStudentPage.Add(10, drRow.Item("STU_ID"))
                    Else
                        NVCStudentPage.Add(11, drRow.Item("STU_ID"))
                    End If
                Next
            End If
        ElseIf Session("sbsuid") <> "125010" And Session("sbsuid") <> "123004" Then
            If dsStudents.Tables(0).Rows.Count >= 1 Then
                For Each drRow As DataRow In dsStudents.Tables(0).Rows
                    intRowCnt += 1
                    If intRowCnt < 6 Then
                        NVCStudentPage.Add(1, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 6 And intRowCnt < 11 Then
                        NVCStudentPage.Add(2, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 11 And intRowCnt < 16 Then
                        NVCStudentPage.Add(3, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 16 And intRowCnt < 21 Then
                        NVCStudentPage.Add(4, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 21 And intRowCnt < 26 Then
                        NVCStudentPage.Add(5, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 26 And intRowCnt < 31 Then
                        NVCStudentPage.Add(6, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 31 And intRowCnt < 36 Then
                        NVCStudentPage.Add(7, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 36 And intRowCnt < 41 Then
                        NVCStudentPage.Add(8, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 41 And intRowCnt < 46 Then
                        NVCStudentPage.Add(9, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 46 And intRowCnt < 51 Then
                        NVCStudentPage.Add(10, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 51 And intRowCnt < 56 Then
                        NVCStudentPage.Add(11, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 56 And intRowCnt < 61 Then
                        NVCStudentPage.Add(12, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 61 And intRowCnt < 66 Then
                        NVCStudentPage.Add(13, drRow.Item("STU_ID"))
                    ElseIf intRowCnt >= 66 And intRowCnt < 71 Then
                        NVCStudentPage.Add(14, drRow.Item("STU_ID"))
                    Else
                        NVCStudentPage.Add(15, drRow.Item("STU_ID"))
                    End If
                    'If intRowCnt < 6 Then
                    '    NVCStudentPage.Add(1, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 6 And intRowCnt < 11 Then
                    '    NVCStudentPage.Add(2, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 11 And intRowCnt < 16 Then
                    '    NVCStudentPage.Add(3, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 16 And intRowCnt < 21 Then
                    '    NVCStudentPage.Add(4, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 21 And intRowCnt < 26 Then
                    '    NVCStudentPage.Add(5, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 26 And intRowCnt < 31 Then
                    '    NVCStudentPage.Add(6, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 31 And intRowCnt < 36 Then
                    '    NVCStudentPage.Add(7, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 36 And intRowCnt < 41 Then
                    '    NVCStudentPage.Add(8, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 41 And intRowCnt < 46 Then
                    '    NVCStudentPage.Add(9, drRow.Item("STU_ID"))
                    'ElseIf intRowCnt >= 46 And intRowCnt < 51 Then
                    '    NVCStudentPage.Add(10, drRow.Item("STU_ID"))
                    'Else
                    '    NVCStudentPage.Add(11, drRow.Item("STU_ID"))
                    'End If

                Next
            End If

        Else
            For Each drRow As DataRow In dsStudents.Tables(0).Rows
                NVCStudentPage.Add(1, drRow.Item("STU_ID"))
            Next
        End If
        ViewState("StuPages") = NVCStudentPage
        ViewState("PageNo") = "1"
        ViewState("TotalPages") = NVCStudentPage.Count
        lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")

        Dim i As Integer
        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("pageno", GetType(String)))
        Dim dr As DataRow
        For i = 1 To Val(ViewState("TotalPages"))
            dr = dt.NewRow
            dr.Item(0) = i
            dt.Rows.Add(dr)
        Next

        dlPages.DataSource = dt
        dlPages.DataBind()


        'Catch ex As Exception

        'End Try
    End Sub

    Protected Sub gvStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            Try
                gvStudents.PageIndex = e.NewPageIndex
                BindStudents()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Catch ex As Exception

        End Try
    End Sub

  
    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudents.RowDataBound
        Dim cs As ClientScriptManager = Page.ClientScript
        'For Each gvRow As GridViewRow In gvInfo.Rows
        Dim dsHeader As DataSet
        Dim i As Integer
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                For intcnt As Integer = 1 To CInt(Session("ColummnCnt"))

                    ''------- To Attach the Client Id For Building Javascript Array.
                    cs.RegisterArrayDeclaration("txt" + intcnt.ToString, String.Concat("'", e.Row.FindControl("txt" + intcnt.ToString).ClientID, "'"))

                    ''-------- To Handle Image Button ---------------
                    Dim imgBtn As ImageButton = TryCast(e.Row.FindControl("img" + intcnt.ToString), ImageButton)
                    If Not imgBtn Is Nothing Then
                        Dim strStuId As String = e.Row.Cells(1).Text
                        Dim str As String = gvStudents.Columns(intcnt + 3).HeaderText
                        imgBtn.OnClientClick = "javascript:getcomments('" + e.Row.FindControl("txt" + intcnt.ToString).ClientID + "','ALLCMTS','" + str + "','" + strStuId + "','" + H_RSM_ID.Value.ToString + "'); return false;"
                    End If

                    ''------- To Display the Existing Record If Exist?
                    dsHeader = GetStudentMarks(e.Row.Cells(1).Text, "")
                    For Each DsCol As DataColumn In TryCast(gvStudents.DataSource, DataTable).Columns
                        'dsHeader = GetStudentMarks(e.Row.Cells(1).Text, DsCol.ColumnName)
                        If Not dsHeader Is Nothing AndAlso dsHeader.Tables(0).Rows.Count >= 1 Then
                            For i = 0 To dsHeader.Tables(0).Rows.Count - 1
                                With dsHeader.Tables(0).Rows(i)
                                    If .Item("RSD_RESULT").ToString = "C" Then

                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")
                                                    
                                                End If
                                            End If

                                        End If
                                    ElseIf .Item("RSD_RESULT").ToString = "L" Then

                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = IIf(IsDBNull(.Item("RST_COMMENTS")) = True, "", .Item("RST_COMMENTS"))
                                                    '  TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Enabled = False
                                                End If
                                            End If

                                        End If

                                    ElseIf .Item("RSD_RESULT").ToString = "D" Then

                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    If e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0).GetType.ToString = "System.Web.UI.WebControls.DropDownList" Then
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_COMMENTS").ToString
                                                    Else
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")
                                                    End If
                                                End If
                                            End If

                                        End If

                                    ElseIf .Item("RSD_RESULT").ToString = "M" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_MARK").ToString
                                                End If
                                            End If

                                        End If
                                    ElseIf .Item("RSD_RESULT").ToString = "G" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_GRADING").ToString
                                                End If
                                            End If

                                        End If
                                    Else

                                    End If
                                End With
                            Next
                        End If
                    Next
                    '-------To End the Display Value Session ------------
                Next
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                e.Row.Cells(0).Text = " Student Number"
                e.Row.Cells(2).Text = " Student Name"

            End If

            If (Session("CurrSuperUser") <> "Y") Then
                If IsNumeric(e.Row.Cells(1).Text) = True Then
                    If checkPublishActiviyEnable(e.Row.Cells(1).Text) = False Then
                        e.Row.Enabled = False
                        e.Row.BackColor = Drawing.Color.Cornsilk
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
        '---
        e.Row.Cells(1).Style.Value = "display: none"

    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()

            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuMarks") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                End If
            End If
            Dim url As String
            ViewState("datamode") = "add"
            gvStudents.Columns.Clear()
            If CInt(ViewState("PageNo")) < CInt(ViewState("TotalPages")) Then
                ViewState("PageNo") = CInt(ViewState("PageNo")) + 1
            End If
            lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")
            BindStudents()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSaveDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()

            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuMarks") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                End If
            End If
            Dim url As String
            ViewState("datamode") = "add"
            gvStudents.Columns.Clear()
            BindStudents()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

  
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        gvStudents.Columns.Clear()
        BindStudents()
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        gvStudents.Columns.Clear()
        BindStudents()
    End Sub

    Protected Sub ddlReportcard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportcard.SelectedIndexChanged
        gvStudents.Columns.Clear()
        Session("StuMarks") = ReportFunctions.CreateTableStuMarks()
        Dim strRpf As String() = ddlReportcard.SelectedValue.Split("|")
        H_RSM_ID.Value = strRpf(0)
        H_RPF_ID.Value = strRpf(1)
        H_RSM_DESCR.Value = strRpf(2)

        BindCategory()

        If GetReportHeader().Length >= 2 Then
            GetStudentsList() ' This Function will Execute only one time in a group
            BindStudents()
            Session("SBGID") = H_SBJ_ID.Value
            Session("Grade") = H_GRD_ID.Value
            btnSave.Enabled = True

            GetHeaderKeys()
        Else
            lblError.Text = "Mark entry columns are not available! "
        End If
    End Sub

    Protected Sub txtCommenttxt_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class
