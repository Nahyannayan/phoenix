<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmNewStudentsAllocate_M.aspx.vb" Inherits="Curriculum_clmNewStudentsAllocate_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">

     
                
     var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvStud.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}



    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }           
                
</script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>  Common Subject Allotment For New Students
        </div>
        <div class="card-body">
            <div class="table-responsive">
   

 <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1"  />
                
            </td>
        </tr>
        <tr>
         <td align="left" >
       <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
        </tr>
        <tr>
            <td  valign="top">
                <table  id="groupTable" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    
                     <tr>
             <td align="left" width="20%">
                    <asp:Label ID="lblAccText" runat="server"  class="field-label" Text="Select Academic Year "></asp:Label></td>
              
                 <td align="left">
                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                        
                           <td align="left">
                    <asp:Label ID="lblGrade" runat="server" class="field-label" Text="Select Grade"></asp:Label></td>
             
                 <td align="left">
                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
               
            </tr>
            
             <tr>
             
                  <td align="left">
                    <asp:Label ID="lblShift" runat="server" class="field-label" Text="Shift"></asp:Label></td>
                
                 <td align="left">
                    <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                              
                   <td align="left">
                    <asp:Label ID="lblStream" runat="server" class="field-label" Text="Stream"></asp:Label></td>
             
                 <td align="left">
                    <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                   </tr>
                   <tr>
                       <td align="left">
                           <span class="field-label"> Subject</span></td>
                 
                       <td align="left">
                           <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                           </asp:DropDownList></td>
                   <td align="left" >
       <asp:Label ID="lbl5" runat="server" class="field-label" Text="Teacher"></asp:Label></td>

       
       <td align="left">
           <asp:DropDownList ID="ddlTeacher" runat="server">
       </asp:DropDownList>
       </td>         
                                      </tr>          
    
       <tr>     
                        <td  align="center" valign="bottom" colspan="4">
             
                <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                    Text="List" TabIndex="9" /></td>
                       </tr>   
                  <tr  id="tr1"  >
                  <td colspan="4" ></td>
                  </tr> 
                 
                           
                           
                           
             <tr id="tr2">
            
           <td colspan="4" style="vertical-align:top " align="left">  
           <asp:Panel ID="panel2" runat="server" style="height:auto" ScrollBars="Auto" >
                <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                       PageSize="20"  AllowPaging="True" >
                         <RowStyle  />
                     <Columns>
                     
                     
                     
                     <asp:TemplateField HeaderText="Available">
                       <EditItemTemplate>
                       <asp:CheckBox ID="chkSelect" runat="server"  />
                       </EditItemTemplate>
                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       <HeaderStyle Wrap="False" />
                        <ItemTemplate>
                       <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                       </ItemTemplate>
                        <HeaderTemplate>
                       
                     Select <br />
                            <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
                     ToolTip="Click here to select/deselect all rows" />
                       </HeaderTemplate>
                     </asp:TemplateField>
                     
                     
<asp:TemplateField HeaderText="STU_ID" Visible="False"><ItemTemplate>
                      <asp:Label ID="lblStuId" runat="server" text='<%# Bind("STU_ID") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="SCT_ID" Visible="False"><ItemTemplate>
                      <asp:Label ID="lblSctId" runat="server" text='<%# Bind("SCT_ID") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Stud No."><HeaderTemplate>
                               
                                  <asp:Label ID="lblh1" runat="server" Text="Stud. No"></asp:Label>
                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                               <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
</HeaderTemplate>
<ItemTemplate>
                      <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><HeaderTemplate>
                                
                                  <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label>
                                      <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                               <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
</HeaderTemplate>
<ItemTemplate>
                      <asp:Label ID="lblStuName" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="Center"><HeaderTemplate>
                      
                       Section
                            <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
                          OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged" >
                          </asp:DropDownList>
</HeaderTemplate>
<ItemTemplate>
                      <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Subjects Allotted"><ItemTemplate>
                      <asp:Label ID="lblSubjects" runat="server" text='<%# Bind("SUBJECTS") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>
</Columns>  
                         <SelectedRowStyle />
                         <HeaderStyle />
                         <AlternatingRowStyle />
                     </asp:GridView>
                     
                     
          
                     </asp:Panel>            
               </td>
               
                <td  style="vertical-align:top " align="left" >
             <asp:Panel ID="panel1" runat="server" ScrollBars="Auto" style="height:auto" Direction="LeftToRight" >
              &nbsp;<asp:CheckBox id="chkSelectAll" text="Select All" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged">
             </asp:CheckBox>
             <asp:CheckBoxList id="lstGroup" runat="server" style="text-align: left"  >
             </asp:CheckBoxList>&nbsp;</asp:Panel>  
           </td>
        </tr>
       
    
        
        <tr><td  align="center" class="matters" colspan="4"   >
            <asp:Button ID="btnAllocate" runat="server" CssClass="button" Text="Allocate" ValidationGroup="groupM1" TabIndex="7"   />
             <asp:LinkButton ID="btnHidden" runat="server" CssClass="link"></asp:LinkButton>
                <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnHidden"
                    PopupControlID="pnlEmail" OkControlID="btnClose" CancelControlID="btnCancel"
                    DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                </ajaxToolkit:ModalPopupExtender>


            </td>
            </tr>
            
            
               
                </table>
                
                
                
                
         </td>
        </tr>
               <tr>
            <td class="matters" valign="bottom"  >
                <asp:HiddenField id="hfACD_ID" runat="server">
                </asp:HiddenField><asp:HiddenField id="hfGRD_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSHF_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSTM_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfEMP_ID" runat="server">
                </asp:HiddenField>
            </td>
          
        </tr>
    </table>
<input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />


                
                    <asp:Panel ID="pnlEmail" Style="display: none;" runat="server">
        <div style="min-width: 400px;  border-radius: 8px; " class="pb-4 bg-white border">
         
                  <div class="row"> 
                  <div class=" col-12 griditem_alternative_dark text-right">
                  
                        <asp:Button ID="btnClose" CssClass="button_small" runat="server" Text="X" />
                    </div>
        </div>
                <div class="row"> 
                  <div class=" col-12 subHeader text-center font-weight-bold">
                   Confirmation!!!
                     </div>
        </div>
                 <div class="row"> 
                  <div class=" col-12 text-center">
                        <asp:Label ID="lblMessage" CssClass="error_password" runat="server" Text="Are you agree you proceed?"></asp:Label>
                     </div>
        </div>
            
            <div class="text-center">
                <asp:Button ID="btnOkay" CssClass="button" runat="server" Text="Yes" />    
                <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />
              
            </div> 
        </div>
    </asp:Panel>

            </div>
        </div>
    </div>
            
</asp:Content>

