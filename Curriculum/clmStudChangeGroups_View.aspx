<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmStudChangeGroups_View.aspx.vb" Inherits="Curriculum_clmStudChangeGroups_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Student Change Group</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">

                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnSearch">
                                <table id="tblTC" runat="server" align="center" width="100%">


                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                        
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                         <td align="left" width="20%" ></td>
                                         <td align="left" width="30%" ></td>
                                    </tr>

                                    <tr>

                                        <td align="left"><span class="field-label">Select Grade</span></td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"  >
                                            </asp:DropDownList>
                                        </td>


                                        <td align="left"><span class="field-label">Select Section</span></td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server"  >
                                            </asp:DropDownList>
                                        </td>


                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Student ID</span></td>
                                        
                                        <td align="left">
                                            <asp:TextBox ID="txtStuNo" runat="server">
                                            </asp:TextBox></td>
                                        <td align="left"><span class="field-label">Student Name</span></td>
                                        
                                        <td align="left">
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                      
                                    </tr>
                                    
                                    <tr>
                                         <td colspan="4" align="center" >
                                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"   /></td>
                                    </tr>

                                    <tr>
                                         <td colspan="4"  >
                                           </td>
                                    </tr>
                                    <tr>



                                        <td align="center" colspan="4" valign="top">

                                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                  PageSize="20">
                                                <RowStyle CssClass="griditem"   />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Student No" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            
                                                                        <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label><br />
                                                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                        </ItemTemplate>

                                                        <ItemStyle ></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            
                                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
                                                                        <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />
                                                                    
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Grade" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                           
                                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade">Grade<br />
                                                                        </asp:Label><asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                               
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Section">
                                                        <HeaderTemplate>
                                                            
                                                                        <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label><br />
                                                                        <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />
                                                             
              
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>




                                                    <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                                    </asp:ButtonField>

                                                </Columns>
                                                <SelectedRowStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </td>
                                    </tr>


                                </table>
                            </asp:Panel>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>

</asp:Content>

