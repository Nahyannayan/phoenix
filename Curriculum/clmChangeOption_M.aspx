<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeOption_M.aspx.vb" Inherits="Curriculum_clmChangeOption_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" Text="Change Option Approval" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" align="center"  cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td align="left" colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" 
                                ValidationGroup="groupM1" Style="text-align: left"  />
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label>
                            
                        </td>                      
                    </tr>
                   
                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="5" cellspacing="0" width="100%">
                              
                                <tr>

                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblStu" runat="server" Text="Student No"  class="field-label"></asp:Label></td>
                                    
                                    <td align="left"  width="30%">
                                        <asp:Label ID="lblStuNo" runat="server" class="field-value"  ></asp:Label></td>

                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblNtext" runat="server" Text="Student Name" class="field-label" ></asp:Label></td>
                                    
                                    <td align="left"  width="30%">
                                        <asp:Label ID="lblStuName" runat="server" class="field-value"  ></asp:Label></td>

                                </tr>

                                <tr>
                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblGr" runat="server" Text="Stream" class="field-label"></asp:Label></td>
                                    
                                    <td align="left"  width="30%">
                                        <asp:Label ID="lblGrade" runat="server" class="field-value"  ></asp:Label></td>

                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblStre" runat="server" Text="Stream" class="field-label"></asp:Label></td>
                                   
                                    <td align="left"  width="30%">
                                        <asp:Label ID="lblStream" runat="server" class="field-value"  ></asp:Label></td>
                                </tr>



                                <tr >


                                    <td align="left" width="20%"  >
                                        <asp:Label ID="Label2" runat="server" Text="Changed Subjects" class="field-label"></asp:Label></td>
                                   
                                    <td align="left" width="30%" >
                                        <asp:Label ID="lblSubjects" runat="server" class="field-value" ></asp:Label></td>


                                    <td align="left" width="20%"  > <span class="field-label">Approval Date </span></td>
                                   
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtDate" runat="server"  AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4" >
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="groupM1" TabIndex="7"  />
                    <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" TabIndex="7"  CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="7"  CausesValidation="False" /></td>
                                </tr>

                            </table>




                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom" >
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCS_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTM_ID" runat="server"></asp:HiddenField>
                            <asp:RequiredFieldValidator ID="rfDate" runat="server" ControlToValidate="txtDate"
                                Display="None" ErrorMessage="Please enter the field Approval Date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSHF_ID" runat="server"></asp:HiddenField>


                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                PopupButtonID="txtDate" TargetControlID="txtDate" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgDate" TargetControlID="txtDate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


</asp:Content>

