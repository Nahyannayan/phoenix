﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Imports Telerik.Web.UI
Imports System.Web.Services
Imports System.Web.Script.Services

Partial Class Curriculum_clmLessonStudentCriterias_Popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            lblAcademicYear.Text = Request.QueryString("accyear")
            lblGrade.Text = Request.QueryString("grade")
            lblGroup.Text = Request.QueryString("group")
            lblSubject.Text = Request.QueryString("subject")
            lblName.Text = Request.QueryString("name")
            lblStudentID.Text = Request.QueryString("stuno")
            lblTopic.Text = Request.QueryString("topic")
            lblObjective.Text = Request.QueryString("obj_name")
            Dim IsAgeBand As Int16 = Request.QueryString("isageband")
            '  lblCurrentLevel.Text = Request.QueryString("clvl")

            If IsAgeBand = 0 Then
                tdAgeBand.Visible = False
            Else
                tdAgeBand.Visible = True
                BindAgeBand(IsAgeBand)
            End If

            hACD_ID.Value = Request.QueryString("acdid").Replace(".00", "").Replace(",", "")
            hSBG_ID.Value = Request.QueryString("sbgid").Replace(".00", "").Replace(",", "")
            hGRD_ID.Value = Request.QueryString("grdid").Replace(".00", "").Replace(",", "")
            hTopic_ID.Value = Request.QueryString("topicid").Replace(".00", "").Replace(",", "")
            hTSM_ID.Value = Request.QueryString("tsmid").Replace(".00", "").Replace(",", "")
            hSBM_ID.Value = Request.QueryString("sbmid").Replace(".00", "").Replace(",", "")
            hSGR_ID.Value = Request.QueryString("sgr_id").Replace(".00", "").Replace(",", "").ToString()
            If Request.QueryString("obj_id") IsNot Nothing And Request.QueryString("obj_id") <> "" Then
                hObj_ID.Value = Request.QueryString("obj_id").Replace(".00", "").Replace(",", "").ToString()
                hSGR_ID.Value = Request.QueryString("sgr_id").Replace(".00", "").Replace(",", "").ToString()
            End If

            If hObj_ID.Value Is Nothing Or hObj_ID.Value = "" Then
                GetStu_ID()
                BindPhoto()
                GridBind()
                If CheckTermStatus() Then
                    btnSave.Visible = False
                    div_status.Visible = True
                    lbl_BlockStatus.Text = "Entries for current selected term has been blocked."
                Else
                    btnSave.Visible = True
                    div_status.Visible = False
                    lbl_BlockStatus.Text = ""
                End If
            Else
                GridBind_Student()
                imgStud.Visible = False
                lblObjective.Visible = True
                lblGrade.Visible = False
            End If
            BindGrade()

        End If
    End Sub
    Sub BindAgeBand(ByVal IsAgeBand As Int16)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim stopro As String = "[OASIS].dbo.[GET_GRADE_AGEBAND]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, stopro)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlAgeBand.DataValueField = "GRD_AGEB_ID"
            ddlAgeBand.DataTextField = "GRD_AGEB_CATEGORY"
            ddlAgeBand.DataSource = ds.Tables(0)
            ddlAgeBand.DataBind()
            ddlAgeBand.Items.FindByValue(IsAgeBand).Selected = True
        End If
    End Sub
    'Protected Function CheckAgeBand() As Boolean

    '    Try
    '        Dim bool As Boolean = False
    '        ' code portion to find the ageband flag 
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim param(2) As SqlParameter
    '        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
    '        param(1) = Mainclass.CreateSqlParameter("@ACD_ID", hACD_ID.Value, SqlDbType.BigInt)
    '        param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
    '        bool = SqlHelper.ExecuteScalar(str_conn, "GET_GRADE_ISAGEBAND", param)
    '        ' end of code portion 
    '        Return bool

    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function
    Sub BindPhoto()

        If hStuPhotoPath.Value <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + hStuPhotoPath.Value
            imgStud.ImageUrl = strImagePath
        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

    Sub GetStu_ID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH FROM STUDENT_M WHERE STU_NO='" + lblStudentID.Text + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hSTU_ID.Value = ds.Tables(0).Rows(0).Item(0)
            hStuPhotoPath.Value = ds.Tables(0).Rows(0).Item(1)
            Session("APP_STU_ID") = hSTU_ID.Value
        End If
    End Sub
    Sub GridBind(Optional ByVal Grade As String = "")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = ""
        Dim dtColor As DataTable = Session("COLOR_CODES")
        'If hTopic_ID.Value = "" Then

        '    str_query = "SELECT  SYC_ID,SYC_DESCR,ISNULL(STC_STATUS,'--') STC_STATUS,SUBSTRING(CONVERT(VARCHAR(100),SYC_ENDDT,106),0,7) SYC_STARTDT," _
        '                         & " CASE WHEN STC_UPDATEDATE=SYC_ENDDT OR STC_UPDATEDATE IS NULL THEN '' ELSE SUBSTRING(CONVERT(VARCHAR(100),STC_UPDATEDATE,106),0,7) END STC_UPDATEDATE,SYC_SYD_ID,STC_EVIDENCE_PATH, " _
        '                         & "   (SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID)+'-' + SYD_DESCR AS  Topic FROM SYL.LESSON_CRITERIA_M LEFT OUTER JOIN " _
        '                         & " SYL.STUDENT_LESSONCRITERIA_S ON SYC_ID=STC_SYC_ID  AND STC_STU_ID=" + hSTU_ID.Value + "AND STC_TSM_ID='" + hTSM_ID.Value + "'" _
        '                         & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID" _
        '                         & " WHERE SYD_SBG_ID=" + hSBG_ID.Value + " AND ISNULL(SYC_bEVALUATION,0)=1" _
        '                         & " ORDER BY SYC_ENDDT"
        'Else

        '    str_query = "SELECT  SYC_ID,SYC_DESCR,ISNULL(STC_STATUS,'--') STC_STATUS,SUBSTRING(CONVERT(VARCHAR(100),SYC_ENDDT,106),0,7) SYC_STARTDT," _
        '                         & " CASE WHEN STC_UPDATEDATE=SYC_ENDDT OR STC_UPDATEDATE IS NULL THEN '' ELSE SUBSTRING(CONVERT(VARCHAR(100),STC_UPDATEDATE,106),0,7) END STC_UPDATEDATE,SYC_SYD_ID, STC_EVIDENCE_PATH," _
        '                         & "   (SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID)+'-' + SYD_DESCR AS  Topic FROM SYL.LESSON_CRITERIA_M LEFT OUTER JOIN " _
        '                         & " SYL.STUDENT_LESSONCRITERIA_S ON SYC_ID=STC_SYC_ID  AND STC_STU_ID=" + hSTU_ID.Value + "AND STC_TSM_ID='" + hTSM_ID.Value + "'" _
        '                         & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID" _
        '                         & " WHERE (SYD_ID=" + hTopic_ID.Value + " OR SYD_PARENT_ID=" + hTopic_ID.Value + ") " _
        '                         & " ORDER BY SYC_ENDDT"

        'End If
        ''FOR OBJECTIVES  OTHER THAN THE STUDENTS GRADE
        'If Grade <> "" Then
        '    str_query = "SELECT  SYC_ID,SYC_DESCR,ISNULL(STC_STATUS,'--') STC_STATUS,SUBSTRING(CONVERT(VARCHAR(100),SYC_ENDDT,106),0,7) SYC_STARTDT," _
        '                             & " CASE WHEN STC_UPDATEDATE=SYC_ENDDT OR STC_UPDATEDATE IS NULL THEN '' ELSE SUBSTRING(CONVERT(VARCHAR(100),STC_UPDATEDATE,106),0,7) END STC_UPDATEDATE,SYC_SYD_ID,STC_EVIDENCE_PATH, " _
        '                             & "   (SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID)+'-' + SYD_DESCR AS  Topic FROM SYL.LESSON_CRITERIA_M LEFT OUTER JOIN " _
        '                             & " SYL.STUDENT_LESSONCRITERIA_S ON SYC_ID=STC_SYC_ID  AND STC_STU_ID=" + hSTU_ID.Value + "AND STC_TSM_ID='" + hTSM_ID.Value + "'" _
        '                             & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID" _
        '                             & " WHERE SYD_SBG_ID IN  (SELECT sbg_id FROM SUBJECTS_GRADE_S where SBG_ACD_ID=" + hACD_ID.Value _
        '                             & "and SBG_SBM_ID=" + hSBM_ID.Value + " and SBG_GRD_ID ='" + ddlGrade.SelectedValue.Split("|")(0) + "') AND ISNULL(SYC_bEVALUATION,0)=1" _
        '                             & " ORDER BY SYC_ENDDT"

        'End If

        '   Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@SYD_ID", IIf(hTopic_ID.Value = "", 0, hTopic_ID.Value))
        params(1) = New SqlParameter("@SBG_ID", hSBG_ID.Value)
        params(2) = New SqlParameter("@TSM_ID", hTSM_ID.Value)
        params(3) = New SqlParameter("@STU_ID", hSTU_ID.Value)
        params(4) = New SqlParameter("@MARK_SCHEME", GET_GRADE_SCHEME(hSGR_ID.Value))
        If Grade <> "" Then
            params(5) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0))
            params(6) = New SqlParameter("@SBM_ID", hSBM_ID.Value)
            params(7) = New SqlParameter("@ACD_ID", hACD_ID.Value)
        End If
        If tdAgeBand.Visible Then
            params(8) = New SqlParameter("@ISAGEBAND", ddlAgeBand.SelectedValue)
        Else
            params(8) = New SqlParameter("@ISAGEBAND", 0)
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SYL].[STUDENT_TRACKER_POPUP]", params)


        If ds.Tables.Count > 0 Then
            gvObjectives.DataSource = ds
            gvObjectives.DataBind()

        End If

        BindDropdowns()
        Dim i As Integer
        Dim lblStatus As Label
        Dim ddlStatus As DropDownList
        Dim lblFileName As LinkButton
        Dim lblObjective As Label
        Dim lblObjId As Label

        For i = 0 To gvObjectives.Rows.Count - 1
            lblObjId = gvObjectives.Rows(i).FindControl("lblObjId")
            lblStatus = gvObjectives.Rows(i).FindControl("lblStatus")
            ddlStatus = gvObjectives.Rows(i).FindControl("ddlStatus")
            lblFileName = gvObjectives.Rows(i).FindControl("lblFileName")
            lblObjective = gvObjectives.Rows(i).FindControl("lblObjective")
            If lblObjId.Text.Split("|").Length >= 1 Then
                If lblObjId.Text.ToString <> "" Then
                    Dim a = lblObjId.Text.ToString.Replace("|", "_") 'ObjDescr(lblHdrId.Text.ToString)
                    lblObjective.Attributes.Add("onclick", "javascript:var popup = HoverPopUp('" + a + "');")
                End If
            End If
            If Not ddlStatus.Items.FindByValue(lblStatus.Text) Is Nothing Then
                ddlStatus.Items.FindByValue(lblStatus.Text).Selected = True
                Dim DropDownListItemBlank As New ListItem
                DropDownListItemBlank.Value = "0"
                DropDownListItemBlank.Text = "-"
                ddlStatus.Items.Insert(0, DropDownListItemBlank)
            Else
                Dim DropDownListItemBlank As New ListItem
                DropDownListItemBlank.Value = "0"
                DropDownListItemBlank.Text = "-"
                ddlStatus.Items.Insert(0, DropDownListItemBlank)
                ddlStatus.SelectedValue = 0
            End If
            For cnt = 0 To dtColor.Rows.Count - 1
                If ddlStatus.SelectedItem.Value.ToString = dtColor.Rows(cnt).Item("DAD_CODE") Then
                    ddlStatus.Style.Add("background", dtColor.Rows(cnt).Item("DAD_COLOR_CODE"))
                    ddlStatus.Items.FindByValue(ddlStatus.SelectedItem.Value).Attributes.Add("style", "background:" + dtColor.Rows(cnt).Item("DAD_COLOR_CODE"))

                End If
            Next
            lblFileName.Text = ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString.Substring(ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString.LastIndexOf("\") + 1)
            lblFileName.ToolTip = ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString
        Next

    End Sub

    Sub GridBind_Student()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dtColor As DataTable = Session("COLOR_CODES")
        Dim str_query As String = ""
        str_query = "SELECT DISTINCT STU_ID,OASIS.[dbo].[fnProperCase](STU_FIRSTNAME+' ' + ISNULL(STU_LASTNAME,ISNULL(STU_MIDNAME,''))) as STU_NAME, STU_NO,ISNULL(STC_STATUS,'0')  as STC_STATUS,STC_UPDATEDATE,STC_SYD_ID,STC_EVIDENCE_PATH " _
                             & " FROM STUDENT_M AS A  INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                             & "  LEFT OUTER JOIN SYL.STUDENT_LESSONCRITERIA_S  ON STC_STU_ID=SSD_STU_ID AND " _
                             & " STC_SYC_ID=" + hObj_ID.Value + " AND STC_TSM_ID='" + hTSM_ID.Value + "' AND STC_SBG_ID=" + hSBG_ID.Value _
                             & " LEFT OUTER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID   " _
                             & "WHERE STU_CURRSTATUS<>'CN' AND  SSD_SGR_ID=" + hSGR_ID.Value _
                             & " ORDER BY STU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvObjectives_Students.DataSource = ds
        gvObjectives_Students.DataBind()
        BindDropdowns_Students()
        Dim i As Integer
        Dim lblStatus As Label
        Dim ddlStatus As DropDownList
        Dim lblFileName As LinkButton
        For i = 0 To gvObjectives_Students.Rows.Count - 1
            lblStatus = gvObjectives_Students.Rows(i).FindControl("lblStatus_Student")
            ddlStatus = gvObjectives_Students.Rows(i).FindControl("ddlStatus_Student")
            lblFileName = gvObjectives_Students.Rows(i).FindControl("lblFileName")
            If Not ddlStatus.Items.FindByValue(lblStatus.Text) Is Nothing Then
                ddlStatus.Items.FindByValue(lblStatus.Text).Selected = True
                Dim DropDownListItemBlank As New ListItem
                DropDownListItemBlank.Value = "0"
                DropDownListItemBlank.Text = "-"
                ddlStatus.Items.Insert(0, DropDownListItemBlank)
            Else
                Dim DropDownListItemBlank As New ListItem
                DropDownListItemBlank.Value = "0"
                DropDownListItemBlank.Text = "-"
                ddlStatus.Items.Insert(0, DropDownListItemBlank)
                ddlStatus.SelectedValue = 0
            End If
            For cnt = 0 To dtColor.Rows.Count - 1
                If ddlStatus.SelectedItem.Value.ToString = dtColor.Rows(cnt).Item("DAD_CODE") Then
                    ddlStatus.Style.Add("background", dtColor.Rows(cnt).Item("DAD_COLOR_CODE"))
                    ddlStatus.Items.FindByValue(ddlStatus.SelectedItem.Value).Attributes.Add("style", "background:" + dtColor.Rows(cnt).Item("DAD_COLOR_CODE"))

                End If
            Next
            lblFileName.Text = ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString.Substring(ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString.LastIndexOf("\") + 1)
            lblFileName.ToolTip = ds.Tables(0).Rows(i).Item("STC_EVIDENCE_PATH").ToString
        Next
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlObj As DropDownList
        Dim lblStuId As Label
        Dim lblSydId As Label
        Dim uploadFile As RadAsyncUpload
        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strXml As String = ""

        Dim grade As String() = hGRD_ID.Value.Split("|")

        If hObj_ID.Value Is Nothing Or hObj_ID.Value = "" Then
            For i = 0 To gvObjectives.Rows.Count - 1
                With gvObjectives.Rows(i)
                    lblObjId = .FindControl("lblObjId")
                    ddlObj = .FindControl("ddlStatus")
                    lblSydId = .FindControl("lblSydId")
                    uploadFile = gvObjectives.Rows(i).FindControl("uploadFile")
                    Dim obj_id As String() = lblObjId.Text.Split("|")
                    For k = 0 To obj_id.Length() - 1   'LOOPING FOR THE NO OBJECTIVE UNDER A PARTICULAR TOPIC OR SUBTOPIC
                        If lblObjId.Text <> "" And lblObjId.Text <> "0" Then
                            strXml += "<ID><STU_ID>" + hSTU_ID.Value + "</STU_ID>"
                            strXml += "<SYC_ID>" + obj_id(k) + "</SYC_ID>"
                            strXml += "<STC_STATUS>" + ddlObj.SelectedValue + "</STC_STATUS>"
                            strXml += "<SYD_ID>" + lblSydId.Text + "</SYD_ID>"
                            strXml += "<STC_EVIDENCE_PATH>" + UploadEvidence(uploadFile, k) + "</STC_EVIDENCE_PATH>"
                            strXml += "<TSM_ID>" + hTSM_ID.Value + "</TSM_ID></ID>"
                        End If
                    Next
                End With
            Next
        Else
            For i = 0 To gvObjectives_Students.Rows.Count - 1
                With gvObjectives_Students.Rows(i)
                    lblStuId = .FindControl("lblStudentId")
                    ddlObj = .FindControl("ddlStatus_Student")
                    lblSydId = .FindControl("lblSydId_Student")
                    uploadFile = gvObjectives_Students.Rows(i).FindControl("uploadFile")
                    If lblStuId.Text <> "" And lblStuId.Text <> "0" Then
                        strXml += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID>"
                        strXml += "<SYC_ID>" + hObj_ID.Value + "</SYC_ID>"
                        strXml += "<STC_STATUS>" + ddlObj.SelectedValue + "</STC_STATUS>"
                        strXml += "<SYD_ID>" + lblSydId.Text + "</SYD_ID>"
                        strXml += "<STC_EVIDENCE_PATH>" + UploadEvidence(uploadFile, 0) + "</STC_EVIDENCE_PATH>"
                        strXml += "<TSM_ID>" + hTSM_ID.Value + "</TSM_ID></ID>"
                    End If

                End With
            Next
        End If


        strXml = "<IDS>" + strXml + "</IDS>"
        str_query = " EXEC [SYL].[saveSTUDENTCRITERIAS_BULK_v2]" _
                & " @ACD_ID =" + hACD_ID.Value + "," _
                & " @GRD_ID ='" + grade(0) + "'," _
                & " @SBG_ID =" + hSBG_ID.Value + "," _
                & " @STC_XML ='" + strXml + "'," _
                & " @STC_UPDATEDATE ='" + IIf(txtDate.Text = "", Now.ToString, txtDate.Text) + "'," _
                & " @STC_USER ='" + Session("susr_id") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Closing Function", "ClosePopup();", True)
        Session("Popup") = "1"

        lblError.Text = "Record saved successfully"
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Sub BindDropdowns_Students()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grd_id As String() = hGRD_ID.Value.ToString.Split("|")
        Dim str_query As String = ""
        str_query = "SELECT DAD_CODE,DAD_DESCR,DAD_COLOR_CODE,DAD_ORDER FROM  dbo.defaultaero_m " _
                            & " INNER JOIN  dbo.defaultaero_d	ON  dam_id = dad_dam_id" _
                            & " WHERE DAM_BSU_ID ='" + Session("sBsuid") + "' and dad_type='AD' and dam_grd_ids LIKE '%" + grd_id(0) + "%'"
        str_query += " ORDER BY DAD_ORDER DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds IsNot Nothing And ds.Tables(0) IsNot Nothing Then
            Session("COLOR_CODES") = ds.Tables(0)
        End If
        Dim i As Integer
        Dim j, k As Integer

        Dim objH, objId As String

        Dim ddlObjH As DropDownList
        Dim ddlObj As DropDownList
        If gvObjectives_Students.HeaderRow IsNot Nothing Then
            objH = "ddlAll"
            ddlObjH = gvObjectives_Students.HeaderRow.FindControl(objH)
            ddlObjH.Items.Clear()

            For i = 0 To ds.Tables(0).Rows.Count - 1


                Dim DropDownListItem As New ListItem
                DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE"))
                ddlObjH.Items.Insert(0, DropDownListItem)
            Next
            Dim DropDownListItemBlank As New ListItem
            DropDownListItemBlank.Value = "0"
            DropDownListItemBlank.Text = "-"
            ddlObjH.Items.Insert(0, DropDownListItemBlank)

        End If
        For Each gvrow As GridViewRow In gvObjectives_Students.Rows
            objId = "ddlStatus_Student"
            ddlObj = gvrow.FindControl(objId)
            ddlObj.Items.Clear()
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim DropDownListItem As New ListItem
                DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE"))
                ddlObj.Items.Insert(0, DropDownListItem)
            Next
            'ddlObj.Items.Insert(0, DropDownListItemBlank)
        Next
    End Sub

    Sub BindDropdowns()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grd_id As String() = hGRD_ID.Value.ToString.Split("|")
        Dim str_query As String = ""
        str_query = "SELECT DAD_CODE,DAD_DESCR,DAD_COLOR_CODE,DAD_ORDER FROM  dbo.defaultaero_m " _
                            & " INNER JOIN  dbo.defaultaero_d	ON  dam_id = dad_dam_id" _
                            & " WHERE DAM_BSU_ID ='" + Session("sBsuid") + "' and dad_type='AD' and dam_grd_ids LIKE '%" + grd_id(0) + "%'"
        str_query += " ORDER BY DAD_ORDER DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds IsNot Nothing And ds.Tables(0) IsNot Nothing Then
            Session("COLOR_CODES") = ds.Tables(0)
        End If
        Dim i As Integer
        Dim j, k As Integer

        Dim objH, objId As String

        Dim ddlObjH As DropDownList
        Dim ddlObj As DropDownList
        If gvObjectives.HeaderRow IsNot Nothing Then
            objH = "ddlAll"
            ddlObjH = gvObjectives.HeaderRow.FindControl(objH)
            ddlObjH.Items.Clear()

            For i = 0 To ds.Tables(0).Rows.Count - 1


                Dim DropDownListItem As New ListItem
                DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE"))
                ddlObjH.Items.Insert(0, DropDownListItem)
            Next
            Dim DropDownListItemBlank As New ListItem
            DropDownListItemBlank.Value = "0"
            DropDownListItemBlank.Text = "-"
            ddlObjH.Items.Insert(0, DropDownListItemBlank)
        End If


        For Each gvrow As GridViewRow In gvObjectives.Rows
            objId = "ddlStatus"
            ddlObj = gvrow.FindControl(objId)
            ddlObj.Items.Clear()
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim DropDownListItem As New ListItem
                DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE"))
                ddlObj.Items.Insert(0, DropDownListItem)
            Next
            ' ddlObj.Items.Insert(0, DropDownListItemBlank)
        Next
    End Sub
    Function UploadEvidence(uploadFile As RadAsyncUpload, ByVal k As Integer) As String
        Dim str As String = ""
        Try
            If k = 0 Then


                Dim STU_ID As String = hSTU_ID.Value
                Dim STU_BSU_ID As String = Session("sBsuid")
                Dim PHOTO_PATH As String = String.Empty
                Dim ConFigPath As String = Convert.ToString(ConfigurationManager.AppSettings("ObjectiveEvidence"))


                If Not Directory.Exists(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\") Then
                    Directory.CreateDirectory(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\")
                End If

                If uploadFile.UploadedFiles.Count > 0 Then
                    Dim tempDir As String = ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\"

                    Dim tempFileName As String = uploadFile.UploadedFiles(0).GetName
                    Dim tempFileNameUsed As String = tempDir + tempFileName
                    If uploadFile.UploadedFiles.Count Then
                        If File.Exists(tempFileNameUsed) Then
                            File.Delete(tempFileNameUsed)
                        End If
                        Try
                            uploadFile.UploadedFiles(0).SaveAs(tempFileNameUsed)

                        Catch ex As Exception

                        End Try
                        Try
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                            lblError.Text = "Request could not be processed"
                        End Try
                    End If
                    str = "\" & STU_BSU_ID & "\" & STU_ID & "\" & tempFileName
                End If
            End If
            Return str
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "UploadEvidence")
        End Try
        Return str
    End Function

    Sub DownloadFile(filename As String)
        Dim tempDir As String = Convert.ToString(ConfigurationManager.AppSettings("ObjectiveEvidence"))
        Dim tempFileName As String = filename
        Dim tempFileNameUsed As String = tempDir + tempFileName

        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.End()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)

        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub lblFileName_Click(sender As Object, e As EventArgs)
        Dim lblFile As LinkButton = CType(sender, LinkButton)
        DownloadFile(lblFile.ToolTip)
    End Sub
    Protected Sub gvObjectives_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvObjectives.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblFileName As LinkButton = e.Row.FindControl("lblFileName")

                Dim ScriptManager1 As ScriptManager = DirectCast(Page.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(lblFileName)


            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gvObjectives_Students_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvObjectives_Students.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblFileName As LinkButton = e.Row.FindControl("lblFileName")

                Dim ScriptManager1 As ScriptManager = DirectCast(Page.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(lblFileName)
            End If
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        GridBind(ddlGrade.SelectedValue)
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                              & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                              & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                              & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " _
                              & " WHERE GRM_ACD_ID='" + hACD_ID.Value + "'"
        str_query += " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Function GET_GRADE_SCHEME(ByVal sgr_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@SGR_ID", sgr_id)

        Dim str As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "[SYL].[GET_GRADE_SCHEME]", params)

        Return IIf(str = "", "OBJ", str)
    End Function
    <WebMethod()> _
  <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function ObjDescr(ByVal obj_id As String) As String
        'Dim obj_id As String = "274"
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select SYC_DESCR from syl.LESSON_CRITERIA_M where syc_id in(" + obj_id.Replace("_", ",") + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim str As String = "<h3>Underlying Objectives </h3>"
        str += "<ul>"
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            str += "<li>" + ds.Tables(0).Rows(i)("SYC_DESCR") + "</li>"
        Next
        str += "</ul>"
        Return str
    End Function
    Function CheckTermStatus() As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select TSM_BBlocked from oasis..TERM_SUB_MASTER where TSM_ID=" + hTSM_ID.Value
        Dim status As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return status
    End Function

    Protected Sub ddlAgeBand_SelectedIndexChanged(sender As Object, e As EventArgs)
        GridBind()
    End Sub
End Class
