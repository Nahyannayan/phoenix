﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.IO

Partial Class Curriculum_clmUpdateLessonPlanner
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100026") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    FillAcd()

                    FillGrade()
                    FillTerm()
                    PopulateSyllabus()
                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub FillAcd()

        ddlAcademicYear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub
    Sub FillTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "select distinct TRM_ID,TRM_DESCRIPTION from SYL.SYLLABUS_M  inner join VW_TRM_M on SYM_TRM_ID =  TRM_ID " _
                    & " where sym_acd_id=" + ddlAcademicYear.SelectedValue + "  and SYM_GRD_ID ='" + ddlGrade.SelectedValue + "' and " _
                    & " sym_sbg_id ='" + ddlSubject.SelectedValue + "' "



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlTerm.Items.Insert(0, li)
    End Sub
    Sub FillGrade()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct GRM_GRD_ID, grm_display ,grd_displayorder FROM grade_bsu_m,grade_m WHERE " _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                                       & "  grm_acd_id=" + acdid
        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A  " _
                                 & " WHERE SBG_ACD_ID=" + acd_id





        str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'"



        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Return ddlSubject
    End Function
    Sub PopulateSyllabus()
        ddlSyllabus.Items.Clear()


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""




        If (ddlTerm.SelectedItem.Text = "ALL") Then
            str_query = "select SYM_ID,SYM_DESCR  from SYL.SYLLABUS_M  where SYM_BSU_ID=" + Session("sBsuid") + " and " _
                                      & " SYM_ACD_ID =" + ddlAcademicYear.SelectedValue + " and SYM_GRD_ID = '" + ddlGrade.SelectedValue + "' and " _
                                      & " SYM_SBG_ID = " + ddlSubject.SelectedValue + "   "
        Else
            str_query = "select SYM_ID,SYM_DESCR  from SYL.SYLLABUS_M  where SYM_BSU_ID=" + Session("sBsuid") + " and " _
                                        & " SYM_ACD_ID =" + ddlAcademicYear.SelectedValue + " and SYM_GRD_ID = '" + ddlGrade.SelectedValue + "' and " _
                                        & " SYM_SBG_ID = " + ddlSubject.SelectedValue + "   and SYM_TRM_ID = " + ddlTerm.SelectedValue


        End If




        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSyllabus.DataSource = ds
        ddlSyllabus.DataTextField = "SYM_DESCR"
        ddlSyllabus.DataValueField = "SYM_ID"
        ddlSyllabus.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSyllabus.Items.Insert(0, li)

    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        FillGrade()
        gridbind()
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateSyllabus()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'gridbind()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        FillTerm()
        PopulateSyllabus()
        gridbind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        FillTerm()
        PopulateSyllabus()
        gridbind()
    End Sub
    Protected Sub gvSyllabus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvSyllabus.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Sub gridbind()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""


            Dim ds As New DataSet



            str_Sql = "SELECT  SYL.SYLLABUS_M.SYM_ID , SYL.SYLLABUS_D.SYD_ID AS SydId,SYL.SYLLABUS_D.SYD_DESCR AS SYBDESC, " _
                      & " SYLLABUS_D_1.SYD_DESCR AS SYBPARENTDESC, SYL.SYLLABUS_D.SYD_STDT AS SYBSTARTDT, " _
                      & "  SYL.SYLLABUS_D.SYD_ENDDT AS SYBENDDT, SYL.SYLLABUS_D.SYD_TOT_HRS AS SYBTOTHRS " _
                     & " FROM SYL.SYLLABUS_M  inner join syl.SYLLABUS_D on SYM_ID=SYD_SYM_ID " _
                      & " LEFT OUTER JOIN   SYL.SYLLABUS_D AS SYLLABUS_D_1 ON   SYL.SYLLABUS_D.SYD_PARENT_ID = SYLLABUS_D_1.SYD_ID " _
                     & " WHERE sym_acd_id=" + ddlAcademicYear.SelectedValue + " "


            Dim strFilter As String = ""

            Dim strSearch As String = ""

            Dim txtSearch As New TextBox
            Dim optSearch As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String = ""
            Dim strFilter1 As String = ""
            Dim str_search As String = ""
            If ddlGrade.SelectedValue <> "0" Then
                str_Sql += " AND sym_grd_id='" + ddlGrade.SelectedValue.ToString + "'"
            End If

            If ddlSubject.SelectedValue <> "0" Then
                str_Sql += " AND sym_sbg_id='" + ddlSubject.SelectedValue.ToString + "'"
            End If

            If ddlTerm.SelectedValue <> "0" Then
                str_Sql += " AND SYLLABUS_M.SYM_trm_ID=" + ddlTerm.SelectedValue.ToString
            End If

            If ((ddlSyllabus.SelectedValue <> "0") And (ddlSyllabus.SelectedValue <> "")) Then
                str_Sql += " AND SYLLABUS_M.SYM_ID=" + ddlSyllabus.SelectedValue.ToString
            End If


            If gvSyllabus.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvSyllabus.HeaderRow.FindControl("txtOption")
                
                If txtSearch.Text <> "" Then

                    If str_search = "LI" Then
                        strFilter = " and SYL.SYLLABUS_D.SYD_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "NLI" Then
                        strFilter = "  AND  NOT  SYL.SYLLABUS_D.SYD_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "SW" Then
                        strFilter = " AND SYL.SYLLABUS_D.SYD_DESCR  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        strFilter = " AND SYL.SYLLABUS_D.SYD_DESCR NOT LIKE '" & txtSearch.Text & "%'"

                    ElseIf str_search = "EW" Then
                        strFilter = " AND SYL.SYLLABUS_D.SYD_DESCR LIKE  '%" & txtSearch.Text & "'"

                    ElseIf str_search = "NEW" Then
                        strFilter = " AND SYL.SYLLABUS_D.SYD_DESCR NOT LIKE '%" & txtSearch.Text & "'"

                    End If


                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If


                txtSearch1 = gvSyllabus.HeaderRow.FindControl("txtOption1")
              
                If txtSearch1.Text <> "" Then

                    If str_search = "LI" Then
                        strFilter1 = " and SYLLABUS_D_1.SYD_DESCR LIKE '%" & txtSearch1.Text & "%'"

                    ElseIf str_search = "NLI" Then
                        strFilter1 = "  AND  NOT  SYLLABUS_D_1.SYD_DESCR LIKE '%" & txtSearch1.Text & "%'"

                    ElseIf str_search = "SW" Then
                        strFilter1 = " AND SYLLABUS_D_1.SYD_DESCR  LIKE '" & txtSearch1.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        strFilter1 = " AND SYLLABUS_D_1.SYD_DESCR NOT LIKE '" & txtSearch1.Text & "%'"

                    ElseIf str_search = "EW" Then
                        strFilter1 = " AND SYLLABUS_D_1.SYD_DESCR LIKE  '%" & txtSearch1.Text & "'"

                    ElseIf str_search = "NEW" Then
                        strFilter1 = " AND SYLLABUS_D_1.SYD_DESCR NOT LIKE '%" & txtSearch1.Text & "'"

                    End If

                    optSearch1 = txtSearch1.Text

                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If
                End If
                str_Sql += "order by  ISNULL(SYL.SYLLABUS_D.SYD_ORDER,0)"

                ''    Dim str_Sid_search() As String
                ''    str_Sid_search = h_Selected_menu_1.Value.Split("__")
                ''    str_search = str_Sid_search(0)
                ''    txtSearch2 = gvSyllabus.HeaderRow.FindControl("txtSubject")
                ''    txtSearch = gvSyllabus.HeaderRow.FindControl("txtSyllabus")
                ''    str_CAM_DESC = txtSearch.Text
                ''    If txtSearch.Text <> "" Then
                ''        If str_search = "LI" Then
                ''            str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR LIKE '%" & txtSearch.Text & "%'"

                ''        ElseIf str_search = "NLI" Then
                ''            str_filter_CAM_DESC = "  AND  NOT SYL.SYLLABUS_M.SYM_DESCR LIKE '%" & txtSearch.Text & "%'"

                ''        ElseIf str_search = "SW" Then
                ''            str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR  LIKE '" & txtSearch.Text & "%'"
                ''        ElseIf str_search = "NSW" Then
                ''            str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR NOT LIKE '" & txtSearch.Text & "%'"

                ''        ElseIf str_search = "EW" Then
                ''            str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR LIKE  '%" & txtSearch.Text & "'"

                ''        ElseIf str_search = "NEW" Then
                ''            str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR NOT LIKE '%" & txtSearch.Text & "'"

                ''        End If
                ''    End If

                ''    If txtSearch2.Text <> "" Then
                ''        If str_search = "LI" Then

                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        ElseIf str_search = "NLI" Then

                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        ElseIf str_search = "SW" Then
                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        ElseIf str_search = "NSW" Then

                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        ElseIf str_search = "EW" Then

                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        ElseIf str_search = "NEW" Then

                ''            str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                ''        End If
                ''    End If

                ''End If

                'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAM_DESC & " ORDER BY CAM_DESC")

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then

                    gvSyllabus.DataSource = ds.Tables(0)
                    gvSyllabus.DataBind()

                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    gvSyllabus.DataSource = ds.Tables(0)
                    Try
                        gvSyllabus.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvSyllabus.Rows(0).Cells.Count

                    gvSyllabus.Rows(0).Cells.Clear()
                    gvSyllabus.Rows(0).Cells.Add(New TableCell)
                    gvSyllabus.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvSyllabus.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvSyllabus.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                End If
                txtSearch = New TextBox
                txtSearch = gvSyllabus.HeaderRow.FindControl("txtOption")
                txtSearch.Text = optSearch

                txtSearch1 = New TextBox
                txtSearch1 = gvSyllabus.HeaderRow.FindControl("txtOption1")
                txtSearch1.Text = optSearch1

                'txtSearch = gvclmSyllabus.HeaderRow.FindControl("txtCAM_DESC")
                'txtSearch.Text = str_CAM_DESC


            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnempid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnuid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSyllabus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSyllabus.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub gvSyllabus_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSyllabus.RowDataBound
        Try


           
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddl = DirectCast(e.Row.FindControl("drTerm"), DropDownList)
                Dim lbl = DirectCast(e.Row.FindControl("lblID"), Label)
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String
                ddl.Items.Clear()
                str_query = "select distinct TRM_ID,TRM_DESCRIPTION from VW_TRM_M where TRM_ACD_ID= " + ddlAcademicYear.SelectedValue



                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                ddl.DataSource = ds
                ddl.DataTextField = "TRM_DESCRIPTION"
                ddl.DataValueField = "TRM_ID"
                ddl.DataBind()

                str_query = "select distinct TRM_ID,TRM_DESCRIPTION from syl.SYLLABUS_m inner join VW_TRM_M  on SYM_TRM_ID =TRM_ID and SYM_ID= " & Val(lbl.Text)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                Dim li As New ListItem
                li.Text = ds.Tables(0).Rows(0).Item(1)
                li.Value = ds.Tables(0).Rows(0).Item(0)

                ddl.Items(ddl.Items.IndexOf(li)).Selected = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvSyllabus_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSyllabus.RowCommand


        '  If IsPostBack = False Then
        Try
            '    Dim url As String

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvSyllabus.Rows(index), GridViewRow)

            If e.CommandName = "Update" Then
                Dim lblid As New Label
                Dim lblSYDID As New Label
                Dim txtFDATE As New TextBox
                Dim txtTDATE As New TextBox
                Dim txtHRS As New TextBox
                Dim ddl As New DropDownList

                lblid = selectedRow.FindControl("lblID")
                lblSYDID = selectedRow.FindControl("lblsydID")

                txtFDATE = selectedRow.FindControl("txtFrom")
                txtTDATE = selectedRow.FindControl("txtTo")
                txtHRS = selectedRow.FindControl("lblTotalHrs")
                ddl = selectedRow.FindControl("drTerm")
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String


                str_query = "exec syl.UpdateSYLLABUS_D " & lblSYDID.Text & " ," & lblid.Text & ",'" & txtFDATE.Text & "','" & txtTDATE.Text & "'," & txtHRS.Text & "," & ddl.SelectedValue

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                lblError.Text = "Updated..."


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        ' End If
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try


            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""





            For Each row As GridViewRow In gvSyllabus.Rows
                Dim lblid As New Label
                Dim lblSYDID As New Label
                Dim txtFDATE As New TextBox
                Dim txtTDATE As New TextBox
                Dim txtHRS As New TextBox
                Dim ddl As New DropDownList

                lblid = row.FindControl("lblID")
                lblSYDID = row.FindControl("lblsydID")
                txtFDATE = row.FindControl("txtFrom")
                txtTDATE = row.FindControl("txtTo")
                txtHRS = row.FindControl("lblTotalHrs")
                ddl = row.FindControl("drTerm")
                str_query = "exec syl.UpdateSYLLABUS_D " & lblSYDID.Text & " ," & lblid.Text & ",'" & txtFDATE.Text & "','" & txtTDATE.Text & "'," & txtHRS.Text & "," & ddl.SelectedValue

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

            lblError.Text = "Updated..."
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   
    Protected Sub gvSyllabus_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvSyllabus.RowUpdating

    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvSyllabus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvSyllabus.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSyllabus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvSyllabus.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub
End Class
