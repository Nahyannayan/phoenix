Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class Curriculum_clmReportRule_M
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") = "add" Then
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                        BindReportCard()
                        BindPrintedFor()
                        BindGrade()
                        BindTerm()
                        BindHeader()
                        BindGradingSlab()
                        Session("dtRules") = SetDataTable()
                        DisplayControls(True)
                    Else
                        hfRRM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rrmid").Replace(" ", "+"))
                        GetData()
                        DisplayControls(False)
                    End If


                    ddlActivity = currFns.PopulateActivityMaster(ddlActivity, Session("sbsuid"))
                    BindActivities()
                    GetGradeSlabTotMark()
                End If

            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        End If
    End Sub

    Protected Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSbgId As Label = TryCast(sender.FindControl("lblSbgId"), Label)
        h_SBG_IDs.Value = h_SBG_IDs.Value.Replace(lblSbgId.Text + "___", "")
        h_SBG_IDs.Value = h_SBG_IDs.Value.Replace("___" + lblSbgId.Text, "")
        If h_SBG_IDs.Value.IndexOf("_") = -1 Then
            h_SBG_IDs.Value = "0"
        End If
        BindSubject()
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
        BindBFinalReport()
    End Sub

    Sub BindBFinalReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M " _
                                 & " WHERE RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        hfbFinalReport.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.ToLower
    End Sub
    Sub BindPrintedFor()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindTerm()
        ddlTerm.Items.Clear()
        If hfbFinalReport.Value = "false" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlTerm.DataSource = ds
            ddlTerm.DataTextField = "TRM_DESCRIPTION"
            ddlTerm.DataValueField = "TRM_ID"
            ddlTerm.DataBind()
        Else
            Dim li As New ListItem
            li.Text = "TERM FINAL"
            li.Value = "0"
            ddlTerm.Items.Add(li)
        End If
    End Sub


    Sub BindSubject()
        Dim strSbgIds As String = h_SBG_IDs.Value.Replace("___", ",")
        If strSbgIds <> "" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_DISPLAY FROM SUBJECTS_GRADE_S " _
                                   & " AS A INNER JOIN OASIS..GRADE_BSU_M AS B ON A.SBG_GRD_ID=B.GRM_GRD_ID" _
                                   & " AND A.SBG_ACD_ID=B.GRM_ACD_ID" _
                                   & " WHERE SBG_ID IN(" + strSbgIds + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvSubjects.DataSource = ds
            gvSubjects.DataBind()
        End If
    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND RSD_bDIRECTENTRY='FALSE' AND ISNULL(RSD_bFINALREPORT,'FALSE')='FALSE' ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub

    Sub BindActivities()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAD_ID,CAD_DESC,WTG='' FROM ACT.ACTIVITY_D WHERE CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ddlTerm.SelectedValue <> "0" Then
            str_query += " AND CAD_TRM_ID=" + ddlTerm.SelectedValue.ToString _
                        & " AND CAD_CAM_ID=" + ddlActivity.SelectedValue.ToString
            ' tbAct.Rows(0).Visible = True
            tdAc.Visible = True
            tdAs.Visible = True
        Else
            tdAc.Visible = False
            tdAs.Visible = False
            '  tbAct.Rows(0).Visible = False
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOperations.DataSource = ds
        gvOperations.DataBind()
        gvOperations.Columns(3).Visible = False
    End Sub

    Sub BindGradingSlab()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GSM_SLAB_ID,GSM_DESC FROM ACT.GRADING_SLAB_M WHERE GSM_BSU_ID='" + Session("SBSUID") + "'  AND ISNULL(GSM_bDISPLAY,'TRUE')='TRUE'" _
                                & " ORDER BY GSM_DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGradingSlab.DataSource = ds
        ddlGradingSlab.DataTextField = "GSM_DESC"
        ddlGradingSlab.DataValueField = "GSM_SLAB_ID"
        ddlGradingSlab.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlGradingSlab.Items.Insert(0, li)
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "CAM_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "IDS"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OPT"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RULES"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "WEIGHT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "WTS"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function
    Sub AddEditRule()
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim dt As New DataTable
        dt = Session("dtRules")
        Dim dr As DataRow
        Dim ids As String = ""
        Dim rule As String = ""
        Dim lblCadId As Label
        Dim lblDesc As Label
        Dim wts As String = ""
        Dim txtWT As TextBox
        With gvOperations
            For i = 0 To .Rows.Count - 1
                chkSelect = .Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblCadId = .Rows(i).FindControl("lblCadId")
                    lblDesc = .Rows(i).FindControl("lblDesc")

                    If ids <> "" Then
                        ids += "|"
                    End If
                    ids += lblCadId.Text

                  
                    If rule <> "" Then
                        rule += ", "
                    End If

                    If ddlOperation.SelectedValue = "WT" Then
                        txtWT = .Rows(i).FindControl("txtWT")
                        If wts <> "" Then
                            wts += "|"
                        End If
                        wts += lblCadId.Text + "_" + txtWT.Text.ToString
                        rule += txtWT.Text + "% of " + lblDesc.Text
                    Else
                        rule += lblDesc.Text
                    End If


                End If
            Next
        End With

        Dim keys As Object()
        ReDim keys(0)

        keys(0) = ddlActivity.SelectedValue.ToString
        Dim row As DataRow = dt.Rows.Find(keys)
     
        If Not row Is Nothing Then
            dt.Rows.Find(keys).Item(0) = ddlActivity.SelectedValue.ToString
            dt.Rows.Find(keys).Item(1) = ids
            dt.Rows.Find(keys).Item(2) = ddlOperation.SelectedValue

            dt.Rows.Find(keys).Item(3) = ddlOperation.SelectedItem.Text + "(" + rule + ")"
            ' dt.Rows.Find(keys).Item(4) = 
            dt.Rows.Find(keys).Item(5) = wts
        Else
            dr = dt.NewRow
            dr.Item(0) = ddlActivity.SelectedValue.ToString
            dr.Item(1) = ids
            dr.Item(2) = ddlOperation.SelectedValue
            dr.Item(3) = ddlOperation.SelectedItem.Text + "(" + rule + ")"
            ' dr.Item(4) = 
            dr.Item(5) = wts
            dt.Rows.Add(dr)
        End If

        gvRule.DataSource = dt
        gvRule.DataBind()
        Session("dtRules") = dt
    End Sub

    Sub AddData()
        Dim i As Integer
        Dim str_query As String = ""
        Dim lblCamId As Label
        Dim lblCadIds As Label
        Dim lblOpt As Label
        Dim txtWT As TextBox
        Dim lblWeight As Label
        Dim transaction As SqlTransaction
        Dim j As Integer
        Dim lblSbgId As Label
        Dim rrm_id As Integer
        Dim cadIds As String()
        Dim wts As String()
        Dim cadid As String
        Dim wt As String
        Dim x As Integer
        Dim rss_id As Integer
        Dim opt As String = ""
        Dim gradeWt As Double

        For j = 0 To gvSubjects.Rows.Count - 1

            lblSbgId = gvSubjects.Rows(j).FindControl("lblSbgId")

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    'Save RULE_M
                    ' gradeWt = GetGradeSlabWeigtage()

                    str_query = "EXEC RPT.saveREPORTRULE_M " _
                                & "0," _
                                & lblSbgId.Text + "," _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & ddlReportCard.SelectedValue.ToString + "," _
                                & ddlPrintedFor.SelectedValue.ToString + "," _
                                & ddlHeader.SelectedValue.ToString + "," _
                                & ddlTerm.SelectedValue.ToString + "," _
                                & "'" + txtDescr.Text + "'," _
                                & IIf(ddlGradingSlab.SelectedValue.ToString = "0", "NULL", ddlGradingSlab.SelectedValue.ToString) + "," _
                                & "'" + ddlLevel.SelectedValue.ToString + "'," _
                                & Val(txtPassMark.Text).ToString + "," _
                                & Val(txtMaxMarks.Text).ToString + "," _
                                & IIf(txtGradeWt.Text = "", "1", txtGradeWt.Text.ToString) + "," _
                                & "'add'"

                    rrm_id = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                    With gvRule
                        For i = 0 To .Rows.Count - 1
                            lblCamId = .Rows(i).FindControl("lblCamId")
                            lblCadIds = .Rows(i).FindControl("lblCadIds")
                            lblOpt = .Rows(i).FindControl("lblOpt")
                            txtWT = .Rows(i).FindControl("txtWT")
                            lblWeight = .Rows(i).FindControl("lblWeight")
                            If lblOpt.Text = "WT" Then
                                opt = "WEIGHTAGE"
                            ElseIf lblOpt.Text = "MAX" Then
                                opt = "BEST OF"
                            Else
                                opt = lblOpt.Text
                            End If
                            str_query = "RPT.saveREPORTSCHEDULE_M " _
                                      & rrm_id.ToString + "," _
                                      & lblCamId.Text + "," _
                                      & "'" + IIf(lblOpt.Text = "WT", "SUM", lblOpt.Text) + "'," _
                                      & IIf(txtWT.Text = "", "100", txtWT.Text) + "," _
                                      & "'" + opt + "'," _
                                      & "'ADD'"

                            rss_id = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                            If lblOpt.Text <> "WT" Then
                                cadIds = lblCadIds.Text.Split("|")
                            Else
                                cadIds = lblWeight.Text.Split("|")
                            End If
                            For x = 0 To cadIds.Length - 1
                                If lblOpt.Text = "WT" Then
                                    wts = cadIds(x).Split("_")
                                    cadid = wts(0)
                                    wt = wts(1)
                                Else
                                    cadid = cadIds(x)
                                    wt = "100"
                                End If

                                str_query = "RPT.saveREPORTSCHEDULE_D " _
                                        & rrm_id.ToString + "," _
                                        & rss_id.ToString + "," _
                                        & lblCamId.Text + "," _
                                        & cadid + "," _
                                        & IIf(wt = "0", "100", wt) + "," _
                                        & "'add'"

                                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                            Next

                        Next
                    End With
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using

        Next

    End Sub

    Sub GetGradeSlabTotMark()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(GSM_TOT_MARK,0) FROM ACT.GRADING_SLAB_M WHERE GSM_SLAB_ID=" + ddlGradingSlab.SelectedValue.ToString
        hfGSM_TOTmark.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If hfGSM_TOTmark.Value = "" Then
            hfGSM_TOTmark.Value = "0"
        End If
    End Sub
    Function GetGradeSlabWeigtage() As Double
        Dim totalMarks As Double = 0
        Dim maxMarks As Double = CDbl(hfGSM_TOTmark.Value)
        Dim txtWt As TextBox
        If ddlGradingSlab.SelectedValue = "0" Then
            Return 1
        Else
            Dim i As Integer
            For i = 0 To gvRule.Rows.Count - 1
                txtWt = gvRule.Rows(i).FindControl("txtWt")
                totalMarks += Val(txtWt.Text)
            Next

            If totalMarks = 0 Then
                Return 1
            Else
                Return maxMarks / totalMarks
            End If
        End If
    End Function

    Sub EditData()
        Dim i As Integer
        Dim str_query As String = ""
        Dim lblCamId As Label
        Dim lblCadIds As Label
        Dim lblOpt As Label
        Dim txtWT As TextBox
        Dim lblWeight As Label
        Dim transaction As SqlTransaction
        Dim j As Integer
        Dim lblSbgId As Label
        Dim rrm_id As Integer
        Dim cadIds As String()
        Dim wts As String()
        Dim cadid As String
        Dim wt As String
        Dim x As Integer
        Dim rss_id As Integer
        Dim opt As String = ""
        Dim gradeWt As Double

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                'Save RULE_M
                ' gradeWt = GetGradeSlabWeigtage()

                str_query = "EXEC RPT.saveREPORTRULE_M " _
                            & hfRRM_ID.Value + "," _
                            & hfSBG_ID.Value + "," _
                            & ddlAcademicYear.SelectedValue.ToString + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & ddlPrintedFor.SelectedValue.ToString + "," _
                            & ddlHeader.SelectedValue.ToString + "," _
                            & ddlTerm.SelectedValue.ToString + "," _
                            & "'" + txtDescr.Text + "'," _
                            & IIf(ddlGradingSlab.SelectedValue.ToString = "0", "NULL", ddlGradingSlab.SelectedValue.ToString) + "," _
                            & "'" + ddlLevel.SelectedValue.ToString + "'," _
                            & Val(txtPassMark.Text).ToString + "," _
                            & Val(txtMaxMarks.Text).ToString + "," _
                            & IIf(txtGradeWt.Text = "", "1", txtGradeWt.Text.ToString) + "," _
                            & "'edit'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                With gvRule
                    For i = 0 To .Rows.Count - 1
                        lblCamId = .Rows(i).FindControl("lblCamId")
                        lblCadIds = .Rows(i).FindControl("lblCadIds")
                        lblOpt = .Rows(i).FindControl("lblOpt")
                        txtWT = .Rows(i).FindControl("txtWT")
                        lblWeight = .Rows(i).FindControl("lblWeight")
                        If lblOpt.Text = "WT" Then
                            opt = "WEIGHTAGE"
                        ElseIf lblOpt.Text = "MAX" Then
                            opt = "BEST OF"
                        Else
                            opt = lblOpt.Text
                        End If
                        str_query = "RPT.saveREPORTSCHEDULE_M " _
                                  & hfRRM_ID.Value + "," _
                                  & lblCamId.Text.Trim + "," _
                                  & "'" + IIf(lblOpt.Text = "WT", "SUM", lblOpt.Text) + "'," _
                                  & IIf(txtWT.Text = "", "100", txtWT.Text) + "," _
                                  & "'" + opt + "'," _
                                  & "'ADD'"

                        rss_id = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                        If lblOpt.Text <> "WT" Then
                            cadIds = lblCadIds.Text.Split("|")
                        Else
                            cadIds = lblWeight.Text.Split("|")
                        End If
                        For x = 0 To cadIds.Length - 1
                            If lblOpt.Text = "WT" Then
                                wts = cadIds(x).Split("_")
                                cadid = wts(0)
                                wt = wts(1)
                            Else
                                cadid = cadIds(x)
                                wt = "100"
                            End If

                            str_query = "RPT.saveREPORTSCHEDULE_D " _
                                    & hfRRM_ID.Value + "," _
                                    & rss_id.ToString + "," _
                                    & lblCamId.Text.Trim + "," _
                                    & cadid + "," _
                                    & IIf(wt = "0", "100", wt) + "," _
                                    & "'add'"

                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        Next

                    Next
                End With
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub GetData()
        Dim li As New ListItem

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT ACD_ID,ACY_DESCR,RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR,RSD_ID,RSD_HEADER, " _
                                & " SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_GRD_ID,GRM_DISPLAY,ISNULL(GSM_SLAB_ID,0),ISNULL(GSM_DESC,'--'),ISNULL(TRM_ID,0),ISNULL(TRM_DESCRIPTION,'FINAL')," _
                                & " RRM_TYPE, RRM_DESCR,RRM_ID,ISNULL(RRM_PASSMARK,0),ISNULL(RRM_MAXMARK,0),ISNULL(RRM_GRADE_WEIGTAGE,0) FROM RPT.REPORT_RULE_M AS A " _
                                & " INNER JOIN OASIS..ACADEMICYEAR_D AS B ON A.RRM_ACD_ID=B.ACD_ID" _
                                & " INNER JOIN OASIS..ACADEMICYEAR_M AS C ON B.ACD_ACY_ID=C.ACY_ID" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS D ON A.RRM_RSM_ID=D.RSM_ID" _
                                & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS E ON A.RRM_RPF_ID=E.RPF_ID" _
                                & " INNER JOIN RPT.REPORT_SETUP_D AS F ON A.RRM_RSD_ID=F.RSD_ID" _
                                & " INNER JOIN OASIS..GRADE_BSU_M AS G ON A.RRM_GRD_ID=G.GRM_GRD_ID AND B.ACD_ID=G.GRM_ACD_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS H ON A.RRM_SBG_ID=H.SBG_ID " _
                                & " LEFT OUTER JOIN VW_TRM_M AS I ON A.RRM_TRM_ID=I.TRM_ID " _
                                & " LEFT OUTER JOIN ACT.GRADING_SLAB_M AS J ON A.RRM_GSM_SLAB_ID=J.GSM_SLAB_ID" _
                                & " WHERE RRM_ID=" + hfRRM_ID.Value

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        With reader
            While .Read
                li.Value = .GetValue(0)
                li.Text = .GetString(1)
                ddlAcademicYear.Items.Add(li)

                li = New ListItem
                li.Value = .GetValue(2)
                li.Text = .GetString(3)
                ddlReportCard.Items.Add(li)

                li = New ListItem
                li.Value = .GetValue(4)
                li.Text = .GetString(5)
                ddlPrintedFor.Items.Add(li)

                li = New ListItem
                li.Value = .GetValue(6)
                li.Text = .GetString(7)
                ddlHeader.Items.Add(li)

                hfSBG_ID.Value = .GetValue(8)
                txtSubject.Text = .GetString(9)

                hfGRD_ID.Value = .GetValue(10)
                lblGrade.Text = .GetString(11)

                BindGradingSlab()
                li = New ListItem
                li.Value = .GetValue(12)
                li.Text = .GetString(13)
                If Not ddlGradingSlab.Items.FindByValue(.GetValue(12)) Is Nothing Then
                    ddlGradingSlab.Items.FindByValue(.GetValue(12)).Selected = True
                End If

                li = New ListItem
                li.Value = .GetValue(14)
                li.Text = .GetString(15)
                ddlTerm.Items.Add(li)

                ddlLevel.Items.FindByValue(.GetString(16).Trim).Selected = True

                txtDescr.Text = .GetString(17)
                hfRRM_ID.Value = .GetValue(18)
                txtPassMark.Text = .GetValue(19).ToString.Replace(".00", "")
                txtMaxMarks.Text = .GetValue(20).ToString.Replace(".00", "")
                txtGradeWt.Text = .GetValue(21).ToString.Replace(".000", "")
            End While
        End With
        reader.Close()


        str_query = "SELECT RSS_CAM_ID AS CAM_ID," _
                   & " IDS=(select STUFF((SELECT '|' + convert(varchar(100),RRD_CAD_ID)" _
                   & " from RPT.REPORT_SCHEDULE_D where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))," _
                   & " WTS=(select STUFF((SELECT '|' + convert(varchar(100),RRD_CAD_ID)+'_'+convert(varchar(100),RRD_WEIGHTAGE) " _
                   & " from RPT.REPORT_SCHEDULE_D where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))," _
                   & " RULES=RSS_OPRT_DISPLAY+'('+(select STUFF((SELECT ',' + CASE A.RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN" _
                   & " convert(varchar(100),RRD_WEIGHTAGE)+'% of '+convert(varchar(100),CAD_DESC) ELSE convert(varchar(100),CAD_DESC) END" _
                   & " from RPT.REPORT_SCHEDULE_D AS P INNER JOIN ACT.ACTIVITY_D AS Q ON P.RRD_CAD_ID=Q.CAD_ID" _
                   & " where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))+')'," _
                   & " OPT=CASE RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN 'WT' WHEN 'BEST OF' THEN 'MAX' ELSE RSS_OPRT_DISPLAY END,WEIGHT=RSS_WEIGHTAGE" _
                   & " FROM RPT.REPORT_SCHEDULE_M AS A WHERE RSS_RRM_ID=" + hfRRM_ID.Value + " ORDER BY CAM_ID"

        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
       
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetValue(0)
            dr.Item(1) = reader.GetString(1)
            dr.Item(2) = reader.GetString(4)
            dr.Item(3) = reader.GetString(3)
            dr.Item(4) = reader.GetValue(5)
            dr.Item(5) = reader.GetString(2)
            dt.Rows.Add(dr)
        End While


        gvRule.DataSource = dt
        gvRule.DataBind()

        Session("dtRules") = dt
    End Sub

    Sub DisplayControls(ByVal value As Boolean)
        imgSub.Visible = value
        gvSubjects.Visible = value
        lblGrade.Visible = Not value
        lstGrade.Visible = value
    End Sub
    Function CheckRulesAddedSubjects() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT COUNT(RRM_ID) FROM RPT.REPORT_RULE_M WHERE RRM_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                 & " AND RRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND RRM_RSD_ID=" + ddlHeader.SelectedValue.ToString _
                 & " AND RRM_TRM_ID=" + ddlTerm.SelectedValue.ToString
        Dim i As Integer
        'Dim ds As DataSet
        Dim lblSbgId As Label
        Dim lblSubject As Label
        Dim str_query1 As String
        Dim count As Integer
        Dim subjects As String = ""

        For i = 0 To gvSubjects.Rows.Count - 1
            lblSbgId = gvSubjects.Rows(i).FindControl("lblSbgId")
            lblSubject = gvSubjects.Rows(i).FindControl("lblSubject")
            str_query1 = str_query + " AND RRM_SBG_ID=" + lblSbgId.Text
            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query1)
            If count <> 0 Then
                If subjects <> "" Then
                    subjects += ","
                End If
                subjects += lblSubject.Text
            End If
        Next

        If subjects <> "" Then
            lblError.Text = "Rules for the subject(s) " + subjects + " is already  created for " + ddlHeader.SelectedItem.Text + " for the term " + ddlTerm.SelectedItem.Text
            'lblError.Focus()
            Return False
        End If

        Return True

    End Function
#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindBFinalReport()
        BindTerm()
        BindPrintedFor()
        BindGrade()
        BindHeader()
        If hfbFinalReport.Value = "true" Then
            BindActivities()
            gvRule.DataBind()
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        BindTerm()
        BindGradingSlab()
        BindHeader()
        ddlActivity = currFns.PopulateActivityMaster(ddlActivity, Session("sbsuid"))
        BindActivities()
        Session("dtRules") = SetDataTable()
        DisplayControls(True)
        gvRule.DataBind()

    End Sub

    Protected Sub lstGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrade.SelectedIndexChanged
        Dim strSelectedGrades As String = String.Empty
        Dim strSeperator As String = String.Empty
        For Each lstItem As ListItem In lstGrade.Items
            If lstItem.Selected Then
                strSelectedGrades += strSeperator + lstItem.Value
                strSeperator = "___"
            End If
        Next
        h_GRD_IDs.Value = strSelectedGrades
    End Sub

    Protected Sub imgSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSub.Click
        BindSubject()
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActivity.SelectedIndexChanged
        BindActivities()
        If ddlOperation.SelectedValue = "WT" Then
            gvOperations.Columns(3).Visible = True
        End If
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        BindActivities()
        gvRule.DataBind()
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        AddEditRule()
    End Sub

    Protected Sub ddlOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperation.SelectedIndexChanged
        If ddlOperation.SelectedValue = "WT" Then
            gvOperations.Columns(3).Visible = True
        Else
            gvOperations.Columns(3).Visible = False
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtDescr.Text = "" Then
            lblError.Text = "Please enter the field description"
        End If
        If ViewState("datamode") = "add" Then
            If CheckRulesAddedSubjects() = True Then
                AddData()
            Else
                Exit Sub
            End If
        Else
            EditData()
        End If
    End Sub

    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\clmReportRule_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("dtRules") = SetDataTable()
        gvRule.DataSource = Session("dtRules")
        gvRule.DataBind()
    End Sub

    Protected Sub ddlGradingSlab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGradingSlab.SelectedIndexChanged
        GetGradeSlabTotMark()
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        BindSubject()

    End Sub

    Protected Sub txtSubject_TextChanged(sender As Object, e As EventArgs)
        txtSubject.Text = ""
        BindSubject()
    End Sub
End Class
