﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Partial Class Curriculum_clmExamSetup_Approve
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'SendEmailToParent("nikunj.finesse@Gemseducation.com", "nikunj.finesse@Gemseducation.com", "Test Email")
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S888001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    rbPending.Checked = True
                    studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    BindGrade()

                    btnApprove.Visible = False
                    btnReject.Visible = False
                    'GridBind()
                    gvExamSetup_Approve.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnApprove)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReject)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSearch)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GridBind()
    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SaveData("Approve", False, "")
        GridBind()
        ' SendNotificationEmail("approved")
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        pnl_Approve.Visible = True
        ' SaveData("Reject", -1)
        'GridBind()
        ' SendNotificationEmail("rejected")
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        'GridBind()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        'GridBind()
    End Sub
    Sub SaveData(ByVal Status As String, ByVal IsConfirm As Boolean, ByVal remark As String)
        Try
            ViewState("VS_EmailNPaperName") = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim i As Integer
            'ViewState("VS_EmailNPaperName") = "nikunj@gmail.com|Subject 1"
            Dim lblESA_ID As Label
            Dim lblESA_STU_ID As Label
            Dim lblESA_ES_ID As Label
            Dim hdn_PARENT_EMAIL As HiddenField
            Dim hdn_ExamPaper As HiddenField
            Dim chkChild As CheckBox
            For i = 0 To gvExamSetup_Approve.Rows.Count - 1
                With gvExamSetup_Approve.Rows(i)
                    hdn_PARENT_EMAIL = .FindControl("hdn_PARENT_EMAIL")
                    hdn_ExamPaper = .FindControl("hdn_ExamPaper")
                    lblESA_ID = .FindControl("lblESA_ID")
                    chkChild = .FindControl("chkChild")
                    lblESA_STU_ID = .FindControl("lblESA_STU_ID")
                    lblESA_ES_ID = .FindControl("lblESA_ES_ID")
                    If chkChild.Checked = True Then

                        'Add email id and subject name for send mail notification.
                        Dim EmailNPaperName As String = hdn_PARENT_EMAIL.Value & "|" & hdn_ExamPaper.Value
                        If ViewState("VS_EmailNPaperName") = "" Then
                            ViewState("VS_EmailNPaperName") = EmailNPaperName
                        Else
                            ViewState("VS_EmailNPaperName") = ViewState("VS_EmailNPaperName") & "," & EmailNPaperName
                        End If


                        Dim cmd As New SqlCommand
                        Dim objConn As New SqlConnection(str_conn)
                        objConn.Open()
                        cmd = New SqlCommand("[dbo].[saveExamSetup_Approve]", objConn)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("@ESA_STATUS", Status)
                        cmd.Parameters.AddWithValue("@ESA_USER_ID", Session("sUsr_id"))
                        cmd.Parameters.AddWithValue("@ESA_ID", lblESA_ID.Text)
                        cmd.Parameters.AddWithValue("@ESA_DATE", String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now))
                        cmd.Parameters.AddWithValue("@IsConfirm", IsConfirm)
                        cmd.Parameters.AddWithValue("@ESA_STU_ID", lblESA_STU_ID.Text)
                        cmd.Parameters.AddWithValue("@Acd_ID", ddlAcademicYear.SelectedValue)
                        cmd.Parameters.AddWithValue("@ESA_ES_ID", lblESA_ES_ID.Text)
                        cmd.Parameters.AddWithValue("@MODE", "UPDATE_STATUS")
                        cmd.Parameters.AddWithValue("@REMARKS", remark)
                        cmd.ExecuteNonQuery()
                        cmd.Dispose()
                        objConn.Close()
                    End If
                End With
            Next

            'ViewState("VS_EmailNPaperName") = ViewState("VS_EmailNPaperName") & "," & "ABCD@gmail.com|ACCOUNT PAPER 1"
            'ViewState("VS_EmailNPaperName") = ViewState("VS_EmailNPaperName") & "," & "ABCD@gmail.com|ACCOUNT PAPER 2"
            'ViewState("VS_EmailNPaperName") = ViewState("VS_EmailNPaperName") & "," & "ABCD@gmail.com|ECONOMY PAPER 3"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim ds As New DataSet
            Dim strStatus As String = String.Empty
            If rbAll.Checked = True Then
                strStatus = ""
            ElseIf rbPending.Checked = True Then
                strStatus = "Pending"
            ElseIf rbApprove.Checked = True Then
                strStatus = "Approve"
            ElseIf rbReject.Checked = True Then
                strStatus = "Reject"
            End If

            Dim aryGrade() As String
            aryGrade = ddlGrade.SelectedValue.Split("|")
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MODE", "GET_DISPLAY_DATA")
            pParms(1) = New SqlClient.SqlParameter("@Acd_ID", ddlAcademicYear.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@Grade_ID", aryGrade(0))
            pParms(3) = New SqlClient.SqlParameter("@FromDate", txtFromDate.Text.ToString)
            pParms(4) = New SqlClient.SqlParameter("@ToDate", txtToDate.Text.ToString)
            pParms(5) = New SqlClient.SqlParameter("@ESA_STATUS", strStatus)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[saveExamSetup_Approve]", pParms)

            If ds.Tables(0).Rows.Count > 0 Then

                gvExamSetup_Approve.DataSource = ds.Tables(0)
                gvExamSetup_Approve.DataBind()

                btnApprove.Visible = True
                btnReject.Visible = True
            Else
                gvExamSetup_Approve.DataSource = Nothing
                gvExamSetup_Approve.DataBind()

                btnApprove.Visible = False
                btnReject.Visible = False

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvExamSetup_Approve.DataSource = ds.Tables(0)
                Try
                    gvExamSetup_Approve.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvExamSetup_Approve.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvExamSetup_Approve.Rows(0).Cells.Clear()
                gvExamSetup_Approve.Rows(0).Cells.Add(New TableCell)
                gvExamSetup_Approve.Rows(0).Cells(0).ColumnSpan = columnCount
                gvExamSetup_Approve.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvExamSetup_Approve.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                              & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
        ddlGrade.ClearSelection()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = " |0"
        ddlGrade.Items.Insert(0, li)
    End Sub
    Protected Sub OnDataBound(sender As Object, e As EventArgs)
        For i As Integer = gvExamSetup_Approve.Rows.Count - 1 To 1 Step -1
            Dim row As GridViewRow = gvExamSetup_Approve.Rows(i)
            Dim previousRow As GridViewRow = gvExamSetup_Approve.Rows(i - 1)

            For j As Integer = 0 To row.Cells.Count - 10
                If row.Cells(j).Text = previousRow.Cells(j).Text Then
                    If previousRow.Cells(j).RowSpan = 0 Then
                        If row.Cells(j).RowSpan = 0 Then
                            previousRow.Cells(j).RowSpan += 2
                        Else
                            previousRow.Cells(j).RowSpan = row.Cells(j).RowSpan + 1
                        End If
                        row.Cells(j).Visible = False
                    End If
                End If
            Next
        Next
    End Sub
    Sub SendNotificationEmail(ByVal Flag As String)
        If Not ViewState("VS_EmailNPaperName") = Nothing Then
            Try
                Dim EmailBody As String = String.Empty
                Dim dtEmailList As DataTable
                dtEmailList = New DataTable("TblEmailList")

                Dim EmailID As DataColumn = New DataColumn("EmailID", GetType(String))
                Dim SubjectList As DataColumn = New DataColumn("SubjectList", GetType(String))

                dtEmailList.Columns.Add(EmailID)
                dtEmailList.Columns.Add(SubjectList)

                Dim UniqueEmail As String() = ViewState("VS_EmailNPaperName").ToString().Split(",")
                Dim Row1 As DataRow
                For i = 0 To UniqueEmail.Length - 1
                    Dim spltData As String() = UniqueEmail(i).Split("|")

                    'Add data in DataTable
                    Row1 = dtEmailList.NewRow()
                    Row1.Item("EmailID") = spltData(0)
                    Row1.Item("SubjectList") = spltData(1)

                    dtEmailList.Rows.Add(Row1)
                Next

                Dim distinctDT As DataTable = dtEmailList.DefaultView.ToTable(True, "EmailID")

                If distinctDT.Rows.Count > 0 Then
                    For i = 0 To distinctDT.Rows.Count - 1
                        Dim ToEmailID As String = distinctDT.Rows(i)("EmailID")

                        If Not ToEmailID = "" Then
                            EmailBody = ""
                            EmailBody = "  <table align='center'> <tr> <td> Following subject are " & Flag & ". </td> </tr> </table>"
                            EmailBody += " <table align='center' style='border:1px solid #aabec9;font-size:12px; border-collapse:collapse;font-family:Verdana,  Arial, Helvetica, sans-serif;' width='300px' border='1' cellpadding='5'> " &
                                         " <tr style='border:1px solid #aabec9; padding:9px;color:#ffffff;background-color:#084777;font-weight:bold;' align='center'> <td>Subject Name</td> </tr> "

                            'Add subject name
                            Dim filter As String = "EmailID = '" & ToEmailID & "'"
                            Dim FilteredRows As DataRow() = dtEmailList.Select(filter)
                            For Each row As DataRow In FilteredRows
                                EmailBody += " <tr style='color:#084777;'> " &
                                             " <td align='center'>" & row("SubjectList") & "</td> </tr> "
                            Next

                            EmailBody += " </table> <br /> <br />  " &
                                         " <table align='center' width='300px' border='0' cellpadding='5'> " &
                                         " <tr> <td style='color:#084777;' align='center'>Powered by GEMS OASIS</td> </tr> </table>"
                            'For sending email 
                            NotificationEmail.InsertIntoEmailSendSchedule(Session("sBsuid"), "EXAMPAPERALERT", "SYSTEM", ToEmailID, "Proceed to payment alert", EmailBody)
                        End If
                    Next
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnRejectExam_Click(sender As Object, e As EventArgs) Handles btnRejectExam.Click

        SaveData("Reject", -1, txtRemarks.InnerText)
        txtRemarks.InnerText = ""
        pnl_Approve.Visible = False
        GridBind()
    End Sub
    Protected Sub lnkClosePop_Click(sender As Object, e As EventArgs)
        pnl_Approve.Visible = False
    End Sub
End Class
