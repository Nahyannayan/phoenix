<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGroup_View.aspx.vb" Inherits="Curriculum_clmGroup_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Group Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr >
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                    
                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                           <asp:LinkButton ID="lbAuto" runat="server" Font-Bold="True" Visible="False">Auto Allocate Groups</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label"> Select Academic Year</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"
                               >
                            </asp:DropDownList>
                        </td>
                        <td width="25%"></td>
                       <td width="25%"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvGroup" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20"   BorderStyle="None">
                                <rowstyle wrap="False" CssClass="griditem"/>
                                <emptydatarowstyle wrap="False" />
                                <columns>
<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblSgrId" runat="server" Text='<%# Bind("SGR_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SGR_SBG_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblStmId" runat="server" Text='<%# Bind("SGR_STM_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblShfId" runat="server" Text='<%# Bind("SGR_SHF_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("SGR_GRD_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sgr_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblUntisGroup" runat="server" Text='<%# Bind("SGR_UNTIS_GRP") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="Group Name"><HeaderTemplate>

<asp:Label ID="lblopt" runat="server" Text="Group Name"></asp:Label><br />
    <asp:TextBox ID="txtGroup" runat="server"  ></asp:TextBox>
<asp:ImageButton ID="btnGroup_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGroup_Search_Click" /></td>

</HeaderTemplate>
<ItemTemplate>
<asp:Label ID="lblGroup" runat="server" text='<%# Bind("SGR_DESCR") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Subject"><HeaderTemplate>

<asp:Label ID="lblSub" runat="server" Text="Subject"></asp:Label><br />
    <asp:TextBox ID="txtSubject" runat="server"  ></asp:TextBox>
<asp:ImageButton ID="btnSubject_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSubject_Search_Click" />
</HeaderTemplate>
<ItemTemplate>
<asp:Label ID="lblSubject" runat="server" text='<%# Bind("SBG_DESCR") %>'></asp:Label>


</ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Parent Subject"  ItemStyle-VerticalAlign="Middle"><ItemTemplate>
<asp:Label ID="lblParent" runat="server" Text='<%# Bind("SBG_PARENTS") %>'></asp:Label>

</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="Grade" ItemStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"><HeaderTemplate>

Grade<br />
    <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" 
OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged"  >
</asp:DropDownList>

</HeaderTemplate>
<ItemTemplate>
<asp:Label ID="lblGrade" runat="server" text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>

</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Shift" ItemStyle-HorizontalAlign="Center">
    <HeaderTemplate>
Shift<br />
<asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" 
OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged"  >
</asp:DropDownList>
</HeaderTemplate>
<ItemTemplate>
<asp:Label ID="lblShift" runat="server" text='<%# Bind("shf_descr") %>'></asp:Label>

</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Stream" ><HeaderTemplate>
Stream<br />
<asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" 
OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" >
</asp:DropDownList>
</HeaderTemplate>
<ItemTemplate>
<asp:Label ID="lblStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>

</ItemTemplate>

<ItemStyle HorizontalAlign="center"></ItemStyle>
</asp:TemplateField>
<asp:ButtonField CommandName="View" Text="View" HeaderText="View">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
</asp:ButtonField>
</columns>
                                <selectedrowstyle wrap="False" />
                                <headerstyle  wrap="False" />
                                <editrowstyle wrap="False" />
                                <alternatingrowstyle wrap="False" />
                            </asp:GridView>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                        </td>
        </tr>
    </table>
    </td> </tr> </table>
    
            </div>
        </div>
    </div>

</asp:Content>

