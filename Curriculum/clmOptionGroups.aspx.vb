Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class clmActivity_D_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100045") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    'ViewState("datamode") = "add"

                    callYEAR_DESCRBind()
                    BindGrade()
                    BindStream()
                    BindSubject()
                    If ViewState("datamode") = "add" Then

                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        GetDetails(ViewState("viewid"))
                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub


    Private Sub GetDetails(ByVal vID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim drReader As SqlDataReader
            str_Sql = "SELECT  SGM_DESCR, SGM_ACD_ID, SGM_STM_ID, SGM_GRD_ID " & _
            " FROM SUBJECTOPTION_GROUP_M WHERE SGM_BSU_ID = '" & Session("sBsuid") & "' " & _
            " AND SGM_ID = " & vID

            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

            While (drReader.Read())
                txtOpt_GroupName.Text = drReader("SGM_DESCR")
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(drReader("SGM_ACD_ID")).Selected = True
                BindGrade()
                ddlGRD_ID.ClearSelection()
                ddlGRD_ID.Items.FindByValue(drReader("SGM_GRD_ID")).Selected = True
                BindStream()
                ddlStream.ClearSelection()
                ddlStream.Items.FindByValue(drReader("SGM_STM_ID")).Selected = True
                BindSubject()
                PopulateSubject(vID)
                DISABLE_CONTROL()
            End While

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub PopulateSubject(ByVal vID As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        Dim drReader As SqlDataReader
        str_Sql = "SELECT SGS_SBG_ID, ISNULL(SGS_bOPTIONAL, 0) SGS_bOPTIONAL " & _
        " FROM SUBJECTOPTION_GROUP_S WHERE SGS_SGM_ID = " & vID

        drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

        While (drReader.Read())
            For Each gvRow As GridViewRow In gvSubject_Detail.Rows

                Dim chkSBG_ID, chkOptional As CheckBox
                Dim lblSBG_ID As Label
                chkSBG_ID = gvRow.FindControl("chkSelect")
                chkOptional = gvRow.FindControl("chkOptional")
                lblSBG_ID = gvRow.FindControl("lblSBG_ID")

                If (chkSBG_ID Is Nothing) OrElse _
                (chkOptional Is Nothing) OrElse _
                (lblSBG_ID Is Nothing) Then
                    Continue For
                End If

                If lblSBG_ID.Text = drReader("SGS_SBG_ID").ToString Then
                    chkSBG_ID.Checked = True
                    If drReader("SGS_bOPTIONAL") Then
                        chkOptional.Checked = True
                    Else
                        chkOptional.Checked = False
                    End If
                    Exit For
                Else
                    'chkSBG_ID.Checked = False
                    'chkOptional.Checked = False
                End If
            Next
        End While

    End Sub

    Sub DISABLE_CONTROL()
        ddlAca_Year.Enabled = False
        ddlGRD_ID.Enabled = False
        txtOpt_GroupName.Enabled = False
        ddlStream.Enabled = False
        gvSubject_Detail.Enabled = False
    End Sub
    Sub ENABLE_CONTROL()
        ddlAca_Year.Enabled = True
        ddlGRD_ID.Enabled = True
        txtOpt_GroupName.Enabled = True
        ddlStream.Enabled = True
        gvSubject_Detail.Enabled = True
    End Sub

    Sub ClearDetails()
        BindGrade()
        BindStream()
        BindSubject()
        txtOpt_GroupName.Text = ""
        ViewState("datamode") = "none"

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim transaction As SqlTransaction
        Dim conn As New SqlConnection(str_conn)
        Try
            conn.Open()
            transaction = conn.BeginTransaction("SampleTransaction")
            Dim iRetVal As Integer = SaveDetails(conn, transaction)
            If iRetVal = 0 Then
                transaction.Commit()
                DISABLE_CONTROL()
                lblError.Text = "Record updated successfully"
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
            End If
        Catch ex As Exception
        Finally
            conn.Close()
        End Try
    End Sub

    Private Function SaveDetails(ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String

        Dim iReturnValue As Integer = 1000
        Try

            Dim cmd As New SqlCommand("saveSUBJECTOPTION_GROUP_M", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSGM_ID As New SqlParameter("@SGM_ID", SqlDbType.Int)
            If ViewState("datamode") = "edit" Then
                sqlpSGM_ID.Value = ViewState("viewid")
            Else
                sqlpSGM_ID.Value = 0
            End If
            cmd.Parameters.Add(sqlpSGM_ID)

            Dim sqlpSGM_DESCR As New SqlParameter("@SGM_DESCR", SqlDbType.VarChar, 50)
            sqlpSGM_DESCR.Value = txtOpt_GroupName.Text
            cmd.Parameters.Add(sqlpSGM_DESCR)

            Dim sqlpSGM_BSU_ID As New SqlParameter("@SGM_BSU_ID", SqlDbType.VarChar, 20)
            sqlpSGM_BSU_ID.Value = Session("sBsuid") & ""
            cmd.Parameters.Add(sqlpSGM_BSU_ID)

            Dim sqlpSGM_ACD_ID As New SqlParameter("@SGM_ACD_ID", SqlDbType.Int)
            sqlpSGM_ACD_ID.Value = ddlAca_Year.SelectedValue
            cmd.Parameters.Add(sqlpSGM_ACD_ID)

            Dim sqlpSGM_GRD_ID As New SqlParameter("@SGM_GRD_ID", SqlDbType.VarChar, 20)
            sqlpSGM_GRD_ID.Value = ddlGRD_ID.SelectedValue
            cmd.Parameters.Add(sqlpSGM_GRD_ID)

            Dim sqlpSGM_STM_ID As New SqlParameter("@SGM_STM_ID", SqlDbType.Int)
            sqlpSGM_STM_ID.Value = ddlStream.SelectedValue
            cmd.Parameters.Add(sqlpSGM_STM_ID)

            Dim sqlpNEW_SGM_ID As New SqlParameter("@NEW_SGM_ID", SqlDbType.BigInt)
            sqlpNEW_SGM_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpNEW_SGM_ID)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnValue = retValParam.Value
            If iReturnValue <> 0 Then
                Return iReturnValue
            Else
                Dim vNEW_SGM_ID As Integer = sqlpNEW_SGM_ID.Value
                If ViewState("datamode") = "edit" Then
                    iReturnValue = DeleteSUBJECTOPTION_GROUP_S(vNEW_SGM_ID, conn, trans)
                End If
                If iReturnValue = 0 Then
                    iReturnValue = saveSUBJECTOPTION_GROUP_S(vNEW_SGM_ID, conn, trans)
                End If
            End If
            Return iReturnValue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000
        End Try
    End Function

    Private Function DeleteSUBJECTOPTION_GROUP_S(ByVal NEW_SGM_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim iReturnValue As Integer = 1000
        Try

            Dim cmd As New SqlCommand("DeleteSUBJECTOPTION_GROUP_S", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSGM_ID As New SqlParameter("@SGM_ID", SqlDbType.Int)
            sqlpSGM_ID.Value = NEW_SGM_ID
            cmd.Parameters.Add(sqlpSGM_ID)


            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnValue = retValParam.Value
            Return iReturnValue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000
        End Try
    End Function


    Private Function saveSUBJECTOPTION_GROUP_S(ByVal NEW_SGM_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim iReturnvalue As Integer
        Try
            For Each gvRow As GridViewRow In gvSubject_Detail.Rows

                Dim chkSBG_ID, chkOptional As CheckBox
                Dim lblSBG_ID As Label
                chkSBG_ID = gvRow.FindControl("chkSelect")
                chkOptional = gvRow.FindControl("chkOptional")
                lblSBG_ID = gvRow.FindControl("lblSBG_ID")

                If (chkSBG_ID Is Nothing) OrElse _
                (chkOptional Is Nothing) OrElse _
                (lblSBG_ID Is Nothing) Then
                    Continue For
                End If

                If chkSBG_ID.Checked Then

                    Dim cmd As New SqlCommand("saveSUBJECTOPTION_GROUP_S", conn, trans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpSGS_ID As New SqlParameter("@SGS_ID", SqlDbType.Int)
                    sqlpSGS_ID.Value = 0
                    cmd.Parameters.Add(sqlpSGS_ID)

                    Dim sqlpSGS_SGM_ID As New SqlParameter("@SGS_SGM_ID", SqlDbType.Int)
                    sqlpSGS_SGM_ID.Value = NEW_SGM_ID
                    cmd.Parameters.Add(sqlpSGS_SGM_ID)

                    Dim sqlpSGS_SBG_ID As New SqlParameter("@SGS_SBG_ID", SqlDbType.Int)
                    sqlpSGS_SBG_ID.Value = lblSBG_ID.Text
                    cmd.Parameters.Add(sqlpSGS_SBG_ID)

                    Dim sqlpSGS_bOPTIONAL As New SqlParameter("@SGS_bOPTIONAL", SqlDbType.Bit)
                    sqlpSGS_bOPTIONAL.Value = chkOptional.Checked
                    cmd.Parameters.Add(sqlpSGS_bOPTIONAL)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        Exit For
                    End If
                End If
            Next
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000
        End Try
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        ENABLE_CONTROL()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ENABLE_CONTROL()
        ClearDetails()
        ViewState("datamode") = "add"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            DISABLE_CONTROL()
            ClearDetails()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtOpt_GroupName.Text = ""
    End Sub


    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindGrade()
        BindStream()
        BindSubject()
    End Sub

    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
            " FROM vw_ACADEMICYEAR_D INNER JOIN VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
            " WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" _
            & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindGrade()
        'ddlGrade.DataSource = ActivityFunctions.GetGrade_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlGRD_ID.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlGRD_ID.DataTextField = "GRM_DISPLAY"
        ddlGRD_ID.DataValueField = "GRM_GRD_ID"
        ddlGRD_ID.DataBind()
    End Sub

    Sub BindSubject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT SBG_ID, SBG_DESCR FROM SUBJECTS_GRADE_S " & _
            " WHERE SBG_BSU_ID = '" & Session("sBsuid") & "' " & _
            " AND SBG_ACD_ID = " & ddlAca_Year.SelectedValue & _
            " AND SBG_GRD_ID='" & ddlGRD_ID.SelectedValue & "'" & _
            " AND SBG_STM_ID = " & ddlStream.SelectedValue & _
            " AND ISNULL(SBG_bMAJOR, 0 ) = 1 AND ISNULL(SBG_PARENT_ID,0) = 0 " & _
            " ORDER BY SBG_ORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            gvSubject_Detail.DataSource = ds
            gvSubject_Detail.DataBind()
            'chkLstSubjects.DataSource = ds
            'chkLstSubjects.DataTextField = "SBG_DESCR"
            'chkLstSubjects.DataValueField = "SBG_ID"
            'chkLstSubjects.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGRD_ID.SelectedIndexChanged
        BindStream()
        BindSubject()
    End Sub

    Private Sub BindStream()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT DISTINCT STM_ID, STM_DESCR FROM GRADE_BSU_M " & _
            " INNER JOIN STREAM_M ON STM_ID = GRM_STM_ID " & _
            " WHERE GRM_BSU_ID = '" & Session("sBsuid") & "' " & _
            " AND GRM_ACD_ID = " & ddlAca_Year.SelectedValue & _
            " AND GRM_GRD_ID ='" & ddlGRD_ID.SelectedValue & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindSubject()
    End Sub
End Class
