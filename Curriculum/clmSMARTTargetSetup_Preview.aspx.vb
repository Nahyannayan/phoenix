Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO

Partial Class Curriculum_clmSMARTTargetSetup_Preview
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfTGM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("tgm_id").Replace(" ", "+"))
        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grd_id").Replace(" ", "+"))
        BindQuestions()
    End Sub

    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblType As Label = e.Row.FindControl("lblType")
            Dim txtAnswer As TextBox = e.Row.FindControl("txtAnswer")

            If lblType.Text = "medium" Then
                txtAnswer.TextMode = TextBoxMode.MultiLine
                txtAnswer.Height = 70
            ElseIf lblType.Text = "large" Then
                txtAnswer.TextMode = TextBoxMode.MultiLine
                txtAnswer.Height = 100
            End If
        End If
    End Sub

#Region "Privat Methods"
    Sub BindQuestions()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGD_ID,TGD_TEXTTYPE,ISNULL(TGD_SBG_ID,0) TGD_SBG_ID,TGD_SLNO,TGD_DESCR,TGD_OPTIONS,TGD_MINANSWER,TGD_PRETEXT," _
                              & " ISNULL(TGD_bMANDATORY,'FALSE') TGD_bMANDATORY,ISNULL(TGD_bTARGET,'FALSE') TGD_bTARGET " _
                              & " FROM SMART.TARGET_SETUP_D WHERE TGD_TGM_ID=" + hfTGM_ID.Value _
                              & " AND TGD_GRD_ID='" + hfGRD_ID.Value + "'" _
                              & " ORDER BY TGD_SLNO "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlQuestions.DataSource = ds
        dlQuestions.DataBind()

        Dim i As Integer

        Dim lblTextType As Label
        Dim lblOption As Label
        Dim lblMinAnswer As Label
        Dim lblPretext As Label
        Dim lblTarget As Label
        Dim lblStar As Label
        Dim lblMandatory As Label

        Dim txtTargetScore As TextBox
        Dim strLno As String() = {"a.", "b.", "c.", "d.", "e.", "f.", "g.", "h.", "i.", "j.", "k.", "l."}

        Dim dt As DataTable
        Dim dr As DataRow

        Dim gvOptions As GridView
        Dim options As Integer

        Dim p As Integer

        For i = 0 To dlQuestions.Items.Count - 1
            With dlQuestions.Items(i)
                lblTextType = .FindControl("lblTextType")
                lblOption = .FindControl("lblOption")
                lblMinAnswer = .FindControl("lblMinAnswer")
                lblPretext = .FindControl("lblPretext")
                lblTarget = .FindControl("lblTarget")
                lblStar = .FindControl("lblStar")
                lblMandatory = .FindControl("lblMandatory")

                If lblMandatory.Text.ToLower = "false" Then
                    lblStar.Visible = False
                End If
                txtTargetScore = .FindControl("txtTargetScore")
                gvOptions = .FindControl("gvOptions")
                If lblTarget.Text.ToLower = "false" Then
                    txtTargetScore.Visible = False
                    If lblOption.Text = 0 Then
                        options = 1
                    Else
                        options = lblOption.Text
                    End If

                    dt = SetDataTable()

                    For p = 0 To options - 1
                        dr = dt.NewRow
                        dr.Item(0) = strLno(p)
                        dr.Item(1) = lblPretext.Text
                        dr.Item(2) = lblTextType.Text
                        dt.Rows.Add(dr)
                    Next

                    gvOptions.DataSource = dt
                    gvOptions.DataBind()

                    If gvOptions.Rows.Count > 0 Then
                        gvOptions.HeaderRow.Visible = False
                    End If
                Else
                    gvOptions.Visible = False
                End If
            End With
        Next
    End Sub
#End Region

    Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SLNO"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "PRETEXT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TEXTTYPE"
        dt.Columns.Add(column)

        Return dt
    End Function
End Class
