Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_BsuBudgetedStrength
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            ViewState("datamode") = "add"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
              
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050055") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("datamode") = "add"
                    FillAccadaamicYear()
                    gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim transaction As SqlTransaction
        Dim intResult As Integer

        Try

            For Each GvRow As GridViewRow In gvUNITS.Rows
                If GvRow.RowType = DataControlRowType.DataRow Then

                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try

                            Dim str_query As String = "exec dbo.saveSCHOOLSTRENGTH '" + GvRow.Cells(0).Text + "'," + ddlAcdYear.SelectedValue.ToString + "," + TryCast(GvRow.FindControl("Textbox1"), TextBox).Text + ""
                                                
                            intResult = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                            transaction.Commit()
                            lblError.Text = "Record Saved Successfully"

                        Catch myex As ArgumentException
                            transaction.Rollback()
                            lblError.Text = myex.Message
                            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        Catch ex As Exception
                            transaction.Rollback()
                            'lblError.Text = "Record could not be Saved"
                            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        End Try
                    End Using

                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim strCatFilter As String = ""

            Dim ds As New DataSet

            If ddlAcdYear.SelectedValue <> "" Then
                strCatFilter = " "
            End If

            str_Sql = " SELECT BSU_ID,BSU_NAME,'' AS STRENGTH FROM BUSINESSUNIT_M " 

            'GetSelectedGrades
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " ORDER BY 1")

            If ds.Tables(0).Rows.Count > 0 Then
                gvUNITS.DataSource = ds.Tables(0)
                gvUNITS.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvUNITS.DataSource = ds.Tables(0)
                Try
                    gvUNITS.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvUNITS.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvUNITS.Rows(0).Cells.Clear()
                gvUNITS.Rows(0).Cells.Add(New TableCell)
                gvUNITS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvUNITS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvUNITS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FillAccadaamicYear()
        Dim str_query As String
        ddlAcdYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        str_query = "SELECT ACY_ID,ACY_DESCR FROM ACADEMICYEAR_M ORDER BY 1"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcdYear.DataSource = ds
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACY_ID"
        ddlAcdYear.DataBind()

    End Sub

    Private Function GetResults(ByVal BSU_ID As String, ByVal ACD_ID As Integer) As String
        Dim str_query As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        str_query = " SELECT STG_VALUE FROM BSU_STRENGTH WHERE STG_BSU_ID='" & BSU_ID & "' AND STG_ACD_ID=" & ACD_ID & " "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count >= 1 Then
            Return ds.Tables(0).Rows(0).Item(0).ToString
        Else
            Return ""
        End If

    End Function

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvUNITS_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvUNITS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUNITS.PageIndexChanging
        Try
            gvUNITS.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvUNITS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUNITS.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                TryCast(e.Row.FindControl("TextBox1"), TextBox).Text = GetResults(e.Row.Cells(0).Text.ToString(), ddlAcdYear.SelectedValue)

            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
