<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGradeSlabMasterView.aspx.vb" Inherits="clmGradeSlabMasterView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Grade Slab Master
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left" colspan="2" style="text-align: left">&nbsp;<asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table id="tbl_test" runat="server" align="center" border="0"
                    cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img" valign="top">
                        <td align="left" colspan="9" valign="middle" class="title-bg">
                            <asp:Label ID="lblHeader" runat="server" Text="Grade Slab Master...."></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="9">
                            <asp:GridView ID="gvGradeSlab" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="SLAB_ID">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGSM_SLAB_ID" runat="server" Text='<%# Bind("GSM_SLAB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Available" HeaderStyle-HorizontalAlign ="Center" HeaderStyle-Width="10%">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderTemplate  >
                                            Suppress  
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("bSUPRESS") %>' onclick="javascript:highlightView(this);" />
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Slab_Descr">
                                        <HeaderTemplate>
                                            Grade Slab Description <br />
                                                                        <asp:TextBox ID="txtCounterDescr" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("GSM_DESC") %>' __designer:wfdid="w3"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Total_Mark">
                                        <HeaderTemplate>
                                           Total Marks    <br />                <asp:TextBox ID="txtUser" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("GSM_TOT_MARK") %>' __designer:wfdid="w6"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSuppress" TabIndex="7" runat="server" Text="Suppress" CssClass="button" />
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>
