﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmIBTorPassDetails.aspx.vb" Inherits="Curriculum_clmIBTorPassDetails" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> <asp:Label ID="lblHeader" runat="server" Text="Upload IBT or Pass Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">


    <table align="center"  style="border-collapse: collapse" cellpadding="4" cellspacing="0" width="100%">
        
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Academic Year</span>
            </td>
            
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" valign="middle" width="20%">
                <span class="field-label">Grade</span>
            </td>
            <td align="left" valign="middle" width="30%">
                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;
            </td>
        </tr>
        <tr id="20">
            
            <td align="left" valign="middle" width="20%">
               <span class="field-label">Section</span>
            </td>
            <td align="left" colspan="1" valign="middle" width="30%">
                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left">
                <span class="field-label">Type</span>
            </td>
            
            <td align="left">
                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" >
                    <asp:ListItem Value="0" Text="IBT"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Pass"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
       
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td colspan="6">

    <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0" 
        Style="width: 100%">
        <ajaxToolkit:TabPanel ID="HT1" runat="server" HeaderText="Details Updation">
            <ContentTemplate>
                <div style="overflow: auto">
                    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr id="Tr1" runat="server">
                            <td id="Td1" runat="server" align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    SkinID="error"></asp:Label>
                            </td>
                        </tr>
                        <tr id="Tr2" runat="server">
                            <td id="Td2" align="left" runat="server">
                                <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="CMTID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="cmtId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student ID">
                                            <HeaderTemplate>
                                                
                                                            <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Student ID"></asp:Label>
                                                       <br />
                                                            <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnEmpid_Search_Click" />
                                                                        
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubId" runat="server" Text='<%# Bind("STU_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <HeaderTemplate>
                                               
                                                      <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="Name"></asp:Label>
                                                        <br />
                                                      <asp:TextBox ID="txtOption1" runat="server"></asp:TextBox>
                                                      <asp:ImageButton ID="btnstuname_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnstuname_Search_Click" />
                                                                        
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("STU_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle></HeaderStyle>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Math Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtnv" runat="server"  Text='<%# Bind("ID_MS")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Math Percentile">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtv" runat="server"  Text='<%# Bind("ID_MP")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtqu" runat="server"  Text='<%# Bind("ID_ES")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English Percentile">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtsp" runat="server"  Text='<%# Bind("ID_EP")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Science Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtmc" runat="server"  Text='<%# Bind("ID_SS")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Science Percentile">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtscp" runat="server"  Text='<%# Bind("ID_SP")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                          <tr id="Tr4" runat="server" visible="false">
                            <td id="Td4" align="left" runat="server">
                                <asp:GridView ID="grdPass" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="CMTID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="cmtId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student ID">
                                            <HeaderTemplate>
                                                
                                                            <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Student ID"></asp:Label>
                                                       
                                                                        <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>
                                                                    <
                                                                            <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnEmpid_Search_Click" />
                                                                        
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubId" runat="server" Text='<%# Bind("STU_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <HeaderTemplate>
                                               
                                                            <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="Name"></asp:Label>
                                                       
                                                                        <asp:TextBox ID="txtOption1" runat="server" ></asp:TextBox>
                                                                    
                                                                            <asp:ImageButton ID="btnstuname_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnstuname_Search_Click" />
                                                                        
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Width="200px" Text='<%# Bind("STU_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle></HeaderStyle>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Perceived Learning capacity">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtnv" runat="server"  Text='<%# Bind("PD_PLC")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Self-regard, as a learner">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtv" runat="server"  Text='<%# Bind("PD_SRL")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Preparedness for learning">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtqu" runat="server" Width="50px" Text='<%# Bind("PD_PFL")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="General work ethic">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtsp" runat="server"  Text='<%# Bind("PD_GWE")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                        <asp:TemplateField HeaderText="Confidence in learning">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtmc" runat="server"  Text='<%# Bind("PD_CIL")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Feelings about school">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFS" runat="server"  Text='<%# Bind("PD_FAS")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Attitudes to Teachers">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtat" runat="server"  Text='<%# Bind("PD_ATT")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Attitudes to Attendance">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAA" runat="server"  Text='<%# Bind("PD_ATA")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Response to Curriculum">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRc" runat="server"  Text='<%# Bind("PD_RTC")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="Tr67" runat="server">
                            <td id="Td67" align="center" colspan="12" runat="server">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR"
                                     />
                               
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Upload">
            <ContentTemplate>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table width="100%" id="Table3" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                <tr>
                                    <td colspan="3">
                                        
                                        <tr id="Tr3" runat="server">
                                            <td id="Td3" runat="server" colspan="3" >
                                                <asp:Label ID="lblIBT" runat="server" Text="Please upload the excel with column names as StudentID, StudentName, MathScore, MathPercentile, EnglishScore, EnglishPercentile, ScienceScore, SciencePercentile"                                                 
                                                    CssClass="error" EnableViewState="False" ></asp:Label>
                                                 <asp:Label ID="lblPass" visible="false" runat="server" Text="Please upload the excel with column names as StudentID, StudentName, PerceivedLearningCapacity, Selfregard, PreparednessForLearning, GeneralWorkEthic, ConfidenceInLearning,FeelingsAboutSchool,AttitudesToTeachers,AttitudesToAttendance,ResponseToCurriculum" 
                                                     CssClass="error" EnableViewState="False" ></asp:Label>
                                            </td>
                                        </tr>
                                        <asp:Label ID="lblerror3" runat="server" CssClass="error" EnableViewState="False" SkinID="error"></asp:Label>
                                    </td>
                                </tr>
                                
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">Select File</span>
                                        </td>
                                        
                                        <td align="left">
                                            <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload"
                                                OnClick="btnUpload_Click" CausesValidation="False" />
                                        </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
            
            </td>
        </tr>
    </table>

        </div>
    </div>
</div>
</asp:Content>

