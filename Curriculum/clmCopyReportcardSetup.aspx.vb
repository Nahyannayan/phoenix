﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_clmCopyReportcardSetup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100530") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                BindBusinessUnit()
                BindCurriculum()
                GetAcademicYears()
                BindReportCards()

                '    Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBusinessUnit()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_SHORTNAME FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN(SELECT RSM_BSU_ID FROM RPT.REPORT_SETUP_M) and bsu_shortname<>'AIS' ORDER BY BSU_SHORTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusinessUnit.DataSource = ds
        ddlBusinessUnit.DataTextField = "BSU_SHORTNAME"
        ddlBusinessUnit.DataValueField = "BSU_ID"
        ddlBusinessUnit.DataBind()
    End Sub

    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CLM_ID,CLM_DESCR FROM OASIS..CURRICULUM_M INNER JOIN OASIS..ACADEMICYEAR_D ON ACD_CLM_ID=CLM_ID WHERE ACD_BSU_ID='" + ddlBusinessUnit.SelectedValue.ToString + "' ORDER BY CLM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurriculum.DataSource = ds
        ddlCurriculum.DataTextField = "CLM_DESCR"
        ddlCurriculum.DataValueField = "CLM_ID"
        ddlCurriculum.DataBind()
    End Sub

    Sub GetAcademicYears()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ACD_ID,ACD_ACY_ID FROM OASIS..ACADEMICYEAR_D WHERE ACD_BSU_ID='" + ddlBusinessUnit.SelectedValue.ToString _
                                  & "' AND ACD_CLM_ID='" + ddlCurriculum.SelectedValue.ToString + "' AND ACD_CURRENT=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hfACD_CURRENT.Value = ds.Tables(0).Rows(0).Item(0)
            hfACY_CURRENT.Value = ds.Tables(0).Rows(0).Item(1)
        End If

        str_query = "SELECT ACD_ID FROM OASIS..ACADEMICYEAR_D WHERE ACD_BSU_ID='" + ddlBusinessUnit.SelectedValue.ToString _
                    & "' AND ACD_CLM_ID='" + ddlCurriculum.SelectedValue.ToString + "' AND ACD_ACY_ID=" + (hfACY_CURRENT.Value - 1).ToString
        hfACD_PREV.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindReportCards()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_ID,RSM_DESCR FROM RPT.REPORT_sETUP_M WHERE RSM_ACD_ID=" + hfACD_PREV.Value _
                               & " AND RSM_DESCR NOT IN(SELECT RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + hfACD_CURRENT.Value + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlReportCard.Items.Insert(0, li)
    End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec COPY.COPY_REPORTCARDSETUP " _
                                  & " @BSU_ID='" + ddlBusinessUnit.SelectedValue.ToString + "'," _
                                  & " @ACD_COPIEDFROM=" + hfACD_PREV.Value + "," _
                                  & " @RSM_COPIEDFROM=" + ddlReportCard.SelectedValue.ToString + "," _
                                  & " @ACD_COPIEDTO=" + hfACD_CURRENT.Value
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub ddlBusinessUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        BindCurriculum()
        GetAcademicYears()
        BindReportCards()
        lblError.Text = ""
    End Sub

    Protected Sub ddlCurriculum_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        GetAcademicYears()
        BindReportCards()
        lblError.Text = ""
    End Sub

    Protected Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        If ddlReportCard.SelectedValue = "0" Then
            lblError.Text = "Please select a report card"
            Exit Sub
        End If
        SaveData()
        BindReportCards()
        lblError.Text = "Record saved successfully"
    End Sub
End Class
