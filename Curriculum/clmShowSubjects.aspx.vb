Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmShowSubjects
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hfACD_ID.Value = Request.QueryString("acdid")
            hfGRD_ID.Value = Request.QueryString("grdid")
            hfSTM_ID.Value = Request.QueryString("stmid")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            GridBind()
            If hfGRD_ID.Value <> "0" Then
                gvSubjects.Columns(2).Visible = False
            End If
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID

                s = gvSubjects.HeaderRow.FindControl("mnu_3_img")
               
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If hfGRD_ID.Value <> "0" And hfGRD_ID.Value <> "" Then
            str_query = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                      & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + hfACD_ID.Value _
                      & " AND SBG_GRD_ID='" + hfGRD_ID.Value + "'"
        Else
            str_query = "SELECT DISTINCT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY,SBG_PARENTS_SHORT ,SBG_GRD_ID" _
                      & " FROM SUBJECTS_GRADE_S AS A INNER JOIN GRADE_BSU_M AS B ON A.SBG_GRD_ID=B.GRM_GRD_ID" _
                      & "  AND A.SBG_ACD_ID=B.GRM_ACD_ID WHERE SBG_ACD_ID = " + hfACD_ID.Value
        End If

        If hfSTM_ID.Value <> "0" And hfSTM_ID.Value <> "" Then
            str_query += " AND SBG_STM_ID=" + hfSTM_ID.Value
        End If




        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim subjectSearch As String = ""
        Dim shortSearch As String = ""

        Dim txtSearch As New TextBox
        Dim ddlgvOpt As New DropDownList
        Dim selectedOpt As String = ""

        If gvSubjects.Rows.Count > 0 Then

            txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SBG_DESCR", txtSearch.Text, strSearch)
            subjectSearch = txtSearch.Text

            ddlgvOpt = gvSubjects.HeaderRow.FindControl("ddlgvOpt")
            If ddlgvOpt.Text <> "ALL" Then
                strFilter += " and SBG_bOPTIONAL=" + IIf(ddlgvOpt.Text = "YES", "'true'", "'false'")
                selectedOpt = ddlgvOpt.Text
            End If


            txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBG_PARENTS", txtSearch.Text, strSearch)
            shortSearch = txtSearch.Text

            txtSearch = gvSubjects.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            shortSearch = txtSearch.Text


            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If
        str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSubjects.DataBind()
            Dim columnCount As Integer = gvSubjects.Rows(0).Cells.Count
            gvSubjects.Rows(0).Cells.Clear()
            gvSubjects.Rows(0).Cells.Add(New TableCell)
            gvSubjects.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSubjects.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSubjects.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSubjects.DataBind()
        End If

        txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
        txtSearch.Text = subjectSearch

        txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
        txtSearch.Text = shortSearch

        ddlgvOpt = gvSubjects.HeaderRow.FindControl("ddlgvOpt")
        ddlgvOpt.Text = selectedOpt

    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Protected Sub gvSubjects_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjects.PageIndexChanging
        gvSubjects.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub btnSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnParent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvOpt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub gvSubjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSubjects.SelectedIndexChanged
        Dim lblSbgId As Label
        Dim lnkSubject As LinkButton
        Dim lblSbgParent As Label

        Dim subject As String

        lblSbgId = gvSubjects.SelectedRow.FindControl("lblSbgId")
        lnkSubject = gvSubjects.SelectedRow.FindControl("lnkSubject")
        lblSbgParent = gvSubjects.SelectedRow.FindControl("lblSbgParent")

        If lblSbgParent.Text = "NA" Then
            subject = lnkSubject.Text
        Else
            subject = lnkSubject.Text + "-" + lblSbgParent.Text
        End If

        Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & subject + "|" + lblSbgId.Text & "';")
        Response.Write("var oArg = new Object();")
        Response.Write("oArg.Result ='" & subject + "|" + lblSbgId.Text & "';")
        Response.Write("var oWnd = GetRadWindow('" & subject + "|" + lblSbgId.Text & "');")
        Response.Write("oWnd.close(oArg);")
        'Response.Write("window.close();")
        Response.Write("} </script>")
        'h_SelectedId.Value = "Close"
    End Sub
    'test

End Class
