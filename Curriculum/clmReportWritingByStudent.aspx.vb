
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.Configuration
Partial Class Curriculum_clmReportWritingByStudent
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")



        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330335") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"


                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

                    ' trAcd.Visible = False
                    BindTeacherGrades()
                    BindReportCard()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSection()

                    Show_HideRows(True)

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            'studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub

#Region "Private Methods"

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function



    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        If Session("CurrSuperUser") <> "Y" Then
            Dim li As New ListItem
            li.Text = "All"
            li.Value = "0"
            ddlSection.Items.Insert(0, li)
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindTeacherGrades()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SSD_GRD_ID FROM STUDENT_GROUPS_S AS A " _
                                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SSD_SGR_ID=B.SGS_SGR_ID" _
                                & " WHERE SGS_TODATE IS NULL AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                                & " AND SSD_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            If hfTeacherGrades.Value <> "" Then
                hfTeacherGrades.Value += ","
            End If
            hfTeacherGrades.Value += "'" + ds.Tables(0).Rows(i).Item(0) + "'"
        Next
        If hfTeacherGrades.Value = "" Then
            hfTeacherGrades.Value = "0"
        End If
    End Sub

    Sub BindReportCard()
        Session("CurrSuperUser") = "Y"
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            If ViewState("GRD_ACCESS") > 0 Then
                str_query = "SELECT DISTINCT RSM_DESCR,RSM_ID,RSM_DISPLAYORDER FROM RPT.REPORT_SETUP_M AS A " _
                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON A.RSM_ID=B.RSG_RSM_ID " _
                 & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                 & " AND RSG_GRD_ID IN (select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                 & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                 & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))" _
                 & " ORDER BY RSM_DISPLAYORDER "
            Else
                str_query = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                           & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                           & " ORDER BY RSM_DISPLAYORDER "
            End If

        Else
            str_query = "SELECT DISTINCT RSM_DESCR,RSM_ID,RSM_DISPLAYORDER FROM RPT.REPORT_SETUP_M AS A " _
                   & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON A.RSM_ID=B.RSG_RSM_ID " _
                   & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                   & " AND RSG_GRD_ID IN (" + hfTeacherGrades.Value + ")" _
                   & " ORDER BY RSM_DISPLAYORDER "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub


    Sub BindStudents()
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM STUDENT_M AS A INNER JOIN VW_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN VW_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                        & " WHERE STU_CURRSTATUS='EN' AND STU_ACD_ID=" + hfACD_ID.Value
        Else
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM VW_STUDENT_DETAILS_PREVYEARS" _
                        & " WHERE STU_CURRSTATUS='EN' AND STU_ACD_ID=" + hfACD_ID.Value
        End If


        If studClass.isEmpTeacher(Session("EMPLOYEEID")) = True And Session("CurrSuperUser") <> "Y" Then
            str_query += " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S AS A INNER JOIN" _
                        & " GROUPS_TEACHER_S AS B ON A.SSD_SGR_ID=B.SGS_SGR_ID AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                        & " AND SGS_TODATE IS NULL AND SSD_ACD_ID=" + hfACD_ID.Value + ") "
        End If

        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvStudent.DataSource = ds
        gvStudent.DataBind()
        If gvStudent.Rows.Count > 0 Then
            gvStudent.HeaderRow.Visible = False
        End If
    End Sub



    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                        & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                        & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY RSG_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                         & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                         & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                         & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                         & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                         & " AND RSG_GRD_ID IN (" + hfTeacherGrades.Value + ")" _
                         & " ORDER BY RSG_DISPLAYORDER"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SSD_SGR_ID,SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                       & " INNER JOIN STUDENT_GROUPS_S AS B ON A.SBG_ID=B.SSD_SBG_ID" _
                       & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SSD_STU_ID=" + hfSTU_ID.Value
        ElseIf studClass.isEmpTeacher(Session("EMPLOYEEID")) = True Then
            str_query = "SELECT DISTINCT  SSD_SGR_ID,SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN STUDENT_GROUPS_S AS B ON A.SBG_ID=B.SSD_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SSD_SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SSD_STU_ID=" + hfSTU_ID.Value _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubject.DataSource = ds
        gvSubject.DataBind()

    End Sub


    Sub BindHeader(ByVal gvMarks As GridView, ByVal sbg_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String
        ''New Code Added For GIP. AS Per Chrales Sir Requirment. 02-04-2017.
        If ((Session("sBsuid") = "315018") Or (Session("sBsuid") = "315888")) Then

            str_query = "SELECT SBG_ID='" + sbg_id + "',RSD_ID,RSD_HEADER," _
                                    & " V1=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V2=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V3=CASE WHEN RSD_RESULT='D' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V4=CASE WHEN RSD_RESULT='L' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " TYPE=CASE WHEN RSD_RESULT='L' then 'lbl' WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'txtsmall' " _
                                    & " WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'txtmulti'" _
                                    & "  WHEN RSD_RESULT='D' THEN 'dropdown' ELSE 'txtsmall' END" _
                                    & " FROM ("

            str_query += "SELECT SBG_ID=" + sbg_id + ",RSD_ID,RSD_HEADER=CASE WHEN RSD_HEADER LIKE 'POI%' THEN RSD_HEADER WHEN RSD_HEADER='Programme of Inquiry' THEN RSD_HEADER ELSE CASE WHEN ISNULL(RSD_SUB_DESC,'')='' THEN RSD_HEADER ELSE RSD_HEADER END END ," _
                                & " RSD_CSSCLASS,RSD_RESULT=CASE WHEN (SELECT COUNT(*) FROM RPT.REPORTSETUP_DEFAULTS_S " _
                                & " WHERE RSP_RSD_ID=A.RSD_ID AND RSP_SBG_ID=" + sbg_id + ")>0 THEN 'D' WHEN RSD_bDIRECTENTRY='FALSE' THEN 'L' ELSE 'C' END," _
                                & " RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D AS A WHERE RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                                & " AND RSD_bALLSUBJECTS='TRUE'  AND ISNULL(RSD_bCOMMON,0)=0  AND ISNULL(RSD_bSUPRESS,0)=0" _
                                & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID='" + sbg_id + "')"




            str_query += ")PP ORDER BY RSD_DISPLAYORDER,RSD_HEADER"

        Else

            str_query = "SELECT SBG_ID='" + sbg_id + "',RSD_ID,RSD_HEADER," _
                                    & " V1=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V2=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V3=CASE WHEN RSD_RESULT='D' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " V4=CASE WHEN RSD_RESULT='L' THEN 'TRUE' ELSE 'FALSE' END," _
                                    & " TYPE=CASE WHEN RSD_RESULT='L' then 'lbl' WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'txtsmall' " _
                                    & " WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'txtmulti'" _
                                    & "  WHEN RSD_RESULT='D' THEN 'dropdown' ELSE 'txtsmall' END" _
                                    & " FROM ("

            'str_query += "SELECT SBG_ID=" + sbg_id + ",RSD_ID,RSD_HEADER=CASE WHEN RSD_HEADER LIKE 'POI%' THEN RSD_HEADER WHEN RSD_HEADER='Programme of Inquiry' THEN RSD_HEADER ELSE CASE WHEN ISNULL(RSD_SUB_DESC,'')='' THEN RSD_HEADER ELSE RSD_SUB_DESC END END ," _
            '                        & " RSD_CSSCLASS,RSD_RESULT=CASE WHEN (SELECT COUNT(*) FROM RPT.REPORTSETUP_DEFAULTS_S " _
            '                        & " WHERE RSP_RSD_ID=A.RSD_ID AND RSP_SBG_ID=" + sbg_id + ")>0 THEN 'D' ELSE 'C' END," _
            '                        & " RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D AS A WHERE RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
            '                        & " AND RSD_bALLSUBJECTS='TRUE' AND RSD_bDIRECTENTRY='TRUE' AND ISNULL(RSD_bCOMMON,0)=0" _
            '                        & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID='" + sbg_id + "')"

            str_query += "SELECT SBG_ID=" + sbg_id + ",RSD_ID=-1,RSD_HEADER='Previous Results'," _
                              & " RSD_CSSCLASS='TEXTBOXSMALL',RSD_RESULT='L'," _
                              & " RSD_DISPLAYORDER=-1 UNION ALL "


            str_query += "SELECT SBG_ID=" + sbg_id + ",RSD_ID,RSD_HEADER=CASE WHEN RSD_HEADER LIKE 'POI%' THEN RSD_HEADER WHEN RSD_HEADER='Programme of Inquiry' THEN RSD_HEADER ELSE CASE WHEN ISNULL(RSD_SUB_DESC,'')='' THEN RSD_HEADER ELSE RSD_SUB_DESC END END ," _
                                & " RSD_CSSCLASS,RSD_RESULT=CASE WHEN (SELECT COUNT(*) FROM RPT.REPORTSETUP_DEFAULTS_S " _
                                & " WHERE RSP_RSD_ID=A.RSD_ID AND RSP_SBG_ID=" + sbg_id + ")>0 THEN 'D' WHEN RSD_bDIRECTENTRY='FALSE' THEN 'L' ELSE 'C' END," _
                                & " RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D AS A WHERE RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                                & " AND RSD_bALLSUBJECTS='TRUE'  AND ISNULL(RSD_bCOMMON,0)=0  AND ISNULL(RSD_bSUPRESS,0)=0" _
                                & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID='" + sbg_id + "')"




            str_query += ")PP ORDER BY RSD_DISPLAYORDER,RSD_HEADER"

        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvMarks.DataSource = ds
        gvMarks.DataBind()
    End Sub

    Sub BindDefaultValues(ByVal ddlGrades As DropDownList, ByVal sbg_id As String, ByVal rsd_id As String)
        Dim li As ListItem
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S " _
                                 & " WHERE RSP_RSD_ID=" + rsd_id + " AND RSP_SBG_ID=" + sbg_id _
                                 & " ORDER BY RSP_DISPLAYORDER,RSP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        For i = 0 To ds.Tables(0).Rows.Count - 1
            li = New ListItem
            li.Text = ds.Tables(0).Rows(i).Item(0).ToString
            li.Value = ds.Tables(0).Rows(i).Item(0).ToString
            ddlGrades.Items.Add(li)
        Next

        If ddlGrades.Items.FindByValue("--") Is Nothing Then
            li = New ListItem
            li.Text = "--"
            li.Value = "--"
            ddlGrades.Items.Insert(0, li)
        End If
    End Sub


    Sub BindValues()
        


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfACD_ID.Value = Session("Current_ACD_ID") Then
            str_query = "SELECT DISTINCT RST_SBG_ID,RST_RSD_ID,CASE WHEN RST_COMMENTS IS NOT NULL THEN RST_COMMENTS WHEN RST_MARK IS NOT NULL  " _
                                    & " AND RST_GRADING IS NOT NULL THEN CONVERT(VARCHAR(100),RST_MARK)+'-'+RST_GRADING " _
                                    & " WHEN RST_MARK IS NOT NULL AND RST_GRADING IS NULL THEN CONVERT(VARCHAR(100),RST_MARK) " _
                                    & " WHEN RST_MARK IS NULL AND RST_GRADING IS NOT NULL THEN RST_GRADING ELSE isnull(RST_COMMENTS,'') END FROM " _
                                    & " RPT.REPORT_STUDENT_S WHERE RST_STU_ID=" + hfSTU_ID.Value _
                                    & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                                    & " AND ISNULL(RST_SBG_ID,0)<>0 and rst_rsd_id<>486162"
        Else
            str_query = "SELECT DISTINCT RST_SBG_ID,RST_RSD_ID,isnull(RST_COMMENTS,'') FROM " _
                                 & " RPT.REPORT_STUDENT_PREVYEARS WHERE RST_STU_ID=" + hfSTU_ID.Value _
                                 & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                                 & " AND ISNULL(RST_SBG_ID,0)<>0"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim grades As New Hashtable

        Dim i, j, k As Integer


        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                grades.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2))
            End With
        Next



        Dim lblSbgId As Label
        Dim gvMarks As GridView
        Dim lblRsdId As Label
        Dim txtGrade As TextBox
        Dim txtComments As TextBox
        Dim ddlGrades As DropDownList
        Dim lblType As Label
        Dim value As String = ""
        Dim lblGrade As Label
        Dim lblMark As Label
        Dim lblHeader As Label
        Dim spellArray As String()
        Dim spellString As String = ""

        For j = 0 To gvSubject.Rows.Count - 1
            lblSbgId = gvSubject.Rows(j).FindControl("lblSbgId")
            gvMarks = gvSubject.Rows(j).FindControl("gvMarks")
            For k = 0 To gvMarks.Rows.Count - 1
                With gvMarks.Rows(k)
                    lblHeader = gvMarks.Rows(k).FindControl("lblHeader")
                    lblGrade = .FindControl("lblGrade")
                    If lblHeader.Text <> "Previous Results" Then
                        lblRsdId = .FindControl("lblRsdId")
                        lblType = .FindControl("lblType")

                        If grades.ContainsKey(lblSbgId.Text + "_" + lblRsdId.Text) = True Then
                            value = grades.Item(lblSbgId.Text + "_" + lblRsdId.Text)
                        Else
                            value = ""
                        End If
                        If lblType.Text = "txtmulti" Then
                            txtComments = .FindControl("txtComments")
                            txtComments.Text = value
                            If spellString <> "" Then
                                spellString += "|"
                            End If
                            spellString += txtComments.ClientID
                        ElseIf lblType.Text = "dropdown" Then
                            ddlGrades = .FindControl("ddlGrades")
                            If Not ddlGrades.Items.FindByText(value) Is Nothing Then
                                ddlGrades.ClearSelection()
                                ddlGrades.Items.FindByText(value).Selected = True
                            End If
                        ElseIf lblType.Text = "txtsmall" Then
                            txtGrade = .FindControl("txtGrade")
                            txtGrade.Text = value
                        Else
                            lblMark = .FindControl("lblMark")
                            lblMark.Text = value
                          
                        End If
                        lblGrade.Text = value
                    Else
                        str_query = "select isnull(STUFF((SELECT ', '+  RPF_DESCR+' - '+RST_COMMENTS" _
                                 & " FROM RPT.REPORT_STUDENT_S AS A " _
                                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RST_RPF_ID=B.RPF_ID" _
                                 & " INNER JOIN RPT.REPORT_SETUP_D AS C ON A.RST_RSD_ID=C.RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1" _
                                 & " WHERE RST_RPF_ID<>" + hfRPF_ID.Value + " AND RST_SBG_ID=" + lblSbgId.Text + " AND RST_STU_ID=" + hfSTU_ID.Value + " AND RST_COMMENTS IS NOT NULL for xml path('')),1,1,''),'')"
                        lblMark = .FindControl("lblMark")
                        lblMark.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                       
                    End If

                End With
            Next
        Next
        spellArray = spellString.Split("|")
        RadSpell1.ControlsToCheck = spellArray
        RadSpell2.ControlsToCheck = spellArray


    End Sub

    Function SaveData() As Boolean
        Dim strMark As String = ""
        Dim strGrade As String = ""


        Dim lblSbgId As Label
        Dim gvMarks As GridView
        Dim lblRsdId As Label
        Dim txtGrade As TextBox
        Dim txtComments As TextBox
        Dim ddlGrades As DropDownList
        Dim lblType As Label
        Dim value As String = ""
        Dim lblGrade As Label
        Dim lblSgrId As Label

        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim stTrans As SqlTransaction




        Dim i, j, k As Integer

        objConn.Open()
        stTrans = objConn.BeginTransaction
        Dim iReturnvalue As Integer
        Try
            For j = 0 To gvSubject.Rows.Count - 1
                lblSbgId = gvSubject.Rows(j).FindControl("lblSbgId")
                gvMarks = gvSubject.Rows(j).FindControl("gvMarks")
                lblSgrId = gvSubject.Rows(j).FindControl("lblSgrId")

                For k = 0 To gvMarks.Rows.Count - 1
                    With gvMarks.Rows(k)

                        lblRsdId = .FindControl("lblRsdId")
                        lblType = .FindControl("lblType")
                        lblGrade = .FindControl("lblGrade")

                        If lblType.Text = "txtmulti" Then
                            txtComments = .FindControl("txtComments")
                            value = txtComments.Text
                        ElseIf lblType.Text = "dropdown" Then
                            ddlGrades = .FindControl("ddlGrades")
                            value = ddlGrades.SelectedItem.Text
                        ElseIf lblType.Text = "txtsmall" Then
                            txtGrade = .FindControl("txtGrade")
                            value = txtGrade.Text
                        End If


                        If value <> lblGrade.Text And lblType.Text <> "lbl" Then
                            If Session("Current_ACD_ID") = hfACD_ID.Value Then
                                cmd = New SqlCommand("[RPT].[SaveREPORT_STUDENT_S]", objConn, stTrans)
                            Else
                                cmd = New SqlCommand("RPT.SaveREPORT_STUDENT_PREVYEARS", objConn, stTrans)
                            End If
                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.AddWithValue("@RST_ID", 0)
                            cmd.Parameters.AddWithValue("@RST_RPF_ID", hfRPF_ID.Value)
                            cmd.Parameters.AddWithValue("@RST_RSD_ID", lblRsdId.Text) '-- Report Setup
                            cmd.Parameters.AddWithValue("@RST_ACD_ID", hfACD_ID.Value)
                            cmd.Parameters.AddWithValue("@RST_GRD_ID", hfGRD_ID.Value)
                            cmd.Parameters.AddWithValue("@RST_STU_ID", hfSTU_ID.Value)
                            cmd.Parameters.AddWithValue("@RST_RSS_ID", 0)
                            cmd.Parameters.AddWithValue("@RST_SGR_ID", lblSgrId.Text)
                            cmd.Parameters.AddWithValue("@RST_SBG_ID", lblSbgId.Text)
                            cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", 0)
                            cmd.Parameters.AddWithValue("@RST_MARK", DBNull.Value)
                            cmd.Parameters.AddWithValue("@RST_COMMENTS", value)
                            cmd.Parameters.AddWithValue("@RST_GRADING", DBNull.Value)
                            cmd.Parameters.AddWithValue("@RST_USER", Session("sUsr_name"))
                            cmd.Parameters.AddWithValue("@bEdit", 0)
                            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                            cmd.ExecuteNonQuery()
                            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                            If iReturnvalue <> 0 Then
                                stTrans.Rollback()
                                objConn.Close()
                                lblError.Text = "Request could not be processed"
                                Return False
                            End If
                        End If
                    End With
                Next
            Next
            stTrans.Commit()
            objConn.Close()
            lblError.Text = ""
            Return True
        Catch ex As Exception
            stTrans.Rollback()
            objConn.Close()
            lblError.Text = "Request could not be processed"
            Return False
        End Try
    End Function


    Sub BindPhoto()
        Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
        Dim lblStuPhotoPath As Label = gvStudent.Rows(0).FindControl("lblStuPhotoPath")
        lblStudent.Text = lblStuName.Text
        If lblStuPhotoPath.Text <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + lblStuPhotoPath.Text
            imgStud.ImageUrl = strImagePath


        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

    Sub Show_HideRows(ByVal display As Boolean)

        tblTC.Rows(0).Visible = display
        tblTC.Rows(1).Visible = display
        tblTC.Rows(2).Visible = display
        tblTC.Rows(3).Visible = display
        tblTC.Rows(4).Visible = display

        tblTC.Rows(5).Visible = Not display
        tblTC.Rows(6).Visible = Not display
        tblTC.Rows(7).Visible = Not display
    End Sub

#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTeacherGrades()
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSection()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            PopulateSection()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Try

        Session("liUserList") = New List(Of String)
        Session("hashCheck") = Nothing

        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSCT_ID.Value = ddlSection.SelectedValue
        hfRSM_ID.Value = ddlReportCard.SelectedValue
        hfRPF_ID.Value = ddlPrintedFor.SelectedValue
        gvStudent.PageIndex = 0
        BindStudents()
        If gvStudent.Rows.Count > 0 Then
            Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
            hfSTU_ID.Value = lblStuId.Text
            BindPhoto()
        Else
            hfSTU_ID.Value = 0
            lblStudent.Text = ""
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        BindSubjects()
        BindValues()



        Show_HideRows(False)
        lblError.Text = ""
        'Catch ex As Exception
        '    lblError.Text = "Request could not be processed"
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Sub


    Protected Sub gvStudent_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudent.PageIndexChanged
        If gvStudent.Rows.Count > 0 Then
            Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
            hfSTU_ID.Value = lblStuId.Text
            BindPhoto()
            BindSubjects()
            BindValues()
        End If
    End Sub

    Protected Sub gvStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudent.PageIndexChanging
        gvStudent.PageIndex = e.NewPageIndex
        BindStudents()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        PopulateSection()
    End Sub

    Protected Sub gvSubject_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubject.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvMarks As GridView = e.Row.FindControl("gvMarks")
            Dim lblSbgId As Label = e.Row.FindControl("lblSbgId")
            BindHeader(gvMarks, lblSbgId.Text)
            If gvMarks.Rows.Count > 0 Then
                gvMarks.HeaderRow.Visible = False
            End If
        End If
    End Sub

    Protected Sub gvMarks_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblType As Label = e.Row.FindControl("lblType")
            Dim lblHeader As Label = e.Row.FindControl("lblHeader")
            Dim lblSbgId As Label = e.Row.FindControl("lblSbgId")
            Dim txtComments As TextBox = e.Row.FindControl("txtComments")
            Dim ddlGrades As DropDownList = e.Row.FindControl("ddlGrades")
            Dim lblRsdId As Label = e.Row.FindControl("lblRsdId")

            If lblType.Text = "txtmulti" Then
                Dim imgBtn As ImageButton = e.Row.FindControl("imgBtn")
                If Not imgBtn Is Nothing Then
                    imgBtn.OnClientClick = "javascript:getcomments('" + txtComments.ClientID + "','ALLCMTS','" + lblHeader.Text + "','" + hfSTU_ID.Value + "','" + hfRSM_ID.Value.ToString + "','" + lblSbgId.Text + "'); return false;"
                End If
            ElseIf lblType.Text = "dropdown" Then
                BindDefaultValues(ddlGrades, lblSbgId.Text, lblRsdId.Text)
            End If
        End If

    End Sub

    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        If gvStudent.PageIndex <> gvStudent.PageCount - 1 Then
            If SaveData() = False Then
                Exit Sub
            End If

            gvStudent.PageIndex += 1
            BindStudents()

            If gvStudent.Rows.Count > 0 Then
                Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
                hfSTU_ID.Value = lblStuId.Text
                BindPhoto()
                BindSubjects()
                BindValues()
            Else
                hfSTU_ID.Value = 0
                imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
                lblStudent.Text = ""
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSearch.Click
        Show_HideRows(True)
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        ViewState("MainMnu_code") = "PdfReport"

        Dim url As String = "~/Curriculum/Reports/Aspx/rptMonthlyProgressReportStudWise.aspx?" _
                          & "MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")) _
                          & "&datamode=" + Encr_decrData.Encrypt("view") _
                          & "&rsmid=" + Encr_decrData.Encrypt(hfRSM_ID.Value) _
                          & "&rpfid=" + Encr_decrData.Encrypt(hfRPF_ID.Value) _
                          & "&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                          & "&stuid=" + Encr_decrData.Encrypt(hfSTU_ID.Value) _
                          & "&grdid=" + Encr_decrData.Encrypt(hfGRD_ID.Value)

        ResponseHelper.Redirect(url, "_blank", "toolbar=0")


    End Sub
End Class

