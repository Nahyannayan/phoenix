Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReportAbsentees
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindHeader()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSection()
                    BindSubject()
                    tbStud.Rows(6).Visible = False
                    tbStud.Rows(7).Visible = False
                    tbStud.Rows(8).Visible = False
                    tbStud.Rows(9).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub PopulateSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND GRM_GRD_ID='" + grade(0) + "' and GRM_STM_ID=" + grade(1) + ") AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        
    End Sub
    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_GRD_ID='" + grade(0) + "' AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND SBG_STM_ID=" + grade(1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

    End Sub

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + RSM_ID _
                                 & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim RSM_ID As String = String.Empty
     
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_displayorder,STM_ID" _
                              & " FROM OASIS..grade_bsu_m AS A INNER JOIN RPT.REPORTSETUP_GRADE_S AS B" _
                              & " ON A.grm_grd_id=B.RSG_grd_id  " _
                              & " INNER JOIN OASIS..STREAM_M AS C ON A.grm_stm_id=C.stm_id  " _
                              & "  WHERE grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " ORDER BY RSG_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
      

    End Sub

    Sub GridBind()
       

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,STU_ID,STU_NO , " _
                                 & " CASE WHEN RTT_ATTENDANCE IS NULL THEN 'P' ELSE RTT_ATTENDANCE END AS ATT " _
                                 & " FROM OASIS..STUDENT_M AS A " _
                                 & " INNER JOIN STUDENT_GROUPS_S AS C ON A.STU_ID=C.SSD_STU_ID " _
                                 & " AND SSD_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                                 & " LEFT OUTER JOIN RPT.REPORT_STUDENT_ATTENDANCE AS B " _
                                 & " ON A.STU_ID=B.RTT_STU_ID AND RTT_RSD_ID=" + ddlHeader.SelectedValue.ToString _
                                 & " AND RTT_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                                 & " AND RTT_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                                 & " WHERE STU_SCT_ID=" + ddlSection.SelectedValue.ToString _
                                 & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
        
    End Sub

  
    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND RSD_bDIRECTENTRY='FALSE' ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim str_query As String
        Dim lblStuId As Label
        Dim ddlAttendance As DropDownList
        For i = 0 To gvStud.Rows.Count - 1
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            ddlAttendance = gvStud.Rows(i).FindControl("ddlAttendance")
            str_query = "EXEC RPT.saveEXAMATTENDANE " _
                      & lblStuId.Text + "," _
                      & ddlReportCard.SelectedValue.ToString + "," _
                      & ddlHeader.SelectedValue.ToString + "," _
                      & ddlPrintedFor.SelectedValue.ToString + "," _
                      & "'" + ddlGrade.SelectedValue.ToString + "'," _
                      & ddlSection.SelectedValue.ToString + "," _
                      & ddlAcademicYear.SelectedValue.ToString + "," _
                      & ddlSubject.SelectedValue.ToString + "," _
                      & ddlAttendance.SelectedValue.ToString
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next

    End Sub

#End Region


    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()
        BindGrade()
        PopulateSection()
        BindSubject()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSection()
        BindSubject()
    End Sub



    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
        BindSubject()
    End Sub

 
  
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        GridBind()
        tbStud.Rows(6).Visible = True
        tbStud.Rows(7).Visible = True
        tbStud.Rows(8).Visible = True
        tbStud.Rows(9).Visible = True
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub
End Class
