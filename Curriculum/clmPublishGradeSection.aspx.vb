Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmPublishGradeSection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300070") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindPrintedFor()
                    BindTerm()
                    GetHeaderCount()
                    BindGrade()
                    GridBind()
                    txtRelease.Text = Format(Now.Date, "dd/MMM/yyyy")
                    gvGrade.Attributes.Add("bordercolor", "#deffb4")  '#1b80b6
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function


    Sub GetHeaderCount()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If
        Dim str_query As String = "SELECT COUNT(RSD_ID) FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + RSM_ID _
                                 & " AND RSD_bDIRECTENTRY='FALSE'"
        hfHeaderCount.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

  
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + RSM_ID _
                                 & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + RSM_ID

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If


        str_query += " ORDER BY RSG_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)

    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,SCT_DESCR,SCT_ID FROM OASIS..GRADE_BSU_M AS A" _
        '                       & " INNER JOIN OASIS..SECTION_M AS B ON A.GRM_ID=B.SCT_GRM_ID " _
        '                       & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID " _
        '                       & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.GRM_GRD_ID=D.SBG_GRD_ID AND " _
        '                       & " A.GRM_STM_ID=D.SBG_STM_ID AND SBG_bREPCRD_DISPLAY='TRUE'" _
        '                       & " WHERE (SELECT COUNT(DISTINCT RST_ID) FROM RPT.REPORT_STUDENT_S WHERE RST_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
        '                       & " AND RST_SBG_ID=D.SBG_ID)=" + hfHeaderCount.Value


        Dim RSM_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If

        If ddlGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlGrade.SelectedValue.ToString
        Else
            GRD_ID = "0"
        End If


        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,SCT_DESCR,SCT_ID,GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..SECTION_M AS B ON A.GRM_ID=B.SCT_GRM_ID " _
                        & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID " _
                        & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.GRM_GRD_ID=D.SBG_GRD_ID " _
                        & " INNER JOIN OASIS..GRADE_M AS E ON A.GRM_GRD_ID=E.GRD_ID " _
                        & " WHERE  rtrim(ltrim(upper(SCT_DESCR)))<>'TEMP' AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND RSG_RSM_ID=" + RSM_ID

        If ddlGrade.SelectedValue <> "" Then
            str_query += " AND GRM_GRD_ID='" + GRD_ID + "'"
        End If

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND SCT_ID IN(select DISTINCT SCT_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If



        str_query += " ORDER BY GRD_DISPLAYORDER,SCT_DESCR"
                       

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGrade.DataSource = ds
        gvGrade.DataBind()

    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim str_query As String 
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim chkGenerate As CheckBox
        Dim lblSctId As Label
        Dim lblGrdId As Label
        Dim txtDate As TextBox

        Dim transaction As SqlTransaction

        For i = 0 To gvGrade.Rows.Count - 1
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                Transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    chkPublish = gvGrade.Rows(i).FindControl("chkPublish")
                    chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                    chkGenerate = gvGrade.Rows(i).FindControl("chkGenerate")
                    lblGrdId = gvGrade.Rows(i).FindControl("lblGrdId")
                    lblSctId = gvGrade.Rows(i).FindControl("lblSctId")
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")

                    If chkPublish.Checked = True Or chkRelease.Checked = True Then
                        str_query = "EXEC RPT.savePUBLISHRELEASESECTIONWISE " _
                                             & ddlReportCard.SelectedValue.ToString + "," _
                                             & ddlPrintedFor.SelectedValue.ToString + "," _
                                             & ddlAcademicYear.SelectedValue.ToString + "," _
                                             & ddlTerm.SelectedValue.ToString + "," _
                                             & "'" + lblGrdId.Text + "'," _
                                             & lblSctId.Text + "," _
                                             & chkPublish.Checked.ToString + "," _
                                             & chkRelease.Checked.ToString + "," _
                                             & IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'") + "," _
                                             & "'" + Session("sUsr_name") + "'," + chkGenerate.Checked.ToString + ""

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"
                Catch myex As ArgumentException
                    Transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    Transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using

        Next


    End Sub


    Sub CheckUncheck(ByVal gRow As GridViewRow)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim chkGenerate As CheckBox
        Dim lblGrdId As Label
        Dim lblSctId As Label

        lblGrdId = gRow.FindControl("lblGrdId")
        lblSctId = gRow.FindControl("lblSctId")
        chkPublish = gRow.FindControl("chkPublish")
        chkRelease = gRow.FindControl("chkrelease")
        chkGenerate = gRow.FindControl("chkGenerate")

        Dim count As Integer

        str_query = "SELECT COUNT(RPP_ID) FROM RPT.REPORT_STUDENTS_PUBLISH WHERE " _
                  & " RPP_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                  & " AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                  & " AND RPP_SCT_ID=" + lblSctId.Text + " AND RPP_bPUBLISH='TRUE'"
        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            chkPublish.Checked = False
        Else
            chkPublish.Checked = True
            chkPublish.Enabled = False
        End If


        str_query = "SELECT COUNT(RPP_ID) FROM RPT.REPORT_STUDENTS_PUBLISH WHERE " _
                          & " RPP_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                          & " AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                          & " AND RPP_SCT_ID=" + lblSctId.Text + " AND RPP_bRELEASEONLINE='TRUE'"
        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            chkRelease.Checked = False
        Else
            chkRelease.Checked = True
        End If

        str_query = "SELECT COUNT(RPP_ID) FROM RPT.REPORT_STUDENTS_PUBLISH WHERE " _
                          & " RPP_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                          & " AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                          & " AND RPP_SCT_ID=" + lblSctId.Text + " AND RPP_bGENERATE='TRUE'"
        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            chkGenerate.Checked = False
        Else
            chkGenerate.Checked = True
        End If



    End Sub

#End Region


    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindTerm()
        BindGrade()
        GridBind()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrade.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGrade.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblGrdId As Label
                Dim lblSctId As Label
                Dim lblGrade As Label
                Dim lblSection As Label

                With selectedRow
                    lblGrdId = .FindControl("lblGrdId")
                    lblSctId = .FindControl("lblSctId")
                    lblGrade = .FindControl("lblGrade")
                    lblSection = .FindControl("lblSection")
                End With

                Dim url As String
                url = String.Format("~\Curriculum\clmPublishReleaseStudents.aspx?" _
                                   & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                   & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                   & "&sctid=" + Encr_decrData.Encrypt(lblSctId.Text) _
                                   & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                   & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                   & "&trmid=" + Encr_decrData.Encrypt(ddlTerm.SelectedValue.ToString) _
                                   & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                                   & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                                   & "&reportcard=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                   & "&schedule=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text) _
                                  & "&MainMnu_code={0}&datamode={1}&viewid=", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGrade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGrade.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            CheckUncheck(e.Row)
        End If
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim txtDate As TextBox
            Dim chkPublish As CheckBox
            Dim chkRelease As CheckBox
            For i = 0 To gvGrade.Rows.Count - 1
                chkPublish = gvGrade.Rows(i).FindControl("chkPublish")
                chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                If chkPublish.Checked = True Or chkRelease.Checked = True Then
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtRelease.Text
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        GridBind()
    End Sub
End Class
