﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Data
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Class HifzTracker_ManageHifzTracker
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not Page.IsPostBack Then
            Dim isAccess As String = isHifzAccess()
            If (isAccess = "1") Then
                BindControls()
            Else
                lblError.Text = "Sorry! You have no access to this page."
                btnSearch.Visible = False
                gv_ManageHifzTracker.Visible = False
            End If
            BindControls()
        End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Page.IsValid Then
            BindManageHifzTrackerGrid()
            gv_ManageHifzTracker.DataBind()
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGrade(ddlAcademicYear.SelectedItem.Value)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindHifzTitle(ddlGrade.SelectedItem.Value)
        BindSection(ddlAcademicYear.SelectedItem.Value, ddlGrade.SelectedItem.Value)
    End Sub

    Protected Sub gv_ManageHifzTracker_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        BindManageHifzTrackerGrid()
    End Sub
    Private Sub BindManageHifzTrackerGrid()
        Try
            gv_ManageHifzTracker.DataSource = GetStudentHifzRelatedData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function GetStudentHifzRelatedData() As DataSet
        Dim academicYearId = If(Not String.IsNullOrEmpty(ddlAcademicYear.SelectedValue), Convert.ToInt32(ddlAcademicYear.SelectedValue), CType(Nothing, Integer?))
        Dim gradeId = ddlGrade.SelectedValue
        Dim titleText = ddlTitle.SelectedValue
        Dim sectionId = ddlSection.SelectedValue
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@BSU_ID", GetCurrentSchoolId())
        parameters(1) = New SqlParameter("@AcademicYearId", academicYearId)
        parameters(2) = New SqlParameter("@GradeId", gradeId)
        parameters(3) = New SqlParameter("@Title", titleText)
        parameters(4) = New SqlParameter("@SectionId", sectionId)
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetStudentHifzRelatedData", parameters)
    End Function
    Private Sub BindControls()
        BindAcademicYear()
        BindGrade()
        BindSection()
        BindHifzTitle()
    End Sub
    Private Sub BindAcademicYear()

        ddlAcademicYear.Items.Clear()
        Try
            Dim parameters As SqlParameter() = New SqlParameter(4) {}
            parameters(0) = New SqlParameter("@BSU_ID", GetCurrentSchoolId())
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetCurrentAcademicYearList", parameters)
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "AcademicYearDescription"
            ddlAcademicYear.DataValueField = "AcademicYearId"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub BindGrade(Optional ByVal academicYearId As String = "")

        ddlGrade.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(academicYearId) Then
                Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " & " ,grm_grd_id AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " & " and grade_bsu_m.grm_stm_id=stream_m.stm_id " & " and grm_acd_id=" & academicYearId & " order by grd_displayorder"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.Text, str_query)
                ddlGrade.DataSource = ds
                ddlGrade.DataTextField = "grm_display"
                ddlGrade.DataValueField = "grm_grd_id"
                ddlGrade.DataBind()
            End If

            ddlGrade.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub BindSection(Optional ByVal academicYearId As String = "", Optional ByVal gradeId As String = "")
        ddlSection.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(gradeId) Then
                Dim str_query As String = "SELECT sect.sct_id AS Id, sect.sct_descr AS Name  FROM dbo.section_m sect WHERE sect.sct_bsu_id = '" & GetCurrentSchoolId() & "' and sect.sct_acd_id = " & academicYearId & " and sect.sct_grd_id='" & gradeId & "'"

                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.Text, str_query)
                ddlSection.DataSource = ds
                ddlSection.DataTextField = "Name"
                ddlSection.DataValueField = "Id"
                ddlSection.DataBind()
            End If

            ddlSection.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindHifzTitle(Optional ByVal gradeId As String = "")

        ddlTitle.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(gradeId) Then
                Dim str_query As String = "SELECT hft.hft_title AS Title FROM [dbo].[Hifz_Tracker_M] hft WHERE hft_bsu_id = '" & GetCurrentSchoolId() & "' and  hft_grade_id='" & gradeId & "'"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.Text, str_query)
                ddlTitle.DataSource = ds
                ddlTitle.DataTextField = "Title"
                ddlTitle.DataValueField = "Title"
                ddlTitle.DataBind()
            End If

            ddlTitle.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function GetCurrentSchoolId() As String
        Return Convert.ToString(HttpContext.Current.Session("sBsuid"))
    End Function


    Private Function isHifzAccess() As String
        Dim username = Convert.ToString(HttpContext.Current.Session("sUsr_name"))
        Dim parameters As SqlParameter() = New SqlParameter(2) {}
        parameters(0) = New SqlParameter("@usr_name", username)
        Dim usraccess As String = String.Empty
        Try
            usraccess = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.Hifz_Usr_Access", parameters)
            Return usraccess
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
End Class
