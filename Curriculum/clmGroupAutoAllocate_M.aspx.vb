Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroupAutoAllocate_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'firm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100110") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt("C100050")
            Dim url As String
            url = String.Format("~\Curriculum\clmGroup_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
            'Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub PopulateGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                                 & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                             & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lstGrade.DataSource = ds
        lstGrade.DataTextField = "grm_display"
        lstGrade.DataValueField = "grm_grd_id"
        lstGrade.DataBind()

        Dim li As ListItem
        Dim index As Integer
        str_query = "SELECT DISTINCT SGR_GRD_ID,GRM_DISPLAY FROM GROUPS_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SGR_GRD_ID=B.GRM_GRD_ID" _
                   & " WHERE SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            li = New ListItem
            li.Value = reader.GetString(0)
            li.Text = reader.GetString(1)
            index = lstGrade.Items.IndexOf(li)
            If index <> -1 Then
                lstGrade.Items(index).Enabled = False
            End If

        End While
        reader.Close()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        With lstGrade
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str_query = "exec saveGROUPS_AUTOALLOCATE '" + .Items(i).Value + "'," + ddlAcademicYear.SelectedValue.ToString
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            Next
        End With
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            PopulateGrade()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
