﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmLessonPlannerStudentCriterias
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Session("Popup") = "0"
            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100549") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                '  gvStudent.Attributes.Add("bordercolor", "#1b80b6")

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindSubjects()
                BindGroup()
                BindTerm()
                btnSave.Visible = False
                ViewState("Sort") = ""
                'gvStudent.Attributes.Add("bordercolor", "#1b80b6")
                trGrid.Visible = False
                trSave.Visible = False
                h_TopicID.Value = ""
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        Else
            'reload data after savingthrough popup
            If Session("Popup") = "1" Then
                Session("Popup") = "0"
                btnView_Click(sender, e)
            End If
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                                  & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                  & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                  & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " _
                                  & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'"

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else

            'str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY  ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
            '              & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID" _
            '              & " ,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
            '              & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
            '              & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
            '              & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
            '              & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " _
            '              & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
            '              & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
            '              & " AND SGS_TODATE IS NULL" _
            '              & " ORDER BY GRD_DISPLAYORDER"

            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY  ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                        & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID" _
                        & " ,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                        & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                        & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " '_
            ' & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
            ' & " AND SGS_TODATE IS NULL" _
            ' & " ORDER BY GRD_DISPLAYORDER"


        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubjects()
        Dim grd_id() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                       & " AND SBG_STM_ID=" + grd_id(1)
        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                & " AND SBG_STM_ID=" + grd_id(1) _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub



    Sub GridBind()
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT STU_ID,STU_FIRSTNAME,ISNULL(STU_LASTNAME,ISNULL(STU_MIDNAME,'')) AS STU_LASTNAME," _
                       & " STU_NO FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                       & " WHERE STU_CURRSTATUS<>'CN' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO=" + txtStudentID.Text
        End If

        If txtStudentName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtStudentName.Text + "'%"
        End If



        str_query += " ORDER BY STU_FIRSTNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As DataView = ds.Tables(0).DefaultView

        If ViewState("SortFilter") = "First Name" Then
            dv.Sort = "STU_FIRSTNAME"
        End If

        If ViewState("SortFilter") = "Last Name" Then
            dv.Sort = "STU_LASTNAME"
        End If

        gvStudent.DataSource = dv
        gvStudent.DataBind()

        GetCriterias(sbg_id(0))
        GetData(sbg_id(1), sbg_id(0))
    End Sub


    Sub GetCriterias(ByVal sbg_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        If txtTopic.Text = "" Then
            str_query = "SELECT SYC_ID,SYC_DESCR,SYC_STARTDT,SYC_SYD_ID FROM SYL.LESSON_CRITERIA_M " _
                                & " INNER JOIN SYL.SYLLABUS_D ON SYC_SYD_ID=SYD_ID " _
                                & " WHERE SYC_SBG_ID=" + sbg_id _
                                & " AND ISNULL(SYC_bEVALUATION,0)=1"

        Else
            str_query = "SELECT SYC_ID,SYC_DESCR,SYC_STARTDT,SYC_SYD_ID FROM SYL.LESSON_CRITERIA_M " _
                                    & " INNER JOIN SYL.SYLLABUS_D ON SYC_SYD_ID=SYD_ID " _
                                    & " WHERE SYC_SBG_ID=" + sbg_id _
                                    & " AND (SYD_ID=" + h_TopicID.Value & " OR SYD_PARENT_ID=" + h_TopicID.Value + ") " _
                                    & " AND ISNULL(SYC_bEVALUATION,0)=1"

        End If

        str_query += " ORDER BY SYC_STARTDT,SYC_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        Dim j, k As Integer

        Dim hdr, hdrId, dt, syd As String

        Dim lblHdr As Label
        Dim lblHdrId As Label
        Dim lblDt As Label
        Dim lblSydID As Label

        If rdObj2.Checked = True Then
            If ds.Tables(0).Rows.Count > 25 Then
                j = 25
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj3.Checked = True Then
            If ds.Tables(0).Rows.Count > 50 Then
                j = 50
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj4.Checked = True Then
            If ds.Tables(0).Rows.Count > 75 Then
                j = 75
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        Else
            j = 0
            If ds.Tables(0).Rows.Count > 25 Then
                k = 25
            Else
                k = ds.Tables(0).Rows.Count
            End If
        End If

        Dim p As Integer = 1
        For i = j To k - 1
            hdr = "lblObj" + CStr(p) + "H"
            hdrId = "lblObjId" + CStr(p)
            dt = "lblObjDt" + CStr(p)
            syd = "lblSydID" + CStr(p)

            lblHdr = gvStudent.HeaderRow.FindControl(hdr)
            lblHdrId = gvStudent.HeaderRow.FindControl(hdrId)
            lblDt = gvStudent.HeaderRow.FindControl(dt)
            lblSydID = gvStudent.HeaderRow.FindControl(syd)

            lblHdr.Text = ds.Tables(0).Rows(i).Item(1)
            lblHdrId.Text = ds.Tables(0).Rows(i).Item(0)
            lblSydID.Text = ds.Tables(0).Rows(i).Item(3)
            If Not lblDt Is Nothing Then
                lblDt.Text = Format(CDate(ds.Tables(0).Rows(i).Item(2).ToString), "dd-MMM")
            End If
            lblHdr.Attributes.Add("title", ds.Tables(0).Rows(i).Item(1))

            'If ds.Tables(0).Rows(i).Item(2) = True Then
            '    lblHdr.ForeColor = Drawing.Color.Maroon
            'End If
            p += 1
        Next


        'Dim lblFirstName As Label
        'Dim lblLastName As Label
        'lblFirstName = gvStudent.Rows(0).FindControl("lblFirstName")
        'lblLastName = gvStudent.Rows(0).FindControl("lblLastName")
        'Dim lblObj1H As Label = gvStudent.HeaderRow.FindControl("lblObj1H")

        'gvStudent.Rows(0).Attributes.Add("title", lblFirstName.Text + " " + lblLastName.Text + vbCrLf + lblObj1H.Text)

        'gvStudent.HeaderRow.Cells(0).CssClass = "locked"
        gvStudent.HeaderRow.Cells(1).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(2).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(3).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(4).CssClass = "FrozenCell"

        'For i = 0 To gvStudent.Rows.Count - 1
        '    gvStudent.Rows(i).Cells(0).CssClass = "locked"
        '    gvStudent.Rows(i).Cells(1).CssClass = "locked"
        '    gvStudent.Rows(i).Cells(2).CssClass = "locked"
        'Next



    End Sub


    Sub GetData(ByVal sbm_id As String, ByVal sbg_id As String)
        Dim objData As New Hashtable
        Dim str_query As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If txtTopic.Text <> "" Then
            str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS FROM SYL.STUDENT_LESSONCRITERIA_S" _
                               & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
                               & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
                               & " WHERE (SYD_ID=" + h_TopicID.Value + " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND STC_SBG_ID=" + sbg_id _
                               & " AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        Else
            str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS FROM SYL.STUDENT_LESSONCRITERIA_S" _
                                   & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
                                   & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
                                   & " WHERE  STC_SBG_ID=" + sbg_id _
                                   & " AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim j As Integer

        Dim lblHdr As Label
        Dim lblHdrId As Label
        Dim lblSydId As Label

        Dim ddlObj As DropDownList
        Dim lblFirstName As LinkButton
        Dim lblLastName As Label
        Dim lblStuId As Label
        Dim lblStuNo As Label

        Dim strLink As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvStudent.Rows.Count - 1
            lblFirstName = gvStudent.Rows(i).FindControl("lblFirstName")
            lblLastName = gvStudent.Rows(i).FindControl("lblLastName")
            lblStuId = gvStudent.Rows(i).FindControl("lblStuId")
            lblStuNo = gvStudent.Rows(i).FindControl("lblStuNo")


            strLink = "clmLessonStudentCriterias_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                     & "&grade=" + ddlGrade.SelectedItem.Text _
                     & "&group=" + ddlGroup.SelectedItem.Text _
                     & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                     & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                     & "&sbgid=" + sbg_id _
                     & "&grdid=" + ddlGrade.SelectedValue.ToString _
                     & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                     & "&stuno=" + lblStuNo.Text _
                     & "&topic=" + txtTopic.Text _
                     & "&topicid=" + h_TopicID.Value
            ' lblFirstName.Attributes.Add("onclick", "javascript:var popup = window.showModalDialog('" + strLink + "','','dialogHeight:600px;dialogWidth:700px;scroll:no;resizable:no;status:no;');")
            lblFirstName.Attributes.Add("onclick", "javascript:var popup = Popup('" + strLink + "');")

            For j = 0 To 24
                lblHdr = gvStudent.HeaderRow.FindControl("lblObj" + CStr(j + 1) + "H")
                lblHdrId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                lblSydId = gvStudent.HeaderRow.FindControl("lblSydId" + CStr(j + 1))
                If lblHdrId.Text <> "" And lblHdrId.Text <> "0" Then
                    ddlObj = gvStudent.Rows(i).FindControl("ddlObj" + CStr(j + 1) + "Id")
                    If Not objData.Item(lblStuId.Text + "_" + lblHdrId.Text) Is Nothing Then
                        '  chkObj.Checked = objData.Item(lblStuId.Text + "_" + lblHdrId.Text)
                        If Not ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text)) Is Nothing Then
                            ddlObj.ClearSelection()
                            ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text)).Selected = True
                            If ddlObj.SelectedItem.Value.ToString = "E" Then
                                ddlObj.Style.Add("color", "green")
                            ElseIf ddlObj.SelectedItem.Value.ToString = "M" Then
                                ddlObj.Style.Add("color", "#ffe30d")
                            ElseIf ddlObj.SelectedItem.Value.ToString = "W" Then
                                ddlObj.Style.Add("color", "red")
                            Else
                                ddlObj.Style.Add("color", "black")
                            End If
                        End If
                    End If
                    'chkObj.Attributes.Add("title", lblFirstName.Text + " " + lblLastName.Text + vbCrLf + lblHd r.Text)
                    gvStudent.Columns(6 + j).Visible = True
                Else
                    gvStudent.Columns(6 + j).Visible = False
                End If
            Next
        Next

    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlObj As DropDownList
        Dim lblStuId As Label
        Dim lblSydId As Label

        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim grd_id As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim strXml As String = ""

        For i = 0 To gvStudent.Rows.Count - 1
            With gvStudent.Rows(i)
                For j = 0 To 24
                    lblObjId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                    lblSydId = gvStudent.HeaderRow.FindControl("lblSydId" + CStr(j + 1))
                    ddlObj = .FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblStuId = .FindControl("lblStuId")

                    If lblObjId.Text <> "" And lblObjId.Text <> "0" Then
                        strXml += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID>"
                        strXml += "<SYC_ID>" + lblObjId.Text + "</SYC_ID>"
                        strXml += "<STC_STATUS>" + ddlObj.SelectedValue + "</STC_STATUS>"
                        strXml += "<SYD_ID>" + lblSydId.Text + "</SYD_ID></ID>"
                    End If
                Next
            End With
        Next

        strXml = "<IDS>" + strXml + "</IDS>"
        str_query = " EXEC [SYL].[saveSTUDENTCRITERIAS_BULK]" _
                & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                & " @GRD_ID ='" + grd_id(0) + "'," _
                & " @SBG_ID =" + sbg_id(0) + "," _
                & " @STC_XML ='" + strXml + "'," _
                & " @STC_UPDATEDATE ='" + IIf(txtDate.Text = "", Now.ToString, txtDate.Text) + "'," _
                & " @STC_USER ='" + Session("susr_id") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        lblError.Text = "Record saved successfully"
    End Sub


#End Region

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        'If txtTopic.Text = "" Then
        '    lblError.Text = "Please select a topic"
        '    Exit Sub
        'Else
        '    lblError.Text = ""
        'End If

        ViewState("SortFilter") = ""
        '  h_LVL_ID.Value = ddlLevel.SelectedValue.ToString
        GridBind()
        btnSave.Visible = True
        trGrid.Visible = True
        trSave.Visible = True
    End Sub

    Protected Sub lnkFirstNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "First Name"
        GridBind()
    End Sub

    Protected Sub lnkLastNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "Last Name"
        GridBind()
    End Sub


    Protected Sub gvStudent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudent.RowDataBound
        '  Dim ddlObj As DropDownList
        If e.Row.RowType = DataControlRowType.DataRow Then
            'For count As Integer = 1 To e.Row.Cells.Count - 6
            '    Dim ddlObj As DropDownList = CType(e.Row.FindControl("ddlObj" + count.ToString() + "Id"), DropDownList)
            '    If ddlObj.SelectedItem.Value.ToString = "1" Then
            '        ddlObj.Style.Add("color", "green")
            '    ElseIf ddlObj.SelectedItem.Value.ToString = "2" Then
            '        ddlObj.Style.Add("color", "#ffe30d")
            '    ElseIf ddlObj.SelectedItem.Value.ToString = "3" Then
            '        ddlObj.Style.Add("color", "red")
            '    Else
            '        ddlObj.Style.Add("color", "black")
            '    End If
            'Next
            'e.Row.Cells(0).CssClass = "locked"
            'e.Row.Cells(1).CssClass = "locked"
            'e.Row.Cells(2).CssClass = "locked"
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()

        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindGrade()
        BindSubjects()
        BindGroup()
        BindTerm()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindSubjects()
        BindGroup()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindGroup()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub
End Class
