<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportProcess_KGBrownBook.aspx.vb" Inherits="Curriculum_clmReportProcess_KGBrownBook" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Processing Report
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--<table id="Table1" width="100%" border="0">
        <tbody>
            <tr style="font-size: 12pt">
                <td class="title" align="left" width="50%">
                    <asp:Literal ID="ltHeader" runat="server" Text="Processing Report"></asp:Literal></td>
            </tr>
        </tbody>
    </table>--%>

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<span style="display: block; left: 0px; float: left">--%>
                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                <span style="font-size: 8pt; color: #800000">&nbsp;</span>
                            </div>
                            <%--  </span>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--  <tr>
                                    <td colspan="2">

                                        <asp:Literal ID="ltLabel" runat="server" Text="Filter Condition" ></asp:Literal>
                                        <span class="field-label">Filter Condition</span>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Section</span></td>

                                    <td align="left" width="30%">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstSection" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                        <asp:Label ID="lblNote" runat="server"></asp:Label></td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

