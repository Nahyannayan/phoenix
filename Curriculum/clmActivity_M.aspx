<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmActivity_M.aspx.vb" Inherits="Curriculum_clmActivity_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
 
function change_chk_state(chkdObj, secObj)
 {
    var chckedObj = document.getElementById(chkdObj); 
    var chkOtherObj = document.getElementById(secObj); 
    if(chckedObj.checked)
    {
     document.getElementById(secObj).checked = false;   
    }
  }

</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Assessment Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" width="100%">
        <tr>
            <td align="left" >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"  style="text-align: center" ></asp:Label>
           
            </td>
        </tr>
        <tr>
            <td class="matters"   valign="top">
                <table width="100%">
                         <tr>
                        <td align="left" class="matters" width="20%"  >
                          <span class="field-label" >  Description</span></td>
                        <td align="left" class="matters"   width="30%">
                               <asp:TextBox ID="txtCLM_DESC" runat="server" TabIndex="1" MaxLength="150"  ></asp:TextBox>
                            <td width="20%"></td>
                            <td width="30%"></td>
                      </td>          
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <asp:CheckBox ID="chkbHasPeriod" runat="server" Text="Does this assessment spread over a period of time ?"  /><br />
                            <asp:CheckBox ID="chkHasTime_Duration" runat="server" Text="Does this assessment has Time and Duration?" /></td>
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td class="matters" align="center" valign="bottom">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                </td>
        </tr>
 
    </table>


            </div>
        </div>
    </div>

</asp:Content>

