Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmPublishReleaseStudents
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300095") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'GridBind()
                    hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgrid").Replace(" ", "+"))
                    hfCAD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("cadid").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("cad").Replace(" ", "+"))

                    GRIDBIND()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GRIDBIND()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT Sta_ID,Stu_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') as Stu_Name," _
                                & " ISNULL(STA_bRELEASEONLINE,'FALSE') AS bRELEASE," _
                                & " STA_RELEASEDATE" _
                                & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B ON " _
                                & " A.STU_ID=B.STA_STU_ID And STA_SGR_ID = " + hfSGR_ID.Value + " And STA_CAD_ID = " + hfCAD_ID.Value _
                                & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim transaction As SqlTransaction
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
         Dim txtDate As TextBox
        Dim str_query As String
        Dim lblStuId As Label
        Dim lblStaId As Label

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvStud.Rows.Count - 1

                    chkRelease = gvStud.Rows(i).FindControl("chkRelease")
                    lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                    lblStaId = gvStud.Rows(i).FindControl("lblStaId")
                      txtDate = gvStud.Rows(i).FindControl("txtDate")
                    str_query = "exec ACT.saveRELEASEAOL_STUDENT " _
                             & lblStaId.Text + "," _
                             & chkRelease.Checked.ToString + "," _
                             & IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim txtDate As TextBox
            Dim chkRelease As CheckBox
            For i = 0 To gvStud.Rows.Count - 1
                chkRelease = gvStud.Rows(i).FindControl("chkRelease")
                If chkRelease.Checked = True Then
                    txtDate = gvStud.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtRelease.Text
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub
End Class
