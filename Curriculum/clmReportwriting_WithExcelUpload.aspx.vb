﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports System.Data.OleDb
Imports GemBox.Spreadsheet
Partial Class Curriculum_clmReportwriting_WithExcelUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330334") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                BindGrade()
                BindSubject()
                BindGroup()
                BindReportCard()
                If ddlPrintedFor.Items.Count > 0 Then
                    PopulateHeaderTree()
                End If
                End If

                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '    lblError.Text = "Request could not be processed"
                'End Try
        End If
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadTemplate)
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + Session("CURRENT_ACD_ID")
                If ViewState("GRD_ACCESS") > 0 Then
                    str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                             & " WHERE (GSA_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                             & " UNION" _
                             & " SELECT SGR_GRD_ID FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID WHERE SGR_ACd_ID=" + Session("CURRENT_ACD_ID") _
                             & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") + " AND SGS_TODATE IS NULL" _
                             & ")"
                End If
                str_query += " order by grd_displayorder"
            Else

                str_query = "SELECT DISTINCT " & _
                            "CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY + '-' + STM_DESCR END AS GRM_DISPLAY,   " & _
                            " GRADE_BSU_M.GRM_GRD_ID + '|' + CONVERT(VARCHAR(100), STREAM_M.STM_ID) AS GRM_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
                            " STREAM_M.STM_ID FROM GROUPS_TEACHER_S INNER JOIN " & _
                            " GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID  " & _
                            " AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
                            "GRADE_BSU_M INNER JOIN " & _
                            " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
                            " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID ON GROUPS_M.SGR_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                            " GROUPS_M.SGR_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND GROUPS_M.SGR_STM_ID = GRADE_BSU_M.GRM_STM_ID AND " & _
                            " GROUPS_M.SGR_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & " WHERE " _
                                  & "  grm_acd_id=" + Session("CURRENT_ACD_ID") & " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId")

                str_query += " order by grd_displayorder"

            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        ddlSubject.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")



        strCondition = " AND SBG_GRD_ID='" & grade(0) & "' AND SBG_STM_ID='" + grade(1) + "'"

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & Session("CURRENT_ACD_ID") & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR2"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & Session("CURRENT_ACD_ID") & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR2"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "DESCR2"
        ddlSubject.DataValueField = "ID"
        ddlSubject.DataBind()

    End Sub
    Sub BindGroup()
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")


        strCondition += " AND SGR_GRD_ID='" + grade(0) + "'"



        strCondition += " AND SGR_SBG_ID='" & ddlSubject.SelectedValue & "'"


        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"

            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "DESCR2"
        ddlGroup.DataValueField = "ID"
        ddlGroup.DataBind()

    End Sub

    Sub BindReportCard()
        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CONVERT(VARCHAR(100),RPF_ID)+'|'+CONVERT(VARCHAR(100),RPF_RSM_ID) RPF_ID,RPF_DESCR, " _
                                   & " ISNULL(RPF_REPORTORDER,RPF_DISPLAYORDER) FROM RPT.REPORT_PRINTEDFOR_M " _
                                   & " INNER JOIN RPT.REPORT_SETUP_M ON RPF_rSM_ID=RSM_ID " _
                                   & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID " _
                                   & " WHERE RSM_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND RSG_GRD_ID='" + grade(0) + "'" _
                                   & " AND ISNULL(RSM_bENABLEUPLOAD,0)=1"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    


    Public Function GetSelectedNode(Optional ByVal seperator As String = "||") As String
        Dim strBSUnits As New StringBuilder
        For Each node As TreeNode In tvHeaders.CheckedNodes
            If node.Value.Length > 2 Then
                strBSUnits.Append(node.Value)
                strBSUnits.Append(seperator)
            End If
        Next
        Return strBSUnits.ToString()
    End Function

    Public Sub ExpandOne(ByVal Level As Integer)
        tvHeaders.ExpandDepth = Level
        tvHeaders.DataBind()
    End Sub



    Private Sub PopulateSubLevel(ByVal parentid As String, _
      ByVal parentNode As TreeNode)
        PopulateNodes(GetReportHeaders(parentid, parentNode.Value), parentNode.ChildNodes)
    End Sub

    Function GetReportHeaders(parentid As String, headerId As String) As DataTable
        Dim rpf As String() = ddlPrintedFor.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String = "exec  RPT.GETREPORT_HEADERS " + rpf(1) + "," _
                                        & ddlSubject.SelectedValue.ToString + "," _
                                        & headerId + "," _
                                        & parentid
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Return ds.Tables(0)
    End Function
    Private Sub PopulateRootLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        tvHeaders.Nodes.Clear()
        str_Sql = "SELECT 0 AS HEADER_ID,'All' AS HEADER_DESC, COUNT (*) AS childnodecount FROM RPT.REPORT_SETUP_D WHERE RSD_ID=1"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvHeaders.Nodes)
        PopulateSubLevel(0, tvHeaders.Nodes(0))
        Dim node As TreeNode
        For Each node In tvHeaders.Nodes(0).ChildNodes
            PopulateSubLevel(1, node)
        Next
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("HEADER_DESC").ToString()
            tn.Value = dr("HEADER_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            nodes.Add(tn)


            'If node has child nodes, then enable on-demand populating
            '  tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateHeaderTree() 'Generate Tree
        tvHeaders.Nodes.Clear()
        PopulateRootLevel()

        tvHeaders.DataBind()
        tvHeaders.CollapseAll()
    End Sub

#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        BindGroup()
        BindReportCard()
        If ddlPrintedFor.Items.Count > 0 Then
            PopulateHeaderTree()
        End If
    End Sub

    Function getHeaders(Optional allHeaders As Boolean = False) As String
        Dim str As String = ""

        Dim pNode As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each pNode In tvHeaders.Nodes
            For Each cNode In pNode.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If allHeaders = False Then
                        If ccNode.Checked = True Then
                            If str <> "" Then
                                str += "|"
                            End If
                            str += ccNode.Value
                        End If
                    Else
                        If str <> "" Then
                            str += "|"
                        End If
                        str += ccNode.Value
                    End If
                Next
            Next
        Next
        Return str
    End Function
    Sub ExportExcel()
        Dim strHeaders As String = GetHeaders()
        If strHeaders = "" Then
            lblError.Text = "Please select a header"
            Exit Sub
        End If
        Dim rpf As String() = ddlPrintedFor.SelectedValue.ToString.Split("|")
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim Form1 As New System.Web.UI.HtmlControls.HtmlForm
        Form1.Controls.Clear()
        Form1.Controls.Add(gvSubj)
        gvSubj.Visible = True
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC RPT.GETREPORTDATA_FOREXCELUPLOAD " _
                                 & "@ACD_ID='" + Session("CURRENT_ACD_ID") + "'," _
                                 & "@GRD_ID='" + grade(0) + "'," _
                                 & "@RSM_ID='" + rpf(1) + "'," _
                                 & "@RPF_ID='" + rpf(0) + "'," _
                                 & "@SBG_ID='" + ddlSubject.SelectedValue.ToString + "'," _
                                 & "@SGR_ID='" + ddlGroup.SelectedValue.ToString + "'," _
                                 & "@RSD_IDS='" + strHeaders + "'"

        Dim sheetName As String = ddlGroup.SelectedItem.Text + "_" + ddlPrintedFor.SelectedItem.Text
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + sheetName + "_" + Session("sUsr_name") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"


        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add(sheetName)
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)

        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        'gvSubj.DataSource = ds
        'gvSubj.DataBind()

        'Dim sw As New StringWriter()
        'Dim htmlWriter As New HtmlTextWriter(sw)
        'gvSubj.RenderControl(htmlWriter)

        'Response.Clear()
        'Response.ClearHeaders()
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("content-disposition", "attachment;filename=" + sheetName + ".xls")
        'Response.Write(sw.ToString())
        'Response.Flush()
        'Response.End()
        ' gvSubj.Visible = False
    End Sub


    Sub ShowData(rsm_id As String, rpf_id As String, grade As String, strHeaders As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC RPT.GETREPORTDATA_FOREXCELUPLOAD " _
                                 & "@ACD_ID='" + Session("CURRENT_ACD_ID") + "'," _
                                 & "@GRD_ID='" + grade + "'," _
                                 & "@RSM_ID='" + rsm_id + "'," _
                                 & "@RPF_ID='" + rpf_id + "'," _
                                 & "@SBG_ID='" + ddlSubject.SelectedValue.ToString + "'," _
                                 & "@SGR_ID='" + ddlGroup.SelectedValue.ToString + "'," _
                                 & "@RSD_IDS='" + strHeaders + "'"

          Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubj.DataSource = ds
        gvSubj.DataBind()
        gvSubj.Visible = True
    End Sub
    Private Sub UpLoadExcelFile()
        Dim strFileNameOnly As String
        Dim xmlString As String = ""

        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                '  Dim PhotoVirtualpath = System.Configuration.ConfigurationManager.AppSettings("ReportFileUpload") 'Server.MapPath("~/Curriculum/ReportUploads/")
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportUploads/")
                strFileNameOnly = Session("sBsuid") & "_" & ddlGroup.SelectedItem.Text + "_" + ddlPrintedFor.SelectedItem.Text + "_" + Format(Date.Now, "ddMMyyHmmss").Replace("/", "_") + ".xlsx"
                uploadFile.SaveAs(PhotoVirtualpath & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.ACE.OLEDB.12.0;" & _
                "Data Source=" & PhotoVirtualpath & strFileNameOnly & ";" & _
                 "Extended Properties=""Excel 12.0;HDR=YES;"""

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(PhotoVirtualpath & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim icount As Integer = dt.Rows.Count


                Dim strCon As String = ConnectionManger.GetOASISConnectionString
                Dim stu_id As String = ""
                Dim dsStu As DataSet
                Dim str_query As String = ""
                Dim r As Integer
                Dim c As Integer
                For c = 2 To dt.Columns.Count - 1
                    For r = 0 To dt.Rows.Count - 1
                        xmlString += "<ID><STUDENTID>" + dt.Rows(r).Item(0).ToString + "</STUDENTID>"
                        xmlString += "<STUDENTNAME>" + dt.Rows(r).Item(1).ToString + "</STUDENTNAME>"
                        xmlString += "<HEADER>" + dt.Columns(c).ColumnName + "</HEADER>"
                        xmlString += "<MARKS>" + dt.Rows(r).Item(c).ToString + "</MARKS></ID>"
                    Next
                Next

                xmlString = "<IDS>" + xmlString + "</IDS>"

                Dim cmd As New SqlCommand
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Try
                    Dim rpf As String() = ddlPrintedFor.SelectedValue.Split("|")
                    Dim grd_id As String() = ddlGrade.SelectedValue.ToString.Split("|")

                    cmd = New SqlCommand("[RPT].[SAVE_REPORT_STUDENT_EXCELUPLOAD]", objConn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ACD_ID", Session("CURRENT_ACD_ID"))
                    cmd.Parameters.AddWithValue("@RSM_ID", rpf(1))
                    cmd.Parameters.AddWithValue("@RPF_ID", rpf(0))
                    cmd.Parameters.AddWithValue("@GRD_ID", grd_id(0))
                    cmd.Parameters.AddWithValue("@SBG_ID", ddlSubject.SelectedValue.ToString)
                    cmd.Parameters.AddWithValue("@SGR_ID", ddlGroup.SelectedValue.ToString)
                    cmd.Parameters.AddWithValue("@USER", Session("susr_name"))
                    Dim p As SqlParameter
                    p = cmd.Parameters.AddWithValue("@data", xmlString)
                    p.SqlDbType = SqlDbType.Xml

                    Dim strHeaders As String = cmd.ExecuteScalar()

                    ShowData(rpf(1), rpf(0), grd_id(0), strHeaders)
                Catch ex As Exception
                    lblError.Text = "Error: " & ex.Message.ToString
                Finally
                    cmd.Dispose()
                End Try

            Catch ex As Exception
                lblError.Text = "Error: " & ex.Message.ToString
            End Try
        Else
                lblError.Text = "Please select a file to upload."
        End If


    End Sub

   

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
        PopulateHeaderTree()
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        PopulateHeaderTree()
    End Sub

    Protected Sub btnDownloadTemplate_Click(sender As Object, e As EventArgs) Handles btnDownloadTemplate.Click
        'Dim url As String = String.Format("~\Curriculum\clmExportExcelGrid.aspx?MainMnu_code={0}&datamode={1}", _
        '                                   ViewState("MainMnu_code"), ViewState("datamode"))
        ''   ResponseHelper.Redirect(url, "_blank", "width=100,height=100")
        'ResponseHelper.Redirect(url, "_blank", "")
        lblError.Text = ""
        ExportExcel()
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        lblError.Text = ""
        gvSubj.Visible = False
        UpLoadExcelFile()
    End Sub

    Protected Sub gvSubj_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSubj.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim i As Integer
            For i = 0 To e.Row.Cells.Count - 1
                If e.Row.Cells(i).Text = "&nbsp;" Then
                    e.Row.Cells(i).BackColor = Drawing.Color.LightCoral
                End If
            Next
        End If
    End Sub
End Class
