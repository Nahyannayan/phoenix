<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmEditPublihedData.aspx.vb" Inherits="Curriculum_clmEditPublihedData" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Edit Published Marks"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%" id="tblStudent" runat="server">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Select Report Card</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="1"><span class="field-label">Select Report Schedule</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade</span></td>

                                    <td align="left">&nbsp;<asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                                    <td align="left" colspan="1"><span class="field-label">Select Section</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Subject</span></td>
                                    <td align="left" colspan="1">

                                        <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbSubject" Width="100%"
                                            MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                            HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                            OnDataBound="cmbSubject_DataBound" OnItemDataBound="cmbSubject_ItemDataBound"
                                            EmptyMessage="Start typing to search...">
                                            <HeaderTemplate>
                                                <ul>

                                                    <li class="col2">Subject Name</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>

                                                    <li class="col2">
                                                        <%# DataBinder.Eval(Container.DataItem, "SBG_DESCR")%></li>
                                                </ul>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                                items
                                            </FooterTemplate>
                                        </telerik:RadComboBox>

                                    </td>
                                    <td align="left"><span class="field-label">Select Term</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>


                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Student</span></td>
                                    <td align="left" colspan="1">
                                        <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbStudent" Width="100%"
                                            MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                            HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                            OnDataBound="cmbStudent_DataBound" OnItemDataBound="cmbStudent_ItemDataBound"
                                            EmptyMessage="Start typing to search...">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col1">Student Code</li>
                                                    <li class="col2">Student Name</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col1">
                                                        <%# DataBinder.Eval(Container.DataItem, "stu_no")%></li>
                                                    <li class="col2">
                                                        <%# DataBinder.Eval(Container.DataItem, "StudentName")%></li>
                                                </ul>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                                items
                                            </FooterTemplate>
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnList" runat="server" CssClass="button" Text="List" ValidationGroup="groupM1" TabIndex="7" /></td>
                                </tr>








                                <tr>
                                    <td align="left" colspan="8">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lbtnDetail" runat="server">View Rule</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lbtnMark" runat="server">View Processed Marks</asp:LinkButton></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="10">
                                        <asp:GridView ID="gvActivities" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No data."
                                            PageSize="20" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />


                                            <Columns>


                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" Text='<%# Bind("CAS_ID") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStaId" Text='<%# Bind("STA_ID") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Assesment">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" Text='<%# Bind("CAS_DESC") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMark" Text='<%# Bind("STA_MARK") %>' runat="server"></asp:Label>
                                                        <asp:TextBox ID="txtMark" Visible="false" Text='<%# Bind("STA_MARK") %>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" Width="15%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" Text='<%# Bind("STA_GRADE") %>' runat="server"></asp:Label>
                                                        <asp:TextBox ID="txtGrade" Visible="false" Text='<%# Bind("STA_GRADE") %>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" Width="15%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" Enabled='<%# Bind("ENABLE") %>' runat="server">Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancel" OnClick="lnkCancel_Click" Visible="false" runat="server">Cancel</asp:LinkButton>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>

                                            </Columns>


                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="10">
                                        <asp:Button ID="btnProcess" runat="server" CssClass="button" Text="Process" TabIndex="7" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="10">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="3">
                                                    <asp:GridView ID="gvHeader" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No Rules"
                                                        PageSize="20" BorderStyle="None">
                                                        <RowStyle CssClass="griditem" Wrap="False" />

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRsdId" Text='<%# Bind("RSD_ID") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMark" Text='<%# Bind("RSD_MARK") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" Text='<%# Bind("RSD_GRADE") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="Header">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHeader" Text='<%# Bind("RSD_HEADER") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Marks">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtMark" Text='<%# Bind("RSD_MARK") %>' runat="server"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="center" Width="15%" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtGrade" Text='<%# Bind("RSD_GRADE") %>' runat="server"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="center" Width="15%"></ItemStyle>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                        <AlternatingRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="10">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" /></td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom">&nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;<asp:HiddenField ID="hfRRM_ID" runat="server" />
                            <asp:HiddenField ID="hfSBG_ID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfRPF_ID" runat="server" />
                            <asp:HiddenField ID="hfRSM_ID" runat="server" />
                            <asp:HiddenField
                                ID="hfSubjectURL" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfTRM_ID" runat="server" />
                            <asp:HiddenField
                                ID="hfStudentUrl" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                        </td>

                    </tr>
                </table>
                <div id="popup" runat="server" class="panel-cover" style="display: none;">
                    <div class="title-bg-small">Report Processing Rule For Selected Subject</div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="modalPopup1" >
                        <asp:Literal ID="ltProcessRule" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server"
                    TargetControlID="lbtnDetail"
                    PopupControlID="popup"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="0"
                    OffsetY="20"
                    PopDelay="50" />



                <div id="popup2" runat="server" class="panel-cover" style="display: none;">
                    <div class="title-bg-small">
                        Marks
                    </div>
                    <asp:Panel ID="PopupMenu1" ScrollBars="Vertical" runat="server" CssClass="modalPopup1">
                        <asp:Literal ID="ltMarks" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="Server"
                    TargetControlID="lbtnMark"
                    PopupControlID="popup2"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="0"
                    OffsetY="20"
                    PopDelay="50" />

            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>
</asp:Content>

