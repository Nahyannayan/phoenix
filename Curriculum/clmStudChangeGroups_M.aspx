<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmStudChangeGroups_M.aspx.vb" Inherits="Curriculum_clmStudChangeGroups_M" title="Untitled Page" Debug="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users  mr-3"></i> Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
cellspacing="0" width="100%">

    <tr valign="bottom">
    <td align="left" valign="bottom" style="width: 100%;">
    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
    SkinID="error"></asp:Label></td>
    </tr>

    <tr>
    <td class="matters" valign="top">
    <table align="center"  cellpadding="5" cellspacing="0"
    style="width: 100%" class="BlueTableView">
     
        <tr>
        <td align="left">
            <span class="field-label">Student ID</span></td>
       
        <td align="left">
            <asp:Label id="lblStuNo" runat="server" class="field-value"></asp:Label></td>
        <td align="left">
           <span class="field-label"> Name</span></td>
        
        <td align="left" colspan="4">
            <asp:Label id="lblName" runat="server" class="field-value"></asp:Label></td>
        </tr>
          <tr>
            <td align="left">
               <span class="field-label"> Grade</span></td>
            
            <td align="left">
                <asp:Label id="lblGrade" runat="server" class="field-value"></asp:Label></td>
            <td align="left">
                <span class="field-label">Section</span></td>
            
            <td align="left" colspan="4">
                <asp:Label id="lblSection" runat="server" class="field-value"></asp:Label></td>
        </tr>
        <tr>
           <td align="left" class="gridheader_pop" colspan="9"> Groups</td>
       </tr>
        <tr>
        <td align="center" colspan="9" style="height: 197px" >
        <asp:GridView id="gvGroup" runat="server" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2" CssClass="table table-bordered table-row">
        <RowStyle CssClass="griditem"   />
        <Columns>
                <asp:TemplateField HeaderText="sbgId" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblSbgId" runat="server" text='<%# Bind("SBG_ID") %>' ></asp:Label>
                  </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Capacity" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblSgrId" runat="server" text='<%# Bind("SGR_ID") %>'></asp:Label>
                   </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Subject"><ItemTemplate>
                        <asp:Label ID="lblSubject" runat="server" text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                         </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Group"><ItemTemplate>
                        <asp:Label ID="lblGroup" runat="server" text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                         </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Change to Group">
                    <ItemTemplate>
                        <asp:dropdownlist ID="ddlCGroup" width="180px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCGroup_SelectedIndexChanged"   ></asp:dropdownlist>
                         <asp:Label ID="lblTeacher" runat="server" ></asp:Label>
                         </ItemTemplate>
                </asp:TemplateField>
         </Columns>
        <SelectedRowStyle CssClass="Green" ></SelectedRowStyle>
        <HeaderStyle CssClass="gridheader_pop" Height="30px" ></HeaderStyle>
        <AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>
        </asp:GridView>
        </td>
        </tr>

     


<tr>
<td class="matters" align="center" valign="bottom" colspan="9">
&nbsp;&nbsp;
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
    &nbsp;
<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  CausesValidation="False" />&nbsp;
</td>
</tr>
</table>
<asp:HiddenField ID="hfSTU_ID" runat="server" />
<asp:HiddenField ID="hfACD_ID" runat="server" />
<asp:HiddenField ID="hfGRD_ID" runat="server" />
</td></tr></table>

    
            </div>
          </div>
        </div>
</asp:Content>

