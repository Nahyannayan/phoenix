<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentsMarks_KGS.aspx.vb" Inherits="Curriculum_rptStudentsMarks_KGS" Title="Untitled Page" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
   
    <script language="javascript" type="text/javascript">


    function confirm_delete()
    {
      if (confirm("You are about to delete this record.Do you want to proceed?")==true)
        return true;
      else
        return false;
     }//GRADE
        
     // To Get The Comments Only
     
     function getcomments(ctlid,code,header,stuid,rsmid)
       {    
//      alert(ctlid); 
        var sFeatures;
        sFeatures="dialogWidth: 530px; ";
        sFeatures+="dialogHeight: 450px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var result;
        var parentid;
        var varray= new Array();
        var str;
        if(code=='ALLCMTS')
        {
            document.getElementById('<%=H_CTL_ID.ClientID%>').value = ctlid
            var oWnd = radopen(url, "pop_comment");
          //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code +"","", sFeatures);
          //result = window.showModalDialog("clmCommentsList.aspx?ID=" + code +"&HEADER=" + header +"&STUID=" + stuid + "&RSMID="+rsmid,"", sFeatures);          
          //  if(result != '' && result != undefined)
          //  {
          //      str=document.getElementById(ctlid).value;
          //      document.getElementById(ctlid).value =str + result.replace(/_#_/g,"'");
          //      //result.split('___')[0]
          //      //document.getElementById(ctlid.split(';')[1]).value=varray[0];
          //  }
          //  else
          //  {
          //      return false;
          //  }
        }
               
       }
       function OnClientClose2(oWnd, args) {
            
           var NameandCode;
            var arg = args.get_argument();
            var ctlid = document.getElementById('<%=H_CTL_ID.ClientID%>').value;
            if (arg) {
                NameandCode = arg.Comment.split('||');              
                str = document.getElementById(ctlid).value;
                document.getElementById(ctlid).value = str + NameandCode[0].replace("/ap/", "'");
            }
        }
        //-------------------------
       
     
    function GetPopUp(id)
       {     
            //alert(id);
            var sFeatures;
            sFeatures="dialogWidth: 500px; ";
            sFeatures+="dialogHeight: 500px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           
          if(id==3)//Group
           {
                var GradeGrpId
                var GradeId
                GradeId=document.getElementById('<%=ddlGrade.ClientID %>').value;
                GradeGrpId = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
              var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&GRDID=" + GradeId + "&SBM_IDs=" + GradeGrpId + "", "pop_student");
                <%-- result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&GRDID="+GradeId+"&SBM_IDs="+GradeGrpId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }--%>
            }
            
          else if(id==4)//Subjejct
           {
                var GrdDetId
                GrdDetId=document.getElementById('<%=ddlGrade.ClientID %>').value;
                var SBGID
                SBGID = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID=" + GrdDetId + "&SBGID=" + SBGID + "", "pop_student");
                <%--result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID="+GrdDetId+"&SBGID="+SBGID+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SBJ_GRD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }--%>
            }
          else if(id==5)//Subject Grades
           {
                var GradeId
                var AcdId
                AcdId=document.getElementById('<%=ddlAcdID.ClientID %>').value;
                GradeId=document.getElementById('<%=ddlGrade.ClientID %>').value;
                 //alert(GradeId + 'ddd');
                var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=SBJACDGRD&GRDID=" + GradeId + "&ACDID=" + AcdId + "", "pop_student");
                <%--result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJACDGRD&GRDID="+GradeId+"&ACDID="+AcdId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;
                    
                    
                }
                else
                {
                    return false;
                }--%>
            }
          else if(id==8)//Student Info
           {
                var GradeId
                GradeId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "pop_student");
                <%--result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs="+GradeId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }--%>
            }   
           else if(id==12)//Student Info
           {
               
                var GradeId;
                var RepSch;
                var RepHeader;
                GradeId=document.getElementById('<%=ddlGrade.ClientID %>').value;
                acdId=document.getElementById('<%=ddlAcdID.ClientID %>').value;
                sbgId=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                sgr_Descr=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                RepSch=document.getElementById('<%=ddlRptSchedule.ClientID %>').value;
                RepHeader=''
                
                var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=STUDENT_MARKS&GETSTU_NO=true&SGR_IDs=" + GradeId + "&ACD_IDs=" + acdId + "&SBG_IDs=" + sbgId + "&SGR_DESCR=" + sgr_Descr + "&REP_SCH=" + RepSch + "&REP_HEADER=" + RepHeader + "", "pop_student");
                <%--result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_MARKS&GETSTU_NO=true&SGR_IDs="+GradeId+"&ACD_IDs="+acdId+"&SBG_IDs="+sbgId+"&SGR_DESCR="+sgr_Descr+"&REP_SCH="+RepSch+"&REP_HEADER="+RepHeader+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    //alert(document.getElementById('<%=lblStudNo.ClientID %>').value);
                    document.getElementById('<%=H_STU_ID.ClientID %>').value =result.split('___')[0];
                    document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
                    document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
                }
                else
                {
                    return false;
                }--%>
            }
        }//CMTSCAT
        
       function OnClientClose3(oWnd, args) {
          
            var arg = args.get_argument();

            if (arg) {
                alert(NameandCode[0]);
                NameandCode = arg.NameCode.split('||');
                alert(NameandCode[0]);
                document.getElementById('<%=H_STU_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = NameandCode[2];
                __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
            }
       }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

  function GetStudentValues()
        {
          //alert(1);
          var ColsId;
         var cellcnt=0;
         ColsId=document.getElementById('<%=H_CommentCOLS.ClientID %>').value;
         //alert(ColsId);
         var araay = new Array();
         // alert(txt1.length);
         for(i=0;i<txt1.length;i++)
         {
         araay[i]=new Array(ColsId) 
           //alert(ColsId);
          for(j=0;j<ColsId;j++)
           {
            var ArrName = "txt" + (Number(j) + 1);
            //alert(ArrName);
            ArrName = eval(ArrName);
              //alert(ArrName);
            try
            {
                var strtext;
                
            strtext=document.getElementById(ArrName[i]).value.replace(/,/gi,'~');
            strtext = replaceAll(strtext, ',', '~');
           // alert(araay[i][j]);
            araay[i][j]=strtext.replace(/,/gi,'~');
           // Alert(strtext);
            // document.getElementById(ArrName[i]).value.replace(',','%')
            }
           catch(err)
              {
              araay[i][j].toString().replace(/,/gi,'~');
              //alert(araay[i][j]);
              //Handle errors here
              }
            //alert(araay[i][j]);
            cellcnt=cellcnt+1;
           }
        }
        //alert(araay.length);
        var strvalue='';
        //Sending Array values into ServerSide
         for(var i=0; i<araay.length; i++) 
         {   
          strvalue =strvalue + araay[i] + '|'; 
         } 
      // alert(strvalue);   
        document.getElementById('<%=H_CommentCOLS.ClientID %>').value=strvalue;
    }
    
    function replaceAll(stringValue, replaceValue, newValue)
    {
      var functionReturn = new String(stringValue);
         while ( true )
         {
          var currentValue = functionReturn;
          functionReturn = functionReturn.replace(replaceValue, newValue);
          if ( functionReturn == currentValue )
           break;
         }
     return functionReturn;
    }
        
    
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>  
         <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>        
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:Label ID="lblHeader" runat="server" Text="Assesment Grade Entry"></asp:Label>
        </div>
       <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="top">
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error">
                            </asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td align="left" width="20%" ><span class="field-label">Academic Year</span> </td>

                        <td align="left" width="30%"  >
                            <asp:DropDownList ID="ddlAcdID" runat="server"  OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" ><span class="field-label">Report</span> </td>
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlReportId" runat="server"  OnSelectedIndexChanged="ddlReportId_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span> </td>
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  width="20%"><span class="field-label">Subject</span> 
                        </td>
                        <td align="left"width="30%" >
                            <%--<asp:TextBox ID="txtGrdSubject" runat="server" Enabled="False" Width="164px"></asp:TextBox>
                <asp:ImageButton ID="imgSbjGrade" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(5)"
                    OnClick="imgSbjGrade_Click"></asp:ImageButton>--%>
                            <asp:DropDownList ID="ddlSubjects" runat="server"  AutoPostBack="True" Visible="true">
                            </asp:DropDownList></td>

                    </tr>
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Subject Group</span> </td>

                        <td align="left"  width="30%">
                            <%--<asp:TextBox ID="txtGroup" runat="server" Width="168px" Enabled="False"></asp:TextBox>
                <asp:ImageButton ID="imgGroup" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(3)"
                    OnClick="imgGroup_Click"></asp:ImageButton>--%>
                            <asp:DropDownList ID="ddlSubjectGroup" runat="server"  AutoPostBack="True" Visible="true">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"   id="tdCat1" runat="Server"><span class="field-label">Subject Category</span> </td>

                        <td align="left" width="30%"  id="tdCat3" runat="Server">
                            
                                        <asp:DropDownList ID="ddlCategory" runat="server">
                                        </asp:DropDownList> <br />
                                        <asp:CheckBox ID="chkReportColumns" runat="server" Text="With Report Columns" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Report Schedule</span> </td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlRptSchedule" runat="server"  OnSelectedIndexChanged="ddlRptSchedule_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnView" runat="server" CssClass="button"
                                Text="View Students"  />
                            <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save & Next" ValidationGroup="MAINERROR"
                                OnClientClick="GetStudentValues();" OnClick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnCanceldata" runat="server" CssClass="button" Text="Cancel" OnClick="btnCanceldata_Click"
                    />
                            <asp:DropDownList ID="ddlLevel" runat="server" Visible="False">
                                <asp:ListItem>Core</asp:ListItem>
                                <asp:ListItem>Extended</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr  valign="top" >
                        <td align="left" colspan="4"  >
                            <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Search Students</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Panel ID="Panel1" runat="server">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="field-label"> Student Name :  </span> 
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(12); return false;"></asp:ImageButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStudNo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>                                                                                                                                             
                            </asp:Panel>
                            <table>
                                <tr>
                                    <td>
                                        <telerik:RadSpell ID="RadSpell1" AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd"
                                            HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" ButtonType="LinkButton" IsClientID="true"
                                            runat="server" SupportedLanguages="en-US,English" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPages" runat="server" ></asp:Label> <br />
                                        <asp:DataList ID="dlPages" runat="server" RepeatColumns="100">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ForeColor="#1b80b6" Text='<%# Bind("pageno") %>' ID="lblPageNo"
                                                    OnClick="lblPageNo_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList></td>
                                   
                                    <td  align="left">
                                        <asp:LinkButton ID="lbtnKeys" runat="server" Visible="false" OnClientClick="javascript:return false;">Header Keys</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCommonData" runat="server"  Font-Bold="True" Font-Size="Large"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                </table>
                 </div>
        </div>
    </div>
    <div class="card mb-3">
        
       <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="0" cellspacing="0"  Width="100%"  >                  
                   
                    <tr style="display: none">
                        <td align="left" colspan="4" >&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" >
                            <asp:GridView ID="gvStudents" runat="server" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" EnableTheming="True" OnPageIndexChanging="gvStudents_PageIndexChanging"
                                HorizontalAlign="Center" Width="100%">
                                <HeaderStyle   />
                                <RowStyle HorizontalAlign="Center" />
                                <AlternatingRowStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSaveDetails" runat="server" CssClass="button" Text="Save " ValidationGroup="MAINERROR"
                                OnClientClick="GetStudentValues();" OnClick="btnSaveDetails_Click" />
                            &nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save & Next" ValidationGroup="MAINERROR"
                    OnClientClick="GetStudentValues();" OnClick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnSubCancel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="H_GRP_ID" runat="server"></asp:HiddenField>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    TargetControlID="Panel1" CollapsedSize="0" ExpandedSize="70" Collapsed="true" 
                    ExpandControlID="LnkSearch" CollapseControlID="LnkSearch" AutoCollapse="False"
                    AutoExpand="False" ScrollContents="false" TextLabelID="LnkSearch" CollapsedText="Search Students"
                    ExpandedText="Hide Search">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_STU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SBJ_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SETUP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CommentCOLS" runat="server"></asp:HiddenField>
                 <asp:HiddenField ID="H_CTL_ID" runat="server"></asp:HiddenField>
                <div id="popup" runat="server" class="subHeader_E" style="display: none; text-align: left; vertical-align: middle;  margin-top: -2px; margin-left: -3px; margin-right: 1px; border-style: solid;  border-width: 1.5px; background-color:white;">
                    <div style="vertical-align: middle; padding-bottom: 2px;" class="title-bg-small">
                        Header Keys
                    </div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" Height="25%" Width="100%">
                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" TargetControlID="lbtnKeys"
                    PopupControlID="popup" HoverCssClass="popupHover" PopupPosition="Center" OffsetX="0"
                    OffsetY="20" PopDelay="50" />
            </div>
        </div>
    </div>
</asp:Content>
