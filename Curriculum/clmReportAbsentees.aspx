<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportAbsentees.aspx.vb" Inherits="Curriculum_clmReportAbsentees" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Exam Attendance
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr align="left">
                        <td>&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>




                    <tr align="left">
                        <td>
                            <table id="tbStud" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--   <tr class="subheader_img">
                        <td align="center" colspan="3"  style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Attendance </font></div>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Report Card</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Report Schedule</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Header</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlHeader" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Section</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Subject</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnList" runat="server" CssClass="button" Text="List" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Records Found" PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attendance">


                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlAttendance" runat="server" SelectedValue='<%# Bind("ATT") %>'>
                                                            <asp:ListItem Value="P">Present</asp:ListItem>
                                                            <asp:ListItem Value="A">Absent</asp:ListItem>
                                                            <asp:ListItem Value="L">Approved Leave</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:HiddenField ID="hfHeaderCount" runat="server"></asp:HiddenField>

                            <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" />

                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>


</asp:Content>

