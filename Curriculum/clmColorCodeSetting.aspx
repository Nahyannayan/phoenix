<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="clmColorCodeSetting.aspx.vb" Inherits="Skills_Setting_M" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Color Code Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Please enter the field Max Marks" ControlToValidate="txtMaxMark" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />

                        </td>

                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr valign="bottom">
                                    <td align="center" valign="middle" colspan="4"><span class="text-danger">Fields Marked with(*)are mandatory</span></td>
                                </tr>

                                <%--<tr class="subheader_img">
                        <td align="left" colspan="12" style="height: 16px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">Color Code Set Up&nbsp;</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Grades</span></td>
                                    <td align="left" width="30%">
                                        <telerik:RadComboBox RenderMode="Lightweight" ID="lstGrade" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                            OnSelectedIndexChanged="lstGrade_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Select Exam </span></td>

                                    <td align="left">
                                        <telerik:RadComboBox RenderMode="Classic" ID="ddlExam" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlExam_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="left"><span class="field-label">Enter Max Marks</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtMaxMark" runat="server" AutoPostBack="True">
                                        </asp:TextBox>
                                    </td>
                                </tr>


                                <tr runat="server" id="trNewRow1" visible="false">
                                    <td align="left"><span class="field-label">Enter From Marks</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtNewMarkFrom" runat="server"></asp:TextBox></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr runat="server" id="trNewRow2" visible="false">
                                    <td align="left"><span class="field-label">Enter To Marks</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtNewMarkTo" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr runat="server" id="trNewRow3" visible="false">
                                    <td align="left"><span class="field-label">Choose Color</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <telerik:RadColorPicker RenderMode="Lightweight" runat="server" ID="RadColorPicker1"   PaletteModes="HSV" CssClass="ColorPickerPreview" Style="width:100% !important;" >
                                        </telerik:RadColorPicker>
                                        <%--    <asp:TextBox ID="txtNewColorCode" runat="server" Width="95px"></asp:TextBox>
                          <cc1:ColorPickerExtender ID="txtBGColor_ColorPickerExtender" runat="server" Enabled="True" TargetControlID="txtNewColorCode"></cc1:ColorPickerExtender>--%>

                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnNewRow" runat="server" Text="Add New Range" CssClass="button" OnClick="btnNewRow_Click" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                                <%--  <tr>
                        <td align="center"  class="matters" colspan="12">
                            
                                <asp:Button ID="btnAdd" runat="server" CssClass="button" Height="24px" Text="Save" ValidationGroup="groupM1" Width="138px" />
                        </td>
                    </tr>
                    <br />--%>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:GridView ID="gvcolor" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" BorderStyle="None" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Exam" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExam" runat="server" Text='<%# Bind("RPF_DESCR")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("CCM_GRD_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mark From" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMarkFrom" runat="server" Text='<%# Bind("CCD_VALUE_FROM")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mark To" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMarkTo" runat="server" Text='<%# Bind("CCD_VALUE_TO")%>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Color" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblColor" runat="server" Text="__________" BackColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("CCD_COLOR"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <EmptyDataRowStyle />
                                            <EditRowStyle />

                                        </asp:GridView>
                                    </td>
                                </tr>


                            </table>

                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

</asp:Content>

