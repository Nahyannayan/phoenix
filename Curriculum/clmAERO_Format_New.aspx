﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmAERO_Format_New.aspx.vb" Inherits="Curriculum_clmAERO_Format" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">
        function GetSubjects() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var TermId;
            var SbgId;

            TermId = document.getElementById('<%=ddlTerm.ClientID %>').value;
            SbgId = document.getElementById('<%=ddlSubject.ClientID %>').value;


            var oWnd = radopen("showTopics_Term.aspx?TermId=" + TermId + "&SbgId=" + SbgId, "pop_topic");
           <%-- result = window.showModalDialog("showTopics_Term.aspx?TermId=" + TermId + "&SbgId=" + SbgId, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
            }
            return false;--%>
        }
       
         function OnClientClose3(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.Topic.split('||');            
                 document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
         }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

         function validate() {
            

            if (document.getElementById('<%=chkCore.ClientID %>').checked == 1) {

                document.getElementById('<%=txtCore.ClientID%>').disabled = false;

            } else {
            document.getElementById('<%=txtCore.ClientID%>').disabled = true;
            document.getElementById('<%=txtCore.ClientID%>').value = '';
            }
        }

        function validate1() {

            
            if (document.getElementById('<%=chkBasic.ClientID %>').checked == 1) {

                document.getElementById('<%=txtBasic.ClientID%>').disabled = false;

            } else {
            document.getElementById('<%=txtBasic.ClientID%>').disabled = true;
            document.getElementById('<%=txtBasic.ClientID%>').value = '';
            }
        }
        function validate2() {
           

            if (document.getElementById('<%=chkExtended.ClientID %>').checked == 1) {

                document.getElementById('<%=txtExtended.ClientID%>').disabled = false;

            } else {
            document.getElementById('<%=txtExtended.ClientID%>').disabled = true;
            document.getElementById('<%=txtExtended.ClientID%>').value = '';
            }
        }

    </script>
 <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
       <div class="card-body">
            <div class="table-responsive m-auto">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr id="Tr1" runat="server">
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table align="center"  width="100%">
                                    
                    <tr>
                        <td align="left"  width="20%" ><span class="field-label">Academic Year</span> 
                        </td>
                       
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlAcyear" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" ><span class="field-label">Term</span> 
                        </td>
                        
                        <td align="left" width="30%"  >
                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  > <span class="field-label">Grade </span> 
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                        <td align="left" > <span class="field-label">Subject</span> 
                        </td>                       
                        <td align="left"  >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trhide" runat="server">
                                    <td align="left"><span class="field-label">Age Band </span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAgeband" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgeband_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label"></span>
                                    </td>
                                    <td align="left">
                                        
                                    </td>
                                </tr>
                    <tr>
                        <td align="left" > <span class="field-label">Topic</span> 
                        </td>
                       
                        <td align="left"  colspan="3">
                            <asp:TextBox ID="txtTopic" runat="server"  Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSubjects(); return false;"></asp:ImageButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg-light">
                            <asp:Label ID="Label1"  class="field-label" runat="server" Text="Details.." ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  ><span class="field-label">Criteria Description</span> 
                            <asp:Label ID="Label4"  runat="server" ForeColor="Red" Text="*"></asp:Label>
                            </td>                        
                       
                        <td align="left" colspan="3"  >                             
                            <asp:TextBox ID="txtCrDesc" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCrDesc"
                                Display="Dynamic" ErrorMessage="Criteria Description" ForeColor="White" 
                                ValidationGroup="SUBERROR" ></asp:RequiredFieldValidator>
                        </td>
                        </tr>
                    <tr>
                        <td id="Td1" colspan="4" runat="server" valign="top">
                            <div style="height: 100%;  overflow: auto">
                                <asp:GridView ID="gvDets" runat="server" AutoGenerateColumns="False" 
                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." CssClass="table table-striped table-bordered"
                                    >
                                    <RowStyle CssClass="griditem"  />

                                    <Columns>
                                        <asp:TemplateField HeaderText="objid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("SYC_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Topic">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTopic" runat="server" Text='<%# Bind("SYC_TOPIC")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("SYC_DESCR") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSDate" runat="server" Text='<%# Bind("SYC_STARTDT","{0:dd/MMM/yyyy}") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbledate" runat="server" Text='<%# Bind("SYC_ENDDT","{0:dd/MMM/yyyy}") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="edit" Text="Edit" HeaderText="Edit">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                        </asp:ButtonField>
                                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete"
                                                    Text="Delete"></asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="lnkDelete" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                    runat="server">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle />
                                    <HeaderStyle   />
                                    <AlternatingRowStyle  />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Start Date</span> 
                        </td>
                      
                        <td align="left" >&nbsp;<asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox><asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="SUBERROR"
                                Text="The From Date is required!"
                                runat="server" />
                        </td>
                        <td align="left" ><span class="field-label">End Date</span> 
                        </td>                       
                        <td align="left" >
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox><asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="SUBERROR"
                                Text="The To Date is required!"
                                runat="server" /><asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtFromDate"
                                    ControlToValidate="txtToDate" Display="Dynamic" ErrorMessage="End date is less than start date"
                                    Operator="GreaterThanEqual" ValidationGroup="SUBERROR">
                                </asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Number of Lessons</span> 
                        </td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtNoLessons" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                ControlToValidate="txtNoLessons" Display="Dynamic" ValidationGroup="SUBERROR"
                                Text="The No Of Lessons field is required!"
                                runat="server" />
                        </td>
                        <td align="left"  colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkRubric" runat="server" Text="Link to rubric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkEval" runat="server" Text="Link to Topic skills evaluation" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"> 
                            <asp:CheckBox ID="chkBasic" runat="server" Text="Basic" onClick="validate1();" />
                        </td>
                        <td align="left">
                            
                       <span class="field-label" colspan="3">Planned activities</span> 
                        
                            <asp:TextBox ID="txtBasic" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine"  Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>  <asp:CheckBox ID="chkCore" runat="server" Text="Core" onClick="validate();" />

                        </td>
                        <td align="left">
                          
                       <span class="field-label" colspan="3"> Planned activities</span> 
                        
                            <asp:TextBox ID="txtCore" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine"  Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <asp:CheckBox ID="chkExtended" runat="server" Text="Extended" onClick="validate2();" />
                        </td>
                        <td align="left" >
                           
                       <span class="field-label" colspan="3">Planned activities</span> 
                       
                            <asp:TextBox ID="txtExtended" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine"  Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Co-curricular Links</span> 
                        </td>
                       
                        <td align="left"  >
                            <asp:TextBox ID="txtLinks" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Flipped classroom resource </span> 
                        </td>
                      
                        <td align="left"  colspan="3">
                            <asp:TextBox ID="txtResource" runat="server"  SkinID="MultiText_Medium"
                                TextMode="MultiLine" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_TopicID" runat="server" />
                <asp:HiddenField ID="h_SYC_ID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
