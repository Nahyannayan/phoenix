﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmCurriculumcoordinator
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")

            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


          

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100300") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    GridBind()
                    gvOption.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub btnempid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_query As String = "SELECT A.GUID AS GUID,EMP_ID,EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),USR_NAME,CUR_EMP_ID " _
          & " FROM EMPLOYEE_M AS A  INNER JOIN users_m as c on  a.emp_id=c.usr_emp_id " _
          & "   left outer join  EMPLOYEE_CURRICULUMCORDINATORS as d on a.EMP_ID=d.CUR_EMP_ID  WHERE " _
          & " EMP_ECT_ID IN(1,3,4) AND EMP_BSU_ID='" + Session("sbsuid") + "' AND EMP_STATUS IN(1,2)"


        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim txtSearch As New TextBox
        Dim optSearch As String
        Dim txtSearch1 As New TextBox
        Dim optSearch1 As String
        Dim strFilter1 As String = ""

        If gvOption.Rows.Count > 0 Then

            txtSearch = gvOption.HeaderRow.FindControl("txtOption")
            'strSidsearch = h_Selected_menu_1.Value.Split("__")
            'strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter = " and ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')  like '%" & txtSearch.Text & "%' "
                optSearch = txtSearch.Text



                If strFilter.Trim <> "" Then
                    str_query += " " + strFilter
                End If
            End If
            txtSearch1 = gvOption.HeaderRow.FindControl("txtOption1")
            'strSidsearch = h_Selected_menu_1.Value.Split("__")
            'strSearch = strSidsearch(0)
            If txtSearch1.Text <> "" Then
                strFilter1 = " and USR_NAME  like '" & txtSearch1.Text & "%' "
                optSearch1 = txtSearch1.Text

                If strFilter1.Trim <> "" Then
                    str_query += " " + strFilter1
                End If
            End If

        End If

        '  str_query += " order by opt_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvOption.DataBind()
            Dim columnCount As Integer = gvOption.Rows(0).Cells.Count
            gvOption.Rows(0).Cells.Clear()
            gvOption.Rows(0).Cells.Add(New TableCell)
            gvOption.Rows(0).Cells(0).ColumnSpan = columnCount
            gvOption.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvOption.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvOption.DataBind()
        End If

        txtSearch = New TextBox
        txtSearch = gvOption.HeaderRow.FindControl("txtOption")
        txtSearch.Text = optSearch

        txtSearch1 = New TextBox
        txtSearch1 = gvOption.HeaderRow.FindControl("txtOption1")
        txtSearch1.Text = optSearch1

    End Sub
    Protected Sub btnuid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvOption_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOption.PageIndexChanging
        Try
            gvOption.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvOption_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOption.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim chkisdefault As CheckBox = DirectCast(e.Row.FindControl("cb1"), CheckBox)

            If chkisdefault.Text = "" Then
                chkisdefault.Checked = False
            Else
                chkisdefault.Checked = True
                chkisdefault.Text = ""
            End If
        End If

    End Sub


    Protected Sub gvOption_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOption.RowCommand

        Try
            '    Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvOption.Rows(index), GridViewRow)

            If e.CommandName = "Save" Then
                Dim lblid As New Label
                Dim chkb As CheckBox
                Dim t As Integer
                lblid = selectedRow.FindControl("Label1")
                chkb = selectedRow.FindControl("cb1")

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_query As String
                If chkb.Checked Then
                    t = 0
                Else
                    t = 1
                End If
             
                str_query = "exec dbo.insertCURRICULUMCORDINATORS '" & lblid.Text & "' , " & Val(t)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)




            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


End Class
