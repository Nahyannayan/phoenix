<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmDefaultBank_M.aspx.vb" Inherits="Curriculum_clmDefaultBank_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  Default Values Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="5">

                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td colspan="5" align="center" >
                            <table id="tbReport" runat="server" align="center" border="0" cellpadding="0" cellspacing="0"  width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"> <span class="field-label">Default Bank Name</span></td>
                                
                                    <td align="left" >
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox></td>

                                     <td align="left" > <span class="field-label">Add Default Values</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                                        </td>

                                       <td align="left" >
                                      
                                        <asp:Button ID="btnAddNew" runat="server" CssClass="button" TabIndex="7" Text="Add"
                                            ValidationGroup="groupM1" /></td>
                                </tr>
                               
                              
                                <tr>
                                    <td align="center" colspan="5">

                                        <asp:GridView ID="gvDefault" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle   CssClass="griditem" />
                                            <EmptyDataRowStyle />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Value">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDefault" runat="server" Text='<%# Bind("RDD_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Display Order">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder" runat="server" Text='<%# Bind("RDD_ORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:ButtonField CommandName="edit" Text="Delete" HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>

                                            <SelectedRowStyle  />
                                            <HeaderStyle />
                                            <EditRowStyle  />
                                            <AlternatingRowStyle  CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" >
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="7" Text="Save"
                                            ValidationGroup="groupM1" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False" /></td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfRDM_ID" runat="server"></asp:HiddenField>
                &nbsp;
    <asp:HiddenField ID="hfAllSubjects" runat="server"></asp:HiddenField>
            </td>
        </tr>

    </table>

    
                </div>
        </div>
    </div>

</asp:Content>

