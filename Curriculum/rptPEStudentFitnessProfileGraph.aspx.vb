Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class rptPEStudentFitnessProfileGraph
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC70020") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindActivitySchedule()


                    BindGrade()
                    BindSection()


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub

    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then

            str_query = "SELECT DISTINCT GRM_DISPLAY ,GRM_GRD_ID AS GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                          & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                          & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                          & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                          & " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT GRM_DISPLAY ,GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                         & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                         & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                         & " INNER JOIN PE.AUTHORIZED_STAFF ON GRD_ID=AS_GRD_ID" _
                                         & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                         & " AS_EMP_ID=" + Session("EmployeeId") + " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                                         & " ORDER BY GRD_DISPLAYORDER"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

    End Sub

    Sub BindSection()
         Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M " _
                     & " WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                     & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M INNER JOIN " _
                         & " INNER JOIN PE.AUTHORIZED_STAFF ON SCT_ID=AS_SCT_ID WHERE " _
                         & " SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                         & " SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                         & " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                         & " ORDER BY SCT_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub


    Function GetSelectedStudents(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
         " FROM  vw_STUDENT_DETAILS "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_sql)
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hfbDownload.Value = 0
        CallReports()
    End Sub

    Private Function GetAllCBSEBusinessUnit(ByVal BSU_ID As String) As String
        Dim strQuery As String = " SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_CLM_ID = 1 AND BSU_ID = '" & BSU_ID & "'"
        Dim vBSU_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Return vBSU_ID
    End Function

   
    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
           " AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,0,''),0) "
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
     " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
     " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
     "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
     " AND STP_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,0,''),0) "
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        Return str
    End Function




    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Sub LoadReports(ByVal rptClass)
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.ReportDocument.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If

                If ViewState("MainMnu_code") = "PdfReport" Then
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                    rs.ReportDocument.Close()
                    rs.Dispose()
                Else
                    Try
                        exportReport(rs.ReportDocument, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                    Catch ee As Exception
                    End Try
                    rs.ReportDocument.Close()
                    rs.Dispose()
                End If

            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            rs.ReportDocument.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                Dim hop As New CrystalDecisions.Shared.ExportOptions
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        '' HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.ContentType = "application/pdf"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        System.IO.File.Delete(tempFileNameUsed)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If Session("sbsuid") = "125017" Then
                            If Not Session("noimg") Is Nothing Then
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                            Else
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/NOIMG/no_img_gwa.jpg")
                            End If

                        ElseIf Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\125017\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Sub CallReports()
        If h_STU_IDs.Value = "" Then
            Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
            h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, str_sec)
        End If
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@ACS_ID", ddlSchedule.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@STUDENT_IDS", h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptPEStudentFitnessProfileGraph.rpt")
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function


    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReports()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            rs.Dispose()
        Catch ex As Exception

        End Try
    End Sub
    
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
        BindGrade()
        BindSection()
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

End Class
