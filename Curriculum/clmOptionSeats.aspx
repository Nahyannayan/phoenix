<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOptionSeats.aspx.vb" Inherits="Curriculum_clmOptionSeats" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Optional Subjects Seats"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0">
        
        <tr>
            <td align="left"  style="font-weight: bold; " valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center" 
                   cellpadding="7" cellspacing="0" style="width: 100%">
                    <tr id="trAcd1" runat="server">
                        <td align="left" >
                            <span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlAcademicYear"  runat="server" AutoPostBack="True"
                                 Width="108px">
                            </asp:DropDownList>
                        </td>
                       
                    <td align="left" >
                           <span class="field-label"> Grade</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True"
                                 Width="100px">
                            </asp:DropDownList>
                        </td>
                                  
                         </tr>
                       <tr id="Tr1" runat="server">
                                    <td id="Td1" align="center"  runat="server" colspan="6">
                                        <asp:GridView ID="grdStud" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            Width="100%" >
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="atd" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AtdId" runat="server" Text='<%# bind("SBG_SBM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subjects">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblno" runat="server" Width="150px" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Width="150px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Seats">
                                                    <ItemTemplate>
                                                        <asp:Textbox ID="txtSeats" runat="server" Width="40px" Text='<%# Bind("SBG_TOTSEATS") %>'></asp:Textbox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Width="40px"></ItemStyle>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Fees">
                                                    <ItemTemplate>
                                                        <asp:Textbox ID="txtfee" runat="server" Width="40px" Text='<%# Bind("SBG_FEES") %>'></asp:Textbox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Width="40px"></ItemStyle>
                                                </asp:TemplateField>
                                                  
                                                                                       </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1" TabIndex="7" Width="115px" />
                                    </td>
                                </tr>
                                
                                                       
                </table>
            
            </td>
        </tr>
    </table>
                 </div>
        </div>
    </div>
</asp:Content>

