
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Partial Class Curriculum_clmActivity_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    chkbHasPeriod.Attributes.Add("onclick", "change_chk_state('" & chkbHasPeriod.ClientID & "', '" & chkHasTime_Duration.ClientID & "');")
                    chkHasTime_Duration.Attributes.Add("onclick", "change_chk_state('" & chkHasTime_Duration.ClientID & "', '" & chkbHasPeriod.ClientID & "');")
                    If ViewState("datamode") = "add" Then
                        txtCLM_DESC.Text = ""
                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        bindControl()
                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Sub bindControl()
        Using readerACTIVITY_M As SqlDataReader = ACTIVITYMASTER.GetACTIVITY_M(Session("sBSUID"), ViewState("viewid"))
            While readerACTIVITY_M.Read

                txtCLM_DESC.Text = Convert.ToString(readerACTIVITY_M("CAM_DESC"))
                chkbHasPeriod.Checked = readerACTIVITY_M("CAM_bHASPERIOD")
                chkHasTime_Duration.Checked = readerACTIVITY_M("CAM_bHASTIME_DURATION")

                txtCLM_DESC.Enabled = False
                chkbHasPeriod.Enabled = False
                chkHasTime_Duration.Enabled = False
            End While
        End Using
    End Sub



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then

            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                txtCLM_DESC.Enabled = False
                lblError.Text = "Record Saved Successfully"
            Else
                lblError.Text = errorMessage
            End If
        End If

    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim CAM_ID As String = ""
        Dim CLM_DESC As String = txtCLM_DESC.Text


        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer

                    bEdit = False


                    status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, Session("sBSUID"), chkbHasPeriod.Checked, chkHasTime_Duration.Checked, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If
                    '  End If
                    ' Next
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        transaction.Commit()
                        errorMessage = ""

                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer


                    CAM_ID = ViewState("viewid")
                    bEdit = True


                    status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, Session("sBSUID"), chkbHasPeriod.Checked, chkHasTime_Duration.Checked, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If


                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else

                        transaction.Commit()
                        errorMessage = ""
                    End If
                End Try

            End Using
        End If
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            txtCLM_DESC.Text = ""
            txtCLM_DESC.Enabled = True
            chkbHasPeriod.Checked = False
            chkHasTime_Duration.Checked = False
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("datamode") = "edit"

            txtCLM_DESC.Enabled = True
            chkbHasPeriod.Enabled = True
            chkHasTime_Duration.Enabled = True

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
             
                txtCLM_DESC.Text = ""
                txtCLM_DESC.Enabled = False
                chkbHasPeriod.Enabled = False
                chkHasTime_Duration.Enabled = False

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

End Class
