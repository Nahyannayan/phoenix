<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportSubjects_Add.aspx.vb" Inherits="Curriculum_clmReportSubjects_Add" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
  
	
</script>
   
<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
                REPORT SUBJECTS</td>
        </tr>
    </table>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr style="font-size: 8pt; color: #800000" valign="bottom">
            <td align="center" class="matters"  valign="middle">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="matters"  valign="bottom" >
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    class="matters"  width="65%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal id="ltLabel" runat="server" Text="Report Subjects"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px; width: 64px;">
                            Academic Year</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged" >
                            </asp:DropDownList></td>
                        <td align="left" style="height: 21px">
                            Report</td>
                        <td align="center" style="width: 1px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px"><asp:DropDownList id="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged" >
                        </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 64px; height: 21px">
                            Grade</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px">
                            <asp:DropDownList id="ddlGrades" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrades_SelectedIndexChanged" >
                            </asp:DropDownList></td>
                        <td align="left" style="height: 21px">
                            Subject</td>
                        <td align="center" style="width: 1px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px">
                            <asp:DropDownList id="ddlSubj" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubj_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 64px; height: 21px">
                            Report Header</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" colspan="4" style="height: 21px"><asp:DropDownList id="ddlRepHeader" runat="server" OnSelectedIndexChanged="ddlRepHeader_SelectedIndexChanged" >
                        </asp:DropDownList></td>
                    </tr>
                    <tr >
                        <td align="left" style="height: 21px; width: 64px;">
                            Answer Type</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px" colspan="4">
                            <asp:TextBox id="txtDescription" runat="server" Height="157px" TextMode="MultiLine"
                                Width="360px" SkinID="MultiText_Large"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                                Display="Dynamic" ErrorMessage="Sub Activity required" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr runat="server" id="tr20">
                        <td align="right" colspan="6" style="height: 13px">
                            <asp:Button id="btnAddDetail" runat="server" CssClass="button" Text="Add Detail" OnClick="btnAddDetail_Click" ValidationGroup="AttGroup" />
                            <asp:Button id="btnUpdate" runat="server" CssClass="button" Text="Update Detail" OnClick="btnUpdate_Click" ValidationGroup="AttGroup" />
                            <asp:Button id="btnUpdateCancel" runat="server" CssClass="button" OnClick="btnUpdateCancel_Click"
                                Text="Cancel" /></td>
                    </tr>
                   <tr class="gridheader_pop"  runat="server" id="tr21" >
                       <td align="left" class="matters" colspan="9">
                           Add Detail</td>
                    </tr>
                    <tr   runat="server" id="tr22">
                        <td align="center" colspan="6" style="height: 21px">
                <asp:GridView id="gvSUBJ_Detail" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                    Width="100%" OnRowEditing="gvSUBJ_Detail_RowEditing" OnRowDeleting="gvSUBJ_Detail_RowDeleting" DataKeyNames="ID" OnPageIndexChanging="gvSUBJ_Detail_PageIndexChanging">
                    <rowstyle cssclass="griditem" height="25px" />
                    <emptydatarowstyle cssclass="matters" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <columns>
<asp:TemplateField HeaderText="ID"><ItemTemplate>
<asp:Label id="lblID" runat="server" Text='<%# Bind("ID") %>' __designer:wfdid="w19"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="RSBID"><ItemTemplate>
<asp:Label id="lblRSB_ID" runat="server" Text='<%# Bind("RSB_ID") %>' __designer:wfdid="w12"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="RSMID"><ItemTemplate>
<asp:Label id="lblRSB_RSM_ID" runat="server" Text='<%# Bind("RSB_RSM_ID") %>' __designer:wfdid="w2"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="GRDID"><ItemTemplate>
<asp:Label id="lblRSB_GRD_ID" runat="server" Text='<%# Bind("RSB_GRD_ID") %>' __designer:wfdid="w6"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="SBGID"><ItemTemplate>
<asp:Label id="lblRSB_SBG_ID" runat="server" Text='<%# Bind("RSB_SBG_ID") %>' __designer:wfdid="w7"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="RSDID"><ItemTemplate>
<asp:Label id="lblRSB_RSD_ID" runat="server" Text='<%# Bind("RSB_RSD_ID") %>' __designer:wfdid="w8"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="RSB_DESCR"><ItemTemplate>
<asp:Label id="lblRSB_DESCR" runat="server" Text='<%# Bind("RSB_DESCR") %>' __designer:wfdid="w11"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <selectedrowstyle cssclass="griditem_hilight" />
                    <headerstyle cssclass="gridheader_new" height="25px" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView></td>
                    </tr>
                  
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 9px" valign="bottom">
                <br />
                &nbsp;</td>
        </tr>
        <tr>
            <td class="matters" style="height: 10px" valign="bottom">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" Width="50px" /><asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnEdit_Click" Text="Edit" Width="50px" /><asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" Width="50px" /><asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" Width="50px" /></td>
        </tr>
    </table>
    <input id="h_Row" type="hidden" runat="server" /><br />
    <asp:HiddenField id="h_ReportId" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_AcdId" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_GradeId" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SubjId" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_RepHeaderid" runat="server">
    </asp:HiddenField>
</asp:Content>

