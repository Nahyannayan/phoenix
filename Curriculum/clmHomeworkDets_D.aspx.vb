﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmHomeworkDets_D
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            H_ACD_ID.Value = Request.QueryString("Acdid")
            H_GRD_ID.Value = Request.QueryString("Grdid")

            Try

                BindGrade()

                ddlGrade.SelectedValue = H_GRD_ID.Value
                BindSection()
                BindSubject()

                txtDate.Text = String.Format("{0:MMM/yyyy}", Date.Today)
                showdets()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Sub BindGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        'ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAcademicYear.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        If bSuperUsr Then
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER  " & _
            "  FROM VW_GRADE_BSU_M INNER JOIN oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID  " & _
             " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & H_ACD_ID.Value & "') " & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER" & _
            "  FROM VW_GRADE_BSU_M inner join oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " & _
            " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & H_ACD_ID.Value & "') " & _
            " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EmployeeID") & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrade.DataSource = ds


        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grd_id"
        ddlGrade.DataBind()

    End Sub
    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + H_ACD_ID.Value


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        If (Not ddlSubject.Items Is Nothing) AndAlso (ddlSubject.Items.Count > 1) Then
            ddlSubject.Items.Add(New ListItem("ALL", "0"))
            ddlSubject.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindSection()


        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + H_ACD_ID.Value


        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"


        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
        BindSubject()
    End Sub
    Protected Sub calHomework_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calHomework.DayRender
        Dim SStyle As New Style()
        With SStyle
            .BackColor = System.Drawing.Color.FromArgb(31, 73, 125)
            .BorderColor = System.Drawing.Color.White
            .BorderWidth = New Unit(3)
            .ForeColor = System.Drawing.Color.White
            .Height = "30"
            .Font.Size = "7"
        End With

        If e.Day.IsOtherMonth Then
            e.Cell.Controls.Clear()
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String = ""
        Dim grade As String()
        Dim dt As New Date(Year(txtDate.Text), Month(txtDate.Text), 1)

        grade = ddlGrade.SelectedValue.Split("|")
        Dim ds As New DataSet
        str_Sql = "select distinct HW_ID ,HW_SDATE,HW_CDATE,SGR_DESCR,SCT_DESCR   from HOMEWORKDETS INNER JOIN dbo.GROUPS_M  ON HW_SGR_ID=SGR_ID " _
                    & " inner join STUDENT_GROUPS_S on HW_ACD_ID=SSD_ACD_ID and HW_GRD_ID=SSD_GRD_ID and HW_SBG_ID=SSD_SBG_ID and HW_SGR_ID=SSD_Sgr_ID " _
                    & " inner join oasis.dbo.SECTION_M on ssd_sct_id=SCT_ID " _
                    & " where  HW_ACD_ID =" + H_ACD_ID.Value + " and HW_GRD_ID='" + grade(0) + "' " _
                    & " and MONTH(HW_SDATE)=" & dt.Month & " and YEAR(HW_SDATE)=" & dt.Year

        If ddlsection.SelectedValue <> 0 Then
            str_Sql += "and SSD_SCT_ID =" + ddlsection.SelectedValue
        End If
        If ddlSubject.SelectedValue <> 0 Then
            str_Sql += "and HW_SBG_ID=" + ddlSubject.SelectedValue
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim i As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If e.Day.Date = ds.Tables(0).Rows(i).Item("HW_SDATE") Then
                    e.Cell.ApplyStyle(SStyle)
                    e.Cell.Style.Add("vertical-align", "top")
                    e.Cell.Style.Add("text-align", "left")

                    Dim aLabel As Label = New Label()
                    '  aLabel.Style.Add("text-align", "center")
                    aLabel.Text = "<br>&nbsp;&nbsp;&nbsp;" & ds.Tables(0).Rows(i).Item("SGR_DESCR")
                    aLabel.ToolTip = ds.Tables(0).Rows(i).Item("HW_CDATE")
                    aLabel.Style.Add("cursor", "default")
                    e.Cell.Controls.Add(aLabel)

                End If
            Next
        End If

      
    End Sub



    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        showdets()
    End Sub
    Sub showdets()
        calHomework.Visible = True

        calHomework.VisibleDate = New Date(Year(txtDate.Text), Month(txtDate.Text), 1)
    End Sub
End Class
