<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProcessFailedStudents.aspx.vb" Inherits="Curriculum_clmProcessFailedStudents" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click();//fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }





    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Process Failed Students
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>

                        <td align="left" class="title">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblTC" runat="server" width="100%" align="center" cellspacing="0">


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>

                                </tr>

                                <tr>

                                    <td align="left">
                                        <span class="field-label">Select Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left">
                                        <span class="field-label">Select Section</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Student ID</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Student Name</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>


                                <tr>
                                    <td colspan="7"></td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Action</span></td>
                                    <td>
                                        <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True" SkinID="smallcmb">
                                            <asp:ListItem Value="Pass">Promote</asp:ListItem>
                                            <asp:ListItem Value="Fail">Detain</asp:ListItem>
                                            <asp:ListItem Value="Retest">Retest</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="7">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>



                                    <td align="center" colspan="7" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Available">
                                                    <HeaderTemplate>
                                                        Select
                                                <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Result" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblResult" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Final Marks">
                                                    <ItemTemplate>
                                                        <asp:DataList runat="server" ID="dlMarks" RepeatDirection="Horizontal" Width="100%">

                                                            <ItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <th colspan="2" style="white-space: nowrap;">
                                                                            <asp:Label runat="server" Text='<%# Bind("Subject") %>' ID="lblSubject"></asp:Label></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Marks</td>
                                                                        <td>Grade</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"MColor").ToString()) %>' runat="server" Text='<%# Bind("Marks") %>' ID="lblMark"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"MColor").ToString()) %>' runat="server" Text='<%# Bind("Grade") %>' ID="lblGrade"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="7" valign="top">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>

</asp:Content>

