<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmExamSetup.aspx.vb" Inherits="Curriculum_clmExamSetup" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">

        $(document).ready(function() {
            $("[id*=txtBaseAmount]").val("0");
            $("[id*=txtVAT]").val("0");
            $("[id*=txtBoardAmount]").val("0");
            $("[id*=txtcost]").val("0");
        });

        $("body").on("change keyup", "[id*=txtBaseAmount], [id*=txtVAT], [id*=txtBoardAmount]", function () {
            var row = $(this).closest("tr");

            var BaseAmount = $("[id*=txtBaseAmount]", row).val();
            if (isNaN(BaseAmount) || BaseAmount == "") {
                BaseAmount = 0;
            }

            var VAT = $("[id*=txtVAT]", row).val();
            if (isNaN(VAT) || VAT == "") {
                VAT = 0;
            }

            var BoardAmount = $("[id*=txtBoardAmount]", row).val();
            if (isNaN(BoardAmount) || BoardAmount == "") {
                BoardAmount = 0;
            }

            var SubTotal = parseFloat(BaseAmount) + parseFloat(VAT) + parseFloat(BoardAmount)
            $("[id*=txtcost]", row).val(SubTotal.toFixed(2));
        });
        
        function isNumberKey(txt, evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                if (txt.value.indexOf('.') === -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (charCode > 31 &&
                    (charCode < 48 || charCode > 57))
                    return false;
            }
            return true;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Exam Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Subject</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="matters" colspan="4">
                                        <asp:CheckBox ID="chkSubj" runat="server" Text="Subject wise Fee " CssClass="field-label" />
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False" 
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                PageSize="20">
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>

                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("es_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Exam Paper">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSkill" runat="server" Text='<%# Bind("es_paper") %>' Width="150" autocomplete="off"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exam Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtShort" runat="server" Text='<%# Bind("es_date", "{0:dd/MM/yyyy}") %>' Width="115" autocomplete="off"></asp:TextBox>
                                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtShort">
                                            </ajaxToolkit:CalendarExtender>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration/Hr">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtdur" runat="server" Text='<%# Bind("es_dur") %>' Width="60" autocomplete="off"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtdur" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$"> </asp:RegularExpressionValidator>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Board Cost">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBoardAmount" runat="server" Text='<%# Bind("es_BoardAmount") %>' Width="65" autocomplete="off" onkeypress="return isNumberKey(this, event);"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revBoardAmount" runat="server"
                                                ControlToValidate="txtBoardAmount" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="School Cost">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBaseAmount" runat="server" Text='<%# Bind("es_BaseAmount") %>' Width="65" autocomplete="off" onkeypress="return isNumberKey(this, event);"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revBaseAmount" runat="server"
                                                ControlToValidate="txtBaseAmount" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VAT">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVAT" runat="server" Text='<%# Bind("es_VAT") %>' Width="65" autocomplete="off" onkeypress="return isNumberKey(this, event);"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revVat" runat="server"
                                                ControlToValidate="txtVAT" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtcost" runat="server" Text='<%# Bind("es_cost") %>' Width="85" autocomplete="off" onkeypress="return isNumberKey(this, event);" onFocus="this.blur()" ></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtcost" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$"> </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Examintion Level">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtExamintionLevel" runat="server" Text='<%# Bind("ES_Examintion_Level") %>' Width="90" autocomplete="off"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtExamintionLevel" ErrorMessage="*" ValidationExpression="^\d*([.]\d*)?|[.]\d+$"> </asp:RegularExpressionValidator>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Examination Board">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlExaminationBoard">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="App. Req.">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAppReq" runat="server" Width="50" Checked='<%# If(Eval("es_ApprovalRequired").ToString() = "False", "False", "True") %>'></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:FileUpload ID="fupload" runat="server" Width="145px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:ImageButton
                                                ID="imgF" runat="server" OnClick="imgF_Click" CommandArgument='<%# Bind("es_id") %>' />
                                            <asp:LinkButton ID="txtfile" runat="server"
                                                Text='<%# Bind("es_filename") %>' CommandName="View" CommandArgument='<%# Bind("es_id") %>'
                                                OnClick="txtfile_Click" />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

