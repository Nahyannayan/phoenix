﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmSmartTagetRelease.aspx.vb" Inherits="Curriculum_clmSmartTagetRelease"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


        function change_chk_state1(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect1") != -1 && document.forms[0].elements[i].disabled == false) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>SMART Target Release
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" width="100%" cellpadding="7" cellspacing="0">
                                <tr id="trAcd1" runat="server">
                                    <td align="left">
                                        <span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Set Up</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSetup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left">
                                        <span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" id="t4" runat="server">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" id="t1" runat="server" visible="false">
                                        <span class="field-label">Section</span>

                                    </td>

                                    <td align="left" colspan="10" id="t3" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlsection" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Date From</span>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtFrom" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                    </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Date To</span>
                                    </td>

                                    <td>
                                       <asp:TextBox ID="txtTo" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <br />
                                                    
                                    </td>
                                    <td><asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply" /></td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%">
                                <tr id="Tr1" runat="server">
                                    <td align="center">
                                        <asp:Button ID="lnkAdd" runat="server" CssClass="button"
                                            Text="All"></asp:Button>
                                        <asp:Button ID="lnkEdit" runat="server" CssClass="button" Text="Section wise"></asp:Button>
                                        <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                                            <asp:View ID="View1" runat="server">
                                                <asp:GridView ID="grdSect" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                    Width="100%">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Center">
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                            </EditItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <HeaderStyle />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                Select<br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                    ToolTip="Click here to select/deselect all rows" />
                                                            </HeaderTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="atd" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="AtdId" runat="server" Text='<%# bind("STU_SCT_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sect Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Start Date">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txts" runat="server" Text='<%# Bind("TSM_RELEASE_SDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>

                                                                <asp:ImageButton ID="imgs" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imgs" TargetControlID="txts">
                                                                </ajaxToolkit:CalendarExtender>

                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="End Date">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txte" runat="server" Text='<%# Bind("TSM_RELEASE_EDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>

                                                                <asp:ImageButton ID="imge" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imge" TargetControlID="txte">
                                                                </ajaxToolkit:CalendarExtender>

                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expired">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbEx" runat="server" Text='<%# Bind("Expire") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:ButtonField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                                <asp:Button ID="btnSecUpdate" runat="server" CssClass="button" Text="Update" OnClick="btnSecUpdate_Click" />
                                            </asp:View>
                                            <asp:View ID="View2" runat="server">
                                                <asp:GridView ID="grdStud" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                    Width="100%">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Select" Visible="false">
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkSelect1" runat="server" />
                                                            </EditItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <HeaderStyle Wrap="False" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect1" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                Select
                                                           <br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                                    ToolTip="Click here to select/deselect all rows" />
                                                            </HeaderTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="atd" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="stuId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stud ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stud Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Start Date">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txtstdt" runat="server" Text='<%# Bind("TSM_RELEASE_SDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                                <asp:ImageButton ID="imgsdate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imgsdate" TargetControlID="txtstdt">
                                                                </ajaxToolkit:CalendarExtender>

                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="End Date">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txtedate" runat="server" Text='<%# Bind("TSM_RELEASE_EDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                                <asp:ImageButton ID="imgedate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imgedate" TargetControlID="txtedate">
                                                                </ajaxToolkit:CalendarExtender>

                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expired">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cb1" runat="server" Text='<%# Bind("Expire") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:ButtonField>
                                                    </Columns>
                                                    <SelectedRowStyle />
                                                    <HeaderStyle />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" />
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="H_TEMP" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
