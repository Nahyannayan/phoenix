<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmReportWritingByStudent.aspx.vb" Inherits="Curriculum_clmReportWritingByStudent"
    Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

      function getcomments(ctlid, code, header, stuid, rsmid) {
            
            var sFeatures;
            sFeatures = "dialogWidth: 530px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            var str;
           
            if (code == 'ALLCMTS') {
                document.getElementById('<%=H_CTL_ID.ClientID%>').value = ctlid
                // result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);
                var oWnd = radopen("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "pop_comment");

               
            }

        }

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            var hv = document.getElementById('h_' + obj);



            if (div.style.display == "none") {
                div.style.display = "inline";
                hv.value = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                hv.value = "none"
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";

            }
        }


     function autoSizeWithCalendar(oWindow) {
         var iframe = oWindow.get_contentFrame();
         var body = iframe.contentWindow.document.body;

         var height = body.scrollHeight;
         var width = body.scrollWidth;

         var iframeBounds = $telerik.getBounds(iframe);
         var heightDelta = height - iframeBounds.height;
         var widthDelta = width - iframeBounds.width;

         if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
         if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
         oWindow.center();
     }
       function OnClientClose(oWnd, args) {
               var NameandCode;
            var arg = args.get_argument();
            var ctlid = document.getElementById('<%=H_CTL_ID.ClientID%>').value;
            if (arg) {
                NameandCode = arg.Comment.split('||');
              
                str = document.getElementById(ctlid).value;
                    document.getElementById(ctlid).value = str + NameandCode[0].replace("/ap/", "'");
            }
        }


         
        //------------------------------------------------------------------------------------------------------------------------------------
     
        
    </script>
      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="Literal1" runat="server" Text="Report Writing - By Student"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr id="trAcd" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                           >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Report Card</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td  align="left" width="20%"><span class="field-label">Report Card Schedule</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span>
                                    </td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" ><span class="field-label">Section</span>
                                    </td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span>
                                    </td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server"> </asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Student Name</span>
                                    </td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"> </asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="4" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <telerik:RadSpell ID="RadSpell2" ButtonType="LinkButton" IsClientID="true"
                                                        runat="server" SupportedLanguages="en-US,English" AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="vertical-align: top;">
                                                    <asp:GridView ID="gvStudent" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                                        AllowPaging="True" AllowSorting="True" PageSize="1" Width="100%">
                                                        <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                            Position="Top" PageButtonCount="100" />
                                                        <RowStyle CssClass="griditem" />
                                                        <PagerStyle Font-Bold="True" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                            Font-Size="Small"  HorizontalAlign="Right" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuID" runat="server" Text='<%# Eval("Stu_ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuPhotoPath" runat="server" Text='<%# Eval("Stu_PhotoPath") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Eval("Section") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("Stu_No") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server" Text='<%# Eval("Stu_Name") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td>
                                                    
                                                    <asp:Panel runat="server" ID="pnlStudent">
                                                        <table id="tbStudent" runat="server">
                                                            <tr>
                                                                <td align="center" class="border">
                                                                    <asp:Image ID="imgStud" runat="server" Height="80px" ImageUrl="~/Images/Photos/no_image.gif"
                                                                        Width="100px" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="text">
                                                                    <asp:Label ID="lblStudent" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnCancelSearch" runat="server" Text="Cancel Search" CssClass="button"
                                                        TabIndex="4" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="7" valign="top">
                                        <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="5" Width="100%">
                                            <RowStyle CssClass="griditem_alternative" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("SBG_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("SBG_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("SBG_ID") %>', 'alt');">
                                                            <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("Sbg_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSgrId" runat="server" Text='<%# Bind("ssd_sgr_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Sbg_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        </td>
                    </tr>
                    <tr>
                        <td colspan="100%">
                            <div id="div<%# Eval("SBG_ID") %>" style="display: inline;">
                                <asp:GridView ID="gvMarks" runat="server" Width="100%" CssClass="table table-bordered table-row" AutoGenerateColumns="false"
                                    EmptyDataText="No Data." OnRowDataBound="gvMarks_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="RsdId" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("sbg_id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="RsdId" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("rsd_id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TYPE" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblHeader" runat="server" Text='<%# Bind("rsd_header") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle></ItemStyle>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtGrade" Width="30px" runat="server" Visible='<%# Bind("V1") %>'></asp:TextBox>
                                                
                                                            <asp:TextBox ID="txtComments" TextMode="MultiLine" SkinID="MultiText_Large" runat="server" Visible='<%# Bind("V2") %>'></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="imgBtn" ImageUrl="~/Images/comment.jpg" Visible='<%# Bind("V2") %>' runat="server" />
                                                        
                                                <asp:DropDownList ID="ddlGrades" runat="server" Visible='<%# Bind("V3") %>'></asp:DropDownList>
                                                <asp:Label ID="lblMark" runat="server" Visible='<%# Bind("V4") %>'></asp:Label>
                                                <asp:Label ID="lblGrade" runat="server" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="75%"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <HeaderStyle CssClass="gridheader_pop" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadSpell ID="RadSpell1" ButtonType="LinkButton" IsClientID="true" AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" runat="server" SupportedLanguages="en-US,English" />
                                    </td>
                                    <td align="center"  colspan="6" valign="top">

                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"
                                             />
                                        <asp:Button ID="btnSaveNext" runat="server" Text="Save and Next" CssClass="button"
                                            TabIndex="4" />
                                        <asp:Button ID="btnPreview" runat="server" Text="View ReportCard" CssClass="button"
                                            TabIndex="4" />
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfTeacherGrades" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="H_CTL_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <%--<ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                TargetControlID="pnlStudent" VerticalSide="Middle" HorizontalOffset="10"
                                ScrollEffectDuration="10">
                            </ajaxToolkit:AlwaysVisibleControlExtender>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
