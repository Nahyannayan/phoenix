<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmStudentSelfAwareness.aspx.vb" Inherits="Curriculum_clmStudentSelfAwareness"
    Title="Untitled Page" %>
    
    <asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <script type="text/javascript" >
    function ScrollTop() {
     document.body.scrollTop = document.documentElement.scrollTop = 0;
    return true;
    }
    </script>
        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-book mr-3"></i>Student Self Awarness
            </div>

            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                            <td align="left" valign="bottom">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    SkinID="error"  ></asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table id="Table2" runat="server" align="center" width="100%"
                                    cellpadding="5" cellspacing="0" >
                                  
                                    <tr id="tr1" runat="server">
                                        
                                        <td align="left" ><span class="field-label">Academic Year</span></td>   
                                        <td align="left" >                                    
                                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                           </td> 
                                        <td colspan="2"></td>                                                                               
                                    </tr>
                                    <tr id="tr2" runat="server">
                                        <td align="left" ><span class="field-label">Grade</span> 
                                        </td>
                                        
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" ><span class="field-label">Section</span>
                                        </td>
                                     
                                        <td align="left"  colspan="2">
                                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tr3" runat="server">
                                        <td align="left" ><span class="field-label">Student ID</span> 
                                        </td>
                                        
                                        <td align="left" >
                                            <asp:TextBox ID="txtStuNo" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td align="left" ><span class="field-label">Student Name</span> 
                                        </td>
                                       
                                        <td align="left" >
                                            <asp:TextBox ID="txtName" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td >
                                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                                />
                                        </td>
                                    </tr>
                                    <tr id="tr4" runat="server">
                                        <td align="center"  colspan="7" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvStudent" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                                            AllowPaging="True" AllowSorting="True" PageSize="1"  >
                                                            <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                                Position="Top" PageButtonCount="100" />
                                                            <RowStyle  />
                                                            <PagerStyle Font-Bold="True" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                Font-Size="Small" ForeColor="White" HorizontalAlign="Right" />
                                                            <AlternatingRowStyle  />
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStuID" runat="server" Text='<%# Eval("Stu_ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStuPhotoPath" runat="server" Text='<%# Eval("Stu_PhotoPath") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Eval("Section") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("Stu_No") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Eval("Stu_Name") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="imgStud" runat="server" Height="70px" ImageUrl='<%# GetPhoto(Eval("Stu_Photopath")) %>' Width="50px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <asp:Button ID="btnCancelSearch" runat="server" Text="Cancel Search" CssClass="button"
                                                            TabIndex="4"  />
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="tr6" runat="server">
                                        <td colspan="7">
                                            <asp:GridView ID="gvSetup" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                HeaderStyle-Height="30" PageSize="20">
                                                <RowStyle  Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSemId" runat="server" Text='<%# Bind("SEM_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Skill">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtSkill" runat="server"  Text='<%# Bind("SEM_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtDescr" runat="server" TextMode="MultiLine" SkinID="MultiText_Large" Width="500px" Text='<%# Bind("SED_DESCR") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle  Wrap="False" />
                                                <HeaderStyle  Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr id="tr7" runat="server">
                                        <td colspan="7" align="center">
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                                TabIndex="7" />
                                            <asp:Button ID="btnSaveNext" OnClientClick="javascript:scroll(0,0);return true;" runat="server" CssClass="button" Text="Save and Next" ValidationGroup="groupM1"
                                                TabIndex="7" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hfSTU_ID" runat="server" />
                </div>
            </div>
        </div>

</asp:Content>
