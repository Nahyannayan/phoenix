﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProgressTest.aspx.vb" Inherits="Curriculum_clmProgressTest" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="Label2" runat="server" Text="Upload Progress Test Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">


    <table align="center"  style="border-collapse: collapse"
                    cellpadding="4" cellspacing="0" width="100%">
       
        <tr>
            <td align="left" width="20%">
                Academic Year
            </td>
          
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="20">
            <td align="left" valign="middle" width="20%">
                Grade
            </td>
            
            <td align="left" valign="middle"width="30%">
                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;
            </td>
            <td align="left" valign="middle"width="20%">
                Section
            </td>
            
            <td align="left" colspan="1" valign="middle" width="30%">
                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
     <tr>
                        <td colspan="6">
    <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="1"
                               width="100%">
        <ajaxToolkit:TabPanel ID="HT1" runat="server" HeaderText="Progress Test Updation">
            <ContentTemplate>
                <div style="overflow: auto">
                    <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                        <tr id="Tr1" runat="server">
                            <td id="Td1" runat="server">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    SkinID="error" Width="133px" Style="text-align: center"></asp:Label>
                            </td>
                        </tr>
                        <tr id="Tr2" runat="server">
                            <td id="Td2" align="left" class="matters" runat="server">
                                <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-row"
                                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                            Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="CMTID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="cmtId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student ID">
                                            <HeaderTemplate>
                                                
                                                            <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Student ID"></asp:Label><br />
                                                       
                                                                        <asp:TextBox ID="txtOption" runat="server" Width="60px"></asp:TextBox>
                                                                    
                                                                            <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnEmpid_Search_Click" />
                                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubId" runat="server" Width="120px" Text='<%# Bind("STU_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <HeaderTemplate>
                                               
                                                            <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="Name"></asp:Label><br />
                                                       
                                                                        <asp:TextBox ID="txtOption1" runat="server" Width="60px"></asp:TextBox>
                                                                   
                                                                            <asp:ImageButton ID="btnstuname_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                OnClick="btnstuname_Search_Click" />
                                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Width="200px" Text='<%# Bind("STU_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ></HeaderStyle>
                                            <ItemStyle ></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Math Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtnv" runat="server" Width="50px" Text='<%# Bind("PT_MS")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Math Stanine">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtv" runat="server" Width="50px" Text='<%# Bind("PT_MT")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtqu" runat="server" Width="50px" Text='<%# Bind("PT_ES")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English Stanine">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtsp" runat="server" Width="50px" Text='<%# Bind("PT_ET")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Science Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtmc" runat="server" Width="50px" Text='<%# Bind("PT_SS")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Science Stanine">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtscp" runat="server" Width="50px" Text='<%# Bind("PT_ST")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="Tr67" runat="server">
                            <td id="Td67" align="center"  colspan="12" runat="server">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR"
                                     />&nbsp;
                               
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Upload">
            <ContentTemplate>
                <table align="center" width="100%">
                    <tr>
                        <td>
                            <table  width="100%" id="Table3"  cellpadding="0"
                                                        cellspacing="0" style="border-collapse: collapse" width="100%">
                                <tr>
                                    <td colspan="2" >
                                        <font color="red"></font>
                                        <tr id="Tr3" runat="server">
                                            <td id="Td3" runat="server" colspan="2">
                                                <asp:Label ID="Label1" runat="server" Text="Please upload the excel with column names as StudentID,StudentName,MathScore,MathStanine,EnglishScore,EnglishStanine,ScienceScore, ScienceStanine"
                                                    CssClass="error" EnableViewState="False" SkinID="error" Style="text-align: center"></asp:Label>
                                            </td>
                                        </tr>
                                        <asp:Label ID="lblerror3" runat="server" CssClass="error" EnableViewState="False"
                                            SkinID="error"  Style="text-align: left"></asp:Label>
                                    </td>
                                </tr>
                               
                                    
                                    <tr>
                                        <td align="left" width="20%">
                                            Select File
                                        </td>
                                        
                                        <td align="left" >
                                            <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"
                                                 />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Width="101px"
                                                OnClick="btnUpload_Click" CausesValidation="False" />
                                        </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    
                        </td>
                    </tr>
                </table>
 </div>
        </div>
    </div>
   
</asp:Content>

