<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPEStudentFitnessProfileGraph.aspx.vb" Inherits="rptPEStudentFitnessProfileGraph" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


<script language="javascript" type="text/javascript">
    
     function openWin() {
            var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 600px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
        var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
        var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
        if (GRD_IDs == '') {
            alert('Please select atleast one Grade')
            return false;
        }    
                                      
        var oWnd = radopen("../../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.Students.split('|');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }
        

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    
    function GetSTUDENTS() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 600px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
        var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
        var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
        if (GRD_IDs == '') {
            alert('Please select atleast one Grade')
            return false;
        }
        result = window.showModalDialog("../../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures)
        if (result != '' && result != undefined) {
            document.getElementById('<%=h_STU_IDs.ClientID %>').value = result; //NameandCode[0];
        }
        else {
            return false;
        }
    }
         
   </script>
   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
    <table class ="matters" align="center" border="1" bordercolor="#1b80b6" style="border-collapse:collapse" cellpadding="4" cellspacing="0" >
                    <tr>
                        <td valign="middle" class="subheader_img" colspan="6">
                            <asp:Label id="lblHeader" runat="server" Text="Progress Report"></asp:Label>
                            <asp:Button ID="btnCheck" style="display:none;"  runat="server" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Academic Year</td>
                        <td align="center">
                            :</td>
                        <td align="left" colspan="4">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
        <tr>
            <td align="left">
                Activity Schedule</td>
            <td align="center">
                :</td>
            <td align="left" colspan="4">
                <asp:DropDownList id="ddlSchedule" runat="server" Width="264px">
                </asp:DropDownList></td>
        </tr>
        <tr id="trGrade" runat="server">
            <td align="left">
                Grade</td>
            <td align="center">
                :</td>
            <td align="left">
                            <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
            <td>
                Section</td>
            <td>
                :</td>
            <td>
                <asp:DropDownList ID="ddlSection" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
                    <tr>
                        <td align="left" valign="top" >
                            Student</td>
                        <td align="center" valign="top" >
                            :</td>
                        <td align="left" colspan="4">
                            <asp:TextBox id="txtStudIDs" runat="server" Width="330px"></asp:TextBox>
                            <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click">
                            </asp:ImageButton>
                            <asp:GridView id="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="353px" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <columns>
<asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
<asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
</columns>
                                <headerstyle cssclass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
        <tr>
            <td align="left" colspan="6" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />&nbsp;
                <asp:Button id="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                    Text="Download Report in Pdf" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
                </table>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField id="hfbFinalReport" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_TYPE" runat="server"/>
    <cr:crystalreportsource id="rs" runat="server" cacheduration="1">
    </cr:crystalreportsource>
    <asp:HiddenField id="hfbDownload" runat="server">
    </asp:HiddenField>
</asp:Content>

