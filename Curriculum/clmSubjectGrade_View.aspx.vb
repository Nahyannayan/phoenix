Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmSubjectGrade_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GridBind()
                    gvGrades.Attributes.Add("bordercolor", "#1b80b6")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT GRD_ID,STM_ID,GRM_DISPLAY,STM_DESCR,GRD_DISPLAYORDER FROM " _
                                & " GRADE_BSU_M AS A INNER JOIN STREAM_M AS B ON A.GRM_STM_ID=B.STM_ID" _
                                & " INNER JOIN GRADE_M AS C ON A.GRM_GRD_ID=C.GRD_ID WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String


        Dim ddlgvGrade As New DropDownList
        Dim ddlgvStream As New DropDownList
        Dim selectedGrade As String = ""
        Dim selectedStream As String = ""
        Dim objGrade As Object

        If gvGrades.Rows.Count > 0 Then
            ddlgvGrade = gvGrades.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
                strFilter += " and grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

          
            ddlgvStream = gvGrades.HeaderRow.FindControl("ddlgvStream")
            If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then
                strFilter += " and stm_descr='" + ddlgvStream.Text + "'"
                selectedStream = ddlgvStream.Text
            End If

            If strFilter.Trim <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " order by grd_displayorder"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvGrades.DataSource = ds
        gvGrades.DataBind()

        Dim dt As DataTable = ds.Tables(0)

        If gvGrades.Rows.Count > 0 Then
            ddlgvGrade = gvGrades.HeaderRow.FindControl("ddlgvGrade")
            ddlgvStream = gvGrades.HeaderRow.FindControl("ddlgvStream")


            Dim dr As DataRow
            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")

            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")

            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    ''check for duplicate values and add to dropdownlist 

                    If ddlgvGrade.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(2))
                    End If

                    If ddlgvStream.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvStream.Items.Add(.Item(3))
                    End If

                End With
            Next

            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If
        End If
      
        If Session("multistream") = 0 Then
            gvGrades.Columns(4).Visible = False
        End If

    End Sub

    Sub BindSubjects(ByVal grd_id As String, ByVal stm_id As String, ByVal gvSubjects As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        'Dim str_query As String = "SELECT B.SBM_DESCR AS SBM_DESCR,bOPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes'" _
        '                         & " ELSE 'No' END,DPT_DESCR,SBM_PARENT=CASE SBG_PARENT_ID WHEN 0 THEN ''" _
        '                         & " ELSE D.SBM_DESCR END FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS B ON A.SBG_SBM_ID=" _
        '                         & " B.SBM_ID INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID LEFT OUTER JOIN SUBJECT_M AS D" _
        '                         & " ON A.SBG_PARENT_ID=D.SBM_ID WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        '                         & " AND SBG_GRD_ID='" + grd_id + "' AND SBG_STM_ID=" + stm_id

        Dim str_query As String = "SELECT SBG_DESCR , " _
                             & "  DPT_DESCR,bOPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes'" _
                             & " ELSE 'No' END,SBG_PARENTS,SBG_bREPCRD_DISPLAY,SBG_bMRK_DISPLAY," _
                             & " SBG_ENTRYTYPE,SBG_MAJOR=CASE SBG_bMAJOR WHEN 'True' then 'Yes' else 'No' End" _
                             & " FROM SUBJECTS_GRADE_S AS A  " _
                             & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
                             & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_GRD_ID='" + grd_id + "'" _
                             & " AND SBG_STM_ID=" + stm_id + " ORDER BY SBG_ORDER,SBG_DESCR,SBG_PARENTS"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvSubjects.DataSource = ds
        gvSubjects.DataBind()

    End Sub

#End Region

    Protected Sub gvGrades_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGrades.PageIndexChanging
        Try
            gvGrades.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvGrades_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrades.RowCommand
        Try
            If e.CommandName = "View" Or e.CommandName = "Select" Or e.CommandName = "Select2" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGrades.Rows(index), GridViewRow)

                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblGrdId As Label
                Dim lblStmId As Label
                Dim lblGrade As Label
                Dim lblStream As Label

                With selectedRow
                    lblGrdId = .FindControl("lblGrdId")
                    lblStmId = .FindControl("lblStmId")
                    lblGrade = .FindControl("lblGrade")
                    lblStream = .FindControl("lblStream")
                End With

                Dim url As String
                If e.CommandName = "View" Then
                    ViewState("datamode") = Encr_decrData.Encrypt("none")
                    url = String.Format("~\Curriculum\clmSubjectGrade_M.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                         & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                         & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                         & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                ElseIf e.CommandName = "Select" Then
                    ViewState("datamode") = Encr_decrData.Encrypt("edit")
                    url = String.Format("~\Curriculum\clmSubjectGradeSetOrder_M.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                         & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                         & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                         & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Else
                    ViewState("datamode") = Encr_decrData.Encrypt("edit")
                    url = String.Format("~\Curriculum\clmSubjectBrownbookSettings.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                         & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                         & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                         & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))

                End If
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvGrades_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGrades.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblGrdid As New Label
                Dim lblStmid As New Label

                lblGrdid = e.Row.FindControl("lblGrdId")
                lblStmid = e.Row.FindControl("lblStmId")

                Dim gvSubjects As New GridView
                gvSubjects = e.Row.FindControl("gvSubjects")
                BindSubjects(lblGrdid.Text, lblStmid.Text, gvSubjects)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvGrades_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvGrades.RowEditing

    End Sub
End Class
