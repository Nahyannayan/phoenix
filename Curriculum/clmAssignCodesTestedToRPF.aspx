﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmAssignCodesTestedToRPF.aspx.vb" Inherits="Curriculum_clmAssignCodesTestedToRPF" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Assign Codes Tested
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnAddCriteria">
                                <table align="center" cellpadding="5"
                                    cellspacing="0" id="tb1" runat="server" width="100%">

                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Select Academic Year</span> </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;</td>
                                        <td align="left" width="20%"><span class="field-label">Select Report Card</span> </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Select Grade</span> </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Report Schedule</span> </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlRPF" runat="server">
                                            </asp:DropDownList>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Select Subject</span> </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Select Category</span></td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlCodesTestedCategory" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>

                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnAddCriteria" runat="server" CssClass="button" Text="Show Codes Tested"
                                                ValidationGroup="groupM1" />
                                        </td>
                                    </tr>
                                    <tr id="trGrid" runat="server">
                                        <td colspan="4" align="center">
                                            <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                EmptyDataText="No record added." HeaderStyle-Height="30"
                                                PageSize="20" CssClass="table table-bordered table-row">
                                                <RowStyle Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                        <HeaderStyle />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCTMID" runat="server" Text='<%# Bind("CTM_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status ID" Visible="false">
                                                        <HeaderStyle />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatusID" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CTC ID" Visible="false">
                                                        <HeaderStyle />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCTCID" runat="server" Text='<%# Bind("CTM_CTC_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Assigned to Chosen RPF">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStatusHeader" Text="Assigned" runat="server"></asp:Label><br />
                                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                                                <asp:ListItem Text="All" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <HeaderStyle />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkStatus" runat="server" />
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("CTA_STATUS") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Codes Tested Description" Visible="true">
                                                        <HeaderStyle Width="50" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCodesTested" runat="server" Text='<%# Bind("CTM_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle Wrap="False" />
                                                <HeaderStyle Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr id="trSave" runat="server">
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

