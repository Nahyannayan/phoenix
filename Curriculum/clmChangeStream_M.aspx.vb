Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmChangeStream_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim subjs As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_NEWID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))

                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblStuName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                    GetRequestInfo()

                    PopulateNewSection()


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub


#Region "private Methods"

    Sub GetRequestInfo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCS_ID,SCS_STM_NEW_ID,SCS_SUBJECTS,STM_DESCR,SCS_REMARKS FROM" _
                                 & " STUDENT_CHANGESTREAMREQ_S AS A INNER JOIN STREAM_M AS B " _
                                 & " ON A.SCS_STM_NEW_ID=B.STM_ID WHERE SCS_STU_ID=" + hfSTU_ID.Value _
                                 & " AND SCS_ACD_ID=" + hfACD_ID.Value + " AND SCS_APPROVE=0 AND SCS_TYPE='CS'"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        hfSCS_ID.Value = 0
        While reader.Read
            hfSCS_ID.Value = reader.GetValue(0).ToString
            hfSTM_NEWID.Value = reader.GetValue(1)
            lblSubjects.Text = reader.GetString(2)
            lblNewStream.Text = reader.GetString(3)
            lblRemarks.Text = reader.GetString(4).ToString.Replace("''", "'")
        End While
        reader.Close()

        If hfSCS_ID.Value <> 0 Then
            ViewState("datamode") = "edit"
        Else
            ViewState("datamode") = "add"
        End If
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Sub PopulateNewSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M AS A " _
                               & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                               & " WHERE GRM_ACD_ID=" + hfACD_ID.Value + " AND GRM_GRD_ID='" + hfGRD_ID.Value.ToString + "'" _
                               & " AND GRM_SHF_ID=" + hfSHF_ID.Value + " AND GRM_STM_ID=" + hfSTM_NEWID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function SaveData(ByVal approve As Integer) As Boolean
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If approve = 1 Then
                    UtilityObj.InsertAuditdetails(transaction, "delete", "STUDENT_GROUPS_S", "SSD_ID", "SSD_STU_ID", "SSD_STU_ID=" + hfSTU_ID.Value + " AND SSD_ACD_ID=" + hfACD_ID.Value)
                    UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_PROMO_S", "STP_ID", "STP_STU_ID", "STP_STU_ID=" + hfSTU_ID.Value + " AND STP_ACD_ID=" + hfACD_ID.Value)
                End If

                Dim str_query As String = "exec saveCHANGESTREAMAPPROVAL " _
                                         & hfACD_ID.Value + "," _
                                         & hfSTU_ID.Value + "," _
                                         & "'" + hfGRD_ID.Value + "'," _
                                         & hfSHF_ID.Value + "," _
                                         & hfSTM_NEWID.Value + "," _
                                         & ddlSection.SelectedValue.ToString + "," _
                                         & hfSCS_ID.Value + "," _
                                         & approve.ToString + "," _
                                         & "'" + IIf(txtDate.Text = "", "01/01/1900", Format(Date.Parse(txtDate.Text), "MM/dd/yyyy")) + "'," _
                                         & "'" + Session("susr_name") + "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + hfSTU_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                 Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function
    
#End Region

   
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If ddlSection.SelectedValue = 0 Then
                lblError.Text = "Please select a section"
            End If

            If SaveData(1) = True Then
                lblError.Text = "The student " + lblStuName.Text + " has been successfully transfered from " + lblStream.Text + " stream to  " + lblNewStream.Text + " stream"
                btnApprove.Enabled = False
                btnReject.Enabled = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            If SaveData(2) = True Then
                lblError.Text = "The request for stream change is rejected"
                btnReject.Enabled = False
                btnApprove.Enabled = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
