<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstatusofMarks.aspx.vb" Inherits="Curriculum_rptstatusofMarks" title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">

   function GetPopUp(id)
       {     
            //alert(id);
            var sFeatures;
            sFeatures="dialogWidth: 429px; ";
            sFeatures+="dialogHeight: 345px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           if(id==5)//Accadamic Year
           {
                var GradeId
                GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
               var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=SBJGRD&GRDID=" + GradeId + "", "pop_subject");
                //alert(GradeId + 'ddd');
               <%-- result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRD&GRDID="+GradeId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result.split('___')[0] ;
                    document.getElementById('<%=txtGrdSubject.ClientID %>').value = result.split('___')[1];
                }
                else
                {
                    return false;
                }--%>
            }
        }//CMTSCAT
             
</script>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Assesment Grade Entry Status <br />
            <asp:Label ID="lblHeader" runat="server" ></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                   
                    <tr valign="top">
                        <td align="left" colspan="4" >
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                            </asp:Label><asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            &nbsp;
                        </td>
                    </tr>                    
                    <tr>
                        <td align="left" width="20%"  ><span class="field-label">Academic Year</span></td>
                    
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlAcdID" runat="server" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged" >
                            </asp:DropDownList></td>
                        <td align="left" width="20%"  ><span class="field-label">Report</span></td>
                       
                        <td align="left" width="30%"  >
                            <asp:DropDownList ID="ddlReportId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportId_SelectedIndexChanged"
                               >
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Grade</span></td>
                      
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged"
                               >
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"  ><span class="field-label">Subject</span>
                        </td>
                       
                        <td align="left" width="30%"  >
                           <%-- <asp:TextBox ID="txtGrdSubject" runat="server" Enabled="False" >
                            </asp:TextBox>
                            <asp:ImageButton ID="imgSbjGrade" runat="server" ImageUrl="../Images/cal.gif" OnClick="imgSbjGrade_Click"
                                OnClientClick="GetPopUp(5)"></asp:ImageButton>--%>
                             <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList>         
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Report Schedule</span></td>
                        
                        <td align="left" width="30%"  >
                            <asp:DropDownList ID="ddlRptSchedule" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRptSchedule_SelectedIndexChanged"
                                >
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"  ></td>
                       
                        <td align="left" width="30%"  >
                            <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click1"
                                Text="Search"  />
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left"  colspan="4">
                            <asp:GridView ID="gvStudents" runat="server" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                EnableTheming="True" SkinID="GridViewNormal" AllowPaging="True" OnPageIndexChanging="gvStudents_PageIndexChanging" PageSize="20" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="SUBJECT" HeaderText="SUBJECT"></asp:BoundField>
                                    <asp:BoundField DataField="GROUPNAME" HeaderText="GROUP"></asp:BoundField>
                                    <asp:BoundField DataField="GRADE" HeaderText="GRADE"></asp:BoundField>
                                    <asp:BoundField DataField="SBJTYPE" HeaderText="TYPE"></asp:BoundField>
                                    <asp:BoundField DataField="STATUS" HeaderText="STATUS"></asp:BoundField>
                                    <asp:HyperLinkField DataTextField="ViewScreen" NavigateUrl="~/Curriculum/rptStudentsMarks.aspx" HeaderText="VIEW" Visible="False"></asp:HyperLinkField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                   
                </table>
                <asp:HiddenField ID="H_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
                &nbsp;
            </div>
        </div>
    </div>
    
</asp:Content>

