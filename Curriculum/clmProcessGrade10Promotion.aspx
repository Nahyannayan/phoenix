<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProcessGrade10Promotion.aspx.vb" Inherits="Curriculum_clmProcessGrade10Promotion" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudPromote.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Promote Students"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <table align="center" id="tbPromote" runat="server" width="100%" cellpadding="0" cellspacing="0" >

                                <tr>
                                    <td align="left" width="20%"  > <span class="field-label">Select Academic Year</span></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%" > <span class="field-label">Select Section</span></td>
                                   
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%" > <span class="field-label">Student ID</span></td>
                                   
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left" width="20%" > <span class="field-label">Student Name</span></td>
                                   
                                    <td align="left" width="30%"   colspan="2">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"  > <span class="field-label">Stream</span></td>
                                    
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlStream" SkinID="smallcmb" runat="server"  >
                                        </asp:DropDownList></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                   <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>


                                    <td align="left" width="20%"  > <span class="field-label">Promote to section</span></td>
                                   
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlPromoteSection" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:Button ID="btnUpdate1" runat="server" Text="Update" CssClass="button" TabIndex="4" Visible="False" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvStudPromote" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            HeaderStyle-Height="30" PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsctId" runat="server" Text='<%# Bind("STU_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Promoted Section">
                                                    <HeaderTemplate>
                                                       Promoted Section
                                                                    <br />
                                                                    <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged"  >
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle  CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem"  />
                                            <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="4" align="center">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" TabIndex="4" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID_PROMOTED" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID_PROMOTED" runat="server" />
                            <asp:HiddenField ID="hfSCT_ID" runat="server" />
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

