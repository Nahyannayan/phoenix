<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmATTEntry_M.aspx.vb" Inherits="Curriculum_clmATTEntry_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Attendance Entry
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left"  valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left"  valign="top" width="15%"><span class="field-label">Activity</span></td>
                                    
                                    <td align="left"  valign="top" width="35%">
                                        <asp:Label ID="lblActivity" runat="server" class="field-value"></asp:Label></td>
                                    <td align="left"  valign="top" width="15%"><span class="field-label">Subject</span></td>
                                    
                                    <td align="left"  valign="top" width="35%">
                                        <asp:Label ID="lblSubject" runat="server" class="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"  valign="top"><span class="field-label">Group</span></td>
                                    
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblGroup" runat="server" class="field-value"></asp:Label></td>
                                    <td align="left"  valign="top"><span class="field-label">Date</span></td>
                                    
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblDate" runat="server" class="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="8" valign="top"></td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="8" valign="top">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                        <asp:Button ID="btnMark1" runat="server" Text="Mark Entry" CssClass="button" TabIndex="4" /></td>
                                </tr>

                                <tr>
                                    <td align="center"  colspan="8" valign="top">

                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            Width="100%" PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStaId" runat="server" Text='<%# Bind("Sta_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        <asp:Label ID="lblStatus" ForeColor="red" runat="server" Text='<%# Bind("Stu_Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attendance">


                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlAttendance" runat="server" SelectedValue='<%# Bind("ATT") %>'>
                                                            <asp:ListItem Value="P">Present</asp:ListItem>
                                                            <asp:ListItem Value="A">Absent</asp:ListItem>
                                                            <asp:ListItem Value="L">Approved Leave</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="6" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                        <asp:Button ID="btnMark" runat="server" Text="Mark Entry" CssClass="button" TabIndex="4" /></td>


                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfCAS_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMarkUrl" runat="server"></asp:HiddenField>

</asp:Content>

