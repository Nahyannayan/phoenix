Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CURRICULUM

Partial Class CUR_GRADESLABMASTER
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim CurUsr_id As String
    Dim CurRole_id As String
    Dim CurBsUnit As String
    Dim USR_NAME As String
    Dim content As ContentPlaceHolder

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            CurUsr_id = Session("sUsr_id")
            CurRole_id = Session("sroleid")
            CurBsUnit = Session("sBsuid")
            USR_NAME = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If
            'C100125
            If USR_NAME = "" Or CurBsUnit = "" Or MainMnu_code <> CURR_CONSTANTS.MNU_GRADESLAB_MASTER Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'disable the control based on the rights
                content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight2.setpage(content, ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("datamode") = "view" Then
                DissableControls(True)
                Dim vGSM_SLAB_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("GSM_SLAB_ID").Replace(" ", "+"))
                FillDetails(vGSM_SLAB_ID)
            End If
        End If
    End Sub

    Private Sub FillDetails(ByVal vGSM_SLAB_ID As Integer)
        Dim vGRADE_SLAB As CURRICULUM.GRADESLABMASTER
        vGRADE_SLAB = CURRICULUM.GRADESLABMASTER.GetDetails(vGSM_SLAB_ID)
        txtGradeSlabDescr.Text = vGRADE_SLAB.GRADE_SLAB_DESCR
        txtGradeTotalMark.Text = vGRADE_SLAB.TOTAL_MARK
        h_GRADE_SLAB_ID.Value = vGRADE_SLAB.GRADE_SLAB_ID
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        ClearAllFields()
        DissableControls(False)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        txtGradeSlabDescr.ReadOnly = dissable
        txtGradeTotalMark.ReadOnly = dissable
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If Not IsNumeric(txtGradeTotalMark.Text) Then
            lblError.Text = "Invalid Total Mark"
            Return
        End If
        Dim vGRD_SLAB As New GRADESLABMASTER
        vGRD_SLAB.GRADE_SLAB_DESCR = txtGradeSlabDescr.Text
        vGRD_SLAB.TOTAL_MARK = txtGradeTotalMark.Text
        vGRD_SLAB.BSU_ID = Session("sBSUID")
        vGRD_SLAB.GRADE_SLAB_ID = IIf(h_GRADE_SLAB_ID.Value <> "", h_GRADE_SLAB_ID.Value, 0)
        Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = GRADESLABMASTER.SaveDetails(vGRD_SLAB, objConn, stTrans)
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            ' errNo = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            errNo = 0
            If errNo = 0 Then
                stTrans.Commit()
                ClearAllFields()
                lblError.Text = "Grade Slab Details saved sucessfully..."
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(errNo)
            End If
        Catch
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(-1)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearAllFields()
        txtGradeSlabDescr.Text = ""
        txtGradeTotalMark.Text = ""
        h_GRADE_SLAB_ID.Value = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearAllFields()
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

End Class
