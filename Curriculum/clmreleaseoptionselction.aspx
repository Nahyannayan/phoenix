<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmreleaseoptionselction.aspx.vb" Inherits="Curriculum_clmreleaseoptionselction" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Option Selection"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" 
                    cellpadding="5"
                    cellspacing="0" style="width: 100%">

                    <tr height="1px">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>




                    <tr>

                        <td align="center" colspan="8">

                            <table id="Table1" runat="server" align="center"
                                cellpadding="5"
                                cellspacing="0" style="width: 100%">

                                <tr>
                                    <td colspan="3" align="center" >

                                        <table id="Table2" runat="server" align="center" 
                                            cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse">
                                            
                                            <tr>
                                                <td align="left" ><span class="field-label">Current Academic Year</span></td>
                                                <td  align="center">:</td>
                                                <td align="left" >
                                                    <asp:Label ID="lblacid" runat="server" class="field-value"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="center" style="width: 100%">
                                                    <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" Width="100%" BorderStyle="None">
                                                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOptId" runat="server" Text='<%# Bind("grm_id") %>'></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>




                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOption" runat="server" Text='<%# Bind("GRM_GRD_ID") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Release">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkAll" runat="server" Text='<%# Bind("tag") %>' /></ItemTemplate>
                                                            </asp:TemplateField>





                                                        </Columns>

                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                    </asp:GridView>


                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="center" style="width: 200px">
                                                    <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" Width="115px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />

                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

