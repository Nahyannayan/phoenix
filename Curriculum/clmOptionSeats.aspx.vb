﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM



Partial Class Curriculum_clmOptionSeats
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100360") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))



                    BindGrade()

                    gridbind()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindGrade()
        ddlGrade.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                              & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim grade As String()
            Dim ds As New DataSet
            lblError.Text = ""
            grdStud.DataSource = Nothing
            grdStud.DataBind()

            grade = ddlGrade.SelectedValue.Split("|")
            str_Sql = "select distinct SBG_SBM_ID,SBG_DESCR,SBG_ORDER,SBG_TOTSEATS,SBG_FEES  from SUBJECTS_GRADE_S " _
                    & " where SBG_ACD_ID =" + ddlAcademicYear.SelectedValue + " and SBG_GRD_ID ='" + grade(0) + "' and SBG_STM_ID =" + grade(1) + " and " _
                    & " SBG_bOPTIONAL =1 order by SBG_descr "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)




            If ds.Tables(0).Rows.Count > 0 Then
                grdStud.DataSource = ds.Tables(0)
                grdStud.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdStud.DataSource = ds.Tables(0)
                Try
                    grdStud.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdStud.Rows(0).Cells.Clear()
                grdStud.Rows(0).Cells.Add(New TableCell)
                grdStud.Rows(0).Cells(0).ColumnSpan = columnCount
                grdStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        gridbind()
    End Sub
 
    Sub UpdateData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = ""

        Dim i As Integer
        Dim lblid As New Label
        Dim txt As New TextBox
        Dim txtf As New TextBox
        Dim grade As String()


        grade = ddlGrade.SelectedValue.Split("|")
        For i = 0 To grdStud.Rows.Count - 1
            With grdStud.Rows(i)
                lblid = .FindControl("AtdId")
                txt = .FindControl("txtSeats")
                txtf = .FindControl("txtfee")
                If txt.Text <> "" Then
                    Dim cmd As New SqlCommand
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    cmd = New SqlCommand("dbo.UpdateOptionalSeats", objConn)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SBG_SBM_ID", lblid.Text)
                    cmd.Parameters.AddWithValue("@SBG_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                    cmd.Parameters.AddWithValue("@SBG_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@SBG_STM_ID", grade(1))
                    cmd.Parameters.AddWithValue("@SBG_TOTSEATS", txt.Text)
                    cmd.Parameters.AddWithValue("@SBG_FEES", txtf.Text)
                    cmd.ExecuteNonQuery()

                    cmd.Dispose()
                    objConn.Close()
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        UpdateData()
    End Sub
End Class
