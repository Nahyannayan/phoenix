﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmExamSetup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C410126") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindGrade()
                    BindSubject()
                    GridBind()
                    gvSkill.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                              & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub
    Public Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_PARENTS+'-'+SBG_SHORTCODE END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue


        If ddlGrade.SelectedValue <> "" Then


            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)


        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()


    End Sub
    Private Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim grade As String()
        Dim bShowSlno As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        grade = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT es_id,es_paper ,es_date ,es_dur  ,es_cost,es_doc,es_filename,ES_Examintion_Level,ES_EXL_ID, es_BaseAmount, es_VAT, es_BoardAmount, es_ApprovalRequired  FROM dbo.EXAMSETUP" _
                                & " WHERE ES_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND " _
                                & " ES_GRD_ID='" + grade(0) + "' AND " _
                                & " ES_SBG_ID='" + ddlSubject.SelectedValue + "'"

        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = Convert.ToDateTime(.Item(2)).ToString("dd/MMM/yyyy")
                dr.Item(3) = .Item(3)
                dr.Item(4) = .Item(4)
                dr.Item(5) = .Item(5)
                dr.Item(6) = .Item(6)
                dr.Item(7) = .Item(7)
                dr.Item(8) = .Item(8)
                dr.Item(9) = "edit"
                dr.Item(10) = i.ToString
                dr.Item(11) = .Item("es_BaseAmount")
                dr.Item(12) = .Item("es_VAT")
                dr.Item(13) = .Item("es_BoardAmount")
                dr.Item(14) = .Item("es_ApprovalRequired")
                dt.Rows.Add(dr)


            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 50 rows
        For i = 0 To 15 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = ""
            dr.Item(3) = ""
            dr.Item(4) = ""
            dr.Item(5) = ""
            dr.Item(6) = ""
            dr.Item(7) = ""
            dr.Item(8) = ""
            dr.Item(9) = "edit"
            dr.Item(10) = i.ToString
            dr.Item(11) = ""
            dr.Item(12) = ""
            dr.Item(13) = ""
            dr.Item(14) = 1

            dt.Rows.Add(dr)
            'With ds.Tables(0).Rows(i)
            '    Dim ddlExaminationBoard As DropDownList
            '    ddlExaminationBoard = gvSkill.Rows(i).FindControl("ddlExaminationBoard")
            '    ddlExaminationBoard.SelectedValue = .Item(8)
            'End With
        Next

        Session("dtUnit") = dt



        gvSkill.DataSource = dt
        gvSkill.DataBind()
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                Dim ddlExaminationBoard As DropDownList
                ddlExaminationBoard = gvSkill.Rows(i).FindControl("ddlExaminationBoard")
                ddlExaminationBoard.SelectedValue = .Item(8)
            End With
        Next

    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_id"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_paper"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_date"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_dur"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_cost"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_doc"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_filename"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ES_Examintion_Level"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ES_EXL_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)



        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_BaseAmount"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_VAT"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "es_BoardAmount"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.Boolean")
        column.ColumnName = "es_ApprovalRequired"
        dt.Columns.Add(column)

        Return dt
    End Function


    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = ""

        Dim i As Integer

        Dim lblSksId As Label
        Dim txtSkill As TextBox
        Dim txtOrder As TextBox
        Dim txtShort As TextBox
        Dim txtdur As TextBox
        Dim txtcost As TextBox

        Dim txtBaseAmount As TextBox
        Dim txtVAT As TextBox
        Dim txtBoardAmount As TextBox
        Dim chkAppReq As CheckBox

        Dim lblDelete As Label
        Dim txtExamintionLevel As TextBox
        Dim ddlExaminationBoard As DropDownList
        Dim fd As FileUpload
        Dim mode As String = ""
        Dim filebyte As Byte() = Nothing
        Dim grade As String()
        Dim ck As Integer
        Dim FilePath As String = ""

        grade = ddlGrade.SelectedValue.Split("|")
        For i = 0 To gvSkill.Rows.Count - 1
            With gvSkill.Rows(i)
                lblSksId = .FindControl("lblSksId")
                txtSkill = .FindControl("txtSkill")
                txtOrder = .FindControl("txtOrder")
                lblDelete = .FindControl("lblDelete")
                txtShort = .FindControl("txtShort")
                txtdur = .FindControl("txtdur")
                txtcost = .FindControl("txtcost")

                txtBaseAmount = .FindControl("txtBaseAmount")
                txtVAT = .FindControl("txtVAT")
                txtBoardAmount = .FindControl("txtBoardAmount")
                chkAppReq = .FindControl("chkAppReq")

                txtExamintionLevel = .FindControl("txtExamintionLevel")
                ddlExaminationBoard = .FindControl("ddlExaminationBoard")
                fd = TryCast(DirectCast(.FindControl("fupload"), FileUpload), FileUpload)


                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblSksId.Text = "0" And txtSkill.Text <> "" Then
                    mode = "add"
                ElseIf txtSkill.Text <> "" Then
                    mode = "edit"
                Else
                    mode = ""
                End If
                ck = 0
                If mode <> "" Then


                    'filebyte = System.IO.File.ReadAllBytes("NC Level for reportcard.pdf")
                    ' End If
                    ' str_query = "exec dbo.saveExamSetup " _
                    '& " @ID=" + lblSksId.Text + "," _
                    '& " @ES_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                    '& " @ES_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                    '& " @ES_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'," _
                    '& " @ES_PAPER='" + txtSkill.Text + "'," _
                    '& " @ES_DATE='" + CDate(txtShort.Text) + "'," _
                    '& " @ES_DUR =" + (txtdur.Text) + "," _
                    '& " @ES_COST =" + (txtcost.Text) + "," _
                    '& " @ES_DOC=" + filebyte + "," _
                    '& " @MODE='" + mode + "'"


                    ' SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Dim cmd As New SqlCommand
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    cmd = New SqlCommand("dbo.saveExamSetup", objConn)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ID", lblSksId.Text)
                    cmd.Parameters.AddWithValue("@ES_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                    cmd.Parameters.AddWithValue("@ES_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@ES_SBG_ID", ddlSubject.SelectedValue.ToString)
                    cmd.Parameters.AddWithValue("@ES_PAPER", txtSkill.Text)
                    cmd.Parameters.AddWithValue("@ES_DATE", CDate(txtShort.Text))
                    cmd.Parameters.AddWithValue("@ES_DUR", txtdur.Text)
                    cmd.Parameters.AddWithValue("@ES_COST", txtcost.Text)

                    cmd.Parameters.AddWithValue("@es_BaseAmount", txtBaseAmount.Text)
                    cmd.Parameters.AddWithValue("@es_VAT", txtVAT.Text)
                    cmd.Parameters.AddWithValue("@es_BoardAmount", txtBoardAmount.Text)
                    cmd.Parameters.AddWithValue("@es_ApprovalRequired", chkAppReq.Checked)

                    cmd.Parameters.AddWithValue("@ES_Examintion_Level", txtExamintionLevel.Text)
                    cmd.Parameters.AddWithValue("@ES_EXL_ID", ddlExaminationBoard.SelectedValue)
                    If fd.HasFile Then
                        Dim p As SqlParameter
                        FilePath = Server.MapPath("~/Curriculum/ReportDownloads") & "/" & fd.FileName
                        fd.SaveAs(FilePath)
                        filebyte = System.IO.File.ReadAllBytes(FilePath)
                        ck = 1
                        p = cmd.Parameters.AddWithValue("@ES_DOC", filebyte)
                        'p.Value = SqlDbType.Binary
                        cmd.Parameters.AddWithValue("@es_filename", fd.FileName)
                        cmd.Parameters.AddWithValue("@content", fd.PostedFile.ContentType)

                    End If

                    cmd.Parameters.AddWithValue("@MODE", mode)

                    cmd.Parameters.AddWithValue("@type", ck)
                    cmd.Parameters.AddWithValue("@bSubject", chkSubj.Checked)
                    cmd.ExecuteNonQuery()

                    cmd.Dispose()
                    objConn.Close()
                    If File.Exists(FilePath) Then File.Delete(FilePath)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvSkill.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub gvSkill_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSkill.RowCommand


        Try
            If e.CommandName = "View" Then

                Dim i As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvSkill.Rows(i), GridViewRow)

                Dim lblId As New Label
                lblId = selectedRow.FindControl("lblSksId")
                Dim strQuery As String = "select es_fileName, es_Content, es_doc,ES_Examintion_Level from dbo.EXAMSETUP where es_id=" + lblId.Text

                Dim cmd As SqlCommand = New SqlCommand(strQuery)

                Dim dt As DataTable = GetData(cmd)

                If dt IsNot Nothing Then

                    download(dt)

                End If



            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("es_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("es_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("es_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub gvSkill_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvSkill.RowUpdating

    End Sub

    Protected Sub gvSkill_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSkill.RowDataBound
        Dim str_conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim img As ImageButton = e.Row.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.jpg"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.jpg"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.jpg"
                Else
                    img.ImageUrl = ""
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)

                Dim ddlExaminationBoard As DropDownList = e.Row.FindControl("ddlExaminationBoard")

                Dim ds As New DataSet
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Examintion_Level_BY_BUSID", param)
                ddlExaminationBoard.Items.Clear()
                ddlExaminationBoard.DataSource = ds.Tables(0)
                ddlExaminationBoard.DataTextField = "EXL_Title"
                ddlExaminationBoard.DataValueField = "EXL_ID"
                ddlExaminationBoard.DataBind()

                Dim li As New ListItem
                li.Text = "-Select-"
                li.Value = "0"
                ddlExaminationBoard.Items.Insert(0, li)

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            str_conn.Close()
        End Try


    End Sub

    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select es_fileName,es_Content,es_doc from dbo.EXAMSETUP where es_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub


    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select es_fileName,es_Content,es_doc from dbo.EXAMSETUP where es_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub

End Class
