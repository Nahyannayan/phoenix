﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml
Imports System.Drawing

Partial Class Curriculum_clmMarkEntry_DynamicAOL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    lblActivity.Text = Encr_decrData.Decrypt(Request.QueryString("activity").Replace(" ", "+"))
                    lblSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    lblGroup.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    lblDate.Text = Encr_decrData.Decrypt(Request.QueryString("date").Replace(" ", "+"))
                    hfCAS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("casid").Replace(" ", "+"))
                    hfMEntered.Value = Encr_decrData.Decrypt(Request.QueryString("menterd").Replace(" ", "+"))
                    hfEntryType.Value = Encr_decrData.Decrypt(Request.QueryString("entry").Replace(" ", "+"))
                    hfGradeSlab.Value = Encr_decrData.Decrypt(Request.QueryString("gradeslab").Replace(" ", "+"))
                    hfMinMarks.Value = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfMaxMarks.Value = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMaxmarks.Text = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMinmarks.Text = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfWithoutSkills.Value = Encr_decrData.Decrypt(Request.QueryString("ws").Replace(" ", "+"))
                    getSubject()
                    GridBind()
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                    '  tblact.Rows(3).Visible = False
                    '  tblact.Rows(4).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


#Region "Private methods"
    Sub getSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAS_SBG_ID,ISNULL(CAS_AOL_SKILL1_MAXMARK,0),ISNULL(CAS_AOL_SKILL2_MAXMARK,0),ISNULL(CAS_AOL_SKILL3_MAXMARK,0),ISNULL(CAS_AOL_SKILL4_MAXMARK,0),ISNULL(CAS_AOL_SKILL5_MAXMARK,0),ISNULL(CAS_AOL_SKILL6_MAXMARK,0),ISNULL(CAS_AOL_SKILL7_MAXMARK,0),ISNULL(CAS_AOL_SKILL8_MAXMARK,0),ISNULL(CAS_AOL_SKILL9_MAXMARK,0),ISNULL(CAS_AOL_SKILL10_MAXMARK,0),ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10) FROM ACT.ACTIVITY_SCHEDULE WHERE CAS_ID='" + hfCAS_ID.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        With ds.Tables(0).Rows(0)
            Session("SBGID") = .Item(0).ToString
            'hfKUMaxMarks.Value = Replace(.Item(1).ToString, ".0", "")
            'hfAPPLMaxMarks.Value = Replace(.Item(2).ToString, ".0", "")
            'hfCOMMMaxMarks.Value = Replace(.Item(3).ToString, ".0", "")
            'hfHOTSMaxMarks.Value = Replace(.Item(4).ToString, ".0", "")

            hfSkill1MaxMarks.Value = Replace(.Item(1).ToString, ".00", "")
            hfSkill2MaxMarks.Value = Replace(.Item(2).ToString, ".00", "")
            hfSkill3MaxMarks.Value = Replace(.Item(3).ToString, ".00", "")
            hfSkill4MaxMarks.Value = Replace(.Item(4).ToString, ".00", "")
            hfSkill5MaxMarks.Value = Replace(.Item(5).ToString, ".00", "")
            hfSkill6MaxMarks.Value = Replace(.Item(6).ToString, ".00", "")
            hfSkill7MaxMarks.Value = Replace(.Item(7).ToString, ".00", "")
            hfSkill8MaxMarks.Value = Replace(.Item(8).ToString, ".00", "")
            hfSkill9MaxMarks.Value = Replace(.Item(9).ToString, ".00", "")
            hfSkill10MaxMarks.Value = Replace(.Item(10).ToString, ".00", "")

            hfWSMaxMarks.Value = Replace(.Item(11).ToString, ".00", "")
        End With

    End Sub
    Sub getSubjectCaption()
        Dim lSkill1 As Label

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SKL_ID,SKL_DESCR FROM SKIL.SKILLS_M INNER JOIN ( " _
                                    & "SELECT     Split.a.value('.', 'NVARCHAR(200)') AS ID   " _
                                    & "FROM  (SELECT   " _
                                    & "CAST ('<M>' + REPLACE([CAS_SKL_ID], '|', '</M><M>') + '</M>' AS XML) AS String  " _
                                    & "FROM  ACT.ACTIVITY_SCHEDULE WHERE CAS_ID='" + hfCAS_ID.Value + "') AS A CROSS APPLY String.nodes ('/M') AS Split(a) " _
                                    & ")A ON SKL_ID=ID "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = ds.Tables(0)
        Dim CountSkills As Integer = dt.Rows.Count

        If (CountSkills > 0) Then
            For jj As Integer = 6 To (6 + CountSkills - 1)
                gvStud.Columns(jj).Visible = True
                'gvStud.Columns(jj).HeaderText = dt.Rows(jj - 6).Item(0).ToString
                CType(gvStud.HeaderRow.FindControl("lblSkill" + CType(jj - 5, String)), Label).Text = dt.Rows(jj - 6).Item(1).ToString
            Next
            For kk As Integer = (6 + CountSkills) To 15
                gvStud.Columns(kk).Visible = False
            Next
        Else
            For ll As Integer = 6 To 15
                gvStud.Columns(ll).Visible = False
            Next
        End If

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvStud.PageSize * gvStud.PageIndex)
    End Function
    Sub GridBind()
        ViewState("slno") = 0
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STA_ID,STU_ID,STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                 & " +' '+ISNULL(STU_LASTNAME,'')),STU_NO,CASE  WHEN STA_MARK IS NULL THEN '' " _
                                 & " ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STA_MARK) AS VARCHAR),'.00','') END AS MARKS,STA_GRADE," _
                                 & " MAXMARKS=" + hfMaxMarks.Value + ",MINMARKS=" + hfMinMarks.Value _
                                 & " ,ERR='Marks Should be between 0 and " + hfMaxMarks.Value + "'" _
                                 & " ,CASE  WHEN STA_bATTENDED='P' THEN 'TRUE' WHEN STA_bATTENDED IS NULL THEN 'TRUE' ELSE 'FALSE' END AS ENABLE, " _
                                 & " CASE  WHEN STS_SKILL1_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL1_M) AS VARCHAR),'.00','') END AS SKILL1_MARKS,STS_SKILL1_G," _
                                 & " CASE  WHEN STS_SKILL2_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL2_M) AS VARCHAR),'.00','') END AS SKILL2_MARKS,STS_SKILL2_G," _
                                 & " CASE  WHEN STS_SKILL3_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL3_M) AS VARCHAR),'.00','') END AS SKILL3_MARKS,STS_SKILL3_G," _
                                 & " CASE  WHEN STS_SKILL4_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL4_M) AS VARCHAR),'.00','') END AS SKILL4_MARKS,STS_SKILL4_G," _
                                 & " CASE  WHEN STS_SKILL5_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL5_M) AS VARCHAR),'.00','') END AS SKILL5_MARKS,STS_SKILL5_G," _
                                 & " CASE  WHEN STS_SKILL6_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL6_M) AS VARCHAR),'.00','') END AS SKILL6_MARKS,STS_SKILL6_G," _
                                 & " CASE  WHEN STS_SKILL7_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL7_M) AS VARCHAR),'.00','') END AS SKILL7_MARKS,STS_SKILL7_G," _
                                 & " CASE  WHEN STS_SKILL8_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL8_M) AS VARCHAR),'.00','') END AS SKILL8_MARKS,STS_SKILL8_G," _
                                 & " CASE  WHEN STS_SKILL9_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL9_M) AS VARCHAR),'.00','') END AS SKILL9_MARKS,STS_SKILL9_G," _
                                 & " CASE  WHEN STS_SKILL10_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_SKILL10_M) AS VARCHAR),'.00','') END AS SKILL10_MARKS,STS_SKILL10_G," _
                                 & " CASE  WHEN STS_WITHOUTSKILLS_M IS NULL THEN '' ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STS_WITHOUTSKILLS_M) AS VARCHAR),'.00','') END AS WITHOUTSKILLS_MARKS,STS_WITHOUTSKILLS_G," _
                                 & " STS_CHAPTER,STS_FEEDBACK,STS_WOT,STS_TARGET," _
                                 & " SKILL1_MAXMARKS=ISNULL(CAS_AOL_SKILL1_MAXMARK,0),SKILL2_MAXMARKS=ISNULL(CAS_AOL_SKILL2_MAXMARK,0),SKILL3_MAXMARKS=ISNULL(CAS_AOL_SKILL3_MAXMARK,0),SKILL4_MAXMARKS=ISNULL(CAS_AOL_SKILL4_MAXMARK,0),SKILL5_MAXMARKS=ISNULL(CAS_AOL_SKILL5_MAXMARK,0),SKILL6_MAXMARKS=ISNULL(CAS_AOL_SKILL6_MAXMARK,0),SKILL7_MAXMARKS=ISNULL(CAS_AOL_SKILL7_MAXMARK,0),SKILL8_MAXMARKS=ISNULL(CAS_AOL_SKILL8_MAXMARK,0),SKILL9_MAXMARKS=ISNULL(CAS_AOL_SKILL9_MAXMARK,0),SKILL10_MAXMARKS=ISNULL(CAS_AOL_SKILL10_MAXMARK,0),WITHOUTSKILLS_MAXMARKS=ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10)" _
                                 & " ,ERR_SKILL1='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL1_MAXMARK,0))" _
                                 & " ,ERR_SKILL2='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL2_MAXMARK,0))" _
                                 & " ,ERR_SKILL3='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL3_MAXMARK,0))" _
                                 & " ,ERR_SKILL4='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL4_MAXMARK,0))" _
                                 & " ,ERR_SKILL5='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL5_MAXMARK,0))" _
                                 & " ,ERR_SKILL6='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL6_MAXMARK,0))" _
                                 & " ,ERR_SKILL7='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL7_MAXMARK,0))" _
                                 & " ,ERR_SKILL8='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL8_MAXMARK,0))" _
                                 & " ,ERR_SKILL9='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL9_MAXMARK,0))" _
                                 & " ,ERR_SKILL10='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_SKILL10_MAXMARK,0))" _
                                 & " ,ERR_WITHOUTSKILLS='Marks Should be between 0 and ' + CONVERT(VARCHAR(100),ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK,10))" _
                                 & " ,STA_bATTENDED" _
                                 & ", CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END AS STU_STATUS" _
                                 & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B" _
                                 & " ON A.STU_ID=B.STA_STU_ID" _
                                 & " LEFT OUTER JOIN ACT.STUDENT_ACTIVITY_AOL AS C ON B.STA_ID=C.STS_STA_ID" _
                                 & " LEFT OUTER JOIN ACT.ACTIVITY_SCHEDULE AS D ON B.STA_CAS_ID=D.CAS_ID" _
                                 & " WHERE STA_CAS_ID=" + hfCAS_ID.Value _
                                 & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()


        'Dim lKUMarks As Label

        'lKUMarks = gvStud.HeaderRow.FindControl("lKUMarks")
        'If hfKUMaxMarks.Value <> "0" Then
        '    lKUMarks.Text = "(" + hfKUMaxMarks.Value + ")"
        'End If


        'Dim lAPPLMarks As Label

        'lAPPLMarks = gvStud.HeaderRow.FindControl("lAPPLMarks")
        'If hfAPPLMaxMarks.Value <> "0" Then
        '    lAPPLMarks.Text = "(" + hfAPPLMaxMarks.Value + ")"
        'End If


        'Dim lCOMMMarks As Label

        'lCOMMMarks = gvStud.HeaderRow.FindControl("lCOMMMarks")
        'If hfCOMMMaxMarks.Value <> "0" Then
        '    lCOMMMarks.Text = "(" + hfCOMMMaxMarks.Value + ")"
        'End If

        'Dim lHOTSMarks As Label

        'lHOTSMarks = gvStud.HeaderRow.FindControl("lHOTSMarks")
        'If hfHOTSMaxMarks.Value <> "0" Then
        '    lHOTSMarks.Text = "(" + hfHOTSMaxMarks.Value + ")"
        'End If
        getSubjectCaption()

        Dim lSkill1Marks As Label
        Dim lSkill2Marks As Label
        Dim lSkill3Marks As Label
        Dim lSkill4Marks As Label
        Dim lSkill5Marks As Label
        Dim lSkill6Marks As Label
        Dim lSkill7Marks As Label
        Dim lSkill8Marks As Label
        Dim lSkill9Marks As Label
        Dim lSkill10Marks As Label

        lSkill1Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark1")
        If hfSkill1MaxMarks.Value <> "0" Then
            lSkill1Marks.Text = "(" + hfSkill1MaxMarks.Value + ")"
        Else
            gvStud.Columns(6).Visible = False
        End If

        lSkill2Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark2")
        If hfSkill2MaxMarks.Value <> "0" Then
            lSkill2Marks.Text = "(" + hfSkill2MaxMarks.Value + ")"
        Else
            gvStud.Columns(7).Visible = False
        End If

        lSkill3Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark3")
        If hfSkill3MaxMarks.Value <> "0" Then
            lSkill3Marks.Text = "(" + hfSkill3MaxMarks.Value + ")"
        Else
            gvStud.Columns(8).Visible = False
        End If

        lSkill4Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark4")
        If hfSkill4MaxMarks.Value <> "0" Then
            lSkill4Marks.Text = "(" + hfSkill4MaxMarks.Value + ")"
        Else
            gvStud.Columns(9).Visible = False
        End If

        lSkill5Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark5")
        If hfSkill5MaxMarks.Value <> "0" Then
            lSkill5Marks.Text = "(" + hfSkill5MaxMarks.Value + ")"
        Else
            gvStud.Columns(10).Visible = False
        End If

        lSkill6Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark6")
        If hfSkill6MaxMarks.Value <> "0" Then
            lSkill6Marks.Text = "(" + hfSkill6MaxMarks.Value + ")"
        Else
            gvStud.Columns(11).Visible = False
        End If

        lSkill7Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark7")
        If hfSkill7MaxMarks.Value <> "0" Then
            lSkill7Marks.Text = "(" + hfSkill7MaxMarks.Value + ")"
        Else
            gvStud.Columns(12).Visible = False
        End If

        lSkill8Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark8")
        If hfSkill8MaxMarks.Value <> "0" Then
            lSkill8Marks.Text = "(" + hfSkill8MaxMarks.Value + ")"
        Else
            gvStud.Columns(13).Visible = False
        End If

        lSkill9Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark9")
        If hfSkill9MaxMarks.Value <> "0" Then
            lSkill9Marks.Text = "(" + hfSkill9MaxMarks.Value + ")"
        Else
            gvStud.Columns(14).Visible = False
        End If

        lSkill10Marks = gvStud.HeaderRow.FindControl("lblSkillMaxMark10")
        If hfSkill10MaxMarks.Value <> "0" Then
            lSkill10Marks.Text = "(" + hfSkill10MaxMarks.Value + ")"
        Else
            gvStud.Columns(15).Visible = False
        End If


        Dim lWSMarks As Label

        lWSMarks = gvStud.HeaderRow.FindControl("lWSMarks")
        If hfWSMaxMarks.Value <> "0" Then
            lWSMarks.Text = "(" + hfWSMaxMarks.Value + ")"
        End If

        If hfWithoutSkills.Value.ToLower = "true" Then
            gvStud.Columns(6).Visible = False
            gvStud.Columns(7).Visible = False
            gvStud.Columns(8).Visible = False
            gvStud.Columns(9).Visible = False
            gvStud.Columns(10).Visible = False
            gvStud.Columns(11).Visible = False
            gvStud.Columns(12).Visible = False
            gvStud.Columns(13).Visible = False
            gvStud.Columns(14).Visible = False
            gvStud.Columns(15).Visible = False
        Else
            gvStud.Columns(16).Visible = False
        End If




    End Sub


    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim lblStaId As Label
        'Dim txtKUMarks As TextBox
        'Dim txtAPPMarks As TextBox
        'Dim txtCOMMMarks As TextBox
        'Dim txtHOTSMarks As TextBox

        Dim txtSkill1Marks As TextBox
        Dim txtSkill2Marks As TextBox
        Dim txtSkill3Marks As TextBox
        Dim txtSkill4Marks As TextBox
        Dim txtSkill5Marks As TextBox
        Dim txtSkill6Marks As TextBox
        Dim txtSkill7Marks As TextBox
        Dim txtSkill8Marks As TextBox
        Dim txtSkill9Marks As TextBox
        Dim txtSkill10Marks As TextBox

        Dim txtWSMarks As TextBox
        Dim txtChapter As TextBox
        Dim txtFeedBack As TextBox
        Dim txtWOT As TextBox
        Dim txtTarget As TextBox


        Dim grade As String
        Dim ddlGrade As DropDownList
        Dim cmd As SqlCommand
        Dim AuditID As Int32

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                cmd = New SqlCommand("[ACT].[SaveAUDITDATA]", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 50)
                sqlpBSU_ID.Value = Session("SBsuid")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDOCTYPE As New SqlParameter("@AUD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpDOCNO As New SqlParameter("@AUD_DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpPROCEDURE As New SqlParameter("@AUD_PROCEDURE", SqlDbType.VarChar, 200)
                sqlpPROCEDURE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpPROCEDURE)

                Dim sqlpLOGDT As New SqlParameter("@AUD_LOGDT", SqlDbType.DateTime)
                sqlpLOGDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpLOGDT)

                Dim sqlpACTION As New SqlParameter("@AUD_ACTION", SqlDbType.VarChar, 50)
                sqlpACTION.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpACTION)

                Dim sqlpDT As New SqlParameter("@AUD_DT", SqlDbType.DateTime)
                sqlpDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpDT)

                Dim sqlpUSER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
                sqlpUSER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpUSER)

                Dim sqlpREMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 200)
                sqlpREMARKS.Value = "Marks"
                cmd.Parameters.Add(sqlpREMARKS)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                AuditID = retValParam.Value
                If AuditID <> 0 Then
                    For i = 0 To gvStud.Rows.Count - 1

                        lblStaId = gvStud.Rows(i).FindControl("lblStaId")

                        'txtKUMarks = gvStud.Rows(i).FindControl("txtKUMarks")
                        'txtAPPMarks = gvStud.Rows(i).FindControl("txtAPPMarks")
                        'txtCOMMMarks = gvStud.Rows(i).FindControl("txtCOMMMarks")
                        'txtHOTSMarks = gvStud.Rows(i).FindControl("txtHOTSMarks")

                        txtSkill1Marks = gvStud.Rows(i).FindControl("txtSkill1Marks")
                        txtSkill2Marks = gvStud.Rows(i).FindControl("txtSkill2Marks")
                        txtSkill3Marks = gvStud.Rows(i).FindControl("txtSkill3Marks")
                        txtSkill4Marks = gvStud.Rows(i).FindControl("txtSkill4Marks")
                        txtSkill5Marks = gvStud.Rows(i).FindControl("txtSkill5Marks")
                        txtSkill6Marks = gvStud.Rows(i).FindControl("txtSkill6Marks")
                        txtSkill7Marks = gvStud.Rows(i).FindControl("txtSkill7Marks")
                        txtSkill8Marks = gvStud.Rows(i).FindControl("txtSkill8Marks")
                        txtSkill9Marks = gvStud.Rows(i).FindControl("txtSkill9Marks")
                        txtSkill10Marks = gvStud.Rows(i).FindControl("txtSkill10Marks")





                        txtWSMarks = gvStud.Rows(i).FindControl("txtWSMarks")

                        txtChapter = gvStud.Rows(i).FindControl("txtChapter")
                        txtFeedBack = gvStud.Rows(i).FindControl("txtFeedBack")
                        txtWOT = gvStud.Rows(i).FindControl("txtWOTs")
                        txtTarget = gvStud.Rows(i).FindControl("txtTarget")


                        ddlGrade = gvStud.Rows(i).FindControl("ddlGrade")

                        'If txtKUMarks.Enabled = True Then
                        '    str_query = "ACT.saveMARKENTRY_AOL " _
                        '                   & lblStaId.Text + "," _
                        '                   & IIf(txtKUMarks.Text = "", "NULL", txtKUMarks.Text.ToString) + "," _
                        '                   & IIf(txtAPPMarks.Text = "", "NULL", txtAPPMarks.Text.ToString) + "," _
                        '                   & IIf(txtCOMMMarks.Text = "", "NULL", txtCOMMMarks.Text.ToString) + "," _
                        '                   & IIf(txtHOTSMarks.Text = "", "NULL", txtHOTSMarks.Text.ToString) + "," _
                        '                   & IIf(txtWSMarks.Text = "", "NULL", txtWSMarks.Text.ToString) + "," _
                        '                   & "'" + txtChapter.Text.Replace("'", "''") + "'," _
                        '                   & "'" + txtFeedBack.Text.Replace("'", "''") + "'," _
                        '                   & "'" + txtWOT.Text.Replace("'", "''") + "'," _
                        '                   & "'" + txtTarget.Text.Replace("'", "''") + "'," _
                        '                   & "'" + hfWithoutSkills.Value + "'," _
                        '                   & CStr(AuditID) + "," _
                        '                   & "'" + Session("SUSR_nAME") + "'"

                        '    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        'End If

                        If txtSkill1Marks.Enabled = True Then
                            str_query = "ACT.saveMARKENTRY_AOL_Dynamic " _
                                           & lblStaId.Text + "," _
                                           & IIf(txtSkill1Marks.Text = "", "0", txtSkill1Marks.Text.ToString) + "," _
                                           & IIf(txtSkill2Marks.Text = "", "0", txtSkill2Marks.Text.ToString) + "," _
                                           & IIf(txtSkill3Marks.Text = "", "0", txtSkill3Marks.Text.ToString) + "," _
                                           & IIf(txtSkill4Marks.Text = "", "0", txtSkill4Marks.Text.ToString) + "," _
                                           & IIf(txtSkill5Marks.Text = "", "0", txtSkill5Marks.Text.ToString) + "," _
                                           & IIf(txtSkill6Marks.Text = "", "0", txtSkill6Marks.Text.ToString) + "," _
                                           & IIf(txtSkill7Marks.Text = "", "0", txtSkill7Marks.Text.ToString) + "," _
                                           & IIf(txtSkill8Marks.Text = "", "0", txtSkill8Marks.Text.ToString) + "," _
                                           & IIf(txtSkill9Marks.Text = "", "0", txtSkill9Marks.Text.ToString) + "," _
                                           & IIf(txtSkill10Marks.Text = "", "0", txtSkill10Marks.Text.ToString) + "," _
                                           & IIf(txtWSMarks.Text = "", "NULL", txtWSMarks.Text.ToString) + "," _
                                           & "'" + txtChapter.Text.Replace("'", "''") + "'," _
                                           & "'" + txtFeedBack.Text.Replace("'", "''") + "'," _
                                           & "'" + txtWOT.Text.Replace("'", "''") + "'," _
                                           & "'" + txtTarget.Text.Replace("'", "''") + "'," _
                                           & "'" + hfWithoutSkills.Value + "'," _
                                           & CStr(AuditID) + "," _
                                           & "'" + Session("SUSR_nAME") + "'"

                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If

                        lblError.Text = "Record saved successfully"

                    Next
                End If
                str_query = "ACT.updateBMARKENTERD " + hfCAS_ID.Value
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub


    Function checkPublishActiviyEnable(ByVal stu_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
                                 & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS B ON A.STA_CAS_ID=B.CAS_ID" _
                                 & " INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON B.CAS_CAD_ID=C.RRD_CAD_ID" _
                                 & " INNER JOIN RPT.REPORT_RULE_M AS E ON C.RRD_RRM_ID=E.RRM_ID" _
                                 & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS F ON A.STA_STU_ID=F.RPP_STU_ID " _
                                 & " AND F.RPP_RPF_ID=E.RRM_RPF_ID AND F.RPP_TRM_ID=E.RRM_TRM_ID" _
                                 & " AND F.RPP_ACD_ID=E.RRM_ACD_ID AND F.RPP_GRD_ID=E.RRM_GRD_ID" _
                                 & " WHERE RPP_bPUBLISH='TRUE' AND RPP_STU_ID=" + stu_id _
                                 & " AND STA_CAS_ID=" + hfCAS_ID.Value
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub CopyHeader(ByVal header As String)
        Dim txt As TextBox
        Dim i As Integer
        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)
                txt = .FindControl(header)
                txt.Text = txtHeader.Text
            End With
        Next
    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        'Select Case ddlHeader.SelectedValue
        '    Case "CH"
        '        CopyHeader("txtChapter")
        '    Case "FB"
        '        CopyHeader("txtFeedBack")
        '    Case "WOT"
        '        CopyHeader("txtWOT")
        '    Case "TG"
        '        CopyHeader("txtTarget")
        'End Select
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim imgBtn As ImageButton
            Dim lblStuId As Label
            Dim lblAtt As Label
            Dim lblStatus As Label

            'Dim txtKUMarks As TextBox
            'Dim txtAPPMarks As TextBox
            'Dim txtCOMMMarks As TextBox
            'Dim txtHOTSMarks As TextBox

            Dim txtSkill1Marks As TextBox
            Dim txtSkill2Marks As TextBox
            Dim txtSkill3Marks As TextBox
            Dim txtSkill4Marks As TextBox
            Dim txtSkill5Marks As TextBox
            Dim txtSkill6Marks As TextBox
            Dim txtSkill7Marks As TextBox
            Dim txtSkill8Marks As TextBox
            Dim txtSkill9Marks As TextBox
            Dim txtSkill10Marks As TextBox

            Dim txtWSMarks As TextBox
            lblStatus = e.Row.FindControl("lblStatus")

            If lblStatus.Text <> "" Then
                lblStatus.ForeColor = Drawing.Color.Red
            End If

            Dim txtFeedBack As TextBox
            txtFeedBack = e.Row.FindControl("txtFeedBack")

            Dim txtChapter As TextBox
            txtChapter = e.Row.FindControl("txtChapter")

            ClientScript.RegisterArrayDeclaration("txtCH", String.Concat("'", txtChapter.ClientID, "'"))
            ClientScript.RegisterArrayDeclaration("txtFB", String.Concat("'", txtFeedBack.ClientID, "'"))

            lblStuId = e.Row.FindControl("lblStuId")
            imgBtn = e.Row.FindControl("imgBtn")
            lblAtt = e.Row.FindControl("lblAtt")

            'txtKUMarks = e.Row.FindControl("txtKUMarks")
            'txtAPPMarks = e.Row.FindControl("txtAPPMarks")
            'txtCOMMMarks = e.Row.FindControl("txtCOMMMarks")
            'txtHOTSMarks = e.Row.FindControl("txtHOTSMarks")
            txtSkill1Marks = e.Row.FindControl("txtSkill1Marks")
            txtSkill2Marks = e.Row.FindControl("txtSkill2Marks")
            txtSkill3Marks = e.Row.FindControl("txtSkill3Marks")
            txtSkill4Marks = e.Row.FindControl("txtSkill4Marks")
            txtSkill5Marks = e.Row.FindControl("txtSkill5Marks")
            txtSkill6Marks = e.Row.FindControl("txtSkill6Marks")
            txtSkill7Marks = e.Row.FindControl("txtSkill7Marks")
            txtSkill8Marks = e.Row.FindControl("txtSkill8Marks")
            txtSkill9Marks = e.Row.FindControl("txtSkill9Marks")
            txtSkill10Marks = e.Row.FindControl("txtSkill10Marks")

            txtWSMarks = e.Row.FindControl("txtWSMarks")

            If lblAtt.Text.ToLower = "a" Or lblAtt.Text.ToLower = "l" Then
                'txtKUMarks.BackColor = Drawing.Color.Gray
                'txtAPPMarks.BackColor = Drawing.Color.Gray
                'txtCOMMMarks.BackColor = Drawing.Color.Gray
                'txtHOTSMarks.BackColor = Drawing.Color.Gray

                txtSkill1Marks.BackColor = Drawing.Color.Gray
                txtSkill2Marks.BackColor = Drawing.Color.Gray
                txtSkill3Marks.BackColor = Drawing.Color.Gray
                txtSkill4Marks.BackColor = Drawing.Color.Gray
                txtSkill5Marks.BackColor = Drawing.Color.Gray
                txtSkill6Marks.BackColor = Drawing.Color.Gray
                txtSkill7Marks.BackColor = Drawing.Color.Gray
                txtSkill8Marks.BackColor = Drawing.Color.Gray
                txtSkill9Marks.BackColor = Drawing.Color.Gray
                txtSkill10Marks.BackColor = Drawing.Color.Gray


                txtWSMarks.BackColor = Drawing.Color.Gray

                'txtKUMarks.ToolTip = "Absent"
                'txtAPPMarks.ToolTip = "Absent"
                'txtCOMMMarks.ToolTip = "Absent"
                'txtHOTSMarks.ToolTip = "Absent"

                txtSkill1Marks.ToolTip = "Absent"
                txtSkill2Marks.ToolTip = "Absent"
                txtSkill3Marks.ToolTip = "Absent"
                txtSkill4Marks.ToolTip = "Absent"
                txtSkill5Marks.ToolTip = "Absent"
                txtSkill6Marks.ToolTip = "Absent"
                txtSkill7Marks.ToolTip = "Absent"
                txtSkill8Marks.ToolTip = "Absent"
                txtSkill9Marks.ToolTip = "Absent"
                txtSkill10Marks.ToolTip = "Absent"


                txtWSMarks.ToolTip = "Absent"
            End If


            If Not imgBtn Is Nothing Then
                imgBtn.OnClientClick = "javascript:getcomments('" + txtFeedBack.ClientID + "','ALLCMTS','AOL_CMTS','" + lblStuId.Text + "','0'); return false;"
            End If

            Try

                Dim bt As Boolean = checkPublishActiviyEnable(lblStuId.Text)
                If bt = False Then
                    e.Row.BackColor = Color.FromName("#DEFFB4") 'Drawing.Color.Coral
                    txtSkill1Marks.Enabled = False
                    txtSkill2Marks.Enabled = False
                    txtSkill3Marks.Enabled = False
                    txtSkill4Marks.Enabled = False
                    txtSkill5Marks.Enabled = False
                    txtSkill6Marks.Enabled = False
                    txtSkill7Marks.Enabled = False
                    txtSkill8Marks.Enabled = False
                    txtSkill9Marks.Enabled = False
                    txtSkill10Marks.Enabled = False
                    txtChapter.Enabled = False
                    txtWSMarks.Enabled = False
                End If
            Catch ex As Exception
            End Try

        End If
    End Sub
End Class
