Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmATTEntry_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    lblActivity.Text = Encr_decrData.Decrypt(Request.QueryString("activity").Replace(" ", "+"))
                    lblSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    lblGroup.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    lblDate.Text = Encr_decrData.Decrypt(Request.QueryString("date").Replace(" ", "+"))
                    hfCAS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("casid").Replace(" ", "+"))
                    hfMarkUrl.Value = Encr_decrData.Decrypt(Request.QueryString("markurl").Replace(" ", "+"))

                    GridBind()

                    Dim attent As String = Encr_decrData.Decrypt(Request.QueryString("attent").Replace(" ", "+"))
                    If attent.ToLower <> "yes" And attent.ToLower <> "" Then
                        btnMark.Enabled = False
                        btnMark1.Enabled = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STA_ID,STU_ID,STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                 & " +' '+ISNULL(STU_LASTNAME,'')), STU_NO,isnull(STA_bATTENDED,'P') AS ATT," _
                                 & " CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END AS STU_STATUS" _
                                 & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B" _
                                 & " ON A.STU_ID=B.STA_STU_ID" _
                                 & " WHERE STA_CAS_ID=" + hfCAS_ID.Value _
                                 & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub

    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim lblStaId As Label
        Dim ddlAttendance As DropDownList
        Dim transaction As SqlTransaction
        Dim att As String
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvStud.Rows.Count - 1

                    lblStaId = gvStud.Rows(i).FindControl("lblStaId")
                    ddlAttendance = gvStud.Rows(i).FindControl("ddlAttendance")
                    'If chkSelect.Enabled = False Then
                    '    att = "L"
                    'ElseIf chkSelect.Checked = True Then
                    '    att = "P"
                    'ElseIf chkSelect.Checked = False Then
                    '    att = "A"
                    'End If
                    str_query = "ACT.saveATTENDANCEENTRY " _
                               & lblStaId.Text + "," _
                               & "'" + ddlAttendance.SelectedValue.ToString + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    lblError.Text = "Record saved successfully"
                Next

                str_query = "ACT.updateBATTENDED " + hfCAS_ID.Value
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                Transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        btnMark.Enabled = True
        btnMark1.Enabled = True
    End Sub

    Protected Sub btnMark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMark.Click
        Try
            Response.Redirect(hfMarkUrl.Value)
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        btnMark.Enabled = True
        btnMark1.Enabled = True
    End Sub

    Protected Sub btnMark1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMark1.Click
        Try
            Response.Redirect(hfMarkUrl.Value)
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
