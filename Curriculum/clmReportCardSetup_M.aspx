<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportCardSetup_M.aspx.vb" Inherits="Curriculum_clmReportCardSetup_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Report Card Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr >
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="8">

                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4" align="center" class="matters">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <%--<tr class="subheader_img">
                                                <td align="left" valign="middle" colspan="3">
                                                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
REPORT CARD SETUP</span></font></td>
                                            </tr>--%>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Report Card</span></td>
                                             
                                                <td align="left" >
                                                    <asp:Label ID="lblReportCard" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>

                                                 <td align="left" colspan="2" ></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" ></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center" >

                                                    <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%">
                                                        <RowStyle CssClass="griditem" />
                                                        <EmptyDataRowStyle  />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAllSubj" runat="server" Text='<%# Bind("ALLSUB") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="Header">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Set Direct Enrty">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle Wrap="False" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" Checked='<%# Bind("DIRECT") %>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Set Default Values">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblDefault" OnClick="lblDefault_Click" Enabled='<%# Bind("DIRECT") %>' runat="server">Set Default Values</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                        <SelectedRowStyle  />
                                                        <HeaderStyle  />
                                                        <EditRowStyle  />
                                                        <AlternatingRowStyle  CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4" ></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4" >
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="7" Text="Save"
                                                        ValidationGroup="groupM1" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                            CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False" /></td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />

                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>

                </table>


            </div>
        </div>
    </div>


</asp:Content>

