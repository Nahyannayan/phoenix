<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmSMARTTargetSetup_Preview.aspx.vb"
    Inherits="Curriculum_clmSMARTTargetSetup_Preview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SMART Target Setup Preview</title>
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%">
                <tr>
                    <td align="center">
                        <asp:DataList ID="dlQuestions" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                            ShowHeader="False">
                            <ItemTemplate>
                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblStar" runat="server" Text='*' ForeColor="Red"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblSerialNo" CssClass="field-label"  runat="server" Text='<%# BIND("TGD_SLNO") %>'></asp:Label>.
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblQuestion" CssClass="field-label" runat="server" Text='<%# BIND("TGD_DESCR") %>'
                                                ></asp:Label>

                                            <asp:TextBox ID="txtTargetScore" runat="server" Width="50px"></asp:TextBox>
                                            <asp:Label ID="lblTgdId" runat="server" Text='<%# BIND("TGD_ID") %>' Visible="false" ></asp:Label>
                                            <asp:Label ID="lblSbgId" runat="server" Text='<%# BIND("TGD_SBG_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblTextType" runat="server" Text='<%# Bind("TGD_TEXTTYPE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblOption" runat="server" Text='<%# Bind("TGD_OPTIONS") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblMinAnswer" runat="server" Text='<%# Bind("TGD_MINANSWER") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPretext" runat="server" Text='<%# Bind("TGD_PRETEXT") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblTarget" runat="server" Text='<%# Bind("TGD_bTARGET") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblMandatory" runat="server" Text='<%# Bind("TGD_bMANDATORY") %>'
                                                Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td valign="top" align="left">
                                            <asp:GridView ID="gvOptions" runat="server" CssClass="table table-bordered" AllowPaging="false" AutoGenerateColumns="False"
                                                 OnRowDataBound="gvOptions_RowDataBound">
                                                <RowStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle BorderStyle="None" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSlno" Text='<%# Bind("SLNO") %>' runat="server"></asp:Label>
                                                            <br />
                                                                        <asp:Label ID="lblPreText" Text='<%# Bind("PRETEXT") %>' CssClass="field-label"  runat="server"></asp:Label><br />
                                                                        <asp:Label ID="lblType" Text='<%# Bind("TEXTTYPE") %>' Visible="false" runat="server"></asp:Label>
                                                                    <br />
                                                                        <asp:TextBox ID="txtAnswer"  runat="server" Width="75%"></asp:TextBox>
                                                                    
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" BorderStyle="None" BorderWidth="0px"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle Wrap="False" Height="0px" BorderStyle="None" />
                                                <EditRowStyle Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:HiddenField ID="hfTGM_ID" runat="server" />
                        <asp:HiddenField ID="hfGRD_ID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                            TabIndex="7" />
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="groupM1"
                            TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
