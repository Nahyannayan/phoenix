Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Partial Class Curriculum_clmProcessPromotion
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

      
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If ViewState("MainMnu_code") = "C330061" Then
                    ltLabel.Text = "Process Rank"
                Else
                    ltLabel.Text = "Process Promotion"
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330060" And ViewState("MainMnu_code") <> "C330061") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindSection()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
      
        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid

        If ViewState("MainMnu_code") <> "C330061" Then
            If Session("sbsuid") <> "125005" And Session("sbsuid") <> "165010" And Session("sbsuid") <> "125010" And Session("sbsuid") <> "135010" And Session("sbsuid") <> "125018" And Session("sbsuid") <> "125002" And Session("sbsuid") <> "126008" And Session("sbsuid") <> "125011" And Session("sbsuid") <> "115002" Then
                If Session("sbsuid") = "151001" And Session("clm") <> 1 Then
                Else
                    str_query += "  and grm_grd_id<>'10'"
                End If
            End If
        End If

        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub BindSection()
        Dim li As New ListItem
        lstSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSection.DataSource = ds
        lstSection.DataTextField = "SCT_DESCR"
        lstSection.DataValueField = "SCT_ID"
        lstSection.DataBind()


        Dim sct_ids As String
        Dim i As Integer
        For i = 0 To lstSection.Items.Count - 1
            If sct_ids <> "" Then
                sct_ids += ","
            End If
            sct_ids += lstSection.Items(i).Value
        Next


       
    End Sub

    Sub SaveData()
           Dim str_query As String

        Dim i As Integer
        Dim transaction As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim j As Integer
       
        ' Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                ' transaction = conn.BeginTransaction("SampleTransaction")
                'Try

                Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")

                Select Case Session("SBSUID")
                    Case "125018"
                        str_query = "exec [RPT].[PROCESS_BROWNBOOK] " _
                                             & "'" + Session("sbsuid") + "'," _
                                             & lstSection.Items(i).Value + "," _
                                             & ddlAcademicYear.SelectedValue.ToString + "," _
                                             & "'" + grade(0) + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Case "125005", "125002", "125011", "125010", "125012", "115011", "165010"

                        str_query = "exec [RPT].[PROCESS_BROWNBOOK] " _
                       & "'" + Session("sbsuid") + "'," _
                       & lstSection.Items(i).Value + "," _
                       & ddlAcademicYear.SelectedValue.ToString + "," _
                       & "'" + grade(0) + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


                        str_query = "exec [RPT].[PROCESS_PROMOTION_SECTION] " _
                        & "'" + Session("sbsuid") + "'," _
                        & Session("clm") + "," _
                        & lstSection.Items(i).Value + "," _
                        & "'" + Format(Now.Date, "yyyy-MM-dd") + "'," _
                        & "'" + Session("susr_name") + "'," _
                        & ddlAcademicYear.SelectedValue.ToString + "," _
                        & "'" + grade(0) + "'"

                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Case "125010"

                        If grade(0) = "01" Or grade(0) = "02" Or grade(0) = "03" Or grade(0) = "04" Or grade(0) = "05" Or grade(0) = "06" Then
                            str_query = "exec [RPT].[PROCESS_BROWNBOOK] " _
                                         & "'" + Session("sbsuid") + "'," _
                                         & lstSection.Items(i).Value + "," _
                                         & ddlAcademicYear.SelectedValue.ToString + "," _
                                         & "'" + grade(0) + "'"
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                        End If

                        str_query = "exec [RPT].[PROCESS_PROMOTION_SECTION] " _
                                   & "'" + Session("sbsuid") + "'," _
                                   & Session("clm") + "," _
                                   & lstSection.Items(i).Value + "," _
                                   & "'" + Format(Now.Date, "yyyy-MM-dd") + "'," _
                                   & "'" + Session("susr_name") + "'," _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'"

                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                    Case Else
                        str_query = "exec [RPT].[PROCESS_PROMOTION_SECTION] " _
                        & "'" + Session("sbsuid") + "'," _
                        & Session("clm") + "," _
                        & lstSection.Items(i).Value + "," _
                        & "'" + Format(Now.Date, "yyyy-MM-dd") + "'," _
                        & "'" + Session("susr_name") + "'," _
                        & ddlAcademicYear.SelectedValue.ToString + "," _
                        & "'" + grade(0) + "'"

                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End Select


                'transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                j += 1
                'Catch myex As ArgumentException
                '    transaction.Rollback()
                '    lblError.Text = myex.Message
                '    lblError.Text = "Record could not be processed"
                'Catch ex As Exception
                '    transaction.Rollback()
                '    lblError.Text = "Record could not be processed"
                'End Try

            End If
        Next
        '  End Using

    End Sub

    Sub ProcessRank()

        Dim strSections As String = ""
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If strSections <> "" Then
                    strSections += "|"
                End If
                strSections += lstSection.Items(i).Value
            End If
        Next

        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        str_query = "exec  [RPT].[PROCESS_FINAL_STUDENT_RANK] " _
                     & ddlAcademicYear.SelectedValue.ToString + "," _
                     & "'" + strSections + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        lblError.Text = "Record Saved Successfully"
    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
       
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()

    End Sub

 

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click

       
        Dim i As Integer
 
        Dim strSections As String = ""
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If strSections <> "" Then
                    strSections += "|"
                End If
                strSections += lstSection.Items(i).Value
            End If
        Next

        If strSections = "" Then
            lblError.Text = "Please select a section"
            Exit Sub
        End If
        If ViewState("MainMnu_code") = "C330061" Then
            ProcessRank()
        Else
            SaveData()
        End If

    End Sub

   
End Class
