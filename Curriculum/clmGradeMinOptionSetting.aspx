<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="clmGradeMinOptionSetting.aspx.vb" Inherits="Curriculum_clmGradeMinOptionSetting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Grade Option Setting
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                Style="text-align: left" ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table id="tbPromote" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label"> Academic Year</span></td>
                                    <td align="left" class="matters" width="20%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="left" class="matters"></td>
                                </tr>
                                <tr>


                                    <td align="center" class="matters" colspan="4" valign="top">

                                        <asp:GridView ID="gvGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%" AllowSorting="True">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="TRD_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRM_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TRD_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStmId" runat="server" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stream">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Min Options">
                                                    <ItemTemplate>
                                                                    <asp:Label ID="lbloPT" Text='<%# Bind("gro_options") %>' runat="server"></asp:Label><br />
                                                                    <asp:TextBox ID="txtOpt" runat="server" Visible="false"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                    <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>
                </table>

                <script type="text/javascript">


                    cssdropdown.startchrome("Div1")
                    cssdropdown.startchrome("Div2")

                </script>
            </div>
        </div>
    </div>

</asp:Content>

