<%@ Page Language="VB" AutoEventWireup="false" CodeFile="noAccess.aspx.vb" Inherits="login" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon"> 
    <title>Login Page</title>


    <!-- Bootstrap core JavaScript-->
                <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
                <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
                <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
              
    
    <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


     <script language="javascript" type="text/javascript">
        //not in use
    /*  function validate_add() 
      {     
   
      if (document.getElementById("txtUsername").value=='')
      {     
       document.getElementById("label1").value=='Kindly enter user Name';
      alert("Kindly enter User Name");

      return false;
      }
      else if (document.getElementById("txtPassword").value=='' )     
     {
      alert("Kindly enter valid password");
      return false;
      }
      else
      {
      var pwd=document.getElementById("txtPassword").value;
      if( pwd.length<2)
          {
          
        alert("Kindly enter valid password");
          return false;
          }
      }

       return true;

    }  */
    </script>

    <style>
        .cover-top-radius{
        border-top-left-radius:10px;
        border-top-right-radius:10px;
        }
    </style>
</head>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled fixed-nav" id="page-top" >
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div>

            <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
                <a class="navbar-brand">
                    <img src="/PHOENIXBETA/images/gems-education-logo.png" class="margin-neg" /></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarResponsive">

                     

                    <ul class="navbar-nav sidenav-toggler">

                        <li class="nav-item">
                            <a class="nav-link text-center" id="sidenavToggler">
                                <i class="fa fa-fw fa-angle-left"></i>
                            </a>
                        </li>
                    </ul>

                    




                </div>


            </nav>


        <!-- main bootstrap body stats here -->
            <div class="content-wrapper mb-5">
                <div class="container-fluid">
                 <p style="top: 70px;right: 15px;color: rgb(51, 51, 51);font-size: 12px;font-weight: 500;margin-top: 10px;position: absolute;">Logged into : <span id="spBsuName" runat="server"></span></p>
                    <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/PHOENIXBETA/homepage.aspx">Home</a>
        </li>
        <li class="breadcrumb-item active"><asp:Label ID="lblFilePath" runat="server"></asp:Label></li>
      </ol>


        <div class="card card-login mx-auto text-center mb-5">
                    <div class="card-header-pills badge-danger text-capitalize cover-top-radius font-weight-bold">Access Denied</div>
                    <div class="card-body">
                      <p> Your access to the current module is denied. </p>
                                <p>
                                    You will be redirected after 5 second to access page.</p>
                                <p>
                                    <asp:HyperLink ID="HyperLink2" runat="server" Font-Italic="True" Font-Names="Verdana"
                                         NavigateUrl="/PHOENIXBETA/login.aspx">Click here to  return to the login page</asp:HyperLink>&nbsp;</p>
                                
    

                    </div>
                </div>



        


            </div>
                <!-- /.container-fluid-->
                <!-- /.content-wrapper-->
                <footer class="sticky-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4">
                                <small>Logged in Module : Student Management</small>
                            </div>
                            <div class="col-4">
                                <div class="text-center">
                                    <small>Copyright �</small>PHOENIX</div>
                            </div>
                            <div class="col-4 text-right">
                                <small>Logged in Date &amp; Time : 12-04-2018  13:25</small>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fa fa-angle-up"></i>
                </a>
                <!-- Logout Modal-->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <a class="btn btn-primary" href="/login.aspx">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
               
                 <!-- Core plugin JavaScript-->
                <script src="/PHOENIXBETA/vendor/jquery-easing/jquery.easing.min.js"></script>
                <!-- Page level plugin JavaScript-->
                <script src="/PHOENIXBETA/vendor/datatables/jquery.dataTables.js"></script>
                <script src="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.js"></script>
                <!-- Custom scripts for all pages-->
                <script src="/PHOENIXBETA/js/sb-admin.js"></script>
                <!-- Custom scripts for this page-->
                <script src="/PHOENIXBETA/js/sb-admin-datatables.min.js"></script>


                <!-- Toggle between fixed and static navbar-->
                <script>
                    $('#toggleNavPosition').click(function () {
                        $('body').toggleClass('fixed-nav');
                        $('nav').toggleClass('fixed-top static-top');
                    });

                </script>
                <!-- Toggle between dark and light navbar-->
                <script>
                    $('#toggleNavColor').click(function () {
                        $('nav').toggleClass('navbar-dark navbar-light');
                        $('nav').toggleClass('bg-dark bg-light');
                        $('body').toggleClass('bg-dark bg-light');
                    });
                </script>
                <!-- Toggle the body class on screen change -->
                <script>
                    $(window).resize(function () {
                        if ($(window).width() < 991) {
                         
                            $('body').removeClass('sidenav-toggled');
                        }
                        else {
                            $('body').addClass('sidenav-toggled');
                        }
                    });
                </script>
                
            </div>
    
    </div>
    </form>
</body>
</html>
