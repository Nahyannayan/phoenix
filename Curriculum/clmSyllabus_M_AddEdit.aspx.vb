

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_clmSyllabus_M_AddEdit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Session("Syllabus") = SyllabusMaster.CreateDataTableSyllabus()

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100025" And ViewState("MainMnu_code") <> "C300165") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'ddlAcademicYear = ActivityFunctions.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    If ViewState("datamode") = "add" Then

                        FillAcd()
                        FillTerm()
                        FillGrade()
                        FillSubjects()
                        ClearPartialDetails()
                        GetDetails()

                        'txtCLM_DESC.Text = ""
                    Else

                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        FillDetails()
                    End If

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then

            'str_err = calltransaction(errorMessage)

            'If str_err = "0" Then
            '    'txtCLM_DESC.Enabled = False
            '    lblError.Text = "Record Saved Successfully"
            'Else
            '    lblError.Text = errorMessage
            'End If
            SaveSyllabus()
        End If

    End Sub
    

#End Region

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("datamode") = "add"
            'txtCLM_DESC.Text = ""
            'txtCLM_DESC.Enabled = True
            FillAcd()
            FillTerm()
            ddlGrade.Enabled = True
            ddlSubject.Enabled = True
            FillGrade()
            FillSubjects()
            ClearPartialDetails()
            GetDetails()


            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("datamode") = "edit"
            ddlGrade.Enabled = True
            ddlSubject.Enabled = True
            txtSyllabus.Enabled = True
            txtSyllabus.Text = ""
            gvSyllabus.Columns(13).Visible = True
            gvSyllabus.Columns(14).Visible = True
            'txtCLM_DESC.Enabled = True
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Dim url As String = ""
            ViewState("viewid") = 0
            'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            'ViewState("datamode") = "none"

            'txtCLM_DESC.Text = ""
            'txtCLM_DESC.Enabled = False
            'Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'Else
            'Response.Redirect(ViewState("ReferrerUrl"))
            'ViewState("datamode") = "none"
            'Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'End If
            'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Sub FillAcd()
        
        ddlAcademicYear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub

    Sub FillTerm()
        
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillTerm()
        FillGrade()
        FillSubjects()

    End Sub
    Sub FillGrade()
        
        'ddlGrade.DataSource = ActivityFunctions.GetGrade_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlGrade.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillGrade()
        FillSubjects()
        GetDetails()
    End Sub
    Sub FillSubjects()
       
        'ddlSubject.DataSource = ActivityFunctions.GetSUBJECT_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, ddlGrade.SelectedItem.Value)
        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_SBG As String = ""
        sqlGetOpen_SBG = "SELECT SBG_ID, SBG_DESCR " _
              & " FROM " _
              & " SUBJECTS_GRADE_S WHERE SBG_BSU_ID='" & Session("sBsuid") & "' and SBG_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' AND SBG_GRD_ID='" & ddlGrade.SelectedItem.Value & "' " _
              & " AND SBG_ID NOT IN (SELECT SYM_SBG_ID FROM SYL.SYLLABUS_M WHERE SYM_BSU_ID='" & Session("sBsuid") & "' AND SYM_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' AND " _
              & " SYM_TRM_ID='" & ddlTerm.SelectedItem.Value & "' and SYM_GRD_ID='" & ddlGrade.SelectedItem.Value & "') ORDER BY SBG_ID"
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_SBG, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        ddlSubject.DataSource = reader
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillSubjects()

    End Sub
    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable

    End Function

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        Try
            If ViewState("datamode") = "add" Then

                ' Session("Syllabus") = SyllabusMaster.CreateDataTableSyllabus()
                Session("gDtlDataMode") = "Add"
                If Session("gDtlDataMode") = "Add" Then
                    ldrNew = Session("Syllabus").NewRow
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("Id") = Session("gintGridLine")
                    'h_NextLine.Value = Session("gintGridLine")
                    ldrNew("AcdId") = ddlAcademicYear.SelectedItem.Value
                    ldrNew("AcdYear") = ddlAcademicYear.SelectedItem.Text
                    ldrNew("TrmId") = ddlTerm.SelectedItem.Value
                    ldrNew("Term") = ddlTerm.SelectedItem.Text
                    ldrNew("GrdId") = ddlGrade.SelectedItem.Value
                    ldrNew("Grade") = ddlGrade.SelectedItem.Text
                    ldrNew("SubjId") = ddlSubject.SelectedItem.Value
                    ldrNew("Subject") = ddlSubject.SelectedItem.Text
                    ldrNew("hide") = "<span style='color: blue;font-weight:bold'>Hide</span>"
                    ldrNew("tempview") = StringTrim(txtSyllabus.Text) + "</br><span style='color: blue;font-weight:bold'> More... </span>"
                    ldrNew("Syllabus") = Trim(txtSyllabus.Text)
                    ldrNew("SylId") = 0
                    ldrNew("GUID") = System.DBNull.Value
                    Session("Syllabus").Rows.Add(ldrNew)
                    ddlAcademicYear.Enabled = False
                    ddlTerm.Enabled = False
                End If
            End If
            If ViewState("datamode") = "edit" Then
                If Not Session("Syllabus") Is Nothing Then
                    For lintIndex = 0 To Session("Syllabus").Rows.Count - 1
                        If (Session("Syllabus").Rows(lintIndex)("Id") = Session("gintGridLine")) Then
                            Session("Syllabus").Rows(lintIndex)("Syllabus") = txtSyllabus.Text
                            'Session("Syllabus")
                        End If
                    Next
                End If
            End If
            GridBind()
            'FillGrade()
            'FillSubjects()
            txtSyllabus.Text = ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        'ClearPartialDetails()
    End Sub
    Private Function GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = SyllabusMaster.CreateDataTableSyllabus()
        If Session("Syllabus").Rows.Count > 0 Then
            For i = 0 To Session("Syllabus").Rows.Count - 1
                'If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                Dim ldrTempNew As DataRow
                ldrTempNew = dtTempDtl.NewRow
                For j As Integer = 0 To Session("Syllabus").Columns.Count - 1
                    ldrTempNew.Item(j) = Session("Syllabus").Rows(i)(j)
                Next
                dtTempDtl.Rows.Add(ldrTempNew)
                'End If
            Next
        End If
        gvSyllabus.DataSource = dtTempDtl
        gvSyllabus.DataBind()
        gvSyllabus.Columns(0).Visible = False
        gvSyllabus.Columns(1).Visible = False
        gvSyllabus.Columns(2).Visible = False
        gvSyllabus.Columns(3).Visible = False
        gvSyllabus.Columns(4).Visible = False
        gvSyllabus.Columns(5).Visible = False
        gvSyllabus.Columns(6).Visible = False
        gvSyllabus.Columns(8).Visible = False
        gvSyllabus.Columns(11).Visible = False
        gvSyllabus.Columns(13).Visible = False
        gvSyllabus.Columns(14).Visible = False
        
        'gvDTL.Columns(16).Visible = True
        'If Session("dtDTL").Rows.Count <= 0 Then
        '    GridInitialize()
        'End If
        'Clear_Details_Partial()
    End Function

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtSyllabus.Text = ""
        'GetDetails()
    End Sub
    Sub ClearPartialDetails()
        FillGrade()
        FillSubjects()
        txtSyllabus.Enabled = True
        txtSyllabus.Text = ""
        ddlAcademicYear.Enabled = True
        ddlTerm.Enabled = True
        gvSyllabus.DataSource = Nothing
        gvSyllabus.DataBind()
        Session("gintGridLine") = 0
        Session("Syllabus") = Nothing
        h_SYL_ID.Value = 0
    End Sub
    Sub SaveSyllabus()
        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim strQuery As String
        Dim dr As SqlDataReader
        Dim cmd As New SqlCommand
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            For iIndex = 0 To Session("Syllabus").Rows.Count - 1
                If CheckDataExists(iIndex, objConn, stTrans) = False Then
                    cmd = New SqlCommand("[SYL].[SaveSYLLABUS_M]", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    'cmd.Parameters.AddWithValue("@GUID", Session("Syllabus").Rows(iIndex)("GUID"))
                    If ViewState("datamode") = "add" Then
                        cmd.Parameters.AddWithValue("@SYM_ID", 0)
                    End If
                    If ViewState("datamode") = "edit" Then
                        cmd.Parameters.AddWithValue("@SYM_ID", h_SYL_ID.Value)
                    End If
                    cmd.Parameters.AddWithValue("@SYM_BSU_ID", Session("sBsuid"))
                    cmd.Parameters.AddWithValue("@SYM_ACD_ID", Session("Syllabus").Rows(iIndex)("AcdId"))
                    cmd.Parameters.AddWithValue("@SYM_TRM_ID", Session("Syllabus").Rows(iIndex)("TrmId"))
                    cmd.Parameters.AddWithValue("@SYM_GRD_ID", Session("Syllabus").Rows(iIndex)("GrdId"))
                    cmd.Parameters.AddWithValue("@SYM_SBG_ID", Session("Syllabus").Rows(iIndex)("SubjId"))
                    cmd.Parameters.AddWithValue("@SYM_DESCR", Session("Syllabus").Rows(iIndex)("Syllabus"))
                    If ViewState("datamode") = "add" Then
                        cmd.Parameters.AddWithValue("@bEdit", 0)
                    End If
                    If ViewState("datamode") = "edit" Then
                        cmd.Parameters.AddWithValue("@bEdit", 1)
                    End If
                    cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmd.ExecuteNonQuery()
                    iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                End If
            Next
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                objConn.Close()
                Exit Sub
            End If
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
            objConn.Close()
            ClearPartialDetails()
            GetDetails()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Function GetDetails()
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer
        Dim ldrNew As DataRow
        Try
            Session("Syllabus") = SyllabusMaster.CreateDataTableSyllabus()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            strSql = "SELECT   SYL.SYLLABUS_M.SYM_ACD_ID as AcdId, VW_ACADEMICYEAR_M.ACY_DESCR as AcdYear, SYL.SYLLABUS_M.SYM_TRM_ID as TrmId, VW_TRM_M.TRM_DESCRIPTION as Term, " _
                    & " SYL.SYLLABUS_M.SYM_GRD_ID as GrdId, VW_GRADE_M.GRD_DISPLAY as Grade, SYL.SYLLABUS_M.SYM_SBG_ID as SubjId, SUBJECTS_GRADE_S.SBG_DESCR as Subject, " _
                    & " '<span style=''color: blue;font-weight:bold''>Hide</span>' as hide,(substring(SYL.SYLLABUS_M.SYM_DESCR,0,50)+ '</br><span style=''color: blue;font-weight:bold''> More... </span>')tempview,SYL.SYLLABUS_M.SYM_DESCR as Syllabus,SYL.SYLLABUS_M.SYM_ID as SylId " _
                    & " FROM SYL.SYLLABUS_M INNER JOIN " _
                    & " VW_BUSINESSUNIT_M ON SYL.SYLLABUS_M.SYM_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                    & " vw_ACADEMICYEAR_D ON SYL.SYLLABUS_M.SYM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " _
                    & " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID INNER JOIN " _
                    & " VW_TRM_M ON SYL.SYLLABUS_M.SYM_TRM_ID = VW_TRM_M.TRM_ID INNER JOIN " _
                    & " VW_GRADE_M ON SYL.SYLLABUS_M.SYM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " _
                    & " SUBJECTS_GRADE_S ON SYL.SYLLABUS_M.SYM_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " _
                    & " WHERE (SYL.SYLLABUS_M.SYM_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "')  AND (SYL.SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "')"
            If Not ddlTerm.SelectedIndex = -1 Then
                strSql += "AND (SYL.SYLLABUS_M.SYM_TRM_ID = '" & ddlTerm.SelectedItem.Value & "')"
            End If
            If Not ddlSubject.SelectedIndex = -1 Then
                strSql += "AND (SYL.SYLLABUS_M.SYM_SBG_ID='" & ddlSubject.SelectedItem.Value & "')"
            End If
            strSql += "order by SylId"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            'gvSyllabus.DataSource = ds
            'gvSyllabus.DataBind()
            If Not Session("Syllabus") Is Nothing Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    ldrNew = Session("Syllabus").NewRow
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("Id") = Session("gintGridLine")
                    'h_NextLine.Value = Session("gintGridLine")
                    ldrNew("AcdId") = ds.Tables(0).Rows(i)("AcdId")
                    ldrNew("AcdYear") = ds.Tables(0).Rows(i)("AcdYear")
                    ldrNew("TrmId") = ds.Tables(0).Rows(i)("TrmId")
                    ldrNew("Term") = ds.Tables(0).Rows(i)("Term")
                    ldrNew("GrdId") = ds.Tables(0).Rows(i)("GrdId")
                    ldrNew("Grade") = ds.Tables(0).Rows(i)("Grade")
                    ldrNew("SubjId") = ds.Tables(0).Rows(i)("SubjId")
                    ldrNew("Subject") = ds.Tables(0).Rows(i)("Subject")
                    ldrNew("hide") = ds.Tables(0).Rows(i)("hide")
                    ldrNew("tempview") = ds.Tables(0).Rows(i)("tempview")
                    ldrNew("Syllabus") = ds.Tables(0).Rows(i)("Syllabus")
                    ldrNew("SylId") = ds.Tables(0).Rows(i)("SylId")
                    ldrNew("GUID") = System.DBNull.Value
                    Session("Syllabus").Rows.Add(ldrNew)
                Next
                GridBind()
                'ddlAcademicYear.Enabled = True
                'ddlTerm.Enabled = True
                'Session("Syllabus") = Nothing
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function
    Private Function CheckDataExists(ByVal iIndex As Integer, ByVal ObjConn As SqlConnection, ByVal sttrans As SqlTransaction) As Boolean
        Dim strQuery As String
        Dim dr As SqlDataReader
        Dim cmdSub As SqlCommand
        Dim iReturnvalue As Integer
        Try
            'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            If Not Session("Syllabus") Is Nothing Then
                '    strQuery = "SELECT SYM_DESCR FROM SYL.SYLLABUS_M WHERE SYM_BSU_ID='" & Session("sBsuid") & "' and " _
                '              & " SYM_ACD_ID='" & Session("Syllabus").Rows(iIndex)("AcdId") & "' and " _
                '              & " SYM_TRM_ID='" & Session("Syllabus").Rows(iIndex)("TrmId") & "' and " _
                '              & " SYM_GRD_ID='" & Session("Syllabus").Rows(iIndex)("GrdId") & "' and " _
                '              & " SYM_DESCR='" & Session("Syllabus").Rows(iIndex)("Syllabus") & "'"
                '    dr = SqlHelper.ExecuteReader(ObjConn, CommandType.Text, strQuery)

                '    CheckDataExists = dr.Read()
                cmdSub = New SqlCommand("[SYL].[GETSYLLABUSDATA]", ObjConn, sttrans)
                cmdSub.CommandType = CommandType.StoredProcedure
                'cmd.Parameters.AddWithValue("@GUID", Session("Syllabus").Rows(iIndex)("GUID"))
                cmdSub.Parameters.AddWithValue("@SYM_BSU_ID", Session("sBsuid"))
                cmdSub.Parameters.AddWithValue("@SYM_ACD_ID", Session("Syllabus").Rows(iIndex)("AcdId"))
                cmdSub.Parameters.AddWithValue("@SYM_TRM_ID", Session("Syllabus").Rows(iIndex)("TrmId"))
                cmdSub.Parameters.AddWithValue("@SYM_GRD_ID", Session("Syllabus").Rows(iIndex)("GrdId"))
                cmdSub.Parameters.AddWithValue("@SYM_DESCR", Session("Syllabus").Rows(iIndex)("Syllabus"))
                cmdSub.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmdSub.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmdSub.ExecuteNonQuery()
                iReturnvalue = CInt(cmdSub.Parameters("@ReturnValue").Value)
                If iReturnvalue <> 0 Then
                    CheckDataExists = True
                Else
                    CheckDataExists = False
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function

    
    Sub FillDetails()
        Dim strSql As String
        Dim ds As DataSet
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        strSql = "SELECT     SYL.SYLLABUS_M.SYM_ACD_ID as AcdId, VW_ACADEMICYEAR_M.ACY_DESCR as AcdYear, SYL.SYLLABUS_M.SYM_TRM_ID as TrmId, VW_TRM_M.TRM_DESCRIPTION as Term, " _
                    & " SYL.SYLLABUS_M.SYM_GRD_ID as GrdId, VW_GRADE_M.GRD_DISPLAY as Grade, SYL.SYLLABUS_M.SYM_SBG_ID as SubjId, SUBJECTS_GRADE_S.SBG_DESCR as Subject, " _
                    & " '<span style=''color: blue;font-weight:bold''>Hide</span>' as hide,(substring(SYL.SYLLABUS_M.SYM_DESCR,0,50)+ '</br><span style=''color: blue;font-weight:bold''> More... </span>')tempview,SYL.SYLLABUS_M.SYM_DESCR as Syllabus,SYL.SYLLABUS_M.SYM_ID as SylId " _
                    & " FROM SYL.SYLLABUS_M INNER JOIN " _
                    & " VW_BUSINESSUNIT_M ON SYL.SYLLABUS_M.SYM_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                    & " vw_ACADEMICYEAR_D ON SYL.SYLLABUS_M.SYM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " _
                    & " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID INNER JOIN " _
                    & " VW_TRM_M ON SYL.SYLLABUS_M.SYM_TRM_ID = VW_TRM_M.TRM_ID INNER JOIN " _
                    & " VW_GRADE_M ON SYL.SYLLABUS_M.SYM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " _
                    & " SUBJECTS_GRADE_S ON SYL.SYLLABUS_M.SYM_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " _
                    & " WHERE (SYL.SYLLABUS_M.SYM_ID='" & ViewState("viewid") & "') "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "AcdYear"
            ddlAcademicYear.DataValueField = "AcdId"
            ddlAcademicYear.DataBind()

            ddlTerm.DataSource = ds
            ddlTerm.DataTextField = "Term"
            ddlTerm.DataValueField = "TrmId"
            ddlTerm.DataBind()

            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "Grade"
            ddlGrade.DataValueField = "GrdId"
            ddlGrade.DataBind()

            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "Subject"
            ddlSubject.DataValueField = "SubjId"
            ddlSubject.DataBind()

            txtSyllabus.Text = ds.Tables(0).Rows(0)("Syllabus")

            ddlAcademicYear.Enabled = False
            ddlTerm.Enabled = False
            ddlGrade.Enabled = False
            ddlSubject.Enabled = False
            txtSyllabus.Enabled = False
            GetDetails()
        End If

    End Sub

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblAcdId As New Label
            Dim lblTrmId As New Label
            Dim lblSubjId As New Label
            Dim lblSylId As New Label
            Dim url As String

            'lblCAM_ID = TryCast(sender.FindControl("lblCAM_ID"), Label)
            lblAcdId = TryCast(sender.FindControl("lblACDID"), Label)
            lblTrmId = TryCast(sender.FindControl("lblTrmID"), Label)
            lblSubjId = TryCast(sender.FindControl("lblSubjID"), Label)
            lblSylId = TryCast(sender.FindControl("lblSylId"), Label)
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_D_AddEdit.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") & "&AccId=" & Encr_decrData.Encrypt(lblAcdId.Text) & "&TermId=" & Encr_decrData.Encrypt(lblTrmId.Text) & "&SubjID=" & Encr_decrData.Encrypt(lblSubjId.Text) & "&SyllabusId=" & Encr_decrData.Encrypt(lblSylId.Text))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As Label
        Dim lintIndex As Integer
        lblRowId = TryCast(sender.FindControl("lblID"), Label)
        Session("gintGridLine") = lblRowId.Text
        For lintIndex = 0 To Session("Syllabus").Rows.Count - 1
            If (Session("Syllabus").Rows(lintIndex)("Id") = Session("gintGridLine")) Then
                txtSyllabus.Text = Trim(Session("Syllabus").Rows(lintIndex)("Syllabus"))
                h_SYL_ID.Value = Trim(Session("Syllabus").Rows(lintIndex)("SylId"))
            End If
        Next
        btnDetAdd.Text = "Update"
    End Sub

    
  
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SYLId As New Label
        Dim strSQL As String
        Dim strSub As String
        Dim ds As DataSet
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                SYLId = TryCast(sender.parent.FindControl("lblSylId"), Label)
                If Not SYLId Is Nothing Then
                    strSQL = "SELECT SYD_ID FROM SYL.SYLLABUS_D WHERE SYD_SYM_ID='" & SYLId.Text & "'"
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblError.Text = "Can't Delete, Transaction Exists"
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    Else
                        strSub = "DELETE FROM SYL.SYLLABUS_M WHERE SYM_ID='" & SYLId.Text & "' AND SYM_BSU_ID='" & Session("SBsuid") & "' and SYM_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' and SYM_TRM_ID='" & ddlTerm.SelectedItem.Value & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSub)
                    End If

                End If
                stTrans.Commit()
                lblError.Text = "Successfully Deleted"
            Catch ex As Exception
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"

            Finally
                objConn.Close()
            End Try
            GetDetails()

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim strSQL As String
        Dim strSub As String
        Dim ds As DataSet

        'If ViewState("viewid") <> "" Then
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                If ViewState("viewid") <> "" Then
                    strSQL = "SELECT SYD_ID FROM SYL.SYLLABUS_D WHERE SYD_SYM_ID='" & ViewState("viewid") & "'"
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblError.Text = "Can't Delete, Transaction Exists"
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    Else
                        strSub = "DELETE FROM SYL.SYLLABUS_M WHERE SYM_ID='" & ViewState("viewid") & "' AND SYM_BSU_ID='" & Session("SBsuid") & "' and SYM_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' and SYM_TRM_ID='" & ddlTerm.SelectedItem.Value & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSub)
                    End If

                End If
                stTrans.Commit()
                lblError.Text = "Successfully Deleted"
            Catch ex As Exception
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            Finally
                objConn.Close()
            End Try
            GetDetails()
            txtSyllabus.Text = ""
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
            'End If
    End Sub
    Private Function StringTrim(ByVal strmain As String) As String
        If strmain.Length > 50 Then
            Return strmain.Substring(1, 50)
        Else
            Return strmain
        End If
        'Dim strSplit As String = ""
        'Dim strParentStr As String = strmain
        'Try
        '    Dim intIndx As Integer = 1
        '    If strParentStr.Length > 50 Then
        '        For inti As Integer = 1 To 10
        '            If strParentStr.Length > 50 Then
        '                Try
        '                    strSplit += vbCrLf + strParentStr.Substring(intIndx, 50)
        '                Catch ex As Exception
        '                    strSplit += vbCrLf + strParentStr.Substring(intIndx)
        '                    Return strSplit
        '                End Try

        '                'strParentStr = strParentStr.Substring(strSplit.Length, strParentStr.Length - strSplit.Length)
        '            End If
        '            intIndx += 50
        '        Next
        '        Return strSplit
        '    Else
        '        Return strParentStr
        '    End If
        'Catch ex As Exception

        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try
    End Function
End Class

