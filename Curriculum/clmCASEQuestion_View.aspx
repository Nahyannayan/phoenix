﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmCASEQuestion_View.aspx.vb" Inherits="Curriculum_clmCASEQuestion_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Questions Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" valign="top" colspan="4">
                            <asp:LinkButton ID="lnkAddNew" runat="server"  >Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="matters">
                            <asp:GridView ID="gvQuestions" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" AllowSorting="True">
                                <RowStyle CssClass="griditem"   />
                                <Columns>
                                    <asp:TemplateField HeaderText="Grade" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQsmId" runat="server" Text='<%# Bind("QSM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSbmbId" runat="server" Text='<%# Bind("QSM_SBM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("QSM_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QuestionBank">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuestion" runat="server" Text='<%# Bind("QSM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Questions">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalQuestions" runat="server" Text='<%# Bind("QSM_TOTALQUESTIONS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalMarks" runat="server" Text='<%# Bind("QSM_TOTALMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop"   HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
