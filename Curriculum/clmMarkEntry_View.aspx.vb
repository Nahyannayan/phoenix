Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmMarkEntry_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    BindTerm()

                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGrade.Items.Insert(0, li)

                   
                    'ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    'ddlSubject = PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlSubject.Items.Add(li)

                    'ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                    ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGroup.Items.Insert(0, li)



                    ddlActivity = currFns.PopulateActivityMaster(ddlActivity, Session("sbsuid"))
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlActivity.Items.Insert(0, li)

                    tblClm.Rows(4).Visible = False
                    tblClm.Rows(5).Visible = False
                    ' tblClm.Rows(6).Visible = False
                    SelectSearches(sender, e)

                    If (Not Session("accSearch_m") Is Nothing) Or _
                    (Not Session("trmSearch_m") Is Nothing) Or _
                    (Not Session("grdSearch_m") Is Nothing) Or _
                    (Not Session("grpSearch_m") Is Nothing) Or _
                    (Not Session("asmSearch_m") Is Nothing) Or _
                    (Not Session("sbgSearch_m") Is Nothing) Then
                        tblClm.Rows(4).Visible = True
                        tblClm.Rows(5).Visible = True
                        ' tblClm.Rows(6).Visible = True
                        GridBind()
                    End If


                End If

                gvStud.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

    Sub SelectSearches(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("accSearch_m") Is Nothing Then
            If Not ddlAcademicYear.Items.FindByValue(Session("accSearch_m")) Is Nothing Then
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.Items.FindByValue(Session("accSearch_m")).Selected = True
                ddlAcademicYear_SelectedIndexChanged(sender, e)
            End If
        End If

        If Not Session("trmSearch_m") Is Nothing Then
            If Not ddlTerm.Items.FindByValue(Session("trmSearch_m")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("trmSearch_m")).Selected = True
            End If
        End If

        If Not Session("grdSearch_m") Is Nothing Then
            If Not ddlGrade.Items.FindByValue(Session("grdSearch_m")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grdSearch_m")).Selected = True
                ddlGrade_SelectedIndexChanged(sender, e)
            End If
        End If

        If Not Session("sbgSearch_m") Is Nothing Then
            If Not ddlSubject.Items.FindByValue(Session("sbgSearch_m")) Is Nothing Then
                ddlSubject.ClearSelection()
                ddlSubject.Items.FindByValue(Session("sbgSearch_m")).Selected = True
                ddlSubject_SelectedIndexChanged(sender, e)

            End If
        End If

        If Not Session("grpSearch_m") Is Nothing Then
            If Not ddlGroup.Items.FindByValue(Session("grpSearch_m")) Is Nothing Then
                ddlGroup.ClearSelection()
                ddlGroup.Items.FindByValue(Session("grpSearch_m")).Selected = True
            End If
        End If

        If Not Session("asmSearch_m") Is Nothing Then
            If Not ddlActivity.Items.FindByValue(Session("asmSearch_m")) Is Nothing Then
                ddlActivity.ClearSelection()
                ddlActivity.Items.FindByValue(Session("asmSearch_m")).Selected = True
            End If
        End If
    End Sub


    Protected Sub lblMarks_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            Dim lblMaxMarks As Label
            Dim lblEntry As Label
            Dim lblAOL As Label
            Dim lblSkill As Label
            Dim lblWS As Label
            Dim lblGrdId As Label
            Dim lblGrpId As Label
            Dim lblSbgId As Label
            With sender
                lblAOL = TryCast(.FindControl("lblAOL"), Label)
                lblSkill = TryCast(.FindControl("lblSkill"), Label)
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
                lblMaxMarks = TryCast(.FindControl("lblMaxMarks"), Label)
                lblEntry = TryCast(.FindControl("lblEntry"), Label)
                lblWS = TryCast(.FindControl("lblWS"), Label)
                lblGrdId = TryCast(.FINDCONTROL("lblGrdId"), Label)
                lblGrpId = TryCast(.FINDCONTROL("lblGrpId"), Label)
                lblSbgId = TryCast(.FINDCONTROL("lblSbgId"), Label)
            End With
            Dim url As String
            If lblAOL.Text.ToLower = "false" Then
                url = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                                   & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                                   & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                                   & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                                   & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                                   & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                                   & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                                   & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                                   & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                                   & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                                   & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                                   & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                   & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                   & "&sgrid=" + Encr_decrData.Encrypt(lblGrpId.Text) _
                                    & "&sbgid=" + Encr_decrData.Encrypt(lblSbgId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf lblSkill.Text.ToLower = "true" Then
                url = String.Format("~\Curriculum\clmMarkEntry_DynamicAOL.aspx?MainMnu_code={0}&datamode={1}" _
                             & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                             & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                             & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                             & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                             & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                             & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                             & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                             & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                             & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                             & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                             & "&ws=" + Encr_decrData.Encrypt(lblWS.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\clmMarkEntry_AOL.aspx?MainMnu_code={0}&datamode={1}" _
                             & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                             & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                             & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                             & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                             & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                             & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                             & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                             & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                             & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                             & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                             & "&ws=" + Encr_decrData.Encrypt(lblWS.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub lblAttendance_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            Dim lblMaxMarks As Label
            Dim lblEntry As Label
            Dim lblAOL As Label
            Dim lblSkill As Label
            Dim lblWS As Label
            Dim lblAentered As Label
            Dim lblGrdId As Label
            Dim lblGrpId As Label
            Dim lblSbgId As Label

            With sender
                lblAOL = TryCast(.FindControl("lblAOL"), Label)
                lblSkill = TryCast(.FindControl("lblSkill"), Label)
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
                lblMaxMarks = TryCast(.FindControl("lblMaxMarks"), Label)
                lblEntry = TryCast(.FindControl("lblEntry"), Label)
                lblWS = TryCast(.FindControl("lblWS"), Label)
                lblAentered = TryCast(.FindControl("lblAentered"), Label)
                lblGrdId = TryCast(.FINDCONTROL("lblGrdId"), Label)
                lblGrpId = TryCast(.FINDCONTROL("lblGrpId"), Label)
                lblSbgId = TryCast(.FINDCONTROL("lblSbgId"), Label)
            End With

            Dim markurl As String
            If lblAOL.Text.ToLower = "false" Then
                markurl = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                                   & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                                   & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                                   & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                                   & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                                   & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                                   & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                                   & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                                   & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                                   & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                                   & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                                   & "&ws=" + Encr_decrData.Encrypt(lblWS.Text) _
                                   & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                   & "&sgrid=" + Encr_decrData.Encrypt(lblGrpId.Text) _
                                   & "&sbgid=" + Encr_decrData.Encrypt(lblSbgId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf lblSkill.Text.ToLower = "true" Then
                markurl = String.Format("~\Curriculum\clmMarkEntry_DynamicAOL.aspx?MainMnu_code={0}&datamode={1}" _
                             & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                             & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                             & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                             & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                             & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                             & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                             & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                             & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                             & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                             & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                             & "&ws=" + Encr_decrData.Encrypt(lblWS.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                markurl = String.Format("~\Curriculum\clmMarkEntry_AOL.aspx?MainMnu_code={0}&datamode={1}" _
                             & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                             & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                             & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                             & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                             & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                             & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                             & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                             & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                             & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                             & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text) _
                             & "&ws=" + Encr_decrData.Encrypt(lblWS.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            End If


            Dim url As String
            url = String.Format("~\Curriculum\clmATTEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&markurl=" + Encr_decrData.Encrypt(markurl) _
                               & "&attent=" + Encr_decrData.Encrypt(lblAentered.Text), ViewState("MainMnu_code"), ViewState("datamode"))



            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub ddlgvAttendance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub


    Protected Sub ddlgvMarks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub


#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlTerm.Items.Insert(0, li)
        li = New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + acdid
                If ViewState("GRD_ACCESS") > 0 Then
                    str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                             & " WHERE (GSA_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                             & " UNION" _
                             & " SELECT SGR_GRD_ID FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID WHERE SGR_ACd_ID=" + acdid _
                             & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") + " AND SGS_TODATE IS NULL" _
                             & ")"
                End If
                str_query += " order by grd_displayorder"
            Else
                'str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                '                      & "  grm_acd_id=" + acdid
                'str_query += " order by grd_displayorder"

                str_query = "SELECT DISTINCT " & _
"CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY + '-' + STM_DESCR END AS GRM_DISPLAY,   " & _
" GRADE_BSU_M.GRM_GRD_ID + '|' + CONVERT(VARCHAR(100), STREAM_M.STM_ID) AS GRM_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
" STREAM_M.STM_ID FROM GROUPS_TEACHER_S INNER JOIN " & _
" GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID  " & _
" AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
"GRADE_BSU_M INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
" STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID ON GROUPS_M.SGR_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
" GROUPS_M.SGR_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND GROUPS_M.SGR_STM_ID = GRADE_BSU_M.GRM_STM_ID AND " & _
" GROUPS_M.SGR_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & " WHERE " _
      & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString & " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId")

                str_query += " order by grd_displayorder"

            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function




    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function


    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strFilter As String = ""
        If Session("CurrSuperUser") = "Y" Then
            If ddlActivity.SelectedValue = "" Then
                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                             & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,CAS_MARKENTERED" _
                             & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                             & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                             & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                             & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE," _
                             & " ISNULL(CAS_bHasAOLEXAM,'FALSE') AS CAS_bHasAOLEXAM,ISNULL(CAS_bWITHOUTSKILLS,'FALSE') AS CAS_bWITHOUTSKILLS,ISNULL(CAD_bSKILLS,'FALSE') AS CAS_bSKILLS,SBG_GRD_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                             & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                             & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                             & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                             & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString



            Else
                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                            & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,CAS_MARKENTERED" _
                            & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                            & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                            & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                            & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE," _
                            & " ISNULL(CAS_bHasAOLEXAM,'FALSE') AS CAS_bHasAOLEXAM,ISNULL(CAS_bWITHOUTSKILLS,'FALSE') AS CAS_bWITHOUTSKILLS,ISNULL(CAD_bSKILLS,'FALSE') AS CAS_bSKILLS,SBG_GRD_ID  FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                            & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                            & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                            & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                            & " INNER JOIN ACT.ACTIVITY_M AS F ON E.CAD_CAM_ID=F.CAM_ID" _
                            & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                            & " AND CAM_ID=" + ddlActivity.SelectedValue.ToString
            End If

            ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))


            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND (SGR_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"

                str_query += " OR SGR_ID IN(SELECT SGS_SGR_ID FROM GROUPS_TEACHER_S WHERE SGS_EMP_ID=" + Session("EMPLOYEEID") + " and SGS_TODATE IS NULL ))"
            End If


        Else
            If ddlActivity.SelectedValue = "" Then
                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                             & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,CAS_MARKENTERED" _
                             & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                             & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                             & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                             & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,ISNULL(CAS_bHasAOLEXAM,'FALSE') AS CAS_bHasAOLEXAM,ISNULL(CAS_bWITHOUTSKILLS,'FALSE') AS CAS_bWITHOUTSKILLS,ISNULL(CAD_bSKILLS,'FALSE') AS CAS_bSKILLS ,SBG_GRD_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                             & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                             & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                             & " INNER JOIN GROUPS_TEACHER_S AS D ON D.SGS_SGR_ID=C.SGR_ID" _
                             & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                             & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND SGS_TODATE IS NULL AND SGS_EMP_ID=" + Session("EMPLOYEEID")
            Else
                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                            & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,CAS_MARKENTERED" _
                            & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                            & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                            & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                            & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,ISNULL(CAS_bHasAOLEXAM,'FALSE') AS CAS_bHasAOLEXAM,ISNULL(CAS_bWITHOUTSKILLS,'FALSE') AS CAS_bWITHOUTSKILLS,ISNULL(CAD_bSKILLS,'FALSE') AS CAS_bSKILLS ,SBG_GRD_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                            & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                            & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                            & " INNER JOIN GROUPS_TEACHER_S AS D ON D.SGS_SGR_ID=C.SGR_ID" _
                            & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                            & " INNER JOIN ACT.ACTIVITY_M AS F ON E.CAD_CAM_ID=F.CAM_ID" _
                            & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                            & " AND SGS_TODATE IS NULL AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                            & " AND CAM_ID=" + ddlActivity.SelectedValue.ToString
            End If
        End If


        If (ddlGrade.SelectedValue <> "") Then
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND SGR_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)
            'str_query += " AND SGR_GRD_ID='" & ddlGrade.SelectedValue & "'"
        End If

        If ddlTerm.SelectedValue <> "" Then
            str_query += " AND CAD_TRM_ID=" + ddlTerm.SelectedValue.ToString
        End If

        If ddlGroup.SelectedValue <> "" Then
            str_query += " AND SGR_ID=" + ddlGroup.SelectedValue
        End If



        If ddlSubject.SelectedValue <> "" Then
            str_query += " AND SBG_ID=" + ddlSubject.SelectedValue
        End If


        Dim ddlgvAttendance As New DropDownList
        Dim ddlgvMarks As New DropDownList
        Dim selectedAtt As String = ""
        Dim selectedMarks As String = ""

        If gvStud.Rows.Count > 0 Then

            ddlgvAttendance = gvStud.HeaderRow.FindControl("ddlgvAttendance")
            If ddlgvAttendance.Text <> "ALL" Then
                If ddlgvAttendance.Text = "YES" Then
                    strFilter += " AND ISNULL(CAS_bATT_ENTERED,'FALSE')='TRUE'"
                Else
                    strFilter += " AND  ISNULL(CAS_bATT_ENTERED,'FALSE')='FALSE'"
                End If
            End If
            selectedAtt = ddlgvAttendance.Text

            ddlgvMarks = gvStud.HeaderRow.FindControl("ddlgvMarks")
            If ddlgvMarks.Text <> "ALL" Then
                If ddlgvMarks.Text = "YES" Then
                    strFilter += "  AND ISNULL(CAS_MARKENTERED,'NO')='YES'"
                ElseIf ddlgvMarks.Text = "NO" Then
                    strFilter += "  AND ISNULL(CAS_MARKENTERED,'NO')='NO'"
                Else
                    strFilter += "  AND ISNULL(CAS_MARKENTERED,'NO')='PARTIAL'"
                End If
            End If
            selectedMarks = ddlgvMarks.Text
        End If

        str_query += strFilter

        str_query += " ORDER BY CAS_TIME,CAS_DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            ds.Tables(0).Rows(0)("bMARKS") = True
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If

        If gvStud.Rows.Count > 0 Then
            ddlgvAttendance = gvStud.HeaderRow.FindControl("ddlgvAttendance")
            ddlgvAttendance.Text = selectedAtt

            ddlgvMarks = gvStud.HeaderRow.FindControl("ddlgvMarks")
            ddlgvMarks.Text = selectedMarks
        End If
    End Sub

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim gradeCount As Integer
        grade = ddlGrade.SelectedValue.Split("|")
        If ViewState("GRD_ACCESS") > 0 Then
            str_query = " select COUNT(DISTINCT SCT_GRD_ID) FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & " AND SCT_GRD_ID='" + grade(0) + "'"


            gradeCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            gradeCount = 1
        End If

        If gradeCount <> 0 Then
            str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                        & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                        & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                        & " WHERE SBG_ACD_ID=" + acd_id
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("employeeId"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Return ddlSubject
        End If

        If ddlGrade.SelectedValue <> "" Then


            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim gradeCount As Integer
        If ViewState("GRD_ACCESS") > 0 Then
            str_query = " select COUNT(DISTINCT SCT_GRD_ID) FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_aCD_ID=" + Session("CURRENT_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & " AND SCT_GRD_ID='" + grd_id + "'"


            gradeCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            gradeCount = 1
        End If

        If gradeCount = 0 Then
            Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
            ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Return ddlGroup
        End If

        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + acd_id

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function

    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & Grdid & "'"
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
            'If v_GradeID <> "" Then
            '    str_sql += "AND SBG_GRD_ID IN ('" & v_GradeID & "') ORDER BY GRM_DISPLAY "
            'End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "") As DropDownList
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & Grdid & "'"
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + acdid + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"

            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function


    Sub CallReport()
        Dim param As New Hashtable
        Dim casids As String = ""
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblCasId As Label

        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            lblCasId = gvStud.Rows(i).FindControl("lblCasId")
            If chkSelect.Checked = True Then
                If casids <> "" Then
                    casids += "|"
                End If
                casids += lblCasId.Text
            End If
        Next

        param.Add("@CAS_IDS", casids)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Curriculum/Reports/Rpt/rptStudentMarklist.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Session("rptClassPopup") = rptClass
        'ResponseHelper.Redirect("~/Reports/ASPX Report/rptReportViewer_Popup.aspx", "_blank", "")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region
    
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            BindTerm()

            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGrade.Items.Insert(0, li)


            'ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            'ddlSubject = PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlSubject.Items.Add(li)

            'ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
            ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            Dim li As ListItem
            If ddlGrade.SelectedValue <> "" Then
                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")
                If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

                Else
                    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                End If
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlSubject.Items.Insert(0, li)
                If Session("CurrSuperUser") = "Y" Then
                    ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0))
                Else
                    ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0))
                End If
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlGroup.Items.Insert(0, li)
            Else
                ddlSubject.Items.Clear()
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlSubject.Items.Add(li)

                If Session("CurrSuperUser") = "Y" Then
                    ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                Else
                    ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                End If
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlGroup.Items.Insert(0, li)


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            Dim li As ListItem
            Dim grade() As String = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            tblClm.Rows(4).Visible = True
            tblClm.Rows(5).Visible = True
            ' tblClm.Rows(6).Visible = True
            GridBind()
            Session("accSearch_m") = ddlAcademicYear.SelectedValue
            Session("trmSearch_m") = ddlTerm.SelectedValue
            Session("grdSearch_m") = ddlGrade.SelectedValue
            Session("grpSearch_m") = ddlGroup.SelectedValue
            Session("sbgSearch_m") = ddlSubject.SelectedValue
            Session("asmSearch_m") = ddlActivity.SelectedValue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    
  
    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CallReport()
    End Sub
End Class
