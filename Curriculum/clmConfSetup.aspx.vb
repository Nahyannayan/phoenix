﻿Option Explicit On
'Option Strict On
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Globalization
Partial Class Curriculum_ConfigSetup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim Str_Conn_OASIS_CURRICULUM As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
    Dim Str_Conn_OASIS As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
    Dim strGUID As String = System.Guid.NewGuid.ToString()
    Dim icount As Integer = 0
    Dim strSelrsmID As String
    Dim BolAddEdit As Boolean
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") Is ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                MaintainScrollPositionOnPostBack = True
                Dim str_sql As String = ""
                Dim USR_NAME As String = CStr(Session("sUsr_name"))
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("SelectedRow") = -1
                If ViewState("datamode") = "add" Then
                    ViewState("RptTable_Group3") = Nothing
                    ViewState("RptTable_Group2") = Nothing
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100130") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else
                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, Session("sBsuid").ToString, CStr(ViewState("MainMnu_code")))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), CStr(ViewState("menu_rights")), CStr(ViewState("datamode")))


                    If CStr(ViewState("datamode")) = "add" Then
                        ViewState("rsmid") = Nothing
                        Call ClearFields()
                        MaintainScrollPositionOnPostBack = True
                    ElseIf CStr(ViewState("datamode")) = "view" Then
                        ViewState("rsmid") = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                        strSelrsmID = ViewState("rsmid")
                        hdf_rsmid.Value = CStr(ViewState("rsmid"))
                        Call ViewData()
                        MaintainScrollPositionOnPostBack = True
                    ElseIf CStr(ViewState("datamode")) = "edit" Then
                        btnSave.Text = "Update"
                        ViewState("rsmid") = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                        strSelrsmID = ViewState("rsmid")
                        hdf_rsmid.Value = CStr(ViewState("rsmid"))
                        Call EditData()
                        MaintainScrollPositionOnPostBack = True
                    End If
                'End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed  " & ex.Message
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Protected Sub EditData()
        studClass.PopulateAcademicYear(DrpAcademicYear, CStr(Session("clm")), CStr(Session("sBSUID")))
        ' Call GetAdcGrade()
        Call GetRptResult()
        Call GetDisplayType()
        Call GetCssClass()
        For Each g1 As GridViewRow In Grd_Group2.Rows
            g1.FindControl("btnedit_group2").Visible = False
        Next
        '----------------------------------------------
        Dim sqlCon2 As New SqlCommand("ACT.usp_ReportConfig_List", Str_Conn_OASIS_CURRICULUM)
        Str_Conn_OASIS_CURRICULUM.Open()
        sqlCon2.CommandType = CommandType.StoredProcedure
        With sqlCon2
            .Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = Session("sBsuid").ToString
            .Parameters.Add("@intRSMID", SqlDbType.Int).Value = strSelrsmID
        End With
        Dim myReader As SqlDataReader = sqlCon2.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read()
                DrpAcademicYear.SelectedValue = Convert.ToInt32(myReader("RSM_ACD_ID"))
                txtRptDescription.Text = Convert.ToString(myReader("RSM_DESCR"))
                ChkFinalReports.Checked = IIf(IsDBNull(myReader("RSM_bFINALREPORT")), 0, 1)
                ChkAolPremium.Checked = IIf(IsDBNull(myReader("RSM_bAOLPROCESSING")), 0, 1)
                TxtReportType.Text = Convert.ToString(myReader("RSM_REPORTTYPE"))
                TxtReportFormat.Text = Convert.ToString(myReader("RSM_REPORTFORMAT"))
                ViewState("strSelGuid") = Convert.ToString(myReader("RSM_GUID"))
            Loop
        End If
        myReader.Close()

        Using cmd As New SqlCommand("ACT.usp_ReportConfig_List", Str_Conn_OASIS_CURRICULUM)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = Session("sBsuid").ToString
            cmd.Parameters.Add("@intRSMID", SqlDbType.Int).Value = strSelrsmID
            Using adapter As New SqlDataAdapter(cmd)
                '-----------------------------
                Call GetAdcGrade()
                Dim ds1 As New DataSet()
                adapter.Fill(ds1)
                For i As Integer = 0 To ds1.Tables(1).Rows.Count - 1
                    For Each item As ListItem In ChkGradeList.Items
                        If item.Text = ds1.Tables(1).Rows(i)("GRM_DISPLAY") Then   'GRM_DISPLAY
                            item.Selected = True
                        End If
                    Next
                Next
                '-------------------------
                Dim ds2 As New DataSet()
                adapter.Fill(ds2)
                With Grd_Group2
                    .DataSource = ds2.Tables(3)
                    .DataBind()
                    'ViewState("RptTable_Group2") = ds2.Tables(2)    Comment by Nikunj Aghera (10-Feb-2020) Desc : Wrong table data bind in view state.
                    ViewState("RptTable_Group2") = ds2.Tables(3)
                End With
                '-------------------------
                Dim ds3 As New DataSet()
                adapter.Fill(ds3)
                With Grd_Group3
                    .DataSource = ds3.Tables(4)
                    .DataBind()
                    'ViewState("RptTable_Group3") = ds3.Tables(3)    Comment by Nikunj Aghera (10-Feb-2020) Desc : Wrong table data bind in view state.
                    ViewState("RptTable_Group3") = ds3.Tables(4)
                End With
            End Using
        End Using
    End Sub
    Protected Sub ViewData()
        btnSave.Visible = False
        btnAddNew_Group3.Visible = False
        btnAddNew_Group2.Visible = False
        TxtRptHeader.Enabled = False
        'TblPanelReportCardSetup.Enabled = False
        tdReportNames.Visible = False
        TxtRptName.Visible = False
        TxtRptName.Visible = False
        DrpDisplayType.Visible = False
        DrpResult.Visible = False
        tdDisplaytype.Visible = False
        tdresult.Visible = False
        ChkDirectEntry.Visible = False
        ChkAllSubject.Visible = False
        ChkRuleReq.Visible = False
        ChkPerReq.Visible = False
        ChkGraphReq.Visible = False
        TxtRptHeader.Visible = False
        DrpCCSClass.Visible = False
        tdheader.Visible = False
        tdcssclass.Visible = False
        tdblank1.Visible = False
        studClass.PopulateAcademicYear(DrpAcademicYear, CStr(Session("clm")), CStr(Session("sBSUID")))
        Call GetAdcGrade()
        Call GetRptResult()
        Call GetDisplayType()
        Call GetCssClass()
        For Each g1 As GridViewRow In Grd_Group2.Rows
            g1.FindControl("btnedit_group2").Visible = False
        Next
        '----------------------------------------------
        Dim sqlCon2 As New SqlCommand("ACT.usp_ReportConfig_List", Str_Conn_OASIS_CURRICULUM)
        Str_Conn_OASIS_CURRICULUM.Open()
        sqlCon2.CommandType = CommandType.StoredProcedure
        With sqlCon2
            .Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = Session("sBsuid").ToString
            .Parameters.Add("@intRSMID", SqlDbType.Int).Value = strSelrsmID
        End With
        Dim myReader As SqlDataReader = sqlCon2.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read()
                DrpAcademicYear.SelectedValue = Convert.ToInt32(myReader("RSM_ACD_ID"))
                txtRptDescription.Text = Convert.ToString(myReader("RSM_DESCR"))
                ChkFinalReports.Checked = IIf(IsDBNull(myReader("RSM_bFINALREPORT")), 0, 1)
                ChkAolPremium.Checked = IIf(IsDBNull(myReader("RSM_bAOLPROCESSING")), 0, 1)
                TxtReportType.Text = IIf(IsDBNull(myReader("RSM_REPORTTYPE")), "", Convert.ToString(myReader("RSM_REPORTTYPE")))
                TxtReportFormat.Text = IIf(IsDBNull(myReader("RSM_REPORTFORMAT")), "", Convert.ToString(myReader("RSM_REPORTFORMAT")))
            Loop
        End If
        myReader.Close()

        Using cmd As New SqlCommand("ACT.usp_ReportConfig_List", Str_Conn_OASIS_CURRICULUM)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@strBsuID", SqlDbType.VarChar).Value = Session("sBsuid").ToString
            cmd.Parameters.Add("@intRSMID", SqlDbType.Int).Value = strSelrsmID
            Using adapter As New SqlDataAdapter(cmd)
                Call GetAdcGrade()
                Dim ds1 As New DataSet()
                adapter.Fill(ds1)
                For i As Integer = 0 To ds1.Tables(1).Rows.Count - 1
                    For Each item As ListItem In ChkGradeList.Items
                        If item.Text = ds1.Tables(1).Rows(i)("GRM_DISPLAY") Then
                            item.Selected = True
                        End If
                    Next
                Next
                '-------------------------
                Dim ds2 As New DataSet()
                adapter.Fill(ds2)
                With Grd_Group2
                    .DataSource = ds2.Tables(3)
                    .DataBind()
                    'ViewState("RptTable_Group2") = ds2.Tables(2)    Comment by Nikunj Aghera (10-Feb-2020) Desc : Wrong table data bind in view state.
                    ViewState("RptTable_Group2") = ds2.Tables(3)
                End With
                '-------------------------
                Dim ds3 As New DataSet()
                adapter.Fill(ds3)
                With Grd_Group3
                    .DataSource = ds3.Tables(4)
                    .DataBind()
                    'ViewState("RptTable_Group3") = ds3.Tables(3)   Comment by Nikunj Aghera (10-Feb-2020) Desc : Wrong table data bind in view state.
                    ViewState("RptTable_Group3") = ds3.Tables(4)
                End With
            End Using
        End Using
    End Sub
    Sub GetCssClass()
        DrpCCSClass.Items.Clear()
        DrpCCSClass.Items.Add("")
        DrpCCSClass.Items.Add("TEXTBOXSMALL")
        DrpCCSClass.Items.Add("TEXTBOXMEDIUM")  'textboxmedium
        DrpCCSClass.Items.Add("TEXTBOXMULTI")  'textboxmulti
    End Sub
    Sub GetDisplayType()
        DrpDisplayType.Items.Clear()
        DrpDisplayType.Items.Add("")
        DrpDisplayType.Items.Add("COMMENT")
        DrpDisplayType.Items.Add("GRADE")
        DrpDisplayType.Items.Add("MARK")
    End Sub
    Sub GetRptResult()
        DrpResult.Items.Clear()
        DrpResult.Items.Add("")
        DrpResult.Items.Add("C")
        DrpResult.Items.Add("D")
        DrpResult.Items.Add("M")
    End Sub
    Sub GetAdcGrade()
        ChkGradeList.Items.Clear()
        Try
            If Str_Conn_OASIS.State = ConnectionState.Closed Then Str_Conn_OASIS.Open()
            Dim cmd As New SqlCommand()
            cmd.CommandText = "OASIS.usp_Get_All_GradesFrom_AcdID"
            cmd.Connection = Str_Conn_OASIS
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = Session("sBSUID")
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = DrpAcademicYear.SelectedValue
            Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
            If sqlReader.HasRows Then
                With ChkGradeList
                    .DataSource = sqlReader
                    .DataTextField = "GRM_DISPLAY"  '"GRM_DESCR"
                    .DataValueField = "GRM_GRD_ID"
                    .DataBind()
                End With
            End If
        Catch ex As SqlException
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed" & ex.Message.ToString
        Finally
            Str_Conn_OASIS.Close()
        End Try
    End Sub
    Protected Sub DrpAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpAcademicYear.SelectedIndexChanged
        Call GetAdcGrade()
        Call GetRptResult()
    End Sub
    Protected Sub DrpResult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpResult.SelectedIndexChanged
    End Sub
    Protected Sub DrpDisplayType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpDisplayType.SelectedIndexChanged
    End Sub
    Protected Sub DrpCCSClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpCCSClass.SelectedIndexChanged
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Call ValidateAndInsertUpdateData()
    End Sub
    Protected Sub ValidateAndInsertUpdateData()
        ViewState("RptTable_Group2") = Nothing
        ViewState("RptTable_Group3") = Nothing

        Dim strDataMode As String = btnSave.Text.ToString
        Dim intRSM_ID As Int32
        Dim strValidGUID As String
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
        '-----------------------------------------------
        If CStr(ViewState("datamode")) = "edit" Then
            strValidGUID = CStr(ViewState("strSelGuid"))
        Else
            strValidGUID = strGUID
        End If
        '---CheckList box count
        For Each lItem As ListItem In ChkGradeList.Items
            If lItem.Selected = True Then
                icount += 1
            End If
        Next
        If DrpAcademicYear.SelectedValue.ToString() = "" Or
            txtRptDescription.Text = "" Or
            Grd_Group2.Rows.Count = 0 Or
            Grd_Group3.Rows.Count = 0 Or
            icount = 0 Then
            lblError.Text = "Fields Marked with (*) are mandatory"
        Else
            Try
                If Str_Conn_OASIS_CURRICULUM.State = ConnectionState.Closed Then
                    Str_Conn_OASIS_CURRICULUM.Open()
                End If

                'Report Types
                If CStr(ViewState("datamode")) = "edit" Then
                    intRSM_ID = hdf_rsmid.Value
                    Dim sqlComm01 As New SqlCommand("ACT.usp_ReportConfig_header_Entry", Str_Conn_OASIS_CURRICULUM)
                    sqlComm01.CommandType = CommandType.StoredProcedure
                    With sqlComm01
                        .Parameters.AddWithValue("@strGUID", strValidGUID)
                        .Parameters.AddWithValue("@strBsuID", Session("sBsuid").ToString)
                        .Parameters.AddWithValue("@strRptAcaYearID", DrpAcademicYear.SelectedValue.ToString())
                        .Parameters.AddWithValue("@strRptDescription", Trim$(txtRptDescription.Text.ToString()))
                        .Parameters.AddWithValue("@intRptFinalReports", IIf(ChkFinalReports.Checked = True, 1, 0))
                        .Parameters.AddWithValue("@intRptAolPremium", IIf(ChkAolPremium.Checked = True, 1, 0))
                        .Parameters.AddWithValue("@strRptType", Trim$(TxtReportType.Text.ToString()))
                        .Parameters.AddWithValue("@strRptFormat", Trim$(TxtReportFormat.Text.ToString()))
                        .Parameters.AddWithValue("@strMode", strDataMode)
                        .Parameters.AddWithValue("@intDisplayOrder", 1)
                        .ExecuteNonQuery()
                    End With
                Else
                    Dim sp_name As String = "ACT.usp_ReportConfig_header_Entry"
                    Dim Trans As SqlTransaction = Nothing
                    Try
                        Dim objconn As New SqlConnection(str_conn)
                        objconn.Open()
                        Trans = objconn.BeginTransaction
                        Dim param(9) As SqlParameter
                        param(0) = Mainclass.CreateSqlParameter("@strGUID", strGUID, SqlDbType.Text)
                        param(1) = Mainclass.CreateSqlParameter("@strBsuID", Session("sBsuid").ToString, SqlDbType.Text)
                        param(2) = Mainclass.CreateSqlParameter("@strRptAcaYearID", DrpAcademicYear.SelectedValue.ToString(), SqlDbType.Int)
                        param(3) = Mainclass.CreateSqlParameter("@strRptDescription", Trim$(txtRptDescription.Text.ToString()), SqlDbType.Text)
                        param(4) = Mainclass.CreateSqlParameter("@intRptFinalReports", IIf(ChkFinalReports.Checked = True, 1, 0), SqlDbType.BigInt)
                        param(5) = Mainclass.CreateSqlParameter("@intRptAolPremium", IIf(ChkAolPremium.Checked = True, 1, 0), SqlDbType.BigInt)
                        param(6) = Mainclass.CreateSqlParameter("@strRptType", Trim$(TxtReportType.Text.ToString()), SqlDbType.Text)
                        param(7) = Mainclass.CreateSqlParameter("@strRptFormat", Trim$(TxtReportFormat.Text.ToString()), SqlDbType.Text)
                        param(8) = Mainclass.CreateSqlParameter("@strMode", strDataMode, SqlDbType.Text)
                        param(9) = Mainclass.CreateSqlParameter("@intDisplayOrder", 1, SqlDbType.Int)
                        intRSM_ID = SqlHelper.ExecuteScalar(Trans, sp_name, param)
                        Trans.Commit()
                    Catch ex As Exception
                        Trans.Rollback()
                    End Try
                End If

                'Grades
                For Each item As ListItem In ChkGradeList.Items
                    If item.Selected Then
                        Dim sqlComm1 As New SqlCommand("ACT.usp_ReportConfig_Grade_Entry", Str_Conn_OASIS_CURRICULUM)
                        sqlComm1.CommandType = CommandType.StoredProcedure
                        With sqlComm1
                            .Parameters.AddWithValue("@strGUID", strValidGUID)
                            .Parameters.AddWithValue("@strRSMID", intRSM_ID)
                            .Parameters.AddWithValue("@strRptGrade", item.Value.ToString())
                            .Parameters.AddWithValue("@strMode", strDataMode)
                            .Parameters.AddWithValue("@intGrdDisplayOrder", Convert.ToInt32(ChkGradeList.Items.IndexOf(item)) + 1)
                            .ExecuteNonQuery()
                        End With
                    End If
                Next

                'Report Headers
                For Each g1 As GridViewRow In Grd_Group2.Rows
                    Dim sqlComm2 As New SqlCommand("ACT.usp_ReportConfig_Details_Enty", Str_Conn_OASIS_CURRICULUM)
                    sqlComm2.CommandType = CommandType.StoredProcedure
                    With sqlComm2
                        .Parameters.AddWithValue("@strGUID", strValidGUID)
                        .Parameters.AddWithValue("@strBsuID", Session("sBsuid").ToString)
                        .Parameters.AddWithValue("@intRSMID", intRSM_ID)
                        .Parameters.AddWithValue("@strRptHeader", CType(g1.FindControl("lblRpt_HeaderName"), Label).Text)
                        .Parameters.AddWithValue("@strRptDisplayType", CType(g1.FindControl("lblRpt_DisplayType"), Label).Text)
                        .Parameters.AddWithValue("@intRptDirectEntry", IIf(CType(g1.FindControl("lblRpt_DirectEntry"), Label).Text = "YES", 1, 0))
                        .Parameters.AddWithValue("@intRptAllSubject", IIf(CType(g1.FindControl("lblRpt_AllSubject"), Label).Text = "YES", 1, 0))
                        .Parameters.AddWithValue("@intRptRulerequired", IIf(CType(g1.FindControl("lblRpt_RuleReq"), Label).Text = "YES", 1, 0))
                        .Parameters.AddWithValue("@intRptPerfRequired", IIf(CType(g1.FindControl("lblRpt_PerfoReq"), Label).Text = "YES", 1, 0))
                        .Parameters.AddWithValue("@intRptGraph", IIf(CType(g1.FindControl("lblRpt_Graph"), Label).Text = "YES", 1, 0))
                        .Parameters.AddWithValue("@strRptCSSClass", CType(g1.FindControl("lblRpt_CssClass"), Label).Text)
                        .Parameters.AddWithValue("@strRptResult", CType(g1.FindControl("lblRpt_Result"), Label).Text)
                        .Parameters.AddWithValue("@intRptFinalReports", IIf(ChkFinalReports.Checked = True, 1, 0))
                        .Parameters.AddWithValue("@strMode", strDataMode)
                        .Parameters.AddWithValue("@strdatamode", btnAddNew_Group2.Text.ToString)
                        .Parameters.AddWithValue("@strID", CType(g1.FindControl("lblrpt_HeaderID"), Label).Text)
                        .Parameters.AddWithValue("@intDisplayOrder", Convert.ToInt32(CType(g1.FindControl("lblSrno"), Label).Text))
                        .ExecuteNonQuery()
                    End With
                Next

                'Report Names 
                For Each g2 As GridViewRow In Grd_Group3.Rows
                    Dim sqlComm3 As New SqlCommand("ACT.usp_ReportConfig_RptNameDetails_Entry", Str_Conn_OASIS_CURRICULUM)
                    sqlComm3.CommandType = CommandType.StoredProcedure
                    With sqlComm3
                        .Parameters.AddWithValue("@strGUID", strValidGUID)
                        .Parameters.AddWithValue("@intRSMID", intRSM_ID)
                        .Parameters.AddWithValue("@strRptName", CType(g2.FindControl("lblRpt_name"), Label).Text)
                        .Parameters.AddWithValue("@strMode", strDataMode)
                        .Parameters.AddWithValue("@strRptID", CType(g2.FindControl("lblrpt_ReportID"), Label).Text)
                        .Parameters.AddWithValue("@intDisplayOrder", Convert.ToInt32(CType(g2.FindControl("lblSrno_rptname"), Label).Text))
                        .ExecuteNonQuery()
                    End With
                Next
                Str_Conn_OASIS_CURRICULUM.Close()
                Call ClearFields() '----Comments
                lblError.Text = "Record (s) saved sucessfully !  "
                btnSave.Text = "Save"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed  " & ex.Message
            End Try
        End If
    End Sub
    Sub ClearFields()
        lblError.Text = ""
        txtRptDescription.Text = ""
        TxtReportType.Text = ""
        TxtReportFormat.Text = ""
        TxtRptHeader.Text = ""
        DrpDisplayType.Text = ""
        DrpCCSClass.Text = ""
        DrpResult.Text = ""
        TxtRptName.Text = ""
        ChkFinalReports.Checked = False
        ChkGradeList.ClearSelection()
        ChkDirectEntry.Checked = False
        ChkAllSubject.Checked = False
        ChkRuleReq.Checked = False
        ChkPerReq.Checked = False
        ChkGraphReq.Checked = False
        studClass.PopulateAcademicYear(DrpAcademicYear, CStr(Session("clm")), CStr(Session("sBSUID")))
        ChkAolPremium.Checked = False
        Grd_Group3.DataSource = Nothing
        Grd_Group2.DataSource = Nothing
        Grd_Group3.DataBind()
        Grd_Group2.DataBind()
        '----------------------
        Call GetAdcGrade()
        Call GetRptResult()
        Call GetDisplayType()
        Call GetCssClass()
        MaintainScrollPositionOnPostBack = True
    End Sub
    Protected Sub btnAddNew_Group3_Click(sender As Object, e As EventArgs) Handles btnAddNew_Group3.Click
        If TxtRptName.Text = "" Then
            lblError.Text = "Fields Marked with (*) are mandatory"
        Else
            Call BindData_Group3()
        End If
    End Sub
    Sub BindData_Group3()
        If btnAddNew_Group3.Text = "Update" Then
            Dim dr As DataRow = CType(ViewState("RptTable_Group3"), DataTable).Rows(ViewState("RowIDGroup3"))
            'dr.Item("ReportName_ID") = 0
            dr.Item("Report_Name") = Trim$(TxtRptName.Text.ToString)
            Grd_Group3.DataSource = ViewState("RptTable_Group3")
            Grd_Group3.DataBind()
            TxtRptName.Text = ""
            btnAddNew_Group3.Text = "Add New"
        ElseIf btnAddNew_Group3.Text = "Add New" Then
            Dim dt As New DataTable
            Try
                If ViewState("RptTable_Group3") Is Nothing Then
                    dt = New DataTable()
                    dt.Columns.AddRange(New DataColumn() {New DataColumn("ReportName_ID", GetType(String)),
                                                          New DataColumn("Report_Name", GetType(String))}) 'Add multiple array here
                Else
                    dt = TryCast(ViewState("RptTable_Group3"), DataTable)
                End If
                dt.Rows.Add(
                            0,
                            TxtRptName.Text)  'Add multiple array here
                Grd_Group3.DataSource = dt
                Grd_Group3.DataBind()
                ViewState("RptTable_Group3") = dt
                TxtRptName.Text = ""
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed  " & ex.Message
            End Try
        End If
    End Sub
    Protected Sub btnAddNew_Group2_Click(sender As Object, e As EventArgs) Handles btnAddNew_Group2.Click
        Call BindData_Group2()
    End Sub
    Sub BindData_Group2()
        If btnAddNew_Group2.Text = "Update" Then
            Dim dr As DataRow = CType(ViewState("RptTable_Group2"), DataTable).Rows(ViewState("RowIDGroup2"))
            ' dr.Item("Rpt_ID") = 0
            dr.Item("Rpt_HeaderName") = TxtRptHeader.Text.ToString.Trim()
            dr.Item("Rpt_DisplayType") = DrpDisplayType.SelectedItem.Text
            dr.Item("Rpt_DirectEntry") = IIf(ChkDirectEntry.Checked = True, "YES", "No")
            dr.Item("Rpt_AllSubject") = IIf(ChkAllSubject.Checked = True, "YES", "No")
            dr.Item("Rpt_RuleReq") = IIf(ChkRuleReq.Checked = True, "YES", "No")
            dr.Item("Rpt_PerfoReq") = IIf(ChkPerReq.Checked = True, "YES", "No")
            dr.Item("Rpt_Graph") = IIf(ChkGraphReq.Checked = True, "YES", "No")
            dr.Item("Rpt_CssClass") = DrpCCSClass.SelectedItem.Text.ToString
            dr.Item("Rpt_Result") = DrpResult.Text.ToString
            Grd_Group2.DataSource = ViewState("RptTable_Group2")
            Grd_Group2.DataBind()
            '----------------------
            TxtRptHeader.Text = ""
            'Call GetAdcGrade()
            Call GetRptResult()
            Call GetDisplayType()
            Call GetCssClass()
            ChkAllSubject.Checked = False
            ChkDirectEntry.Checked = False
            ChkRuleReq.Checked = False
            ChkPerReq.Checked = False
            ChkGraphReq.Checked = False
            btnAddNew_Group2.Text = "Add New"
        ElseIf btnAddNew_Group2.Text = "Add New" Then
            Dim dt As New DataTable
            Dim keys() As DataColumn
            ReDim keys(8)
            If TxtRptHeader.Text = "" Then
                lblError.Text = "Fields Marked with (*) are mandatory"
            Else
                Try
                    If ViewState("RptTable_Group2") Is Nothing Then
                        dt = New DataTable()
                        dt.Columns.AddRange(New DataColumn() {New DataColumn("Rpt_ID", GetType(String)),
                                                              New DataColumn("Rpt_HeaderName", GetType(String)),
                                                              New DataColumn("Rpt_DisplayType", GetType(String)),
                                                              New DataColumn("Rpt_DirectEntry", GetType(String)),
                                                              New DataColumn("Rpt_AllSubject", GetType(String)),
                                                              New DataColumn("Rpt_RuleReq", GetType(String)),
                                                              New DataColumn("Rpt_PerfoReq", GetType(String)),
                                                              New DataColumn("Rpt_Graph", GetType(String)),
                                                              New DataColumn("Rpt_CssClass", GetType(String)),
                                                              New DataColumn("Rpt_Result", GetType(String))})
                    Else
                        dt = TryCast(ViewState("RptTable_Group2"), DataTable)
                    End If
                    dt.Rows.Add(
                                0,
                                TxtRptHeader.Text.ToString.Trim(),
                                DrpDisplayType.SelectedItem.Text,
                                IIf(ChkDirectEntry.Checked = True, "YES", "NO"),
                                IIf(ChkAllSubject.Checked = True, "YES", "NO"),
                                IIf(ChkRuleReq.Checked = True, "YES", "NO"),
                                IIf(ChkPerReq.Checked = True, "YES", "NO"),
                                IIf(ChkGraphReq.Checked = True, "YES", "NO"),
                                DrpCCSClass.SelectedItem.Text.ToString,
                                DrpResult.Text.ToString
                                )
                    Grd_Group2.DataSource = dt
                    Grd_Group2.DataBind()
                    ViewState("RptTable_Group2") = dt
                    TxtRptHeader.Text = ""
                    DrpDisplayType.ClearSelection()
                    ChkDirectEntry.Checked = False
                    ChkAllSubject.Checked = False
                    ChkRuleReq.Checked = False
                    ChkPerReq.Checked = False
                    ChkGraphReq.Checked = False
                    DrpCCSClass.ClearSelection()
                    DrpResult.ClearSelection()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed  " & ex.Message
                End Try
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        Dim url As String
        url = String.Format("~\Curriculum\clmReportCardSetup_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub
    Protected Sub Grd_Group3_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Grd_Group3.RowDataBound
        If CStr(ViewState("datamode")) = "view" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Controls(0).Visible = False
                e.Row.Cells(4).Controls(0).Visible = False
            End If
        End If
        If CStr(ViewState("datamode")) = "edit" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Controls(0).Visible = False
            End If
        End If
    End Sub
    Protected Sub Grd_Group3_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles Grd_Group3.RowDeleting
        If ViewState("datamode") = "edit" Then
            If Grd_Group3.Rows.Count = 1 Then
                lblError.Text = "Atleast one entry is mandatory"
            Else
                Dim dt As DataTable = ViewState("RptTable_Group3")
                dt.Rows.RemoveAt(e.RowIndex)
                dt.AcceptChanges()
                Grd_Group3.DataSource = dt
                Grd_Group3.DataBind()
            End If
        Else
            Dim dt As DataTable = ViewState("RptTable_Group3")
            dt.Rows.RemoveAt(e.RowIndex)
            dt.AcceptChanges()
            Grd_Group3.DataSource = dt
            Grd_Group3.DataBind()
        End If       
    End Sub
    Protected Sub Grd_Group2_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Grd_Group2.RowDataBound
        If CStr(ViewState("datamode")) = "view" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(11).Controls(0).Visible = False
                e.Row.Cells(12).Controls(0).Visible = False
            End If
        End If
        If CStr(ViewState("datamode")) = "edit" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(12).Controls(0).Visible = False
            End If
        End If
    End Sub
    Protected Sub Grd_Group2_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles Grd_Group2.RowDeleting
        If ViewState("datamode") = "edit" Then
            If Grd_Group2.Rows.Count = 1 Then
                lblError.Text = "Atleast one entry is mandatory"
            Else

                Dim dt As DataTable = ViewState("RptTable_Group2")
                dt.Rows.RemoveAt(e.RowIndex)
                dt.AcceptChanges()
                Grd_Group2.DataSource = dt
                ViewState("RptTable_Group2") = dt
                Grd_Group2.DataBind()

            End If
        Else
            Dim dt As DataTable = ViewState("RptTable_Group2")
            dt.Rows.RemoveAt(e.RowIndex)
            dt.AcceptChanges()
            Grd_Group2.DataSource = dt
            ViewState("RptTable_Group2") = dt
            Grd_Group2.DataBind()
        End If
    End Sub
    Protected Sub Grd_Group2_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles Grd_Group2.RowEditing
        Dim GrdSelEditRowGroup2 As Integer = e.NewEditIndex
        ViewState("RowIDGroup2") = e.NewEditIndex
        BolAddEdit = True
        TxtRptHeader.Text = CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_HeaderName"), Label).Text
        DrpDisplayType.Text = CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_DisplayType"), Label).Text
        ChkDirectEntry.Checked = IIf(CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_DirectEntry"), Label).Text = "YES", 1, 0)
        ChkAllSubject.Checked = IIf(CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_AllSubject"), Label).Text = "YES", 1, 0)
        ChkRuleReq.Checked = IIf(CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_RuleReq"), Label).Text = "YES", 1, 0)
        ChkPerReq.Checked = IIf(CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_PerfoReq"), Label).Text = "YES", 1, 0)
        ChkGraphReq.Checked = IIf(CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_Graph"), Label).Text = "YES", 1, 0)
        DrpCCSClass.Text = CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_CssClass"), Label).Text
        DrpResult.Text = CType(Grd_Group2.Rows(GrdSelEditRowGroup2).FindControl("lblRpt_Result"), Label).Text
        btnAddNew_Group2.Text = "Update"
    End Sub
    Protected Sub Grd_Group3_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles Grd_Group3.RowEditing
        ViewState("RowIDGroup3") = e.NewEditIndex
        TxtRptName.Text = CType(Grd_Group3.Rows(e.NewEditIndex).FindControl("lblRpt_name"), Label).Text
        btnAddNew_Group3.Text = "Update"
    End Sub
End Class
