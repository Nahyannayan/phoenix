﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectAssessmentReport_ConsolidatedReport.aspx.vb" Inherits="Curriculum_Reports_Aspx_clmSubjectAssessmentReport_ConsolidatedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
        
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 37) == 'ctl00_cphMasterpage_cblReportSchedule') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Assessment Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left"  />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" ><asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            SkinID="error" Style="text-align: center" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td  style="font-weight: bold;" valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnGetData">
                                <table align="center" class="BlueTableView" width="100%" cellpadding="5"
                                    cellspacing="0"  id="tb1" runat="server">
                                    
                                    <tr>
                                        <td align="left"   width="20%"><span class="field-label">Select Academic Year</span> </td>                                     
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;</td>
                                        <td align="left"   width="20%"><span class="field-label">Select Report Card</span> </td>                                       
                                        <td align="left" width="30%" >
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  width="20%" ><span class="field-label">Select Grade</span> </td>                                        
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList></td>
                                        <td align="left"  width="20%" ><span class="field-label">Select Section</span></td>                                      
                                        <td align="left"  width="30%">
                                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList></td>

                                    </tr>
                                    <tr>
                                        <td align="left"   width="20%" ><span class="field-label">Report Schedule</span> </td>
                                    
                                        <td align="left" width="30%" >
                                            
                                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll2(this);" runat="server"
                                                Text="Select All" AutoPostBack="True" /><br />
                                            <div class="checkbox-list "><asp:CheckBoxList ID="cblReportSchedule" runat="server" AutoPostBack="True" 
                                                  RepeatLayout="Flow" 
                                                >
                                            </asp:CheckBoxList></div>
                                        </td>
                                        <td align="left"    width="20%"><span class="field-label">Select Subject</span> </td>
                                     
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>

                                    </tr>
                                    <tr>
                                        <td align="center"  colspan="4">
                                            <asp:CheckBox ID="chkBlank" runat="server" /><br />
                                            <asp:Button ID="btnGetData" runat="server" CssClass="button" TabIndex="7" Text="Generate Report"
                                                ValidationGroup="groupM1" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDownReport" runat="server" CssClass="button" TabIndex="7" Text="Download Report"
                                    ValidationGroup="groupM1"  />
                                            &nbsp;&nbsp;&nbsp;
                                <asp:Button
                                    ID="btnDownExcel" runat="server" CssClass="button" TabIndex="7"
                                    Text="Download in Excel" ValidationGroup="groupM1"  />
                                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                            </CR:CrystalReportSource>

                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfgetData" runat="server" />
                <asp:HiddenField ID="hfDownloadReport" runat="server" />
                <asp:HiddenField ID="hfDownloadGraph" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
