Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAuthorized_Leave_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100045") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    ' h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_Sid_img = h_Selected_menu_1.Value.Split("__")
        'getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
       
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))



    End Sub

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvCAD_DRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCAD_DRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvCAD_DRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCAD_DRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvCAD_DRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCAD_DRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvCAD_DRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCAD_DRecord.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_AllocatedBy As String = String.Empty
            Dim str_filter_GRD_DESC As String = String.Empty
            Dim str_filter_TRM_DESC As String = String.Empty
            Dim str_filter_CAD_DESC As String = String.Empty
            Dim str_filter_CAM_DESC As String = String.Empty

            Dim ds As New DataSet

            str_Sql = " SELECT DISTINCT SGM_ID, SGM_DESCR, SGM_BSU_ID, " & _
            "VW_ACADEMICYEAR_M.ACY_DESCR, VW_GRADE_BSU_M.GRM_DISPLAY " & _
            "FROM SUBJECTOPTION_GROUP_M INNER JOIN " & _
            "vw_ACADEMICYEAR_D ON SUBJECTOPTION_GROUP_M.SGM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID " & _
            " AND SUBJECTOPTION_GROUP_M.SGM_BSU_ID = vw_ACADEMICYEAR_D.ACD_BSU_ID  " & _
            "INNER JOIN VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID  " & _
            "INNER JOIN VW_GRADE_BSU_M ON SUBJECTOPTION_GROUP_M.SGM_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
            "SUBJECTOPTION_GROUP_M.SGM_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID AND " & _
            "SUBJECTOPTION_GROUP_M.SGM_BSU_ID = VW_GRADE_BSU_M.GRM_BSU_ID " & _
            " WHERE SGM_BSU_ID = '" & Session("sBSUID") & "' "


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String

            Dim str_AllocatedBy As String = String.Empty
            'Dim str_GRD_DESC As String = String.Empty
            Dim str_TRM_DESC As String = String.Empty
            Dim str_CAD_DESC As String = String.Empty
            Dim str_CAM_DESC As String = String.Empty

            If gvCAD_DRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_Sid_search = h_Selected_menu_1.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtGRD_DESC")
                'str_GRD_DESC = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_GRD_DESC = " AND a.GRD_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_GRD_DESC = "  AND  NOT a.GRD_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_GRD_DESC = " AND a.GRD_DESC  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_GRD_DESC = " AND a.GRD_DESC NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_GRD_DESC = " AND a.GRD_DESC LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_GRD_DESC = " AND a.GRD_DESC NOT LIKE '%" & txtSearch.Text & "'"
                'End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtTRM_DESC")
                If Not txtSearch Is Nothing Then
                    str_TRM_DESC = txtSearch.Text
                Else

                End If


                'If str_search = "LI" Then
                '    str_filter_TRM_DESC = " AND a.TRM_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_TRM_DESC = "  AND  NOT a.TRM_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_TRM_DESC = " AND a.TRM_DESC  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_TRM_DESC = " AND a.TRM_DESC NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_TRM_DESC = " AND a.TRM_DESC LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_TRM_DESC = " AND a.TRM_DESC NOT LIKE '%" & txtSearch.Text & "'"
                'End If


                'str_Sid_search = h_Selected_menu_3.Value.Split("__")
                'str_search = str_Sid_search(0)
                'txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtAllocated_By")
                'str_AllocatedBy = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_AllocatedBy = " AND a.Allocated_By LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_AllocatedBy = "  AND  NOT a.Allocated_By  LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_AllocatedBy = " AND a.Allocated_By   LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_AllocatedBy = " AND a.Allocated_By  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_AllocatedBy = " AND a.Allocated_By LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_AllocatedBy = " AND a.Allocated_By  NOT LIKE '%" & txtSearch.Text & "'"
                'End If


                'str_Sid_search = h_Selected_menu_4.Value.Split("__")
                'str_search = str_Sid_search(0)
                'txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtCAD_DESC")
                'str_CAD_DESC = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_CAD_DESC = " AND a.CAD_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_CAD_DESC = "  AND  NOT a.CAD_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_CAD_DESC = " AND a.CAD_DESC  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_CAD_DESC = " AND a.CAD_DESC NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_CAD_DESC = " AND a.CAD_DESC LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_CAD_DESC = " AND a.CAD_DESC NOT LIKE '%" & txtSearch.Text & "'"
                'End If

                'str_Sid_search = h_Selected_menu_5.Value.Split("__")
                'str_search = str_Sid_search(0)
                'txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtCAM_DESC")
                'str_CAD_DESC = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_CAM_DESC = " AND a.CAM_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_CAM_DESC = "  AND  NOT a.CAM_DESC LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_CAM_DESC = " AND a.CAM_DESC  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_CAM_DESC = " AND a.CAM_DESC NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_CAM_DESC = " AND a.CAM_DESC LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_CAM_DESC = " AND a.CAM_DESC NOT LIKE '%" & txtSearch.Text & "'"
                'End If
            End If

            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_AllocatedBy & str_filter_TRM_DESC & str_filter_CAD_DESC & str_filter_CAM_DESC & " ORDER BY a.CAM_DESC")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvCAD_DRecord.DataSource = ds.Tables(0)
                gvCAD_DRecord.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCAD_DRecord.DataSource = ds.Tables(0)
                Try
                    gvCAD_DRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCAD_DRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCAD_DRecord.Rows(0).Cells.Clear()
                gvCAD_DRecord.Rows(0).Cells.Add(New TableCell)
                gvCAD_DRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCAD_DRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCAD_DRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            'txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtGRD_DESC")
            'txtSearch.Text = str_GRD_DESC
            txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtAllocated_By")
            txtSearch.Text = str_AllocatedBy
            txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtCAD_DESC")
            txtSearch.Text = str_CAD_DESC
            txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtCAM_DESC")
            txtSearch.Text = str_CAM_DESC
            txtSearch = gvCAD_DRecord.HeaderRow.FindControl("txtTRM_DESC")
            txtSearch.Text = str_TRM_DESC
            'Call callYEAR_DESCRBind()

            'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCAD_DRecord.PageIndexChanging
        gvCAD_DRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub lblView_Click(ByVal viewID As String)

        Try
            Dim url As String
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("clmOptionGroups.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()

    End Sub


    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Curriculum\clmOptionGroups.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub


    Protected Sub gvCAD_DRecord_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCAD_DRecord.RowCommand
        If e.CommandName = "View" Then
            lblView_Click(e.CommandArgument)
        End If
    End Sub
End Class
