<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeGroup_M.aspx.vb" Inherits="Curriculum_clmChangeGroup_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_move()
{

  if (confirm("You are about to move the selected students from this group.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
 function getSubject() 
 {        
            var sFeatures;
            sFeatures="dialogWidth: 445px; ";
            sFeatures+="dialogHeight: 310px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
           // url='../Students/ShowAcademicInfo.aspx?id='+mode;
         
           url=document.getElementById("<%=hfSubjectURL.ClientID %>").value
                     
            result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false; 
            }   
             NameandCode = result.split('|');  
             document.getElementById("<%=txtSubject.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfSBG_ID.ClientID %>").value=NameandCode[1];
           
                       
                      
           }
 
       
                
                  
                
     var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvGroup.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}



    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }           
                
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Change Group
        </div>
        <div class="card-body">
            <div class="table-responsive">

   <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1" style="text-align: left" />
                           </td>
        </tr>
        <tr>
         <td align="left"  valign="bottom" >
          <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                      ></asp:Label></td>
        </tr>
        <tr>
            <td valign="top">
                <table id="tblGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    
                     <tr>
             <td align="left" width="20%">
                    <asp:Label ID="lblAccText" runat="server" class="field-label" Text="Select Academic Year"></asp:Label></td>
                    
                 <td align="left" >
                    <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                        </asp:DropDownList></td>
                        
                           <td align="left">
                    <asp:Label ID="lblGrade" runat="server" class="field-label" Text="Select Grade"  ></asp:Label></td>
             
                 <td align="left" >
                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
               
            </tr>
            
             <tr>
             <td align="left" width="20%">
                    <asp:Label ID="lblShift" runat="server" class="field-label" Text="Shift"  ></asp:Label></td>
                 
                 <td align="left">
                    <asp:DropDownList ID="ddlShift" runat="server">
                        </asp:DropDownList></td>
                        
                   <td align="left">
                    <asp:Label ID="lblStream" runat="server" class="field-label" Text="Stream"  ></asp:Label></td>
                   
                 <td align="left">
                    <asp:DropDownList ID="ddlStream" runat="server">
                        </asp:DropDownList></td>
              </tr>          
    
                <tr>
                
                 <td align="left" >
       <asp:Label ID="lbl27" runat="server" class="field-label" Text="Subject"></asp:Label></td>
  
       
       <td align="left">
       <asp:TextBox id="txtSubject" runat="server" Enabled="False" MaxLength="100"
               tabIndex="2" Visible = "false"></asp:TextBox><asp:ImageButton id="imgSub" runat="server"
                   ImageUrl="~/Images/forum_search.gif" OnClientClick="getSubject();return false;" tabIndex="18" Visible="false"></asp:ImageButton>
                   <asp:DropDownList ID="ddlSubjects" runat="server">
                        </asp:DropDownList>
                   </td>
       
          <td align="left" >
       <asp:Label ID="lbl5" runat="server" class="field-label" Text="Teacher" ></asp:Label></td>
     
       
       <td align="left">
           <asp:DropDownList ID="ddlTeacher" runat="server">
       </asp:DropDownList>
       </td>
       
                
                      </tr>
       <tr>     
                        <td  align="center" valign="bottom" colspan="4">
             
                <asp:Button ID="btnList" runat="server" CssClass="button"
                    Text="List" TabIndex="9"  ValidationGroup="groupM1" /></td>
                       </tr>   
                  <tr >
                  <td colspan="4"></td>
                  </tr> 
                  <tr>
                   <td align="left"   >
       <asp:Label ID="lbl4" runat="server" class="field-label" Text="Select Group"></asp:Label></td>
      
       
       <td align="left">
           <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
       </asp:DropDownList></td>
       
       
        <td align="left" >
       <asp:Label ID="lbl3" runat="server" class="field-label" Text="Move to Group"></asp:Label></td>

       
       <td align="left">
       <table><tr><td><asp:DropDownList ID="ddlMTeacher" runat="server" AutoPostBack="True">
       </asp:DropDownList></td></tr><tr><td>
           <asp:DropDownList ID="ddlNewGroup" runat="server" AutoPostBack="True">
       </asp:DropDownList>
       
       </td></tr></table>
       </td>
       
                  </tr>    
                           
                           
                           
                           
             <tr>
             <td colspan="2" style="vertical-align:top " >
             <asp:Panel ID="panel1" runat="server" ScrollBars="Auto" style="height:auto" >
             <table id="tb1" width="100%">
                <tr>
                <td align="center" >
                <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                      PageSize="20" ShowFooter="True" width="100%">
                     <Columns>
                       <asp:TemplateField HeaderText="Available">
                       <EditItemTemplate>
                       <asp:CheckBox ID="chkSelect" runat="server"  />
                       </EditItemTemplate>
                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       <HeaderStyle Wrap="False" />
                        <ItemTemplate>
                       <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                       </ItemTemplate>
                        <HeaderTemplate>
                       <table>
                       <tr>
                      <td align="center">
                     Select <br /><asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
                     ToolTip="Click here to select/deselect all rows" /></td>
                     </tr>
                      </table>
                       </HeaderTemplate>
                     </asp:TemplateField>
                     
                        
                        <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                      <ItemTemplate>
                      <asp:Label ID="lblStuId" runat="server" text='<%# Bind("STU_ID") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="SSD_ID" Visible="False" >
                      <ItemTemplate>
                      <asp:Label ID="lblSsdId" runat="server" text='<%# Bind("SSD_ID") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="SL.No"  >
                      <ItemTemplate>
                      <asp:Label ID="lblSlNo" runat="server" text='<%# getSerialNo() %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                   
                          <asp:TemplateField HeaderText="Stud No.">
                                <HeaderTemplate>
                               
                                  <asp:Label ID="lblh1" runat="server" Text="Stud. No"></asp:Label>
                              <asp:TextBox ID="txtStuNo" runat="server"  ></asp:TextBox>
                               <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo1_Search_Click" />
                                   
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
       
                       <asp:TemplateField HeaderText="Student Name">
                             <HeaderTemplate>
                                
                                  <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label>
                                      <asp:TextBox ID="txtStudName" runat="server"  ></asp:TextBox>
                               <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName1_Search_Click" />
                              
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblStuName" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                           
                           <asp:TemplateField HeaderText="Section">
                        <HeaderTemplate>
                        <table>
                        <tr>
                       <td align="center">
                       Section <br /><asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
                          OnSelectedIndexChanged="ddlgvSection1_SelectedIndexChanged" >
                          </asp:DropDownList></td>
                       </tr>
                         </table>
                         </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                       
                             </Columns>  
                         <HeaderStyle />
                         <RowStyle />
                         <SelectedRowStyle />
                         <AlternatingRowStyle />
                     </asp:GridView>
                </td>
                </tr>
                <tr>
                <td align="left">
                    &nbsp;</td>
                </tr>
                </table>
              </asp:Panel>  
           </td>
           <td colspan="2" style="vertical-align:top ">  
           <asp:Panel ID="panel2" runat="server" style="height:auto" ScrollBars="Auto" >
                <asp:GridView ID="gvNewGroup" runat="server" AutoGenerateColumns="False"
                 CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                      PageSize="20" width="100%"  >
                     <Columns>
                       
                     
                        
                        <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                      <ItemTemplate>
                      <asp:Label ID="lblStuId" runat="server" text='<%# Bind("STU_ID") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="SSD_ID" Visible="False" >
                      <ItemTemplate>
                      <asp:Label ID="lblSsdId" runat="server" text='<%# Bind("SSD_ID") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="SL.No"  >
                      <ItemTemplate>
                      <asp:Label ID="lblSlNo" runat="server" text='<%# getSerialNo() %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                   
                          <asp:TemplateField HeaderText="Stud No.">
                                <HeaderTemplate>
                                
                                  <asp:Label ID="lblh1" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label>
                                       <asp:TextBox ID="txtStuNo" runat="server" ></asp:TextBox>
                               <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo2_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
       
                       <asp:TemplateField HeaderText="Student Name">
                             <HeaderTemplate>
                                
                                  <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label>
                                       <asp:TextBox ID="txtStudName" runat="server" ></asp:TextBox>
                               <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName2_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblStuName" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                           
                           <asp:TemplateField HeaderText="Section">
                        <HeaderTemplate>
                      
                       Section
                           <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
                          OnSelectedIndexChanged="ddlgvSection2_SelectedIndexChanged" >
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                       
                             </Columns>  
                         <HeaderStyle />
                         <RowStyle />
                         <SelectedRowStyle />
                         <AlternatingRowStyle />
                     </asp:GridView>
<%--                <table id="Table1" width="auto">
                <tr height="20px">
                <td align="center" style="width: 6px">
                    &nbsp;</td>
                </tr>
        
                     </table>--%>
                     </asp:Panel>            
               </td>
        </tr>
       
    
        
        <tr><td  align="center" colspan="4"   >
            <asp:Button ID="btnMove" runat="server" CssClass="button" Text="Move" ValidationGroup="groupM1" TabIndex="7"  OnClientClick="javascript:return confirm_move();" /></td>
            </tr>
            
            
               
                </table>
                
                
                
                
         </td>
        </tr>
               <tr>
            <td class="matters" valign="bottom" >
                <asp:HiddenField ID="hfSGR_ID" runat="server" /><asp:HiddenField ID="hfGROUP" runat="server" />
                &nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
            </td>
          
        </tr>
    </table>

            </div>
        </div>
    </div>


    <asp:HiddenField id="hfSubjectURL" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfSBG_ID" runat="server">
    </asp:HiddenField>
    <%--<asp:RequiredFieldValidator id="rfSub" runat="server" ControlToValidate="txtSubject"
        Display="None" ErrorMessage="Please select a subject" ValidationGroup="groupM1">
    </asp:RequiredFieldValidator>--%>
<input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
          
</asp:Content>

