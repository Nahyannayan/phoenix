<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProcessPromotion.aspx.vb" Inherits="Curriculum_clmProcessPromotion" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal id="ltLabel" runat="server" Text="Process Rank"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td align="left">
                
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
               
            </td>
        </tr>
        <tr>
            <td   valign="bottom">
                <table align="center" width="100%">
                   
                    <tr>
                        <td align="left" >
                           <span class="field-label"> Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>

                        <td align="left" >
                            <span class="field-label">Select Grade</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlGrade" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Section</span></td>
                       
                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                            <asp:CheckBoxList id="lstSection" runat="server" RepeatColumns="12" RepeatDirection="Horizontal" Width="100%">
                            </asp:CheckBoxList></div>
                            </td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td  valign="bottom">
            </td>
        </tr>
        <tr align="center">
            <td   valign="bottom">
                
                <asp:Button id="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process"  ValidationGroup="AttGroup"  /></td>
        </tr>
        <tr>
            <td   valign="bottom">
                <asp:HiddenField id="hfbFinalReport" runat="server">
                </asp:HiddenField>
                </td>
        </tr>
    </table>
              </div>
            </div>
     </div>
            

</asp:Content>

