Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Partial Class Curriculum_clmPathfinderProcess_GradeSection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C3300160" And ViewState("MainMnu_code") <> "C330128") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    BindSection()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private methods"


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSection()
        Dim li As New ListItem
        lstSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + Session("Current_ACD_ID")

        str_query += " AND GRM_GRD_ID='10'"

        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSection.DataSource = ds
        lstSection.DataTextField = "SCT_DESCR"
        lstSection.DataValueField = "SCT_ID"
        lstSection.DataBind()

    End Sub

    Sub SavePathfinderData(ByVal strSections As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [PATHFINDER].[PROCESSMARKS] " _
                              & Session("Current_ACD_ID") + "," _
                              & "'" + strSections + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblError.Text = "Record Saved Successfully"
    End Sub

    Sub SaveStreamAllocationMarks(ByVal strSections As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [STREAM].[STUDENT_PROCESSSTREAMALLOCATION] " _
                              & Session("Current_ACD_ID") + "," _
                              & "'" + strSections + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblError.Text = "Record Saved Successfully"
    End Sub

#End Region






    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim strSections As String = ""
        Dim i As Integer
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If strSections <> "" Then
                    strSections += "|"
                End If
                strSections += lstSection.Items(i).Value
            End If
        Next

        If strSections = "" Then
            lblError.Text = "Please select a section"
            Exit Sub
        End If

        If ViewState("MainMnu_code") = "C330128" Then
            SaveStreamAllocationMarks(strSections)
        Else
            SavePathfinderData(strSections)
        End If


    End Sub




End Class
