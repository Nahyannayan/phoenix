<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentMarks_View.aspx.vb" Inherits="Curriculum_rptStudentMarks_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
<script language="javascript" type="text/javascript">
      function GetPopUp(id)
       {     
            //alert(id);
            var sFeatures;
            sFeatures="dialogWidth: 429px; ";
            sFeatures+="dialogHeight: 345px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var acdId;
            var sbgId;
            var sgr_Descr;
            
           if(id==1)//Student Info
           {
                var GradeId;
                var RepSch;
                var RepHeader;
                //GradeId=1;//document.getElementById('<%=ddlGrade.ClientID %>').SelectedValue;
                GradeId=document.getElementById('<%=h_GRD_ID.ClientID %>').value;
                acdId=document.getElementById('<%=h_ACD_ID.ClientID %>').value;
                sbgId=document.getElementById('<%=h_SUBJ_ID.ClientID %>').value;
                sgr_Descr=document.getElementById('<%=h_SBG_ID.ClientID %>').value;
                RepSch=document.getElementById('<%=H_SCH_ID.ClientID %>').value;
                RepHeader=document.getElementById('<%=H_SETUP.ClientID %>').value;
                //alert(sgr_Descr);
                
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_MARKS&GETSTU_NO=true&SGR_IDs="+GradeId+"&ACD_IDs="+acdId+"&SBG_IDs="+sbgId+"&SGR_DESCR="+sgr_Descr+"&REP_SCH="+RepSch+"&REP_HEADER="+RepHeader+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
                
                
            }
           else if(id==2)//Report Setup
           {
             result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUP","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_RPT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
           }
           else if(id==3)
           {
                var RPTId
                RPTId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSCH&RPTID="+RPTId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SCH_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
           }
           else if(id==4)
           {
                var RSetupId
                var AllSubjects
                RSetupId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                AllSubjects=1;
                //RSetupRsdId=document.getElementById('<%=H_SCH_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUPHEADER&SETUP_ID="+RSetupId+"&ALL_SUB="+AllSubjects+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SETUP.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
           }
          
        }//CMTSCAT
        function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                alert(path); 
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }            
               function test2(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }            
             
    </script>
<div id="dropmenu1" class="dropmenudiv" style="width: 110px; left: 0px; top: 0px;">
        <a href="javascript:test1('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test1('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test1('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test1('NSW');">
                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test1('EW');">
                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test1('NEW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
     <div id="dropmenu2" class="dropmenudiv" style="width: 110px; left: 0px; top: 0px;">
        <a href="javascript:test2('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test2('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test2('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test2('NSW');">
                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test2('EW');">
                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test2('NEW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>

    <table align="center" cellpadding="5" cellspacing="0" class="BlueTable">
        <tr class="title" valign="top">
            <td align="left" colspan="8" style="height: 18px"> Student Mark's List
            </td>
        </tr>
        <tr class="subheader_img" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblHeader" runat="server" Text="View Topics"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 89px">
                Academic Year</td>
            <td align="left" class="matters" style="font-size: 9pt">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 105px">
                <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True"
                    SkinID="DropDownListNormal" Width="156px" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Grade</td>
            <td align="left" class="matters" colspan="1">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True"
                    Width="155px" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 89px">
                Subject</td>
            <td align="left" class="matters" style="font-size: 9pt">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 105px">
                <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True"
                    Width="155px" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Group
            </td>
            <td align="left" class="matters" colspan="1">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:DropDownList id="ddlGroup" runat="server" AutoPostBack="True"
                    Width="156px" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Report</td>
            <td align="left" class="matters" style="font-size: 9pt">
                :</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox id="txtReport" runat="server" SkinID="TextBoxNormal" Width="160px"></asp:TextBox>
                <asp:ImageButton id="ImgReport" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(2)" OnClick="ImgReport_Click">
                </asp:ImageButton></td>
            <td align="left" class="matters" colspan="1">
                Report Schedule</td>
            <td align="left" class="matters" colspan="1">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtRepSchedule" runat="server" SkinID="TextBoxNormal" Width="160px"></asp:TextBox>
                <asp:ImageButton id="ImgRepSchedule" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(3)" OnClick="ImgRepSchedule_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Report Header</td>
            <td align="left" class="matters" style="font-size: 9pt">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:TextBox id="txtRepHeader" runat="server" SkinID="TextBoxNormal" Width="160px">
                </asp:TextBox>
                <asp:ImageButton id="imgRepHeader" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(4)" OnClick="imgRepHeader_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 89px">
                Student Name</td>
            <td align="left" class="matters" style="font-size: 9pt">
                :</td>
            <td align="left" class="matters" colspan="5">
                <asp:TextBox id="txtStudName" runat="server" SkinID="TextBoxNormal" Width="205px"></asp:TextBox>
                <asp:ImageButton id="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(1)" OnClick="ImageButton2_Click">
                </asp:ImageButton></td>
                <td class="matters">
                &nbsp;&nbsp;
                <asp:Label id="lblStudNo" runat="server" Width="130px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="subheader_img" colspan="8">
                <asp:Label id="Label1" runat="server" Text="Details..">
           </asp:Label></td>
        </tr>
        <tr>
        <td style="height: 33px">
            <asp:LinkButton id="lnkNew" runat="server" Width="72px" OnClick="lnkNew_Click">New Entry</asp:LinkButton></td>
            <td align="center" class="matters" colspan="8" style="height: 33px">
                &nbsp;<asp:Button id="btnView" runat="server" CssClass="button"
                    Text="View Details" OnClick="btnView_Click1" /></td>
        </tr>
        <tr>
            <td colspan="9" class="matters" style="border-right: #1b80b6 1pt solid; border-left: #1b80b6 1pt solid;
                width: 503px; border-bottom: #1b80b6 1pt solid;">
                <asp:GridView id="gvMarks" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="505px" AllowPaging="True" OnPageIndexChanging="gvMarks_PageIndexChanging" PageSize="15" OnSelectedIndexChanged="gvMarks_SelectedIndexChanged" CssClass="gridstyle" ><columns>
<asp:TemplateField HeaderText="Student:Name"><HeaderTemplate>
<TABLE><TBODY><TR><TD align=center><asp:Label id="lblName" runat="server" Text="Student Name" CssClass="gridheader_text" __designer:wfdid="w129"></asp:Label></TD></TR><TR><TD><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD><DIV id="chromemenu1" class="chromestyle"><UL><LI><A href="#" rel="dropmenu1"></A><IMG id="mnu_2_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /> </LI></UL></DIV></TD><TD><asp:TextBox id="txtStudName" runat="server" Width="100px" __designer:wfdid="w130"></asp:TextBox></TD><TD></TD><TD vAlign=middle><asp:ImageButton id="btnStudName_Search" onclick="btnStudName_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w131"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</HeaderTemplate>

<ItemStyle Width="120px"></ItemStyle>

<HeaderStyle Width="120px"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblStudent" runat="server" Text='<%# Bind("STUNAME") %>' __designer:wfdid="w200"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Subject:Name"><HeaderTemplate>
<TABLE><TBODY><TR><TD align=center><asp:Label id="lblSub" runat="server" Text="Subject" CssClass="gridheader_text" __designer:wfdid="w110"></asp:Label></TD></TR><TR><TD><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD><DIV id="chromemenu2" class="chromestyle"><UL><LI><A href="#" rel="dropmenu2"></A><IMG id="mnu_1_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /> </LI></UL></DIV></TD><TD><asp:TextBox id="txtSubject" runat="server" Width="60px" __designer:wfdid="w111"></asp:TextBox></TD><TD></TD><TD vAlign=middle><asp:ImageButton id="btnSubject_Search" onclick="btnSubject_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w112"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>' __designer:wfdid="w84"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Id"><ItemTemplate>
<asp:Label id="lblId" runat="server" Text='<%# Bind("RST_ID") %>' __designer:wfdid="w87"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="AcdId"><ItemTemplate>
<asp:Label id="lblAcdId" runat="server" Text='<%# bind("RST_ACD_ID") %>' __designer:wfdid="w74"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Acc:Year"><ItemTemplate>
<asp:Label id="lblAcdYear" runat="server" Text='<%# bind("ACY_DESCR") %>' __designer:wfdid="w75"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="GrdId"><ItemTemplate>
<asp:Label id="lblGrdId" runat="server" Text='<%# bind("RST_GRD_ID") %>' __designer:wfdid="w76"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Grade"><ItemTemplate>
<asp:Label id="lblGrade" runat="server" Text='<%# bind("RST_GRD_ID") %>' __designer:wfdid="w77"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="RepHeader"><ItemTemplate>
<asp:Label id="lblRepHeader" runat="server" Text='<%# Bind("RSD_HEADER") %>' __designer:wfdid="w201"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="GroupId"><ItemTemplate>
<asp:Label id="lblGrpId" runat="server" Text='<%# Bind("RST_SBG_ID") %>' __designer:wfdid="w78"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Group"><ItemTemplate>
<asp:Label id="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>' __designer:wfdid="w79"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="studId"><ItemTemplate>
<asp:Label id="lblStudId" runat="server" Text='<%# Bind("RST_STU_ID") %>' __designer:wfdid="w80"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mark"><ItemTemplate>
<asp:Label id="lblMark" runat="server" Text='<%# Bind("RST_MARK") %>' __designer:wfdid="w81"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Level">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
<ItemTemplate>
<asp:Label id="lblResGrade" runat="server" Text='<%# Bind("RST_GRADING") %>' __designer:wfdid="w86"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Comments">
<ItemStyle Width="200px"></ItemStyle>

<HeaderStyle Width="200px"></HeaderStyle>
<ItemTemplate>
<asp:Label id="lblCmts" runat="server" Text='<%# Bind("RST_COMMENTS") %>' __designer:wfdid="w88"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="View"><ItemTemplate>
<asp:LinkButton id="lnkView" runat="server" __designer:wfdid="w89" OnClick="lnkView_Click">View</asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <rowstyle cssclass="griditem" />
                    <headerstyle cssclass="gridheader_pop" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView></td>
        </tr>
    </table>
    
    
    <asp:HiddenField id="H_STU_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_GRD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_ACD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_TRM_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SUBJ_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SBG_ID" runat="server">
    </asp:HiddenField>
   
    <asp:HiddenField id="H_RPT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SCH_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SETUP" runat="server">
    </asp:HiddenField>
    
    
    
   <script type="text/javascript">


cssdropdown.startchrome("chromemenu1")
cssdropdown.startchrome("chromemenu2")


    </script>
     <asp:HiddenField id="h_selected_menu_1" runat="server">    </asp:HiddenField>
    <asp:HiddenField id="h_selected_menu_2" runat="server">    </asp:HiddenField>
</asp:Content>

