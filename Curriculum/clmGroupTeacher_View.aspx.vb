Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroupTeacher_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100070") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GridBind()

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    gvTeacher.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnTeacher_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnDPT_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String


        If Session("CurrSuperUser") = "Y" Then

            'FOR JC ADMIN CATEGORY ALSO HAS TO BE SHOWN
            'If Session("SBSUID") = "125002" Then

            str_query = "SELECT A.GUID AS GUID,EMP_ID,EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),DPT_DESCR" _
                                        & " FROM EMPLOYEE_M AS A LEFT OUTER JOIN DEPARTMENT_M AS B ON A.EMP_DPT_ID=B.DPT_ID WHERE " _
                                        & " EMP_ECT_ID IN(1,3,4) AND EMP_BSU_ID='" + Session("sbsuid") + "' AND EMP_STATUS IN(1,2)"
            'Else
            '    str_query = "SELECT A.GUID AS GUID,EMP_ID,EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),DPT_DESCR" _
            '                         & " FROM EMPLOYEE_M AS A LEFT OUTER JOIN DEPARTMENT_M AS B ON A.EMP_DPT_ID=B.DPT_ID WHERE " _
            '                         & " EMP_ECT_ID IN(1,3,4) AND EMP_BSU_ID='" + Session("sbsuid") + "' AND EMP_STATUS=1"

            'End If
        ElseIf studClass.isEmpTeacher(empId) = True Then
            str_query = "SELECT A.GUID AS GUID,EMP_ID,EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),DPT_DESCR" _
                            & " FROM EMPLOYEE_M AS A LEFT OUTER JOIN DEPARTMENT_M AS B ON A.EMP_DPT_ID=B.DPT_ID WHERE " _
                            & " EMP_ID=" + empId.ToString
        End If

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim empSearch As String = ""
        Dim dptSearch As String = ""

        Dim txtSearch As New TextBox


        If gvTeacher.Rows.Count > 0 Then
            txtSearch = gvTeacher.HeaderRow.FindControl("txtTeacher")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')", txtSearch.Text, strSearch)
            empSearch = txtSearch.Text

            txtSearch = gvTeacher.HeaderRow.FindControl("txtDPT")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("DPT_DESCR", txtSearch.Text, strSearch)
            dptSearch = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If
        str_query += " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvTeacher.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTeacher.DataBind()
            Dim columnCount As Integer = gvTeacher.Rows(0).Cells.Count
            gvTeacher.Rows(0).Cells.Clear()
            gvTeacher.Rows(0).Cells.Add(New TableCell)
            gvTeacher.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTeacher.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTeacher.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTeacher.DataBind()
        End If

        If gvTeacher.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTeacher.HeaderRow.FindControl("txtTeacher")
            txtSearch.Text = empSearch

            txtSearch = New TextBox
            txtSearch = gvTeacher.HeaderRow.FindControl("txtDPT")
            txtSearch.Text = dptSearch
        End If
    End Sub

    Sub BindGroups(ByVal gvGroup As GridView, ByVal emp_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SGR_DESCR FROM GROUPS_M AS A INNER JOIN " _
                                & " GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID" _
                                & " WHERE SGS_TODATE IS NULL AND SGS_EMP_ID=" + emp_id _
                                & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvGroup.DataSource = ds
        gvGroup.DataBind()
    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTeacher.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTeacher.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTeacher.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTeacher.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
#End Region

    Protected Sub gvTeacher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTeacher.PageIndexChanging
        Try
            gvTeacher.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTeacher.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvTeacher.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt("edit")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblEmpId As Label
                Dim lblTeacher As Label
                With selectedRow
                    lblEmpId = .FindControl("lblEmpId")
                    lblTeacher = .FindControl("lblEmpName")
                End With
                Dim url As String
                url = String.Format("~\Curriculum\clmGroupTeacher_M.aspx?MainMnu_code={0}&datamode={1}&empid=" + Encr_decrData.Encrypt(lblEmpId.Text) + "&teacher=" + Encr_decrData.Encrypt(lblTeacher.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTeacher.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblEmpId As New Label

                lblEmpId = e.Row.FindControl("lblEmpId")
                If lblEmpId.Text <> "" Then
                    Dim gvGroup As New GridView
                    gvGroup = e.Row.FindControl("gvGroup")
                    BindGroups(gvGroup, lblEmpId.Text)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
