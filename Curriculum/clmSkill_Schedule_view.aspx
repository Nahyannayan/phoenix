<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSkill_Schedule_view.aspx.vb" Inherits="clmSkill_Schedule_view" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">


                 function switchViews(obj, row) {
                     var div = document.getElementById(obj);
                     var img = document.getElementById('img' + obj);

                     if (div.style.display == "none") {
                         div.style.display = "inline";
                         if (row == 'alt') {
                             img.src = "../Images/expand_button_white_alt_down.jpg";
                         }
                         else {
                             img.src = "../Images/Expand_Button_white_Down.jpg";
                         }
                         img.alt = "Click to close";
                     }
                     else {
                         div.style.display = "none";
                         if (row == 'alt') {
                             img.src = "../Images/Expand_button_white_alt.jpg";
                         }
                         else {
                             img.src = "../Images/Expand_button_white.jpg";
                         }
                         img.alt = "Click to expand";
                     }
                 }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Skill Schedule
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="Table1" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--  <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                SKILL SCHEDULE</span></font></td>
                    </tr>--%>

                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <table id="tbl_test" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <%--<span style="clear: left; display: inline; float: left; visibility: visible;">--%>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic year</span>
                                    </td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server"
                                            AutoPostBack="True" CssClass="listbox" Width="132px" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"  width="20%">
                                        <span class="field-label">Term</span>
                                    </td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" CssClass="listbox"
                                            OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged" Width="127px">
                                        </asp:DropDownList>

                                    </td>
                                    <%-- </span>--%>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            Width="100%" OnRowDataBound="gvAuthorizedRecord_RowDataBound" PageSize="20">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("SBG_ID") %>_<%# Eval("GRD_ID") %>_<%# Eval("CAD_ID") %>_<%# Eval("PARENT_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("SBG_ID") %>_<%# Eval("GRD_ID") %>_<%# Eval("CAD_ID") %>_<%# Eval("PARENT_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("SBG_ID") %>_<%# Eval("GRD_ID") %>_<%# Eval("CAD_ID") %>_<%# Eval("PARENT_ID") %>', 'alt');">
                                                            <img id="imgdiv<%# Eval("SBG_ID") %>_<%# Eval("GRD_ID") %>_<%# Eval("CAD_ID") %>_<%# Eval("PARENT_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblGRD_DESCRH" runat="server" Text="Grade"></asp:Label><br />
                                                        <asp:TextBox ID="txtGRD_DESC" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGRD_DESC" OnClick="btnSearchGRD_DESC_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>

                                                    <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_DESC" runat="server" Text='<%# Bind("GRD_DESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblSTM_IDH" runat="server" Text="Stream"></asp:Label><br />
                                                        <asp:TextBox ID="txtSTM_DESC" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSTM_DESC" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" OnClick="btnSearchSTM_DESC_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTM_DESC" runat="server" Text='<%# bind("STM_DESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SUBJECT">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblSUBJECTH" runat="server" Text="Subject"></asp:Label><br />
                                                        <asp:TextBox ID="txtSBG_DESC" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSBG_DESC" OnClick="btnSearchSBG_DESC_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSBG_DESC" runat="server" Text='<%# Bind("SBG_DESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ACTIVITY">
                                                    <HeaderTemplate>
                                                        Assessment<br />
                                                        <asp:TextBox ID="txtCAD_DESC" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchCAD_DESC" OnClick="btnSearchCAD_DESC_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCAD_DESC" runat="server" Text='<%# Bind("CAD_DESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("UNIQUE_ID") %>'
                                                            CommandName="Deleting" Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Schedule will be deleted permanently.Are you sure you want to continue?" runat="server"></ajaxToolkit:ConfirmButtonExtender>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        </td></tr>
             <tr>
                 <td align="left"  colspan="6">
                     <div id="div<%# Eval("SBG_ID") %>_<%# Eval("GRD_ID") %>_<%# Eval("CAD_ID") %>_<%# Eval("PARENT_ID") %>" style="display: none; position: relative; left: 20px;">
                         <asp:GridView ID="gvGroupInfo" runat="server" Width="100%"
                             AutoGenerateColumns="false" OnRowCommand="gvDetails_RowCommand" EmptyDataText="No Info available.">

                             <Columns>

                                 <asp:BoundField DataField="SGR_DESC" HeaderText="GROUP" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="Left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="CAS_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="DATE" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="center" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="CAS_DESC" HeaderText="Act. Description" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="Left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="MARK" HeaderText="MARK" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="Center" />
                                 </asp:BoundField>
                                 <asp:TemplateField HeaderText="Allocate">
                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <ItemTemplate>
                                         <asp:LinkButton ID="lblAllocate" runat="server" OnClick="lblAllocate_Click">Allocate</asp:LinkButton>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Retest">
                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <ItemTemplate>
                                         <asp:LinkButton ID="lblRetest" runat="server" OnClick="lblRetest_Click">Retest</asp:LinkButton>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Edit">
                                     <EditItemTemplate>
                                         &nbsp;
                                     </EditItemTemplate>
                                     <HeaderTemplate>
                                         <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                     </HeaderTemplate>
                                     <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <ItemTemplate>
                                         &nbsp;<asp:LinkButton ID="lbledit" runat="server" OnClick="lblView_Click">Edit</asp:LinkButton>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                     <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("CAS_ID") %>'
                                             CommandName="Deleting" Text="Delete"></asp:LinkButton>
                                         <ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="LinkButton1" ConfirmText="Schedule will be deleted permanently.Are you sure you want to continue? " runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="CAS_ID" Visible="False">
                                     <ItemTemplate>
                                         <asp:Label ID="lblCAS_ID" runat="server" Text='<%# bind("CAS_ID") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>

                             </Columns>

                             <RowStyle  />
                             <HeaderStyle  />
                         </asp:GridView>
                 </td>
             </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_ID" runat="server" Text='<%# bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SBG_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSBG_ID" runat="server" Text='<%# bind("SBG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CAD_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCAD_ID" runat="server" Text='<%# bind("CAD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PARENT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPARENT_ID" runat="server" Text='<%# bind("PARENT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle  CssClass="griditem"  />
                                            <SelectedRowStyle  />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <br />
                                        <br />


                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_11" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_8" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
       



            </div>
        </div>
    </div>

</asp:Content>

