<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProcessAPP_Lock.aspx.vb" Inherits="Curriculum_clmProcessAPP_Lock" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:Literal ID="ltLabel" runat="server" Text="Lock App Processing"
                                                EnableViewState="False"></asp:Literal>
        </div>
      <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                        ></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>                                  
                                </div>                           
                        </td>
                    </tr>
                    <tr>
                        <td   valign="bottom">
                            <table align="center" cellpadding="5" cellspacing="0"  width="100%">                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="102px">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%" ><span class="field-label">Select Report card</span> </td>
                                  
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>                              
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="False"
                                            EmptyDataText="No subjects to display" CssClass="table table-bordered table-row">
                                            <RowStyle />
                                            <EmptyDataRowStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Report Card">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="RPF_ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lock">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkLock" Checked='<%# Bind("RPF_bPROCESSLOCK") %>' Enabled='<%# Bind("bLock") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle   />
                                            <AlternatingRowStyle  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom"><br /></td>
                    </tr>
                    <tr>
                        <td  valign="bottom" align="center">

                            <asp:Button ID="btnSave" runat="server" CausesValidation="False"
                                CssClass="button" Text="Save" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td   valign="bottom">
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
            </div>
        </div>
    </div>
</asp:Content>

