﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmObjTrackStudentLevels.aspx.vb" Inherits="Curriculum_clmObjTrackStudentLevels" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script>
        function change_chk_state(chkThis, chkObj) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(chkObj) != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Student Levels
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" border="0" cellpadding="0"
                    cellspacing="0" style="vertical-align: top;" width="100%">
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <%--  <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkSearch" runat="server">Search</asp:LinkButton>
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlSearch"
                             CollapseControlID="lnkSearch" ExpandControlID="lnkSearch" TextLabelID="lnkSearch"
                             CollapsedText="Show Details" ExpandedText="Hide Details"  Collapsed="false">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left">
                            <div class="accordion" id="accordionExample3">
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseOne">Search</button> 
                                          
                                    </div>


                                    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample3">
                                        <div class="card-body">
                                            <asp:Panel ID="pnlSearch" runat="server">
                                                <table id="Table2" runat="server" align="left" width="100%"
                                                    cellpadding="5" cellspacing="0">
                                                    <tr>
                                                        <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                                                        <td align="left" width="30%">
                                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList></td>
                                                        <td align="left" width="20%"><span class="field-label">Grade</span> </td>
                                                        <td align="left" width="30%">
                                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" width="20%"><span class="field-label">Subject</span> </td>
                                                        <td align="left" width="30%">
                                                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList></td>
                                                        <td align="left" width="20%"><span class="field-label">Group</span> </td>
                                                        <td align="left" width="30%">
                                                            <asp:DropDownList ID="ddlGroup" runat="server">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="4">
                                                            <asp:Button ID="btnView" runat="server" CssClass="button" TabIndex="7"
                                                                Text="View Students" ValidationGroup="groupM1" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <%--  <td align="left">
                            <table>
                                <tr>--%>
                        <td align="left">
                            <CR:CrystalReportViewer ID="crv" runat="server" DisplayPage="true"
                                EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReportSourceID="rs"
                                ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" ShowAllPageIds="false"
                                BestFitPage="True" HasToggleGroupTreeButton="False" HasCrystalLogo="False" HasSearchButton="False"
                                HasDrillUpButton="False" HasZoomFactorList="False" SeparatePages="False" />
                            &nbsp;<CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                            <asp:HiddenField ID="hView" runat="server" />
                        </td>
                    </tr>
                </table>
                <%-- </td>
                    </tr>
                </table>--%>
            </div>
        </div>
    </div>
</asp:Content>

