<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
	<!-- Find the root node called Menus 
       and call MenuListing for its children -->
	<xsl:template match="/SUBJECTS">
		<MenuItems>
			<xsl:call-template name="MenuListing" />
		</MenuItems>
	</xsl:template>

	<!-- Allow for recusive child node processing -->
	<xsl:template name="MenuListing">
		<xsl:apply-templates select="SUBJECT" />
	</xsl:template>

	<xsl:template match="SUBJECT">
		<MenuItem>
			<!-- Convert Menu child elements to MenuItem attributes -->

			<xsl:attribute name="valuefield">
				<xsl:value-of select="SBG_ID"/>
			</xsl:attribute>
					<xsl:attribute name="Text">
					<xsl:value-of select="SBG_DESCR"/>
				</xsl:attribute>
			
			<!-- Call MenuListing if there are child Menu nodes -->
			<xsl:if test="count(SUBJECT) > 0">
				<xsl:call-template name="MenuListing" />
			</xsl:if>
		</MenuItem>
	</xsl:template>
</xsl:stylesheet>

