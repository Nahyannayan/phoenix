﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmMarkEntry_AOL.aspx.vb" Inherits="Curriculum_clmMarkEntry_AOL" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function copyData() {
            var e = document.getElementById("<%=ddlHeader.ClientID %>");

           var strVal = e.options[e.selectedIndex].value;

           var copyText = document.getElementById("<%=txtHeader.ClientID %>").value;
       var i;
       if (strVal == 'CH') {

           for (i = 0; i < txtCH.length; i++) {
               document.getElementById(txtCH[i]).value = copyText;
           }
       }
       else {
           for (i = 0; i < txtFB.length; i++) {
               document.getElementById(txtFB[i]).value = copyText;
           }
       }

       return false;

   }




   function getcomments(ctlid, code, header, stuid, rsmid) {
       var sFeatures;
       sFeatures = "dialogWidth: 530px; ";
       sFeatures += "dialogHeight: 450px; ";
       sFeatures += "help: no; ";
       sFeatures += "resizable: no; ";
       sFeatures += "scroll: yes; ";
       sFeatures += "status: no; ";
       sFeatures += "unadorned: no; ";
       var result;
       var parentid;
       var varray = new Array();
       if (code == 'ALLCMTS') {

           //result = window.showModalDialog("clmCommentsList.aspx?ID=" + code +"&HEADER=" + header +"&STUID=" + stuid + "&RSMID="+rsmid,"", sFeatures);          
           //  if(result != '' && result != undefined)
           //  {
           //      document.getElementById(ctlid).value = result.replace(/_#_/g,"'");
           //      //result.split('___')[0]
           //      //document.getElementById(ctlid.split(';')[1]).value=varray[0];
           //  }
           //  else
           //  {
           //      return false;
           //  }
       }
       popup("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", 'Comments List')

   }

   var color = '';

   function popup(url, header) {

       //var url = url1;
       if ($("#modal").length > 0) {
       }
       else {
           $('<div style="display:none"  id="modal"></div>').appendTo('body');
       }
       var dialog = $("#modal");
       dialog.html('<iframe style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
       dialog.dialog({
           width: '50%',
           height: 300,
           title: header,
           modal: true
       });

   };
   function highlight(obj) {
       var rowObject = getParentRow(obj);
       var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#deffb4';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Mark Entry
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table id="tblact" runat="server" align="center"  width="100%">
                                <tr>
                                    <td align="left"  valign="top" width="20%"><span class="field-label">Assesment</span></td>
                                    
                                    <td align="left"  valign="top" width="35%">
                                        <asp:Label ID="lblActivity" runat="server" class="field-value"></asp:Label></td>
                                    <td align="left"  valign="top" width="20%"><span class="field-label">Subject</span></td>
                                    
                                    <td align="left"  valign="top" width="25%">
                                        <asp:Label ID="lblSubject" runat="server" class="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"  valign="top"><span class="field-label">Group</span></td>
                                    
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblGroup" runat="server" class="field-value"></asp:Label></td>
                                    <td align="left"  valign="top"><span class="field-label">Date</span></td>
                                    
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblDate" runat="server" class="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"  valign="top"><span class="field-label">Min.Marks</span></td>
                                    
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblMinmarks" runat="server" class="field-value"></asp:Label></td>
                                    <td align="left"  valign="top"><span class="field-label">Max Marks</span></td>
                                   
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblMaxmarks" runat="server" class="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="field-label">Select Header</span></td>
                                                
                                                <td>
                                                    <asp:DropDownList ID="ddlHeader" runat="server">
                                                        <asp:ListItem Value="CH">Chapter</asp:ListItem>
                                                        <asp:ListItem Value="FB">FeedBack</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtHeader" TextMode="MultiLine"
                                                        SkinID="MultiText"></asp:TextBox></td>
                                                <td>
                                                    <asp:Button runat="server" ID="btnCopy" OnClientClick="return copyData();" class="button" Text="Copy to All" /></td>
                                            
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4" valign="top">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4" valign="top">

                                        <asp:GridView ID="gvStud" Width="100%" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL.No" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStaId" runat="server" Text='<%# Bind("Sta_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAtt" runat="server" Text='<%# Bind("STA_bATTENDED") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Stu_Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="K/U">
                                                    <HeaderTemplate>
                                                       <asp:Label ID="Label4" runat="server" Text=" K/U" />
                                                                    <asp:Label ID="lKUMarks" runat="server" />
                                                                
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtKUMarks" Text='<%# Bind("KU_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtKUMarks"
                                                            ErrorMessage='<%# Bind("ERR_KU") %>' Display="Dynamic" Type="Double" MaximumValue='<%# Bind("KU_MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator>
                                                        <%-- <table >
                   
                    <tr><td >
                    <asp:TextBox ID="txtKUMarks"  text='<%# Bind("KU_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr>
                     <tr><td ><asp:RangeValidator id="RangeValidator1" runat="server" ControlToValidate="txtKUMarks"
                     ErrorMessage='<%# Bind("ERR_KU") %>'  Display="Dynamic"  Type="Double" MaximumValue='<%# Bind("KU_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>--%>
                                                        <asp:Label ID="lblKUGrade" runat="server" Text='<%# Bind("STS_KU_G") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Appln">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text="Appln" />
                                                                    <asp:Label ID="lAPPLMarks" runat="server" />
                                                                
                                                    </HeaderTemplate>


                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAPPMarks" Text='<%# Bind("APP_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server"  />
                                                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtAPPMarks"
                                                            ErrorMessage='<%# Bind("ERR_APPL") %>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("APP_MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator>
                                                        <%-- <table ><tr><td >
                    <asp:TextBox ID="txtAPPMarks"  text='<%# Bind("APP_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator2" runat="server" ControlToValidate="txtAPPMarks"
                    ErrorMessage='<%# Bind("ERR_APPL") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("APP_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>--%>
                                                        <asp:Label ID="lblAPPGrade" runat="server" Text='<%# Bind("STS_APPL_G") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Commn">

                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="COMM" />
                                                            
                                                                    <asp:Label ID="lCOMMMarks" runat="server" />
                                                                
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCOMMMarks" Text='<%# Bind("COMM_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server"  />
                                                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtCOMMMarks"
                                                            ErrorMessage='<%# Bind("ERR_COMM") %>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("COMM_MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator>
                                                        <%--<table ><tr><td >
                    <asp:TextBox ID="txtCOMMMarks"  text='<%# Bind("COMM_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator3" runat="server" ControlToValidate="txtCOMMMarks"
                    ErrorMessage='<%# Bind("ERR_COMM") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("COMM_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>--%>
                                                        <asp:Label ID="lblCOMMGrade" runat="server" Text='<%# Bind("STS_COMM_G") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HOTS">

                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label3" runat="server" text="HOTS"/>
                                                                    <asp:Label ID="lHotsMarks" runat="server" />
                                                               
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHOTSMarks" Text='<%# Bind("HOTS_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server"  />
                                                        <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtHOTSMarks"
                                                            ErrorMessage='<%# Bind("ERR_HOTS") %>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("HOTS_MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator>
                                                        <%--<table ><tr><td  >
                    <asp:TextBox ID="txtHOTSMarks"  text='<%# Bind("HOTS_MARKS") %>' enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator4" runat="server" ControlToValidate="txtHOTSMarks"
                    ErrorMessage='<%# Bind("ERR_HOTS") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("HOTS_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>--%>
                                                        <asp:Label ID="lblHOTSGrade" runat="server" Text='<%# Bind("STS_HOTS_G") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="HOTS">

                                                    <HeaderTemplate>
                                                        <asp:Label ID="lWSMarks11" runat="server" text="WITHOUT SKILLS" /><br />
                                                                    <asp:Label ID="lWSMarks" runat="server" />
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWSMarks" Text='<%# Bind("WITHOUTSKILLS_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server"  />
                                                        <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtWSMarks"
                                                            ErrorMessage='<%# Bind("ERR_WITHOUTSKILLS") %>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("WITHOUTSKILLS_MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator>
                                                        <%--<table ><tr><td  >
                    <asp:TextBox ID="txtWSMarks" text='<%# Bind("WITHOUTSKILLS_MARKS") %>' enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator5" runat="server" ControlToValidate="txtWSMarks"
                    ErrorMessage='<%# Bind("ERR_WITHOUTSKILLS") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("WITHOUTSKILLS_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>--%>
                                                        <asp:Label ID="lblWSGrade" runat="server" Text='<%# Bind("STS_WITHOUTSKILLS_G") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Chapter">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtChapter" runat="server" Text='<%# Bind("STS_CHAPTER") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FeedBack">
                                                    <ItemTemplate>
                                                        
                                                                    <asp:TextBox ID="txtFeedBack" runat="server" Text='<%# Bind("STS_FEEDBACK") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgBtn" ImageUrl="~/Images/comment.jpg" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Work On This" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWots" runat="server" Text='<%# Bind("STS_WOT") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Target" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTarget" runat="server" Text='<%# Bind("STS_TARGET") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Marks">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMarks" runat="server" Text='<%# Bind("MARKS") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STA_GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>


                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfCAS_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMEntered" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hfMaxMarks" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hfKUMaxMarks" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hfAPPLMaxMarks" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hfCOMMMaxMarks" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hfHOTSMaxMarks" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hfWSMaxMarks" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hfGradeSlab" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMinMarks" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfEntryType" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hfWithoutSkills" runat="server"></asp:HiddenField>


</asp:Content>

