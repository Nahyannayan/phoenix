﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmReportRule_Popup.aspx.vb"
    Inherits="Curriculum_clmReportRule_Popup" Title="RULES" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
<%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <table  border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="left" >
                       <span class="field-label"> Subject</span></td>
                   
                    <td align="left" >
                        <asp:Label ID="lblSubject" runat="server"  CssClass="field-value" >
                        </asp:Label></td>
                </tr>
                  <tr>
                    <td align="left">
                      <span class="field-label">  Report Card</span></td>
                 
                    <td align="left" >
                        <asp:Label ID="lblReport" runat="server"  CssClass="field-value" >
                        </asp:Label></td>
                </tr>
                  <tr>
                    <td align="left" >
                       <span class="field-label"> Header</span></td>
                   
                    <td align="left" >
                        <asp:Label ID="lblHeader" runat="server"  CssClass="field-value">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td align="left" >
                       <span class="field-label"> Grading Slab</span></td>
                   
                    <td align="left" >
                        <asp:Label ID="lblGradeSlab" runat="server"  CssClass="field-value" >
                        </asp:Label></td>
                </tr>
                <tr>
                    <td align="left" >
                      <span class="field-label">  Min Pass Marks</span></td>
               
                    <td align="left">
                        <asp:Label ID="lblMinPassmark" runat="server" CssClass="field-value" >
                        </asp:Label></td>
                </tr>
                <tr>
                    <td align="left" >
                      <span class="field-label">  Max Marks</span></td>
             
                    <td align="left" >
                        <asp:Label ID="lblMaxmark" runat="server"  CssClass="field-value"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" >
                      <span class="field-label">  Grade Weightage</span></td>
                
                    <td align="left" >
                        <asp:Label ID="lblGradeWt" runat="server" CssClass="field-value">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:GridView ID="gvRule" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                            EmptyDataText="No Rules" PageSize="20" width="100%"
                            BorderStyle="Solid">
                            <RowStyle CssClass="griditem" />
                            <Columns>
                                <asp:TemplateField HeaderText="Operation" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblRule" Text='<%# Bind("RULES") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Wrap="true" ></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weightage">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWT" Text='<%# Bind("WEIGHT") %>'  runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle  />
                            <EditRowStyle  />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hfRRM_ID" runat="server" />
        </div>
    </form>
</body>
</html>
