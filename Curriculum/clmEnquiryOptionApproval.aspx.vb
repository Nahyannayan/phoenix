﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Collections.Generic
Imports System.Collections
Partial Class Curriculum_clmStudOptionApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim ENR_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100295") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    trApr2.Visible = False
                    trGrd.Visible = False


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindOptions()


                    BindSuject_1()
                    BindOption_2()



                    trOption.Visible = False
                    trSubject.Visible = False


                    gvEnquiry.Attributes.Add("bordercolor", "#1b80b6")

                    btnOnHold.Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try


        End If



    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False

    End Sub

#Region "Private Methods"



    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm _
                                  & " AND ACD_ID IN(" + Session("CURRENT_ACD_ID") + "," + Session("NEXT_ACD_ID") + ")" _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()


        ddlAcademicYear.Items.FindByValue(Session("NEXT_ACD_ID")).Selected = True
        Return ddlAcademicYear
    End Function





    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindOptions()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR,1  AS ID  FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString _
                                & "' AND SBG_STM_ID=1" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        str_query += " UNION ALL SELECT 0 AS OPT_ID,'TOTAL' AS OPT_DESCR,2 AS ID "
        str_query = "SELECT OPT_ID,OPT_DESCR FROM ( " + str_query + ")PP ORDER BY ID"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlOptions.DataSource = ds
        dlOptions.DataBind()
        hfTotalOptions.Value = dlOptions.Items.Count - 1
        BindSubjectsOptionCount()
    End Sub

    Sub BindSubjectsOptionCount()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim acd_id As String


        Dim strMarquee As String = ""

        'If opt_id <> 0 Then
        '    str_query = "SELECT DISTINCT SBG_DESCR, " _
        '                & " (SELECT COUNT(ENR_EQS_ID) FROM ENQ.ENQUIRY_OPTIONREQ_S WHERE ENR_SBG_ID=A.SBG_ID AND ENR_OPT_ID=" + opt_id + ") AS REQ_COUNT_E," _
        '                & " (SELECT COUNT(ENR_EQS_ID) FROM ENQ.ENQUIRY_OPTIONREQ_S WHERE ENR_APR_SBG_ID=A.SBG_ID AND ENR_OPT_ID=" + opt_id + ") AS ALT_COUNT_E," _
        '                & " (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S INNER JOIN STUDENT_M ON STU_ID=STR_STU_ID AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + Session("CURRENT_ACD_ID") + " WHERE STR_SBG_ID=A.SBG_ID AND STR_OPT_ID=" + opt_id + ") AS REQ_COUNT_S," _
        '                & " (SELECT COUNT(SSD_STU_ID) FROM STUDENT_GROUPS_S INNER JOIN STUDENT_M ON STU_ID=SSD_STU_ID AND STU_CURRSTATUS='EN' WHERE SSD_SBG_ID=A.SBG_ID AND SSD_OPT_ID=" + opt_id + ") AS ALT_COUNT_S" _
        '                & " FROM SUBJECTS_GRADE_S AS A" _
        '                & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
        '                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        '                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
        '                & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + opt_id _
        '                & " ORDER BY SBG_DESCR"
        'Else
        str_query = "SELECT DISTINCT SBG_DESCR, " _
             & " (SELECT COUNT(ENR_EQS_ID) FROM ENQ.ENQUIRY_OPTIONREQ_S  WHERE ENR_SBG_ID=A.SBG_ID ) AS REQ_COUNT_E," _
             & " (SELECT COUNT(ENR_EQS_ID) FROM ENQ.ENQUIRY_OPTIONREQ_S INNER JOIN ENQ.ENQUIRY_OPTIONAPPROVAL ON ENR_EQS_ID=ENA_EQS_ID WHERE ENR_APR_SBG_ID=A.SBG_ID AND ENA_bAPPROVED=1) AS ALT_COUNT_E," _
             & " (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S INNER JOIN STUDENT_M ON STU_ID=STR_STU_ID AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + Session("CURRENT_ACD_ID") + " WHERE STR_SBG_ID=A.SBG_ID ) AS REQ_COUNT_S," _
             & " (SELECT COUNT(SSD_STU_ID) FROM STUDENT_GROUPS_S INNER JOIN STUDENT_M ON STU_ID=SSD_STU_ID AND STU_CURRSTATUS='EN' WHERE SSD_SBG_ID=A.SBG_ID ) AS ALT_COUNT_S," _
             & " (SELECT COUNT(ENR_EQS_ID) FROM ENQ.ENQUIRY_OPTIONREQ_S INNER JOIN ENQ.ENQUIRY_OPTIONAPPROVAL ON ENR_EQS_ID=ENA_EQS_ID WHERE ENR_SBG_ID=A.SBG_ID AND ENA_bONHOLD=1) AS ALT_COUNT_H," _
             & " (SELECT SBG_TOTSEATS FROM SUBJECTS_GRADE_S AS B WHERE B.SBG_ID=A.SBG_ID) AS SEAT_COUNT " _
             & " FROM SUBJECTS_GRADE_S AS A" _
             & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
             & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
             & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
             & " AND SBG_STM_ID=1 " _
             & " ORDER BY SBG_DESCR"
        'End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjectCounts.DataSource = ds
        gvSubjectCounts.DataBind()

        If gvSubjectCounts.Rows.Count > 0 Then
            Dim i As Integer
            Dim lblReq As Label
            Dim lblAllot As Label
            Dim lblAvailable As Label


            For i = 0 To gvSubjectCounts.Rows.Count - 1
                With gvSubjectCounts.Rows(i)
                    strMarquee += .Cells(0).Text + " : "
                    lblReq = .FindControl("lblReq")
                    lblAllot = .FindControl("lblAllot")
                    lblAvailable = .FindControl("lblAvailable")
                    lblReq.Text = Val(.Cells(1).Text) + Val(.Cells(4).Text)
                    lblAllot.Text = Val(.Cells(2).Text) + Val(.Cells(5).Text)
                    lblAvailable.Text = Val(.Cells(6).Text) - (Val(.Cells(2).Text) + Val(.Cells(5).Text))

                    If Val(lblAvailable.Text) > 0 Then
                        lblAvailable.ForeColor = Drawing.Color.Green
                    Else
                        lblAvailable.ForeColor = Drawing.Color.Red
                    End If
                    lblAvailable.Font.Size = 11

                    strMarquee += lblAvailable.Text + "&nbsp;&nbsp;&nbsp;&nbsp;"
                End With
            Next

            ltMarquee.Text = "<marquee direction=""left"" onmouseover=""this.stop()"" onmouseout=""this.start()""><font size='4px'>" + strMarquee + "</font><marquee>"
        End If
    End Sub
    Public Sub GetPrevGrade(ByVal vGRD_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER - 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            hfGRD_ID.Value = dr("GRD_ID")
        End While

    End Sub

    Sub HideGridColumns()
        Dim i As Integer
        If gvEnquiry.Rows.Count > 0 Then
            For i = 7 To 16
                gvEnquiry.Columns(i).Visible = False
            Next
        End If
    End Sub
    Sub GridBind()
        ViewState("slno") = 0

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        If txtRecords.Text <> "" Then
            str_query = "SELECT TOP " + txtRecords.Text + " EQS_ID,EQS_APPLNO,ENQ_NAME,bENABLE='" + IIf(rdReject.Checked = True, "false", "true") + "',ISNULL(ENA_REMARKS,'') REMARKS FROM VW_ENQUIRY_DETAILS " _
                      & " LEFT OUTER JOIN ENQ.ENQUIRY_OPTIONAPPROVAL ON EQS_ID=ENA_EQS_ID WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND EQS_GRD_ID='" + hfGRD_ID.Value + "'"


        Else
            str_query = "SELECT EQS_ID,EQS_APPLNO,ENQ_NAME,bENABLE='" + IIf(rdReject.Checked = True, "false", "true") + "',ISNULL(ENA_REMARKS,'') REMARKS FROM VW_ENQUIRY_DETAILS" _
            & "  LEFT OUTER JOIN ENQ.ENQUIRY_OPTIONAPPROVAL ON EQS_ID=ENA_EQS_ID  WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND EQS_GRD_ID='" + hfGRD_ID.Value + "'"

        End If


        If chkShowPending.Checked = False Then
            str_query += " AND EQS_ID IN(SELECT ENR_EQS_ID FROM ENQ.ENQUIRY_OPTIONREQ_S WHERE ENR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            If hfOptionFilter.Value = "option1" Then
                str_query += " AND ENR_OPT_ID=" + ddlOption1.SelectedValue.ToString
            ElseIf hfOptionFilter.Value = "option1" Then
                str_query += " AND ENR_OPT_ID=" + ddlOption2.SelectedValue.ToString
            End If

            If hfSubjectFilter.Value = "subject1" Then
                str_query += " AND ENR_SBG_ID=" + ddlSubject1.SelectedValue.ToString
            ElseIf hfSubjectFilter.Value = "subject2" Then
                str_query += " AND ENR_SBG_ID=" + ddlSubject2.SelectedValue.ToString
            End If
            If rdAlloted.Checked = True Then
                str_query += " AND ENR_APR_SBG_ID IS NOT NULL"
            End If
            str_query += ")"

            If rdNotAlloted.Checked = True Then
                str_query += " AND EQS_ID NOT IN(SELECT ENA_EQS_ID FROM ENQ.ENQUIRY_OPTIONAPPROVAL WHERE ENA_bAPPROVED=1 OR ENA_bREJECTED=1)"
            End If

            If rdReject.Checked = True Then
                str_query += " AND EQS_ID IN(SELECT ENA_EQS_ID FROM ENQ.ENQUIRY_OPTIONAPPROVAL WHERE  ENA_bREJECTED=1)"
            End If

            If rdOnHold.Checked = True Then
                str_query += " AND EQS_ID IN(SELECT ENA_EQS_ID FROM ENQ.ENQUIRY_OPTIONAPPROVAL WHERE  ENA_bONHOLD=1)"
            End If

        Else
            str_query += " AND EQS_ID NOT IN(SELECT ENR_EQS_ID FROM ENQ.ENQUIRY_OPTIONREQ_S WHERE ENR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + ")"
        End If

        If txtApplName.Text <> "" Then
            str_query += " AND ENQ_NAME LIKE '%" + txtApplName.Text + "%'"
        End If

        If txtApplNo.Text <> "" Then
            str_query += " AND EQS_APPLNO LIKE '%" + txtApplNo.Text + "%'"
        End If

        str_query += " ORDER BY ENQ_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvEnquiry.DataSource = ds
        gvEnquiry.DataBind()
        HideGridColumns()

        Dim i As Integer
        Dim lblOption As LinkButton
        Dim lblOptId As Label

        Dim lblOpt_H As Label
        Dim lblOptId_H As Label

        If gvEnquiry.Rows.Count > 0 Then
            If hfOptionFilter.Value = "all" And hfSubjectFilter.Value = "all" Then
                For i = 0 To dlOptions.Items.Count - 2
                    lblOption = dlOptions.Items(i).FindControl("lblOption")
                    lblOptId = dlOptions.Items(i).FindControl("lblOptId")
                    lblOpt_H = gvEnquiry.HeaderRow.Cells(7 + i).FindControl("lblOpt_H" + CStr(i + 1))
                    lblOptId_H = gvEnquiry.HeaderRow.Cells(7 + i).FindControl("lblOptId_H" + CStr(i + 1))
                    lblOpt_H.Text = lblOption.Text
                    lblOptId_H.Text = lblOptId.Text
                    gvEnquiry.Columns(7 + i).Visible = True
                Next

            ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
                For i = 1 To ddlOption1.Items.Count - 1
                    lblOpt_H = gvEnquiry.HeaderRow.Cells(6 + i).FindControl("lblOpt_H" + CStr(i))
                    lblOptId_H = gvEnquiry.HeaderRow.Cells(6 + i).FindControl("lblOptId_H" + CStr(i))
                    lblOpt_H.Text = ddlOption1.Items(i).Text
                    lblOptId_H.Text = ddlOption1.Items(i).Value
                    gvEnquiry.Columns(6 + i).Visible = True
                Next
            ElseIf hfOptionFilter.Value = "option1" Then
                lblOpt_H = gvEnquiry.HeaderRow.FindControl("lblOpt_H1")
                lblOptId_H = gvEnquiry.HeaderRow.FindControl("lblOptId_H1")
                lblOpt_H.Text = ddlOption1.SelectedItem.Text
                lblOptId_H.Text = ddlOption1.SelectedValue.ToString
                gvEnquiry.Columns(7).Visible = True
            ElseIf hfOptionFilter.Value = "option2" Then
                lblOpt_H = gvEnquiry.HeaderRow.FindControl("lblOpt_H1")
                lblOptId_H = gvEnquiry.HeaderRow.FindControl("lblOptId_H1")
                lblOpt_H.Text = ddlOption2.SelectedItem.Text
                lblOptId_H.Text = ddlOption2.SelectedValue.ToString
                gvEnquiry.Columns(7).Visible = True
            End If
        End If
    End Sub

    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(1)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OPT_DESCR"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_DESCR"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt


    End Function


    Sub BindEnquiryOptions(ByVal gRow As GridViewRow)

        Dim lblEqsId As Label = gRow.FindControl("lblEqsId")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_DESCR,SBG_DESCR FROM OPTIONS_M AS A " _
                                & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.OPT_ID=B.SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGO_SBG_ID=C.SBG_ID" _
                                & " INNER JOIN ENQ.ENQUIRY_OPTIONREQ_S AS D ON C.SBG_ID=D.ENR_SBG_ID AND A.OPT_ID=D.ENR_OPT_ID" _
                                & " WHERE ENR_EQS_ID=" + lblEqsId.Text + " AND ENR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If hfOptionFilter.Value = "option1" Then
            str_query += " AND ENR_OPT_ID=" + ddlOption1.SelectedValue.ToString
        ElseIf hfOptionFilter.Value = "option2" Then
            str_query += " AND ENR_OPT_ID=" + ddlOption2.SelectedValue.ToString
        End If

        If hfSubjectFilter.Value = "subject1" Then
            str_query += " AND ENR_SBG_ID=" + ddlSubject1.SelectedValue.ToString
        ElseIf hfSubjectFilter.Value = "subject2" Then
            str_query += " AND ENR_SBG_ID=" + ddlSubject2.SelectedValue.ToString
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim dr As DataRow
        Dim dt As DataTable = SetDataTable()
        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                dr = dt.NewRow
                dr.Item(0) = .Rows(i).Item(0)
                dr.Item(1) = .Rows(i).Item(1)
                dt.Rows.Add(dr)
            Next
        End With

        Dim lblOption As LinkButton
        Dim lblOpt As Label
        Dim key As Object

        Dim row As DataRow

        If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
            For i = 0 To dlOptions.Items.Count - 1
                lblOption = dlOptions.Items(i).FindControl("lblOption")
                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                key = lblOption.Text
                row = dt.Rows.Find(key)
                If Not row Is Nothing Then
                    lblOpt.Text = row.Item(1)
                Else
                    lblOpt.Text = "--"
                End If
            Next
        ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
            For i = 1 To ddlOption1.Items.Count - 1
                lblOpt = gRow.FindControl("lblOpt" + CStr(i))
                key = ddlOption1.Items(i).Text
                row = dt.Rows.Find(key)
                If Not row Is Nothing Then
                    lblOpt.Text = row.Item(1)
                Else
                    lblOpt.Text = "--"
                End If
            Next
        ElseIf hfOptionFilter.Value = "option1" Then
            key = ddlOption1.SelectedItem.Text
            lblOpt = gRow.FindControl("lblOpt1")
            row = dt.Rows.Find(key)
            If Not row Is Nothing Then
                lblOpt.Text = row.Item(1)
            Else
                lblOpt.Text = "--"
            End If
        ElseIf hfOptionFilter.Value = "option2" Then
            key = ddlOption2.SelectedItem.Text
            lblOpt = gRow.FindControl("lblOpt1")
            row = dt.Rows.Find(key)
            If Not row Is Nothing Then
                lblOpt.Text = row.Item(1)
            Else
                lblOpt.Text = "--"
            End If
        End If

    End Sub



    Function BindOptionDropDownList(ByVal ddlOption As DropDownList, ByVal opt_id As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                      & " FROM SUBJECTS_GRADE_S AS A" _
                      & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                      & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + opt_id _
                      & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOption.DataSource = ds
        ddlOption.DataTextField = "SBG_DESCR"
        ddlOption.DataValueField = "SBG_ID"
        ddlOption.DataBind()


        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0

        ddlOption.Items.Insert(0, li)

        Dim i As Integer

        For i = 0 To ddlOption.Items.Count - 1
            ddlOption.Items(i).Attributes.Add("title", ddlOption.Items(i).Text)
        Next


        ddlOption.Font.Size = FontSize.Medium
        Return ddlOption
    End Function


    '**Edit Grid Options
    Sub EditOptions(ByVal gRow As GridViewRow)
        Dim lblOption As LinkButton
        Dim lblOptId As Label
        Dim lblOpt As Label
        Dim ddlOpt As DropDownList

        Dim i As Integer
        If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
            For i = 0 To dlOptions.Items.Count - 1
                lblOption = dlOptions.Items(i).FindControl("lblOption")
                lblOptId = dlOptions.Items(i).FindControl("lblOptId")


                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i + 1))

                ddlOpt = BindOptionDropDownList(ddlOpt, lblOptId.Text)

                If lblOpt.Text <> "" Then
                    If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                        ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                    End If
                End If

                ddlOpt.Visible = True
                lblOpt.Visible = False
            Next
        ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
            For i = 1 To ddlOption1.Items.Count - 1
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i))
                lblOpt = gRow.FindControl("lblOpt" + CStr(i))
                ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption1.Items(i).Value)
                If lblOpt.Text <> "" Then
                    If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                        ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                    End If
                End If
                ddlOpt.Visible = True
                lblOpt.Visible = False
            Next
        ElseIf hfOptionFilter.Value = "option1" Then
            lblOpt = gRow.FindControl("lblOpt1")
            ddlOpt = gRow.FindControl("ddlOption1")
            ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption1.SelectedValue.ToString)
            If lblOpt.Text <> "" Then
                If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                    ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                End If
            End If
            ddlOpt.Visible = True
            lblOpt.Visible = False
        ElseIf hfOptionFilter.Value = "option2" Then
            lblOpt = gRow.FindControl("lblOpt1")
            ddlOpt = gRow.FindControl("ddlOption1")
            ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption2.SelectedValue.ToString)
            If lblOpt.Text <> "" Then
                If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                    ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                End If
            End If
            ddlOpt.Visible = True
            lblOpt.Visible = False
        End If

    End Sub

    Sub UpdateOptions(ByVal gRow As GridViewRow, ByVal lblEdit As LinkButton, ByVal chkSelect As CheckBox)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i, j As Integer
        Dim str_query As String

        Dim lblOptId As Label
        Dim ddlOpt As DropDownList
        Dim lblOpt As Label


        Dim subjects As New ArrayList
        Dim lblEqsId As Label = gRow.FindControl("lblEqsId")
        Dim transaction As SqlTransaction
        Dim ids As New ArrayList
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection

            For i = 0 To dlOptions.Items.Count - 1
                lblOptId = dlOptions.Items(i).FindControl("lblOptId")

                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i + 1))

                ids.Add(CStr(i + 1))

                If ddlOpt.SelectedValue.ToString <> 0 Then
                    If subjects.Contains(ddlOpt.SelectedValue.ToString) Then

                        lblGrdError.Text = "Options Should be Unique"
                        lblEdit.Text = "Update"
                        chkSelect.Enabled = False
                        transaction.Rollback()

                        For j = 0 To ids.Count - 1
                            lblOpt = gRow.FindControl("lblOpt" + ids(j))
                            ddlOpt = gRow.FindControl("ddlOption" + ids(j))
                            lblOpt.Visible = False
                            ddlOpt.Visible = True
                        Next

                        Exit Sub
                    Else
                        subjects.Add(ddlOpt.SelectedValue.ToString)
                    End If
                End If

                Try
                    transaction = conn.BeginTransaction("SampleTransaction")
                    str_query = "exec ENQ.saveENQUIRY_OPTIONREQ " _
                               & lblEqsId.Text + "," _
                               & ddlAcademicYear.SelectedValue.ToString + "," _
                               & "'" + hfGRD_ID.Value + "'," _
                               & lblOptId.Text + "," _
                               & ddlOpt.SelectedValue.ToString + "," _
                               & "'" + Session("username") + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    transaction.Commit()

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try


                lblOpt.Text = ddlOpt.SelectedItem.Text
                lblOpt.Visible = True
                ddlOpt.Visible = False
            Next


            lblGrdError.Text = ""

        End Using
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)
        Dim lblSlNo As Label = TryCast(sender.FindControl("lblSlNo"), Label)
        Dim selectedRow As GridViewRow = DirectCast(gvEnquiry.Rows(CInt(lblSlNo.Text) - 1), GridViewRow)
        Dim chkSelect As CheckBox = TryCast(sender.FindControl("chkSelect"), CheckBox)
        If lblEdit.Text = "Edit" Then
            EditOptions(selectedRow)
            lblEdit.Text = "Update"
            chkSelect.Enabled = False
        ElseIf lblEdit.Text = "Update" Then
            lblEdit.Text = "Edit"
            chkSelect.Enabled = True
            UpdateOptions(selectedRow, lblEdit, chkSelect)
        End If
    End Sub

    '***Filter By Subject and Option

    Sub BindSuject_1()
        ddlSubject1.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE " _
                               & " SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " AND SBG_bOPTIONAL=1" _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        ddlSubject1.DataSource = ds
        ddlSubject1.DataTextField = "SBG_DESCR"
        ddlSubject1.DataValueField = "SBG_ID"
        ddlSubject1.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlSubject1.Items.Insert(0, li)


        ddlOption1.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlOption1.Items.Add(li)
    End Sub

    Sub BindOption_1()

        ddlOption1.Items.Clear()
        Dim li As New ListItem
        If ddlSubject1.SelectedValue <> "0" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR  FROM " _
                               & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                               & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                               & " WHERE SGO_SBG_ID=" + ddlSubject1.SelectedValue.ToString _
                               & " ORDER BY OPT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlOption1.DataSource = ds
            ddlOption1.DataTextField = "OPT_DESCR"
            ddlOption1.DataValueField = "OPT_ID"
            ddlOption1.DataBind()

            li.Text = "ALL"
            li.Value = 0
            ddlOption1.Items.Insert(0, li)

        Else

            ddlOption1.Items.Clear()
            li = New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlOption1.Items.Add(li)

        End If
    End Sub

    '*******************************

    '***Filter By Option and Subject

    Sub BindOption_2()
        ddlOption2.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR  FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString _
                                & "' AND SBG_STM_ID=1" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY OPT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlOption2.DataSource = ds
        ddlOption2.DataTextField = "OPT_DESCR"
        ddlOption2.DataValueField = "OPT_ID"
        ddlOption2.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlOption2.Items.Insert(0, li)


        ddlSubject2.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject2.Items.Add(li)


    End Sub

    Sub BindSubject2()
        ddlSubject2.Items.Clear()
        Dim li As New ListItem
        If ddlOption2.SelectedValue <> 0 Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                      & " FROM SUBJECTS_GRADE_S AS A" _
                      & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + Session("Next_ACD_ID") _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                      & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + ddlOption2.SelectedValue.ToString _
                      & " ORDER BY SBG_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSubject2.DataSource = ds
            ddlSubject2.DataTextField = "SBG_DESCR"
            ddlSubject2.DataValueField = "SBG_ID"
            ddlSubject2.DataBind()

            li.Text = "ALL"
            li.Value = 0
            ddlSubject2.Items.Insert(0, li)

        Else
            li.Text = "ALL"
            li.Value = 0
            ddlSubject2.Items.Add(li)
        End If



    End Sub

    '*******************************

    Sub BindChoice2(ByVal gvChoice2 As GridView, ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_DESCR,ISNULL(SBG_DESCR,'--') AS SBG_DESCR FROM OPTIONS_M AS A " _
                                & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.OPT_ID=B.SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGO_SBG_ID=C.SBG_ID" _
                                & " INNER JOIN ENQ.ENQUIRY_OPTIONREQ_S AS D ON C.SBG_ID=D.ENR_CHOICE2 AND A.OPT_ID=D.ENR_OPT_ID" _
                                & " WHERE ENR_STU_ID=" + stu_id + " AND ENR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvChoice2.DataSource = ds
        gvChoice2.DataBind()
    End Sub


    Sub SaveEnquiryOptions(ByVal status As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim j As Integer
        Dim transaction As SqlTransaction
        Dim chkSelect As CheckBox
        Dim lblEqsId As Label

        Dim lblSubject As Label
        Dim lblOptId As Label
        Dim str_query As String
        Dim txtRemarks As TextBox
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection

            For i = 0 To gvEnquiry.Rows.Count - 1

                lblEqsId = gvEnquiry.Rows(i).FindControl("lblEqsId")
                chkSelect = gvEnquiry.Rows(i).FindControl("chkSelect")
                txtRemarks = gvEnquiry.Rows(i).FindControl("txtRemarks")
                If chkSelect.Checked = True Then
                    Try
                        transaction = conn.BeginTransaction("SampleTransaction")

                        str_query = "exec [ENQ].[saveENQUIRYOPTIONAPPROVAL_M] " _
                                 & " @ENA_EQS_ID=" + lblEqsId.Text + "," _
                                 & " @ENA_bAPPROVED='" + IIf(status = "approve", "true", "false") + "'," _
                                 & " @ENA_bREJECT='" + IIf(status = "reject", "true", "false") + "'," _
                                 & " @ENA_bONHOLD ='" + IIf(status = "onhold", "true", "false") + "'," _
                                 & " @ENA_REMARKS='" + txtRemarks.Text + "'," _
                                 & " @ENA_USER='" + Session("susr_name") + "'"
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                        If status = "approve" Then
                            For j = 0 To dlOptions.Items.Count - 2

                                lblSubject = gvEnquiry.Rows(i).FindControl("lblOpt" + CStr(j + 1))
                                lblOptId = dlOptions.Items(j).FindControl("lblOptID")

                                If lblSubject.Text <> "--" Then

                                    str_query = "exec ENQ.saveENQUIRYOPTION_APPROVE_S " _
                                               & "@EQS_ID=" + lblEqsId.Text + "," _
                                               & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                               & "@GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                               & "@SBG_DESCR='" + lblSubject.Text + "'," _
                                               & "@OPT_ID=" + lblOptId.Text + "," _
                                               & "@USER='" + Session("susr_name") + "'"
                                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                                End If
                            Next
                        End If
                        transaction.Commit()
                        lblError.Text = "Record saved successfully"
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                        UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Saved"
                        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    End Try
                End If
            Next
        End Using
    End Sub


    ''If the student is below the min level in option subjects mapping then show in red color
    'Sub BindAssessmentLevel(ByVal gRow As GridViewRow)
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = ""
    '    Dim lblOpt As Label
    '    Dim sbg_ids As String
    '    Dim i As Integer
    '    If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
    '        For i = 0 To dlOptions.Items.Count - 1
    '            lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
    '            If lblOpt.Text <> "--" Then
    '                If sbg_ids <> "" Then
    '                    sbg_ids += ","
    '                End If
    '                sbg_ids += ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
    '            End If
    '        Next
    '    ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
    '        For i = 1 To ddlOption1.Items.Count - 1
    '            lblOpt = gRow.FindControl("lblOpt" + CStr(i))
    '            If lblOpt.Text <> "--" Then
    '                If sbg_ids <> "" Then
    '                    sbg_ids += ","
    '                End If
    '                sbg_ids += ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
    '            End If
    '        Next
    '    ElseIf hfOptionFilter.Value = "option1" Then
    '        lblOpt = gRow.FindControl("lblOpt1")
    '        If lblOpt.Text <> "--" Then
    '            sbg_ids = ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
    '        End If
    '    ElseIf hfOptionFilter.Value = "option2" Then
    '        lblOpt = gRow.FindControl("lblOpt1")
    '        If lblOpt.Text <> "--" Then
    '            sbg_ids = ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
    '        End If
    '    End If

    '    Dim rpf As String() = ddlReportCard.SelectedValue.ToString.Split("|")
    '    Dim lblStuId As Label
    '    lblStuId = gRow.FindControl("lblStuId")

    '    If ddlAcademicYear2.SelectedValue.ToString = Session("Current_ACD_ID") Then
    '        str_query = "SELECT SBG_DESCR,RST_COMMENTS,ISNULL(RGP_VALUE,0) RGP_VALUE,SSM_MINLEVELVALUE FROM RPT.REPORT_STUDENT_S AS A" _
    '                   & " INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.RST_COMMENTS=B.RGP_GRADE AND A.RST_SBG_ID=B.RGP_SBG_ID" _
    '                   & " INNER JOIN OPTION_SUBJECTMAPPING_S C ON A.RST_SBG_ID=C.SSM_MAPPED_SBG_ID " _
    '                   & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.RST_SBG_ID=D.SBG_ID" _
    '                   & " WHERE SSM_SBG_ID IN(" + sbg_ids + ") AND RST_RPF_ID=" + rpf(0) _
    '                   & " AND RST_RSD_ID=" + rpf(1) + " AND RGP_RSM_ID=" + rpf(2) _
    '                   & " AND RST_STU_ID=" + lblStuId.Text
    '    Else
    '        str_query = "SELECT SBG_DESCR,RST_COMMENTS,ISNULL(RGP_VALUE,0) RGP_VALUE,SSM_MINLEVELVALUE FROM RPT.REPORT_STUDENT_PREVYEARS AS A" _
    '                        & " INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.RST_COMMENTS=B.RGP_GRADE AND A.RST_SBG_ID=B.RGP_SBG_ID" _
    '                        & " INNER JOIN OPTION_SUBJECTMAPPING_S C ON A.RST_SBG_ID=C.SSM_MAPPED_SBG_ID " _
    '                        & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.RST_SBG_ID=D.SBG_ID" _
    '                        & " WHERE SSM_SBG_ID IN(" + sbg_ids + ") AND RST_RPF_ID=" + rpf(0) _
    '                        & " AND RST_RSD_ID=" + rpf(1) + " AND RGP_RSM_ID=" + rpf(2) _
    '                        & " AND RST_STU_ID=" + lblStuId.Text
    '    End If

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    Dim str As String = ""

    '    Dim bMin As Boolean = False

    '    For i = 0 To ds.Tables(0).Rows.Count - 1
    '        With ds.Tables(0).Rows(i)
    '            If str <> "" Then
    '                str += "<br>"
    '            End If
    '            If .Item("SSM_MINLEVELVALUE") > .Item("RGP_VALUE") Then
    '                str += "<font color=red>" + .Item("SBG_DESCR") + ":" + .Item("RST_COMMENTS") + "</font>"
    '                bMin = True
    '            Else
    '                str += .Item("SBG_DESCR") + " : " + .Item("RST_COMMENTS")
    '            End If
    '        End With
    '    Next

    '    Dim ltLevel As Literal
    '    ltLevel = gRow.FindControl("ltLevel")

    '    ltLevel.Text = str

    '    If bMin = True Then
    '        gRow.BackColor = Drawing.Color.Cornsilk
    '    End If
    'End Sub

#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        'If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
        '  GetPrevGrade(ddlGrade.SelectedValue.ToString)
        ' Else
        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        ' End If
        BindOptions()
        BindSuject_1()
        BindOption_2()

    End Sub

    Protected Sub dlOptions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlOptions.ItemDataBound
        'Dim lblOptId As Label
        'Dim gvOptions As GridView
        'lblOptId = e.Item.FindControl("lblOptId")
        'gvOptions = e.Item.FindControl("gvOptions")
        'If lblOptId.Text <> "" Then
        '    BindSubjectsOptionCount(lblOptId.Text, gvOptions)
        'End If
    End Sub

    Protected Sub rdAllOption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAllOption.CheckedChanged
        If rdAllOption.Checked = True Then
            trOption.Visible = False
            trSubject.Visible = False
        End If
    End Sub

    Protected Sub rdOption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOption.CheckedChanged
        trSubject.Visible = False
        trOption.Visible = True
    End Sub

    Protected Sub rdSubject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdSubject.CheckedChanged
        trSubject.Visible = True
        trOption.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        trApr2.Visible = True
        If ddlSubject1.SelectedValue <> 0 And rdSubject.Checked = True Then
            hfSubjectFilter.Value = "subject1"
        ElseIf ddlSubject2.SelectedValue <> 0 And rdOption.Checked = True Then
            hfSubjectFilter.Value = "subject2"
        Else
            hfSubjectFilter.Value = "all"
        End If

        If ddlOption1.SelectedValue <> 0 And rdSubject.Checked = True Then
            hfOptionFilter.Value = "option1"
        ElseIf ddlOption2.SelectedValue <> 0 And rdOption.Checked = True Then
            hfOptionFilter.Value = "option2"
        Else
            hfOptionFilter.Value = "all"
        End If

        GridBind()




        trGrd.Visible = True


        If rdAlloted.Checked = True Then
            btnOnHold.Visible = True
        Else
            btnOnHold.Visible = False
        End If

        If rdOnHold.Checked = True Then
            btnApprove.Visible = True
            btnReject.Visible = True
        ElseIf rdNotAlloted.Checked = True Then
            btnApprove.Visible = True
            btnReject.Visible = True
        Else
            btnApprove.Visible = False
            btnReject.Visible = False
        End If

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If

        BindOptions()
        BindSuject_1()
        BindOption_2()


    End Sub



    Protected Sub gvEnquiry_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEnquiry.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            lblStuId = e.Row.FindControl("lblStuId")
            If chkShowPending.Checked = False Then
                BindEnquiryOptions(e.Row)
            End If
        End If
    End Sub


    Protected Sub ddlSubject1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject1.SelectedIndexChanged
        BindOption_1()
    End Sub

    Protected Sub ddlOption2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOption2.SelectedIndexChanged
        BindSubject2()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SaveEnquiryOptions("approve")
        GridBind()
        BindSubjectsOptionCount()
    End Sub

  
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        SaveEnquiryOptions("reject")
        GridBind()
        BindSubjectsOptionCount()
    End Sub

    Protected Sub btnOnHold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOnHold.Click
        SaveEnquiryOptions("onhold")
        GridBind()
        BindSubjectsOptionCount()
    End Sub
End Class
