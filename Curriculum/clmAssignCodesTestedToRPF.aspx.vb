﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Curriculum_clmAssignCodesTestedToRPF
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100393") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindReportSchedule()
                    Session("CurrSuperUser") = "Y"
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    BindCodesTestedCategory()
                    ViewState("SelectedRow") = -1
                    ShowHideGrid(False)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub

    Sub GridBind()

    End Sub

    Sub BindReportSchedule()

        Dim grades As String()
        Dim grade As String
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str As String = ""
        Dim i As Integer
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade + "' AND " _
                                & " RSM_ID=" + ddlReportCard.SelectedValue + " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlRPF.DataSource = ds
        ddlRPF.DataTextField = "RPF_DESCR"
        ddlRPF.DataValueField = "RPF_ID"
        ddlRPF.DataBind()


    End Sub

    Sub BindCodesTestedCategory()

        ddlCodesTestedCategory.Items.Clear()
        Dim grades As String()
        Dim grade As String = ""
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CTC_DESCR,CTC_ID FROM CODESTESTED_CATEGORY" _
                               & " WHERE CTC_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND CTC_GRD_ID='" + grade + "'" _
                               & " AND CTC_SBG_ID=" + ddlSubject.SelectedItem.Value + "ORDER BY CTC_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCodesTestedCategory.DataSource = ds
                ddlCodesTestedCategory.DataTextField = "CTC_DESCR"
                ddlCodesTestedCategory.DataValueField = "CTC_ID"
                ddlCodesTestedCategory.DataBind()
                ddlCodesTestedCategory.Items.Insert(0, New ListItem("All", "0"))
                ddlCodesTestedCategory.Items(1).Selected = True
            Else
                ddlCodesTestedCategory.Items.Insert(0, New ListItem("--", "0"))
            End If
        Else
            ddlCodesTestedCategory.Items.Insert(0, New ListItem("--", "0"))
        End If



    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()

        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_bSKILLS=1 AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If Session("sbsuid") = "123004" And Session("clm") = 3 Then
            str_query += " AND (RSM_DESCR LIKE '%KG1%' OR RSM_DESCR LIKE '%KG2%')"
        End If
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ShowHideGrid(False)
        BindReportCard()
        If ddlReportCard.Items.Count > 0 Then
            btnAddCriteria.Enabled = True
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            BindReportSchedule()
            Session("CurrSuperUser") = "Y"
            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
            BindCodesTestedCategory()
        Else
            lblError.Text = "Reports are not available for the chosen Academic Year."
            btnAddCriteria.Enabled = False
        End If

    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        trGrid.Visible = False
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindReportSchedule()
        Session("CurrSuperUser") = "Y"
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindCodesTestedCategory()
        ShowHideGrid(False)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ShowHideGrid(False)
        BindReportSchedule()
        Session("CurrSuperUser") = "Y"
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindCodesTestedCategory()
    End Sub

    Protected Sub btnAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCriteria.Click
        If ddlCodesTestedCategory.SelectedItem.Text = "--" Then
            ShowHideGrid(False)
        Else
            BindGrid()
        End If

    End Sub

    Sub ShowHideGrid(ByVal bool As Boolean)
        trGrid.Visible = bool
        trSave.Visible = bool
    End Sub

    Sub BindGrid()

        ShowHideGrid(True)
        Dim grades As String()
        Dim grade As String = ""
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim i As Integer = 0
        Dim strCat As String = ""

        If ddlCodesTestedCategory.SelectedItem.Text = "All" Then
            For i = 1 To ddlCodesTestedCategory.Items.Count - 1
                If strCat <> "" Then
                    strCat += "|"
                End If
                strCat += ddlCodesTestedCategory.Items(i).Value
            Next
        Else
            strCat = ddlCodesTestedCategory.SelectedValue
        End If

        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        param(1) = New SqlParameter("@GRD_ID", grade)
        param(2) = New SqlParameter("@CTC_ID", strCat)
        param(3) = New SqlParameter("@SBG_ID", ddlSubject.SelectedValue)
        param(4) = New SqlParameter("@RPF_ID", ddlRPF.SelectedValue)

        'Dim str_query As String = " SELECT CTM_ID,CTM_CTC_ID,CASE WHEN CTA_RPF_ID=" + ddlRPF.SelectedValue + " THEN '1' ELSE '0' END AS 'CTA_STATUS',CTM_DESCR FROM CODESTESTED_MASTER " _
        '                        & " LEFT OUTER JOIN CODESTESTED_ASSIGNMENT_DETAILS ON CTM_CTC_ID=CTA_CTC_ID AND CTM_SBG_ID=CTA_SBG_ID AND CTM_ACD_ID=CTA_ACD_ID AND CTA_CTM_ID=CTM_ID AND CTA_RPF_ID=" + ddlRPF.SelectedItem.Value _
        '                        & " WHERE CTM_SBG_ID=" + ddlSubject.SelectedItem.Value + " AND CTM_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND CTM_GRD_ID='" + grade + "' " _
        '                        & " AND CTM_CTC_ID=" + ddlCodesTestedCategory.SelectedItem.Value


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DISPLAY_CODESTESTED", param)
        gvCriteria.DataSource = ds
        gvCriteria.DataBind()

    End Sub
    Dim count As Integer = 0


    Protected Sub gvCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCriteria.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If e.Row.RowIndex > 0 Then
                count = count + 1
            End If
            Dim index As Integer = e.Row.RowIndex
            Dim grades As String()
            Dim grade As String = ""
            If ddlGrade.SelectedValue <> "" Then
                grades = ddlGrade.SelectedValue.Split("|")
                grade = grades(0)
            End If


            Dim lblCTMID As Label = CType(e.Row.FindControl("lblCTMID"), Label)
            Dim lblCTCID As Label = CType(e.Row.FindControl("lblCTCID"), Label)
            Dim lblStatus As Label = CType(e.Row.FindControl("lblStatus"), Label)
            Dim lblStatusID As Label = CType(e.Row.FindControl("lblStatusID"), Label)
            Dim chkStatus As CheckBox = CType(e.Row.FindControl("chkStatus"), CheckBox)

            If lblStatus.Text = "0" Then
                chkStatus.Checked = False
                lblStatusID.Text = "0"
            Else
                chkStatus.Checked = True
                lblStatusID.Text = "1"
            End If



        End If
    End Sub

    Protected Sub ddlRPF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRPF.SelectedIndexChanged
        ShowHideGrid(False)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindCodesTestedCategory()
        ShowHideGrid(False)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim strXml As String = ""
        Try
            Dim grades As String()
            Dim grade As String = ""
            Dim strCTM_ID As String = ""
            If ddlGrade.SelectedValue <> "" Then
                grades = ddlGrade.SelectedValue.Split("|")
                grade = grades(0)
            End If
            Dim i As Integer
            For i = 0 To gvCriteria.Rows.Count - 1

                With gvCriteria.Rows(i)
                    
                    Dim lblCTMID As Label = gvCriteria.Rows(i).FindControl("lblCTMID")
                    Dim lblCTCID As Label = gvCriteria.Rows(i).FindControl("lblCTCID")
                    Dim lblStatusID As Label = gvCriteria.Rows(i).FindControl("lblStatusID")
                    Dim chkStatus As CheckBox = gvCriteria.Rows(i).FindControl("chkStatus")

                    If lblStatusID.Text = "0" And chkStatus.Checked Then

                        strXml += "<ID><CTA_ACD_ID>" + ddlAcademicYear.SelectedValue + "</CTA_ACD_ID>"
                        strXml += "<CTA_GRD_ID>" + grade + "</CTA_GRD_ID>"
                        strXml += "<CTA_RSM_ID>" + ddlReportCard.SelectedValue + "</CTA_RSM_ID>"
                        strXml += "<CTA_RPF_ID>" + ddlRPF.SelectedValue + "</CTA_RPF_ID>"
                        strXml += "<CTA_SBG_ID>" + ddlSubject.SelectedValue + "</CTA_SBG_ID>"
                        strXml += "<CTA_CTC_ID>" + lblCTCID.Text + "</CTA_CTC_ID>"
                        strXml += "<CTA_CTM_ID>" + lblCTMID.Text + "</CTA_CTM_ID></ID>"

                    End If

                    If lblStatusID.Text = "1" And chkStatus.Checked = False Then
                        If strCTM_ID <> "" Then
                            strCTM_ID += "|"
                        End If
                        strCTM_ID += lblCTMID.Text
                    End If
                   
                End With
            Next


            strXml = "<IDS>" + strXml + "</IDS>"
            strXml = strXml.Replace("&", "&amp;")

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@assessmentData", strXml)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.ASSIGN_CODESTESTED", param)
            Dim paramSet(1) As SqlParameter
            paramSet(0) = New SqlParameter("@CTM_IDS", strCTM_ID)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.DELETE_CODESTESTED", paramSet)
            lblError.Text = "Records saved successfully"
            BindGrid()
        Catch ex As Exception
            lblError.Text = "Records could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Protected Sub ddlCodesTestedCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCodesTestedCategory.SelectedIndexChanged
        ShowHideGrid(False)
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If gvCriteria.Rows.Count > 0 Then
            Dim i As Integer
            For i = 0 To gvCriteria.Rows.Count - 1
                Dim chk As CheckBox = gvCriteria.Rows(i).FindControl("chkStatus")
                Dim ddl As DropDownList = gvCriteria.HeaderRow.FindControl("ddlStatus")
                If ddl.SelectedValue = "1" Then
                    If chk.Checked = True Then
                        gvCriteria.Rows(i).Visible = True
                    Else
                        gvCriteria.Rows(i).Visible = False
                    End If
                ElseIf ddl.SelectedValue = "0" Then
                    If chk.Checked = True Then
                        gvCriteria.Rows(i).Visible = False
                    Else
                        gvCriteria.Rows(i).Visible = True
                    End If
                Else
                    gvCriteria.Rows(i).Visible = True
                End If
                
            Next
        End If
    End Sub
End Class
