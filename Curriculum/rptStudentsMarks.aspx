<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentsMarks.aspx.vb" Inherits="Curriculum_rptStudentsMarks" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
                .table td select, table td input[type=text], table td input[type=date], table td textarea {
            width:auto;
                min-width: 100% !important;
        }

        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">


        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }//GRADE

        // To Get The Comments Only

        function getcomments(ctlid, code, header, stuid, rsmid) {

            var sFeatures;
            sFeatures = "dialogWidth: 530px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            var str;

            if (code == 'ALLCMTS') {
                document.getElementById('<%=H_CTL_ID.ClientID%>').value = ctlid
                // result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);
                var oWnd = radopen("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "pop_comment");


            }

        }

        //-------------------------

        function GetPopUp(id) {
            //alert(id);
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            if (id == 3)//Group
            {
                var GradeGrpId
                var GradeId
                GradeId = document.getElementById('<%=ddlGrade.ClientID %>').value;
                GradeGrpId = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                // result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&GRDID=" + GradeId + "&SBM_IDs=" + GradeGrpId + "", "", sFeatures)
                popup("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&GRDID=" + GradeId + "&SBM_IDs=" + GradeGrpId + "")
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }

            else if (id == 4)//Subjejct
            {
                var GrdDetId
                GrdDetId = document.getElementById('<%=ddlGrade.ClientID %>').value;
                var SBGID
                SBGID = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID=" + GrdDetId + "&SBGID=" + SBGID + "", "", sFeatures)
                popup("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID=" + GrdDetId + "&SBGID=" + SBGID + "")
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SBJ_GRD_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }
            else if (id == 5)//Subject Grades
            {
                var GradeId
                var AcdId
                AcdId = document.getElementById('<%=ddlAcdID.ClientID %>').value;
                GradeId = document.getElementById('<%=ddlGrade.ClientID %>').value;
                //alert(GradeId + 'ddd');
                //  result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJACDGRD&GRDID=" + GradeId + "&ACDID=" + AcdId + "", "", sFeatures)
                popup("clmPopupForm.aspx?multiselect=false&ID=SBJACDGRD&GRDID=" + GradeId + "&ACDID=" + AcdId + "")
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;


              }
              else {
                  return false;
              }
          }
          else if (id == 8)//Student Info
          {
              var GradeId
              GradeId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "", sFeatures)
                popup("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "")
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }
            else if (id == 12)//Student Info
            {
                var GradeId;
                var RepSch;
                var RepHeader;
                GradeId = document.getElementById('<%=ddlGrade.ClientID %>').value;
                acdId = document.getElementById('<%=ddlAcdID.ClientID %>').value;
                sbgId = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                sgr_Descr = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                RepSch = document.getElementById('<%=ddlRptSchedule.ClientID %>').value;
                RepHeader = ''

                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_MARKS&GETSTU_NO=true&SGR_IDs=" + GradeId + "&ACD_IDs=" + acdId + "&SBG_IDs=" + sbgId + "&SGR_DESCR=" + sgr_Descr + "&REP_SCH=" + RepSch + "&REP_HEADER=" + RepHeader + "", "", sFeatures)
              <%-- if (result != '' && result != undefined) {
                   //alert(document.getElementById('<%=lblStudNo.ClientID %>').value);
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result.split('___')[0];
                    document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
                    document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
                }--%>
                //else {
                //    return false;
                //}
            }
        }//CMTSCAT


        function GetStudentValues() {
            var ColsId;
            var cellcnt = 0;
            ColsId = document.getElementById('<%=H_CommentCOLS.ClientID %>').value;
    //alert(ColsId);
    var araay = new Array();
    //alert(txt1.length);
    for (i = 0; i < txt1.length; i++) {
        araay[i] = new Array(ColsId)
        //alert(ColsId);
        for (j = 0; j < ColsId; j++) {
            var ArrName = "txt" + (Number(j) + 1);
            //alert(ArrName);
            ArrName = eval(ArrName);
            try {
                var strtext;
                strtext = document.getElementById(ArrName[i]).value.replace(/,/gi, '~');
                strtext = replaceAll(strtext, ',', '~');
                araay[i][j] = strtext.replace(/,/gi, '~');
                //Alert(strtext)
                // document.getElementById(ArrName[i]).value.replace(',','%')
            }
            catch (err) {
                araay[i][j].toString().replace(/,/gi, '~');
                //alert(araay[i][j]);
                //Handle errors here
            }
            //alert(araay[i][j]);
            cellcnt = cellcnt + 1;
        }
    }
    //alert(araay.length);
    var strvalue = '';
    //Sending Array values into ServerSide
    for (var i = 0; i < araay.length; i++) {
        strvalue = strvalue + araay[i] + '|';
    }
    //alert(strvalue);   
    document.getElementById('<%=H_CommentCOLS.ClientID %>').value = strvalue;
 }

 function replaceAll(stringValue, replaceValue, newValue) {
     var functionReturn = new String(stringValue);
     while (true) {
         var currentValue = functionReturn;
         functionReturn = functionReturn.replace(replaceValue, newValue);
         if (functionReturn == currentValue)
             break;
     }
     return functionReturn;
 }


 function autoSizeWithCalendar(oWindow) {
     var iframe = oWindow.get_contentFrame();
     var body = iframe.contentWindow.document.body;

     var height = body.scrollHeight;
     var width = body.scrollWidth;

     var iframeBounds = $telerik.getBounds(iframe);
     var heightDelta = height - iframeBounds.height;
     var widthDelta = width - iframeBounds.width;

     if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
     if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
     oWindow.center();
 }
 function OnClientClose(oWnd, args) {
     var NameandCode;
     var arg = args.get_argument();
     var ctlid = document.getElementById('<%=H_CTL_ID.ClientID%>').value;
            if (arg) {
                NameandCode = arg.Comment.split('||');

                str = document.getElementById(ctlid).value;
                document.getElementById(ctlid).value = str + NameandCode[0].replace("/ap/", "'");
            }
        }



        //------------------------------------------------------------------------------------------------------------------------------------


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Assessment Grade Entry"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr valign="top">
                        <td align="left" colspan="8">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="Error">
                            </asp:Label>&nbsp;
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcdID" runat="server" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" colspan="1" width="20%">
                            <span class="field-label">Report</span></td>

                        <td align="left" colspan="1" width="30%">
                            <asp:DropDownList ID="ddlReportId" runat="server" OnSelectedIndexChanged="ddlReportId_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
                    <tr>
                         <td align="left"><span class="field-label">Stream</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Grade</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                      
                    </tr>
                    <tr>
                          <td align="left" colspan="1"><span class="field-label">Subject </span></td>

                        <td align="left" colspan="1">
                            <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbSubject" Width="100%" AutoPostBack="true"
                                MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains" OnSelectedIndexChanged="cmbSubject_SelectedIndexChanged"
                                HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                OnDataBound="cmbSubject_DataBound" OnItemDataBound="cmbSubject_ItemDataBound"
                                EmptyMessage="Start typing to search...">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col2">Subject Name</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>

                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "SBG_DESCR")%></li>
                                    </ul>
                                </ItemTemplate>
                                <FooterTemplate>
                                    A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                    items
                                </FooterTemplate>
                            </telerik:RadComboBox>
                            </td>
                        <td align="left"><span class="field-label">Subject   Group</span></td>

                        <td align="left">
                            <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbSubjectGroup" Width="100%"
                                MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                OnDataBound="cmbSubjectGroup_DataBound" OnItemDataBound="cmbSubjectGroup_ItemDataBound"
                                EmptyMessage="Start typing to search...">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col2">Description</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>

                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "DESCR2")%></li>
                                    </ul>
                                </ItemTemplate>
                                <FooterTemplate>
                                    A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                    items
                                </FooterTemplate>
                            </telerik:RadComboBox>

                        </td>
                     
                    </tr>
                    <tr>
                           <td align="left" colspan="1" id="tdCat1" runat="Server"><span class="field-label">Subject Category</span></td>

                        <td align="left" colspan="1" id="tdCat3" runat="Server">
                            <asp:DropDownList ID="ddlCategory" runat="server">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Report Schedule</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlRptSchedule" runat="server" OnSelectedIndexChanged="ddlRptSchedule_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                               <td align="left" colspan="2">
                                   </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click1"
                                Text="View Students" />
                            <asp:Button ID="btnSave1" runat="server" CssClass="button"
                                Text="Save & Next" ValidationGroup="MAINERROR" OnClientClick="GetStudentValues()" OnClick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnCanceldata" runat="server" CssClass="button"
                    Text="Cancel" OnClick="btnCanceldata_Click" />
                            <asp:DropDownList ID="ddlLevel" runat="server" Visible="False">
                                <asp:ListItem>Core</asp:ListItem>
                                <asp:ListItem>Extended</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr valign="top">
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Search Students</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Panel ID="Panel1" runat="server">
                                <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbStudent" Width="100%" AutoPostBack="true"
                                    MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                    HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                    OnDataBound="cmbStudent_DataBound" OnItemDataBound="cmbStudent_ItemDataBound"
                                    EmptyMessage="Start typing to search...">
                                    <HeaderTemplate>
                                        <ul>
                                            <li class="col1">Student Code</li>
                                            <li class="col2">Student Name</li>
                                        </ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul>
                                            <li class="col1">
                                                <%# DataBinder.Eval(Container.DataItem, "stu_no")%></li>
                                            <li class="col2">
                                                <%# DataBinder.Eval(Container.DataItem, "StudentName")%></li>
                                        </ul>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                        items
                                    </FooterTemplate>
                                </telerik:RadComboBox>
                            </asp:Panel>

                            <table>

                                <tr>
                                    <td>
                                        <telerik:RadSpell ID="RadSpell1" AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" ButtonType="LinkButton" IsClientID="true" runat="server" SupportedLanguages="en-US,English" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label
                                            ID="lblPages" runat="server" Font-Bold="True" ForeColor="#016b05"></asp:Label></td>
                                    <td>
                                        <asp:DataList ID="dlPages" runat="server" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server"  Text='<%# Bind("pageno") %>' ID="lblPageNo" OnClick="lblPageNo_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList></td>
                                    <td>
                                        <asp:LinkButton ID="lbtnKeys" runat="server" OnClientClick="javascript:return false;">Header Keys</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCommonData" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label></td>
                                </tr>
                            </table>

                        </td>

                    </tr>
                    <tr>
                        <td align="left" colspan="4">

                            <asp:GridView ID="gvStudents" runat="server"
                                EmptyDataText="No Data Found" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EnableTheming="True"
                                OnPageIndexChanging="gvStudents_PageIndexChanging" HorizontalAlign="Center">
                                <HeaderStyle CssClass="gridheader_pop_big" />
                                <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                                <AlternatingRowStyle CssClass="griditem_alternative" HorizontalAlign="Center" />
                            </asp:GridView>



                        </td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSaveDetails" runat="server" CssClass="button"
                                Text="Save " ValidationGroup="MAINERROR" OnClientClick="GetStudentValues()" OnClick="btnSaveDetails_Click" />
                            &nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button"
                    Text="Save & Next" ValidationGroup="MAINERROR" OnClientClick="GetStudentValues()" OnClick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnSubCancel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="H_GRP_ID" runat="server"></asp:HiddenField>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    TargetControlID="Panel1"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="LnkSearch"
                    CollapseControlID="LnkSearch"
                    AutoCollapse="False"
                    AutoExpand="False"
                    ScrollContents="false"
                    TextLabelID="LnkSearch"
                    CollapsedText="Search Students"
                    ExpandedText="Hide Search">
                </ajaxToolkit:CollapsiblePanelExtender>

                <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_STU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SBJ_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SETUP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CTL_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CommentCOLS" runat="server"></asp:HiddenField>

                <div id="popup" runat="server" class="subHeader_E" style="display: none; text-align: left; vertical-align: middle; margin-top: -2px; margin-left: -3px; margin-right: 1px; border-style: solid; border-color: #1b80b6; border-width: 1.5px; font-family: Times New Roman;">
                    <div class="msg" style="color: #1b80b6; vertical-align: middle; padding-bottom: 2px;">Header Keys</div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="modalPopup1" Style="border-top-color: #1b80b6; border-top-style: solid; border-top-width: 1px; background-color: #ebeff1" Width="100%">
                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server"
                    TargetControlID="lbtnKeys"
                    PopupControlID="popup"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="0"
                    OffsetY="20"
                    PopDelay="50" />
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>
</asp:Content>

