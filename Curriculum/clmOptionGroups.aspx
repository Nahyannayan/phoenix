<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOptionGroups.aspx.vb" Inherits="clmActivity_D_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Option Group Detail
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"  cellspacing="0" width="100%">
        <tr>
            <td align="left" >
               
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                
            </td>
        </tr>
     <%--   <tr  valign="bottom">            
             <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>--%>
        <tr>
            <td  valign="bottom" >
                <table align="center" border="0" cellpadding="0" cellspacing="0"  width="100%">
                   <%-- <tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal id="ltLabel" runat="server" Text="Option Group Details"></asp:Literal></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Academic Year</span></td>
                       
                        <td align="left">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="true" >
                            </asp:DropDownList></td>
                        <td align="left" >
                           <span class="field-label"> Grade</span></td>
                       
                        <td align="left">
                            <asp:DropDownList id="ddlGRD_ID" runat="server"  AutoPostBack="true">
                            </asp:DropDownList></td>
                        <td align="left">
                           <span class="field-label"> Stream</span></td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlStream" runat="server"  AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr >
                        <td align="left">
                           <span class="field-label"> Option Group Name</span></td>
                       
                        <td align="left" colspan="5">
                            <asp:TextBox id="txtOpt_GroupName" runat="server" Width="220px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="Top">
                           <span class="field-label"> Subjects</span></td>
                        
                        <td align="left" colspan="5" valign="Top">
                <asp:GridView id="gvSubject_Detail" runat="server" AutoGenerateColumns="False" EmptyDataText="No subjects to display"
                   DataKeyNames="SBG_ID" class="table table-bordered table-row">
                    <rowstyle />
                    <emptydatarowstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SBG_ID" Visible ="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSBG_ID" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Optional">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkOptional" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </columns>
                    <selectedrowstyle />
                    <headerstyle />
                    <alternatingrowstyle />
                </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6" >
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add"  /><asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave" runat="server" CssClass="button" Text="Save"  />
                            <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                  
                </table>
            </td>
        </tr>
    </table>
    <input id="h_Row" type="hidden" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

