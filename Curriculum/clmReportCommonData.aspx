<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportCommonData.aspx.vb" Inherits="Curriculum_clmReportCommonData" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Course Content
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left"  />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            SkinID="error"  Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnAddCriteria">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="matters" width="20%" ><span class="field-label">Select Academic Year</span></td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Select Report Card</span></td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters" ><span class="field-label">Select Report Schedule</span></td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlRpf" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="matters" colspan="1"><span class="field-label">Select Header</span></td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlheader" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">Select Grade</span></td>
                                        <td align="left" class="matters" >
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"
                                             >
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="matters" colspan="1" ><span class="field-label">Select Subject</span></td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">Description</span></td>
                                        <td align="left" class="matters" colspan="3">
                                            <asp:TextBox ID="txtDetail" runat="server"  Width="100%"
                                                TextMode="MultiLine"  ></asp:TextBox>
                                            <telerik:RadSpell ID="RadSpell1" runat="server" ControlToCheck="txtDetail"
                                                SupportedLanguages="en-US,English" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="matters" colspan="4"
                                            >
                                            <asp:Button ID="btnAddCriteria" runat="server" CssClass="button" TabIndex="7"
                                                Text="Save " ValidationGroup="groupM1"   />
                                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                CssClass="button" TabIndex="8" Text="Cancel" UseSubmitBehavior="False"
                               />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="matters" colspan="4">
                                            <asp:GridView ID="gvcommon" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Academic year">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_ACD_ID" runat="server" Value='<%# Bind("RCP_ACD_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RPF Descr">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRSM_DESCR" runat="server" Text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_RSM_ID" runat="server" Value='<%# Bind("RCP_RSM_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RPF Descr">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRPF_DESCR" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_RPF_ID" runat="server" Value='<%# Bind("RCP_RPF_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Header">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRSD_HEADER" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_RSD_ID" runat="server" Value='<%# Bind("RCP_RSD_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Descr">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("RCP_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_RCP_ID" runat="server" Value='<%# Bind("RCP_ID") %>' />
                                                            <asp:HiddenField ID="HF_GRD_ID" runat="server" Value='<%# Bind("RCP_GRD_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_Edit" runat="server" OnClick="lnk_Edit_Click">Edit</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters"   valign="bottom"></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom"  >
                            <asp:HiddenField ID="hfRandom" runat="server" Value="1000000000"></asp:HiddenField>
                            <asp:RequiredFieldValidator ID="rdDetail" runat="server" ControlToValidate="txtDetail"
                                Display="None" ErrorMessage="Please enter data in the field subject criteria short name"
                                ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            &nbsp;&nbsp; &nbsp;
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

