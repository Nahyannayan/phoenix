<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReleaseStudentAOL.aspx.vb" Inherits="Curriculum_clmPublishReleaseStudents" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script>
              
     var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvStud.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}

    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkPublish")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }           
                
                
                function change_chk_state1(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkRelease")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }      
          
</script>
<table width="100%" id="Table1" border="0px">


<tr style="font-size: 12pt;">

<td width="50%" align="left" class="title" style="height: 45px">
    PUBLISH REPORT CARD</td>
</tr>
</table>
<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
cellspacing="0" style="width: 100%">
<tr>
<td align="left"  class="matters" style="font-weight: bold; width: 797px;"  valign="top">
<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
</td>
</tr>

<tr>
<td style="width: 797px">
<table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="0" cellspacing="0" style="width: 520px">
    <tr>
        <td align="left" class="matters" valign="top">
            Group</td>
        <td align="center" class="matters" valign="top">
            :</td>
        <td align="left" class="matters" valign="top">
            <asp:Label id="lblGrade" runat="server"></asp:Label></td>
        <td align="left" class="matters" valign="top">
            Assessment</td>
        <td align="center" class="matters" valign="top">
            :</td>
        <td align="left" class="matters" valign="top" style="width: 153px">
            <asp:Label id="lblSection" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td align="left" class="matters">
            Release Date</td>
        <td align="center" class="matters">
            :</td>
        <td align="left" class="matters" colspan="4" >
         <asp:TextBox runat="server" ID=txtRelease>
                            </asp:TextBox>
                            <asp:ImageButton ID="imgRelease" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />&nbsp;
                            <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply to All" Width="93px" /></td>
        
       
    </tr>
    <tr>
        <td align="left" class="matters" colspan="6" valign="top">
            <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" /></td>
    </tr>

<tr>
<td align="center" class="matters" colspan="6" style="height: 135px" valign="top">
            
                <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                CssClass="gridstyle" EmptyDataText="No Records Found"
                HeaderStyle-Height="30" PageSize="20" Width="556px" >
                <RowStyle CssClass="griditem" Height="25px" />
                <Columns>
                
                <asp:TemplateField HeaderText="ENQ_ID" Visible="False"><ItemTemplate>
                    <asp:Label ID="lblStAId" runat="server" text='<%# Bind("Sta_ID") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="ENQ_ID" Visible="False"><ItemTemplate>
                    <asp:Label ID="lblStuId" runat="server" text='<%# Bind("Stu_ID") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student ID"><ItemTemplate>
                    <asp:Label ID="lblStuNo" runat="server" text='<%# Bind("Stu_No") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><ItemTemplate>
                    <asp:Label ID="lblStuName" runat="server" text='<%# Bind("Stu_Name") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>

                   
                        
                         
                        
                        <asp:TemplateField HeaderText="Release"><EditItemTemplate>
                        <asp:CheckBox ID="chkRelease" runat="server"  />
                        </EditItemTemplate>
                        <HeaderTemplate>
                        <table><tr><td align="center">
                        Release </td>
                        </tr><tr>
                        <td align="center">
                        <asp:CheckBox ID="chkAll1" runat="server"  onclick="javascript:change_chk_state1(this);"
                        ToolTip="Click here to select/deselect all rows" /></td>
                        </tr>
                        </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:CheckBox ID="chkRelease" checked='<%# Bind("bRELEASE") %>' runat="server" onclick="javascript:highlight(this);" />
                        </ItemTemplate>
                        <HeaderStyle Wrap="False"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        
                        
                         <asp:TemplateField HeaderText="Release Date" >
                      <ItemStyle HorizontalAlign="left"   />
                      <ItemTemplate    >
                      <table>
                      <tr><td>
                      <asp:TextBox ID="txtDate" text='<%# Bind("STA_RELEASEDATE", "{0:dd/MMM/yyyy}") %>'  runat="server" CausesValidation="true" Width="100px"></asp:TextBox>
                      </td><td>
                       <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                       </td><td>
                       <asp:Label ID="lblErr" runat="server" Text="*" ForeColor="RED" visible="false"></asp:Label>
                       </td></tr>
                       </table>
                       <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                       Format="dd/MMM/yyyy" PopupButtonID="imgDate"  PopupPosition="BottomLeft" TargetControlID="txtDate">
                      </ajaxToolkit:CalendarExtender>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                        
</Columns>
                <SelectedRowStyle CssClass="Green" />
                <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
</td></tr>
<tr>


<td style="width: 403px; height: 19px;" colspan="6" align="left"><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
    </td>


</tr>
</table>
<input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
type="hidden" value="=" /></td></tr>


</table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgRelease" PopupPosition="BottomLeft" TargetControlID="txtRelease">
    </ajaxToolkit:CalendarExtender>
    &nbsp;<asp:HiddenField ID="hfSGR_ID" runat="server" />
&nbsp;<asp:HiddenField id="hfCAD_ID" runat="server">
    </asp:HiddenField>
    

</asp:Content>

