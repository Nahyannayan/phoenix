Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule_bulk
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_ACTIVITY_SCHEDULE) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    'Populate Term 
                    BindTerm()
                    'Populate Activity
                    BindActivity()

                    BindActivityDetails()
                    'Populate Grade
                    GetAllGradeForActivity()
                    BindGradeSlabDetails()
                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        PopulateDetails(ViewState("viewid"))
                        Enable_disable_control(False, True)
                    ElseIf ViewState("datamode") = "retest" Then
                        ltLabel.Text = "Retest Activity Schedule"
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        PopulateDetails(ViewState("viewid"), True)
                        h_PARENT_ID.Value = ViewState("viewid")
                        ViewState("datamode") = "add"
                        h_CAS_ID.Value = ""
                        ViewState("retest") = True
                        Enable_disable_control(False, True)
                        Enable_disable_control(True, False)
                        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ElseIf ViewState("datamode") = "add" Then
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub PopulateDetails(ByVal vCAS_ID As String, Optional ByVal bRetest As Boolean = False)
        Dim params As String() = vCAS_ID.Split("____")
        Dim vpGRD_ID, vpSBG_ID, vpCAD_ID As String
        vpGRD_ID = params(0)
        vpSBG_ID = params(4)
        vpCAD_ID = params(8)
        Dim vACT_SCH() As ACTIVITYSCHEDULE
        vACT_SCH = ACTIVITYSCHEDULE.GetScheduleDetails(vpGRD_ID, vpSBG_ID, vpCAD_ID)
       
        UsrAssessmentScheduler1.FillDetails(vACT_SCH)

        'ddlAca_Year.SelectedValue = vACT_SCH.ACD_ID
        'ddlTerm.SelectedValue = vACT_SCH.TRM_ID
        'ddlACTIVITYDET.SelectedValue = vACT_SCH.ACTIVITY_ID
        'FillSelectedDetails(chkGRD_ID.Items, vACT_SCH.GRADE_ID)
        'h_PARENT_ID.Value = vACT_SCH.PARENT_ID
        'Dim bCORE_EXT As Boolean = False
        'If vACT_SCH.TYPE_LEVEL <> "NORMAL" Then
        '    bCORE_EXT = True
        'End If
        'h_SBM_ID.Value = vACT_SCH.SUBJECT_ID & "___" & CInt(bCORE_EXT) & "___" & vACT_SCH.SUBJECT

        'imgSubject_Click(Nothing, Nothing)
        'FillSelectedDetails(lstSubjectGroups.Items, vACT_SCH.SUBJECT_GROUP_ID)
        'HideShowDurationDetails()
        'If bRetest Then

        '    lblDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
        '    lblGradeSlab.Text = ddlGradeSlab.Items.FindByValue(vACT_SCH.GRADE_SLAB_ID).Text
        '    lblDuration.Text = vACT_SCH.DURATION & " Hrs"
        '    Dim strTime As String
        '    strTime = IIf(vACT_SCH.ACTIVITY_TIME.Hour > 12, vACT_SCH.ACTIVITY_TIME.Hour - 12, vACT_SCH.ACTIVITY_TIME.Hour) & " : "
        '    strTime += Format(vACT_SCH.ACTIVITY_TIME, "mm")
        '    If vACT_SCH.ACTIVITY_TIME.Hour < 12 Then
        '        strTime += " AM"
        '    Else
        '        strTime += " PM"
        '    End If
        '    lblTime.Text = strTime
        '    lblFromDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
        '    lblToDate.Text = Format(vACT_SCH.ACTIVTY_TO_DATE, OASISConstants.DateFormat)
        '    lblMinMark.Text = Math.Round(vACT_SCH.MIN_MARK, 2)
        '    lblMaxMark.Text = Math.Round(vACT_SCH.MAX_MARK, 2)
        '    lblDescription.Text = vACT_SCH.DESCRIPTION
        '    lblRemarks.Text = vACT_SCH.REMARKS
        '    chkviewbmandatory.Checked = vACT_SCH.bATTEND_COMPULSORY
        'Else
        '    h_CAS_ID.Value = vACT_SCH.CAS_ID
        '    txtFromDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
        '    txtToDate.Text = Format(vACT_SCH.ACTIVTY_TO_DATE, OASISConstants.DateFormat)
        '    txtDate.Text = Format(vACT_SCH.ACTIVTY_DATE, OASISConstants.DateFormat)
        '    ddlTimeH.SelectedValue = IIf(vACT_SCH.ACTIVITY_TIME.Hour > 12, vACT_SCH.ACTIVITY_TIME.Hour - 12, vACT_SCH.ACTIVITY_TIME.Hour)
        '    If vACT_SCH.ACTIVITY_TIME.Hour < 12 Then
        '        ddlTimeAMPM.SelectedIndex = 0
        '    Else
        '        ddlTimeAMPM.SelectedIndex = 1
        '    End If
        '    ddlTimeM.SelectedValue = vACT_SCH.ACTIVITY_TIME.Minute
        '    txtDuration.Text = vACT_SCH.DURATION
        '    ddlGradeSlab.SelectedValue = vACT_SCH.GRADE_SLAB_ID
        '    txtMinMark.Text = Math.Round(vACT_SCH.MIN_MARK, 2)
        '    txtMaxMark.Text = Math.Round(vACT_SCH.MAX_MARK, 2)
        '    txtDescription.Text = vACT_SCH.DESCRIPTION
        '    txtremarks.Text = vACT_SCH.REMARKS
        '    chkbAttendanceMandatory.Checked = vACT_SCH.bATTEND_COMPULSORY
        'End If
    End Sub

    Private Sub BindGradeSlabDetails()
        ddlGradeSlab.DataSource = ACTIVITYSCHEDULE.GetGradeSlab(Session("sBSUID"))
        ddlGradeSlab.DataTextField = "GSM_DESC"
        ddlGradeSlab.DataValueField = "GSM_SLAB_ID"
        ddlGradeSlab.DataBind()
        ddlGradeSlab.Items.Add(New ListItem("--", 0))
        ddlGradeSlab.Items.FindByText("--").Selected = True
        ddlGradeSlab.Attributes.Add("onchange", "SetMaxMark(this,'" & txtMax.ClientID & "');")
    End Sub

    Private Sub FillSelectedDetails(ByRef lstitems As ListItemCollection, ByVal selVal As Hashtable)
        Dim ienum As IDictionaryEnumerator = selVal.GetEnumerator
        While ienum.MoveNext
            lstitems.FindByValue(ienum.Value).Selected = True
        End While
    End Sub

    Private Sub BindActivity()
        ddlActivity.DataSource = ACTIVITYMASTER.GetACTIVITY_M(Session("sBSUID"))
        ddlActivity.DataTextField = "CAM_DESC"
        ddlActivity.DataValueField = "CAM_ID"
        ddlActivity.DataBind()
        BindActivityDetails()
    End Sub

    Private Sub BindActivityDetails()
        ddlACTIVITYDET.DataSource = ACTIVITYSCHEDULE.GetActivityDetails(ddlAca_Year.SelectedValue, ddlTerm.SelectedValue, ddlActivity.SelectedValue)
        ddlACTIVITYDET.DataTextField = "CAD_DESC"
        ddlACTIVITYDET.DataValueField = "CAD_ID"
        ddlACTIVITYDET.DataBind()
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub

    Sub GetAllGradeForActivity()
        'ddlGrade.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBSUID"), ddlAca_Year.SelectedValue)
        'ddlGrade.DataTextField = "GRM_GRD_ID"
        'ddlGrade.DataValueField = "GRM_DISPLAY"
        'ddlGrade.DataBind()
        PopulateGrade(ddlGrade, ddlAca_Year.SelectedValue)
        h_GRD_IDs.Value = ddlGrade.SelectedValue
        GetSelectedSubjects()
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + acdid
                If ViewState("GRD_ACCESS") > 0 Then
                    str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                             & " WHERE GSA_ACD_ID=" + Session("Current_ACD_ID") + " and  (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
                End If
                str_query += " order by grd_displayorder"
            Else
                'str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                '                      & "  grm_acd_id=" + acdid

                str_query = "SELECT DISTINCT " & _
                "CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY + '-' + STM_DESCR END AS GRM_DISPLAY, " & _
" GRADE_BSU_M.GRM_GRD_ID + '|' + CONVERT(VARCHAR(100), STREAM_M.STM_ID) AS GRM_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
" STREAM_M.STM_ID FROM GROUPS_TEACHER_S INNER JOIN " & _
" GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID  " & _
" AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
"GRADE_BSU_M INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
" STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID ON GROUPS_M.SGR_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
" GROUPS_M.SGR_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND GROUPS_M.SGR_STM_ID = GRADE_BSU_M.GRM_STM_ID AND " & _
" GROUPS_M.SGR_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & " WHERE " _
                      & "  grm_acd_id=" & acdid & " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId")
                str_query += " order by grd_displayorder"
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If ddlTerm.SelectedIndex <> -1 Then
            BindActivity()
        Else
            ddlACTIVITYDET.Items.Clear()
        End If
    End Sub



    'Sub Student_Detail_Leaves()

    '    Using readerSTUD_Detail_Leaves As SqlDataReader = AccessStudentClass.GetStudent_Detail_Leaves(ViewState("viewid"))
    '        While readerSTUD_Detail_Leaves.Read


    '            ViewState("STU_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STU_ID"))
    '            ViewState("ACD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("ACD_ID"))
    '            ViewState("GRD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("GRD_ID"))
    '            ViewState("SCT_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SCT_ID"))
    '            ViewState("STM_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STM_ID"))
    '            ViewState("SHF_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SHF_ID"))



    '        End While
    '    End Using
    'End Sub
    Sub Enable_disable_control(ByVal bEnable As Boolean, ByVal bAll As Boolean)
        If bAll Then
            ddlAca_Year.Enabled = bEnable
            ddlTerm.Enabled = bEnable
            ddlGrade.Enabled = bEnable
            ddlACTIVITYDET.Enabled = bEnable
        End If
    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlAca_Year.Enabled = False
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnSave.Enabled = True
        'Dim idRow As New Label
        'idRow = TryCast(row.FindControl("lblID"), Label)
        'Dim iEdit As Integer = 0
        'Dim iIndex As Integer = 0
        'iIndex = CInt(idRow.Text)
        ''loop through the data table row  for the selected rowindex item in the grid view
        ''For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        ''Next
        'For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        '    If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") = ddlGRD_ID_EDIT.SelectedItem.Value
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("GRD_DESC") = ddlGRD_ID_EDIT.SelectedItem.Text
        '        Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT") = txtCAD_DESC_Edit.Text
        '        Exit For
        '    End If
        'Next
    End Sub

    Private Function ValidPage() As Boolean
        'lblError.Text = ""
        'If GetSubjectGroups().Count <= 0 Then
        '    lblError.Text += "<br>Please Select atleast one group"
        'End If
        'If trPeriod.Visible Then
        '    If txtFromDate.Text = "" OrElse Not IsDate(txtFromDate.Text) Then
        '        lblError.Text += "<br>Please Enter valid From Date"
        '    End If
        '    If txtToDate.Text = "" OrElse Not IsDate(txtToDate.Text) Then
        '        lblError.Text += "<br>Please Enter valid To Date"
        '    End If
        'Else
        '    If txtDate.Text = "" OrElse Not IsDate(txtDate.Text) Then
        '        lblError.Text += "<br>Please Enter valid Date"
        '    End If
        '    If txtDuration.Text = "" Then
        '        lblError.Text += "<br>Please Enter Activity Duration"
        '    End If
        'End If

        'If txtMinMark.Text = "" Then
        '    lblError.Text += "<br>Please Enter Minimum Pass Mark"
        'End If
        'If txtMaxMark.Text = "" Then
        '    lblError.Text += "<br>Please Enter Maximum Mark"
        'End If
        'If lblError.Text = "" Then
        '    Return True
        'Else
        '    Return False
        'End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'usrGroupList.GetSelectedNode(httab)


        'If Not ValidPage() Then
        '    Exit Sub
        'End If
        'Dim ACT_SCH As New ACTIVITYSCHEDULE

        'ACT_SCH.ACD_ID = ddlAca_Year.SelectedValue
        'ACT_SCH.TRM_ID = ddlTerm.SelectedValue
        'ACT_SCH.ACTIVITY_ID = ddlACTIVITYDET.SelectedValue
        'ACT_SCH.GRADE_ID = GetSelectedGrades()
        'ACT_SCH.SUBJECT_ID = h_SBM_ID.Value
        'ACT_SCH.SUBJECT_GROUP_ID = GetSubjectGroups()

        'If trPeriod.Visible Then
        '    ACT_SCH.ACTIVTY_DATE = CDate(txtFromDate.Text)
        '    ACT_SCH.ACTIVTY_TO_DATE = CDate(txtToDate.Text)
        '    ACT_SCH.ACTIVITY_TIME = CDate(txtToDate.Text)
        '    ACT_SCH.DURATION = 0
        'Else
        '    ACT_SCH.ACTIVTY_DATE = CDate(txtDate.Text)
        '    ACT_SCH.ACTIVTY_TO_DATE = CDate(txtDate.Text)
        '    ACT_SCH.ACTIVITY_TIME = GetTime()
        '    ACT_SCH.DURATION = txtDuration.Text
        'End If

        'ACT_SCH.GRADE_SLAB_ID = ddlGradeSlab.SelectedValue
        'ACT_SCH.MIN_MARK = txtMinMark.Text
        'ACT_SCH.MAX_MARK = txtMaxMark.Text
        'ACT_SCH.DESCRIPTION = txtDescription.Text
        'ACT_SCH.REMARKS = txtremarks.Text
        'ACT_SCH.TYPE_LEVEL = GetTypeLevel()
        'ACT_SCH.bATTEND_COMPULSORY = chkbAttendanceMandatory.Checked
        'ACT_SCH.bAUTOALLOCATE = chkAutoAllocate.Checked
        'ACT_SCH.PARENT_ID = IIf(h_PARENT_ID.Value <> "", h_PARENT_ID.Value, 0)
        'ACT_SCH.CAS_ID = IIf(h_CAS_ID.Value <> "", h_CAS_ID.Value, 0)
        ''If Not Master.IsSessionMatchesForSave() Then
        ''    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        ''    Exit Sub
        ''End If
        Dim ACT_SCH() As ACTIVITYSCHEDULE = UsrAssessmentScheduler1.GetScheduleDetails(lblError.Text)
        If lblError.Text <> "" Then
            Exit Sub
        End If
        Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = ACTIVITYSCHEDULE.SaveDetails(ACT_SCH, objConn, stTrans)
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'errNo = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            If errNo = 0 Then
                stTrans.Commit()
                'stTrans.Rollback()
                ClearAllFields()
                lblError.Text = "Details saved sucessfully..."
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(errNo)
            End If
        Catch
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(-1)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearAllFields(Optional ByVal bClearTerm As Boolean = True)
        If bClearTerm Then
            ddlTerm.ClearSelection()
        End If
        ddlGrade.ClearSelection()
        h_SBM_ID.Value = ""
        usrSubjects.SelectAll = False
        usrSubjects.ClearSelection()
        UsrAssessmentScheduler1.Bind()
        UsrAssessmentScheduler1.Clear()

    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Enable_disable_control(True, False)
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Enable_disable_control(True, True)
            ClearAllFields()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAllFields()
        BindTerm()
        ' UsrAssessmentScheduler1.ACD_ID = ddlAca_Year.SelectedValue
    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    'Protected Sub chkGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGRD_ID.SelectedIndexChanged
    '    Dim strSelectedGrades As String = String.Empty
    '    Dim strSeperator As String = String.Empty
    '    'For Each lstItem As ListItem In chkGRD_ID.Items
    '    '    If lstItem.Selected Then
    '    '        strSelectedGrades += strSeperator + lstItem.Value
    '    '        strSeperator = "___"
    '    '    End If
    '    'Next
    '    h_GRD_IDs.Value = ddlGrade.SelectedValue
    '    'UsrAssessmentScheduler1.Grades = strSelectedGrades
    '    'UsrAssessmentScheduler1.ACD_ID = ddlAca_Year.SelectedValue
    '    'UsrAssessmentScheduler1.AssessmentType = GetAssessmentType()
    '    'UsrAssessmentScheduler1.TRM_ID = ddlTerm.SelectedValue
    '    'UsrAssessmentScheduler1.Activity_ID = ddlACTIVITYDET.SelectedValue
    '    'UsrAssessmentScheduler1.ACTIVITY_DESCRIPTION = ddlACTIVITYDET.SelectedItem.Text
    '    'UsrAssessmentScheduler1.Bind()
    '    GetSelectedSubjects()
    'End Sub

    Private Sub GetSelectedSubjects()

        Dim STM_ID As String = h_GRD_IDs.Value.Split("|")(1)
        h_GRD_IDs.Value = h_GRD_IDs.Value.Split("|")(0)
        Dim str_sql As String = String.Empty

        If Session("CurrSuperUser") = "Y" Then
            str_sql = "SELECT DISTINCT SUBJECTS_GRADE_S.SBG_ID," & _
            "(CASE SBG_PARENTS WHEN 'NA' THEN '' ELSE SBG_PARENTS+ '-' END)  + SBG_DESCR SBG_DESCR " & _
            " FROM SUBJECTS_GRADE_S " & _
            " WHERE SUBJECTS_GRADE_S.SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
            " AND SUBJECTS_GRADE_S.SBG_GRD_ID IN ('" & h_GRD_IDs.Value.Replace("___", "', '") & "')" & _
            " AND SUBJECTS_GRADE_S.SBG_STM_ID = " & STM_ID & _
            " AND SUBJECTS_GRADE_S.SBG_ACD_ID = " & ddlAca_Year.SelectedValue & _
            " ORDER BY SBG_DESCR "
        Else
            str_sql = "SELECT DISTINCT SUBJECTS_GRADE_S.SBG_ID," & _
        "(CASE SBG_PARENTS WHEN 'NA' THEN '' ELSE SBG_PARENTS+ '-' END)  + SBG_DESCR SBG_DESCR " & _
        " FROM GROUPS_TEACHER_S INNER JOIN GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID " & _
        " AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
"SUBJECTS_GRADE_S ON GROUPS_M.SGR_BSU_ID = SUBJECTS_GRADE_S.SBG_BSU_ID AND  " & _
"GROUPS_M.SGR_ACD_ID = SUBJECTS_GRADE_S.SBG_ACD_ID AND  " & _
"GROUPS_M.SGR_GRD_ID = SUBJECTS_GRADE_S.SBG_GRD_ID AND GROUPS_M.SGR_SBG_ID = SUBJECTS_GRADE_S.SBG_ID AND " & _
" GROUPS_M.SGR_STM_ID = SUBJECTS_GRADE_S.SBG_STM_ID " & _
        " WHERE SUBJECTS_GRADE_S.SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
        " AND SUBJECTS_GRADE_S.SBG_GRD_ID IN ('" & h_GRD_IDs.Value.Replace("___", "', '") & "')" & _
        " AND SUBJECTS_GRADE_S.SBG_STM_ID = " & STM_ID & _
        " AND SUBJECTS_GRADE_S.SBG_ACD_ID = " & ddlAca_Year.SelectedValue & _
        " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId") & _
        " ORDER BY SBG_DESCR "
        End If
        'str_sql = "SELECT  SUBJECTS_GRADE_S.SBG_ID, " & _
        '" SUBJECTS_GRADE_S.SBG_DESCR SBG_DESCR  " & _
        '" FROM SUBJECTS_GRADE_S INNER JOIN VW_GRADE_BSU_M ON " & _
        '" SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
        '" SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID AND " & _
        '" SUBJECTS_GRADE_S.SBG_BSU_ID = VW_GRADE_BSU_M.GRM_BSU_ID AND " & _
        '" SUBJECTS_GRADE_S.SBG_STM_ID = VW_GRADE_BSU_M.GRM_STM_ID " & _
        '" WHERE VW_GRADE_BSU_M.GRM_BSU_ID = '" & Session("sBSUID") & "' " & _
        '" AND SUBJECTS_GRADE_S.SBG_GRD_ID IN ('" & h_GRD_IDs.Value.Replace("___", "', '") & "')" & _
        '" AND SUBJECTS_GRADE_S.SBG_STM_ID = " & STM_ID & _
        '" ORDER BY SUBJECTS_GRADE_S.SBG_ORDER "
        usrSubjects.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

        'usrSubjects.DataSource = ACTIVITYSCHEDULE.GetSubjectGroups(Session("sBSUID"), ACD_ID, 0, lblSBG_ID.Text)
        usrSubjects.DataTextField = "SBG_DESCR"
        usrSubjects.DataValueField = "SBG_ID"
        'usrSubjects.ParentDataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        'usrSubjects.ParentLinkField = "SBG_PARENT_ID"
        usrSubjects.SelectAll = True
        usrSubjects.Bind()
    End Sub

    Private Function GetAssessmentType() As ASSESSMENT_TYPE
        Return ACTIVITYSCHEDULE.GetAssessmentType(ddlACTIVITYDET.SelectedValue)
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        h_PARENT_ID.Value = ""
        ViewState("reset") = Nothing
        ltLabel.Text = "Activity Schedule"
        ClearAllFields()
        Enable_disable_control(True, True)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        ClearAllFields(False)
        BindActivity()
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        UsrAssessmentScheduler1.Grades = h_GRD_IDs.Value
        UsrAssessmentScheduler1.ACD_ID = ddlAca_Year.SelectedValue
        UsrAssessmentScheduler1.BSU_ID = Session("sBSUID")
        Dim STM_ID As String = ddlGrade.SelectedValue.Split("|")(1)
        UsrAssessmentScheduler1.STM_ID = STM_ID '"1"
        UsrAssessmentScheduler1.AssessmentType = GetAssessmentType()
        UsrAssessmentScheduler1.TRM_ID = ddlTerm.SelectedValue
        UsrAssessmentScheduler1.Activity_ID = ddlACTIVITYDET.SelectedValue
        UsrAssessmentScheduler1.ACTIVITY_DESCRIPTION = ddlACTIVITYDET.SelectedItem.Text
        UsrAssessmentScheduler1.Subjects = usrSubjects.GetSelectedNode("___")
        UsrAssessmentScheduler1.maxMark = txtMax.Text
        UsrAssessmentScheduler1.minMark = txtMin.Text
        UsrAssessmentScheduler1.startDate = txtDate.Text
        UsrAssessmentScheduler1.gradingSlab = ddlGradeSlab.SelectedItem.Text
        UsrAssessmentScheduler1.Bind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_GRD_IDs.Value = ddlGrade.SelectedValue
        GetSelectedSubjects()
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindActivityDetails()
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub

    Protected Sub ddlACTIVITYDET_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACTIVITYDET.SelectedIndexChanged
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub


    Public Sub SetAOLDetails(ByVal vCAD_ID As String)
        Dim str_sql As String = String.Empty
        str_sql = "SELECT ISNULL(CAD_bAOL, 0) CAD_bAOLAD_ID,ISNULL(CAD_bWITHOUTSKILLS, 0) CAD_bWITHOUTSKILLS FROM ACT.ACTIVITY_D " & _
        " WHERE CAD_ACD_ID= '" & ddlAca_Year.SelectedValue & "' AND  CAD_ID ='" & vCAD_ID + "'"
        'Dim bHasAOLDetails As Boolean
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            UsrAssessmentScheduler1.HasAOLExams = ds.Tables(0).Rows(0).Item(0)
            UsrAssessmentScheduler1.WithoutSkills = ds.Tables(0).Rows(0).Item(1)
        End If
    End Sub

End Class
