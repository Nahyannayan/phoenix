<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSkill_Schedule.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetSUBJECT() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            var STM_ID = document.getElementById('<%=h_STM_ID.ClientID %>').value;

            var AcdId
            AcdId = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
          if (GRD_IDs == '') {
              alert('Please select Grade')
              return false;
          }
          result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + AcdId + "&STM_ID=" + STM_ID, "", sFeatures)
          if (result != '' && result != undefined) {
              document.getElementById('<%=h_SBM_ID.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Skill Schedule
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="9">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <%--  <td align="center" class="matters" valign="middle">&nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                            style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
                        <td align="center" class="text-danger font-small" >Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td   colspan="4">
                                        <asp:Panel ID="pnlQuickHeader" runat="server" CssClass="" Width="100%">
                                            <div style="width: 100%; height: 100%; cursor: pointer;">
                                                <asp:ImageButton ID="imgQuick" Style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)" />
                                                <asp:Label ID="ltLabel" runat="server" Text="Assessment Schedule" CssClass="field-label"></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" >
                                        <asp:Panel ID="pnlQuickDetail" runat="server" Width="100%">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr runat="server" id="trResetDuration">
                                                    <td width="10%"><span class="field-label">Date </span>
                                                    </td>

                                                    <td width="20%">
                                                        <asp:Label runat="server" ID="lblDate" CssClass="field-value"></asp:Label>
                                                    </td>
                                                    <td width="10%"><span class="field-label">Time </span>
                                                    </td>

                                                    <td width="20%">
                                                        <asp:Label runat="server" ID="lblTime" CssClass="field-value"></asp:Label>
                                                    </td>
                                                    <td width="10%"><span class="field-label">Duration </span>
                                                    </td>

                                                    <td width="20%">
                                                        <asp:Label runat="server" ID="lblDuration" CssClass="field-value"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trResetPeriod">
                                                    <td width="10%">
                                                        <span class="field-label">From Date </span></td>

                                                    <td width="20%">
                                                        <asp:Label ID="lblFromDate" runat="server" CssClass="field-value"></asp:Label></td>
                                                    <td width="10%">
                                                        <span class="field-label">To Date</span></td>

                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblToDate" runat="server" CssClass="field-value"></asp:Label></td>
                                                   <td width="40%"  colspan="2">
                                                        </td>
                                                 
                                                </tr>
                                                <tr>
                                                    <td width="10%"><span class="field-label">Grade Slab</span>
                                                    </td>

                                                    <td align="left" colspan="5" width="90%">
                                                        <asp:Label ID="lblGradeSlab" runat="server" CssClass="field-value" Text="-"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%"><span class="field-label">Min Pass Mark</span>
                                                    </td>

                                                    <td width="20%">
                                                        <asp:Label ID="lblMinMark" runat="server"  Text="-" CssClass="field-value"></asp:Label>
                                                    </td>
                                                    <td width="10%">
                                                        <span class="field-label">Max Mark</span>
                                                    </td>

                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblMaxMark" runat="server"  Text="-" CssClass="field-value"></asp:Label>
                                                    </td>
                                                    <td width="40%"  colspan="2">
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                        <span class="field-label">Description</span></td>

                                                    <td align="left" colspan="5" width="90%">
                                                        <asp:Label ID="lblDescription" runat="server" Text="-" CssClass="field-value"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="10%">
                                                        <span class="field-label">Remarks</span></td>

                                                    <td align="left" colspan="5"  width="90%">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="-" CssClass="field-value"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="6">
                                                        <asp:CheckBox ID="chkviewbmandatory" Text="Attendance Mandatory" runat="server" Enabled="False" CssClass="field-label"></asp:CheckBox></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeQuick" runat="server" CollapseControlID="pnlQuickHeader"
                                            Collapsed="true" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Original Test Details...)"
                                            ExpandControlID="pnlQuickHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
                                            ImageControlID="imgQuick" TargetControlID="pnlQuickDetail">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"  width="20%">
                                        <span class="field-label">Term</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Assessment</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlACTIVITY" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACTIVITY_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="20" visible="false">
                                    <td align="left">
                                        <span class="field-label">Grade</span></td>

                                    <td align="left" colspan="3" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="21">
                                    <td align="left">
                                        <span class="field-label">Subject</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtSubject" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgSubject" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSUBJECT();" OnClick="imgSubject_Click"></asp:ImageButton></td>
                                </tr>
                                <tr id="trCoreExt" runat="server" visible="false">
                                    <td align="left" colspan="4">
                                        <asp:RadioButton ID="radCore" runat="server" Checked="True" GroupName="LEVEL" Text="Core"></asp:RadioButton>&nbsp;
                                        <asp:RadioButton ID="radExtended" runat="server" GroupName="LEVEL"
                                            Text="Extended"></asp:RadioButton>
                                        <asp:RadioButton ID="radNormal" runat="server" GroupName="LEVEL"
                                            Text="Normal" Visible="False"></asp:RadioButton></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Groups</span></td>

                                    <td align="left" colspan="3">
                                        <asp:ListBox ID="lstSubjectGroups" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
                                </tr>
                                <tr runat="server" id="trPeriod">
                                    <td align="left">
                                        <span class="field-label">From Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" /></td>
                                    <td align="left" >
                                        <span class="field-label">To Date </span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false" /></td>
                                </tr>
                                <tr runat="server" id="trDate">
                                    <td align="left">
                                        <span class="field-label">Date</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                                </tr>
                                <tr runat="server" id="trTime">
                                    <td align="left">
                                        <span class="field-label">Time</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTimeH" runat="server" >
                                        </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlTimeM" runat="server" ></asp:DropDownList>&nbsp;<asp:DropDownList
                                            ID="ddlTimeAMPM" runat="server">
                                            <asp:ListItem Selected="True">AM</asp:ListItem>
                                            <asp:ListItem>PM</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" >
                                        <span class="field-label">Duration</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDuration" runat="server" ></asp:TextBox>
                                        Hrs</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Grade Slab</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlGradeSlab" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Min. Pass Mark</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMinMark" runat="server" ></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Max Mark</span></td>

                                    <td align="left" ><asp:TextBox ID="txtMaxMark" runat="server" ></asp:TextBox></td>
                                </tr>
                                <tr runat="server" id="trAOLMarks" visible="false">
                                    <td align="left" colspan="4">
                                        <table align="center" border="0" cellpadding="5" cellspacing="0"  width="100%">
                                            <tr >
                                                <td align="center" colspan="4">
                                                    <span class="field-label">Maximum Mark AOL Exams</span></td>
                                            </tr>
                                            <tr align="center">
                                                <td >
                                                    <span class="field-label">KU</span></td>
                                                <td >
                                                    <span class="field-label">APP</span></td>
                                                <td >
                                                    <span class="field-label">COMM</span></td>
                                                <td >
                                                    <span class="field-label">HOTS</span></td>
                                            </tr>
                                            <tr align="center">
                                                <td >
                                                    <asp:TextBox ID="txtAOLKU" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtAOLAPP" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtAOLCOMM" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtAOLHOTS" runat="server"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>





                                <tr runat="server" id="trSkill">
                                    <td align="left" colspan="4">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr >
                                                <td align="center"  colspan="10" class="title-bg">
                                                    <span class="field-label">Maximum Mark Skills</span></td>
                                            </tr>
                                            <tr align="center">
                                                <td >
                                                    <asp:Label ID="lblSkill1" runat="server" Text="Skill 1"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill2" runat="server" Text="Skill 2"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill3" runat="server" Text="Skill 3"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill4" runat="server" Text="Skill 4"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill5" runat="server" Text="Skill 5"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill6" runat="server" Text="Skill 6"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill7" runat="server" Text="Skill 7"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill8" runat="server" Text="Skill 8"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill9" runat="server" Text="Skill 9"></asp:Label></td>
                                                <td >
                                                    <asp:Label ID="lblSkill10" runat="server" Text="Skill 10"></asp:Label></td>
                                            </tr>
                                            <tr align="center">
                                                <td >
                                                    <asp:TextBox ID="txtSkill1" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill2" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill3" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill4" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill5" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill6" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill7" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill8" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill9" runat="server"></asp:TextBox></td>
                                                <td >
                                                    <asp:TextBox ID="txtSkill10" runat="server"></asp:TextBox></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>





                                <tr id="trWS" runat="server">
                                    <td align="left">
                                        <span class="field-label">Max Marks Without Skills </span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtWS" runat="server" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Description </span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDescription" runat="server" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtremarks" runat="server" TextMode="MultiLine" Width="367px" SkinID="MultiText"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:CheckBox ID="chkbAttendanceMandatory" runat="server" Text="Attendance Compulsory" CssClass="field-label"></asp:CheckBox><br />
                                        <asp:CheckBox ID="chkAutoAllocate" runat="server" Text="Auto Allocate Student" CssClass="field-label"></asp:CheckBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <%-- <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"  Text="Add" Visible="false" />--%>
                            <asp:Button ID="btnEdit" runat="server"
                                CausesValidation="False" CssClass="button" OnClick="btnEdit_Click" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_SBM_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_PARENT_ID" runat="server" />
                <asp:HiddenField ID="h_CAS_ID" runat="server" />
                <asp:HiddenField ID="h_" runat="server" />
                <asp:HiddenField ID="h_STM_ID" runat="server" />
                <asp:HiddenField ID="h_SBG_ID" runat="server" />



            </div>
        </div>
    </div>

</asp:Content>

