Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmStreamAllocationScience
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C310027") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    tblTC.Rows(5).Visible = False
                    tblTC.Rows(6).Visible = False
                    tblTC.Rows(7).Visible = False
                    tblTC.Rows(8).Visible = False
                    BindSection()

                    'lstChoice1 = BindOptions(lstChoice1)
                    'lstChoice2 = BindOptions(lstChoice2)
                    'lstChoice3 = BindOptions(lstChoice3)
                    ' BindAlloted()
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub


    Protected Sub ddlgvChoice1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvChoice2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvChoice3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private Methods"




    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim ddlgvChoice1 As DropDownList
        Dim ddlgvChoice2 As DropDownList
        Dim ddlgvChoice3 As DropDownList

        Dim sChoice1 As String = ""
        Dim sChoice2 As String = ""
        Dim sChoice3 As String = ""
        ViewState("slno") = 0

        'If txtRecords.Text <> "" Then
        '    str_query = "SELECT TOP " + txtRecords.Text + " STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR ," _
        '             & " STU_BSU_ID,ISNULL(SCIENCE,0) AS SCIENCE,ISNULL(MATHS,0) AS MATHS,ISNULL(OVERALL,0) AS OVERALL,ISNULL(ENGLISH,0) AS ENGLISH,ISNULL(SOCIAL,0) AS SOCIAL,CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,ALLOTEDOPTION FROM  " _
        '             & " FN_GetStudentStreamAllocationDetails('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + ",'10')" _
        '             & " WHERE 1=1"
        'Else
        '    str_query = "SELECT STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR ," _
        '                         & " STU_BSU_ID,ISNULL(SCIENCE,0) AS SCIENCE,ISNULL(MATHS,0) AS MATHS,ISNULL(OVERALL,0) AS OVERALL,ISNULL(ENGLISH,0) AS ENGLISH,ISNULL(SOCIAL,0) AS SOCIAL,CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,ALLOTEDOPTION FROM  " _
        '                         & " FN_GetStudentStreamAllocationDetails('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + ",'10')" _
        '                         & " WHERE 1=1"
        'End If






        If txtRecords.Text <> "" Then
            str_query = "SELECT TOP " + txtRecords.Text + " STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR,STU_BSU_ID,MARKS_BIO,MARKS,MARKS_PCB," _
                     & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,ALLOTEDOPTION FROM  " _
                     & " FN_GetStudentStreamAllocationDetails_science('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + ",'10')" _
                     & " WHERE 1=1"
        Else
            str_query = "SELECT  STU_ID,STU_GRD_ID,STU_ACD_ID,STU_SCT_ID,STU_NO,STU_NAME,SCT_DESCR,STU_BSU_ID,MARKS_BIO,MARKS,MARKS_PCB," _
                   & " CHOICE1,CHOICE2,CHOICE3,ALLOTEDSTREAM,ALLOTEDOPTION FROM  " _
                   & " FN_GetStudentStreamAllocationDetails_science('" + Session("sbsuid") + "'," + Session("Current_ACD_ID") + ",'10')" _
                   & " WHERE 1=1"
        End If

        If ddlType.SelectedValue = "MATHS" Then
            str_query += " AND MARKS_BIO IS NULL"
        ElseIf ddlType.SelectedValue = "BIOLOGY" Then
            str_query += " AND MARKS_BIO IS NOT NULL"
        End If



        If txtName.Text <> "" Then
            str_query += " AND STU_NAME LIKE '%" + txtName.Text + "%'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If

        If ddlSection.SelectedValue <> "" Then
            str_query += " AND SCT_DESCR='" + ddlSection.SelectedItem.Text + "'"
        End If

        If txtMPC.Text <> "" Then
            str_query += " AND MARKS>=" + txtMPC.Text
        End If

        If txtMPCB.Text <> "" Then
            str_query += " AND MARKS_BIO>=" + txtMPCB.Text
        End If

        If txtPCB.Text <> "" Then
            str_query += " AND MARKS_PCB>=" + txtPCB.Text
        End If
        If rdAllocated.Checked = True Then
            str_query += " AND ALLOTEDSTREAM<>''"
        End If

        If rdNotAllocated.Checked = True Then
            str_query += " AND ALLOTEDSTREAM=''"
        End If

        Dim strChoice1 As String = getCheckedLists(lstChoice1)
        If strChoice1 <> "" Then
            str_query += " AND CHOICE1_ID IN(" + strChoice1 + ")"
        End If

        Dim strChoice2 As String = getCheckedLists(lstChoice2)
        If strChoice2 <> "" Then
            str_query += " AND CHOICE2_ID IN(" + strChoice2 + ")"
        End If


        Dim strChoice3 As String = getCheckedLists(lstChoice3)
        If strChoice3 <> "" Then
            str_query += " AND CHOICE3_ID IN(" + strChoice3 + ")"
        End If

        If gvStud.Rows.Count > 0 Then

            ddlgvChoice1 = gvStud.HeaderRow.FindControl("ddlgvChoice1")
            ddlgvChoice2 = gvStud.HeaderRow.FindControl("ddlgvChoice2")
            ddlgvChoice3 = gvStud.HeaderRow.FindControl("ddlgvChoice3")

            If Not ddlgvChoice1 Is Nothing Then
                If ddlgvChoice1.SelectedItem.Text <> "ALL" And ddlgvChoice1.SelectedItem.Text <> "" Then
                    str_query += " AND CHOICE1 LIKE '" + ddlgvChoice1.SelectedItem.Text + "%'"
                    sChoice1 = ddlgvChoice1.SelectedItem.Text
                End If

                If ddlgvChoice2.SelectedItem.Text <> "ALL" And ddlgvChoice2.SelectedItem.Text <> "" Then
                    str_query += " AND CHOICE2 LIKE '" + ddlgvChoice2.SelectedItem.Text + "%'"
                    sChoice2 = ddlgvChoice2.SelectedItem.Text
                End If

                If ddlgvChoice3.SelectedItem.Text <> "ALL" And ddlgvChoice2.SelectedItem.Text <> "" Then
                    str_query += " AND CHOICE3 LIKE '" + ddlgvChoice3.SelectedItem.Text + "%'"
                    sChoice3 = ddlgvChoice3.SelectedItem.Text
                End If
            End If
        End If



        If ddlType.SelectedValue = "BIOLOGY" Then
            str_query += " ORDER BY MARKS_BIO,MARKS DESC"
        Else
            str_query += " ORDER BY MARKS DESC"
        End If
        'str_query += " ORDER BY OVERALL DESC,SCIENCE DESC,MATHS DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()

        End If

        ddlgvChoice1 = gvStud.HeaderRow.FindControl("ddlgvChoice1")
        ddlgvChoice2 = gvStud.HeaderRow.FindControl("ddlgvChoice2")
        ddlgvChoice3 = gvStud.HeaderRow.FindControl("ddlgvChoice3")


        If Not ddlgvChoice1 Is Nothing Then
            ddlgvChoice1 = BindGridOption(ddlgvChoice1)
            If Not ddlgvChoice1.Items.FindByText(sChoice1) Is Nothing Then
                ddlgvChoice1.Items.FindByText(sChoice1).Selected = True
            End If
        End If
        If Not ddlgvChoice2 Is Nothing Then
            ddlgvChoice2 = BindGridOption(ddlgvChoice2)
            If Not ddlgvChoice2.Items.FindByText(sChoice2) Is Nothing Then
                ddlgvChoice2.Items.FindByText(sChoice2).Selected = True
            End If
        End If
        If Not ddlgvChoice3 Is Nothing Then
            ddlgvChoice3 = BindGridOption(ddlgvChoice3)
            If Not ddlgvChoice3.Items.FindByText(sChoice3) Is Nothing Then
                ddlgvChoice3.Items.FindByText(sChoice3).Selected = True
            End If
        End If


    End Sub

    Function getCheckedLists(ByVal lst As CheckBoxList) As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To lst.Items.Count - 1
            If lst.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += "'" + lst.Items(i).Value + "'"
            End If
        Next

        Return str
    End Function


    Sub GetHeaderKeys(ByVal ltprocess As Literal, ByVal stmId As String)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        ' Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try

            Dim strSQL As String = "SELECT SGM_DESCR,isnull((SELECT STUFF((SELECT ','+ SBG_DESCR " _
            & " FROM SUBJECTS_GRADE_S AS K INNER JOIN  SUBJECTOPTION_GROUP_S AS L ON K.SBG_ID=L.SGS_SBG_ID" _
            & "  WHERE SGS_SGM_ID = A.SGM_ID AND ISNULL(SGS_bOPTIONAL,'FALSE')='FALSE'" _
            & " ORDER BY SBG_DESCR for xml path('')),1,1,'')),''), " _
            & "isnull((SELECT STUFF((SELECT '/'+ SBG_DESCR " _
            & " FROM SUBJECTS_GRADE_S AS K INNER JOIN  SUBJECTOPTION_GROUP_S AS L ON K.SBG_ID=L.SGS_SBG_ID" _
            & "  WHERE SGS_SGM_ID = A.SGM_ID AND ISNULL(SGS_bOPTIONAL,'FALSE')='TRUE'" _
            & " ORDER BY SBG_DESCR for xml path('')),1,1,'')),'')," _
            & "(SELECT COUNT(SSM_ID) FROM SUBJECTSTREAMALLOCATION WHERE SSM_SGM_ID=A.SGM_ID)" _
            & " FROM SUBJECTOPTION_GROUP_M AS A" _
            & " WHERE SGM_GRD_ID='11' AND SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
            & " AND SGM_STM_ID=" + stmId

            reader = SqlHelper.ExecuteReader(conn, CommandType.Text, strSQL)
            sb.AppendLine("<table class=""table table-bordered"" border=1 >")
            sb.AppendLine("<tr ><td >Option</td><td >Alloted</td><td >Subjects</td></tr>")
            While reader.Read
                sb.AppendLine("<tr ><td >" + reader.GetString(0) + "</td><td  align=center>" + reader.GetValue(3).ToString + "</td><td >" + reader.GetString(1) + IIf(reader.GetString(2) <> "", "," + reader.GetString(2), "") + "</td></tr>")

                'sb.AppendLine("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>" + reader.GetString(0) + "</Div>")
                'sb.AppendLine("<div style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>Alloted-" + reader.GetValue(3).ToString + "</div>")
                'sb.AppendLine("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #800000;font-weight: bold;'>Subjects- " + reader.GetString(1) + IIf(reader.GetString(2) <> "", "," + reader.GetString(2), "") + "</Div>")
                'sb.AppendLine("<br>")
            End While
            reader.Close()
            sb.AppendLine("</table>")
            'If sb.ToString = "" Then
            '    sb.Append("<div style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>No Rule Available !!!</div>")
            'End If
            ltprocess.Text = sb.ToString

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Sub GetHeaderKeysRequested(ByVal ltrprocess As Literal, ByVal stream As String)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        ' Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Dim choice1 As Integer = 0
        Dim choice2 As Integer = 0
        Dim choice3 As Integer = 0

        sqlstr = "SELECT STM_DESCR,SGM_DESCR,ISNULL(SBG_DESCR,'') AS SBG_DESCR," _
               & " SUM(CHOICE1) AS CHOICE1,SUM(CHOICE2) AS COICE2,SUM(CHOICE3) AS CHOICE3" _
               & " FROM(SELECT STM_DESCR,SGM_DESCR,SBG_DESCR,COUNT(SOGS_ID) AS CHOICE1,CHOICE2=0,CHOICE3=0 FROM " _
               & " SUBJECTOPTION_GROUP_M AS A " _
               & " INNER JOIN VW_STREAM_M AS D ON A.SGM_STM_ID=D.STM_ID" _
               & " INNER JOIN SUBJECTOPTIONGROUP_SELECTION AS B ON A.SGM_ID=B.SOGS_CHOICE1" _
               & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS C ON B.SOGS_CHOICE1_OPT_SBG_ID=C.SBG_ID" _
               & " WHERE SOGS_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " AND SGM_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " GROUP BY STM_DESCR,SGM_DESCR,SBG_DESCR" _
               & " UNION ALL" _
               & " SELECT STM_DESCR,SGM_DESCR,SBG_DESCR,CHOICE2=0,COUNT(SOGS_ID) AS CHOICE2,CHOICE3=0 FROM " _
               & " SUBJECTOPTION_GROUP_M AS A " _
               & " INNER JOIN VW_STREAM_M AS D ON A.SGM_STM_ID=D.STM_ID" _
               & " INNER JOIN SUBJECTOPTIONGROUP_SELECTION AS B ON A.SGM_ID=B.SOGS_CHOICE2" _
               & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS C ON B.SOGS_CHOICE2_OPT_SBG_ID=C.SBG_ID" _
               & " WHERE SOGS_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " AND SGM_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " GROUP BY STM_DESCR,SGM_DESCR,SBG_DESCR" _
               & " UNION ALL " _
               & " SELECT STM_DESCR,SGM_DESCR,SBG_DESCR,CHOICE2=0,CHOICE2=0,COUNT(SOGS_ID) AS  CHOICE3 FROM" _
               & " SUBJECTOPTION_GROUP_M AS A " _
               & " INNER JOIN VW_STREAM_M AS D ON A.SGM_STM_ID=D.STM_ID" _
               & " INNER JOIN SUBJECTOPTIONGROUP_SELECTION AS B ON A.SGM_ID=B.SOGS_CHOICE2" _
               & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS C ON B.SOGS_CHOICE2_OPT_SBG_ID=C.SBG_ID" _
               & " WHERE SOGS_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " AND SGM_ACD_ID='" + Session("NEXT_ACD_ID") + "'" _
               & " GROUP BY STM_DESCR,SGM_DESCR,SBG_DESCR) P" _
               & " WHERE STM_DESCR='" + stream + "'" _
               & " GROUP BY STM_DESCR,SGM_DESCR,SBG_DESCR" _
               & " ORDER BY STM_DESCR,SGM_DESCR,SBG_DESCR"

        reader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlstr)

        sb.AppendLine("<table class=""table table-bordered"" border=1 >")
        sb.AppendLine("<tr ><td >Option</td><td >Subject</td><td >Choice 1</td><td >Choice 2</td><td >Choice 3</td></tr>")
        While reader.Read
            sb.AppendLine("<tr><td >" + reader.GetString(1) + "</td><td  >" + reader.GetString(2) + "</td><td  align=center>" + reader.GetValue(3).ToString + "</td><td  align=center>" + reader.GetValue(4).ToString + "</td><td  align=center>" + reader.GetValue(5).ToString + "</td></tr>")
            choice1 += reader.GetValue(3)
            choice2 += reader.GetValue(4)
            choice3 += reader.GetValue(5)
        End While
        reader.Close()
        sb.AppendLine("<tr><td >Total</td><td >&nbsp;</td><td  align=center>" + choice1.ToString + "</td><td  align=center>" + choice2.ToString + "</td><td  align=center>" + choice3.ToString + "</td></tr>")

        sb.AppendLine("</table>")
        ltrprocess.Text = sb.ToString
    End Sub

    Sub RequestedOptions()

    End Sub


    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + Session("CURRENT_ACD_ID") _
                   & " AND GRM_GRD_ID='10'"


        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Insert(0, li)
    End Sub

    Function BindOptions(ByVal lst As CheckBoxList) As CheckBoxList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        lst.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lst.DataSource = ds
        lst.DataTextField = "SGM_DESCR"
        lst.DataValueField = "SGM_ID"
        lst.DataBind()
        Return lst
    End Function

    Sub BindAllotedStream()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlStream.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID FROM OASIS..STREAM_M AS A" _
                               & " INNER JOIN OASIS..GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID" _
                               & " WHERE GRM_GRD_ID='11' AND GRM_ACD_ID='" + Session("NEXT_ACD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
    End Sub


    Sub BindAllotedOption()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlOption.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11' AND SGM_STM_ID='" + ddlStream.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlOption.DataSource = ds
        ddlOption.DataTextField = "SGM_DESCR"
        ddlOption.DataValueField = "SGM_ID"
        ddlOption.DataBind()

    End Sub

    Sub BindAllotedSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlSubject.Items.Clear()
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN SUBJECTOPTION_GROUP_S AS B ON A.SBG_ID=B.SGS_SBG_ID " _
                               & " WHERE SGS_SGM_ID=" + ddlOption.SelectedValue.ToString _
                               & " AND SGS_bOPTIONAL=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlSubject.Items.Insert(0, li)

    End Sub

    Sub BindAlloted()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID,(SELECT COUNT(SSM_ID) FROM  " _
                                     & " SUBJECTSTREAMALLOCATION WHERE SSM_STM_ID=A.STM_ID AND SSM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                     & " AND SSM_GRD_ID='11') AS STM_COUNT" _
                                     & " FROM OASIS..STREAM_M AS A" _
                                     & " INNER JOIN OASIS..GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID" _
                                     & " WHERE GRM_GRD_ID='11' AND GRM_ACD_ID='" + Session("NEXT_ACD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlStreams.DataSource = ds
        dlStreams.DataBind()

        dlRStreams.DataSource = ds
        dlRStreams.DataBind()
    End Sub


    Function BindGridOption(ByVal ddlChoice As DropDownList) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlChoice.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11' "
        str_query += " UNION ALL "

        str_query += "SELECT SGM_ID,SGM_DESCR+'('+SBG_SHORTCODE+')' AS SGM_DESCR FROM SUBJECTOPTION_GROUP_M  AS A " _
                    & " INNER JOIN SUBJECTOPTION_GROUP_S AS B ON A.SGM_ID=B.SGS_SGM_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGS_SBG_ID=C.SBG_ID" _
                    & " WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                    & " AND SGM_GRD_ID='11' AND SGS_bOPTIONAL=1"

        str_query = "SELECT * FROM (" + str_query + ") P ORDER BY SGM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlChoice.DataSource = ds
        ddlChoice.DataTextField = "SGM_DESCR"
        ddlChoice.DataValueField = "SGM_DESCR"
        ddlChoice.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlChoice.Items.Insert(0, li)
        Return ddlChoice
    End Function


    Sub SaveData(ByVal mode As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim lblStuId As Label
        Dim str_query As String
        Dim chkSelect As CheckBox

        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            If chkSelect.Checked = True Then
                str_query = "exec saveSTREAMALLOCATION " _
                         & lblStuId.Text + "," _
                         & Session("Next_ACD_ID") + "," _
                         & "'11'," _
                         & ddlStream.SelectedValue.ToString + "," _
                         & ddlOption.SelectedValue.ToString + "," _
                         & "'" + mode + "'," _
                         & ddlSubject.SelectedValue.ToString

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If

        Next
    End Sub

#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ViewState("slno") = 0
        tblTC.Rows(5).Visible = True
        tblTC.Rows(6).Visible = True
        tblTC.Rows(7).Visible = True
        tblTC.Rows(8).Visible = True
        BindAllotedStream()
        BindAllotedOption()
        BindAllotedSubject()
        GridBind()
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindAllotedOption()
        BindAllotedSubject()
    End Sub

    Protected Sub btnAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllot.Click
        SaveData("Add")
        GridBind()
        BindAlloted()
    End Sub

    Protected Sub btnDeAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeAllot.Click
        SaveData("Delete")
        GridBind()
        BindAlloted()
    End Sub

    Protected Sub dlStreams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlStreams.ItemDataBound
        Dim lblStmId As Label
        Dim ltProcess As Literal
        lblStmId = e.Item.FindControl("lblStmId")
        ltProcess = e.Item.FindControl("ltProcess")
        GetHeaderKeys(ltProcess, lblStmId.Text)
    End Sub

    Protected Sub ddlOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOption.SelectedIndexChanged
        BindAllotedSubject()
    End Sub


    Protected Sub dlRStreams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlRStreams.ItemDataBound
        Dim lnkRStream As LinkButton
        Dim ltProcess As Literal
        lnkRStream = e.Item.FindControl("lnkRStream")
        ltProcess = e.Item.FindControl("ltRProcess")
        GetHeaderKeysRequested(ltProcess, lnkRStream.Text)
    End Sub


End Class

