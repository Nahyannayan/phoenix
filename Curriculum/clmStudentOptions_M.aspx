<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmStudentOptions_M.aspx.vb" Inherits="Curriculum_clmStudentOptions_M" Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to remove the selected students from this group.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
         }

         function test2(val) {
             var path;
             if (val == 'LI') {
                 path = '../Images/operations/like.gif';
             } else if (val == 'NLI') {
                 path = '../Images/operations/notlike.gif';
             } else if (val == 'SW') {
                 path = '../Images/operations/startswith.gif';
             } else if (val == 'NSW') {
                 path = '../Images/operations/notstartwith.gif';
             } else if (val == 'EW') {
                 path = '../Images/operations/endswith.gif';
             } else if (val == 'NEW') {
                 path = '../Images/operations/notendswith.gif';
             }
             document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
                   }




                   var color = '';
                   function highlight(obj) {
                       var rowObject = getParentRow(obj);
                       var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Student Option Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table id="tdOption" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                STUDENT OPTION ALLOCATION</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAccText" runat="server" CssClass="field-label" Text="Select Academic Year"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-label" Text="Select Grade"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                </tr>

                                <tr>

                                    <td align="left" width="20%">
                                        <asp:Label ID="lblStream" runat="server" CssClass="field-label" Text="Stream"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStream" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Select Section</span></td>

                                    <td>
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center" valign="bottom" colspan="4">

                                        <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                            Text="List" TabIndex="9" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DataList ID="dlOptions" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" ShowHeader="False">
                                            <ItemTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("SGO_OPT_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR") %>' CssClass="field-label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="optPanel" EnableViewState="true" runat="server" BorderStyle="None">
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </ItemTemplate>

                                        </asp:DataList>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="4" align="center">
                                        <table id="Table1" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                                        PageSize="40" Width="100%">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Available">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" />

                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    Select 
                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                        ToolTip="Click here to select/deselect all rows" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />

                                                                </ItemTemplate>

                                                                <HeaderStyle Wrap="False"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SL.No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo() %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stud No.">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblh1" runat="server" Text="Stud. No"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Student Name">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStudName" runat="server" Style="min-width: 50% !important"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    Section         
                                                                    <br />
                                                                    <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Alloted Subjects">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblsubject" runat="server" Text='<%# Bind("SUBJECTS") %>'></asp:Label>

                                                                </ItemTemplate>

                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>





                            </table>




                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTM_ID" runat="server"></asp:HiddenField>
                        </td>

                    </tr>

                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />


            </div>
        </div>
    </div>

</asp:Content>

