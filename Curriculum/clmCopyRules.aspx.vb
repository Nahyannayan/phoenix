﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class Curriculum_clmCopyRules
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYearFrom = studClass.PopulateAcademicYear(ddlAcademicYearFrom, Session("clm"), Session("sbsuid"))
                    ddlAcademicYearTo = studClass.PopulateAcademicYear(ddlAcademicYearTo, Session("clm"), Session("sbsuid"))
                    BindTerm(ddlTermFrom, ddlAcademicYearFrom.SelectedValue.ToString)
                    BindTerm(ddlTermTo, ddlAcademicYearTo.SelectedValue.ToString)
                    BindReportCard(ddlReportCardFrom, ddlAcademicYearFrom.SelectedValue.ToString)
                    BindReportCard(ddlReportCardTo, ddlAcademicYearTo.SelectedValue.ToString)
                    BindReportSchedule(ddlReportScheduleFrom, ddlReportCardFrom.SelectedValue.ToString)
                    BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
                    BindGrade()
                    BindAssessment()
                    BindHeader()
                    gvHeader.Attributes.Add("bordercolor", "#1b80b6")
                    gvAssessment.Attributes.Add("bordercolor", "#1b80b6")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindTerm(ByVal ddlTerm As DropDownList, ByVal acd_id As String)
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + acd_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()

        Dim li As New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"

        ddlTerm.Items.Add(li)
    End Sub

    Sub BindReportCard(ByVal ddlReportCard As DropDownList, ByVal acd_id As String)
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + acd_id _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindReportSchedule(ByVal ddlReportSchedule As DropDownList, ByVal rsm_id As String)
        ddlReportSchedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + rsm_id _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub

    Sub BindGrade()
        'lstGrade.Items.Clear()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYearTo.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCardTo.SelectedValue.ToString _
                                & " AND RSG_GRD_ID NOT IN(SELECT RRM_GRD_ID FROM RPT.REPORT_RULE_M " _
                                & " WHERE RRM_RPF_ID='" + ddlReportScheduleTo.SelectedValue.ToString + "' AND RRM_GRD_ID=A.GRM_GRD_ID)" _
                                & " AND RSG_GRD_ID IN(SELECT RSG_GRD_ID FROM RPT.REPORTSETUP_GRADE_S " _
                                & " WHERE RSG_RSM_ID=" + ddlReportCardFrom.SelectedValue.ToString + ")" _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstGrade.DataSource = ds
        'lstGrade.DataTextField = "GRM_DISPLAY"
        'lstGrade.DataValueField = "GRM_GRD_ID"
        'lstGrade.DataBind()
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "GRM_DISPLAY"
        ddlPrintedFor.DataValueField = "GRM_GRD_ID"
        ddlPrintedFor.DataBind()

    End Sub

    Sub BindAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CAD_ID,CAD_CAM_ID,CAD_DESC FROM ACT.ACTIVITY_D AS A " _
                                & " INNER JOIN RPT.REPORT_SCHEDULE_D AS B ON A.CAD_ID=B.RRD_CAD_ID" _
                                & " INNER JOIN RPT.REPORT_RULE_M AS C ON B.RRD_RRM_ID=C.RRM_ID" _
                                & " WHERE RRM_RPF_ID=" + ddlReportScheduleFrom.SelectedValue.ToString


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvAssessment.DataSource = ds
        gvAssessment.DataBind()
    End Sub

    Sub BindMappedAssessments(ByVal ddlAssessment As DropDownList, ByVal cam_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT CAD_ID,CAD_DESC FROM ACT.ACTIVITY_D AS A " _
               & " WHERE CAD_ACD_ID=" + ddlAcademicYearTo.SelectedValue.ToString _
               & " ORDER BY CAD_DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAssessment.DataSource = ds
        ddlAssessment.DataTextField = "CAD_DESC"
        ddlAssessment.DataValueField = "CAD_ID"
        ddlAssessment.DataBind()

    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D " _
                               & " WHERE RSD_RSM_ID='" + ddlReportCardFrom.SelectedValue.ToString + "'" _
                               & " AND RSD_bDIRECTENTRY=0 AND ISNULL(RSD_bRULEREQD,0)=1"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvHeader.DataSource = ds
        gvHeader.DataBind()


        str_query = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D " _
                      & " WHERE RSD_RSM_ID='" + ddlReportCardTo.SelectedValue.ToString + "'" _
                      & " AND RSD_bDIRECTENTRY=0 AND ISNULL(RSD_bRULEREQD,0)=1"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim lblHeader As Label
        Dim ddlHeader As DropDownList
        Dim li As ListItem

        For i = 0 To gvHeader.Rows.Count - 1
            lblHeader = gvHeader.Rows(i).FindControl("lblHeader")
            ddlHeader = gvHeader.Rows(i).FindControl("ddlHeader")
            ddlHeader.DataSource = ds
            ddlHeader.DataTextField = "RSD_HEADER"
            ddlHeader.DataValueField = "RSD_ID"
            ddlHeader.DataBind()
            If Not ddlHeader.Items.FindByText(lblHeader.Text) Is Nothing Then
                ddlHeader.Items.FindByText(lblHeader.Text).Selected = True
            Else
                li = New ListItem
                li.Text = "--"
                li.Value = "0"
                ddlHeader.Items.Insert(0, li)
            End If
        Next




    End Sub

    Function getGrades() As String
        'Dim str As String = ""
        'Dim i As Integer

        'For i = 0 To ddlPrintedFor.Items.Count - 1
        '    If ddlPrintedFor.Items(i).Selected = True Then
        '        If str <> "" Then
        '            str += "|"
        '        End If
        '        str += ddlPrintedFor.Items(i).Value
        '    End If
        'Next
        'Return str


        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Text
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str
    End Function

    Protected Sub ddlPrintedFor_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        Dim grades As String = ddlPrintedFor.SelectedItem.Text
        Dim Reportcard As String = "'" + GetSelectedReport().Replace("|", "','") + "'"

    End Sub

    Function GetSelectedReport() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Text
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function
    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim transaction As SqlTransaction
        Dim i As Integer
        Dim lblCadId As Label
        Dim ddlAssessement As DropDownList
        Dim lblRsdId As Label
        Dim ddlHeader As DropDownList




        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_query = "exec COPY.COPYRULES " _
                            & "@ACD_ID=" + ddlAcademicYearFrom.SelectedValue.ToString + "," _
                            & "@ACD_NEWID=" + ddlAcademicYearTo.SelectedValue.ToString + "," _
                            & "@TRM_OLDID=" + ddlTermFrom.SelectedValue.ToString + "," _
                            & "@TRM_NEWID=" + ddlTermTo.SelectedValue.ToString.ToString + "," _
                            & "@RPF_ID=" + ddlReportScheduleFrom.SelectedValue.ToString + "," _
                            & "@RPF_NEWID=" + ddlReportScheduleTo.SelectedValue.ToString + "," _
                            & "@TEXT='" + txtFrom.Text + "'," _
                            & "@NEWTEXT='" + txtTo.Text + "'," _
                            & "@RSM_ID=" + ddlReportCardFrom.SelectedValue.ToString + "," _
                            & "@RSM_NEWID=" + ddlReportCardTo.SelectedValue.ToString + "," _
                            & "@GRD_IDS='" + getGrades() + "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                For i = 0 To gvAssessment.Rows.Count - 1
                    lblCadId = gvAssessment.Rows(i).FindControl("lblCadId")
                    ddlAssessement = gvAssessment.Rows(i).FindControl("ddlAssessment")

                    str_query = "exec COPY.COPYRULES_MAPASSESSMENT " _
                              & "@RPF_NEWID=" + ddlReportScheduleTo.SelectedValue.ToString + "," _
                              & "@CAD_ID=" + lblCadId.Text + "," _
                              & "@CAD_NEWID=" + ddlAssessement.SelectedValue.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next



                For i = 0 To gvHeader.Rows.Count - 1
                    lblRsdId = gvHeader.Rows(i).FindControl("lblRsdId")
                    ddlHeader = gvHeader.Rows(i).FindControl("ddlHeader")
                    str_query = "exec COPY.COPYRULES_MAPHEADERS " _
                               & "@RPF_NEWID=" + ddlReportScheduleTo.SelectedValue.ToString + "," _
                               & "@RSD_ID=" + lblRsdId.Text + "," _
                               & "@RSD_NEWID=" + ddlHeader.SelectedValue.ToString

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using




    End Sub

#End Region

    Protected Sub ddlAcademicYearFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYearFrom.SelectedIndexChanged
        BindTerm(ddlTermFrom, ddlAcademicYearFrom.SelectedValue.ToString)
        BindReportCard(ddlReportCardFrom, ddlAcademicYearFrom.SelectedValue.ToString)
        BindReportSchedule(ddlReportScheduleFrom, ddlReportCardFrom.SelectedValue.ToString)
        BindGrade()
        BindAssessment()
        BindHeader()
    End Sub


    Protected Sub ddlAcademicYearTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYearTo.SelectedIndexChanged
        BindTerm(ddlTermTo, ddlAcademicYearTo.SelectedValue.ToString)
        BindReportCard(ddlReportCardTo, ddlAcademicYearTo.SelectedValue.ToString)
        BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
        BindGrade()
        BindAssessment()
        BindHeader()
    End Sub

    Protected Sub ddlReportCardFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCardFrom.SelectedIndexChanged
        BindReportSchedule(ddlReportScheduleFrom, ddlReportCardFrom.SelectedValue.ToString)
        BindGrade()
        BindAssessment()
        BindHeader()
    End Sub

    Protected Sub ddlReportCardTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCardTo.SelectedIndexChanged
        BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
        BindGrade()
        BindHeader()
    End Sub

    Protected Sub ddlReportScheduleFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportScheduleFrom.SelectedIndexChanged
        BindAssessment()
    End Sub

    Protected Sub gvAssessment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAssessment.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlAssessment As DropDownList = e.Row.FindControl("ddlAssessment")
            Dim lblCamId As Label = e.Row.FindControl("lblCamId")
            BindMappedAssessments(ddlAssessment, lblCamId.Text)
        End If
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        SaveData()
    End Sub

    Protected Sub ddlReportScheduleTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportScheduleTo.SelectedIndexChanged
        BindGrade()
    End Sub


End Class
