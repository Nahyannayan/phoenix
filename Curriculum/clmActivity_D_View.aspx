<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmActivity_D_View.aspx.vb" Inherits="Students_studAuthorized_Leave_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"> </i> Assessment Detail
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="Table1" border="0" width="100%">

        <tr>
            <td align="left"  valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
    </table>
    <table align="center" width="100%">
        
        <tr>
            <td align="left" colspan="4" valign="top">
                <asp:LinkButton ID="lbAddNew" runat="server"   OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="left" colspan="4" valign="top">
                <table id="tbl_test" runat="server" width="100%">
                    <tr class="matters">
                        <td align="left" valign="middle" width="20%"><span class="field-label">Select Academic year</span> </td>
                        <td width="30%"><asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" CssClass="listbox" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                        </asp:DropDownList>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                        </tr>
                    <tr  >
                        <td align="left" class="matters" colspan="4" valign="top" style="text-align:center">
                            <asp:GridView ID="gvCAD_DRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="TERM">
                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStud_NameH" runat="server" Text="Term" CssClass="gridheader_text"  ></asp:Label>
                                                                        <br />
                                                                            <asp:TextBox ID="txtTerm_Desc" runat="server"  ></asp:TextBox>
                                                             
                                                                            <asp:ImageButton ID="btnSearchTerm_desc" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"  OnClick="btnSearchTerm_desc_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTRM_DESC" runat="server" Text='<%# Bind("TRM_DESC") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Allocated by">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                                            <asp:Label ID="lblGradeH" runat="server" Text="Allocated By" CssClass="gridheader_text" ></asp:Label><br />
                                                            <asp:TextBox ID="txtAllocated" runat="server" ></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchAllocatedBy" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"  ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="lblALLOCATED_BYH" runat="server" Text='<%# Bind("Allocated_BY") %>'  ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activity">
                                        <HeaderTemplate>
                                                            <asp:Label ID="lblCAM_DESCH" runat="server" Text="Assessment" CssClass="gridheader_text"></asp:Label><br />
   
                                                                            <asp:TextBox ID="txtCAM_DESC" runat="server" ></asp:TextBox>
                                                                        
                                                                            <asp:ImageButton ID="btnSearchActivity" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAM_DESC" runat="server" Text='<%# BIND("CAM_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                                            <asp:Label ID="lblToDateH" runat="server" Text="Description" CssClass="gridheader_text"></asp:Label><br />
                                                            
                                                                            <asp:TextBox ID="txtCAD_DESC" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchCAD_DESC" OnClick="btnSearchToDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAD_DESCH" runat="server" Text='<%# Bind("CAD_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CAD_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAD_ID" runat="server" Text='<%# Bind("CAD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            &nbsp;<br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

            </div>
        </div>
    </div>

    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

</asp:Content>

