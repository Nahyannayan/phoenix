<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_View.aspx.vb" Inherits="Curriculum_clmSyllabus_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Syllabus Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="8">
                            <table id="Table1" runat="server" width="100%">
                                <tr>
                                    <td colspan="4" align="center">
                                        <table id="Table2" runat="server" width="100%">
                                            <tr>
                                                <td align="left" colspan="4">
                                                    <asp:LinkButton ID="lbAddNew" runat="server">Add New</asp:LinkButton></td>
                                            </tr>
                                            <tr align="left" width="20%">
                                                <td><span class="field-label">Academic Year</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                            
                                                <td align="left" width="20%"><span class="field-label">Term</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Grade</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                            
                                                <td align="left"><span class="field-label">Subject</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><br /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center" style="text-align: center">
                                                    <asp:GridView ID="gvclmSyllabus" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
                                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." CssClass="table table-bordered table-row"
                                                        PageSize="20" BorderStyle="None">
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="False" HeaderText="syl_id">
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("SYM_ID") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="False" HeaderText="syl_id">
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Subject">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblSub" runat="server" Text="Subject" CssClass="gridheader_text" ></asp:Label><br />

                                                                    <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSubject_Search" OnClick="btnSubject_Search_Click" runat="server"
                                                                        ImageUrl="~/Images/forum_search.gif" ImageAlign="middle"></asp:ImageButton>


                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Syllabus">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblsh" runat="server" Text="Syllabus" CssClass="gridheader_text" __designer:wfdid="w8"></asp:Label><br />

                                                                    <asp:TextBox ID="txtSyllabus" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSyllabus_Search" OnClick="btnSyllabus_Search_Click" runat="server"
                                                                        ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" ></asp:ImageButton>



                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                <HeaderStyle></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSyllabus" runat="server"  Text='<%# Bind("SYM_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterStyle></FooterStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblView" runat="server" OnClick="lblView_Click">View</asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Set Topics">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblSetTopics" runat="server" OnClick="lblTopics_Click">Set Topics</asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <EditRowStyle  />
                                                        <SelectedRowStyle  />
                                                        <HeaderStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>
