Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml
Imports System.Drawing

Partial Class Curriculum_clmMarkEntry_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    lblActivity.Text = Encr_decrData.Decrypt(Request.QueryString("activity").Replace(" ", "+"))
                    lblSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    lblGroup.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    lblDate.Text = Encr_decrData.Decrypt(Request.QueryString("date").Replace(" ", "+"))
                    hfCAS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("casid").Replace(" ", "+"))
                    hfMEntered.Value = Encr_decrData.Decrypt(Request.QueryString("menterd").Replace(" ", "+"))
                    hfEntryType.Value = Encr_decrData.Decrypt(Request.QueryString("entry").Replace(" ", "+"))
                    hfGradeSlab.Value = Encr_decrData.Decrypt(Request.QueryString("gradeslab").Replace(" ", "+"))
                    hfMinMarks.Value = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfMaxMarks.Value = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMaxmarks.Text = Encr_decrData.Decrypt(Request.QueryString("maxmarks").Replace(" ", "+")).Replace(".00", "")
                    lblMinmarks.Text = Encr_decrData.Decrypt(Request.QueryString("minmarks").Replace(" ", "+")).Replace(".00", "")
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Replace(".00", "")
                    hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgrid").Replace(" ", "+")).Replace(".00", "")
                    hfSBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+")).Replace(".00", "")
                    If lblActivity.Text.ToUpper.Contains("PATH FINDER") Or lblActivity.Text.ToUpper.Contains("PATHFINDER") Then
                        GetPathFinderBlock()
                    Else
                        hfPathFinderBlock.Value = "0"
                    End If

                    GridBind()
                    tblact.Rows(3).Visible = False
                    tblact.Rows(4).Visible = False
                    gvStud.Attributes.Add("bordercolor", "#deffb4") '#1b80b6
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"

    Sub GetPathFinderBlock()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query = "SELECT COUNT(PTB_ID) FROM PATHFINDER.BLOCKDATAENTRY WHERE PTB_ACD_ID=" + Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            hfPathFinderBlock.Value = "0"
        Else
            hfPathFinderBlock.Value = "1"
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvStud.PageSize * gvStud.PageIndex)
    End Function
    Sub GridBind()
        ViewState("slno") = 0
        Dim li As New ListItem
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STA_ID,STU_ID,STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                 & " +' '+ISNULL(STU_LASTNAME,'')),STU_NO,CASE  WHEN STA_MARK IS NULL THEN '' " _
                                 & " ELSE REPLACE(CAST(CONVERT(NUMERIC(6,2),STA_MARK) AS VARCHAR),'.00','') END AS MARKS,STA_GRADE," _
                                 & " MAXMARKS=" + hfMaxMarks.Value + ",MINMARKS=" + hfMinMarks.Value _
                                 & " ,ERR='Marks Should be between 0 and " + hfMaxMarks.Value + "'" _
                                 & " ,CASE  WHEN STA_bATTENDED='P' THEN 'TRUE' WHEN STA_bATTENDED IS NULL THEN 'TRUE' ELSE 'FALSE' END AS ENABLE " _
                                 & ", CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE '('+STU_CURRSTATUS+')' END AS STU_STATUS" _
                                 & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B" _
                                 & " ON A.STU_ID=B.STA_STU_ID" _
                                 & " WHERE STA_CAS_ID=" + hfCAS_ID.Value _
                                 & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
        Dim lblGrade As Label
        ViewState("slno") = 0

        If hfEntryType.Value.ToLower = "grade" Then
            str_query = "SELECT GSD_DESC FROM ACT.GRADING_SLAB_D WHERE GSD_GSM_SLAB_ID=" + hfGradeSlab.Value _
                      & " ORDER BY GSD_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer
            Dim ddlGrade As DropDownList
            For i = 0 To gvStud.Rows.Count - 1
                ddlGrade = gvStud.Rows(i).FindControl("ddlGrade")
                ddlGrade.DataSource = ds
                ddlGrade.DataTextField = "GSD_DESC"
                ddlGrade.DataValueField = "GSD_DESC"
                ddlGrade.DataBind()
                li = New ListItem
                li.Text = "--"
                li.Value = "--"
                ddlGrade.Items.Insert(0, li)

                lblGrade = gvStud.Rows(i).FindControl("lblGrade")
                If lblGrade.Text.Trim <> "" Then
                    If Not ddlGrade.Items.FindByValue(lblGrade.Text) Is Nothing Then
                        ddlGrade.Items.FindByValue(lblGrade.Text).Selected = True
                    End If
                End If
            Next
            gvStud.Columns(5).Visible = False
            gvStud.Columns(6).Visible = False


        Else
            gvStud.Columns(7).Visible = False
        End If
    End Sub


    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim lblStaId As Label
        Dim txtMarks As TextBox
        Dim grade As String
        Dim ddlGrade As DropDownList
        Dim cmd As SqlCommand
        Dim AuditID As Int32

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                cmd = New SqlCommand("[ACT].[SaveAUDITDATA]", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 50)
                sqlpBSU_ID.Value = Session("SBsuid")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDOCTYPE As New SqlParameter("@AUD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpDOCNO As New SqlParameter("@AUD_DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpPROCEDURE As New SqlParameter("@AUD_PROCEDURE", SqlDbType.VarChar, 200)
                sqlpPROCEDURE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpPROCEDURE)

                Dim sqlpLOGDT As New SqlParameter("@AUD_LOGDT", SqlDbType.DateTime)
                sqlpLOGDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpLOGDT)

                Dim sqlpACTION As New SqlParameter("@AUD_ACTION", SqlDbType.VarChar, 50)
                sqlpACTION.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpACTION)

                Dim sqlpDT As New SqlParameter("@AUD_DT", SqlDbType.DateTime)
                sqlpDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpDT)

                Dim sqlpUSER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
                sqlpUSER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpUSER)

                Dim sqlpREMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 200)
                sqlpREMARKS.Value = "Marks"
                cmd.Parameters.Add(sqlpREMARKS)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                AuditID = retValParam.Value
                If AuditID <> 0 Then
                    For i = 0 To gvStud.Rows.Count - 1

                        lblStaId = gvStud.Rows(i).FindControl("lblStaId")
                        txtMarks = gvStud.Rows(i).FindControl("txtMarks")
                        ddlGrade = gvStud.Rows(i).FindControl("ddlGrade")
                        If hfEntryType.Value.ToLower = "grade" Then
                            grade = ddlGrade.SelectedValue
                            txtMarks.Text = "0"
                        Else
                            grade = ""
                        End If
                        If txtMarks.Text <> "" Or (grade <> "--" And grade <> "") Then
                            str_query = "ACT.saveMARKENTRY " _
                                       & lblStaId.Text + "," _
                                       & IIf(txtMarks.Enabled = False, "0", txtMarks.Text.ToString) + "," _
                                       & hfGradeSlab.Value + "," _
                                       & hfMinMarks.Value + "," _
                                       & hfMaxMarks.Value + "," _
                                       & "'" + grade + "'," _
                                       & "'" + hfEntryType.Value + "', " _
                                       & "" + CStr(AuditID) + ""
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                            lblError.Text = "Record saved successfully"
                        End If
                    Next
                End If
                str_query = "ACT.updateBMARKENTERD " + hfCAS_ID.Value
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub SaveFromExcel(ByVal dt As DataSet)
        Dim i As Integer
        Dim lblStaId As Label
        Dim txtMarks As TextBox
        Dim dr As DataRow
        Try
            For i = 0 To gvStud.Rows.Count - 1
                lblStaId = gvStud.Rows(i).FindControl("lblStaId")
                txtMarks = gvStud.Rows(i).FindControl("txtMarks")
                If (txtMarks IsNot Nothing) AndAlso (lblStaId IsNot Nothing) Then
                    For Each dr In dt.Tables("Data").Rows
                        If lblStaId.Text = dr.Item(2) Then
                            txtMarks.Text = dr.Item(4).ToString
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Function checkPublishActiviyEnable(ByVal stu_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
                                 & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS B ON A.STA_CAS_ID=B.CAS_ID" _
                                 & " INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON B.CAS_CAD_ID=C.RRD_CAD_ID" _
                                 & " INNER JOIN RPT.REPORT_RULE_M AS E ON C.RRD_RRM_ID=E.RRM_ID" _
                                 & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS F ON A.STA_STU_ID=F.RPP_STU_ID " _
                                 & " AND F.RPP_RPF_ID=E.RRM_RPF_ID AND F.RPP_TRM_ID=E.RRM_TRM_ID" _
                                 & " AND F.RPP_ACD_ID=E.RRM_ACD_ID AND F.RPP_GRD_ID=E.RRM_GRD_ID" _
                                 & " WHERE RPP_bPUBLISH='TRUE' AND RPP_STU_ID=" + stu_id _
                                 & " AND STA_CAS_ID=" + hfCAS_ID.Value
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strFileNamePath As String = ""
        If Not (FileUpload1.PostedFile Is Nothing) Then
            strFileNamePath = FileUpload1.PostedFile.FileName
            Dim myDS As DataSet = New DataSet
            Dim settings As New XmlReaderSettings()
            settings.ConformanceLevel = ConformanceLevel.Fragment
            settings.IgnoreWhitespace = True
            settings.IgnoreComments = True
            settings.ValidationType = ValidationType.None
            Dim reader As XmlReader = XmlReader.Create(strFileNamePath, settings)
            myDS.ReadXml(reader)
            myDS = New DataSet
            myDS.ReadXml(strFileNamePath, XmlReadMode.IgnoreSchema)
        End If
        Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        If Not Directory.Exists(str_img & "\temp") Then
            Directory.CreateDirectory(str_img & "\temp")
        End If
        Dim strFilepath As String = str_img & "\temp\"
        Dim intFileNameLength As Integer
        '  Dim strFileNamePath As String = ""
        Dim strFileNameOnly As String = ""
        Try
            If Not (FileUpload1.PostedFile Is Nothing) Then
                strFileNamePath = FileUpload1.PostedFile.FileName

                intFileNameLength = InStr(1, StrReverse(strFileNamePath), "\")

                strFileNameOnly = Mid(strFileNamePath, (Len(strFileNamePath) - intFileNameLength) + 2)


                'If File.Exists(paths & strFileNameOnly) Then
                'lblMessage.Text = "Image of Similar name already Exist,Choose other name"
                'Else
                If FileUpload1.PostedFile.ContentLength > 3340000 Then
                    lblError.Text = "The Size of file is greater than 4 MB"
                ElseIf strFileNameOnly = "" Then
                    Exit Sub
                Else
                    strFileNameOnly = Session("sUsr_name") & "-" & Session("sBsuid") & "-" & Format(Date.Today, "dd/MMM/yyyy").Replace("/", "-") & "-" & AccountFunctions.GetRandomString() & ".xls"
                    FileUpload1.PostedFile.SaveAs(strFilepath & strFileNameOnly)
                    lblError.Text = "File Upload Success."
                    Session("Img") = strFileNameOnly
                End If
            End If

            Dim excelDataset As New DataSet()

            ' Dim fs As New FileStream("c:\1234.xml", FileMode.Open)
            excelDataset.ReadXml(strFilepath & strFileNameOnly)

            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
            "Data Source=" & strFilepath & strFileNameOnly & ";" & _
            "Extended Properties=Excel 8.0;"

            'You must use the $ after the object you reference in the spreadsheet
            Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(strFilepath & strFileNameOnly) & "]", strConn)
            myData.TableMappings.Add("Table", "ExcelTest")
            'myData.Fill(excelDataset)
            SaveFromExcel(excelDataset)
        Catch
            lblError.Text = "Error in File or this format is not supported for exporting"
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False

    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STA_ID,STU_ID,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+ " _
                                 & " ISNULL(STU_LASTNAME,'') AS STU_NAME,STU_NO,CASE  WHEN STA_MARK IS NULL THEN '' " _
                                 & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),STA_MARK) AS VARCHAR),'.00',''),'.0','') END AS MARKS,STA_GRADE" _
                                 & " FROM OASIS..STUDENT_M AS A INNER JOIN ACT.STUDENT_ACTIVITY AS B" _
                                 & " ON A.STU_ID=B.STA_STU_ID" _
                                 & " WHERE STA_CAS_ID=" + hfCAS_ID.Value _
                                 & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        If Not Directory.Exists(str_img & "\temp") Then
            Directory.CreateDirectory(str_img & "\temp")
        End If
        Dim strFilepath As String = str_img & "\temp\test.xls"
        'exp.ExportToExcel(ds.Tables(0).DefaultView, strFilepath, "Sheet1")
        DataSetToExcel.Convert(ds, Response, Server.MapPath("../Reports/ASPX Report/ExcelFormat.xsl"), strFilepath)
        'DataSetToExcel.WriteToExcelSpreadsheet(strFilepath, ds.Tables(0))
        Convert123(ds, Response, "")
    End Sub

    Sub Convert123(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal strFName As String)
        Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & "test.xsl" & ";" & _
        "Extended Properties=Excel 8.0;"

        ' Dim factory As Common.DbProviderFactory = Common.DbProviderFactories.GetFactory("System.Data.OleDb")
        'Using connection As Common.DbConnection = factory.CreateConnection()
        '    connection.ConnectionString = strConn
        '    Using command As Common.DbCommand = connection.CreateCommand()
        '        command.CommandText = "INSERT INTO [Cities$] (ID, City, State) VALUES(4,""Tampa"",""Florida"")"
        '        connection.Open()
        '        command.ExecuteNonQuery()
        '    End Using
        'End Using
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        Try
            SaveData()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnComment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnComment.Click
        Try
            '    Response.Redirect("~/Curriculum/rptStudentsMarks.aspx?MainMnu_code=PzZyO9/cOv0=&datamode=Zo4HhpVNpXc=")
            Response.Redirect("~/Curriculum/clmReportWriting_M.aspx?MainMnu_code=ep+0EdaUErc=&datamode=Zo4HhpVNpXc=" _
            & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
            & "&grdid=" + Encr_decrData.Encrypt(hfGRD_ID.Value) _
            & "&grade=" + Encr_decrData.Encrypt(hfGRD_ID.Value) _
            & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
            & "&grpid=" + Encr_decrData.Encrypt(hfSGR_ID.Value) _
            & "&sbgid=" + Encr_decrData.Encrypt(hfSBG_ID.Value))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnComment1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnComment1.Click
        Try
            '   Response.Redirect("~/Curriculum/rptStudentsMarks.aspx?MainMnu_code=PzZyO9/cOv0=&datamode=Zo4HhpVNpXc=")
            Response.Redirect("~/Curriculum/clmReportWriting_M.aspx?MainMnu_code=ep+0EdaUErc=&datamode=Zo4HhpVNpXc=" _
        & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
        & "&grdid=" + Encr_decrData.Encrypt(hfGRD_ID.Value) _
        & "&grade=" + Encr_decrData.Encrypt(hfGRD_ID.Value) _
        & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
        & "&grpid=" + Encr_decrData.Encrypt(hfSGR_ID.Value) _
        & "&sbgid=" + Encr_decrData.Encrypt(hfSBG_ID.Value))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblStuId As Label
                Dim txtMarks As TextBox
                Dim ddlGrade As DropDownList
                Dim lblStatus As Label
                lblStuId = e.Row.FindControl("lblStuId")
                txtMarks = e.Row.FindControl("txtMarks")
                ddlGrade = e.Row.FindControl("ddlGrade")
                lblStatus = e.Row.FindControl("lblStatus")

                If lblStatus.Text <> "" Then
                    lblStatus.ForeColor = Drawing.Color.Red
                End If

                ' if the report is published the record should be disabled
                Dim bt As Boolean

                If hfPathFinderBlock.Value = "1" Then
                    bt = False
                Else
                    bt = checkPublishActiviyEnable(lblStuId.Text)
                End If



                'if not absent
                If txtMarks.Enabled = True Then
                    txtMarks.Enabled = bt
                End If

                If ddlGrade.Enabled = True Then
                    ddlGrade.Enabled = bt
                End If

                If bt = False Then
                    e.Row.BackColor = Color.FromName("#DEFFB4") 'Drawing.Color.Coral
                End If


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
End Class
