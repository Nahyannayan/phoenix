<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportRule_M.aspx.vb" Inherits="Curriculum_clmReportRule_M" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var AcdId
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            AcdId = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;

            if (GRD_IDs == '') {
                alert('Please select Grade')
                return false;
            }
            var oWnd = radopen("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + AcdId, "RadWindow1");
            //result = window.showModalDialog("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs+"&ACD_ID="+AcdId ,"", sFeatures)            

        }

        function GetSUBJECT() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var AcdId
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
    AcdId = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;

    if (GRD_IDs == '') {
        alert('Please select Grade')
        return false;
    }
    result = window.showModalDialog("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + AcdId, "", sFeatures)
    if (result != '' && result != undefined) {
        if (document.getElementById('<%=h_SBG_IDs.ClientID %>').value == "") {
             document.getElementById('<%=h_SBG_IDs.ClientID %>').value = result;
         }
         else {
             document.getElementById('<%=h_SBG_IDs.ClientID %>').value += "___" + result;
         }
         //NameandCode[0];
     }
     else {
         return false;
     }
 }

 function OnClientClose(oWnd, args) {
     //get the transferred arguments
     var arg = args.get_argument();
     if (arg) {

         NameandCode = arg.NameCode.split('||');
         if (document.getElementById('<%=h_SBG_IDs.ClientID %>').value == "") {
                          document.getElementById('<%=h_SBG_IDs.ClientID %>').value = NameandCode[0];
                          document.getElementById('<%=txtSubject.ClientID %>').value = NameandCode[0];

                      }
                      else {
                          document.getElementById('<%=h_SBG_IDs.ClientID %>').value += "___" + NameandCode[0];
                          document.getElementById('<%=txtSubject.ClientID%>').value += "___" + NameandCode[0];
                      }

                      __doPostBack('<%= txtSubject.ClientID%>', 'TextChanged');
        }
    }


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;
        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Rule Setup Master"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td>
                            <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0" width="100%"
                                cellspacing="0">
                                <tr>
                                    <td align="left" valign="bottom" colspan="4">
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                            ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" valign="top">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left"  valign="middle">
                                                    <asp:LinkButton ID="lnkAddNew" runat="server">Add New Rule</asp:LinkButton>
                                                    <asp:Button ID="btnCheck" Style="display: none;" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Academic Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left">
                                        <span class="field-label">Select Report Card</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Report Schedule</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server">
                                        </asp:DropDownList></td>

                                    <td align="left">
                                        <span class="field-label">Select Header</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlHeader" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">

                                        <span class="field-label">Select Grade</span>
                                    </td>

                                    <td align="left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstGrade" runat="server" AutoPostBack="True" RepeatLayout="Flow" Style="overflow: auto">
                                            </asp:CheckBoxList>
                                        </div>
                                        <asp:Label ID="lblGrade" runat="server" Text="Label" class="field-value"></asp:Label></td>

                                    <td align="left">
                                        <span class="field-label">Select Term</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Grading Slab</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGradingSlab" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left">
                                        <span class="field-label">Exam Level</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                                            <asp:ListItem>Normal</asp:ListItem>
                                            <asp:ListItem>Core</asp:ListItem>
                                            <asp:ListItem>Extended</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Description</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDescr" runat="server">
                                        </asp:TextBox></td>

                                    <td align="left">
                                        <span class="field-label">Min Pass Marks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPassMark" runat="server">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Max Marks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMaxMarks" runat="server"></asp:TextBox></td>

                                    <td align="left">
                                        <span class="field-label">Grade Weightage</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtGradeWt" runat="server">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Subject</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSubject" runat="server" Enabled="False" MaxLength="100" TabIndex="2" OnTextChanged="txtSubject_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgSub" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="openWin();return false;" TabIndex="18"></asp:ImageButton></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No records selected."
                                            PageSize="20" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />


                                            <Columns>


                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" Text='<%# Bind("SBG_ID") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" Text='<%# Bind("SBG_DESCR") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" Text='<%# Bind("GRM_DISPLAY") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="5%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkRemove" OnClick="lnkRemove_Click" runat="server">Remove</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" Width="10%"></ItemStyle>
                                                </asp:TemplateField>

                                            </Columns>


                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table id="tbAct" runat="server" width="100%">
                                <tr>
                                    
                                    <td align="left" runat="server" id="tdAs"><span class="field-label">Select Assesment Type</span></td>

                                    <td align="left" runat="server" id="tdAc">
                                        <asp:DropDownList ID="ddlActivity" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left"><span class="field-label">Select Operation</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlOperation" runat="server" AutoPostBack="True">
                                            <asp:ListItem>SUM</asp:ListItem>
                                            <asp:ListItem>AVG</asp:ListItem>
                                            <asp:ListItem Value="MAX">BEST OF</asp:ListItem>
                                            <asp:ListItem Value="WT">WEIGHTAGE</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"> <br /> </td>
                                </tr>
                                <tr>
                                    <td colspan="4">

                                        <asp:GridView ID="gvOperations" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Activities"
                                            HeaderStyle-Height="30" PageSize="20" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />


                                            <Columns>

                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCadId" Text='<%# Bind("CAD_ID") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Assesment">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" Text='<%# Bind("CAD_DESC") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Weightage">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWT" Text='<%# Bind("WTG") %>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>


                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply" ValidationGroup="groupM1" TabIndex="7" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvRule" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Rules"
                                            HeaderStyle-Height="30" PageSize="20" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />

                                            <Columns>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCamId" Text='<%# Bind("CAM_ID") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCadIds" Text='<%# Bind("IDS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt" Text='<%# Bind("OPT") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Operation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRule" Text='<%# Bind("RULES") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Weightage">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWT" Text='<%# Bind("WEIGHT") %>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblWeight" Text='<%# Bind("WTS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton ID="lnkClear" Text="Clear" runat="server" OnClick="lnkClear_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="h_SBG_IDs" runat="server" />
                            <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                            <asp:HiddenField ID="hfRRM_ID" runat="server" />
                            <asp:HiddenField ID="hfSBG_ID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfRSM_ID" runat="server" />
                            <asp:HiddenField ID="hfGSM_TOTmark" runat="server" />
                            <asp:HiddenField
                                ID="hfbFinalReport" runat="server"></asp:HiddenField>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

