Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Partial Class Curriculum_clmReportProcess_GradeSection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    '                    ddlAcademicYear.Enabled = False


                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                   

                    BindTerm()
                    BindReportCard()
                    BindPrintedFor()
                    BindHeader()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindSection()
                    BindBFinalReport()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
            ddlAcademicYear.Enabled = False
        End If
    End Sub
#Region "Private methods"
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID" _
                              & " FROM GRADE_BSU_M AS A INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                              & " INNER JOIN STREAM_M AS C ON A.GRM_STM_ID=C.STM_ID " _
                              & " INNER JOIN OASIS_CURRICULUM.RPT.REPORTSETUP_GRADE_S AS D ON A.GRM_GRD_ID=D.RSG_GRD_ID" _
                              & " WHERE grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString



        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + acdid + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If

        str_query += " ORDER BY grd_displayorder "



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = ""
        ddlTerm.Items.Insert(0, li)
        li = New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSM_ID IN(SELECT RSG_RSM_ID FROM RPT.REPORTSETUP_GRADE_S WHERE RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))))"
        End If

        ' str_query += " AND RSM_DESCR<>'BROWN BOOK KG1-KG2'"

        str_query += "ORDER BY RSM_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindHeader()
        lstHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND RSD_bDIRECTENTRY='FALSE' AND ISNULL(RSD_bFINALREPORT,'FALSE')='FALSE'  ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub

    Sub BindSection()
        Dim li As New ListItem
        lstSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSection.DataSource = ds
        lstSection.DataTextField = "SCT_DESCR"
        lstSection.DataValueField = "SCT_ID"
        lstSection.DataBind()

    End Sub

    Sub BindProcessData()

        Dim sct_ids As String
        Dim i As Integer
        For i = 0 To lstSection.Items.Count - 1
            If sct_ids <> "" Then
                sct_ids += ","
            End If
            sct_ids += lstSection.Items(i).Value
        Next


        Dim noteString As String = ""
        Dim headerString As String = ""
        Dim section As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT DISTINCT RSD_HEADER,SCT_DESCR,RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D AS A " _
                  & "  INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.RSD_ID=B.RST_RSD_ID " _
                  & "  INNER JOIN OASIS..SECTION_M AS C ON B.RST_SCT_ID=C.SCT_ID" _
                  & "   WHERE RST_RPF_ID =" + ddlPrintedFor.SelectedValue.ToString + " And RST_SCT_ID IN(" + sct_ids + ")" _
                  & " AND RSD_bDIRECTENTRY='FALSE'  AND ISNULL(RSD_bFINALREPORT,'FALSE')='FALSE' ORDER BY SCT_DESCR,RSD_DISPLAYORDER "

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read

            If section <> reader.GetString(1) And section <> "" Then
                noteString += "For section " + section + " headers " + headerString + " are already processed"
                headerString = ""
            End If

            If section <> reader.GetString(1) Then
                section = reader.GetString(1)
                If noteString <> "" Then
                    noteString += "<BR/>"
                End If
            End If

            If headerString <> "" Then
                headerString += ","
            End If
            headerString += reader.GetString(0)
        End While
        reader.Close()
        If section <> "" Then
            noteString += "For section " + section + " headers " + headerString + " are already processed"
        End If
        If noteString <> "" Then
            lblNote.Text = "Note : " + vbCrLf + noteString
        Else
            lblNote.Text = ""
        End If


    End Sub


    Sub SaveData(ByVal strHeaders As String, ByVal strSections As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String

        If Session("sbsuid") = "125002" Then
            hfbFinalReport.Value = "false"
        End If


        If grade(0) = "KG1" Or grade(0) = "KG2" Then
            If ddlReportCard.SelectedItem.Text.ToUpper.Contains("INTERIM") And Session("sbsuid") = "123016" Then
                str_query = "exec [RPT].[NMS_PROCESS_KG_OVERALLGRADE]  " _
                        & ddlAcademicYear.SelectedValue.ToString + "," _
                        & "'" + grade(0) + "'," _
                        & ddlPrintedFor.SelectedValue.ToString + "," _
                        & "'" + strHeaders + "'," _
                        & ddlReportCard.SelectedValue.ToString + "," _
                        & "'" + strSections + "'"
            Else
                str_query = "exec [RPT].[CBSE_REPORT_FINAL_PROCESS_KG1_KG2]  " _
                            & ddlAcademicYear.SelectedValue.ToString + "," _
                            & "'" + grade(0) + "'," _
                            & ddlPrintedFor.SelectedValue.ToString + "," _
                            & "'" + strHeaders + "'," _
                            & ddlTerm.SelectedValue.ToString + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & "'" + strSections + "'"
            End If
        ElseIf hfbAOLprocessing.Value = "true" Then
            str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION_AOL] " _
                                 & ddlAcademicYear.SelectedValue.ToString + "," _
                                 & "'" + grade(0) + "'," _
                                 & ddlPrintedFor.SelectedValue.ToString + "," _
                                 & "'" + strHeaders + "'," _
                                 & ddlTerm.SelectedValue.ToString + "," _
                                 & ddlReportCard.SelectedValue.ToString + "," _
                                 & "'" + strSections + "'"
        ElseIf hfbFinalReport.Value = "false" Then
            If Session("SBSUID") = "125010" Then
                str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION_TWS] " _
                                    & ddlAcademicYear.SelectedValue.ToString + "," _
                                    & "'" + grade(0) + "'," _
                                    & ddlPrintedFor.SelectedValue.ToString + "," _
                                    & "'" + strHeaders + "'," _
                                    & ddlTerm.SelectedValue.ToString + "," _
                                    & ddlReportCard.SelectedValue.ToString + "," _
                                    & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "123004" Then
                str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION_DMHS] " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & ddlTerm.SelectedValue.ToString + "," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "123016" And (ddlPrintedFor.SelectedItem.Text.ToUpper.Contains("INTERIM REPORT") Or ddlPrintedFor.SelectedItem.Text.ToUpper.Contains("ENTRY LEVEL")) Then
                str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION] " _
                                                         & ddlAcademicYear.SelectedValue.ToString + "," _
                                                         & "'" + grade(0) + "'," _
                                                         & ddlPrintedFor.SelectedValue.ToString + "," _
                                                         & "'" + strHeaders + "'," _
                                                         & ddlTerm.SelectedValue.ToString + "," _
                                                         & ddlReportCard.SelectedValue.ToString + "," _
                                                         & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "123016" Then
                str_query = "exec [RPT].[NMS_SUMMATIVEPROCESS] " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "450030" Then
                str_query = "exec [RPT].[CIK_REPORTPROCESS] " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & "'" + strSections + "'"
            Else
                If ddlPrintedFor.SelectedItem.Text.ToLower.Contains("summative") Then
                    str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION_SUMMATIVE] " _
                                    & ddlAcademicYear.SelectedValue.ToString + "," _
                                    & "'" + grade(0) + "'," _
                                    & ddlPrintedFor.SelectedValue.ToString + "," _
                                    & "'" + strHeaders + "'," _
                                    & ddlTerm.SelectedValue.ToString + "," _
                                    & ddlReportCard.SelectedValue.ToString + "," _
                                    & "'" + strSections + "'"
                Else
                    str_query = "exec [RPT].[SaveREPORT_STUDENT_PROCESS_GRADEANDSECTION] " _
                                           & ddlAcademicYear.SelectedValue.ToString + "," _
                                           & "'" + grade(0) + "'," _
                                           & ddlPrintedFor.SelectedValue.ToString + "," _
                                           & "'" + strHeaders + "'," _
                                           & ddlTerm.SelectedValue.ToString + "," _
                                           & ddlReportCard.SelectedValue.ToString + "," _
                                           & "'" + strSections + "'"
                End If
            End If
        Else
            If ViewState("REPORTTYPE").ToString().ToUpper.Contains("CBSE_FINAL_FORMAT") Then
                str_query = "exec [RPT].[CBSE_REPORT_FINAL_PROCESS_NEW]  " _
                           & ddlAcademicYear.SelectedValue.ToString + "," _
                           & "'" + grade(0) + "'," _
                           & ddlPrintedFor.SelectedValue.ToString + "," _
                           & "'" + strHeaders + "'," _
                           & ddlTerm.SelectedValue.ToString + "," _
                           & ddlReportCard.SelectedValue.ToString + "," _
                           & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "125010" Then
                str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION_TWS]  " _
                            & ddlAcademicYear.SelectedValue.ToString + "," _
                            & "'" + grade(0) + "'," _
                            & ddlPrintedFor.SelectedValue.ToString + "," _
                            & "'" + strHeaders + "'," _
                            & ddlTerm.SelectedValue.ToString + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & "'" + strSections + "'"
            ElseIf Session("sbsuid") = "123004" Then
                str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION_DMHS]  " _
                            & ddlAcademicYear.SelectedValue.ToString + "," _
                            & "'" + grade(0) + "'," _
                            & ddlPrintedFor.SelectedValue.ToString + "," _
                            & "'" + strHeaders + "'," _
                            & ddlTerm.SelectedValue.ToString + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "126008" Then
                str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION_AKNS]  " _
                            & ddlAcademicYear.SelectedValue.ToString + "," _
                            & "'" + grade(0) + "'," _
                            & ddlPrintedFor.SelectedValue.ToString + "," _
                            & "'" + strHeaders + "'," _
                            & ddlTerm.SelectedValue.ToString + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & "'" + strSections + "'"
            ElseIf Session("SBSUID") = "115002" Then
                Select Case grade(0)
                    Case "11", "12", "13"
                        str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION]  " _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & "'" + grade(0) + "'," _
                                & ddlPrintedFor.SelectedValue.ToString + "," _
                                & "'" + strHeaders + "'," _
                                & ddlTerm.SelectedValue.ToString + "," _
                                & ddlReportCard.SelectedValue.ToString + "," _
                                & "'" + strSections + "'"
                    Case Else
                        str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION_CHS]   " _
                                     & ddlAcademicYear.SelectedValue.ToString + "," _
                                     & "'" + grade(0) + "'," _
                                     & ddlPrintedFor.SelectedValue.ToString + "," _
                                     & "'" + strHeaders + "'," _
                                     & ddlTerm.SelectedValue.ToString + "," _
                                     & ddlReportCard.SelectedValue.ToString + "," _
                                     & "'" + strSections + "'"
                End Select
            Else

                Select Case Session("sbsuid")
                    Case "121012", "121021", "121013", "123006", "121014", "141001", "151001", "111001", "121009", "131001", "131002", "133006", "225007", "131003"
                        Select Case grade(0)
                            Case "01", "02", "03", "04"
                                If (Session("sbsuid") = "123006" Or Session("sbsuid") = "225007" Or Session("sbsuid") = "131003") And (grade(0) = "03" Or grade(0) = "04") Then
                                    str_query = "exec [RPT].[CBSE_REPORT_FINAL_SUMMATIVE_PROCESS]  " _
                                  & ddlAcademicYear.SelectedValue.ToString + "," _
                                  & "'" + grade(0) + "'," _
                                  & ddlPrintedFor.SelectedValue.ToString + "," _
                                  & "'" + strHeaders + "'," _
                                  & ddlTerm.SelectedValue.ToString + "," _
                                  & ddlReportCard.SelectedValue.ToString + "," _
                                  & "'" + strSections + "'"
                                Else
                                    str_query = "exec [RPT].[CBSE_REPORT_FINAL_PROCESS_01_04]  " _
                                    & ddlAcademicYear.SelectedValue.ToString + "," _
                                    & "'" + grade(0) + "'," _
                                    & ddlPrintedFor.SelectedValue.ToString + "," _
                                    & "'" + strHeaders + "'," _
                                    & ddlTerm.SelectedValue.ToString + "," _
                                    & ddlReportCard.SelectedValue.ToString + "," _
                                    & "'" + strSections + "'"
                                End If
                            Case "05", "06", "07", "08"
                                str_query = "exec [RPT].[CBSE_REPORT_FINAL_SUMMATIVE_PROCESS]  " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & ddlTerm.SelectedValue.ToString + "," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & "'" + strSections + "'"
                            Case "09", "10"
                                If Session("clm") = 1 Then
                                    str_query = "exec [RPT].[CBSE_REPORT_FINAL_SUMMATIVE_PROCESS]  " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & ddlTerm.SelectedValue.ToString + "," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & "'" + strSections + "'"
                                Else
                                    str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION]  " _
                                               & ddlAcademicYear.SelectedValue.ToString + "," _
                                               & "'" + grade(0) + "'," _
                                               & ddlPrintedFor.SelectedValue.ToString + "," _
                                               & "'" + strHeaders + "'," _
                                               & ddlTerm.SelectedValue.ToString + "," _
                                               & ddlReportCard.SelectedValue.ToString + "," _
                                               & "'" + strSections + "'"
                                End If
                            Case "11", "12"
                                str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION]  " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & ddlTerm.SelectedValue.ToString + "," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & "'" + strSections + "'"
                        End Select
                    Case "123016"
                        str_query = "exec [RPT].[NMS_REPORT_FINAL_PROCESS]  " _
                                   & ddlAcademicYear.SelectedValue.ToString + "," _
                                   & "'" + grade(0) + "'," _
                                   & ddlPrintedFor.SelectedValue.ToString + "," _
                                   & "'" + strHeaders + "'," _
                                   & ddlTerm.SelectedValue.ToString + "," _
                                   & ddlReportCard.SelectedValue.ToString + "," _
                                   & "'" + strSections + "'"

                    Case Else
                        str_query = "exec [RPT].[SaveREPORT_FINALREPORT_PROCESS_GRADEANDSECTION]  " _
                                    & ddlAcademicYear.SelectedValue.ToString + "," _
                                    & "'" + grade(0) + "'," _
                                    & ddlPrintedFor.SelectedValue.ToString + "," _
                                    & "'" + strHeaders + "'," _
                                    & ddlTerm.SelectedValue.ToString + "," _
                                    & ddlReportCard.SelectedValue.ToString + "," _
                                    & "'" + strSections + "'"
                End Select
            End If
        End If

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            lblError.Text = "Record Saved Successfully"

    End Sub

    Function checkRuleExits(ByVal strHeaders As String) As Boolean


        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " EXEC RPT.getRULENOTSETCOUNT " _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & ddlReportCard.SelectedValue.ToString + "," _
                                & ddlPrintedFor.SelectedValue.ToString + "," _
                                & "'" + strHeaders + "'," _
                                & "'ALL'," _
                                & "'" + grade(0) + "'," _
                                & "'" + grade(1) + "'"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Function getHeaders() As String
        Dim i As Integer
        Dim strHeaders As String = ""
        For i = 0 To lstHeader.Items.Count - 1
            If lstHeader.Items(i).Selected = True Then
                If strHeaders <> "" Then
                    strHeaders += "|"
                End If
                strHeaders += lstHeader.Items(i).Value
            End If
        Next
        Return strHeaders
    End Function


    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue.ToString)
        param.Add("@RPF_ID", ddlPrintedFor.SelectedValue.ToString)
        param.Add("@RSD_ID", getHeaders)
        param.Add("@SBG_ID", "ALL")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        param.Add("@GRD_ID", grade(0))
        param.Add("@STM_ID", grade(1))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@MODE", "NO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Curriculum/Reports/Rpt/rptReportRuleList.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Sub BindBFinalReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE'),ISNULL(RSM_bAOLPROCESSING,'FALSE') ,isnull(RSM_REPORTTYPE,'') FROM RPT.REPORT_SETUP_M " _
                                 & " WHERE RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfbFinalReport.Value = ds.Tables(0).Rows(0).Item(0).ToString.ToLower
        hfbAOLprocessing.Value = ds.Tables(0).Rows(0).Item(1).ToString.ToLower
        ViewState("REPORTTYPE") = ds.Tables(0).Rows(0).Item(2).ToString
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTerm()
        BindReportCard()
        BindPrintedFor()
        BindHeader()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()
        BindBFinalReport()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()
        BindBFinalReport()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click

        If ddlTerm.SelectedValue = "" Then
            lblError.Text = "Please select a Term"
            Exit Sub
        End If
        Dim i As Integer
        Dim strHeaders As String = ""
        For i = 0 To lstHeader.Items.Count - 1
            If lstHeader.Items(i).Selected = True Then
                If strHeaders <> "" Then
                    strHeaders += "|"
                End If
                strHeaders += lstHeader.Items(i).Value
            End If
        Next

        If strHeaders = "" Then
            lblError.Text = "Please select a header"
            Exit Sub
        End If

        Dim strSections As String = ""
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If strSections <> "" Then
                    strSections += "|"
                End If
                strSections += lstSection.Items(i).Value
            End If
        Next

        If strSections = "" Then
            lblError.Text = "Please select a section"
            Exit Sub
        End If

        'If checkRuleExits(strHeaders) = False Then
        '    Panel1.Visible = True
        '    btnProcess.Visible = False
        '    Exit Sub
        'End If
        SaveData(strHeaders, strsections)

    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Panel1.Visible = False
        CallReport()
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Panel1.Visible = False
        btnProcess.Visible = True
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        ' BindProcessData()
    End Sub
End Class
