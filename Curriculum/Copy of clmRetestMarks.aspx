<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Copy of clmRetestMarks.aspx.vb" Inherits="Curriculum_clmRetestMarks" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" >
                 
               <tr style="font-size: 12pt;">
              
                <td width="50%" align="left" class="title" style="height: 50px">
                    <asp:Label id="lblTitle" runat="server">RETEST MARKS</asp:Label>
                   </td>
              </tr>
             
           <tr>
            <td align="center"  class="matters" style="font-weight: bold;  height: 215px" valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table ID="tblTC" runat="server" align="center" border="1" Class="BlueTableView" bordercolor="#1b80b6" cellpadding="0" cellspacing="0">
                    
                  
                      <tr>
                        <td align="left" class="matters"  >
                            Select Academic Year</td>
                        <td class="matters"  >
                            :</td>
                        <td align="left" class="matters"  colspan="5">
                           <asp:DropDownList ID="ddlAcademicYear" SKINID="smallcmb" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>               </td>
                         
                    </tr>
                    
                    <tr>
                    
                       <td align="left" class="matters" >
                            Select Grade</td>
                        <td class="matters" style="width: 7px;" >
                            :</td>
                        <td align="left" class="matters" style="height: 10px; ">
                            <asp:DropDownList ID="ddlGrade" SKINID="smallcmb" runat="server" AutoPostBack="True" Width="149px">
                            </asp:DropDownList>               
                      </td>
                             
                           
                        <td align="left" class="matters"  >
                            Select Section</td>
                        <td class="matters" style="width: 6px;" >
                            :</td>
                        <td align="left" class="matters" style="height: 10px; " colspan="2">
                            <asp:DropDownList ID="ddlSection" SKINID="smallcmb" runat="server" Width="71px">
                            </asp:DropDownList>               
                      </td>
                    
                    
                    </tr>
                    <tr>
                        <td align="left" class="matters">
                            Student ID</td>
                        <td class="matters" style="width: 7px">
                            :</td>
                        <td align="left" class="matters" style=" height: 10px">
                            <asp:TextBox id="txtStuNo" runat="server">
                            </asp:TextBox></td>
                        <td align="left" class="matters" >
                            Student Name</td>
                        <td class="matters" style="width: 6px">
                            :</td>
                        <td align="left" class="matters" style="height: 10px">
                            <asp:TextBox id="txtName" runat="server"></asp:TextBox></td>
                        <td style="height: 10px">
                       <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" Width="51px"  /></td>
                    </tr>
                    
                    <tr>
                    <td colspan="7" style="height: 16px"></td>
                    </tr>
                      <tr>
                      
                      
                      
                     <td align="center" class="matters" colspan="7" valign="top">
                     <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                     CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      HeaderStyle-Height="30" PageSize="20" >
                         <RowStyle CssClass="griditem" Height="25px" />
                     <Columns>
                        <asp:TemplateField HeaderText="HideID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblStuId" runat="server"  Text='<%# Bind("Stu_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="HideID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblSctId" runat="server"  Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="HideID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblGrdId" runat="server"  Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="HideID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblDoj" runat="server"  Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        
                        <asp:TemplateField HeaderText="Student No"><ItemTemplate>
                        <asp:Label ID="lblStuNo" Width="70px" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px"></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR"><ItemTemplate>
                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="180px"></ItemStyle>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="Retest Result" SortExpression="DESCR"><ItemTemplate>
                           <asp:DropDownList id="ddlResult"  Enabled="false"  runat="server" AutoPostBack="True" SKINID="smallcmb"
                                Width="118px">
                                <asp:ListItem Value="Pass" >Pass</asp:ListItem>
                                <asp:ListItem Value="Fail" >Fail</asp:ListItem>
                                <asp:ListItem Value="Retest" >Retest</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle Width="180px"></ItemStyle>
                        </asp:TemplateField>
                        
                      

                         <asp:TemplateField HeaderText="Retest Marks"><ItemTemplate>
                            <asp:DataList runat="server" id="dlMarks" repeatcolumns=50 Enabled="false" >
                            <Itemtemplate><table border=1 class="BlueTable">
                            <tr><td colspan="2" class="matters" style="height:25px"><asp:Label runat="server" Text='<%# Bind("Subject") %>' id="lblSubject" ></asp:Label></td></tr>
                            <tr><td class="matters">Marks</td></tr>
                            <tr><td class="matters" ><asp:TextBox  runat="server" Text='<%# Bind("Marks") %>' id="txtMark" Width="40PX"  ></asp:TextBox>
                             <asp:Label  runat="server" Text='<%# Bind("Sbg_ID") %>' id="lblSbgId" Visible="False"  />
                            </td></tr>
                             </table> </Itemtemplate>
                            </asp:DataList>
                           </ItemTemplate>
                         </asp:TemplateField>
                         
                          <asp:TemplateField>
                        <HeaderTemplate>
                        <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                        <ItemTemplate>
                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click"  Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField> 
                         
                         
                         
</Columns>  
                         <SelectedRowStyle CssClass="Green" />
                         <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                     </td></tr>
                                        
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField id="hfACD_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfGRD_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSCT_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSTUNO" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfNAME" runat="server">
                </asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
               </td></tr>
           
    
</table>
    
      
  
</asp:Content>

