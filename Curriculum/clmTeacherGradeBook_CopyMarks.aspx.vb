﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Data.SqlClient

Partial Class Curriculum_clmTeacherGradeBook_CopyMarks
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim USR_NAME As String = Session("sUsr_name")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If


            hfEMP_ID.Value = Encr_decrData.Decrypt(Request.QueryString("empid").Replace(" ", "+"))
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfSBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))
            hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgrid").Replace(" ", "+"))
            lblDetails.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
            Dim trm As String = Encr_decrData.Decrypt(Request.QueryString("trmid").Replace(" ", "+"))
            BindTerm(trm)
            BindAssessment()
            BindCopyAssessments()
            tbMarks.Visible = False
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindTerm(ByVal trm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACd_ID=" + hfACD_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Sub BindAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GBM_ID,GBM_DESCR+'('+GBM_SHORT+')' GBM_DESCR ,GBM_SHORT,replace(convert(varchar(100),GBM_MAXMARK),'.00','')GBM_MAXMARK ,GBM_DATE FROM ACT.TEACHER_GRADEBOOK_M" _
                            & " WHERE GBM_EMP_ID=" + hfEMP_ID.Value + " AND GBM_SBG_ID=" + hfSBG_ID.Value _
                            & " AND GBM_SGR_ID=" + hfSGR_ID.Value + " AND GBM_TRM_ID=" + ddlTerm.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvAssmt.DataSource = ds
        gvAssmt.DataBind()
    End Sub

    Sub BindCopyAssessments()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAS_ID,CAS_DESC FROM ACT.ACTIVITY_SCHEDULE AS P " _
                                & " INNER JOIN ACT.ACTIVITY_D ON CAS_CAD_ID=CAD_ID " _
                                & " WHERE CAS_SGR_ID=" + hfSGR_ID.Value + " AND " _
                                & " CAD_TRM_ID=" + ddlTerm.SelectedValue.ToString + " AND" _
                                & " (SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
                                & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS B ON A.STA_CAS_ID=B.CAS_ID" _
                                & " INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON B.CAS_CAD_ID=C.RRD_CAD_ID" _
                                & " INNER JOIN RPT.REPORT_RULE_M AS E ON C.RRD_RRM_ID=E.RRM_ID" _
                                & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS F ON A.STA_STU_ID=F.RPP_STU_ID " _
                                & " AND F.RPP_RPF_ID=E.RRM_RPF_ID AND F.RPP_TRM_ID=E.RRM_TRM_ID" _
                                & " AND F.RPP_ACD_ID=E.RRM_ACD_ID AND F.RPP_GRD_ID=E.RRM_GRD_ID" _
                                & " WHERE RPP_bPUBLISH='TRUE' AND STA_CAS_ID=P.CAS_ID)=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAssessment.DataSource = ds
        ddlAssessment.DataTextField = "CAS_DESC"
        ddlAssessment.DataValueField = "CAS_ID"
        ddlAssessment.DataBind()
    End Sub

    Sub ProcessMarks()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim strWt As String = ""
        Dim strGbm As String = ""
        Dim chkSelect As CheckBox
        Dim lblGbmId As Label
        Dim txtWT As TextBox

        For i = 0 To gvAssmt.Rows.Count - 1
            With gvAssmt.Rows(i)
                chkSelect = .FindControl("chkSelect")
                lblGbmId = .FindControl("lblGbmId")
                txtWT = .FindControl("txtWT")
                If chkSelect.Checked = True Then
                    If strGbm <> "" Then
                        strGbm += "|"
                    End If
                    strGbm += lblGbmId.Text

                    If ddlOperation.SelectedValue = "WT" Then
                        If strWt <> "" Then
                            strWt += "|"
                        End If
                        strWt += txtWT.Text
                    End If
                End If
            End With
        Next

        If strGbm <> "" Then
            str_query = "EXEC ACT.getGRADEBOOKPROCESSEDMARKS " _
                        & "@SGR_ID=" + hfSGR_ID.Value + "," _
                        & "@CAS_ID=" + ddlAssessment.SelectedValue.ToString + "," _
                        & "@OPERATION='" + ddlOperation.SelectedValue.ToString + "'," _
                        & "@GBM_IDS='" + strGbm + "'," _
                        & "@WEIGHTAGES='" + strWt + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvMarks.DataSource = ds
            gvMarks.DataBind()
            tbMarks.Visible = True
        End If

    End Sub

    Sub SaveProcessAudit()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim strWt As String = ""
        Dim strGbm As String = ""
        Dim strAssmtQuery As String
        Dim chkSelect As CheckBox
        Dim lblGbmId As Label
        Dim txtWT As TextBox
        Dim lblDesc As Label

        For i = 0 To gvAssmt.Rows.Count - 1
            With gvAssmt.Rows(i)
                chkSelect = .FindControl("chkSelect")
                lblGbmId = .FindControl("lblGbmId")
                txtWT = .FindControl("txtWT")
                lblDesc = .FindControl("lblDesc")
                If chkSelect.Checked = True Then
                    If strGbm <> "" Then
                        strGbm += "|"
                    End If
                    strGbm += lblGbmId.Text

                    If ddlOperation.SelectedValue = "WT" Then
                        If strWt <> "" Then
                            strWt += "|"
                        End If
                        strWt += txtWT.Text

                        If strAssmtQuery <> "" Then
                            strAssmtQuery += "+"
                        End If
                        strAssmtQuery = lblDesc.Text + "*" + txtWT.Text
                    Else
                        If strAssmtQuery <> "" Then
                            strAssmtQuery += ","
                        End If
                        strAssmtQuery += lblDesc.Text
                    End If
                End If
            End With
        Next

        strAssmtQuery = ddlOperation.SelectedItem.Text + "(" + strAssmtQuery + ")"

        str_query = "exec ACT.saveTEACHER_GRADEBOOK_PROCESSAUDIT" _
                & " @GBP_EMP_ID=" + hfEMP_ID.Value + "," _
                & " @GBP_SGR_ID=" + hfSGR_ID.Value + "," _
                & " @GBP_CAS_ID=" + ddlAssessment.SelectedValue.ToString + "," _
                & " @GBP_OPERATION='" + ddlOperation.SelectedValue + "'," _
                & " @GBP_GBM_IDS='" + strGbm + "'," _
                & " @GBP_WEIGHTAGES='" + strWt + "'," _
                & " @GBP_QUERY='" + strAssmtQuery + "'," _
                & " @GBP_USER='" + Session("susr_name") + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


    End Sub

    Sub SaveProcessedMarks()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAS_MIN_MARK,CAS_MAX_MARK FROM ACT.ACTIVITY_SCHEDULE WHERE CAS_ID=" + ddlAssessment.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim strMax As String = ds.Tables(0).Rows(0).Item(1)
        Dim strMin As String = ds.Tables(0).Rows(0).Item(0)

        Dim i As Integer
        Dim AuditID As Int32

        Dim cmd As SqlCommand
        Dim lblStuId As Label
        Dim lblMarks As Label
        Dim lblGrade As Label
        Dim ddlAttendance As DropDownList


        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                cmd = New SqlCommand("[ACT].[SaveAUDITDATA]", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 50)
                sqlpBSU_ID.Value = Session("SBsuid")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDOCTYPE As New SqlParameter("@AUD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpDOCNO As New SqlParameter("@AUD_DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpPROCEDURE As New SqlParameter("@AUD_PROCEDURE", SqlDbType.VarChar, 200)
                sqlpPROCEDURE.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpPROCEDURE)

                Dim sqlpLOGDT As New SqlParameter("@AUD_LOGDT", SqlDbType.DateTime)
                sqlpLOGDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpLOGDT)

                Dim sqlpACTION As New SqlParameter("@AUD_ACTION", SqlDbType.VarChar, 50)
                sqlpACTION.Value = "Mark Entry"
                cmd.Parameters.Add(sqlpACTION)

                Dim sqlpDT As New SqlParameter("@AUD_DT", SqlDbType.DateTime)
                sqlpDT.Value = Date.Today()
                cmd.Parameters.Add(sqlpDT)

                Dim sqlpUSER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
                sqlpUSER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpUSER)

                Dim sqlpREMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 200)
                sqlpREMARKS.Value = "Marks"
                cmd.Parameters.Add(sqlpREMARKS)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                AuditID = retValParam.Value
                For i = 0 To gvMarks.Rows.Count - 1
                    lblStuId = gvMarks.Rows(i).FindControl("lblStuId")
                    lblMarks = gvMarks.Rows(i).FindControl("lblMarks")
                    lblGrade = gvMarks.Rows(i).FindControl("lblGrade")
                    ddlAttendance = gvMarks.Rows(i).FindControl("ddlAttendance")
                    If lblMarks.Text <> "" Then
                        str_query = "exec ACT.saveTEACHER_GRADEBOOK_PROCESSMARKS " _
                                   & " @STU_ID=" + lblStuId.Text + "," _
                                   & " @SGR_ID=" + hfSGR_ID.Value + "," _
                                   & " @SBG_ID=" + hfSBG_ID.Value + "," _
                                   & " @CAS_ID=" + ddlAssessment.SelectedValue.ToString + "," _
                                   & " @STA_MARK=" + Val(lblMarks.Text).ToString + "," _
                                   & " @STA_GRADE='" + lblGrade.Text + "'," _
                                   & " @CAS_MIN_MARK=" + strMin + "," _
                                   & " @CAS_MAX_MARK=" + strMax + "," _
                                   & " @ATTENDANCE='" + ddlAttendance.SelectedValue.ToString + "'," _
                                   & " @AUDITID=" + AuditID.ToString + ""
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next

                str_query = "ACT.updateBMARKENTERD " + ddlAssessment.SelectedValue.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                str_query = "ACT.updateBATTENDED " + ddlAssessment.SelectedValue.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)


                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                lblError.Text = "Record saved successfully"
            End Try
        End Using

    End Sub

    Protected Sub ddlOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperation.SelectedIndexChanged
        If ddlOperation.SelectedValue = "WT" Then
            gvAssmt.Columns(4).Visible = True
        Else
            gvAssmt.Columns(4).Visible = False
        End If
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        BindAssessment()
        BindCopyAssessments()
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        ProcessMarks()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveProcessAudit()
        SaveProcessedMarks()
    End Sub
End Class
