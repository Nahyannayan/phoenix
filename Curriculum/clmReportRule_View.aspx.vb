Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReportRule_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330001") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGrade.Items.Insert(0, li)

                    BindTerm()
                    BindSubjects()

                    updateSubjectURL()

                    If (Not Session("accYear_rule") Is Nothing) Or _
                    (Not Session("term_rule") Is Nothing) Or _
                    (Not Session("grade_rule") Is Nothing) Or _
                    (Not Session("subj_rule") Is Nothing) Or _
                      (Not Session("schedule_rule") Is Nothing) Or (Not Session("header_rule") Is Nothing) Then
                        SelectFields()
                        GridBind()
                    Else
                        tblClm.Rows(4).Visible = False
                        tblClm.Rows(5).Visible = False
                    End If

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub



    Protected Sub lblMarks_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
            End With
            Dim url As String
            url = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                               & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                               & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Sub SelectFields()
        If (Not Session("accYear_rule") Is Nothing) Then
            If Not ddlAcademicYear.Items.FindByValue(Session("accYear_rule")) Is Nothing Then
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.Items.FindByValue(Session("accYear_rule")).Selected = True
            End If
        End If
        If (Not Session("term_rule") Is Nothing) Then
            If Not ddlTerm.Items.FindByValue(Session("term_rule")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("term_rule")).Selected = True
            End If
        End If
        If (Not Session("grade_rule") Is Nothing) Then
            If Not ddlGrade.Items.FindByValue(Session("grade_rule")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grade_rule")).Selected = True
            End If
        End If
    End Sub

    Protected Sub lblAttendance_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label

            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
            End With
            Dim url As String
            url = String.Format("~\Curriculum\clmATTEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub ddlgvAttendance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub


    Protected Sub ddlgvMarks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub ddlgvHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"

    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlTerm.Items.Insert(0, li)
        li = New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub updateSubjectURL()
        If ddlGrade.SelectedValue = "" Then
            hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                              & "&grdid=0&stmid=0"
        Else
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            If grade.Length > 1 Then
                hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                                             & "&grdid=" + grade(0) + "&stmid=" + grade(1)
            Else
                hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                                                            & "&grdid=" + ddlGrade.SelectedValue.ToString + "&stmid=1"
            End If
        End If
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strFilter As String = ""

        'str_query = "SELECT RRM_ID,RRM_DESCR,RRM_SBG_ID,RRM_ACD_ID,RRM_GRD_ID," _
        '           & " SBG_DESCR, GRM_DISPLAY, RRM_TRM_ID, TRM_DESCRIPTION" _
        '           & " FROM RPT.REPORT_RULE_M  AS A INNER JOIN OASIS..GRADE_BSU_M AS B" _
        '           & " ON A.RRM_GRD_ID=B.GRM_GRD_ID AND A.RRM_ACD_ID=B.GRM_ACD_ID " _
        '           & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.RRM_SBG_ID=C.SBG_ID" _
        '           & " INNER JOIN VW_TRM_M AS D ON A.RRM_TRM_ID=D.TRM_ID"

        If ddlTerm.SelectedValue <> "" And ddlTerm.SelectedValue <> "0" Then
            str_query = "SELECT DISTINCT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR,RSD_ID,RSD_HEADER, " _
                            & " SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_GRD_ID,GRM_DISPLAY,TRM_ID,TRM_DESCRIPTION," _
                            & " RRM_TYPE, RRM_DESCR,RRM_ID FROM RPT.REPORT_RULE_M AS A " _
                            & " INNER JOIN RPT.REPORT_SETUP_M AS D WITH(NOLOCK) ON A.RRM_RSM_ID=D.RSM_ID" _
                            & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS E WITH(NOLOCK) ON A.RRM_RPF_ID=E.RPF_ID" _
                            & " INNER JOIN RPT.REPORT_SETUP_D AS F WITH(NOLOCK) ON A.RRM_RSD_ID=F.RSD_ID" _
                            & " INNER JOIN OASIS..GRADE_BSU_M AS G WITH(NOLOCK) ON A.RRM_GRD_ID=G.GRM_GRD_ID AND A.RRM_ACD_ID=G.GRM_ACD_ID " _
                            & " INNER JOIN SUBJECTS_GRADE_S AS H WITH(NOLOCK) ON A.RRM_SBG_ID=H.SBG_ID " _
                            & " INNER JOIN VW_TRM_M AS I WITH(NOLOCK) ON A.RRM_TRM_ID=I.TRM_ID " _
                            & " WHERE RRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Else
            str_query = "SELECT * FROM (SELECT DISTINCT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR,RSD_ID,RSD_HEADER, " _
                           & " SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_GRD_ID,GRM_DISPLAY," _
                           & " CASE WHEN TRM_ID IS NULL THEN '0' ELSE TRM_ID END AS TRM_ID  ,CASE WHEN TRM_DESCRIPTION IS NULL THEN 'TERM FINAL' ELSE TRM_DESCRIPTION END AS TRM_DESCRIPTION ," _
                           & " RRM_TYPE, RRM_DESCR,RRM_ID,RRM_GRD_ID,RRM_SBG_ID,SBG_STM_ID FROM RPT.REPORT_RULE_M AS A " _
                           & " INNER JOIN RPT.REPORT_SETUP_M AS D WITH(NOLOCK) ON A.RRM_RSM_ID=D.RSM_ID" _
                           & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS E WITH(NOLOCK) ON A.RRM_RPF_ID=E.RPF_ID" _
                           & " INNER JOIN RPT.REPORT_SETUP_D AS F WITH(NOLOCK) ON A.RRM_RSD_ID=F.RSD_ID" _
                           & " INNER JOIN OASIS..GRADE_BSU_M AS G WITH(NOLOCK) ON A.RRM_GRD_ID=G.GRM_GRD_ID AND A.RRM_ACD_ID=G.GRM_ACD_ID " _
                           & " INNER JOIN SUBJECTS_GRADE_S AS H WITH(NOLOCK) ON A.RRM_SBG_ID=H.SBG_ID " _
                           & " LEFT OUTER JOIN VW_TRM_M AS I WITH(NOLOCK) ON A.RRM_TRM_ID=I.TRM_ID " _
                           & " WHERE RRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + ") P WHERE RRM_ID<>0 "

        End If

        If (ddlGrade.SelectedValue <> "") Then
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND RRM_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        If ddlSubjects.SelectedValue <> "" Then
            str_query += " AND SBG_ID=" + ddlSubjects.SelectedValue
        End If


        If ddlTerm.SelectedValue <> "" Then
            str_query += " AND TRM_ID=" + ddlTerm.SelectedValue.ToString
        End If

        Dim ddlgvSchedule As New DropDownList
        Dim ddlgvHeader As New DropDownList

        Dim selectedHeader As String = ""
        Dim selectedSchedule As String = ""

        If gvStud.Rows.Count > 0 Then

            ddlgvSchedule = gvStud.HeaderRow.FindControl("ddlgvSchedule")
            If ddlgvSchedule.Text <> "ALL" And ddlgvSchedule.Text <> "" Then
                strFilter += " AND RPF_DESCR='" + ddlgvSchedule.Text + "'"
                Session("schedule_rule") = ddlgvSchedule.Text
                selectedSchedule = ddlgvSchedule.Text
            Else
                Session("schedule_rule") = "ALL"
                selectedSchedule = "ALL"
            End If

            ddlgvHeader = gvStud.HeaderRow.FindControl("ddlgvHeader")
            If ddlgvHeader.Text <> "ALL" And ddlgvHeader.Text <> "" Then
                strFilter += " AND rsd_header='" + ddlgvHeader.Text + "'"
                Session("header_rule") = ddlgvHeader.Text
                selectedHeader = ddlgvHeader.Text
            Else
                Session("header_rule") = "ALL"
                selectedHeader = "ALL"
            End If

            str_query += strFilter
        End If

        If Session("schedule_rule") <> "ALL" And Session("schedule_rule") <> "" Then
            str_query += " AND RPF_DESCR='" + Session("schedule_rule") + "'"
            selectedSchedule = Session("schedule_rule")
        End If
        If Session("header_rule") <> "ALL" And Session("header_rule") <> "" Then
            str_query += " AND rsd_header='" + Session("header_rule") + "'"
            selectedHeader = Session("header_rule")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.Visible = True
            gvStud.DataBind()
        End If

        Dim dr As DataRow
        Dim dt As DataTable
        dt = ds.Tables(0)

        If gvStud.Rows.Count > 0 Then
            ddlgvSchedule = gvStud.HeaderRow.FindControl("ddlgvSchedule")
            ddlgvHeader = gvStud.HeaderRow.FindControl("ddlgvHeader")
            ddlgvSchedule.Items.Clear()
            ddlgvSchedule.Items.Add("ALL")


            ddlgvHeader.Items.Clear()
            ddlgvHeader.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                    If ddlgvSchedule.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvSchedule.Items.Add(.Item(3))
                    End If

                    If ddlgvHeader.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvHeader.Items.Add(.Item(5))
                    End If


                End With

            Next

            ddlgvSchedule.Text = selectedSchedule
            ddlgvHeader.Text = selectedHeader
        End If
    End Sub

    Sub DeleteRule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblRrmId As Label
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblRrmId = gvStud.Rows(i).FindControl("lblRrmId")
                str_query = "EXEC RPT.deleteRULE " + lblRrmId.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next
    End Sub
    Function isMarkProcessed(ByVal rrmid As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RST_RRM_ID) FROM RPT.REPORT_STUDENT_S WHERE RST_RRM_ID='" + rrmid + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTerm()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)
        BindSubjects()
        updateSubjectURL()
    End Sub




    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblClm.Rows(4).Visible = True
        tblClm.Rows(5).Visible = True

        Session("schedule_rule") = "ALL"
        Session("header_rule") = "ALL"


        GridBind()

        Session("accYear_rule") = ddlAcademicYear.SelectedValue.ToString
        Session("term_rule") = ddlTerm.SelectedValue.ToString
        Session("grade_rule") = ddlGrade.SelectedValue.ToString
        Session("subj_rule") = ddlSubjects.SelectedItem.Value

    End Sub

    Sub SelectSearches(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("accYear_rule") Is Nothing Then
            If Not ddlAcademicYear.Items.FindByValue(Session("accYear_rule")) Is Nothing Then
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.Items.FindByValue(Session("accYear_rule")).Selected = True
                ddlAcademicYear_SelectedIndexChanged(sender, e)
            End If
        End If

        If Not Session("term_rule") Is Nothing Then
            If Not ddlTerm.Items.FindByValue(Session("term_rule")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("term_rule")).Selected = True
            End If
        End If

        If Not Session("grade_rule") Is Nothing Then
            If Not ddlGrade.Items.FindByValue(Session("grade_rule")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grade_rule")).Selected = True
                ddlGrade_SelectedIndexChanged(sender, e)
            End If
        End If

        If Not Session("subj_rule") Is Nothing Then
            hfSBG_ID.Value = Session("subj_rule")
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ID='" + hfSBG_ID.Value + "'"
            txtSubject.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        updateSubjectURL()
        GridBind()
    End Sub

    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\clmReportRule_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt("edit")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblRrmId As Label
                With selectedRow
                    lblRrmId = .FindControl("lblRrmId")
                End With
                Dim url As String
                url = String.Format("~\Curriculum\clmReportRule_M.aspx?MainMnu_code={0}&datamode={1}&rrmid=" + Encr_decrData.Encrypt(lblRrmId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    
    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelect As CheckBox
            Dim lblRrmId As Label
            Dim lblRule As LinkButton = e.Row.FindControl("lblRule")
            Dim lblSubject As Label = e.Row.FindControl("lblSubject")
            Dim lblPrinted As Label = e.Row.FindControl("lblPrinted")
            Dim lblHeader As Label = e.Row.FindControl("lblHeader")

            chkSelect = e.Row.FindControl("chkSelect")
            lblRrmId = e.Row.FindControl("lblRrmId")
            If isMarkProcessed(lblRrmId.Text) = True Then
                chkSelect.Enabled = False
            End If
            Dim strLink As String = "clmReportRule_Popup.aspx?rrm_id=" + lblRrmId.Text + "&subject=" + lblSubject.Text + "&report=" + lblPrinted.Text + "&header=" + lblHeader.Text
            lblRule.Attributes.Add("onclick", "ShowWindowWithClose('" + strLink + "','search', '35%', '85%');")

        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteRule()
        GridBind()
    End Sub

    Sub BindSubjects()

        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            If ddlGrade.SelectedValue <> "" Then
                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")
                str_query = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                          & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                          & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + ddlAcademicYear.SelectedValue _
                          & " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1)
            Else
                ddlSubjects.Items.Clear()
                ddlSubjects.Items.Insert(0, New ListItem("All", ""))
            End If

            If str_query <> "" Then
                str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlSubjects.DataSource = ds
                    ddlSubjects.DataTextField = "SBG_DESCR"
                    ddlSubjects.DataValueField = "SBG_ID"
                    ddlSubjects.DataBind()
                End If

                ddlSubjects.Items.Insert(0, New ListItem("All", ""))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try


    End Sub

End Class
