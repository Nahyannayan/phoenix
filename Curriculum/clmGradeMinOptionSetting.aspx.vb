Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmGradeMinOptionSetting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100160") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("CLM"), Session("SBSUID"))
                    GridBind()
                    gvGrade.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
   
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)

            Dim lblGrdId As Label = TryCast(sender.FindControl("lblGrdId"), Label)
            Dim lblStmId As Label = TryCast(sender.FindControl("lblStmId"), Label)

            Dim lbloPT As Label = TryCast(sender.FindControl("lbloPT"), Label)
        
            Dim txtOpt As TextBox = TryCast(sender.FindControl("txtOpt"), TextBox)
        

            If lblEdit.Text = "Edit" Then
                lblEdit.Text = "Update"

               
                txtOpt.Visible = True

                
                txtOpt.Text = lbloPT.Text

            Else
                lblEdit.Text = "Edit"
                SaveData(lblGrdId.Text, lblStmId.Text, txtOpt.Text)
                lbloPT.Text = txtOpt.Text
                txtOpt.Visible = False
                lblError.Text = "Record Saved Successfully"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "PrivateMethods"

    Function GetEmpMobilNo(ByVal empId As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select isnull(EMD_CUR_MOBILE,'') from employee_d where emd_emp_id=" + empId
        Dim mobile As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return mobile
    End Function

 
  
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,STM_ID,GRM_ACD_ID,GRM_GRD_ID,ISNULL(GRO_OPTIONS,0) AS GRO_OPTIONS,STM_DESCR " _
                                 & " FROM GRADE_BSU_M AS A INNER JOIN STREAM_M AS B " _
                                 & " ON A.GRM_STM_ID=B.STM_ID LEFT OUTER JOIN " _
                                 & " OASIS_CURRICULUM..GRADE_MINOPTIONSETTING AS C " _
                                 & " ON A.GRM_GRD_ID=C.GRO_GRD_ID AND A.GRM_ACD_ID=C.GRO_ACD_ID " _
                                 & " AND A.GRM_STM_ID=C.GRO_STM_ID" _
                                 & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY GRM_DISPLAY"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGrade.DataSource = ds
        gvGrade.DataBind()
    End Sub

    Sub SaveData(ByVal grd_id As String, ByVal stm_id As String, ByVal options As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec saveGRADE_MINOPTIONS " _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & "'" + grd_id + "'," _
                                & stm_id + "," _
                                & options

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

  
#End Region

    




    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvGrade_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvGrade.RowEditing

    End Sub
End Class
