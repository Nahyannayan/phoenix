<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmReportCardSetup_View_Old.aspx.vb" Inherits="Curriculum_clmReportCardSetup_View_Old"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Report Card Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr >
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                           </td>
        </tr>
        <tr>
            <td align="center" colspan="6">
                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="3" align="center" class="matters">
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="subheader_img">
                                    <td align="left" valign="middle" colspan="3">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                            REPORT CARD SETUP</span></font></td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="10%">
                                       <span class="field-label"> Select Academic Year </span>
                                    </td>
                                   
                                    <td width="20%">
                                        <asp:DropDownList id="ddlAcademicYear" runat="server"  AutoPostBack="True" >
                                        </asp:DropDownList></td>
                                    <td colspan="2" width="70%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left" >
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" style="width: 338px">
                                        <asp:GridView ID="gvReport" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" >
                                            <rowstyle CssClass="griditem"  />
                                            <emptydatarowstyle  />
                                            <columns>

<asp:TemplateField HeaderText="sbm_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblRsmId" runat="server" Text='<%# Bind("RSM_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="Report Card">
<ItemTemplate>
<asp:Label ID="lblReport" runat="server" text='<%# Bind("RSM_DESCR") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>


<asp:ButtonField CommandName="View" Text="View" HeaderText="View">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
</asp:ButtonField>

</columns>
                                            <selectedrowstyle  />
                                            <headerstyle  />
                                            <editrowstyle  />
                                            <alternatingrowstyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

                  </div>
        </div>
    </div>
</asp:Content>
