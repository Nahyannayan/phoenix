<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectGradeSetOrder_M.aspx.vb" Inherits="Curriculum_clmSubjectGradeSetOrder_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function moveOptionUp() {
            var obj = document.getElementById("<%=lstSubjects.ClientId%>")
    if (!hasOptions(obj)) { return; }
    for (i = 0; i < obj.options.length; i++) {
        if (obj.options[i].selected) {
            if (i != 0 && !obj.options[i - 1].selected) {
                swapOptions(obj, i, i - 1);
                obj.options[i - 1].selected = true;
            }
        }
    }
    persistOptionsList();
}
function hasOptions(obj) {
    if (obj != null && obj.options != null) { return true; }
    return false;
}


function moveOptionDown() {
    var obj = document.getElementById("<%=lstSubjects.ClientId%>")
    if (!hasOptions(obj)) { return; }
    for (i = obj.options.length - 1; i >= 0; i--) {
        if (obj.options[i].selected) {
            if (i != (obj.options.length - 1) && !obj.options[i + 1].selected) {
                swapOptions(obj, i, i + 1);
                obj.options[i + 1].selected = true;
            }
        }
    }
    persistOptionsList();
}
        function UncheckOtherNodes(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var oTree = document.getElementById("<%=tvSubjects.ClientID%>");
            childChkBoxes = oTree.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                if (childChkBoxes[i].id != src.id) {
                    childChkBoxes[i].checked = false;
                }
            }
        }

    }


function removeSelectedOptions() {
    var from = document.getElementById("<%=lstSubjects.ClientId%>")

    if (!hasOptions(from)) { return; }
    if (from.type == "select-one") {
        from.options[from.selectedIndex] = null;
    }
    else {
        for (var i = (from.options.length - 1) ; i >= 0; i--) {
            var o = from.options[i];
            if (o.selected) {
                from.options[i] = null;
            }
        }
    }
    from.selectedIndex = -1;
    persistOptionsList();
}


function RemoveOption() {

}
function swapOptions(obj, i, j) {
    var o = obj.options;
    var i_selected = o[i].selected;
    var j_selected = o[j].selected;
    var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
    var temp2 = new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
    o[i] = temp2;
    o[j] = temp;
    o[i].selected = j_selected;
    o[j].selected = i_selected;
}


//to retain values in the trips listbox after postback the list items are stored in an input box	
function persistOptionsList() {
    var listBox1 = document.getElementById("<%=lstSubjects.ClientId%>");
    var optionsList = '';
    if (listBox1.options.length > 0) {
        for (var i = 0; i < listBox1.options.length; i++) {
            var optionText = listBox1.options[i].text;
            var optionValue = listBox1.options[i].value;

            if (optionsList.length > 0)
                optionsList += '|';
            optionsList += optionText + "$" + optionValue;
        }
    }
    document.getElementById("<%=lstValues.ClientId%>").value = optionsList;
 return false;
}

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Subject Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="5"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="Table1" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td>
                                        <table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td align="left" >
                                                    <asp:Label ID="Label1" class="field-label" runat="server"  Text="Academic Year"></asp:Label></td>                                               
                                                <td align="left" width="25%">&nbsp;<asp:Label ID="lblAcademicYear" class="field-value" runat="server"></asp:Label></td>

                                                <td align="left"  >
                                                    <asp:Label ID="Label2" class="field-label" runat="server" Text="Grade"></asp:Label></td>                                                
                                                <td align="left" width="20%">&nbsp;<asp:Label ID="lblGrade" class="field-value" runat="server"></asp:Label></td>

                                                <td align="left"  >
                                                    <asp:Label ID="Label3" class="field-label" runat="server" Text="Stream"></asp:Label></td>                                            
                                                <td align="left" width="20%" >&nbsp;<asp:Label ID="lblStream"  class="field-value" runat="server"></asp:Label>                                                 
                                                </td>
                                            </tr>                                          
                                        </table>
                                    </td>
                                </tr> 
                              <tr>
                                  <td >
                                  </td>
                              </tr>                          
                                <tr >
                                    <td align="left"  valign="middle" class="title-bg">
                                        <span class="field-label">Details</span> 
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  width="100%">
                                        <table id="Table4" runat="server" border="0" cellpadding="12" cellspacing="0" class="table">
                                            <tr width="100%">
                                               
                                                <td valign="middle" align="center" width="20%">
                                                    <asp:TreeView ID="tvSubjects" runat="server" Width="100%" Height="100%"
                                                        DataSourceID="XmlSubjects" ExpandDepth="1" NodeIndent="15" onclick="return UncheckOtherNodes(event);"
                                                        Style="overflow: auto; text-align: left">
                                                        <ParentNodeStyle Font-Bold="True" />
                                                        <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Underline="False"
                                                            HorizontalPadding="3px" VerticalPadding="1px" />
                                                        <DataBindings>
                                                            <asp:TreeNodeBinding DataMember="menuItem" TextField="Text" ValueField="valuefield"></asp:TreeNodeBinding>
                                                        </DataBindings>
                                                    </asp:TreeView>                                                   
                                                </td>
                                                <td valign="middle"  align="center"  width="50%" style="border: 0px solid green;">
                                                     <asp:ListBox ID="lstSubjects" runat="server" SelectionMode="Multiple"
                                                        Style="overflow: auto;" Rows="12"></asp:ListBox>
                                                </td>
                                                <td width="30%"></td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>                                        
                                                <td align="center">
                                                    <input id="btnUp" type="button" value="Up" onclick="moveOptionUp()" class="button" />
                                                 
                                                    <input id="btnDown" type="button" value="Down" onclick="moveOptionDown()" class="button" />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                        <br /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td  valign="bottom"  align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" OnClick="btnCancel_Click1" />
                                    </td>
                                </tr>
                            </table>
                            <asp:XmlDataSource ID="XmlSubjects" runat="server" EnableCaching="False" TransformFile="~/Curriculum/subjectorderXSLT.xsl"></asp:XmlDataSource>
                            &nbsp;&nbsp;
               <input id="lstValues" name="lstValues" runat="server" type="hidden" />
                            &nbsp; &nbsp;&nbsp;
               <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfSTM_ID" runat="server" />
                            &nbsp;
               <asp:HiddenField ID="hfParent" runat="server" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
        >
    </div>

</asp:Content>

