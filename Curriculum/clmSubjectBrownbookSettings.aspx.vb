﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSubjectBrownbookSettings
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("SelectedRow") = -1
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblAcademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    BindSubjects()
                    gvSubject.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfGRD_ID.Value = "KG1" Or hfGRD_ID.Value = "KG2" Then
            str_query = "SELECT SBG_ID,SBG_DESCR,ISNULL(SBG_MINMARK,33) SBG_MINMARK,ISNULL(SBG_bRETEST,'false') SBG_bRETEST," _
                            & " ISNULL(SBG_bBROWNBOOKDISPLAY,'true') SBG_bBROWNBOOKDISPLAY,ISNULL(SBG_bBTEC,'FALSE') SBG_bBTEC FROM SUBJECTS_GRADE_S" _
                            & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
                            & " AND SBG_STM_ID=" + hfSTM_ID.Value _
                            & " ORDER BY SBG_ORDER"
        Else
            str_query = "SELECT SBG_ID,SBG_DESCR,ISNULL(SBG_MINMARK,33) SBG_MINMARK,ISNULL(SBG_bRETEST,'false') SBG_bRETEST," _
                                        & " ISNULL(SBG_bBROWNBOOKDISPLAY,'true') SBG_bBROWNBOOKDISPLAY,ISNULL(SBG_bBTEC,'FALSE') SBG_bBTEC FROM SUBJECTS_GRADE_S" _
                                        & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
                                        & " AND SBG_STM_ID=" + hfSTM_ID.Value + " AND SBG_PARENT_ID=0" _
                                        & " ORDER BY SBG_ORDER"
        End If
    
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubject.DataSource = ds
        gvSubject.DataBind()
    End Sub
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim i As Integer
        Dim str_query As String
        Dim lblSbgId As Label
        Dim txtMinMark As TextBox
        Dim chkRetest As CheckBox
        Dim chkDisplay As CheckBox
        Dim chkBtec As CheckBox
        For i = 0 To gvSubject.Rows.Count - 1
            With gvSubject.Rows(i)
                lblSbgId = .FindControl("lblSbgId")
                txtMinMark = .FindControl("txtMinMark")
                chkRetest = .FindControl("chkRetest")
                chkDisplay = .FindControl("chkDisplay")
                chkBtec = .FindControl("chkBtec")
            End With
            str_query = "exec   updateSUBJECTBROWNBOOKSETTINGS " _
                       & "@SBG_ID=" + lblSbgId.Text + "," _
                       & "@SBG_MINMARK=" + Val(txtMinMark.Text).ToString + "," _
                       & "@SBG_bRETEST='" + chkRetest.Checked.ToString + "'," _
                       & "@SBG_bBROWNBOOKDISPLAY='" + chkDisplay.Checked.ToString + "'," _
                       & "@SBG_bBTEC='" + chkBtec.Checked.ToString + "'"

            SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Next

    End Sub
#End Region


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub gvSubject_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubject.PageIndexChanging
        gvSubject.PageIndex = e.NewPageIndex
        BindSubjects()
    End Sub
End Class
