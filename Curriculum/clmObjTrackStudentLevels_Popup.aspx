﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmObjTrackStudentLevels_Popup.aspx.vb"  Inherits="Curriculum_clmObjTrackStudentLevels_Popup" MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>Student Levels</title>
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>
</head>
    
<script type="text/javascript">

function SetTextHeight(txtBox,evt)
{
txtBox.style.height = 100+ "px";
txtBox.style.width = 300+ "px";
return true;
}

function SetTextHeightNormal(txtBox,evt)
{
txtBox.style.height =  "15 px";
txtBox.style.width =  "20 px";
return true;
}


function getcomments(ctlid,code,header,stuid,rsmid,sbgid)
{    
        var sFeatures;
        sFeatures="dialogWidth: 530px; ";
        sFeatures+="dialogHeight: 450px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var result;
        var parentid;
        var varray= new Array();
        if(code=='ALLCMTS')
        {
            //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code +"","", sFeatures);
              document.getElementById('<%=H_CTL_ID.ClientID%>').value = ctlid
            var oWnd = radopen(url, "pop_comment");
      //  result = window.showModalDialog("clmCommentsList.aspx?ID=" + code +"&HEADER=" + header +"&STUID=" + stuid + "&RSMID="+rsmid+"&SBGID="+sbgid,"", sFeatures);          
        //if(result != '' && result != undefined)
        //{
        //    document.getElementById(ctlid).value = document.getElementById(ctlid).value + result.replace(/_#_/g,"'");
        //    //result.split('___')[0]
        //    //document.getElementById(ctlid.split(';')[1]).value=varray[0];
        //}
        //else
        //{
        //    return false;
        //}
     }
 
}
    function OnClientClose(oWnd, args) {
            
           var NameandCode;
            var arg = args.get_argument();
            var ctlid = document.getElementById('<%=H_CTL_ID.ClientID%>').value;
            if (arg) {
                NameandCode = arg.Comment.split('||'); 
                str = document.getElementById(ctlid).value;
                document.getElementById(ctlid).value = str + NameandCode[0].replace("/ap/", "'");
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    function change_chk_state(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkStatus") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click(); //fire the click event of the child element
            }
        }
    }


    function change_chk_state1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkDisplay") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click(); //fire the click event of the child element
            }
        }
    }


    function change_ddl_state(ddlThis, ddlObj) {



        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;


            if (currentid.indexOf(ddlObj) != -1) {

                if (ddlThis.selectedIndex != 0) {

                    document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex - 1;



                    if (document.forms[0].elements[i].selectedIndex == 0) {
                        ddlThis.style.backgroundColor = "green";
                        document.forms[0].elements[i].style.backgroundColor = "green";
                    }
                    else if (document.forms[0].elements[i].selectedIndex == 1) {
                        ddlThis.style.backgroundColor = "yellow";
                        document.forms[0].elements[i].style.backgroundColor = "yellow";
                    }
                    else if (document.forms[0].elements[i].selectedIndex == 2) {
                        ddlThis.style.backgroundColor = "red";
                        document.forms[0].elements[i].style.backgroundColor = "red";
                    }


                }


            }
        }
    }

    function setBackColor(ddlThis) {

        if (ddlThis.selectedIndex == 0) {
            ddlThis.style.backgroundColor = "green";
        }
        else if (ddlThis.selectedIndex == 1) {
            ddlThis.style.backgroundColor = "yellow";
        }
        else if (ddlThis.selectedIndex == 2) {
            ddlThis.style.backgroundColor = "red";
        }
    }

</script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>  
            
    </telerik:RadWindowManager>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="overflow: hidden">
        <p>
            &nbsp;</p>
        <table id="Table2" runat="server" align="center" width="100%">
            <tr id="trError" runat="server">
         <td align="left"  valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"  ></asp:Label></td>
        </tr>
            <tr>
                <td  align="left">
                   <span class="field-label" >Academic Year</span>
                </td>
               
                <td  align="left">
                    <asp:Label ID="lblAcademicYear" CssClass="field-value" runat="server"></asp:Label>
                </td>
                <td  align="left">
                   <span class="field-label" > Grade</span> 
                </td>
               
                <td  align="left">
                    <asp:Label ID="lblGrade" CssClass="field-value" runat="server"></asp:Label>
                </td>
                <td rowspan="3"  align="center">
                    <asp:Label ID="lblCurrentLevel" runat="server" CssClass="field-label"></asp:Label>
                    <asp:Label ID="lblCurrentGrade" runat="server" CssClass="field-label"></asp:Label>
                </td>
                <td rowspan="3" align="center">
                    <asp:Image ID="imgStud" runat="server" class="img-thumbnail" ImageUrl="~/Images/Photos/no_image.gif" Width="100px" 
                        />
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <span class="field-label" width="20%" >Subject</span> 
                </td>
               
                <td  align="left" width="40%">
                    <asp:Label ID="lblSubject" CssClass="field-value"  runat="server"></asp:Label>
                </td>
                <td  align="left">
                   <span class="field-label" width="20%" > Group</span> 
                </td>
              
                <td  align="left">
                    <asp:Label ID="lblGroup" CssClass="field-value" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                   <span class="field-label" > Student ID</span> 
                </td>
                
                <td  align="left">
                    <asp:Label ID="lblStudentID"  CssClass="field-value" runat="server"></asp:Label>
                </td>
                <td  align="left">
                   <span class="field-label" > Student Name</span> 
                </td>
               
                <td  align="left">
                    <asp:Label ID="lblName" CssClass="field-value" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr id="trLevel1" runat="server">
                <td  align="left">
                   <span class="field-label" > Level</span> 
                </td>
               
                <td  align="left">
                    <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td  align="left">
                   <span class="field-label" > Category </span> 
                </td>
                <td  colspan="5">
                    <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trLevel2" runat="server">
                <td  colspan="3">
                    <asp:Label ID="lblTotalObjectives" CssClass="field-label" runat="server"></asp:Label>
                </td>
                <td  colspan="5">
                    <asp:Label ID="lblLevelObjectives" CssClass="field-label" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  colspan="8" align="left">
                    <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0" Width="100%">
                        <ajaxToolkit:TabPanel ID="HT1" runat="server">
                            <ContentTemplate>
                                <div style="overflow: auto">
                                    <asp:GridView ID="gvObjectives" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" Width="100%" BorderStyle="None">
                                        <RowStyle CssClass="griditem"   />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="NC Level" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("OBJ_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Objectives">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjective" runat="server" Text='<%# Bind("OBJ_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" onChange="Javascript:setBackColor(this);" >
                                                        <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                        <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                        <asp:ListItem Text="N" Value="N" style="background: red" title="No" Selected="True" />
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" />
                                                <HeaderTemplate>
                                                     Status
                                                                <br />
                                                                <asp:DropDownList ID="ddlAll" runat="server" onclick="javascript:change_ddl_state(this,'ddlStatus');">
                                                                    <asp:ListItem Text="--" Value="--" />
                                                                    <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                    <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                    <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                </asp:DropDownList>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Comments">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtObjComments"  onkeydown="return SetTextHeight(this,event);" onmousedown="return SetTextHeight(this,event);" onblur="return SetTextHeightNormal(this,event);" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridheader_pop"  />
                                        <SelectedRowStyle CssClass="Green"  />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                 <asp:Label ID="lblHdrObjectives" runat="server" Text="Objectives"></asp:Label>  
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT2" runat="server">
                            <ContentTemplate>
                                <div style="overflow: auto">
                                    <asp:GridView ID="gvTarget" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" Width="100%" BorderStyle="None">
                                        <RowStyle CssClass="griditem"  />
                                        <EmptyDataRowStyle />
                                        <Columns>
                                            <asp:TemplateField HeaderText="NC Level" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("OBJ_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Objectives">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObjective" runat="server" Text='<%# Bind("OBJ_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Target">
                                                <HeaderTemplate>
                                                     Select Targets
                                                                <br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkStatus" runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Display in Reportcard">
                                                <HeaderTemplate>
                                                   Display in Reportcard
                                                                <br />
                                                                 <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_state1(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDisplay" runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="Green"  />
                                        <HeaderStyle  CssClass="gridheader_pop"  />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrTargets" runat="server" Text="Set Targets"></asp:Label>  
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT3" runat="server">
                            <ContentTemplate>
                                <div style="overflow: auto">
                                    <table width="100%" cellpadding="5" cellspacing="0" >
                                        <tr>
                                            <td >
                                                Select ReportCard
                                            </td>
                                         
                                            <td  >
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="tr1" runat="server"><td colspan="3"  align="right"><telerik:RadSpell ID="RadSpell1"    ButtonType="LinkButton"  IsClientID="true" AjaxUrl =  "~/Telerik.Web.UI.SpellCheckHandler.axd" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd"  runat="server"  SupportedLanguages="en-US,English" /></td></tr>
                                        <tr id="tr2" runat="server" >
                                            <td colspan="3">
                                                <asp:GridView ID="gvMarks" runat="server" Width="100%" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                                    EmptyDataText="No Data." OnRowDataBound="gvMarks_RowDataBound">
                                                    <Columns>
                                                       
                                                        <asp:TemplateField HeaderText="RsdId" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("rsd_id") %>'></asp:Label>
                                                            </ItemTemplate> 
                                                            <ItemStyle ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="TYPE" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeader" runat="server" Text='<%# Bind("rsd_header") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtGrade"  runat="server" Visible='<%# Bind("V1") %>'></asp:TextBox>
                                                                <%--<table>
                                                                    <tr>
                                                                        <td>
                                                                            
                                                                        </td>
                                                                        <td>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </table>--%>
                                                                <asp:TextBox ID="txtComments"  TextMode="MultiLine" SkinID="MultiText_Large"
                                                                                runat="server" Visible='<%# Bind("V2") %>'></asp:TextBox>
                                                                            <br />
                                                                            <asp:ImageButton ID="imgBtn" ImageUrl="~/Images/comment.jpg" Visible='<%# Bind("V2") %>'
                                                                                runat="server" />
                                                                <asp:DropDownList ID="ddlGrades" runat="server" Visible='<%# Bind("V3") %>'>
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblMark" runat="server" Visible='<%# Bind("V4") %>'></asp:Label>
                                                                <asp:Label ID="lblGrade" runat="server" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle ></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="griditem"   />
                                                    <HeaderStyle CssClass="gridheader_pop"  />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Report Writing
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="8">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="7"
                        Width="115px" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hSTU_ID" runat="server" />
        <asp:HiddenField ID="hACD_ID" runat="server" />
        <asp:HiddenField ID="hSBM_ID" runat="server" />
        <asp:HiddenField ID="hSGR_ID" runat="server" />
        <asp:HiddenField ID="hSBG_ID" runat="server" />
        <asp:HiddenField ID="hGRD_ID" runat="server" /><asp:HiddenField ID="hfAppSubject" runat="server" />
        <asp:HiddenField ID="hRSM_ID" runat="server" />
        <asp:HiddenField ID="hObjCompleted" runat="server" />
        <asp:HiddenField ID="hStuPhotoPath" runat="server" />
          <asp:HiddenField ID="H_CTL_ID" runat="server"></asp:HiddenField>
    </div>
    </form>
</body>
</html>
