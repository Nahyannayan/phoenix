Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Partial Class Curriculum_clmReportCommonData
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100191") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindReportPrintFor()
                    BindReportsetup()
                    bindGrid()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If

                    ViewState("SelectedRow") = -1
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub
    Sub BindReportPrintFor()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                    & " ORDER BY RPF_DISPLAYORDER "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlRpf.DataSource = ds
            ddlRpf.DataTextField = "RPF_DESCR"
            ddlRpf.DataValueField = "RPF_ID"
            ddlRpf.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub BindReportsetup()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_bCOMMON=1 AND RSD_RSM_ID = " + ddlReportCard.SelectedValue.ToString _
                                    & " ORDER BY RSD_DISPLAYORDER "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlheader.DataSource = ds
            ddlheader.DataTextField = "RSD_HEADER"
            ddlheader.DataValueField = "RSD_ID"
            ddlheader.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RDM_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_HEADER"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_SUB_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_DISPLAYORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub BindDefaultValues(ByVal rdm_id As String, ByVal ddlDefaultValues As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RDD_ID,RDD_DESCR FROM RPT.DEFAULTBANK_D WHERE RDD_RDM_ID=" + rdm_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDefaultValues.DataSource = ds
        ddlDefaultValues.DataTextField = "RDD_DESCR"
        ddlDefaultValues.DataValueField = "RDD_ID"
        ddlDefaultValues.DataBind()
    End Sub



    Sub EnableDisableControls(ByVal Value As Boolean)
        ddlReportCard.Enabled = Value
        ddlAcademicYear.Enabled = Value
        ddlGrade.Enabled = Value
        ddlSubject.Enabled = Value
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        bindGrid()
    End Sub

    Protected Sub btnAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCriteria.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(15) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@RCP_RSM_ID", ddlReportCard.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@RCP_RPF_ID", ddlRpf.SelectedValue)
            param(2) = New SqlClient.SqlParameter("@RCP_RSD_ID", ddlheader.SelectedValue)
            param(3) = New SqlClient.SqlParameter("@RCP_SBG_ID", ddlSubject.SelectedValue)
            param(4) = New SqlClient.SqlParameter("@RCP_DESCR", txtDetail.Text)
            param(5) = New SqlClient.SqlParameter("@RCP_ACD_ID", ddlAcademicYear.SelectedValue)
            param(6) = New SqlClient.SqlParameter("@RCP_GRD_ID", ddlGrade.SelectedValue)
            If ViewState("HF_RCP_ID") <> "" Then
                param(7) = New SqlClient.SqlParameter("@RCP_ID", ViewState("HF_RCP_ID"))
            Else
                param(7) = New SqlClient.SqlParameter("@RCP_ID", DBNull.Value)
            End If
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "RPT.SAVEREPORT_SETUP_COMMONDATA", param)

            ViewState("HF_RCP_ID") = ""
            bindGrid()

            If btnAddCriteria.Text = "Update" Then
                btnAddCriteria.Text = "Save"
            End If
            txtDetail.Text = ""
            'EnableDisableControls(False)
            ' ClearControls()
            'ViewState("SelectedRow") = -1
            'btnAddCriteria.Text = "Add Criteria"
            lblError.Text = "Records Saved Successfully"
        Catch ex As Exception
            lblError.Text = ex.Message.ToString
        End Try
    End Sub
    Sub ClearControls()
        txtDetail.Text = ""
        ViewState("HF_RCP_ID") = ""
        btnAddCriteria.Text = "Save"
        BindReportCard()
        BindReportPrintFor()
        BindReportsetup()
        bindGrid()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

    End Sub
    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindReportPrintFor()
        BindReportsetup()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        bindGrid()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        bindGrid()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnableDisableControls(True)

    End Sub

    Protected Sub ddlRpf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRpf.SelectedIndexChanged
        BindReportsetup()
        bindGrid()
    End Sub
    Sub bindGrid()
        Try
            Dim ds As DataSet
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT  RPT.REPORT_SETUP_COMMONDATA.RCP_ID, RPT.REPORT_SETUP_COMMONDATA.RCP_RSM_ID, " _
                               & " RPT.REPORT_SETUP_COMMONDATA.RCP_RPF_ID, RPT.REPORT_SETUP_COMMONDATA.RCP_RSD_ID, " _
                               & " RPT.REPORT_SETUP_COMMONDATA.RCP_SBG_ID, RPT.REPORT_SETUP_COMMONDATA.RCP_DESCR, " _
                               & " RPT.REPORT_SETUP_COMMONDATA.RCP_ACD_ID, RPT.REPORT_SETUP_COMMONDATA.RCP_GRD_ID, VW_ACADEMICYEAR_M.ACY_DESCR, " _
                               & " RPT.REPORT_PRINTEDFOR_M.RPF_DESCR, RPT.REPORT_SETUP_D.RSD_HEADER, RPT.REPORT_SETUP_M.RSM_DESCR" _
                               & " FROM RPT.REPORT_SETUP_COMMONDATA" _
                               & " INNER JOIN  vw_ACADEMICYEAR_D ON RPT.REPORT_SETUP_COMMONDATA.RCP_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID " _
                               & " INNER JOIN  VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " _
                               & " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON RPT.REPORT_SETUP_COMMONDATA.RCP_RPF_ID = RPT.REPORT_PRINTEDFOR_M.RPF_ID" _
                               & " INNER JOIN  RPT.REPORT_SETUP_D ON RPT.REPORT_SETUP_COMMONDATA.RCP_RSD_ID = RPT.REPORT_SETUP_D.RSD_ID " _
                               & " INNER JOIN  RPT.REPORT_SETUP_M ON RPT.REPORT_SETUP_COMMONDATA.RCP_RSM_ID = RPT.REPORT_SETUP_M.RSM_ID" _
                               & " WHERE RSM_ID='" + ddlReportCard.SelectedValue.ToString + "' AND RSD_ID='" + ddlheader.SelectedValue.ToString + "'" _
                               & " AND RPF_ID='" + ddlRpf.SelectedValue.ToString + "'" _
                               & " AND RCP_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " AND RCP_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                               & "	order BY RCP_ID DESC"
            '   ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "RPT.GETREPORT_SETUP_COMMONDATA")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvcommon.DataSource = ds
            gvcommon.DataBind()
            'EnableDisableControls(False)
            'ClearControls()
            'ViewState("SelectedRow") = -1
            'btnAddCriteria.Text = "Add Criteria"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnk_Edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("HF_RCP_ID") = TryCast(sender.parent.FindControl("HF_RCP_ID"), HiddenField).Value
            Dim HF_ACD_ID As String = ""
            Dim HF_RSM_ID As String = ""
            Dim HF_RPF_ID As String = ""
            Dim HF_RSD_ID As String = ""
            Dim HF_GRD_ID As String = ""

            If ViewState("HF_RCP_ID") <> "" Then
                'HF_ACD_ID = TryCast(sender.parent.FindControl("HF_ACD_ID"), HiddenField).Value
                'ddlAcademicYear.SelectedValue = HF_ACD_ID
                'ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

                'HF_RSM_ID = TryCast(sender.parent.FindControl("HF_RSM_ID"), HiddenField).Value
                'ddlReportCard.SelectedValue = HF_RSM_ID
                'ddlReportCard_SelectedIndexChanged(ddlReportCard, Nothing)

                'HF_RPF_ID = TryCast(sender.parent.FindControl("HF_RPF_ID"), HiddenField).Value
                'ddlRpf.SelectedValue = HF_RPF_ID
                'ddlRpf_SelectedIndexChanged(ddlRpf, Nothing)

                'HF_RSD_ID = TryCast(sender.parent.FindControl("HF_RSD_ID"), HiddenField).Value
                'ddlheader.SelectedValue = HF_RSD_ID
                'ddlheader_SelectedIndexChanged(ddlheader, Nothing)

                'HF_GRD_ID = TryCast(sender.parent.FindControl("HF_GRD_ID"), HiddenField).Value
                'ddlGrade.SelectedValue = HF_GRD_ID
                'ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

                txtDetail.Text = TryCast(sender.parent.FindControl("lblDescr"), Label).Text
                'ViewState("HF_RCP_ID") = TryCast(sender.parent.FindControl("HF_RCP_ID"), HiddenField).Value
                'ViewState("HF_RCP_ID") = TryCast(sender.parent.FindControl("HF_RCP_ID"), HiddenField).Value



                ViewState("status") = "update"
                btnAddCriteria.Text = "Update"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlheader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlheader.SelectedIndexChanged
        bindGrid()
    End Sub
End Class
