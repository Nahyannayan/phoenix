﻿
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmCASEQuestion_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C410055") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                BindAcademicYear()
                GridBind()
                gvQuestions.Attributes.Add("bordercolor", "#1b80b6")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACY_ID FROM ACADEMICYEAR_M WHERE ACY_ID>=23"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ACY_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                              & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=" + Session("CLM")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT QSM_ID,QSM_GRD_ID,QSM_SBM_ID,QSM_DESCR,QSM_TOTALMARKS,QSM_TOTALQUESTIONS,SBM_DESCR FROM CE.QUESTIONS_M AS A " _
                               & " INNER JOIN SUBJECT_M ON QSM_SBM_ID=SBM_ID WHERE QSM_ACY_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " ORDER BY QSM_GRD_ID,SBM_DESCR,QSM_DESCR"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvQuestions.DataSource = ds
        gvQuestions.DataBind()
    End Sub
#End Region
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblQsmId As Label
            Dim lblSbmbId As Label
            Dim lblGrade As Label
            Dim lblSubject As Label
            Dim lblQuestion As Label
            Dim lblTotalQuestions As Label
            Dim lblTotalMarks As Label

            Dim url As String

            lblQsmId = TryCast(sender.FindControl("lblQsmId"), Label)
            lblSbmbId = TryCast(sender.FindControl("lblSbmbId"), Label)
            lblGrade = TryCast(sender.FindControl("lblGrade"), Label)
            lblSubject = TryCast(sender.FindControl("lblSubject"), Label)
            lblQuestion = TryCast(sender.FindControl("lblQuestion"), Label)
            lblTotalQuestions = TryCast(sender.FindControl("lblTotalQuestions"), Label)
            lblTotalMarks = TryCast(sender.FindControl("lblTotalMarks"), Label)

            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmCASEQuestions_M.aspx?MainMnu_code={0}&datamode={1}" _
                                & "&qsmid=" + Encr_decrData.Encrypt(lblQsmId.Text) _
                                & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                & "&sbmid=" + Encr_decrData.Encrypt(lblSbmbId.Text) _
                                & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                                & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                                & "&question=" + Encr_decrData.Encrypt(lblQuestion.Text) _
                                & "&totalquestion=" + Encr_decrData.Encrypt(lblTotalQuestions.Text) _
                                & "&totalmarks=" + Encr_decrData.Encrypt(lblTotalMarks.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click

        ViewState("datamode") = Encr_decrData.Encrypt("add")
        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
        Dim url As String
        url = String.Format("~\Curriculum\clmCASEQuestions_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub
End Class
