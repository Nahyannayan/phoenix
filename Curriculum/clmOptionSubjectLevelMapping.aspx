﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmOptionSubjectLevelMapping.aspx.vb" Inherits="Curriculum_clmOptionSubjectLevelMapping"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>  Option Subject Mapping
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="Table1" border="0" width="100%">
        
    </table>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" >
        <tr>
            <td align="left">
               
                    <div align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary>
                        </div>
                
            </td>
        </tr>
       <%-- <tr valign="bottom">
            <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>--%>
        <tr>
            <td valign="bottom">
                <table align="center" border="0"  cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg" colspan="4">
                            <asp:Literal ID="ltLabel" runat="server" Text="Option Group Details"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Academic Year</span>
                        </td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Grade</span>
                        </td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Subject</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>

                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" valign="Top">
                            <span class="field-label">Previous Year Subjects</span>
                        </td>
                      
                        <td align="left" colspan="3" valign="Top">
                            <asp:GridView ID="gvSubject" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="No subjects to display"
                                DataKeyNames="SBG_ID">
                                <RowStyle  />
                                <EmptyDataRowStyle CssClass="matters" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    
                                    <asp:TemplateField HeaderText="SbgId" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mapped Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Min.Level Required">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlLevel" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle   />
                                <AlternatingRowStyle  />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button
                                ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>
