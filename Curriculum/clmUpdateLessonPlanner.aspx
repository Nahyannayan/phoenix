<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmUpdateLessonPlanner.aspx.vb" Inherits="Curriculum_clmUpdateLessonPlanner"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Lesson Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            
                                        <table id="Table2" class="BlueTable" runat="server" width="100%"  >
                                            <tr align="left">
                                                <td class="matters" width="20%"><span class="field-label">Academic Year</span>
                                                </td>
                                                <td width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" class="matters" width="20%"><span class="field-label">Grade</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters"><span class="field-label">Subject</span>
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" class="matters"><span class="field-label">Term</span>
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                </tr>
                                            <tr>
                                                <td align="left" class="matters"><span class="field-label">Syllabus</span>
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlSyllabus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSyllabus_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                               <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                   
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4" style="text-align: center">
                            <asp:GridView ID="gvSyllabus" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="25" OnPageIndexChanging="gvSyllabus_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="ID">
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("SYM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ID">
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblsydID" runat="server" Text='<%# Bind("SydId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Topic">
                                        <HeaderTemplate>
                                        
                                                        <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="Topic"></asp:Label> <br />
                                                                    <asp:TextBox ID="txtOption" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnEmpid_Search_Click" />
                                                  
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubTopic" runat="server"  Text='<%# bind("SYBDESC") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="true" HeaderText="Parent Topic">
                                        <HeaderTemplate>
                                          
                                                        <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Parent Topic"></asp:Label><br />
                                                                    <asp:TextBox ID="txtOption1" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnuid_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnuid_Search_Click" />
                                                   
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentId" runat="server"   Text='<%# bind("SYBPARENTDESC") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Term">
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drTerm" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned From Date">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtFrom" runat="server" 
                                                            Text='<%# Bind("SYBSTARTDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox></td>
                                                    <td>
                                                        <asp:ImageButton
                                                            ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned To Date">
                                        <ItemStyle HorizontalAlign="left"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTo" runat="server"   AutoCompleteType="Disabled" Text='<%# Bind("SYBENDDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton
                                                            ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:CalendarExtender
                                                ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgTo" TargetControlID="txtTo">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned Total Hrs">
                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="lblTotalHrs" runat="server"   Text='<%# Bind("SYBTOTHRS") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="12">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" ValidationGroup="MAINERROR"
                                 />&nbsp;
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
