﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_clmCINAnalysis
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code1") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280039") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights


                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    BindDropdowns()
                    BindSection("")
                    GridBind()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)


    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSection(ByVal sctid As String)

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        If sctid <> "" Then
            str_query += " and sct_id=" + sctid
        End If
        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)


    End Sub
    Sub BindHeader(ByVal lstHeader As CheckBoxList, ByVal rsm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + rsm_id + "'" _
                                & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + acdid _
                              & " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSub.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)
        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function
    Function PopulatePrinted(ByVal ddlPRINTED As DropDownList, ByVal acd_id As String, ByVal grd_id As String)

        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "select RPF_ID,RPF_DESCR from rpt.REPORT_PRINTEDFOR_M where RPF_RSM_ID in(select RSM_ID from rpt.REPORT_SETUP_M where RSM_BSU_ID='" & Session("sBsuid") & "' and RSM_ACD_ID='" & acd_id & "')ORDER BY RPF_DESCR"
        Dim str_query As String = "select *from rpt.REPORT_PRINTEDFOR_M INNER JOIN RPT.REPORT_sETUP_M ON RPF_RSM_ID=RSM_ID INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID WHERE RSM_ACD_ID='" & acd_id & "' AND RSG_GRD_ID in(select ID  from oasis.dbo.fnSplitMe('" & grd_id & "','|'))"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPRINTED.DataSource = ds
        ddlPRINTED.DataTextField = "RPF_DESCR"
        ddlPRINTED.DataValueField = "RPF_ID"
        ddlPRINTED.DataBind()
        Return ddlPRINTED
    End Function
    Function PopulateTargetSetup(ByVal ddlTarSet As DropDownList, ByVal acd_id As String, ByVal grd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ' Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id   where isnull(RSD_bTARGET,0)=1 AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlTarget.SelectedValue & "'"
        Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id   where isnull(rsd_Ballsubjects,0)=1 and rsd_cssclass='textboxsmall'  and RSD_SBG_ID is null AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlTarget.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTarSet.DataSource = ds
        ddlTarSet.DataTextField = "rsd_header"
        ddlTarSet.DataValueField = "rsd_id"
        ddlTarSet.DataBind()
        Return ddlTarSet
    End Function
    Function PopulatePredictionSetup(ByVal ddlPreSet As DropDownList, ByVal acd_id As String, ByVal grd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d where isnull(RSD_bprediction,0) =1 and rsd_rsm_id in( select RSM_ID from rpt.REPORT_SETUP_M where RSM_BSU_ID='" & Session("sBsuid") & "' and RSM_ACD_ID='" & acd_id & "')ORDER BY rsd_header"
        ' Dim str_query As String = "SELECT rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id  where isnull(RSD_bprediction,0) =1 AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlPrediction.SelectedValue & "'"
        Dim str_query As String = "SELECT rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id  where isnull(rsd_Ballsubjects,0)=1 and rsd_cssclass='textboxsmall'  and RSD_SBG_ID is null AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlPrediction.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPreSet.DataSource = ds
        ddlPreSet.DataTextField = "rsd_header"
        ddlPreSet.DataValueField = "rsd_id"
        ddlPreSet.DataBind()
        Return ddlPreSet
    End Function
    Function PopulateReportCardTargetSetup(ByVal ddlRepCardSet As DropDownList, ByVal acd_id As String, ByVal grd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d where isnull(RSD_bprediction,0) =0 and isnull(RSD_bTARGET,0) =0 and isnull(RSD_bALLSUBJECTS,0) =1 and RSD_SBG_ID is null and rsd_rsm_id in( select RSM_ID from rpt.REPORT_SETUP_M where RSM_BSU_ID='" & Session("sBsuid") & "' and RSM_ACD_ID='" & acd_id & "')ORDER BY rsd_header"
        'Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id  where isnull(RSD_bprediction,0) =0 and isnull(RSD_bTARGET,0) =0 and isnull(RSD_bALLSUBJECTS,0) =1 and RSD_SBG_ID is null AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlReportCard.SelectedValue & "'"
        Dim str_query As String = "select rsd_id,rsd_header from  rpt.report_setup_d INNER JOIN RPT.REPORT_sETUP_M ON RSM_ID=RSD_RSM_ID inner join rpt.report_printedfor_m on rsm_id=rpf_rsm_id  where isnull(rsd_Ballsubjects,0)=1 and rsd_cssclass='textboxsmall' and RSD_SBG_ID is null AND RSM_aCd_ID='" & acd_id & "' AND  rpf_id='" & ddlReportCard.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlRepCardSet.DataSource = ds
        ddlRepCardSet.DataTextField = "rsd_header"
        ddlRepCardSet.DataValueField = "rsd_id"
        ddlRepCardSet.DataBind()
        Return ddlRepCardSet
    End Function
    Sub BindDropdowns()
        ddlSub = PopulateSubjects(ddlSub, ddlAcademicYear.SelectedValue.ToString)
        ddlTarget = PopulatePrinted(ddlTarget, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlPrediction = PopulatePrinted(ddlPrediction, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlReportCard = PopulatePrinted(ddlReportCard, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlTargetSetup = PopulateTargetSetup(ddlTargetSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlPredictionSetup = PopulatePredictionSetup(ddlPredictionSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlReportCardSetup = PopulateReportCardTargetSetup(ddlReportCardSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub
    Sub GridBind()
    End Sub
#End Region
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection("")
        GridBind()
        BindDropdowns()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection("")
        ddlSub = PopulateSubjects(ddlSub, ddlAcademicYear.SelectedValue.ToString)
        BindDropdowns()
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            hfbDownload.Value = 0
            CallReport()
        End If

    End Sub
    Private Sub CallReport()
        Dim param As New Hashtable
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")

        If ddlTargetSetup.SelectedValue.ToString() = "" Then
            lblerror.Text = "Select Target Selection,"
        End If
        If ddlReportCardSetup.SelectedValue.ToString() = "" Then
            lblerror.Text = lblerror.Text + " Select Report Card Selection,"
        End If
        If ddlPredictionSetup.SelectedValue.ToString() = "" Then
            lblerror.Text = lblerror.Text + " Select Prediction Selection,"
        End If
        If ddlTarget.SelectedValue.ToString() = "" Then
            lblerror.Text = lblerror.Text + " Select Target,"
        End If
        If ddlReportCard.SelectedValue.ToString() = "" Then
            lblerror.Text = lblerror.Text + " Select Report Card,"
        End If
        If ddlPrediction.SelectedValue.ToString() = "" Then
            lblerror.Text = lblerror.Text + " Select Prediction,"
        End If

        If lblerror.Text = "" Then
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@BSU_ID", Session("SBSUID"))
            Dim AC_id As String = ""
            AC_id = LTrim(RTrim(ddlAcademicYear.SelectedValue))
            param.Add("@ACD_ID", AC_id)
            param.Add("@SBJ_ID", ddlSub.SelectedValue.ToString())
            param.Add("@RPF_ID_TARGET", ddlTarget.SelectedValue.ToString())
            param.Add("@RSD_ID_TARGET", IIf(ddlTargetSetup.SelectedValue.ToString() = "", 0, ddlTargetSetup.SelectedValue.ToString()))
            param.Add("@RPF_ID_DATAENTRY", ddlReportCard.SelectedValue.ToString())
            param.Add("@RSD_ID_DATAENTRY", IIf(ddlReportCardSetup.SelectedValue.ToString() = "", 0, ddlReportCardSetup.SelectedValue.ToString()))
            param.Add("@RPF_ID_PREDICTION", ddlPrediction.SelectedValue.ToString())
            param.Add("@RSD_ID_PREDICTION", IIf(ddlPredictionSetup.SelectedValue.ToString() = "", 0, ddlPredictionSetup.SelectedValue.ToString()))
            param.Add("@SCT_ID", ddlSection.SelectedValue.ToString())
            param.Add("@GRD_ID", grade(0))
            param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
            param.Add("grade", ddlGrade.SelectedItem.Text)
            param.Add("section", ddlSection.SelectedItem.Text)
            param.Add("report", "")
            Dim rptClass As New rptClass
            'rptClass.reportPath = Server.MapPath("Reports/Rpt/rpt_RA_CIN_Analsys.rpt")
            If grade(0) = "KG1" Or grade(0) = "KG2" Or grade(0) = "1" Or grade(0) = "2" Or grade(0) = "3" Or grade(0) = "4" Or grade(0) = "5" Or grade(0) = "6" Then
                rptClass.reportPath = Server.MapPath("Reports/Rpt/rpt_RA_CIN_Analsys.rpt")
            Else
                rptClass.reportPath = Server.MapPath("Reports/Rpt/rpt_RA_CIN_Analsys_Prediction.rpt")
            End If


            rptClass.reportParameters = param
            rptClass.crDatabase = "oasis_curriculum"
            If hfbDownload.Value = 1 Then
                Dim rptDownload As New ReportDownload
                rptDownload.LoadReports(rptClass, rs)
                rptDownload = Nothing
            Else
                Session("rptClass") = rptClass
                'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        End If

    End Sub
    Protected Sub ddlTarget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTarget.SelectedIndexChanged
        ddlTargetSetup = PopulateTargetSetup(ddlTargetSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub
    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        ddlReportCardSetup = PopulateReportCardTargetSetup(ddlReportCardSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub
    Protected Sub ddlPrediction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrediction.SelectedIndexChanged
        ddlPredictionSetup = PopulatePredictionSetup(ddlPredictionSetup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class