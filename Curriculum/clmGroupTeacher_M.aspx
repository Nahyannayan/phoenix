<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGroupTeacher_M.aspx.vb" Inherits="Curriculum_clmGroupTeacher_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<script language="javascript" type="text/javascript">

 function getSubject() 
 {        
            var sFeatures;
            sFeatures="dialogWidth: 445px; ";
            sFeatures+="dialogHeight: 310px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
           // url='../Students/ShowAcademicInfo.aspx?id='+mode;
         
           url=document.getElementById("<%=hfSubjectURL.ClientID %>").value
                     
            result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false; 
            }   
             NameandCode = result.split('|');  
             document.getElementById("<%=txtSubject.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfSBG_ID.ClientID %>").value=NameandCode[1];
           
                       
                      
           }
 </script>  --%>

    <script>
        function getSubject() {

            var url;
            var NameandCode;
            url = document.getElementById("<%=hfSubjectURL.ClientID %>").value
            popup(url)
        }
        <%--function popup(url1) {

            var url = url1;


            if ($("#modal").length > 0) {
            }
            else {
                $('<div style="display:none" class="loading" id="modal"></div>').appendTo('body');
            }
            var dialog = $("#modal");
            dialog.html('<iframe style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            dialog.dialog({
                width: 445,
                height: 310,
                title: "Select Subject",
                modal: true
            });
            NameandCode = result.split('|');
            document.getElementById("<%=txtSubject.ClientID %>").value = NameandCode[0];
          document.getElementById("<%=hfSBG_ID.ClientID %>").value = NameandCode[1];

     };--%>
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>Allocate Teacher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">


                                        <table id="Table3" runat="server" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                             <tr>
                                                <td align="left">
                                                    </td>

                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">AcademicYear </span>
                                                </td>

                                                <td align="left">
                                                    <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>

                                                <td align="left">
                                                    <span class="field-label">Teacher </span>
                                                </td>

                                                <td align="left" colspan="2">
                                                    <asp:Label ID="lblteacher" runat="server" CssClass="field-value"></asp:Label></td>

                                            </tr>


                                            <tr id="trShift" runat="server">

                                                <td align="left">
                                                    <span class="field-label">Select Grade</span></td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>


                                                <td align="left">
                                                    <span class="field-label">Select Shift</span></td>

                                                <td align="left" colspan="2">
                                                    <asp:DropDownList ID="ddlShift" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>

                                            <tr id="trStream" runat="server">
                                                <td align="left">
                                                    <span class="field-label">Select Stream</span></td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>

                                                <td align="left">
                                                    <span class="field-label">Select Subject </span></td>

                                                <td align="left" colspan="2">
                                                   <%-- <asp:TextBox ID="txtSubject" runat="server" Enabled="False" MaxLength="100" TabIndex="2"></asp:TextBox><asp:ImageButton ID="imgSub" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getSubject();" TabIndex="18"></asp:ImageButton>--%>
                                                     <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="left">
                                                    <span class="field-label">Select Group</span>
                                                </td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlGroup" runat="server">
                                                    </asp:DropDownList>
                                                </td>


                                                <td align="left" id="td1">
                                                    <span class="field-label">From Date </span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                    <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                                </td>

                                                <td id="td2">
                                                    <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" /></td>

                                            </tr>


                                            <tr id="trsch">
                                                <td align="left">
                                                    <span class="field-label">Sechedule</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtSchedule" runat="server" MaxLength="100" TabIndex="2">
                                                    </asp:TextBox></td>
                                                <td align="left">
                                                    <span class="field-label">Room No</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtRoomNo" runat="server" MaxLength="100" TabIndex="2">
                                                    </asp:TextBox></td>
                                                <td>
                                                    <asp:Button ID="btnAdd1" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" /></td>
                                            </tr>
                                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table5" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No groups for this teacher" PageSize="20" Width="100%">
                                            <RowStyle />
                                            <Columns>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSgrId" runat="server" Text='<%# Bind("SGR_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Group Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Schedule">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSchedule" runat="server" Text='<%# Bind("SGS_SCHEDULE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Room No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRoomNo" runat="server" Text='<%# Bind("SGS_ROOMNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="From Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFrom" runat="server" Text='<%# Bind("FROMDATE") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>



                                                <asp:ButtonField CommandName="delete" Text="Delete" HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center">
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" ValidationGroup="groupM1" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                        </td>
                    </tr>
                    </table> 
           
           
         </td></tr>
           
                  
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                    PopupButtonID="txtFrom" TargetControlID="txtFrom" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="rffrom" runat="server" ErrorMessage="Please enter the field From Date" ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
         
         
    <asp:HiddenField ID="hfEMP_ID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="hfACD_ID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="hfSBG_ID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="hfSubjectURL" runat="server"></asp:HiddenField>
               <%-- <asp:RequiredFieldValidator ID="rfSub" runat="server" ControlToValidate="txtSubject"
                    Display="None" ErrorMessage="Please select a subject" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>--%>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

