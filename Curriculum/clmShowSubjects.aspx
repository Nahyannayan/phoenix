<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmShowSubjects.aspx.vb" Inherits="Curriculum_clmShowSubjects" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Subjects</title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />



    <script language='javascript'>

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    </script>

</head>
<body onload="listen_window();">

    <form id="form1" runat="server">


        <table align="center" cellpadding="0" cellspacing="0" width="100%">
            <tr>

                <td align="center" colspan="2">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center" cellpadding="0" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:GridView ID="gvSubjects" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                Width="100%" CssClass="table table-bordered table-row">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="sbg_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Subjects">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("SBG_DESCR") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSubject" runat="server" CommandName="Select" Text='<%# Bind("SBG_DESCR") %>'></asp:LinkButton>&nbsp;
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblID2" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                            Text="Subjects"></asp:Label>
                                                           <br />
                                                            <asp:TextBox ID="txtSubject" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSubject" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                                        OnClick="btnSubject_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="GRADE">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("GRM_DISPLAY") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblGrade" runat="server" CommandName="Select" Text='<%# Bind("GRM_DISPLAY") %>'></asp:LinkButton>&nbsp;
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblID1" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                            Text="Grade"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnGrade" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                                        OnClick="btnGrade_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Optional">
                                                        <HeaderTemplate>
                                                            Optional
                                                            <br />
                                                            <asp:DropDownList ID="ddlgvOpt" runat="server" CssClass="listbox" Width="70px" AutoPostBack="True" OnSelectedIndexChanged="ddlgvOpt_SelectedIndexChanged">
                                                                                <asp:ListItem>ALL</asp:ListItem>
                                                                                <asp:ListItem>YES</asp:ListItem>
                                                                                <asp:ListItem>NO</asp:ListItem>
                                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOpt" runat="server" Text='<%# BIND("OPT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Parent" ShowHeader="False">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblParent" runat="server" CssClass="gridheader_text" Text="Parent Subject"
                                                                           ></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtParent" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnParent" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                                        OnClick="btnParent_Click" />

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblParent" runat="server" Text='<%# BIND("SBG_PARENTS") %>' Width="214px"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="sbg_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSbgParent" runat="server" Text='<%# Bind("SBG_PARENTS_SHORT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>



                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <RowStyle CssClass="griditem" Height="25px" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 511px; height: 12px"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="width: 567px; height: 28px"
                    valign="middle">
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                            type="hidden" value="=" /><input id="h_selected_menu_3" runat="server"
                                type="hidden" value="=" />
                    &nbsp;
                </td>
            </tr>
        </table>

       
    
    
      

        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
    </form>
</body>
</html>
