<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGrade10BoardExcelImportAllHeaders.aspx.vb" Inherits="Curriculum_clmGrade10BoardExcelImportAllHeaders" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

          <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>    <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
             <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
            <div class="table-responsive">
    <table width="100%" id="tblrule" runat="server" align="center">
           
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" width="20%">
                            <span class="field-label">Subject</span></td>
                        
                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
        
        <tr><td colspan="4">&nbsp;</td></tr>
       
       
        <tr>
            <td align="left" colspan="4" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                   Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
        </tr>
                </table>
  </div>
            </div>
             </div>
</asp:Content>

