﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Curriculum_clmLevelMapping
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100395") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    '   Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                GridBind()
                If gvLevels.Rows.Count = 0 Then
                    trGrid.Visible = False
                End If
                gvLevels.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ROW_NUMBER()OVER(ORDER BY LMM_ID)-1 AS UNIQUEID,LMM_ID,LMM_ACD_ID,LML_LVL,LML_ORDER,LML_VALUE from RPT.LEVELMAPPING_M WHERE  LMM_BSU_ID='" + Session("SBSUID") + "' and LMM_ACD_ID='" & ddlAcademicYear.SelectedValue & "' ORDER BY LML_ORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvLevels.DataSource = ds
        gvLevels.DataBind()
    End Sub
    Sub ClearData()
        txtLevel.Text = ""
        txtOrder.Text = ""
        txtValue.Text = ""
    End Sub
    Sub SaveData()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_query As String = "EXEC saveRPT_LEVELMAPPING_M " _
                                    & " @LMM_BSU_ID='" + Session("sbsuid") + "'," _
                                    & " @LMM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'," _
                                    & " @LML_LVL='" + txtLevel.Text.ToString + "'," _
                                    & " @LML_ORDER=" + txtOrder.Text.ToString + "," _
                                    & " @LML_VALUE=" + txtValue.Text.ToString + "," _
                                    & " @MODE='" & UCase(ViewState("datamode")) & "'," _
                                    & " @LMM_ID='" & hfLMM_ID.Value & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            lblError.Text = "Error while saving Data"
        End Try

    End Sub
#End Region
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
        ClearData()
        trGrid.Visible = True
        If ViewState("datamode") = "add" Then
            lblError.Text = "Record Saved Sucessfully..."
        Else
            lblError.Text = "Record Updated Sucessfully..."
        End If
    End Sub
    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLevels.PageIndexChanging
        gvLevels.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLevels.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(gvLevels.Rows(index), GridViewRow)

        With selectedRow
            Dim lblID As Label = .FindControl("lblID")
            Dim lblOrder As Label = .FindControl("lblOrder")
            Dim lblLevel As Label = .FindControl("lblLevel")
            Dim lblvalue As Label = .FindControl("lblValue")
            Dim lblACDID As Label = .FindControl("lblACDID")

            If e.CommandName = "edit" Then
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.Items.FindByValue(lblACDID.Text).Selected = True
                hfLMM_ID.Value = lblID.Text
                txtLevel.Text = lblLevel.Text
                txtValue.Text = lblvalue.Text
                txtValue.Text = lblvalue.Text
                txtOrder.Text = lblOrder.Text
                ViewState("datamode") = "edit"
            ElseIf e.CommandName = "delete" Then
                Try

                
                    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                    Dim str_query As String = "EXEC saveRPT_LEVELMAPPING_M" _
                                            & " @LMM_BSU_ID='" + Session("sbsuid") + "'," _
                                            & " @LMM_ACD_ID=" + lblACDID.Text + "," _
                                            & " @LML_LVL=" + lblACDID.Text + "," _
                                            & " @LML_ORDER=" + lblACDID.Text + "," _
                                            & " @LML_VALUE=" + lblACDID.Text + "," _
                                            & " @MODE='DELETE'," _
                                            & " @LMM_ID='" & lblID.Text & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    selectedRow.Visible = False
                    GridBind()
                    lblError.Text = "Record Deleted Sucessfully"
                Catch ex As Exception
                    lblError.Text = "Error while Deleting Data !"
                End Try
            End If
        End With
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ViewState("datamode") = "add"
        GridBind()
    End Sub
    Protected Sub gvLevels_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLevels.RowDeleting

    End Sub
    Protected Sub gvLevels_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvLevels.RowEditing

    End Sub
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        If gvLevels.Rows.Count = 0 Then
            copydata()
            GridBind()
            trGrid.Visible = True
        Else
            lblError.Text = "Already Levels are existing,You can not copy Now"
        End If
    End Sub
    Private Sub copydata()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_query As String = "EXEC saveRPT_LEVELMAPPING_M " _
                                    & " @LMM_BSU_ID='" + Session("sbsuid") + "'," _
                                    & " @LMM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'," _
                                    & " @LML_LVL=''," _
                                    & " @LML_ORDER=0," _
                                    & " @LML_VALUE=0," _
                                    & " @MODE='COPY'," _
                                    & " @LMM_ID='" & hfLMM_ID.Value & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            lblError.Text = "Error while Copying Data !"
        End Try
    End Sub

End Class

