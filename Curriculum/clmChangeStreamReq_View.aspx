<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeStreamReq_View.aspx.vb" Inherits="Curriculum_clmChangeStreamReq_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">




        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}



function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    


        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label ID="lblTitle" Text="Request For Changing Stream" runat="server" ></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table width="100%" id="Table3" border="0">


        <tr>

            <td align="left" class="title">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

            </td>
        </tr>

    </table>

    <table id="tbl_AddGroup" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0" >

        <tr>
            <td  valign="top">
                <table id="studTable" runat="server" align="center" cellpadding="5" cellspacing="0"  Width="100%">
                    
                    <tr>
                        <td align="left" width="20%" >
                            <asp:Label ID="Label1" runat="server" Text="Select Academic Year " class="field-label"></asp:Label>
                            </td>
                        <td align="left" >
                            
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:Label ID="Label2" runat="server" Text="Select Grade"  class="field-label"></asp:Label>
                              </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>

                    </tr>

                    <tr>
                        <td align="left" >
                            <asp:Label ID="Label3" runat="server" Text="Shift"  class="field-label"></asp:Label>
                          </td>
                        <td align="left" >                            
                            <asp:DropDownList ID="ddlShift" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <asp:Label ID="Label4" runat="server" Text="Stream"  class="field-label"></asp:Label>
                         </td>   
                        <td align="left" >
                           <asp:DropDownList ID="ddlStream" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Student ID</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtStuNo" runat="server">
                            </asp:TextBox></td>
                         <td align="left">
                            <span class="field-label">Student Name</span>
                         </td>
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server">
                            </asp:TextBox></td>
                    </tr>


                    <tr>
                        <td align="center"  valign="bottom" colspan="6">

                            <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                Text="List" TabIndex="9"  /></td>
                    </tr>
                    <tr  id="tr1">
                        <td colspan="8" ></td>
                    </tr>




                    <tr id="tr2">

                        <td colspan="8" style="vertical-align: top" align="center">
                            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered" EmptyDataText="No Records Found"
                               PageSize="20"  AllowPaging="True" Width="100%" BorderWidth="0">
                                <RowStyle CssClass="griditem"  />
                                <Columns>

                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="-" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStmId" runat="server" Text='<%# Bind("STU_STM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student Name" HeaderStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <HeaderTemplate>Section                                                    
                                            <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged" Width="50%" >
                                                        </asp:DropDownList>
                                            
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:ButtonField>

                                </Columns>


                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>


                        </td>
                    </tr>






                </table>




            </td>
        </tr>
        <tr>
            <td valign="bottom" >
                <asp:HiddenField ID="hfGRM_ID" runat="server"></asp:HiddenField>
                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
            </td>

        </tr>
    </table>
                </div>
            </div>
            </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

</asp:Content>

