﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmProcessCBSEUpgrade
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330019") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    gvUpgrade.Attributes.Add("bordercolor", "#1b80b6")
                    BindBrownbookDate()
                    BindReportcard()
                    BindSection()
                    GrindBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

           
        End If
       
    End Sub
#Region "Private Methods"

    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + Session("current_acd_id")
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportcard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR FROM RPT.REPORT_SETUP_M AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                             & " WHERE RSM_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND RSG_GRD_ID='09'" _
                             & " AND ISNULL(RSM_bFINALREPORT,0)=1"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportcard.DataSource = ds
        ddlReportcard.DataTextField = "RSM_DESCR"
        ddlReportcard.DataValueField = "RSM_ID"
        ddlReportcard.DataBind()

        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()

    End Sub


    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM VW_SECTION_M WHERE SCT_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                & " AND SCT_GRD_ID='09' AND SCT_DESCR <>'TEMP'"

        If Session("CurrSuperUser") <> "Y" Then
            str_query += " AND SCT_EMP_ID=" + Session("EMPLOYEE_ID")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Sub ProcessCBSEUpgrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [RPT].[PROCESS_CBSE_GRADEUPGRADE]" _
                                & " @ACD_ID=" + Session("CURRENT_ACD_ID") + "," _
                                & " @RSM_ID=" + ddlReportcard.SelectedValue.ToString + "," _
                                & " @RPF_ID=" + ddlReportSchedule.SelectedValue.ToString + "," _
                                & " @SCT_IDS='" + ddlSection.SelectedValue.ToString + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", Session("CURRENT_ACD_ID"))
        param.Add("@RPF_ID", ddlReportSchedule.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Curriculum/Reports/Rpt/rptCBSEUpgradeDetails.rpt")
        End With
        Session("rptClassPopup") = rptClass
        ResponseHelper.Redirect("~/Reports/ASPX Report/rptReportViewer_Popup.aspx", "_blank", "")
    End Sub

    Sub GrindBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,' ')+ISNULL(STU_LASTNAME,'') AS STU_NAME " _
                               & " FROM STUDENT_M AS A WHERE STU_SCT_ID=" + ddlSection.SelectedValue.ToString _
                               & " AND STU_CURRSTATUS<>'CN' AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                               & " ORDER BY ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,' ')+ISNULL(STU_LASTNAME,'')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvUpgrade.DataSource = ds
        gvUpgrade.DataBind()
    End Sub

    Sub BindUpgradeDetails(ByVal dlUpgrade As DataList, ByVal stu_id As String, ByVal lblAverage As Label)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_DESCR,FGU_GRADE_ACTUAL,FGU_GRADE_UPGRADE,ISNULL(FGU_COSCHOLASTICAVG,0) FGU_COSCHOLASTICAVG FROM SUBJECTS_GRADE_S " _
                                & " INNER JOIN RPT.FINAL_CBSE_UPGRADE ON SBG_ID=FGU_SBG_ID " _
                                & " WHERE FGU_RPF_ID=" + ddlReportSchedule.SelectedValue.ToString _
                                & " AND FGU_STU_ID=" + stu_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlUpgrade.DataSource = ds
        dlUpgrade.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            lblAverage.Text = ds.Tables(0).Rows(0).Item("FGU_COSCHOLASTICAVG").ToString.Replace(".00", "")
        End If
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim lblStuId As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvUpgrade.Rows.Count - 1
            With gvUpgrade.Rows(i)
                lblStuId = .FindControl("lblStuId")
                chkSelect = .FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    str_query = "exec CBSE_GRADEUPGRADE_APPROVE " _
                    & " @ACD_ID=" + Session("CURRENT_ACD_ID") + "," _
                    & " @RPF_ID=" + ddlReportSchedule.SelectedValue.ToString + "," _
                    & "@STU_ID=" + lblStuId.Text
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next
    End Sub

#End Region

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        ProcessCBSEUpgrade()
        GrindBind()
    End Sub

    Protected Sub gvUpgrade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUpgrade.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label = e.Row.FindControl("lblStuId")
            Dim dlUpgrade As DataList = e.Row.FindControl("dlUpgrade")
            Dim lblAverage As Label = e.Row.FindControl("lblAverage")

            BindUpgradeDetails(dlUpgrade, lblStuId.Text, lblAverage)
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CallReport()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        GrindBind()
    End Sub
End Class
