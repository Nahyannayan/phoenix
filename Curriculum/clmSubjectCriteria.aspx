<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmSubjectCriteria.aspx.vb" Inherits="Curriculum_clmSubjectCriteria"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Subject Specific Criteria
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="height: 1px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnAddCriteria">
                                <table align="center" cellpadding="5"
                                    cellspacing="0" id="tb1" runat="server" width="100%">

                                    <tr>
                                        <td align="left"><span class="field-label">Select Academic Year</span></td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;</td>
                                        <td align="left" ><span class="field-label">Select Report Card</span></td>
                                        
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Select Grade</span></td>
                                       
                                        <td align="left">
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" ><span class="field-label">Select Subject</span></td>
                                        
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="7"></td>
                                    </tr>
                                    <tr id="trCat" runat="server">
                                        <td align="left"><span class="field-label">Subject Category</span></td>
                                       
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCategory" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trSkills" runat="server">
                                        <td align="left"><span class="field-label">Skills</span></td>
                                       
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSkills" runat="server">
                                                <asp:ListItem Text="K/U" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="HOT" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="APPS" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="COMM" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Subject Criteria short name</span></td>
                                        
                                        <td align="left">
                                            <asp:TextBox ID="txtShort" runat="server"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                            <asp:Label ID="lblText" runat="server" Text="Subject Criteria Description" class="field-label"></asp:Label>
                                        </td>
                                        
                                        <td align="left" colspan="4">
                                            <asp:TextBox SkinID="MultiText" ID="txtDetail" runat="server"
                                                TextMode="MultiLine"></asp:TextBox>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Select Default Values</span></td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddlDefaults" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" colspan="1"><span class="field-label">Display Order</span></td>
                                        
                                        <td align="left" colspan="1">
                                            <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnAddCriteria" runat="server" CssClass="button" TabIndex="7" Text="Add Criteria"
                                                ValidationGroup="groupM1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="7"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="7">

                                            <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                CssClass="table table-bordered table-row" EmptyDataText="No record added."
                                                PageSize="20" Width="100%">
                                                <RowStyle CssClass="griditem" />
                                                <EmptyDataRowStyle />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRdmId" runat="server" Text='<%# Bind("RDM_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSbtId" runat="server" Text='<%# Bind("SBT_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject Criteria Shortname">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCriteriaShort" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject Criteria" ItemStyle-Wrap="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCriteria" runat="server" Width="300px" Text='<%# Bind("RSD_SUB_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("SBT_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Default Values">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlDefaultvalues" runat="server">
                                                            </asp:DropDownList>
                                                            </ItemStyle>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Display Order">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("RSD_DISPLAYORDER") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Skill" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSkills" runat="server" Text='<%# Bind("RSD_SUB_SKILLS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="edit">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:ButtonField>
                                                    <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="delete">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:ButtonField>
                                                    <asp:TemplateField HeaderText="Add Details">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblEdit" runat="server" Text="Add Details" Enabled='<%# Bind("bENABLE") %>'
                                                                OnClick="lblEdit_Click" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <SelectedRowStyle CssClass="Green" Wrap="true" />
                                                <HeaderStyle CssClass="gridheader_pop" Wrap="true" />
                                                <EditRowStyle Wrap="true" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="true" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td align="center" colspan="8">
                            <asp:Panel ID="pnlPopup" runat="server" BackColor="white" CssClass="modalPopup" Width="100%">
                                <asp:UpdatePanel ID="uppnlPopup" runat="server">
                                    <ContentTemplate>
                                        <table border="1" class="BlueTableView" id="Table2" cellspacing="2" cellpadding="2"
                                            width="100%">
                                            <tbody>
                                                <tr border="1" class="subheader_img">
                                                    <td colspan="6">Programme of Enquiry
                                                    </td>
                                                </tr>
                                                <tr align="left">
                                                    <td><span class="field-label">Select Report Scehdule</span></td>
                                                    
                                                    <td colspan="4">
                                                        <asp:DropDownList ID="ddlPrintedFor" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr align="left">
                                                    <td><span class="field-label">Description 1</span></td>
                                                    
                                                    <td style="height: 18px">
                                                        <asp:TextBox ID="txtDetail1" runat="server" SkinID="MultiText"
                                                            TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                    <td><span class="field-label">Description 2</span></td>
                                                   
                                                    <td>
                                                        <asp:TextBox ID="txtDetail2" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><span class="field-label">Display Order</span></td>
                                                   
                                                    <td align="left">
                                                        <asp:TextBox ID="txtPOIOrder" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                    </td>
                                                    <td align="right" colspan="3">
                                                        <asp:Button ID="btnAddDetail" runat="server" CssClass="button" TabIndex="7" Text="Add Detail" />
                                                        <asp:Button ID="btnUpdateDetail" runat="server" CssClass="button" Text="Update Detail"
                                                            Visible="False" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="6">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="6">
                                                        <asp:GridView ID="gvPOI" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                                            EmptyDataText="No Details Added" OnPageIndexChanging="gvPOI_PageIndexChanging"
                                                            CssClass="table table-bordered table-row">
                                                            <RowStyle CssClass="griditem" />
                                                            <EmptyDataRowStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="RPF_DESCR" HeaderText="Report Schedule">
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RPD_DESCR1" HeaderText="Description 1" HtmlEncode="False">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RPD_DESCR2" HeaderText="Description 2">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RPD_DISPLAYORDER" HeaderText="Display Order">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                                    <EditItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                                                            Text="Update"></asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                            Text="Cancel"></asp:LinkButton>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lblEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                                            Text="Edit"></asp:LinkButton>
                                                                        &nbsp;
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lblDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                                            Text="Delete"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="RPF_ID" HeaderText="RPF_ID" Visible="False"></asp:BoundField>
                                                                <asp:BoundField DataField="RSD_ID" HeaderText="RSD_ID" Visible="False"></asp:BoundField>
                                                                <asp:BoundField DataField="SBG_ID" HeaderText="SBG_ID" Visible="False"></asp:BoundField>
                                                            </Columns>
                                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                                            <HeaderStyle CssClass="gridheader_new" />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="6">
                                                        <asp:Button ID="btnStudSave" runat="server" CausesValidation="False" CssClass="button"
                                                            TabIndex="7" Text="Save" ValidationGroup="groupM1" />
                                                        <asp:Button ID="btnClose" runat="server" CausesValidation="false" CssClass="button"
                                                            Text="Close" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" BackgroundCssClass="modalBackground"
                                DropShadow="true" PopupControlID="pnlPopup" CancelControlID="btnClose" RepositionMode="RepositionOnWindowResizeAndScroll"
                                TargetControlID="lblError">
                            </ajaxToolkit:ModalPopupExtender>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="8" style="height: 31px">
                            <asp:Panel ID="pnlDefault" runat="server" BackColor="white" CssClass="modalPopup" Width="100%">
                                <asp:UpdatePanel ID="updtDefault" runat="server">
                                    <ContentTemplate>
                                        <table id="Table1" cellspacing="2" cellpadding="2"
                                            width="100%">
                                            <tbody>
                                                <tr border="1" class="subheader_img">
                                                    <td colspan="6">Default Details
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="6">
                                                        <asp:GridView ID="gvDefaults" runat="server" AutoGenerateColumns="False"
                                                            EmptyDataText="No Details Added" AllowPaging="false"
                                                            CssClass="table table-bordered table-row">
                                                            <RowStyle CssClass="griditem" Height="25px" />
                                                            <EmptyDataRowStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Grade">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("RSP_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtDescr" Width="400px" TextMode="MultiLine" SkinID="MultiText" runat="server" Text='<%# Bind("RSP_TEXT") %>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Description" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("RSP_RSD_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                                            <HeaderStyle CssClass="gridheader_new" />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="6">
                                                        <asp:Button ID="btnSaveDefault" runat="server" CausesValidation="False" CssClass="button"
                                                            TabIndex="7" Text="Save" ValidationGroup="groupM1" />
                                                        <asp:Button ID="btnCloseDefault" runat="server" CausesValidation="false" CssClass="button"
                                                            Text="Close" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>

                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                    TabIndex="7" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:HiddenField ID="hfRSD_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" style="height: 45px">
                            <asp:HiddenField ID="hfRandom" runat="server" Value="1000000000"></asp:HiddenField>
                            <asp:RegularExpressionValidator ID="regOrder" runat="server" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="Display order bust be a number" ValidationExpression="^\d+$"
                                ValidationGroup="groupM1">
                            </asp:RegularExpressionValidator>&nbsp;
                <asp:RequiredFieldValidator ID="rdShort" runat="server" ControlToValidate="txtShort"
                    Display="None" ErrorMessage="Please enter data in the field subject criteria short name"
                    ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>
                            &nbsp;&nbsp; &nbsp;
                <asp:RequiredFieldValidator ID="rfOrder" runat="server" ControlToValidate="txtOrder"
                    Display="None" ErrorMessage="Please enter data in the field  display order" ForeColor="RosyBrown"
                    ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>

                            <ajaxToolkit:ModalPopupExtender ID="MDefault" runat="server" BackgroundCssClass="modalBackground"
                                DropShadow="true" PopupControlID="pnlDefault" CancelControlID="btnClose" RepositionMode="RepositionOnWindowResizeAndScroll"
                                TargetControlID="lblError">
                            </ajaxToolkit:ModalPopupExtender>
                        </td>

                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
