Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Skills_Setting_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        trlock.Visible = False
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "C100017" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    '  GridBind()
                    BindGrade()
                    BindSubjects(ddlGrade.SelectedItem.Value)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub


    Protected Sub gvSkill_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSkill.PageIndexChanging
        Try
            gvSkill.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    'Protected Sub gvSkill_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSkill.RowCommand
    '    Try

    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim selectedRow As GridViewRow = DirectCast(gvSkill.Rows(index), GridViewRow)
    '        Dim skl_id As New Label
    '        Dim skl_Name As New Label
    '        Dim skl_desc As New TextBox
    '        Dim url As String
    '        'define the datamode to view if view is clicked
    '        ViewState("datamode") = "view"
    '        'Encrypt the data that needs to be send through Query String


    '        If e.CommandName = "UpdateRow" Then
    '            skl_id.Text = CType(selectedRow.Cells(0).FindControl("lblSkill_id"), Label).Text
    '            skl_Name.Text = CType(selectedRow.Cells(1).FindControl("lblSkill"), Label).Text
    '            skl_desc.Text = CType(selectedRow.Cells(2).FindControl("txtDescription"), TextBox).Text
    '            If skl_desc.Text = "" Then
    '                lblError.Text = "Please enter a description "
    '            Else
    '                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '                Dim str_query As String
    '                If skl_id Is Nothing Or skl_id.Text = "" Then
    '                    str_query = "exec [SKIL].SaveSkillMaster " & ddlAcademicYear.SelectedItem.Value & "," & ddlGrade.SelectedItem.Value & "," & ddlSubject.SelectedItem.Value & " ,'" & skl_Name.Text & "' ,'" & skl_desc.Text & "','" & skl_id.Text & "','Add'," & chkLock.Checked
    '                Else

    '                    str_query = "exec [SKIL].SaveSkillMaster " & ddlAcademicYear.SelectedItem.Value & "," & ddlGrade.SelectedItem.Value & "," & ddlSubject.SelectedItem.Value & " ,'" & skl_Name.Text & "' ,'" & skl_desc.Text & "','" & skl_id.Text & "','" & e.CommandName & "'," & chkLock.Checked
    '                End If

    '                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    '                lblError.Text = "Updated Successfully"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try

    'End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ViewState("SelectedYear") = ddlAcademicYear.SelectedValue
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        str_query = "EXEC [SKIL].[LoadSkillMaster] '" + ddlAcademicYear.SelectedItem.Value + "','" + ddlGrade.SelectedItem.Value + "','" + ddlSubject.SelectedItem.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = ds.Tables(0)
        If dt.Rows.Count >= 10 Then
            trlock.Visible = True
        End If
        gvSkill.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSkill.DataBind()
            Dim columnCount As Integer = gvSkill.Rows(0).Cells.Count
            gvSkill.Rows(0).Cells.Clear()
            gvSkill.Rows(0).Cells.Add(New TableCell)
            gvSkill.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSkill.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSkill.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSkill.DataBind()
        End If
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M  " _
                                  & " INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID WHERE" _
                                  & "  GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("Select", 0))
    End Sub
    Sub BindSubjects(grades As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE  " _
                                & " SBG_GRD_ID  IN ('" + grades + "')" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_PARENT_ID=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        ddlSubject.Items.Insert(0, New ListItem("Select", 0))
    End Sub
#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindSubjects(ddlGrade.SelectedItem.Value)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)
        GridBind()
    End Sub

    Sub SaveSKills()
        Dim lblSkillName As Label
        Dim txtSkillDesc As TextBox
        Dim skl_id As Label
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        For i = 0 To gvSkill.Rows.Count - 1
            lblSkillName = gvSkill.Rows(i).FindControl("lblSkill")
            txtSkillDesc = gvSkill.Rows(i).FindControl("txtDescription")
            skl_id = gvSkill.Rows(i).FindControl("lblSkill_id")
            str_query = "exec [SKIL].SaveSkillMaster " & ddlAcademicYear.SelectedItem.Value & "," & _
                ddlGrade.SelectedItem.Value & "," & _
                ddlSubject.SelectedItem.Value & " ,'" & _
                lblSkillName.Text & "' ,'" & _
                txtSkillDesc.Text & "','" & _
                skl_id.Text & "'," & _
                chkLock.Checked
            Try
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                lblError.Text = "Updated Successfully"
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try
        Next

    End Sub

    Protected Sub btnSaveSkill_Click(sender As Object, e As EventArgs)
        SaveSKills()
    End Sub
End Class
