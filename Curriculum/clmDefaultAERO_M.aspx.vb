Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Imports Telerik.Web.UI
Imports System.Drawing

Partial Class Curriculum_clmDefaultAERO_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100319") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    Session("dtDefaultValues") = SetDataTable()
                    If ViewState("datamode") = "add" Then
                        hfRDM_ID.Value = 0
                    Else
                        hfRDM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rdmid").Replace(" ", "+"))
                        ddlDescriptorType.SelectedValue = Encr_decrData.Decrypt(Request.QueryString("default").Replace(" ", "+"))
                        BindDefaultValues()
                    End If
                    Dim sel_grade = ""
                    If Request.QueryString("grades") IsNot Nothing Then

                        sel_grade = Encr_decrData.Decrypt(Request.QueryString("grades").Replace(" ", "+"))
                    End If
                    If Request.QueryString("acd_id") IsNot Nothing Then

                        hf_acd_id.Value = Encr_decrData.Decrypt(Request.QueryString("acd_id").Replace(" ", "+"))
                    Else
                        hf_acd_id.Value = Session("Current_ACD_ID")
                    End If
                    BindGrade(sel_grade)
                    BindSubject()
                    BindTerm()
                    BindTermMaster()
                    Bind_SubTerm()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DAD_CODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DAD_DESCR"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DAD_ORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DAD_COLOR_CODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DAD_VALUE"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub BindDefaultValues()
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        Dim reader As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(DAD_ID) FROM DBO.DEFAULTAERO_D WHERE " _
                                & " DAD_DAM_ID='" + hfRDM_ID.Value + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            gvDefault.Visible = True
            str_query = "SELECT DAD_CODE,DAD_DESCR,DAD_ORDER,DAD_COLOR_CODE,ISNULL(DAD_VALUE,'') as DAD_VALUE FROM DBO.DEFAULTAERO_D WHERE " _
                        & " DAD_DAM_ID='" + hfRDM_ID.Value + "'" _
                        & " AND DAD_TYPE='" + ddlDescriptorType.SelectedValue + "'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = dt.NewRow
                dr.Item(0) = reader.GetString(0)
                dr.Item(1) = reader.GetString(1)
                dr.Item(2) = reader.GetValue(2)
                dr.Item(3) = reader.GetString(3)
                dr.Item(4) = "add"
                dr.Item(5) = reader.GetString(4)
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvDefault.DataSource = dt
            gvDefault.DataBind()
            Session("dtDefaultValues") = dt
        Else
            Session("dtDefaultValues") = SetDataTable()
            gvDefault.DataSource = Session("dtDefaultValues")
            gvDefault.DataBind()
            gvDefault.Visible = False
        End If

    End Sub

    Sub AddRows()
        gvDefault.Visible = True
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtDefaultValues")
        Dim keys As Object()
        ReDim keys(0)
        keys(0) = txtValue.Text.Trim
        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This value is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = ""
        dr.Item(1) = txtValue.Text
        dr.Item(2) = "" 'System.Drawing.ColorTranslator.ToHtml(RadColorPicker1.SelectedColor)
        dr.Item(3) = ""
        dr.Item(4) = "add"
        dr.Item(5) = ""
        dt.Rows.Add(dr)
        Session("dtDefaultValues") = dt
        GridBind(dt)
    End Sub

    Sub EditRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = SetDataTable()
        Dim i As Integer
        Dim txtCode As TextBox
        Dim txtDefault As TextBox
        Dim txtOrder As TextBox
        Dim txtValue As TextBox
        Dim RadColorPicker1 As RadColorPicker
        Dim keys As Object()
        ReDim keys(0)
        Dim row As DataRow
        For i = 0 To gvDefault.Rows.Count - 1
            txtCode = gvDefault.Rows(i).FindControl("txtCode")
            txtDefault = gvDefault.Rows(i).FindControl("txtDefault")
            txtOrder = gvDefault.Rows(i).FindControl("txtOrder")
            RadColorPicker1 = gvDefault.Rows(i).FindControl("RadColorPicker1")
            txtValue = gvDefault.Rows(i).FindControl("txtValue")
            keys(0) = txtDefault.Text.Trim
            row = dt.Rows.Find(keys)
            If row Is Nothing Then
                dr = dt.NewRow
                dr.Item(0) = txtCode.Text
                dr.Item(1) = txtDefault.Text
                dr.Item(2) = Val(txtOrder.Text)
                dr.Item(3) = System.Drawing.ColorTranslator.ToHtml(RadColorPicker1.SelectedColor)
                dr.Item(4) = "add"
                dr.Item(5) = txtValue.Text
                dt.Rows.Add(dr)
            End If
        Next
        Session("dtDefaultValues") = dt
        GridBind(dt)

    End Sub


    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(3) <> "delete" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvDefault.DataSource = dtTemp
        gvDefault.DataBind()
    End Sub

    Sub SaveData()

        Dim transaction As SqlTransaction
        Dim str_query As String


        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_query = "EXEC DBO.saveDEFAULTAERO_M " _
                          & hfRDM_ID.Value + "," _
                          & "'" + ddlDescriptorType.SelectedValue + "'," _
                          & "'" + GetSelectedGrade() + "'," _
                          & "'" + Session("SBSUID") + "'," _
                          & "'" + hf_acd_id.Value + "'"

                hfRDM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                Dim dr As DataRow

                For Each dr In dt.Rows
                    With dr
                        If .Item(1) <> "delete" Then
                            str_query = "exec  DBO.saveDEFAULTAERO_D " _
                                     & hfRDM_ID.Value + "," _
                                     & "'" + .Item(0) + "'," _
                                     & "'" + .Item(1) + "'," _
                                     & "'" + .Item(2) + "'," _
                                     & "'" + .Item(3) + "'," _
                                     & "'" + .Item(5) + "'," _
                                     & "'" + ddlDescriptorType.SelectedValue + "'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If
                    End With
                Next

                transaction.Commit()
                lblError.Text = "Record saved successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ' lblError.Text = "Record could not be Saved"
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved.Possibly duplicate grade."
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        End Using
    End Sub
#End Region

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        AddRows()
        txtValue.Text = ""
    End Sub

    Protected Sub gvDefault_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDefault.RowCommand
        Try

            If e.CommandName = "Delete" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvDefault.Rows(index), GridViewRow)
                Dim txtDefault As TextBox
                txtDefault = selectedRow.FindControl("txtDefault")
                Dim keys As Object()
                ReDim keys(0)
                keys(0) = txtDefault.Text
                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                index = dt.Rows.IndexOf(dt.Rows.Find(keys))
                dt.Rows(index).Item(4) = "delete"
                Session("dtDefaultValues") = dt
                GridBind(dt)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvDefault_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDefault.RowDeleting

    End Sub

    Protected Sub gvDefault_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDefault.RowEditing

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        EditRows()
        SaveData()
        BindDefaultValues()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Function GetSelectedGrade() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = rcb_Grade.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str
    End Function

    Function GetSelectedGradeForQuery() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = rcb_Grade.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += "'" + item.Value + "'"
                If str <> "" Then
                    str += ","
                End If
            Next
        End If
        Return str.TrimEnd(",")
    End Function
    Sub BindGrade(ByVal SelectedGrade As String)
        rcb_Grade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "select DISTINCT GRM_GRD_ID as CODE,GRM_DISPLAY as [NAME]" _
                 & " from GRADE_BSU_M with (nolock) where grm_acd_id=" + hf_acd_id.Value.ToString() _
                 & " ORDER BY NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        rcb_Grade.DataSource = ds
        rcb_Grade.DataTextField = "NAME"
        rcb_Grade.DataValueField = "CODE"
        rcb_Grade.DataBind()
        If SelectedGrade <> "" Then
            Dim grd As String() = SelectedGrade.TrimEnd("|").Split("|")
            For i = 0 To grd.Length - 1
                For Each item As RadComboBoxItem In rcb_Grade.Items
                    If grd(i) = item.Value Then
                        item.Checked = True
                    End If
                Next
            Next
        End If
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_SBM_ID,SBM_DESCR FROM SUBJECTS_GRADE_S  INNER JOIN SUBJECT_M ON SBG_SBM_ID=SBM_ID" _
                               & " WHERE SBG_ACD_ID=" + hf_acd_id.Value _
                               & " AND SBG_GRD_ID IN(" + IIf(GetSelectedGradeForQuery() = "", "''", GetSelectedGradeForQuery()) + ")" _
                               & " ORDER BY SBM_DESCR"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
        'ddlSubject.Items.Insert(0, New ListItem("Select Subject ", "0"))
    End Sub
    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION FROM [dbo].[term_sub_master] " _
                                & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
                                & " AND TRM_ACD_ID =" + hf_acd_id.Value _
                                & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TSM_DESCRIPTION"
        ddlTerm.DataValueField = "TSM_ID"
        ddlTerm.DataBind()
        ddlTerm.Items.Insert(0, New ListItem("Select Term", "0"))
    End Sub

    Protected Sub btnMarkAdd_Click(sender As Object, e As EventArgs)
        Try
            Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            If btnMarkAdd.Text <> "Update Descriptor" Then
                If (collection.Count <> 0) Then
                    For Each item As RadComboBoxItem In collection

                        Dim str_query As String = " EXEC [SYL].[SAVE_AERO_EXPECTATION_EOY]" _
                                                  & " @AEE_ID =0," _
                                                  & " @AEE_DAM_ID =" + hfRDM_ID.Value + "," _
                                                  & " @AEE_DESCR ='" + txtDescriptors.Text + "'," _
                                                  & " @AEE_COLOR_CODE =" + System.Drawing.ColorTranslator.ToHtml(Expectation_Color_picker.SelectedColor) + "," _
                                                  & " @AEE_MARK_FROM ='" + txtFromMarks.Text + "'," _
                                                  & " @AEE_MARK_TO ='" + txtToMarks.Text + "'," _
                                                  & " @AEE_SBG_ID ='" + item.Value + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Next
                End If
                lblError.Text = "Descriptor Added Successfully"
                Bind_EOY()
            Else
                If (collection.Count <> 0) Then
                    For Each item As RadComboBoxItem In collection

                        'Dim str_query As String = " EXEC [DBO].[UPDATE_TERM_SUB_MASTER]" _
                        '                  & " @TSM_ID =" + ViewState("Subtermid") + "," _
                        '                  & " @TSM_TRM_ID =" + ddlTermMaster.SelectedValue + "," _
                        '                  & " @TSM_DESCRIPTION ='" + txtSubTerm.Text + "'," _
                        '                  & " @TSM_DISPLAY_ORDER =" + txt_term_order.Text



                        Dim str_query As String = " EXEC [SYL].[UPDATE_AERO_EXPECTATION_EOY]" _
                                                   & " @AEE_ID =" + ViewState("EOYid") + "," _
                                                  & " @AEE_DAM_ID =" + hfRDM_ID.Value + "," _
                                                  & " @AEE_DESCR ='" + txtDescriptors.Text + "'," _
                                                  & " @AEE_COLOR_CODE =" + System.Drawing.ColorTranslator.ToHtml(Expectation_Color_picker.SelectedColor) + "," _
                                                  & " @AEE_MARK_FROM ='" + txtFromMarks.Text + "'," _
                                                  & " @AEE_MARK_TO ='" + txtToMarks.Text + "'," _
                                                  & " @AEE_SBG_ID ='" + item.Value + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Next
                End If
                'Expectation_Color_picker.SelectedColor =
                txtDescriptors.Text = ""
                txtFromMarks.Text = ""
                txtToMarks.Text = ""
                lblError.Text = "Descriptor Updated Successfully"
                Bind_EOY()
            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnTermkAdd_Click(sender As Object, e As EventArgs)
        Try
            Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            If btnTermkAdd.Text <> "Update Expectation" Then
                If (collection.Count <> 0) Then
                    For Each item As RadComboBoxItem In collection

                        Dim str_query As String = " EXEC [SYL].[SAVE_AERO_EXPECTATION_TERM]" _
                                                  & " @AET_ID =0," _
                                                  & " @AET_DAM_ID =" + hfRDM_ID.Value + "," _
                                                  & " @AET_TSM_ID ='" + ddlTerm.SelectedValue + "'," _
                                                  & " @AET_EXPECTATION_PERC =" + txtTermExpectation.Text + "," _
                                                  & " @AET_SBG_ID ='" + item.Value + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Next
                End If
                lblError.Text = "Expectation Percentage Added Successfully"
                Bind_Term()
            Else
                If (collection.Count <> 0) Then
                    For Each item As RadComboBoxItem In collection

                        Dim str_query As String = " EXEC [SYL].[UPDATE_AERO_EXPECTATION_TERM]" _
                                                  & " @AET_ID =" + ViewState("Termid") + "," _
                                                  & " @AET_DAM_ID =" + hfRDM_ID.Value + "," _
                                                  & " @AET_TSM_ID ='" + ddlTerm.SelectedValue + "'," _
                                                  & " @AET_EXPECTATION_PERC =" + txtTermExpectation.Text + "," _
                                                  & " @AET_SBG_ID ='" + item.Value + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Next
                End If
                lblError.Text = "Expectation Percentage Updated Successfully"
                Bind_Term()
            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gv_EOY_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gv_EOY.DataKeys(e.RowIndex).Value)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = " EXEC [SYL].[SAVE_AERO_EXPECTATION_EOY]" _
                                      & " @AEE_ID =" + COND_ID.ToString + "," _
                                      & " @AEE_DAM_ID =0," _
                                      & " @AEE_DESCR =''," _
                                      & " @AEE_COLOR_CODE =''," _
                                      & " @AEE_MARK_FROM ='0'," _
                                      & " @AEE_MARK_TO ='0'," _
                                      & " @AEE_SBG_ID ='0'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblError.Text = "Descriptor Removed Successfully"
            Bind_EOY()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gv_Term_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gv_Term.DataKeys(e.RowIndex).Value)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = " EXEC [SYL].[SAVE_AERO_EXPECTATION_TERM]" _
                                      & " @AET_ID =" + COND_ID.ToString + "," _
                                      & " @AET_DAM_ID =0," _
                                      & " @AET_TSM_ID ='0'," _
                                      & " @AET_EXPECTATION_PERC =0," _
                                      & " @AET_SBG_ID ='0'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblError.Text = "Expectation Percentage Removed Successfully"
            Bind_Term()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)
        Bind_EOY()
        Bind_Term()
    End Sub
    Sub Bind_EOY()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT * FROM AERO_EXPECTATION_EOY INNER JOIN DEFAULTAERO_M ON AEE_DAM_ID = DAM_ID " _
        & " INNER JOIN SUBJECT_M on SBM_ID=AEE_SBG_ID WHERE AEE_SBG_ID IN (" + GetSelectedSubjectForQuery() + ")" _
        & " AND AEE_DAM_ID =" + hfRDM_ID.Value
        '& " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gv_EOY.DataSource = ds
        gv_EOY.DataBind()
    End Sub
    Sub Bind_Term()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT AET_ID,TSM_DESCRIPTION,AET_EXPECTATION_PERC from AERO_EXPECTATION_TERM INNER JOIN  OASIS..TERM_SUB_MASTER ON AET_TSM_ID=TSM_ID " _
        & " WHERE AET_SBG_ID IN (" + GetSelectedSubjectForQuery() + ")" _
        & " AND AET_DAM_ID =" + hfRDM_ID.Value
        '& " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gv_Term.DataSource = ds
        gv_Term.DataBind()
    End Sub

    Sub BindTermMaster()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        ddlTermMaster.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), hf_acd_id.Value)
        ddlTermMaster.DataTextField = "TRM_DESCRIPTION"
        ddlTermMaster.DataValueField = "TRM_ID"
        ddlTermMaster.DataBind()
    End Sub

    Sub Bind_SubTerm()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION,TRM_DESCRIPTION,TSM_DISPLAY_ORDER,TSM_BBLOCKED FROM [dbo].[term_sub_master] " _
                                & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
                                & " AND TRM_ACD_ID =" + hf_acd_id.Value _
                                & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gv_SubTerm.DataSource = ds
        gv_SubTerm.DataBind()
    End Sub

    Protected Sub btnSubTermAdd_Click(sender As Object, e As EventArgs)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            If btnSubTermAdd.Text <> "Update" Then

                Dim str_query As String = " EXEC [DBO].[SAVE_TERM_SUB_MASTER]" _
                                          & " @TSM_ID =0," _
                                          & " @TSM_TRM_ID =" + ddlTermMaster.SelectedValue + "," _
                                          & " @TSM_DESCRIPTION ='" + txtSubTerm.Text + "'," _
                                          & " @TSM_DISPLAY_ORDER =" + txt_term_order.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                lblError.Text = "Assessment Term Added Successfully"
                Bind_SubTerm()
            Else



                Dim str_query As String = " EXEC [DBO].[UPDATE_TERM_SUB_MASTER]" _
                                          & " @TSM_ID =" + ViewState("Subtermid") + "," _
                                          & " @TSM_TRM_ID =" + ddlTermMaster.SelectedValue + "," _
                                          & " @TSM_DESCRIPTION ='" + txtSubTerm.Text + "'," _
                                          & " @TSM_DISPLAY_ORDER =" + txt_term_order.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                lblError.Text = "Assessment Term Updated Successfully"
                Bind_SubTerm()
                txtSubTerm.Text = ""
                txt_term_order.Text = ""

            End If




        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gv_SubTerm_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gv_SubTerm.DataKeys(e.RowIndex).Value)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = " EXEC [DBO].[SAVE_TERM_SUB_MASTER]" _
                                      & " @TSM_ID ='" + COND_ID.ToString + "'," _
                                      & " @TSM_TRM_ID =0," _
                                      & " @TSM_DESCRIPTION =''," _
                                      & " @TSM_DISPLAY_ORDER =0"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblError.Text = "Assessment Term Deleted Successfully"
            Bind_SubTerm()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub rcb_Grade_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        BindSubject()
    End Sub

    Protected Sub ddlDescriptorType_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindDefaultValues()
    End Sub
    Function GetSelectedSubject() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str
    End Function
    Function GetSelectedSubjectForQuery() As String
        Dim str As String = "''"
        Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems

        If (collection.Count <> 0) Then
            str = ""
            For Each item As RadComboBoxItem In collection
                str += "'" + item.Value + "'"
                If str <> "" Then
                    str += ","
                End If
            Next
        End If
        Return str.TrimEnd(",")
    End Function

    Protected Sub lnkBtnEdit_Click(sender As Object, e As EventArgs)
        Dim DT As DataTable = New DataTable
        Dim DS As DataSet = New DataSet
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                       GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        Dim Subterm_id As Integer
        Subterm_id = gv_SubTerm.DataKeys(gvRow.RowIndex)("TSM_ID")
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@tsm_id", Subterm_id, SqlDbType.BigInt)
        DS = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetSubtermDatabyId", Param)
        DT = DS.Tables(0)
        ddlTermMaster.SelectedValue = DT.Rows(0).Item("TRM_ID")
        txt_term_order.Text = DT.Rows(0).Item("TSM_DISPLAY_ORDER")
        txtSubTerm.Text = DT.Rows(0).Item("TSM_DESCRIPTION")
        ViewState("Subtermid") = Convert.ToString(Subterm_id)
        btnSubTermAdd.Text = "Update"


    End Sub

    Protected Sub lnkBtnEditgv_EOY_Click(sender As Object, e As EventArgs)
        Dim DT As DataTable = New DataTable
        Dim DS As DataSet = New DataSet
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                       GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        Dim EOY_id As Integer
        EOY_id = gv_EOY.DataKeys(gvRow.RowIndex)("AEE_ID")
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@AEE_ID", EOY_id, SqlDbType.BigInt)
        DS = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetEOYDetailsByID", Param)
        DT = DS.Tables(0)
        txtDescriptors.Text = DT.Rows(0).Item("AEE_DESCR")
        txtFromMarks.Text = DT.Rows(0).Item("AEE_MARK_FROM")
        txtToMarks.Text = DT.Rows(0).Item("AEE_MARK_TO")
        Dim colorCode As Color = ColorTranslator.FromHtml(DT.Rows(0).Item("AEE_COLOR_CODE"))
        Expectation_Color_picker.SelectedColor = colorCode
        ViewState("EOYid") = Convert.ToString(EOY_id)
        btnMarkAdd.Text = "Update Descriptor"
    End Sub

    Protected Sub lnkBtnEditgv_Term_Click(sender As Object, e As EventArgs)
        Dim DT As DataTable = New DataTable
        Dim DS As DataSet = New DataSet
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                       GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        Dim Term_id As Integer
        Term_id = gv_Term.DataKeys(gvRow.RowIndex)("AET_ID")
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@AEE_ID", Term_id, SqlDbType.BigInt)
        DS = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetTermbyID", Param)
        DT = DS.Tables(0)

        ddlTerm.SelectedValue = DT.Rows(0).Item("TSM_ID")
        txtTermExpectation.Text = DT.Rows(0).Item("AET_EXPECTATION_PERC")

        ViewState("Termid") = Convert.ToString(Term_id)
        btnTermkAdd.Text = "Update Expectation"
    End Sub

    Protected Sub lnkBtnLock_Click(sender As Object, e As EventArgs)
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                       GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        Dim Subterm_id, bStatus As Integer
        Dim Lock_Status As String = CType(gvRow.FindControl("lnkBtnLock"), LinkButton).Text
        Subterm_id = gv_SubTerm.DataKeys(gvRow.RowIndex)("TSM_ID")
        If Lock_Status = "Lock" Then
            bStatus = 1
        Else
            bStatus = 0
        End If
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim str_query As String = "UPDATE TERM_SUB_MASTER SET TSM_BBLOCKED='" + bStatus.ToString() + "' WHERE TSM_ID=" + Subterm_id.ToString()
        SqlHelper.ExecuteNonQuery(sqlCon, CommandType.Text, str_query)
        Bind_SubTerm()
    End Sub
End Class
