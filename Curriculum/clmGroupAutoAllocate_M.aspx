<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGroupAutoAllocate_M.aspx.vb" Inherits="Curriculum_clmGroupAutoAllocate_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
  curr_elem = document.forms[0].elements[i];
  
  if(curr_elem.type == 'checkbox' )
  {
  if (curr_elem.disabled==false)
   {
   curr_elem.checked = !master_box.checked;
      }
 }
 }
  master_box.checked=!master_box.checked;
}



</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Auto Allocate Group Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
   

    <table align="center" width="100%">
        <tr align="left">
            <td>
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
      
                    
                    
                    
        <tr align="left">
            <td width="100%">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                       
                     <tr>
                        <td align="left" width="20%">
                              <span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                         <td colspan="2"></td>
                    </tr>
                     <tr>
                        <td align="left" >
                          <span class="field-label"> Grade</span></td>
                      
                         <td align=left >
                  <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" /><br />
                  <asp:CheckBoxList ID="lstGrade" runat="server" style="vertical-align: middle; overflow: auto; text-align: left; border-color:inherit;" BorderStyle="Solid" BorderWidth="1px" Height="149px" Width="100%" RepeatLayout="Flow">
                  </asp:CheckBoxList> 
                   </td>
                         <td colspan="2"></td>
                    </tr>
                    
                    
                    
                   
                    
                      
                    
                   
                    <tr>
                        <td align="center" colspan="4" >
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Generate Groups" />
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>


</asp:Content>

