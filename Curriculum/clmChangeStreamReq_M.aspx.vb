Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmChangeStreamReq_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim subjs As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))

                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblStuName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                    PopulateNewStream()
                    subjs = GetRequestInfo()


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
  End If
      


        Try
            BindDataList()
            If Not Page.IsPostBack Then
                If ViewState("datamode") = "edit" Then
                    Listoptions(subjs)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

#Region "Private Methods"
    Function GetRequestInfo() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim subjects As String = ""
        Dim str_query As String = "SELECT SCS_ID,SCS_STM_NEW_ID,SCS_SUBJECTS,SCS_REMARKS FROM" _
                                 & " STUDENT_CHANGESTREAMREQ_S WHERE SCS_STU_ID=" + hfSTU_ID.Value _
                                 & " AND SCS_ACD_ID=" + hfACD_ID.Value + " AND SCS_APPROVE=0 AND SCS_TYPE='CS'"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        hfSCS_ID.Value = 0
        While reader.Read
            hfSCS_ID.Value = reader.GetValue(0).ToString
            ddlStream.Items.FindByValue(reader.GetValue(1)).Selected = True
            subjects = reader.GetString(2)
            txtRemarks.Text = reader.GetString(3)
        End While
        reader.Close()

        If hfSCS_ID.Value <> 0 Then
            ViewState("datamode") = "edit"
        Else
            ViewState("datamode") = "add"
        End If
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Return subjects
    End Function

    Sub Listoptions(ByVal subjects As String)
        Dim item As DataListItem
        Dim lblOptId As Label
        Dim ddlOption As DropDownList

        Dim i As Integer = 0
        Dim optSubjects As String() = subjects.Split(",")
        For Each item In dlOptions.Items
            lblOptId = item.FindControl("lblOptId")
            ddlOption = item.FindControl(lblOptId.Text)
            ddlOption.Items.FindByText(optSubjects(i).Trim).Selected = True
            i += 1
        Next
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub PopulateNewStream()
        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + hfACD_ID.Value + " and grm_grd_id='" + hfGRD_ID.Value _
                                 & "' and stm_id<>" + hfSTM_ID.Value
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
    End Sub

    Sub BindDataList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT SGO_OPT_ID,OPT_DESCR FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID.Value _
                                & "' AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
                                & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                                & " ORDER BY OPT_DESCR "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlOptions.DataSource = ds
        dlOptions.DataBind()

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow
        Dim ddlOption As DropDownList
        Dim lblOptId As Label
        Dim li As ListItem
        Dim optPanel As Panel

        Dim ds1 As DataSet

        Dim dlItem As DataListItem

        For Each dlItem In dlOptions.Items

            lblOptId = dlItem.FindControl("lblOptId")

            str_query = "SELECT DISTINCT SBG_ID=CONVERT(VARCHAR(100),SBG_ID)+'|'+CONVERT(VARCHAR(100),SBG_SBM_ID),SBG_DESCR FROM " _
                       & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                       & " SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                       & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID.Value _
                       & "' AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
                       & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                       & " AND SGO_OPT_ID=" + lblOptId.Text _
                       & " ORDER BY SBG_DESCR "

            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlOption = New DropDownList
            ddlOption.ID = lblOptId.Text
            'ddlOption.Width = 120
            ddlOption.DataSource = ds1
            ddlOption.DataTextField = "SBG_DESCR"
            ddlOption.DataValueField = "SBG_ID"
            ddlOption.DataBind()

            li = New ListItem
            li.Text = "--"
            li.Value = "0"

            ddlOption.Items.Insert(0, li)

            optPanel = dlItem.FindControl("optPanel")
            optPanel.Controls.Add(ddlOption)

        Next
    End Sub

    Function SaveData() As Boolean
        Dim item As DataListItem
        Dim lblOptId As Label
        Dim ddlOption As DropDownList
        Dim options As New ArrayList
        Dim subjects As New ArrayList
        Dim optSubjects As String = ""

        For Each item In dlOptions.Items
            lblOptId = item.FindControl("lblOptId")
            ddlOption = item.FindControl(lblOptId.Text)
            If ddlOption.SelectedValue <> "0" Then
                If subjects.IndexOf(ddlOption.SelectedValue.ToString) <> -1 Then
                    lblError.Text = "The subjects from each bucket has to be unqiue"
                    Return False
                End If
                subjects.Add(ddlOption.SelectedValue.ToString)
                options.Add(lblOptId.Text + "|" + ddlOption.SelectedValue.ToString)
                If optSubjects <> "" Then
                    optSubjects += ", "
                End If
                optSubjects += ddlOption.SelectedItem.Text
            End If
        Next

        Dim optcount As Integer = dlOptions.Items.Count
        If options.Count <> optcount Then
            lblError.Text = "Please select one subject from all option buckets"
            Return False
        End If


        Dim optionXML As String = GetOptionXML(options)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "exec saveCHANGESTREAMREQ " _
                                 & hfSCS_ID.Value + "," _
                                 & hfACD_ID.Value + "," _
                                 & "'" + hfGRD_ID.Value + "'," _
                                 & hfSTM_ID.Value + "," _
                                 & ddlStream.SelectedValue.ToString + "," _
                                 & hfSTU_ID.Value + "," _
                                 & "'" + optionXML + "'," _
                                 & "'" + optSubjects + "'," _
                                 & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                 & "'CS'," _
                                 & "'" + ViewState("datamode") + "'"

        hfSCS_ID.Value = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Return True
    End Function

    Function GetOptionXML(ByVal options As ArrayList) As String
        Dim i As Integer
        Dim str As String = ""
        Dim opt As String()
        For i = 0 To options.Count - 1
            opt = options(i).ToString.Split("|")
            str += "<ID><OPT_ID>" + opt(0) + "</OPT_ID><SBG_ID>" + opt(1) + "</SBG_ID><SBM_ID>" + opt(2) + "</SBM_ID></ID>"
        Next

        Return "<IDS>" + str + "</IDS>"
    End Function
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If SaveData() = True Then
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("datamode") = "edit"
                lblError.Text = "Record saved successfully"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            SaveData()
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


End Class
