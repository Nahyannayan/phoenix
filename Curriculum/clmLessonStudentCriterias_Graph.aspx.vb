﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Imports Telerik.Web.UI

Partial Class Curriculum_clmLessonStudentCriterias_Graph
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            'lblAcademicYear.Text = Request.QueryString("accyear")
            lblGrade.Text = Request.QueryString("grade")
            'lblGroup.Text = Request.QueryString("group")
            'lblSubject.Text = Request.QueryString("subject")
            lblName.Text = Request.QueryString("name")
            'lblStudentID.Text = Request.QueryString("stuno")
            'lblTopic.Text = Request.QueryString("topic")
            'lblObjective.Text = Request.QueryString("obj_name")
            '  lblCurrentLevel.Text = Request.QueryString("clvl")

            hACD_ID.Value = Request.QueryString("acdid").Replace(".00", "").Replace(",", "")
            hSBG_ID.Value = Request.QueryString("sbgid").Replace(".00", "").Replace(",", "")
            hGRD_ID.Value = Request.QueryString("grdid").Replace(".00", "").Replace(",", "")
            hTopic_ID.Value = Request.QueryString("topicid").Replace(".00", "").Replace(",", "")
            hSGR_ID.Value = Request.QueryString("sgr_id").Replace(".00", "").Replace(",", "").ToString()
            hTSM_ID.Value = Request.QueryString("tsmid").Replace(".00", "").Replace(",", "")
            If Request.QueryString("obj_id") IsNot Nothing And Request.QueryString("obj_id") <> "" Then
                hObj_ID.Value = Request.QueryString("obj_id").Replace(".00", "").Replace(",", "").ToString()
            End If
            GetStu_ID()
            BindPhoto()
            Load_Attainment_Report()
            'Load_Detailed_Report()

        End If
    End Sub

    Sub BindPhoto()

        If hStuPhotoPath.Value <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + hStuPhotoPath.Value
            imgStud.ImageUrl = strImagePath


        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub



    Sub GetStu_ID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH FROM STUDENT_M WHERE STU_NO='" + Request.QueryString("stuno").ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hSTU_ID.Value = ds.Tables(0).Rows(0).Item(0)
            hStuPhotoPath.Value = ds.Tables(0).Rows(0).Item(1)
            Session("APP_STU_ID") = hSTU_ID.Value
        End If
    End Sub
  
    Sub Load_Detailed_Report()
        Dim param As New Hashtable
        ' param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STC_SBG_ID", hSBG_ID.Value)
        param.Add("@SSD_SGR_ID", hSGR_ID.Value)
        param.Add("@STU_ID", hSTU_ID.Value)
        param.Add("@TSM_ID", hTSM_ID.Value)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("/PHOENIXBETA/Curriculum/Reports/Rpt/RptObjectives_Detailed.rpt")
        End With
        Session("rptClass") = rptClass
        Session("ReportSel") = "PDF"
        ifrm_DetailedR.Src = Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx")
    End Sub
    Sub Load_Attainment_Report()
        Dim param As New Hashtable
        ' param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STC_SBG_ID", hSBG_ID.Value)
        param.Add("@SSD_SGR_ID", hSGR_ID.Value)
        param.Add("@STU_IDS", hSTU_ID.Value)
        param.Add("@TSM_IDS", Get_Assessed_TSM()) 'hTSM_ID.Value
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", hACD_ID.Value)
        param.Add("@GRD_ID", hGRD_ID.Value.Split("|")(0).ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("/PHOENIXBETA/Curriculum/Reports/Rpt/RPT_PUPIL_TRACKER_ATTAINMENT.rpt")
        End With
        Session("rptClass") = rptClass
        Session("ReportSel") = "PDF"
        ifrm_Attainment.Src = Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx")
    End Sub
    Sub Load_Progress_Report()
        Dim param As New Hashtable
        ' param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STC_SBG_ID", hSBG_ID.Value)
        param.Add("@SSD_SGR_ID", hSGR_ID.Value)
        param.Add("@STU_IDS", hSTU_ID.Value)
        param.Add("@TSM_IDS", Get_Assessed_TSM()) 'hTSM_ID.Value
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", hACD_ID.Value)
        param.Add("@GRD_ID", hGRD_ID.Value.Split("|")(0).ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("/PHOENIXBETA/Curriculum/Reports/Rpt/RPT_PUPIL_TRACKER_PROGRESS.rpt")
        End With
        Session("rptClass") = rptClass
        Session("ReportSel") = "PDF"
        ifrm_Progress.Src = Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx")
    End Sub

    Protected Sub TabContainer1_ActiveTabChanged(sender As Object, e As EventArgs)
        If TabContainer1.ActiveTabIndex = 0 Then
            Load_Attainment_Report()
        ElseIf TabContainer1.ActiveTabIndex = 1 Then
            Load_Progress_Report()
        ElseIf TabContainer1.ActiveTabIndex = 2 Then
            Load_Detailed_Report()
        End If
    End Sub
    Function Get_Assessed_TSM() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Try
            Dim TSM_IDS As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "EXEC [SYL].[RPT_GET_ASSESSED_TERMS] '" + hSBG_ID.Value + "','" + hSGR_ID.Value + "','" + hSTU_ID.Value + "'")

            Return TSM_IDS
        Catch ex As Exception

        End Try

    End Function
End Class
