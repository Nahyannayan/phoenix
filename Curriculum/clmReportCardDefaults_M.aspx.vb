Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReportCardDefaults_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100131") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("dtDefaultValues") = SetDataTable()
                    hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfRSD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsdid").Replace(" ", "+"))
                    lblReportCard.Text = Encr_decrData.Decrypt(Request.QueryString("report").Replace(" ", "+"))
                    lblHeader.Text = Encr_decrData.Decrypt(Request.QueryString("header").Replace(" ", "+"))
                    hfAllSubjects.Value = Encr_decrData.Decrypt(Request.QueryString("allSubjects").Replace(" ", "+"))

                    If hfAllSubjects.Value = "Y" Then
                        BindGrade()
                        BindSubject()
                        BindOtherSubjects()
                        BindDefaultValuesForAllSubjects()
                        BindCopyHeader(True)

                        '    tbReport.Rows(11).Visible = False
                        '    tbReport.Rows(12).Visible = False
                        '    tbReport.Rows(13).Visible = False
                    Else
                        tbReport.Rows(3).Visible = False
                        tbReport.Rows(4).Visible = False
                        tbReport.Rows(9).Visible = False
                        tbReport.Rows(10).Visible = False
                        tbReport.Rows(11).Visible = False
                        BindDefaultValues()
                        BindCopyHeader(False)

                    End If

                End If

                gvDefault.Attributes.Add("bordercolor", "#1b80b6")
                gvHeaders.Attributes.Add("bordercolor", "#1b80b6")
                gvSubject.Attributes.Add("bordercolor", "#1b80b6")
                BindDefaultBank()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"

    Sub BindDefaultBank()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RDM_ID,RDM_DESCR FROM RPT.DEFAULTBANK_M WHERE RDM_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvReport.DataSource = ds
        gvReport.DataBind()
        If gvReport.Rows.Count > 0 Then
            gvReport.HeaderRow.Visible = False
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + hfACD_ID.Value _
                                & " AND RSG_RSM_ID=" + hfRSM_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR,GRM_DISPLAY FROM SUBJECTS_GRADE_S " _
                                         & " AS A INNER JOIN OASIS..GRADE_BSU_M AS B ON A.SBG_GRD_ID=B.GRM_GRD_ID" _
                                         & " AND A.SBG_ACD_ID=B.GRM_ACD_ID" _
                                         & " WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                         & " AND SBG_ACD_ID=" + hfACD_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindCopyHeader(ByVal allSubjects As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + hfRSM_ID.Value _
                                & " AND RSD_ID <>" + hfRSD_ID.Value + " AND RSD_bDIRECTENTRY='TRUE' AND RSD_bALLSUBJECTS='" + allSubjects.ToString + "'" _
                                & " AND RSD_CSSCLASS<>'TEXTBOXMULTI' AND  RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvHeaders.DataSource = ds
        gvHeaders.DataBind()
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_DESCR"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_DISPLAYORDER"
        dt.Columns.Add(column)



        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub BindDefaultValuesForAllSubjects()
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        Dim reader As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RSP_ID) FROM RPT.REPORTSETUP_DEFAULTS_S WHERE " _
                                & " RSP_RSD_ID='" + hfRSD_ID.Value + "' AND RSP_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            gvDefault.Visible = True
            str_query = "SELECT RSP_DESCR,RSP_DISPLAYORDER FROM RPT.REPORTSETUP_DEFAULTS_S WHERE " _
                                & " RSP_RSD_ID='" + hfRSD_ID.Value + "' AND RSP_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = dt.NewRow
                dr.Item(0) = reader.GetString(0)
                dr.Item(1) = reader.GetValue(1).ToString
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvDefault.DataSource = dt
            gvDefault.DataBind()
            Session("dtDefaultValues") = dt
            ViewState("norecord") = 0
        Else
            Session("dtDefaultValues") = SetDataTable()
            gvDefault.DataSource = Session("dtDefaultValues")
            gvDefault.DataBind()
            gvDefault.Visible = False
            ViewState("norecord") = 1
        End If

        str_query = "SELECT SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
        hfSBM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Sub

    Sub BindDefaultValues()
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        Dim reader As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RSP_ID) FROM RPT.REPORTSETUP_DEFAULTS_S WHERE " _
                                & " RSP_RSD_ID='" + hfRSD_ID.Value + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            gvDefault.Visible = True
            str_query = "SELECT RSP_DESCR,RSP_DISPLAYORDER FROM RPT.REPORTSETUP_DEFAULTS_S WHERE " _
                                & " RSP_RSD_ID='" + hfRSD_ID.Value + "'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = dt.NewRow
                dr.Item(0) = reader.GetString(0)
                dr.Item(1) = reader.GetValue(1).ToString
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvDefault.DataSource = dt
            gvDefault.DataBind()
            Session("dtDefaultValues") = dt
            ViewState("norecord") = 0
        Else
            Session("dtDefaultValues") = SetDataTable()
            gvDefault.DataSource = Session("dtDefaultValues")
            gvDefault.DataBind()
            gvDefault.Visible = False

            ViewState("norecord") = 1
        End If

    End Sub


    Sub AddRows()
        gvDefault.Visible = True
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtDefaultValues")
        Dim keys As Object()
        ReDim keys(0)
        keys(0) = txtValue.Text.Trim
        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This value is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = txtValue.Text
        dr.Item(1) = ""
        dr.Item(2) = "add"
        dt.Rows.Add(dr)
        Session("dtDefaultValues") = dt
        GridBind(dt)

        If ViewState("norecord") = 1 Then
            gvDefault.Columns(1).Visible = False
        End If
    End Sub

    Sub EditRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = SetDataTable()
        Dim i As Integer
        Dim txtDefault As TextBox
        Dim txtOrder As TextBox
        Dim keys As Object()
        ReDim keys(0)
        Dim row As DataRow
        For i = 0 To gvDefault.Rows.Count - 1
            txtDefault = gvDefault.Rows(i).FindControl("txtDefault")
            txtOrder = gvDefault.Rows(i).FindControl("txtOrder")
            keys(0) = txtDefault.Text.Trim
            row = dt.Rows.Find(keys)
            If row Is Nothing Then
                dr = dt.NewRow
                dr.Item(0) = txtDefault.Text
                dr.Item(1) = txtOrder.Text
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End If
        Next

        Session("dtDefaultValues") = dt
        GridBind(dt)

    End Sub

    Sub BindDefaultValuesFromBank(ByVal rdm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RDD_DESCR,RDD_ORDER FROM RPT.DEFAULTBANK_D WHERE RDD_RDM_ID=" + rdm_id _
                                & " ORDER BY RDD_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As New DataTable
        dt = Session("dtDefaultValues")
        Dim dr As DataRow
        Dim i As Integer
        Dim keys As Object()
        Dim row As DataRow
        ReDim keys(0)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            keys(0) = ds.Tables(0).Rows(i).Item(0)
            row = dt.Rows.Find(keys)
            If row Is Nothing Then
                dr = dt.NewRow
                dr.Item(0) = ds.Tables(0).Rows(i).Item(0)
                dr.Item(1) = ds.Tables(0).Rows(i).Item(1)
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End If
        Next
        Session("dtDefaultValues") = dt
        GridBind(dt)
        gvDefault.Visible = True
    End Sub


    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(2) <> "delete" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvDefault.DataSource = dtTemp
        gvDefault.DataBind()
    End Sub

    Sub SaveData(ByVal SBG_ID As String, ByVal rsd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec RPT.removeREPORTDEFAULTS " _
                                               & rsd_id + "," _
                                               & SBG_ID + "," _
                                               & "'" + Session("sUsr_name") + "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                Dim dr As DataRow

                For Each dr In dt.Rows
                    With dr
                        If .Item(1) <> "delete" Then
                            str_query = "exec  RPT.saveREPORTDEFAULTS " _
                                     & hfRSM_ID.Value + "," _
                                     & rsd_id + "," _
                                     & SBG_ID + "," _
                                     & "'" + .Item(0) + "'," _
                                     & "'" + Val(.Item(1)).ToString + "'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If
                    End With
                Next

                str_query = " exec RPT.updateRESULTTYPE " + rsd_id
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                transaction.Commit()
                lblError.Text = "Record saved successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub



    Sub BindOtherSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,SBG_DESCR,GRM_DISPLAY FROM SUBJECTS_GRADE_S AS A" _
                              & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON B.RSG_GRD_ID=A.SBG_GRD_ID" _
                              & " INNER JOIN OASIS..GRADE_BSU_M AS C ON A.SBG_GRD_ID=C.GRM_GRD_ID AND A.SBG_ACD_ID=C.GRM_ACD_ID" _
                              & " WHERE SBG_GRD_ID<>'" + ddlGrade.SelectedValue.ToString + "'" _
                              & " AND SBG_SBM_ID='" + hfSBM_ID.Value + "'" _
                              & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                              & " AND RSG_RSM_ID=" + hfRSM_ID.Value


        str_query += "UNION ALL "

        str_query += "SELECT DISTINCT SBG_ID,SBG_DESCR,GRM_DISPLAY FROM SUBJECTS_GRADE_S AS A" _
                              & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON B.RSG_GRD_ID=A.SBG_GRD_ID" _
                              & " INNER JOIN OASIS..GRADE_BSU_M AS C ON A.SBG_GRD_ID=C.GRM_GRD_ID AND A.SBG_ACD_ID=C.GRM_ACD_ID" _
                              & " WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                              & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                              & " AND RSG_RSM_ID=" + hfRSM_ID.Value _
                              & " AND SBG_ID<>'" + ddlSubject.SelectedValue.ToString + "'"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubject.DataSource = ds
        gvSubject.DataBind()
        If gvSubject.Rows.Count = 0 Then
            tbReport.Rows(9).Visible = False
            tbReport.Rows(10).Visible = False
            tbReport.Rows(11).Visible = False
        Else
            tbReport.Rows(9).Visible = True
            tbReport.Rows(10).Visible = True
            tbReport.Rows(11).Visible = True
        End If
    End Sub
#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        lblError.Text = ""
        BindSubject()
        Session("dtDefaultValues") = SetDataTable()
        BindDefaultValuesForAllSubjects()
        BindOtherSubjects()

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        lblError.Text = ""
        Session("dtDefaultValues") = SetDataTable()
        BindDefaultValuesForAllSubjects()
        BindOtherSubjects()
    End Sub

    Protected Sub gvDefault_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDefault.RowCommand
        Try

            If e.CommandName = "edit" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvDefault.Rows(index), GridViewRow)
                Dim txtDefault As TextBox
                txtDefault = selectedRow.FindControl("txtDefault")
                Dim keys As Object()
                ReDim keys(0)
                keys(0) = txtDefault.Text
                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                index = dt.Rows.IndexOf(dt.Rows.Find(keys))
                dt.Rows(index).Item(2) = "delete"
                Session("dtDefaultValues") = dt
                GridBind(dt)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        EditRows()
        Dim i As Integer
        Dim j As Integer
        Dim lblCRsdId As Label
        Dim chkCSelect As CheckBox
        If hfAllSubjects.Value = "Y" Then
            SaveData(ddlSubject.SelectedValue.ToString, hfRSD_ID.Value)
            Dim lblSbgId As Label
            Dim chkSelect As CheckBox
            For i = 0 To gvSubject.Rows.Count - 1
                chkSelect = gvSubject.Rows(i).FindControl("chkSelect")
                lblSbgId = gvSubject.Rows(i).FindControl("lblSbgId")
                If chkSelect.Checked = True Then
                    SaveData(lblSbgId.Text, hfRSD_ID.Value)
                End If
            Next

            For j = 0 To gvHeaders.Rows.Count - 1
                chkCSelect = gvHeaders.Rows(j).FindControl("chkCSelect")
                lblCRsdId = gvHeaders.Rows(j).FindControl("lblCRsdId")
                If chkCSelect.Checked = True Then
                    SaveData(ddlSubject.SelectedValue.ToString, lblCRsdId.Text)
                    For i = 0 To gvSubject.Rows.Count - 1
                        chkSelect = gvSubject.Rows(i).FindControl("chkSelect")
                        lblSbgId = gvSubject.Rows(i).FindControl("lblSbgId")
                        If chkSelect.Checked = True Then
                            SaveData(lblSbgId.Text, lblCRsdId.Text)
                        End If
                    Next
                End If
            Next
            BindDefaultValuesForAllSubjects()
            BindCopyHeader(True)
        Else
            SaveData("NULL", hfRSD_ID.Value)
            For i = 0 To gvHeaders.Rows.Count - 1
                chkCSelect = gvHeaders.Rows(i).FindControl("chkCSelect")
                lblCRsdId = gvHeaders.Rows(i).FindControl("lblCRsdId")
                If chkCSelect.Checked = True Then
                    SaveData("NULL", lblCRsdId.Text)
                End If
            Next
            BindCopyHeader(False)
            BindDefaultValues()
        End If

        If gvDefault.Rows.Count > 0 Then
            gvDefault.Columns(1).Visible = True
        End If
    End Sub



    Protected Sub gvDefault_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDefault.RowEditing

    End Sub



    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        AddRows()
        txtValue.Text = ""
    End Sub

    Protected Sub gvReport_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReport.RowCommand
        If e.CommandName = "view" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvReport.Rows(index), GridViewRow)
            Dim lblRdmId As Label = selectedRow.FindControl("lblRdmId")
            BindDefaultValuesFromBank(lblRdmId.Text)
        End If
    End Sub
End Class
