Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class clmActivity_D_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    ViewState("datamode") = "add"
                    lblAcademicYear.Text = GetCurrentACY_DESCR()
                    lblGrade.Text = " 10 "
 
                    BindSection()
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub


    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.Integer"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STU_NAME As New DataColumn("STU_NAME", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NAME)
            dtDt.Columns.Add(STU_NAME)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
   
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        GridBind()
        Session("OptionalSubjects") = Nothing
        Session("OptionGroup") = Nothing
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Sub GridBind()
        'Dim dtTab As DataTable = GetOptionsForGrade(ddlAca_Year.SelectedValue, ddlGRD_ID.SelectedValue)
        'For Each drRow As DataRow In dtTab.Rows
        Dim conn_str As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sBSUID")
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = Session("Current_ACD_ID")
        pParms(2) = New SqlClient.SqlParameter("@GRD_ID", SqlDbType.VarChar)
        pParms(2).Value = "10"
        pParms(3) = New SqlClient.SqlParameter("@SCT_ID", SqlDbType.Int)
        If ddlSection.SelectedValue <> "ALL" Then
            pParms(3).Value = ddlSection.SelectedValue
        Else
            pParms(3).Value = 0
        End If
        pParms(4) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar)
        pParms(4).Value = txtStud_ID.Text
        pParms(5) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar)
        pParms(5).Value = txtStud_Name.Text

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.StoredProcedure, "GetStudentOptionAllocationDetails", pParms)


        'Dim str_sql As String = " SELECT * FROM (" & _
        '" SELECT STU_ID, STU_GRD_ID, STU_ACD_ID, STU_SCT_ID, STU_NO, " & _
        '"ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') STU_NAME, " & _
        '" STU_BSU_ID FROM STUDENT_M WHERE STU_BSU_ID = '" & Session("sBSUID") & "' AND STU_GRD_ID = '10'" & _
        '" AND STU_ACD_ID = " & Session("Current_ACD_ID") & ") A " & _
        '" WHERE 1=1 "
        'If ddlSection.SelectedValue <> "ALL" Then
        '    str_sql += " AND STU_SCT_ID = " & ddlSection.SelectedValue
        'End If
        'If txtStud_ID.Text <> "" Then
        '    str_sql += " AND STU_NO like '%" & txtStud_ID.Text & "%'"
        'End If
        'If txtStud_Name.Text <> "" Then
        '    str_sql += " AND STU_NAME like '%" & txtStud_Name.Text & "%'"
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)

        gvStud.DataSource = ds
        gvStud.DataBind()
        'Next

    End Sub

    Private Sub BindStream(ByVal ddlStream As DropDownList)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT STM_ID, STM_DESCR FROM VW_STREAM_M WHERE STM_ID <> 1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
    End Sub

    Private Sub BindOPtionGroup(ByVal STM_ID As Integer)
        'Dim dtTab As DataTable = GetOptionsForGrade(ddlAca_Year.SelectedValue, ddlGRD_ID.SelectedValue)
        'For Each drRow As DataRow In dtTab.Rows
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = " SELECT SGM_ID, SGM_DESCR FROM SUBJECTOPTION_GROUP_M " & _
        " WHERE SGM_ACD_ID = " & Session("next_ACD_ID") & " AND SGM_STM_ID = " & STM_ID & _
        " AND SGM_BSU_ID = '" & Session("sBSUID") & "' ORDER BY SGM_ORDER_BY "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)

        Session("OptionGroup") = ds
    End Sub

    Private Sub BindOptionGroupDropdown(ByVal STM_ID As Integer, ByVal ddlOption As DropDownList)
        'If Session("OptionGroup") Is Nothing Then
        '    BindOPtionGroup(STM_ID)
        'End If
        BindOPtionGroup(STM_ID)
        ddlOption.DataSource = Session("OptionGroup")
        ddlOption.DataTextField = "SGM_DESCR"
        ddlOption.DataValueField = "SGM_ID"
        ddlOption.DataBind()
    End Sub

    Private Sub BindOptionalSubjects()
        'Dim dtTab As DataTable = GetOptionsForGrade(ddlAca_Year.SelectedValue, ddlGRD_ID.SelectedValue)
        'For Each drRow As DataRow In dtTab.Rows
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = " SELECT SGS_SGM_ID, SGS_SBG_ID, SBG_DESCR FROM SUBJECTOPTION_GROUP_S " & _
        " INNER JOIN SUBJECTS_GRADE_S ON SUBJECTOPTION_GROUP_S.SGS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " & _
        " WHERE ISNULL(SGS_bOPTIONAL, 0)= 1 "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, str_sql)

        Dim htChoices As New Hashtable
        Dim htOptSubjects As New Hashtable
        If Not ds Is Nothing Then
            For Each dr As DataRow In ds.Tables(0).Rows
                htOptSubjects = htChoices(dr("SGS_SGM_ID"))
                If htOptSubjects Is Nothing Then
                    htOptSubjects = New Hashtable
                End If
                htOptSubjects(dr("SGS_SBG_ID")) = dr("SBG_DESCR")
                htChoices(dr("SGS_SGM_ID")) = htOptSubjects
            Next
        End If
        Session("OptionalSubjects") = htChoices
    End Sub

    Private Function GetOptionalSubject(ByVal optionID As Integer) As Hashtable
        If Session("OptionalSubjects") Is Nothing Then
            BindOptionalSubjects()
        End If
        Dim htchoices As Hashtable = Session("OptionalSubjects")
        If htchoices Is Nothing Then
            Return Nothing
        Else
            Return htchoices(optionID)
        End If

    End Function

    Private Sub BindOptionalSubjectDropdown(ByVal optionID As Integer, ByVal ddlOptionalSubject As DropDownList, ByVal lblElectiveText As Label)
        If Session("OptionalSubjects") Is Nothing Then
            BindOptionalSubjects()
        End If
        ddlOptionalSubject.DataSource = GetOptionalSubject(optionID)
        If ddlOptionalSubject.DataSource Is Nothing Then
            ddlOptionalSubject.DataBind()
            ddlOptionalSubject.Visible = False
            lblElectiveText.Visible = False
        Else
            ddlOptionalSubject.DataTextField = "value"
            ddlOptionalSubject.DataValueField = "key"
            ddlOptionalSubject.DataBind()
            ddlOptionalSubject.Visible = True
            lblElectiveText.Visible = True
        End If
    End Sub

    Private Sub CreateDynamicColumns()
        'For Each col As DataColumn In 
        Dim Tfield As New TemplateColumn
        'Tfield.ItemStyle.c()
        'bfield.DataField = dc.ColumnName
        'bfield.HeaderText = dc.ColumnName
        'dngrview.Columns.Add(bfield)
        'Next
        gvStud.DataSource = Nothing
        gvStud.DataBind()
    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim panel As UpdatePanel = Master.FindControl("UpdatePanel1")
    '    panel.UpdateMode = UpdatePanelUpdateMode.Conditional
    '    panel.ChildrenAsTriggers = True
    'End Sub

    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub


    'Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
    '    BindGrade()
    'End Sub

    Private Function GetCurrentACY_DESCR() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & Session("next_ACD_ID") & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    'Sub callYEAR_DESCRBind()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        str_Sql = "SELECT VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
    '        " FROM vw_ACADEMICYEAR_D INNER JOIN VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
    '        " WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" _
    '        & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        ddlAca_Year.Items.Clear()
    '        ddlAca_Year.DataSource = ds.Tables(0)
    '        ddlAca_Year.DataTextField = "ACY_DESCR"
    '        ddlAca_Year.DataValueField = "ACD_ID"
    '        ddlAca_Year.DataBind()
    '        If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
    '            ddlAca_Year.ClearSelection()
    '            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
    '        End If
    '        'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    'Sub BindGrade()
    '    'ddlGrade.DataSource = ActivityFunctions.GetGrade_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
    '    ddlGRD_ID.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
    '    ddlGRD_ID.DataTextField = "GRM_DISPLAY"
    '    ddlGRD_ID.DataValueField = "GRM_GRD_ID"
    '    ddlGRD_ID.DataBind()
    'End Sub

    'Protected Sub ddlGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGRD_ID.SelectedIndexChanged
    '    BindSection()
    'End Sub

    Sub BindSection()
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), Session("Current_ACD_ID"), "10")
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Add("ALL")
    End Sub
    Protected Sub ddlStream1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptionGroupDropdown(ddlStream1.SelectedValue, ddlChoice1)
        dllChoice1_SelectedIndexChanged(ddlChoice1, Nothing)
    End Sub

    Protected Sub ddlStream2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptionGroupDropdown(ddlStream2.SelectedValue, ddlChoice2)
        dllChoice2_SelectedIndexChanged(ddlChoice2, Nothing)
    End Sub

    Protected Sub ddlStream3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindOptionGroupDropdown(ddlStream3.SelectedValue, ddlChoice3)
        dllChoice3_SelectedIndexChanged(ddlChoice3, Nothing)
    End Sub

    Protected Sub dllChoice1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim vdllChoice1 As DropDownList = DirectCast(sender, DropDownList)

        Dim ddlOptional1 As DropDownList

        ddlOptional1 = TryCast(sender.FindControl("ddlOptional1"), DropDownList)
        Dim lblElectiveText As Label = TryCast(sender.FindControl("lblElective1"), Label)
        Dim lblOption1Subjects As Label = TryCast(sender.FindControl("lblOption1Subjects"), Label)
        lblOption1Subjects.Text = GetCompulsorySubjects(vdllChoice1.SelectedValue)
        If Not ddlOptional1 Is Nothing Then
            BindOptionalSubjectDropdown(vdllChoice1.SelectedValue, ddlOptional1, lblElectiveText)
        End If
    End Sub

    Protected Sub dllChoice2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim vdllChoice2 As DropDownList = DirectCast(sender, DropDownList)

        Dim ddlOptional2 As DropDownList

        ddlOptional2 = TryCast(sender.FindControl("ddlOptional2"), DropDownList)
        Dim lblElectiveText As Label = TryCast(sender.FindControl("lblElective2"), Label)
        Dim lblOption2Subjects As Label = TryCast(sender.FindControl("lblOption2Subjects"), Label)
        lblOption2Subjects.Text = GetCompulsorySubjects(vdllChoice2.SelectedValue)
        If Not ddlOptional2 Is Nothing Then
            BindOptionalSubjectDropdown(vdllChoice2.SelectedValue, ddlOptional2, lblElectiveText)
        End If
    End Sub

    Protected Sub dllChoice3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim vdllChoice3 As DropDownList = DirectCast(sender, DropDownList)

        Dim ddlOptional3 As DropDownList

        ddlOptional3 = TryCast(sender.FindControl("ddlOptional3"), DropDownList)
        Dim lblElectiveText As Label = TryCast(sender.FindControl("lblElective3"), Label)
        Dim lblOption3Subjects As Label = TryCast(sender.FindControl("lblOption3Subjects"), Label)
        lblOption3Subjects.Text = GetCompulsorySubjects(vdllChoice3.SelectedValue)
        If Not ddlOptional3 Is Nothing Then
            BindOptionalSubjectDropdown(vdllChoice3.SelectedValue, ddlOptional3, lblElectiveText)
        End If
    End Sub

    Private Function GetCompulsorySubjects(ByVal SGM_ID As Integer) As String
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = " SELECT SBG_DESCR FROM SUBJECTOPTION_GROUP_S " & _
        " INNER JOIN SUBJECTS_GRADE_S ON SUBJECTOPTION_GROUP_S.SGS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " & _
        " WHERE ISNULL(SGS_bOPTIONAL, 0)= 0  AND SGS_SGM_ID =" & SGM_ID

        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_sql)
        Dim strSubject As String = String.Empty
        Dim comma As String = String.Empty
        While drReader.Read()
            strSubject = strSubject & comma & drReader("SBG_DESCR")
            comma = " ,"
        End While
        Return strSubject
    End Function

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        'Try
        '    If (e.Row.RowType = DataControlRowType.DataRow) Then
        '        Dim ddlChoice1, ddlChoice2, ddlChoice3 As New DropDownList
        '        ddlChoice1 = e.Row.FindControl("ddlChoice1")
        '        ddlChoice2 = e.Row.FindControl("ddlChoice2")
        '        ddlChoice3 = e.Row.FindControl("ddlChoice3")
        '        If Not ddlChoice1 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice1)
        '            dllChoice1_SelectedIndexChanged(ddlChoice1, e)
        '        End If
        '        If Not ddlChoice2 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice2)
        '            dllChoice2_SelectedIndexChanged(ddlChoice2, e)
        '        End If
        '        If Not ddlChoice3 Is Nothing Then
        '            BindOptionGroupDropdown(ddlChoice3)
        '            dllChoice3_SelectedIndexChanged(ddlChoice3, e)
        '        End If
        '    End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try

    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand

        '    Try
        '        Dim index As String = e.CommandArgument
        '        Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)

        '        If (selectedRow.RowType = DataControlRowType.DataRow) Then
        '            Dim ddlChoice1, ddlChoice2, ddlChoice3 As New DropDownList
        '            ddlChoice1 = selectedRow.FindControl("ddlChoice1")
        '            ddlChoice2 = selectedRow.FindControl("ddlChoice2")
        '            ddlChoice3 = selectedRow.FindControl("ddlChoice3")
        '            If Not ddlChoice1 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice1)
        '                dllChoice1_SelectedIndexChanged(ddlChoice1, e)
        '            End If
        '            If Not ddlChoice2 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice2)
        '                dllChoice2_SelectedIndexChanged(ddlChoice2, e)
        '            End If
        '            If Not ddlChoice3 Is Nothing Then
        '                BindOptionGroupDropdown(ddlChoice3)
        '                dllChoice3_SelectedIndexChanged(ddlChoice3, e)
        '            End If
        '        End If
        '    Catch ex As Exception
        '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '        lblError.Text = "Request could not be processed"
        '    End Try

        'End If

    End Sub

    Protected Sub gvStud_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStud.RowEditing
        Dim index As Integer = e.NewEditIndex
        Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
        Dim lblStuID, lblStuNo, lblStuName, lblAggrigate, lblScience, lblMaths As New Label
        lblStuID = selectedRow.FindControl("lblStuID")
        lblStuNo = selectedRow.FindControl("lblStuNo")
        lblStuName = selectedRow.FindControl("lblStuName")
        lblAggrigate = selectedRow.FindControl("lblAggrigate")
        lblScience = selectedRow.FindControl("lblScience")
        lblMaths = selectedRow.FindControl("lblMaths")
        If (Not lblStuID Is Nothing) AndAlso (Not lblStuNo Is Nothing) AndAlso _
        (Not lblStuName Is Nothing) AndAlso (Not lblAggrigate Is Nothing) AndAlso _
        (Not lblScience Is Nothing) AndAlso (Not lblMaths Is Nothing) Then
            lblStudAggr.Text = lblAggrigate.Text
            lblStudScience.Text = lblScience.Text
            lblStudMATHS.Text = lblMaths.Text
            lblstudNo.Text = lblStuNo.Text
            lblStudName.Text = lblStuName.Text
            ViewState("EditSTU_ID") = lblStuID.Text
            SetDropDownDatas(ViewState("EditSTU_ID"), Session("next_ACD_ID"), Session("sBSUID"))
        End If
        MPSetOption.Show()
    End Sub

    Private Sub SetDropDownDatas(ByVal vSTU_ID As String, ByVal vACD_ID As String, ByVal vBSU_ID As String)
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = " SELECT [SOGS_CHOICE1] ,[SOGS_CHOICE1_OPT_SBG_ID] " & _
        ",[SOGS_CHOICE2] ,[SOGS_CHOICE2_OPT_SBG_ID] ,[SOGS_CHOICE3] ,[SOGS_CHOICE3_OPT_SBG_ID]" & _
        ", SUO1.SGM_STM_ID CHOICE1_STM_ID, SUO2.SGM_STM_ID CHOICE2_STM_ID, SUO3.SGM_STM_ID CHOICE3_STM_ID " & _
        " FROM [SUBJECTOPTIONGROUP_SELECTION] " & _
        " INNER JOIN SUBJECTOPTION_GROUP_M AS SUO1 ON SUBJECTOPTIONGROUP_SELECTION.SOGS_CHOICE1 = SUO1.SGM_ID " & _
        " INNER JOIN SUBJECTOPTION_GROUP_M AS SUO2 ON SUBJECTOPTIONGROUP_SELECTION.SOGS_CHOICE2 = SUO2.SGM_ID  " & _
        " INNER JOIN SUBJECTOPTION_GROUP_M AS SUO3 ON SUBJECTOPTIONGROUP_SELECTION.SOGS_CHOICE3 = SUO3.SGM_ID  " & _
        " WHERE  SOGS_BSU_ID = '" & vBSU_ID & "' AND  SOGS_GRD_ID = '10' " & _
        " AND SOGS_ACD_ID = " & vACD_ID & " AND SOGS_STU_ID =" & vSTU_ID

        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_sql)
        Dim strSubject As String = String.Empty
        Dim comma As String = String.Empty
        While drReader.Read()
            ddlStream1.SelectedIndex = -1
            ddlStream1.Items.FindByValue(drReader("CHOICE1_STM_ID")).Selected = True
            ddlStream1_SelectedIndexChanged(ddlStream1, Nothing)

            ddlStream2.SelectedIndex = -1
            ddlStream2.Items.FindByValue(drReader("CHOICE2_STM_ID")).Selected = True
            ddlStream2_SelectedIndexChanged(ddlStream2, Nothing)

            ddlStream3.SelectedIndex = -1
            ddlStream3.Items.FindByValue(drReader("CHOICE3_STM_ID")).Selected = True
            ddlStream3_SelectedIndexChanged(ddlStream2, Nothing)

            ddlChoice1.SelectedIndex = -1
            ddlChoice1.Items.FindByValue(drReader("SOGS_CHOICE1")).Selected = True
            dllChoice1_SelectedIndexChanged(ddlChoice1, Nothing)
            If ddlOptional1.Visible = True Then
                ddlOptional1.Items.FindByValue(drReader("SOGS_CHOICE1_OPT_SBG_ID")).Selected = True
            End If

            ddlChoice2.SelectedIndex = -1
            ddlChoice2.Items.FindByValue(drReader("SOGS_CHOICE2")).Selected = True
            dllChoice2_SelectedIndexChanged(ddlChoice2, Nothing)
            If ddlOptional2.Visible = True Then
                ddlOptional2.Items.FindByValue(drReader("SOGS_CHOICE2_OPT_SBG_ID")).Selected = True
            End If
            ddlChoice3.SelectedIndex = -1
            ddlChoice3.Items.FindByValue(drReader("SOGS_CHOICE3")).Selected = True
            dllChoice3_SelectedIndexChanged(ddlChoice3, Nothing)
            If ddlOptional3.Visible = True Then
                ddlOptional3.Items.FindByValue(drReader("SOGS_CHOICE3_OPT_SBG_ID")).Selected = True
            End If
        End While

    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlStream1 Is Nothing Then
            BindStream(ddlStream1)
        End If
        If Not ddlStream2 Is Nothing Then
            BindStream(ddlStream2)
        End If
        If Not ddlStream3 Is Nothing Then
            BindStream(ddlStream3)
        End If

        If Not ddlChoice1 Is Nothing Then
            BindOptionGroupDropdown(ddlStream1.SelectedValue, ddlChoice1)
            dllChoice1_SelectedIndexChanged(ddlChoice1, e)
        End If
        If Not ddlChoice2 Is Nothing Then
            BindOptionGroupDropdown(ddlStream2.SelectedValue, ddlChoice2)
            dllChoice2_SelectedIndexChanged(ddlChoice2, e)
        End If
        If Not ddlChoice3 Is Nothing Then
            BindOptionGroupDropdown(ddlStream3.SelectedValue, ddlChoice3)
            dllChoice3_SelectedIndexChanged(ddlChoice3, e)
        End If

        'MPSetOption.Show()
    End Sub
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MPSetOption.Hide()
        MPSetOption.Dispose()
        MPSetOption = Nothing
    End Sub

    Protected Sub btnStudSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudSave.Click
        If Not IsValid Then
            Return
        End If
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim retVal As Integer
                retVal = SaveDetails(False, transaction)
                If retVal = 0 Then
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"
                    MPSetOption.Hide()
                    GridBind()
                Else
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                End If
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Public Function ValidateSelection() As Boolean
        If ddlChoice1.SelectedValue = ddlChoice2.SelectedValue Then
            Return False
        End If
        Return True
    End Function

    Public Function SaveDetails(ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        Try
            Dim vSTU_ID As String = ViewState("EditSTU_ID")
            Dim vGRD_ID As String = "10"

            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOGS_BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@SOGS_ACD_ID", Session("next_ACD_ID"))
            pParms(2) = New SqlClient.SqlParameter("@SOGS_STU_ID", vSTU_ID)
            pParms(3) = New SqlClient.SqlParameter("@SOGS_GRD_ID", vGRD_ID)
            pParms(4) = New SqlClient.SqlParameter("@SOGS_CHOICE1_SGM_ID", ddlChoice1.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@SOGS_CHOICE2_SGM_ID", ddlChoice2.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@SOGS_CHOICE3_SGM_ID", ddlChoice3.SelectedValue)

            If ddlOptional1.Visible = True Then
                pParms(7) = New SqlClient.SqlParameter("@SOGS_CHOICE1_OPT_SBG_ID", ddlOptional1.SelectedValue)
            Else
                pParms(7) = New SqlClient.SqlParameter("@SOGS_CHOICE1_OPT_SBG_ID", DBNull.Value)
            End If

            If ddlOptional2.Visible = True Then
                pParms(8) = New SqlClient.SqlParameter("@SOGS_CHOICE2_OPT_SBG_ID", ddlOptional2.SelectedValue)
            Else
                pParms(8) = New SqlClient.SqlParameter("@SOGS_CHOICE2_OPT_SBG_ID", DBNull.Value)
            End If
            If ddlOptional3.Visible = True Then
                pParms(9) = New SqlClient.SqlParameter("@SOGS_CHOICE3_OPT_SBG_ID", ddlOptional3.SelectedValue)
            Else
                pParms(9) = New SqlClient.SqlParameter("@SOGS_CHOICE3_OPT_SBG_ID", DBNull.Value)
            End If
            pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[SAVE_SUBJECTOPTIONGROUP_SELECTION]", pParms)
            Dim ReturnFlag As Integer = pParms(11).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Protected Sub cvChoice1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If ddlChoice1.SelectedValue = ddlChoice2.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional2.SelectedValue Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        ElseIf ddlChoice1.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional3.SelectedValue Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        Else
            args.IsValid = True
        End If

    End Sub

    Protected Sub cvChoice2_ServerValidate(ByVal source As Object, ByVal e As System.Web.UI.WebControls.ServerValidateEventArgs)
        If ddlChoice1.SelectedValue = ddlChoice2.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional2.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        ElseIf ddlChoice2.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional2.SelectedValue = ddlOptional3.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        Else
            e.IsValid = True
        End If
    End Sub

    Protected Sub cvChoice3_ServerValidate(ByVal source As Object, ByVal e As System.Web.UI.WebControls.ServerValidateEventArgs)
        If ddlChoice2.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional2.SelectedValue = ddlOptional3.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        ElseIf ddlChoice1.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional3.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        Else
            e.IsValid = True
        End If
    End Sub

    Protected Sub cusCustom_ServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        If ddlChoice1.SelectedValue = ddlChoice2.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional2.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        ElseIf ddlChoice2.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional2.SelectedValue = ddlOptional3.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        ElseIf ddlChoice1.SelectedValue = ddlChoice3.SelectedValue Then
            If ddlOptional1.SelectedValue = ddlOptional3.SelectedValue Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        Else
            e.IsValid = True
        End If

    End Sub


End Class
