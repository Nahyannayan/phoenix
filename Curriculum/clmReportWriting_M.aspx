<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmReportWriting_M.aspx.vb" Inherits="Curriculum_rptStudentsMarks"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <style>
                .table td select, table td input[type=text], table td input[type=date], table td textarea {
            width:auto;
                min-width: 100% !important;
        }
         </style>
    <script language="javascript" type="text/javascript">


    function confirm_delete()
    {
      if (confirm("You are about to delete this record.Do you want to proceed?")==true)
        return true;
      else
        return false;
     }//GRADE
     
     // To Get The Comments Only
     
    function getcomments(ctlid, code, header, stuid, rsmid) {
        //      alert(ctlid); 
        var sFeatures;
        sFeatures = "dialogWidth: 530px; ";
        sFeatures += "dialogHeight: 450px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var result;
        var parentid;
        var varray = new Array();
        var str;
        if (code == 'ALLCMTS') {
            //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code +"","", sFeatures);
            document.getElementById('<%=hfctlid.ClientID%>').value = ctlid;
            var oWnd = radopen("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "pop_comment");

            //  result = window.showModalDialog("clmCommentsList.aspx?ID=" + code +"&HEADER=" + header +"&STUID=" + stuid + "&RSMID="+rsmid,"", sFeatures);          
            //    if(result != '' && result != undefined)
            //    {

            //        str=document.getElementById(ctlid).value;
            //        document.getElementById(ctlid).value =str + result.replace(/_#_/g,"'");
            //        //result.split('___')[0]
            //        //document.getElementById(ctlid.split(';')[1]).value=varray[0];
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}

        }
    }
     
         function OnClientClose2(oWnd, args) {
             //get the transferred arguments

             var arg = args.get_argument();            
             if (arg) {
                 //alert(1);
                 //alert(arg);
                 NameandCode = arg.Comment.split('||');
                 //alert(NameandCode);
                 //str = document.getElementById(ctlid).value;
                 str = document.getElementById('<%=hfctlid.ClientID%>').value
                 document.getElementById(document.getElementById('<%=hfctlid.ClientID%>').value).value =  NameandCode//.replace("/ap/", "'");
                 document.getElementById('<%= txtCommenttxt.ClientID%>').value = NameandCode//.replace("/ap/", "'");
                 __doPostBack('<%= txtCommenttxt.ClientID%>', 'TextChanged');
             }
         }


     //-------------------------
     
         function GetPopUp(id)
         {     
             //alert(id);
             var sFeatures;
             sFeatures="dialogWidth: 429px; ";
             sFeatures+="dialogHeight: 345px; ";
             sFeatures+="help: no; ";
             sFeatures+="resizable: no; ";
             sFeatures+="scroll: yes; ";
             sFeatures+="status: no; ";
             sFeatures+="unadorned: no; ";
             var NameandCode;
             var result;
           
             if(id==3)//Group
             {
                 var GradeGrpId
                 var GradeId
                 GradeId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                 GradeGrpId=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                 result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&GRDID="+GradeId+"&SBM_IDs="+GradeGrpId+"","", sFeatures)            
                 if(result != '' && result != undefined)
                 {
                     document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                 }
                 else
                 {
                     return false;
                 }
             }
            
             else if(id==4)//Subjejct
             {
                 var GrdDetId
                 GrdDetId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                 var SBGID
                 SBGID=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                 result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID="+GrdDetId+"&SBGID="+SBGID+"","", sFeatures)            
                 if(result != '' && result != undefined)
                 {
                     document.getElementById('<%=H_SBJ_GRD_ID.ClientID %>').value = result;
                 }
                 else
                 {
                     return false;
                 }
             }
             else if(id==5)//Subject Grades
             {
                 var GradeId
                 var AcdId
                 AcdId=document.getElementById('<%=H_ACD_ID.ClientID %>').value;
                 GradeId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                 //alert(GradeId + 'ddd');
                 result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJACDGRD&GRDID="+GradeId+"&ACDID="+AcdId+"","", sFeatures)            
                 if(result != '' && result != undefined)
                 {
                     document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;
                    
                    
                 }
                 else
                 {
                     return false;
                 }
             }
             else if(id==8)//Student Info
             {
                 var GradeId
                 GradeId=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                 //  result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "", sFeatures)
                 var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "pop_up");
                 <%--if(result != '' && result != undefined)
                {
                  //  document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                     document.getElementById('<%=H_STU_ID.ClientID %>').value =result.split('___')[0];
                    document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
                  //  document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
                             
                              
                              }
                else
                {
                    return false;
                }--%>
             }   
             else if(id==12)//Student Info
             {
                 var GradeId;
                 var RepSch;
                 var RepHeader;
                 GradeId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                 acdId=document.getElementById('<%=H_ACD_ID.ClientID %>').value;
                 sbgId=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                 sgr_Descr=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                 RepSch=document.getElementById('<%=H_RPF_ID.ClientID %>').value;
                 RepHeader=''
                
                 result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_MARKS&GETSTU_NO=true&SGR_IDs="+GradeId+"&ACD_IDs="+acdId+"&SBG_IDs="+sbgId+"&SGR_DESCR="+sgr_Descr+"&REP_SCH="+RepSch+"&REP_HEADER="+RepHeader+"","", sFeatures)            
                 if(result != '' && result != undefined)
                 {
                     //alert(document.getElementById('<%=lblStudNo.ClientID %>').value);
                     document.getElementById('<%=H_STU_ID.ClientID %>').value =result.split('___')[0];
                     document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
                     document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
                 }
                 else
                 {
                     return false;
                 }
             }
         }//CMTSCAT
        
      function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById('<%=H_STU_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

  function GetStudentValues()
    {
     var ColsId;
     var cellcnt=0;
     ColsId=document.getElementById('<%=H_CommentCOLS.ClientID %>').value;
         //alert(ColsId);
     var araay = new Array();
        //alert(txt1.length);
        
       try
       {
               for(i=0;i<txt1.length;i++)
                {
                 araay[i]=new Array(ColsId) 
                   //alert(ColsId);
                  for(j=0;j<ColsId;j++)
                   {
                    var ArrName = "txt" + (Number(j) + 1);
                    //alert(ArrName);
                    ArrName = eval(ArrName);
                    try
                    {
                    var strtext;
                    strtext=document.getElementById(ArrName[i]).value.replace(/,/gi,'~');
                    strtext=replaceAll(strtext,',','~');
                    araay[i][j]=strtext.replace(/,/gi,'~');
                    //Alert(strtext)
                    // document.getElementById(ArrName[i]).value.replace(',','%')
                    }
                   catch(err)
                      {
                      araay[i][j].toString().replace(/,/gi,'~');
                      //alert(araay[i][j]);
                      //Handle errors here
                      }
                    //alert(araay[i][j]);
                    cellcnt=cellcnt+1;
                   }
                }
        }
        catch(err1)
          {
          }
              
        //alert(araay.length);
        var strvalue='';
        //Sending Array values into ServerSide
         for(var i=0; i<araay.length; i++) 
         {   
          strvalue =strvalue + araay[i] + '|'; 
         } 
        //alert(strvalue);   
        document.getElementById('<%=H_CommentCOLS.ClientID %>').value=strvalue;
    }
    
    function replaceAll(stringValue, replaceValue, newValue)
    {
      var functionReturn = new String(stringValue);
         while ( true )
         {
          var currentValue = functionReturn;
          functionReturn = functionReturn.replace(replaceValue, newValue);
          if ( functionReturn == currentValue )
           break;
         }
     return functionReturn;
    }

    
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
               <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>         
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             <asp:Label ID="lblHeader" runat="server" Text="Report Writing"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"
        EnableTheming="False"></asp:Label>
    <table align="center" cellpadding="5" cellspacing="0" width="100%">
        
        <tr>
            <td align="left" width="20%">
                <span class="field-label">ReportCard</span></td>
           
            <td align="left" width="30%">
                <asp:Label ID="lblReport" runat="server" Text=""></asp:Label>
                <asp:DropDownList ID="ddlReportcard" runat="server" Visible="false" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td align="left" width="20%">
                <span class="field-label">Grade</span></td>
            
            <td align="left" width="30%">
                <asp:Label ID="lblGrade" runat="server" CssClass="field-value" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Subject</span></td>
            
            <td align="left" width="30%">
                <asp:Label ID="lblSubject" CssClass="field-value" runat="server"></asp:Label>
            </td>
            <td align="left" width="20%">
                <span class="field-label">Group</span>
            </td>
            
            <td align="left" width="30%">
                <asp:Label ID="lblGroup" runat="server" CssClass="field-value"></asp:Label>
            </td>
        </tr>
        <tr id="trCat" runat="server">
            <td align="left" width="20%">
                <span class="field-label">Subject Category</span></td>
            
            <td align="left"   colspan="2">
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="250px">
                        </asp:DropDownList><td>
                            <asp:Button ID="btnView" runat="server" CausesValidation="False" Text="View" CssClass="button"
                                ></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr  valign="top">
            <td align="left" colspan="4" class="title-bg">
                <span class="field-label">Search Students</span></td>
        </tr>
        <tr>
            <td align="left" width="20%" >
                <span class="field-label">Student Name</span> </td>
            <td>
                <asp:TextBox ID="txtStudName" runat="server" SkinID="TextBoxNormal" Width="274px"></asp:TextBox>
                <asp:ImageButton ID="imgStud" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(8);return false;">
                </asp:ImageButton>
                <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                    Text="List"  />
                <asp:Label ID="lblStudNo" runat="server" CssClass="field-value" Visible="False"></asp:Label>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td align="left"  colspan="4">
                <table width="100%">
                    <tr>
                        <td>
                            <telerik:RadSpell ID="RadSpell1" ButtonType="LinkButton" AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd"
                                HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" IsClientID="true" runat="server"
                                SupportedLanguages="en-US,English" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lbtnKeys" runat="server" OnClientClick="javascript:return false;">Header Keys</asp:LinkButton></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" >
                <asp:Label ID="lblPages" runat="server" CssClass="error"></asp:Label>
                <asp:DataList ID="dlPages" runat="server" RepeatColumns="100">
                    <ItemTemplate>
                        <asp:LinkButton runat="server"  Text='<%# Bind("pageno") %>' ID="lblPageNo"
                            OnClick="lblPageNo_Click"></asp:LinkButton>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td align="left"  colspan="4">
                <asp:GridView ID="gvStudents" CssClass="table table-bordered table-row" runat="server" EmptyDataText="No Data Found" 
                    Width="100%" AutoGenerateColumns="False" EnableTheming="True" OnPageIndexChanging="gvStudents_PageIndexChanging">
                       <HeaderStyle Height="30px" CssClass="gridheader_pop_big"/>
                      <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                       <AlternatingRowStyle CssClass="griditem_alternative" HorizontalAlign="Center"/>
                </asp:GridView>
                <asp:TextBox ID="txtCommenttxt" runat="server" Visible="false" OnTextChanged="txtCommenttxt_TextChanged" ></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none">
            <td align="left"  colspan="6">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center"  colspan="6">
                <asp:Button ID="btnSaveDetails" runat="server" CssClass="button" Text="Save " ValidationGroup="MAINERROR"
                     OnClientClick="GetStudentValues()" OnClick="btnSaveDetails_Click" />
                &nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save & Next" ValidationGroup="MAINERROR"
                     OnClientClick="GetStudentValues()" OnClick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnSubCancel_Click" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfctlid" runat="server" />
    <asp:HiddenField ID="H_GRP_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_RSM_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_RSM_DESCR" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_STU_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_SBJ_GRD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_SETUP" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_RPF_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_ACD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_GRD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="H_CommentCOLS" runat="server"></asp:HiddenField>
    <div id="popup" runat="server" class="panel-cover" style="display: none; text-align: left;
        vertical-align: middle; width: 500px; margin-top: -2px; margin-left: -3px; margin-right: 1px;
        border-style: solid; border-color: #1b80b6; border-width: 1.5px;">
        <div class="title-bg-small" style="width: 500px; color: #1b80b6; vertical-align: middle; padding-bottom: 2px;">
            Header Keys</div>
        <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="modalPopup1"
            Style="border-top-color: #1b80b6; border-top-style: solid; border-top-width: 1px;
            background-color: #ebeff1" Height="25%" Width="100%">
            <asp:Literal ID="ltProcess" runat="server"></asp:Literal></asp:Panel>
    </div>
    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" TargetControlID="lbtnKeys"
        PopupControlID="popup" HoverCssClass="popupHover" PopupPosition="Center" OffsetX="0"
        OffsetY="20" PopDelay="50" />

        </div>
    </div>
</div>

</asp:Content>
