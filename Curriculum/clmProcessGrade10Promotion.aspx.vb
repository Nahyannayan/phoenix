Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmProcessGrade10Promotion
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C3300120") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    PopulateSection()

                    PopulatePromotedSection()
                    BindAllotedStream()
                    '  BindAllotedOption()
                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvStudPromote.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    tbPromote.Rows(4).Visible = False
                    tbPromote.Rows(5).Visible = False
                    tbPromote.Rows(6).Visible = False
                    tbPromote.Rows(7).Visible = False
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        Else

            highlight_grid()

        End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            tbPromote.Rows(3).Visible = True
            tbPromote.Rows(4).Visible = True
            tbPromote.Rows(5).Visible = True
            tbPromote.Rows(6).Visible = True
            tbPromote.Rows(7).Visible = True
            lblError.Text = ""
            hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
            hfGRD_ID_PROMOTED.Value = GetpromotedGrade()
            hfGRD_ID.Value = "10"
            hfSCT_ID.Value = ddlSection.SelectedValue.ToString
            GridBind()

            PopulatePromotedSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function
    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='10') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    Function GetPromotedYear() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " select top 1 acd_id from academicyear_d where acd_startdt>" _
                                & "(select acd_startdt from academicyear_d where acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                & " and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + ") and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + " order by acd_startdt"
        Dim acdid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return acdid
    End Function


    Private Sub PopulatePromotedSection()
        Dim acd_id As Integer
        acd_id = GetPromotedYear()
        hfACD_ID_PROMOTED.Value = acd_id
        ddlPromoteSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + acd_id.ToString _
                                 & " AND GRM_GRD_ID=(SELECT TOP 1 GRD_ID FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                 & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='10') " _
                                 & " AND GRM_STM_ID='" + ddlStream.SelectedValue.ToString + "'" _
                                 & " ORDER BY GRD_DISPLAYORDER))" _
                                 & " ORDER BY SCT_DESCR"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPromoteSection.DataSource = ds
        ddlPromoteSection.DataTextField = "SCT_DESCR"
        ddlPromoteSection.DataValueField = "SCT_ID"
        ddlPromoteSection.DataBind()

    End Sub

    Function GetpromotedGrade() As String
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT TOP 1 GRD_ID FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
        '                          & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + ddlGrade.SelectedValue + "') ORDER BY GRD_DISPLAYORDER"
        'Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return "11"
    End Function
    Sub SaveData()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim xmlString As String = GetXMlString()
        Dim stuIds As String
        If xmlString = "" Then
            lblError.Text = "No Records  selected"
            Exit Sub
        End If
        'if the grade is max grade of the school then pass sectionid as zero
        str_query = "exec studPROMOTE '" + xmlString + "','" + Session("sbsuid") + "'," _
                    & Session("clm") + ",'PASS'," _
                    & ddlPromoteSection.SelectedValue.ToString + ",'" + Format(Now.Date, "yyyy-MM-dd") + "','" + Session("sUsr_name") + "'"
        Try
            stuIds = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + stuIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            'transaction.Commit()
            lblError.Text = "Record Saved Successfully"
        Catch myex As ArgumentException
            'transaction.Rollback()
            lblError.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            'transaction.Rollback()
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'End Using
    End Sub

    Function GetXMlString() As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblsctId As Label
        Dim str As String = ""
        With gvStudPromote
            For i = 0 To .Rows.Count - 1
                chkSelect = .Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblStuId = .Rows(i).FindControl("lblStuId")
                    lblsctId = .Rows(i).FindControl("lblsctId")
                    str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID><SCT_ID>" + lblsctId.Text + "</SCT_ID></ID>"
                End If
            Next
        End With
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>"
        Else
            Return ""
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudPromote.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudPromote.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudPromote.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudPromote.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvStudPromote.Rows.Count - 1
            Dim row As GridViewRow = gvStudPromote.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
        '                         & " STU_SCT_ID,ISNULL(SCT_DESCR,'') AS SCT_DESCR" _
        '                         & " FROM STUDENT_M  AS A " _
        '                         & " LEFT OUTER JOIN STUDENT_PROMO_S AS B ON A.STU_ID=B.STP_STU_ID AND STP_ACD_ID=" + Session("NEXT_ACD_ID") _
        '                         & " LEFT OUTER JOIN  SECTION_M AS C ON" _
        '                         & " B.STP_SCT_ID = C.SCT_ID " _
        '                         & " LEFT OUTER JOIN OASIS_CURRICULUM..SUBJECTSTREAMALLOCATION AS E ON A.STU_ID=E.SSM_STU_ID AND SSM_ACD_ID=" + Session("NEXT_ACD_ID") _
        '                         & " WHERE  STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
        '                         & " AND STU_GRD_ID='10'" _
        '                         & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
        '                         & "  AND STU_CurrStatus<>'CN'" _
        '                         & " AND STU_ID NOT IN (SELECT TCM_STU_ID FROM TCM_M WHERE ISNULL(TCM_bCANCELLED,'FALSE')='FALSE')" _
        '                         & " AND SSM_STM_ID=" + ddlStream.SelectedValue.ToString

        Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                         & " STU_SCT_ID,ISNULL(SCT_DESCR,'') AS SCT_DESCR" _
                         & " FROM STUDENT_M  AS A " _
                         & " LEFT OUTER JOIN STUDENT_PROMO_S AS B ON A.STU_ID=B.STP_STU_ID AND STP_ACD_ID=" + Session("NEXT_ACD_ID") _
                         & " LEFT OUTER JOIN  SECTION_M AS C ON" _
                         & " B.STP_SCT_ID = C.SCT_ID " _
                         & " LEFT OUTER JOIN OASIS_CURRICULUM.STREAM.STREAM_ALLOTED AS E ON A.STU_ID=E.SAA_STU_ID AND SAA_ACD_ID=" + Session("NEXT_ACD_ID") _
                         & " WHERE  STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                         & " AND STU_GRD_ID='10'" _
                         & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                         & "  AND STU_CurrStatus<>'CN'" _
                         & " AND STU_ID NOT IN (SELECT TCM_STU_ID FROM TCM_M WHERE ISNULL(TCM_bCANCELLED,'FALSE')='FALSE')" _
                         & " AND SAA_STM_ID=" + ddlStream.SelectedValue.ToString




        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If
        If ddlSection.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If
        'If ddlOption.SelectedValue.ToString <> "0" Then
        '    str_query += " AND SSM_SGM_ID=" + ddlOption.SelectedValue.ToString
        'End If


        ' AND C.SCT_GRM_ID=B.GRM_ID" 
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvStatus As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedStatus As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If gvStudPromote.Rows.Count > 0 Then

            ddlgvSection = gvStudPromote.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then
                If strFilter = "" Then
                    strFilter = "sct_descr='" + ddlgvSection.Text + "'"
                Else
                    strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"
                End If
                selectedSection = ddlgvSection.Text
            End If


        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView
        If strFilter <> "" Then
            dv.RowFilter = strFilter
        End If

        Dim dt As DataTable
        gvStudPromote.DataSource = dv

        gvStudPromote.DataBind()

        dt = dv.Table
        If gvStudPromote.Rows.Count > 0 Then

            ddlgvSection = gvStudPromote.HeaderRow.FindControl("ddlgvSection")

            Dim dr As DataRow

            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                    If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(4))
                    End If
                End With

            Next

            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If

        End If

        If gvStudPromote.Rows.Count > 0 Then
            btnUpdate.Visible = True
            btnUpdate1.Visible = True
        Else
            btnUpdate.Visible = False
            btnUpdate1.Visible = False
        End If

        set_Menu_Img()
    End Sub

    Sub BindAllotedStream()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlStream.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID FROM OASIS..STREAM_M AS A" _
                               & " INNER JOIN OASIS..GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID" _
                               & " WHERE GRM_GRD_ID='11' AND GRM_ACD_ID='" + Session("NEXT_ACD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
    End Sub


    'Sub BindAllotedOption()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    ddlOption.Items.Clear()
    '    Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
    '                             & " AND SGM_GRD_ID='11' AND SGM_STM_ID='" + ddlStream.SelectedValue.ToString + "'"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '    ddlOption.DataSource = ds
    '    ddlOption.DataTextField = "SGM_DESCR"
    '    ddlOption.DataValueField = "SGM_ID"
    '    ddlOption.DataBind()

    '    Dim li As New ListItem
    '    li.Text = "ALL"
    '    li.Value = "0"
    '    ddlOption.Items.Insert(0, li)
    'End Sub

    Function getPromotedMisMatchSections()
        Dim strSec As String = ""
        Dim i As Integer
        Dim lblSection As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvStudPromote.Rows.Count - 1
            chkSelect = gvStudPromote.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSection = gvStudPromote.Rows(i).FindControl("lblSection")
                If strSec <> "" Then
                    strSec += ","
                End If
                strSec += "'" + lblSection.Text + "'"
            End If
        Next
        If strSec = "" Then
            strSec = "'0'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_DESCR FROM SECTION_M " _
                                 & " WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID.Value + " AND SCT_DESCR  " _
                                 & " NOT IN(SELECT SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + hfGRD_ID_PROMOTED.Value + "' AND SCT_ACD_ID=" + hfACD_ID_PROMOTED.Value + ")" _
                                 & " AND SCT_DESCR IN(" + strSec + ")"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sections As String = ""
        While reader.Read
            If sections <> "" Then
                sections += " , "
            End If
            sections += reader.GetString(0)
        End While
        reader.Close()
        Return sections
    End Function

    Function getDetainedMisMatchSections()
        Dim strSec As String = ""
        Dim i As Integer
        Dim lblSection As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvStudPromote.Rows.Count - 1
            chkSelect = gvStudPromote.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSection = gvStudPromote.Rows(i).FindControl("lblSection")
                If strSec <> "" Then
                    strSec += ","
                End If
                strSec += "'" + lblSection.Text + "'"
            End If
        Next
        If strSec = "" Then
            strSec = "'0'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_DESCR FROM SECTION_M " _
                                 & " WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID.Value + " AND SCT_DESCR  " _
                                 & " NOT IN(SELECT SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID_PROMOTED.Value + ")" _
                                 & " AND SCT_DESCR IN(" + strSec + ")"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sections As String = ""
        While reader.Read
            If sections <> "" Then
                sections += " , "
            End If
            sections += reader.GetString(0)
        End While
        reader.Close()
        Return sections
    End Function

#End Region


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try

            SaveData()

            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            PopulateSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        '  PopulatePromotedSection()
    End Sub


    Protected Sub gvStudPromote_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudPromote.PageIndexChanging
        Try
            gvStudPromote.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Protected Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        Try
            SaveData()
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        ' BindAllotedOption()
    End Sub
End Class
