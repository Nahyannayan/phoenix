﻿<%@ Page Title="Untitled Page" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmTransscript_TWS.aspx.vb" Inherits="Curriculum_clmTransscript_TWS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table id="tblClm" runat="server" align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" height="40px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table id="Table1" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="right" width="30%">
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Data"
                                ValidationGroup="groupM1" OnClick="btnDownload_Click" />
                        </td>
                        <td align="left" width="30%">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                        Text="Upload Data" CausesValidation="false" />
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnGenerateReport" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left" width="20%"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

