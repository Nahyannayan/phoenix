﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCopyReportcardSetup.aspx.vb" Inherits="Curriculum_clmCopyReportcardSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span>   </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusinessUnit" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                        <td align="left" width="20%">
                            <span class="field-label">Curriculum</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlCurriculum" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" width="20%">
                            <span class="field-label">ReportCard</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnCopy" runat="server" CssClass="button"
                                Text="Add Setup" ValidationGroup="groupM1" />
                            <asp:HiddenField ID="hfACD_CURRENT" runat="server" />
                            <asp:HiddenField ID="hfACY_CURRENT" runat="server" />
                            <asp:HiddenField ID="hfACD_PREV" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

