<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeOptionReq_M.aspx.vb" Inherits="Curriculum_clmChangeOptionReq_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>  Request For Changing Option
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0" >
        <tr>
            <td >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" />
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                SkinID="error" ></asp:Label></td>
        </tr>
        <tr>
            <td valign="top">
                <table id="reqTable" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="subheader_img" visible="false">
                        <td align="left" colspan="9" valign="middle">
                           </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblStu" runat="server" Text="Student No" CssClass="field-label" ></asp:Label></td>
                    
                        <td align="left" >
                            <asp:Label ID="lblStuNo" runat="server" CssClass="field-value" ></asp:Label></td>

                        <td align="left" >
                            <asp:Label ID="lblNtext" runat="server" Text="Student Name" CssClass="field-label"></asp:Label></td>
             
                        <td align="left" >
                            <asp:Label ID="lblStuName" runat="server" CssClass="field-value"></asp:Label></td>

                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblGr" runat="server" Text="Grade" CssClass="field-label"></asp:Label></td>
                    
                        <td align="left" >
                            <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>

                        <td align="left" >
                            <asp:Label ID="lblStre" runat="server" Text="Stream" CssClass="field-label"></asp:Label></td>
                     
                        <td align="left" >
                            <asp:Label ID="lblStream" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" > <span class="field-label">Current Options</span></td>
          
                        <td align="left"  colspan="3" >
                            <asp:Label ID="lblOpts" runat="server" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" >
                            <asp:DataList ID="dlOptions" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" ShowHeader="False">
                                <ItemTemplate>
                                    <table border="0"  cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOptId" runat="server" Text='<%# BIND("SGO_OPT_ID") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblOption" runat="server" Text='<%# BIND("OPT_DESCR") %>' ></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="optPanel" EnableViewState="true" runat="server" BorderStyle="None">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <%--<td colspan="4" align="center">
                            <table id="Table1" width="100%"  border="0">
                                     <tr>--%>
                                    <td align="left" > <span class="field-label">Remarks</span></td>
                    
                                    <td align="left" colspan="3" >&nbsp;<asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="100%" ></asp:TextBox></td>
                              <%-- </tr>

                            </table>

                        </td>--%>
                    </tr>



                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7"  />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="groupM1" TabIndex="7"  /></td>
                    </tr>

                </table>




            </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" >
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSCS_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
            </td>

        </tr>

    </table>
</div>
            </div>
        </div> 

</asp:Content>

