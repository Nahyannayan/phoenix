Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroupTeacher_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ViewState("SelectedRow") = -1
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100070") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Session("dtGroups") = SetDataTable()
                    lblteacher.Text = Encr_decrData.Decrypt(Request.QueryString("teacher").Replace(" ", "+"))
                    hfEMP_ID.Value = Encr_decrData.Decrypt(Request.QueryString("empid").Replace(" ", "+"))
                    lblAcademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, hfACD_ID.Value)
                    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, hfACD_ID.Value)
                    PopulateGradeStream()
                    BindSubject(ddlSubject)
                    updateSubjectURL()
                    GetRows()

                    If Session("multistream") = 0 Then
                        trStream.Cells(0).Visible = False
                        trStream.Cells(1).Visible = False
                        'trStream.Cells(2).Visible = False
                        trStream.Cells(3).ColSpan = 2
                    End If

                    If Session("multishift") = 0 Then
                        trShift.Cells(2).Visible = False
                        trShift.Cells(3).Visible = False
                        'trShift.Cells(5).Visible = False
                        trShift.Cells(1).ColSpan = 3
                    End If
                End If

                If Session("sbsuid") <> "125017" Then
                    Table3.Rows(5).Visible = False
                    gvGroup.Columns(2).Visible = False
                    gvGroup.Columns(3).Visible = False
                    gvGroup.Columns(5).Visible = False
                Else
                    Table3.Rows(4).Cells(6).Visible = False
                    Table3.Rows(4).Cells(5).ColSpan = 2
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"
    Sub BindSubject(ByVal ddlSubject As DropDownList)

       

        ddlSubject.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + rsm_id _
        '                        & " ORDER BY RPF_DISPLAYORDER"


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ddlGrade.SelectedValue.ToString <> "0" And ddlGrade.SelectedValue.ToString <> "" Then
            str_query = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                      & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + hfACD_ID.Value _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        Else
            str_query = "SELECT DISTINCT SBG_ID,SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY,SBG_PARENTS_SHORT ,SBG_GRD_ID" _
                      & " FROM SUBJECTS_GRADE_S AS A INNER JOIN GRADE_BSU_M AS B ON A.SBG_GRD_ID=B.GRM_GRD_ID" _
                      & "  AND A.SBG_ACD_ID=B.GRM_ACD_ID WHERE SBG_ACD_ID = " + hfACD_ID.Value
        End If

        If ddlStream.SelectedValue.ToString <> "0" And ddlStream.SelectedValue.ToString <> "" Then
            str_query += " AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString
        End If
        str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        hfSBG_ID.Value = ddlSubject.SelectedValue
      
    End Sub
    Sub updateSubjectURL()
        hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + hfACD_ID.Value _
                          & "&grdid=" + ddlGrade.SelectedValue + "&stmid=" + ddlStream.SelectedValue.ToString
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub PopulateGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE " _
                        & " SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                        & " AND SGR_ACD_ID=" + hfACD_ID.Value _
                        & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                        & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString _
                        & " AND SGR_SBG_ID=" + hfSBG_ID.Value _
                        & " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub


    Public Sub PopulateGradeStream()

        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + hfACD_ID.Value + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "' and grm_shf_id=" + ddlShift.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()

    End Sub



    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGR_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGR_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "FROMDATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_SCHEDULE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ROOMNO"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub GetRows()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR,SGS_FROMDATE,SGS_ID,ISNULL(SGS_TODATE,'01/01/1900'),ISNULL(SGS_SCHEDULE,'') AS SGS_SCHEDULE,ISNULL(SGS_ROOMNO,'') AS SGS_ROOMNO FROM GROUPS_M AS A " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                                 & " WHERE SGS_EMP_ID=" + hfEMP_ID.Value.ToString _
                                 & " AND SGS_TODATE IS NULL AND SGR_ACD_ID=" + hfACD_ID.Value
        '   & " AND ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01' AND '" + Format(Now.Date, "yyyy-MM-dd") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = SetDataTable()
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetValue(0).ToString
            dr.Item(1) = reader.GetString(1).ToString
            dr.Item(2) = Format(reader.GetDateTime(2), "dd/MMM/yyyy")
            dr.Item(3) = "edit"
            dr.Item(4) = reader.GetValue(3).ToString
            dr.Item(5) = reader.GetString(5)
            dr.Item(6) = reader.GetString(6)
            dt.Rows.Add(dr)
        End While
        If dt.Rows.Count <> 0 Then
            gvGroup.DataSource = dt
            gvGroup.DataBind()
            btnEdit.Text = "Edit"
        Else
            btnEdit.Text = "Add"
            btnSave.Visible = False


        End If
        Session("dtGroups") = dt
    End Sub

    Sub EditRows()
        Dim dt As DataTable = Session("dtGroups")

        Dim dr As DataRow

        Dim index As Integer = ViewState("SelectedRow")
        With dt.Rows(index)
            If .Item(3) = "edit" Then
                .Item(3) = "update"
            End If
            .Item(5) = txtSchedule.Text
            .Item(6) = txtRoomNo.Text
        End With

        gvGroup.DataSource = dt
        gvGroup.DataBind()
        Session("dtGroups") = dt
    End Sub


    Sub AddRows()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dt As DataTable = Session("dtGroups")

        Dim keys As Object()
        ReDim keys(0)
        keys(0) = ddlGroup.SelectedValue.ToString


        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This group is already added"
            Exit Sub
        End If

        Dim dr As DataRow
        dr = dt.NewRow
        dr.Item(0) = ddlGroup.SelectedValue.ToString
        dr.Item(1) = ddlGroup.SelectedItem.Text
        dr.Item(2) = Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy")
        dr.Item(3) = "add"
        dr.Item(4) = "0"
        dr.Item(5) = txtSchedule.Text
        dr.Item(6) = txtRoomNo.Text
        dt.Rows.Add(dr)
        GridBind(dt)
    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(3) <> "delete" And dt.Rows(i)(3) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvGroup.DataSource = dtTemp
        gvGroup.DataBind()

    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dt As DataTable = Session("dtGroups")
        Dim dr As DataRow
        Dim str_query As String
        For Each dr In dt.Rows
            If dr.Item(3) <> "edit" Then
                str_query = "saveGROUPTEACHER " _
                                         & dr.Item(4) + "," _
                                         & dr.Item(0) + "," _
                                         & hfEMP_ID.Value.ToString + "," _
                                         & "'" + dr.Item(2) + "'," _
                                         & "'" + dr.Item(3) + "'," _
                                         & "'" + dr.Item(5) + "'," _
                                         & "'" + dr.Item(6) + "'," _
                                         & "'" + Session("sUsr_name") + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        ddlGrade.Enabled = value
        ddlShift.Enabled = value
        ddlStream.Enabled = value
        ddlGroup.Enabled = value
        gvGroup.Enabled = value
        btnAddNew.Enabled = value
        txtFrom.Enabled = value
        imgFrom.Enabled = value
    End Sub
#End Region

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            AddRows()
            btnSave.Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, hfACD_ID.Value)
            PopulateGradeStream()
            updateSubjectURL()
            BindSubject(ddlSubject)
            PopulateGroup()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            'If hfSBG_ID.Value <> "" Then
            hfSBG_ID.Value = ddlSubject.SelectedValue
            PopulateGroup()
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            PopulateGradeStream()
            updateSubjectURL()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            updateSubjectURL()
            BindSubject(ddlSubject)
            PopulateGroup()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub gvGroup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand
        Try

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvGroup.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtGroups")

            Dim lblGroupId As Label


            lblGroupId = selectedRow.FindControl("lblSgrId")

            ReDim keys(0)

            keys(0) = lblGroupId.Text


            index = dt.Rows.IndexOf(dt.Rows.Find(keys))

            If e.CommandName = "delete" Then
                If txtFrom.Text = "" Then
                    lblError.Text = "Please enter data in the field from date"
                    Exit Sub
                End If


                If dt.Rows(index).Item(3) = "add" Then
                    dt.Rows(index).Item(3) = "remove"
                Else
                    dt.Rows(index).Item(2) = txtFrom.Text
                    dt.Rows(index).Item(3) = "delete"
                End If
                Session("dtGroups") = dt
                GridBind(dt)
            ElseIf e.CommandName = "edit" Then
                ViewState("SelectedRow") = index
                btnAdd1.Text = "Update"
                With dt.Rows(index)
                    txtFrom.Text = Format(Date.Parse(.Item(2)), "dd/MMM/yyyy")
                    txtFrom.Enabled = False
                    txtSchedule.Text = .Item(5)
                    txtRoomNo.Text = .Item(6)
                End With
                Table3.Rows(2).Visible = False
                Table3.Rows(3).Visible = False
                Table3.Rows(4).Visible = False

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvGroup.RowDeleting

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            EnableDisableControls(True)
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            If gvGroup.Enabled = False Then
                Response.Redirect("~/curriculum/clmGroupTeacher_View.aspx?MainMnu_code=2QBfLqOnz54=&datamode=Zo4HhpVNpXc=")
                Exit Sub
            End If
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                    EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "add" Then
                    Session("dtGroups") = SetDataTable()
                    gvGroup.DataSource = Session("dtGroups")
                    gvGroup.DataBind()
                End If

                If Session("sbsuid") = "125017" Then
                    Table3.Rows(2).Visible = True
                    Table3.Rows(3).Visible = True
                    Table3.Rows(4).Visible = True
                    txtSchedule.Text = ""
                    txtRoomNo.Text = ""
                End If
            Else
                Response.Redirect("~/curriculum/clmGroupTeacher_View.aspx?MainMnu_code=2QBfLqOnz54=&datamode=Zo4HhpVNpXc=")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            lblError.Text = "Records saved successfully"
            Session("dtGroups") = SetDataTable()
            GetRows()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(False)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    'Protected Sub imgSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSub.Click
    '    Try
    '        If hfSBG_ID.Value <> "" Then
    '            PopulateGroup()
    '        End If

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    Protected Sub btnAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                ValidationSummary1.Visible = True
                AddRows()
            Else
                EditRows()
                ValidationSummary1.Visible = False
            End If
            ViewState("SelectedRow") = -1
            btnAdd1.Text = "Add"
            btnSave.Visible = True
            Table3.Rows(2).Visible = True
            Table3.Rows(3).Visible = True
            Table3.Rows(4).Visible = True
            txtSchedule.Text = ""
            txtRoomNo.Text = ""
            ValidationSummary1.Enabled = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvGroup.RowEditing

    End Sub
End Class
