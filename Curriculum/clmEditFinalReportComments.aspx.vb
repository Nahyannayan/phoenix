Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Partial Class Curriculum_clmEditFinalReportComments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330081") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSection()
                    BindSubjects()
                    BindGroup()
                    tblTC.Rows(6).Visible = False
                    tblTC.Rows(7).Visible = False

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"

    Sub BindSubjects()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        

        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

    End Sub

    Sub BindGroup()

        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SGR_SBG_ID=" + ddlSubject.SelectedValue.ToString

        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGroup.Items.Insert(0, li)

    End Sub


    'Sub DisplayDiv()
    '    Dim i As Integer
    '    Dim lblStuId As Label
    '    Dim hv As HtmlInputText
    '    For i = 0 To gvStud.Rows.Count - 1
    '        lblStuId = gvStud.Rows(i).FindControl("lblStuId")
    '        hv = Page.Master.FindControl("cphMasterpage").FindControl("h_div" + lblStuId.Text)
    '        'Page.FindControl("h_div" + lblStuId.Text)
    '    Next
    'End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " STU_SCT_ID,STU_GRD_ID,B.GRM_DISPLAY,SCT_DESCR" _
                        & " FROM STUDENT_M  AS A" _
                        & " INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                        & " WHERE CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                        & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Else
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " STU_SCT_ID,STU_GRD_ID,B.GRM_DISPLAY,SCT_DESCR" _
                        & " FROM OASIS_CURRICULUM..vw_STUDENT_DETAILS_PREVYEARS  AS A" _
                        & " INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " WHERE CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                        & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        End If



        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            Dim grade As String() = hfGRD_ID.Value.Split("|")
            str_query += " AND STU_GRD_ID= '" + grade(0) + "' AND STU_STM_ID=" + grade(1)
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If ddlGroup.SelectedValue <> "0" Then
            str_query += " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString + ")"
        ElseIf ddlSubject.SelectedValue <> "0" Then
            str_query += " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_SBG_ID=" + ddlSubject.SelectedValue.ToString + ")"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox



        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If



        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID,RSG_DISPLAYORDER " _
                                & " FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS D ON A.GRM_STM_ID=D.STM_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If

        str_query += " ORDER BY RSG_DISPLAYORDER "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Private Sub PopulateSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + grade(0) + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))"
        End If

        str_query += " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)

    End Sub

    Sub BindReportCard()
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               '& " AND ISNULL(RSM_bFINALREPORT,'FALSE')='TRUE' "
        'If ViewState("GRD_ACCESS") > 0 Then
        '    str_query += " AND RSM_ID IN(SELECT RSG_RSM_ID FROM RPT.REPORTSETUP_GRADE_S WHERE RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
        '             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
        '             & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))))"
        'End If

        str_query += "ORDER BY RSM_DISPLAYORDER"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                               & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub getHeaders()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                               & " AND RSD_bALLSUBJECTS='TRUE' AND RSD_CSSCLASS='TEXTBOXMULTI'" _
                               & " ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ViewState("headersource") = ds
    End Sub

    Sub BindHeader(ByVal dlHeader As DataList)
        dlHeader.DataSource = ViewState("headersource")
        dlHeader.DataBind()
    End Sub

    Sub BindSubjects(ByVal stu_id As String, ByVal gvSubjects As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SSD_STU_ID,SBG_DESCR,SBG_ID,Sbg_Entrytype,SSD_SGR_ID," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_LNAME,'') AS EMP_NAME FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN STUDENT_GROUPS_S AS B ON A.SBG_ID=B.SSD_SBG_ID" _
                               & " INNER  JOIN GROUPS_TEACHER_S AS C ON b.SSD_SGR_ID=C.SGS_SGR_ID" _
                               & " INNER JOIN VW_EMPLOYEE_M AS D ON C.SGS_EMP_ID=D.EMP_ID" _
                               & " WHERE SSD_SGR_ID IS NOT NULL AND SSD_STU_ID='" + stu_id + "'" _
                               & " AND SSD_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                               & " AND SGS_TODATE IS NULL"

        If ddlSubject.SelectedValue <> "0" Then
            str_query += " AND SBG_ID=" + ddlSubject.SelectedValue.ToString
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub

    Sub BindComments(ByVal stu_id As String, ByVal sbg_id As String, ByVal dlMarks As DataList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT ISNULL(RST_COMMENTS,'') AS COMMENTS,RSD_ID" _
                      & " FROM RPT.REPORT_SETUP_D AS B " _
                      & " LEFT OUTER JOIN RPT.REPORT_STUDENT_S AS A  ON A.RST_RSD_ID= " _
                      & " B.RSD_ID AND RST_STU_ID='" + stu_id + "' AND RST_RPF_ID='" + hfRPF_ID.Value + "'" _
                      & " AND RST_SBG_ID='" + sbg_id + "'" _
                      & " WHERE RSD_bDIRECTENTRY='true' AND RSD_bALLSUBJECTS='TRUE' AND RSD_CSSCLASS='TEXTBOXMULTI' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                      & " ORDER BY RSD_DISPLAYORDER"
        Else
            str_query = "SELECT ISNULL(RST_COMMENTS,'') AS COMMENTS,RSD_ID" _
                     & " FROM RPT.REPORT_SETUP_D AS B " _
                     & " LEFT OUTER JOIN RPT.REPORT_STUDENT_PREVYEARS AS A  ON A.RST_RSD_ID= " _
                     & " B.RSD_ID AND RST_STU_ID='" + stu_id + "' AND RST_RPF_ID='" + hfRPF_ID.Value + "'" _
                     & " AND RST_SBG_ID='" + sbg_id + "'" _
                     & " WHERE RSD_bDIRECTENTRY='true' AND RSD_bALLSUBJECTS='TRUE' AND RSD_CSSCLASS='TEXTBOXMULTI' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                     & " ORDER BY RSD_DISPLAYORDER"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlMarks.DataSource = ds
        dlMarks.DataBind()
    End Sub


    Sub BindGrades(ByVal stu_id As String, ByVal sbg_id As String, ByVal dlMarks As DataList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT ISNULL(RST_COMMENTS,ISNULL(RST_GRADING,'')) AS COMMENTS,RSD_ID,RSD_HEADER" _
                      & " FROM RPT.REPORT_SETUP_D AS B " _
                      & " LEFT OUTER JOIN RPT.REPORT_STUDENT_S AS A  ON A.RST_RSD_ID= " _
                      & " B.RSD_ID AND RST_STU_ID='" + stu_id + "' AND RST_RPF_ID='" + hfRPF_ID.Value + "'" _
                      & " AND RST_SBG_ID='" + sbg_id + "'" _
                      & " WHERE  RSD_bALLSUBJECTS='TRUE' AND RSD_SBG_ID IS NULL AND RSD_CSSCLASS='TEXTBOXSMALL' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                      & " ORDER BY RSD_DISPLAYORDER"
        Else
            str_query = "SELECT ISNULL(RST_COMMENTS,ISNULL(RST_GRADING,'')) AS COMMENTS,RSD_ID,RSD_HEADER" _
                     & " FROM RPT.REPORT_SETUP_D AS B " _
                     & " LEFT OUTER JOIN RPT.REPORT_STUDENT_PREVYEARS AS A  ON A.RST_RSD_ID= " _
                     & " B.RSD_ID AND RST_STU_ID='" + stu_id + "' AND RST_RPF_ID='" + hfRPF_ID.Value + "'" _
                     & " AND RST_SBG_ID='" + sbg_id + "'" _
                     & " WHERE  RSD_bALLSUBJECTS='TRUE' AND RSD_SBG_ID IS NULL AND RSD_CSSCLASS='TEXTBOXSMALL' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                     & " ORDER BY RSD_DISPLAYORDER"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlMarks.DataSource = ds
        dlMarks.DataBind()
    End Sub


    Sub SaveData(ByVal stu_id As String, ByVal sbg_id As String, ByVal entrytype As String, ByVal dlMarks As DataList, ByVal sgr_id As String)
        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim lblCmt As Label
        Dim txtCmt As TextBox
        Dim lblRsdID As Label
        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Dim lblSgrId As Label
        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim stTrans As SqlTransaction

        Dim i As Integer
        objConn.Open()
        stTrans = objConn.BeginTransaction
        Dim iReturnvalue As Integer
        Try
            For i = 0 To dlMarks.Items.Count - 1
                lblCmt = dlMarks.Items(i).FindControl("lblCmt")
                txtCmt = dlMarks.Items(i).FindControl("txtCmt")
                lblRsdID = dlMarks.Items(i).FindControl("lblRsdId")
                If lblCmt.Text <> txtCmt.Text Then
                    If Session("Current_ACD_ID") = hfACD_ID.Value Then
                        cmd = New SqlCommand("[RPT].[SaveREPORT_STUDENT_S]", objConn, stTrans)
                    Else
                        cmd = New SqlCommand("RPT.SaveREPORT_STUDENT_PREVYEARS", objConn, stTrans)
                    End If

                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.AddWithValue("@RST_ID", 0)
                    cmd.Parameters.AddWithValue("@RST_RPF_ID", hfRPF_ID.Value)
                    cmd.Parameters.AddWithValue("@RST_RSD_ID", lblRsdID.Text) '-- Report Setup
                    cmd.Parameters.AddWithValue("@RST_ACD_ID", hfACD_ID.Value)
                    cmd.Parameters.AddWithValue("@RST_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@RST_STU_ID", stu_id)
                    cmd.Parameters.AddWithValue("@RST_RSS_ID", 0)
                    cmd.Parameters.AddWithValue("@RST_SGR_ID", sgr_id)
                    cmd.Parameters.AddWithValue("@RST_SBG_ID", sbg_id)
                    cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", 0)

                    cmd.Parameters.AddWithValue("@RST_MARK", DBNull.Value)
                    cmd.Parameters.AddWithValue("@RST_COMMENTS", txtCmt.Text)
                    cmd.Parameters.AddWithValue("@RST_GRADING", DBNull.Value)
                    cmd.Parameters.AddWithValue("@RST_USER", Session("sUsr_name"))
                    cmd.Parameters.AddWithValue("@bEdit", 1)
                    cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmd.ExecuteNonQuery()
                    iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                    If iReturnvalue <> 0 Then
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    End If
                End If
                'If lblCmt.Text <> txtCmt.Text Then
                '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                '    Dim str_query As String
                '    If Session("Current_ACD_ID") = hfACD_ID.Value Then
                '        str_query = "EXEC RPT.SaveREPORT_STUDENT_S " _
                '                                & "0," _
                '                                & hfRPF_ID.Value + "," _
                '                                & lblRsdID.Text + "," _
                '                                & hfACD_ID.Value + "," _
                '                                & "'" + grade(0) + "'," _
                '                                & stu_id + "," _
                '                                & "0," _
                '                                & sgr_id + "," _
                '                                & sbg_id + "," _
                '                                & "'0'," _
                '                                & "NULL," _
                '                                & "'" + txtCmt.Text.Replace("'", "''") + "'," _
                '                                & "NULL," _
                '                                & "'" + Session("susr_name") + "'," _
                '                                & "0"
                '    Else

                '        str_query = "EXEC RPT.SaveREPORT_STUDENT_PREVYEARS " _
                '                                & "0," _
                '                                & hfRPF_ID.Value + "," _
                '                                & lblRsdID.Text + "," _
                '                                & hfACD_ID.Value + "," _
                '                                & "'" + grade(0) + "'," _
                '                                & stu_id + "," _
                '                                & "0," _
                '                                & sgr_id + "," _
                '                                & sbg_id + "," _
                '                                & "'0'," _
                '                                & "NULL," _
                '                                & "'" + txtCmt.Text.Replace("'", "''") + "'," _
                '                                & "NULL," _
                '                                & "'" + Session("susr_name") + "'," _
                '                                & "0"

                '    End If



            Next
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
        Finally
            objConn.Close()
        End Try
    End Sub

#End Region
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Try
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)

        Dim dlMarks As DataList = TryCast(sender.FindControl("dlMarks"), DataList)



        If lblEdit.Text = "Edit" Then
            lblEdit.Text = "Update"
            dlMarks.Enabled = True
        Else
            lblEdit.Text = "Edit"
            dlMarks.Enabled = False
            Dim lblEntryType As Label
            Dim lblStuIdSub As Label
            Dim lblSbgId As Label
            Dim lblMark As Label
            Dim txtMark As Label
            Dim lblSgrId As Label
            lblStuIdSub = TryCast(sender.FindControl("lblStuIdSub"), Label)
            lblSbgId = TryCast(sender.FindControl("lblSbgId"), Label)
            lblEntryType = TryCast(sender.FindControl("lblEntryType"), Label)
            txtMark = TryCast(sender.FindControl("txtMark"), Label)
            lblSgrId = TryCast(sender.FindControl("lblSgrId"), Label)
            SaveData(lblStuIdSub.Text, lblSbgId.Text, lblEntryType.Text.ToLower, dlMarks, lblSgrId.Text)

            lblError.Text = "Record Saved Successfully"

        End If
        ' Catch ex As Exception
        'UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        ' lblError.Text = "Request could not be processed"
        ' End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSection()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        PopulateSection()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub



    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand

    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvSubjects As GridView
            gvSubjects = e.Row.FindControl("gvSubjects")
            gvSubjects.Attributes.Add("bordercolor", "#1b80b6")
            Dim lblStuId As Label

            lblStuId = e.Row.FindControl("lblStuId")
            If lblStuId.Text <> "" Then
                BindSubjects(lblStuId.Text, gvSubjects)

                If gvSubjects.Rows.Count <> 0 Then
                    Dim dlHeader As DataList
                    dlHeader = gvSubjects.HeaderRow.FindControl("dlHeader")
                    If Not dlHeader Is Nothing Then
                        BindHeader(dlHeader)
                    End If
                End If
            End If
        End If
    End Sub
    Protected Sub gvSubjects_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            Dim lblSbgId As Label

            Dim dlMarks As DataList
            Dim dlGrades As DataList
            '  Dim lblEntryType As Label
            'lblEntryType = e.Row.FindControl("lblEntrytype")
            lblSbgId = e.Row.FindControl("lblSbgId")
            lblStuId = e.Row.FindControl("lblStuIdSub")
            dlMarks = e.Row.FindControl("dlMarks")
            dlGrades = e.Row.FindControl("dlGrades")
            If Not dlMarks Is Nothing Then
                BindComments(lblStuId.Text, lblSbgId.Text, dlMarks)
                BindGrades(lblStuId.Text, lblSbgId.Text, dlGrades)
            End If

            Dim lblEdit As LinkButton
            lblEdit = e.Row.FindControl("lblEdit")



        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        tblTC.Rows(6).Visible = True
        tblTC.Rows(7).Visible = True
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSCT_ID.Value = ddlSection.SelectedValue
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        hfRSM_ID.Value = ddlReportCard.SelectedValue.ToString
        hfRPF_ID.Value = ddlPrintedFor.SelectedValue.ToString
        getHeaders()
        GridBind()
        ViewState("headersource") = Nothing
    End Sub


    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
    End Sub
End Class
