<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmMarkEntry_M.aspx.vb" Inherits="Curriculum_clmMarkEntry_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#deffb4';//#f6deb2
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Mark Entry
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="tbl_ShowScreen" runat="server" align="center"  cellpadding="0" width="100%"
        cellspacing="0" >
        <tr>
            <td align="left"    valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>

        <tr>
            <td >
                <table id="tblact" runat="server" align="center"  cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Assesment</span></td>
                        
                        <td align="left"  width="30%">
                            <asp:Label ID="lblActivity" runat="server" class="field-value"></asp:Label></td>
                        <td align="left"  width="20%"><span class="field-label">Subject</span></td>
                       
                        <td align="left"  width="30%">
                            <asp:Label ID="lblSubject" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top"><span class="field-label">Group</span></td>
                       
                        <td align="left"  valign="top">
                            <asp:Label ID="lblGroup" runat="server" class="field-value"></asp:Label></td>
                        <td align="left"  valign="top"><span class="field-label">Date</span></td>
                        
                        <td align="left"  valign="top">
                            <asp:Label ID="lblDate" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top"><span class="field-label">Min.Marks</span></td>
                       
                        <td align="left"  valign="top">
                            <asp:Label ID="lblMinmarks" runat="server" class="field-value"></asp:Label></td>
                        <td align="left"  valign="top"><span class="field-label">Max Marks</span></td>
                       
                        <td align="left"  valign="top">
                            <asp:Label ID="lblMaxmarks" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top"><span class="field-label">Export To excel</span></td>
                        
                        <td align="left"  colspan="3" valign="top">
                            <asp:Button ID="btnExportExcel" runat="server" CssClass="button" OnClick="btnExportExcel_Click"
                                Text="Export Format" /></td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top"><span class="field-label">Import</span></td>
                        
                        <td align="left"  colspan="3" valign="top">
                            <asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
                            <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click"
                                Text="Import Excel" /></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4" valign="top"></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4" valign="top">
                            <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                            <asp:Button ID="btnComment1" runat="server" Text="Comment Entry" CssClass="button" TabIndex="4" /></td>
                    </tr>

                    <tr>
                        <td align="center"  colspan="4"  valign="top">

                            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                               CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                               PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaId" runat="server" Text='<%# Bind("Sta_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SL.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" width="5%"/>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Stu_Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Marks">
                                        <ItemTemplate>
                                             <asp:TextBox ID="txtMarks"  Text='<%# Bind("MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server"  />
                                             <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMarks"
                                                            ErrorMessage='<%# Bind("ERR") %>' Display="Dynamic" Type="Double" MaximumValue='<%# Bind("MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator></td>
                                         <%--   <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtMarks"  Text='<%# Bind("MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMarks"
                                                            ErrorMessage='<%# Bind("ERR") %>' Display="Dynamic" Type="Double" MaximumValue='<%# Bind("MAXMARKS") %>' MinimumValue="0">
                                                        </asp:RangeValidator></td>
                                                </tr>
                                            </table>--%>

                                        </ItemTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STA_GRADE") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlGrade" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>


                        <td  colspan="6" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                            <asp:Button ID="btnComment" runat="server" Text="Comment Entry" CssClass="button" TabIndex="4" /></td>


                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                        type="hidden" value="=" /></td>
        </tr>


    </table>
                </div>
            </div>
        </div>
    <asp:HiddenField ID="hfCAS_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMEntered" runat="server"></asp:HiddenField>

    &nbsp;
    <asp:HiddenField ID="hfMaxMarks" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfGradeSlab" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMinMarks" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfPathFinderBlock" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfEntryType" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSGR_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSBG_ID" runat="server"></asp:HiddenField>
</asp:Content>

