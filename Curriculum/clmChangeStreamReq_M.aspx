<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmChangeStreamReq_M.aspx.vb" Inherits="Curriculum_clmChangeStreamReq_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .table th, .table td {
            vertical-align: middle !important;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(141, 194, 76, .2) !important;
        }

        table td select, table td input[type=text], table td input[type=date] {
            border-color: #dee2da !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            padding: 10px;
            min-width: 80% !important;
            width: 75%;
            /*font-size:14px !important;*/
            color: #999;
        }

        table td textarea {
            border-color: #dee2da !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .card-header:first-child {
            color: #333 !important;
        }

        /*.bg-dark .card-header {
            background-color: transparent !important;
            border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important;
        }*/

        .card .mb-3 {
            box-shadow: 0px 0px 4px rgba(0,0,0,0.1) !important;
        }

        .griditem, .griditem_alternative {
            line-height: 28px;
        }

        table td div select {
            min-width: 75% !important;
        }

        .table-bordered {
            border: 1px solid #dee2e6;
        }

        table {
            font-size: 14px !important;
        }

            table tr {
                line-height: 40px;
            }

        .bg-dark table td input.button {
            line-height: 20px;
        }

        table td span.field-label {
            font-size: 18px;
            color: #999999;
        }

        table td span.field-value {
            border-radius: 6px;
            border: 1px solid #dee2da !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            padding: 10px;
            color: #999;
            background-color: rgba(0,0,0,0.01);
        }
    </style>

    <div class="card mb-3">
          <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Request For Changing Stream
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                     
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom"><asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table class="BlueTableView" id="reqTable" width="100%" runat="server" align="center" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblStu" class="field-label" runat="server" Text="Student No"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblStuNo" class="field-value" runat="server"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblNtext" runat="server" class="field-label" Text="Student Name"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblStuName" class="field-value" runat="server"></asp:Label></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblGr" class="field-label" runat="server" Text="Stream"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblGrade" class="field-value" runat="server"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblStre" class="field-label" runat="server" Text="Stream"></asp:Label></td>
                                    <td align="left">
                                        <asp:Label ID="lblStream" class="field-value" runat="server"></asp:Label></td>
                                </tr>



                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblstm" class="field-label" runat="server" Text="Move To Stream "></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:DataList ID="dlOptions" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOptId" class="field-label" runat="server" Text='<%# BIND("SGO_OPT_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblOption" class="field-label" runat="server" Text='<%# BIND("OPT_DESCR") %>'></asp:Label>
                                                <asp:Panel ID="optPanel" EnableViewState="true" runat="server" BorderStyle="None">
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>


                             <%--   <tr>
                                    <td colspan="4" align="center">
                                        <table id="Table1" width="100%">
                                            <tr>
                                                <td align="center">&nbsp;</td>
                                            </tr>--%>
                                            <tr>

                                                <td align="left"><span class="field-label">Remarks</span></td>

                                                <td align="left" colspan="3"><asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="107px" SkinID="MultiText_Large"></asp:TextBox></td>
                                            </tr>

<%--                                        </table>

                                    </td>
                                </tr>--%>



                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" ValidationGroup="groupM1" TabIndex="7" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="groupM1" TabIndex="7" OnClick="btnSave_Click" /></td>
                                </tr>

                            </table>




                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" style="height: 52px; width: 540px;">
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCS_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                            &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                        </td>

                    </tr>

                </table>

            </div>
        </div>
    </div>


</asp:Content>

