Imports System.Data.SqlClient
Imports CURRICULUM
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports system
Imports ActivityFunctions
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_clmSyllabus_LessonAllocation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'Session("CURRENT_ACD_ID")="1273"
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100545") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'ddlAcademicYear = ActivityFunctions.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    If ViewState("datamode") = "add" Then
                        Select Case Session("susr_name").ToString.ToLower
                            Case "dhanya", "charles", "charan", "rajesh"
                                If Session("sbsuid") = "125010" Then
                                    h_EMP_ID.Value = "658"
                                Else
                                    h_EMP_ID.Value = "2325"
                                End If

                            Case Else
                                h_EMP_ID.Value = Session("EmployeeId")
                        End Select
                          BindGroup()
                          ClearControls()
                        If Session("CurrSuperUser") = "Y" Then
                            trEmp.Visible = True
                        Else
                            trEmp.Visible = False
                        End If
                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gvLesson.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnAddDocument)
    End Sub
    Protected Sub btnGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
      

        Dim lblSgrId As Label = TryCast(sender.findcontrol("lblSgrId"), Label)
        getSubject(lblSgrId.Text)
        Dim btnGroup As Button = DirectCast(sender.findcontrol("btnGroup"), Button)
        h_GRP_ID.Value = lblSgrId.Text
        '   lblDetails.Text = "<font color=Purple size=2pt>Subject : " + hfSubject.Value + "&nbsp;&nbsp;&nbsp;Group : " + hfGroup.Value + "</font>"
        SetButtonBackGround(btnGroup.Text)
        ClearControls()
        BindLessonSetup()
        BindCopyGroup()
    End Sub
    Sub SetButtonBackGround(ByVal group As String)
        Dim i As Integer
        Dim btnGroup As Button
        For i = 0 To dlGroups.Items.Count - 1
            btnGroup = dlGroups.Items(i).FindControl("btnGroup")
            If btnGroup.Text = group Then
                btnGroup.CssClass = "button_menu_click button"
            Else
                btnGroup.CssClass = "button_menu button"
            End If
        Next
    End Sub
    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
            & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
            & " AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'" _
            & " AND SGS_EMP_ID=" + h_EMP_ID.Value

        'If ddlGrade.SelectedValue <> "0" Then
        '    str_query += " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        'End If
        str_query += " ORDER BY SGR_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlGroups.DataSource = ds
        dlGroups.DataBind()

        If dlGroups.Items.Count > 0 Then
            Dim lblSgrId As Label = dlGroups.Items(0).FindControl("lblSgrId")
            getSubject(lblSgrId.Text)
            Dim btnGroup As Button = dlGroups.Items(0).FindControl("btnGroup")
            h_GRP_ID.Value = lblSgrId.Text
            '   lblDetails.Text = "<font color=Purple size=2pt>Subject : " + hfSubject.Value + "&nbsp;&nbsp;&nbsp;Group : " + hfGroup.Value + "</font>"
            SetButtonBackGround(btnGroup.Text)
            BindLessonSetup()
        End If
        BindCopyGroup()
    End Sub

    Sub BindCopyGroup()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        'str_query = "SELECT SGR_ID AS ID,SGR_DESCR AS TEXT FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID " _
        '        & " WHERE SGS_EMP_ID=" + h_EMP_ID.Value + " AND SGR_SBG_ID=" + h_SUBJ_ID.Value + " AND SGR_ID<>" + h_GRP_ID.Value _
        '        & " AND SGS_TODATE IS NULL FOR XML AUTO"

        str_query = "SELECT SGR_ID AS ID,SGR_DESCR AS TEXT FROM GROUPS_M  " _
                      & " WHERE  SGR_SBG_ID=" + h_SUBJ_ID.Value + " AND SGR_ID<>" + h_GRP_ID.Value _
                      & "  ORDER BY SGR_DESCR FOR XML AUTO"


        '   Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '  If ds.Tables(0).Rows.Count > 0 Then
        'tvCopyGroup.DataSource = ds
        ' tvCopyGroup.DataTextField = "DESCR"
        'tvCopyGroup.DataValueField = "ID"
        ' tvCopyGroup.DataBind()
        '  End if


        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

             xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvCopyGroup.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvCopyGroup.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvCopyGroup.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


    End Sub
    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Sub getSubject(ByVal sgr_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_GRD_ID,SBG_DESCR,SGR_DESCR FROM SUBJECTS_GRADE_S INNER JOIN " _
                               & " GROUPS_M ON SGR_SBG_ID=SBG_ID WHERE SGR_ID=" + sgr_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        h_SUBJ_ID.Value = ds.Tables(0).Rows(0).Item(0)
        h_GRD_ID.Value = ds.Tables(0).Rows(0).Item(1)
        lblSubject.Text = ds.Tables(0).Rows(0).Item(2)
        lblGroup.Text = ds.Tables(0).Rows(0).Item(3)
        lblGrade.Text = ds.Tables(0).Rows(0).Item(2)
    End Sub
    
    Protected Sub btnadddetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i, j As Integer
        If IsDate(txtFromDate.Text) = False Then
            lblError.Text = "Invalid FromDate"
            Exit Sub
        End If
        If IsDate(txtToDate.Text) = False Then
            lblError.Text = "Invalid ToDate"
            Exit Sub
        End If
        If Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
            lblError.Text = "FromDate must be less than ToDate"
            Exit Sub
        End If
        If txtempname.Text = "" Then
            lblError.Text = "Select Employee"
            Exit Sub
        End If
        If ViewState("datamode") = "add" Then
            If CheckTopicExists() = True Then
                lblError.Text = "Topic already allocated"
                Exit Sub
            End If
            Dim ldrNew As DataRow
            Session("gDtlDataMode") = "Add"
            If Session("gDtlDataMode") = "Add" Then
                ldrNew = Session("dtLessonTeac").NewRow
                Session("gintGridLine") = Session("gintGridLine") + 1
                ldrNew("Id") = Session("gintGridLine")
                ldrNew("TOPICID") = h_TopicID.Value
                ldrNew("TOPICNAME") = txtTopic.Text
                ldrNew("EMPID") = h_EMP_ID.Value
                ldrNew("EMPNAME") = txtempname.Text
                ldrNew("GROUPID") = h_GRP_ID.Value
                ldrNew("LESFROMDT") = txtFromDate.Text
                ldrNew("LESTODT") = txtToDate.Text
                ldrNew("LESHRSTK") = txtHrs.Text
                ldrNew("LESOBJECTIVE") = txtobjective.Text
                ldrNew("LesSYTId") = 0
                ldrNew("LESRESOURCE") = txtResources.Text
                Session("dtLessonTeac").Rows.Add(ldrNew)
            End If
        Else

            If ViewState("datamode") = "Edit" Then
                If Not Session("dtLessonTeac") Is Nothing Then
                    For i = 0 To Session("dtLessonTeac").Rows.Count - 1
                        If (Session("dtLessonTeac").Rows(i)("Id") = Session("gintGridLine")) Then

                            Session("dtLessonTeac").Rows(i)("TOPICID") = h_TopicID.Value
                            Session("dtLessonTeac").Rows(i)("EMPID") = h_EMP_ID.Value
                            Session("dtLessonTeac").Rows(i)("EMPNAME") = txtempname.Text
                            Session("dtLessonTeac").Rows(i)("GROUPID") = h_GRP_ID.Value
                            Session("dtLessonTeac").Rows(i)("LESFROMDT") = txtFromDate.Text
                            Session("dtLessonTeac").Rows(i)("LESTODT") = txtToDate.Text
                            Session("dtLessonTeac").Rows(i)("LESHRSTK") = txtHrs.Text
                            Session("dtLessonTeac").Rows(i)("LESOBJECTIVE") = txtobjective.Text
                            Session("dtLessonTeac").Rows(i)("TOPICNAME") = txtTopic.Text
                            Session("dtLessonTeac").Rows(i)("LESRESOURCE") = txtResources.Text
                        End If
                    Next
                End If
            End If

            ' btnadddetails.Text = "Add"
        End If


        txtTopic.Text = ""
        txtobjective.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtHrs.Text = ""
        txtempname.Text = ""
        txtResources.Text = ""
    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearControls()
    End Sub
    Private Sub ClearControls()
        txtHrs.Text = ""
        txtobjective.Text = ""
        txtResources.Text = ""
        h_SyllabusId.Value = 0
        h_TopicID.Value = 0
        txtSyllabus.Text = ""
        txtTopic.Text = ""
        txtToDate.Text = ""
        txtFromDate.Text = ""
        Session("dtLessonTeac") = Nothing
        Session("gintGridLine") = 0
        h_SYT_ID.Value = 0
        h_Completed.Value = 0
        txtPlannedStDate.Text = ""
        txtPlannedEndDate.Text = ""
        dlDocs.DataSource = Nothing
        dlDocs.DataBind()

        grdTeachDoc.DataSource = Nothing
        grdTeachDoc.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer
        Try
            Dim ckBxselect As New CheckBox
            Dim ckBxComplete As New CheckBox
            SaveSyllabus()
            GetLessonDetails()
            CopySyllabus_m()
            'CopySyllabus_m()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Sub SaveSyllabus()
        Dim iReturnvalue As Integer
        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_M]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@SYT_ID", SqlDbType.Int)
            cmd.Parameters("@SYT_ID").Direction = ParameterDirection.InputOutput
            If h_SYT_ID.Value <> 0 Then
                cmd.Parameters("@SYT_ID").Value = Val(h_SYT_ID.Value).ToString
            Else
                cmd.Parameters("@SYT_ID").Value = 0
            End If
            cmd.Parameters.AddWithValue("@SYT_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@SYT_SYD_ID", h_TopicID.Value)
            cmd.Parameters.AddWithValue("@SYT_EMP_ID", h_EMP_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_SGR_ID", h_GRP_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_SBG_ID", h_SUBJ_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_STDT", txtFromDate.Text)
            cmd.Parameters.AddWithValue("@SYT_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@SYT_HRS", Val(txtHrs.Text).ToString)
            cmd.Parameters.AddWithValue("@SYT_REMARKS", 0)
            cmd.Parameters.AddWithValue("@SYT_OBJECTIVE", txtobjective.Text)
            cmd.Parameters.AddWithValue("@SYT_COMPLETED", h_Completed.Value)
            cmd.Parameters.AddWithValue("@SYT_RESOURCE", txtResources.Text)

            If ViewState("datamode") = "add" Then
                cmd.Parameters.AddWithValue("@bEdit", 0)
            Else
                cmd.Parameters.AddWithValue("@bEdit", 1)
            End If
            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.Parameters.AddWithValue("@SYT_USER", Session("susr_name"))
            cmd.ExecuteNonQuery()
            If h_SYT_ID.Value = 0 Then
                h_SYT_ID.Value = cmd.Parameters("@SYT_ID").Value
            End If
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                objConn.Close()
                Exit Sub
            End If

            Dim i As Integer
            Dim lblLesId As Label
            Dim txtComments As TextBox
            Dim lblSylId As Label
            For i = 0 To gvLesson.Rows.Count - 1
                lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
                txtComments = gvLesson.Rows(i).FindControl("txtComments")
                lblSylId = gvLesson.Rows(i).FindControl("lblSylId")

                cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_D]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@SYL_ID", Val(lblSylId.Text).ToString)

                cmd.Parameters.AddWithValue("@SYL_SYT_ID", h_SYT_ID.Value)
                cmd.Parameters.AddWithValue("@SYL_SYD_ID", h_TopicID.Value)
                cmd.Parameters.AddWithValue("@SYL_LES_ID", lblLesId.Text)
                cmd.Parameters.AddWithValue("@SYL_DESCR", txtComments.Text)
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = "Unexpected error"
                    objConn.Close()
                    Exit Sub
                End If
            Next
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Unexpected error"
            objConn.Close()
            Exit Sub
        End Try

        stTrans.Commit()
        lblError.Text = "Lesson Allocated IS Successfully Saved"
        objConn.Close()
    End Sub



    Sub SaveSyllabus_Copied(ByVal sgr_id As String)
        Dim syt_id As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(SYT_ID,0) FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_EMP_ID=" + h_EMP_ID.Value _
                             & " AND SYT_SYD_ID=" + h_TopicID.Value + " AND SYT_SGR_ID=" + sgr_id
        syt_id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
       

        Dim iReturnvalue As Integer
        Dim cmd As New SqlCommand

        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_M]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@SYT_ID", SqlDbType.Int)
            cmd.Parameters("@SYT_ID").Direction = ParameterDirection.InputOutput
            If syt_id <> 0 Then
                cmd.Parameters("@SYT_ID").Value = Val(syt_id).ToString
            Else
                cmd.Parameters("@SYT_ID").Value = 0
            End If
            cmd.Parameters.AddWithValue("@SYT_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@SYT_SYD_ID", h_TopicID.Value)
            cmd.Parameters.AddWithValue("@SYT_EMP_ID", h_EMP_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_SGR_ID", sgr_id)
            cmd.Parameters.AddWithValue("@SYT_SBG_ID", h_SUBJ_ID.Value)
            cmd.Parameters.AddWithValue("@SYT_STDT", txtFromDate.Text)
            cmd.Parameters.AddWithValue("@SYT_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@SYT_HRS", txtHrs.Text)
            cmd.Parameters.AddWithValue("@SYT_REMARKS", 0)
            cmd.Parameters.AddWithValue("@SYT_OBJECTIVE", txtobjective.Text)
            cmd.Parameters.AddWithValue("@SYT_COMPLETED", h_Completed.Value)
            cmd.Parameters.AddWithValue("@SYT_RESOURCE", txtResources.Text)

            If ViewState("datamode") = "add" Then
                cmd.Parameters.AddWithValue("@bEdit", 0)
            Else
                cmd.Parameters.AddWithValue("@bEdit", 1)
            End If
            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.Parameters.AddWithValue("@SYT_USER", Session("susr_name"))
            cmd.ExecuteNonQuery()
            If syt_id = 0 Then
                syt_id = cmd.Parameters("@SYT_ID").Value
            End If
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                objConn.Close()
                Exit Sub
            End If

            Dim i As Integer
            Dim lblLesId As Label
            Dim txtComments As TextBox
            Dim lblSylId As Label
            Dim sylId As String

            For i = 0 To gvLesson.Rows.Count - 1

                lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
                txtComments = gvLesson.Rows(i).FindControl("txtComments")
                lblSylId = gvLesson.Rows(i).FindControl("lblSylId")

                cmd = New SqlCommand("[SYL].[SaveSYLLABUS_TEACHER_D_COPYFROMGROUP]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                '                cmd.Parameters.AddWithValue("@SYL_ID", Val(lblSylId.Text).ToString)

                cmd.Parameters.AddWithValue("@SYL_SYT_ID", syt_id)
                cmd.Parameters.AddWithValue("@SYL_SYD_ID", h_TopicID.Value)
                cmd.Parameters.AddWithValue("@SYL_LES_ID", lblLesId.Text)
                cmd.Parameters.AddWithValue("@SYL_DESCR", txtComments.Text)
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = "Unexpected error"
                    objConn.Close()
                    Exit Sub
                End If
            Next
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Unexpected error"
            objConn.Close()
            Exit Sub
        End Try

        stTrans.Commit()
        lblError.Text = "Lesson Allocated IS Successfully Saved"
        objConn.Close()
    End Sub

    Sub CopySyllabus_m()
        Dim tNode, cNode As TreeNode
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim sgr_id As String = ""
        For Each tNode In tvCopyGroup.Nodes
            For Each cNode In tNode.ChildNodes
                If cNode.Checked = True Then
                    If sgr_id <> "" Then
                        sgr_id += "|"
                    End If
                    sgr_id += cNode.Value.ToString

                    'SaveSyllabus_Copied(sgr_id)
                End If
            Next
        Next
        If sgr_id <> "" Then
            str_query = "exec SYL.COPYLESSONALLOCATION " _
                     & " @SYT_ID=" + h_SYT_ID.Value + "," _
                     & " @SGR_IDS='" + sgr_id + "'," _
                     & " @USER='" + Session("susr_name") + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End If
    End Sub

    


    Protected Sub gvallocation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)

    End Sub
    Sub GetLessonDetails()
        Dim strSQL As String
        Dim ds As DataSet
        Dim dsSub As DataSet
        Dim IntCmplt As Integer = 0
        Dim i, j As Integer
        Dim ldrNew As DataRow
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        strSQL = "SELECT SYT_ID,SYT_BSU_ID,SYT_SYD_ID,SYT_EMP_ID,SYT_SGR_ID,SYT_SBG_ID,SYT_STDT," _
                & " SYT_ENDDT, SYT_HRS, SYT_REMARKS,isnull(SYT_OBJECTIVE,'') SYT_OBJECTIVE, SYT_bCOMPLETE,ISNULL(SYT_RESOURCE,'') SYT_RESOURCE" _
                & " FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_SGR_ID=" + h_GRP_ID.Value _
                & " AND SYT_SYD_ID=" + h_TopicID.Value + " AND SYT_EMP_ID=" + h_EMP_ID.Value

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            h_SYT_ID.Value = ds.Tables(0).Rows(0)("SYT_ID")
            txtFromDate.Text = Format(ds.Tables(0).Rows(0)("SYT_STDT"), "dd/MMM/yyyy")
            txtToDate.Text = Format(ds.Tables(0).Rows(0)("SYT_ENDDT"), "dd/MMM/yyyy")
            txtobjective.Text = ds.Tables(0).Rows(0)("SYT_OBJECTIVE")
            txtHrs.Text = ds.Tables(0).Rows(0)("SYT_HRS")
            txtResources.Text = ds.Tables(0).Rows(0)("SYT_RESOURCE")
        End If

        strSQL = "SELECT SYL_ID,SYL_LES_ID,ISNULL(SYL_DESCR,'') SYL_DESCR FROM SYL.SYLLABUS_TEACHER_D WHERE SYL_SYT_ID=" + h_SYT_ID.Value
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        Dim lblLesId As Label
        Dim txtComments As TextBox
        Dim lblSylId As Label
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To gvLesson.Rows.Count - 1
                lblLesId = gvLesson.Rows(i).FindControl("lblLesId")
                txtComments = gvLesson.Rows(i).FindControl("txtComments")
                lblSylId = gvLesson.Rows(i).FindControl("lblSylId")
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If lblLesId.Text = ds.Tables(0).Rows(j)("SYL_LES_ID") Then
                        If ds.Tables(0).Rows(j)("SYL_DESCR") <> "" Then
                            txtComments.Text = ds.Tables(0).Rows(j)("SYL_DESCR")
                        End If
                        lblSylId.Text = ds.Tables(0).Rows(j)("SYL_ID")
                    End If
                Next
            Next
        End If
        gridbind()
    End Sub

    Sub BindLessonSetup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LES_ID,'&nbsp;'+LES_DESCRIPTION LES_DESCRIPTION,ISNULL(LES_COMMENTS,'') LES_COMMENTS FROM SYL.LESSONSETUP_M WHERE LES_SBG_ID='" + h_SUBJ_ID.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvLesson.DataSource = ds
        gvLesson.DataBind()
        If gvLesson.Rows.Count > 0 Then
            gvLesson.HeaderRow.Visible = False
        End If
    End Sub
    Protected Sub txtSyllabus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GetLessonDetails()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckTopicExists() As Boolean
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        strSql = "SELECT SYT_ID FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_BSU_ID='" & Session("SBsuid") & "' and " _
                & " SYT_SYD_ID='" & h_TopicID.Value & "' and " _
                & " SYT_SGR_ID='" & h_GRP_ID.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            CheckTopicExists = True
        Else
            CheckTopicExists = False
        End If

    End Function
    Sub GetEmpName()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        strSql = "SELECT VW_EMPLOYEE_M.EMP_ID, VW_EMPLOYEE_M.EMP_FNAME + ' ' + VW_EMPLOYEE_M.EMP_MNAME + ' ' + VW_EMPLOYEE_M.EMP_LNAME as empname, " _
                    & " VW_EMPLOYEE_M.EMPNO  FROM GROUPS_TEACHER_S INNER JOIN  VW_EMPLOYEE_M ON GROUPS_TEACHER_S.SGS_EMP_ID = VW_EMPLOYEE_M.EMP_ID " _
                    & " WHERE GROUPS_TEACHER_S.SGS_SGR_ID='" & h_GRP_ID.Value & "' AND " _
                    & " VW_EMPLOYEE_M.EMP_BSU_ID='" & Session("SBsuid") & "' AND " _
                    & " VW_EMPLOYEE_M.EMP_ID='" & h_EMP_ID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtempname.Text = ds.Tables(0).Rows(0)("EMPNAME")
        End If
    End Sub

    Protected Sub txtTopic_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GetPlannedDates()
        GetLessonDetails()
        bind_List()
    End Sub
    Sub GetPlannedDates()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        If txtTopic.Text <> "" And h_TopicID.Value <> "" Then
            strSql = "SELECT SYD_STDT,SYD_ENDDT,SYD_TOT_HRS,ISNULL(SYD_OBJECTIVE,'') SYD_OBJECTIVE,ISNULL(SYD_RESOURCE,'')  SYD_RESOURCE FROM SYL.SYLLABUS_D WHERE SYD_ID='" & h_TopicID.Value & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPlannedStDate.Text = Format(ds.Tables(0).Rows(0)("SYD_STDT"), "dd/MMM/yyyy")
                txtPlannedEndDate.Text = Format(ds.Tables(0).Rows(0)("SYD_ENDDT"), "dd/MMM/yyyy")
                txtobjective.Text = ds.Tables(0).Rows(0)("SYD_OBJECTIVE")
                txtResources.Text = ds.Tables(0).Rows(0)("SYD_RESOURCE")
            End If
        End If
    End Sub

    Protected Sub lnkSubTopics_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim url As String
            Dim lblSytId As New Label
            Dim strSytId As String

            lblSytId = TryCast(sender.FindControl("lblSYTId"), Label)
            If Not lblSytId Is Nothing Then
                strSytId = lblSytId.Text
                ViewState("datamode") = "add"
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                url = String.Format("~\Curriculum\clmSyllabus_ScheduleAllocate.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") & "&ParentId=" & Encr_decrData.Encrypt(h_TopicID.Value) & "&SyllabusId=" & Encr_decrData.Encrypt(h_SyllabusId.Value) & "&SytID=" & strSytId)
                Response.Redirect(url)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub ClearPartialDetails()
        h_GRP_ID.Value = 0
        h_SyllabusId.Value = 0
        h_TopicID.Value = 0
        txtSyllabus.Text = ""
        txtTopic.Text = ""
        txtPlannedStDate.Text = ""
        txtPlannedEndDate.Text = ""

    End Sub

    Protected Sub txtempname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempname.TextChanged
        BindGroup()
        ClearControls()
    End Sub
    Sub bind_List()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "select SDD_DOC_ID as id,SD_FILENAME as FNAME  from syl.SYLLABUS_D_DOC inner join OASIS_DOCS.dbo.SYLLABUS_DOC " _
                                  & " on SD_ID=SDD_DOC_ID where SDD_SYD_ID =" + h_TopicID.Value




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlDocs.DataSource = ds
      
        dlDocs.DataBind()
    End Sub
    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub
    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_DOCSConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("sd_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("sd_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("sd_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub dlDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim txtfile As LinkButton = e.Item.FindControl("txtfile")
                Dim img As ImageButton = e.Item.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".xlsx" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".docx" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.png"
                Else
                    img.ImageUrl = "~/Curriculum/images/img.png"
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnAddDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDocument.Click
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim filebyte As Byte() = Nothing
        Dim str_query As String = ""
        Dim FilePath As String = ""
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("SYL.SaveSYLLABUS_TEACH_DOC", objConn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@STD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@STD_GRD_ID", h_GRP_ID.Value)
            cmd.Parameters.AddWithValue("@STD_SBM_ID", h_SUBJ_ID.Value)
            cmd.Parameters.AddWithValue("@STD_SYD_ID", h_TopicID.Value)
            cmd.Parameters.AddWithValue("@STD_EMP_ID", h_EMP_ID.Value)

            If fp.HasFile Then
                Dim p As SqlParameter

                FilePath = Server.MapPath("~/Curriculum/ReportDownloads") & "/" & fp.FileName
                fp.SaveAs(FilePath)
                filebyte = System.IO.File.ReadAllBytes(FilePath)

                p = cmd.Parameters.AddWithValue("@STD_DOC", filebyte)
                'p.Value = SqlDbType.Binary
                cmd.Parameters.AddWithValue("@STD_FILENAME", fp.FileName)
                cmd.Parameters.AddWithValue("@STD_CONTENT", fp.PostedFile.ContentType)

            End If


            cmd.ExecuteNonQuery()




            gridbind()
            File.Delete(FilePath)
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub
    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_DOCSConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
         

            str_Sql = "select STD_ID,STD_FILENAME from OASIS_DOCS.dbo.SYLLABUS_TEACH_DOC  " _
                      & " where STD_BSU_ID='" + Session("sBsuid") + "' AND STD_EMP_ID=" + h_EMP_ID.Value + " and STD_SYD_ID=" + h_TopicID.Value

        
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdTeachDoc.DataSource = ds.Tables(0)
                grdTeachDoc.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdTeachDoc.DataSource = ds.Tables(0)
                Try
                    grdTeachDoc.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdTeachDoc.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdTeachDoc.Rows(0).Cells.Clear()
                grdTeachDoc.Rows(0).Cells.Add(New TableCell)
                grdTeachDoc.Rows(0).Cells(0).ColumnSpan = columnCount
                grdTeachDoc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdTeachDoc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub grdTeachDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdTeachDoc.PageIndexChanging
        grdTeachDoc.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub txtDocName_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select std_fileName, std_Content, std_doc from SYLLABUS_TEACH_DOC where std_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download_Doc(dt)

        End If

    End Sub
    Protected Sub imgD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select std_fileName, std_Content, std_doc from SYLLABUS_TEACH_DOC where std_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download_Doc(dt)

        End If
    End Sub

    Protected Sub grdTeachDoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTeachDoc.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtDocName")
                Dim img As ImageButton = e.Row.FindControl("imgD")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".xlsx" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".docx" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.png"
                Else
                    img.ImageUrl = "~/Curriculum/images/img.png"
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub grdTeachDoc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTeachDoc.RowDeleting
        Try
            grdTeachDoc.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdTeachDoc.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lb_id"), Label)

            If lblID.Text <> "" Then
               
                Dim str_conn As String = ConnectionManger.GetOASIS_DOCSConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM SYLLABUS_TEACH_DOC where STD_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If

            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub download_Doc(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("std_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("std_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("std_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
End Class