Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Collections.Generic
Partial Class Curriculum_clmNewStudentsAllocate_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnAllocate)

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100090") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                    PopulateGradeStream()
                    PopulateSubject()
                    PopulateTeacher()

                    ViewState("stumode") = "add"
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()

                    groupTable.Rows(4).Visible = False
                    groupTable.Rows(5).Visible = False
                    groupTable.Rows(6).Visible = False

                    Session("liUserList") = New List(Of String)
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub

    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvStud.PageSize * gvStud.PageIndex)
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


    Public Sub PopulateGradeStream()

        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "' and grm_shf_id=" + ddlShift.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If

        

    End Sub
    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String
        Dim empTeacher As Boolean = studClass.isEmpTeacher(empId)
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                           & " EMP_ID FROM EMPLOYEE_M AS A " _
                                           & " INNER JOIN GROUPS_TEACHER_S AS B ON A.EMP_ID=B.SGS_EMP_ID" _
                                           & " INNER JOIN GROUPS_M AS C ON B.SGS_SGR_ID=C.SGR_ID " _
                                           & " WHERE EMP_ECT_ID IN(1,4) AND ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01'" _
                                           & " AND '" + Format(Now.Date, "yyyy-MM-dd") + "'" _
                                           & " AND EMP_STATUS=1 AND SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                           & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                           & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                           & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString
            If ddlSubject.SelectedValue.ToString <> "0" Then
                str_query += " AND SGR_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
            End If
            str_query += " order by emp_fname,emp_mname,emp_lname"
        ElseIf empTeacher = True Then
            str_query = "SELECT DISTINCT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                               & " EMP_ID FROM EMPLOYEE_M " _
                               & " WHERE EMP_ID=" + empId.ToString

        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlTeacher.Items.Insert(0, li)

    End Sub

    Sub PopulateSubject()
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND ISNULL(SBG_bOPTIONAL,0)=0"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

    End Sub
    Sub PopulateGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ddlTeacher.SelectedValue.ToString <> "0" Then

            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                      & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID" _
                      & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGR_SBG_ID=C.SBG_ID AND ISNULL(SBG_bOPTIONAL,0)=0 " _
                      & " WHERE SGS_EMP_ID=" + hfEMP_ID.Value _
                      & " AND ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01'" _
                      & " AND '" + Format(Now.Date, "yyyy-MM-dd") + "'" _
                      & " AND SGR_GRD_ID='" + hfGRD_ID.Value + "'" _
                      & " AND SGR_ACD_ID=" + hfACD_ID.Value _
                      & " AND SGR_SHF_ID=" + hfSHF_ID.Value _
                      & " AND SGR_STM_ID=" + hfSTM_ID.Value

            If ddlSubject.SelectedValue.ToString <> "0" Then
                str_query += " AND SGR_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
            End If

        ElseIf ddlSubject.SelectedValue.ToString <> "0" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE SGR_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Else
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND ISNULL(SBG_bOPTIONAL,0)=0  "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGroup.DataSource = ds
        lstGroup.DataTextField = "SGR_DESCR"
        lstGroup.DataValueField = "SGR_ID"
        lstGroup.DataBind()
    End Sub

    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String
        Dim i As Integer
        'students for which groups for all common subjects are not alloted has to be populated

        Dim comSubjects As Integer

        strQuery = "SELECT SUM(CASE ISNULL(SBM_DESCR,'') WHEN 'ISLAMIC STUDIES' THEN 0 ELSE 1 END) FROM SUBJECTS_GRADE_S AS A " _
                  & " INNER JOIN SUBJECT_M AS B ON A.SBG_SBM_ID=B.SBM_ID " _
                  & " WHERE  SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
                  & " AND SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_STM_ID=" + hfSTM_ID.Value _
                  & " AND SBG_bOPTIONAL='FALSE' AND ISNULL(SBG_bAUTOGROUP,'FALSE')='TRUE'"
        comSubjects = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        strQuery = "SELECT STU_ID,SCT_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                      & " SCT_DESCR ,SUBJECTS=ISNULL((SELECT STUFF((SELECT ', '+SBG_DESCR FROM SUBJECTS_GRADE_S AS D" _
                      & " INNER JOIN  STUDENT_GROUPS_S AS G ON G.SSD_SBG_ID=D.SBG_ID " _
                      & " AND G.SSD_STU_ID=A.STU_ID AND D.SBG_bOPTIONAL='FALSE' AND G.SSD_SGR_ID IS NOT NULL AND G.SSD_ACD_ID=" + hfACD_ID.Value _
                      & " for xml path('')),1,1,'')) ,'')" _
                      & " FROM STUDENT_M  AS A " _
                      & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                      & " WHERE STU_bACTIVE='TRUE' AND STU_ACD_ID=" + hfACD_ID.Value _
                      & " AND STU_GRD_ID='" + hfGRD_ID.Value + "'" _
                      & " AND STU_STM_ID=" + hfSTM_ID.Value _
                      & " AND (SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID " _
                      & " AND SSD_ACD_ID=" + hfACD_ID.Value + " AND SSD_OPT_ID IS NULL AND SSD_SGR_ID IS NOT NULL)<" + comSubjects.ToString _
                      & " AND STU_CURRSTATUS NOT IN('CN','TF')"





        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox


        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""

        Dim sectionItems As ListItemCollection

        If gvStud.Rows.Count > 0 Then

            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
                sectionItems = ddlgvSection.Items
            End If


            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        strQuery += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            'If ViewState("stumode") = "view" Then
            '    btnRemove.Visible = False
            'Else
            '    btnAllocate.Visible = False
            'End If
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("stumode") = "view" Then
                gvStud.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvStud.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvStud.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvStud.Rows.Count > 0 Then


            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            Dim dr As DataRow

            If selectedSection = "" Then
                ddlgvSection.Items.Clear()
                ddlgvSection.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr

                        If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                            ddlgvSection.Items.Add(.Item(4))
                        End If
                    End With

                Next
            Else
                ddlgvSection.Items.Clear()
                For i = 0 To sectionItems.Count - 1
                    ddlgvSection.Items.Add(sectionItems.Item(i))
                Next
                ddlgvSection.Text = selectedSection
            End If


        End If
        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim groupXML As String = GetGroupXML()
        If groupXML = "" Then
            'UserMsgBox("Please select a group", Me)
            lblError.Text = "Please select a group"
            Exit Sub
        End If

        Dim stuXML As String = GetStudXML()
        If stuXML = "" Then
            'UserMsgBox("Please select a student", Me)
            lblError.Text = "Please select a student"
            Exit Sub
        End If


        Dim str_query As String = "exec saveNEWSTUDENTGROUP " _
                                 & "'" + stuXML + "'," _
                                 & "'" + groupXML + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub
    Public Sub UserMsgBox(ByVal sMsg As String, ByVal frmMe As Object)

        Dim sb As New StringBuilder()
        Dim oFormObject As New System.Web.UI.Control
        If sMsg = "" Then
            Exit Sub
        End If
        sMsg = sMsg.Replace("'", "\'")
        sMsg = sMsg.Replace(Chr(34), "\" & Chr(34))
        sMsg = sMsg.Replace(vbCrLf, "\n")
        sMsg = "<script language=javascript>alert(""" & sMsg & """)</script>"

        sb = New StringBuilder()
        sb.Append(sMsg)

        For Each oFormObject In frmMe.Controls
            If TypeOf oFormObject Is HtmlForm Then
                Exit For
            End If
        Next

        ' Add the javascript after the form object so that the 
        ' message doesn't appear on a blank screen.
        oFormObject.Controls.AddAt(oFormObject.Controls.Count, New LiteralControl(sb.ToString()))

    End Sub

    Function GetStudXML() As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblSctId As Label
        Dim str As String = ""
        With gvStud
            For i = 0 To .Rows.Count - 1
                chkSelect = .Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblStuId = .Rows(i).FindControl("lblStuid")
                    lblSctId = .Rows(i).FindControl("lblSctId")
                    str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID><SCT_ID>" + lblSctId.Text + "</SCT_ID></ID>"
                End If
            Next
        End With
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>"
        Else
            Return ""
        End If
    End Function


    Function GetGroupXML() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                str += "<ID><SGR_ID>" + lstGroup.Items(i).Value.ToString + "</SGR_ID></ID>"
            End If
        Next

        'Return "<IDS>" + str + "</IDS>"
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>"
        Else
            Return ""
        End If
    End Function

#End Region

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
            hfGRD_ID.Value = ddlGrade.SelectedValue
            hfSTM_ID.Value = ddlStream.SelectedValue.ToString
            hfSHF_ID.Value = ddlShift.SelectedValue.ToString
            hfEMP_ID.Value = ddlTeacher.SelectedValue.ToString
            PopulateGroup()
            gvStud.DataBind()
            GridBind()
            groupTable.Rows(4).Visible = True
            groupTable.Rows(5).Visible = True
            groupTable.Rows(6).Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            PopulateGradeStream()
            PopulateSubject()
            PopulateTeacher()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            PopulateGradeStream()
            PopulateSubject()
            PopulateTeacher()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            PopulateGradeStream()
            PopulateTeacher()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            PopulateTeacher()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  
    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click
        Me.MPE1.Show()
    End Sub
    Protected Sub btnOkay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkay.Click
        Try
            SaveData()
            lstGroup.ClearSelection()
            Session("liUserList") = New List(Of String)

            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim i As Integer
            If chkSelectAll.Checked = True Then
                For i = 0 To lstGroup.Items.Count - 1
                    lstGroup.Items(i).Selected = True
                Next
            Else
                lstGroup.ClearSelection()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        PopulateTeacher()
    End Sub
End Class
