Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CURRICULUM

Partial Class clmGradeSlabMasterView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_GRADESLAB_MASTER Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                hlAddNew.NavigateUrl = "clmGradeSlabMaster.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                GridBind()
                gvGradeSlab.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGradeSlab.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvGradeSlab.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            Dim txtSearch2 As New TextBox
            Dim str_search As String
            Dim str_filter_CAM_DESC As String = String.Empty
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvGradeSlab.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                '   --- FILTER CONDITIONS ---
                
                '  --- txtCounterDescr Search
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvGradeSlab.HeaderRow.FindControl("txtCounterDescr")
                '  --- txtUser
                txtSearch2 = gvGradeSlab.HeaderRow.FindControl("txtUser")
                
            End If

            Dim str_cond As String = String.Empty
            str_cond = " GSM_BSU_ID = '" & Session("sBSUID") & "'"
            str_Sql = "SELECT GSM_SLAB_ID, GSM_DESC, GSM_BSU_ID, GSM_TOT_MARK,CASE  ISNULL(GSM_bDISPLAY,'TRUE') WHEN 'TRUE' THEN 'FALSE' ELSE 'TRUE' END AS bSUPRESS " & _
            " FROM ACT.GRADING_SLAB_M WHERE " & str_cond & str_Filter


            If txtSearch.Text <> "" Then
                If str_search = "LI" Then
                    str_filter_CAM_DESC = " AND GSM_DESC LIKE '%" & txtSearch.Text & "%'"

                ElseIf str_search = "NLI" Then
                    str_filter_CAM_DESC = "  AND  NOT GSM_DESC LIKE '%" & txtSearch.Text & "%'"

                ElseIf str_search = "SW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC NOT LIKE '" & txtSearch.Text & "%'"

                ElseIf str_search = "EW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC LIKE  '%" & txtSearch.Text & "'"

                ElseIf str_search = "NEW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC NOT LIKE '%" & txtSearch.Text & "'"

                End If
            End If
            If txtSearch2.Text <> "" Then
                If str_search = "LI" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK LIKE '%" & txtSearch2.Text & "%'"

                ElseIf str_search = "NLI" Then
                    str_filter_CAM_DESC = "  AND  NOT GSM_TOT_MARK LIKE '%" & txtSearch2.Text & "%'"

                ElseIf str_search = "SW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK  LIKE '" & txtSearch2.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK NOT LIKE '" & txtSearch2.Text & "%'"

                ElseIf str_search = "EW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK LIKE  '%" & txtSearch2.Text & "'"

                ElseIf str_search = "NEW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK NOT LIKE '%" & txtSearch2.Text & "'"

                End If
            End If

            str_filter_CAM_DESC += " ORDER BY GSM_TOT_MARK,GSM_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAM_DESC)
            gvGradeSlab.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGradeSlab.DataBind()
                Dim columnCount As Integer = gvGradeSlab.Rows(0).Cells.Count

                gvGradeSlab.Rows(0).Cells.Clear()
                gvGradeSlab.Rows(0).Cells.Add(New TableCell)
                gvGradeSlab.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGradeSlab.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGradeSlab.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                
                gvGradeSlab.DataBind()
            End If
            'gvGradeSlab.DataBind()
            'txtSearch = gvGradeSlab.HeaderRow.FindControl("txtBSUNAME")
            'txtSearch.Text = lstrCondn1

            'txtSearch = gvGradeSlab.HeaderRow.FindControl("txtCounterDescr")
            'txtSearch.Text = lstrCondn2

            'txtSearch = gvGradeSlab.HeaderRow.FindControl("txtUser")
            'txtSearch.Text = lstrCondn3

            'txtSearch = gvGradeSlab.HeaderRow.FindControl("txtTDate")
            'txtSearch.Text = lstrCondn4

            'txtSearch = gvGradeSlab.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub SupressGradeSlab()
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblGSM_SLAB_ID As Label
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        For i = 0 To gvGradeSlab.Rows.Count - 1
            chkSelect = gvGradeSlab.Rows(i).FindControl("chkSelect")
            lblGSM_SLAB_ID = gvGradeSlab.Rows(i).FindControl("lblGSM_SLAB_ID")
            str_query = "exec ACT.SUPRESSGRADINGSLAB " _
                     & "@GSM_SLAB_ID=" + lblGSM_SLAB_ID.Text + "," _
                     & "@bDISPLAY='" + IIf(chkSelect.Checked, False, True).ToString + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next

    End Sub

    Protected Sub gvGradeSlab_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGradeSlab.PageIndexChanging
        gvGradeSlab.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvGradeSlab_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGradeSlab.RowDataBound
        Try
            Dim lblGSM_SLAB_ID As New Label
            lblGSM_SLAB_ID = TryCast(e.Row.FindControl("lblGSM_SLAB_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblGSM_SLAB_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "clmGradeSlabMaster.aspx?GSM_SLAB_ID=" & Encr_decrData.Encrypt(lblGSM_SLAB_ID.Text) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnSuppress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSuppress.Click
        SupressGradeSlab()
    End Sub
End Class
