<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ClmCategory.aspx.vb" Inherits="Curriculum_ClmCategory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<base  target="_self" />
    <title>Untitled Page</title>
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../cssfiles/tabber.js"></script> 
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
            cellspacing="0" style="width: 397px">
            <tr class="title">
                <td align="left" style="height: 31px">
                    Set Comments Category</td>
            </tr>
            <tr>
                <td style="height: 79px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                        ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                        Style="text-align: left" ValidationGroup="groupM1" Width="316px" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" style="height: 20px" valign="bottom">
                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                        Height="24px" SkinID="error" Style="text-align: center" Width="133px"></asp:Label></td>
            </tr>
            <tr>
                <td class="matters" style="font-weight: bold; height: 93px" valign="top">
                    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                        class="BlueTableView" style="width: 348px">
                        <tr class="subheader_img">
                            <td align="left" colspan="9" valign="middle">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                    Comments Category</span></font></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters" style="width: 366px">
                            </td>
                            <td align="left" class="matters" colspan="4">
                                <asp:CheckBox ID="chkGrade" runat="server" Text="Grade Wise Category" /></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters" style="width: 366px">
                                <asp:Label ID="Label2" runat="server" Text="Category Name" Width="95px" SkinID="lblhome"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Red"
                                    Height="12px" Text="*" Width="14px"></asp:Label></td>
                            <td align="left" class="matters" colspan="4">
                                <asp:TextBox ID="txtCategory" runat="server" Width="283px" SkinID="TextBoxNormal"></asp:TextBox></td>
                        </tr>
                        <tr class="subheader_img">
                            <td align="left" colspan="6" style="font-family: Verdana">
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="matters" style="height: 19px" valign="bottom" align="center">
                    &nbsp;
                    <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                        TabIndex="7" Text="Save" ValidationGroup="groupM1" Width="45px" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                        OnClick="btnCancel_Click" TabIndex="8" Text="Close" UseSubmitBehavior="False"
                        Width="44px" />
                    </td>
            </tr>
            <tr>
                <td class="matters" style="height: 52px" valign="bottom" align="center">
                    <asp:HiddenField ID="H_CMT_ID" runat="server" />
                    &nbsp; &nbsp;
                    <asp:RequiredFieldValidator ID="frsComments" runat="server" ControlToValidate="txtCategory"
                        Display="None" ErrorMessage="Please enter the field Categoiry" ValidationGroup="groupM1"></asp:RequiredFieldValidator></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
