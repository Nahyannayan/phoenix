Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Partial Class Curriculum_clmSubjectCriteria
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100190") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Session("CurrSuperUser") = "Y"
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    BindCategory()
                    CheckSkillsStatus()
                    BindDefaultMaster()
                    GetRows()
                    ViewState("SelectedRow") = -1
                    gvCriteria.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub CheckSkillsStatus()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bSkills,0) from rpt.REPORT_SETUP_M WHERE RSM_ID=" + ddlReportCard.SelectedValue
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows(0).Item(0) <> 1 Then
            trSkills.Visible = False
        Else
            trSkills.Visible = True
        End If

    End Sub

    Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBT_ID,SBT_DESCR FROM SUBJECT_REPORT_CATEGORY WHERE " _
                               & " SBT_SBG_ID=" + ddlSubject.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "SBT_DESCR"
        ddlCategory.DataValueField = "SBT_ID"
        ddlCategory.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)

        If ddlCategory.Items.Count = 1 Then
            trCat.Visible = False
        Else
            trCat.Visible = True
        End If


    End Sub

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If Session("sbsuid") = "123004" And Session("clm") = 3 Then
            str_query += " AND ISNULL(RSM_bCRITERIA,0)=1"
        End If
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()


    End Sub

    Sub BindDefaultMaster()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RDM_ID,RDM_DESCR FROM RPT.DEFAULTBANK_M WHERE RDM_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDefaults.DataSource = ds
        ddlDefaults.DataTextField = "RDM_DESCR"
        ddlDefaults.DataValueField = "RDM_ID"
        ddlDefaults.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = ""
        ddlDefaults.Items.Insert(0, li)
    End Sub

    Sub BindDefaulText(ByVal rsd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSP_DESCR,RSP_TEXT,RSP_RSD_ID FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSD_ID=" + rsd_id _
                                & " ORDER BY RSP_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvDefaults.DataSource = ds
        gvDefaults.DataBind()
    End Sub

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RDM_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_HEADER"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_SUB_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_DISPLAYORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.Boolean")
        column.ColumnName = "bENABLE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBT_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBT_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_SUB_SKILLS"
        dt.Columns.Add(column)
        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub AddRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtCriteria")
        Dim keys As Object()
        ReDim keys(0)

        keys(0) = txtShort.Text

        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This subject criteria is already added"
            Exit Sub
        End If

        dr = dt.NewRow

        dr.Item(0) = 0
        dr.Item(1) = ddlDefaults.SelectedValue
        dr.Item(2) = txtShort.Text
        dr.Item(3) = txtDetail.Text.Replace("'", "''")
        dr.Item(4) = txtOrder.Text
        dr.Item(5) = "add"
        dr.Item(6) = False
        dr.Item(7) = ddlCategory.SelectedValue.ToString
        dr.Item(8) = ddlCategory.SelectedItem.Text
        dr.Item(9) = ddlSkills.SelectedItem.Text
        dt.Rows.Add(dr)
        Session("dtCriteria") = dt
        GridBind(dt)

    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(5) <> "delete" And dt.Rows(i)(5) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvCriteria.DataSource = dtTemp
        gvCriteria.DataBind()
        If trSkills.Visible = False Then
            gvCriteria.Columns(8).Visible = False
        Else
            gvCriteria.Columns(8).Visible = True
        End If
    End Sub

    Sub GridBindPOI(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetPOIDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(9) <> "delete" And dt.Rows(i)(9) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvPOI.DataSource = dtTemp
        gvPOI.DataBind()
    End Sub

    Sub BindDefaultValues(ByVal rdm_id As String, ByVal ddlDefaultValues As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RDD_ID,RDD_DESCR FROM RPT.DEFAULTBANK_D WHERE RDD_RDM_ID=" + rdm_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDefaultValues.DataSource = ds
        ddlDefaultValues.DataTextField = "RDD_DESCR"
        ddlDefaultValues.DataValueField = "RDD_ID"
        ddlDefaultValues.DataBind()
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtCriteria")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(1) = ddlDefaults.SelectedValue
                .Item(2) = txtShort.Text
                .Item(3) = txtDetail.Text.Replace("'", "''")
                .Item(4) = txtOrder.Text
                If .Item(5) = "edit" Then
                    .Item(5) = "update"
                End If
                .Item(6) = False
                .Item(7) = ddlCategory.SelectedValue.ToString
                .Item(8) = ddlCategory.SelectedItem.Text
                .Item(9) = ddlSkills.SelectedItem.Text
            End With
            Session("dtCriteria") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub GetRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("sbsuid") = "125017" And (ddlSubject.SelectedItem.Text.ToUpper = "INFORMATION TECHNOLOGY" Or ddlSubject.SelectedItem.Text.ToUpper = "LIBRARY" Or ddlSubject.SelectedItem.Text.ToUpper = "VISUAL ARTS") Then
            str_query = "SELECT RSD_ID,isnull(RSD_RDM_ID,0),RSD_HEADER,isnull(RSD_SUB_DESC,''),ISNULL(RSD_DISPLAYORDER,1),bENABLE='TRUE',RSD_SUB_SKILLS FROM " _
                    & " RPT.REPORT_SETUP_D WHERE RSD_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                    & " AND RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        ElseIf ddlSubject.SelectedItem.Text.ToUpper = "PROGRAMME OF INQUIRY" Or ddlSubject.SelectedItem.Text.ToUpper = "CURRENT PROGRAMME OF INQUIRY" Then
            str_query = "SELECT RSD_ID,isnull(RSD_RDM_ID,0),RSD_HEADER,isnull(RSD_SUB_DESC,''),ISNULL(RSD_DISPLAYORDER,1),bENABLE='TRUE',RSD_SUB_SKILLS FROM " _
                     & " RPT.REPORT_SETUP_D WHERE RSD_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                     & " AND RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        ElseIf Session("sbsuid") = "223006" Then
            str_query = "SELECT RSD_ID,isnull(RSD_RDM_ID,0),RSD_HEADER,isnull(RSD_SUB_DESC,''),ISNULL(RSD_DISPLAYORDER,1),bENABLE='" + IIf((Session("SBSUID") = "223006"), "true", "FALSE") + "',ISNULL(SBT_ID,0),ISNULL(SBT_DESCR,''),RSD_SUB_SKILLS FROM " _
                  & " RPT.REPORT_SETUP_D LEFT OUTER JOIN SUBJECT_REPORT_CATEGORY ON RSD_SBG_CATEGORY_ID=SBT_ID WHERE RSD_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                  & " AND RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        Else
            str_query = "SELECT RSD_ID,isnull(RSD_RDM_ID,0),RSD_HEADER,isnull(RSD_SUB_DESC,''),ISNULL(RSD_DISPLAYORDER,1),bENABLE='" + IIf((Session("SBSUID") = "123016"), "true", "FALSE") + "',ISNULL(SBT_ID,0),ISNULL(SBT_DESCR,''),RSD_SUB_SKILLS FROM " _
                     & " RPT.REPORT_SETUP_D LEFT OUTER JOIN SUBJECT_REPORT_CATEGORY ON RSD_SBG_CATEGORY_ID=SBT_ID WHERE RSD_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                     & " AND RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        End If

        Session("dtCriteria") = SetDataTable()
        dt = Session("dtCriteria")
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                dr = dt.NewRow
                dr.Item(0) = .GetValue(0)
                dr.Item(1) = .GetValue(1)
                dr.Item(2) = .GetString(2)
                dr.Item(3) = .GetString(3)
                dr.Item(4) = .GetValue(4)
                dr.Item(5) = "edit"
                dr.Item(6) = CType(.GetString(5), Boolean)
                If ddlSubject.SelectedItem.Text.ToUpper = "PROGRAMME OF INQUIRY" Or ddlSubject.SelectedItem.Text.ToUpper = "CURRENT PROGRAMME OF INQUIRY" Then
                    dr.Item(7) = 0
                    dr.Item(8) = ""
                ElseIf (ddlSubject.SelectedItem.Text.ToUpper = "INFORMATION TECHNOLOGY" Or ddlSubject.SelectedItem.Text.ToUpper = "LIBRARY" Or ddlSubject.SelectedItem.Text.ToUpper = "VISUAL ARTS") And Session("sbsuid") = "125017" Then
                    dr.Item(7) = 0
                    dr.Item(8) = ""
                    dr.Item(9) = .GetValue(6)
                Else
                    dr.Item(7) = .GetValue(6)
                    dr.Item(8) = .GetString(7)
                    dr.Item(9) = .GetValue(8)
                End If

                dt.Rows.Add(dr)
            End With
        End While
        reader.Close()

        gvCriteria.DataSource = dt
        gvCriteria.DataBind()
        Session("dtCriteria") = dt

        If ddlCategory.Items.Count = 1 Then
            gvCriteria.Columns(5).Visible = False
        Else
            gvCriteria.Columns(5).Visible = True
        End If
        If trSkills.Visible = False Then
            gvCriteria.Columns(8).Visible = False
        Else
            gvCriteria.Columns(8).Visible = True
        End If
    End Sub

    Sub SaveData()
        Dim str_query As String

        Dim dt As DataTable
        dt = Session("dtCriteria")
        Dim dr As DataRow
        Dim RSD_ID As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If .Item(5) <> "delete" And .Item(5) <> "edit" Then
                            str_query = " exec RPT.saveSUBJECTCRITERIA " _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & dr.Item(0) + "," _
                            & ddlSubject.SelectedValue.ToString + "," _
                            & IIf(dr.Item(1) = "", "NULL", dr.Item(1)) + "," _
                            & "N'" + dr.Item(2) + "'," _
                            & "N'" + dr.Item(3) + "'," _
                            & dr.Item(4) + "," _
                            & IIf(dr.Item(5) = "", "NULL", "'" + dr.Item(5) + "'") + "," _
                            & dr.Item(7).ToString + "," _
                            & "N'" + dr.Item(9).ToString + "'," _
                            & ddlCategory.SelectedValue

                            RSD_ID = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)


                        End If
                    End With
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub ClearControls()
        txtDetail.Text = ""
        txtOrder.Text = ""
        txtShort.Text = ""
    End Sub

    Sub EnableDisableControls(ByVal Value As Boolean)
        ddlReportCard.Enabled = Value
        ddlAcademicYear.Enabled = Value
        ddlGrade.Enabled = Value
        ddlSubject.Enabled = Value
    End Sub

    'Sub BindPrintedFor()
    '    ddlPrintedfor.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlPrintedfor.DataSource = ds
    '    ddlPrintedfor.DataTextField = "RPF_DESCR"
    '    ddlPrintedfor.DataValueField = "RPF_ID"
    '    ddlPrintedfor.DataBind()
    'End Sub

    Sub BindPrntedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                  & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlPrintedFor.Items.Insert(0, li)
    End Sub

    Function SetPOIDataTable()
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPF_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPF_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPD_DESCR1"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPD_DESCR2"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPD_DISPLAYORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RPD_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub GetPOI(ByVal rsd_id As String, ByVal sbg_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_ID,RPF_DESCR,RPD_DESCR1,RPD_DESCR2,RPD_DISPLAYORDER,RPD_ID FROM " _
                            & " RPT.REPORT_PRINTEDFOR_M AS A INNER JOIN RPT.REPORT_SCHEDULE_HEADER_D AS B " _
                            & " ON A.RPF_ID=B.RPD_RPF_ID WHERE RPD_RSD_ID=" + rsd_id + " AND RPD_SBG_ID=" + sbg_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = Session("POITABLE")
        Dim dr As DataRow


        Dim i As Integer
        ViewState("SRNO") = 1
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item("ID") = ViewState("SRNO")
                dr.Item("RSD_ID") = hfRSD_ID.Value
                dr.Item("RPF_ID") = .Item(0).ToString
                dr.Item("SBG_ID") = sbg_id
                dr.Item("RPF_DESCR") = .Item(1)
                dr.Item("RPD_DESCR1") = .Item(2)
                dr.Item("RPD_DESCR2") = .Item(3)
                dr.Item("RPD_DISPLAYORDER") = .Item(4)
                dr.Item("MODE") = "edit"
                dr.Item("RPD_ID") = .Item(5)
                dt.Rows.Add(dr)
                ViewState("SRNO") += 1
            End With
        Next

        gvPOI.DataSource = dt
        gvPOI.DataBind()
        Session("POITABLE") = dt
    End Sub

    Sub AddPOI()
        Try
            Dim i As Integer
            Dim rDt As DataRow

            rDt = Session("POITABLE").NewRow
            rDt("ID") = CInt(ViewState("SRNO"))
            rDt("RPD_ID") = 0
            rDt("RPF_DESCR") = ddlPrintedFor.SelectedItem.Text
            rDt("RPD_DESCR1") = txtDetail1.Text
            rDt("RPD_DESCR2") = txtDetail2.Text
            rDt("RPF_ID") = ddlPrintedFor.SelectedValue.ToString
            rDt("RSD_ID") = hfRSD_ID.Value
            rDt("SBG_ID") = ddlSubject.SelectedValue.ToString
            rDt("RPD_DISPLAYORDER") = txtPOIOrder.Text
            rDt("MODE") = "add"
            Session("POITABLE").rows.add(rDt)
            ViewState("SRNO") = ViewState("SRNO") + 1
            GridBindPOI(Session("POITABLE"))
            gvPOI.Columns(4).Visible = True
            gvPOI.Columns(5).Visible = True
        Catch ex As Exception
            lblError.Text = "Request couldnot be processed"
        End Try
    End Sub

    Sub EditPOI()
        Dim row As GridViewRow = gvPOI.Rows(gvPOI.SelectedIndex)

        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        'For iEdit = 0 To Session("BRAND_DETAIL").Rows.Count - 1
        'Next
        For iEdit = 0 To Session("POITABLE").Rows.Count - 1
            If iIndex = Session("POITABLE").Rows(iEdit)("ID") Then
                Session("POITABLE").Rows(iEdit)("RPF_DESCR") = ddlPrintedFor.SelectedItem.Text
                Session("POITABLE").Rows(iEdit)("RPD_DESCR1") = txtDetail1.Text
                Session("POITABLE").Rows(iEdit)("RPD_DESCR2") = txtDetail2.Text
                Session("POITABLE").Rows(iEdit)("RPF_ID") = ddlPrintedFor.SelectedValue.ToString
                Session("POITABLE").Rows(iEdit)("RPD_DISPLAYORDER") = txtPOIOrder.Text
                If Session("POITABLE").Rows(iEdit)("MODE") = "edit" Then
                    Session("POITABLE").Rows(iEdit)("MODE") = "update"
                End If
                Exit For
            End If
        Next

        gvPOI.Columns(4).Visible = True
        gvPOI.Columns(5).Visible = True


        btnStudSave.Enabled = True
        btnAddDetail.Visible = True
        btnUpdateDetail.Visible = False
        gvPOI.SelectedIndex = -1
        GridBindPOI(Session("POITABLE"))
    End Sub

    Sub SavePOI()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String
        Dim dt As DataTable = Session("POITABLE")

        Dim i As Integer

        For i = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("MODE") <> "edit" Then
                str_query = "RPT.savePOI  " _
                        & dt.Rows(i)("RPD_ID") + "," _
                        & hfRSD_ID.Value.ToString + "," _
                        & dt.Rows(i)("RPF_ID") + "," _
                        & ddlSubject.SelectedValue.ToString + "," _
                        & "'" + dt.Rows(i)("RPD_DESCR1").ToString.Replace("'", "''") + "'," _
                        & "'" + dt.Rows(i)("RPD_DESCR2").ToString.Replace("'", "''") + "'," _
                        & dt.Rows(i)("RPD_DISPLAYORDER") + "," _
                        & "'" + dt.Rows(i)("MODE") + "'"
                dt.Rows(i)("RPD_ID") = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next
    End Sub

    Sub SaveDefaultDetails()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim pParms(2) As SqlClient.SqlParameter
        Dim lblRsdId As Label
        Dim lblGrade As Label
        Dim txtDescr As TextBox
        For i = 0 To gvDefaults.Rows.Count - 1
            With gvDefaults.Rows(i)
                lblRsdId = .FindControl("lblRsdId")
                lblGrade = .FindControl("lblGrade")
                txtDescr = .FindControl("txtDescr")
            End With
            pParms(0) = New SqlClient.SqlParameter("@RSD_ID", lblRsdId.Text)
            pParms(1) = New SqlClient.SqlParameter("@RSP_DESCR", lblGrade.Text)
            pParms(2) = New SqlClient.SqlParameter("@RSP_TEXT", txtDescr.Text)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "RPT.UPDTAE_DEFAULTTEXT", pParms)
        Next
    End Sub
#End Region


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindCategory()
        BindDefaultMaster()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindCategory()
        GetRows()
    End Sub

    Protected Sub btnAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCriteria.Click
        If ViewState("SelectedRow") = -1 Then
            AddRows()
        Else
            EditRows()
        End If
        EnableDisableControls(False)
        ClearControls()

        ViewState("SelectedRow") = -1
        btnAddCriteria.Text = "Add Criteria"

    End Sub

    Protected Sub gvCriteria_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCriteria.RowCommand
        Try
            If e.CommandName = "edit" Or e.CommandName = "add" Or e.CommandName = "delete" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvCriteria.Rows(index), GridViewRow)
                Dim lblCriteriaShort As Label
                Dim lblCriteria As Label
                Dim lblRdmId As Label
                Dim lblOrder As Label
                Dim lblSbtId As Label

                With selectedRow
                    lblRdmId = .FindControl("lblRdmId")
                    lblCriteriaShort = .FindControl("lblCriteriaShort")
                    lblCriteria = .FindControl("lblCriteria")
                    lblOrder = .FindControl("lblOrder")
                    lblSbtId = .FindControl("lblSbtId")
                End With

                Dim keys As Object()
                Dim dt As DataTable
                dt = Session("dtCriteria")

                ReDim keys(0)
                keys(0) = lblCriteriaShort.Text
                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If e.CommandName = "edit" Then
                    txtDetail.Text = lblCriteria.Text
                    txtShort.Text = lblCriteriaShort.Text
                    txtOrder.Text = lblOrder.Text
                    If Not ddlDefaults.Items.FindByValue(lblRdmId.Text) Is Nothing Then
                        ddlDefaults.ClearSelection()
                        ddlDefaults.Items.FindByValue(lblRdmId.Text).Selected = True
                    End If
                    btnAddCriteria.Text = "Update Criteria"
                    If Not ddlCategory.Items.FindByValue(lblSbtId.Text) Is Nothing Then
                        ddlCategory.ClearSelection()
                        ddlCategory.Items.FindByValue(lblSbtId.Text).Selected = True
                    End If

                    ViewState("SelectedRow") = index
                ElseIf e.CommandName = "add" Or e.CommandName = "delete" Then
                    If dt.Rows(index).Item(5) = "add" Then
                        dt.Rows(index).Item(5) = "delete"
                    ElseIf dt.Rows(index).Item(5) = "edit" Then
                        dt.Rows(index).Item(5) = "remove"
                    End If
                    dt.Rows(index).Item(2) = hfRandom.Value
                    hfRandom.Value += 1
                    Session("dtCriteria") = dt
                    GridBind(dt)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCriteria.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblRdmId As Label
            Dim ddlDefaultvalues As DropDownList

            lblRdmId = e.Row.FindControl("lblRdmId")
            ddlDefaultvalues = e.Row.FindControl("ddlDefaultvalues")
            If lblRdmId.Text <> "" Then
                BindDefaultValues(lblRdmId.Text, ddlDefaultvalues)
            Else
                ddlDefaultvalues.Visible = False
            End If
        End If
    End Sub

    Protected Sub gvCriteria_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCriteria.RowDeleting

    End Sub

    Protected Sub gvCriteria_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvCriteria.RowEditing

    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindCategory()
        CheckSkillsStatus()
        GetRows()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindCategory()
        GetRows()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GetRows()
        EnableDisableControls(True)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnableDisableControls(True)
        GetRows()
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRsdId As Label
        lblRsdId = TryCast(sender.findcontrol("lblRsdId"), Label)
        If ((Session("SBSUID") = "123016") Or (Session("SBSUID") = "223006")) Then

            BindDefaulText(lblRsdId.Text)
            MDefault.Show()
        Else



            Dim lblRpfId As Label


            lblRpfId = TryCast(sender.findcontrol("lblRpfId"), Label)

            BindPrntedFor()
            ViewState("SRNO") = 1
            Session("POITABLE") = SetPOIDataTable()

            hfRSD_ID.Value = lblRsdId.Text
            ddlPrintedFor.ClearSelection()
            txtDetail1.Text = ""
            txtDetail2.Text = ""
            txtPOIOrder.Text = ""

            btnAddDetail.Visible = True
            btnUpdateDetail.Visible = True

            btnStudSave.Enabled = True

            GetPOI(lblRsdId.Text, ddlSubject.SelectedValue.ToString)
            MPOI.Show()
        End If

    End Sub

    Protected Sub gvPOI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPOI.PageIndexChanging

    End Sub

    Protected Sub gvPOI_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPOI.RowDeleting
        MPOI.Show()
        Dim COND_ID As Integer = CInt(gvPOI.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("POITABLE").Rows.Count
            If Session("POITABLE").rows(i)("Id") = COND_ID Then
                If Session("POITABLE").rows(i)("MODE") = "add" Then
                    Session("POITABLE").rows(i).delete()
                Else
                    Session("POITABLE").rows(i)("MODE") = "delete"
                End If
                Exit While
            Else
                i = i + 1
            End If
        End While
        GridBindPOI(Session("POITABLE"))
    End Sub

    'Protected Sub gvPOI_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvPOI.RowEditing
    '    btnSave.Enabled = False
    '    btnUpdateCancel.Visible = True
    '    gvbrand.SelectedIndex = e.NewEditIndex
    '    Dim row As GridViewRow = gvbrand.Rows(e.NewEditIndex)
    '    gvBrand.Columns(3).Visible = False
    '    gvBrand.Columns(4).Visible = False
    '    Dim lblID As New Label
    '    lblID = TryCast(row.FindControl("lblID"), Label)
    '    Dim iEdit As Integer = 0
    '    Dim iIndex As Integer = 0
    '    iIndex = CInt(lblID.Text)

    '    For iEdit = 0 To Session("BRAND_DETAIL").Rows.Count - 1
    '        If iIndex = Session("BRAND_DETAIL").Rows(iEdit)("ID") Then
    '            If Not ddlMftr.Items.FindByValue(Session("BRAND_DETAIL").Rows(iEdit)("MAN_ID")) Is Nothing Then
    '                ddlMftr.ClearSelection()
    '                ddlMftr.Items.FindByValue(Session("BRAND_DETAIL").Rows(iEdit)("MAN_ID")).Selected = True
    '            End If

    '            If Not ddlCategory.Items.FindByValue(Session("BRAND_DETAIL").Rows(iEdit)("CAD_ID")) Is Nothing Then
    '                ddlCategory.ClearSelection()
    '                ddlCategory.Items.FindByValue(Session("BRAND_DETAIL").Rows(iEdit)("CAD_ID")).Selected = True
    '            End If

    '            txtBrand.Text = Session("BRAND_DETAIL").Rows(iEdit)("BRD_DESCR")
    '            Exit For
    '        End If
    '    Next

    '    btnAddDetail.Visible = False
    '    btnUpdate.Visible = True
    '    btnUpdateCancel.Visible = True

    '    GridBind()
    'End Sub

    Protected Sub gvPOI_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvPOI.RowEditing
        MPOI.Show()
        btnStudSave.Enabled = False
        btnUpdateDetail.Visible = True
        gvPOI.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvPOI.Rows(e.NewEditIndex)
        gvPOI.Columns(4).Visible = False
        gvPOI.Columns(5).Visible = False
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        For iEdit = 0 To Session("POITABLE").Rows.Count - 1
            If iIndex = Session("POITABLE").Rows(iEdit)("ID") Then
                If Not ddlPrintedFor.Items.FindByValue(Session("POITABLE").Rows(iEdit)("RPF_ID")) Is Nothing Then
                    ddlPrintedFor.ClearSelection()
                    ddlPrintedFor.Items.FindByValue(Session("POITABLE").Rows(iEdit)("RPF_ID")).Selected = True
                End If

                txtDetail1.Text = Session("POITABLE").Rows(iEdit)("RPD_DESCR1")
                txtDetail2.Text = Session("POITABLE").Rows(iEdit)("RPD_DESCR2")
                txtPOIOrder.Text = Session("POITABLE").Rows(iEdit)("RPD_DISPLAYORDER")
                Exit For
            End If
        Next

        btnAddDetail.Visible = False
        btnUpdateDetail.Visible = True
        GridBindPOI(Session("POITABLE"))

    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetail.Click
        MPOI.Show()
        AddPOI()
        txtDetail1.Text = ""
        txtDetail2.Text = ""
        txtPOIOrder.Text = ""
    End Sub

    Protected Sub btnUpdateDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDetail.Click
        MPOI.Show()
        EditPOI()
    End Sub

    Protected Sub btnStudSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudSave.Click
        MPOI.Show()
        SavePOI()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnSaveDefault_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDefault.Click
        SaveDefaultDetails()
        MDefault.Show()
    End Sub

End Class
