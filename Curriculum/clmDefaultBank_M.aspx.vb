Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Partial Class Curriculum_clmDefaultBank_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If



                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100200") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    Session("dtDefaultValues") = SetDataTable()
                    If ViewState("datamode") = "add" Then
                        hfRDM_ID.Value = 0
                    Else
                        hfRDM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rdmid").Replace(" ", "+"))
                        txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("default").Replace(" ", "+"))
                        BindDefaultValues()
                    End If



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub

#Region "Private Methods"
   
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RDD_DESCR"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RDD_ORDER"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub BindDefaultValues()
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        Dim reader As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RDD_ID) FROM RPT.DEFAULTBANK_D WHERE " _
                                & " RDD_RDM_ID='" + hfRDM_ID.Value + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            gvDefault.Visible = True
            str_query = "SELECT RDD_DESCR,RDD_ORDER FROM RPT.DEFAULTBANK_D WHERE " _
                        & " RDD_RDM_ID='" + hfRDM_ID.Value + "'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = dt.NewRow
                dr.Item(0) = reader.GetString(0)
                dr.Item(1) = reader.GetValue(1)
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvDefault.DataSource = dt
            gvDefault.DataBind()
            Session("dtDefaultValues") = dt
        Else
            Session("dtDefaultValues") = SetDataTable()
            gvDefault.DataSource = Session("dtDefaultValues")
            gvDefault.DataBind()
            gvDefault.Visible = False
        End If

    End Sub

    Sub AddRows()
        gvDefault.Visible = True
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtDefaultValues")
        Dim keys As Object()
        ReDim keys(0)
        keys(0) = txtValue.Text.Trim
        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This value is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = txtValue.Text
        dr.Item(2) = "add"
        dt.Rows.Add(dr)
        Session("dtDefaultValues") = dt
        GridBind(dt)
    End Sub

    Sub EditRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = SetDataTable()
        Dim i As Integer
        Dim txtDefault As TextBox
        Dim txtOrder As TextBox
        Dim keys As Object()
        ReDim keys(0)
        Dim row As DataRow
        For i = 0 To gvDefault.Rows.Count - 1
            txtDefault = gvDefault.Rows(i).FindControl("txtDefault")
            txtOrder = gvDefault.Rows(i).FindControl("txtOrder")
            keys(0) = txtDefault.Text.Trim
            row = dt.Rows.Find(keys)
            If row Is Nothing Then
                dr = dt.NewRow
                dr.Item(0) = txtDefault.Text
                dr.Item(1) = Val(txtOrder.Text)
                dr.Item(2) = "add"
                dt.Rows.Add(dr)
            End If
        Next

        Session("dtDefaultValues") = dt
        GridBind(dt)

    End Sub


    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(2) <> "delete" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvDefault.DataSource = dtTemp
        gvDefault.DataBind()
    End Sub

    Sub SaveData()

        Dim transaction As SqlTransaction
        Dim str_query As String


        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_query = "EXEC RPT.saveDEFAULTBANK_M " _
                          & hfRDM_ID.Value + "," _
                          & "'" + txtDescr.Text + "'," _
                          & "'" + Session("SBSUID") + "'"

                hfRDM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                Dim dr As DataRow

                For Each dr In dt.Rows
                    With dr
                        If .Item(1) <> "delete" Then
                            str_query = "exec  RPT.saveDEFAULTBANK_D " _
                                     & hfRDM_ID.Value + "," _
                                     & "'" + .Item(0) + "'," _
                                     & "'" + .Item(1) + "'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If
                    End With
                Next

                transaction.Commit()
                lblError.Text = "Record saved successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Record could not be Saved"
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
         
        END USING
    End Sub
#End Region

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        AddRows()
        txtValue.Text = ""
    End Sub

    Protected Sub gvDefault_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDefault.RowCommand
        Try

            If e.CommandName = "edit" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvDefault.Rows(index), GridViewRow)
                Dim txtDefault As TextBox
                txtDefault = selectedRow.FindControl("txtDefault")
                Dim keys As Object()
                ReDim keys(0)
                keys(0) = txtDefault.Text
                Dim dt As DataTable
                dt = Session("dtDefaultValues")
                index = dt.Rows.IndexOf(dt.Rows.Find(keys))
                dt.Rows(index).Item(2) = "delete"
                Session("dtDefaultValues") = dt
                GridBind(dt)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvDefault_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDefault.RowDeleting

    End Sub

    Protected Sub gvDefault_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDefault.RowEditing

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        EditRows()
        SaveData()
        BindDefaultValues()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If ViewState("datamode") = "edit" Then



                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
End Class
