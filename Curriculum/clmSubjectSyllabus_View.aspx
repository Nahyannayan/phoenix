<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectSyllabus_View.aspx.vb" Inherits="Curriculum_clmSubjectSyllabus_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Syllabus/Curriculum Wise Subjects
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0"  Width="100%">
     
               
               
      
        
           <tr>
      
            <td align="center"  >
            

         
         <table id="Table2" runat="server" align="center" border="0" 
                    cellpadding="0" cellspacing="0" Width="100%" >
          
         
            <tr><td align="left"><asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton></td></tr>
            <tr><td  align="center" >
           <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="false" EmptyDataText="-" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row" >
            <Columns>
            
            
                <asp:TemplateField HeaderText="sbs_id" Visible="False">
                <ItemStyle HorizontalAlign="Left" />
                 <ItemTemplate>
                 <asp:Label ID="lblSbsId" runat="server" Text='<%# Bind("SBS_ID") %>'></asp:Label>
                 </ItemTemplate>
                  </asp:TemplateField>
                  
                 <asp:TemplateField HeaderText="Curriculum" >
                 <HeaderTemplate>                
                 <asp:Label ID="lblcm" runat="server"  Text="Curriculum"></asp:Label><br /><asp:TextBox ID="txtClm" runat="server"></asp:TextBox>
                  <asp:ImageButton ID="btnClm_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnClm_Search_Click" />
                 </HeaderTemplate>
                <ItemStyle HorizontalAlign="Left" />
                 <ItemTemplate>
                 <asp:Label ID="lblClm" runat="server" Text='<%# Bind("CLM_DESCR") %>'></asp:Label>
                 </ItemTemplate>
                  </asp:TemplateField>     
               
                 <asp:TemplateField HeaderText="Subject" >
                 <HeaderTemplate>                
                 <asp:Label ID="lblsb" runat="server"  Text="Subject"></asp:Label><br /><asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                  <asp:ImageButton ID="btnSubject_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSubject_Search_Click" />
                 </HeaderTemplate>
                <ItemStyle HorizontalAlign="Left" />
                 <ItemTemplate>
                 <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBM_DESCR") %>'></asp:Label>
                 </ItemTemplate>
                  </asp:TemplateField>
                          
                                        
               <asp:TemplateField HeaderText="Board Code" >
                    <HeaderTemplate>
                 
                 <asp:Label ID="lblbc" runat="server" CssClass="gridheader_text" Text="Board Code"></asp:Label><br />
                 <asp:TextBox ID="txtBoard" runat="server"></asp:TextBox>
                  <asp:ImageButton ID="btnBoard_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnBoard_Search_Click" />
                 </HeaderTemplate>
                 <ItemStyle HorizontalAlign="Left" />
                  <ItemTemplate>
                  <asp:Label ID="lblBoardCode" runat="server" Text='<%# Bind("SBS_BOARDCODE") %>'></asp:Label>
                 </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  /> 
                </asp:TemplateField>
                  
          
           
              <asp:ButtonField CommandName="View" HeaderText="View" Text="View"  >
           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           </asp:ButtonField>
           
        
              </Columns>
                   <RowStyle  CssClass="griditem"  />
                            <HeaderStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <SelectedRowStyle  />
                            <PagerStyle   HorizontalAlign="Left"  />                        
               </asp:GridView>   
        </td></tr>
        </table>
                   
         
               
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="hfTripDate" runat="server" type="hidden"  />
               
                  
                </td></tr>
        
        </table>

<input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
  
    
       </div>
    </div>
</div>
       

</asp:Content>

