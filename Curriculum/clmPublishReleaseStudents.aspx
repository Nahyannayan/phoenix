<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmPublishReleaseStudents.aspx.vb" Inherits="Curriculum_clmPublishReleaseStudents" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script>
              
     var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvStud.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
    rowObject.style.backgroundColor = '#deffb4'; //#f6deb2 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}

    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkPublish")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }           
                
                
                function change_chk_state1(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkRelease")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }      
                function change_chk_state2(chkThis) {
                    var chk_state = !chkThis.checked;
                    for (i = 0; i < document.forms[0].elements.length; i++) {
                        var currentid = document.forms[0].elements[i].id;
                        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkGenerate") != -1) {
                            //if (document.forms[0].elements[i].type=='checkbox' )
                            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                            document.forms[0].elements[i].checked = chk_state;
                            document.forms[0].elements[i].click();//fire the click event of the child element
                        }
                    }
                }
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Publish Report Card
        </div>
        <div class="card-body">
            <div class="table-responsive">



<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
cellspacing="0" width="100%">
<tr>
<td align="left"  valign="top">
<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
</td>
</tr>

<tr>
<td>
<table align="center" width="100%">
    <tr>
        <td align="left" valign="top" width="20%">
           <span class="field-label"> Report Card</span></td>
        
        <td align="left" valign="top" width="30%">
            <asp:Label id="lblReport" runat="server" CssClass="field-value"></asp:Label></td>
        <td align="left" valign="top" width="20%">
            <span class="field-label">Report Schedule</span></td>
        
        <td align="left" valign="top" width="30%">
            <asp:Label id="lblPrintedFor" runat="server" CssClass="field-value"></asp:Label></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <span class="field-label">Grade</span></td>
        
        <td align="left" valign="top">
            <asp:Label id="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
        <td align="left" valign="top">
            <span class="field-label">Section</span></td>
        
        <td align="left" valign="top">
            <asp:Label id="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
    </tr>
    <tr>
        <td align="center" colspan="4" valign="top">
        </td>
    </tr>
    <tr>
        <td align="left">
            <span class="field-label">Release Date</span></td>
        <td align="left">
         <asp:TextBox runat="server" ID=txtRelease>
                            </asp:TextBox>
                            <asp:ImageButton ID="imgRelease" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
        <td align="center" colspan="2">
                            <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply to All" /></td>
        
       
    </tr>
    <tr>
        <td align="center" colspan="4" valign="top">
            <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" /></td>
    </tr>

<tr>
<td align="center" colspan="4" valign="top">
            
                <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                PageSize="20" Width="100%" >
                <RowStyle CssClass="griditem" />
                <Columns>

<asp:TemplateField HeaderText="ENQ_ID" Visible="False"><ItemTemplate>
                    <asp:Label ID="lblStuId" runat="server" text='<%# Bind("Stu_ID") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student ID"><ItemTemplate>
                    <asp:Label ID="lblStuNo" runat="server" text='<%# Bind("Stu_No") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><ItemTemplate>
                    <asp:Label ID="lblStuName" runat="server" text='<%# Bind("Stu_Name") %>'></asp:Label>
                    
</ItemTemplate>
</asp:TemplateField>

                    <asp:TemplateField HeaderText="Generate" HeaderStyle-HorizontalAlign="Center"><EditItemTemplate>
                        <asp:CheckBox ID="chkGenerate" runat="server"  />
                        </EditItemTemplate>
                        <HeaderTemplate>
                        
                        Generate <br />
                        <asp:CheckBox ID="chkAll2" runat="server"  onclick="javascript:change_chk_state2(this);"
                        ToolTip="Click here to select/deselect all rows" />
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:CheckBox ID="chkGenerate" checked='<%# Bind("bGENERATE") %>' runat="server" onclick="javascript:highlight(this);" />
                        </ItemTemplate>
                        <HeaderStyle Wrap="False"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Publish" HeaderStyle-HorizontalAlign="Center"><EditItemTemplate>
                        <asp:CheckBox ID="chkPublish" runat="server"  />
                        </EditItemTemplate>
                        <HeaderTemplate>
                        
                        Publish<br />
                        <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
                        ToolTip="Click here to select/deselect all rows" />
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:CheckBox ID="chkPublish" checked='<%# Bind("bPUBLISH") %>' ENABLED='<%# Bind("bENABLE") %>' runat="server" onclick="javascript:highlight(this);" />
                        </ItemTemplate>
                        <HeaderStyle Wrap="False"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField HeaderText="Release" HeaderStyle-HorizontalAlign="Center"><EditItemTemplate>
                        <asp:CheckBox ID="chkRelease" runat="server"  />
                        </EditItemTemplate>
                        <HeaderTemplate>
                        
                        Release <br />
                        <asp:CheckBox ID="chkAll1" runat="server"  onclick="javascript:change_chk_state1(this);"
                        ToolTip="Click here to select/deselect all rows" />
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:CheckBox ID="chkRelease" checked='<%# Bind("bRELEASE") %>' runat="server" onclick="javascript:highlight(this);" />
                        </ItemTemplate>
                        <HeaderStyle Wrap="False"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        
                        
                      <asp:TemplateField HeaderText="Release Date" HeaderStyle-HorizontalAlign="Center" >
                      <ItemStyle HorizontalAlign="left"   />
                      <ItemTemplate>
                      
                      <asp:TextBox ID="txtDate" text='<%# Bind("RPP_RELEASEDATE", "{0:dd/MMM/yyyy}") %>'  runat="server" CausesValidation="true" Width="100px"></asp:TextBox>
                      
                       <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><br />
                       
                       <asp:Label ID="lblErr" runat="server" Text="*" ForeColor="RED" visible="false"></asp:Label>
                       
                       <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                       Format="dd/MMM/yyyy" PopupButtonID="imgDate"  PopupPosition="BottomLeft" TargetControlID="txtDate">
                      </ajaxToolkit:CalendarExtender>
                      </ItemTemplate>
                          <ItemStyle Width="15%" />
                      </asp:TemplateField>
                      
                        
</Columns>
                <SelectedRowStyle CssClass="Green" />
                <HeaderStyle CssClass="gridheader_pop" />
                <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
</td></tr>
<tr>


<td  colspan="4" align="center"><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
    </td>


</tr>
</table>
<input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
type="hidden" value="=" /></td></tr>


</table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgRelease" PopupPosition="BottomLeft" TargetControlID="txtRelease">
    </ajaxToolkit:CalendarExtender>
    &nbsp;
    <asp:HiddenField id="hfSCT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfRSM_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfGRD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfRPF_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfACD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="hfTRM_ID" runat="server">
    </asp:HiddenField>

              </div>
        </div>
    </div>


</asp:Content>

