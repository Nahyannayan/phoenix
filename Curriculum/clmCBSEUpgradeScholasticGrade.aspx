<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCBSEUpgradeScholasticGrade.aspx.vb" Inherits="Curriculum_clmCBSEUpgradeScholasticGrade" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            var hv = document.getElementById('h_' + obj);



            if (div.style.display == "none") {
                div.style.display = "inline";
                hv.value = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                hv.value = "none"
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";

            }
        }

        function showDiv(obj) {
            var div = document.getElementById(obj);
            /*div.style.display = "inline";
            var img = document.getElementById('img' + obj);
            img.src="../Images/Expand_Button_white_Down.jpg" 
            img.alt = "Click to close";*/



            var hv = document.getElementById('h_' + obj);
            hv.value = obj;
            var display;

            if (hv.value == "inline") {
                //div.style.display = "inline";
                display = "display.inline";
            }
            else {
                //div.style.display = "none";
                display = "display.none;"
            }
            return display;
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Edit Final Report Marks
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">

                    <tr>
                        <td align="left" class="title">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblTC" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">


                                <tr >
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select ReportCard</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Report Schedule</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrintedFor" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Select Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left" width="20%"><span class="field-label">Select Section</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>

                                    <td align="center" colspan="4" valign="top">


                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="5" AllowPaging="true">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("STU_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("STU_ID") %>', 'alt');">
                                                            <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        </td>
                                                        </tr>
                        <tr>
                            <td colspan="100%">

                                <div id="div<%# Eval("STU_ID") %>" style="display: inline-block;">

                                    <asp:GridView ID="gvSubjects" runat="server" CssClass="table table-bordered table-row"
                                        AutoGenerateColumns="false" EmptyDataText="No subjects for this grade."
                                        OnRowDataBound="gvSubjects_RowDataBound">

                                        <Columns>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStuIdSub" runat="server" Text='<%# Bind("ssd_Stu_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("Sbg_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEntryType" runat="server" Text='<%# Bind("Sbg_Entrytype") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Subject">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Sbg_descr") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="grade" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalGrade" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Overall-Grade">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlTotalGrade" runat="server" Enabled="false">
                                                        <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                        <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                        <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                        <asp:ListItem Text="B1" Value="B1"></asp:ListItem>
                                                        <asp:ListItem Text="B2" Value="B2"></asp:ListItem>
                                                        <asp:ListItem Text="C1" Value="C1"></asp:ListItem>
                                                        <asp:ListItem Text="C2" Value="C2"></asp:ListItem>
                                                        <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                        <asp:ListItem Text="E1" Value="E1"></asp:ListItem>
                                                        <asp:ListItem Text="E2" Value="E2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle Font-Names="Verdana" />
                                        <HeaderStyle />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>



                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                            <input id="h_DivStyle" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>


</asp:Content>

