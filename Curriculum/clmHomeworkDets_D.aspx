<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="clmHomeworkDets_D.aspx.vb" Inherits="Curriculum_clmHomeworkDets_D"
    Title="Untitled Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <base target="_self" />
    <title></title>
   
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="../cssfiles/sb-admin.css" rel="stylesheet" />
   
    <style type="text/css">
        BODY
        {
        	margin:0; 
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
          
            
        }
.ajax__calendar_container          { border:1px solid #c8d7ee !important; background-color:#fff;}
.ajax__calendar_container table    { margin-left:-5px !important;}
.ajax__calendar_container table td { margin:0px !important; padding:0px;}
table.ajax__validatorcallout_popup_table img { width:auto; height:auto;}
table.ajax__validatorcallout_popup_table tr  { border:none; background:none; padding:0px;}
table.ajax__validatorcallout_popup_table td  { padding:0px;}
table.ajax__validatorcallout_popup_table tr:hover { border:none; background:none;}
table.ajax__validatorcallout_popup_table .ajax__validatorcallout_error_message_cell { padding:5px 0 5px 0;}
table.ajax__validatorcallout_popup_table .ajax__validatorcallout_icon_cell { padding:5px 0 5px 5px;}

    </style>
</head>



    <script language="javascript" type="text/javascript">
        function onCalendarShown() {
            var cal = $find("calendar1");
            //Setting the default mode to month    
            cal._switchMode("months", true);
            //Iterate every month Item and attach click event to it    
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }
        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }     
        
    </script>
<body>

    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager12" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
    <table id="tbl_AddGroup" runat="server"  width="100%" height="100%" >
        <tr>
            <td align="center" valign="bottom"  >
                <asp:Label ID="lblError" runat="server"  EnableViewState="False"
                    ></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" runat="server" align="center" >
                    <tr >
                        <td align="left" valign="middle" colspan ="6"  class="title-bg">
                           <span class="field-label" > Homework Details</span>
                        </td>
                    </tr>
                    <tr id="trAcd2" runat="server">
                        <td align="left"  ><span class="field-label">
                            Grade</span>
                        </td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True"
                                >
                            </asp:DropDownList>
                        </td>
                        <td align="left" ><span class="field-label">
                            Section
                        </td>
                        <td align="left" Width="20%"  >
                            <asp:DropDownList ID="ddlsection" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList>
                        </td>
                        <td align="left" ><span class="field-label">
                            Subject</span>
</td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td   ><span class="field-label">Submission Month</span>
                    </td>
                        <td align="left"  >
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtDate_CalendarExtender" runat="server" OnClientHidden="onCalendarHidden" 
                                OnClientShown="onCalendarShown" BehaviorID="calendar1" Enabled="True" TargetControlID="txtDate" Format="MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td >
                        <td align="left" colspan ="6"  >
                            <asp:Button ID="btnShow" CssClass="btn btn-success button" runat="server" Text="Show" />
                        </td>
                    </tr>
                   <tr>
                   <td align="left"  colspan ="6">
                       <asp:Calendar ID="calHomework" runat="server"   Width="100%" CssClass="ajax__calendar"
                           ShowNextPrevMonth="False" Visible="False" ShowGridLines="True" ></asp:Calendar>
                   </td>
                   
                   </tr> 
                    
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="H_ACD_ID" runat="server" />
<asp:HiddenField ID="H_GRD_ID" runat="server" />
    </div>
    
    </form>
</body>
</html>