﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmObjtrackApptracker.aspx.vb" Inherits="Curriculum_clmObjtrackApptracker" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
        function changebuttonstyle(btn) {
            alert(btn.Class);
            if (btn.CssClass == "button") {
                btn.CssClass = "button";
            }
            return true;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="750px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Objectives By Student
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" border="0" cellpadding="5" cellspacing="0" style="vertical-align: top;" width="100%">
                    <tr>
                        <td align="center">
                            <table id="Table2" runat="server" width="100%"
                                cellpadding="0" cellspacing="0" style="border-collapse: collapse" >
                                <tr align="right">
                                    
                                    <td width="20%">
                                        <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="30%"></td>
                                    <td width="30%"></td>
                                    <td width="20%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" width="100%" align="left">
                                        <asp:DataList ID="dlGroups" runat="server" class="menu_a" RepeatColumns="10" RepeatDirection="Horizontal" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblSgrId" runat="server" Text='<% #Bind("SGR_ID")%>' Visible="false" />
                                                <asp:Button ID="btnGroup" runat="server" Text='<% #Bind("SGR_DESCR")%>'  CssClass="button_menu" OnClick="btnGroup_Click" />
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr id="trDetails" runat="server">
                                    <td colspan="4">
                                        <asp:Literal ID="lblDetails" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DataList ID="dlStudents" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" Width="100%" Height="100%">
                                            <ItemTemplate>
                                                <table  <%--class="img-thumbnail" style="border-collapse: collapse; background-image: url(../images/objImgBackground.png); background-repeat: no-repeat; background-position: top"--%>>
                                                    <tr>
                                                        <td>
                                                            <div class="card align-content-center align-items-center" width="150px">
                                                                <asp:Image ID="imgStud" runat="server" class="card-img-top" Width="150px" Height="150px"  ImageUrl='<%# getPhoto(Eval("Stu_PhotoPath")) %>'/>
                                                                <div class="card-body">
                                                                    <p class="card-text">
                                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Eval("Stu_id") %>' Visible="false" />
                                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("stu_no") %>' Visible="false" />
                                                                        <asp:LinkButton ID="lnkStudent" Text='<%# Eval("Stu_Name") %>'  runat="server"></asp:LinkButton>
                                                                    
                                                                        <asp:Label ID="lvlId" runat="server" Text='<%# Eval("lvl_Id") %>' Visible="false" />
                                                                        <span class="badge badge-success"><asp:Label ID="lblLevel" runat="server"  Text='<%# Eval("level") %>' /></span>
                                                                    </p>

                                                                </div>

                                                            </div>
                                                            <%--<table id="tbStudent" runat="server" width="100%" valign="top">
                                                                <tr>
                                                                    <td align="center" colspan="2" style="vertical-align:top;">
                                                                        <asp:Image ID="imgStud" runat="server" Width="150px" Height="150px"  ImageUrl='<%# getPhoto(Eval("Stu_PhotoPath")) %>'/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center"  valign="top" width="75%">
                                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Eval("Stu_id") %>' Visible="false" />
                                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("stu_no") %>' Visible="false" />
                                                                        <asp:LinkButton ID="lnkStudent" Text='<%# Eval("Stu_Name") %>'  runat="server"></asp:LinkButton></td>
                                                                    <td align="center" width="25%">
                                                                        <asp:Label ID="lvlId" runat="server" Text='<%# Eval("lvl_Id") %>' Visible="false" />
                                                                        <asp:Label ID="lblLevel" runat="server" BackColor="#1b80b6" Font-Size="small" BorderStyle="Solid" BorderWidth="2px" ForeColor="White" Text='<%# Eval("level") %>' />
                                                                    </td>
                                                                </tr>
                                                            </table>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEMP_ID" runat="server" />
                <asp:HiddenField ID="hfSBM_ID" runat="server" />
                <asp:HiddenField ID="hfSBG_ID" runat="server" />
                <asp:HiddenField ID="hfSGR_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfGroup" runat="server" />
                <asp:HiddenField ID="hfSubject" runat="server" />
                <asp:HiddenField ID="hfACY_DESCR" runat="server" />
                <asp:HiddenField ID="hfAPPSubject" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

