﻿
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text.pdf.parser
Imports Microsoft.ApplicationBlocks.Data

Partial Class Curriculum_clmNonPhoenixFileUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        divSuccess.Visible = False
        divError.Visible = False
        If Not Page.IsPostBack Then
            BindControls()
            If Not String.IsNullOrEmpty(Convert.ToString(Request.QueryString("success"))) Then
                Dim isSuccess = Convert.ToBoolean(Request.QueryString("success"))
                If isSuccess Then
                    divSuccess.Visible = True
                Else
                    divError.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub BindControls()
        BindAcademicYear()
        BindReportCardSetup()
        BindReportSchedule()
        BindReportGrade()
    End Sub

    Private Sub BindAcademicYear()

        ddlAcademicYear.Items.Clear()
        Try
            Dim parameters As SqlParameter() = New SqlParameter(4) {}
            parameters(0) = New SqlParameter("@BSU_ID", GetCurrentSchoolId())
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetCurrentAcademicYearList", parameters)
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "AcademicYearDescription"
            ddlAcademicYear.DataValueField = "AcademicYearId"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindReportCardSetup(Optional ByVal academicYearId As String = "", Optional ByVal reportSource As String = "")

        ddlReportCardSetup.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(academicYearId) AndAlso Not String.IsNullOrEmpty(reportSource) Then
                Dim str_query As String = "SELECT RSM_ID AS ReportCardSetupId, RSM_DESCR AS ReportCardSetupName FROM [RPT].[REPORT_SETUP_M] WHERE RSM_ACD_ID=" & academicYearId & " AND RSM_SOURCE='" & reportSource & "' ORDER BY RSM_DISPLAYORDER"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString(), CommandType.Text, str_query)
                ddlReportCardSetup.DataSource = ds
                ddlReportCardSetup.DataTextField = "ReportCardSetupName"
                ddlReportCardSetup.DataValueField = "ReportCardSetupId"
                ddlReportCardSetup.DataBind()
            End If

            ddlReportCardSetup.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindReportSchedule(Optional ByVal reportCardSetupId As String = "")

        ddlReportSchedule.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(reportCardSetupId) Then
                Dim str_query As String = "SELECT RPF_ID AS ReportScheduleId, RPF_DESCR AS ReportScheduleName FROM [RPT].[REPORT_PRINTEDFOR_M] WHERE RPF_RSM_ID=" & reportCardSetupId & " ORDER BY RPF_DISPLAYORDER"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString(), CommandType.Text, str_query)
                ddlReportSchedule.DataSource = ds
                ddlReportSchedule.DataTextField = "ReportScheduleName"
                ddlReportSchedule.DataValueField = "ReportScheduleId"
                ddlReportSchedule.DataBind()
            End If

            ddlReportSchedule.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindReportGrade(Optional ByVal reportCardSetupId As String = "")

        ddlReportGrade.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(reportCardSetupId) Then
                Dim str_query As String = "SELECT RSG_ID AS GradeId, RSG_GRD_ID AS GradeName FROM [RPT].[REPORTSETUP_GRADE_S] WHERE RSG_RSM_ID=" & reportCardSetupId & " ORDER BY RSG_DISPLAYORDER"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString(), CommandType.Text, str_query)
                ddlReportGrade.DataSource = ds
                ddlReportGrade.DataTextField = "GradeName"
                ddlReportGrade.DataValueField = "GradeId"
                ddlReportGrade.DataBind()
            End If

            ddlReportGrade.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetCurrentSchoolId() As String
        Return Convert.ToString(HttpContext.Current.Session("sBsuid"))
    End Function
    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindReportCardSetup(ddlAcademicYear.SelectedValue, ddlReportSource.SelectedValue)
        BindReportSchedule(ddlReportCardSetup.SelectedValue)
        BindReportGrade(ddlReportCardSetup.SelectedValue)
    End Sub
    Protected Sub ddlReportSource_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindReportCardSetup(ddlAcademicYear.SelectedValue, ddlReportSource.SelectedValue)
        BindReportSchedule(ddlReportCardSetup.SelectedValue)
        BindReportGrade(ddlReportCardSetup.SelectedValue)
    End Sub
    Protected Sub ddlReportCardSetup_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindReportSchedule(ddlReportCardSetup.SelectedValue)
        BindReportGrade(ddlReportCardSetup.SelectedValue)
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs)
        Dim parameter As String = String.Empty

        If Page.IsValid AndAlso otherCurriculumFileUpload.HasFile Then
            Dim isSuccess = ProcessPdfDecryption()
            If isSuccess Then
                parameter = "?success=true"
            Else
                parameter = "?success=false"
            End If
            Response.Redirect(Request.RawUrl & parameter)
        End If
    End Sub

    Public Function ProcessPdfDecryption() As Boolean
        Try
            Dim outputPath As String = CheckIfOutputPathExistsAndReturnFilePath()
            Dim reader As PdfReader = New PdfReader(otherCurriculumFileUpload.FileBytes)

            If ddlReportSource.SelectedValue = "POWERSCHOOL" Then
                PdfDecryption_POWERSCHOOL(reader, outputPath)
            End If
            Return True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub PdfDecryption_POWERSCHOOL(ByVal reader As PdfReader, ByVal outputPath As String)
        Dim studentList = New List(Of Student)()
        Try
            For pageNumber As Integer = 1 To reader.NumberOfPages
                Dim studentDetail As New Student
                Dim extractedText = PdfTextExtractor.GetTextFromPage(reader, pageNumber)
                Dim splitCondition1 = "Student ID"
                Dim splitCondition2 = "STUDENT NUMBER"
                If extractedText.Contains(splitCondition1) Or extractedText.Contains(splitCondition2) Then
                    If extractedText.Contains(splitCondition1) Then
                        studentDetail = ReturnPageIndex(pageNumber, extractedText, splitCondition1)
                    Else
                        studentDetail = ReturnPageIndex(pageNumber, extractedText, splitCondition2)
                    End If
                    studentList.Add(studentDetail)
                End If
            Next

            For studentIndex As Integer = 0 To studentList.Count - 1
                Dim student = studentList(studentIndex)
                Dim startPage = student.StudentPageIndex
                Dim interval = If((studentList.Count - 1) = studentIndex, reader.NumberOfPages - startPage, studentList(studentIndex + 1).StudentPageIndex - startPage)
                SplitAndSaveInterval(outputPath, startPage, interval, student.StudentPdfFileName)
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Function ReturnPageIndex(ByVal pageNumber As Integer, ByVal extractedText As String, ByVal splitCondition As String) As Student
        Try
            Dim studentIdPageIndex = extractedText.IndexOf(splitCondition) + (splitCondition).Length + 1
            Dim newExtractedText = extractedText.Substring(studentIdPageIndex)
            Dim newLineIndex = newExtractedText.IndexOf(vbLf)
            Dim studentId = newExtractedText.Substring(0, newLineIndex).Trim()
            Return New Student(studentId, pageNumber)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Private Sub SplitAndSaveInterval(ByVal outputPath As String, ByVal startPage As Integer, ByVal interval As Integer, ByVal pdfFileName As String)
        Try
            Dim reader As PdfReader = New PdfReader(otherCurriculumFileUpload.FileBytes)
            Dim document As iTextSharp.text.Document = New iTextSharp.text.Document()
            Dim copy As PdfCopy = New PdfCopy(document, New FileStream(outputPath & "\" & pdfFileName & ".pdf", FileMode.Create))
            document.Open()
            For pagenumber As Integer = startPage To (startPage + interval) - 1

                If reader.NumberOfPages >= pagenumber Then
                    copy.AddPage(copy.GetImportedPage(reader, pagenumber))
                Else
                    Exit For
                End If
            Next
            document.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function CheckIfOutputPathExistsAndReturnFilePath() As String
        Try
            Dim outputPath As String = Convert.ToString(ConfigurationManager.AppSettings("OtherCurriculumUploadFolderPath"))
            Dim folderpath = String.Empty
            Dim schoolId = GetCurrentSchoolId()
            Dim reportScheduleId As String = ddlReportSchedule.SelectedValue
            folderpath = Server.MapPath(Path.Combine(outputPath, schoolId, reportScheduleId))
            If Not Directory.Exists(folderpath) Then Directory.CreateDirectory(folderpath)
            Return folderpath
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

End Class

Public Class Student
    Public Sub New()
    End Sub
    Public Sub New(ByVal _studentId As String, ByVal _pageNumber As Integer)
        StudentPageIndex = _pageNumber
        StudentId = _studentId
        StudentPdfFileName = GetStudentUniqueNo(_studentId)
    End Sub

    Public Property StudentPageIndex As Integer
    Public Property StudentId As String

    Public Property StudentUniqueNo As String
    Public Property StudentPdfFileName As String

    Public Function GetStudentUniqueNo(ByVal studentFeeId As String) As String
        Dim schoolId = Convert.ToString(HttpContext.Current.Session("sBsuid"))
        Try
            Dim str_query As String = "SELECT STU_NO FROM dbo.Student_M WHERE STU_FEE_ID = '" & studentFeeId & "' AND STU_BSU_ID = '" & schoolId & "'"
            Dim studentUniqueNo = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnection(), CommandType.Text, str_query))
            Return If(studentUniqueNo IsNot String.Empty, studentUniqueNo, studentFeeId)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
End Class
