<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectGrade_View.aspx.vb" Inherits="Curriculum_clmSubjectGrade_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">


       
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Subjects-By Grade
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" >
                            <table id="Table2" runat="server" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left"  width="20%">
                                        <span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left"  width="20%" >&nbsp;
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td  width="60%"></td>
                                </tr>

                                <tr >
                                    <td colspan="3" align="center">
                                        <asp:GridView ID="gvGrades" runat="server"
                                            AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            OnRowDataBound="gvGrades_RowDataBound" PageSize="20" AllowPaging="True" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStmId" runat="server" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("GRD_ID") %>_<%# Eval("STM_ID") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("GRD_ID") %>_<%# Eval("STM_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("GRD_ID") %>_<%# Eval("STM_ID") %>', 'alt');">
                                                            <img id="imgdiv<%# Eval("GRD_ID") %>_<%# Eval("STM_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        Grade
                                                         <br />
                                                        <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                   <%-- <ItemStyle HorizontalAlign="Left" />--%>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Select" HeaderText="Set Order" Text="Set Order">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Select2" HeaderText="Other Settings" Text="Other Settings">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        </td></tr>
                                                         <tr>
                                                             <td colspan="100%">
                                                                 <div id="div<%# Eval("GRD_ID") %>_<%# Eval("STM_ID") %>" style="display: none; position: relative; left: 20px;">
                                                                     <asp:GridView ID="gvSubjects" CssClass="table table-bordered table-row" runat="server" Width="98%"
                                                                         AutoGenerateColumns="false" EmptyDataText="No subjects for this grade.">

                                                                         <Columns>
                                                                             <asp:BoundField DataField="SBG_DESCR" HeaderText="Subjects" HtmlEncode="False">
                                                                                 <ItemStyle HorizontalAlign="left" />
                                                                             </asp:BoundField>

                                                                             <asp:BoundField DataField="bOPT" HeaderText="Optional" HtmlEncode="False">
                                                                                 <ItemStyle HorizontalAlign="left" />
                                                                             </asp:BoundField>

                                                                             <asp:BoundField DataField="SBG_PARENTS" HeaderText="Parent Subject" HtmlEncode="False">
                                                                                 <ItemStyle HorizontalAlign="left" />
                                                                             </asp:BoundField>

                                                                             <asp:BoundField DataField="SBG_ENTRYTYPE" HeaderText="Entry Type" HtmlEncode="False">
                                                                                 <ItemStyle HorizontalAlign="left" />
                                                                             </asp:BoundField>

                                                                             <asp:BoundField DataField="SBG_MAJOR" HeaderText="Major Subject" HtmlEncode="False">
                                                                                 <ItemStyle HorizontalAlign="left" />
                                                                             </asp:BoundField>
                                                                         </Columns>
                                                                         <RowStyle />
                                                                         <HeaderStyle  />
                                                                     </asp:GridView>
                                                                 </div>
                                                             </td>
                                                         </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle />
                                            <SelectedRowStyle />
                                            <PagerStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>

</asp:Content>

