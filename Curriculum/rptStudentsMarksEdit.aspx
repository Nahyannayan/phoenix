<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentsMarksEdit.aspx.vb" Inherits="Curriculum_rptStudentsMarksEdit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
    function confirm_delete()
    {
      if (confirm("You are about to delete this record.Do you want to proceed?")==true)
        return true;
      else
        return false;
     }//GRADE
    function GetPopUp(id)
       {     
            //alert(id);
            var sFeatures;
            sFeatures="dialogWidth: 429px; ";
            sFeatures+="dialogHeight: 345px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           if(id==1)//Accadamic Year
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=ACCYEAR","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_ACD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            
          else if(id==2)//Grade
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=GRADE","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_GRD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            
           else if(id==3)//Group
           {
                var GradeGrpId
                GradeGrpId=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&SBM_IDs="+GradeGrpId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            
          else if(id==4)//Subjejct
           {
                var GrdDetId
                GrdDetId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                var SBGID
                SBGID=document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRP&GRDID="+GrdDetId+"&SBGID="+SBGID+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SBJ_GRD_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
          else if(id==5)//Subjejct Grades
           {
                var GradeId
                GradeId=document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                //alert(GradeId + 'ddd');
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRD&GRDID="+GradeId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==6)//Report Setup
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUP","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_RPT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
            else if(id==7)//Report Schedule
           {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSCH","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SCH_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==8)//Student Info
           {
                var GradeId
                GradeId=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs="+GradeId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==9)//Comment Category Info
           {
               
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_CMT_CAT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==10)//Comments List
           {
                var CmtCatId
                CmtCatId=document.getElementById('<%=H_CMT_CAT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTS&CAT_ID="+CmtCatId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_CMT_ID.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
           else if(id==11)//Report SetUp_D List
           {
                var RSetupId
                RSetupId=document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSETUPD&SETUP_ID="+RSetupId+"","", sFeatures)            
                if(result != '' && result != undefined)
                {
                    document.getElementById('<%=H_SETUP.ClientID %>').value = result;
                }
                else
                {
                    return false;
                }
            }
        }//CMTSCAT
        
    </script>


    <table align="center" cellpadding="5" cellspacing="0" class="BlueTable">
        <tr class="title" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                Student Marks Comments</td>
        </tr>
        <tr valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                </asp:Label><asp:ValidationSummary id="ValidationSummary1" runat="server" ValidationGroup="SUBERROR">
                </asp:ValidationSummary></td>
        </tr>
        <tr class="subheader_img" valign="top">
            <td align="left" colspan="8" style="height: 18px">
                <asp:Label id="lblHeader" runat="server" Text="Marks Details"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Academic Year</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px"><asp:DropDownList id="ddlAcdID" runat="server" Width="174px">
            </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1" style="width: 60px">
                Grade</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtGrade" runat="server" Enabled="False" Width="164px"></asp:TextBox>&nbsp;
                <asp:ImageButton id="imgGrade" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(2)" OnClick="imgGrade_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Report</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px">
                <asp:TextBox id="txtReportId" runat="server" Enabled="False" Width="169px"></asp:TextBox>
                <asp:ImageButton id="imgReport" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(6)" OnClick="imgReport_Click">
                </asp:ImageButton></td>
            <td align="left" class="matters" colspan="1" style="width: 60px">
                Subject Grade</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtGrdSubject" runat="server" Enabled="False" Width="164px"></asp:TextBox>
                <asp:ImageButton id="imgSbjGrade" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(5)" OnClick="imgSbjGrade_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Subject
                Group</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px">
                <asp:TextBox id="txtGroup" runat="server" Width="168px" Enabled="False"></asp:TextBox>
                <asp:ImageButton id="imgGroup" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(3)" OnClick="imgGroup_Click">
                </asp:ImageButton></td>
            <td align="left" class="matters" colspan="1" style="width: 60px">
                Type Level</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:DropDownList id="ddlLevel" runat="server" Width="137px">
                    <asp:ListItem>Core</asp:ListItem>
                    <asp:ListItem>Extended</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Subject</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px">
                <asp:TextBox id="txtSubject" runat="server" Width="168px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imgSubject" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(4)" OnClick="imgSubject_Click">
                </asp:ImageButton></td>
            <td align="left" class="matters" colspan="1" style="width: 60px">
                Report Schedule</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox id="txtRptSchedule" runat="server" Width="166px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imgRptSchedule" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(7)" OnClick="imgRptSchedule_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px; height: 23px">
                Student Name</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px; height: 23px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px; height: 23px">
                <asp:TextBox id="txtStuName" runat="server" Width="168px" Enabled="False">
                </asp:TextBox>&nbsp;<asp:ImageButton id="imgStuNo" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(8)" OnClick="imgStuNo_Click"></asp:ImageButton></td>
            <td align="left" class="matters" colspan="1" style="width: 60px; height: 23px">
                Type Header</td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 23px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 23px"><asp:TextBox id="txtHeader" runat="server" Width="166px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imgHeader" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(11)" OnClick="imgHeader_Click"></asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px; height: 23px">
                Marks</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px; height: 23px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 219px; height: 23px">
                <asp:TextBox id="txtMarks" runat="server" Width="55px"></asp:TextBox>
                <asp:TextBox id="txtStuNo" runat="server" Width="122px" Enabled="False"></asp:TextBox></td>
            <td align="left" class="matters" colspan="1" style="height: 23px; width: 60px;">
                Grade Level</td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 23px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 23px"><asp:TextBox id="txtGrdLevel" runat="server" Width="55px">
            </asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="subheader_img" colspan="8">
                <asp:Label id="Label1" runat="server" Text="Comments.." Width="105px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Comment<br />
                Category</td>
            <td align="left" class="matters" style="width: 15px">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:TextBox id="txtCategory" runat="server" Width="197px" Enabled="False">
                </asp:TextBox>
                <asp:ImageButton id="imgCategory" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(9)" OnClick="imgCategory_Click">
                </asp:ImageButton>&nbsp;&nbsp; Select Comments
                <asp:ImageButton id="imgComments" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(10)" OnClick="imgComments_Click">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Comments</td>
            <td align="left" class="matters" style="width: 15px">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:TextBox id="txtComments" runat="server" Height="111px" SkinID="MultiText_Large"
                    Width="502px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px; height: 24px">
            </td>
            <td align="left" class="matters" style="width: 15px; height: 24px">
            </td>
            <td align="left" class="matters" colspan="6" style="height: 24px">
                &nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                <asp:Button id="btnDetAdd" runat="server" CssClass="button"
                    Text="Add" ValidationGroup="SUBERROR" Width="48px" OnClick="btnDetAdd_Click" Visible="False" />
                <asp:Button id="btnSubCancel" runat="server" CssClass="button"
                    Text="Cancel" OnClick="btnSubCancel_Click" Visible="False" />
            </td>
        </tr>
        <tr align="center">
            <td align="center" class="subheader_img" colspan="8">
                <asp:Label id="Label2" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="White"
                    Height="13px" Text="Student Marks" Width="185px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: left">
                <asp:GridView id="gvMarks" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                    SkinID="GridViewNormal" Width="99%">
                    <columns>
<asp:TemplateField Visible="False" HeaderText="Id"><ItemTemplate>
<asp:Label id="lblId" runat="server" Text='<%# Bind("Id") %>' __designer:wfdid="w31"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="AcdId"><ItemTemplate>
<asp:Label id="lblAcdId" runat="server" Text='<%# bind("AcdId") %>' __designer:wfdid="w9"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="AcdYear"><ItemTemplate>
<asp:Label id="lblAcdYear" runat="server" Text='<%# bind("AcdYear") %>' __designer:wfdid="w10"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="GrdId"><ItemTemplate>
<asp:Label id="lblGrdId" runat="server" Text='<%# bind("GrdId") %>' __designer:wfdid="w11"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Grade"><ItemTemplate>
<asp:Label id="lblGrade" runat="server" Text='<%# bind("Grade") %>' __designer:wfdid="w12"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="GroupId"><ItemTemplate>
<asp:Label id="lblGrpId" runat="server" Text='<%# Bind("GroupId") %>' __designer:wfdid="w17"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Group"><ItemTemplate>
<asp:Label id="lblGroup" runat="server" Text='<%# Bind("Group") %>' __designer:wfdid="w18"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="SBJID"><ItemTemplate>
<asp:Label id="lblSbjId" runat="server" Text='<%# Bind("SBJID") %>' __designer:wfdid="w20"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Subject"><ItemTemplate>
<asp:Label id="lblSubject" runat="server" Text='<%# Bind("Subject") %>' __designer:wfdid="w20"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="studId"><ItemTemplate>
<asp:Label id="lblStudId" runat="server" Text='<%# Bind("studId") %>' __designer:wfdid="w26"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="StudName"><ItemTemplate>
<asp:Label id="lblStudName" runat="server" Text='<%# Bind("StudName") %>' __designer:wfdid="w27"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mark"><ItemTemplate>
<asp:Label id="lblMark" runat="server" Text='<%# Bind("Mark") %>' __designer:wfdid="w29"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Comments"><ItemTemplate>
<asp:Label id="lblCmts" runat="server" Text='<%# Bind("Comments") %>' __designer:wfdid="w30"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:CommandField ShowCancelButton="False" HeaderText="Edit" ShowEditButton="True"></asp:CommandField>
<asp:CommandField ShowDeleteButton="True" HeaderText="Remove"></asp:CommandField>
</columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="8">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" Width="46px" />
                <asp:Button id="btnEdit" runat="server" CssClass="button" Text="Edit" Width="45px" />&nbsp;
                <asp:Button id="btnSave" runat="server" CssClass="button"
                    Text="Save" ValidationGroup="MAINERROR" Width="60px" OnClick="btnSave_Click" />&nbsp;
                <asp:Button id="btnDelete" runat="server" CssClass="button" Text="Delete" OnClick="btnDelete_Click" />
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    <asp:HiddenField id="H_ACD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_GRD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_GRP_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_SBJ_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_RPT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SCH_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_STU_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_CMT_CAT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="H_SBJ_GRD_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_CMT_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_ID" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_SETUP" runat="server">
    </asp:HiddenField><asp:HiddenField id="H_RPF_ID" runat="server">
    </asp:HiddenField>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Do you want to Delete this student?"
        TargetControlID="btnDelete">
    </ajaxToolkit:ConfirmButtonExtender>
</asp:Content>

