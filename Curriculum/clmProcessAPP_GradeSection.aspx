<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmProcessAPP_GradeSection.aspx.vb" Inherits="Curriculum_clmProcessAPP_GradeSection" Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
.RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
.RadComboBox_Default {
    font :inherit !important;
    width : 100% !important;
}


    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="CBSE-I KG Process"
                EnableViewState="False"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block;">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Select Report card</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left"width="20%"><span class="field-label">Report Schedule</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="trHeader" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Column Header</span></td>
                                    <td align="left">
                                        <%-- <asp:CheckBoxList ID="lstHeader" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>--%>
                                        <telerik:RadComboBox RenderMode="Lightweight" ID="lstHeader" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true">
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Section</span></td>
                                    <td align="left" width="30%">
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstSection" runat="server" >
                                        </asp:CheckBoxList></div>
                                        <asp:Label ID="lblNote" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom"></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

