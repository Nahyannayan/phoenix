<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="clmActivity_Schedule_Alloc.aspx.vb" Inherits="Students_studGrade_Att" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">

<script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
</script>

<script language="javascript" type="text/javascript">
    function change_chk_state(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkList") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
        }
    }


</script>

<%--<form runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:updatepanel id="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>
                               
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Allocate Students
        </div>
        <div class="card-body">
            <div class="table-responsive">

    
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0">
        <tr>
            <td align="left">
                <span >
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr style="font-size: 8pt; color: #800000" valign="bottom">
            <td align="center" valign="middle">
                <table align="center"  cellpadding="5" cellspacing="0" width="100%"  >
                   
                    <tr>
                        <td align="left" >
                            <span class="field-label">Academic Year</span></td>
                       
                        <td align="left" >
                            <asp:Label id="ltAcd_Year" runat="server" class="field-value"></asp:Label></td>
                        <td align="left" >
                           <span class="field-label"> Term</span></td>
                        
                        <td align="left" >
                            <asp:Label id="ltTerm" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Grade</span></td>
                       
                        <td align="left" >
                            <asp:Label id="ltGrade" runat="server" class="field-value"></asp:Label></td>
                        <td align="left" >
                           <span class="field-label"> Subject</span></td>
                       
                        <td align="left" >
                            <asp:Label id="ltSub" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Assessment</span></td>
                       
                        <td align="left" colspan="4" style="">
                            <asp:Label id="ltActivity" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Group</span></td>
                       
                        <td align="left" colspan="4" style="">
                            <asp:Label id="ltGroup" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label"> Level</span></td>
                       
                        <td align="left" style="" colspan="4">
                            <asp:Label id="ltLevel" runat="server" class="field-value"></asp:Label></td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td  valign="bottom" >
            </td>
        </tr>
        <tr>
            <td  valign="bottom">
        
                <asp:GridView id="gvAlloc" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="40" Width="100%" DataKeyNames="STU_ID" OnRowDataBound="gvAlloc_RowDataBound">
                    <rowstyle cssclass="griditem"  />
                    <columns>
<asp:TemplateField HeaderText="Select"><HeaderTemplate>
&nbsp;<asp:CheckBox id="chkAll" onclick="javascript:change_chk_state(this);" runat="server"></asp:CheckBox>&nbsp;
</HeaderTemplate>
<ItemTemplate>
&nbsp;<asp:CheckBox id="chkList" runat="server"></asp:CheckBox> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Stu.ID" Visible="False"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblstu_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student No"><ItemTemplate>
<asp:Label id="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblSTUNAME" runat="server" Text='<%# Bind("STUNAME") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="GRD_SEC" HeaderText="Grade &amp; Section">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="AVAIL" Visible="False"><ItemTemplate>
<asp:Label id="lblAvail" runat="server" Text='<%# Bind("AVAIL") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <selectedrowstyle backcolor="Wheat" />
                    <headerstyle cssclass="gridheader_pop" horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView>
              
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                &nbsp;
                                        </td>
        </tr>
        <tr align="center">
            <td valign="bottom" >
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                    Text="Cancel" /></td>
        </tr>
        <tr>
            <td valign="bottom" >
                 <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />&nbsp;
                
                <asp:HiddenField ID="hiddenENQIDs" runat="server" /> 


                </td>
        </tr>
    </table>
     
   </div>
            </div>
            </div>
 
                     <%--   </ContentTemplate>
                    </asp:updatepanel>
</form>--%>
</asp:Content>

