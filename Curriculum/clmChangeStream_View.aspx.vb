Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmChangeStream_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300030" And ViewState("MainMnu_code") <> "C300060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ViewState("stumode") = "add"
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    '    studTable.Rows(4).Visible = False
                    '    studTable.Rows(5).Visible = False
                    set_Menu_Img()
                    GridBind()
                    If ViewState("MainMnu_code") = "C300060" Then
                        lblTitle.Text = "Change Option Approval"
                    End If
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim strQuery As String


        Dim comSubjects As Integer

        If ViewState("MainMnu_code") = "C300030" Then
            strQuery = "SELECT STU_ID,SCT_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                     & " SCT_DESCR,GRM_DISPLAY,STU_GRD_ID,STU_STM_ID,STM_DESCR,STU_SHF_ID FROM STUDENT_M  AS A " _
                     & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                     & " INNER JOIN GRADE_BSU_M AS D ON A.STU_GRM_ID=D.GRM_ID " _
                     & " INNER JOIN STREAM_M AS E ON A.STU_STM_ID=E.STM_ID " _
                     & " INNER JOIN STUDENT_CHANGESTREAMREQ_S AS F ON A.STU_ID=F.SCS_STU_ID AND SCS_APPROVE=0 AND SCS_TYPE='CS'" _
                     & " WHERE STU_bACTIVE='TRUE' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Else
            strQuery = "SELECT STU_ID,SCT_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                     & " SCT_DESCR,GRM_DISPLAY,STU_GRD_ID,STU_STM_ID,STM_DESCR,STU_SHF_ID FROM STUDENT_M  AS A " _
                     & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                     & " INNER JOIN GRADE_BSU_M AS D ON A.STU_GRM_ID=D.GRM_ID " _
                     & " INNER JOIN STREAM_M AS E ON A.STU_STM_ID=E.STM_ID " _
                     & " INNER JOIN STUDENT_CHANGESTREAMREQ_S AS F ON A.STU_ID=F.SCS_STU_ID AND SCS_APPROVE=0 AND SCS_TYPE='CO'" _
                     & " WHERE STU_bACTIVE='TRUE' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If

        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox


        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""

        If gvStud.Rows.Count > 0 Then

            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvGrade = gvStud.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" Then

                strFilter = strFilter + " and grm_display='" + ddlgvGrade.Text + "'"

                selectedGrade = ddlgvGrade.Text
            End If


            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
            End If


            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "No records to view"
        Else
            gvStud.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvStud.Rows.Count > 0 Then


            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            ddlgvGrade = gvStud.HeaderRow.FindControl("ddlgvGrade")


            Dim dr As DataRow


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(4))
                    End If

                    If ddlgvGrade.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(5))
                    End If
                End With

            Next
            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If


        End If
        studClass.SetChk(gvStud, Session("liUserList"))
        set_Menu_Img()
    End Sub

#End Region


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Dim lblStuId As Label
                Dim lblStuNo As Label
                Dim lblStuName As Label
                Dim lblStmId As Label
                Dim lblGrdId As Label
                Dim lblGrade As Label
                Dim lblStream As Label
                Dim lblShfId As Label

                With selectedRow
                    lblStuId = .FindControl("lblStuId")
                    lblStuNo = .FindControl("lblStuNo")
                    lblStuName = .FindControl("lblStuName")
                    lblStmId = .FindControl("lblStmId")
                    lblStream = .FindControl("lblStream")
                    lblGrade = .FindControl("lblGrade")
                    lblGrdId = .FindControl("lblGrdId")
                    lblShfId = .FindControl("lblShfId")
                End With
                Dim url As String
                If ViewState("MainMnu_code") = "C300030" Then
                    ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                    url = String.Format("~\Curriculum\clmChangeStream_M.aspx?MainMnu_code={0}&datamode={1}" _
                                       & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                                       & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                                       & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) _
                                       & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                       & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                       & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                                       & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) _
                                       & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) _
                                       & "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Else
                    ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                    url = String.Format("~\Curriculum\clmChangeOption_M.aspx?MainMnu_code={0}&datamode={1}" _
                                       & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                                       & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                                       & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) _
                                       & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                       & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                                       & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                                       & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) _
                                       & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) _
                                       & "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                End If

                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
