﻿Imports System.Data.SqlClient
Imports CURRICULUM
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports ActivityFunctions
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports Telerik.Web.UI

Partial Class Curriculum_clmCommendation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100410") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    ddlType = PopulateType(ddlType, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If

                    gridbind()
                    BindCopyGroup()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT  grm_display  " _
                              & " ,grm_grd_id,grd_displayorder FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " where grm_acd_id=" + acdid _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function PopulateType(ByVal ddl As DropDownList, ByVal acdid As String, ByVal grdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT ctc_type " _
                              & "  FROM RPT.COMMENDATION_type_cat " _
                              & " where ctc_acd_id=" + acdid + " order by ctc_id"



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "ctc_type"
        ddl.DataValueField = "ctc_type"
        ddl.DataBind()
        Return ddl
    End Function
    Function PopulateSubjects(ByVal ddlSubject As RadComboBox, ByVal acd_id As String)
        ddlSubject.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_PARENTS+'-'+SBG_SHORTCODE END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then


            str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'"


        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function
    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As RadComboBox, ByVal acd_id As String)
        ddlSubject.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_PARENTS+'-'+SBG_SHORTCODE END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID "
        '& "  AND SBG_PARENT_ID=0 AND SBG_bMAJOR=1"

        str_query += " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                   & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function
    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To ddlSubject.Items.Count - 1
            If ddlSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += ddlSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function
    Private Sub subclear()
        Dim i As Integer

        For i = 0 To ddlSubject.Items.Count - 1
            ddlSubject.Items(i).Selected = False
        Next
    End Sub

    Protected Sub ddlPart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPart.SelectedIndexChanged
        subclear()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlType = PopulateType(ddlType, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindCopyGroup()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ddlType = PopulateType(ddlType, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        gridbind()
        BindCopyGroup()
    End Sub



    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet




            str_Sql = "select CT_ID,CP_PART,CP_SBG_ID,CT_TYPE,CT_GRADE,CT_NOS from rpt.COMMENDATION  " _
                       & " inner join rpt.COMMENDATION_PART on  CD_ID=CP_CD_ID  " _
                       & " inner join rpt.COMMENDATION_TYPE on  CP_ID=CT_CP_ID  " _
                       & " WHERE CD_ACD_ID=" + ddlAcademicYear.SelectedValue + " and cd_grd_id='" + ddlGrade.SelectedValue + "' order by ct_type,cp_part"



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdType.DataSource = ds.Tables(0)
                grdType.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdType.DataSource = ds.Tables(0)
                Try
                    grdType.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdType.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdType.Rows(0).Cells.Clear()
                grdType.Rows(0).Cells.Add(New TableCell)
                grdType.Rows(0).Cells(0).ColumnSpan = columnCount
                grdType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdType.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lnkType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkType.Click
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String

            If ViewState("datamode") = "edit" Then
                str_query = "exec rpt.updateCOMMENDATION " & H_CAT_ID.Value & "," & ddlAcademicYear.SelectedValue & " ,'" & ddlGrade.SelectedValue & "','0','" & ddlPart.SelectedItem.Text & "','" & getSubjects() & "','" & ddlType.SelectedItem.Text & "','" & txtGrading.Text.ToUpper & "'," & (txtNos.Text) & " "
            Else
                str_query = "exec rpt.saveCOMMENDATION " & ddlAcademicYear.SelectedValue & " ,'" & ddlGrade.SelectedValue & "','0','" & ddlPart.SelectedItem.Text & "','" & getSubjects() & "','" & ddlType.SelectedItem.Text & "','" & txtGrading.Text.ToUpper & "'," & (txtNos.Text) & " "
            End If
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            gridbind()
            clears()
            lnkType.Text = "Save"
            ViewState("datamode") = "add"
            ddlType = PopulateType(ddlType, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"

        End Try
    End Sub
    Protected Sub grdType_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdType.RowDataBound
        Try
            Dim str As String = ""
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblSub = DirectCast(e.Row.FindControl("lblSubject"), Label)
                Dim lbl = DirectCast(e.Row.FindControl("lblName"), Label)
                str = Replace(lblSub.Text, "|", ",")
                lbl.Text = getSubName(str)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function getSubName(ByVal str As String) As String
        Dim subs As String = ""
        Dim i As Integer = 0

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim ds As DataSet

        str_query = "select   sbg_descr FROM SUBJECTS_GRADE_S sg WHERE (sg.SBG_ID in  (" + str + ") ) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1

            If subs <> "" Then
                subs += ","
            End If
            subs += ds.Tables(0).Rows(i)("sbg_descr")

        Next
        Return subs

    End Function

    Protected Sub btnSaveCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCategory.Click
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String


            str_query = "exec rpt.saveCOMMENDATION_type_cat " & ddlAcademicYear.SelectedValue & " ,'" & ddlGrade.SelectedValue & "','" & txtCategory.Text & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            ddlType = PopulateType(ddlType, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub clears()
        txtCategory.Text = ""
        txtGrading.Text = ""
        txtNos.Text = ""
    End Sub

    Protected Sub grdType_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdType.RowDeleting
        Try
            grdType.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdType.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("tyId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM rpt.COMMENDATION_TYPE WHERE Ct_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdType_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdType.RowEditing
        grdType.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = grdType.Rows(e.NewEditIndex)

        Dim lblID As New Label
        lblID = TryCast(row.FindControl("tyId"), Label)

        If lblID.Text <> "" Then
            Displayitems(lblID.Text)
            H_CAT_ID.Value = lblID.Text
        End If



        lnkType.Text = "Update"

    End Sub
    Private Sub Displayitems(ByVal ID As Int64)

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "select cd_id,cp_id,CT_ID,cd_acd_id,cd_grd_id,CP_PART,CP_SBG_ID,CT_TYPE,CT_GRADE,CT_NOS from rpt.COMMENDATION " _
                       & " inner join rpt.COMMENDATION_PART on  CD_ID=CP_CD_ID " _
                      & " inner join rpt.COMMENDATION_TYPE on  CP_ID=CT_CP_ID  " _
                    & " WHERE CT_ID = " & ID

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then
                    ddlAcademicYear.SelectedValue = ds.Tables(0).Rows(0).Item("cd_acd_id")
                    ddlGrade.SelectedValue = ds.Tables(0).Rows(0).Item("cd_grd_id")
                    ddlPart.SelectedValue = ds.Tables(0).Rows(0).Item("CP_PART")
                    subj = Replace(ds.Tables(0).Rows(0).Item("CP_SBG_ID"), "|", ",")
                    getSubSelect(subj)
                    ddlType.SelectedValue = ds.Tables(0).Rows(0).Item("CT_TYPE")
                    txtGrading.Text = ds.Tables(0).Rows(0).Item("CT_GRADE")
                    txtNos.Text = ds.Tables(0).Rows(0).Item("CT_NOS")
                    ViewState("datamode") = "edit"
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub


    Private Sub getSubSelect(ByVal str As String)
        Dim subs As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim ds As DataSet
        subclear()
        str_query = "select SBG_ID FROM SUBJECTS_GRADE_S sg WHERE (sg.SBG_ID in  (" + str + ") ) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For j = 0 To ddlSubject.Items.Count - 1
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If (ds.Tables(0).Rows(i)("SBG_ID").ToString = ddlSubject.Items(j).Value) Then
                    ddlSubject.Items(j).Selected = True
                End If
            Next
        Next

    End Sub
    Sub BindCopyGroup()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String




        'str_query = "SELECT DISTINCT  grm_display AS TEXT  " _
        '                      & " ,grm_grd_id AS ID,grd_displayorder FROM " _
        '                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
        '                      & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
        '                      & " and grm_grd_id <> '" + ddlGrade.SelectedValue + "'order by grd_displayorder FOR XML AUTO"

        str_query = "SELECT DISTINCT  grm_display AS TEXT  " _
                         & " ,grm_grd_id AS ID,grd_displayorder FROM " _
                         & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                         & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                         & " and grm_grd_id <> '" + ddlGrade.SelectedValue + "'order by grd_displayorder"

        'Dim reader As SqlDataReader
        'reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'Dim sq As New SqlString
        'Dim xmlData As New XmlDocument
        'Dim str As String
        'While reader.Read
        '    str += reader.GetString(0)
        'End While
        'Dim xl As New SqlString

        'xl = "<root>" + str + "</root>"
        'Dim xmlReader As New XmlTextReader(New StringReader(xl))
        'reader.Close()

        'tvCopyGroup.Nodes.Clear()
        'xmlData.Load(xmlReader)
        'tvCopyGroup.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        'Dim tnode As TreeNode
        'tnode = tvCopyGroup.Nodes(0)
        'AddNode(xmlData.DocumentElement, tnode)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstGrade.DataSource = ds
        'lstGrade.DataTextField = "GRM_DISPLAY"
        'lstGrade.DataValueField = "GRM_GRD_ID"
        'lstGrade.DataBind()
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "TEXT"
        ddlPrintedFor.DataValueField = "ID"
        ddlPrintedFor.DataBind()


    End Sub
    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub
    'Private Sub copygrade()
    '    Dim tNode, cNode As TreeNode
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String
    '    Dim gr_id As String = ""
    '    For Each tNode In tvCopyGroup.Nodes
    '        For Each cNode In tNode.ChildNodes
    '            If cNode.Checked = True Then

    '                gr_id = cNode.Value.ToString
    '                If gr_id <> "" Then
    '                    str_query = "exec RPT.copyCOMMENDATION " _
    '                  & " @ACD_ID=" + ddlAcademicYear.SelectedValue + "," _
    '                  & " @GRD_ID='" + gr_id + "'," _
    '                  & " @GRD_ID_t='" + ddlGrade.SelectedValue + "'"
    '                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    '                End If
    '            End If
    '        Next
    '    Next

    'End Sub

    Function copygrade() As String
        Dim str As String = ""
        Dim gr_id As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer

        For i = 0 To ddlPrintedFor.Items.Count - 1
            If ddlPrintedFor.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += ddlPrintedFor.Items(i).Value
                gr_id = ddlPrintedFor.Items(i).Value
                If gr_id <> "" Then
                    str_query = "exec RPT.copyCOMMENDATION " _
                  & " @ACD_ID=" + ddlAcademicYear.SelectedValue + "," _
                  & " @GRD_ID='" + gr_id + "'," _
                  & " @GRD_ID_t='" + ddlGrade.SelectedValue + "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If

            End If
        Next
        Return str
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        copygrade()
        BindCopyGroup()
    End Sub


End Class
