<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSetCutoffDates.aspx.vb" Inherits="clmActivity_D_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
  
	
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Set Cut Off Details"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="Table1" border="0" width="100%">
                </table>
                <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="text-danger font-small" >Fields Marked with ( * ) are mandatory</td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center" cellpadding="5" cellspacing="0"
                                width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Report Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCAD_CAM_ID" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Report Printed For </span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportPrintedFor" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Fee Cut Off Amount</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAmount" runat="server" Width="58px"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Date</span></td>

                                    <td align="left">
                                        <uc1:usrDatePicker ID="usrDPFeeCutoff" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Attendance Start Date</span></td>

                                    <td align="left">
                                        <uc1:usrDatePicker ID="usrDPAttStDt" runat="server" />
                                    </td>
                                    <td align="left"><span class="field-label">End Date</span></td>

                                    <td align="left">
                                        <uc1:usrDatePicker ID="usrDPAttEndDt" runat="server" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 9px" valign="bottom">
                            <br />
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 10px" valign="bottom" align="center">
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnEdit_Click" Text="Edit" /><asp:Button ID="btnSave"
                                    runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"
                                OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <input id="h_Row" type="hidden" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

