Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports CURRICULUM
Imports SKILLSCHEDULE

Partial Class clmSkill_Schedule_view
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_ACTIVITY_SCHEDULE And ViewState("MainMnu_code") <> "C320020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    GetEmpID()
                    '
                    'If ViewState("MainMnu_code") = "S059005" Then
                    '    ltHeader.Text = "REGISTER PERMISSION"
                    'ElseIf ViewState("MainMnu_code") = "S059010" Then
                    '    ltHeader.Text = "LEAVE APPROVAL PERMISSION"
                    'End If

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_11.Value = "LI__../Images/operations/like.gif"
                    callYEAR_DESCRBind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
            gvAuthorizedRecord.Attributes.Add("bordercolor", "#1b80b6")
        End If
        set_Menu_Img()
    End Sub

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_11.Value.Split("__")
        getid11(str_Sid_img(2))


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid11(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_11_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty

            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_GRADE As String = String.Empty
            Dim str_filter_STREAM As String = String.Empty
            Dim str_filter_Group As String = String.Empty
            Dim str_filter_Activity As String = String.Empty
            Dim str_filter_SUBJECT As String = String.Empty
            Dim str_filter_CAS_DESC As String = String.Empty
            Dim str_filter_MARK As String = String.Empty
            Dim str_filter_DATE As String = String.Empty

            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            Dim TRM_ID As String = String.Empty

            If ddlTerm.SelectedIndex = -1 Then
                TRM_ID = "  AND A.TRM_DESC<>''"
            Else
                If ddlTerm.SelectedItem.Text = "ALL" Then
                    TRM_ID = " AND A.TRM_DESC<>''"
                Else
                    TRM_ID = " AND A.TRM_DESC='" & ddlTerm.SelectedItem.Text & "'"
                End If

            End If

            ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

            Dim ds As New DataSet
           
            If Not Session("CurrSuperUser") Is Nothing Then
                If Session("CurrSuperUser") = "Y" Then
                    str_Sql = "SELECT * FROM ( " & _
                        " SELECT VW_STREAM_M.STM_DESCR AS STM_DESC, VW_GRADE_M.GRD_DISPLAYORDER AS DISPLAYORDER, " & _
                        " VW_GRADE_BSU_M.GRM_DISPLAY AS GRD_DESC, " & _
                        " (CASE SBG_PARENTS WHEN 'NA' THEN '' ELSE SBG_PARENTS+ ' - ' END)  + SBG_DESCR AS SBG_DESC, " & _
                        " (SELECT ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS ENAME " & _
                        " FROM VW_EMPLOYEE_M WHERE (EMP_ID = M.SGR_EMP_ID)) AS ENAME, ACT.ACTIVITY_SCHEDULE.CAS_TYPE_LEVEL AS TYPE_LEVEL, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID, ACT.ACTIVITY_D.CAD_TRM_ID, ACT.ACTIVITY_D.CAD_ACD_ID ACD_ID,ACT.ACTIVITY_SCHEDULE.CAS_SKL_ID, " & _
                        " VW_TRM_M.TRM_DESCRIPTION TRM_DESC, ACT.ACTIVITY_D.CAD_DESC, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID SBG_ID, ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID CAD_ID , M.SGR_GRD_ID GRD_ID, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID PARENT_ID, " & _
                        " CONVERT(VARCHAR,CAS_PARENT_ID) + '___' + CONVERT(VARCHAR, ACT.ACTIVITY_D.CAD_ID) " & _
                        " + '___' + CONVERT(VARCHAR, M.SGR_GRD_ID) + '___' + " & _
                        " CONVERT(VARCHAR, SUBJECTS_GRADE_S.SBG_ID) UNIQUE_ID  " & _
                        " FROM ACT.ACTIVITY_SCHEDULE INNER JOIN " & _
                        " ACT.ACTIVITY_D ON ACT.ACTIVITY_D.CAD_ID = ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID " & _
                        " INNER JOIN GROUPS_M AS M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = M.SGR_ID " & _
                        " INNER JOIN VW_GRADE_BSU_M ON M.SGR_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                        " AND M.SGR_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                        " INNER JOIN VW_TRM_M ON ACT.ACTIVITY_D.CAD_TRM_ID = VW_TRM_M.TRM_ID " & _
                        " AND ACT.ACTIVITY_D.CAD_ACD_ID = VW_TRM_M.TRM_ACD_ID " & _
                        " INNER JOIN VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " & _
                        " INNER JOIN SUBJECTS_GRADE_S ON ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " & _
                        " INNER JOIN VW_STREAM_M ON M.SGR_STM_ID = VW_STREAM_M.STM_ID " & _
                        " GROUP BY VW_STREAM_M.STM_DESCR , VW_GRADE_M.GRD_DISPLAYORDER, " & _
                        " VW_GRADE_BSU_M.GRM_DISPLAY, SUBJECTS_GRADE_S.SBG_DESCR, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_TYPE_LEVEL, ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID, " & _
                        " ACT.ACTIVITY_D.CAD_TRM_ID, ACT.ACTIVITY_D.CAD_ACD_ID,SGR_EMP_ID, VW_TRM_M.TRM_DESCRIPTION, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID, ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID, M.SGR_GRD_ID, ACT.ACTIVITY_D.CAD_DESC, " & _
                        " ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID, ACT.ACTIVITY_D.CAD_ID ,SUBJECTS_GRADE_S.SBG_ID,SUBJECTS_GRADE_S.SBG_PARENTS,ACT.ACTIVITY_SCHEDULE.CAS_SKL_ID  " & _
                        ")A where isnull(CAS_SKL_ID,'')<>'' AND a.ACD_ID='" & ACD_ID & "'"
                    If ViewState("GRD_ACCESS") > 0 Then
                        str_Sql += " AND A.GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                                 & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                                 & " WHERE GSA_ACD_ID='" & ACD_ID & "' AND (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
                    End If
                Else

                End If
            End If

            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_GRADE As String = String.Empty
            Dim str_STREAM As String = String.Empty
            Dim str_Group As String = String.Empty
            Dim str_SUBJECT As String = String.Empty
            Dim str_activity As String = String.Empty
            Dim str_CAS_DESC As String = String.Empty
            Dim str_MARK As String = String.Empty
            Dim str_DATE As String = String.Empty


            If gvAuthorizedRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRD_DESC")
                str_GRADE = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRADE = " AND A.GRD_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRADE = "  AND  NOT A.GRD_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRADE = " AND A.GRD_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRADE = " AND A.GRD_DESC NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRADE = " AND A.GRD_DESC LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRADE = " AND A.GRD_DESC NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_11.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTM_DESC")
                str_STREAM = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STREAM = " AND A.STM_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STREAM = "  AND  NOT A.STM_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STREAM = " AND A.STM_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STREAM = " AND A.STM_DESC NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STREAM = " AND A.STM_DESC LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STREAM = " AND A.STM_DESC NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSBG_DESC")
                str_SUBJECT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SUBJECT = " AND a.SBG_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SUBJECT = "  AND  NOT a.SBG_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SUBJECT = " AND a.SBG_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SUBJECT = " AND a.SBG_DESC NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SUBJECT = " AND a.SBG_DESC LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SUBJECT = " AND a.SBG_DESC NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCAD_DESC")
                str_activity = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Activity = " AND a.CAD_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Activity = "  AND  NOT a.CAD_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Activity = " AND a.CAD_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Activity = " AND a.CAD_DESC NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Activity = " AND a.CAD_DESC LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Activity = " AND a.CAD_DESC NOT LIKE '%" & txtSearch.Text & "'"
                End If

               
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_GRADE & str_filter_STREAM & str_filter_Group & str_filter_Activity & str_filter_SUBJECT & str_filter_CAS_DESC & str_filter_MARK & str_filter_DATE & TRM_ID & " ORDER BY a.DISPLAYORDER,A.STM_DESC")
            '& ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.Y_DESCR")
            If ds.Tables(0).Rows.Count > 0 Then
                gvAuthorizedRecord.DataSource = ds.Tables(0)
                gvAuthorizedRecord.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                gvAuthorizedRecord.DataSource = ds.Tables(0)
                Try
                    gvAuthorizedRecord.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvAuthorizedRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAuthorizedRecord.Rows(0).Cells.Clear()
                gvAuthorizedRecord.Rows(0).Cells.Add(New TableCell)
                gvAuthorizedRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAuthorizedRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAuthorizedRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRD_DESC")
            txtSearch.Text = str_GRADE
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTM_DESC")
            txtSearch.Text = str_STREAM
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSBG_DESC")
            txtSearch.Text = str_SUBJECT
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCAD_DESC")
            txtSearch.Text = str_activity
            'Call callYEAR_DESCRBind()
            'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub BindGroups(ByVal grd_id As String, ByVal SBG_ID As String, ByVal CAD_ID As String, ByVal vPARENT_ID As String, ByVal gvSubjects As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_Sql = " SELECT DISTINCT * FROM(SELECT     ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.ACTIVITY_D.CAD_DESC, VW_STREAM_M.STM_DESCR AS STM_DESC, " & _
                      " M.SGR_DESCR AS SGR_DESC, ACT.ACTIVITY_D.CAD_TRM_ID AS TRM_DESC, VW_GRADE_M.GRD_DISPLAYORDER AS DISPLAYORDER, " & _
                      " VW_GRADE_BSU_M.GRM_DISPLAY AS GRD_DESC, ACT.ACTIVITY_SCHEDULE.CAS_DATE, ACT.ACTIVITY_SCHEDULE.CAS_TIME, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_DUR_OF_EXAM AS DUR_OF_EXAM, REPLACE(CAST(ACT.ACTIVITY_SCHEDULE.CAS_MIN_MARK AS varchar(6)), '.00', '') " & _
                      " + ' & ' + REPLACE(CAST(ACT.ACTIVITY_SCHEDULE.CAS_MAX_MARK AS varchar(6)), '.00', '') AS MARK, " & _
                      " SUBJECTS_GRADE_S.SBG_DESCR AS SBG_DESC,(SELECT     ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS ENAME " & _
                      " FROM          VW_EMPLOYEE_M WHERE      (EMP_ID = M.SGR_EMP_ID)) AS ENAME, ACT.ACTIVITY_D.CAD_ACD_ID AS ACD_ID, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_TYPE_LEVEL AS TYPE_LEVEL, ACT.ACTIVITY_SCHEDULE.CAS_DESC, ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID PARENT_ID, " & _
                      " M.SGR_EMP_ID, ACT.ACTIVITY_D.CAD_ID, M.SGR_GRD_ID AS GRD_ID, SUBJECTS_GRADE_S.SBG_ID " & _
                      " FROM ACT.ACTIVITY_SCHEDULE INNER JOIN " & _
                      " ACT.ACTIVITY_D ON ACT.ACTIVITY_D.CAD_ID = ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID INNER JOIN " & _
                      " GROUPS_M AS M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = M.SGR_ID INNER JOIN " & _
                      " VW_GRADE_BSU_M ON M.SGR_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
                      " M.SGR_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                      " VW_TRM_M ON ACT.ACTIVITY_D.CAD_TRM_ID = VW_TRM_M.TRM_ID AND ACT.ACTIVITY_D.CAD_ACD_ID = VW_TRM_M.TRM_ACD_ID INNER JOIN " & _
                      " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                      " SUBJECTS_GRADE_S ON ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID INNER JOIN " & _
                      " VW_STREAM_M ON M.SGR_STM_ID = VW_STREAM_M.STM_ID)A " & _
                      " where A.CAD_ID =" & CAD_ID & " AND A.GRD_ID = '" & grd_id & _
                      "' AND SBG_ID = " & SBG_ID & " AND PARENT_ID = " & vPARENT_ID
                'If ViewState("GRD_ACCESS") > 0 Then
                '    str_Sql += " AND A.GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                '             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                '             & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
                'End If
            Else
                str_Sql = "  SELECT DISTINCT * FROM( SELECT     ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.ACTIVITY_D.CAD_DESC, VW_STREAM_M.STM_DESCR AS STM_DESC, " & _
                " GROUPS_M.SGR_DESCR AS SGR_DESC, ACT.ACTIVITY_D.CAD_TRM_ID AS TRM_DESC, VW_GRADE_M.GRD_DISPLAYORDER AS DISPLAYORDER, " & _
                  "  VW_GRADE_BSU_M.GRM_DISPLAY AS GRD_DESC, ACT.ACTIVITY_SCHEDULE.CAS_DATE, ACT.ACTIVITY_SCHEDULE.CAS_TIME, " & _
                  "  ACT.ACTIVITY_SCHEDULE.CAS_DUR_OF_EXAM AS DUR_OF_EXAM, REPLACE(CAST(ACT.ACTIVITY_SCHEDULE.CAS_MIN_MARK AS varchar(6)), '.00', '') " & _
                  "  + ' & ' + REPLACE(CAST(ACT.ACTIVITY_SCHEDULE.CAS_MAX_MARK AS varchar(6)), '.00', '') AS MARK, " & _
                  "  SUBJECTS_GRADE_S.SBG_DESCR AS SBG_DESC," & _
                  "  (SELECT     ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS ENAME " & _
                  "  FROM VW_EMPLOYEE_M   WHERE      (EMP_ID = S.SGS_EMP_ID)) AS ENAME, ACT.ACTIVITY_D.CAD_ACD_ID AS ACD_ID, " & _
                  "  ACT.ACTIVITY_SCHEDULE.CAS_TYPE_LEVEL AS TYPE_LEVEL, ACT.ACTIVITY_SCHEDULE.CAS_DESC, ACT.ACTIVITY_SCHEDULE.CAS_PARENT_ID PARENT_ID, " & _
                  "  ACT.ACTIVITY_D.CAD_ID, GROUPS_M.SGR_GRD_ID AS GRD_ID, SUBJECTS_GRADE_S.SBG_ID, " & _
                  "  GROUPS_M.SGR_EMP_ID, S.SGS_TODATE FROM ACT.ACTIVITY_SCHEDULE INNER JOIN " & _
                  "  ACT.ACTIVITY_D ON ACT.ACTIVITY_D.CAD_ID = ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID INNER JOIN " & _
                  "  GROUPS_M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = GROUPS_M.SGR_ID INNER JOIN " & _
                  "  VW_GRADE_BSU_M ON GROUPS_M.SGR_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
                  "  GROUPS_M.SGR_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                  "  VW_TRM_M ON ACT.ACTIVITY_D.CAD_TRM_ID = VW_TRM_M.TRM_ID AND ACT.ACTIVITY_D.CAD_ACD_ID = VW_TRM_M.TRM_ACD_ID INNER JOIN " & _
                  "  VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                  "  SUBJECTS_GRADE_S ON ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID INNER JOIN " & _
                  "  VW_STREAM_M ON GROUPS_M.SGR_STM_ID = VW_STREAM_M.STM_ID INNER JOIN " & _
                  "  GROUPS_TEACHER_S AS S ON GROUPS_M.SGR_ID = S.SGS_SGR_ID AND GROUPS_M.SGR_EMP_ID = S.SGS_EMP_ID)A " & _
                  " where A.SGS_TODATE Is NULL " & _
                  " AND A.CAD_ID =" & CAD_ID & " AND A.GRD_ID = '" & grd_id & "' AND SBG_ID = " & SBG_ID & _
                  " AND A.SGR_EMP_ID='" & ViewState("EMP_ID") & "'" & " AND PARENT_ID = " & vPARENT_ID
            End If
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
        gvSubjects.Attributes.Add("OnRowCommand", "gvDetails_RowCommand")
    End Sub

    Protected Sub btnSearchCAS_DATE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSCT_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSTM_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSHF_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvAuthorizedRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuthorizedRecord.PageIndexChanging
        gvAuthorizedRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Sub callYEAR_DESCRBind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
            " FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
            " WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub CALLTERM_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            str_Sql = " SELECT    TRM_ID, TRM_DESCRIPTION FROM  VW_TRM_M WHERE TRM_ACD_ID= '" & ACD_ID & "' ORDER BY TRM_DESCRIPTION "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlTerm.Items.Clear()
            ddlTerm.DataSource = ds.Tables(0)
            ddlTerm.DataTextField = "TRM_DESCRIPTION"
            ddlTerm.DataValueField = "TRM_ID"
            ddlTerm.DataBind()
            ddlTerm.Items.Add(New ListItem("ALL", ""))
            ddlTerm.ClearSelection()
            ddlTerm.Items.FindByText("ALL").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblCAS_ID, lblSBG_ID, lblCAD_ID As New Label
            lblCAS_ID = TryCast(sender.FindControl("lblCAS_ID"), Label)
            'lblSBG_ID = TryCast(sender.FindControl("lblSBG_ID"), Label)
            'lblCAD_ID = TryCast(sender.FindControl("lblCAD_ID"), Label)
            Dim url As String
            Dim viewid As String
            viewid = lblCAS_ID.Text '+ "____" + lblSBG_ID.Text + "____" + lblCAD_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("clmSkill_Schedule.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLTERM_DESCRBind()
        gridbind()

    End Sub


    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))


            If Encr_decrData.Decrypt(ViewState("MainMnu_code").replace(" ", "+")) = "C312015" Then
                url = String.Format("clmSkill_Schedule.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("clmSkill_Schedule_Dynamic.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub



    Protected Sub btnSearchGRD_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSGR_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSBG_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCAD_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchENAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchMark_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSTM_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub lblRetest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblCAS_ID As New Label
            Dim url As String
            Dim viewid As String
            lblCAS_ID = TryCast(sender.FindControl("lblCAS_ID"), Label)
            viewid = lblCAS_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "retest"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("clmSkill_Schedule.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvAuthorizedRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAuthorizedRecord.RowDataBound
        Try
            Dim lblGRD_ID, lblSBG_ID, lblCAD_ID, lblPARENT_ID As New Label
            lblGRD_ID = e.Row.FindControl("lblGRD_ID")
            lblSBG_ID = e.Row.FindControl("lblSBG_ID")
            lblCAD_ID = e.Row.FindControl("lblCAD_ID")
            lblPARENT_ID = e.Row.FindControl("lblPARENT_ID")
            If (Not lblGRD_ID Is Nothing AndAlso lblGRD_ID.Text <> "") AndAlso _
            (Not lblSBG_ID Is Nothing AndAlso lblSBG_ID.Text <> "") AndAlso _
            (Not lblCAD_ID Is Nothing AndAlso lblCAD_ID.Text <> "") AndAlso _
            (Not lblPARENT_ID Is Nothing AndAlso lblPARENT_ID.Text <> "") Then
                Dim gvGroupInfo As New GridView
                gvGroupInfo = e.Row.FindControl("gvGroupInfo")
                BindGroups(lblGRD_ID.Text, lblSBG_ID.Text, lblCAD_ID.Text, lblPARENT_ID.Text, gvGroupInfo)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        'End If
    End Sub


    Protected Sub lblAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblCAS_ID As New Label
            Dim url As String
            Dim viewid As String
            lblCAS_ID = TryCast(sender.FindControl("lblCAS_ID"), Label)
            viewid = lblCAS_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("clmSkill_Schedule_Alloc.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvAuthorizedRecord_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAuthorizedRecord.RowCommand
        If e.CommandName = "Deleting" Then
            Dim id = e.CommandArgument
            Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Dim stTrans As SqlTransaction
            Dim vParent_id, vCAD_ID, vGRD_ID, vSBG_ID As String
            Dim errNo As Integer
            Try
                vParent_id = id.ToString().Split("___")(0)
                vCAD_ID = id.ToString().Split("___")(3)
                vGRD_ID = id.ToString().Split("___")(6)
                vSBG_ID = id.ToString().Split("___")(9)
                objConn.Close()
                objConn.Open()
                stTrans = objConn.BeginTransaction
                errNo = SKILLSCHEDULE.SKILLSCHEDULE.DeleteBulkDetails(vParent_id, vCAD_ID, vGRD_ID, vSBG_ID, Session("sUsr_name"), objConn, stTrans)
                Dim str_KEY As String = "DELETE"
                If errNo = 0 Then
                    stTrans.Commit()
                    lblError.Text = "Schedules Deleted sucessfully..."
                    gridbind()
                Else
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(errNo)
                End If
            Catch
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(-1)
            Finally
                objConn.Close()
            End Try
        End If
    End Sub

    Protected Sub gvDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "Deleting" Then
            Dim id = e.CommandArgument
            Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Dim stTrans As SqlTransaction
            Dim errNo As Integer
            Try
                objConn.Close()
                objConn.Open()
                stTrans = objConn.BeginTransaction
                errNo = SKILLSCHEDULE.SKILLSCHEDULE.DeleteDetails(id, Session("sUsr_name"), objConn, stTrans)
                Dim str_KEY As String = "DELETE"
                If errNo = 0 Then
                    stTrans.Commit()
                    lblError.Text = "Schedule Deleted sucessfully..."
                    gridbind()
                Else
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(errNo)
                End If
            Catch
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(-1)
            Finally
                objConn.Close()
            End Try
        End If
    End Sub


End Class
