﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmTeacherGradeBook_CopyMarks.aspx.vb" Inherits="Curriculum_clmTeacherGradeBook_CopyMarks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
 
</head>
<body>
    <form id="form1" runat="server">
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <div >
     <asp:Label ID="lblError" runat="server" CssClass="error"/>
    <table width="100%" >
    <tr><td align="center" style="vertical-align:top;">
        <table  cellpadding="5" cellspacing="0" style="border-collapse: collapse" width="100%">
        <tr>
           <td  colspan="4" class="title-bg">
                    <asp:Label ID="lblDetails" runat="server"></asp:Label>
                </td>
        </tr>
            <tr>
             
                <td align="left" width="25%">
                   <span class="field-label">Term</span>
                </td>
                
                <td  align="left" width="30%">
                    <asp:DropDownList ID="ddlTerm" AutoPostBack="true" runat="server">
                    </asp:DropDownList>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <table id="tbAct" runat="server" Width="100%">
                        <tr>
                            <td align="left"  width="25%">
                                <span class="field-label">Select Operation</span>
                            </td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlOperation" runat="server" Width="127px" AutoPostBack="True">
                                    <asp:ListItem>SUM</asp:ListItem>
                                    <asp:ListItem>AVG</asp:ListItem>
                                    <asp:ListItem Value="MAX">BEST OF</asp:ListItem>
                                    <asp:ListItem Value="WT">WEIGHTAGE</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="gvAssmt" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    EmptyDataText="No Activities" Width="100%" AllowPaging="false">
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <HeaderStyle />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGbmId" Text='<%# Bind("GBM_ID") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assesment">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesc" Text='<%# Bind("GBM_DESCR") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Marks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMax" Text='<%# Bind("GBM_MAXMARK") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Weightage" Visible="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWT" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td align="left"><span class="field-label">Copy to Assessment</span></td>
            
            <td align="center"><asp:DropDownList runat="server" ID="ddlAssessment"></asp:DropDownList></td>
            <td colspan="2"><asp:Button runat="server" ID="btnProcess" Text="Process" Class="button" /></td>

            </tr>
            </table>
         </td>
            <td>
            <table id="tbMarks" runat="server" Width="100%">
            
            <tr>
            <td colspan="4">
                        <asp:GridView ID="gvMarks" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                          EmptyDataText="No Activities"  PageSize="20" Width="100%">
                                    <RowStyle CssClass="griditem" Height="15px" Wrap="False" />
                                    <Columns>
                                       <asp:TemplateField HeaderText="stu_id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStuId" Text='<%# Bind("STU_ID") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStuNo" Text='<%# Bind("STU_NO") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" Text='<%# Bind("STU_NAME") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Marks">
                                            <ItemTemplate>
                                               <asp:Label ID="lblMarks" Text='<%# Bind("MARKS") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Grade">
                                            <ItemTemplate>
                                               <asp:Label ID="lblGrade" Text='<%# Bind("GRADE") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Grade">
                                            <ItemTemplate>
                                                <asp:DropDownList id="ddlAttendance" runat="server" selectedValue='<%# Bind("ATTENDANCE") %>'>
                                                <asp:ListItem Value="P">Present</asp:ListItem>
                                                <asp:ListItem Value="A">Absent</asp:ListItem>
                                                <asp:ListItem Value="L">Approved Leave</asp:ListItem>
                                                </asp:DropDownList> 
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                      </Columns>
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                </asp:GridView>
           </td> </tr>
           <tr>
           <td colspan="4" align="center">
           <asp:Button runat="server" ID="btnSave" Text="Save" class="button" />
          
           </td>
           </tr>
        </table>
        </td></tr></table>
    </div>
                 </div>
            </div>
        </div>
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfSGR_ID" runat="server" />
        <asp:HiddenField ID="hfEMP_ID" runat="server" />
        <asp:HiddenField ID="hfSBG_ID" runat="server" />
    </form>
</body>
</html>
