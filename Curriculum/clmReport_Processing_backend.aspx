<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="clmReport_Processing_backend.aspx.vb" Inherits="Students_studAtt_Registration" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 
<TABLE id="Table1" width="100%" border=0><TBODY><TR style="FONT-SIZE: 12pt"><TD class="title" align=left width="50%"><asp:Literal id="ltHeader" runat="server" Text="Processing Report"></asp:Literal></TD></TR></TBODY></TABLE>

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="color: #c00000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 3px" valign="bottom">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    class="matters" style="width: 70%;">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal id="ltLabel" runat="server" Text="Filter Condition"></asp:Literal></span></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px">
                            Academic Year</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:DropDownList id="ddlAcd_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcd_Year_SelectedIndexChanged" >
                            </asp:DropDownList></td>
                        <td align="left" style="height: 21px">
                            Term
                        </td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px">
                            <asp:DropDownList id="ddlTrm_id" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTrm_id_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px">
                            Select Report card</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:DropDownList id="ddlRSM_ID" runat="server" OnSelectedIndexChanged="ddlRSM_ID_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" style="height: 21px">
                            Printed For</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" style="height: 21px">
                            <asp:DropDownList id="ddlRPF_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRPF_ID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px">
                            Column Header</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" colspan="4" style="height: 21px">
                                                    <asp:CheckBoxList id="chkRSD_IDs" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px">
                            Grade</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" colspan="4" style="height: 21px">
                            <asp:CheckBoxList id="chkGrade" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                            </asp:CheckBoxList></td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td class="matters" style="height: 9px" valign="bottom">
                <br />
             
                <asp:Button id="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process"  ValidationGroup="AttGroup" OnClick="btnProcess_Click" /></td>
        </tr>
        <tr>
            <td class="matters" style="height: 7px" valign="bottom">
                            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 7px" valign="bottom">
                </td>
        </tr>
    </table>
    &nbsp;&nbsp;
 
            
</asp:Content>

