<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmSubjectBrownbookSettings.aspx.vb" Inherits="Curriculum_clmSubjectBrownbookSettings"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Brownbook Settings
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" width="100%"
                    cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <%--<table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="center">--%>
                                        <table id="Table2" runat="server" align="center"
                                            cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left"  width="15%">
                                                    <asp:Label ID="Label1" class="field-label" runat="server" Text="Academic Year"></asp:Label></td>

                                                <td align="left">&nbsp;<asp:Label ID="lblAcademicYear" class="field-value" runat="server"></asp:Label></td>

                                                <td align="left"  width="15%">
                                                    <asp:Label ID="Label2" class="field-label" runat="server" Text="Grade"></asp:Label></td>

                                                <td align="left">&nbsp;<asp:Label ID="lblGrade" class="field-value" runat="server"></asp:Label></td>

                                                <td align="left"  width="15%">
                                                    <asp:Label ID="Label3" class="field-label" runat="server" Text="Stream"></asp:Label></td>

                                                <td align="left">&nbsp;<asp:Label ID="lblStream" class="field-value" runat="server"></asp:Label></td>

                                            </tr>
                                            <tr>
                                                <td colspan ="6">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" align="center" style="width: 100%">
                                                    <asp:GridView ID="gvSubject" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                       CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" Width="100%" BorderStyle="None">
                                                        <RowStyle Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Subject">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Min.Mark">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtMinMark" runat="server" Text='<%# Bind("SBG_MINMARK") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Retest ">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRetest" runat="server" Checked='<%# Bind("SBG_BRETEST") %>'></asp:CheckBox>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Display In Excel ">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkDisplay" runat="server" Checked='<%# Bind("SBG_bBROWNBOOKDISPLAY") %>'></asp:CheckBox>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Btec Subject ">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkBtec" runat="server" Checked='<%# Bind("SBG_bBTEC") %>'></asp:CheckBox>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="bottom" align="center" colspan="6">

                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                                        ValidationGroup="groupM1" CausesValidation="False" />
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Cancel" UseSubmitBehavior="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />
                                    </td>
                                </tr>
                            </table>
                       <%-- </td>
                    </tr>
                </table>--%>

            </div>
        </div>
    </div>

       <asp:HiddenField ID="hfACD_ID" runat="server" />
               <asp:HiddenField ID="hfGRD_ID" runat="server" />
               <asp:HiddenField ID="hfSTM_ID" runat="server" />


</asp:Content>
