Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SKILLSCHEDULE
Imports CURRICULUM

Partial Class clmSkillSchedule_Dynamic
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_ACTIVITY_SCHEDULE) Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else
                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                'Populate Academic Year
                Dim studCl As New studClass
                studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                'Populate Term 
                BindTerm()
                'Populate Activity
                BindActivity()

                BindActivityDetails()
                'Populate Grade
                GetAllGradeForActivity()
                ' PopulateDetails()
                BindGradeSlabDetails()
                If ViewState("datamode") = "view" Then
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    PopulateDetails(ViewState("viewid"))
                    Enable_disable_control(False, True)
                ElseIf ViewState("datamode") = "retest" Then
                    ltLabel.Text = "Retest Skill Schedule"
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    PopulateDetails(ViewState("viewid"), True)
                    h_PARENT_ID.Value = ViewState("viewid")
                    ViewState("datamode") = "add"
                    h_CAS_ID.Value = ""
                    ViewState("retest") = True
                    Enable_disable_control(False, True)
                    Enable_disable_control(True, False)
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ElseIf ViewState("datamode") = "add" Then
                End If
                'End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub PopulateDetails()
      
        Dim vACT_SCH() As SKILLSCHEDULE.SKILLSCHEDULE
        vACT_SCH = SKILLSCHEDULE.SKILLSCHEDULE.GetScheduleDetails("07", "52175", "2642")

        usrSkillScheduler1.FillDetails(vACT_SCH)

    End Sub

    Private Sub PopulateDetails(ByVal vCAS_ID As String, Optional ByVal bRetest As Boolean = False)
        Dim params As String() = vCAS_ID.Split("____")
        Dim vpGRD_ID, vpSBG_ID, vpCAD_ID As String
        vpGRD_ID = params(0)
        vpSBG_ID = params(4)
        vpCAD_ID = params(8)
        Dim vACT_SCH() As SKILLSCHEDULE.SKILLSCHEDULE
        vACT_SCH = SKILLSCHEDULE.SKILLSCHEDULE.GetScheduleDetails(vpGRD_ID, vpSBG_ID, vpCAD_ID)

        usrSkillScheduler1.FillDetails(vACT_SCH)

    End Sub

    Private Sub BindGradeSlabDetails()
        ddlGradeSlab.DataSource = SKILLSCHEDULE.SKILLSCHEDULE.GetGradeSlab(Session("sBSUID"))
        ddlGradeSlab.DataTextField = "GSM_DESC"
        ddlGradeSlab.DataValueField = "GSM_SLAB_ID"
        ddlGradeSlab.DataBind()
        ddlGradeSlab.Items.Add(New ListItem("--", 0))
        ddlGradeSlab.Items.FindByText("--").Selected = True
        ddlGradeSlab.Attributes.Add("onchange", "SetMaxMark(this,'" & txtMax.ClientID & "');")
    End Sub

    Private Sub FillSelectedDetails(ByRef lstitems As ListItemCollection, ByVal selVal As Hashtable)
        Dim ienum As IDictionaryEnumerator = selVal.GetEnumerator
        While ienum.MoveNext
            lstitems.FindByValue(ienum.Value).Selected = True
        End While
    End Sub

    Private Sub BindActivity()
        ddlActivity.DataSource = ACTIVITYMASTER.GetACTIVITY_M(Session("sBSUID"))
        ddlActivity.DataTextField = "CAM_DESC"
        ddlActivity.DataValueField = "CAM_ID"
        ddlActivity.DataBind()
        BindActivityDetails()
    End Sub

    Private Sub BindActivityDetails()
        ddlACTIVITYDET.DataSource = SKILLSCHEDULE.SKILLSCHEDULE.GetActivityDetails(ddlAca_Year.SelectedValue, ddlTerm.SelectedValue, ddlActivity.SelectedValue)
        ddlACTIVITYDET.DataTextField = "CAD_DESC"
        ddlACTIVITYDET.DataValueField = "CAD_ID"
        ddlACTIVITYDET.DataBind()
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub

    Sub GetAllGradeForActivity()
        PopulateGrade(ddlGrade, ddlAca_Year.SelectedValue)
        h_GRD_IDs.Value = ddlGrade.SelectedValue
        GetSelectedSubjects()
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

        Session("CurrSuperUser") = "Y"  ' FOR TESTING PURPOSE ONLY
        If Not Session("CurrSuperUser") Is Nothing Then
            If Session("CurrSuperUser") = "Y" Then
                str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + acdid
                If ViewState("GRD_ACCESS") > 0 Then
                    str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                             & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                             & " WHERE GSA_ACD_ID=" + Session("Current_ACD_ID") + " and  (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
                End If
                str_query += " order by grd_displayorder"
            Else
                
                str_query = "SELECT DISTINCT " & _
                "CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY + '-' + STM_DESCR END AS GRM_DISPLAY, " & _
                " GRADE_BSU_M.GRM_GRD_ID + '|' + CONVERT(VARCHAR(100), STREAM_M.STM_ID) AS GRM_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
                " STREAM_M.STM_ID FROM GROUPS_TEACHER_S INNER JOIN " & _
                " GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID  " & _
                " AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
                "GRADE_BSU_M INNER JOIN " & _
                " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
                " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID ON GROUPS_M.SGR_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                " GROUPS_M.SGR_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND GROUPS_M.SGR_STM_ID = GRADE_BSU_M.GRM_STM_ID AND " & _
                " GROUPS_M.SGR_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & " WHERE " _
                      & "  grm_acd_id=" & acdid & " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId")
                str_query += " order by grd_displayorder"
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If ddlTerm.SelectedIndex <> -1 Then
            BindActivity()
        Else
            ddlACTIVITYDET.Items.Clear()
        End If
    End Sub

    Sub Enable_disable_control(ByVal bEnable As Boolean, ByVal bAll As Boolean)
        If bAll Then
            ddlAca_Year.Enabled = bEnable
            ddlTerm.Enabled = bEnable
            ddlGrade.Enabled = bEnable
            ddlACTIVITYDET.Enabled = bEnable
        End If
    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlAca_Year.Enabled = False
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ACT_SCH() As SKILLSCHEDULE.SKILLSCHEDULE = usrSkillScheduler1.GetScheduleDetails(lblError.Text)
        If lblError.Text <> "" Then
            Exit Sub
        End If
        Dim objConn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = SKILLSCHEDULE.SKILLSCHEDULE.SaveDetails(ACT_SCH, objConn, stTrans)
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'errNo = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            If errNo = 0 Then
                stTrans.Commit()
                'stTrans.Rollback()
                ClearAllFields()
                lblError.Text = "Details saved sucessfully..."
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(errNo)
            End If
        Catch
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(-1)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearAllFields(Optional ByVal bClearTerm As Boolean = True)
        If bClearTerm Then
            ddlTerm.ClearSelection()
        End If
        'ddlGrade.ClearSelection()
        h_SBM_ID.Value = ""
        'usrSubjects.SelectAll = False
        'ddlSubjects.ClearSelection()
        usrSkillScheduler1.Bind()
        usrSkillScheduler1.Clear()

    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Enable_disable_control(True, False)
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Enable_disable_control(True, True)
            ClearAllFields()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAllFields()
        BindTerm()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

  

    Private Sub GetSelectedSubjects()

        Dim STM_ID As String = h_GRD_IDs.Value.Split("|")(1)
        h_GRD_IDs.Value = h_GRD_IDs.Value.Split("|")(0)
        Dim str_sql As String = String.Empty

        If Session("CurrSuperUser") = "Y" Then
            str_sql = "SELECT DISTINCT SUBJECTS_GRADE_S.SBG_ID," & _
            "(CASE SBG_PARENTS WHEN 'NA' THEN '' ELSE SBG_PARENTS+ '-' END)  + SBG_DESCR SBG_DESCR " & _
            " FROM SUBJECTS_GRADE_S " & _
            " WHERE SUBJECTS_GRADE_S.SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
            " AND SUBJECTS_GRADE_S.SBG_GRD_ID IN ('" & h_GRD_IDs.Value.Replace("___", "', '") & "')" & _
            " AND SUBJECTS_GRADE_S.SBG_STM_ID = " & STM_ID & _
            " AND SUBJECTS_GRADE_S.SBG_ACD_ID = " & ddlAca_Year.SelectedValue & _
            " ORDER BY SBG_DESCR "
        Else
            str_sql = "SELECT DISTINCT SUBJECTS_GRADE_S.SBG_ID," & _
        " (CASE SBG_PARENTS WHEN 'NA' THEN '' ELSE SBG_PARENTS+ '-' END)  + SBG_DESCR SBG_DESCR " & _
        " FROM GROUPS_TEACHER_S INNER JOIN GROUPS_M ON GROUPS_TEACHER_S.SGS_SGR_ID = GROUPS_M.SGR_ID " & _
        " AND (GROUPS_TEACHER_S.SGS_TODATE IS NULL) INNER JOIN " & _
        " SUBJECTS_GRADE_S ON GROUPS_M.SGR_BSU_ID = SUBJECTS_GRADE_S.SBG_BSU_ID AND  " & _
        " GROUPS_M.SGR_ACD_ID = SUBJECTS_GRADE_S.SBG_ACD_ID AND  " & _
        " GROUPS_M.SGR_GRD_ID = SUBJECTS_GRADE_S.SBG_GRD_ID AND GROUPS_M.SGR_SBG_ID = SUBJECTS_GRADE_S.SBG_ID AND " & _
        " GROUPS_M.SGR_STM_ID = SUBJECTS_GRADE_S.SBG_STM_ID " & _
        " WHERE SUBJECTS_GRADE_S.SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
        " AND SUBJECTS_GRADE_S.SBG_GRD_ID IN ('" & h_GRD_IDs.Value.Replace("___", "', '") & "')" & _
        " AND SUBJECTS_GRADE_S.SBG_STM_ID = " & STM_ID & _
        " AND SUBJECTS_GRADE_S.SBG_ACD_ID = " & ddlAca_Year.SelectedValue & _
        " AND GROUPS_TEACHER_S.SGS_EMP_ID = " & Session("EmployeeId") & _
        " ORDER BY SBG_DESCR "
        End If
 
        ddlSubjects.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

        ddlSubjects.DataTextField = "SBG_DESCR"
        ddlSubjects.DataValueField = "SBG_ID"
        'usrSubjects.SelectAll = True
        ddlSubjects.DataBind()
    End Sub

    Private Function GetAssessmentType() As ASSESSMENT_TYPE
        Return SKILLSCHEDULE.SKILLSCHEDULE.GetAssessmentType(ddlACTIVITYDET.SelectedValue)
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        h_PARENT_ID.Value = ""
        ViewState("reset") = Nothing
        ltLabel.Text = "Activity Schedule"
        ClearAllFields()
        Enable_disable_control(True, True)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        ClearAllFields(False)
        BindActivity()
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        usrSkillScheduler1.Grades = h_GRD_IDs.Value
        usrSkillScheduler1.ACD_ID = ddlAca_Year.SelectedValue
        usrSkillScheduler1.BSU_ID = Session("sBSUID")
        Dim STM_ID As String = ddlGrade.SelectedValue.Split("|")(1)
        usrSkillScheduler1.STM_ID = STM_ID '"1"
        usrSkillScheduler1.AssessmentType = GetAssessmentType()
        usrSkillScheduler1.TRM_ID = ddlTerm.SelectedValue
        usrSkillScheduler1.Activity_ID = ddlACTIVITYDET.SelectedValue
        usrSkillScheduler1.ACTIVITY_DESCRIPTION = ddlACTIVITYDET.SelectedItem.Text
        'usrSkillScheduler1.Subjects = usrSubjects.GetSelectedNode("___")
        usrSkillScheduler1.Subjects = ddlSubjects.SelectedItem.Value
        usrSkillScheduler1.maxMark = txtMax.Text
        usrSkillScheduler1.minMark = txtMin.Text
        usrSkillScheduler1.startDate = txtDate.Text
        usrSkillScheduler1.gradingSlab = ddlGradeSlab.SelectedItem.Text
        GetSkillCount()
        usrSkillScheduler1.Bind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_GRD_IDs.Value = ddlGrade.SelectedValue
        GetSelectedSubjects()
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindActivityDetails()
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub

    Protected Sub ddlACTIVITYDET_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACTIVITYDET.SelectedIndexChanged
        SetAOLDetails(ddlACTIVITYDET.SelectedValue)
    End Sub


    Public Sub SetAOLDetails(ByVal vCAD_ID As String)
        Dim str_sql As String = String.Empty
        str_sql = "SELECT ISNULL(CAD_bAOL, 0) CAD_bAOLAD_ID,ISNULL(CAD_bWITHOUTSKILLS, 0) CAD_bWITHOUTSKILLS FROM ACT.ACTIVITY_D " & _
        " WHERE CAD_ACD_ID= '" & ddlAca_Year.SelectedValue & "' AND  CAD_ID ='" & vCAD_ID + "'"
        'Dim bHasAOLDetails As Boolean
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            usrSkillScheduler1.HasAOLExams = ds.Tables(0).Rows(0).Item(0)
            usrSkillScheduler1.WithoutSkills = ds.Tables(0).Rows(0).Item(1)
        End If
    End Sub

    Public Sub GetSkillCount()
        Dim str_sql As String = String.Empty
        'str_sql = "SELECT	(len(sks_skl_ids)- len(replace(sks_skl_ids,'|','')))+1 as subcount ," & _
        '          "c.ID as skl_id,skl_descr " & _
        '          "FROM	skil.skills_s a " & _
        '          "INNER JOIN subjects_grade_s b ON " & _
        '          "sbg_id = sks_sbg_id " & _
        '          "CROSS APPLY dbo.fnSplitMe(a.SKS_SKL_IDS,'|') c " & _
        '          "INNER JOIN skil.SKILLS_M	on " & _
        '          "skl_id = c.ID " & _
        '          "WHERE SKS_CAD_ID = " + ddlACTIVITYDET.SelectedItem.Value + " AND " & _
        '          " SKS_GRD_ID = '" + h_GRD_IDs.Value + "' AND " & _
        '          " SKL_ACD_ID = " + ddlAca_Year.SelectedItem.Value + " AND " & _
        '          " SKS_SBG_ID = " + ddlSubjects.SelectedItem.Value

        str_sql = "SELECT " & _
                    "(	Select Count(*) " & _
                    "FROM SKIL.SKILLS_M " & _
                    "WHERE SKL_GRD_ID = " + h_GRD_IDs.Value + " And " & _
                    "SKL_ACD_ID = " + ddlAca_Year.SelectedItem.Value + " And " & _
                    "SKL_SBG_ID = " + ddlSubjects.SelectedItem.Value + " " & _
                    ") subcount , SKL_ID,SKL_DESCR " & _
                    "FROM SKIL.SKILLS_M " & _
                    "WHERE " & _
                    "SKL_GRD_ID = " + h_GRD_IDs.Value + "    AND  " & _
                    "SKL_ACD_ID = " + ddlAca_Year.SelectedItem.Value + "	AND  " & _
                    "SKL_SBG_ID = " + ddlSubjects.SelectedItem.Value + " "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            usrSkillScheduler1.CountSkills = ds.Tables(0).Rows(0).Item(0)
            Session("CountSkills") = ds.Tables(0).Rows(0).Item(0)
            Session("SkillIds") = ds.Tables(0)
            usrSkillScheduler1.SKL_IDs = ds.Tables(0)
            'usrSkillScheduler1.SKILL_IDs = convertDataTableToHashTable(ds.Tables(0), "skl_id", "skl_descr")

        Else
            usrSkillScheduler1.CountSkills = 0
        End If
    End Sub

    Public Shared Function convertDataTableToHashTable(ByVal dtIn As DataTable, ByVal keyField As String, ByVal valueField As String) As Hashtable
        Dim htOut As Hashtable = New Hashtable()
        For Each drIn As DataRow In dtIn.Rows
            htOut.Add(drIn(keyField).ToString(), drIn(valueField).ToString())
        Next

        Return htOut
    End Function

End Class
