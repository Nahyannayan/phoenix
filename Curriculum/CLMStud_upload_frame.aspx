﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CLMStud_upload_frame.aspx.vb" Inherits="Students_Stud_upload_frame" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <%--<link href="../cssfiles/ControlStyle.css" rel="stylesheet" type="text/css" />
<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <%--  <link href="../cssfiles/sb-admin.css" rel="stylesheet" >--%>


<head runat="server">
    <title>Untitled Page</title>
</head>
 
<script language="javascript" type="text/javascript">
 //Trim the input text
  function Trim(input)
  {
    var lre = /^\s*/; 
    var rre = /\s*$/; 
    input = input.replace(lre, ""); 
    input = input.replace(rre, ""); 
    return input; 
   }
 
   // filter the files before Uploading for text file only  
   function CheckUploadedFile() 
   {
        var file = document.getElementById('<%=fileload.ClientID%>');
        var fileName=file.value;    
        //var objFSO = new ActiveXObject("Scripting.FileSystemObject");
//            var files;
//            var size;
//             var path = file.value;    
        //Checking for file browsed or not 
        if (Trim(fileName) =='' )
        {
            alert("Please select a file to upload!!!");
            file.focus();
            return false;
        }
 
       //Setting the extension array for diff. type of text files 
         
       var extType= new Array(".zip", ".rar");

       //getting the file name
       while (fileName.indexOf("\\") != -1)
         fileName = fileName.slice(fileName.indexOf("\\") + 1);
 
 
       //Getting the file extension                     
       var ext = fileName.slice(fileName.lastIndexOf(".")).toLowerCase();
 
       //matching extension with our given extensions.
    for (var i = 0; i < extType.length; i++) 
        {

         if (extType[i] == ext) 
         { 
           return true;
         }
         
           }  

//        //alert(path);

//        files = objFSO.getFile(path);
//      

//        size = files.size ; // This size will be in Bytes

//  // We are converting it to KB as below

//        alert('File Size is : ' + files.size /1024 +' KB'); 

         alert("Please only upload files that end in types:  "+(extType.join("  ").toUpperCase())  
           +  "\nPlease select a new "
           + "file to upload and submit again.");
           file.focus();
           return false;                
   }  
 

    </script>
<body  style="margin-top:0px; padding-top:0px; padding-bottom:10px;">
    <form id="form1" runat="server">
    <div>
    <asp:FileUpload ID="fileload" runat="server" 
            /> &nbsp;&nbsp; 
      <asp:Button ID="btnProcess" runat="server" Text="Process file to Upload"  OnClientClick="return CheckUploadedFile();" CssClass="button"  /><br />
                    <div>
                        <asp:Literal ID="ltNote" runat="server"></asp:Literal>
                    </div>
        <asp:Label ID="lblErrorLoad" runat="server" Text="" CssClass="error"></asp:Label>  
        <asp:HiddenField ID="hf_reload" runat="server" Value="0" />         
    </div>
    
    </form>
</body>
</html>
<%--<script language="javascript" type="text/javascript">
var status =document.getElementById('<%=hf_reload.clientId %>').value;
if (status=="1")
 {
 alert("hello");
parent.location.reload();
document.getElementById('<%=hf_reload.ClientID %>').value="0";
 }
 
</script>--%>