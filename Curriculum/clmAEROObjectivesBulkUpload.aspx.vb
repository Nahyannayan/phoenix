﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Imports Telerik.Web.UI
Imports System.Data.OleDb
Partial Class clmAEROObjectivesBulkUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100219") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    lblReportCaption.Text = Mainclass.GetMenuCaption(ViewState("MainMnu_code"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    '      Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("isAgeBand") = False
                    BindAcyear()
                    BindTerm()
                    BindGrade()
                    ' BindAgeBand()
                    ' BindSubject()
                    ' GridBind()
                    trhide.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SYC_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Topic"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Sub_Topic"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Objective"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Start_Date"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "End_Date"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "No_Of_Lesson"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "b_Rubric"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "b_Topic_Skill"
        dt.Columns.Add(column)
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Step"
        dt.Columns.Add(column)
        Return dt
    End Function

    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBJ_ID,OBJ_DISPLAYORDER,OBJ_DESCR,OBJ_WEIGHTAGE,ISNULL(OBJ_OBT_ID,0) AS OBJ_OBT_ID," _
                                & " ISNULL(OBJ_bTARGET,'FALSE') AS OBJ_bTARGET FROM " _
                                & " OBJTRACK.OBJECTIVES_M WHERE OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                                & " AND OBJ_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                                '& " AND OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString
        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = .Item(2)
                dr.Item(3) = .Item(3)
                dr.Item(4) = "edit"
                dr.Item(5) = i.ToString
                dr.Item(6) = .Item(4)
                dr.Item(7) = .Item(5)
                dr.Item(8) = .Item(6)
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 100 rows
        For i = 0 To 100 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = ""
            dr.Item(3) = ""
            dr.Item(4) = "add"
            dr.Item(5) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(6) = "0"
            dr.Item(7) = "false"
            dr.Item(8) = ""
            dt.Rows.Add(dr)
        Next

        Session("dtObj") = dt



        gvObjectives.DataSource = dt
        gvObjectives.DataBind()

        gvObjectives.Columns(2).Visible = bShowSlno

        Dim ddlCategory As DropDownList
        Dim lblCatId As Label
        Dim li As ListItem

        str_query = "SELECT OBT_ID,OBT_DESCR FROM " _
                     & " OBJTRACK.OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                     & " AND OBT_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                     & " ORDER BY OBT_DESCR"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To gvObjectives.Rows.Count - 1
            lblCatId = gvObjectives.Rows(i).FindControl("lblCatId")
            ddlCategory = gvObjectives.Rows(i).FindControl("ddlCategory")
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "OBT_DESCR"
            ddlCategory.DataValueField = "OBT_ID"
            ddlCategory.DataBind()

            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlCategory.Items.Insert(0, li)

            If Not ddlCategory.Items.FindByValue(lblCatId.Text) Is Nothing Then
                ddlCategory.Items.FindByValue(lblCatId.Text).Selected = True
            End If

        Next


    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblObjId As Label
        Dim txtTopic As TextBox
        Dim txtSubTopic As TextBox
        Dim txtObjective As TextBox
        Dim txtStartDate As TextBox
        Dim txtEndDate As TextBox
        Dim txtLesson As TextBox
        Dim chkRubric As CheckBox
        Dim chkTopicSkill As CheckBox
        Dim txtStep As TextBox
        Dim lblDelete As Label
        Dim mode As String = "add"

        For i = 0 To gvObjectives.Rows.Count - 1
            With gvObjectives.Rows(i)
                lblObjId = .FindControl("lblObjId")
                txtTopic = .FindControl("txtTopic")
                txtSubTopic = .FindControl("txtSubTopic")
                txtObjective = .FindControl("txtObjective")
                txtStartDate = .FindControl("txtStartDate")
                txtEndDate = .FindControl("txtEndDate")
                txtLesson = .FindControl("txtLesson")
                chkRubric = .FindControl("chkRubric")
                Dim grade = ddlGrade.SelectedValue.Split("|")
                chkTopicSkill = .FindControl("chkTopicSkill")
                txtStep = .FindControl("txtStep")
                lblDelete = .FindControl("lblDelete")

                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblObjId.Text = "0" And txtObjective.Text <> "" Then
                    mode = "add"
                    'ElseIf lblObjective.Text <> txtObjective.Text Or lblOrder.Text <> txtOrder.Text Or txtWeightage.Text <> lblWeightage.Text Or lblCatId.Text <> ddlCategory.SelectedValue.ToString.ToString Or lblTarget.Text <> chkTarget.Checked.ToString Then
                    '    mode = "edit"
                Else
                    mode = ""
                End If
                Dim lesson As Double = IIf(String.IsNullOrEmpty(txtLesson.Text), 0, txtLesson.Text)

                If mode <> "" Then
                    ' str_query = "exec saveAERO_OBJECTIVES_BULK " _
                    '& " @SYC_ID=" + lblObjId.Text + "," _
                    '& " @SYC_ACD_ID=" + ddlAcyear.SelectedValue + "," _
                    '& " @SYC_GRD_ID='" + grade(0) + "'," _
                    '& " @SYC_SBG_ID=" + ddlSubject.SelectedValue + "," _
                    '& " @SYC_SYD_ID=" + "0" + "," _
                    '& " @SYC_DESCR=N'" + txtObjective.Text.Replace("'", "''") + "'," _
                    '& " @SYC_STARTDT='" + txtStartDate.Text.ToString + "'," _
                    '& " @SYC_ENDDT='" + txtEndDate.Text.ToString + "'," _
                    '& " @SYC_LESSONS=" + Convert.ToString(lesson) + " ," _
                    '& " @SYD_SubTopic='" + txtSubTopic.Text.Replace("'", "''") + "'," _
                    '& " @SYD_Topic='" + txtTopic.Text.Replace("'", "''") + "'," _
                    '& " @SYC_bRUBRIC='" + chkRubric.Checked.ToString + "'," _
                    '& " @SYM_BSU_ID='" + Session("sBsuid").ToString + "'," _
                    '& " @SYM_TRM_ID='" + ddlTerm.SelectedValue + "'," _
                    '& " @SYC_bEVALUATION='" + chkTopicSkill.Checked.ToString + "'," _
                    '& " @SYS_ISAGEBAND=" + IIf(ViewState("isAgeBand"), ddlAgeband.SelectedValue, "0").ToString() + "," _
                    '& " @SYC_STEP='" + txtStep.Text.ToString + "'"

                    str_query = "exec saveAERO_OBJECTIVES_BULK " _
               & " @SYC_ID=" + lblObjId.Text + "," _
               & " @SYC_ACD_ID=" + ddlAcyear.SelectedValue + "," _
               & " @SYC_GRD_ID='" + grade(0) + "'," _
               & " @SYC_SBG_ID=" + ddlSubject.SelectedValue + "," _
               & " @SYC_SYD_ID=" + "0" + "," _
               & " @SYC_DESCR=N'" + txtObjective.Text.Replace("'", "''") + "'," _
               & " @SYC_STARTDT='" + txtStartDate.Text.ToString + "'," _
               & " @SYC_ENDDT='" + txtEndDate.Text.ToString + "'," _
               & " @SYC_LESSONS=" + Convert.ToString(lesson) + " ," _
               & " @SYD_SubTopic=N'" + txtSubTopic.Text.ToString + "'," _
               & " @SYD_Topic=N'" + txtTopic.Text.ToString + "'," _
               & " @SYC_bRUBRIC='" + chkRubric.Checked.ToString + "'," _
               & " @SYM_BSU_ID='" + Session("sBsuid").ToString + "'," _
               & " @SYM_TRM_ID='" + ddlTerm.SelectedValue + "'," _
               & " @SYC_bEVALUATION='" + chkTopicSkill.Checked.ToString + "'," _
               & " @SYS_ISAGEBAND=" + IIf(ViewState("isAgeBand"), ddlAgeband.SelectedValue, "0").ToString() + "," _
               & " @SYC_STEP='" + txtStep.Text.ToString + "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If

            End With
        Next
        If mode <> "" Then
            lblError.Text = "Record Saved Successfully."
            btnSave1.Visible = False
            btnSave.Visible = False
            gvObjectives.Visible = False
        Else
            lblError.Text = "Record couldn't be processed."
        End If


    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvObjectives.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub UpLoadExcelFile()
        If uploadFile.UploadedFiles.Count > 0 Then
            Dim tempDir As String = HttpContext.Current.Server.MapPath("~/Curriculum/ReportDownloads/")
            'Dim tempDir As String = "~/Curriculum/ReportDownloads/"

            Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls" ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."

            Dim tempFileNameUsed As String = tempDir + tempFileName
            Dim fname As String = uploadFile.UploadedFiles(0).GetNameWithoutExtension










            If uploadFile.UploadedFiles.Count Then
                If File.Exists(tempFileNameUsed) Then
                    File.Delete(tempFileNameUsed)
                End If

                uploadFile.UploadedFiles(0).SaveAs(tempFileNameUsed)
                Try
                    getdataExcel(tempFileNameUsed)
                    File.Delete(tempFileNameUsed)
                    lblError.Text = ""
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed"
                End Try
            End If
        End If
    End Sub

    Sub getdataExcel(ByVal filename As String)
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        ' Dim ef As ExcelFile = New ExcelFile
        'Upload Excel data to datatb
        Dim connString As String = ""
        Dim strFileType As String = Path.GetExtension(uploadFile.UploadedFiles(0).FileName).ToLower()
        Dim path__1 As String = uploadFile.UploadedFiles(0).FileName
        Dim extension As String = Path.GetExtension(uploadFile.UploadedFiles(0).FileName)
        Select Case extension
            Case ".xls"
                'Excel 97-03

                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                Exit Select
            Case ".xlsx"
                'Excel 07 or higher
                'connString = ConfigurationManager.ConnectionStrings("Excel07+ConString").ConnectionString
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filename & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Exit Select

        End Select
        connString = String.Format(connString)
        Dim dtExcelData As New DataTable()
        Using excel_con As New OleDbConnection(connString)
            excel_con.Open()
            Dim sheet1 As String = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing).Rows(0)("TABLE_NAME").ToString()



            Using oda As New OleDbDataAdapter((Convert.ToString("SELECT * FROM [") & sheet1) + "]", excel_con)
                oda.Fill(dtExcelData)
            End Using
            excel_con.Close()

        End Using

        Dim mObj As ExcelRowCollection

        Dim iRowRead As Boolean
        iRowRead = True

        'Dim ef = ExcelFile.Load(filename)
        Dim mRowObj As ExcelRow
        'mObj = ef.Worksheets(0).Rows
        Dim iRow As Integer = 0

        Dim dt As DataTable = SetDataTable()

        Dim columnCount As Integer = dtExcelData.Columns.Count
        Dim rowCount As Integer = dtExcelData.Rows.Count

        Dim dr As DataRow

        While iRow < rowCount
            Dim colIndex As Integer = 0

            If dtExcelData.Rows(iRow).Item(0) Is Nothing Then
                Exit While
            End If
            If dtExcelData.Columns(0).ToString = "" Then
                Exit While
            End If
            If dtExcelData.Rows(iRow).Item(2).ToString() = "" Then
                Exit While
            End If
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = iRow
            dr.Item(2) = IIf(dtExcelData.Rows(iRow).Item(0) Is Nothing, "", dtExcelData.Rows(iRow).Item(0)).ToString
            dr.Item(3) = IIf(dtExcelData.Rows(iRow).Item(1) Is Nothing, "", dtExcelData.Rows(iRow).Item(1)).ToString
            dr.Item(4) = IIf(dtExcelData.Rows(iRow).Item(2) Is Nothing, "", dtExcelData.Rows(iRow).Item(2)).ToString

            dr.Item(5) = DirectCast(IIf(dtExcelData.Rows(iRow).Item(3) Is Nothing, CType("1/1/1900", Date), dtExcelData.Rows(iRow).Item(3)), DateTime).ToString("dd/MMM/yyyy")

            dr.Item(6) = DirectCast(IIf(dtExcelData.Rows(iRow).Item(4) Is Nothing, CType("1/1/1900", Date), dtExcelData.Rows(iRow).Item(4)), DateTime).ToString("dd/MMM/yyyy")
            dr.Item(7) = IIf(dtExcelData.Rows(iRow).Item(5) Is Nothing, "", dtExcelData.Rows(iRow).Item(5)).ToString
            dr.Item(8) = "True" 'IIf(IIf(mRowObj.Cells(6).Value Is Nothing, "Yes", mRowObj.Cells(6).Value).ToString = "Yes", "True", "False")
            dr.Item(9) = "True" 'IIf(IIf(mRowObj.Cells(7).Value Is Nothing, "Yes", mRowObj.Cells(7).Value).ToString = "Yes", "True", "False")
            dr.Item(10) = IIf(dtExcelData.Rows(iRow).Item(6) Is Nothing, "", dtExcelData.Rows(iRow).Item(6)).ToString
            dt.Rows.Add(dr)

            iRow += 1
        End While


        gvObjectives.DataSource = dt
        gvObjectives.DataBind()
    End Sub





#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        ' GridBind()

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If Not uploadFile.UploadedFiles.Count > 0 Then
            lblError.Text = "Select Excel Sheet...!"
            Exit Sub
        End If
        If uploadFile.UploadedFiles.Count > 1 Then
            lblError.Text = "Only 1 Excel Sheet Can Be Uploaded At A Time...!"
            Exit Sub
        End If
        If Not (uploadFile.UploadedFiles(0).FileName.EndsWith("xls") Or uploadFile.UploadedFiles(0).FileName.EndsWith("xlsx")) Then
            lblError.Text = "Invalid file type.. Only excel files are alowed.!"
            Exit Sub
        End If


        UpLoadExcelFile()
        btnSave.Visible = True
        btnSave1.Visible = True
    End Sub
    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        '  GridBind()
    End Sub
    Sub BindAcyear()
        '   Session("clm") = "1"
        ddlAcyear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcyear.DataTextField = "ACY_DESCR"
        ddlAcyear.DataValueField = "ACD_ID"
        ddlAcyear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcyear.Items(ddlAcyear.Items.IndexOf(li)).Selected = True
    End Sub
    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAcyear.SelectedItem.Value)
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        ''Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        'Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION FROM [dbo].[term_sub_master] " _
        '                        & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
        '                        & " AND TRM_ACD_ID =" + ddlAcyear.SelectedItem.Value _
        '                        & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID,grade_bsu_m.GRM_ISAGEBAND FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + ddlAcyear.SelectedValue
        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)

    End Sub
    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A  " _
                                 & " WHERE SBG_ACD_ID=" + ddlAcyear.SelectedValue
        'INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID  (REMOVED AS REQUESTED BY CHARAN)

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)
    End Sub

    Sub BindAgeBand()
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("[OASIS].dbo.[GET_GRADE_AGEBAND]", sqlCon)
        cmd.CommandType = CommandType.StoredProcedure
        sqlCon.Open()
        Using dr As SqlDataReader = cmd.ExecuteReader()
            If dr.HasRows Then
                ddlAgeband.DataSource = dr
                ddlAgeband.DataTextField = "GRD_AGEB_CATEGORY"
                ddlAgeband.DataValueField = "GRD_AGEB_ID"
                ddlAgeband.DataBind()
                Dim li As New ListItem
                li.Text = "Select"
                li.Value = "0"
                ddlAgeband.Items.Insert(0, li)
            End If
        End Using
        sqlCon.Close()
    End Sub
    Protected Sub ddlAcyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcyear.SelectedIndexChanged
        BindTerm()
        BindGrade()
        BindSubject()
    End Sub
    Protected Function CheckAgeBand() As Boolean

        Try
            Dim bool As Boolean = False
            ' code portion to find the ageband flag 
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcyear.SelectedValue, SqlDbType.BigInt)
            param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
            bool = SqlHelper.ExecuteScalar(str_conn, "GET_GRADE_ISAGEBAND", param)
            ' end of code portion 
            Return bool

        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ViewState("isAgeBand") = False
        BindSubject()
        'check if this grade has age band, if so then do bind age band
        Dim isAgeBand As Boolean = CheckAgeBand()

        If isAgeBand Then
            ViewState("isAgeBand") = isAgeBand
            BindAgeBand()
            ' ddlAgeband.Enabled = True
            trhide.Visible = True
        Else
            ViewState("isAgeBand") = False
            trhide.Visible = False

        End If
    End Sub

    Protected Sub btnTemplate_Click(sender As Object, e As EventArgs)
        Dim filename As String = Server.MapPath("~/Curriculum/Reports/RPT/ObjectivesUploadTemplate.xls")
        DownloadFile(filename)
    End Sub
    Sub DownloadFile(filename As String)
        Try

            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.ClearHeaders()
            'HttpContext.Current.Response.ClearContent()
            'HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename)
            'HttpContext.Current.Response.ContentType = "application/msexcel"
            Dim bytes() As Byte = File.ReadAllBytes(filename)

            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(filename))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
