﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmCodesTested

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100392") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindSubject()
                    BindCodesTestedCategory()
                    GridBind()
                    gvCategory.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub


#Region "Private Methods"
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S" _
                               & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "CTM_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "CTM_DESCR"
        dt.Columns.Add(column)

        
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "CTM_SHORTCODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "CTM_ORDER"
        dt.Columns.Add(column)

        Return dt
    End Function
    Sub GridBind()

        Try
            Dim dt As DataTable = SetDataTable()
            Dim bShowSlno As Boolean = False

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String

            If ddlCodesTestedCategory.SelectedItem.Text = "--" Then
                str_query = "SELECT CTM_ID,CTM_DESCR,ISNULL(CTM_ORDER,0) CTM_ORDER,ISNULL(CTM_SHORTCODE,'') AS CTM_SHORTCODE FROM " _
                                        & " CODESTESTED_MASTER WHERE " _
                                        & " CTM_SBG_ID= " + ddlSubject.SelectedValue.ToString _
                                        & " ORDER BY CTM_ORDER"
            Else
                str_query = "SELECT CTM_ID,CTM_DESCR,ISNULL(CTM_ORDER,0) CTM_ORDER,ISNULL(CTM_SHORTCODE,'') AS CTM_SHORTCODE FROM " _
                                        & " CODESTESTED_MASTER WHERE " _
                                        & " CTM_SBG_ID= " + ddlSubject.SelectedValue.ToString _
                                        & " AND CTM_CTC_ID=" + ddlCodesTestedCategory.SelectedItem.Value _
                                        & " ORDER BY CTM_ORDER"
            End If
            

            Dim i As Integer
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim dr As DataRow
            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    dr = dt.NewRow
                    dr.Item(0) = .Item(0)
                    dr.Item(1) = .Item(1)
                    dr.Item(2) = .Item(3)
                    dr.Item(3) = "edit"
                    dr.Item(4) = i.ToString
                    dr.Item(5) = .Item(2)

                    dt.Rows.Add(dr)
                End With
            Next

            If ds.Tables(0).Rows.Count > 0 Then
                bShowSlno = True
            End If

            'add empty rows to show 50 rows
            For i = 0 To 50 - ds.Tables(0).Rows.Count
                dr = dt.NewRow
                dr.Item(0) = "0"
                dr.Item(1) = ""
                dr.Item(2) = ""
                dr.Item(3) = "add"
                dr.Item(4) = (ds.Tables(0).Rows.Count + i).ToString
                dr.Item(5) = ""
                dt.Rows.Add(dr)
            Next

            Session("dtCat") = dt
            gvCategory.DataSource = dt
            gvCategory.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Sub BindCodesTestedCategory()

        ddlCodesTestedCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CTC_DESCR,CTC_ID FROM CODESTESTED_CATEGORY" _
                               & " WHERE CTC_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND CTC_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " AND CTC_SBG_ID=" + ddlSubject.SelectedItem.Value + "ORDER BY CTC_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCodesTestedCategory.DataSource = ds
                ddlCodesTestedCategory.DataTextField = "CTC_DESCR"
                ddlCodesTestedCategory.DataValueField = "CTC_ID"
                ddlCodesTestedCategory.DataBind()
            Else
                ddlCodesTestedCategory.Items.Insert(0, New ListItem("--", "0"))
            End If
        Else
            ddlCodesTestedCategory.Items.Insert(0, New ListItem("--", "0"))
        End If



    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblCatId As Label
        Dim txtCategory As TextBox
        Dim txtShortname As TextBox
        Dim txtOrder As TextBox
        Dim lblDelete As Label
        Dim mode As String = ""

        For i = 0 To gvCategory.Rows.Count - 1
            With gvCategory.Rows(i)
                lblCatId = .FindControl("lblCatId")
                txtCategory = .FindControl("txtCategory")
                txtOrder = .FindControl("txtOrder")
                lblDelete = .FindControl("lblDelete")
                txtShortname = .FindControl("txtShortname")

                If lblDelete.Text = "1" Then
                    mode = "Delete"
                ElseIf lblCatId.Text = "0" And txtCategory.Text <> "" Then
                    mode = "Add"
                ElseIf txtCategory.Text <> "" Then
                    mode = "Edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then
                    str_query = "exec saveCODESTESTED " _
                   & " @CTM_ID=" + lblCatId.Text + "," _
                   & " @CTM_CTC_ID=" + ddlCodesTestedCategory.SelectedItem.Value + "," _
                   & " @CTM_SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                   & " @CTM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                   & " @CTM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                   & " @CTM_DESCR=N'" + txtCategory.Text + "'," _
                   & " @MODE='" + mode + "'," _
                   & " @CTM_ORDER='" + Val(txtOrder.Text).ToString + "', " _
                   & " @CTM_SHORTCODE='" + txtShortname.Text + "'"


                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvCategory.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#End Region

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindCodesTestedCategory()
        GridBind()
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubject()
        BindCodesTestedCategory()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        BindCodesTestedCategory()
        GridBind()
    End Sub

    Protected Sub ddlCodesTestedCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCodesTestedCategory.SelectedIndexChanged
        GridBind()
    End Sub
End Class
