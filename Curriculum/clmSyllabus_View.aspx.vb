

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM

Partial Class Curriculum_clmSyllabus_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")
        'smScriptManager.EnablePartialRendering = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100025" And ViewState("MainMnu_code") <> "C300165") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    FillAcd()
                    FillTerm()
                    FillGrade()
                    gridbind()
                    gvclmSyllabus.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvclmSyllabus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvclmSyllabus.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvclmSyllabus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvclmSyllabus.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnCAM_DESC_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub
    Public Sub gridbind()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_CAM_DESC As String = String.Empty

            Dim ds As New DataSet
            Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")


            str_Sql = "SELECT     VW_ACADEMICYEAR_M.ACY_DESCR, VW_BUSINESSUNIT_M.BSU_NAME, VW_TRM_M.TRM_DESCRIPTION, SUBJECTS_GRADE_S.SBG_DESCR, " _
                        & " SYL.SYLLABUS_M.SYM_DESCR, SYL.SYLLABUS_M.SYM_ID,SBG_ID " _
                        & " FROM SYL.SYLLABUS_M INNER JOIN " _
                        & " VW_BUSINESSUNIT_M ON SYL.SYLLABUS_M.SYM_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                        & " vw_ACADEMICYEAR_D ON SYL.SYLLABUS_M.SYM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " _
                        & " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID INNER JOIN " _
                        & " VW_TRM_M ON SYL.SYLLABUS_M.SYM_TRM_ID = VW_TRM_M.TRM_ID INNER JOIN " _
                        & " SUBJECTS_GRADE_S ON SYL.SYLLABUS_M.SYM_SBG_ID = SUBJECTS_GRADE_S.SBG_ID AND " _
                        & " SYL.SYLLABUS_M.SYM_GRD_ID = SUBJECTS_GRADE_S.SBG_GRD_ID " _
                    & " WHERE SYLLABUS_M.SYM_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' and " _
                    & " SYLLABUS_M.SYM_TRM_ID='" & ddlTerm.SelectedItem.Value & "' and SYLLABUS_M.SYM_GRD_ID='" & grade(0) & "' and SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "'"


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim txtSearch2 As New TextBox
            Dim str_search As String
            Dim str_CAM_DESC As String = String.Empty

            If ddlSubject.SelectedValue <> "0" Then
                str_Sql += " AND SYLLABUS_M.SYM_SBG_ID=" + ddlSubject.SelectedValue.ToString
            End If
            If gvclmSyllabus.Rows.Count > 0 Then

                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch2 = gvclmSyllabus.HeaderRow.FindControl("txtSubject")
                txtSearch = gvclmSyllabus.HeaderRow.FindControl("txtSyllabus")
                str_CAM_DESC = txtSearch.Text
                If txtSearch.Text <> "" Then
                    If str_search = "LI" Then
                        str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "NLI" Then
                        str_filter_CAM_DESC = "  AND  NOT SYL.SYLLABUS_M.SYM_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "SW" Then
                        str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR NOT LIKE '" & txtSearch.Text & "%'"

                    ElseIf str_search = "EW" Then
                        str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR LIKE  '%" & txtSearch.Text & "'"

                    ElseIf str_search = "NEW" Then
                        str_filter_CAM_DESC = " AND SYL.SYLLABUS_M.SYM_DESCR NOT LIKE '%" & txtSearch.Text & "'"

                    End If
                End If

                If txtSearch2.Text <> "" Then
                    If str_search = "LI" Then

                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    ElseIf str_search = "NLI" Then

                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    ElseIf str_search = "NSW" Then

                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    ElseIf str_search = "EW" Then

                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    ElseIf str_search = "NEW" Then

                        str_filter_CAM_DESC = " AND SUBJECTS_GRADE_S.SBG_DESCR LIKE '%" & txtSearch2.Text & "%'"
                    End If
                End If

            End If

            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAM_DESC & " ORDER BY CAM_DESC")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAM_DESC & " ORDER BY SYL.SYLLABUS_M.SYM_ID")
            If ds.Tables(0).Rows.Count > 0 Then

                gvclmSyllabus.DataSource = ds.Tables(0)
                gvclmSyllabus.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvclmSyllabus.DataSource = ds.Tables(0)
                Try
                    gvclmSyllabus.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvclmSyllabus.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvclmSyllabus.Rows(0).Cells.Clear()
                gvclmSyllabus.Rows(0).Cells.Add(New TableCell)
                gvclmSyllabus.Rows(0).Cells(0).ColumnSpan = columnCount
                gvclmSyllabus.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvclmSyllabus.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


            'txtSearch = gvclmSyllabus.HeaderRow.FindControl("txtCAM_DESC")
            'txtSearch.Text = str_CAM_DESC


            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

#End Region

    Protected Sub gvclmSyllabus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvclmSyllabus.PageIndexChanging
        Try
            gvclmSyllabus.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_M_AddEdit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvclmSyllabus_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvclmSyllabus.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblCAM_ID As Label
                With selectedRow
                    lblCAM_ID = .FindControl("lblCAM_ID")

                End With

                Dim url As String
                url = String.Format("~\Curriculum\clmActivity_M.aspx?MainMnu_code={0}&datamode={1}&viewid=", ViewState("MainMnu_code"), ViewState("datamode"), Encr_decrData.Encrypt(lblCAM_ID.Text))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub FillAcd()

        ddlAcademicYear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub
    Sub FillTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub
    Sub FillGrade()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + acdid
        str_query += " order by grd_displayorder"
        

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        Return ddlSubject
    End Function
    Protected Sub btnSubject_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub btnSyllabus_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillTerm()
        FillGrade()
        gridbind()
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillGrade()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblSYL_ID As New Label
            Dim url As String

            lblSYL_ID = TryCast(sender.FindControl("lblSBMId"), Label)

            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_M_AddEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Encr_decrData.Encrypt(lblSYL_ID.Text))
            'url = String.Format("~\Curriculum\clmSyllabus_M_AddEdit.aspx")
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub lblTopics_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblSYL_ID As New Label
            lblSYL_ID = TryCast(sender.FindControl("lblSbmId"), Label)
            Dim lblSbgId As Label = TryCast(sender.FindControl("lblSbgId"), Label)

            Dim url As String

            'lblCAM_ID = TryCast(sender.FindControl("lblCAM_ID"), Label)
                 'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_D_AddEdit.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") _
             & "&AccId=" & Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
             & "&TermId=" & Encr_decrData.Encrypt(ddlTerm.SelectedValue.ToString) _
             & "&SubjID=" & Encr_decrData.Encrypt(lblSbgId.Text) & "&SyllabusId=" & Encr_decrData.Encrypt(lblSYL_ID.Text))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
    End Sub
End Class

