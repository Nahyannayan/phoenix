Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReleaseGroupAOL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300095") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    BindAOL()
                    GridBind()
                    txtRelease.Text = Format(Now.Date, "dd/MMM/yyyy")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private Methods"
    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""


        Dim grade As String() = Grdid.Split("|")

        strCondition = " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID='" + grade(1) + "'"

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()

        'Dim li As New ListItem
        'li.Text = "--"
        'li.Value = "0"
        'ddl.Items.Insert(0, li)
        Return ddl
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " order by grd_displayorder"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        'Dim li As New ListItem
        'li.Text = "ALL"
        'li.Value = ""
        'ddlGrade.Items.Insert(0, li)

    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String
        Dim strCondition As String

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        strCondition += " AND SGR_GRD_ID='" & grade(0) & "' AND SGR_STM_ID=" + grade(1)

        strCondition += " AND SGR_SBG_ID='" & ddlSubject.SelectedValue.ToString & "'"

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT SGR_ID, SGR_DESCR FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID,SGR_DESCR FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'"

            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        gvGrade.DataSource = ds
        gvGrade.DataBind()
    End Sub

    Sub BindAOL()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CAD_ID,CAD_DESC FROM ACT.ACTIVITY_D WHERE CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & "  AND CAD_bAOL='true'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAOL.DataSource = ds
        ddlAOL.DataTextField = "CAD_DESC"
        ddlAOL.DataValueField = "CAD_ID"
        ddlAOL.DataBind()
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim str_query As String
        Dim chkRelease As CheckBox
        Dim lblSgrId As Label
        Dim txtDate As TextBox

        Dim transaction As SqlTransaction

        For i = 0 To gvGrade.Rows.Count - 1
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                    lblSgrId = gvGrade.Rows(i).FindControl("lblSgrId")
                 
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")


                    If chkRelease.Checked = True Then
                        str_query = "exec saveRELEASEAOL_GROUP " _
                       & lblSgrId.Text + "," _
                       & ddlAOL.SelectedValue.ToString + "," _
                       & IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'")
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"
                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using

        Next


    End Sub


    Sub CheckUncheck(ByVal gRow As GridViewRow)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim lblSgrId As Label
        lblSgrId = gRow.FindControl("lblSgrId")
        chkRelease = gRow.FindControl("chkRelease")
        Dim count As Integer
        Dim count1 As Integer
        str_query = "SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
                  & " WHERE ISNULL(STA_bRELEASEONLINE,0)=0 AND STA_CAD_ID='" + ddlAOL.SelectedValue.ToString + "'" _
                  & " AND STA_SGR_ID=" + lblSgrId.Text

        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            str_query = "SELECT COUNT(STA_ID) FROM ACT.STUDENT_ACTIVITY AS A " _
              & " WHERE ISNULL(STA_bRELEASEONLINE,0)=1 AND STA_CAD_ID='" + ddlAOL.SelectedValue.ToString + "'" _
              & " AND STA_SGR_ID=" + lblSgrId.Text
            count1 = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If count1 <> 0 Then
                chkRelease.Checked = True
            End If
        Else
            chkRelease.Checked = False
        End If

    End Sub

#End Region


    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        BindAOL()
        GridBind()
    End Sub


 

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        GridBind()
    End Sub

    Protected Sub gvGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrade.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGrade.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

                Dim lblSgrId As Label
                Dim lblGroup As Label

                With selectedRow
                    lblSgrId = .FindControl("lblSgrId")
                    lblGroup = .FindControl("lblGroup")
                End With

                Dim url As String
                url = String.Format("~\Curriculum\clmReleaseStudentAOL.aspx?" _
                                    & "&sgrid=" + Encr_decrData.Encrypt(lblSgrId.Text) _
                                   & "&cadid=" + Encr_decrData.Encrypt(ddlAOL.SelectedValue.ToString) _
                                   & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                                    & "&cad=" + Encr_decrData.Encrypt(ddlAOL.SelectedItem.Text) _
                                   & "&MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGrade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGrade.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            CheckUncheck(e.Row)
        End If
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim txtDate As TextBox

            Dim chkRelease As CheckBox
            For i = 0 To gvGrade.Rows.Count - 1

                chkRelease = gvGrade.Rows(i).FindControl("chkRelease")
                If chkRelease.Checked = True Then
                    txtDate = gvGrade.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtRelease.Text
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub ddlAOL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAOL.SelectedIndexChanged
        GridBind()
    End Sub
End Class
