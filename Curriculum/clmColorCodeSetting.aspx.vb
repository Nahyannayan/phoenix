Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Telerik.Web.UI
Imports System.Collections.Generic

Partial Class Skills_Setting_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "C100017" Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("SelectedRow") = -1

                If ViewState("datamode") = "add" Then
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                Else
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                End If
                'BindReport()
                BindGrade()
                'GridBind()
                'End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReport(ByVal grades As String)
        ddlExam.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        str_query = "SELECT DISTINCT RPF_DESCR AS RPF_DESCR,RPF_ID FROM RPT.REPORT_SETUP_M AS A" _
                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID" _
                 & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND RPF_DESCR NOT LIKE '%REVIEW REPORT%' AND RPF_DESCR NOT LIKE '%FEEDBACK REPORT%' " _
                 & " AND RSG_GRD_ID NOT IN ('KG1','KG2') " _
                 & " AND RSG_GRD_ID IN (" + grades + ")" _
                 & " ORDER BY RPF_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlExam.DataSource = ds
        ddlExam.DataTextField = "RPF_DESCR"
        ddlExam.DataValueField = "RPF_ID"
        ddlExam.DataBind()
        'ddlExam.Items.Insert(0, New ListItem("Select ", "0"))

    End Sub


    Private Sub GridBind()
        Dim Exam As String = "'" + GetSelectedExam().Replace("|", "','") + "'"
        Dim grades As String = "'" + GetSelectedGrades().Replace("|", "','") + "'"
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT A.*,B.*,C.RPF_DESCR FROM RPT.COLOR_CODE_D A  " & _
                                    "INNER JOIN RPT.COLOR_CODE_M B ON " & _
                                     "CCD_CCM_ID = CCM_ID " & _
                                     "INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON CCM_RPF_ID=C.RPF_ID " & _
                                    "WHERE CCM_ACD_ID = " + ddlAcademicYear.SelectedItem.Value + " And " & _
                                    "CCM_RPF_ID	IN  (" + Exam + ") AND " & _
                                    "CCM_GRD_ID IN  (" + grades + ")  "

        '  str_query += " ORDER BY skl_id desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = ds.Tables(0)

        gvcolor.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            gvcolor.Visible = False
        Else
            gvcolor.DataBind()
            gvcolor.Visible = True
        End If
    End Sub

    Protected Sub btnNewRow_Click(ByVal sender As Object, ByVal e As EventArgs)
        If (btnNewRow.Text <> "Save") Then
            trNewRow1.Visible = True
            trNewRow2.Visible = True
            trNewRow3.Visible = True
            btnNewRow.Text = "Save"
        Else
            'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            'Dim str_query As String = "SELECT A.*,B.*,C.RPF_DESCR FROM RPT.COLOR_CODE_D A  " & _
            '                       "INNER JOIN RPT.COLOR_CODE_M B ON " & _
            '                        "CCD_CCM_ID = CCM_ID " & _
            '                        "INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON CCM_RPF_ID=C.RPF_ID " & _
            '                       "WHERE CCM_ACD_ID = " + ddlAcademicYear.SelectedItem.Value + " And " & _
            '                       "CCM_RPF_ID	IN  (" + Exam + ") "

            ''  str_query += " ORDER BY skl_id desc"
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            'Dim dt As DataTable = ds.Tables(0)
            'Dim NewRow As DataRow = dt.NewRow()
            'NewRow(2) = txtNewMarkFrom.Text
            'NewRow(3) = txtNewMarkTo.Text
            'NewRow(4) = txtNewColorCode.Text
            'dt.Rows.Add(NewRow)
            'gvcolor.DataSource = dt
            'gvcolor.DataBind()
            SaveRange()
            GridBind()
            ClearAll()
        End If
    End Sub
    Protected Sub ClearAll()
        txtNewMarkFrom.Text = ""
        txtNewMarkTo.Text = ""
        'txtNewColorCode.Text = ""
        btnNewRow.Text = "Add New Row"
        trNewRow1.Visible = False
        trNewRow2.Visible = False
        trNewRow3.Visible = False
    End Sub


    Protected Sub ddlExam_SelectedIndexChanged(sender As Object, e As EventArgs)
        GridBind()
    End Sub
    Protected Sub lstGrade_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        GridBind()

    End Sub
    Protected Sub SaveRange()
        Dim strconn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = String.Empty
        Dim Examcollection As IList(Of RadComboBoxItem) = ddlExam.CheckedItems
        Dim Gradecollection As IList(Of RadComboBoxItem) = lstGrade.CheckedItems
        If (Examcollection.Count <> 0) Then
            For Each examitem As RadComboBoxItem In Examcollection
                If (Gradecollection.Count <> 0) Then
                    For Each gradeitem As RadComboBoxItem In Gradecollection
                        str_query = "EXEC [RPT].[SaveColorCode] " & _
                             "'" + ddlAcademicYear.SelectedItem.Value + "'," & _
                             "'" + examitem.Value + "'," & _
                             "'" + gradeitem.Value + "'," & _
                             "'" + txtMaxMark.Text + "'," & _
                             "'" + txtNewMarkFrom.Text + "'," & _
                             "'" + txtNewMarkTo.Text + "'," & _
                             "'" + System.Drawing.ColorTranslator.ToHtml(RadColorPicker1.SelectedColor) + "'"
                        SqlHelper.ExecuteNonQuery(strconn, CommandType.Text, str_query)
                    Next
                End If
            Next
        End If


    End Sub

    Function GetSelectedExam() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlExam.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function

    Function GetSelectedGrades() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstGrade.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M  " _
                                  & " INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID WHERE" _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Protected Sub lstGrade_SelectedIndexChanged1(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles lstGrade.SelectedIndexChanged
        Dim grades As String = "'" + GetGrades().Replace("|", "','") + "'"
        BindReport(grades)
    End Sub
    Function GetGrades() As String
        Dim str As String = ""
        Dim i As Integer
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrade.Items(i).Value
            End If
        Next
        Return str
    End Function
End Class