<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportProcess_Attendance.aspx.vb" Inherits="Curriculum_clmReportProcess_Attendance" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Processing Report"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><span style="color: #c00000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    
                                </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td   valign="bottom">
                            <table align="center" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Term</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Report card</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>

                                    <td align="left"><span class="field-label">Report Schedule</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                </tr>
                               
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>

                                    <td align="left"><span class="field-label">Select Section</span></td>
                                   
                                    <td align="left">
                                        <asp:CheckBoxList ID="lstSection" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                        <asp:Label ID="lblNote" runat="server" ></asp:Label></td>
                                </tr>
                               
                            </table>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td  valign="bottom" align="center">

                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td   valign="bottom">
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>

