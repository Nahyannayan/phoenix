﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmLessonStudentCriterias_Graph.aspx.vb"
    Inherits="Curriculum_clmLessonStudentCriterias_Graph" MaintainScrollPositionOnPostback="true" Title="Lesson Criteria" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>Untitled Page</title>
    <style type="text/css">
        .field-label {
            font-weight: 600 !important;
            font-size: 16px !important;
        }

        table td span.field-value {
            padding: 9px !important;
            min-height: 34px !important;
        }
    </style>
    <style type="text/css">
        .img-thumbnail {
            border-radius: 50% !important;
            box-shadow: 1px 2px 10px rgba(0,0,0,0.12) !important;
        }

        .popup-name {
            font-size: 24px;
            color: #333;
            line-height: 60px;
        }

        .popup-grade {
            font-size: 20px;
            color: #656565;
        }
    </style>
   
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>


                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row mt-2 border-bottom">
                            <div class="col-md-2 col-sm-2 text-center">
                                <asp:Image ID="imgStud" runat="server" class="img-thumbnail" ImageUrl="~/Images/Photos/no_image.gif" Width="100px" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <asp:Label ID="lblName" CssClass="popup-name" runat="server"></asp:Label>
                                <br />
                                <span class="popup-grade">Grade </span>
                                <asp:Label ID="lblGrade" CssClass="popup-grade" runat="server"></asp:Label>
                                <br />
                                <asp:Label ID="lblObjective" runat="server" CssClass="popup-grade"></asp:Label>
                            </div>
                            <div class="col-md-4 col-sm-4 text-right">
                                  <asp:Image ID="img_graph" runat="server" ImageUrl="~/Images/progress-bar.png" style="padding: 29px 47px; width:150px"   />
                            </div>
                        </div>

                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Style="text-align: left"></asp:Label>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive m-auto">
                            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" TabIndex="7" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged" >
                                <asp:TabPanel ID="TabPanel1" runat="server"  >
                                    <HeaderTemplate>Attainment</HeaderTemplate>
                                    <ContentTemplate>
                                        <iframe id="ifrm_Attainment" runat="server" width="100%" height="600px" ></iframe>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel ID="TabPanel2" runat="server" >
                                    <HeaderTemplate>Progress</HeaderTemplate>
                                    <ContentTemplate>
                                        <iframe id="ifrm_Progress" runat="server" width="100%" height="600px" ></iframe>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel ID="TabPanel3" runat="server" >
                                    <HeaderTemplate>Detailed View</HeaderTemplate>
                                    <ContentTemplate>
                                        <iframe id="ifrm_DetailedR" runat="server" width="100%" height="600px" ></iframe>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="hSTU_ID" runat="server"  />
                <asp:HiddenField ID="hACD_ID" runat="server" />
                <asp:HiddenField ID="hSBM_ID" runat="server" />
                <asp:HiddenField ID="hSGR_ID" runat="server" />
                <asp:HiddenField ID="hSBG_ID" runat="server" />
                <asp:HiddenField ID="hGRD_ID" runat="server" />
                <asp:HiddenField ID="hTopic_ID" runat="server" />
                <asp:HiddenField ID="hObj_ID" runat="server" />
                <asp:HiddenField ID="hTSM_ID" runat="server" />
                <asp:HiddenField ID="h_TAB_INDEX" runat="server"  />
                <asp:HiddenField ID="hStuPhotoPath" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
<%-- <script>
     function tab_click(tab_value) {
         document.getElementById('<%=h_TAB_INDEX.ClientID%>').value = tab_value;
            __doPostBack('<%= h_TAB_INDEX.ClientID%>', '');
        }
    </script>--%>
    </form>
</body>
</html>
