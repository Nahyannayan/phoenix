﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Data
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Net
Imports System.IO
Imports Telerik.Web.UI

Partial Class HifzTracker_ViewStudentHifzTracker
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not Page.IsPostBack Then
            Dim transactionId = Convert.ToString(Request.QueryString("transactionId"))

            hdnHifzTransactionId.Value = transactionId
            divSuccess.Visible = False
            divError.Visible = False

        End If
    End Sub
    Protected Sub tbl_StudentHifzTracker_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        BindStudentHifzTrackerGrid()
    End Sub
    Private Sub BindStudentHifzTrackerGrid()
        Try
            Dim dt As DataTable = GetStudentHifzTrackerData()
            Dim isCertificateEnabled As Boolean = Convert.ToBoolean(dt.Rows(0)("IsCertificateEnabled"))
            tbl_StudentHifzTracker.DataSource = dt
            DisabledCertificateEnablingOption(isCertificateEnabled)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub DisabledCertificateEnablingOption(ByVal isCertificateEnabled As Boolean)
        btnEnableCerificate.Enabled = Not isCertificateEnabled
        If isCertificateEnabled Then
            btnEnableCerificate.CssClass = "button pull-right field-disabled"
            btnEnableCerificate.Text = "Certificate Enabled"
        End If
    End Sub
    Private Function GetStudentHifzTrackerData() As DataTable
        Dim transactionId = Convert.ToString(hdnHifzTransactionId.Value)
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@TransactionId", transactionId)
        Dim ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetHifzTrackerDataPerStudent", parameters)
        Return ds.Tables(0)
    End Function
    Protected Sub btnSave_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim btnSave As ImageButton = (TryCast(sender, ImageButton))
            Dim uploadedFileName = btnSave.CommandArgument
            Dim folderPath As String = Convert.ToString(ConfigurationManager.AppSettings("HifzTrackerAudioFolderLocalPath"))
            Dim fullFilePath As String = Path.Combine(folderPath, uploadedFileName)

            Dim folderVirtualPath As String = Convert.ToString(ConfigurationManager.AppSettings("HifzTrackerAudioFolderVirtualPath"))
            Dim fullFileVirtualPath As String = Path.Combine(folderPath, uploadedFileName)

            If File.Exists(fullFilePath) Then
                Response.ContentType = "audio/mpeg3;audio/x-mpeg-3;video/mpeg;video/x-mpeg;text/xml"
                Response.AppendHeader("Content-Disposition", "attachment; filename=" & uploadedFileName)
                Response.TransmitFile(fullFileVirtualPath)
                'Response.[End]()
                Response.Flush()
            End If
        Catch ex As Exception
            ShowNotificationMessage(False)
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs)
        Try
            Dim relationIds = New List(Of String)()
            For Each item As GridDataItem In tbl_StudentHifzTracker.MasterTableView.Items()
                Dim isChecked As Boolean = (TryCast(item.FindControl("chkVerse"), CheckBox)).Checked
                If isChecked Then
                    Dim relationId As String = item.GetDataKeyValue("HifzRelationId").ToString()
                    relationIds.Add(relationId)
                End If
            Next

            If relationIds.Any() Then
                ShowNotificationMessage(UpdateReviewStatusForHifzTracker(String.Join(",", relationIds)))
                BindStudentHifzTrackerGrid()
                tbl_StudentHifzTracker.DataBind()
            End If
        Catch ex As Exception
            ShowNotificationMessage(False)
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub ShowNotificationMessage(ByVal isSuccess As Boolean)
        divSuccess.Visible = isSuccess
        divError.Visible = Not isSuccess
    End Sub
    Private Function UpdateReviewStatusForHifzTracker(ByVal relationIds As String) As Boolean
        Dim userId = Convert.ToString(HttpContext.Current.Session("EmployeeId"))
        Dim userName = Convert.ToString(HttpContext.Current.Session("sUsr_Display_Name"))
        Dim parameters As SqlParameter() = New SqlParameter(9) {}
        parameters(0) = New SqlParameter("@UserId", userId)
        parameters(1) = New SqlParameter("@UserName", userName)
        parameters(2) = New SqlParameter("@HifzRelationIds", relationIds)
        parameters(3) = New SqlParameter("@output", SqlDbType.Int)
        parameters(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.UpdateReviewStatusForHifzTracker", parameters)
        Return CInt(parameters(3).Value) > 0
    End Function

    Protected Sub btnEnableCerificate_Click(sender As Object, e As EventArgs)
        Try
            Dim transactionId As Integer = hdnHifzTransactionId.Value
            Dim isEnabled As Boolean = EnableHifzTrackerCertificateStatus(transactionId)
            ShowNotificationMessage(isEnabled)
            DisabledCertificateEnablingOption(isEnabled)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function EnableHifzTrackerCertificateStatus(ByVal transactionId As Integer) As Boolean
        Dim parameters As SqlParameter() = New SqlParameter(9) {}
        parameters(0) = New SqlParameter("@TransactionId", transactionId)
        parameters(1) = New SqlParameter("@output", SqlDbType.Int)
        parameters(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.EnableHifzTrackerCertificateStatus", parameters)
        Return CInt(parameters(1).Value) > 0
    End Function
End Class
