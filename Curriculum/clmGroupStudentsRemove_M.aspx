﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGroupStudentsRemove_M.aspx.vb" Inherits="Curriculum_clmGroupStudentsRemove_M" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script>
  function change_chk_state(chkThis)
     {
    var chk_state= ! chkThis.checked ;
     for(i=0; i<document.forms[0].elements.length; i++)
           {
           var currentid =document.forms[0].elements[i].id; 
           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
         {
           //if (document.forms[0].elements[i].type=='checkbox' )
              //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked=chk_state;
                 document.forms[0].elements[i].click();//fire the click event of the child element
             }
          }
      }     
           
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Remove Students From Group
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
         <td align="left"  valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"  ></asp:Label></td>
        </tr>
        <tr><td>
 <table id="tbgroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                   
                    <tr>
                     <td align="left" width="20%"> <span  class="field-label">Academic Year</span></td>
                    
                     <td align="left" ><asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>
                        
                           <td align="left"> <span  class="field-label">Grade</span></td>
                
                     <td align="left" ><asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>
                        
                        
                    </tr>
                    <tr>
                     <td align="left"> <span  class="field-label">Subject</span></td>
            
                     <td align="left" ><asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="true" >
                        </asp:DropDownList ></td>
                        
                             <td align="left"> <span  class="field-label">Group</span></td>
                
                     <td align="left" ><asp:DropDownList ID="ddlGroup" runat="server">
                        </asp:DropDownList ></td>
                        
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:RadioButton id="rdAll" runat="server" Checked="True" GroupName="g1" class="field-label" Text="All">
                            </asp:RadioButton>
                            <asp:RadioButton id="rdMuslim" runat="server" GroupName="g1" class="field-label" Text="Muslim">
                            </asp:RadioButton>
                            <asp:RadioButton id="rdNonMuslim" runat="server" GroupName="g1" class="field-label" Text="Non Muslim">
                            </asp:RadioButton></td>
                        <td align="center" >
    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="List"  ValidationGroup="groupM1" 
                            TabIndex="7" />
                        </td></tr>
   
                    <tr><td colspan="4" >&nbsp;</td></tr>
   
                    <tr><td colspan="4" align="center">
    <asp:GridView ID="gvGroup" runat="server" AllowPaging="false" AutoGenerateColumns="False"
         EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
        PageSize="20" Width="100%"  class="table table-bordered table-row">
        <RowStyle  Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <Columns>
        
             <asp:TemplateField HeaderText="NC Level" Visible="false" >
                <ItemTemplate>
                    <asp:Label ID="lblStuId" runat="server"  Text='<%# Bind("STU_ID") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
             
            
            
                      <asp:TemplateField HeaderText="Select">
<EditItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" />
</EditItemTemplate>

<ItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
</ItemTemplate>
<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>         
                    
                     <asp:TemplateField HeaderText="Student ID"  >
                <ItemTemplate>
                    <asp:Label ID="lblStuNO" runat="server"  Text='<%# Bind("STU_NO") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Student Name" >
                <ItemTemplate>
                    <asp:Label ID="lblStudent" runat="server"  Text='<%# Bind("STU_NAME") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
                    
        </Columns>
      
        <HeaderStyle Wrap="False" />
        <EditRowStyle Wrap="False" />
        <AlternatingRowStyle  Wrap="False" />
    </asp:GridView>
    </td></tr>
    
    <tr>
    <td colspan="4" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"  ValidationGroup="groupM1" TabIndex="7" />
    </td></tr>
    </table>
    </td></tr></table>

            </div>
        </div>
    </div>

</asp:Content>

