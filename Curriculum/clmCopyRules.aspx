﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmCopyRules.aspx.vb" Inherits="Curriculum_clmCopyRules" Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>

        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
    <script language="javascript" type="text/javascript">
function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
  curr_elem = document.forms[0].elements[i];
  if(curr_elem.type == 'checkbox')
  {
  curr_elem.checked = !master_box.checked;
  }
 }
 master_box.checked=!master_box.checked;
}



    </script>

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Copy Rules
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        
        <tr>
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="matters" valign="top">
                <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" >
                    
                    <tr>
                        <td valign="top">
                            <table border="0" cellpadding="0" width="100%" >
                                <tr >
                                    <td align="left" colspan="2" valign="middle" class="title-bg">                                        
                                         <strong>   Copy From </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="40%" >
                                        <asp:Label ID="lblStu" runat="server" CssClass="field-label" Text="Academic Year" ></asp:Label>
                                    </td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcademicYearFrom" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Term</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlTermFrom" runat="server" AppendDataBoundItems="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Report Card" ></asp:Label>
                                    </td>
                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlReportCardFrom" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Report Schedule" ></asp:Label>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlReportScheduleFrom" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table border="0" cellpadding="0" width="100%" >
                                <tr >
                                    <td align="left" colspan="2" valign="middle" class="title-bg">
                                        <strong>Copy To</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  width="40%" >
                                        <asp:Label ID="Label4" runat="server" CssClass="field-label" Text="Academic Year" ></asp:Label>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcademicYearTo" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Term</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlTermTo" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label5" runat="server" CssClass="field-label" Text="Report Card" ></asp:Label>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlReportCardTo" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label6" runat="server" CssClass="field-label" Text="Report Schedule" ></asp:Label>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlReportScheduleTo" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <table border="0" cellpadding="0" width="100%" width="100%">
                                <tr>
                                    <td align="left" width="40%">
                                        <asp:Label ID="Label3" runat="server" CssClass="field-label" Text="Grade" ></asp:Label>
                                    </td>
                                   
                                    <td align="left"  width="60%">
                                       <%-- <table width="100%">
                                            <tr>
                                                <td class="matters">
                                                    <asp:CheckBox ID="chkAll" onclick="javascript:fnSelectAll(this);" runat="server" /> Select All
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="matters">
                                                    <asp:CheckBoxList ID="lstGrade" runat="server"  Width="100%" AutoPostBack="True"
                                                        BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Flow" Style="overflow: auto">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>--%>

                                         <telerik:RadComboBox RenderMode="Lightweight" ID="ddlPrintedFor" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                     OnSelectedIndexChanged="ddlPrintedFor_SelectedIndexChanged"  Width="100%" >
                </telerik:RadComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:GridView ID="gvAssessment" runat="server" Width="100%" Height="1px" EmptyDataText="No Records"
                                AutoGenerateColumns="false" PageSize="2" CssClass="table table-bordered table-row">
                                <RowStyle  CssClass="griditem"></RowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="sbgId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCadId" runat="server" Text='<%# Bind("CAD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="sbgId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCamId" runat="server" Text='<%# Bind("CAD_CAM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assessment">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssessment" runat="server" Text='<%# Bind("CAD_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Map to">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlAssessment" Width="180px" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle ></SelectedRowStyle>
                                <HeaderStyle ></HeaderStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <table border="0" cellpadding="0"  Width="100%" >
                                <%--<tr>
                                    <td class="matters">
                                        Rule Desctiption
                                    </td>
                                    <td class="matters">
                                        :
                                    </td>
                                    <td class="matters">
                                        Replace text
                                    </td>
                                    <td class="matters">
                                        <asp:TextBox ID="txtFrom" Width="60px" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="matters">
                                        with
                                    </td>
                                    <td class="matters">
                                        <asp:TextBox ID="txtTo" Width="60px" runat="server" CausesValidation="True"></asp:TextBox>
                                    </td>
                                </tr>--%>

                                <tr>
                                    <th colspan="2" class="title-bg text-left">
                                        <span >Rule Description</span>
                                    </th>
                                </tr>
                                <tr>
                                    <td align="left" width="40%">
                                        <span class="field-label">Replace text</span>
                                    </td>
                                   
                                    <td >
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <span class="field-label">with</span>
                                    </td>
                                   
                                    <td >
                                        <asp:TextBox ID="txtTo" runat="server" CausesValidation="True"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:GridView ID="gvHeader" runat="server" Width="100%"  EmptyDataText="No Records"
                                AutoGenerateColumns="false" PageSize="2" CssClass="table table-bordered table-row">
                                <RowStyle  CssClass="griditem"></RowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="sbgId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Header">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHeader" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Map to">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlHeader" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle ></SelectedRowStyle>
                                <HeaderStyle ></HeaderStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" >
                            <asp:Button ID="btnCopy" runat="server" CssClass="button" Text="Copy Rules" ValidationGroup="groupM1"
                                TabIndex="7"  />&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>
