﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCASEProcessMarks.aspx.vb" Inherits="Curriculum_clmCASEProcessMarks" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Process Marks"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" align="center"  cellpadding="0"  width="100%"
                    cellspacing="0"  >
                    <tr>
                        <td align="left" valign="bottom"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" style="border-collapse: collapse">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                   
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="true">
                                        </asp:DropDownList></td>
                                
                                    <td align="left" width="20%" ><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="true">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td  align="center" colspan="4">
                                        <asp:Button ID="btnProcess" runat="server" CssClass="button" Text="Process" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

