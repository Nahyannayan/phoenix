﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_BtecTracker_clmBtecTrackStudentObjectives
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Session("Popup") = "0"
            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C510015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                '  gvStudent.Attributes.Add("bordercolor", "#1b80b6")

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindSubjects()
                BindGroup()
                BindLevel()
                btnSave.Visible = False
                ViewState("Sort") = ""
                gvStudent.Attributes.Add("bordercolor", "#1b80b6")
                'Dim url As String
                'url = String.Format("{0}?MainMnu_code={1}", "~/Curriculum/clmObjTrackStudObjTest.aspx", Encr_decrData.Encrypt(ViewState("MainMnu_code")))
                'ResponseHelper.Redirect(url, "_blank", "")

                BindCategory()
                trGrid.Visible = False
                trSave.Visible = False
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        Else
            'reload data after savingthrough popup
            If Session("Popup") = "1" Then
                Session("Popup") = "0"
                btnView_Click(sender, e)
            End If
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            'If Session("sbsuid") = "125010" Then
            '    str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
            '          & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
            '          & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
            '          & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') "
            'Else
            str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                  & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                  & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                  & " AND GRD_ID IN('09','10','11','12','13') "
            'End If



            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            If Session("sbsuid") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                      & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                      & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                      & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                      & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                      & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                      & " AND SGS_TODATE IS NULL" _
                      & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') " _
                      & " ORDER BY GRD_DISPLAYORDER"
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                              & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                              & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                              & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                              & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                              & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                              & " AND SGS_TODATE IS NULL" _
                              & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') " _
                              & " ORDER BY GRD_DISPLAYORDER"
            End If

        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                       & " AND (SBG_DESCR LIKE 'BTEC%'" _
                       & " OR ISNULL(SBG_bBTEC,0)=1)"

        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                & " AND (SBG_DESCR LIKE 'BTEC%'" _
                & " OR ISNULL(SBG_bBTEC,0)=1)"

        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub


    Sub BindLevel()
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LVL_ID,LVL_DESCR FROM BTEC.BTEC_LEVEL_BSU_M WHERE LVL_BSU_ID='" + Session("SBSUID") + "'"
        If ddlSubject.SelectedIndex <> -1 Then
            If ddlSubject.SelectedItem.Text.ToUpper.Contains("ISLAMIC STUDIES") = False Then
                str_query += " AND ISNULL(LVL_bISL,'false')='FALSE'"
            End If

            str_query += " AND LVL_ID IN(SELECT OBJ_LVL_ID FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                        & " AND OBJ_SBM_ID=" + sbg_id(1) + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlLevel.DataSource = ds
            ddlLevel.DataTextField = "LVL_DESCR"
            ddlLevel.DataValueField = "LVL_ID"
            ddlLevel.DataBind()
        End If
      
    End Sub

    Sub BindCategory()
        ddlCategory.Items.Clear()
        If ddlSubject.SelectedIndex <> -1 Then
            Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT OBT_ID,OBT_DESCR FROM " _
                                    & " BTEC.BTEC_OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                                    & " AND OBT_SBM_ID=" + sbg_id(1) _
                                    & " ORDER BY OBT_ORDER,OBT_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "OBT_DESCR"
            ddlCategory.DataValueField = "OBT_ID"
            ddlCategory.DataBind()


            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlCategory.Items.Insert(0, li)
        End If
    End Sub

    Sub GridBind()
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If ddlCategory.SelectedValue = "0" Then
            str_query = "SELECT DISTINCT STU_ID,STU_FIRSTNAME,ISNULL(STU_LASTNAME,ISNULL(STU_MIDNAME,'')) AS STU_LASTNAME," _
                       & " ISNULL(SCL_CURRENTLEVEL,'') AS SCL_CURRENTLEVEL ,STU_NO, " _
                       & " OBJCOUNT=(SELECT COUNT(STO_OBJ_ID) FROM BTEC.BTEC_STUDENT_OBJECTIVES_S WHERE " _
                       & " STO_SBM_ID=" + sbg_id(1) + " AND STO_STU_ID=A.STU_ID AND STO_bCOMPLETED='Y' ) " _
                       & " FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                       & " LEFT OUTER JOIN BTEC.BTEC_STUDENT_CURRENTLEVEL_S AS C ON A.STU_ID=C.SCL_STU_ID " _
                       & " AND SCL_SBM_ID=" + sbg_id(1) _
                       & " WHERE STU_CURRSTATUS<>'CN' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        Else
            str_query = "SELECT DISTINCT STU_ID,STU_FIRSTNAME,ISNULL(STU_LASTNAME,ISNULL(STU_MIDNAME,'')) AS STU_LASTNAME," _
                      & " ISNULL(SCL_CURRENTLEVEL,'') AS SCL_CURRENTLEVEL ,STU_NO, " _
                      & " OBJCOUNT=(SELECT COUNT(STO_OBJ_ID) FROM BTEC.BTEC_STUDENT_OBJECTIVES_S INNER JOIN BTEC.BTEC_OBJECTIVES_M ON STO_OBJ_ID=OBJ_ID WHERE " _
                      & " STO_SBM_ID=" + sbg_id(1) + " AND STO_STU_ID=A.STU_ID AND STO_bCOMPLETED='Y' " _
                      & " AND OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString + " ) " _
                      & " FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                      & " LEFT OUTER JOIN BTEC.BTEC_STUDENT_CURRENTLEVEL_S AS C ON A.STU_ID=C.SCL_STU_ID " _
                      & " AND SCL_SBM_ID=" + sbg_id(1) _
                      & " WHERE STU_CURRSTATUS<>'CN' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        End If
        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO=" + txtStudentID.Text
        End If

        If txtStudentName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtStudentName.Text + "'%"
        End If



        str_query += " ORDER BY STU_FIRSTNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As DataView = ds.Tables(0).DefaultView

        If ViewState("SortFilter") = "First Name" Then
            dv.Sort = "STU_FIRSTNAME"
        End If

        If ViewState("SortFilter") = "Last Name" Then
            dv.Sort = "STU_LASTNAME"
        End If

        gvStudent.DataSource = dv
        gvStudent.DataBind()

        GetObjectives(sbg_id(1))
        GetData(sbg_id(1), sbg_id(0))
    End Sub


    Sub GetObjectives(ByVal sbm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBJ_ID,OBJ_DESCR,ISNULL(OBJ_bTARGET,0) OBJ_bTARGET FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_SBM_ID=" + sbm_id _
                                & " AND OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString

        If ddlCategory.SelectedValue.ToString <> "0" Then
            str_query += " AND OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString
        End If

        str_query += " ORDER BY OBJ_DISPLAYORDER,OBJ_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        Dim j, k As Integer

        Dim hdr, hdrId As String

        Dim lblHdr As Label
        Dim lblHdrId As Label



        If rdObj2.Checked = True Then
            If ds.Tables(0).Rows.Count > 25 Then
                j = 25
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj3.Checked = True Then
            If ds.Tables(0).Rows.Count > 50 Then
                j = 50
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj4.Checked = True Then
            If ds.Tables(0).Rows.Count > 75 Then
                j = 75
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        Else
            j = 0
            If ds.Tables(0).Rows.Count > 25 Then
                k = 25
            Else
                k = ds.Tables(0).Rows.Count
            End If
        End If

        Dim p As Integer = 1
        For i = j To k - 1
            hdr = "lblObj" + CStr(p) + "H"
            hdrId = "lblObjId" + CStr(p)

            lblHdr = gvStudent.HeaderRow.FindControl(hdr)
            lblHdrId = gvStudent.HeaderRow.FindControl(hdrId)



            lblHdr.Text = ds.Tables(0).Rows(i).Item(1)
            lblHdrId.Text = ds.Tables(0).Rows(i).Item(0)

            lblHdr.Attributes.Add("title", ds.Tables(0).Rows(i).Item(1))

            If ds.Tables(0).Rows(i).Item(2) = True Then
                lblHdr.ForeColor = Drawing.Color.Maroon
            End If
            p += 1
        Next


        'Dim lblFirstName As Label
        'Dim lblLastName As Label
        'lblFirstName = gvStudent.Rows(0).FindControl("lblFirstName")
        'lblLastName = gvStudent.Rows(0).FindControl("lblLastName")
        'Dim lblObj1H As Label = gvStudent.HeaderRow.FindControl("lblObj1H")

        'gvStudent.Rows(0).Attributes.Add("title", lblFirstName.Text + " " + lblLastName.Text + vbCrLf + lblObj1H.Text)

        'gvStudent.HeaderRow.Cells(0).CssClass = "locked"
        gvStudent.HeaderRow.Cells(1).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(2).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(3).CssClass = "FrozenCell"
        gvStudent.HeaderRow.Cells(4).CssClass = "FrozenCell"

        'For i = 0 To gvStudent.Rows.Count - 1
        '    gvStudent.Rows(i).Cells(0).CssClass = "locked"
        '    gvStudent.Rows(i).Cells(1).CssClass = "locked"
        '    gvStudent.Rows(i).Cells(2).CssClass = "locked"
        'Next



    End Sub


    Sub GetData(ByVal sbm_id As String, ByVal sbg_id As String)
        Dim objData As New Hashtable

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT STO_STU_ID,STO_OBJ_ID,STO_bCOMPLETED FROM OBJTRACK.STUDENT_OBJECTIVES_S" _
        '                        & " WHERE STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + " AND STO_SBM_ID=" + sbm_id

        Dim str_query As String = "SELECT STO_STU_ID,STO_OBJ_ID,STO_bCOMPLETED FROM BTEC.BTEC_STUDENT_OBJECTIVES_S" _
                               & " INNER JOIN STUDENT_GROUPS_S ON STO_STU_ID=SSD_STU_ID " _
                               & " WHERE STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + " AND STO_SBM_ID=" + sbm_id _
                               & " AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim j As Integer

        Dim lblHdr As Label
        Dim lblHdrId As Label


        Dim ddlObj As DropDownList
        Dim lblFirstName As LinkButton
        Dim lblLastName As Label
        Dim lblStuId As Label
        Dim lblStuNo As Label

        Dim strLink As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvStudent.Rows.Count - 1
            lblFirstName = gvStudent.Rows(i).FindControl("lblFirstName")
            lblLastName = gvStudent.Rows(i).FindControl("lblLastName")
            lblStuId = gvStudent.Rows(i).FindControl("lblStuId")
            lblStuNo = gvStudent.Rows(i).FindControl("lblStuNo")


            strLink = "clmBtecTrackStudentLevels_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                     & "&grade=" + ddlGrade.SelectedItem.Text _
                     & "&group=" + ddlGroup.SelectedItem.Text _
                     & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                     & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                     & "&stuno=" + lblStuNo.Text _
                     & "&sbmid=" + sbm_id _
                     & "&sbgid=" + sbg_id _
                     & "&grdid=" + ddlGrade.SelectedValue.ToString _
                     & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                     & "&sgrid=" + ddlGroup.SelectedValue.ToString _
                     & "&clvl=" + ddlLevel.SelectedValue.ToString _
                     & "&page=objectives"



            lblFirstName.Attributes.Add("onclick", "javascript:var popup = window.showModalDialog('" + strLink + "','','dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:no;status:no;');")


            For j = 0 To 24
                lblHdr = gvStudent.HeaderRow.FindControl("lblObj" + CStr(j + 1) + "H")
                lblHdrId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                If lblHdrId.Text <> "" And lblHdrId.Text <> "0" Then
                    ddlObj = gvStudent.Rows(i).FindControl("ddlObj" + CStr(j + 1) + "Id")
                    If Not objData.Item(lblStuId.Text + "_" + lblHdrId.Text) Is Nothing Then
                        '  chkObj.Checked = objData.Item(lblStuId.Text + "_" + lblHdrId.Text)
                        If Not ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text)) Is Nothing Then
                            ddlObj.ClearSelection()
                            ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text)).Selected = True
                        End If
                    End If
                    'chkObj.Attributes.Add("title", lblFirstName.Text + " " + lblLastName.Text + vbCrLf + lblHdr.Text)
                    gvStudent.Columns(6 + j).Visible = True
                Else
                    gvStudent.Columns(6 + j).Visible = False
                End If
            Next
        Next

    End Sub

    Sub SaveData_old()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlObj As DropDownList
        Dim lblStuId As Label


        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

        For i = 0 To gvStudent.Rows.Count - 1
            With gvStudent.Rows(i)
                For j = 0 To 24

                    lblObjId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                    ddlObj = .FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblStuId = .FindControl("lblStuId")

                    If lblObjId.Text <> "" And lblObjId.Text <> "0" Then
                        str_query = " EXEC BTEC.BTEC_saveSTUDENTOBJECTIVES @STO_STU_ID =" + lblStuId.Text + "," _
                                    & " @STO_SBM_ID=" + sbg_id(1) + "," _
                                    & " @STO_OBJ_ID=" + lblObjId.Text + "," _
                                    & " @STO_LVL_ID=" + h_LVL_ID.Value + "," _
                                    & " @STO_bCOMPLETED='" + ddlObj.SelectedValue + "'," _
                                    & " @STO_USER='" + Session("SUSR_NAME") + "'"

                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next
            End With
        Next





        str_query = " EXEC BTEC.BTEC_saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                  & " @SGR_ID =" + ddlGroup.SelectedValue.ToString + "," _
                  & " @SBM_ID =" + sbg_id(1)

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        lblError.Text = "Record saved successfully"
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlObj As DropDownList
        Dim lblStuId As Label


        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

        Dim strXml As String = ""




        For i = 0 To gvStudent.Rows.Count - 1
            With gvStudent.Rows(i)
                For j = 0 To 24

                    lblObjId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                    ddlObj = .FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblStuId = .FindControl("lblStuId")

                    If lblObjId.Text <> "" And lblObjId.Text <> "0" Then

                        strXml += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID>"
                        strXml += "<SBM_ID>" + sbg_id(1) + "</SBM_ID>"
                        strXml += "<OBJ_ID>" + lblObjId.Text + "</OBJ_ID>"
                        strXml += "<LVL_ID>" + h_LVL_ID.Value + "</LVL_ID>"
                        strXml += "<bCOMPLETED>" + ddlObj.SelectedValue + "</bCOMPLETED></ID>"

                    End If
                Next
            End With
        Next


        strXml = "<IDS>" + strXml + "</IDS>"


        str_query = " EXEC BTEC.BTEC_saveSTUDENTOBJECTIVES_BULK @STO_XML='" + strXml + "'," _
                   & " @STO_USER='" + Session("SUSR_NAME") + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        str_query = " EXEC BTEC.BTEC_saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                  & " @SGR_ID =" + ddlGroup.SelectedValue.ToString + "," _
                  & " @SBM_ID =" + sbg_id(1)

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        lblError.Text = "Record saved successfully"
    End Sub


#End Region

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        ViewState("SortFilter") = ""
        h_LVL_ID.Value = ddlLevel.SelectedValue.ToString
        GridBind()
        btnSave.Visible = True
        trGrid.Visible = True
        trSave.Visible = True
    End Sub

    Protected Sub lnkFirstNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "First Name"
        GridBind()
    End Sub

    Protected Sub lnkLastNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "Last Name"
        GridBind()
    End Sub


    Protected Sub gvStudent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudent.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(0).CssClass = "locked"
            'e.Row.Cells(1).CssClass = "locked"
            'e.Row.Cells(2).CssClass = "locked"
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()

        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubjects()
        BindGroup()
        BindCategory()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        BindGroup()
        BindLevel()
        BindCategory()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
        BindCategory()
        BindLevel()
        trGrid.Visible = False
        trSave.Visible = False

    End Sub
End Class
