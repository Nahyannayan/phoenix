<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmBtecTrackObjectives_M.aspx.vb" Inherits="Curriculum_BtecTracker_clmBtecTrackObjectives_M"
    Title="Untitled Page" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
        <tr>
            <td align="center" valign="bottom" style="height: 20px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" Style="text-align: center" Height="24px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 376px;border-collapse:collapse " >
                     <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                             Objectives</span></font></td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Select Subject
                        </td>
                        <td class="matters" align="center">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server" Width="302px" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Select Level
                        </td>
                        <td class="matters" align="center">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Import Data From Excel
                        </td>
                        <td class="matters" align="center">
                            :
                        </td>
                        <td class="matters" align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"
                                            Width="204px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnUpload" runat="server" CausesValidation="False" CssClass="button"
                                            TabIndex="30" Text="Upload Excel" Width="100px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left" colspan="3">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="gvObjectives" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="373px">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("OBJ_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sl.No" Visible="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Width="30px" Text='<%# Bind("OBJ_DISPLAYORDER") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Objective">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObjective" runat="server" Width="300px" Text='<%# Bind("OBJ_DESCR") %>'
                                                TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Objective" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblObjective" runat="server" Width="300px" Text='<%# Bind("OBJ_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Weightage">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtWeightage" Text='<%# Bind("OBJ_WEIGHTAGE") %>' Width="40px" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Category" >
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlCategory" runat="server" Width="100px"></asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>   
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Sl.No" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Width="30px" Text='<%# Bind("OBJ_DISPLAYORDER") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Weightage" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeightage" Text='<%# Bind("OBJ_WEIGHTAGE") %>' Width="40px" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                      <asp:TemplateField HeaderText="Mandatory" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTarget" checked='<%# Bind("OBJ_bTARGET") %>' Width="40px" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="Mandatory" visible="false" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblTarget" Text='<%# Bind("OBJ_bTARGET") %>' Width="40px" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="cat" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCatId" Text='<%# Bind("OBJ_OBT_ID") %>' Width="40px" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
 
</asp:Content>
