﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_BtecTracker_clmBtecTrackLevel_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C310005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindSubject()
                    BindCopySubject()
                    GridBind()

                    gvLevel.Attributes.Add("bordercolor", "#1b80b6")
                    gvSubject.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT NCM_DESCR,ISNULL(NCL_LVL_ID,0) AS NCL_LVL_ID, " _
                                & " ISNULL(NCL_DISPLAYORDER,NCM_DISPLAYORDER) AS NCL_DISPLAYORDER, " _
                                & " ISNULL(NCL_VALUE,NCM_VALUE) AS NCL_VALUE," _
                                & " ISNULL(NCL_WEIGHTAGE,0) AS NCL_WEIGHTAGE FROM " _
                                & " BTEC.BTEC_NCLEVEL_M AS A LEFT OUTER JOIN " _
                                & " BTEC.BTEC_NCLEVEL_BSU_M AS B ON A.NCM_DESCR=B.NCL_DESCR " _
                                & " AND B.NCL_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND NCL_SBM_ID=" + ddlSubject.SelectedValue.ToString

        If ddlSubject.SelectedItem.Text.ToUpper.Contains("ISLAMIC") Then
            str_query += " WHERE NCM_bARB_ISL=1"
        Else
            str_query += " WHERE NCM_bARB_ISL=0"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvLevel.DataSource = ds
        gvLevel.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S" _
                               & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND (SBG_DESCR LIKE 'BTEC%'" _
                               & " OR ISNULL(SBG_bBTEC,0)=1)" _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindCopySubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S" _
                               & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID IN('01','02','03','04','05','06','07','08')" _
                               & " AND SBG_SBM_ID<>" + ddlSubject.SelectedValue.ToString _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubject.DataSource = ds
        gvSubject.DataBind()
    End Sub

    Sub BindLevel(ByVal lvl_id As String, ByVal ddlLevel As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LVL_ID,LVL_DESCR FROM BTEC.BTEC_LEVEL_BSU_M WHERE LVL_BSU_ID='" + Session("SBSUID") + "'"
        If ddlSubject.SelectedItem.Text.ToUpper.Contains("ISLAMIC STUDIES") = False Then
            str_query += " AND ISNULL(LVL_bISL,'false')='FALSE'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "LVL_DESCR"
        ddlLevel.DataValueField = "LVL_ID"
        ddlLevel.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlLevel.Items.Insert(0, li)
        If Not ddlLevel.Items.FindByValue(lvl_id) Is Nothing Then
            ddlLevel.Items.FindByValue(lvl_id).Selected = True
        End If
    End Sub

    Sub SaveData(ByVal sbm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim ddlLevel As DropDownList
        Dim lblNCLevel As Label
        Dim lblOrder As Label
        Dim lblValue As Label
        Dim txtWeightage As TextBox
        For i = 0 To gvLevel.Rows.Count - 1

            With gvLevel.Rows(i)
                ddlLevel = .FindControl("ddlLevel")
                lblNCLevel = .FindControl("lblNCLevel")
                lblOrder = .FindControl("lblOrder")
                lblValue = .FindControl("lblValue")
                txtWeightage = .FindControl("txtWeightage")
            End With

            str_query = "EXEC [BTEC].[SAVE_BTEC_NCLEVEL_BSU_M] " _
                         & "'" + Session("sbsuid") + "'," _
                         & ddlAcademicYear.SelectedValue.ToString + "," _
                         & sbm_id + "," _
                         & ddlLevel.SelectedValue.ToString + "," _
                         & "'" + lblNCLevel.Text + "'," _
                         & Val(txtWeightage.Text).ToString + "," _
                         & lblOrder.Text + "," _
                         & lblValue.Text

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)



        Next
    End Sub
#End Region

    Protected Sub gvLevel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLevel.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblLevel As Label
            lblLevel = e.Row.FindControl("lblLevel")
            Dim ddlLevel As DropDownList
            ddlLevel = e.Row.FindControl("ddlLevel")
            BindLevel(lblLevel.Text, ddlLevel)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            SaveData(ddlSubject.SelectedValue.ToString)

            Dim i As Integer
            Dim chkSelect As CheckBox
            Dim lblSbmId As Label


            For i = 0 To gvSubject.Rows.Count - 1
                chkSelect = gvSubject.Rows(i).FindControl("chkSelect")

                If chkSelect.Checked = True Then
                    lblSbmId = gvSubject.Rows(i).FindControl("lblSbmId")
                    SaveData(lblSbmId.Text)
                End If
            Next

            lblError.Text = "Record Saved successfully"
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSubject()
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
        BindCopySubject()
    End Sub
End Class
