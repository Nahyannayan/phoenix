<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmBtecTrackStudentObjectives.aspx.vb" Inherits="Curriculum_BtecTracker_clmBtecTrackStudentObjectives" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script>
        function change_ddl_state(ddlThis, ddlObj) {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;


                if (currentid.indexOf(ddlObj) != -1) {

                    if (ddlThis.selectedIndex != 0) {

                        document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex - 1;

                    }
                }
            }
        }
        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function autoWidth(mySelect) {
            var maxlength = 0;

            //   var mySelect=document.getElementById(GetClientId(ddl));


            for (var i = 0; i < mySelect.options.length; i++) {
                if (mySelect[i].text.length > maxlength) {
                    maxlength = mySelect[i].text.length;
                }
            }
            mySelect.style.width = maxlength * 5;
        }

        function setWidth(mySelect) {
            //var mySelect=document.getElementById(GetClientId(ddl));
            mySelect.style.width = 35;
        }
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Btec Student Objectives</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkSearch" runat="server" Visible="false">Search</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:Panel ID="pnlSearch" runat="server">
                                <table id="Table2" runat="server" width="100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Subject</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                            </asp:DropDownList></td>
                                        <td align="left"><span class="field-label">Group</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlGroup" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>


                                    <tr>
                                        <td align="left"><span class="field-label">Level</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlLevel" runat="server">
                                            </asp:DropDownList></td>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td >
                                                    <span class="field-label">    <asp:RadioButton ID="rdObj1" runat="server" Checked="True" GroupName="g1"
                                                            Text="Objectives 1 to 25" /></span>
                                                    </td>
                                                    <td colspan="2">
                                                      <span class="field-label">  <asp:RadioButton ID="rdObj2" runat="server" GroupName="g1"
                                                            Text="Objectives 26 to 50" /></span>
                                                    </td>
                                                    <td colspan="2">
                                                      <span class="field-label">  <asp:RadioButton ID="rdObj3" runat="server" GroupName="g1"
                                                            Text="Objectives 51 to 75" /></span>
                                                    </td>
                                                    <td colspan="2">
                                                      <span class="field-label">  <asp:RadioButton ID="rdObj4" runat="server" GroupName="g1"
                                                            Text="Objectives 76 to 100" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Objective Category</span></td>
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlCategory" runat="server" >
                                            </asp:DropDownList></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label">Student ID</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left"><span class="field-label">Student Name</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnView" runat="server" CssClass="button" TabIndex="7"
                                                Text="View Students" ValidationGroup="groupM1" />
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>

                        </td>
                    </tr>
                    <tr id="trGrid" runat="server">
                        <td align="left">
                            <table Width="100%">
                                <tr>
                                    <td align="left">
                                        <div id="div_gridholder">
                                            <asp:GridView ID="gvStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False" Width="100%"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                 PageSize="20"  UseAccessibleHeader="True" BorderStyle="Solid">
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle  />
                                                <HeaderStyle  VerticalAlign="Top" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />


                                                <EmptyDataRowStyle />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Stuid" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'  ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"    Wrap="true"></ItemStyle>
                                                        <HeaderStyle   Wrap="true" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="First Name">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lnkFirstNameH" runat="server" Text="First Name" OnClick="lnkFirstNameH_Click" ></asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblFirstName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                                        <HeaderStyle  />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Last Name">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lnkLastNameH" runat="server" Text="Last Name" OnClick="lnkLastNameH_Click"></asp:LinkButton>
                                                        </HeaderTemplate>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("STU_LASTNAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"  ></ItemStyle>
                                                        <HeaderStyle  />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Current Level">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrentLevel" runat="server" Text='<%# Bind("SCL_CURRENTLEVEL") %>'  ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                                        <HeaderStyle    />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Objectives Completed">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblObjCount" runat="server" Text='<%# Bind("OBJCOUNT") %>' ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                                                        <HeaderStyle  />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Stuno" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>' ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"  ></ItemStyle>
                                                        <HeaderStyle    />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj1H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId1" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH1" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj1Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" />
                                                                        </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>

                                                            <asp:DropDownList ID="ddlObj1Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                        <HeaderStyle Wrap="true"   />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj2H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId2" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH2" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj2Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj2Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj3H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId3" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH3" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj3Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj3Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj4H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId4" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH4" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj4Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj4Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj5H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId5" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH5" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj5Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                          
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj5Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj6H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId6" runat="server" Visible="false"></asp:Label><br />
                                                                          <asp:DropDownList ID="ddlObjH6" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj6Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj6Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj7H" runat="server"></asp:Label>
                                                                        <asp:Label ID="lblObjId7" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH7" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj7Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj7Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj8H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId8" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH8" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj8Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj8Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj9H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId9" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH9" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj9Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj9Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj10H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId10" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH10" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj10Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                         
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj10Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj11H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId11" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH11" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj11Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj11Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj12H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId12" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH12" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj12');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                         
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj12Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj13H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId13" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH13" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj13Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj13Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj14H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId14" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH14" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj14Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj14Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj15H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId15" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH15" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj15Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj15Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj16H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId16" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH16" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj16Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj16Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj17H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId17" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH17" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj17Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                         
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj17Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj18H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId18" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH18" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj18Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj18Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj19H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId19" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH19" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj19Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                          
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj19Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj20H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId20" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH20" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj20Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj20Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj21H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId21" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH21" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj21Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj21Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj22H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId22" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH22" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj22Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj22Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj23H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId23" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH23" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj23Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" title="No" />
                                                                        </asp:DropDownList>
                                                          
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj23Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj24H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId24" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH24" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj24Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" />
                                                                        </asp:DropDownList>
                                                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj24Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                                        <asp:Label ID="lblObj25H" runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblObjId25" runat="server" Visible="false"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlObjH25" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj25Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                            <asp:ListItem Text="I" Value="I" style="background: yellow" title="Insufficient Evidence" />
                                                                            <asp:ListItem Text="N" Value="N" style="background: red" />
                                                                        </asp:DropDownList>
                                                           
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj25Id" runat="server">
                                                                <asp:ListItem Text="Y" Value="Y" style="background: green;" title="Yes" />
                                                                <asp:ListItem Text="I" Value="I" style="background: yellow;" title="Insufficient Evidence" />
                                                                <asp:ListItem Text="N" Value="N" style="background: red;" Selected="True" title="No" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                        <asp:HiddenField ID="h_LVL_ID" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

