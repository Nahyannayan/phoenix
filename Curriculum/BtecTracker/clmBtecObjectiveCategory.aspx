<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmBtecObjectiveCategory.aspx.vb" Inherits="Curriculum_BtecTracker_clmBtecObjectiveCategory" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
        <tr>
            <td align="center" valign="bottom" style="height: 20px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" Style="text-align: center" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 376px;border-collapse:collapse " >
                     <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                                OBJECTIVE CATEGORIES</td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Select Subject
                        </td>
                        <td class="matters" align="center">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server" Width="302px" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="gvCategory" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="373px">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCatId" runat="server" Text='<%# Bind("OBT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCategory" runat="server" Width="300px" Text='<%# Bind("OBT_DESCR") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ShortCode">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtShort" runat="server" Width="100px" Text='<%# Bind("OBT_SHORT") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
 

</asp:Content>

