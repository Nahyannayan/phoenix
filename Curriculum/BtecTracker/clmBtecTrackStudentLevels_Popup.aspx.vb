﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_BtecTracker_clmBtecTrackStudentLevels_Popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            lblAcademicYear.Text = Request.QueryString("accyear")
            lblGrade.Text = Request.QueryString("grade")
            lblGroup.Text = Request.QueryString("group")
            lblSubject.Text = Request.QueryString("subject")
            lblName.Text = Request.QueryString("name")
            lblStudentID.Text = Request.QueryString("stuno")
            '  lblCurrentLevel.Text = Request.QueryString("clvl")

            hSBM_ID.Value = Request.QueryString("sbmid").Replace(".00", "").Replace(",", "")
            hACD_ID.Value = Request.QueryString("acdid").Replace(".00", "").Replace(",", "")
            hSGR_ID.Value = Request.QueryString("sgrid").Replace(".00", "").Replace(",", "")
            hSBG_ID.Value = Request.QueryString("sbgid").Replace(".00", "").Replace(",", "")
            hGRD_ID.Value = Request.QueryString("grdid").Replace(".00", "").Replace(",", "")

            Dim pg As String = Request.QueryString("page").Replace(".00", "").Replace(",", "")

            If pg = "objectives" Then
                tabPopup.ActiveTabIndex = 1
            End If
            BindCategory()
            GetStu_ID()
            BindPhoto()
            BindLevel()

            GridBind()
            GetCurrentLevel()
            BindObjectiveCount(True)

            BindReportCard()
            BindRSM_ID()
            BindHeader()
            BindValues()
            gvMarks.Attributes.Add("bordercolor", "#1b80b6")
            trError.Visible = False

            If ddlReportCard.SelectedValue = "0" Then
                tr1.Visible = False
                tr2.Visible = False
            Else
                tr1.Visible = True
                tr2.Visible = True
            End If
        End If
    End Sub

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M " _
                              & " INNER JOIN RPT.REPORT_SETUP_M ON RSM_ID=RPF_RSM_ID " _
                              & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID " _
                              & " WHERE RSM_ACD_ID='" + hACD_ID.Value + "'" _
                              & " AND RSG_GRD_ID='" + hGRD_ID.Value + "'" _
                              & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RPF_DESCR"
        ddlReportCard.DataValueField = "RPF_ID"
        ddlReportCard.DataBind()
        Dim li As New ListItem
        li.Text = "Select Report"
        li.Value = "0"
        ddlReportCard.Items.Insert(0, li)
    End Sub

    Sub BindRSM_ID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_RSM_ID FROM RPT.REPORT_PRINTEDFOR_M " _
                                & " WHERE RPF_ID='" + ddlReportCard.SelectedValue.ToString + "'"
        hRSM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String

        str_query = "SELECT SBG_ID='" + hSBG_ID.Value + "',RSD_ID,RSD_HEADER," _
                                & " V1=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'TRUE' ELSE 'FALSE' END," _
                                & " V2=CASE WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'TRUE' ELSE 'FALSE' END," _
                                & " V3=CASE WHEN RSD_RESULT='D' THEN 'TRUE' ELSE 'FALSE' END," _
                                & " V4=CASE WHEN RSD_RESULT='L' THEN 'TRUE' ELSE 'FALSE' END," _
                                & " TYPE=CASE WHEN RSD_RESULT='L' then 'lbl' WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXSMALL' THEN 'txtsmall' " _
                                & " WHEN RSD_RESULT='C' AND RSD_CSSCLASS='TEXTBOXMULTI' THEN 'txtmulti'" _
                                & "  WHEN RSD_RESULT='D' THEN 'dropdown' ELSE 'txtsmall' END" _
                                & " FROM ("

        'str_query += "SELECT SBG_ID=" + hSBG_ID.Value + ",RSD_ID=-1,RSD_HEADER='Previous Results'," _
        '                  & " RSD_CSSCLASS='TEXTBOXSMALL',RSD_RESULT='L'," _
        '                  & " RSD_DISPLAYORDER=-1 UNION ALL "


        str_query += "SELECT SBG_ID=" + hSBG_ID.Value + ",RSD_ID,RSD_HEADER=CASE WHEN RSD_HEADER LIKE 'POI%' THEN RSD_HEADER WHEN RSD_HEADER='Programme of Inquiry' THEN RSD_HEADER ELSE CASE WHEN ISNULL(RSD_SUB_DESC,'')='' THEN RSD_HEADER ELSE RSD_SUB_DESC END END ," _
                            & " RSD_CSSCLASS,RSD_RESULT=CASE WHEN (SELECT COUNT(*) FROM RPT.REPORTSETUP_DEFAULTS_S " _
                            & " WHERE RSP_RSD_ID=A.RSD_ID AND RSP_SBG_ID=" + hSBG_ID.Value + ")>0 THEN 'D' WHEN RSD_bDIRECTENTRY='FALSE' THEN 'L' ELSE 'C' END," _
                            & " RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D AS A WHERE RSD_RSM_ID='" + hRSM_ID.Value + "'" _
                            & " AND RSD_bALLSUBJECTS='TRUE'  AND ISNULL(RSD_bCOMMON,0)=0  AND ISNULL(RSD_bSUPRESS,0)=0" _
                            & " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID='" + hSBG_ID.Value + "')"




        str_query += ")PP ORDER BY RSD_DISPLAYORDER,RSD_HEADER"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvMarks.DataSource = ds
        gvMarks.DataBind()

        If gvMarks.Rows.Count > 0 Then
            gvMarks.HeaderRow.Visible = False
        End If
    End Sub
    Sub BindValues()



        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT RST_SBG_ID,RST_RSD_ID,CASE WHEN RST_COMMENTS IS NOT NULL THEN RST_COMMENTS WHEN RST_MARK IS NOT NULL  " _
                                    & " AND RST_GRADING IS NOT NULL THEN CONVERT(VARCHAR(100),RST_MARK)+'-'+RST_GRADING " _
                                    & " WHEN RST_MARK IS NOT NULL AND RST_GRADING IS NULL THEN CONVERT(VARCHAR(100),RST_MARK) " _
                                    & " WHEN RST_MARK IS NULL AND RST_GRADING IS NOT NULL THEN RST_GRADING ELSE isnull(RST_COMMENTS,'') END FROM " _
                                    & " RPT.REPORT_STUDENT_S WHERE RST_STU_ID=" + hSTU_ID.Value _
                                    & " AND RST_RPF_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                    & " AND ISNULL(RST_SBG_ID,0)=" + hSBG_ID.Value


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim grades As New Hashtable

        Dim i, j, k As Integer


        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                grades.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2))
            End With
        Next



        Dim lblRsdId As Label
        Dim txtGrade As TextBox
        Dim txtComments As TextBox
        Dim ddlGrades As DropDownList
        Dim lblType As Label
        Dim value As String = ""
        Dim lblGrade As Label
        Dim lblMark As Label
        Dim lblHeader As Label
        Dim spellArray As String()
        Dim spellString As String = ""

        For k = 0 To gvMarks.Rows.Count - 1
            With gvMarks.Rows(k)
                lblHeader = gvMarks.Rows(k).FindControl("lblHeader")
                lblGrade = .FindControl("lblGrade")
                If lblHeader.Text <> "Previous Results" Then
                    lblRsdId = .FindControl("lblRsdId")
                    lblType = .FindControl("lblType")

                    If grades.ContainsKey(hSBG_ID.Value + "_" + lblRsdId.Text) = True Then
                        value = grades.Item(hSBG_ID.Value + "_" + lblRsdId.Text)
                    Else
                        value = ""
                    End If
                    If lblType.Text = "txtmulti" Then
                        txtComments = .FindControl("txtComments")
                        txtComments.Text = value
                        If spellString <> "" Then
                            spellString += "|"
                        End If
                        spellString += txtComments.ClientID
                    ElseIf lblType.Text = "dropdown" Then
                        ddlGrades = .FindControl("ddlGrades")
                        If Not ddlGrades.Items.FindByText(value) Is Nothing Then
                            ddlGrades.ClearSelection()
                            ddlGrades.Items.FindByText(value).Selected = True
                        End If
                    ElseIf lblType.Text = "txtsmall" Then
                        txtGrade = .FindControl("txtGrade")
                        txtGrade.Text = value
                    Else
                        lblMark = .FindControl("lblMark")
                        lblMark.Text = value

                    End If
                    lblGrade.Text = value
                Else
                    str_query = "select isnull(STUFF((SELECT ', '+  RPF_DESCR+' - '+RST_COMMENTS" _
                             & " FROM RPT.REPORT_STUDENT_S AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RST_RPF_ID=B.RPF_ID" _
                             & " INNER JOIN RPT.REPORT_SETUP_D AS C ON A.RST_RSD_ID=C.RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1" _
                             & " WHERE RST_RPF_ID<>'" + ddlReportCard.SelectedValue.ToString + "' AND RST_SBG_ID=" + hSBG_ID.Value + " AND RST_STU_ID=" + hSTU_ID.Value + " AND RST_COMMENTS IS NOT NULL for xml path('')),1,1,''),'')"
                    lblMark = .FindControl("lblMark")
                    lblMark.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                End If

            End With
        Next

        spellArray = spellString.Split("|")
        RadSpell1.ControlsToCheck = spellArray
        ' RadSpell2.ControlsToCheck = spellArray


    End Sub

    Protected Sub gvMarks_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblType As Label = e.Row.FindControl("lblType")
            Dim lblHeader As Label = e.Row.FindControl("lblHeader")
            Dim lblSbgId As Label = e.Row.FindControl("lblSbgId")
            Dim txtComments As TextBox = e.Row.FindControl("txtComments")
            Dim ddlGrades As DropDownList = e.Row.FindControl("ddlGrades")
            Dim lblRsdId As Label = e.Row.FindControl("lblRsdId")

            If lblType.Text = "txtmulti" Then
                Dim imgBtn As ImageButton = e.Row.FindControl("imgBtn")
                If Not imgBtn Is Nothing Then
                    imgBtn.OnClientClick = "javascript:getcomments('" + txtComments.ClientID + "','ALLCMTS','" + lblHeader.Text + "','" + hSTU_ID.Value + "','" + hRSM_ID.Value.ToString + "','" + hSBG_ID.Value + "'); return false;"
                End If
            ElseIf lblType.Text = "dropdown" Then
                BindDefaultValues(ddlGrades, lblRsdId.Text)
            End If
        End If

    End Sub
    Sub BindDefaultValues(ByVal ddlGrades As DropDownList, ByVal rsd_id As String)
        Dim li As ListItem
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S " _
                                 & " WHERE RSP_RSD_ID='" + rsd_id + "' AND RSP_SBG_ID=" + hSBG_ID.Value _
                                 & " ORDER BY RSP_DISPLAYORDER,RSP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        For i = 0 To ds.Tables(0).Rows.Count - 1
            li = New ListItem
            li.Text = ds.Tables(0).Rows(i).Item(0).ToString
            li.Value = ds.Tables(0).Rows(i).Item(0).ToString
            ddlGrades.Items.Add(li)
        Next

        If ddlGrades.Items.FindByValue("--") Is Nothing Then
            li = New ListItem
            li.Text = "--"
            li.Value = "--"
            ddlGrades.Items.Insert(0, li)
        End If
    End Sub

    Sub BindObjectiveCount(ByVal bTotal As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(STO_ID) FROM BTEC.BTEC_STUDENT_OBJECTIVES_S WHERE STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_bCOMPLETED='Y' AND STO_STU_ID=" + hSTU_ID.Value

        Dim str As String
        Dim str1 As String

        If bTotal = True Then
            str = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
            lblTotalObjectives.Text = "Total Objectives Completed : " + str
        End If

        str_query = "SELECT COUNT(STO_ID) FROM BTEC.BTEC_STUDENT_OBJECTIVES_S WHERE STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_bCOMPLETED='Y' AND STO_LVL_ID=" + ddlLevel.SelectedValue.ToString _
                                & " AND STO_STU_ID=" + hSTU_ID.Value
        str1 = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
        lblLevelObjectives.Text = "Objectives Completed in " + ddlLevel.SelectedItem.Text + " : " + str1
    End Sub

    Sub BindPhoto()

        If hStuPhotoPath.Value <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + hStuPhotoPath.Value
            imgStud.ImageUrl = strImagePath


        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub


    Sub BindCategory()
        ddlCategory.Items.Clear()


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBT_ID,OBT_DESCR FROM " _
                                & " BTEC.BTEC_OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                                & " AND OBT_SBM_ID=" + hSBM_ID.Value _
                                & " ORDER BY OBT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "OBT_DESCR"
        ddlCategory.DataValueField = "OBT_ID"
        ddlCategory.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)
    End Sub

    Sub BindLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LVL_ID,LVL_DESCR FROM BTEC.BTEC_LEVEL_BSU_M WHERE LVL_BSU_ID='" + Session("SBSUID") + "'"

        If lblSubject.Text.ToUpper <> "ISLAMIC STUDIES" Then
            str_query += " AND ISNULL(LVL_bISL,'false')='FALSE'"
        End If

        str_query += " AND LVL_ID IN(SELECT OBJ_LVL_ID FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                          & " AND OBJ_SBM_ID=" + hSBM_ID.Value + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "LVL_DESCR"
        ddlLevel.DataValueField = "LVL_ID"
        ddlLevel.DataBind()

        str_query = "SELECT DISTINCT NCL_LVL_ID FROM BTEC.BTEC_NCLEVEL_BSU_M AS A" _
                    & " INNER JOIN BTEC.BTEC_STUDENT_CURRENTLEVEL_S AS B ON " _
                    & " A.NCL_DESCR=B.SCL_CURRENTLEVEL  AND B.SCL_SBM_ID=A.NCL_SBM_ID  " _
                    & " WHERE SCL_STU_ID=" + hSTU_ID.Value + " AND SCL_SBM_ID=" + hSBM_ID.Value _
                    & " AND NCL_ACD_ID=" + hACD_ID.Value

        Dim lvlid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If Not ddlLevel.Items.FindByValue(lvlid) Is Nothing Then
            ddlLevel.Items.FindByValue(lvlid).Selected = True
        End If
    End Sub

    Sub GetStu_ID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH FROM STUDENT_M WHERE STU_NO='" + lblStudentID.Text + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hSTU_ID.Value = ds.Tables(0).Rows(0).Item(0)
            hStuPhotoPath.Value = ds.Tables(0).Rows(0).Item(1)
            Session("APP_STU_ID") = hSTU_ID.Value
        End If
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBJ_DESCR,OBJ_ID FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_LVL_ID='" + ddlLevel.SelectedValue.ToString + "'" _
                                & " AND OBJ_SBM_ID='" + hSBM_ID.Value + "' AND OBJ_BSU_ID='" + Session("SBSUID") + "'"

        If ddlCategory.SelectedValue.ToString <> "0" Then
            str_query += "AND OBJ_OBT_ID='" + ddlCategory.SelectedValue.ToString + "'"
        End If

        str_query += " ORDER BY OBJ_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvObjectives.DataSource = ds
        gvObjectives.DataBind()

        GetStudentObjectives()

        BindTargetGrid()
    End Sub


    Sub BindTargetGrid()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If hObjCompleted.Value <> "" Then
            str_query = "SELECT OBJ_DESCR,OBJ_ID FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_LVL_ID='" + ddlLevel.SelectedValue.ToString + "'" _
                                    & " AND OBJ_SBM_ID='" + hSBM_ID.Value + "' AND OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                                    & " AND OBJ_ID NOT IN(" + hObjCompleted.Value + ")"

        Else
            str_query = "SELECT OBJ_DESCR,OBJ_ID FROM BTEC.BTEC_OBJECTIVES_M WHERE OBJ_LVL_ID='" + ddlLevel.SelectedValue.ToString + "'" _
                                    & " AND OBJ_SBM_ID='" + hSBM_ID.Value + "' AND OBJ_BSU_ID='" + Session("SBSUID") + "'"

        End If

        If ddlCategory.SelectedValue.ToString <> "0" Then
            str_query += "AND OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString
        End If

        str_query += " ORDER BY OBJ_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTarget.DataSource = ds
        gvTarget.DataBind()

        GetStudentTargets()
    End Sub
    Sub GetStudentObjectives()
        Dim objData As New Hashtable
        hObjCompleted.Value = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STO_OBJ_ID,STO_bCOMPLETED FROM BTEC.BTEC_STUDENT_OBJECTIVES_S" _
                                & " WHERE STO_LVL_ID='" + ddlLevel.SelectedValue.ToString + "' AND STO_SBM_ID='" + hSBM_ID.Value + "'" _
                                & " AND STO_STU_ID='" + hSTU_ID.Value + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlStatus As DropDownList

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString, .Item(1).ToString)
                If .Item(1).ToString = "Y" Then
                    If hObjCompleted.Value <> "" Then
                        hObjCompleted.Value += ","
                    End If
                    hObjCompleted.Value += .Item(0).ToString
                End If
            End With
        Next

        For i = 0 To gvObjectives.Rows.Count - 1
            lblObjId = gvObjectives.Rows(i).FindControl("lblObjId")

            If Not objData.Item(lblObjId.Text) Is Nothing Then
                ddlStatus = gvObjectives.Rows(i).FindControl("ddlStatus")
                If Not ddlStatus.Items.FindByValue(objData.Item(lblObjId.Text)) Is Nothing Then
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(objData.Item(lblObjId.Text)).Selected = True
                End If
            End If
        Next
    End Sub

    Sub GetStudentTargets()
        Dim objData As New Hashtable

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STO_OBJ_ID,STO_bTARGET,isnull(STO_bRPTCRDSHOWTARGET,'false') FROM BTEC.BTEC_STUDENT_OBJECTIVES_S" _
                                & " WHERE STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + " AND STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_STU_ID=" + hSTU_ID.Value + " AND ISNULL(STO_bTARGET,'FALSE')='TRUE'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim lblObjId As Label
        Dim chkStatus As CheckBox
        Dim chkDisplay As CheckBox
        Dim strDisplay As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvTarget.Rows.Count - 1
            lblObjId = gvTarget.Rows(i).FindControl("lblObjId")

            If Not objData.Item(lblObjId.Text) Is Nothing Then
                strDisplay = objData.Item(lblObjId.Text)
                If strDisplay.ToLower = "true" Then
                    chkDisplay = gvTarget.Rows(i).FindControl("chkDisplay")
                    chkDisplay.Checked = True
                End If
                chkStatus = gvTarget.Rows(i).FindControl("chkStatus")
                chkStatus.Checked = True
            End If
        Next
    End Sub
    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlStatus As DropDownList


        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim bCheck As Boolean

        For i = 0 To gvObjectives.Rows.Count - 1

            lblObjId = gvObjectives.Rows(i).FindControl("lblObjId")
            ddlStatus = gvObjectives.Rows(i).FindControl("ddlStatus")

            str_query = " EXEC BTEC.BTEC_saveSTUDENTOBJECTIVES @STO_STU_ID =" + hSTU_ID.Value + "," _
                            & " @STO_SBM_ID=" + hSBM_ID.Value + "," _
                            & " @STO_OBJ_ID=" + lblObjId.Text + "," _
                            & " @STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + "," _
                            & " @STO_bCOMPLETED='" + ddlStatus.SelectedValue + "'," _
                            & " @STO_USER='" + Session("SUSR_NAME") + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        Next





        str_query = " EXEC BTEC.BTEC_saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + hACD_ID.Value + "," _
                  & " @SGR_ID =" + hSGR_ID.Value + "," _
                  & " @SBM_ID =" + hSBM_ID.Value + "," _
                  & " @STU_ID=" + hSTU_ID.Value

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        Session("Popup") = "1"
    End Sub

    Sub SaveTarget()
        Dim i As Integer
        Dim lblObjId As Label
        Dim chkStatus As CheckBox


        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim bCheck As Boolean
        Dim chkDisplay As CheckBox
        Dim bDispay As Boolean

        For i = 0 To gvTarget.Rows.Count - 1

            lblObjId = gvTarget.Rows(i).FindControl("lblObjId")
            chkStatus = gvTarget.Rows(i).FindControl("chkStatus")
            chkDisplay = gvTarget.Rows(i).FindControl("chkDisplay")
            bCheck = chkStatus.Checked
            bDispay = chkDisplay.Checked
            str_query = " EXEC BTEC.BTEC_saveSTUDENTOBJECTIVES_TARGET @STO_STU_ID =" + hSTU_ID.Value + "," _
                            & " @STO_SBM_ID=" + hSBM_ID.Value + "," _
                            & " @STO_OBJ_ID=" + lblObjId.Text + "," _
                            & " @STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + "," _
                            & " @STO_bTARGET='" + bCheck.ToString + "'," _
                            & " @STO_USER='" + Session("SUSR_NAME") + "'," _
                            & " @STO_bRPTCRDSHOWTARGET='" + bDispay.ToString + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        Next





        str_query = " EXEC BTEC.BTEC_saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + hACD_ID.Value + "," _
                  & " @SGR_ID =" + hSGR_ID.Value + "," _
                  & " @SBM_ID =" + hSBM_ID.Value + "," _
                  & " @STU_ID=" + hSTU_ID.Value

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub SaveReportCard()
        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim lblRsdId As Label
        Dim txtGrade As TextBox
        Dim txtComments As TextBox
        Dim ddlGrades As DropDownList
        Dim lblType As Label
        Dim value As String = ""
        Dim lblGrade As Label

        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim stTrans As SqlTransaction

        Dim k As Integer

        objConn.Open()
        stTrans = objConn.BeginTransaction
        Dim iReturnvalue As Integer
        Try

            For k = 0 To gvMarks.Rows.Count - 1
                With gvMarks.Rows(k)

                    lblRsdId = .FindControl("lblRsdId")
                    lblType = .FindControl("lblType")
                    lblGrade = .FindControl("lblGrade")

                    If lblType.Text = "txtmulti" Then
                        txtComments = .FindControl("txtComments")
                        value = txtComments.Text
                    ElseIf lblType.Text = "dropdown" Then
                        ddlGrades = .FindControl("ddlGrades")
                        value = ddlGrades.SelectedItem.Text
                    ElseIf lblType.Text = "txtsmall" Then
                        txtGrade = .FindControl("txtGrade")
                        value = txtGrade.Text
                    End If


                    If value <> lblGrade.Text And lblType.Text <> "lbl" Then
                        cmd = New SqlCommand("[RPT].[SaveREPORT_STUDENT_S]", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("@RST_ID", 0)
                        cmd.Parameters.AddWithValue("@RST_RPF_ID", ddlReportCard.SelectedValue.ToString)
                        cmd.Parameters.AddWithValue("@RST_RSD_ID", lblRsdId.Text) '-- Report Setup
                        cmd.Parameters.AddWithValue("@RST_ACD_ID", hACD_ID.Value)
                        cmd.Parameters.AddWithValue("@RST_GRD_ID", hGRD_ID.Value)
                        cmd.Parameters.AddWithValue("@RST_STU_ID", hSTU_ID.Value)
                        cmd.Parameters.AddWithValue("@RST_RSS_ID", 0)
                        cmd.Parameters.AddWithValue("@RST_SGR_ID", hSGR_ID.Value)
                        cmd.Parameters.AddWithValue("@RST_SBG_ID", hSBG_ID.Value)
                        cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", 0)
                        cmd.Parameters.AddWithValue("@RST_MARK", DBNull.Value)
                        cmd.Parameters.AddWithValue("@RST_COMMENTS", value)
                        cmd.Parameters.AddWithValue("@RST_GRADING", DBNull.Value)
                        cmd.Parameters.AddWithValue("@RST_USER", Session("sUsr_name"))
                        cmd.Parameters.AddWithValue("@bEdit", 0)
                        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        cmd.ExecuteNonQuery()
                        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                        If iReturnvalue <> 0 Then
                            stTrans.Rollback()
                            objConn.Close()
                            lblError.Text = "Request could not be processed"
                        End If
                    End If
                End With
            Next
            stTrans.Commit()
            objConn.Close()
            lblError.Text = ""
        Catch ex As Exception
            stTrans.Rollback()
            objConn.Close()
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Sub GetCurrentLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(SCL_CURRENTLEVEL,'') SCL_CURRENTLEVEL FROM BTEC.BTEC_STUDENT_CURRENTLEVEL_S WHERE" _
                                 & " SCL_STU_ID=" + hSTU_ID.Value + " AND SCL_SBM_ID=" + hSBM_ID.Value
        lblCurrentLevel.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Session("APP_STU_CURRENTLEVEL") = lblCurrentLevel.Text
    End Sub




    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If tabPopup.ActiveTabIndex = 0 Then
            SaveData()
            GetCurrentLevel()
            GridBind()
            BindObjectiveCount(True)
        ElseIf tabPopup.ActiveTabIndex = 1 Then
            SaveTarget()
            GridBind()
            BindObjectiveCount(True)
        ElseIf tabPopup.ActiveTabIndex = 2 Then
            If ddlReportCard.SelectedValue.ToString <> 0 Then
                SaveReportCard()
            End If
        End If

    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindObjectiveCount(False)
        GridBind()
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        BindObjectiveCount(False)
        GridBind()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        tabPopup.ActiveTabIndex = 2
        BindRSM_ID()
        BindHeader()
        BindValues()
        If ddlReportCard.SelectedValue = "0" Then
            tr1.Visible = False
            tr2.Visible = False
        Else
            tr1.Visible = True
            tr2.Visible = True
        End If
    End Sub
End Class
