<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmBtecTrackLevel_M.aspx.vb" Inherits="Curriculum_BtecTracker_clmBtecTrackLevel_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<script>
  function change_chk_state(chkThis)
     {
    var chk_state= ! chkThis.checked ;
     for(i=0; i<document.forms[0].elements.length; i++)
           {
           var currentid =document.forms[0].elements[i].id; 
           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
         {
           //if (document.forms[0].elements[i].type=='checkbox' )
              //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked=chk_state;
                 document.forms[0].elements[i].click();//fire the click event of the child element
             }
          }
      }     
           
</script>
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
          <tr>
         <td align="center"  valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" style="text-align: center" Height="24px"></asp:Label></td>
        </tr>
        <tr><td>
 <table id="Table2" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 376px;border-collapse:collapse"  >
                   <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                             Levels</span></font></td>
                    </tr>
                    <tr>
                     <td class="matters" align="left">Academic Year</td>
                     <td class="matters" align="center">:</td>
                     <td class="matters" align="left" ><asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>
                    </tr>
                    <tr>
                     <td class="matters" align="left">Select Subject</td>
                     <td class="matters" align="center">:</td>
                     <td class="matters" align="left" ><asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="true" width="300px">
                        </asp:DropDownList ></td>
                    </tr>
                    <tr><td colspan="3" >&nbsp;</td></tr>
                    <tr>
    <td colspan="3" align="left">
    <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="groupM1" TabIndex="7" />
    </td></tr>
   
                    <tr><td colspan="3">
    <asp:GridView ID="gvLevel" runat="server" AllowPaging="false" AutoGenerateColumns="False"
         EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
        HeaderStyle-Height="30" PageSize="20" Width="373px" SkinID="GridViewView">
        <RowStyle  Height="25px" Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <Columns>
             <asp:TemplateField HeaderText="NC Level" Visible="false" >
                <ItemTemplate>
                    <asp:Label ID="lblLevel" runat="server"  Text='<%# Bind("NCL_LVL_ID") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
                   
             <asp:TemplateField HeaderText="NC Level" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblOrder" runat="server"  Text='<%# Bind("NCL_DISPLAYORDER") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
            
             <asp:TemplateField HeaderText="NC Level" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblValue" runat="server"  Text='<%# Bind("NCL_VALUE") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
                    
            <asp:TemplateField HeaderText="Btec Level" >
                <ItemTemplate>
                    <asp:Label ID="lblNCLevel" runat="server"  Text='<%# Bind("NCM_DESCR") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Level">
                <ItemTemplate>
                    <asp:DropDownList ID="ddlLevel" runat="server"></asp:DropDownList>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            
           
            <asp:TemplateField HeaderText="Weightage" >
                <ItemTemplate>
                   <asp:TextBox ID="txtWeightage"   Text='<%# Bind("NCL_WEIGHTAGE") %>' Width="40px" runat="server"></asp:TextBox>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            
        </Columns>
        <SelectedRowStyle CssClass="Green" Wrap="False" />
        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
        <EditRowStyle Wrap="False" />
        <AlternatingRowStyle  Wrap="False" />
    </asp:GridView>
    </td></tr>
    
     <tr>
        <td align="left" class="matters" colspan="3" style="width: 338px">
            Copy default values to subjects</td>
    </tr>
<tr><td colspan="3" align="center" style="width: 338px">

<asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False"
CssClass="gridstyle"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
HeaderStyle-Height="30" PageSize="20" Width="373px"  >
<RowStyle CssClass="griditem" Height="25px" Wrap="False" />
<EmptyDataRowStyle Wrap="False" />
<Columns>

<asp:TemplateField HeaderText="Select">
<EditItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" />
</EditItemTemplate>
<HeaderTemplate>
<table width="100%">
<tr>
    <td align="center" >
        Select
    </td>
</tr>
<tr>
    <td align="center">
        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
            ToolTip="Click here to select/deselect all rows" />
    </td>
</tr>
</table>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
</ItemTemplate>
<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="sbm_id" Visible="False"><ItemTemplate>
<asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("SBG_SBM_ID") %>'></asp:Label>
</ItemTemplate>
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="Subject"><ItemTemplate>
<asp:Label ID="lblSubject" runat="server" text='<%# Bind("SBG_DESCR") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>


</Columns>  
<SelectedRowStyle CssClass="Green" Wrap="False" />
<HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
<EditRowStyle Wrap="False" />
<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
</td></tr>
    <tr>
    <td colspan="3" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="groupM1" TabIndex="7" />
    </td></tr>
    </table>
    </td></tr></table>
</asp:Content>
