﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Curriculum_BtecTracker_clmBtecTrackObjectives_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C310250") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindSubject()
                    BindLevel()
                    GridBind()
                    gvObjectives.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S" _
                                & " WHERE SBG_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                & " AND (SBG_DESCR LIKE 'BTEC%'" _
                                & " OR ISNULL(SBG_bBTEC,0)=1)" _
                                & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LVL_ID,LVL_DESCR FROM BTEC.BTEC_LEVEL_BSU_M WHERE LVL_BSU_ID='" + Session("SBSUID") + "'"
        If ddlSubject.SelectedItem.Text.ToUpper.Contains("ISLAMIC STUDIES") = False Then
            str_query += " AND ISNULL(LVL_bISL,'false')='FALSE'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "LVL_DESCR"
        ddlLevel.DataValueField = "LVL_ID"
        ddlLevel.DataBind()
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_DISPLAYORDER"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_WEIGHTAGE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_OBT_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_bTARGET"
        dt.Columns.Add(column)
        Return dt
    End Function

    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBJ_ID,OBJ_DISPLAYORDER,OBJ_DESCR,OBJ_WEIGHTAGE,ISNULL(OBJ_OBT_ID,0) AS OBJ_OBT_ID," _
                                & " ISNULL(OBJ_bTARGET,'FALSE') AS OBJ_bTARGET FROM " _
                                & " BTEC.BTEC_OBJECTIVES_M WHERE OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                                & " AND OBJ_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                                & " AND OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString
        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = .Item(2)
                dr.Item(3) = .Item(3)
                dr.Item(4) = "edit"
                dr.Item(5) = i.ToString
                dr.Item(6) = .Item(4)
                dr.Item(7) = .Item(5)
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 100 rows
        For i = 0 To 100 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = ""
            dr.Item(3) = ""
            dr.Item(4) = "add"
            dr.Item(5) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(6) = "0"
            dr.Item(7) = "false"
            dt.Rows.Add(dr)
        Next

        Session("dtObj") = dt



        gvObjectives.DataSource = dt
        gvObjectives.DataBind()

        gvObjectives.Columns(2).Visible = bShowSlno

        Dim ddlCategory As DropDownList
        Dim lblCatId As Label
        Dim li As ListItem

        str_query = "SELECT OBT_ID,OBT_DESCR FROM " _
                     & " BTEC.BTEC_OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                     & " AND OBT_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                     & " ORDER BY OBT_DESCR"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To gvObjectives.Rows.Count - 1
            lblCatId = gvObjectives.Rows(i).FindControl("lblCatId")
            ddlCategory = gvObjectives.Rows(i).FindControl("ddlCategory")
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "OBT_DESCR"
            ddlCategory.DataValueField = "OBT_ID"
            ddlCategory.DataBind()

            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlCategory.Items.Insert(0, li)

            If Not ddlCategory.Items.FindByValue(lblCatId.Text) Is Nothing Then
                ddlCategory.Items.FindByValue(lblCatId.Text).Selected = True
            End If

        Next


    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblObjId As Label
        Dim lblOrder As Label
        Dim txtOrder As TextBox
        Dim lblObjective As Label
        Dim lblWeightage As Label
        Dim txtWeightage As TextBox
        Dim txtObjective As TextBox
        Dim lblDelete As Label
        Dim lblCatId As Label
        Dim ddlCategory As DropDownList
        Dim chkTarget As CheckBox
        Dim lblTarget As Label
        Dim mode As String = ""

        For i = 0 To gvObjectives.Rows.Count - 1
            With gvObjectives.Rows(i)
                lblObjId = .FindControl("lblObjId")
                lblCatId = .FindControl("lblCatId")
                txtObjective = .FindControl("txtObjective")
                txtOrder = .FindControl("txtOrder")
                lblOrder = .FindControl("lblOrder")
                txtWeightage = .FindControl("txtWeightage")
                lblWeightage = .FindControl("lblWeightage")
                ddlCategory = .FindControl("ddlCategory")

                lblDelete = .FindControl("lblDelete")
                lblObjective = .FindControl("lblObjective")

                chkTarget = .FindControl("chkTarget")
                lblTarget = .FindControl("lblTarget")


                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblObjId.Text = "0" And txtObjective.Text <> "" Then
                    mode = "add"
                ElseIf lblObjective.Text <> txtObjective.Text Or lblOrder.Text <> txtOrder.Text Or txtWeightage.Text <> lblWeightage.Text Or lblCatId.Text <> ddlCategory.SelectedValue.ToString.ToString Or lblTarget.Text <> chkTarget.Checked.ToString Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then
                    str_query = "exec [BTEC].[BTEC_saveOBJECTIVES_M] " _
                   & " @OBJ_ID=" + lblObjId.Text + "," _
                   & " @OBJ_BSU_ID=" + Session("sbsuid") + "," _
                   & " @OBJ_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                   & " @OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString + "," _
                   & " @OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString + "," _
                   & " @OBJ_DESCR=N'" + txtObjective.Text.Replace("'", "''") + "'," _
                   & " @OBJ_WEIGHTAGE=" + Val(txtWeightage.Text).ToString + "," _
                   & " @OBJ_DISPLAYORDER=" + Val(txtOrder.Text).ToString + "," _
                   & " @OBJ_bTARGET='" + chkTarget.Checked.ToString + "'," _
                   & " @MODE='" + mode + "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvObjectives.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub UpLoadExcelFile()
        If uploadFile.HasFile Then
            Dim tempDir As String = HttpContext.Current.Server.MapPath("~/Curriculum/ReportDownloads/")
            'Dim tempDir As String = "~/Curriculum/ReportDownloads/"

            Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls" ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."
            Dim tempFileNameUsed As String = tempDir + tempFileName
            If uploadFile.HasFile Then
                If File.Exists(tempFileNameUsed) Then
                    File.Delete(tempFileNameUsed)
                End If
                uploadFile.SaveAs(tempFileNameUsed)
                Try
                    getdataExcel(tempFileNameUsed)
                    File.Delete(tempFileNameUsed)
                    lblError.Text = ""
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed"
                End Try
            End If
        End If
    End Sub

    Sub getdataExcel(ByVal filename As String)
        SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As ExcelFile = New ExcelFile

        Dim mObj As ExcelRowCollection

        Dim iRowRead As Boolean
        iRowRead = True

        ef.LoadXls(filename)
        Dim mRowObj As ExcelRow
        mObj = ef.Worksheets(0).Rows
        Dim iRow As Integer = 0

        Dim dt As DataTable = SetDataTable()
        Dim dr As DataRow
        Dim category As New ArrayList
        While iRowRead
            mRowObj = mObj(iRow)
            If mRowObj.Cells(0).Value Is Nothing Then
                Exit While
            End If
            If mRowObj.Cells(0).Value.ToString = "" Then
                Exit While
            End If

            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = mRowObj.Cells(0).Value.ToString
            dr.Item(3) = mRowObj.Cells(1).Value.ToString
            dr.Item(4) = "add"
            dr.Item(5) = iRow
            dr.Item(6) = "0"
            dr.Item(7) = "false"
            dt.Rows.Add(dr)
            iRow += 1

            If Not mRowObj.Cells(2).Value Is Nothing Then
                category.Add(mRowObj.Cells(2).Value)
            Else
                category.Add("--")
            End If
        End While


        Dim i As Integer
        Dim j As Integer = dt.Rows.Count
        For i = 0 To 100 - j
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = ""
            dr.Item(3) = ""
            dr.Item(4) = "add"
            dr.Item(5) = (j + i).ToString
            dr.Item(6) = "0"
            dr.Item(7) = "false"
            dt.Rows.Add(dr)
        Next

        gvObjectives.DataSource = dt
        gvObjectives.DataBind()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBT_ID,OBT_DESCR FROM " _
                  & " BTEC.BTEC_OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                  & " AND OBT_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                  & " ORDER BY OBT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim ddlCategory As DropDownList
        Dim lblCatId As Label
        Dim li As ListItem

        Dim k As Integer = 0
        Dim txtObjective As TextBox
        For k = 0 To gvObjectives.Rows.Count - 1
            ddlCategory = gvObjectives.Rows(k).FindControl("ddlCategory")
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "OBT_DESCR"
            ddlCategory.DataValueField = "OBT_ID"
            ddlCategory.DataBind()

            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlCategory.Items.Insert(0, li)

            txtObjective = gvObjectives.Rows(k).FindControl("txtObjective")

            If txtObjective.Text <> "" Then
                If Not ddlCategory.Items.FindByText(category.Item(k)) Is Nothing Then
                    ddlCategory.Items.FindByText(category.Item(k)).Selected = True
                End If
            End If
        Next

    End Sub

#End Region




    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindLevel()
        GridBind()
    End Sub



    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If Not uploadFile.HasFile Then
            lblError.Text = "Select Excel Sheet...!"
            Exit Sub
        End If
        If Not (uploadFile.FileName.EndsWith("xls") Or uploadFile.FileName.EndsWith("xlsx")) Then
            lblError.Text = "Invalid file type.. Only excel files are alowed.!"
            Exit Sub
        End If
        UpLoadExcelFile()

    End Sub



    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        GridBind()
    End Sub
End Class
