﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Collections.Generic
Imports System.Collections
Partial Class Curriculum_clmStudOptionApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100280") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    trApr1.Visible = False
                    trApr2.Visible = False
                    trGrd.Visible = False


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindOptions()


                    BindSuject_1()
                    BindOption_2()



                    trOption.Visible = False
                    trSubject.Visible = False

                    PopulateAcademicYear2()

                    PopulateSection()
                    gvStudent.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try


        End If



    End Sub



#Region "Private Methods"

    Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SCT_ID,SCT_DESCR FROM SECTION_M WHERE " _
                                  & " SCT_ACD_ID='" + Session("Current_ACD_ID") + "' AND SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_DESCR<>'TEMP' ORDER BY SCT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String) As DropDownList

        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm _
                                  & " AND ACD_ID IN(" + Session("CURRENT_ACD_ID") + "," + Session("NEXT_ACD_ID") + ")" _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        If Not ddlAcademicYear.Items.FindByValue(Session("NEXT_ACD_ID")) Is Nothing Then
            ddlAcademicYear.Items.FindByValue(Session("NEXT_ACD_ID")).Selected = True
        End If
        Return ddlAcademicYear
    End Function

    Public Sub PopulateAcademicYear2()
        ddlAcademicYear2.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + Session("clm") _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear2.DataSource = ds
        ddlAcademicYear2.DataTextField = "acy_descr"
        ddlAcademicYear2.DataValueField = "acd_id"
        ddlAcademicYear2.DataBind()


        ddlAcademicYear2.Items.FindByValue(ddlAcademicYear.Items(0).Value).Selected = True


        BindReportCard()
    End Sub


    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT CONVERT(VARCHAR(100),RPF_ID)+'|'+CONVERT(VARCHAR(100),RSD_ID)+'|'+CONVERT(VARCHAR(100),RSD_RSM_ID) AS RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " INNER JOIN RPT.REPORT_SETUP_D AS D ON B.RSM_ID=D.RSD_RSM_ID AND RSD_bPERFORMANCE_INDICATOR=1" _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear2.SelectedValue.ToString _
                                & " AND RSG_GRD_ID='" + hfGRD_ID.Value + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RPF_DESCR"
        ddlReportCard.DataValueField = "RPF_ID"
        ddlReportCard.DataBind()

        If ddlReportCard.Items.Count = 0 Then
            Dim li As New ListItem
            li.Text = "--"
            li.Value = "0|0|0"
            ddlReportCard.Items.Add(li)
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindOptions()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR,1  AS ID  FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString _
                                & "' AND SBG_STM_ID=1" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        str_query += " UNION ALL SELECT 0 AS OPT_ID,'TOTAL' AS OPT_DESCR,2 AS ID "
        str_query = "SELECT OPT_ID,OPT_DESCR FROM ( " + str_query + ")PP ORDER BY ID"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlOptions.DataSource = ds
        dlOptions.DataBind()

        hfTotalOptions.Value = dlOptions.Items.Count - 1
    End Sub

    Sub BindSubjectsOptionCount(ByVal opt_id As String, ByVal gvOptions As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim acd_id As String


        If opt_id <> 0 Then
            str_query = "SELECT DISTINCT SBG_DESCR, " _
                        & " (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S INNER JOIN STUDENT_M ON STU_ID=STR_STU_ID AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + Session("CURRENT_ACD_ID") + " WHERE STR_SBG_ID=A.SBG_ID AND STR_OPT_ID=" + opt_id + ") AS REQ_COUNT," _
                        & " (SELECT COUNT(SSD_STU_ID) FROM STUDENT_GROUPS_S  INNER JOIN STUDENT_M ON STU_ID=SSD_STU_ID AND STU_CURRSTATUS='EN'WHERE SSD_SBG_ID=A.SBG_ID AND SSD_OPT_ID=" + opt_id + ") AS ALT_COUNT" _
                        & " FROM SUBJECTS_GRADE_S AS A" _
                        & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                        & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                        & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + opt_id _
                        & " ORDER BY SBG_DESCR"
        Else
            str_query = "SELECT DISTINCT SBG_DESCR, " _
                 & " (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S INNER JOIN STUDENT_M ON STU_ID=STR_STU_ID AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + Session("CURRENT_ACD_ID") + " WHERE STR_SBG_ID=A.SBG_ID) AS REQ_COUNT," _
                 & " (SELECT COUNT(SSD_STU_ID) FROM STUDENT_GROUPS_S INNER JOIN STUDENT_M ON STU_ID=SSD_STU_ID AND STU_CURRSTATUS='EN' WHERE SSD_SBG_ID=A.SBG_ID ) AS ALT_COUNT" _
                 & " FROM SUBJECTS_GRADE_S AS A" _
                 & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                 & " AND SBG_STM_ID=1 " _
                 & " ORDER BY SBG_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOptions.DataSource = ds
        gvOptions.DataBind()
    End Sub
    Public Sub GetPrevGrade(ByVal vGRD_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER - 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            hfGRD_ID.Value = dr("GRD_ID")
        End While

    End Sub

    Sub HideGridColumns()
        Dim i As Integer
        If gvStudent.Rows.Count > 0 Then
            For i = 7 To 16
                gvStudent.Columns(i).Visible = False
            Next
        End If
    End Sub
    Sub GridBind()
        ViewState("slno") = 0

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        If txtRecords.Text <> "" Then
            str_query = "SELECT TOP " + txtRecords.Text + " STU_ID,STU_NO,STU_NAME,SCT_DESCR FROM VW_STUDENT_DETAILS AS A WHERE STU_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                 & " AND STU_STM_ID=1 AND STU_GRD_ID='" + hfGRD_ID.Value + "'"


        Else
            str_query = "SELECT STU_ID,STU_NO,STU_NAME,SCT_DESCR FROM VW_STUDENT_DETAILS AS A WHERE STU_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                 & " AND STU_STM_ID=1 AND STU_GRD_ID='" + hfGRD_ID.Value + "'"

        End If


        If chkShowPending.Checked = False Then
            str_query += " AND STU_ID IN(SELECT STR_STU_ID FROM STUDENT_OPTIONREQ_S WHERE STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            If hfOptionFilter.Value = "option1" Then
                str_query += " AND STR_OPT_ID=" + ddlOption1.SelectedValue.ToString
            ElseIf hfOptionFilter.Value = "option1" Then
                str_query += " AND STR_OPT_ID=" + ddlOption2.SelectedValue.ToString
            End If

            If hfSubjectFilter.Value = "subject1" Then
                str_query += " AND STR_SBG_ID=" + ddlSubject1.SelectedValue.ToString
            ElseIf hfSubjectFilter.Value = "subject2" Then
                str_query += " AND STR_SBG_ID=" + ddlSubject2.SelectedValue.ToString
            End If

            str_query += ")"

            If rdAlloted.Checked = True Then
                str_query += " AND (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S WHERE  STR_STU_ID=A.STU_ID AND STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND STR_APR_SBG_ID IS NOT NULL )>0"
            End If

            If rdNotAlloted.Checked = True Then
                'str_query += " AND STR_APR_SBG_ID IS NULL"
                str_query += " AND (SELECT COUNT(STR_STU_ID) FROM STUDENT_OPTIONREQ_S WHERE  STR_STU_ID=A.STU_ID AND STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "AND STR_APR_SBG_ID IS NOT NULL )=0"
            End If


        Else
            str_query += " AND STU_ID NOT IN(SELECT STR_STU_ID FROM STUDENT_OPTIONREQ_S WHERE STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + ")"
        End If

        If txtStuName.Text <> "" Then
            str_query += " AND STU_NAME LIKE '%" + txtStuName.Text + "%'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_SCT_ID='" + ddlSection.SelectedValue.ToString + "'"
        End If

        str_query += " ORDER BY STU_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudent.DataSource = ds
        gvStudent.DataBind()
        HideGridColumns()

        Dim i As Integer
        Dim lblOption As LinkButton
        Dim lblOptId As Label

        Dim lblOpt_H As Label
        Dim lblOptId_H As Label
        If ds.Tables(0).Rows.Count > 0 Then
            If hfOptionFilter.Value = "all" And hfSubjectFilter.Value = "all" Then
                For i = 0 To dlOptions.Items.Count - 2
                    lblOption = dlOptions.Items(i).FindControl("lblOption")
                    lblOptId = dlOptions.Items(i).FindControl("lblOptId")
                    lblOpt_H = gvStudent.HeaderRow.Cells(7 + i).FindControl("lblOpt_H" + CStr(i + 1))
                    lblOptId_H = gvStudent.HeaderRow.Cells(7 + i).FindControl("lblOptId_H" + CStr(i + 1))
                    lblOpt_H.Text = lblOption.Text
                    lblOptId_H.Text = lblOptId.Text
                    gvStudent.Columns(7 + i).Visible = True
                Next

            ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
                For i = 1 To ddlOption1.Items.Count - 1
                    lblOpt_H = gvStudent.HeaderRow.Cells(6 + i).FindControl("lblOpt_H" + CStr(i))
                    lblOptId_H = gvStudent.HeaderRow.Cells(6 + i).FindControl("lblOptId_H" + CStr(i))
                    lblOpt_H.Text = ddlOption1.Items(i).Text
                    lblOptId_H.Text = ddlOption1.Items(i).Value
                    gvStudent.Columns(6 + i).Visible = True
                Next
            ElseIf hfOptionFilter.Value = "option1" Then
                lblOpt_H = gvStudent.HeaderRow.FindControl("lblOpt_H1")
                lblOptId_H = gvStudent.HeaderRow.FindControl("lblOptId_H1")
                lblOpt_H.Text = ddlOption1.SelectedItem.Text
                lblOptId_H.Text = ddlOption1.SelectedValue.ToString
                gvStudent.Columns(7).Visible = True
            ElseIf hfOptionFilter.Value = "option2" Then
                lblOpt_H = gvStudent.HeaderRow.FindControl("lblOpt_H1")
                lblOptId_H = gvStudent.HeaderRow.FindControl("lblOptId_H1")
                lblOpt_H.Text = ddlOption2.SelectedItem.Text
                lblOptId_H.Text = ddlOption2.SelectedValue.ToString
                gvStudent.Columns(7).Visible = True
            End If
        End If
    End Sub

    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(1)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OPT_DESCR"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_DESCR"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt


    End Function


    Sub BindStudentOptions(ByVal gRow As GridViewRow)

        Dim lblStuId As Label = gRow.FindControl("lblStuId")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_DESCR,SBG_DESCR FROM OPTIONS_M AS A " _
                                & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.OPT_ID=B.SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGO_SBG_ID=C.SBG_ID" _
                                & " INNER JOIN STUDENT_OPTIONREQ_S AS D ON C.SBG_ID=D.STR_SBG_ID AND A.OPT_ID=D.STR_OPT_ID" _
                                & " WHERE STR_STU_ID=" + lblStuId.Text + " AND STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If hfOptionFilter.Value = "option1" Then
            str_query += " AND STR_OPT_ID=" + ddlOption1.SelectedValue.ToString
        ElseIf hfOptionFilter.Value = "option2" Then
            str_query += " AND STR_OPT_ID=" + ddlOption2.SelectedValue.ToString
        End If

        If hfSubjectFilter.Value = "subject1" Then
            str_query += " AND STR_SBG_ID=" + ddlSubject1.SelectedValue.ToString
        ElseIf hfSubjectFilter.Value = "subject2" Then
            str_query += " AND STR_SBG_ID=" + ddlSubject2.SelectedValue.ToString
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim dr As DataRow
        Dim dt As DataTable = SetDataTable()
        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                dr = dt.NewRow
                dr.Item(0) = .Rows(i).Item(0)
                dr.Item(1) = .Rows(i).Item(1)
                dt.Rows.Add(dr)
            Next
        End With

        Dim lblOption As LinkButton
        Dim lblOpt As Label
        Dim key As Object

        Dim row As DataRow

        If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
            For i = 0 To dlOptions.Items.Count - 1
                lblOption = dlOptions.Items(i).FindControl("lblOption")
                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                key = lblOption.Text
                row = dt.Rows.Find(key)
                If Not row Is Nothing Then
                    lblOpt.Text = row.Item(1)
                Else
                    lblOpt.Text = "--"
                End If
            Next
        ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
            For i = 1 To ddlOption1.Items.Count - 1
                lblOpt = gRow.FindControl("lblOpt" + CStr(i))
                key = ddlOption1.Items(i).Text
                row = dt.Rows.Find(key)
                If Not row Is Nothing Then
                    lblOpt.Text = row.Item(1)
                Else
                    lblOpt.Text = "--"
                End If
            Next
        ElseIf hfOptionFilter.Value = "option1" Then
            key = ddlOption1.SelectedItem.Text
            lblOpt = gRow.FindControl("lblOpt1")
            row = dt.Rows.Find(key)
            If Not row Is Nothing Then
                lblOpt.Text = row.Item(1)
            Else
                lblOpt.Text = "--"
            End If
        ElseIf hfOptionFilter.Value = "option2" Then
            key = ddlOption2.SelectedItem.Text
            lblOpt = gRow.FindControl("lblOpt1")
            row = dt.Rows.Find(key)
            If Not row Is Nothing Then
                lblOpt.Text = row.Item(1)
            Else
                lblOpt.Text = "--"
            End If
        End If

    End Sub



    Function BindOptionDropDownList(ByVal ddlOption As DropDownList, ByVal opt_id As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                      & " FROM SUBJECTS_GRADE_S AS A" _
                      & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                      & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + opt_id _
                      & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOption.DataSource = ds
        ddlOption.DataTextField = "SBG_DESCR"
        ddlOption.DataValueField = "SBG_ID"
        ddlOption.DataBind()


        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0

        ddlOption.Items.Insert(0, li)

        Dim i As Integer

        For i = 0 To ddlOption.Items.Count - 1
            ddlOption.Items(i).Attributes.Add("title", ddlOption.Items(i).Text)
        Next


        ddlOption.Font.Size = FontSize.Medium
        Return ddlOption
    End Function


    '**Edit Grid Options
    Sub EditOptions(ByVal gRow As GridViewRow)
        Dim lblOption As LinkButton
        Dim lblOptId As Label
        Dim lblOpt As Label
        Dim ddlOpt As DropDownList

        Dim i As Integer
        If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
            For i = 0 To dlOptions.Items.Count - 1
                lblOption = dlOptions.Items(i).FindControl("lblOption")
                lblOptId = dlOptions.Items(i).FindControl("lblOptId")


                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i + 1))

                ddlOpt = BindOptionDropDownList(ddlOpt, lblOptId.Text)

                If lblOpt.Text <> "" Then
                    If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                        ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                    End If
                End If

                ddlOpt.Visible = True
                lblOpt.Visible = False
            Next
        ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
            For i = 1 To ddlOption1.Items.Count - 1
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i))
                lblOpt = gRow.FindControl("lblOpt" + CStr(i))
                ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption1.Items(i).Value)
                If lblOpt.Text <> "" Then
                    If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                        ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                    End If
                End If
                ddlOpt.Visible = True
                lblOpt.Visible = False
            Next
        ElseIf hfOptionFilter.Value = "option1" Then
            lblOpt = gRow.FindControl("lblOpt1")
            ddlOpt = gRow.FindControl("ddlOption1")
            ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption1.SelectedValue.ToString)
            If lblOpt.Text <> "" Then
                If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                    ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                End If
            End If
            ddlOpt.Visible = True
            lblOpt.Visible = False
        ElseIf hfOptionFilter.Value = "option2" Then
            lblOpt = gRow.FindControl("lblOpt1")
            ddlOpt = gRow.FindControl("ddlOption1")
            ddlOpt = BindOptionDropDownList(ddlOpt, ddlOption2.SelectedValue.ToString)
            If lblOpt.Text <> "" Then
                If Not ddlOpt.Items.FindByText(lblOpt.Text) Is Nothing Then
                    ddlOpt.Items.FindByText(lblOpt.Text).Selected = True
                End If
            End If
            ddlOpt.Visible = True
            lblOpt.Visible = False
        End If

    End Sub

    Sub UpdateOptions(ByVal gRow As GridViewRow, ByVal lblEdit As LinkButton, ByVal chkSelect As CheckBox)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i, j As Integer
        Dim str_query As String

        Dim lblOptId As Label
        Dim ddlOpt As DropDownList
        Dim lblOpt As Label


        Dim subjects As New ArrayList
        Dim lblStuId As Label = gRow.FindControl("lblStuId")
        Dim transaction As SqlTransaction
        Dim ids As New ArrayList
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection

            For i = 0 To dlOptions.Items.Count - 1
                lblOptId = dlOptions.Items(i).FindControl("lblOptId")

                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                ddlOpt = gRow.FindControl("ddlOption" + CStr(i + 1))

                ids.Add(CStr(i + 1))

                If ddlOpt.SelectedValue.ToString <> 0 Then
                    If subjects.Contains(ddlOpt.SelectedValue.ToString) Then

                        lblGrdError.Text = "Options Should be Unique"
                        lblEdit.Text = "Update"
                        chkSelect.Enabled = False
                        transaction.Rollback()

                        For j = 0 To ids.Count - 1
                            lblOpt = gRow.FindControl("lblOpt" + ids(j))
                            ddlOpt = gRow.FindControl("ddlOption" + ids(j))
                            lblOpt.Visible = False
                            ddlOpt.Visible = True
                        Next

                        Exit Sub
                    Else
                        subjects.Add(ddlOpt.SelectedValue.ToString)
                    End If
                End If

                Try
                    transaction = conn.BeginTransaction("SampleTransaction")
                    str_query = "exec saveSTUDENT_OPTIONREQ " _
                               & lblStuId.Text + "," _
                               & ddlAcademicYear.SelectedValue.ToString + "," _
                               & "'" + ddlGrade.SelectedValue.ToString + "'," _
                               & lblOptId.Text + "," _
                               & ddlOpt.SelectedValue.ToString + "," _
                               & "0," _
                               & "'" + Session("username") + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    transaction.Commit()

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try


                lblOpt.Text = ddlOpt.SelectedItem.Text
                lblOpt.Visible = True
                ddlOpt.Visible = False
            Next


            lblGrdError.Text = ""

        End Using
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)
        Dim lblSlNo As Label = TryCast(sender.FindControl("lblSlNo"), Label)
        Dim selectedRow As GridViewRow = DirectCast(gvStudent.Rows(CInt(lblSlNo.Text) - 1), GridViewRow)
        Dim chkSelect As CheckBox = TryCast(sender.FindControl("chkSelect"), CheckBox)
        If lblEdit.Text = "Edit" Then
            EditOptions(selectedRow)
            lblEdit.Text = "Update"
            chkSelect.Enabled = False
        ElseIf lblEdit.Text = "Update" Then
            lblEdit.Text = "Edit"
            chkSelect.Enabled = True
            UpdateOptions(selectedRow, lblEdit, chkSelect)
        End If
    End Sub

    '***Filter By Subject and Option

    Sub BindSuject_1()
        ddlSubject1.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE " _
                               & " SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " AND SBG_bOPTIONAL=1" _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        ddlSubject1.DataSource = ds
        ddlSubject1.DataTextField = "SBG_DESCR"
        ddlSubject1.DataValueField = "SBG_ID"
        ddlSubject1.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlSubject1.Items.Insert(0, li)


        ddlOption1.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlOption1.Items.Add(li)
    End Sub

    Sub BindOption_1()

        ddlOption1.Items.Clear()
        Dim li As New ListItem
        If ddlSubject1.SelectedValue <> "0" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR  FROM " _
                               & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                               & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                               & " WHERE SGO_SBG_ID=" + ddlSubject1.SelectedValue.ToString _
                               & " ORDER BY OPT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlOption1.DataSource = ds
            ddlOption1.DataTextField = "OPT_DESCR"
            ddlOption1.DataValueField = "OPT_ID"
            ddlOption1.DataBind()

            li.Text = "ALL"
            li.Value = 0
            ddlOption1.Items.Insert(0, li)

        Else

            ddlOption1.Items.Clear()
            li = New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlOption1.Items.Add(li)

        End If
    End Sub

    '*******************************

    '***Filter By Option and Subject

    Sub BindOption_2()
        ddlOption2.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR  FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString _
                                & "' AND SBG_STM_ID=1" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY OPT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlOption2.DataSource = ds
        ddlOption2.DataTextField = "OPT_DESCR"
        ddlOption2.DataValueField = "OPT_ID"
        ddlOption2.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlOption2.Items.Insert(0, li)


        ddlSubject2.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject2.Items.Add(li)


    End Sub

    Sub BindSubject2()
        ddlSubject2.Items.Clear()
        Dim li As New ListItem
        If ddlOption2.SelectedValue <> 0 Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID " _
                      & " FROM SUBJECTS_GRADE_S AS A" _
                      & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + Session("Next_ACD_ID") _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                      & " AND SBG_STM_ID=1 AND SGO_OPT_ID=" + ddlOption2.SelectedValue.ToString _
                      & " ORDER BY SBG_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSubject2.DataSource = ds
            ddlSubject2.DataTextField = "SBG_DESCR"
            ddlSubject2.DataValueField = "SBG_ID"
            ddlSubject2.DataBind()

            li.Text = "ALL"
            li.Value = 0
            ddlSubject2.Items.Insert(0, li)

        Else
            li.Text = "ALL"
            li.Value = 0
            ddlSubject2.Items.Add(li)
        End If



    End Sub

    '*******************************

    Sub BindChoice2(ByVal gvChoice2 As GridView, ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_DESCR,ISNULL(SBG_DESCR,'--') AS SBG_DESCR FROM OPTIONS_M AS A " _
                                & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.OPT_ID=B.SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SGO_SBG_ID=C.SBG_ID" _
                                & " INNER JOIN STUDENT_OPTIONREQ_S AS D ON C.SBG_ID=D.STR_CHOICE2 AND A.OPT_ID=D.STR_OPT_ID" _
                                & " WHERE STR_STU_ID=" + stu_id + " AND STR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvChoice2.DataSource = ds
        gvChoice2.DataBind()
    End Sub


    Sub SaveStudentOptions()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim j As Integer
        Dim transaction As SqlTransaction
        Dim chkSelect As CheckBox
        Dim lblStuId As Label

        Dim lblSubject As Label
        Dim lblOptId As Label
        Dim str_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection

            For i = 0 To gvStudent.Rows.Count - 1
                lblStuId = gvStudent.Rows(i).FindControl("lblStuId")
                chkSelect = gvStudent.Rows(i).FindControl("chkSelect")

                If chkSelect.Checked = True Then
                    Try
                        For j = 0 To dlOptions.Items.Count - 2
                            lblSubject = gvStudent.Rows(i).FindControl("lblOpt" + CStr(j + 1))
                            lblOptId = dlOptions.Items(j).FindControl("lblOptID")
                            If lblSubject.Text <> "--" Then

                                transaction = conn.BeginTransaction("SampleTransaction")

                                str_query = "exec saveSTUDENTOPTION_APPROVE " _
                                           & "@STU_ID=" + lblStuId.Text + "," _
                                           & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                           & "@GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                           & "@SBG_DESCR='" + lblSubject.Text + "'," _
                                           & "@OPT_ID=" + lblOptId.Text + "," _
                                           & "@USER='" + Session("susr_name") + "'"
                                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                                transaction.Commit()
                            End If
                        Next
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                        UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Saved"
                        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    End Try
                End If
            Next
        End Using
    End Sub


    'If the student is below the min level in option subjects mapping then show in red color
    Sub BindAssessmentLevel(ByVal gRow As GridViewRow)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim lblOpt As Label
        Dim sbg_ids As String
        Dim i As Integer
        Dim lblStuId1 As Label = gRow.FindControl("lblStuId")
        If hfSubjectFilter.Value = "all" And hfOptionFilter.Value = "all" Then
            For i = 0 To dlOptions.Items.Count - 1
                lblOpt = gRow.FindControl("lblOpt" + CStr(i + 1))
                If lblOpt.Text <> "--" Then
                    If sbg_ids <> "" Then
                        sbg_ids += ","
                    End If
                    sbg_ids += ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
                End If
            Next
        ElseIf hfSubjectFilter.Value = "subject1" And hfOptionFilter.Value = "all" Then
            For i = 1 To ddlOption1.Items.Count - 1
                lblOpt = gRow.FindControl("lblOpt" + CStr(i))
                If lblOpt.Text <> "--" Then
                    If sbg_ids <> "" Then
                        sbg_ids += ","
                    End If
                    sbg_ids += ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
                End If
            Next
        ElseIf hfOptionFilter.Value = "option1" Then
            lblOpt = gRow.FindControl("lblOpt1")
            If lblOpt.Text <> "--" Then
                sbg_ids = ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
            End If
        ElseIf hfOptionFilter.Value = "option2" Then
            lblOpt = gRow.FindControl("lblOpt1")
            If lblOpt.Text <> "--" Then
                sbg_ids = ddlSubject1.Items.FindByText(lblOpt.Text).Value.ToString
            End If
        End If

        Dim rpf As String() = ddlReportCard.SelectedValue.ToString.Split("|")
        Dim lblStuId As Label
        lblStuId = gRow.FindControl("lblStuId")

        If ddlAcademicYear2.SelectedValue.ToString = Session("Current_ACD_ID") Then
            str_query = "SELECT SBG_DESCR,RST_COMMENTS,ISNULL(RGP_VALUE,0) RGP_VALUE,SSM_MINLEVELVALUE FROM RPT.REPORT_STUDENT_S AS A" _
                       & " INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.RST_COMMENTS=B.RGP_GRADE AND A.RST_SBG_ID=B.RGP_SBG_ID" _
                       & " INNER JOIN OPTION_SUBJECTMAPPING_S C ON A.RST_SBG_ID=C.SSM_MAPPED_SBG_ID " _
                       & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.RST_SBG_ID=D.SBG_ID" _
                       & " WHERE SSM_SBG_ID IN(" + sbg_ids + ") AND RST_RPF_ID=" + rpf(0) _
                       & " AND RST_RSD_ID=" + rpf(1) + " AND RGP_RSM_ID=" + rpf(2) _
                       & " AND RST_STU_ID=" + lblStuId.Text
        Else
            str_query = "SELECT SBG_DESCR,RST_COMMENTS,ISNULL(RGP_VALUE,0) RGP_VALUE,SSM_MINLEVELVALUE FROM RPT.REPORT_STUDENT_PREVYEARS AS A" _
                            & " INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.RST_COMMENTS=B.RGP_GRADE AND A.RST_SBG_ID=B.RGP_SBG_ID" _
                            & " INNER JOIN OPTION_SUBJECTMAPPING_S C ON A.RST_SBG_ID=C.SSM_MAPPED_SBG_ID " _
                            & " INNER JOIN SUBJECTS_GRADE_S AS D ON A.RST_SBG_ID=D.SBG_ID" _
                            & " WHERE SSM_SBG_ID IN(" + sbg_ids + ") AND RST_RPF_ID=" + rpf(0) _
                            & " AND RST_RSD_ID=" + rpf(1) + " AND RGP_RSM_ID=" + rpf(2) _
                            & " AND RST_STU_ID=" + lblStuId.Text
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim str As String = ""

        Dim bMin As Boolean = False

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                
                If .Item("SSM_MINLEVELVALUE") > .Item("RGP_VALUE") Then
                    If str.Contains("<font color=red>" + .Item("SBG_DESCR") + " : " + .Item("RST_COMMENTS") + "</font>") = False Then
                        If str <> "" Then
                            str += "<br>"
                        End If
                        str += "<font color=red>" + .Item("SBG_DESCR") + " : " + .Item("RST_COMMENTS") + "</font>"
                        bMin = True
                    End If
                Else
                    If str.Contains(.Item("SBG_DESCR") + " : " + .Item("RST_COMMENTS")) = False Then
                        If str <> "" Then
                            str += "<br>"
                        End If
                        str += .Item("SBG_DESCR") + " : " + .Item("RST_COMMENTS")
                    End If
                End If
            End With
        Next

        Dim ltLevel As Literal
        ltLevel = gRow.FindControl("ltLevel")

        ltLevel.Text = str

        If bMin = True Then
            gRow.BackColor = Drawing.Color.Cornsilk
        End If
    End Sub

#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If
        BindOptions()
        BindSuject_1()
        BindOption_2()
        PopulateAcademicYear2()
        PopulateSection()
    End Sub

    Protected Sub dlOptions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlOptions.ItemDataBound
        Dim lblOptId As Label
        Dim gvOptions As GridView
        lblOptId = e.Item.FindControl("lblOptId")
        gvOptions = e.Item.FindControl("gvOptions")
        If lblOptId.Text <> "" Then
            BindSubjectsOptionCount(lblOptId.Text, gvOptions)
        End If
    End Sub

    Protected Sub rdAllOption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAllOption.CheckedChanged
        If rdAllOption.Checked = True Then
            trOption.Visible = False
            trSubject.Visible = False            
        End If
    End Sub

    Protected Sub rdOption_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOption.CheckedChanged
        trSubject.Visible = False
        trOption.Visible = True
    End Sub

    Protected Sub rdSubject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdSubject.CheckedChanged
        trSubject.Visible = True
        trOption.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If ddlSubject1.SelectedValue <> 0 And rdSubject.Checked = True Then
            hfSubjectFilter.Value = "subject1"
        ElseIf ddlSubject2.SelectedValue <> 0 And rdOption.Checked = True Then
            hfSubjectFilter.Value = "subject2"
        Else
            hfSubjectFilter.Value = "all"
        End If

        If ddlOption1.SelectedValue <> 0 And rdSubject.Checked = True Then
            hfOptionFilter.Value = "option1"
        ElseIf ddlOption2.SelectedValue <> 0 And rdOption.Checked = True Then
            hfOptionFilter.Value = "option2"
        Else
            hfOptionFilter.Value = "all"
        End If

        GridBind()
        trApr1.Visible = True
        trApr2.Visible = True
        trGrd.Visible = True
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If

        BindOptions()
        BindSuject_1()
        BindOption_2()

        PopulateAcademicYear2()
        PopulateSection()
    End Sub



    Protected Sub gvStudent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudent.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            lblStuId = e.Row.FindControl("lblStuId")
            If chkShowPending.Checked = False Then
                BindStudentOptions(e.Row)
                BindAssessmentLevel(e.Row)
            End If
        End If
    End Sub


    Protected Sub ddlSubject1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject1.SelectedIndexChanged
        BindOption_1()
    End Sub

    Protected Sub ddlOption2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOption2.SelectedIndexChanged
        BindSubject2()
    End Sub

    Protected Sub btnApprove1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove1.Click
        SaveStudentOptions()
        GridBind()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SaveStudentOptions()
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear2.SelectedIndexChanged
        BindReportCard()
    End Sub

  
End Class
