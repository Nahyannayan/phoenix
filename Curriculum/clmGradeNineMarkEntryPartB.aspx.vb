﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmGradeNineMarkEntryPartB
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            ' Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330345") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                GetACD_ID()
                BindSection()
                GetReportCardInfo()
                BindSubject()
                GridBind()
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub

    'THIS PAGE IS TO ENTER THE GRADE NINE FINAM MARKS OF 2009-2010 TO BE SENT TO THE BOARD

#Region "Private methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub GetACD_ID()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_ID FROM ACADEMICYEAR_D " _
                              & " INNER JOIN ACADEMICYEAR_M ON ACY_ID=ACD_ACY_ID" _
                              & " WHERE ACD_BSU_ID='" + Session("SBSUID") + "'" _
                              & " AND ACY_DESCR='" + Trim(Session("AcadYear_Descr")) + "'"
        hfACD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + hfACD_ID.Value _
                                & " AND SCT_GRD_ID='09' ORDER BY SCT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + hfACD_ID.Value _
                                & " AND SBG_GRD_ID='09' AND SBG_bMAJOR=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub GetReportCardInfo()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_ID,RPF_RSM_ID FROM RPT.REPORT_PRINTEDFOR_M " _
                                & " INNER JOIN RPT.REPORT_SETUP_M ON RPF_RSM_ID=RSM_ID WHERE RSM_ACD_ID=" + hfACD_ID.Value _
                                & " AND RPF_DESCR='FINAL REPORT'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hfRPF_ID.Value = ds.Tables(0).Rows(0).Item(0)
            hfRSM_ID.Value = ds.Tables(0).Rows(0).Item(1)
        End If

        str_query = "SELECT isnull(RSD_HEADER,0),isnull(RSD_ID,0) FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + hfRSM_ID.Value
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                If .Item(0).ToString.ToUpper = "TOTAL" Then
                    hfRSD_TOTAL.Value = .Item(1)
                End If
            End With
        Next

    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                              & " +' '+ISNULL(STU_LASTNAME,'')) FROM STUDENT_M AS A " _
                              & " INNER JOIN OASIS..STUDENT_PROMO_S AS B ON A.STU_ID=B.STP_STU_ID" _
                              & " AND STP_SCT_ID=" + ddlSection.SelectedValue.ToString _
                              & " AND STP_ACD_ID=" + hfACD_ID.Value _
                              & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub


    Sub BindMarks(ByVal gRow As GridViewRow)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim lblStuId As Label = gRow.FindControl("lblStuId")
        Dim ddlTotalGrade As DropDownList = gRow.FindControl("ddlTotalGrade")

        Dim str_query As String = "SELECT FPR_RSD_ID,FPR_MARKS,FPR_GRADING FROM RPT.FINALPROCESS_REPORT_PREVYEARS " _
                              & " WHERE FPR_STU_ID=" + lblStuId.Text + " AND FPR_SBG_ID='" + ddlSubject.SelectedValue.ToString _
                              & "' AND FPR_RPF_ID=" + hfRPF_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                If .Item(0) = hfRSD_TOTAL.Value Then
                    If Not ddlTotalGrade.Items.FindByValue(.Item(2)) Is Nothing Then
                        ddlTotalGrade.Items.FindByValue(.Item(2)).Selected = True
                    End If
                End If
            End With
        Next

    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        Dim gRow As GridViewRow

        For Each gRow In gvStud.Rows

            Dim lblStuId As Label = gRow.FindControl("lblStuId")
            Dim ddlTotalGrade As DropDownList = gRow.FindControl("ddlTotalGrade")


            str_query = "exec saveCBSEGRADENINEMARKSPARTB" _
                          & " @STU_ID=" + lblStuId.Text + "," _
                          & " @RPF_ID=" + hfRPF_ID.Value + "," _
                          & " @RSD_TOTAL=" + hfRSD_TOTAL.Value + "," _
                          & " @SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                          & " @ACD_ID=" + hfACD_ID.Value + "," _
                          & " @SCT_ID=" + ddlSection.SelectedValue.ToString + "," _
                          & " @GRADING_TOTAL='" + ddlTotalGrade.SelectedValue.ToString + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub
#End Region

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            BindMarks(e.Row)
        End If
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
    End Sub
End Class
