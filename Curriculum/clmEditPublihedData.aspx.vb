Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Imports System.Collections
Imports Telerik.Web.UI
Imports System.Xml
Imports System.Collections.Generic


Partial Class Curriculum_clmEditPublihedData
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300075") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSection()
                    'updateSubjectURL()
                    'updateStudentURL()
                    BindTerm()
                    BindSubjectCombo()
                    BindStudentCombo()
                    tblStudent.Rows(5).Visible = False
                    tblStudent.Rows(6).Visible = False
                    tblStudent.Rows(7).Visible = False
                    tblStudent.Rows(8).Visible = False
                    tblStudent.Rows(9).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID,RSG_DISPLAYORDER " _
                                & " FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS D ON A.GRM_STM_ID=D.STM_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString


        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))


        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "' and GSA_ACD_ID=" & ddlAcademicYear.SelectedValue.ToString & " )), '|')))"
        End If

        str_query += " ORDER BY RSG_DISPLAYORDER "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Private Sub PopulateSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + grade(0) + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)

    End Sub
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND ISNULL(RSM_bFINALREPORT,'FALSE')='FALSE' ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                               & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub updateSubjectURL()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + grade(0) + "&stmid=" + grade(1)
    End Sub

    Sub updateStudentURL()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        hfStudentUrl.Value = "../Curriculum/clmShowStudents.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + grade(0) + "&stmid=" + grade(1) + "&sctid=" + ddlSection.SelectedValue.ToString
    End Sub
    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub

    Sub BindAssesments()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CAS_ID,CAS_DESC,CASE ISNULL(STA_bATTENDED,'P') WHEN 'P' THEN REPLACE(CONVERT(VARCHAR(100),STA_MARK),'.00','')" _
                                 & " WHEN 'A' THEN 'Ab' ELSE STA_bATTENDED END" _
                                 & " AS STA_MARK,CASE ISNULL(STA_bATTENDED,'P') WHEN 'P' THEN STA_GRADE WHEN 'A' THEN 'Ab' ELSE STA_bATTENDED END AS STA_GRADE," _
                                 & " CASE ISNULL(STA_bATTENDED,'P') WHEN 'P' THEN 'TRUE' ELSE 'FALSE' END AS ENABLE,STA_ID" _
                                 & " FROM ACT.ACTIVITY_D AS A " _
                                 & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS F ON A.CAD_ID=F.CAS_CAD_ID AND CAS_SBG_ID='" + GetCheckedSubject() + "'" _
                                 & " INNER JOIN ACT.STUDENT_ACTIVITY AS B ON F.CAS_ID=B.STA_CAS_ID  AND STA_SBG_ID='" + GetCheckedSubject() + "'" _
                                 & " INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON A.CAD_ID=C.RRD_CAD_ID   " _
                                 & " INNER JOIN RPT.REPORT_RULE_M  AS E ON C.RRD_RRM_ID=E.RRM_ID" _
                                 & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS G ON B.STA_STU_ID=G.RPP_STU_ID AND RPP_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                                 & " WHERE STA_bCOUNTED='TRUE' AND STA_bACTIVE='TRUE' AND RRM_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND RRM_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString _
                                 & " AND RRM_SBG_ID='" + GetCheckedSubject() + "'" _
                                 & " AND STA_STU_ID='" + GetCheckedStudent() + "'" _
                                 & " AND STA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvActivities.DataSource = ds
        gvActivities.DataBind()

    End Sub

    Sub UpdateMarks(ByVal sta_id As String, ByVal mark As String, ByVal grade As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "ACT.updatePUBLISHEDDATA " _
                                & sta_id + "," _
                                & IIf(mark = "", "0", mark) + "," _
                                & "'" + grade + "'," _
                                & "'" + Session("sUsr_name") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_HEADER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_MARK"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_GRADE"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub GetProcessedMarks()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Dim str_query As String = "EXEC RPT.getONESTUDENT_PROCESS " _
                                & hfRSM_ID.Value + "," _
                                & hfRPF_ID.Value + "," _
                                & hfACD_ID.Value + "," _
                                & "'" + grade(0) + "'," _
                                & GetCheckedStudent() + "," _
                                & GetCheckedSubject() + "," _
                                & hfTRM_ID.Value

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim dt As DataTable = SetDataTable()
        Dim dr As DataRow

        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetValue(0)
            dr.Item(1) = reader.GetString(1)
            dr.Item(2) = reader.GetValue(2).ToString.Replace(".00", "")
            dr.Item(3) = reader.GetString(3)
            dt.Rows.Add(dr)
        End While
        reader.Close()
        gvHeader.DataSource = dt
        gvHeader.DataBind()

    End Sub

    Sub SaveProcessedMarks()
        Dim i As Integer
        Dim str_query As String
        Dim lblRsdId As Label
        Dim lblMark As Label
        Dim lblGrade As Label
        Dim txtMark As TextBox
        Dim txtGrade As TextBox
        Dim transaction As SqlTransaction
        Dim mark As String
        Dim att As String
        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                For i = 0 To gvHeader.Rows.Count - 1
                    With gvHeader.Rows(i)
                        lblRsdId = .FindControl("lblRsdId")
                        lblMark = .FindControl("lblMark")
                        lblGrade = .FindControl("lblGrade")
                        txtMark = .FindControl("txtMark")
                        txtGrade = .FindControl("txtGrade")
                    End With

                    If IsNumeric(txtMark.Text) = True Then
                        mark = txtMark.Text
                        att = "NULL"
                    Else
                        mark = "0"
                        If txtMark.Text <> "" Then
                            att = "'" + txtMark.Text + "'"
                        Else
                            att = "NULL"
                        End If
                    End If
                    str_query = "RPT.savePROCESSEDMARKS " _
                                & GetCheckedStudent() + "," _
                                & lblRsdId.Text + "," _
                                & hfRPF_ID.Value + "," _
                                & hfACD_ID.Value + "," _
                                & "'" + grade(0) + "'," _
                                & GetCheckedSubject() + "," _
                                & hfTRM_ID.Value + "," _
                                & IIf(lblMark.Text = "", "0", lblMark.Text) + "," _
                                & mark + "," _
                                & "'" + lblGrade.Text + "'," _
                                & "'" + txtGrade.Text + "'," _
                                & att + "," _
                                & "'" + Session("sUsr_name") + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"



            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

    End Sub

    Sub GetProcessRules()
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try

            sqlstr = " SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString + " AND RSD_bDIRECTENTRY='FALSE' ORDER BY RSD_DISPLAYORDER"

            reader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlstr)

            While reader.Read
                sqlstr = " SELECT CAM_ID,GRADE_SLAB,RULES,OPT,WEIGHT FROM (SELECT RSS_CAM_ID AS CAM_ID," & _
                         " GRADE_SLAB=(SELECT GSM_DESC FROM ACT.GRADING_SLAB_M WHERE GSM_SLAB_ID=(SELECT RRM_GSM_SLAB_ID FROM RPT.REPORT_RULE_M WHERE RRM_ID=A.RSS_RRM_ID ))," & _
                         " RULES=RSS_OPRT_DISPLAY+'('+(select STUFF((SELECT ',' + CASE A.RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN " & _
                         " convert(varchar(100),RRD_WEIGHTAGE)+'% of '+convert(varchar(100),CAD_DESC) ELSE convert(varchar(100),CAD_DESC) END " & _
                         " from RPT.REPORT_SCHEDULE_D AS P INNER JOIN ACT.ACTIVITY_D AS Q ON P.RRD_CAD_ID=Q.CAD_ID " & _
                         " where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))+')', " & _
                         " OPT=CASE RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN 'WT' ELSE RSS_OPRT_DISPLAY END, " & _
                         " WEIGHT=RSS_WEIGHTAGE   FROM RPT.REPORT_SCHEDULE_M AS A WHERE RSS_RRM_ID= (SELECT     RRM_ID FROM  RPT.REPORT_RULE_M " & _
                         " where RRM_SBG_ID='" & GetCheckedSubject() & "' and RRM_ACD_ID='" & ddlAcademicYear.SelectedValue.ToString & "' and RRM_GRD_ID='" & grade(0) & "' and  RRM_RPF_ID='" & ddlPrintedFor.SelectedValue.ToString & "' and RRM_RSD_ID='" & reader.GetValue(0) & "' AND " & _
                         " isnull(RRM_TRM_ID,0)='" & ddlTerm.SelectedValue.ToString & "'))A  ORDER BY CAM_ID"




                dsProcessRule = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstr)

                Dim i As Integer
                Dim OutStr As String = String.Empty
                Dim SumOf As String = String.Empty

                If dsProcessRule.Tables(0).Rows.Count > 0 Then
                    sb.Append("<br>")
                    For i = 0 To dsProcessRule.Tables(0).Rows.Count - 1

                        OutStr = OutStr & "<div>Rule : " & i + 1 & "<div><font color='#1b80b6'>Operation : </font>" & dsProcessRule.Tables(0).Rows(i).Item("RULES") & "</div> <div><font color='#1b80b6'>Grade Slab :</font> " & dsProcessRule.Tables(0).Rows(i).Item("GRADE_SLAB") & " </div><div><font color='#1b80b6'> Weight : </font> " & dsProcessRule.Tables(0).Rows(i).Item("WEIGHT") & " </DIV></div>"
                        SumOf = SumOf + " (Rule : " & i + 1 & ") +"

                    Next
                    'If Len(SumOf) > 0 Then
                    '    SumOf = Mid(SumOf, 1, Len(SumOf) - 1)
                    'End If
                    sb.Append("<DIV class='font-weight-bold'>Rule Applied For " & reader.GetString(1) & "</DIV>" & OutStr & "<DIV class='font-weight-bold'>Final result is the sum of " & SumOf.TrimEnd("+") & "</Div>")
                End If
            End While
            reader.Close()
            If sb.ToString = "" Then
                sb.Append("<div class='font-weight-bold'>No Rule Available !!!</div>")
            End If
            ltProcessRule.Text = sb.ToString
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Sub GetReportMarks()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfACD_ID.Value = Session("Current_ACD_ID") Then

            str_query = "SELECT RSD_HEADER,CASE WHEN ISNULL(RST_ATTENDANCE,'')<>'' THEN RST_ATTENDANCE " _
                                  & " ELSE CONVERT(VARCHAR(100),ISNULL(RST_MARK,0)) END AS RST_MARK,CASE WHEN ISNULL(RST_ATTENDANCE,'')<>'' THEN RST_ATTENDANCE " _
                                  & " ELSE CONVERT(VARCHAR(100),ISNULL(RST_GRADING,'')) END AS RST_GRADING FROM " _
                                  & " RPT.REPORT_SETUP_D AS A INNER JOIN RPT.REPORT_STUDENT_S AS B " _
                                  & " ON A.RSD_ID=B.RST_RSD_ID WHERE RST_STU_ID=" + GetCheckedStudent() _
                                  & " AND RST_SBG_ID='" + GetCheckedSubject() + "' AND RST_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString
        Else
            str_query = "SELECT RSD_HEADER,CASE WHEN ISNULL(RST_ATTENDANCE,'')<>'' THEN RST_ATTENDANCE " _
                             & " ELSE CONVERT(VARCHAR(100),ISNULL(RST_MARK,0)) END AS RST_MARK,CASE WHEN ISNULL(RST_ATTENDANCE,'')<>'' THEN RST_ATTENDANCE " _
                             & " ELSE CONVERT(VARCHAR(100),ISNULL(RST_GRADING,'')) END AS RST_GRADING FROM " _
                             & " RPT.REPORT_SETUP_D AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B " _
                             & " ON A.RSD_ID=B.RST_RSD_ID WHERE RST_STU_ID=" + GetCheckedStudent() _
                             & " AND RST_SBG_ID='" + GetCheckedSubject() + "' AND RST_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString
        End If
        '  & " AND ISNULL(RST_TRM_ID,0)=" + ddlTerm.SelectedValue.ToString
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder
        While reader.Read
            sb.Append("<div class='font-weight-bold'>Header : " & reader.GetString(0) & "<div><font color='#1b80b6'>Mark : </font>" & reader.GetValue(1).ToString.Replace(".00", "") & "</div> <div><font color='#1b80b6'>Grade:</font> " & reader.GetString(2) & " </div></div>")
        End While
        reader.Close()
        ltMarks.Text = sb.ToString
    End Sub
#End Region
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkEdit As LinkButton
            Dim lnkCancel As LinkButton

            Dim lblMark As Label
            Dim lblStaId As Label
            Dim lblGrade As Label
            Dim txtMark As TextBox
            Dim txtGrade As TextBox

            With sender
                lnkEdit = TryCast(.FindControl("lnkEdit"), LinkButton)
                lnkCancel = TryCast(.FindControl("lnkCancel"), LinkButton)
                lblMark = TryCast(.FindControl("lblMark"), Label)
                lblStaId = TryCast(.FindControl("lblStaId"), Label)
                lblGrade = TryCast(.FindControl("lblGrade"), Label)
                txtMark = TryCast(.FindControl("txtMark"), TextBox)
                txtGrade = TryCast(.FindControl("txtGrade"), TextBox)
            End With

            If lnkEdit.Text = "Edit" Then
                lblMark.Visible = False
                lblGrade.Visible = False
                txtMark.Visible = True
                txtGrade.Visible = True
                txtMark.Text = lblMark.Text
                txtGrade.Text = lblGrade.Text
                lnkEdit.Text = "Save"
                btnProcess.Enabled = False
                lnkCancel.Visible = True
            Else
                UpdateMarks(lblStaId.Text, txtMark.Text, txtGrade.Text)
                lblMark.Text = txtMark.Text
                lblGrade.Text = txtGrade.Text
                lblMark.Visible = True
                lblGrade.Visible = True
                txtMark.Visible = False
                txtGrade.Visible = False

                lnkEdit.Text = "Edit"
                btnProcess.Enabled = True
                lnkCancel.Visible = False
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkEdit As LinkButton
            Dim lnkCancel As LinkButton
            Dim lblMark As Label
            Dim lblGrade As Label
            Dim txtMark As TextBox
            Dim txtGrade As TextBox
            With sender
                lnkEdit = TryCast(.FindControl("lnkEdit"), LinkButton)
                lnkCancel = TryCast(.FindControl("lnkCancel"), LinkButton)
                lblMark = TryCast(.FindControl("lblMark"), Label)
                lblGrade = TryCast(.FindControl("lblGrade"), Label)
                txtMark = TryCast(.FindControl("txtMark"), TextBox)
                txtGrade = TryCast(.FindControl("txtGrade"), TextBox)
            End With
            txtMark.Text = ""
            txtGrade.Text = ""
            txtMark.Visible = False
            txtGrade.Visible = False
            lblMark.Visible = True
            lblGrade.Visible = True
            lnkEdit.Text = "Edit"
            lnkCancel.Visible = False
            btnProcess.Enabled = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try

            ddlGrade.ClearSelection()
            ddlPrintedFor.ClearSelection()
            ddlSection.ClearSelection()
            ddlTerm.ClearSelection()
            cmbSubject.ClearSelection()
            cmbStudent.ClearSelection()
            BindReportCard()
            BindPrintedFor()
            BindGrade()
            PopulateSection()
            'updateSubjectURL()
            'updateStudentURL()
      
            BindTerm()
            BindSubjectCombo()
            BindStudentCombo()
        Catch ex As Exception
            ddlGrade.ClearSelection()
            ddlPrintedFor.ClearSelection()
            ddlSection.ClearSelection()
            ddlTerm.ClearSelection()
            cmbSubject.ClearSelection()
            cmbStudent.ClearSelection()
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        Try
            cmbSubject.ClearSelection()
            cmbStudent.ClearSelection()
            BindPrintedFor()
            BindGrade()
            PopulateSection()
            'updateStudentURL()
            'updateSubjectURL()

            BindSubjectCombo()
            BindStudentCombo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            cmbSubject.ClearSelection()
            cmbStudent.ClearSelection()
            PopulateSection()
            'updateSubjectURL()
            'updateStudentURL()
            BindSubjectCombo()
            BindStudentCombo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        Try
            cmbSubject.ClearSelection()
            cmbStudent.ClearSelection()
            updateStudentURL()
            BindStudentCombo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try

            If GetCheckedSubject() = "" Then
                lblError.Text = "Please select a subject"
                Exit Sub
            End If
            If GetCheckedStudent() = "" Then
                lblError.Text = "Please select a student"
                Exit Sub
            End If

            hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
            hfRPF_ID.Value = ddlPrintedFor.SelectedValue.ToString
            hfRSM_ID.Value = ddlReportCard.SelectedValue.ToString
            hfTRM_ID.Value = ddlTerm.SelectedValue.ToString

            BindAssesments()
            tblStudent.Rows(5).Visible = True
            tblStudent.Rows(6).Visible = True
            tblStudent.Rows(7).Visible = True
            If GetCheckedSubject() <> "" Then
                GetProcessRules()
                GetReportMarks()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            GetProcessedMarks()
            tblStudent.Rows(8).Visible = True
            tblStudent.Rows(9).Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveProcessedMarks()
            If GetCheckedSubject() <> "" Then
                GetReportMarks()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    '----------------------------------------------------------TELERIK COMBO BOX----------------------------------------------------------------
    Protected Sub cmbStudent_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        CType(cmbStudent.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbStudent.Items.Count)
    End Sub
    Protected Sub cmbStudent_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("StudentName").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stu_id").ToString()
    End Sub
  

    Protected Sub BindStudentCombo()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param(2) = New SqlParameter("@GRDID", grade(0))
        param(3) = New SqlParameter("@SCTID", ddlSection.SelectedValue.ToString)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.LOAD_STUDENTS_COMBO", param)
        cmbStudent.DataSource = ds
        cmbStudent.DataBind()
    End Sub

    Function GetCheckedStudent() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = cmbStudent.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            Try
                str = cmbStudent.SelectedItem.Value
            Catch ex As Exception
            End Try
        End If
        Return str

    End Function



    Protected Sub cmbSubject_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("SBG_DESCR").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("SBG_ID").ToString()
    End Sub
    Protected Sub cmbSubject_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        CType(cmbSubject.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbSubject.Items.Count)
    End Sub
    Protected Sub BindSubjectCombo()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param(1) = New SqlParameter("@GRDID", grade(0))

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.LOAD_SUBJECT_COMBO", param)
        cmbSubject.DataSource = ds
        cmbSubject.DataBind()
    End Sub
    Function GetCheckedSubject() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = cmbSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                If str <> "" Then
                    str += "|"
                End If
                str += item.Value
            Next
            str = str.TrimEnd("|")
        Else
            Try
                str = cmbSubject.SelectedItem.Value
            Catch ex As Exception
            End Try

        End If


     

        Return str

    End Function
End Class
