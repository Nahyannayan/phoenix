<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOption_M.aspx.vb" Inherits="Curriculum_clmOption_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Option Masters
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td >
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Option Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtOption" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;<asp:HiddenField ID="hfOPT_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfOpt" runat="server" ErrorMessage="Please enter the field Option" ControlToValidate="txtOption" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
                        </td>

                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

