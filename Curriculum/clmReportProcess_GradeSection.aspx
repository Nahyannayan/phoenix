<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportProcess_GradeSection.aspx.vb" Inherits="Curriculum_clmReportProcess_GradeSection" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="Literal1" runat="server" Text="Processing Report"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><span style="color: #c00000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td   valign="bottom">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Term</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    
                                </tr>
                                
                                <tr>
                                    <td align="left"><span class="field-label">Select Report card</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Report Schedule</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>

                                   
                                </tr>
                                
                                <tr>
                                     <td align="left"><span class="field-label">Column Header</span></td>
                                    
                                    <td align="left">
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstHeader" runat="server" RepeatColumns="0" RepeatDirection="Vertical">
                                        </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td align="left" ><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>

                                    
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Section</span></td>
                                    
                                    <td align="left" colspan="3">
                                        <asp:CheckBoxList ID="lstSection"  CssClass="field-label" runat="server" RepeatColumns="12" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                        <asp:Label ID="lblNote" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td   valign="bottom" align="center">

                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td    valign="bottom">
                            <asp:Panel ID="Panel1" runat="server" BorderStyle="None"
                            
                                Visible="false">
                                <asp:Label ID="lblText" runat="server" CssClass="error" Font-Bold="True">
                                    Request could not be processed as rules are not set for certain subjects.Click ok to view the discrepancy report</asp:Label>
                                <asp:Button ID="btnYes" runat="server" CssClass="button" Text="OK"   />
                                <asp:Button ID="btnNo" runat="server" CssClass="button"  Text="Cancel" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfbAOLprocessing" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>

