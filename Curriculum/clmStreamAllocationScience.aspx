<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmStreamAllocationScience.aspx.vb" Inherits="Curriculum_clmStreamAllocationScience" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Science Stream Allocation</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server"  width="100%">
                    <tr style="font-size: 12pt">
                        <td align="left" class="title">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>

                            <table align="left">
                                <tr class="matters" style="color: Red">
                                    <td>REQUESTED : </td>
                                    <td align="left" style="color: Red" valign="top">
                                        <asp:DataList ID="dlRStreams" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table>
                                                    <tr style="color: Red">
                                                        <td class="matters" style="color: Red">
                                                            <asp:LinkButton Style="text-decoration: none; color: Red" ID="lnkRStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:LinkButton>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" Visible="false" ID="lblRStmID" Text='<%# Bind("STM_ID") %>'></asp:Label></td>
                                                    </tr>
                                                </table>

                                                <div id="Div1" runat="server" style=" margin-top: -2px; display: none; margin-left: -3px; margin-right: 1px;   text-align: left">
                                                    <div class="title-bg-lite" style="padding-bottom: 2px; vertical-align: middle;"  >
                                                        Option Keys
                                                    </div>
                                                    <asp:Panel ID="PopupMenu1" runat="server" CssClass="panel-cover" Height="25%" ScrollBars="Vertical"
                                                        Width="100%">
                                                        <asp:Literal ID="ltRProcess" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                </div>
                                                <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="Server" HoverCssClass="popupHover"
                                                    OffsetX="0" OffsetY="20" PopDelay="50" PopupControlID="Div1" PopupPosition="Center"
                                                    TargetControlID="lnkRStream">
                                                </ajaxToolkit:HoverMenuExtender>



                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                    <td class="matters" style="color: Green">ALLOTED :</td>
                                    <td align="left" class="matters" style="font-weight: bold;" valign="top">
                                        <asp:DataList ID="dlStreams" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td class="matters">
                                                            <asp:LinkButton Style="text-decoration: none; color: Green" ID="lnkStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:LinkButton>
                                                        </td>
                                                        <td style="text-decoration: none; color: Green">:</td>
                                                        <td>
                                                            <asp:Label runat="server" Style="text-decoration: none; color: Green" ID="lblStAlloted" Text='<%# Bind("STM_COUNT") %>'></asp:Label></td>
                                                        <td>
                                                            <asp:Label runat="server" Visible="false" ID="lblStmID" Text='<%# Bind("STM_ID") %>'></asp:Label></td>
                                                    </tr>
                                                </table>

                                                <div id="popup" runat="server" style="margin-top: -2px; display: none; margin-left: -3px; margin-right: 1px;  text-align: left">
                                                    <div class="title-bg" style="padding-bottom: 2px; vertical-align: middle;">
                                                        Option Keys
                                                    </div>
                                                    <asp:Panel ID="PopupMenu" runat="server" CssClass="panel-cover" Height="25%" ScrollBars="Vertical"
                                                        Width="100%">
                                                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                </div>
                                                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                                    OffsetX="200" OffsetY="20" PopDelay="50" PopupControlID="popup" PopupPosition="Left"
                                                    TargetControlID="lnkStream">
                                                </ajaxToolkit:HoverMenuExtender>



                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>



                    <tr>
                        <td align="center" class="matters" style="font-weight: bold;  " valign="top">

                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnSearch">
                                <table id="tblTC" runat="server" width="100%">
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Student ID</span></td>
                                        <td align="left" class="matters"  >
                                            <asp:TextBox ID="txtStuNo" runat="server">
                                            </asp:TextBox></td>
                                        <td align="left" class="matters"  ><span class="field-label">Student Name</span></td>
                                        <td align="left" class="matters" >
                                            <asp:TextBox ID="txtName" runat="server">
                                            </asp:TextBox></td>
                                        </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">Section</span></td>
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlSection" runat="server"  >
                                            </asp:DropDownList></td>
                                  
                                        <td align="left" class="matters"><span class="field-label">M/P/C</span></td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtMPC" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">M/P/C/B</span></td>
                                        <td align="left" class="matters"  >
                                            <asp:TextBox ID="txtMPCB" runat="server"></asp:TextBox></td>
                                        <td align="left" class="matters"  ><span class="field-label">P/C/B</span></td>
                                        <td align="left" class="matters"  >
                                            <asp:TextBox ID="txtPCB" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">Choice 1</span></td>
                                        <td align="left" class="matters"  >
                                            <asp:CheckBoxList ID="lstChoice1" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                  RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                                >
                                            </asp:CheckBoxList></td>
                                        <td align="left" class="matters"  ><span class="field-label">Choice&nbsp; 2</span></td>
                                        <td align="left" class="matters" >
                                            <asp:CheckBoxList ID="lstChoice2" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                  RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                                >
                                            </asp:CheckBoxList></td>
                                        </tr>
                                    <tr>
                                        <td align="left" class="matters"  ><span class="field-label">Choice 3</span></td>
                                        <td>
                                            <asp:CheckBoxList ID="lstChoice3" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                 RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                                >
                                            </asp:CheckBoxList></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td align="left" ><span class="field-label">Show Records</span></td>
                                        <td align="left" >
                                            <asp:TextBox ID="txtRecords" runat="server"></asp:TextBox></td>
                                        <td align="left" class="matters">List</td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlType" runat="server"  >
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem Value="MATHS">M/P/C</asp:ListItem>
                                                <asp:ListItem Value="BIOLOGY">M/P/C/B</asp:ListItem>
                                            </asp:DropDownList></td>
                                        </tr>
                                    <tr>
                                        <td align="left" class="matters" colspan="2">
                                            <table  >
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="rdAll" runat="server" Text="All" GroupName="g1"></asp:RadioButton></td>
                                                    <td>
                                                        <asp:RadioButton ID="rdAllocated" runat="server" Text="Allotted" GroupName="g1"></asp:RadioButton></td>
                                                    <td  >
                                                        <asp:RadioButton ID="rdNotAllocated" runat="server" GroupName="g1" Text="Not Allotted"  Checked="True"></asp:RadioButton></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="left" class="matters" colspan="2">&nbsp;
                <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"   /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" >Allocate to Stream &amp; Option :
                                        <asp:DropDownList ID="ddlStream" runat="server"   AutoPostBack="True">
                                        </asp:DropDownList>
                                            <asp:DropDownList ID="ddlOption" runat="server"   AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSubject" runat="server" >
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="matters" colspan="4" >
                                            <asp:CheckBox ID="chkMarkSort" runat="server" Text="Sort By Marks" Visible="False"></asp:CheckBox>
                                            <asp:CheckBox ID="chkChoiceSort" runat="server" Text="Sort By Choice" Visible="False"></asp:CheckBox>

                                            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" Width="100%"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                 PageSize="20" >
                                                <RowStyle CssClass="griditem" />

                                                <Columns>
                                                    <asp:TemplateField HeaderText="Available">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />

                                                        </EditItemTemplate>
                                                        <HeaderTemplate>
                                                           Select <br />
                                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                            ToolTip="Click here to select/deselect all rows" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />

                                                        </ItemTemplate>

                                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SL.No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNoView() %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Student No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo"   runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>



                                                        </ItemTemplate>

                                                        <ItemStyle  ></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>



                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Section">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>


                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="M/P/C">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMath" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>


                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="M/P/C/B">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBio" runat="server" Text='<%# Bind("Marks_bio") %>'></asp:Label>


                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="P/C/B">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblpcb" runat="server" Text='<%# Bind("Marks_pcb") %>'></asp:Label>


                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Choice 1">
                                                        <HeaderTemplate>
                                                            Choice1<br />
                                                                        <asp:DropDownList ID="ddlgvChoice1" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                            OnSelectedIndexChanged="ddlgvChoice1_SelectedIndexChanged"  >
                                                                        </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice1" runat="server" Text='<%# Bind("Choice1") %>'></asp:Label>


                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Choice 2">
                                                        <HeaderTemplate>
                                                                        Choice2 <br />
                                                                        <asp:DropDownList ID="ddlgvChoice2" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                            OnSelectedIndexChanged="ddlgvChoice2_SelectedIndexChanged"  >
                                                                        </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice2" runat="server" Text='<%# Bind("Choice2") %>'></asp:Label>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Choice 3">
                                                        <HeaderTemplate>
                                                                        Choice3<br />
                                                                        <asp:DropDownList ID="ddlgvChoice3" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                            OnSelectedIndexChanged="ddlgvChoice3_SelectedIndexChanged" >
                                                                        </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChoice3" runat="server" Text='<%# Bind("Choice3") %>'></asp:Label>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Alloted Stream">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAllotedStream" runat="server" Text='<%# Bind("AllotedStream") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Alloted Option">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAllotedOption" runat="server" Text='<%# Bind("AllotedOption") %>'></asp:Label>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <SelectedRowStyle  />
                                                <HeaderStyle   />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="11"   valign="top">
                                            <asp:Button ID="btnAllot" runat="server" Text="Allot" CssClass="button" TabIndex="4"  />
                                            <asp:Button ID="btnDeAllot" runat="server" Text="De Allot" CssClass="button" TabIndex="4"   /></td>
                                    </tr>


                                </table>
                            </asp:Panel>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>
</asp:Content>

