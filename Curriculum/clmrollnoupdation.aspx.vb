﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Data.OleDb
Partial Class Curriculum_clmrollnoupdation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Try
    '        Dim smScriptManager As New ScriptManager
    '        smScriptManager = Master.FindControl("ScriptManager1")

    '        smScriptManager.EnablePartialRendering = False
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100400") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                   

                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))


                   
                    FillGrade()


                    gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)


               


                    'disable the control buttons based on the rights

                    'gvComments.Attributes.Add("bordercolor", "#1b80b6")





                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '  lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    'Protected Sub gvComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    Try
    '        gvComments.PageIndex = e.NewPageIndex
    '        gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        UpLoadExcelFiletoXml()
    End Sub
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " SELECT DISTINCT GRM_DISPLAY,grm_GRD_ID,GRD_DISPLAYORDER  FROM GRADE_BSU_M  INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID  WHERE " _
                                 & " GRM_BSU_ID ='" + Session("SBSUID") + "' AND GRM_ACD_ID= '" + ddlAca_Year.SelectedValue + "' AND GRD_ID IN('10','12') ORDER BY GRD_DISPLAYORDER "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count >= 1 Then
            ddlGrade.SelectedIndex = 0
            Try
                FillSection()
                ' gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= '" + ddlAca_Year.SelectedValue + "'  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedItem.Value + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If ddlSection.Items.Count >= 1 Then
            ddlSection.SelectedIndex = 0
            Try
                'FillSubjects()
                gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        gridbind(ddlGrade.SelectedValue, "")
        FillSection()

    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        FillGrade()
    End Sub
    Public Sub gridbind(ByVal grid As String, ByVal st As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
           
            Dim txtSearch As New TextBox
            Dim optSearch As String
            Dim strFilter As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String
            Dim strFilter1 As String = ""


            ''If ddlGrade.Items.Count >= 1 Then
            ''    Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            ''    str_Sql = " SELECT DISTINCT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CAT_DESC,RSD_HEADER,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
            ''              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
            ''              " INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID" & _
            ''              " INNER JOIN RPT.REPORT_SETUP_D ON CMT_RSD_ID=RSD_ID AND RSD_RSM_ID=" + ddlReport.SelectedValue.ToString & _
            ''              " WHERE CMT_GRD_ID='" & GRD_ID & "'"
            ''    strSubject = getSubjects()
            ''    If strSubject <> "" Then
            ''        str_Sql += "AND CMT_SBG_ID IN (" & strSubject & ")"
            ''    End If

            ''    strHeader = getHeaders()

            ''    If strHeader <> "" Then
            ''        str_Sql += " AND CMT_RSD_ID IN(" & strHeader & ")"
            ''    End If
            ''    gvComments.Columns(4).Visible = True
            ''    gvComments.Columns(5).Visible = True
            ''Else
            ''    str_Sql = " SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
            ''              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
            ''              " WHERE CMT_GRD_ID='0'"
            ''End If




            'If gvComments.Rows.Count > 0 Then

            '    txtSearch = gvComments.HeaderRow.FindControl("txtOption")
            '    'strSidsearch = h_Selected_menu_1.Value.Split("__")
            '    'strSearch = strSidsearch(0)
            '    If txtSearch.Text <> "" Then
            '        strFilter = " and CMT_COMMENTS  like '" & txtSearch.Text & "%' "
            '        optSearch = txtSearch.Text



            '        If strFilter.Trim <> "" Then
            '            str_Sql += " " + strFilter
            '        End If
            '    End If
            'End If

            str_Sql = " SELECT STU_ID,STU_NO,stu_NAME=ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'') , " _
                       & " case when (STP_GRD_ID='10') then STU_BOARDREGNO_YR10   else case when (STP_GRD_ID='12') then STU_BOARDREGNO_YR12  else '0' end end as stu_regno " _
                        & " FROM STUDENT_M " _
                      & " INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID WHERE STP_ACD_ID='" + ddlAca_Year.SelectedValue + "'"

            If grid <> "" Then

                str_Sql += " AND STP_GRD_ID='" + grid + "'"
            End If
            If st <> "" Then

                str_Sql += " AND STP_SCT_ID='" + st + "'"
            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch = gvComments.HeaderRow.FindControl("txtOption")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch.Text <> "" Then
                    strFilter = " and stu_no  like '" & txtSearch.Text & "%' "
                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If
            End If


            If gvComments.Rows.Count > 0 Then

                txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
                'strSidsearch = h_Selected_menu_1.Value.Split("__")
                'strSearch = strSidsearch(0)
                If txtSearch1.Text <> "" Then
                    strFilter1 = " and ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')  like '" & txtSearch1.Text & "%' "
                    optSearch1 = txtSearch1.Text



                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If
            End If







            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch

            txtSearch1 = New TextBox
            txtSearch1 = gvComments.HeaderRow.FindControl("txtOption1")
            txtSearch1.Text = optSearch1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
    End Sub

    Protected Sub btnEmpid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnstuname_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvComments_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvComments.RowCommand


        '  If IsPostBack = False Then
        Try
            '    Dim url As String

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvComments.Rows(index), GridViewRow)

            If e.CommandName = "Update" Then
                Dim lblid As New Label
                Dim txtregno As New TextBox
                lblid = selectedRow.FindControl("cmtId")

                txtregno = selectedRow.FindControl("lblclm")
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_query As String


                str_query = "exec dbo.UPDATE_BOARDREGNO " & lblid.Text & " ,'" & ddlGrade.SelectedValue & "','" & txtregno.Text & "'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)




            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        ' End If
    End Sub

    Private Sub UpLoadExcelFiletoXml()
        Dim strFileNameOnly As String

        lblerror3.Text = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "_") & Session("sBsuid") & "_" & ddlGrade.SelectedItem.Value & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly & ";" & _
                "Extended Properties=Excel 8.0;"

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("~/Curriculum/ReportDownloads") & "/" & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim s As New MemoryStream()

                dt.WriteXml(s, True)



                'Retrieve the text from the stream

                s.Seek(0, SeekOrigin.Begin)

                Dim sr As New StreamReader(s)

                Dim xmlString As String

                xmlString = sr.ReadToEnd()



                'close 

                sr.Close()

                sr.Dispose()
                Dim cmd As New SqlCommand
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("dbo.ProcessregnoXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@bsu_ID", Session("sBsuid"))
                cmd.Parameters.AddWithValue("@GRD_ID", ddlGrade.SelectedItem.Value)

                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()

                lblerror3.Text = "Records Saved Sucessfully"
                gridbind(ddlGrade.SelectedValue, ddlSection.SelectedValue)

            Catch ex As Exception
                lblerror3.Text = "Error: " & ex.Message.ToString
            End Try
        Else
            lblerror3.Text = "Please select a file to upload."
        End If


    End Sub

   

    Protected Sub gvComments_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvComments.RowUpdating

    End Sub
End Class


