﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_clmReportRule_Popup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hfRRM_ID.Value = Request.QueryString("rrm_id")
            lblSubject.Text = Request.QueryString("subject")
            lblReport.Text = Request.QueryString("report")
            lblHeader.Text = Request.QueryString("header")
            GetData()
        End If
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        
     


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RULES"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "WEIGHT"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub GetData()
        Dim li As New ListItem

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RRM_PASSMARK,0) RRM_PASSMARK,ISNULL(RRM_MAXMARK,0) RRM_MAXMARK,ISNULL(RRM_GRADE_WEIGTAGE,0) RRM_GRADE_WEIGTAGE,ISNULL(GSM_DESC,'-') GSM_DESC " _
                                & " FROM RPT.REPORT_RULE_M AS A " _
                                & " LEFT OUTER JOIN ACT.GRADING_SLAB_M AS J ON A.RRM_GSM_SLAB_ID=J.GSM_SLAB_ID" _
                                & " WHERE RRM_ID=" + hfRRM_ID.Value

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            lblGradeSlab.Text = reader.GetString(3)
            lblMinPassmark.Text = reader.GetValue(0).ToString.Replace(".00", "")
            lblMaxmark.Text = reader.GetValue(1).ToString.Replace(".00", "")
            lblGradeWt.Text = reader.GetValue(2).ToString.Replace(".000", "")
        End While

        reader.Close()


        str_query = "SELECT  RULES=RSS_OPRT_DISPLAY+' ('+(select STUFF((SELECT ', ' + CASE A.RSS_OPRT_DISPLAY WHEN 'WEIGHTAGE' THEN" _
                   & " convert(varchar(100),RRD_WEIGHTAGE)+'% of '+convert(varchar(100),CAD_DESC) ELSE convert(varchar(100),CAD_DESC) END" _
                   & " from RPT.REPORT_SCHEDULE_D AS P INNER JOIN ACT.ACTIVITY_D AS Q ON P.RRD_CAD_ID=Q.CAD_ID" _
                   & " where RRD_RRM_ID=A.RSS_RRM_ID AND RRD_CAM_ID=A.RSS_CAM_ID  for xml path('')),1,1,''))+')'," _
                   & " WEIGHT=RSS_WEIGHTAGE" _
                   & " FROM RPT.REPORT_SCHEDULE_M AS A WHERE RSS_RRM_ID=" + hfRRM_ID.Value

        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow

            dr.Item(0) = reader.GetString(0)
            dr.Item(1) = reader.GetValue(1)

            dt.Rows.Add(dr)
        End While


        gvRule.DataSource = dt
        gvRule.DataBind()


    End Sub



End Class
