Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSubjectGrade_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("SelectedRow") = -1
                    Session("dtSubject") = SetDataTable()
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    lblacademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    hfCopy.Value = 0
                     PopulateSubjects()
                    '    PopulateParentSubjects()
                    PopulateParentSubjectTree()
                    BindCopyStream()
                    BindCopyGrade()

                    If ddlCopyGrade.Items.Count = 0 Or ddlCopyStream.Items.Count = 0 Then
                        ddlCopyGrade.Enabled = False
                        ddlCopyStream.Enabled = False
                        btnCopy.Enabled = False
                    Else
                        ddlCopyGrade.Enabled = True
                        ddlCopyStream.Enabled = True
                        btnCopy.Enabled = True
                    End If
                    BindDepartment()
                    BindOptions()
                    GetRows()
                    EnableDisableControls(False)
                   
                    If rdMarks.Checked = True Then
                        rdEMark.Checked = True
                        rdEgrade.Checked = False
                        rdEgrade.Enabled = False
                    Else
                        rdEgrade.Enabled = True
                    End If

                    If Session("multistream") = 0 Then
                        ddlCopyStream.Visible = False
                        ''commented bynahyan 13nov2018- rows2 was remopved in new design
                        ' Table2.Rows(2).Visible = False
                        lblStreamText.Visible = False
                        lblStream.Visible = False
                    End If

                    If Session("sbsuid") <> "124001" And Session("sbsuid") <> "300001" Then
                        trCount.Visible = False
                        trCredit.Visible = False
                        trLength.Visible = False
                    End If
                End If
                gvSubject.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private Methods"

    Sub EnableDisableControls(ByVal value As Boolean)
        txtDescr.Enabled = value
        ddlDPT.Enabled = value
        '  ddlOption.Enabled = value
        ' ddlParentSubj.Enabled = value
        ddlSubject.Enabled = value
        rdMarks.Enabled = value
        rdGrade.Enabled = value
        chkOpt.Enabled = value
        chkSub.Enabled = value
        chkTC.Enabled = value
        gvSubject.Enabled = value
        btnAddNew.Enabled = value
        txtShortCode.Enabled = value
        txtDescr.Enabled = value
        chkCE.Enabled = value
        chkGroup.Enabled = value
        ddlMajor.Enabled = value
        rdEMark.Enabled = value
        rdEgrade.Enabled = value
        'tvParentSubjects.Enabled = value
        'If value = True Then
        'PopulateParentSubjectTree()
        'End If
        If value = True And chkOpt.Checked Then
            lstOptions.Enabled = True
        Else
            lstOptions.ClearSelection()
            lstOptions.Enabled = False
        End If

        If rdMarks.Checked = True Then
            rdEMark.Checked = True
            rdEgrade.Checked = False
            rdEgrade.Enabled = False
        Else
            rdEgrade.Enabled = True
        End If

        txtCredit.Enabled = value
        txtWeight.Enabled = value
        ddlLength.Enabled = value
        ddlHr.Enabled = value
        ddlGpa.Enabled = value
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(2)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_SBM_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_DESCR"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_SHORTCODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_DPT_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DPT_DESCR"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bOPTIONAL"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_OPT_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OPT_DESCR"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_PARENT_ID"
        dt.Columns.Add(column)
        keys(1) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_PARENT"
        dt.Columns.Add(column)
        keys(2) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bREPCRD_DISPLAY"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bMRK_DISPLAY"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBM_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_ORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bTC_DISPLAY"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_PARENTS_SHORT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bCOREEXT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bAUTOGROUP"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_bMAJOR"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_ENTRYTYPE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_CREDITS"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_WT"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_LEN"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_HRS"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_GPA"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function
    Sub GetRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT A.SBG_ID,A.SBG_SBM_ID,A.SBG_DESCR AS SBG_DESCR , " _
        '                         & " A.SBG_SHORTCODE,A.SBG_DPT_ID,DPT_DESCR,A.SBG_bOPTIONAL," _
        '                         & " OPT_IDS=ISNULL((SELECT SGO_OPT_ID FROM SUBJECTGRADE_OPTIONS_S WHERE SGO_SBG_ID=A.SBG_ID FOR XML PATH),'<ID><SGO_OPT_ID>0</SGO_OPT_ID></ID>' ) ,ISNULL(A.SBG_OPTIONS,''),A.SBG_PARENT_ID," _
        '                         & " ISNULL(B.SBG_DESCR,'NA') AS SBG_PARENT,A.SBG_bREPCRD_DISPLAY,A.SBG_bMRK_DISPLAY,SBM_DESCR,ISNULL(A.SBG_ORDER,0),ISNULL(A.SBG_bTC_DISPLAY,'FALSE')" _
        '                         & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS E ON A.SBG_SBM_ID=E.SBM_ID LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON" _
        '                         & " A.SBG_PARENT_ID=B.SBG_SBM_ID AND B.SBG_ACD_ID=" + hfACD_ID.Value _
        '                         & " AND B.SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
        '                         & " AND B.SBG_STM_ID=" + hfSTM_ID.Value _
        '                         & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
        '                         & " WHERE A.SBG_ACD_ID=" + hfACD_ID.Value + " AND A.SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
        '                         & " AND A.SBG_STM_ID=" + hfSTM_ID.Value + " ORDER BY A.SBG_DESCR"

        Dim str_query As String = "SELECT SBG_ID,SBG_SBM_ID,SBG_DESCR AS SBG_DESCR , " _
                              & " A.SBG_SHORTCODE,A.SBG_DPT_ID,DPT_DESCR,A.SBG_bOPTIONAL," _
                              & " OPT_IDS=ISNULL((SELECT SGO_OPT_ID FROM SUBJECTGRADE_OPTIONS_S WHERE SGO_SBG_ID=A.SBG_ID FOR XML PATH),'<ID><SGO_OPT_ID>0</SGO_OPT_ID></ID>' ) ," _
                              & " ISNULL(A.SBG_OPTIONS,''),SBG_PARENT_ID," _
                              & " ISNULL(SBG_PARENTS,'NA'),SBG_bREPCRD_DISPLAY,SBG_bMRK_DISPLAY," _
                              & " SBM_DESCR,ISNULL(A.SBG_ORDER,0),ISNULL(A.SBG_bTC_DISPLAY,'FALSE'),ISNULL(SBG_PARENTS_SHORT,'NA'),ISNULL(SBG_bCOREEXT,'FALSE'),ISNULL(SBG_bAUTOGROUP,'FALSE'),ISNULL(SBG_bMAJOR,'TRUE'),ISNULL(SBG_ENTRYTYPE,'Mark')" _
                              & " ,isnull(SBG_CREDITS,0), isnull(SBG_WT,0), isnull(SBG_LEN,''), isnull(SBG_HRS,''), isnull(SBG_GPA,'')" _
                              & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS E ON A.SBG_SBM_ID=E.SBM_ID " _
                              & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
                              & " WHERE A.SBG_ACD_ID=" + hfACD_ID.Value + " AND A.SBG_GRD_ID='" + hfGRD_ID.Value + "'" _
                              & " AND A.SBG_STM_ID=" + hfSTM_ID.Value + " ORDER BY A.SBG_DESCR"


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        dt = Session("dtSubject")

        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = reader.GetValue(0).ToString
                .Item(1) = reader.GetValue(1).ToString
                .Item(2) = reader.GetString(2)
                .Item(3) = reader.GetString(3)
                .Item(4) = reader.GetValue(4).ToString
                .Item(5) = reader.GetString(5)
                .Item(6) = IIf(reader.GetBoolean(6) = True, "Yes", "No")
                .Item(7) = "<IDS>" + reader.GetString(7).ToString.Replace("row", "ID") + "</IDS>"
                .Item(8) = reader.GetString(8)
                .Item(9) = reader.GetValue(9).ToString
                .Item(10) = reader.GetString(10)
                .Item(11) = IIf(reader.GetBoolean(11) = True, "Yes", "No")
                .Item(12) = IIf(reader.GetBoolean(12) = True, "Yes", "No")
                .Item(13) = "update"
                .Item(14) = reader.GetString(13)
                .Item(15) = reader.GetValue(14).ToString
                .Item(16) = IIf(reader.GetBoolean(15) = True, "Yes", "No")
                .Item(17) = reader.GetString(16)
                .Item(18) = IIf(reader.GetBoolean(17) = True, "Yes", "No")
                .Item(19) = IIf(reader.GetBoolean(18) = True, "Yes", "No")
                .Item(20) = IIf(reader.GetBoolean(19) = True, "Yes", "No")
                .Item(21) = reader.GetString(20)
                .Item(22) = reader.GetValue(21).ToString
                .Item(23) = reader.GetValue(22).ToString
                .Item(24) = reader.GetString(23)
                .Item(25) = reader.GetString(24)
                .Item(26) = reader.GetString(25)
            End With
            dt.Rows.Add(dr)
        End While
        reader.Close()
        gvSubject.DataSource = dt
        gvSubject.DataBind()

        If dt.Rows.Count <> 0 Then
            btnCopy.Enabled = False
            ddlCopyGrade.Enabled = False
            ddlCopyStream.Enabled = False
            btnEdit.Text = "Edit"
            ViewState("datamode") = "edit"
        Else
            If ddlCopyGrade.Items.Count <> 0 And ddlCopyStream.Items.Count <> 0 Then
                btnCopy.Enabled = True
                ddlCopyStream.Enabled = True
                ddlCopyGrade.Enabled = True
            Else
                btnCopy.Enabled = False
                ddlCopyStream.Enabled = False
                ddlCopyGrade.Enabled = False
            End If
            btnEdit.Text = "Add"
            ViewState("datamode") = "add"
        End If
        Session("dtSubject") = dt
    End Sub
    Sub AddRows()
        Dim dt As New DataTable
        Dim dr As DataRow
        dt = Session("dtSubject")
        Dim keys As Object()
        ReDim keys(2)

        Dim parent As String
        Dim parents As String()

        parent = GetParent()
        If parent = "Error" Then
            lblError.Text = "Cannot add " + ddlSubject.SelectedItem.Text + " as a child to the same subject"
            Exit Sub
        End If
        parents = parent.Split("|")
        keys(0) = ddlSubject.SelectedValue.ToString
        'keys(1) = ddlParentSubj.SelectedValue.ToString
        keys(1) = parents(1)
        keys(2) = parents(0)

        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This subject is already added"
            Exit Sub
        End If
        dr = dt.NewRow
        With dr
            .Item(0) = "0"
            .Item(1) = ddlSubject.SelectedValue.ToString
            .Item(2) = txtDescr.Text
            .Item(3) = txtShortCode.Text
            .Item(4) = ddlDPT.SelectedValue.ToString
            .Item(5) = ddlDPT.SelectedItem.Text
            .Item(6) = IIf(chkOpt.Checked = True, "Yes", "No")
            If chkOpt.Checked = True Then
                .Item(7) = GetOptionIds()
                .Item(8) = GetOptionNames()
            Else
                .Item(7) = "<IDS><ID>0</ID></IDS>"
                .Item(8) = ""
            End If
            '.Item(9) = ddlParentSubj.SelectedValue.ToString
            .Item(9) = parents(1)
            ' .Item(10) = ddlParentSubj.SelectedItem.Text
            .Item(10) = parents(0)
            .Item(10) = parents(0)
            .Item(11) = IIf(chkSub.Checked = True, "Yes", "No")
            .Item(12) = IIf(rdMarks.Checked = True, "Yes", "No")
            .Item(13) = "add"
            .Item(14) = ddlSubject.SelectedItem.Text
            .Item(15) = "0"
            .Item(16) = IIf(chkTC.Checked = True, "Yes", "No")
            .Item(17) = parents(2)
            .Item(18) = IIf(chkCE.Checked = True, "Yes", "No")
            .Item(19) = IIf(chkGroup.Checked = True, "Yes", "No")
            .Item(20) = IIf(ddlMajor.SelectedValue = "1", "Yes", "No")
            .Item(21) = IIf(rdEMark.Checked = True, "Mark", "Grade")
            .Item(22) = Val(txtCredit.Text)
            .Item(23) = Val(txtWeight.Text)
            .Item(24) = ddlLength.SelectedValue.ToString
            .Item(25) = ddlHr.SelectedValue.ToString
            .Item(26) = ddlGpa.SelectedValue.ToString
        End With
        dt.Rows.Add(dr)
        Session("dtSubject") = dt
        GridBind(dt)

    End Sub
    Sub Listoptions(ByVal ids As String)
        If ids.Trim <> "" Then
            ids = ids.Replace("</IDS>", "")
            ids = ids.Replace("<IDS>", "")
            ids = ids.Replace("<ID><SGO_OPT_ID>", ",")
            ids = ids.Replace("</SGO_OPT_ID></ID>", "")
            ids = ids.Remove(0, 1)

            Dim optIds As String()
            optIds = ids.Split(",")

            Dim i As Integer

            For i = 0 To optIds.Length - 1
                If optIds(i) <> "0" Then
                    lstOptions.Items.FindByValue(optIds(i)).Selected = True
                End If
            Next
        End If
    End Sub
    Function GetOptionIds() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstOptions.Items.Count - 1
            If lstOptions.Items(i).Selected = True Then
                str += "<ID><SGO_OPT_ID>" + lstOptions.Items(i).Value + "</SGO_OPT_ID></ID>"
            End If
        Next
        Return "<IDS>" + str + "</IDS>"
    End Function
    Function GetOptionNames() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstOptions.Items.Count - 1
            If lstOptions.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += lstOptions.Items(i).Text
            End If
        Next
        Return str
    End Function
    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtSubject")
            Dim index As Integer = ViewState("SelectedRow")

            Dim parent As String
            Dim parents As String()

            parent = GetParent()
            If parent = "Error" Then
                lblError.Text = "Cannot add " + ddlSubject.SelectedItem.Text + " as a child to the same subject"
                Exit Sub
            End If
            parents = parent.Split("|")

            With dt.Rows(index)
                .Item(1) = ddlSubject.SelectedValue.ToString
                .Item(2) = txtDescr.Text
                .Item(3) = txtShortCode.Text
                .Item(4) = ddlDPT.SelectedValue.ToString
                .Item(5) = ddlDPT.SelectedItem.Text
                .Item(6) = IIf(chkOpt.Checked = True, "Yes", "No")
                If chkOpt.Checked = True Then
                    .Item(7) = GetOptionIds()
                    .Item(8) = GetOptionNames()
                Else
                    .Item(7) = "0"
                    .Item(8) = ""
                End If
                '.Item(9) = ddlParentSubj.SelectedValue.ToString
                ' .Item(10) = ddlParentSubj.SelectedItem.Text
                .Item(9) = parents(1)
                .Item(10) = parents(0)
                .Item(11) = IIf(chkSub.Checked = True, "Yes", "No")
                .Item(12) = IIf(rdMarks.Checked = True, "Yes", "No")
                If .Item(13) = "update" Then
                    .Item(13) = "edit"
                End If
                .Item(16) = IIf(chkTC.Checked = True, "Yes", "No")
                .Item(17) = parents(2)
                .Item(18) = IIf(chkCE.Checked = True, "Yes", "No")
                .Item(19) = IIf(chkGroup.Checked = True, "Yes", "No")
                .Item(20) = IIf(ddlMajor.SelectedValue = "1", "Yes", "No")
                .Item(21) = IIf(rdEMark.Checked = True, "Mark", "Grade")
                .Item(22) = Val(txtCredit.Text)
                .Item(23) = Val(txtWeight.Text)
                .Item(24) = ddlLength.SelectedValue.ToString
                .Item(25) = ddlHr.SelectedValue.ToString
                .Item(26) = ddlGpa.SelectedValue.ToString
            End With

            Session("dtSubject") = dt

            GridBind(dt)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(13) <> "delete" And dt.Rows(i)(13) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvSubject.DataSource = dtTemp
        gvSubject.DataBind()
    End Sub
    Function SaveData() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim optAddIds As String = ""
        Dim optEditIds As String = ""
        Dim optDeleteIds As String = ""

        Dim dt As DataTable
        dt = Session("dtSubject")
        Dim dr As DataRow

        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If .Item(13) <> "update" And .Item(13) <> "remove" Then
                            If .Item(13) = "edit" Or .Item(13) = "delete" Then
                                UtilityObj.InsertAuditdetails(transaction, .Item(3), "SUBJECTS_GRADE_S", "SBG_ID", "SBG_SBM_ID", "SBG_ID=" + .Item(0).ToString)
                            End If
                            str_query = "exec saveSUBJECTGRADE_M " + .Item(0) + "," _
                                        & "'" + Session("sBsuid") + "'," _
                                        & hfACD_ID.Value.ToString + "," _
                                        & "'" + hfGRD_ID.Value.ToString + "'," _
                                        & hfSTM_ID.Value.ToString + "," _
                                        & .Item(1) + "," _
                                        & "'" + .Item(2) + "'," _
                                        & "'" + .Item(3) + "'," _
                                        & .Item(4) + "," _
                                        & IIf(.Item(6) = "Yes", "true", "false") + "," _
                                        & "'" + .Item(7) + "'," _
                                        & .Item(9) + "," _
                                        & IIf(.Item(11) = "Yes", "true", "false") + "," _
                                        & IIf(.Item(12) = "Yes", "true", "false") + "," _
                                        & .Item(15) + "," _
                                        & IIf(.Item(16) = "Yes", "true", "false") + "," _
                                        & "'" + .Item(8) + "'," _
                                        & "'" + .Item(10) + "'," _
                                        & "'" + .Item(17) + "'," _
                                        & IIf(.Item(18) = "Yes", "true", "false") + "," _
                                        & IIf(.Item(19) = "Yes", "true", "false") + "," _
                                        & IIf(.Item(20) = "Yes", "true", "false") + "," _
                                        & "'" + .Item(21) + "'," _
                                        & "'" + .Item(22) + "'," _
                                        & "'" + .Item(23) + "'," _
                                        & "'" + .Item(24) + "'," _
                                        & "'" + .Item(25) + "'," _
                                        & "'" + .Item(26) + "'," _
                                        & "'" + .Item(13) + "'"
                           

                            dr.Item(0) = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If .Item(13) = "add" Then
                                If optAddIds.Length <> 0 Then
                                    optAddIds += ","
                                End If
                                optAddIds += dr.Item(0)
                            End If

                            If .Item(13) = "update" Then
                                If optEditIds.Length <> 0 Then
                                    optEditIds += ","
                                End If
                                optEditIds += dr.Item(0)
                            End If

                            If .Item(13) = "remove" Then
                                If optDeleteIds.Length <> 0 Then
                                    optDeleteIds += ","
                                End If
                                optDeleteIds += dr.Item(0)
                            End If
                        End If
                    End With
                Next

                Dim flagAudit As Integer
                If optAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + optAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If optEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + optEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If optDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + optDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function

    Sub SaveCopyData()
        Dim dt As DataTable
        dt = Session("dtSubject")
        Dim dr As DataRow

        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ids As String = ""
        For Each dr In dt.Rows
            With dr
                If .Item(13) <> "update" And .Item(13) <> "remove" And .Item(0) = "0" Then
                    str_query = "exec saveSUBJECTGRADE_M " + .Item(0) + "," _
                                & "'" + Session("sBsuid") + "'," _
                                & hfACD_ID.Value.ToString + "," _
                                & "'" + hfGRD_ID.Value.ToString + "'," _
                                & hfSTM_ID.Value.ToString + "," _
                                & .Item(1) + "," _
                                & "'" + .Item(2) + "'," _
                                & "'" + .Item(3) + "'," _
                                & .Item(4) + "," _
                                & IIf(.Item(6) = "Yes", "true", "false") + "," _
                                & "'" + .Item(7) + "'," _
                                & .Item(9) + "," _
                                & IIf(.Item(11) = "Yes", "true", "false") + "," _
                                & IIf(.Item(12) = "Yes", "true", "false") + "," _
                                & .Item(15) + "," _
                                & IIf(.Item(16) = "Yes", "true", "false") + "," _
                                & "'" + .Item(8) + "'," _
                                & "'" + .Item(10) + "'," _
                                & "'" + .Item(17) + "'," _
                                & IIf(.Item(18) = "Yes", "true", "false") + "," _
                                & IIf(.Item(19) = "Yes", "true", "false") + "," _
                                & IIf(.Item(20) = "Yes", "true", "false") + "," _
                                & "'" + .Item(21) + "'," _
                                & "'" + .Item(22) + "'," _
                                & "'" + .Item(23) + "'," _
                                & "'" + .Item(24) + "'," _
                                & "'" + .Item(25) + "'," _
                                & "'" + .Item(26) + "'," _
                                & "'" + .Item(13) + "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Else
                    ids += "<ID><SBG_ID>" + .Item(0) + "</SBG_ID><SBG_PARENT_ID>" + .Item(9) + "</SBG_PARENT_ID></ID>"
                End If
            End With
        Next
        ids = "<IDS>" + ids + "</IDS>"
        str_query = "exec saveSUBJECTGRADE_COPY " _
                   & "'" + ids + "'," _
                   & hfACD_ID.Value + "," _
                   & "'" + hfGRD_ID.Value + "'," _
                   & hfSTM_ID.Value
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindCopyGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A INNER JOIN " _
                                & " GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID WHERE GRM_ACD_ID=" + hfACD_ID.Value + " AND  GRM_STM_ID ='" + ddlCopyStream.SelectedValue.ToString + "'" _
                                & " AND GRD_ID IN(SELECT SBG_GRD_ID FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + hfACD_ID.Value + ") " _
                                & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCopyGrade.DataSource = ds
        ddlCopyGrade.DataTextField = "GRM_DISPLAY"
        ddlCopyGrade.DataValueField = "GRD_ID"
        ddlCopyGrade.DataBind()
    End Sub
    Sub BindCopyStream()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID FROM STREAM_M  " _
                                & " WHERE STM_ID IN(SELECT SBG_STM_ID FROM SUBJECTS_GRADE_S WHERE" _
                                & " SBG_ACD_ID=" + hfACD_ID.Value + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCopyStream.DataSource = ds
        ddlCopyStream.DataTextField = "STM_DESCR"
        ddlCopyStream.DataValueField = "STM_ID"
        ddlCopyStream.DataBind()
    End Sub

    
    'Sub CopySubjects()
    '    Dim dt As DataTable
    '    Dim dr As DataRow
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT A.SBG_ID,A.SBG_SBM_ID,A.SBG_DESCR AS SBG_DESCR , " _
    '                                & " A.SBG_SHORTCODE,A.SBG_DPT_ID,DPT_DESCR,A.SBG_bOPTIONAL," _
    '                                & " ISNULL(A.SBG_OPT_ID,0),ISNULL(OPT_DESCR,''),A.SBG_PARENT_ID," _
    '                                & " ISNULL(B.SBG_DESCR,'NA') AS SBG_PARENT,A.SBG_bREPCRD_DISPLAY,A.SBG_bMRK_DISPLAY,SBM_DESCR,isnull(A.SBG_ORDER,0)" _
    '                                & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS E ON A.SBG_SBM_ID=E.SBM_ID LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON" _
    '                                & " A.SBG_PARENT_ID=B.SBG_SBM_ID AND B.SBG_ACD_ID=" + hfACD_ID.Value _
    '                                & " AND B.SBG_GRD_ID='" + ddlCopyGrade.SelectedValue.ToString + "'" _
    '                                & " AND B.SBG_STM_ID=" + ddlCopyStream.SelectedValue.ToString _
    '                                & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
    '                                & " LEFT OUTER JOIN OPTIONS_M AS D ON A.SBG_OPT_ID=D.OPT_ID" _
    '                                & " WHERE A.SBG_ACD_ID=" + hfACD_ID.Value + " AND A.SBG_GRD_ID='" + ddlCopyGrade.SelectedValue.ToString + "'" _
    '                                & " AND A.SBG_STM_ID=" + ddlCopyStream.SelectedValue.ToString
    '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '    dt = SetDataTable()
    '    While reader.Read
    '        dr = dt.NewRow
    '        With dr
    '            .Item(0) = "0"
    '            .Item(1) = reader.GetValue(1).ToString
    '            .Item(2) = reader.GetString(2)
    '            .Item(3) = reader.GetString(3)
    '            .Item(4) = reader.GetValue(4).ToString
    '            .Item(5) = reader.GetString(5)
    '            .Item(6) = IIf(reader.GetBoolean(6) = True, "Yes", "No")
    '            .Item(7) = reader.GetValue(7).ToString
    '            .Item(8) = reader.GetString(8)
    '            .Item(9) = reader.GetValue(9).ToString
    '            .Item(10) = reader.GetString(10)
    '            .Item(11) = IIf(reader.GetBoolean(11) = True, "Yes", "No")
    '            .Item(12) = IIf(reader.GetBoolean(12) = True, "Yes", "No")
    '            .Item(13) = "add"
    '            .Item(14) = reader.GetString(13)
    '            .Item(15) = reader.GetValue(14).ToString
    '        End With
    '        dt.Rows.Add(dr)
    '    End While
    '    gvSubject.DataSource = dt
    '    gvSubject.DataBind()

    '    EnableDisableControls(True)
    '    btnEdit.Text = "Add"
    '    ViewState("datamode") = "add"
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    '    Session("dtSubject") = dt


    'End Sub

    Sub CopySubjects()
        Dim dr As DataRow
        Dim dt As New DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
      
        'Dim str_query As String = "SELECT SBG_ID,SBG_SBM_ID,SBG_DESCR AS SBG_DESCR , " _
        '                    & " A.SBG_SHORTCODE,A.SBG_DPT_ID,DPT_DESCR,A.SBG_bOPTIONAL," _
        '                    & " OPT_IDS=ISNULL((SELECT SGO_OPT_ID FROM SUBJECTGRADE_OPTIONS_S WHERE SGO_SBG_ID=A.SBG_ID FOR XML PATH),'<ID><SGO_OPT_ID>0</SGO_OPT_ID></ID>' ) ," _
        '                    & " ISNULL(A.SBG_OPTIONS,''),SBG_PARENT_ID," _
        '                    & " ISNULL(SBG_PARENTS,'NA'),SBG_bREPCRD_DISPLAY,SBG_bMRK_DISPLAY," _
        '                    & " SBM_DESCR,ISNULL(A.SBG_ORDER,0),ISNULL(A.SBG_bTC_DISPLAY,'FALSE'),ISNULL(SBG_PARENTS_SHORT,'NA')" _
        '                    & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS E ON A.SBG_SBM_ID=E.SBM_ID " _
        '                    & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
        '                    & " WHERE A.SBG_ACD_ID=" + hfACD_ID.Value + " AND A.SBG_GRD_ID='" + ddlCopyGrade.SelectedValue.ToString + "'" _
        '                    & " AND A.SBG_STM_ID=" + ddlCopyStream.SelectedValue.ToString + " ORDER BY A.SBG_DESCR"



        Dim str_query As String = "SELECT SBG_ID,SBG_SBM_ID,SBG_DESCR AS SBG_DESCR , " _
                              & " A.SBG_SHORTCODE,A.SBG_DPT_ID,DPT_DESCR,A.SBG_bOPTIONAL," _
                              & " OPT_IDS=ISNULL((SELECT SGO_OPT_ID FROM SUBJECTGRADE_OPTIONS_S WHERE SGO_SBG_ID=A.SBG_ID FOR XML PATH),'<ID><SGO_OPT_ID>0</SGO_OPT_ID></ID>' ) ," _
                              & " ISNULL(A.SBG_OPTIONS,''),SBG_PARENT_ID," _
                              & " ISNULL(SBG_PARENTS,'NA'),SBG_bREPCRD_DISPLAY,SBG_bMRK_DISPLAY," _
                              & " SBM_DESCR,ISNULL(A.SBG_ORDER,0),ISNULL(A.SBG_bTC_DISPLAY,'FALSE'),ISNULL(SBG_PARENTS_SHORT,'NA'),ISNULL(SBG_bCOREEXT,'FALSE'),ISNULL(SBG_bAUTOGROUP,'FALSE'),ISNULL(SBG_bMAJOR,'TRUE'),ISNULL(SBG_ENTRYTYPE,'Mark')" _
                               & " ,isnull(SBG_CREDITS,0), isnull(SBG_WT,0), isnull(SBG_LEN,''), isnull(SBG_HRS,''), isnull(SBG_GPA,'')" _
                              & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS E ON A.SBG_SBM_ID=E.SBM_ID " _
                              & " INNER JOIN DEPARTMENT_M AS C ON A.SBG_DPT_ID=C.DPT_ID" _
                              & " WHERE A.SBG_ACD_ID=" + hfACD_ID.Value + " AND A.SBG_GRD_ID='" + ddlCopyGrade.SelectedValue.ToString + "'" _
                              & " AND A.SBG_STM_ID=" + ddlCopyStream.SelectedValue.ToString + " ORDER BY A.SBG_DESCR"


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        dt = SetDataTable()
        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = reader.GetValue(0).ToString
                .Item(1) = reader.GetValue(1).ToString
                .Item(2) = reader.GetString(2)
                .Item(3) = reader.GetString(3)
                .Item(4) = reader.GetValue(4).ToString
                .Item(5) = reader.GetString(5)
                .Item(6) = IIf(reader.GetBoolean(6) = True, "Yes", "No")
                .Item(7) = "<IDS>" + reader.GetString(7).ToString.Replace("row", "ID") + "</IDS>"
                .Item(8) = reader.GetString(8)
                .Item(9) = reader.GetValue(9).ToString
                .Item(10) = reader.GetString(10)
                .Item(11) = IIf(reader.GetBoolean(11) = True, "Yes", "No")
                .Item(12) = IIf(reader.GetBoolean(12) = True, "Yes", "No")
                .Item(13) = "add"
                .Item(14) = reader.GetString(13)
                .Item(15) = reader.GetValue(14).ToString
                .Item(16) = IIf(reader.GetBoolean(15) = True, "Yes", "No")
                .Item(17) = reader.GetString(16)
                .Item(18) = IIf(reader.GetBoolean(17) = True, "Yes", "No")
                .Item(19) = IIf(reader.GetBoolean(18) = True, "Yes", "No")
                .Item(20) = IIf(reader.GetBoolean(19) = True, "Yes", "No")
                .Item(21) = reader.GetString(20)
                .Item(22) = reader.GetValue(21).ToString
                .Item(23) = reader.GetValue(22).ToString
                .Item(24) = reader.GetString(23)
                .Item(25) = reader.GetString(24)
                .Item(26) = reader.GetString(25)
            End With
            dt.Rows.Add(dr)
        End While
        gvSubject.DataSource = dt
        gvSubject.DataBind()
     
        Dim i As Integer
        For i = 0 To gvSubject.Rows.Count - 1
            gvSubject.Rows(i).Cells(12).Enabled = False
        Next

        EnableDisableControls(True)
        btnEdit.Text = "Add"
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Session("dtSubject") = dt
    End Sub
    Private Sub PopulateSubjects()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SBM_ID,SBM_DESCR,isnull(SBM_SHORTCODE,'') FROM SUBJECT_M AS A" _
                                     & " INNER JOIN SYLLABUS_M AS B ON A.SBM_ID=B.SBS_SBM_ID " _
                                     & " WHERE SBS_CLM_ID=" + Session("CLM").ToString + " ORDER BY SBM_DESCR "
                          
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "SBM_DESCR"
            ddlSubject.DataValueField = "SBM_ID"
            ddlSubject.DataBind()

            Dim shortCodes As New ArrayList

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While rd.Read
                shortCodes.Add(rd.GetString(2))
            End While
            rd.Close()

            Session("ShortCodes") = shortCodes
            txtShortCode.Text = shortCodes.Item(0)
            txtDescr.Text = ddlSubject.SelectedItem.Text
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PopulateSubjects")
        End Try
    End Sub
    Sub PopulateParentSubjectTree()
        Dim ds As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT A.SBG_ID AS SBG_ID,A.SBG_DESCR AS SBG_DESCR ,A.SBG_SHORTCODE AS SBG_SHORTCODE,CASE A.SBG_PARENT_ID " _
                                  & " WHEN 0 THEN NULL ELSE B.SBG_ID END AS PARENT_ID  FROM SUBJECTS_GRADE_S AS A " _
                                  & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON A.SBG_PARENT_ID=B.SBG_ID AND A.SBG_GRD_ID=B.SBG_GRD_ID " _
                                  & " AND A.SBG_STM_ID=B.SBG_STM_ID AND A.SBG_ACD_ID=B.SBG_ACD_ID " _
                                  & " WHERE A.SBG_GRD_ID='" + hfGRD_ID.Value + "' AND A.SBG_STM_ID =" + hfSTM_ID.Value + " AND A.SBG_ACD_ID=" + hfACD_ID.Value

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ds.DataSetName = "SUBJECTS"
        ds.Tables(0).TableName = "SUBJECT"
        Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("SUBJECT").Columns("SBG_ID"), ds.Tables("SUBJECT").Columns("PARENT_ID"), True)
        relation.Nested = True
        ds.Relations.Add(relation)
        XmlSubjects.Data = ds.GetXml()
        tvParentSubjects.DataBind()

    End Sub

    'Function GetParent() As String
    '    Dim node As TreeNode
    '    Dim parents As String = ""
    '    Dim parentName As String = "NA"
    '    Dim parentId As String = "0"
    '    Dim parentTag As String = ""
    '    Dim shortCodeTag As String = ""
    '    Dim shortCode As String = "NA"
    '    Dim parentIds As String()

    '    Dim i As Integer

    '    For Each node In tvParentSubjects.CheckedNodes
    '        parents = node.ValuePath
    '        parentName = node.Text
    '        parentId = node.Value
    '        shortCode = node.ToolTip
    '    Next
    '    If parents.IndexOf("/") <> -1 Then
    '        While parents.IndexOf("/") <> -1
    '            parents = parents.Remove(parents.LastIndexOf("/"))
    '            If tvParentSubjects.FindNode(parents).Text = ddlSubject.SelectedItem.Text Then
    '                Return "Error"
    '            End If
    '        End While
    '    Else
    '        If parentName = ddlSubject.SelectedItem.Text Then
    '            Return "Error"
    '        End If
    '    End If


    '    'If parents.IndexOf("/") <> -1 Then
    '    '    parentIds = parents.Split("/")
    '    '    For i = 0 To parentIds.Length - 1
    '    '        If parentIds(i) = ddlSubject.SelectedValue.ToString Then
    '    '            Return "Error"
    '    '        End If
    '    '    Next
    '    'Else
    '    '    If parentName = ddlSubject.SelectedItem.Text Then
    '    '        Return "Error"
    '    '    End If
    '    'End If

    '    'Get the parent tag for the subject (if there are any parents for this subject)
    '    While parents.IndexOf("/") <> -1
    '        parents = parents.Remove(parents.LastIndexOf("/"))
    '        If parentTag <> "" Then
    '            parentTag += "-"
    '            shortCodeTag += "-"
    '        End If
    '        parentTag += tvParentSubjects.FindNode(parents).Text
    '        shortCodeTag += tvParentSubjects.FindNode(parents).ToolTip
    '    End While

    '    If parentTag <> "" Then
    '        parentName = parentTag + "-" + parentName
    '        shortCode = shortCodeTag + "-" + shortCode
    '    End If


    '    Return parentName + "|" + parentId + "|" + shortCode

    'End Function

    Function GetParent() As String
        Dim node As TreeNode
        Dim parent As String = ""
        Dim parents As String()
        Dim parentName As String = "NA"
        Dim parentId As String = "0"
        Dim parentTag As String = ""
        Dim shortCodeTag As String = ""
        Dim shortCode As String = "NA"
        Dim pStr As String = ""

        Dim i As Integer

        For Each node In tvParentSubjects.CheckedNodes
            parent = node.ValuePath
            parentName = node.Text
            parentId = node.Value
            shortCode = node.ToolTip
        Next

        If parent.IndexOf("/") <> -1 Then
            parents = parent.Split("/")
            For i = 0 To parents.Length - 2
                If pStr <> "" Then
                    pStr += "/"
                End If
                pStr += parents(i)
                If tvParentSubjects.FindNode(pStr).Text = ddlSubject.SelectedItem.Text Then
                    Return "Error"
                End If

                If parentTag <> "" Then
                    parentTag += "-"
                    shortCodeTag += "-"
                End If
                parentTag += tvParentSubjects.FindNode(pStr).Text
                shortCodeTag += tvParentSubjects.FindNode(pStr).ToolTip
            Next
        Else
            If parentName = ddlSubject.SelectedItem.Text Then
                Return "Error"
            End If
        End If

        'Get the parent tag for the subject (if there are any parents for this subject)
        'While parents.IndexOf("/") <> -1
        '    parents = parents.Remove(parents.LastIndexOf("/"))
        '    If parentTag <> "" Then
        '        parentTag += "-"
        '        shortCodeTag += "-"
        '    End If
        '    parentTag += tvParentSubjects.FindNode(parents).Text
        '    shortCodeTag += tvParentSubjects.FindNode(parents).ToolTip
        'End While

        If parentTag <> "" Then
            parentName = parentTag + "-" + parentName
            shortCode = shortCodeTag + "-" + shortCode
        End If


        Return parentName + "|" + parentId + "|" + shortCode

    End Function



    Private Sub SelectParent(ByVal searchValue As Integer)
        tvParentSubjects.CollapseAll()
        For Each tn As TreeNode In tvParentSubjects.Nodes
            If tn.Value = searchValue Then
                tn.Expand()
                tn.Checked = True

                Exit For
            End If

            If tn.ChildNodes.Count > 0 Then
                For Each cTn As TreeNode In tn.ChildNodes
                    recurseChildren(cTn, searchValue)
                Next
            End If
        Next

    End Sub



    Private Sub recurseChildren(ByVal tn As TreeNode, ByVal searchValue As Integer)
        If tn.Value = searchValue Then
            tn.Expand()
            ExpandParent(tn)
            tn.Checked = True
            Exit Sub
        End If

        If tn.ChildNodes.Count > 0 Then
            For Each tnC As TreeNode In tn.ChildNodes
                recurseChildren(tnC, searchValue)
            Next
        End If

    End Sub

    Sub ExpandParent(ByVal tn As TreeNode)
        Dim valPath As String = tn.ValuePath
        While valPath.IndexOf("/") <> -1
            valPath = valPath.Remove(valPath.LastIndexOf("/"))
            tvParentSubjects.FindNode(valPath).Expand()
        End While
    End Sub

    'Private Sub PopulateParentSubjects()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim str_query As String = "SELECT SBM_ID,DESCR=CASE WHEN SBG_DESCR IS NULL THEN SBM_DESCR ELSE SBG_DESCR END" _
    '                                 & ",isnull(SBM_SHORTCODE,'') FROM SUBJECT_M AS A" _
    '                                 & " INNER JOIN SYLLABUS_M AS C ON A.SBM_ID=SBS_SBM_ID AND C.SBS_CLM_ID=" + Session("CLM").ToString _
    '                                 & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON A.SBM_ID=B.SBG_SBM_ID " _
    '                                 & " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND SBG_ACD_ID=" + hfACD_ID.Value _
    '                                 & " AND SBG_STM_ID=" + hfSTM_ID.Value + " ORDER BY SBM_DESCR "

    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        ddlParentSubj.DataSource = ds
    '        ddlParentSubj.DataTextField = "DESCR"
    '        ddlParentSubj.DataValueField = "SBM_ID"
    '        ddlParentSubj.DataBind()

    '        Dim li As New ListItem
    '        li.Text = "NA"
    '        li.Value = "0"
    '        ddlParentSubj.Items.Insert(0, li)
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "PopulateSubjects")
    '    End Try
    'End Sub

    Sub BindDepartment()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DPT_ID,DPT_DESCR FROM DEPARTMENT_M ORDER BY DPT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDPT.DataSource = ds
        ddlDPT.DataTextField = "DPT_DESCR"
        ddlDPT.DataValueField = "DPT_ID"
        ddlDPT.DataBind()
    End Sub

    Sub BindOptions()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT OPT_ID,OPT_DESCR FROM OPTIONS_M WHERE OPT_BSU_ID='" + Session("sbsuid") + "' ORDER BY OPT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstOptions.DataSource = ds
        lstOptions.DataTextField = "OPT_DESCR"
        lstOptions.DataValueField = "OPT_ID"
        lstOptions.DataBind()
    End Sub

    'Function CheckParent() As Boolean
    'If ddlParentSubj.SelectedValue = 0 Then
    '    Return True
    'End If
    'Dim dt As DataTable = Session("dtSubject")
    'Dim flag As Boolean = False
    'Dim dr As DataRow
    'For Each dr In dt.Rows
    '    If dr.Item(1) = ddlParentSubj.SelectedValue Then
    '        flag = True
    '        Exit For
    '    End If
    'Next
    'If flag = False Then
    '    lblError.Text = "Please add the subject " + ddlParentSubj.SelectedItem.Text _
    '                    & " before assigning it as a parent to subject " + txtDescr.Text
    '    Return False
    'End If
    'Return True
    'End Function

    Sub ClearControls()
        'ddlSubject.ClearSelection()
        'ddlSubject.Items(0).Selected = True
        'ddlParentSubj.ClearSelection()
        'ddlParentSubj.Items(0).Selected = True

        chkOpt.Checked = False
        ' chkMrk.Checked = False
        chkSub.Checked = False
        chkTC.Checked = False
        chkCE.Checked = False
        chkGroup.Checked = False
        lstOptions.ClearSelection()
        lstOptions.Enabled = False
        Dim shortCodes As New ArrayList
        shortCodes = Session("shortCodes")
        chkGroup.Enabled = True

        'txtShortCode.Text = shortCodes(0)
    End Sub
#End Region


    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            CopySubjects()
            hfCopy.Value = 1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If


            For Each node As TreeNode In tvParentSubjects.CheckedNodes
                node.Checked = False
                Exit For
            Next
            tvParentSubjects.CollapseAll()


            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            ClearControls()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EnableDisableControls(True)
        ViewState("datamode") = "edit"
        ClearControls()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            If gvSubject.Enabled = False Then
                Response.Redirect(ViewState("ReferrerUrl"))
                Exit Sub
            End If
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                txtDescr.Text = ""

                EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "add" Then
                    Session("dtOption") = SetDataTable()
                    gvSubject.DataSource = Session("dtOption")
                    gvSubject.DataBind()
                End If
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            txtDescr.Text = ddlSubject.SelectedItem.Text
            Dim shortCodes As New ArrayList
            shortCodes = Session("shortCodes")
            txtShortCode.Text = shortCodes(ddlSubject.SelectedIndex)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvSubject_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSubject.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvSubject.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtSubject")

            Dim lblsbmId As Label
            Dim lblsbgParentId As Label
            Dim lblsbgParent As Label

            lblsbmId = selectedRow.FindControl("lblsbmId")
            lblsbgParentId = selectedRow.FindControl("lblsbgParentId")
            lblsbgParent = selectedRow.FindControl("lblsbgParent")

            ReDim keys(2)

            keys(0) = lblsbmId.Text
            keys(1) = lblsbgParentId.Text
            keys(2) = lblsbgParent.Text

            index = dt.Rows.IndexOf(dt.Rows.Find(keys))



            If e.CommandName = "view" Then
                ViewState("SelectedRow") = index
                btnAddNew.Text = "Update"
                Dim li As New ListItem

                With dt.Rows(index)
                    li.Value = .Item(1)
                    li.Text = .Item(14)
                    ddlSubject.ClearSelection()
                    ddlSubject.Items(ddlSubject.Items.IndexOf(li)).Selected = True
                    txtDescr.Text = .Item(2)
                    txtShortCode.Text = .Item(3)

                    li = New ListItem
                    li.Value = .Item(4)
                    li.Text = .Item(5)
                    ddlDPT.ClearSelection()
                    ddlDPT.Items(ddlDPT.Items.IndexOf(li)).Selected = True
                    chkOpt.Checked = IIf(.Item(6) = "Yes", True, False)

                    If chkOpt.Checked = True Then
                        lstOptions.ClearSelection()
                        Listoptions(.Item(7))
                        lstOptions.Enabled = True
                    Else
                        lstOptions.ClearSelection()
                        lstOptions.Enabled = False
                    End If

                    'li = New ListItem
                    'li.Value = .Item(9)
                    'li.Text = .Item(10)
                    ' ddlParentSubj.ClearSelection()
                    'ddlParentSubj.Items(ddlParentSubj.Items.IndexOf(li)).Selected = True
                    SelectParent(.Item(9))

                    chkSub.Checked = IIf(.Item(11) = "Yes", True, False)

                    '   chkMrk.Checked = IIf(.Item(12) = "Yes", True, False)
                    If .Item(12) = "Yes" Then
                        rdMarks.Checked = True
                        rdGrade.Checked = False
                    Else
                        rdMarks.Checked = False
                        rdGrade.Checked = True
                    End If

                    chkTC.Checked = IIf(.Item(16) = "Yes", True, False)
                    chkCE.Checked = IIf(.Item(18) = "Yes", True, False)
                    chkGroup.Checked = IIf(.Item(19) = "Yes", True, False)
                    ddlMajor.ClearSelection()
                    If .Item(20) = "Yes" Then
                        ddlMajor.Items(0).Selected = True
                    Else
                        ddlMajor.Items(1).Selected = True
                    End If

                    If .Item(21) = "Mark" Then
                        rdEMark.Checked = True
                        rdEgrade.Checked = False
                    Else
                        rdEgrade.Checked = True
                        rdEMark.Checked = False
                    End If

                    txtCredit.Text = .Item(22)
                    txtWeight.Text = .Item(23)

                    If Not ddlLength.Items.FindByValue(.Item(24)) Is Nothing Then
                        ddlLength.ClearSelection()
                        ddlLength.Items.FindByValue(.Item(24)).Selected = True
                    End If

                    If Not ddlHr.Items.FindByValue(.Item(25)) Is Nothing Then
                        ddlHr.ClearSelection()
                        ddlHr.Items.FindByValue(.Item(25)).Selected = True
                    End If

                    If Not ddlGpa.Items.FindByValue(.Item(26)) Is Nothing Then
                        ddlGpa.ClearSelection()
                        ddlGpa.Items.FindByValue(.Item(26)).Selected = True
                    End If
                End With

                GridBind(dt)

            ElseIf e.CommandName = "delete" Then
                If dt.Rows(index).Item(13) = "add" Then
                    dt.Rows(index).Item(13) = "remove"
                Else
                    dt.Rows(index).Item(13) = "delete"
                End If
                Session("dtSubject") = dt
                GridBind(dt)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvSubject_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSubject.RowDeleting

    End Sub

    Protected Sub gvSubject_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSubject.RowEditing

    End Sub

    Protected Sub chkOpt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOpt.CheckedChanged
        Try
            If chkOpt.Checked = True Then
                lstOptions.Enabled = True
                chkGroup.Enabled = False
                chkGroup.Checked = False
            Else
                lstOptions.ClearSelection()
                lstOptions.Enabled = False
                chkGroup.Enabled = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If hfCopy.Value = 1 Then
                SaveCopyData()
                Session("dtSubject") = SetDataTable()
                GetRows()
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                EnableDisableControls(False)
                ' PopulateParentSubjects()
                PopulateParentSubjectTree()
                BindCopyStream()
                BindCopyGrade()

                If ddlCopyGrade.Items.Count = 0 Or ddlCopyStream.Items.Count = 0 Then
                    ddlCopyGrade.Enabled = False
                    ddlCopyStream.Enabled = False
                    btnCopy.Enabled = False
                Else
                    ddlCopyGrade.Enabled = True
                    ddlCopyStream.Enabled = True
                    btnCopy.Enabled = True
                End If
                ClearControls()
                hfCopy.Value = 0
            Else
                If SaveData() = True Then
                    Session("dtSubject") = SetDataTable()
                    GetRows()
                    ViewState("datamode") = "view"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    EnableDisableControls(False)
                    ' PopulateParentSubjects()
                    PopulateParentSubjectTree()
                    BindCopyStream()
                    BindCopyGrade()

                    If ddlCopyGrade.Items.Count = 0 Or ddlCopyStream.Items.Count = 0 Then
                        ddlCopyGrade.Enabled = False
                        ddlCopyStream.Enabled = False
                        btnCopy.Enabled = False
                    Else
                        ddlCopyGrade.Enabled = True
                        ddlCopyStream.Enabled = True
                        btnCopy.Enabled = True
                    End If
                    ClearControls()
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub rdMarks_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdMarks.CheckedChanged
        If rdMarks.Checked = True Then
            rdEMark.Checked = True
            rdEgrade.Checked = False
            rdEgrade.Enabled = False
        Else
            rdEgrade.Enabled = True
        End If
    End Sub

    
    Protected Sub rdGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGrade.CheckedChanged
        If rdGrade.Checked = False Then
            rdEMark.Checked = True
            rdEgrade.Checked = False
            rdEgrade.Enabled = False
        Else
            rdEgrade.Enabled = True
        End If
    End Sub

    Protected Sub ddlCopyGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCopyGrade.SelectedIndexChanged
        'BindCopyStream()
    End Sub
    Protected Sub ddlCopyStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCopyStream.SelectedIndexChanged
        BindCopyGrade()
    End Sub

End Class
