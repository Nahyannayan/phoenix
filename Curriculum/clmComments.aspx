<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmComments.aspx.vb" Inherits="Curriculum_clmComments" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function InsertText() {
            var textBoxControl = document.getElementById('<%=ddlSymbols.ClientID %>').value;
        return InsertTextAtCursor(textBoxControl);
    }

    function InsertTextAtCursor(con) {
        var txt = document.getElementById('<%= txtComments.ClientID %>');
        var mosPos = 0;

        if (document.selection) {
            txt.focus();
            var ran = document.selection.createRange();
            ran.text = con;
        }
        else if (txt.selectionStart != null) {
            mosPos = txt.selectionStart;
            var strFirst = txt.value.substring(0, mosPos);
            var strLast = txt.value.substring(mosPos);
            if (txt.value == "") {
                txt.value = con;
            }
            else {
                txt.value = strFirst + con + strLast;
            }
        }
    }

    function confirm_delete() {

        if (confirm("You are about to delete this record.Do you want to proceed?") == true)
            return true;
        else
            return false;

    }
    function NewCategory() {
        var sFeatures;
        sFeatures = "dialogWidth: 350px; ";
        sFeatures += "dialogHeight: 250px; ";
        var NameandCode;
        var result;
        var type;
        //result = window.showModalDialog("showTopics.aspx?syllabusId="+syllabusId,"", sFeatures)
        if (document.getElementById('<%=radGeneralComments.ClientID %>').checked == true) {
            if (document.getElementById('<%=chkCatByGrade.ClientID %>').checked == true) {
                result = window.showModalDialog("ClmCategory.aspx?TYP=GEN&Grade=" + document.getElementById('<%=ddlGrade0.ClientID %>').value + "", "", sFeatures)
            }
            else {
                result = window.showModalDialog("ClmCategory.aspx?TYP=GEN&Grade=0", "", sFeatures)
            }
        }
        else {
            result = window.showModalDialog("ClmCategory.aspx?TYP=SUB&Grade=" + document.getElementById('<%=ddlGrade.ClientID %>').value + "", "", sFeatures)
        }
        return false;
    }
    function GetComments() {
        var sFeatures;
        sFeatures = "dialogWidth: 429px; ";
        sFeatures += "dialogHeight: 375px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

        if (document.getElementById('<%=radGeneralComments.ClientID %>').checked == true) {
                if (document.getElementById('<%=chkCatByGrade.ClientID %>').checked == true) {
                    result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT&byGrade=" + document.getElementById('<%=chkCatByGrade.ClientID %>').checked + "&Grade=" + document.getElementById('<%=ddlGrade0.ClientID %>').value + "", "", sFeatures)
                }
                else {
                    result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT&byGrade=false&Grade=0", "", sFeatures)
                }
            }
            else {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT&byGrade=true&Grade=" + document.getElementById('<%=ddlGrade.ClientID %>').value + "", "", sFeatures)
            }
        //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=CMTSCAT&byGrade="+ document.getElementById('<%=chkCatByGrade.ClientID %>').checked + "&Grade=" +  document.getElementById('<%=ddlGrade.ClientID %>').value +"","", sFeatures)            
        if (result != '' && result != undefined) {
            document.getElementById('<%=H_CAT_ID.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

        function HideRows() {
            if (document.getElementById('<%=radGeneralComments.ClientID %>').checked = 1) {
                 document.getElementById('<%=radGeneralComments.ClientID %>').checked = 'true';
                document.getElementById('row1').style.display = 'none';
                document.getElementById('row2').style.display = 'none'; alert()
            }
             //            else
             //            {
             //            
             //            alert('2')
             //            document.getElementById('row1').style.display='';
             //            document.getElementById('row2').style.display='';
             //            onclick='javascript:HideRows();return false;'
             //            }

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Set Comments
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">
                    <tr style="display: none">
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%" cellpadding="5" cellspacing="0" id="tabmain">
                                <tr id="trCommentType">
                                    <td align="left" colspan="4">
                                        <span class="field-label">Comment type</span>
                                        <asp:RadioButton ID="radCatBySubject" runat="server" GroupName="Category" Text="By Subject"
                                            AutoPostBack="True" Checked="True" CssClass="field-label" />
                                        <asp:RadioButton ID="radGeneralComments" runat="server" GroupName="Category" Text="General Comments"
                                            AutoPostBack="True" CssClass="field-label" />
                                        <asp:CheckBox ID="chkAOLcomment" runat="server" Text="AOL Comment" CssClass="field-label" />
                                    </td>
                                </tr>
                                <tr id="trReportCard" runat="server">
                                    <td align="left" width="20%">
                                        <span class="field-label">Report Card</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Grade</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="row1" class="title-bg">
                                    <td align="left" colspan="4" class="title-bg">
                                        <asp:Label ID="lblSubHeader" runat="server" CssClass="field-label" Text="Category By Subject"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trSubjects" runat="server">
                                    <td align="left" valign="top">
                                        <span class="field-label">Subjects</span>
                                    </td>
                                    <td align="left" >
                                        <div align="left" class="checkbox-list">
                                            <asp:CheckBoxList ID="ddlSubject" runat="server" AutoPostBack="True" >
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Category</span>
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkCatByGrade" runat="server" GroupName="Category" Text="By Grade"
                                            AutoPostBack="True" />
                                        <asp:DropDownList ID="ddlGrade0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lnkCategory" runat="server">Create new</asp:LinkButton>
                                    </td>
                                </tr>
                               
                                <tr id="trReportHeaders" runat="server">
                                    <td align="left">
                                        <span class="field-label">Report Headers</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <div align="left">
                                            <asp:CheckBoxList ID="chkHeader" runat="server" AutoPostBack="True" OnSelectedIndexChanged="chkHeader_SelectedIndexChanged1"
                                                RepeatColumns="3" RepeatDirection="Horizontal">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="field-label">Comments</span>
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large"
                                            ForeColor="Red" Height="12px" Text="*" Width="4px"></asp:Label>
                                    </td>
                                    <td colspan="3" align="left">
                                        <table width="100%">
                                            <tr>
                                                <td rowspan="2">
                                                    <asp:TextBox ID="txtComments" runat="server" TabIndex="1" TextMode="MultiLine"></asp:TextBox><br />
                                                </td>
                                                <td align="center">
                                                    <span class="field-label">Insert Symbols</span>
                                                    <asp:DropDownList ID="ddlSymbols" runat="server">
                                                        <asp:ListItem Value="&amp;&amp;Name&amp;&amp;">Student Name</asp:ListItem>
                                                        <asp:ListItem Value="&amp;&amp;His/Her&amp;&amp;">His/Her</asp:ListItem>
                                                        <asp:ListItem Value="&amp;&amp;He/She&amp;&amp;">He/She</asp:ListItem>
                                                        <asp:ListItem Value="&amp;&amp;Him/Her&amp;&amp;">Him/Her</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnInsert" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Insert" UseSubmitBehavior="False" TabIndex="8" OnClientClick="return  InsertText();" />
                                                    <telerik:RadSpell AjaxUrl="~/Telerik.Web.UI.SpellCheckHandler.axd" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd" ID="RadSpell1" runat="server" ControlToCheck="txtComments" SupportedLanguages="en-US,English" CssClass="button" />
                                                </td>
                                            </tr>                                         
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                            TabIndex="7" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                                <tr class="subheader_img">
                                    <td align="left" colspan="4">
                                        <span class="field-label">Comments List</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvComments" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="15" Width="100%" OnPageIndexChanging="gvComments_PageIndexChanging">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="cmtId" runat="server" Text='<%# bind("CMT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SubjectID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubId" runat="server" Text='<%# Bind("CMT_SBG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GradeId" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGradeId" runat="server" Text='<%# Bind("CMT_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# bind("SBG_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Report Header">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRsd" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CAT_DESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comments">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblclm" runat="server" Text='<%# Bind("CMT_COMMENTS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Edit" Text="Edit" HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("CMT_ID") %>'
                                                            CommandName="delete" Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected comment will be deleted permanently.Are you sure you want to continue?" runat="server"></ajaxToolkit:ConfirmButtonExtender>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="H_CAT_ID" runat="server" />
                            <asp:HiddenField ID="hfPos1" runat="server" Value="0" />
                            <asp:HiddenField ID="hfPos2" runat="server" Value="0" />
                            <asp:HiddenField ID="H_CMT_ID" runat="server" />

                            <asp:RequiredFieldValidator ID="rfsGrade" runat="server" ErrorMessage="Please enter the field Grade"
                                ControlToValidate="ddlGrade" Display="None" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="frsComments" runat="server" ErrorMessage="Please enter the field Comments"
                                ControlToValidate="txtComments" Display="None" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfsHeader" runat="server" ErrorMessage="Please select the report header"
                                ControlToValidate="ddlReport" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>



                <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server"
                    BackgroundCssClass="modalBackground" PopupControlID="Panel1" TargetControlID="lnkCategory">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" Width="500px" CssClass="panel-cover">

                    <table style="width: 100%" class="BlueTable">
                        <tr>
                            <td align="left" valign="middle" colspan="2">
                                <h5>Comments Category</h5>
                            </td>
                        </tr>
                        <tr id="trcatGrade" runat="server">
                            <td align="left"></td>
                            <td align="left">
                                <asp:CheckBox ID="chkGrade" runat="server" CssClass="field-label" Text="Grade Wise Category" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblH" runat="server" CssClass="field-label" Text="Category Name"></asp:Label>
                                <asp:Label ID="lbl2" runat="server" ForeColor="Red"
                                    Text="*"></asp:Label></td>
                            <td align="left" colspan="4">
                                <asp:TextBox ID="txtCategory" runat="server"  SkinID="TextBoxNormal"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSaveCategory" runat="server" CssClass="button"
                                    TabIndex="7" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="8" Text="Close"
                                    />
                            </td>
                        </tr>

                    </table>
                </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>
