<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmRetestMarks.aspx.vb" Inherits="Curriculum_clmRetestMarks" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Retest Marks</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" width="100%" align="center" border="0" cellpadding="0"
                    cellspacing="0">


                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">


                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>

                                    <td align="left"><span class="field-label">Select Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left"><span class="field-label">Select Section</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Student Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>

                                </tr>
                                           <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>

                     
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>

                                    <td align="center" colspan="4" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Retest Result" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlResult" Enabled="false" runat="server" AutoPostBack="True" SkinID="smallcmb">
                                                            <asp:ListItem Value="Pass">Pass</asp:ListItem>
                                                            <asp:ListItem Value="Fail">Fail</asp:ListItem>
                                                            <asp:ListItem Value="Retest">Retest</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Retest Marks" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:DataList runat="server" ID="dlMarks" RepeatDirection="Vertical" Enabled="false">
                                                            <ItemTemplate>
                                                                <table class="table table-bordered table-row" width="100%">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:Label runat="server" Text='<%# Bind("Subject") %>' ID="lblSubject"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Marks</td>
                                                                        <td>Grade</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox runat="server" Text='<%# Bind("Marks") %>' ID="txtMark"></asp:TextBox>
                                                                            <asp:Label runat="server" Text='<%# Bind("Sbg_ID") %>' ID="lblSbgId" Visible="False" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" Text='<%# Bind("Grade") %>' ID="txtGrade"></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>


</asp:Content>

