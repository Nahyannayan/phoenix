﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmMarkEntry_DynamicAOL.aspx.vb" Inherits="Curriculum_clmMarkEntry_DynamicAOL" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function copyData() {
            var e = document.getElementById("<%=ddlHeader.ClientID %>");

            var strVal = e.options[e.selectedIndex].value;

            var copyText = document.getElementById("<%=txtHeader.ClientID %>").value;
            var i;
            if (strVal == 'CH') {

                for (i = 0; i < txtCH.length; i++) {
                    document.getElementById(txtCH[i]).value = copyText;
                }
            }
            else {
                for (i = 0; i < txtFB.length; i++) {
                    document.getElementById(txtFB[i]).value = copyText;

                }
            }

            return false;

        }




        function getcomments(ctlid, code, header, stuid, rsmid) {
            var sFeatures;
            sFeatures = "dialogWidth: 530px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            if (code == 'ALLCMTS') {
                //result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code +"","", sFeatures);
                result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);
                if (result != '' && result != undefined) {
                    document.getElementById(ctlid).value = result.replace(/_#_/g, "'");
                    //result.split('___')[0]
                    //document.getElementById(ctlid.split(';')[1]).value=varray[0];
                }
                else {
                    return false;
                }
            }

        }

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
           if (color == '') {
               color = getRowColor();
           }
           if (obj.checked) {
               rowObject.style.backgroundColor = '#f6deb2';
           }
           else {
               rowObject.style.backgroundColor = '';
               color = '';
           }
           // private method

           function getRowColor() {
               if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
               else return rowObject.style.backgroundColor;
           }
       }
       // This method returns the parent row of the object
       function getParentRow(obj) {
           do {
               obj = obj.parentElement;
           }
           while (obj.tagName != "TR")
           return obj;
       }


       // This method returns the parent row of the object
       function getParentRow(obj) {
           do {
               obj = obj.parentElement;
           }
           while (obj.tagName != "TR")
           return obj;
       }

       function change_chk_state(chkThis) {
           var chk_state = !chkThis.checked;
           for (i = 0; i < document.forms[0].elements.length; i++) {
               var currentid = document.forms[0].elements[i].id;
               if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                   //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                   document.forms[0].elements[i].checked = chk_state;
                   document.forms[0].elements[i].click();//fire the click event of the child element
               }
           }
       }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Mark Entry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table width="100%" id="Table1" border="0">
                    <%-- <tr>
                        <td width="50%" align="left" class="title" style="height: 45px">MARK ENTRY
                        </td>
                    </tr>--%>
                </table>
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table id="tblact" runat="server" align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Assesment</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblActivity" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Subject</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblSubject" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Group</span></td>
                                    
                                    <td align="left" >
                                        <asp:Label ID="lblGroup" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" ><span class="field-label">Date</span></td>
                                 
                                    <td align="left" >
                                        <asp:Label ID="lblDate" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Min.Marks</span></td>
                                   
                                    <td align="left" >
                                        <asp:Label ID="lblMinmarks" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" ><span class="field-label">Max Marks</span></td>
                                
                                    <td align="left" >
                                        <asp:Label ID="lblMaxmarks" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>

                                <tr runat="server" visible="false">
                                    <td align="center" colspan="4" >
                                        <table>
                                            <tr>
                                                <td><span class="field-label">Select Header</span></td>
                                           
                                                <td>
                                                    <asp:DropDownList ID="ddlHeader" runat="server" >
                                                        <asp:ListItem Value="CH">Chapter</asp:ListItem>
                                                        <asp:ListItem Value="FB">FeedBack</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtHeader" TextMode="MultiLine"></asp:TextBox></td>
                                                <td>
                                                    <asp:Button runat="server" ID="btnCopy" OnClientClick="return copyData();" class="button" Text="Copy to All"  /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4" >

                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="SL.No" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStaId" runat="server" Text='<%# Bind("Sta_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAtt" runat="server" Text='<%# Bind("STA_bATTENDED") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Stu_Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>






                                                <%--<asp:TemplateField HeaderText="K/U">
<HeaderTemplate>
<table><tr><td>K/U</td></tr>
<tr><td><asp:Label ID="lKUMarks"  width="40px"   runat="server"  />
</td></tr>
</table>
</HeaderTemplate>

<ItemTemplate>
                    <table >
                   
                    <tr><td >
                    <asp:TextBox ID="txtKUMarks"  width="40px" text='<%# Bind("KU_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr>
                     <tr><td ><asp:RangeValidator id="RangeValidator1" runat="server" ControlToValidate="txtKUMarks"
                     ErrorMessage='<%# Bind("ERR_KU") %>'  Display="Dynamic"  Type="Double" MaximumValue='<%# Bind("KU_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>
                    <asp:Label ID="lblKUGrade" runat="server" text='<%# Bind("STS_KU_G") %>'></asp:Label>
                    </ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Appln">
<HeaderTemplate>
<table><tr><td>Appln</td></tr>
<tr><td><asp:Label ID="lAPPLMarks"  width="40px"   runat="server"  />
</td></tr>
</table>
</HeaderTemplate>


<ItemTemplate>
                    <table ><tr><td >
                    <asp:TextBox ID="txtAPPMarks"  width="40px" text='<%# Bind("APP_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator2" runat="server" ControlToValidate="txtAPPMarks"
                    ErrorMessage='<%# Bind("ERR_APPL") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("APP_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>
                    <asp:Label ID="lblAPPGrade" runat="server" text='<%# Bind("STS_APPL_G") %>'></asp:Label>
                    </ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Commn">

<HeaderTemplate>
<table><tr><td>COMM</td></tr>
<tr><td><asp:Label ID="lCOMMMarks"  width="40px"   runat="server"  />
</td></tr>
</table>
</HeaderTemplate>



<ItemTemplate>
                    <table ><tr><td >
                    <asp:TextBox ID="txtCOMMMarks"  width="40px" text='<%# Bind("COMM_MARKS") %>'  enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator3" runat="server" ControlToValidate="txtCOMMMarks"
                    ErrorMessage='<%# Bind("ERR_COMM") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("COMM_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>
                    <asp:Label ID="lblCOMMGrade" runat="server" text='<%# Bind("STS_COMM_G") %>'></asp:Label>
                    </ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="HOTS">

<HeaderTemplate>
<table><tr><td>HOTS</td></tr>
<tr><td><asp:Label ID="lHotsMarks"  width="40px"   runat="server"  />
</td></tr>
</table>
</HeaderTemplate>



<ItemTemplate>
                    <table ><tr><td  >
                    <asp:TextBox ID="txtHOTSMarks"  width="40px" text='<%# Bind("HOTS_MARKS") %>' enabled='<%# Bind("ENABLE") %>' runat="server"  />
                     </td></tr><tr><td ><asp:RangeValidator id="RangeValidator4" runat="server" ControlToValidate="txtHOTSMarks"
                    ErrorMessage='<%# Bind("ERR_HOTS") %>'   Type="Double"  Display="Dynamic"  MaximumValue='<%# Bind("HOTS_MAXMARKS") %>' MinimumValue="0">
                    </asp:RangeValidator></td></tr></table>
                                       <asp:Label ID="lblHOTSGrade" runat="server" text='<%# Bind("STS_HOTS_G") %>'></asp:Label>
                                        </ItemTemplate>
</asp:TemplateField>--%>

                                                <%--DYNAMIC SKILLS BEGIN--%>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill1" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark1" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill1Marks" Text='<%# Bind("Skill1_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtSkill1Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill1")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill1_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill1Grade" runat="server" Text='<%# Bind("STS_Skill1_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill2" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark2" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill2Marks" Text='<%# Bind("Skill2_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtSkill2Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill2")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill2_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill2Grade" runat="server" Text='<%# Bind("STS_Skill2_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill3" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark3" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill3Marks" Text='<%# Bind("Skill3_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtSkill3Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill3")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill3_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill3Grade" runat="server" Text='<%# Bind("STS_Skill3_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill4" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark4" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill4Marks"  Text='<%# Bind("Skill4_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtSkill4Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill4")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill4_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill4Grade" runat="server" Text='<%# Bind("STS_Skill4_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill5" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark5" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill5Marks"  Text='<%# Bind("Skill5_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtSkill5Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill5")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill5_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill5Grade" runat="server" Text='<%# Bind("STS_Skill5_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill6" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark6" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill6Marks"  Text='<%# Bind("Skill6_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtSkill6Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill6")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill6_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill6Grade" runat="server" Text='<%# Bind("STS_Skill6_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill7" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark7" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill7Marks"  Text='<%# Bind("Skill7_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtSkill7Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill7")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill7_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill7Grade" runat="server" Text='<%# Bind("STS_Skill7_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill8" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark8" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill8Marks"  Text='<%# Bind("Skill8_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtSkill8Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill8")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill8_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill8Grade" runat="server" Text='<%# Bind("STS_Skill8_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill9" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark9" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill9Marks"  Text='<%# Bind("Skill9_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="txtSkill9Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill9")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill9_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill9Grade" runat="server" Text='<%# Bind("STS_Skill9_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkill10" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSkillMaxMark10" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtSkill10Marks"  Text='<%# Bind("Skill10_MARKS")%>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="txtSkill10Marks"
                                                                        ErrorMessage='<%# Bind("ERR_Skill10")%>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("Skill10_MAXMARKS")%>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblSkill10Grade" runat="server" Text='<%# Bind("STS_Skill10_G")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>





                                                <%--Dynamic Skills End--%>


                                                <asp:TemplateField HeaderText="HOTS">

                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>WITHOUT SKILLS</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lWSMarks"  runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>



                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtWSMarks" Text='<%# Bind("WITHOUTSKILLS_MARKS") %>' Enabled='<%# Bind("ENABLE") %>' runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="txtWSMarks"
                                                                        ErrorMessage='<%# Bind("ERR_WITHOUTSKILLS") %>' Type="Double" Display="Dynamic" MaximumValue='<%# Bind("WITHOUTSKILLS_MAXMARKS") %>' MinimumValue="0">
                                                                    </asp:RangeValidator></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="lblWSGrade" runat="server" Text='<%# Bind("STS_WITHOUTSKILLS_G") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Chapter" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtChapter" runat="server" Text='<%# Bind("STS_CHAPTER") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FeedBack" Visible="false">
                                                    <ItemTemplate>
                                                       
                                                                    <asp:TextBox ID="txtFeedBack" runat="server" Text='<%# Bind("STS_FEEDBACK") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgBtn" ImageUrl="~/Images/comment.jpg" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Work On This" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWots" runat="server" Text='<%# Bind("STS_WOT") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Target" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTarget" runat="server" Text='<%# Bind("STS_TARGET") %>' TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Marks">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMarks" runat="server" Text='<%# Bind("MARKS") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STA_GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>


                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>


                </table>
                <asp:HiddenField ID="hfCAS_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMEntered" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfMaxMarks" runat="server"></asp:HiddenField>

                <%--  <asp:HiddenField id="hfKUMaxMarks" runat="server">
    </asp:HiddenField>
    
    <asp:HiddenField id="hfAPPLMaxMarks" runat="server">
    </asp:HiddenField>
    
    <asp:HiddenField id="hfCOMMMaxMarks" runat="server">
    </asp:HiddenField>
    
    <asp:HiddenField id="hfHOTSMaxMarks" runat="server">
    </asp:HiddenField>--%>


                <asp:HiddenField ID="hfSkill1MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill2MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill3MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill4MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill5MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill6MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill7MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill8MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill9MaxMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSkill10MaxMarks" runat="server"></asp:HiddenField>




                <asp:HiddenField ID="hfWSMaxMarks" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfGradeSlab" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMinMarks" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfEntryType" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfWithoutSkills" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>

