﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmCASEMarkEntry_M.aspx.vb" Inherits="Curriculum_clmCASEMarkEntry_M"
    Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Mark Entry </div> 
 <div class="card-body">
            <div class="table-responsive m-auto">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0">
        <tr>
            <td align="center"  valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center"
                     cellpadding="7" cellspacing="0" width="100%">
                    <tr id="trAcd1" runat="server">
                        <td align="left" width="20%" >
                            <span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                               >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd2" runat="server">
                        <td align="left" width="20%" >
                           <span class="field-label"> Grade</span>
                        </td>
                       
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                >
                            </asp:DropDownList>
                        </td>
                       <td align="left"  width="20%">
                           <span class="field-label"> Subject</span>
                        </td>
                       
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd3" runat="server">
                      
                      
                           <td align="left"  width="20%" >
                            <span class="field-label">Group</span>
                        </td>
                       
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGroup" SkinID="smallcmb" runat="server" >
                            </asp:DropDownList>
                        </td>
                          <td align="left"  width="20%">
                          <span class="field-label"> Question Bank</span>
                        </td>
                       
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlQuestions" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trAcd4" runat="server">
                        <td align="left"  width="20%">
                            <span class="field-label">Student ID</span>
                        </td>
                       
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStuNo" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left"  width="20%">
                           <span class="field-label"> Student Name </span>
                        </td>
                       
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtName" runat="server">
                            </asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                             <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                 />
                        </td>
                    </tr>
                    <tr id="trStudent" runat="server">
                        <td align="center"  colspan="4" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvStudent" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                            AllowPaging="True" AllowSorting="True" PageSize="1"  >
                                            <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                Position="Top" PageButtonCount="100" />
                                            <RowStyle CssClass="griditem" />
                                            <PagerStyle Font-Bold="True" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                Font-Size="Small" ForeColor="White" HorizontalAlign="Right"  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuID" runat="server" Text='<%# Eval("Stu_ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Eval("Section") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("Stu_No") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Eval("Stu_Name") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td align="right">
                                         <asp:Button ID="btnCancelSearch" runat="server" Text="Cancel Search" CssClass="button"
                                            TabIndex="4"  />
                                    </td>
                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trMarks" runat="server">
                        <td align="center"  colspan="4" valign="top">
                            <asp:GridView ID="gvQuestions" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords." HeaderStyle-Height="30" PageSize="20"
                                >
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQsdId" runat="server" Text='<%# Bind("QSD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMaxMark" runat="server" Text='<%# Bind("QSD_MAXMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Question">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuestion" runat="server"  Text='<%# Bind("QSD_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mark">
                                        <ItemTemplate>
                                          <asp:TextBox ID="txtMark"  runat="server" /><br />
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMark"
                                                            ErrorMessage='<%# Bind("ERR") %>' Display="Dynamic" Type="Double" MaximumValue='<%# Bind("QSD_MAXMARK") %>'
                                                            MinimumValue="0">
                                                        </asp:RangeValidator>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSave" runat="server">
                        <td align="center"  colspan="7" valign="top">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"
                                 />
                            <asp:Button ID="btnSaveNext" runat="server" Text="Save and Next" CssClass="button"
                                TabIndex="4"  />
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfTeacherGrades" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSGR_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfACY_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfQSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSBM_ID" runat="server"></asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>

    </div>
     </div>
    </div>
</asp:Content>
