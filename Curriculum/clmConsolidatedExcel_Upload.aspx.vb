Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports GemBox.Spreadsheet
Imports Microsoft.ApplicationBlocks.Data
Imports Utility

Partial Class Curriculum_ConsolidatedReports_clmConsolidatedExcel_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            'Page.Form.Attributes.Add("enctype", "multipart/form-data")

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If




            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights


            'disable the control buttons based on the rights
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Try
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()

                BindPrintedFor()
                ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")

                BindHeader()

                If ViewState("MainMnu_code") <> "C330271" Then
                    trfilter.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
            ' End If
        End If
        Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
        ScriptManager1.RegisterPostBackControl(btnUpload)
        ScriptManager1.RegisterPostBackControl(btnGenerateExcel)
    End Sub

#Region "Private methods"
    Sub checkEmpFormtutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sctId As String = ""
        Dim grdId As String
        Dim stmId As String = ""
        Dim str_query As String = "SELECT ISNULL(SCT_ID,0),ISNULL(SCT_GRD_ID,''),ISNULL(GRM_STM_ID,0) " _
                                 & " FROM SECTION_M INNER JOIN GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                 & "  SCT_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                 & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sctId = reader.GetValue(0).ToString
            grdId = reader.GetString(1)
            stmId = reader.GetValue(2).ToString

            If grdId <> "" Then
                If Not ddlGrade.Items.FindByValue(grdId + "|" + stmId) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(grdId + "|" + stmId).Selected = True
                End If
            End If
        End While

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub

#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindHeader()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindPrintedFor()
        BindHeader()
    End Sub



    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_DISPLAYORDER"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_WEIGHTAGE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_OBT_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "OBJ_bTARGET"
        dt.Columns.Add(column)
        Return dt
    End Function
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If Not uploadFile.HasFile Then
            lblerror.Text = "Select Excel Sheet...!"
            Exit Sub
        End If
        If Not (uploadFile.FileName.EndsWith("xls") Or uploadFile.FileName.EndsWith("xlsx")) Then
            lblerror.Text = "Invalid file type.. Only excel files are alowed.!"
            Exit Sub
        End If
        UpLoadExcelFile()
    End Sub
    Protected Sub btnGenerateExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateExcel.Click
        lblerror.Text = ""
        Dim _rstHeader As String = ""
        If Not lstHeader.Items.Count > 0 Then
            lblerror.Text = "Header not available"
            Exit Sub
        Else
            For Each li As ListItem In lstHeader.Items
                If li.Selected Then
                    If (_rstHeader = "") Then
                        _rstHeader = "" & li.Text
                    Else
                        _rstHeader = "" & _rstHeader & "," & li.Text
                    End If
                End If
            Next

        End If
        If _rstHeader = "" Then
            lblerror.Text = "No header selected"
            Exit Sub
        End If
        GenerateExcelFile(_rstHeader)
    End Sub
    Private Sub GenerateExcelFile(ByVal strRstHeaders As String)
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection()
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0))
                pParms(3) = New SqlClient.SqlParameter("@RSD_HEADER", strRstHeaders)
                pParms(4) = New SqlClient.SqlParameter("@RSM_ID", ddlReportCard.SelectedValue)


                Dim dsUserAndSubject As DataSet = SqlHelper.ExecuteDataset(connection, "[RPT].[GET_BULK_INSERT_RECORD_STUDENT_S]", pParms)
                Dim dtConvertToExcel As DataTable = dsUserAndSubject.Tables(0)
                Dim dtSubjects As DataTable = dsUserAndSubject.Tables(1)

                For Each dr As DataRow In dtSubjects.Rows
                    dtConvertToExcel.Columns.Add(dr(0).ToString)
                Next


                ' Dim ds As New DataSet
                Dim dtEXCEL As New DataTable

                dtEXCEL = dtConvertToExcel

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim stuFilename As String = Left(dt, Len(dt) - 2) & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '  ws.HeadersFooters.AlignWithMargins = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End Using
        Catch ex As Exception
            Dim a As String = ex.Message
        End Try

    End Sub
    Public Shared Function GetBytesFromDataSet(ByVal ds As DataSet) As Byte()
        Dim data As Byte() = Nothing

        Using stream As MemoryStream = New MemoryStream()
            Dim bf As IFormatter = New BinaryFormatter()
            ds.RemotingFormat = SerializationFormat.Binary
            bf.Serialize(stream, ds)
            data = stream.ToArray()
        End Using

        Return data
    End Function
    Private Sub UpLoadExcelFile()
        If uploadFile.HasFile Then
            Dim tempDir As String = HttpContext.Current.Server.MapPath("~/Curriculum/ReportDownloads/")
            'Dim tempDir As String = "~/Curriculum/ReportDownloads/"

            Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls" ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."
            Dim tempFileNameUsed As String = tempDir + tempFileName
            If uploadFile.HasFile Then
                If File.Exists(tempFileNameUsed) Then
                    File.Delete(tempFileNameUsed)
                End If
                uploadFile.SaveAs(tempFileNameUsed)
                Try
                    getdataExcel(tempFileNameUsed)
                    File.Delete(tempFileNameUsed)
                    lblerror.Text = ""
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblerror.Text = "Request could not be processed"
                End Try
            End If
        End If
    End Sub
    Sub getdataExcel(ByVal filename As String)
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + filename + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        'MyCommand.TableMappings.Add("Table", "Net-informations.com")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        Dim strTypes As String = ""
        For Each li As ListItem In lstHeader.Items
            If (li.Selected = True) Then
                If strTypes = "" Then
                    strTypes = "'" & li.Text & "'"
                Else
                    strTypes &= ",'" & li.Text & "'"
                End If
            End If
        Next
        Dim results As DataRow() = DtSet.Tables(0).Select("[Type] IN(" & strTypes & ")")

        MyConnection.Close()

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")

        Using connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection()
            Dim trans As SqlTransaction = Nothing
            Try
                trans = connection.BeginTransaction("BULKINSERT")
                Dim strXml As String = ""
                For Each dtRow As DataRow In results
                    For Colindex As Integer = 0 To DtSet.Tables(0).Columns.Count - 5
                        Dim strSubject As String = DtSet.Tables(0).Columns(Colindex + 4).ColumnName

                        strXml += "<ID><RPF_ID>" + ddlPrintedFor.SelectedValue + "</RPF_ID>"
                        strXml += "<RSD_HEADER>" + dtRow("Type") + "</RSD_HEADER>"
                        strXml += "<ACD_ID>" + ddlAcademicYear.SelectedValue + "</ACD_ID>"
                        strXml += "<GRD_ID>" + grade(0) + "</GRD_ID>"
                        strXml += "<STU_ID>" + dtRow("StudentID").ToString() + "</STU_ID>"
                        strXml += "<SBG_DESCR>" + strSubject + "</SBG_DESCR>"
                        strXml += "<COMMENTS>" + Convert.ToString(dtRow(strSubject)) + "</COMMENTS>"
                        strXml += "<USER>" + Session("sUsr_name").ToString + "</USER>"
                        strXml += "<RSM_ID>" + ddlReportCard.SelectedValue + "</RSM_ID></ID>"
                    Next
                Next
                strXml = "<IDS>" + strXml + "</IDS>"

                Dim pParms(1) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CURR_XML", strXml)
                pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(1).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[RPT].[EXEC_BULK_INSERT_REPORT_STUDENT_S]", pParms)
                Dim ReturnFlag As Integer = pParms(1).Value
                'Return ReturnFlag
                lblerror.Text = "Record(s) Saved Successfully"
                trans.Commit()

            Catch ex As Exception
                lblerror.Text = "Request could not be processed"
                trans.Rollback()

            End Try
        End Using
    End Sub

End Class
