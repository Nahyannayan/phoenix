﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmObjTrackStudentLevels
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320031") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindSubjects()
                BindGroup()
                'BindLevel()
                'Dim url As String
                'url = String.Format("{0}?MainMnu_code={1}", "~/Curriculum/clmObjTrackStudObjTest.aspx", Encr_decrData.Encrypt(ViewState("MainMnu_code")))
                'ResponseHelper.Redirect(url, "_blank", "")

                hView.Value = "0"
                crv.Visible = False

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try
        End If
        If hView.Value = "1" Then
            CallReport()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(crv)

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            If Session("sbsuid") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                            & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                            & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                            & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','09','10') "
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                            & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                            & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                            & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') "
            End If
            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            If Session("sbsuid") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                       & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                       & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                       & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                       & " AND SGS_TODATE IS NULL" _
                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') " _
                       & " ORDER BY GRD_DISPLAYORDER"
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                       & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                                       & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                       & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                                       & " AND SGS_TODATE IS NULL" _
                                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') " _
                                       & " ORDER BY GRD_DISPLAYORDER"
            End If
          
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"

        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub



    Sub LoadReports(ByVal rptClass As rptClass)
        '   Try

        Dim iRpt As New DictionaryEntry

        Dim crParameterDiscreteValue As ParameterDiscreteValue
        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        Dim crParameterFieldLocation As ParameterFieldDefinition
        Dim crParameterValues As ParameterValues

        With rptClass

            rs.ReportDocument.Load(.reportPath)

            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
            myConnectionInfo.ServerName = .crInstanceName
            myConnectionInfo.DatabaseName = .crDatabase
            myConnectionInfo.UserID = .crUser
            myConnectionInfo.Password = .crPassword


            SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
            SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)



            crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
            If .reportParameters.Count <> 0 Then
                For Each iRpt In .reportParameters
                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                    crParameterValues = crParameterFieldLocation.CurrentValues
                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                    crParameterDiscreteValue.Value = iRpt.Value
                    crParameterValues.Add(crParameterDiscreteValue)
                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                Next
            End If


            If .selectionFormula <> "" Then
                rs.ReportDocument.RecordSelectionFormula = .selectionFormula
            End If


        End With
        '  Catch ex As Exception


        '  End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
     
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next

        myReportDocument.VerifyDatabase()

    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)
                    SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    

    Sub CallReport()
        Dim param As New Hashtable
        Dim sbm_id As String() = ddlSubject.SelectedValue.Split("|")
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)
        param.Add("group", ddlGroup.SelectedItem.Text)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        param.Add("@SBM_ID", sbm_id(1))



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Curriculum/Reports/Rpt/rptObjTrackStudentLevels.rpt")
        End With
        Session("rptClass") = rptClass
        'LoadReports(rptClass)
        ReportLoadSelection()
    End Sub

   

#End Region

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        crv.Visible = True
        hView.Value = "1"
        CallReport()
    End Sub






    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

        'crv.Dispose()
        'crv = Nothing
        ' rs.Dispose()
        ' rs = Nothing

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx')", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank')", True)
        End If
    End Sub
End Class
