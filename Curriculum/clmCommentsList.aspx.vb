Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Partial Class Curriculum_clmCommentsList
    Inherits System.Web.UI.Page

    Sub gridbind(ByVal intHead As Integer)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        Dim strCondition As String = ""

        Dim ds As DataSet
        If ViewState("type") = "ALLCMTS" Then
            If Not Session("SBGID") Is Nothing Then
                If ViewState("header") = "-1" Then
                    strCondition += " AND (CMT_SBG_ID=" + Session("SBGID").ToString() + " OR ISNULL(CMT_SBG_ID,0)=0)"
                Else
                    strCondition += " AND CMT_SBG_ID=" & Session("SBGID").ToString()
                End If

            End If

            If intHead <> 0 Then
                strCondition += " AND CMT_RSD_ID=" & intHead
            End If
        End If

        'str_Sql = "SELECT CMT_ID,Replace(Replace(Replace(ISNULL(CMT_COMMENTS,''),'&&Name&&','" & ViewState("STUNAME") & "'),'&&His/Her&&','" & ViewState("hisher") & "'),'&&He/She&&','" & ViewState("heshe") & "')AS CMT_COMMENTS FROM ACT.COMMENTS_M WHERE CMT_CAT_ID='" & ddlCategory.SelectedValue & "' " & strCondition
        str_Sql = "SELECT CMT_ID, [dbo].GetCommentText( ISNULL(CMT_COMMENTS,''),'" & ViewState("STUNAME") & "', '" & _
        ViewState("hisher") & "','" & ViewState("heshe") & "','" & ViewState("himher") & "','" & ViewState("himherself") & "') AS CMT_COMMENTS FROM ACT.COMMENTS_M WHERE CMT_CAT_ID='" & ddlCategory.SelectedValue & "' " & strCondition & " order by CMT_ID"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvcmtname.DataSource = ds
        gvcmtname.DataBind()
    End Sub

    Private Sub BindBSU()
        If Session("CLM") = "1" Then
            Dim sql_str As String = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE(BSU_CLM_ID = 1 And BSU_bGEMSSCHOOL = 1 And BSU_bASIANSCHOOL = 1) "
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, sql_str)
            ddlBSUUnit.DataSource = drReader
            ddlBSUUnit.DataValueField = "BSU_ID"
            ddlBSUUnit.DataTextField = "BSU_NAME"
            ddlBSUUnit.DataBind()
            ddlBSUUnit.ClearSelection()
            ddlBSUUnit.Items.FindByValue(Session("sBSUID")).Selected = True
        Else
            trBSU.visible = False
        End If

    End Sub

    Private Sub Bindgrade()
        If Session("sBSUID") = "121013" Then
            lblGrade.Visible = True
            ddlGrade.Visible = True
            ddlGrade.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT GRD_ID, GRD_DISPLAY FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "GRD_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "ALL"))
            ddlGrade.Items.FindByValue("ALL").Selected = True
        Else
            lblGrade.Visible = False
            ddlGrade.Visible = False
        End If
    End Sub


    Private Function GetBSU() As String
        If Session("CLM") = "1" Then
            Return ddlBSUUnit.SelectedValue
        Else
            Return Session("sBSUID")
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                If (Not Request.QueryString("SBGID").ToString Is Nothing) And Request.QueryString("SBGID").ToString <> "" Then
                    Session("SBGID") = Request.QueryString("SBGID")
                End If
            Catch ex As Exception
            End Try


            Dim strHeader As String = Request.QueryString("header").ToString
            ViewState("rsmid") = Request.QueryString("RSMID").ToString
            BindBSU()
            Dim intHead As Integer

            If strHeader <> "AOL_CMTS" Then
                intHead = getHeaderID(strHeader)
                ViewState("header") = intHead
            Else
                ViewState("header") = "-1"
            End If
            If Not Session("SBGID") Is Nothing Then
                h_SBGID.Value = Session("SBGID")
            End If
            ViewState("STUNAME") = getStudentName(Request.QueryString("STUID").ToString).Replace("'", "''")
            If Request.QueryString("ID").ToString = "ALLCMTS" Then
                ViewState("type") = Request.QueryString("ID").ToString
                Bindgrade()
                FillCategory()
                gridbind(intHead)
            Else
                ViewState("type") = Request.QueryString("ID").ToString
                Bindgrade()
                FillCategory()
                gridbind(intHead)
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Sub FillCategory()
        Dim strSql As String
        Dim strCondition As String = ""
        Dim strFilter As String = ""
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If ViewState("type").ToString = "ALLCMTS" Then
            If Not Session("Grade") Is Nothing And Session("Grade") <> "" Then
                If Session("SBSUID") <> "126008" Then
                    strCondition += " AND CAT_GRD_ID='" & Session("Grade").ToString() & "'"
                End If
            End If

            If Not Session("SBGID") Is Nothing Then
                ' strFilter += " AND CMT_SBG_ID=" & Session("SBGID").ToString()
                If ViewState("header") = "-1" Then
                    strCondition += " AND (CMT_SBG_ID=" + Session("SBGID").ToString() + " OR ISNULL(CMT_SBG_ID,0)=0)"
                Else
                    strCondition += " AND CMT_SBG_ID=" & Session("SBGID").ToString()
                End If

            End If
            strSql = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE CAT_ID IN (SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_BSU_ID='" & GetBSU() & "'" & strFilter & " AND CMT_RSD_ID=" & ViewState("header") & strCondition & " ) AND CAT_BSU_ID='" & GetBSU() & "' "
        Else
            If ddlGrade.SelectedValue <> "ALL" Then
                strCondition += " AND ISNULL(CAT_GRD_ID, '') = '" & ddlGrade.SelectedValue & "' "
            Else
                strCondition += " AND CAT_GRD_ID IS NULL "
            End If
            strSql = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE CAT_BSU_ID='" & GetBSU() & "' " & strCondition
        End If
        strSql += " ORDER BY CAT_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "CAT_DESC"
        ddlCategory.DataValueField = "CAT_ID"
        ddlCategory.DataBind()

    End Sub

    Private Function getHeaderID(ByVal strHeder As String) As Integer
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If Not Session("SBGID") Is Nothing Then
            strSql = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ViewState("rsmid") + " AND RSD_HEADER='" & strHeder & "' AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " _
          & " AND ISNULL(RSD_SBG_ID,0)=CASE WHEN RSD_SBG_ID IS NULL THEN ISNULL(RSD_SBG_ID,0) ELSE '" + Session("SBGID").ToString + "' END ORDER BY RSD_DISPLAYORDER "
        Else
            strSql = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ViewState("rsmid") + " AND RSD_HEADER='" & strHeder & "' AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " _
                      & " ORDER BY RSD_DISPLAYORDER "
        End If


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

        If ds.Tables(0).Rows.Count >= 1 Then
            Return ds.Tables(0).Rows(0).Item("RSD_ID")
        Else
            Return 0
        End If
    End Function

    Private Function getStudentName(ByVal strSTUID As String) As String
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        strSql = "SELECT dbo.fnProperCase(STU_FIRSTNAME) as STU_FIRSTNAME,STU_GENDER FROM VW_STUDENT_M WHERE STU_ID=" & strSTUID & ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

        If ds.Tables(0).Rows.Count >= 1 Then
            If ds.Tables(0).Rows(0).Item("STU_GENDER") = "F" Then
                ViewState("hisher") = "her"
                ViewState("heshe") = "she"
                ViewState("himher") = "her"
                ViewState("himherself") = "herself"
            Else
                ViewState("hisher") = "his"
                ViewState("heshe") = "he"
                ViewState("himher") = "him"
                ViewState("himherself") = "himself"
            End If
            ViewState("Gender") = ds.Tables(0).Rows(0).Item("STU_GENDER")
            Return ds.Tables(0).Rows(0).Item("STU_FIRSTNAME")
        Else
            Return ""
        End If
    End Function


   
    Protected Sub gvcmtname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvcmtname.SelectedIndexChanged
        Dim javaSr As String
        Dim lblcmtId As Label
        Dim lnkcmtname As LinkButton
        Dim txtcomment As TextBox

        Dim strValue As String = ""
        lblcmtId = gvcmtname.SelectedRow.FindControl("lblcmtid")
        lnkcmtname = gvcmtname.SelectedRow.FindControl("lnkcmtname")
        txtcomment = gvcmtname.SelectedRow.FindControl("txtcomment")

        strValue = lnkcmtname.Text
        'strValue = strValue.Replace(Chr(13), "").Replace("'", "")

        'strValue = strValue.Replace("&&Name&&", ViewState("STUNAME").ToString.Replace("''", "'")).Replace("-", "").Replace("~", "")
        strValue = strValue.Replace("&&Name&&", ViewState("STUNAME").ToString.Replace("''", "'")).Replace("~", "")
        strValue = UtilityObj.CleanupStringForJavascript(strValue)
        strValue = strValue.Replace(vbCrLf, "")
        strValue = strValue.Replace(vbCr, "")
        strValue = strValue.Replace(vbLf, "")


        Try
            If Session("sbsuid") = "126008" Then
                Dim i As Integer = 0
                Dim x As Integer
                Dim j As String = ""
                Dim replacestring As String = ""
                i = CInt(Left(strValue, 1))
                j = strValue.Chars(2)
                If i >= 1 And i <= 9 Then
                    For x = 1 To 250
                        replacestring = CStr(x) + " . "
                        If strValue.StartsWith(replacestring) = True Then
                            strValue = strValue.Replace(replacestring, "")
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
        End Try

        'Response.Write("<script language='javascript'> function listen_window(){")
        ''window.opener.parent.Form1['TextBox1'].value=lsString--+ "___" + lblcmtId.Text 
        'Response.Write("window.returnValue = '" & strValue.Replace("'", "/ap/") & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write(" var oArg = new Object();")
        'Response.Write("oArg.Comment ='" & strValue.Replace("'", "/ap/") & "';")
        'Response.Write("var oWnd = GetRadWindow('" & strValue.Replace("'", "/ap/") & "');")
        'Response.Write("oWnd.close(oArg);")
        'Response.Write("alert(oArg.Comment);")
        'Response.Write("} </script>")


        javaSr = "<script language='javascript'> function listen_window(){"
        javaSr += " var oArg = new Object();"
        javaSr += "oArg.Comment ='" & strValue.Replace("'", "/ap/") & "';"
        javaSr += "var oWnd = GetRadWindow('" & strValue.Replace("'", "/ap/") & "');"
        javaSr += "oWnd.close(oArg);"
        javaSr += "} </script>"
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        h_SelectedId.Value = "Close"

    End Sub


    Function CleanupStringForJavascript(ByVal Str_TexttoCleanup As String) As String
        Str_TexttoCleanup = HttpContext.Current.Server.UrlEncode(Str_TexttoCleanup)
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("'", "\'")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d%0a", " ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0a", "  ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d", " ")
        Return HttpContext.Current.Server.UrlDecode(Str_TexttoCleanup)
    End Function


    Protected Sub gvcmtname_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvcmtname.PageIndexChanging
        Try
            gvcmtname.PageIndex = e.NewPageIndex
            gridbind(ViewState("header"))
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lnkcmtname_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            gridbind(ViewState("header"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlBSUUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUUnit.SelectedIndexChanged
        Dim strHeader As String = Request.QueryString("header").ToString
        ViewState("rsmid") = Request.QueryString("RSMID").ToString
        Dim intHead As Integer



        If strHeader <> "AOL_CMTS" Then
            intHead = getHeaderID(strHeader)
            ViewState("header") = intHead
        Else
            ViewState("header") = "-1"
        End If

        If ddlBSUUnit.Items.Count <> 0 And Not Session("SBGID") Is Nothing Then
            If Session("sbsuid") <> ddlBSUUnit.SelectedValue.ToString Then
                getBsuSubject()
            Else
                Session("SBGID") = h_SBGID.Value
            End If
        End If

        If Request.QueryString("ID").ToString = "ALLCMTS" Then
            ViewState("type") = Request.QueryString("ID").ToString
            Bindgrade()
            FillCategory()
            gridbind(intHead)
        Else
            ViewState("type") = Request.QueryString("ID").ToString
            Bindgrade()
            FillCategory()
            gridbind(intHead)
        End If

    End Sub
    Sub getBsuSubject()
        Dim str_query As String = "SELECT b.SBG_ID FROM SUBJECTS_GRADE_S AS A " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS B ON A.SBG_GRD_ID=B.SBG_GRD_ID AND A.SBG_STM_ID=B.SBG_STM_ID " _
                                & " and A.SBG_SBM_ID=B.SBG_SBM_ID " _
                                & " INNER JOIN ACADEMICYEAR_D AS C ON A.SBG_ACD_ID=C.ACD_ID" _
                                & " INNER JOIN ACADEMICYEAR_D AS D ON B.SBG_ACD_ID=D.ACD_ID" _
                                & " AND C.ACD_ACY_ID=D.ACD_ACY_ID" _
                                & " WHERE D.ACD_BSU_ID='" + ddlBSUUnit.SelectedValue.ToString + "'" _
                                & " AND A.SBG_ID='" + Session("SBGID").ToString + "'"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Session("SBGID") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        FillCategory()
        gridbind(ViewState("header"))
    End Sub
End Class
