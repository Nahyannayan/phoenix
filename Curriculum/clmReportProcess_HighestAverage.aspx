<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportProcess_HighestAverage.aspx.vb" Inherits="Curriculum_clmReportProcess_HighestAverage" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             <asp:Literal id="ltLabel" runat="server" Text="Filter Condition"></asp:Literal> 
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                </span>
            </td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <%--  <tr>
                        <td colspan="2">
                      
                                <asp:Literal id="ltLabel" runat="server" Text="Filter Condition"></asp:Literal></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="15%">
                          <span class="field-label">   Academic Year</span></td>
                       
                        <td align="left" width="35%" >
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True"   >
                            </asp:DropDownList></td>

                         <td align="left" >
                            <span class="field-label"> Select Report card</span></td>
                       
                        <td align="left" >
                            <asp:DropDownList id="ddlReportCard" runat="server"  AutoPostBack="True" >
                            </asp:DropDownList></td>
                        </tr>
                   
                       
                    <tr>
                        <td align="left" >
                            <span class="field-label"> Report Schedule</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlPrintedFor" runat="server"  >
                            </asp:DropDownList></td>
                       </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                
                <asp:Button id="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process"  ValidationGroup="AttGroup"  /></td>
        </tr>
        
        <tr>
            <td >
                <asp:HiddenField id="hfbFinalReport" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfbAOLprocessing" runat="server">
                </asp:HiddenField>
                </td>
        </tr>
    </table>
    
        </div>
        </div>
    </div>

            

</asp:Content>

