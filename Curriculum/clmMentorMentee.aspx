﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmMentorMentee.aspx.vb" Inherits="Curriculum_clmMentorMentee" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">
        function getMentor() {
            var sFeatures;
            sFeatures = "dialogWidth: 350px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfMentorUrl.ClientID %>").value
            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_menter");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('|');
            document.getElementById("<%=hfMentor.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtMentor.ClientID %>").value = NameandCode[2] + " (" + NameandCode[1] + ")";--%>


        }
        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                NameandCode = arg.Student.split('||');
                document.getElementById("<%=hfMentor.ClientID %>").value = NameandCode[0];
               document.getElementById("<%=txtMentor.ClientID %>").value = NameandCode[1] + " (" + NameandCode[2] + ")";
                __doPostBack('<%= txtMentor.ClientID%>', 'TextChanged');
            }
        }

        function getMentee() {
            var sFeatures;
            sFeatures = "dialogWidth: 350px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfMenteeUrl.ClientID %>").value

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_mentee");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('|');
            document.getElementById("<%=hfMentee.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtMentee.ClientID %>").value = NameandCode[2] + " (" + NameandCode[1] + ")";--%>



        }

        function OnClientClose2(oWnd, args) {
           
            //get the transferred arguments
            var arg = args.get_argument();
           
            if (arg) {               
               
                NameandCode = arg.Student.split('||');
                document.getElementById("<%=hfMentee.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtMentee.ClientID %>").value = NameandCode[1] + " (" + NameandCode[2] + ")";
                __doPostBack('<%= txtMentee.ClientID%>', 'TextChanged');
            }
        }

        function Getemployee() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var groupId;
            groupId = "0";
            //result = window.showModalDialog("showemployee.aspx?GrpId=" + groupId + "", "", sFeatures)
            var oWnd = radopen("showemployee.aspx?GrpId=" + groupId + "", "pop_employee");
           <%-- if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtempname.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_EMP_ID.ClientID %>').value = NameandCode[1];

            }
            return false;--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.Employee.split('||');
                document.getElementById('<%=txtempname.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_EMP_ID.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtempname.ClientID %>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
       
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
               <telerik:RadWindow ID="pop_mentee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
          <Windows >
               <telerik:RadWindow ID="pop_menter" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Student Mentor And Mentee
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" CssClass="error" align="left"  Style="text-align: left"></asp:Label>

                <table align="center"  cellpadding="5" cellspacing="0" width="100%" >
                    <tr>
                        <td align="left" width="16%">
                             <span class="field-label">Select Academic Year</span> 
                        </td>
                        <td align="left" colspan="2" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  width="12.5%" >
                             <span class="field-label">Select Term</span>
                        </td>
                        <td align="left" width="10%" >
                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  width="12.5%"> <span class="field-label">Mentor</span> </td>
                       <td colspan="2">
                            <asp:RadioButton ID="rdTeacher" Text="Teacher" AutoPostBack="true" GroupName="g1" runat="server" />&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rdStudent" Text="Student" AutoPostBack="true" GroupName="g1" runat="server" />
                        </td>
                       <%-- <td ></td>--%>
                    </tr>                                   
                    <tr id="trTeacher" runat="server">
                        <td><span class="field-label"  width="20%">Select Mentor Teacher</span>
                        </td>
                        <td align="left" colspan="2" >
                            <asp:TextBox ID="txtempname" runat="server" MaxLength="100" TabIndex="2"
                                Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="Getemployee(); return false;" TabIndex="18"></asp:ImageButton>


                        </td>
                        <td colspan="5"></td>
                    </tr>
                    <tr id="trStudent" runat="server">
                        <td align="left"  width="16%"><span class="field-label" >Mentor Grade</span>
                        </td>
                        <td align="left">&nbsp;<asp:DropDownList ID="ddlMentorGrade" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        </td>
                        <td align="left"  width="12.5%"><span class="field-label" >Mentor Section</span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:DropDownList ID="ddlMentorSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  width="12.5%"><span class="field-label">Select Mentor </span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtMentor" runat="server" MaxLength="100" TabIndex="2"
                                Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="imgMentor" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="getMentor(); return false;" TabIndex="18"></asp:ImageButton>
                        </td>
                       <%-- <td ></td>--%>
                    </tr>
                           <%-- </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <%--<td colspan="4" align="left">
                            <table cellpadding="5">
                                <tr>--%>
                        <td align="left"  width="16%"><span class="field-label">Mentee Grade</span>
                        </td>
                        <td align="left">&nbsp;<asp:DropDownList ID="ddlMenteeGrade" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        </td>
                        <td align="left"  width="12.5%"><span class="field-label">Mentee Section</span>
                        </td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlMenteeSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="12.5%"><span class="field-label">Select Mentee</span>
                        </td>
                        <td align="left" >
                            <asp:TextBox ID="txtMentee" runat="server" MaxLength="100" TabIndex="2"
                                Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="imgMentee" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="getMentee(); return false;" TabIndex="18"></asp:ImageButton>
                        </td>
                         <td align="left" width="10%"><span class="field-label">Subject</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server">
                            </asp:DropDownList>
                        </td>
                        <%-- </tr>
                            </table>
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="8" align="center">
                            <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" valign="top">
                            <table id="tbl_test" runat="server" cellpadding="5" cellspacing="0" width="100%">
                                <tr >
                                    <td align="left"  colspan="9" valign="top">
                                        <asp:GridView ID="gvMentorMentee" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                             CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" AllowSorting="True">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMntId" runat="server" Text='<%# Bind("MNT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                       <span class="field-label">Mentor ID</span> 
                                                        <br />
                                                        <asp:TextBox ID="txtMentorIDSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentor_ID" OnClick="btnMentor_ID_Click" runat="server"
                                                         ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;                                                                    
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMentorID" runat="server" Text='<%# Bind("MENTOR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Mentor Name</span>
                                                        <br />
                                                        <asp:TextBox ID="txtMentorSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentor_DESC" OnClick="btnMentor_DESC_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;                                                                                                                                                       
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMentor" runat="server" Text='<%# Bind("MENTOR_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Mentor Class</span>
                                                        <br />
                                                        <asp:TextBox ID="txtMentorClass" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentor_Class" OnClick="btnMentor_Class_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMentorClass" runat="server" Text='<%# Bind("MENTOR_CLASS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Mentee ID</span>
                                                        <br />
                                                        <asp:TextBox ID="txtMenteeIDSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentee_ID" OnClick="btnMentee_ID_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;
                                                                                   
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMenteeID" runat="server" Text='<%# Bind("MENTEE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Mentee Name</span>
                                                        <br />
                                                        <asp:TextBox ID="txtMenteeSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentee_DESC" OnClick="btnMentee_DESC_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;
                                                                       
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMenteeName" runat="server" Text='<%# Bind("MENTEE_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Mentee Class</span>
                                                        <br />
                                                        <asp:TextBox ID="txtMenteeClass" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMentee_Class" OnClick="btnMentee_Class_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;                                                                           
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMenteeClass" runat="server" Text='<%# Bind("MENTEE_CLASS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">
                                                    <HeaderTemplate>
                                                        <span class="field-label">Subject</span>
                                                        <br />
                                                        <asp:TextBox ID="txtSubjectSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btSubjectSearch" OnClick="btSubjectSearch_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;
                                                                     
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMenteeSubject" runat="server" Text='<%# Bind("MENTEE_SUBJECT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="lnkDelete" ConfirmText="Selected record will be deleted permanently.Are you sure you want to continue?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfMentor" runat="server" />
                <asp:HiddenField ID="hfMentee" runat="server" />
                <asp:HiddenField ID="hfMentorUrl" runat="server" />
                <asp:HiddenField ID="h_EMP_ID" runat="server" />
                <asp:HiddenField ID="hfMenteeUrl" runat="server" />
            </div>
        </div>
    </div>
     <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>
</asp:Content>
