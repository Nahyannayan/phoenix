<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Copy of clmObjTrackStudentLevels_Popup.aspx.vb"
    Inherits="Curriculum_clmObjTrackStudentLevels_Popup" MaintainScrollPositionOnPostback="true" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>Untitled Page</title>
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

  function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkStatus")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          } 
          
          
          function change_chk_state1(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkDisplay")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }  
          
                    
           function change_ddl_state(ddlThis,ddlObj)
         {
         
        
          
          for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               
              
               if( currentid.indexOf(ddlObj)!=-1)
             {
           
                 if (ddlThis.selectedIndex!=0)
                 {
                   
                    document.forms[0].elements[i].selectedIndex=ddlThis.selectedIndex-1;
                    
                  }
                    
                     
                 }
              }
          }           
</script>

<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>

    <div style="overflow: hidden">
        <p>
            &nbsp;</p>
        <table id="Table2" runat="server" align="center" border="1" bordercolor="#1b80b6"
            cellpadding="5" cellspacing="0" style="width: 512px;" class="BlueTableView">
            <tr>
                <td class="matters" align="left">
                    Academic Year
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblAcademicYear" runat="server"></asp:Label>
                </td>
                <td class="matters" align="left">
                    Grade
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblGrade" runat="server"></asp:Label>
                </td>
                <td rowspan="3" width="40px" align="center">
                    <asp:Label ID="lblCurrentLevel" runat="server" Font-Bold="True" Font-Size="XX-Large"
                        ForeColor="#0033CC"></asp:Label>
                </td>
                <td rowspan="3"  align="center">
                    <asp:Image ID="imgStud" runat="server" Height="70px" ImageUrl="~/Images/Photos/no_image.gif"
                        Width="50px" />
                </td>
            </tr>
            <tr>
                <td class="matters" align="left">
                    Subject
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblSubject" runat="server"></asp:Label>
                </td>
                <td class="matters" align="left">
                    Group
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblGroup" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="matters" align="left">
                    Student ID
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblStudentID" runat="server"></asp:Label>
                </td>
                <td class="matters" align="left">
                    Student Name
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="matters" align="left">
                    Level
                </td>
                <td class="matters" align="center">
                    :
                </td>
                <td class="matters" align="left">
                    <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="matters" align="left">
                    Category</td>
                <td class="matters" colspan="5">
                 <asp:DropDownList ID="ddlCategory" runat="server" Width="320px" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                </tr><tr>
                <td class="matters" colspan="3">
                    <asp:Label ID="lblTotalObjectives" runat="server"></asp:Label>
                </td>
                <td class="matters" colspan="5">
                    <asp:Label ID="lblLevelObjectives" runat="server"></asp:Label></td>
                
            </tr>
            <tr>
                <td class="matters" colspan="8" align="center">
                  <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0" >
            <ajaxToolkit:TabPanel ID="HT1" runat="server"  >
                <ContentTemplate>
                  
                   <div style="height: 300px; width: 500px; overflow: auto">
                   
                        <asp:GridView ID="gvObjectives" runat="server" AutoGenerateColumns="False"
                            CssClass="gridstyle" 
                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." 
                            PageSize="20" Width="373px" BorderStyle="None">
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            <Columns>
                                <asp:TemplateField HeaderText="NC Level" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("OBJ_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Objectives">
                                    <ItemTemplate>
                                        <asp:Label ID="lblObjective" runat="server" Text='<%# Bind("OBJ_DESCR") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlStatus" runat="server">
     <asp:ListItem Text="Y" Value="Y" style="background:green;"  title="Yes" />
                    <asp:ListItem Text="I" Value="I" style="background:yellow" title="Insufficient Evidence" />
                    <asp:ListItem Text="N" Value="N" style="background:red" title="No" Selected="True" />
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td class="matters">
                                                    Status
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:DropDownList ID="ddlAll" runat="server" onclick="javascript:change_ddl_state(this,'ddlStatus');">
                                                        <asp:ListItem Text="--" Value="--" />
                    <asp:ListItem Text="Y" Value="Y" style="background:green;"  title="Yes" />
                    <asp:ListItem Text="I" Value="I" style="background:yellow" title="Insufficient Evidence" />
                    <asp:ListItem Text="N" Value="N" style="background:red" title="No"  />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                        </asp:GridView>
                         </div>
                  
                  
                                  
                </ContentTemplate>
                <HeaderTemplate  >
                          Objectives
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                         <div  style="height:300px;width:500px;overflow:auto">
                       <asp:GridView ID="gvTarget" runat="server" AllowPaging="false" AutoGenerateColumns="False"
        CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
        HeaderStyle-Height="30" PageSize="20" Width="373px" BorderStyle="None">
        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <Columns>
             <asp:TemplateField HeaderText="NC Level" Visible="false" >
                <ItemTemplate>
                    <asp:Label ID="lblObjId" runat="server"  Text='<%# Bind("OBJ_ID") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
                   
             <asp:TemplateField HeaderText="Objectives" >
                <ItemTemplate>
                    <asp:Label ID="lblObjective" runat="server"  Text='<%# Bind("OBJ_DESCR") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateField>
            
            
             <asp:TemplateField HeaderText="Target"  >
                <HeaderTemplate  >
                <table><tr ><td class="matters" >Select Targets</td></tr>
                 <tr><td align="center">
                    <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);" />
                  </td></tr>
                 </table>
                 
                </HeaderTemplate>
                <ItemTemplate >
                    <asp:CheckBox ID="chkStatus" runat="server" /> 
                </ItemTemplate>
                <ItemStyle HorizontalAlign="center"></ItemStyle>
            </asp:TemplateField>
            
                    
             <asp:TemplateField HeaderText="Display in Reportcard"  >
                <HeaderTemplate  >
                <table><tr ><td class="matters" >Display in Reportcard</td></tr>
                 <tr><td align="center">
                    <asp:CheckBox ID="chkAll1" runat="server"  onclick="javascript:change_chk_state1(this);" />
                  </td></tr>
                 </table>
                 
                </HeaderTemplate>
                <ItemTemplate >
                    <asp:CheckBox ID="chkDisplay" runat="server" /> 
                </ItemTemplate>
                <ItemStyle HorizontalAlign="center"></ItemStyle>
            </asp:TemplateField>
            
        </Columns>
        <SelectedRowStyle CssClass="Green" Wrap="False" />
        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
        <EditRowStyle Wrap="False" />
        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
    </div>
                    
                                
                </ContentTemplate>
                <HeaderTemplate>
               Set Targets                     
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>

 
            

        </ajaxToolkit:TabContainer>

                </td>
            </tr>
            <tr>
                <td align="left" colspan="8">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="7"
                        Width="115px" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hSTU_ID" runat="server" />
        <asp:HiddenField ID="hACD_ID" runat="server" />
        <asp:HiddenField ID="hSBM_ID" runat="server" />
        <asp:HiddenField ID="hSGR_ID" runat="server" />
         <asp:HiddenField ID="hObjCompleted" runat="server" />
        <asp:HiddenField ID="hStuPhotoPath" runat="server" />
    </div>
    
 
    </form>
</body>
</html>
