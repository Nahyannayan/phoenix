﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO

Partial Class Curriculum_clmSMARTQuestions_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100610") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = "add"
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindRowCount()
                BindTargetSetup()
                BindCopyGrade()
                GridBind()
                gvQuestions.Attributes.Add("bordercolor", "#1b80b6")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                 & " WHERE GRM_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRD_ID NOT IN ('KG1','KG2') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindRowCount()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SMR_ROWS FROM SMART.SMART_SETUP_ROWS WHERE SMR_BSU_ID='" + Session("sbsuid") + "'" _
                                & " AND SMR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            H_ROWS.Value = ds.Tables(0).Rows(0).Item(0)
        Else
            H_ROWS.Value = 49
        End If
    End Sub
    Sub BindTargetSetup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGM_ID,TGM_DESCR FROM SMART.TARGET_SETUP_M WHERE TGM_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY TGM_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTarget.DataSource = ds
        ddlTarget.DataTextField = "TGM_DESCR"
        ddlTarget.DataValueField = "TGM_ID"
        ddlTarget.DataBind()
    End Sub

    Sub BindSubject(ByVal ddlSubject As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                               & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub SaveData()

        Dim lblDelete As Label

       
        Dim lblTgdId As Label
        Dim lblSbgId As Label
        Dim lblTextType As Label
        Dim txtSlno As TextBox
        Dim txtQuestion As TextBox
        Dim txtOption As TextBox
        Dim txtMinAnswer As TextBox
        Dim txtPretext As TextBox
        Dim chkMandatory As CheckBox
        Dim chkTargetScore As CheckBox

        Dim ddlTextType As DropDownList
        Dim ddlSubject As DropDownList



        Dim Descr As String
        Dim i As Integer

       

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String 
        Dim mode As String

        For i = 0 To gvQuestions.Rows.Count - 1
            With gvQuestions.Rows(i)
                lblTgdId = .FindControl("lblTgdId")
                lblSbgId = .FindControl("lblSbgId")
                lblTextType = .FindControl("lblTextType")
                txtSlno = .FindControl("txtSlno")
                txtQuestion = .FindControl("txtQuestion")
                lblDelete = .FindControl("lblDelete")
                txtOption = .FindControl("txtOption")
                txtMinAnswer = .FindControl("txtMinAnswer")
                txtPretext = .FindControl("txtPretext")
                chkMandatory = .FindControl("chkMandatory")
                chkTargetScore = .FindControl("chkTargetScore")
                ddlTextType = .FindControl("ddlTextType")
                ddlSubject = .FindControl("ddlSubject")

              
                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblTgdId.Text = "0" Then
                    mode = "add"
                ElseIf lblTgdId.Text <> "0" Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" And txtQuestion.Text <> "" Then
                    str_query = "EXEC  SMART.saveTARGET_SETUP_M " _
                              & "@TGD_ID=" + lblTgdId.Text + "," _
                              & "@TGD_TGM_ID=" + ddlTarget.SelectedValue.ToString + "," _
                              & "@TGD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                              & "@TGD_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                              & "@TGD_SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                              & "@TGD_SLNO='" + txtSlno.Text + "'," _
                              & "@TGD_DESCR='" + txtQuestion.Text + "'," _
                              & "@TGD_OPTIONS=" + Val(txtOption.Text).ToString + "," _
                              & "@TGD_MINANSWER=" + Val(txtMinAnswer.Text).ToString + "," _
                              & "@TGD_TEXTTYPE='" + ddlTextType.SelectedValue.ToString + "'," _
                              & "@TGD_PRETEXT='" + txtPretext.Text + "'," _
                              & "@TGD_bMANDATORY='" + chkMandatory.Checked.ToString + "'," _
                              & "@TGD_bTARGET='" + chkTargetScore.Checked.ToString + "'," _
                              & "@MODE ='" + mode + "'"


                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Sub CopyData()
        Dim strGrades As String = ""

        Dim i As Integer

        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If strGrades <> "" Then
                    strGrades += "|"
                End If
                strGrades += lstGrade.Items(i).Value.ToString
            End If
        Next

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec SMART.copyTARGETS" _
                               & " @TGM_ID=" + ddlTarget.SelectedValue.ToString + "," _
                               & " @GRD_FROM='" + ddlGrade.SelectedValue.ToString + "'," _
                               & " @GRD_IDS='" + strGrades + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_TEXTTYPE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_SBG_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_SLNO"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_OPTIONS"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_MINANSWER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_PRETEXT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_bMANDATORY"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TGD_bTARGET"
        dt.Columns.Add(column)


        Return dt
    End Function

    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim j As Integer = 0
        Dim i As Integer
        Dim ds As DataSet
        Dim dr As DataRow
              Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGD_ID,TGD_TEXTTYPE,ISNULL(TGD_SBG_ID,0),TGD_SLNO,TGD_DESCR,TGD_OPTIONS,TGD_MINANSWER,TGD_PRETEXT," _
                              & " ISNULL(TGD_bMANDATORY,'FALSE'),ISNULL(TGD_bTARGET,'FALSE') " _
                              & " FROM SMART.TARGET_SETUP_D WHERE TGD_TGM_ID='" + ddlTarget.SelectedValue.ToString + "' " _
                              & " AND TGD_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                              & " ORDER BY TGD_SLNO "


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0).ToString
                dr.Item(1) = .Item(1)
                dr.Item(2) = "edit"
                dr.Item(3) = i.ToString
                dr.Item(4) = .Item(2).ToString
                dr.Item(5) = .Item(3).ToString
                dr.Item(6) = .Item(4).ToString.Replace(".00", "")
                dr.Item(7) = .Item(5).ToString
                dr.Item(8) = .Item(6).ToString
                dr.Item(9) = .Item(7).ToString
                dr.Item(10) = .Item(8).ToString
                dr.Item(11) = .Item(9).ToString
                dt.Rows.Add(dr)
            End With
        Next
        j = ds.Tables(0).Rows.Count



        Dim p As Integer

        Dim rows As Integer = Val(H_ROWS.Value)

        If rows = 0 Then
            rows = 49
        End If


        'If ddlGrade.SelectedValue = "09" Or ddlGrade.SelectedValue = "10" Or ddlGrade.SelectedValue = "11" Then
        '    rows = 74
        'Else
        '    rows = 49
        'End If


        For p = j To rows
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = "small"
            dr.Item(2) = "edit"
            dr.Item(3) = p.ToString
            dr.Item(4) = "0"
            dr.Item(5) = p + 1
            dr.Item(6) = ""
            dr.Item(7) = ""
            dr.Item(8) = ""
            dr.Item(9) = ""
            dr.Item(10) = "false"
            dr.Item(11) = "false"
            dt.Rows.Add(dr)

        Next



        Session("dtQuestions") = dt



        gvQuestions.DataSource = dt
        gvQuestions.DataBind()


        Dim ddlSubject As DropDownList
        Dim lblSbgId As Label
        Dim li As ListItem
        Dim lblTextType As Label
        Dim ddlTextType As DropDownList

        str_query = "SELECT SBG_ID,CASE WHEN STM_ID=1 THEN SBG_DESCR ELSE SBG_DESCR+'('+STM_DESCR+')' END SBG_DESCR FROM SUBJECTS_GRADE_S " _
                 & " INNER JOIN VW_STREAM_M ON STM_ID=SBG_STM_ID " _
                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SBG_DESCR NOT IN('THEORY','PRACT','PRACT.')"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        For i = 0 To gvQuestions.Rows.Count - 1
            ddlSubject = gvQuestions.Rows(i).FindControl("ddlSubject")
            lblSbgId = gvQuestions.Rows(i).FindControl("lblSbgId")
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "SBG_DESCR"
            ddlSubject.DataValueField = "SBG_ID"
            ddlSubject.DataBind()
            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlSubject.Items.Insert(0, li)

            If lblSbgId.Text <> "0" Then
                If Not ddlSubject.Items.FindByValue(lblSbgId.Text) Is Nothing Then
                    ddlSubject.Items.FindByValue(lblSbgId.Text).Selected = True
                End If
            End If

            lblTextType = gvQuestions.Rows(i).FindControl("lblTextType")
            ddlTextType = gvQuestions.Rows(i).FindControl("ddlTextType")
            If Not ddlTextType.Items.FindByValue(lblTextType.Text) Is Nothing Then
                ddlTextType.ClearSelection()
                ddlTextType.Items.FindByValue(lblTextType.Text).Selected = True
            End If
        Next

    End Sub


    Sub BindCopyGrade()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT GRM_GRD_ID ,GRM_DISPLAY  FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON GRD_ID=GRM_GRD_ID" _
                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID NOT IN('KG1','KG2','" + ddlGrade.SelectedValue.ToString + "')" _
                & " AND GRM_GRD_ID NOT IN(SELECT TGD_GRD_ID FROM SMART.TARGET_SETUP_D WHERE TGD_TGM_ID='" + ddlTarget.SelectedValue.ToString + "')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()

    End Sub



    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub



#End Region

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvQuestions.Rows(Val(lblindex.Text)).Visible = False

            If Val(lblindex.Text) <> 0 Then
                Dim lnkDelete As LinkButton = gvQuestions.Rows(Val(lblindex.Text) - 1).FindControl("lnkDelete")
                lnkDelete.Visible = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
 
   

    Protected Sub gvQuestions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQuestions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
           

        End If
    End Sub

    'Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
    '    BindUnit()
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        ViewState("datamode") = "edit"
        GridBind()

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindRowCount()
        BindTargetSetup()
        BindCopyGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindRowCount()
        BindCopyGrade()
        GridBind()
    End Sub

    Protected Sub lnkCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopy.Click
        MPOI.Show()
    End Sub

    Protected Sub ddlTarget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTarget.SelectedIndexChanged
        BindCopyGrade()
        GridBind()
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim url As String = "~/Curriculum/clmSMARTTargetSetup_Preview.aspx?grd_id=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) + "&tgm_id=" + Encr_decrData.Encrypt(ddlTarget.SelectedValue.ToString)
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        CopyData()

    End Sub
End Class
