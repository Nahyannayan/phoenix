<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectSyllabus_M.aspx.vb" Inherits="Curriculum_clmSubjectSyllabus_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

          <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Syllabus Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" >
          <tr>
            <td align="left"  colspan="6">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" Width="442px" />
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
             
                </td>
        </tr>
        <tr><td align="center">
     <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0"  width="100%">    
     <%-- <tr class="subheader_img">
      <td align="left" colspan="18" style="height: 25px; width: 726px;" valign="middle">
     <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
     SYLLABUS SET UP</span></font></td>
      </tr>--%>
        <tr><td  align="center" colspan="3">
        <br />
        <table id="Table3" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0"  Width="100%">
          <tr>
              <td align="left"  >
       <asp:Label ID="Label1" runat="server" Text="Curriculum"></asp:Label>
            </td>
           
       <td align="left" >
       <asp:DropDownList ID="ddlCurriculum" runat="server"  AutoPostBack="True">
       </asp:DropDownList>
            </td>
           <td align="left"  >
       <asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label>
       </td>
      
       <td align="left" >
       <asp:DropDownList ID="ddlSubject" runat="server" >
       </asp:DropDownList>
       </td>

      
         </tr><tr>
       
   
     
                                   <td align="left" >

                         <asp:Label ID="lblBoard" runat="server" Text="Board Code" ></asp:Label></td>
                        
                      <td align="left"  >
                          <asp:TextBox ID="txtBoardCode" runat="server" CausesValidation="True" ValidationGroup="groupM1" Width="102px" TabIndex="2" MaxLength="4"></asp:TextBox></td>
                    <td align="left" colspan="2">
               <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3"  /></td>
                   
           </tr>   
           </table>  
         </td></tr>
  
           <tr><td align="center"   >
     <table id="Table5" runat="server" align="center" border="0" cellpadding="0"  cellspacing="0"  >
           <tr> 
           <td align="center" >
                      <asp:GridView ID="gvSubject" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      HeaderStyle-Height="30" PageSize="20"   >
                     <Columns>
                       <asp:TemplateField HeaderText="Subject" Visible="false">
                      <ItemTemplate>
                      <asp:Label ID="lblSubId" Width="120px" runat="server" text='<%# Bind("SBM_ID") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Subject">
                      <ItemTemplate>
                      <asp:Label ID="lblSubject" Width="120px" runat="server" text='<%# Bind("SBM_DESCR") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Board code" >
                      <ItemTemplate>
                      <asp:Label ID="lblBoard" runat="server" text='<%# Bind("SBS_BOARDCODE") %>'></asp:Label>
                      </ItemTemplate>
                     </asp:TemplateField>
                       <asp:TemplateField HeaderText="clm" Visible="false">
                      <ItemTemplate>
                      <asp:Label ID="lblclm" Width="120px" runat="server" text='<%# Bind("clm") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                      <asp:ButtonField CommandName="edit" HeaderText="edit" Text="edit"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                     
                         <asp:ButtonField CommandName="delete" HeaderText="delete" Text="delete"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                       
                       </Columns>  
                     <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                          <RowStyle CssClass="griditem" Height="25px" />
                          <SelectedRowStyle CssClass="Green" />
                          <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                   
           </td>
           </tr>
         </table>  
         </td></tr>
           <tr>
            <td >
                &nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" />
                </td>
        </tr>
           </table> 
         </td></tr>
           
                  
   </table>
    
            </div>
        </div>
    </div>
</asp:Content>

