Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Curriculum_clmStudentOptions_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100080") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfACD_ID.Value = 0
                    hfGRD_ID.Value = 0
                    hfSTM_ID.Value = 0
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    PopulateGradeStream()
                    PopulateSection()
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    tdOption.Rows(4).Visible = False
                    tdOption.Rows(5).Visible = False
                    'tdOption.Rows(6).Visible = False 'error 

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            studClass.SetChk(gvStud, Session("liUserList"))
        End If

        If hfACD_ID.Value <> 0 Then
            BindDataList()
        End If

    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvStud.Rows.Count - 1
            Dim row As GridViewRow = gvStud.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub


    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvStud.PageSize * gvStud.PageIndex)
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub SaveData()
        Dim item As DataListItem
        Dim lblOptId As Label
        Dim ddlOption As DropDownList
        Dim options As New ArrayList
        Dim subjects As New ArrayList
        

        For Each item In dlOptions.Items
            lblOptId = item.FindControl("lblOptId")
            ddlOption = item.FindControl(lblOptId.Text)
            If ddlOption.SelectedValue <> "0" Then
                If subjects.IndexOf(ddlOption.SelectedValue.ToString) <> -1 Then
                    'lblError.Text = "The subjects from each bucket has to be unqiue"
                    UserMsgBox("The subjects from each bucket has to be unqiue", Me)
                    Exit Sub
                End If
                subjects.Add(ddlOption.SelectedValue.ToString)
                options.Add(lblOptId.Text + "|" + ddlOption.SelectedValue.ToString)
            End If
        Next

        If options.Count = 0 Then
            'lblError.Text = "Please select a subject"
            UserMsgBox("Please select a subject", Me)
            Exit Sub
        End If

        Dim stuXML As String = GetStudXML()
        If stuXML = "" Then
            'lblError.Text = "Please select a student"
            UserMsgBox("Please select a student", Me)
            Exit Sub
        End If
        Dim optionXML As String = GetOptionXML(options)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "exec SaveSTUDENTGRADEOPTIONS_M " _
                                 & hfACD_ID.Value + "," _
                                 & "'" + hfGRD_ID.Value + "'," _
                                 & "'" + optionXML + "'," _
                                 & "'" + stuXML + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub

    Function GetOptionXML(ByVal options As ArrayList) As String
        Dim i As Integer
        Dim str As String = ""
        Dim opt As String()
        For i = 0 To options.Count - 1
            opt = options(i).ToString.Split("|")
            str += "<ID><OPT_ID>" + opt(0) + "</OPT_ID><SBG_ID>" + opt(1) + "</SBG_ID><SBM_ID>" + opt(2) + "</SBM_ID></ID>"
        Next

        Return "<IDS>" + str + "</IDS>"
    End Function

    Function GetStudXML() As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblSctId As Label
        Dim str As String = ""
       
        Dim ids As String()
        Dim hash As New Hashtable

        Dim chk As CheckBox
        Dim id As String
        Dim stuID As String = String.Empty
        Dim sctID As String = String.Empty

        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvStud.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
            stuID = DirectCast(rowItem.FindControl("lblStuId"), Label).Text
            sctID = DirectCast(rowItem.FindControl("lblSctId"), Label).Text
            id = stuID + "|" + sctID
            If chk.Checked = True Then
                If hash.Contains(id) = False Then
                    hash.Add(id, id)
                End If
            Else
                If hash.Contains(id) = True Then
                    hash.Remove(id)
                End If
            End If
        Next

        Session("hashCheck") = hash

        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry
            For Each hashloop In hash
                ids = hashloop.Value.ToString.Split("|")
                str += "<ID><STU_ID>" + ids(0) + "</STU_ID><SCT_ID>" + ids(1) + "</SCT_ID></ID>"
            Next
            'Else
            '    With gvStud
            '        For i = 0 To .Rows.Count - 1
            '            chkSelect = .Rows(i).FindControl("chkSelect")
            '            If chkSelect.Checked = True Then
            '                lblStuId = .Rows(i).FindControl("lblStuid")
            '                lblSctId = .Rows(i).FindControl("lblSctId")
            '                str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID><SCT_ID>" + lblSctId.Text + "</SCT_ID></ID>"
            '            End If
            '        Next
            '    End With
        End If

        If str <> "" Then
            Return "<IDS>" + str + "</IDS>"
        Else
            Return ""
        End If
    End Function

    Sub BindDataList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SGO_OPT_ID,OPT_DESCR FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID.Value _
                                & "' AND SBG_STM_ID=" + hfSTM_ID.Value _
                                & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                                & " ORDER BY OPT_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlOptions.DataSource = ds
        dlOptions.DataBind()

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow
        Dim ddlOption As DropDownList
        Dim lblOptId As Label
        Dim li As ListItem
        Dim optPanel As Panel

        Dim ds1 As DataSet

        Dim dlItem As DataListItem

        For Each dlItem In dlOptions.Items

            lblOptId = dlItem.FindControl("lblOptId")

            str_query = "SELECT DISTINCT SBG_ID=CONVERT(VARCHAR(100),SBG_ID)+'|'+CONVERT(VARCHAR(100),SBG_SBM_ID),SBG_DESCR FROM " _
                       & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                       & " SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                       & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID.Value _
                       & "' AND SBG_STM_ID=" + hfSTM_ID.Value _
                       & " AND SBG_ACD_ID=" + hfACD_ID.Value _
                       & " AND SGO_OPT_ID=" + lblOptId.Text _
                       & " ORDER BY SBG_DESCR "

            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlOption = New DropDownList
            ddlOption.ID = lblOptId.Text
            ddlOption.Width = 120
            ddlOption.DataSource = ds1
            ddlOption.DataTextField = "SBG_DESCR"
            ddlOption.DataValueField = "SBG_ID"
            ddlOption.DataBind()

            li = New ListItem
            li.Text = "--"
            li.Value = "0"

            ddlOption.Items.Insert(0, li)

            optPanel = dlItem.FindControl("optPanel")
            optPanel.Controls.Add(ddlOption)

        Next
    End Sub
    Public Sub PopulateGradeStream()

        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If

    End Sub

    Sub PopulateSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SCT_DESCR<>'TEMP'" _
                              & " ORDER BY SCT_DESCR"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = DS
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim LI As New ListItem
        LI.Text = "ALL"
        LI.Value = "0"

        ddlSection.Items.Insert(0, LI)

    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String

        'students will be displayed for group allocation
        ' after all options for the student are allotted

        strQuery = "SELECT STU_ID,SCT_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                      & " SCT_DESCR ,SUBJECTS=ISNULL((SELECT STUFF((SELECT ', '+SBG_DESCR FROM SUBJECTS_GRADE_S AS D" _
                      & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS E ON D.SBG_ID=E.SGO_SBG_ID " _
                      & " INNER JOIN OPTIONS_M AS F ON  E.SGO_OPT_ID=F.OPT_ID " _
                      & " INNER JOIN  STUDENT_GROUPS_S AS G ON G.SSD_SBG_ID=D.SBG_ID " _
                      & " AND E.SGO_OPT_ID=G.SSD_OPT_ID AND SSD_STU_ID=A.STU_ID AND SSD_ACD_ID=" + hfACD_ID.Value _
                      & " ORDER BY OPT_DESCR for xml path('')),1,1,'')) ,'')" _
                      & " FROM STUDENT_M  AS A " _
                      & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                      & " WHERE STU_ID NOT IN (SELECT STU_ID FROM STUDENT_GROUPS_S WHERE " _
                      & " SSD_STU_ID=A.STU_ID AND SSD_ACD_ID=" + hfACD_ID.Value + " AND SSD_SGR_ID IS NOT NULL AND SSD_OPT_ID IS NOT NULL) " _
                      & " AND  STU_ACD_ID=" + hfACD_ID.Value _
                      & " AND STU_GRD_ID='" + hfGRD_ID.Value + "'" _
                      & " AND STU_STM_ID=" + hfSTM_ID.Value _
                      & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                      & " AND STU_CURRSTATUS NOT IN('CN','TF')"

        If ddlSection.SelectedValue <> "0" Then
            strQuery += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox


        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""

        Dim sectionItems As ListItemCollection


        If gvStud.Rows.Count > 0 Then

            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
                sectionItems = ddlgvSection.Items
            End If


            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If


        strQuery += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            'If ViewState("stumode") = "view" Then
            '    btnRemove.Visible = False
            'Else
            '    btnAllocate.Visible = False
            'End If
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("stumode") = "view" Then
                gvStud.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvStud.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvStud.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvStud.Rows.Count > 0 Then


            ddlgvSection = gvStud.HeaderRow.FindControl("ddlgvSection")
            Dim dr As DataRow
            Dim i As Integer
            If selectedSection = "" Then
                ddlgvSection.Items.Clear()
                ddlgvSection.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                            ddlgvSection.Items.Add(.Item(4))
                        End If
                    End With

                Next
            Else
                ddlgvSection.Items.Clear()
                For i = 0 To sectionItems.Count - 1
                    ddlgvSection.Items.Add(sectionItems.Item(i))
                Next
                ddlgvSection.Text = selectedSection
            End If


        End If
        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub

    Public Sub UserMsgBox(ByVal sMsg As String, ByVal frmMe As Object)

        Dim sb As New StringBuilder()
        Dim oFormObject As New System.Web.UI.Control
        If sMsg = "" Then
            Exit Sub
        End If
        sMsg = sMsg.Replace("'", "\'")
        sMsg = sMsg.Replace(Chr(34), "\" & Chr(34))
        sMsg = sMsg.Replace(vbCrLf, "\n")
        sMsg = "<script language=javascript>alert(""" & sMsg & """)</script>"

        sb = New StringBuilder()
        sb.Append(sMsg)

        For Each oFormObject In frmMe.Controls
            If TypeOf oFormObject Is HtmlForm Then
                Exit For
            End If
        Next

        ' Add the javascript after the form object so that the 
        ' message doesn't appear on a blank screen.
        oFormObject.Controls.AddAt(oFormObject.Controls.Count, New LiteralControl(sb.ToString()))

    End Sub

#End Region

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            Session("liUserList") = New List(Of String)
            Session("hashCheck") = Nothing
            hfACD_ID.Value = ddlAcademicYear.SelectedValue
            hfGRD_ID.Value = ddlGrade.SelectedValue
            hfSTM_ID.Value = ddlStream.SelectedValue
            gvStud.DataBind()
            BindDataList()
            GridBind()
            btnSave.Visible = True
            tdOption.Rows(4).Visible = True
            tdOption.Rows(5).Visible = True
            'tdOption.Rows(6).Visible = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            studClass.SetChk(gvStud, Session("liUserList"))
            PopulateGradeStream()
            PopulateSection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            studClass.SetChk(gvStud, Session("liUserList"))
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            PopulateGradeStream()
            PopulateSection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub gvStud_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStud.PageIndexChanged
        Try
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If


            Dim chk As CheckBox
            Dim stuID As String = String.Empty
            Dim sctId As String = String.Empty
            Dim id As String = String.Empty
            For Each rowItem As GridViewRow In gvStud.Rows
                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
                stuID = DirectCast(rowItem.FindControl("lblStuId"), Label).Text
                sctId = DirectCast(rowItem.FindControl("lblSctId"), Label).Text
                id = stuID + "|" + sctId
                If hash.ContainsValue(id) = True Then
                    chk.Checked = True
                Else
                    chk.Checked = False
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If


            Dim chk As CheckBox
            Dim stuID As String = String.Empty
            Dim sctId As String = String.Empty
            Dim id As String = String.Empty
            For Each rowItem As GridViewRow In gvStud.Rows
                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
                stuID = DirectCast(rowItem.FindControl("lblStuId"), Label).Text
                sctId = DirectCast(rowItem.FindControl("lblSctId"), Label).Text
                id = stuID + "|" + sctId
                If chk.Checked = True Then
                    If hash.Contains(id) = False Then
                        hash.Add(id, id)
                    End If
                Else
                    If hash.Contains(id) = True Then
                        hash.Remove(id)
                    End If
                End If
            Next

            Session("hashCheck") = hash
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            Session("liUserList") = New List(Of String)
            Session.Remove("hashCheck")
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

   
    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        Try
            SaveData()
            Session("liUserList") = New List(Of String)
            Session.Remove("hashCheck")
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
End Class
