Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Partial Class Students_studAtt_Registration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_PROCESSING_REPORT_BACKEND) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call GetEmpID()

                    callYEAR_DESCRBind()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = calltransaction(errorMessage)
        If str_err = "0" Then

            lblError.Text = "Record Saved Successfully"

        Else
            lblError.Text = errorMessage

        End If

    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim status As Integer
        Dim ACD_ID As String = ddlAcd_Year.SelectedValue
        Dim GRD_ID As String = String.Empty
        Dim TRM_ID As String = String.Empty
        Dim SBG_ID As String = String.Empty
        Dim SGR_ID As String = String.Empty
        Dim RSM_ID As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim RSD_ID As String = String.Empty
        Dim RPT_XML As String = ""
       
        If ddlTrm_id.SelectedIndex <> -1 Then
            TRM_ID = ddlTrm_id.SelectedValue
        End If

       
       
        If ddlRSM_ID.SelectedIndex <> -1 Then
            RSM_ID = ddlRSM_ID.SelectedValue
        End If
        If ddlRPF_ID.SelectedIndex <> -1 Then
            RPF_ID = ddlRPF_ID.SelectedValue
        End If
        

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                bEdit = False
                status = ACTIVITYMASTER.SaveREPORT_STUDENT_S(RPT_XML, RPF_ID, ACD_ID, GRD_ID, SGR_ID, SBG_ID, RSD_ID, transaction)

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error in inserting new record", "calltransaction")
                    Return "1"
                End If
                ViewState("viewid") = "0"
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "calltransaction")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
  

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        If ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
   
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
" FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
" WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcd_Year.Items.Clear()
            ddlAcd_Year.DataSource = ds.Tables(0)
            ddlAcd_Year.DataTextField = "ACY_DESCR"
            ddlAcd_Year.DataValueField = "ACD_ID"
            ddlAcd_Year.DataBind()
            If Not ddlAcd_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAcd_Year.ClearSelection()
                ddlAcd_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            ddlAcd_Year_SelectedIndexChanged(ddlAcd_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLTERM_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            str_Sql = " SELECT    TRM_ID, TRM_DESCRIPTION FROM  VW_TRM_M WHERE TRM_ACD_ID= '" & ACD_ID & "' ORDER BY TRM_DESCRIPTION "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlTrm_id.Items.Clear()
            ddlTrm_id.DataSource = ds.Tables(0)
            ddlTrm_id.DataTextField = "TRM_DESCRIPTION"
            ddlTrm_id.DataValueField = "TRM_ID"
            ddlTrm_id.DataBind()
            ddlTrm_id.Items.Add(New ListItem("FINAL TERM", "0"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_CardBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            str_Sql = " SELECT     RSM_ID,RSM_DESCR FROM  RPT.REPORT_SETUP_M WHERE RSM_ACD_ID='" & ACD_ID & "' ORDER BY RSM_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRSM_ID.Items.Clear()
            ddlRSM_ID.DataSource = ds.Tables(0)
            ddlRSM_ID.DataTextField = "RSM_DESCR"
            ddlRSM_ID.DataValueField = "RSM_ID"
            ddlRSM_ID.DataBind()
            ddlRSM_ID_SelectedIndexChanged(ddlRSM_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_PrintForBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim RSM_ID As String = ddlRSM_ID.SelectedItem.Value
            str_Sql = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" & RSM_ID & "' ORDER BY RPF_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRPF_ID.Items.Clear()
            ddlRPF_ID.DataSource = ds.Tables(0)
            ddlRPF_ID.DataTextField = "RPF_DESCR"
            ddlRPF_ID.DataValueField = "RPF_ID"
            ddlRPF_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLReport_HeaderBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim RSM_ID As String = ddlRSM_ID.SelectedItem.Value
            str_Sql = "SELECT    RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D where RSD_RSM_ID='" & RSM_ID & "'  and RSD_bDIRECTENTRY=0 ORDER BY RSD_DISPLAYORDER"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            chkRSD_IDs.Items.Clear()
            chkRSD_IDs.DataSource = ds.Tables(0)
            chkRSD_IDs.DataTextField = "RSD_HEADER"
            chkRSD_IDs.DataValueField = "RSD_ID"
            chkRSD_IDs.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcd_Year.SelectedItem.Value
            Dim TRM_ID As String = String.Empty
            If ddlTrm_id.SelectedIndex <> -1 Then
                If ddlTrm_id.SelectedValue = 0 Then
                    TRM_ID = "  RPT.REPORT_RULE_M.RRM_TRM_ID IS NULL "
                Else
                    TRM_ID = "  RPT.REPORT_RULE_M.RRM_TRM_ID = '" & ddlTrm_id.SelectedValue & "'"
                End If


            End If
            Dim RSM_ID As String = String.Empty
            If ddlRSM_ID.SelectedIndex <> -1 Then
                RSM_ID = ddlRSM_ID.SelectedValue
            End If

            Dim RPF_ID As String = String.Empty
            If ddlRPF_ID.SelectedIndex <> -1 Then
                RPF_ID = ddlRPF_ID.SelectedValue
            End If
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, VW_GRADE_M.GRD_DISPLAYORDER " & _
" FROM   VW_GRADE_BSU_M INNER JOIN    VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
" RPT.REPORT_RULE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORT_RULE_M.RRM_GRD_ID AND " & _
" VW_GRADE_BSU_M.GRM_ACD_ID = RPT.REPORT_RULE_M.RRM_ACD_ID WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') " & _
" and RPT.REPORT_RULE_M.RRM_RSM_ID = '" & RSM_ID & "' and  " & TRM_ID & " and RPT.REPORT_RULE_M.RRM_RPF_ID  = '" & RPF_ID & "'" & _
" ORDER BY VW_GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            chkGrade.Items.Clear()
            chkGrade.DataSource = ds.Tables(0)
            chkGrade.DataTextField = "GRM_DISPLAY"
            chkGrade.DataValueField = "GRD_ID"
            chkGrade.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcd_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLTERM_DESCRBind()
        CALLReport_CardBind()
        bindAcademic_Grade()
    End Sub

    Protected Sub ddlTrm_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLReport_CardBind()
    End Sub

    Protected Sub ddlRSM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CALLReport_PrintForBind()
        CALLReport_HeaderBind()
        bindAcademic_Grade()
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim ACD_ID As String = ddlAcd_Year.SelectedValue
        Dim GRD_IDS As String = String.Empty
        Dim TRM_ID As String = String.Empty
        Dim SBG_ID As String = String.Empty
        Dim SGR_ID As String = String.Empty
        Dim RSM_ID As String = String.Empty
        Dim RPF_ID As String = String.Empty
        Dim RSD_IDS As String = String.Empty
        Dim GRADE_bSELECT As Boolean = False
        Dim HEADER_bSELECT As Boolean = False

        If ddlTrm_id.SelectedIndex <> -1 Then
            TRM_ID = ddlTrm_id.SelectedValue
        End If


        If ddlRSM_ID.SelectedIndex <> -1 Then
            RSM_ID = ddlRSM_ID.SelectedValue
        End If
        If ddlRPF_ID.SelectedIndex <> -1 Then
            RPF_ID = ddlRPF_ID.SelectedValue
        End If

        For Each item As ListItem In chkGrade.Items
            If item.Selected = True Then
                GRADE_bSELECT = True
                GRD_IDS += item.Value + "|"
            End If
        Next
        For Each item As ListItem In chkRSD_IDs.Items
            If item.Selected = True Then
                HEADER_bSELECT = True
                RSD_IDS += item.Value + "|"
            End If
        Next
        If GRADE_bSELECT = False Or HEADER_bSELECT = False Then

            lblError.Text = "Atleast  one Column Header and Grade must be selected !!! "
            Exit Sub
        End If

        Dim status As Integer
        Dim errormessage As String = String.Empty
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                status = ACTIVITYMASTER.SaveREPORT_STUDENT_PROCESS_BACKEND(ACD_ID, GRD_IDS, RPF_ID, _
              RSD_IDS, TRM_ID, RSM_ID, transaction)
             
            Catch ex As Exception
                status = 1
                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")
            Finally
                If status <> 0 Then
                    lblError.Text = "Error occured while processing"
                    transaction.Rollback()
                Else

                    transaction.Commit()
                    lblError.Text = "Report Processed Successfully"
                End If
            End Try

        End Using

    End Sub

    Protected Sub ddlRPF_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
      
        bindAcademic_Grade()
        
    End Sub

    

End Class
