<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmSyllabus_D_AddEdit.aspx.vb" Inherits="Curriculum_clmSyllabus_D_AddEdit"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">

        function GetTopic() {
            var NameandCode;
            var syllabusId = document.getElementById('<%=h_SyllabusId.ClientID %>').value;
            var oWnd = radopen("showTopics.aspx?syllabusId=" + syllabusId, "pop_topic");

        }

        function OnClientClose3(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=h_ParentTopicID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtParentTopic.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtParentTopic.ClientID%>', 'TextChanged');
            }
        }



        function GetDocuments() {
            var NameandCode;
            var result;
            var val;
            var GRD_IDs = document.getElementById('<%=txtGrade.ClientID %>').value;
            var SBM_IDs = document.getElementById('<%=txtSubject.ClientID %>').value;

            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            val = document.getElementById('<%=h_doc_IDs.ClientID %>').value;
            if (val != '') {
                val += "___";
            }

            var oWnd = radopen("showDocument.aspx?multiselect=true&ID=DOC&GRD_IDs=" + GRD_IDs + "&SBM_IDs=" + SBM_IDs, "pop_document");
        }
        function OnClientClose(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Document.split('||');
                val += NameandCode[0];
                document.getElementById('<%=h_doc_IDs.ClientID%>').value = val

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_document" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Syllabus Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr id="Tr1" runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAccYear" runat="server" Enabled="False">
                            </asp:TextBox></td>
                        <td align="left" width="20%"><span class="field-label">Term</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtTerm" runat="server" Enabled="False">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtGrade" runat="server" Enabled="False">
                            </asp:TextBox></td>
                        <td align="left"><span class="field-label">Subject</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtSubject" runat="server" Enabled="False"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg-lite">
                            <asp:Label ID="Label1" runat="server" Text="Details.."></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Parent Topic</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtParentTopic" runat="server" Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetTopic();return false"></asp:ImageButton></td>
                        <td align="left"><span class="field-label">Topic Name</span><asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtSubTopic" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSubTopic"
                                Display="Dynamic" ErrorMessage="Topic Name" ForeColor="White" ValidationGroup="SUBERROR"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Planned From Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                        <td align="left"><span class="field-label">Planned To Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox><asp:ImageButton
                                ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Display Order</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Planned Total Hrs</span>
                            <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtHrs" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Objective</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtobjective" runat="server"
                                TextMode="MultiLine"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Subject Activities/Resources</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtResource" runat="server"
                                TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Document Name</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtStudIDs" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server"
                                ImageUrl="~/Images/cal.gif" OnClientClick="GetDocuments();return false"
                                OnClick="imgStudent_Click" />

                            <asp:GridView ID="grdDoc" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
                                PageSize="5" OnPageIndexChanging="grdDoc_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="id" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Description"></asp:BoundField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="txtfile" runat="server"
                                                Text='<%# Bind("FNAME") %>' CommandName="View" CommandArgument='<%# Bind("ID") %>'
                                                OnClick="txtfile_Click" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="lnkDelete" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR"
                                OnClick="btnDetAdd_Click" Visible="False" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnSubCancel_Click" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="title-bg-lite" colspan="4">
                            <asp:Label ID="Label2" runat="server" Text="Topic Details"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvSyllabus" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" OnPageIndexChanging="gvSyllabus_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="ID">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Topic No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubJId" runat="server" Text='<%# Bind("SUBID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ParentTopic">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMainTopic" runat="server" Text='<%# bind("SYBPARENTDESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubTopic">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubTopic" runat="server" Text='<%# bind("SYBDESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ParentID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentId" runat="server" Text='<%# bind("SYBPARENTID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned From Date">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("SYBSTARTDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned To Date">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("SYBENDDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned Total Hrs">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalHrs" runat="server" Text='<%# Bind("SYBTOTHRS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SydId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSydId" runat="server" Text='<%# Bind("SydId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Objective">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblObjective" runat="server" Text='<%# Bind("SYBOBJECTIVE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject Activities/Resources">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblResource" runat="server" Text='<%# Bind("SYBRESOURCE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Display Order">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("SYBORDER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remove">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" OnClick="lnkDelete_Click" runat="server">Delete</asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="lnkDelete" ConfirmText="Do you want to delete this topic?">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_ParentID" runat="server" />
                <asp:HiddenField ID="h_TERMID" runat="server" />
                <asp:HiddenField ID="h_ACCID" runat="server" />
                <asp:HiddenField ID="h_SUBJID" runat="server" />
                <asp:HiddenField ID="h_SyllabusId" runat="server" />
                <asp:HiddenField ID="h_doc_IDs" runat="server" />
                <asp:HiddenField ID="h_SYD_ID" runat="server"></asp:HiddenField>
                <br />
                <asp:HiddenField ID="h_ParentTopicID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
