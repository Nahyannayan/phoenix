<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidatedFeedback.aspx.vb" Inherits="Curriculum_Reports_Aspx_clmConsolidatedFeedback" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
 
    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }


        function fnSelectAll_bkp(chkObj) {
            var multi = document.getElementById("<%=lstSubject.ClientID%>");
                        if (multi != null && multi.options != null) {
                            if (chkObj.checked)
                                for (i = 0; i < multi.options.length; i++)
                                    multi.options[i].selected = true;
                            else
                                for (i = 0; i < multi.options.length; i++)
                                    multi.options[i].selected = false;
                        }
                    }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Consolidated Report-Feedback"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" width="100%">

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle" class="matters" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr visible="false">
                        <td align="left" class="matters"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr visible="false">
                        <td align="left" valign="middle" class="matters"><span class="field-label">Report Schedule</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Options</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:CheckBox ID="chkCAT" runat="server" Text="CAT 4" Checked="true" CssClass="field-label" /><asp:CheckBox ID="chkSubject" runat="server" Text="Subject" CssClass="field-label" AutoPostBack="true" /></td>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trAsset" visible="false">
                        <td align="left" class="matters" valign="middle "><span class="field-label">Asset</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlAssest" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Science Percentile" Value="SCI" />
                                <asp:ListItem Text="Math Percentile" Value="MAT" />
                                <asp:ListItem Text="English Percentile" Value="ENG" />
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trSubject" runat="server" visible="false">
                        <td align="left" valign="middle" class="matters"><span class="field-label">Subject</span></td>
                        <td align="left" valign="middle" class="matters">

                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" Style="display: none;" />
                            <br />
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="LstSubject" runat="server" AutoPostBack="true" RepeatLayout="Flow" >
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left" valign="middle" class="matters"><span class="field-label">Header</span></td>
                        <td align="left" valign="middle" class="matters">
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="lstHeader" runat="server"
                                    RepeatLayout="Flow">
                                </asp:CheckBoxList>
                            </div>
                            <asp:CheckBox ID="chkCriterias" Text="Include Subject Criterias" runat="server" /></td>
                    </tr>
               
                    <tr runat="server" style="display: none;">
                        <td align="left" class="matters" valign="middle"><span class="field-label">Include TC Students</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:CheckBox ID="chkTc" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="trfilter" style="display: none;">
                        <td align="left" class="matters" valign="middle">Filter By</td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                Text="Generate Report" ValidationGroup="groupM1" />
                            
                <asp:Button ID="btnGeneratePDF" runat="server" CssClass="button"  
                    Text="Generate PDF" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfbDownload" runat="server" />

                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>


            </div>
        </div>
    </div>
</asp:Content>

