<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReportAnalysisMark.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptReportAnalysisMark" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<style>
    table td select {       
        min-width:5% !important;
    }
</style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Subject Mark Analysis By Nationality"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table align="center" width="100%" cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>

                    <tr runat="server" id="trRPFID">
                        <td align="left" width="20%"><span class="field-label">Report Printed For</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Header</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlHeader" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <%-- <tr runat="server" id="trHeader">                                                        
                        </tr>--%>

                    <tr id="trGradeSection" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlGrade" Width="37%" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlStream1" Width="37%"  runat="server"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Section</span>
                        </td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>


                    <tr runat="server" id="trSubjectChkList">
                        <td align="left" width="20%" valign="top"><span class="field-label">Subject</span></td>

                        <td align="left" width="30%" valign="top">
                             <div class="checkbox-list">
                            <asp:CheckBoxList ID="chkListSubjects" runat="server">
                            </asp:CheckBoxList></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:RadioButtonList ID="rdList" runat="server" RepeatDirection="Horizontal" CssClass="field-label" Font-Bold="true">
                                <asp:ListItem Selected="True">Mark</asp:ListItem>
                                <asp:ListItem>Level</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" CssClass="button" runat="server" ValidationGroup="groupM1" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in Pdf" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />

                <asp:HiddenField ID="h_SBG_IDs" runat="server" />

                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>
        </div>
    </div>
</asp:Content>

