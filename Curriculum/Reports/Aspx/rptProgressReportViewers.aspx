<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptProgressReportViewers.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
              <asp:Label ID="lblHeader" runat="server" Text="Progress Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Report Printed For</span></td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server"   >
                            </asp:DropDownList></td>
                        <td align="left" valign="middle"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                       </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label">Section</span>
                        </td>
                        <td align="left" colspan="1" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trReportType" runat="server">
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radViewedUsers" AutoPostBack="true" runat="server" GroupName="Report_Type" CssClass="field-label" Checked="True" Text="Viewed Users"></asp:RadioButton>
                            <asp:RadioButton ID="radPendingUsers" AutoPostBack="true" runat="server" GroupName="Report_Type" CssClass="field-label" Text="Pending For Viewing"></asp:RadioButton>
                            <asp:RadioButton ID="radSummaryUsers" AutoPostBack="true" runat="server" GroupName="Report_Type" CssClass="field-label" Text="Summary"></asp:RadioButton></td>
                    </tr>
                    <tr class="MATTERS" id="trDateRange" runat="server">
                        <td align="left">
                            <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="field-label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="txtfromdate_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtfromdate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2"
                                runat="server" TargetControlID="txtfromdate" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtfromdate" ErrorMessage="Date Required"
                                ValidationGroup="GR">*</asp:RequiredFieldValidator>
                        </td>
                        <td runat="server" id="tdToDate_1"><span class="field-label">To Date</span></td>
                        <td runat="server" id="tdToDate_3">
                            <asp:TextBox ID="txttodate" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txttodate_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txttodate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="ImageButton2" runat="server"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txttodate" ErrorMessage="Date Required" ValidationGroup="GR">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                                runat="server" TargetControlID="txttodate" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" OnClick="btnGenerateReport_Click" ValidationGroup="GR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

