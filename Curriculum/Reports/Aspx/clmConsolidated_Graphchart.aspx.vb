﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Imports UtilityObj
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_Graphchart
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim studClass As New studClass

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If USR_NAME = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindHeader()
                    BindPrintedFor()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

                    If ddlGroup.SelectedItem Is Nothing Then
                        h_SBG_ID.Value = ""
                    Else
                        h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
                    End If

                    BindNationality()
                    ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If

    End Sub
    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")
        Dim currFns As New currFunctions
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim gradeCount As Integer
        If ViewState("GRD_ACCESS") > 0 Then
            str_query = " select COUNT(DISTINCT SCT_GRD_ID) FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_aCD_ID=" + ddlAcademicYear.SelectedValue + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & " AND SCT_GRD_ID='" + grd_id.Split("|")(0) + "'"


            gradeCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            gradeCount = 1
        End If

        If gradeCount = 0 Then
            Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
            ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Return ddlGroup
        End If

        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + acd_id

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.Split("|")(0) + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

        ddlGroup.Items.Insert(0, New ListItem("ALL", 0))
        ddlGroup.Items.FindByText("ALL").Selected = True
        Return ddlGroup
    End Function
    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
         & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlheader.DataSource = ds
        ddlheader.DataTextField = "RSD_HEADER"
        ddlheader.DataValueField = "RSD_ID"
        ddlheader.DataBind()
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlGroup.SelectedIndex = -1 Then
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If
    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id
        If ViewState("MainMnu_code") = "C300008" Then
            'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
            str_query += "  AND SBG_PARENT_ID=0"

        End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Sub BindNationality()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "Select * From (Select 0 as Cty_Id, '[Select]' as Cty_Nationality Union  SELECT CTY_ID, CTY_NATIONALITY FROM Country_M) T WHERE (T.Cty_Nationality IS NOT NULL AND T.Cty_Nationality <> '-') Order By T.Cty_Nationality "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        ddNationality.DataSource = ds 'OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                  & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                              & " order by grd_displayorder"
        ' 




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub
    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnGenReport_Click(sender As Object, e As EventArgs)
        If chkDtl.Checked = False And chkGraph.Checked = False Then
            lblError.Text = "Please select any of the type of report"
            Exit Sub
        End If
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0))
        param.Add("@RPF_ID", ddlPrintedFor.SelectedValue)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue)
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@SBG_ID", ddlSubject.SelectedValue)
        param.Add("@SGR_ID", ddlGroup.SelectedValue)
        param.Add("@STU_GENDER", ddlgender.SelectedValue)

        param.Add("@STU_NATIONALITY", ddNationality.SelectedValue)
        param.Add("@RSD_ID", ddlheader.SelectedValue)
        param.Add("@TEXT", txtValue.Text)

        'changed parameters
        param.Add("@STU_DOJ", txtEnrollDate.Text)
        param.Add("@STU_DOJ_TO", txtEnrollToDate.Text)
        param.Add("@SEN", ddlsen.SelectedValue)  '  -1 : All, 1 : Yes , 0 : No
        If ddlsen.SelectedValue = 1 Then
            param.Add("@WAVE", radwv.SelectedValue) ' 1 - WAVE1, 2- WAVE2, 3- WAVE3
        Else
            param.Add("@WAVE", 0)
        End If

        If chkDtl.Checked Then
            param.Add("@option", 2)
            param.Add("@calc", ddlcalc.SelectedValue)
        ElseIf chkGraph.Checked Then
            param.Add("@option", 1)
            param.Add("@calc", 0)
        End If

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")

        If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
            param.Add("@COMPARE", ddlcompare.SelectedValue)
        Else
            param.Add("@COMPARE", 0)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If chkDtl.Checked Then
                .reportPath = Server.MapPath("../Rpt/rptStudentAttainmentDtl.rpt")
            ElseIf chkGraph.Checked Then
                .reportPath = Server.MapPath("../Rpt/rptStudentAttainment.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindPrintedFor()
        BindHeader()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindReportCard()
        BindHeader()
        BindPrintedFor()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

        If ddlGroup.SelectedItem Is Nothing Then
            h_SBG_ID.Value = ""
        Else
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If

        BindNationality()
        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub chkDtl_CheckedChanged(sender As Object, e As EventArgs)
        If chkDtl.Checked = True Then
            tdCalc1.Visible = True
            tdcalc2.Visible = True
            chkGraph.Checked = False
            If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
                tdheader1.Width = "10%"
                tdheader2.Width = "15%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "15%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "15%"
                tdval1.Width = "10%"
                tdval2.Width = "15%"
            Else
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            End If
        Else
            tdCalc1.Visible = False
            tdcalc2.Visible = False
            chkGraph.Checked = True
            If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            Else
                tdheader1.Width = "20%"
                tdheader2.Width = "30%"
                tdval1.Width = "20%"
                tdval2.Width = "30%"
            End If
        End If
    End Sub

    Protected Sub ddlsen_SelectedIndexChanged(sender As Object, e As EventArgs)
        If ddlsen.SelectedValue = 1 Then
            radwv.Visible = True
        Else
            radwv.Visible = False
        End If
    End Sub

    Protected Sub chkGraph_CheckedChanged(sender As Object, e As EventArgs)
        If chkGraph.Checked = True Then
            tdCalc1.Visible = False
            tdcalc2.Visible = False
            chkDtl.Checked = False
            If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            Else
                tdheader1.Width = "20%"
                tdheader2.Width = "30%"
                tdval1.Width = "20%"
                tdval2.Width = "30%"
            End If
        Else
            tdCalc1.Visible = True
            tdcalc2.Visible = True
            chkDtl.Checked = True
            If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
                tdheader1.Width = "10%"
                tdheader2.Width = "15%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "15%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "15%"
                tdval1.Width = "10%"
                tdval2.Width = "15%"
            Else
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            End If
        End If
    End Sub
    Protected Sub bindcompare()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
         & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlcompare.DataSource = ds
        ddlcompare.DataTextField = "RSD_HEADER"
        ddlcompare.DataValueField = "RSD_ID"
        ddlcompare.DataBind()

        ddlcompare.Items.Remove(ddlcompare.Items.FindByText("Baseline")) ' removing  value 'Baseline'
        ddlcompare.Items.Remove(ddlcompare.Items.FindByText("Expected Progress")) ' removing  value 'Expected Progress'
        ddlcompare.Items.Remove(ddlcompare.Items.FindByText("Exceeding Progress")) ' removing  value 'Exceeding Progress'

    End Sub

    Protected Sub ddlheader_SelectedIndexChanged(sender As Object, e As EventArgs)
        bindcompare()
        If ddlheader.SelectedItem.Text = "Expected Progress" Or ddlheader.SelectedItem.Text = "Exceeding Progress" Then ' if selected value 'Expected Progress'/ 'Exceeding Progress'
            tdcompare1.Visible = True
            tdcompare2.Visible = True
            If chkDtl.Checked = True Then
                tdheader1.Width = "10%"
                tdheader2.Width = "15%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "15%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "15%"
                tdval1.Width = "10%"
                tdval2.Width = "15%"
            Else
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdcompare1.Width = "10%"
                tdcompare2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            End If
        Else
            tdcompare1.Visible = False
            tdcompare2.Visible = False
            If chkGraph.Checked = True Then
                tdheader1.Width = "20%"
                tdheader2.Width = "30%"
                tdval1.Width = "20%"
                tdval2.Width = "30%"
            Else
                tdheader1.Width = "10%"
                tdheader2.Width = "20%"
                tdCalc1.Width = "10%"
                tdcalc2.Width = "20%"
                tdval1.Width = "10%"
                tdval2.Width = "20%"
            End If
        End If
    End Sub
End Class
