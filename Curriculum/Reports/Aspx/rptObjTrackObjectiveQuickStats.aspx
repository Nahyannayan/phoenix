﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptObjTrackObjectiveQuickStats.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptObjTrackObjectiveQuickStats" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs = document.getElementById('<%=h_SGR_IDs.ClientID %>').value;
            if (SGR_IDs == '') {
                alert('Please select atleast one Subject Group')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&SGR_IDs=" + SGR_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="APP Quick Stats"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="center" valign="bottom" style="height: 20px">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="20%">
                                        <span class="field-label">Select Grade</span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="matters" align="left">
                                        <span class="field-label">Select Subject</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>

                                    <td class="matters" align="left">
                                        <span class="field-label">Select Group</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlGroup" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="matters" colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" TabIndex="7" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_STU_IDs" runat="server" />
                            <asp:HiddenField ID="h_SGR_IDs" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

