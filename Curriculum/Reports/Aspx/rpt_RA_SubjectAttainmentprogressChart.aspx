﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_RA_SubjectAttainmentprogressChart.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_SubjectAttainmentprogressChart"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
         <style>
      .col1
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 40%;
    line-height: 14px;
    float: left;
}
.col2
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 60%;
    line-height: 14px;
    float: left;
}
        
        .demo-container label {
    padding-right: 10px;
    width: 100%;
    display: inline-block;
}
 .rcbHeader ul,
.rcbFooter ul,
.rcbItem ul,
.rcbHovered ul,
.rcbDisabled ul {
    margin: 0;
    padding: 0;
    width: 90%;
    display: inline-block;
    list-style-type: none;
}
    .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
        display: inline;
        float: left;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
    }

    .RadComboBox_Default .rcbInner {
        padding: 10px;
        border-color: #dee2da !important;
        border-radius: 6px !important;
        box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        width: 80%;
        background-image: none !important;
        background-color: transparent !important;
    }

    .RadComboBox_Default .rcbInput {
        font-family: 'Nunito', sans-serif !important;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
        box-shadow: none;
    }

    .RadComboBox_Default .rcbActionButton {
        border: 0px;
        background-image: none !important;
        height: 100% !important;
        color: transparent !important;
        background-color: transparent !important;
    }
</style>   
    <script language="javascript" type="text/javascript">

        function openWin() {
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;

            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
          var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;


          if (GRD_IDs == '') {
              alert('Please select atleast one Grade')
              return false;
          }


          var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

      }

      function OnClientClose(oWnd, args) {
          //get the transferred arguments
          var arg = args.get_argument();
          if (arg) {
              NameandCode = arg.NameCode.split('||');
              document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
              document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
          <asp:Label ID="lblHeader" runat="server" Text="Subject Attainment Progress Chart"></asp:Label>
          <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Report Card</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server"   AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Schedule</span>
                        </td>
                        <td align="left">
                        <%--    <asp:CheckBoxList ID="ddlPrintedFor" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                Height="127px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                Width="239px">
                            </asp:CheckBoxList>--%>
                             <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="ddlPrintedFor" 
                                 CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Schedule(s)">  </telerik:RadComboBox>



                        </td>
                        <td align="left" valign="middle"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="field-label">Subject</span>
                        </td>

                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>

                        <td align="left" valign="top" colspan="2">
                            <asp:RadioButton ID="rdGradeWise" runat="server" Checked="True" GroupName="g1" Text="Report By Grade" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rdSectionWise" runat="server" GroupName="g1" Text="Report By Section" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rdStudWise" runat="server" GroupName="g1" Text="Report By Student" CssClass="field-label"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr id="trSection" runat="server">
                        <td align="left" valign="top"><span class="field-label">Section</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlSection" runat="server"  >
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trstud1" runat="server">
                        <td align="left" valign="top"><span class="field-label">Student</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtStudIDs" runat="server"   OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                OnClick="imgStudent_Click"></asp:ImageButton></td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" OnPageIndexChanging="grdStudent_PageIndexChanging" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trStud2" runat="server">
                        <td align="left" valign="top" colspan="4"  >
                            <asp:RadioButton ID="rdSectionCompare" runat="server" Checked="True" GroupName="g2" Text="Compare Student Performance With Section Performance" CssClass="field-label"
                                AutoPostBack="True" /> 
                            <asp:RadioButton ID="rdGradeCompare" runat="server" GroupName="g2" Text="Compare Student Performance With Grade Performance" AutoPostBack="True" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" valign="top"><span class="field-label">Nationality</span></td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlNationality" runat="server"  >
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Emiratis</asp:ListItem>
                                <asp:ListItem>Non Emiratis</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trNat" runat="server">
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                OnClick="btnGenerateReport_Click"  />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="h_SBG_IDs" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
