﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_Reports_Aspx_rptStudentLessonCriterias
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300245") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindTerm()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    BindSection()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub CallReport()

        Dim grade() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", grade(0))
        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        param.Add("@SYD_ID", h_TopicID.Value)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/LESSONPLANNER/rptStudentLessonCriteriasConsolidated.rpt")
        End With
        Session("rptClass") = rptClass

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String) As DropDownList
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String) As DropDownList
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Sub BindSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SCT_ID,SCT_DESCR FROM VW_SECTION_M INNER JOIN VW_GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE SCT_ACD_ID='" + ddlAcademicYear.SelectedValue + "'" _
                                & " AND SCT_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTerm()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection()
    End Sub
End Class
