﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptObjTrackObjectiveTrackerSnapshot.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptObjTrackObjectiveTrackerSnapshot" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs = document.getElementById('<%=h_SGR_IDs.ClientID %>').value;
            if (SGR_IDs == '') {
                alert('Please select atleast one Subject Group')
                return false;
            }

            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&SGR_IDs=" + SGR_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');               
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                 __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
             
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs = document.getElementById('<%=h_SGR_IDs.ClientID %>').value;
            if (SGR_IDs == '') {
                alert('Please select atleast one Subject Group')
                return false;
            }
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&SGR_IDs=" + SGR_IDs, "pop_form");
            <%--result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&SGR_IDs=" + SGR_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }
        function OnClientCloseform(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
                document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_form" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseform"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Objective Tracker Snapshot
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="center" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" ></asp:Label>
                            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" width="100%"
                                cellpadding="10" cellspacing="0" class="BlueTable">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>

                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Group</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGroup"  runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Student</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                                        <br />
                                        <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            PageSize="5" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Baseline Date</span></td>

                                    <td align="left">
                                        <%--<div class="checkbox-list">--%>
                                        <asp:ListBox ID="lstBaseLine" runat="server" AppendDataBoundItems="True"
                                            ></asp:ListBox><%--</div>--%>
                                    </td>
                                    <td><span class="field-label">Comparison Date</span></td>

                                    <td>
                                        <%--<div class="checkbox-list">--%>
                                        <asp:ListBox ID="lstComparison" runat="server" ></asp:ListBox><%--</div>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" TabIndex="7" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                            Text="Download Report in PDF" ValidationGroup="groupM1" TabIndex="7" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_STU_IDs" runat="server" />
                            <asp:HiddenField ID="hfbDownload" runat="server" />
                            <asp:HiddenField ID="h_SGR_IDs" runat="server" />
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

