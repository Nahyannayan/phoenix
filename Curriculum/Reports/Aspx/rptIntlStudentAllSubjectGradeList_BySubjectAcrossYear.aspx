<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlStudentAllSubjectGradeList_BySubjectAcrossYear.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlStudentAllSubjectGradeList_BySubjectAcrossYear" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs+"&ACD_ID="+ACD_IDs,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
   </script>
   <script language="javascript" type="text/javascript">


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);

}
}


function OnTreeClick1(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);
}
}


function CheckUncheckChildren(childContainer, check)
{
var childChkBoxes = childContainer.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = check;
}
}


function CheckUncheckParents(srcChild, check)
{
var parentDiv = GetParentByTagName("div", srcChild);
var parentNodeTable = parentDiv.previousSibling;
if(parentNodeTable)
{
var checkUncheckSwitch;
if(check) //checkbox checked
{
var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
if(isAllSiblingsChecked)
checkUncheckSwitch = true;
else
return; //do not need to check parent if any(one or more) child not checked
}
else //checkbox unchecked
{
checkUncheckSwitch = false;
}
var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
if(inpElemsInParentTable.length > 0)
{
var parentNodeChkBox = inpElemsInParentTable[0];
parentNodeChkBox.checked = checkUncheckSwitch;
//do the same recursively
CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
}
}
}

function AreAllSiblingsChecked(chkBox)
{
var parentDiv = GetParentByTagName("div", chkBox);
var childCount = parentDiv.childNodes.length;
for(var i=0;i<childCount;i++)
{
if(parentDiv.childNodes[i].nodeType == 1)
{
//check if the child node is an element node
if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
{
var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
//if any of sibling nodes are not checked, return false
if(!prevChkBox.checked)
{
return false;
}
}
}
}
return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
{
var parent = childElementObj.parentNode;
while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
{
parent = parent.parentNode;
}
return parent;
}

function UnCheckAll() 
{ 
var oTree=document.getElementById("<%=tvReport.ClientId %>");
childChkBoxes = oTree.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = false;
}

return true;
} 
</script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Student Performance Report </div>
 <div class="card-body">
            <div class="table-responsive m-auto">
   
 <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0" >
                 
              
           <tr>
            <td align="center"    valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblClm" runat="server" align="center"  cellpadding="0" cellspacing="0" width="100%"  >
                     
             
                      <tr>
                        <td align="left" >
                            <span class="field-label">Select Academic Year</span></td>
                       
                        <td align="left"  >
                             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               
                      </td>
                          <td align="left"  >
                            <span class="field-label">Grade</span></td>
                       
                        <td align="left"  >
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>
                       </tr>   
                 
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Select Section</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                        <td align="left"  >
                            <span class="field-label">Subject</span></td>
                       
                        <td align="left"  >
                      <%--  <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true"  >
                        </asp:DropDownList> --%>
                             <div class="checkbox-list">
                <asp:CheckBoxList ID="chkListSubjects" runat="server" OnSelectedIndexChanged="chkListSubjects_SelectedIndexChanged"  AutoPostBack="true" >
                </asp:CheckBoxList>
                </div>


             

                        </td>
                  
                    </tr>
                   
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Group</span></td>
                       
                        <td align="left"  >
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="true"  >
                        </asp:DropDownList>
            </td>
                        <td align="left"  >
                            <span class="field-label">Select Report Schedule</span></td>
                      
                        <td align="left" >
                            <div class="checkbox-list">
                            <asp:TreeView id="tvReport" runat="server" 
                                ExpandDepth="1"  MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                PopulateNodesFromClient="False" ShowCheckBoxes="All" 
                                >
                                <parentnodestyle />
                                <hovernodestyle  />
                                <selectednodestyle 
                                     />
                                <nodestyle   />
                            </asp:TreeView></div></td>
                    </tr>
                  
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Display All Columns</span></td>
                        
                        <td align="left" >
                            <asp:CheckBox id="chkDisplayALL" runat="server">
                            </asp:CheckBox></td>
                         <td align="left"><span class="field-label">Filter By</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlType" runat="server">
                    <asp:ListItem>ALL</asp:ListItem>
                    <asp:ListItem>SEN</asp:ListItem>
                    <asp:ListItem>EAL</asp:ListItem>
                    <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                    <asp:ListItem>EMIRATI</asp:ListItem>
                    <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                </asp:DropDownList></td>
                    </tr>
                    
                      
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" />
                            <asp:Button id="btnDownload" runat="server" CssClass="button"  Text="Download Report in PDF"
                                ValidationGroup="groupM1" /></td>
                    </tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
               </td></tr>
           
    
</table>
    <asp:HiddenField id="h_STU_IDs" runat="server">
    </asp:HiddenField>
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
    <asp:HiddenField id="hfbDownload" runat="server">
    </asp:HiddenField>
     
                </div>
     </div></div>



</asp:Content>

