﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ExamPaperReport.aspx.vb" Inherits="Curriculum_Reports_ASPX_ExamPaperReport" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3 flip"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Exam Paper Payment Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="roomAttendance" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" CssClass="ArabicNumbertxt" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="From Date required" ValidationGroup="roomAttendance">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                                ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[م][ا][ر][س]|[ي][ن][ا][ي][ر]|[ف][ب][ر][ا][ي][ر]|[أ][ب][ر][ي][ل]|[م][ا][ي][و]|[ي][و][ن][ي][و]|[ي][و][ل][ي][و]|[أ][غ][س][ط][س]|[س][ب][ت][م][ب][ر]|[أ][ك][ت][و][ب][ر]|[ن][و][ف][م][ب][ر]|[د][ي][س][م][ب][ر]||[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="roomAttendance">*</asp:RegularExpressionValidator></td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" CssClass="ArabicNumbertxt" runat="server" Width="122px"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                            ErrorMessage="To Date required" ValidationGroup="roomAttendance">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[م][ا][ر][س]|[ي][ن][ا][ي][ر]|[ف][ب][ر][ا][ي][ر]|[أ][ب][ر][ي][ل]|[م][ا][ي][و]|[ي][و][ن][ي][و]|[ي][و][ل][ي][و]|[أ][غ][س][ط][س]|[س][ب][ت][م][ب][ر]|[أ][ك][ت][و][ب][ر]|[ن][و][ف][م][ب][ر]|[د][ي][س][م][ب][ر]||[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="roomAttendance">*</asp:RegularExpressionValidator></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Report</span></td>
                                    <td align="left" colspan="3">
                                        <asp:RadioButtonList RepeatDirection="Horizontal" AutoPostBack="true" ID="radReportType" runat="server" GroupName="rptType" CssClass="field-label">
                                            <asp:ListItem Value="1" Text="Paid" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Un Paid"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="roomAttendance" /></td>
                                </tr>

                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

