Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptReportRuleList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C970045" And ViewState("MainMnu_code") <> "C970060" And ViewState("MainMnu_code") <> "C970075") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()
                BindPrintedFor()
                BindHeader()
                ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                Dim li As New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlGrade.Items.Insert(0, li)

                ddlSubject.Items.Clear()
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlSubject.Items.Insert(0, li)

                If ViewState("MainMnu_code") = "C970045" Then
                    tblrule.Rows(6).Visible = False
                Else
                    tblrule.Rows(6).Visible = True
                    tblrule.Rows(7).Visible = False
                    BindSection()
                End If
                
                If ViewState("MainMnu_code") = "C970075" Then
                    tblrule.Rows(3).Visible = False
                End If
            End If


            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindHeader()
        ddlHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND RSD_bDIRECTENTRY='FALSE' AND ISNULL(RSD_bFINALREPORT,'FALSE')='FALSE'  ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlHeader.Items.Insert(0, li)

    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id

        Dim str_query1 As String = "SELECT DISTINCT A.SBG_ID,A.SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                & " AS A INNER JOIN SUBJECTS_GRADE_S AS B ON A.SBG_ID=B.SBG_PARENT_ID " _
                                & " INNER JOIN GROUPS_M AS C ON B.SBG_ID=C.SGR_SBG_ID " _
                                & " WHERE B.SBG_ACD_ID=" + acd_id




        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

            str_query1 += " AND B.SBG_GRD_ID='" + grade(0) + "'"
            str_query1 += " AND B.SBG_STM_ID=" + grade(1)


        End If
        str_query = str_query + " UNION ALL " + str_query1

        'str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView
        dv.Sort = "SBG_DESCR"
        ddlSubject.DataSource = dv
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer
        If ddlGrade.SelectedValue = "" Then
                Dim ddlS As New DropDownList
            If Session("CurrSuperUser") = "Y" Then
                ddlS = PopulateSubjects(ddlS, ddlAcademicYear.SelectedValue.ToString)

            Else
                ddlS = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlS, ddlAcademicYear.SelectedValue.ToString)
            End If
            For i = 0 To ddlS.Items.Count - 1
                If subs <> "" Then
                    subs += "|"
                End If
                subs += ddlS.Items(i).Value
            Next
            Return subs
        ElseIf ddlSubject.SelectedValue = "" Then
            For i = 1 To ddlSubject.Items.Count - 1
                If subs <> "" Then
                    subs += "|"
                End If
                subs += ddlSubject.Items(i).Value
            Next
            Return subs
        Else
            Return ddlSubject.SelectedValue.ToString
        End If
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue.ToString)
        param.Add("@RPF_ID", ddlPrintedFor.SelectedValue.ToString)
        param.Add("@RSD_ID", IIf(ddlHeader.SelectedValue = "", "0", ddlHeader.SelectedValue))
        param.Add("@SBG_ID", getSubjects)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)

        If ddlGrade.SelectedValue = "" Then
            param.Add("@GRD_ID", "0")
            param.Add("@STM_ID", "0")
        Else
            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            param.Add("@GRD_ID", grade(0))
            param.Add("@STM_ID", grade(1))
        End If
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)

        If ViewState("MainMnu_code") = "C970060" Or ViewState("MainMnu_code") = "C970075" Then
            If ddlSection.SelectedValue = "" Then
                param.Add("@SCT_ID", 0)
            Else
                param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
            End If
            param.Add("rptCard", ddlReportCard.SelectedItem.Text)
            param.Add("rptSchedule", ddlPrintedFor.SelectedItem.Text)
        End If

        If ViewState("MainMnu_code") = "C970045" Then
            If rdAll.Checked = True Then
                param.Add("@MODE", "ALL")
            Else
                param.Add("@MODE", "NO")
            End If
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ViewState("MainMnu_code") = "C970060" Then
                .reportPath = Server.MapPath("../Rpt/rptReportProcessStatus.rpt")
            ElseIf ViewState("MainMnu_code") = "C970075" Then
                .reportPath = Server.MapPath("../Rpt/rptReportPublishStatus.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptReportRuleList.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()

        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)

        
        ddlSubject.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlSubject.Items.Insert(0, li)

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)

        ddlSubject.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlSubject.Items.Insert(0, li)

        BindReportCard()
        BindPrintedFor()
        BindHeader()

        ddlSection.Items.Clear()
        li = New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Insert(0, li)

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlSubject.Items.Insert(0, li)

        If ViewState("MainMnu_code") = "C970060" Or ViewState("MainMnu_code") = "C970075" Then
            BindSection()
        End If

    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
