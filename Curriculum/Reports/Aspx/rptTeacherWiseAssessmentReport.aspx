<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTeacherWiseAssessmentReport.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTeacherWiseGroups" title="Untitled Page" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  &nbsp;<table id="tbl_ShowScreen" runat="server" align="center" border="0" bordercolor="#1b80b6"
        cellpadding="5" cellspacing="0" style="width: 650px">
        <tr>
            <td align="left" style="width: 761px;">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label>
                      </td>
        </tr>
        
        <tr>
            <td align="left"  >
                <table id="Table1" runat="server" align="left" border="1" class="BlueTableView" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0"  width ="95%">
                    <tr class="subheader_img">
                        <td align="left" style="height: 16px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                <asp:Label ID="lblHeader" runat="server">Teacher Wise Groups</asp:Label></span></font></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" >
                            <table id="Table2" runat="server" align="left" border="0" bordercolor="#1b80b6"
                                 cellspacing="5" width="95%" >
                                <tr>
                                 <td>
                                        <asp:Label id="lbaC" runat="server" Text="Academic Year" ></asp:Label></td>
                                     <td>:</td>
                                    <td class="matters">
                                        <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td class="matters">
                                        <asp:Label id="lbst" runat="server" Text="Stream" style="text-align: left"></asp:Label></td>    
                                        <td>:</td>
                                    <td class="matters" align="Left">
                                        <asp:DropDownList id="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>    
                                </tr>
                                <tr>
                                    <td>
                                        From Date</td>
                                    <td>
                                        :</td>
                                    <td class="matters">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="112px"></asp:TextBox> <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                                                ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                                    <td class="matters">
                                        To Date</td>
                                    <td class="matters">
                                        :</td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox" Width="122px">
                </asp:TextBox> <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false" /><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                                ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                    ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                                    EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" >
                            <table id="Table4" runat="server" align="left" border="0" bordercolor="#1b80b6"
                                cellpadding="12" cellspacing="0" style="width: 420px; height: 137px">
                                
                                 <tr>
                                    <td align="left" class="matters" colspan="3">
                                         <table border=".5" bordercolor="#1b80b6">
                                          <tr class="subheader_img">
                                            <td align="left"  style="height: 16px" valign="middle">
                                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                             <span style="font-family: Verdana">Grade</span></font></td></tr>
                                           <tr>
                                                <td align="left" style="height: 216px">
                                                        <asp:CheckBoxList id="lstGrade" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        Height="223px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                                        vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                                        text-align: left" Width="202px" AutoPostBack="True">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                </td>
                                    <td align="left" class="matters" colspan="2" >
                                        &nbsp;<table border=".5" bordercolor="#1b80b6">
                                           <tr class="subheader_img">
                                            <td align="left"  style="height: 16px" valign="middle">
                                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                             <span style="font-family: Verdana">Department</span></font></td></tr>
                                            <tr>
                                                <td align="left" style="height: 194px">
                                                        <asp:CheckBoxList id="lstDPT" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        Height="223px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                                        vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                                        text-align: left" Width="202px" AutoPostBack="True">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                        </td>
                                        
                                         <td align="left" class="matters" colspan="4">
                                           <table border=".5" bordercolor="#1b80b6">
                                          <tr class="subheader_img">
                                            <td align="left"  style="height: 16px" valign="middle">
                                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                             <span style="font-family: Verdana">
                                               <asp:CheckBox id="chkTeacher" runat="server" AutoPostBack="True" Text="Teacher">
                                                    </asp:CheckBox>
                                           </span></font></td></tr>
                                           <tr>
                                                <td align="left" style="height:211px;width:165px">
                                                  
                                                    <asp:CheckBoxList id="lstTeacher" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        Height="223px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                                        vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                                        text-align: left" Width="202px" AutoPostBack="True">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Height="24px"
                                Text="Generate Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
                <input id="lstValues" runat="server" name="lstValues" style="left: 274px; width: 74px;
                    position: absolute; top: 161px; height: 10px" type="hidden" />
                &nbsp;
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
                <asp:HiddenField id="hfTRV_ID" runat="server">
                </asp:HiddenField>
</asp:Content>

