<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlStudentGradeListByOption.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlStudentGradeListByOption" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Wise Performance
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label" >Academic Year</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label" >Select Report Card</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                  
                    <tr>
                        <td align="left" width="20%"><span class="field-label" >Select Report Schedule</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlReportSchedule" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle" ><span class="field-label" >Grade</span></td>

                        <td align="left" valign="middle" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr id="Tr2" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label" >Option</span></td>

                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlOption" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle"><span class="field-label" >Select Subject</span></td>

                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" Width="250px">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr id="Tr1" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label" >Group</span></td>

                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4" >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>

</asp:Content>

