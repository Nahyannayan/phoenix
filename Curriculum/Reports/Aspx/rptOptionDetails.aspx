<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOptionDetails.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptOptionDetails" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Subject Option Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%" style="border-collapse: collapse" cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span> </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" valign="middle">
                            <span class="field-label">Grade</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList>
                    </tr>

                    <tr id="20">
                        <td>
                            <br /><br />
                        </td>
                        
                        <!--            </td>
            <td align="left" valign="middle" >
                            Section
                        </td>
            <td align="left" valign="middle">
                            :</td>
            <td align="left" colspan="1" valign="middle">
                <asp:DropDownList ID="ddlSection" runat="server">
                </asp:DropDownList>
            </td> -->
                    </tr>
                    <tr>
                        <td align="center" valign="middle" colspan="4">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatColumns="2" CssClass="field-label" Font-Bold="true">
                                <asp:ListItem Value="Requested" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Approved"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="GR" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                                Text="Download Report in Pdf" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />

                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>
        </div>
    </div>
</asp:Content>

