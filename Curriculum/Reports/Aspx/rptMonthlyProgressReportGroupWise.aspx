<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptMonthlyProgressReportGroupWise.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
   <script language="javascript" type="text/javascript">
      function GetSUBJECT()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs ,"", sFeatures)            
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_SBM_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
        }
      function GetSUBJECTGROUP()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var SBM_IDs =document.getElementById('<%=h_SBM_IDs.ClientID %>').value;
            if (SBM_IDs== '')
            {
                alert('Please select atleast one Subject')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=SUBGROUP&SBM_IDs=" + SBM_IDs ,"", sFeatures)            
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_SGR_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
        }
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs =document.getElementById('<%=h_SGR_IDs.ClientID %>').value;
            if (SGR_IDs== '')
            {
                alert('Please select atleast one Subject Group')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&SGR_IDs=" + SGR_IDs ,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
   </script>
    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table class ="matters" align="center" border="1" bordercolor="#1b80b6" cellpadding="4" cellspacing="0" width="65%">
                    <tr>
                        <td valign="middle" class="subheader_img" colspan="9">
                            <asp:Label id="lblHeader" runat="server" Text="Monthly Progress Report"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Academic Year</td>
                        <td align="center">
                            :</td>
                        <td align="left">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left">
                            Term</td>
                        <td align="center">
                            :</td>
                        <td align="left" colspan="4">
                            <asp:DropDownList id="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Activity</td>
                        <td align="center" >
                            :</td>
                        <td align="left" colspan="7">
                            <asp:DropDownList id="ddlACTIVITY" runat="server">
                            </asp:DropDownList></td>
                    </tr>
        <tr>
            <td align="left">
                            Date</td>
            <td align="center">
                :</td>
            <td align="left" colspan="7">
                <asp:TextBox id="txtDate" runat="server" Width="112px">
                </asp:TextBox>
                <asp:ImageButton id="imgDate" runat="server" ImageUrl="~/Images/calendar.gif">
                </asp:ImageButton></td>
        </tr>
                    <tr id ="20" >
                        <td align="left" valign="top">
                            Grade</td>
                        <td align="center" valign="top">
                            :</td>
                        <td align="left" colspan="7" valign="top">
                            <asp:CheckBoxList id="chkGRD_ID" runat="server" CellPadding="8" CellSpacing="2" RepeatColumns="8" RepeatDirection="Horizontal" OnSelectedIndexChanged="chkGRD_ID_SelectedIndexChanged" AutoPostBack="True" >
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr id ="21" >
                        <td align="left" valign="top">
                            Subject</td>
                        <td align="center" valign="top">
                            :</td>
                        <td align="left" colspan="7">
                            <asp:TextBox id="txtSubject" runat="server" Width="330px"></asp:TextBox>
                            <asp:ImageButton id="imgSubject" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSUBJECT();" OnClick="imgSubject_Click">
                            </asp:ImageButton><br />
                            <asp:GridView id="grdSubject" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="353px" OnPageIndexChanging="grdSubject_PageIndexChanging">
                                <columns>
<asp:TemplateField HeaderText="GRADE"><ItemTemplate>
<asp:Label id="lblSUBJ_ID" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="SBG_DESCR" HeaderText="Subject Name"></asp:BoundField>
</columns>
                                <headerstyle cssclass="gridheader_new" />
                            </asp:GridView></td>
                    </tr>
        <tr>
            <td align="left" valign="top">
                Subject Group</td>
            <td align="center" valign="top">
                :</td>
            <td align="left" colspan="7">
                <asp:TextBox id="TextBox2" runat="server" Width="330px"></asp:TextBox>&nbsp;<asp:ImageButton id="imgSubGrp" runat="server" OnClientClick="GetSUBJECTGROUP();" ImageUrl="~/Images/cal.gif" OnClick="imgSubGrp_Click"></asp:ImageButton>
                <asp:GridView id="grdSubjGrp" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="353px" OnPageIndexChanging="grdSubjGrp_PageIndexChanging">
                    <columns>
<asp:TemplateField HeaderText="Sub. Grp. ID"><ItemTemplate>
<asp:Label id="lblGroupID" runat="server" Text='<%# Bind("ID") %>' __designer:wfdid="w17"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Group Name"></asp:BoundField>
</columns>
                    <headerstyle cssclass="gridheader_new" />
                </asp:GridView></td>
        </tr>
                    <tr>
                        <td align="left" valign="top" >
                            Student</td>
                        <td align="center" valign="top" >
                            :</td>
                        <td align="left" colspan="7">
                            <asp:TextBox id="TextBox1" runat="server" Width="330px"></asp:TextBox>
                            <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS();" OnClick="imgStudent_Click">
                            </asp:ImageButton>
                            <asp:GridView id="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="353px" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <columns>
<asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
<asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>' __designer:wfdid="w18"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
</columns>
                                <headerstyle cssclass="gridheader_new" />
                            </asp:GridView></td>
                    </tr>
        <tr>
            <td align="left" colspan="9" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" SkinID="ButtonNormal" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button id="btnCancel" runat="server" SkinID="ButtonNormal" Text="Cancel" /></td>
        </tr>
                </table>
    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField  id="h_SBM_IDs" runat="server"/>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_SGR_IDs" runat="server"/>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
</asp:Content>

