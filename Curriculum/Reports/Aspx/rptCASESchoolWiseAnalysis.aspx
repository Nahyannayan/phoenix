<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCASESchoolWiseAnalysis.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCASESchoolWiseAnalysis" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;

            if (mode == "cashFlow") {
                result = window.showModalDialog("../../../../Accounts/selBussinessUnit.aspx?multiSelect=false", "", sFeatures)
            }
            else {
                result = window.showModalDialog("./../../../Accounts/selBussinessUnit.aspx", "", sFeatures)
            }
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }


        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%" >
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td >
                            <table id="tblTC" runat="server" align="center" cellpadding="7" cellspacing="0" width="100%">


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business unit</span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" CssClass="field-label"/><br />
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstBSU" runat="server">
                                        </asp:CheckBoxList></div>
                                    </td>

                                     <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Grade </span></td>

                                    <td align="left" width="30%" style="text-align: left;">

                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%">
                                        <span class="field-label">Subject </span></td>

                                    <td align="left" width="30%" style="text-align: left;">

                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                               

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Gender Wise </span>

                                    </td>
                                    <td width="30%" align="left">
                                        <asp:CheckBox ID="chkGender" runat="server" /></td>
                                </tr>


                                <tr id="trSave" runat="server">
                                    <td align="center" colspan="4" valign="top">
                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" TabIndex="4"
                                             />
                                        <asp:Button ID="btnDownload" runat="server" Text="Download Report in PDF" CssClass="button"
                                            TabIndex="4"  />
                                        <asp:HiddenField ID="hfbDownload" runat="server" />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>
                                    </td>
                                </tr>

                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:GridView ID="GridView1" runat="server">
                            </asp:GridView>
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

