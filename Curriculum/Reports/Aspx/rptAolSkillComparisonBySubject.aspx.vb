﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptAolSkillComparisonBySubject
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290130" And ViewState("MainMnu_code") <> "C290150" And ViewState("MainMnu_code") <> "C500150" _
            And ViewState("MainMnu_code") <> "C280295" And ViewState("MainMnu_code") <> "C500290") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindSubjects()
                    BindGrade()

                    If ViewState("MainMnu_code") = "C290150" Or ViewState("MainMnu_code") = "C500150" Then
                        rdReport.Checked = True
                        rdAssessment.Checked = False
                    End If

                    If ViewState("MainMnu_code") = "C280295" Then
                        trAssmt.Visible = False
                        rdReport.Checked = True
                        rdAssessment.Checked = False
                    End If

                    If ViewState("MainMnu_code") = "C290130" Then
                        lstPrintedfor.Visible = True
                        ddlPrintedFor.Visible = False
                    End If

                    If ViewState("MainMnu_code") = "C290130" Then
                        btnGenerateReport.Text = "Generate Graph"
                    Else
                        btnGenerateData.Visible = False
                    End If


                    BindAssessment()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
      
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE WHEN SBG_PARENTS='NA' THEN SBG_DESCR ELSE SBG_PARENTS+'-'+SBG_DESCR END SBG_DESCR,SBG_SBM_ID" _
                                & " FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_GRD_ID NOT IN('KG1','KG2') " _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID FROM VW_GRADE_BSU_M WHERE " _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindAssessment()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If rdAssessment.Checked = True Then
            If ViewState("MainMnu_code") = "C290130" Then
                str_query = "SELECT CAD_DESC,CAD_ID FROM ACT.ACTIVITY_D " _
                           & " WHERE CAD_bAOL=1 AND CAD_bWITHOUTSKILLS=0" _
                           & " AND CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Else
                str_query = "SELECT CAD_DESC,CAD_ID FROM ACT.ACTIVITY_D " _
                           & " WHERE  CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            End If
        ElseIf ViewState("MainMnu_code") = "C290150" Then
            str_query = "SELECT DISTINCT RPF_DESCR AS CAD_DESC,CASE RPF_DESCR WHEN 'FINAL REPORT' THEN -1 ELSE RPF_ID END AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                    & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                    & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                    & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                    & " AND (RPF_DESCR LIKE '%FORMATIVE%' OR RPF_DESCR LIKE '%SUMMATIVE%' OR RPF_DESCR LIKE '%FINAL%')" _
                    & " AND RSG_GRD_ID<>'10'"
        ElseIf ViewState("MainMnu_code") = "C500150" Then
            str_query = "SELECT DISTINCT RPF_DESCR AS CAD_DESC,CASE RPF_DESCR WHEN 'FINAL REPORT' THEN -1 ELSE RPF_ID END AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                                & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        ElseIf ViewState("MainMnu_code") = "C280295" Then
            str_query = "SELECT DISTINCT RPF_DESCR AS CAD_DESC,RPF_ID AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                                         & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                                         & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                                         & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        ElseIf ViewState("MainMnu_code") = "C500290" Then
            str_query = "SELECT RPF_DESCR AS CAD_DESC,RPF_ID AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                  & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                  & " AND  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Else

            str_query = "SELECT RPF_DESCR AS CAD_DESC,RPF_ID AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                     & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                     & " AND RPF_DESCR LIKE '%FORMATIVE%' AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ViewState("MainMnu_code") <> "C290130" Then
            ddlPrintedFor.DataSource = ds
            ddlPrintedFor.DataTextField = "CAD_DESC"
            ddlPrintedFor.DataValueField = "CAD_ID"
            ddlPrintedFor.DataBind()
        Else
            lstPrintedfor.DataSource = ds
            lstPrintedfor.DataTextField = "CAD_DESC"
            lstPrintedfor.DataValueField = "CAD_ID"
            lstPrintedfor.DataBind()
        End If
    End Sub

    Function GetGrades() As String
        Dim str As String = ""
        Dim i As Integer
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrade.Items(i).Value
            End If
        Next
        Return str
    End Function

    Function GetReportIDs() As String
        Dim str As String = ""
        Dim i As Integer

        For i = 0 To lstPrintedfor.Items.Count - 1
            If lstPrintedfor.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstPrintedfor.Items(i).Value
            End If

        Next

        Return str
    End Function

    Function GetReports() As String
        Dim str As String = ""
        Dim i As Integer

        For i = 0 To lstPrintedfor.Items.Count - 1
            If lstPrintedfor.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += lstPrintedfor.Items(i).Text
            End If

        Next

        Return str
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        If ViewState("MainMnu_code") <> "C290130" Then
            If rdAssessment.Checked = True Then
                param.Add("@CAD_ID", ddlPrintedFor.SelectedValue.ToString)
                param.Add("@RPF_ID", "0")
            Else
                param.Add("@CAD_ID", "0")
                param.Add("@RPF_ID", ddlPrintedFor.SelectedValue.ToString)
            End If
        Else
            If rdAssessment.Checked = True Then
                param.Add("@CAD_IDs", GetReportIDs)
                param.Add("@RPF_IDs", "0")
            Else
                param.Add("@CAD_IDs", "0")
                param.Add("@RPF_IDs", GetReportIDs)
            End If
        End If

        If ViewState("MainMnu_code") <> "C290130" Then
            param.Add("Assessment", ddlPrintedFor.SelectedItem.Text)
        Else
            param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
            param.Add("Assessment", GetReports)
        End If
        param.Add("@GRD_IDS", GetGrades())
        param.Add("Grades", GetGrades.Replace("|", ","))
        param.Add("Subject", ddlSubject.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ViewState("MainMnu_code") = "C290130" Then
                .reportPath = Server.MapPath("../Rpt/rptAolSkillComparisonBySubject_AcrossReport.rpt")
            Else
                param.Add("@RPF_DESCR", ddlPrintedFor.SelectedItem.Text)
                .reportPath = Server.MapPath("../Rpt/rpt_RA_AssessmentComparisonBySubject.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Sub CallDataReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
     
        If rdAssessment.Checked = True Then
            param.Add("@CAD_IDs", GetReportIDs)
            param.Add("@RPF_IDs", "0")
        Else
            param.Add("@CAD_IDs", "0")
            param.Add("@RPF_IDs", GetReportIDs)
        End If


        param.Add("Assessment", GetReports)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@GRD_IDS", GetGrades())
        param.Add("Grades", GetGrades.Replace("|", ","))
        param.Add("Subject", ddlSubject.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptAolSkillComparisonBySubject_AcrossReport_Data.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSubjects()
        BindGrade()
        BindAssessment()
    End Sub

    Protected Sub rdAssessment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAssessment.CheckedChanged
        If rdAssessment.Checked = True Then
            BindAssessment()
        End If
    End Sub

    Protected Sub rdReport_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReport.CheckedChanged
        If rdReport.Checked = True Then
            BindAssessment()
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub btnGenerateData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateData.Click
        CallDataReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
