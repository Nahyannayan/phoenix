Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class rptTermlyAttendaceReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C033005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    BindReportType()
                    BindReportPrintedFor()
                    GetAllGrade()
                End If
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        End If
    End Sub
      Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub

    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
 
    Sub GetAllGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            GenerateTermlyAttendanceReport()
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub


    Private Sub GenerateTermlyAttendanceReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", ddlSection.SelectedValue)
        param.Add("BSU_NAME", Session("bsu_name"))
        param.Add("@FILTER_TYPE", GetFilterType())
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTERMLY_ATTENDANCE_REPORT.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetFilterType() As String
        If radAll.Checked Then
            Return ""
        End If
        Dim sql_str As String = "SELECT RAT_ABSENT, RAT_APRLEAVE " & _
        " FROM RPT.REPORT_ABSENTEE_DISPLAY WHERE RAT_BSU_ID ='" & Session("sBSUID") & "'" & _
        " AND RAT_ACD_ID = " & ddlAca_Year.SelectedValue
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, _
                                                            CommandType.Text, sql_str)
        While (drReader.Read())
            If radAbsent.Checked Then
                Return drReader("RAT_ABSENT").ToString
            ElseIf radApprovedLeave.Checked Then
                Return drReader("RAT_APRLEAVE").ToString
            End If

        End While
        Return ""
    End Function

    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function


    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'code added by dhanya
        BindReportType()
        If (ViewState("MainMnu_code") = "C400005") Then
            BindReportPrintedFor(True)
        Else
            BindReportPrintedFor()
        End If
        GetAllGrade()
        GetSectionForGrade()
    End Sub
End Class
