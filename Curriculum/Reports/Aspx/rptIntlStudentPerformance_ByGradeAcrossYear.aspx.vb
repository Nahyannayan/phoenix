Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM
Imports Telerik.Web.UI
Imports System.Collections.Generic

Partial Class Curriculum_Reports_Aspx_rptIntlStudentPerformance_ByGradeAcrossYear
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400191" And ViewState("MainMnu_code") <> "C400192" And ViewState("MainMnu_code") <> "C400350") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()
                    BindSubjects()
                    If ViewState("MainMnu_code") <> "C400350" Then
                        trFilter.Visible = False
                        btnDownloadExcel.Visible = False
                    End If

                    'If Session("sbsuid") = "125011" Then
                    '    trFilter.Visible = False
                    'End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadExcel)
    End Sub


#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function



    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim strRsd As String = ""
        Dim strRPF As String = ""

        Dim bRSM As Boolean = False


        If ViewState("MainMnu_code") = "C400350" Then
            For Each node In tvReport.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            If cccNode.Checked = True Then
                                If strRPF <> "" Then
                                    strRPF += "|"
                                End If
                                strRPF += cNode.Text + "_" + ccNode.Text + "$" + cccNode.Text

                            End If
                        Next

                    Next
                Next
            Next
        Else
            For Each node In tvReport.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If strRPF <> "" Then
                                strRPF += "|"
                            End If
                            strRPF += cNode.Text + "_" + ccNode.Text
                        End If
                    Next
                Next
            Next

        End If


        Return strRPF.Trim
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        param.Add("@GRD_ID", grade(0))
        param.Add("@RPF_DESCR", getReportCards())
        param.Add("@STM_ID", grade(1))
        param.Add("@SBG_DESCRS", getSubjects)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ViewState("MainMnu_code") = "C400191" Then
                param.Add("@bEVALUATION", "True")
                .reportPath = Server.MapPath("../Rpt/rptIntlStudentPerformanceByGradeAcossYears.rpt")
            ElseIf ViewState("MainMnu_code") = "C400350" Then
                param.Add("@bEVALUATION", "True")
                param.Add("@TYPE", ddlType.SelectedValue.ToString)
                param.Add("@REPORTTYPE", "")
                param.Add("@IMG_BSU_ID", Session("sbsuid"))
                param.Add("@IMG_TYPE", "LOGO")
                .reportPath = Server.MapPath("../Rpt/SEF Reports/rptSEFStudentPerformanceByGradeAcossYears.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptStudentPerformanceByGradeAcossYears_CBSE.rpt")
            End If

        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub


    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")


        Dim STR_QUERY As String

        'FOR SEF REPORTS  CALL REPORT DATA WITH COLUMNS ALSO

        If ViewState("MainMnu_code") = "C400350" Then
            STR_QUERY = "EXEC RPT.[GETREPORTCARDBYYEAR_WITHHEADERS] " _
                               & "'" + Session("SBSUID") + "'," _
                               & "'" + grade(0) + "'," _
                               & Session("CLM").ToString + "," _
                               & ddlAcademicYear.SelectedValue.ToString
        Else
            STR_QUERY = "EXEC RPT.GETREPORTCARDBYYEAR " _
                                   & "'" + Session("SBSUID") + "'," _
                                   & "'" + grade(0) + "'," _
                                   & Session("CLM").ToString + "," _
                                   & ddlAcademicYear.SelectedValue.ToString
        End If
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACY_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        If ViewState("MainMnu_code") = "C400350" Then
            str = str.Replace("RSD_ID", "ID")
            str = str.Replace("RSD_HEADER", "TEXT")

            tvReport.MaxDataBindDepth = 4
        End If

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub


    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindSubjects()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_SBM_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SBG_GRD_ID='" + grade(0) + "'" _
                              & " AND SBG_STM_ID=" + grade(1) _
                              & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    'Function getSubjects() As String
    '    Dim i As Integer
    '    Dim str As String = ""
    '    For i = 0 To ddlSubject.Items.Count - 1
    '        If ddlSubject.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += "|"
    '            End If
    '            str += ddlSubject.Items(i).Text
    '        End If
    '    Next
    '    Return str
    'End Function



    Function getSubjects() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If ddlSubject.SelectedIndex > 0 Then
                str = ddlSubject.SelectedItem.Value
            End If
        End If
        Return str
    End Function

    Sub CheckAll(ByVal chkUncheck As Boolean)
        Dim i As Integer
        Select Case Session("sbsuid")
            Case "125005"
                For i = 0 To ddlSubject.Items.Count - 1
                    If chkUncheck = True Then
                        If ddlSubject.Items(i).Text.ToUpper <> "ARABIC" Then
                            ddlSubject.Items(i).Selected = True
                        End If
                    Else
                        ddlSubject.Items(i).Selected = False
                    End If
                Next
        End Select

    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            lblError.Text = ""
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindSubjects()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            lblError.Text = ""
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindSubjects()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub btnDownloadExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadExcel.Click
        Dim url As String
        Dim MainMnu_code As String = ViewState("MainMnu_code")
        Dim Grade As String = ddlGrade.SelectedItem.Value
        Dim Section As String = ddlSection.SelectedItem.Value
        Dim strRedirect As String
        Dim grd As String() = ddlGrade.SelectedValue.Split("|")

        strRedirect = "~/Curriculum/Reports/Aspx/rptIntlStudentPerformanceByGrade_Excel.aspx"
        MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
        Grade = Encr_decrData.Encrypt(Grade)
        Section = Encr_decrData.Encrypt(Section)

        url = String.Format("{0}?MainMnu_code={1}&grd={2}&sctid={3}&acdid={4}&sbg={5}&rpf={6}&type={7}", strRedirect, MainMnu_code, Grade, Section, Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString), Encr_decrData.Encrypt(getSubjects), Encr_decrData.Encrypt(getReportCards), Encr_decrData.Encrypt(ddlType.SelectedValue.ToString))


        'ResponseHelper.Redirect(url, "_blank", "")
        ResponseHelper.Redirect(url, "_blank", "width=1,height=1")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
