<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptRA_GradePerformance_AllSubjects.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptRA_GradePerformance_AllSubjects"
    Title="GEMS OASIS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
               if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_lstGrade') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

        function fnSelectAll1(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_ddlSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

    </script>

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%">
        
        <tr>
            <td align="left">
                <span class="field-label">Academic Year</span>
            </td>
           
            <td align="left">
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Report Card</span>
            </td>
           
            <td align="left">
                <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="left">
                <span class="field-label">Report Header</span>
            </td>
            
            <td align="left">
                <asp:DropDownList ID="ddlHeader" runat="server" Width="197px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trSubject" runat="server">
            <td align="left">
                <span class="field-label">Grade</span>
            </td>
           
            <td align="left">
                
                <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll2(this);" runat="server"
                    Text="Select All" />
                <div class="checkbox-list">
                <asp:CheckBoxList ID="lstGrade" runat="server" RepeatLayout="Flow">
                </asp:CheckBoxList>
                    </div>
            </td>
            <td align="left">
                <span class="field-label">Subject</span>
            </td>
           
            <td align="left">
                <asp:CheckBox ID="chkSubjectSelect" onclick="javascript:fnSelectAll1(this);" runat="server"
                    Text="Select All" />
                <div class="checkbox-list">
                <asp:CheckBoxList ID="ddlSubject" runat="server" RepeatLayout="Flow" >
                </asp:CheckBoxList>
                    </div>
            </td>
        </tr>
       
         <tr id="trFilter" runat="server">
                        <td align="left" style="width: 184px">
                            <span class="field-label">Filter By</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlType" runat="server" Width="199px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                                <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                            </asp:DropDownList></td>
             <td></td>
             <td></td>
                    </tr>
     <tr>
            <td align="left" colspan="4" >
                <asp:CheckBox ID="chkRange" runat="server" AutoPostBack="true" Text="Set Range" CssClass="field-label" /></td>
        </tr>
        <tr id="trRange" runat="server">
            <td align="center" colspan="4">
                <table width="100%" class="table table-bordered">
                    <tr style="background-color: #ccfd91">
                        <td align="left">
                           <span class="field-label"> Well Above Curriculum Expectations</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWellAbove1" Text="95" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <asp:TextBox ID="txtWellAbove2" Text="100" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="background-color: #ccfd91">
                        <td align="left">
                            <span class="field-label">Above Curriculum Expectations</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAbove1" Text="90" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <asp:TextBox ID="txtAbove2" Text="94" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Meets Curriculum Expectations</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMeet1" Text="65" runat="server"></asp:TextBox></td>
                        <td align="left">
                        <asp:TextBox ID="txtMeet2" Text="89" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="background-color: #ccfd91">
                        <td align="left">
                            <span class="field-label">Below Curriculum Expectations</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBelow1" Text="25" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <asp:TextBox ID="txtBelow2" Text="65" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="background-color: #ccfd91">
                        <td align="left">
                           <span class="field-label"> Well Below Curriculum Expectations</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWBelow1" Text="0" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <asp:TextBox ID="txtWBelow2" Text="25" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                    Text="Generate Report" ValidationGroup="groupM1" />
                &nbsp;<asp:Button ID="btnDownload" runat="server" CssClass="button" TabIndex="7"
                    Text="Download Report In PDF" ValidationGroup="groupM1" />
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </td>
        </tr>
    </table>

                </div>
            </div>
          </div>

</asp:Content>
