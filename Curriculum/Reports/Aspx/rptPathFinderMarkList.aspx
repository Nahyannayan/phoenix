<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPathFinderMarkList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptPathFinderMarkList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Pathfinder Ranklist
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="center" colspan="2">

                              <asp:RadioButton ID="rdMaths" runat="server" Checked="True" GroupName="g1" Text="M/P/C" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdBiology" runat="server" GroupName="g1" Text="M/P/C/B" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdPCB" runat="server" GroupName="g1" Text="P/C/B" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdALL" runat="server" GroupName="g1" Text="All" CssClass="field-label"></asp:RadioButton>
                        </td>
                       
                    </tr>

                   <%-- <tr runat="server">
                        <td align="left" valign="middle" colspan="4">
                          
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

