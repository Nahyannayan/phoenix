<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptStudentLessonCriterias_New.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStudentLessonCriterias"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">
        function GetSubjects() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var trmId;
            var sbgId;
            var trmId;
            var combo = $find('<%=ddlTerm.ClientID%>');
            trmId=combo.get_selectedItem().get_value();
        <%--    trmId = document.getElementById('<%=ddlTerm.ClientID %>').get_selectedItem().get_value();
            alert(trmId);--%>
            sbgId = document.getElementById('<%=ddlSubject.ClientID %>').value.split("|");
            var oWnd = radopen("../../showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "pop_sub");

            //result = window.showModalDialog("../../showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "", sFeatures)

            <%--if (result != "" && result != "undefined") {

                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];

            }
            return false;--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];

                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
     <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
            background-position: 0 !important;
        }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        html body .RadInput_Default .riEmpty, html body .RadInput_Empty_Default, html body .RadInput_Default .riTextBox, html body .RadInputMgr_Default {
            padding: 10px;
        }

        .RadColorPicker {
            width: 80% !important;
        }

            .RadColorPicker .rcpMillionColorsPageView .rcpInputsWrapper li {
                margin-right: 20px !important;
            }
        /*.RadColorPicker ul, .RadColorPicker ul li{
          display:inline-flex !important;
      }*/
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_sub" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table id="tblClm" runat="server" align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Term</span>
                                    </td>

                                    <td align="left" width="30%">
                                   <%--     <asp:DropDownList ID="ddlTerm" runat="server">
                                        </asp:DropDownList>--%>
                                           <telerik:RadComboBox RenderMode="Lightweight" ID="ddlTerm" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="false" AutoPostBack="true"
                                                        Width="100%" EmptyMessage="Select Term(s)...">
                                                    </telerik:RadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    
                                    <td align="left" width="20%"><span class="field-label"><asp:Label ID="lblSecGrp" runat="server"></asp:Label></span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true" Visible="false">
                                        </asp:DropDownList>
                                       <%-- <asp:DropDownList ID="ddlGroup" runat="server" Visible="false"></asp:DropDownList>--%>
                                          <telerik:RadComboBox RenderMode="Lightweight" ID="ddlGroup" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                                        Width="100%" EmptyMessage="Select Group(s)..." Visible="false">
                                                    </telerik:RadComboBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Topic</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTopic" runat="server" AutoPostBack="True" Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSubjects(); return false;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in PDF"
                                            ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_TopicID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>
