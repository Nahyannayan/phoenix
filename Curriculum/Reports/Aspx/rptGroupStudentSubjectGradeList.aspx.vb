Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptGroupStudentSubjectGradeList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100230") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)



                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")


                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If



                    If Session("CurrSuperUser") = "Y" Then
                        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
                    Else
                        ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
                    End If

                    'Dim li As New ListItem
                    'li.Text = "ALL"
                    'li.Value = 0
                    'ddlGroup.Items.Insert(0, li)
                    Try
                        BindReports()
                    Catch ex As Exception
                    End Try
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownLoadExcel)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadPDF)
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRSM As String = ""
        Dim strRPF As String = ""

        Dim bRSM As Boolean = False


        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                bRSM = True
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then

                        If bRSM = True Then
                            If strRSM <> "" Then
                                strRSM += "|"
                            End If
                            strRSM += cNode.Value
                        End If

                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        strRPF += ccNode.Value

                        bRSM = False
                    End If
                Next
            Next
        Next

        Dim strRPT As String = strRSM.Trim + "__" + strRPF.Trim
        Return strRPT
    End Function

    Sub CallReport(ByVal bDownload As Boolean)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)

        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)

        Dim strReports As String() = getReportCards.Trim.Split("__")
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@RSM_ID", strReports(0))
        param.Add("@RPF_ID", strReports(2))
        param.Add("@GRD_ID", grade(0))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("teacher", getGroupTeacher)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .reportPath = Server.MapPath("../Rpt/rptGroupStudentSubjectGradeList.rpt")


        End With

        If bDownload = 0 Then
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else

            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        End If
    End Sub


    Sub DownloadReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)

        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)

        Dim strReports As String() = getReportCards.Trim.Split("__")
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@RSM_ID", strReports(0))
        param.Add("@RPF_ID", strReports(2))
        param.Add("@GRD_ID", grade(0))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("teacher", getGroupTeacher)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .reportPath = Server.MapPath("../Rpt/rptGroupStudentSubjectGradeList.rpt")


        End With

              Dim rptDownload As New ReportDownloadExcel
        rptDownload.LoadReports(rptClass, rs)
        rptDownload = Nothing
      
    End Sub


    Sub CallReportComments()
        Dim param As New Hashtable
           param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)

        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)

        Dim strReports As String() = getReportCards.Trim.Split("__")
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@RSM_ID", strReports(0))
        param.Add("@RPF_ID", strReports(2))
        param.Add("@GRD_ID", grade(0))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("teacher", getGroupTeacher)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If Session("sbsuid") = "123004" Then
                .reportPath = Server.MapPath("../Rpt/rptGroupStudentSubjectCommentList_DMHS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptGroupStudentSubjectCommentList.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "' AND SGS_EMP_ID='" + emp_id + "'" _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")

        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID='" + acd_id + "'"

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID='" + sbg_id + "'"
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function


    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & Grdid & "'"
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
            'If v_GradeID <> "" Then
            '    str_sql += "AND SBG_GRD_ID IN ('" & v_GradeID & "') ORDER BY GRM_DISPLAY "
            'End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & Grdid & "'"
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If
        'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
        '    strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
        '    str_sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
        '            & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
        '            & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
        '            & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & acdid & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
        '            & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        'Else

        '    str_sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
        '            & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
        '            & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID " _
        '            & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & acdid & "' AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
        '            & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        'End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL  AND SGR_ACD_ID='" + acdid + "'"
            'If vSBM_IDs <> "" Then
            'str_query_header += "WHERE SGR_SBG_ID IN ('" & vSBM_IDs.Replace("___", "','") & "') "
            'str_query_header += ""
            'Else
            'str_query_header += " WHERE 1=1 "
            'End If
            'str_sql = str_query_header.Split("||")(0)
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"
            'AND SGR_GRD_ID='" & strGRD_IDs & "' AND SGR_SBG_ID=" & vSBM_IDs & " ORDER BY SGR_ID "
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade() As String = ddlGrade.selectedvalue.split("|")

        Dim STR_QUERY As String = " SELECT RSM_ID,RSM_DESCR,RPF_ID,RPF_DESCR FROM " _
                                & " RPT.REPORT_SETUP_M AS A " _
                                & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON A.RSM_ID=C.RPF_RSM_ID" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS F ON A.RSM_ID=F.RSG_RSM_ID" _
                                & " WHERE RSG_GRD_ID='" + grade(0) + "' AND RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' FOR XML AUTO"

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("RSM_ID", "ID")
        str = str.Replace("RSM_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub
    Function getGroupTeacher() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                & " WHERE SGS_SGR_ID = " + ddlGroup.SelectedValue _
                & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''),'')"
        Dim teacher As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return teacher
    End Function
  #End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
        
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")


            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If



            If Session("CurrSuperUser") = "Y" Then
                ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If

            'Dim li As New ListItem
            'li.Text = "ALL"
            'li.Value = 0
            'ddlGroup.Items.Insert(0, li)

            BindReports()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
         
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")


            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If



            If Session("CurrSuperUser") = "Y" Then
                ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            'Dim li As New ListItem
            'li.Text = "ALL"
            'li.Value = "0"
            'ddlGroup.Items.Insert(0, li)


            BindReports()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            Dim li As ListItem
            Dim grade() As String = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            'li = New ListItem
            'li.Text = "ALL"
            'li.Value = "0"
            'ddlGroup.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport(0)
    End Sub

   
    Protected Sub btnGenComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenComments.Click
        CallReportComments()
    End Sub

    Protected Sub btnDownLoadExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoadExcel.Click
        DownloadReport()
    End Sub

    Protected Sub btnDownloadPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadPDF.Click
        CallReport(1)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
