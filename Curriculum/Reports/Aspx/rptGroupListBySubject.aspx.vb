Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_RPT_SUBJECTLIST_BY_DEPARTMENT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    GetAllGrade()
                    txtSubject.Attributes.Add("ReadOnly", "ReadOnly")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    'Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    h_SBG_IDs.Value = ""
    '    grdSubject.DataSource = Nothing
    '    grdSubject.DataBind()
    'End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Sub GetAllGrade()
        chkGRD_ID.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBSUID"), ddlAca_Year.SelectedValue)
        chkGRD_ID.DataTextField = "GRM_DISPLAY"
        chkGRD_ID.DataValueField = "GRM_GRD_ID"
        chkGRD_ID.DataBind()
    End Sub

    Private Sub GridBindsubject(ByVal vSBG_IDs As String)
        If vSBG_IDs = "" Then
            grdSubject.DataSource = Nothing
            grdSubject.DataBind()
        Else
            grdSubject.DataSource = ReportFunctions.GetGradeSelectedSubjects(vSBG_IDs)
            grdSubject.DataBind()
        End If
    End Sub

    Protected Sub grdSubject_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdSubject.PageIndex = e.NewPageIndex
        GridBindsubject(h_SBG_IDs.Value)
    End Sub

   
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            Select Case ViewState("MainMnu_code")
                Case CURR_CONSTANTS.MNU_RPT_SUBJECTLIST_BY_DEPARTMENT
                    GenerateGroupListBySubject()
            End Select
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub GenerateGroupListBySubject()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@SBM_IDs", h_SBG_IDs.Value.Replace("___", "|"))
        param.Add("@GRD_IDs", GetSelectedGrades())
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptGroupListBySubject.rpt")
        End With
        Session("rptClass") = rptClass
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetSelectedGrades() As String
        Dim strSelGrade As String = String.Empty
        Dim seperator As String = ""
        For Each itm As ListItem In chkGRD_ID.Items
            If itm.Selected Then
                strSelGrade += seperator & itm.Value
                seperator = "|"
            End If
        Next
        Return strSelGrade
    End Function

    Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBindsubject(h_SBG_IDs.Value)
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        GetAllGrade()
        h_SBG_IDs.Value = ""
        GridBindsubject(h_SBG_IDs.Value)
    End Sub

    Protected Sub chkGRD_ID_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGRD_ID.SelectedIndexChanged
        Dim strSelectedGrades As String = String.Empty
        Dim strSeperator As String = String.Empty
        For Each lstItem As ListItem In chkGRD_ID.Items
            If lstItem.Selected Then
                strSelectedGrades += strSeperator + lstItem.Value
                strSeperator = "___"
            End If
        Next
        h_GRD_IDs.Value = strSelectedGrades
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        GridBindsubject(h_SBG_IDs.Value)

    End Sub

    Protected Sub txtSubject_TextChanged(sender As Object, e As EventArgs)
        txtSubject.Text = ""
        GridBindsubject(h_SBG_IDs.Value)
    End Sub
End Class
