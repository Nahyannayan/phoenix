<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlStudentPerformance_ByGradeAcrossYear.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlStudentPerformance_ByGradeAcrossYear" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
         <style>
      .col1
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 40%;
    line-height: 14px;
    float: left;
}
.col2
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 60%;
    line-height: 14px;
    float: left;
}
        
        .demo-container label {
    padding-right: 10px;
    width: 100%;
    display: inline-block;
}
 .rcbHeader ul,
.rcbFooter ul,
.rcbItem ul,
.rcbHovered ul,
.rcbDisabled ul {
    margin: 0;
    padding: 0;
    width: 90%;
    display: inline-block;
    list-style-type: none;
}
    .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
        display: inline;
        float: left;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
    }

    .RadComboBox_Default .rcbInner {
        padding: 10px;
        border-color: #dee2da !important;
        border-radius: 6px !important;
        box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        width: 80%;
        background-image: none !important;
        background-color: transparent !important;
    }

    .RadComboBox_Default .rcbInput {
        font-family: 'Nunito', sans-serif !important;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
        box-shadow: none;
    }

    .RadComboBox_Default .rcbActionButton {
        border: 0px;
        background-image: none !important;
        height: 100% !important;
        color: transparent !important;
        background-color: transparent !important;
    }
</style>   
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];

                if (curr_elem.type == 'checkbox' && curr_elem.name != 'ctl00$cphMasterpage$chkTC') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

    </script>
    <script language="javascript" type="text/javascript">


        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

            }
        }
        function OnTreeClick1(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }
        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }
        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function UnCheckAll() {
            var oTree = document.getElementById("<%=tvReport.ClientId %>");
            childChkBoxes = oTree.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = false;
            }

            return true;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Progress Tracker"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">


                    <tr>
                        <td align="center" class="matters">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblClm" runat="server" width="100%">

                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Section</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters"><span class="field-label">Subject</span></td>
                                    <td align="left" class="matters">
                                        <%--<asp:CheckBox ID="chkAll" runat="server" Text="Select All " onclick="javascript:fnSelectAll(this);"></asp:CheckBox>
                                        <br />
                                        <asp:CheckBoxList ID="ddlSubject" runat="server" BorderStyle="Solid"
                                            BorderWidth="1px" Height="206px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                            Width="193px">
                                        </asp:CheckBoxList>--%>
                                        
 <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="ddlSubject"
      CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Subject(s)">  </telerik:RadComboBox>


                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Report Schedule</span></td>
                                    <td align="left" class="matters">
                                        <div class="checkbox-list">

                                       
                                        <asp:TreeView ID="tvReport" runat="server"
                                            ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                            PopulateNodesFromClient="False" ShowCheckBoxes="All">
                                            <ParentNodeStyle Font-Bold="False" />
                                            <HoverNodeStyle Font-Underline="True" />
                                            <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Underline="False"
                                                HorizontalPadding="3px" VerticalPadding="1px" />
                                            <NodeStyle ForeColor="#1B80B6" HorizontalPadding="5px"
                                                NodeSpacing="1px" VerticalPadding="2px" />
                                        </asp:TreeView>
                                             </div>
                                    </td>
                                    <td align="left" class="matters" colspan="2" valign="middle">
                                        <asp:RadioButtonList ID="rdHeader" runat="server" RepeatDirection="Horizontal">
                                        </asp:RadioButtonList></td>
                                </tr>
                                <tr id="trFilter" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Filter By</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlType" runat="server">
                                            <asp:ListItem>ALL</asp:ListItem>
                                            <asp:ListItem>SEN</asp:ListItem>
                                            <asp:ListItem>EAL</asp:ListItem>
                                            <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                            <asp:ListItem>EMIRATI</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in PDF"
                                            ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownloadExcel" runat="server" CssClass="button" Text="Download Report in Excel"
                                            ValidationGroup="groupM1" /></td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>



            </div>
        </div>
    </div>
</asp:Content>

