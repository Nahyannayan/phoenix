﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptKHDAAverageAnalysis

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
 
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290136" And ViewState("MainMnu_code") <> "C290155") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindSubjects(ddlGrade.SelectedItem.Value)

                    Try
                        BindReports()
                    Catch ex As Exception
                    End Try
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGeneratePDF)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try

            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            BindSubjects(ddlGrade.SelectedItem.Value)
            BindReports()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' AND GRD_ID NOT IN('KG1','KG2','11','12') order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindSubjects(ByVal grade As String)

        cblSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_PARENT_ID=0 AND SBG_GRD_ID NOT IN ('KG1','KG2') AND SBG_GRD_ID='" + grade + "'" _
                                & " AND SBG_BSU_ID='" + Session("sBsuid") + "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cblSubject.DataSource = ds
        cblSubject.DataTextField = "SBG_DESCR"
        cblSubject.DataValueField = "SBG_ID"
        cblSubject.DataBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects(ddlGrade.SelectedItem.Value)
        BindReports()
    End Sub

    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade() As String = ddlGrade.selectedvalue.split("|")

        'Dim STR_QUERY As String = " SELECT DISTINCT ACY_ID,ACY_DESCR,RPF_ID,RPF_DESCR FROM " _
        '                        & " RPT.REPORT_SETUP_M AS A " _
        '                        & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS C ON A.RSM_ID=C.RPF_RSM_ID" _
        '                        & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS F ON A.RSM_ID=F.RSG_RSM_ID" _
        '                        & " INNER JOIN VW_ACADEMICYEAR_D AS G ON A.RSM_ACD_ID=G.ACD_ID" _
        '                        & " INNER JOIN VW_ACADEMICYEAR_M AS H ON G.ACD_ACY_ID=H.ACY_ID" _
        '                        & " WHERE RSG_GRD_ID='" + grade(0) + "' AND RSM_BSU_ID='" + Session("SBSUID") + "' FOR XML AUTO"


        Dim STR_QUERY As String = "EXEC [RPT].[rptKHDAAverageAnalysis_GETREPORTS] " _
                               & "'" + Session("SBSUID") + "'," _
                               & "'" + grade(0) + "'," _
                               & Session("CLM").ToString
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACD_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Sub CallReport()


        Dim strSbg As String = ""
        Dim i As Integer
        For i = 0 To cblSubject.Items.Count - 1
            If cblSubject.Items(i).Selected = True Then
                If strSbg <> "" Then
                    strSbg += "|"
                End If
                strSbg += cblSubject.Items(i).Text
            End If
        Next

        Dim param As New Hashtable
        'param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("@SBG_DESCRS", strSbg)
        param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        param.Add("@RPF_IDS", getReportCards)
        param.Add("@ACTION", ddlOperation.SelectedItem.Text)
        param.Add("@ACY_DESCR", ddlAcademicYear.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/KHDA/rptKHDAAverageAnalysis.rpt")

        End With
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRPF As String = ""

        Dim bRSM As Boolean = False

        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        'strRPF += cNode.Text + "_" + ccNode.Text
                        strRPF += ccNode.Value + "_" + cNode.Text

                    End If

                Next
            Next
        Next


        Return strRPF.Trim
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = "0"
        CallReport()
    End Sub

    Protected Sub btnGeneratePDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneratePDF.Click
        hfbDownload.Value = "1"
        CallReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
