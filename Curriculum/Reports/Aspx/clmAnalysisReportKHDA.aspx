﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmAnalysisReportKHDA.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmAnalysisReportKHDA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
               if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_cblGrade') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
        function fnSelectAll1(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_cblSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
</script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            One Level and Above Swing Analysis Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>

                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="5" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                     <td colspan="2"></td>
                                </tr>
                              
                                <%--<tr>
                        <td align="left"  style="width: 184px">
                            Select Section</td>
                        <td >
                            :</td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" Width="107px">
                            </asp:DropDownList></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll2(this);" runat="server"
                                            Text="Select All" AutoPostBack="True" /><br />
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="cblGrade" runat="server" AutoPostBack="True">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td align="left"  width="20%"><span class="field-label">Select Subject</span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkSubject" onclick="javascript:fnSelectAll1(this);" runat="server"
                                            Text="Select All" AutoPostBack="True" /><br />
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="cblSubject" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    
                                   
                                </tr>

                               
                                <tr>
                                     <td align="left" width="20%"><span class="field-label">Current Report Schedule</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCurSchedule" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%" ><span class="field-label">Previous Report Schedule</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrevSchedule" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Select Header</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlHeader" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnGeneratePDF" runat="server" CssClass="button"
                                            Text="Download PDF" ValidationGroup="groupM1" />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>
                                    </td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />

                            <input id="hfbDownload" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_MnuCode" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>



