<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlStudentPerformanceBySubject.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlStudentPerformanceBySubject" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px ;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function openWin() {
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }




    </script>


    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Student Performance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="BlueTable" id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Report Schedule</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlReportSchedule" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters" ><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle"  class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" class="matters" valign="middle"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trsubject" runat="server">
                        <td align="left" class="matters" valign="middle"><span class="field-label">Subject</span></td>
                        <td align="left" class="matters"   valign="middle">
                            <%--<asp:CheckBox ID="chkAll" runat="server" Text="Select All " AutoPostBack="True"></asp:CheckBox>
                            &nbsp;<br />
                            <asp:CheckBoxList ID="ddlSubject" runat="server" AutoPostBack="True" BorderStyle="Solid"
                                BorderWidth="1px" Height="206px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                Width="193px">
                            </asp:CheckBoxList>--%>
                             <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="ddlSubject" 
                                 CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Subject(s)">  </telerik:RadComboBox>


                        </td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr id="trstudent" runat="server">
                        <td align="left" valign="top" class="matters"><span class="field-label">Student</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtStudIDs" runat="server"  OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                            </td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr  >
                        <td align="left" class="matters" valign="middle">
                            <asp:RadioButton ID="rdAbove" runat="server" GroupName="g1" Text="Grade Above" CssClass="field-label" AutoPostBack="True" Checked="True"></asp:RadioButton><br />
                                        <asp:RadioButton ID="rdBelow" runat="server" AutoPostBack="True" CssClass="field-label" GroupName="g1" Text="Grade Below"></asp:RadioButton><br />
                                        <asp:RadioButton ID="rdBetween" runat="server" AutoPostBack="True" CssClass="field-label" GroupName="g1" Text="Grade Between"></asp:RadioButton>

                        </td>
                        <td align="left" class="matters"   valign="middle">
                            <table id="tbData" runat="server">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlG1" runat="server"></asp:DropDownList></td>
                                    <td>
                                        <asp:Label ID="lblA1" runat="server" CssClass="field-label" Text="And"></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlG2" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtG1" ></asp:TextBox></td>
                                    <td>
                                        <asp:Label ID="lblA2" runat="server" CssClass="field-label" Text="And"></asp:Label></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtG2"  ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr runat="server">
                        <td align="left" class="matters" colspan="4" valign="middle">
                            <asp:RadioButtonList ID="rdHeader" runat="server" RepeatDirection="Horizontal" CssClass="field-label">
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                Text="Generate Report" ValidationGroup="groupM1"   /><asp:Button ID="btnGraph" runat="server" CssClass="button"  
                                    Text="Generate Graph" ValidationGroup="groupM1" Visible="False"  />
                            <asp:Button ID="btnByStudent" runat="server" CssClass="button" 
                                Text="Generate Report By Student" ValidationGroup="groupM1"  /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSubjects" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>

