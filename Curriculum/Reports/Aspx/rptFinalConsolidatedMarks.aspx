﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFinalConsolidatedMarks.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptFinalConsolidatedMarks" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>
          <style>
      .col1
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 40%;
    line-height: 14px;
    float: left;
}
.col2
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 60%;
    line-height: 14px;
    float: left;
}
        
        .demo-container label {
    padding-right: 10px;
    width: 100%;
    display: inline-block;
}
 .rcbHeader ul,
.rcbFooter ul,
.rcbItem ul,
.rcbHovered ul,
.rcbDisabled ul {
    margin: 0;
    padding: 0;
    width: 90%;
    display: inline-block;
    list-style-type: none;
}
    .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
        display: inline;
        float: left;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
    }

    .RadComboBox_Default .rcbInner {
        padding: 10px;
        border-color: #dee2da !important;
        border-radius: 6px !important;
        box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        width: 80%;
        background-image: none !important;
        background-color: transparent !important;
    }

    .RadComboBox_Default .rcbInput {
        font-family: 'Nunito', sans-serif !important;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
        box-shadow: none;
    }

    .RadComboBox_Default .rcbActionButton {
        border: 0px;
        background-image: none !important;
        height: 100% !important;
        color: transparent !important;
        background-color: transparent !important;
    }
</style>  
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Overall Consolidated Report"></asp:Label>
            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Subject</span></td>
                        <td align="left" valign="middle" class="matters">
                     <%--       <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                            <br />
                            <asp:CheckBoxList ID="ddlSubject" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                Height="206px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                Width="193px">
                            </asp:CheckBoxList>--%>
                             <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="ddlSubject" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Subject(s)"> 

                             </telerik:RadComboBox>

                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" class="matters" valign="middle"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" colspan="2" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                Text="DownLoad Report in PDF" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfbDownload" runat="server" />


                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>


            </div>
        </div>
    </div>
</asp:Content>

