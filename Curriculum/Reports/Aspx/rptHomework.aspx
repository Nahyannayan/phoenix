﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptHomework.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptHomework" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Homework Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" valign="bottom" style=" width: 100%;">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" Style="text-align: center"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" runat="server" align="center" width="100%"
                    cellpadding="5" cellspacing="0" style="border-collapse: collapse">
                   
                    <tr>
                        <td  align="left" width="20%">
                            <span class="field-label">Accademic Year</span>
                        </td>
                       
                        <td  align="left" width="30%" >
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="108px">
                            </asp:DropDownList>
                        </td>
                          <td  align="left" width="20%" ></td>
                          <td  align="left" width="30%" ></td>
                    </tr>
                    <tr id="trAcd2" runat="server">
                        <td align="left" >
                            <span class="field-label">Grade</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Section</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label"> Submit Date From</span>
                        </td>
                        
                        <td align="left" >
                        
                            <asp:TextBox ID="txtCDate" runat="server" ></asp:TextBox><asp:ImageButton
                                ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td align="left" >
                             <span class="field-label">Submit Date To</span>
                        </td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtSDate" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        
                    </tr>
                  <tr>
                  <td colspan="10" align="center" >
                  <asp:CheckBox ID="cbDetail" Text="Detailed" runat="server" CssClass="field-label"> </asp:CheckBox>
            </td>
                  </tr>
                    <tr>
                        <td colspan="10" align="center">
                            <asp:Button ID="btReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1"
                                TabIndex="7" />
                                <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in Pdf" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
                </div>
            </div>
        </div>
     <asp:HiddenField ID="hfbDownload" runat="server" />
     <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
</asp:Content>

