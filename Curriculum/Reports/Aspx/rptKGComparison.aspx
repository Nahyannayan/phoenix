﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="rptKGComparison.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptKGComparison" title="Untitled Page" %>
   <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
<script language="javascript" type="text/javascript">

function openWin() {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs+"&ACD_ID="+ACD_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                           NameandCode = arg.NameCode.split('||');
            
            document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
            __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs+"&ACD_ID="+ACD_IDs,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
         
         function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
  curr_elem = document.forms[0].elements[i];
  if(curr_elem.type == 'checkbox')
  {
  curr_elem.checked = !master_box.checked;
  }
 }
 master_box.checked=!master_box.checked;
}
   </script>
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> KG Comparison
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


 <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
   
<asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
                            <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
    <table  id="tblrule" runat="server"   width="100%">

                   
       <tr id="tracc" runat="server">
                        <td align="left" width="20%">
                          <span class="field-label" >   Academic Year</span></td>
                     
                        <td align="left" width="30%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
            <td align="left" width="20%"> </td>
            <td align="left" width="30%"> </td>
                    </tr>
        <tr>
            <td align="left" valign="middle" >
                         <span class="field-label" >    Grade</span></td>
          
            <td align="left" valign="middle" >
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" >
                </asp:DropDownList></td>
            <td align="left" width="20%"> </td>
            <td align="left" width="30%"> </td>
        </tr>
        <tr id="r3" runat="server">
            <td align="left"  valign="middle">
            <span class="field-label" >     Section</span></td>
       
            <td align="left"  valign="middle">
                <asp:DropDownList id="ddlSection" runat="server" AutoPostBack="True" >
                </asp:DropDownList></td>
            <td align="left" width="20%"> </td>
            <td align="left" width="20%"> </td>
        </tr>
        
        <tr id="r5" runat="server">
           
            <td align="left" valign="top" >
            <span class="field-label" > Student</span></td>
          
            <td align="left"  class="matters">
                <asp:TextBox id="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click">
                </asp:ImageButton>
                <asp:GridView id="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                PageSize="5" Width="100%" CssClass="table table-bordered table-row" OnPageIndexChanging="grdStudent_PageIndexChanging">
                <columns>
                <asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
                <asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                </columns>
                <headerstyle cssclass="gridheader_new" />
                </asp:GridView>
            </td>
            <td align="left" width="20%"> </td>
             <td align="left" width="20%"> </td>
        </tr>
         <tr  id="rr1" runat="server">
             

            <td align="left" valign="middle" 
                colspan="2"> 
                            <asp:RadioButton ID="rdYear"  runat="server" GroupName="g1"  CssClass="field-label"
                                Text="Report By Year" Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="rdTerm" GroupName="g1"   runat="server"  CssClass="field-label"
                                Text="Report By Term" AutoPostBack="True" />
                             <asp:RadioButton ID="rdMonth" GroupName="g1"   runat="server"  CssClass="field-label"
                                Text="Report By Month" AutoPostBack="True" />
            </td>
             <td align="left" width="20%"> </td>
             <td align="left" width="20%"> </td>
        </tr>  
        <tr  id="r6" runat="server">
          
            <td align="left" valign="middle"  >
                           <span class="field-label" >  Report Schedule</span></td>
          
            <td align="left" valign="middle" >
           <%--  <table>
             <tr><td colspan=2>
                 <asp:CheckBox id="chkRpf" runat="server" Text="Select All " 
                     AutoPostBack="True" ></asp:CheckBox>
                </td></tr>
                <tr><td>--%>
          <%--      <asp:CheckBoxList id="lstRpf" runat="server" AutoPostBack="True" BorderStyle="Solid"
                    BorderWidth="1px" Height="206px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid;
                    border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid;
                    border-bottom: #1b80b6 1px solid; text-align: left" Width="193px">
                </asp:CheckBoxList>--%>
                    
                            <telerik:RadComboBox RenderMode="Lightweight" ID="lstRpf" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                Width="100%">
                            </telerik:RadComboBox>

               <%-- </td><td valign="bottom">
                    &nbsp;</td></tr>
                 </table>--%>
                 
                </td>
            <td align="left" width="20%"> </td>
              <td align="left" width="20%"> </td>
        </tr>
        <tr runat="server" id="r7">
           
            <td align="left"  valign="middle">
               <span class="field-label" >  By Gender</span></td>
         
            <td align="left"  valign="middle">
                <asp:CheckBox ID="chkGender" runat="server" /></td>
            <td align="left" width="20%"> </td>
             <td align="left" width="20%"> </td>
        </tr>
         <tr runat="server" id="trCriteria">
           
            <td align="left"  valign="middle">
               <span class="field-label" >  Include Criteria</span></td>
         
            <td align="left"  valign="middle">
                <asp:CheckBox ID="chkCriteria" runat="server" /></td>
            <td align="left" width="20%"> </td>
             <td align="left" width="20%"> </td>
        </tr>
       
     
        <tr>

            <td align="left" colspan="4" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                   Text="Generate Report" ValidationGroup="groupM1"  />
                <asp:Button id="btnDownload" runat="server" CssClass="button" 
                   Text="DownLoad Report in PDF" ValidationGroup="groupM1" />
              </td>
        </tr>
                </table>
    <asp:HiddenField id="h_STU_IDs" runat="server">
    </asp:HiddenField>
    

    <asp:HiddenField ID="hfbDownload" runat="server" />
    

    <asp:HiddenField id="hfSubjects" runat="server">
    </asp:HiddenField>


    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
        

                  </div>
        </div>
    </div>
</asp:Content>

