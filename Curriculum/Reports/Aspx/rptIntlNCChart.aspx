﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlNCChart.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlNCChart" Title="Untitled Page" %>

<asp:Content ID="ddlPrintedFor" ContentPlaceHolderID="cphMasterpage"
    runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="NC Chart"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" style="width: 100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                        <td align="left" valign="middle" width="20%">
                            <span class="field-label">Report Card</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlReportSchedule" runat="server" Width="200px" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" valign="middle">
                            <span class="field-label">Subject</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True"
                                Width="200px">
                            </asp:DropDownList></td>

                        <td align="left" valign="middle">
                            <span class="field-label">By Gender</span></td>

                        <td align="left" valign="middle">
                            <asp:CheckBox ID="chkGender" runat="server"></asp:CheckBox></td>
                    </tr>
                    <tr runat="server">
                        <td align="left" valign="middle">
                            <span class="field-label">Filter By</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlType" runat="server"
                                Width="199px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList></td>

                        <td align="left" valign="middle" colspan="3">
                            <asp:RadioButton ID="rdGrade" runat="server" Checked="True" GroupName="g1" Text="By Grade" class="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdKeyStage" runat="server" GroupName="g1" Text="By KeyStage" class="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdSection" runat="server" GroupName="g1" Text="By Section" class="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rdStudent" runat="server" GroupName="g1" Text="By Student" AutoPostBack="true" class="field-label"></asp:RadioButton></td>
                    </tr>
                    <tr runat="server" id="trGrade">
                        <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"
                                Width="200px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trSection">
                        <td align="left" valign="middle">
                            <span class="field-label">Section</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server"
                                Width="200px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                    </tr>
                </table>
            </div>

        </div>
    </div>

</asp:Content>

