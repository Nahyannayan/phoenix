<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReportRuleList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptReportRuleList" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script>
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%" >
                    
                    <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Academic Year</span></td>
                       
                        <td align="left" width="35%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Report Card</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlReportCard" runat="server" AutoPostBack="True"  Width="303px">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
        <tr>
            <td align="left">
                <span class="field-label">Report Schedule</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlPrintedFor" runat="server">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Report Header</span></td>
            
            <td align="left"><asp:DropDownList id="ddlHeader" runat="server"  Width="177px">
            </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                            <span class="field-label">Grade</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" Width="172px">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr id ="trSubject" runat="server" >
            <td align="left">
                <span class="field-label">Subject</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True">
                </asp:DropDownList>&nbsp;
            </td>
            <td></td>
            <td></td>
                    </tr>
        <tr runat="server">
            <td align="left">
                <span class="field-label">Section</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlSection" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server">
            <td align="left">
                <asp:RadioButton id="rdAll" CssClass="field-label" runat="server" GroupName="g1" Text="All" Checked="True">
                </asp:RadioButton>
                <asp:RadioButton id="rdNoRuleSet" CssClass="field-label" runat="server" Text="No Rule Set" GroupName="g1">
                </asp:RadioButton></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" />&nbsp;<asp:Button ID="btnDownload"
                       runat="server" CssClass="button" Text="Download in PDF" /></td>
        </tr>
                </table>
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
    <asp:HiddenField ID="hfbDownload" runat="server" />
    
                </div>
            </div>
        </div>


</asp:Content>

