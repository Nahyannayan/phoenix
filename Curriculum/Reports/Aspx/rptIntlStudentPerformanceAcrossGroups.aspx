<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlStudentPerformanceAcrossGroups.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlStudentPerformanceAcrossGroups" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Subject Performance Across Groups
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" width="100%">
                    <%-- <tr>
                        <td valign="middle" class="subheader_img" colspan="12">
                            Subject wise performance</td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                        <td align="left" width="20%">
                            <span class="field-label">Select Report Card</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Report Schedule</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlReportSchedule" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" valign="middle">
                            <span class="field-label">Subject</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle">
                            <span class="field-label">Group</span></td>

                        <td align="left" valign="middle">
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="lstGroup" runat="server" AutoPostBack="True" RepeatLayout="Flow">
                                </asp:CheckBoxList>
                            </div>

                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" valign="middle">
                            <table boreder="0">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rdAbove" runat="server" GroupName="g1" Text="Grade Above" AutoPostBack="True" Checked="True" CssClass="field-label"></asp:RadioButton><br />
                                        <asp:RadioButton ID="rdBelow" runat="server" AutoPostBack="True" GroupName="g1" Text="Grade Below" CssClass="field-label"></asp:RadioButton><br />
                                        <asp:RadioButton ID="rdBetween" runat="server" AutoPostBack="True" GroupName="g1" Text="Grade Between" CssClass="field-label"></asp:RadioButton></td>
                                </tr>
                            </table>
                        </td>

                        <td align="left" valign="middle">
                            <table id="tbData" runat="server">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlG1" runat="server"></asp:DropDownList></td>
                                    <td>
                                        <asp:Label ID="lblA1" runat="server" Text="And"></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlG2" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtG1"></asp:TextBox></td>
                                    <td>
                                        <asp:Label ID="lblA2" runat="server" Text="And" CssClass="field-label"></asp:Label></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtG2"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" width="20%"> <asp:RadioButtonList ID="rdHeader" runat="server" RepeatDirection="Horizontal" CssClass="field-label">
                            </asp:RadioButtonList></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>

                        <td align="left" style="text-align: center" colspan="4">
                            <asp:Button ID="btnGraph" runat="server" CssClass="button"
                                Text="Generate Graph" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGroups" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>

