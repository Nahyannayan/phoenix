Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    'Populate Term 
                    BindTerm()
                    'Populate Activity
                    BindActivity()
                    'Populate Grade
                    GetAllGradeForActivity()

                    txtSubject.Attributes.Add("ReadOnly", "ReadOnly")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub BindActivity()
        ddlACTIVITY.DataSource = ACTIVITYSCHEDULE.GetActivityDetails(ddlAca_Year.SelectedValue, ddlTerm.SelectedValue)
        ddlACTIVITY.DataTextField = "CAD_DESC"
        ddlACTIVITY.DataValueField = "CAD_ID"
        ddlACTIVITY.DataBind()
    End Sub

    Sub GetAllGradeForActivity()
        chkGRD_ID.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBSUID"), ddlAca_Year.SelectedValue)
        chkGRD_ID.DataTextField = "GRM_GRD_ID"
        chkGRD_ID.DataValueField = "GRM_DISPLAY"
        chkGRD_ID.DataBind()
    End Sub

    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

     Private Function GetSelectedGrades() As Hashtable
        Return Nothing
    End Function

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindTerm()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_SBM_IDs.Value <> "" Then
            GridBindsubject(h_SBM_IDs.Value)
        End If
        'If h_SBM_IDs.Value <> "" AndAlso h_SBM_IDs.Value.Contains("___") Then
        '    Dim vSBM_ID As String = h_SBM_IDs.Value.Split("___")(0)
        '    Dim bCORE_EXT As Boolean = h_SBM_IDs.Value.Split("___")(3)
        '    If bCORE_EXT Then
        '    Else
        '    End If
        '    txtSubject.Text = h_SBM_IDs.Value.Split("___")(6)
        '    h_SBM_IDs.Value = vSBM_ID
        'End If
    End Sub

    Private Sub GridBindsubject(ByVal vSBM_IDs As String)
        grdSubject.DataSource = ReportFunctions.GetSelectedSubjects(vSBM_IDs)
        grdSubject.DataBind()
    End Sub

    Private Sub GridBindsubjectGroups(ByVal vSGR_IDs As String)
        grdSubjGrp.DataSource = ReportFunctions.GetSelectedSubjectGroups(vSGR_IDs)
        grdSubjGrp.DataBind()
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Protected Sub chkGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSelectedGrades As String = String.Empty
        Dim strSeperator As String = String.Empty
        For Each lstItem As ListItem In chkGRD_ID.Items
            If lstItem.Selected Then
                strSelectedGrades += strSeperator + lstItem.Value
                strSeperator = "___"
            End If
        Next
        h_GRD_IDs.Value = strSelectedGrades
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindActivity()
    End Sub

    Protected Sub imgSubGrp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_SGR_IDs.Value <> "" Then
            GridBindsubjectGroups(h_SGR_IDs.Value)
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If

    End Sub

    Protected Sub grdSubject_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdSubject.PageIndex = e.NewPageIndex
        GridBindsubject(h_SBM_IDs.Value)
    End Sub

    Protected Sub grdSubjGrp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdSubjGrp.PageIndex = e.NewPageIndex
        GridBindsubjectGroups(h_SGR_IDs.Value)
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case ViewState("MainMnu_code")
                Case CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT
                    GenerateMonthlyProgressReport()
            End Select
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub GenerateMonthlyProgressReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@PERIOD", CDate(txtDate.Text))
        param.Add("@STU_IDs", h_STU_IDs.Value)
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptMONTHLY_PROGRESS_REPORT_CIS.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
 
End Class
