Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptStreamAllocationList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C970006") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                BindStream()
                BindOption()
            End If
        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindStream()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlStream.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT STM_DESCR,STM_ID FROM OASIS..STREAM_M AS A" _
                               & " INNER JOIN OASIS..GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID" _
                               & " WHERE GRM_GRD_ID='11' AND GRM_ACD_ID='" + Session("NEXT_ACD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlStream.Items.Insert(0, li)
    End Sub


    Sub BindOption()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ddlOption.Items.Clear()
        Dim str_query As String = "SELECT SGM_ID,SGM_DESCR FROM SUBJECTOPTION_GROUP_M WHERE SGM_ACD_ID=" + Session("NEXT_ACD_ID") _
                                 & " AND SGM_GRD_ID='11'"

        If ddlStream.SelectedValue <> "0" Then
            str_query += " AND SGM_STM_ID='" + ddlStream.SelectedValue.ToString + "'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlOption.DataSource = ds
        ddlOption.DataTextField = "SGM_DESCR"
        ddlOption.DataValueField = "SGM_ID"
        ddlOption.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlOption.Items.Insert(0, li)
    End Sub
   
    Private Sub GenerateOptionAllocationReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("CURRENT_ACD_ID"))
        param.Add("@ACD_NEXTID", Session("NEXT_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STM_ID", ddlStream.SelectedValue.ToString)
        param.Add("@SGM_ID", ddlOption.SelectedValue.ToString)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptOPtionGroupAllocated.rpt")
        End With
        Session("rptClass") = rptClass
        
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

#End Region

   
    'Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
    '    CallReport()
    'End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateOptionAllocationReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
