﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCGPAofStudent.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCGPAofStudent" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
              <asp:Label id="lblHeader" runat="server" Text="CGPA Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <table  align="center"  style="border-collapse:collapse" cellpadding="4" cellspacing="0"  width="100%">
       
        <tr>
            <td align="left" width="20%">
                           <span class="field-label"> Academic Year</span></td>
           
            <td align="left" >
                <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                            <span class="field-label">Report card Setup</span></td>
            
            <td align="left" >
                <asp:DropDownList id="ddlReportType" runat="server" AutoPostBack="True"  Width="331px">
                </asp:DropDownList>
            </td>
        
            <td align="left">
                <span class="field-label">Report card schedule</span></td>
            
            <td align="left" >
                <asp:DropDownList id="ddlReportPrintedFor" runat="server" Width="264px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id ="20" >
            <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                &nbsp;
                        </td>
            <td align="left" valign="middle" >
                            <span class="field-label">Section</span>
                        </td>
           
            <td align="left"  valign="middle">
                <asp:DropDownList ID="ddlSection" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
            <td align="left" colspan="6" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                    Text="Generate Report"  ValidationGroup="GR" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" 
                    Text="Download in PDF" />
            </td>
        </tr>
    </table>
                </div></div></div>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="hfbFinalReport" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
</asp:Content>

