﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_SectionWisePerformancePerSubject_Conditional.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_SectionWisePerformancePerSubject_Conditional" title="Untitled Page" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
  curr_elem = document.forms[0].elements[i];
  if(curr_elem.type == 'checkbox')
  {
  curr_elem.checked = !master_box.checked;
  }
 }
 master_box.checked=!master_box.checked;
}
</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <asp:Label id="lblerror" runat="server" CssClass="error" align="left" ></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%">
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>

                        <td align="left">
                           <span class="field-label">Subject</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
                    </tr>
                  
       
                    <tr id ="trSubject" runat="server" >
                        <td align="left">
                            <span class="field-label">Grade</span></td>
                        
                        <td align="left">
                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                            <div class="checkbox-list">
                            <asp:CheckBoxList id="lstGrade" runat="server"
                               RepeatLayout="Flow">
                            </asp:CheckBoxList>
                                </div>
                        </td>
                         <td align="left">
                <span class="field-label">Report Card</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlPrintedFor" runat="server" AutoPostBack="TRUE"   Width="197px">
                </asp:DropDownList>
                </td>

                    </tr>
                    
       
             <tr>
            <td align="left">
                <span class="field-label">Report Header</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlHeader" runat="server">
                </asp:DropDownList>
                </td>
                 <td align="left">
                <span class="field-label">Criteria</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlCriteria" runat="server" AutoPostBack="True" >
                      <asp:ListItem Value="ABOVE">MARKS ABOVE</asp:ListItem>
                    <asp:ListItem Value="BELOW">MARKS BELOW</asp:ListItem>
                    <asp:ListItem Value="BETWEEN">MARKS BETWEEN</asp:ListItem>
                </asp:DropDownList></td>
        </tr>       
       
      
        <tr id="trMark" runat="server" >
            <td align="left"><span class="field-label">Marks From</span></td>
            <td align="left">
                <asp:TextBox ID="txtMark1" runat="server"></asp:TextBox>
</td>
               <td align="left"> <asp:Label ID="lblAnd" runat="server" CssClass="field-label" Text="Marks To"></asp:Label>
</td>
               <td> <asp:TextBox ID="txtMark2" runat="server" Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" />
                &nbsp;<asp:Button ID="btnDownload" runat="server" CssClass="button" 
                                TabIndex="7" Text="Download Report In PDF" ValidationGroup="groupM1" 
                                />
    <asp:HiddenField id="hfbDownload" runat="server">
    </asp:HiddenField>
    

    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
        

                                           </td>
        </tr>
                </table>

                </div>
            </div>
         </div>

</asp:Content>

