﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptGradeEntryAssessment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300401") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))



                    BindReportType()
                    BindReportPrintedFor()
                    GetAllGrade()
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub
    Sub GetAllGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        'ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAcademicYear.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        If bSuperUsr Then
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER  " & _
            "  FROM VW_GRADE_BSU_M INNER JOIN oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcademicYear.SelectedValue & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue & "'" & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = " SELECT DISTINCT grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,STM_ID,CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR end AS GRM_DISPLAY,GRD_DISPLAYORDER" & _
            "  FROM VW_GRADE_BSU_M inner join oasis.dbo.STREAM_M  on GRM_STM_ID=STM_ID" & _
            " inner join VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAcademicYear.SelectedValue & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue & "'" & _
            " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EmployeeID") & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrade.DataSource = ds


        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grd_id"
        ddlGrade.DataBind()

    End Sub
    Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function
    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "")
        ddl.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String()


        If Grdid <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "'"
        End If
        If Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If
       
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL  AND SGR_ACD_ID='" + acdid + "'"
            
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"
            'AND SGR_GRD_ID='" & strGRD_IDs & "' AND SGR_SBG_ID=" & vSBM_IDs & " ORDER BY SGR_ID "
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        If (Not ddl.Items Is Nothing) AndAlso (ddl.Items.Count > 1) Then
            ddl.Items.Insert(0, li)
            ddl.Items.FindByText("ALL").Selected = True
        End If
        Return ddl
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportType()
        BindReportPrintedFor()
        GetAllGrade()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()
        GetAllGrade()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlReportPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportPrintedFor.SelectedIndexChanged

        GetAllGrade()
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub
    Sub CallReport()
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@GRD_ID", grade(0))
        param.Add("@sbg_id", ddlSubject.SelectedValue.ToString)
        param.Add("@sgr_id", ddlGroup.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportType.SelectedValue.ToString)
        param.Add("@rpf_id", ddlReportPrintedFor.SelectedValue.ToString)
        param.Add("@grd", ddlMarks.SelectedItem.ToString)
        param.Add("ac_id", ddlAcademicYear.SelectedItem.ToString)
        param.Add("grd_id", grade(0))
        param.Add("Subject", ddlSubject.SelectedItem.ToString)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptGradeEntryAssessment.rpt")
        End With

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub
End Class
