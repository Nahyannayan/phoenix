Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class Curriculum_Reports_rptIntlStudentPerformanceByGrade_Excel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim acdid As String = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            H_RPF.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
            Dim grd As String = Encr_decrData.Decrypt(Request.QueryString("grd").Replace(" ", "+"))
            h_SBG.Value = Encr_decrData.Decrypt(Request.QueryString("sbg").Replace(" ", "+"))
            Dim sctid As String = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
            h_Type.Value = Encr_decrData.Decrypt(Request.QueryString("type").Replace(" ", "+"))
            GridBind(acdid, H_RPF.Value, sctid, grd, h_SBG.Value)
        End If
    End Sub

#Region "Private Methods"
    Sub GridBind(ByVal acdid As String, ByVal rpf As String, ByVal sctid As String, ByVal grd As String, ByVal sbg As String)
        Dim grdid As String() = grd.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "[RPT].[rptSTUDENTPERFORMANCEREPORT_BYGRADE_ALLYEARS_SEF] " _
                                & "'" + Session("sbsuid") + "'," _
                                & acdid + "," _
                                & "'" + rpf + "'," _
                                & "" + grdid(1) + "," _
                                & sctid + "," _
                                & "'" + grdid(0) + "'," _
                                & "'" + sbg + "'," _
                                & "1,'" + h_Type.Value + "','EXCEL'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()



        Dim sw As New StringWriter
        Dim hw As New HtmlTextWriter(sw)
        gvStud.RenderControl(hw)

        Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "StudentData.xls"))
        Response.ContentType = "application/ms-excel"
        Response.Write(sw.ToString)

        Response.End()
    End Sub


#End Region

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal gvStud As Control)

        ' Verifies that the control is rendered
    End Sub

    Protected Sub gvStud_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then




            Dim i, j As Integer



            '    Dim HeaderGrid As GridView = DirectCast(sender, GridView)

            '    Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            '    Dim HeaderCell As New TableCell()


            '    HeaderCell.Text = "Student ID"
            '    HeaderCell.Font.Bold = True
            '    HeaderCell.RowSpan = 3
            '    HeaderGridRow.Cells.Add(HeaderCell)

            '    HeaderCell = New TableCell()
            '    HeaderCell.Text = "Name"
            '    HeaderCell.Font.Bold = True
            '    HeaderCell.RowSpan = 3
            '    HeaderGridRow.Cells.Add(HeaderCell)

            '    HeaderCell = New TableCell()
            '    HeaderCell.Text = "Section"
            '    HeaderCell.Font.Bold = True
            '    HeaderCell.RowSpan = 3
            '    HeaderGridRow.Cells.Add(HeaderCell)

            '    HeaderCell = New TableCell()
            '    HeaderCell.Text = "Gender"
            '    HeaderCell.Font.Bold = True
            '    HeaderCell.RowSpan = 3
            '    HeaderGridRow.Cells.Add(HeaderCell)

            '    gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow)


            Dim HeaderCell1 As New TableCell()
            Dim HeaderCell2 As New TableCell()
            Dim HeaderCell3 As New TableCell()

            Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            '    Dim HeaderGridRow3 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

            ' Dim subjects As New ArrayList
            '    Dim years As New Hashtable
            '    Dim reports As New Hashtable

            '    Dim sbg As String()
            '    Dim rpf As String()
            '    Dim dt As New DataTable

            '    Dim column As DataColumn

            '    column = New DataColumn
            '    column.DataType = System.Type.GetType("System.String")
            '    column.ColumnName = "SBG"
            '    dt.Columns.Add(column)

            '    column = New DataColumn
            '    column.DataType = System.Type.GetType("System.String")
            '    column.ColumnName = "YR"
            '    dt.Columns.Add(column)


            '    column = New DataColumn
            '    column.DataType = System.Type.GetType("System.String")
            '    column.ColumnName = "RPF"
            '    dt.Columns.Add(column)

            '    Dim dr As DataRow

            Dim Year As String
            Dim Subject As String
            Dim tSubject As String = ""
            Dim scolspan As Integer = 1


            Dim tYear As String = ""
            Dim ycolspan As Integer = 1

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow1.Cells.Add(HeaderCell1)


            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)


            For i = 4 To e.Row.Cells.Count - 1
                Subject = e.Row.Cells(i).Text.Substring(0, e.Row.Cells(i).Text.IndexOf("||"))
                If Subject <> tSubject Then
                    If tSubject <> "" Then
                        HeaderCell2.ColumnSpan = scolspan
                        HeaderGridRow2.Cells.Add(HeaderCell2)

                        HeaderCell1.ColumnSpan = ycolspan
                        HeaderGridRow1.Cells.Add(HeaderCell1)
                        HeaderCell1 = New TableCell()
                    End If

                    HeaderCell2 = New TableCell()
                    HeaderCell2.Text = Subject
                    scolspan = 1
                    ycolspan = 1
                    tSubject = Subject
                    tYear = ""
                Else
                    scolspan += 1
                End If
                Year = e.Row.Cells(i).Text.Substring(e.Row.Cells(i).Text.IndexOf("||") + 2, 9)
                If Year <> tYear Then
                    If tYear <> "" Then
                        HeaderCell1.ColumnSpan = ycolspan
                        HeaderGridRow1.Cells.Add(HeaderCell1)
                    End If

                    HeaderCell1 = New TableCell()
                    HeaderCell1.Text = Year
                    ycolspan = 1
                    tYear = Year
                Else
                    ycolspan += 1
                End If
                e.Row.Cells(i).Text = e.Row.Cells(i).Text.Substring(e.Row.Cells(i).Text.IndexOf("__") + 2)
            Next
            If Subject <> "Overall" Then

                HeaderCell1 = New TableCell()
                HeaderCell1.Text = Year
                HeaderCell1.ColumnSpan = ycolspan
                HeaderGridRow1.Cells.Add(HeaderCell1)

                HeaderCell2 = New TableCell()
                HeaderCell2.Text = Subject
                HeaderCell2.ColumnSpan = scolspan
                HeaderGridRow2.Cells.Add(HeaderCell2)
            End If
            gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow1)
            gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow2)


        End If
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim i As Integer
            Dim str As String()
            For i = 4 To e.Row.Cells.Count - 1
                With e.Row.Cells(i)
                    str = .Text.Split("|")
                    If str.Length > 1 Then
                        e.Row.Cells(i).Text = str(0)
                        e.Row.Cells(i).Font.Bold = True
                        Select Case str(1)
                            Case "RED"
                                e.Row.Cells(i).ForeColor = Drawing.Color.Red
                            Case "GREEN"
                                e.Row.Cells(i).ForeColor = Drawing.Color.Green
                            Case "YELLOW"
                                e.Row.Cells(i).ForeColor = Drawing.Color.LightSalmon
                            Case Else
                                e.Row.Cells(i).ForeColor = Drawing.Color.Black
                        End Select
                    End If
                End With
            Next
        End If
    End Sub


End Class
