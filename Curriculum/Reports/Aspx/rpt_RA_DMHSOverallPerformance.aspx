﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_DMHSOverallPerformance.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_DMHSOverallPerformance" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
  curr_elem = document.forms[0].elements[i];
  if(curr_elem.type == 'checkbox')
  {
  curr_elem.checked = !master_box.checked;
  }
 }
 master_box.checked=!master_box.checked;
}



</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label id="lblHeader" runat="server" Text="DMHS Overall Performers"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%" >
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                        <td align="left">
                            <span class="field-label">Report Card</span></td>
                       
                        <td align="left">
                            <asp:DropDownList id="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    
        <tr>
            <td align="left" valign="middle">
                <span class="field-label">Report Schedule</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlPrintedFor" runat="server">
                </asp:DropDownList></td>
             <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server">
                </asp:DropDownList></td>
        </tr>
             
        <tr >
            <td align="left" valign="middle">
                <span class="field-label">Criteria</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlCriteria" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="TOP">HIGH ACHIEVERS</asp:ListItem>
                    <asp:ListItem Value="BOTTOM">LOW ACHIEVERS</asp:ListItem>
                    <asp:ListItem Value="ABOVE">AVERAGE ABOVE</asp:ListItem>
                    <asp:ListItem Value="BELOW">AVERAGE BELOW</asp:ListItem>
                    <asp:ListItem Value="BETWEEN">AVERAGE BETWEEN</asp:ListItem>
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
      
        <tr id="trMark" runat="server" >
            <td align="left"><span class="field-label">Marks From</span></td>
            <td align="left" valign="middle">
                <asp:TextBox ID="txtMark1" runat="server"></asp:TextBox></td>
            <td align="left">
                <asp:Label ID="lblAnd" CssClass="field-label" runat="server" Text="Marks To"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="txtMark2" runat="server" Visible="False"></asp:TextBox>
            </td>
        </tr>
          <tr >
            <td align="left" valign="middle">
                <span class="field-label">No of Records</span></td>
            
            <td align="left" valign="middle">
                <asp:TextBox ID="txtRecord" runat="server" class="field-value">10</asp:TextBox>
            </td>
              <td></td>
              <td></td>
        </tr>
        <tr>
            <td align="center" colspan="4" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
        </tr>
                </table>
    
                </div>
            </div>
        </div>

</asp:Content>

