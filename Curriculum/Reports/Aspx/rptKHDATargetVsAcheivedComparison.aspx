﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptKHDATargetVsAcheivedComparison.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptKHDATargetVsAcheivedComparison" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>

    </style>

<script language="javascript" type="text/javascript">
    function fnSelectAll2(master_box) {
        var curr_elem;
        var checkbox_checked_status;
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            curr_elem = document.forms[0].elements[i];
            if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_cblGrade') {
                curr_elem.checked = master_box.checked;
            }
        }
        // master_box.checked=!master_box.checked;
    }
    function fnSelectAll1(master_box) {
        var curr_elem;
        var checkbox_checked_status;
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            curr_elem = document.forms[0].elements[i];
            if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_cblSubject') {
                curr_elem.checked = master_box.checked;
            }
        }
        // master_box.checked=!master_box.checked;
    }
</script>
   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            Target Vs Achieved Comparison </div>
 <div class="card-body">
            <div class="table-responsive m-auto">
    
 <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
                 
            <tr >              
              <td  align="left" >
                  <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
              </tr>
             
           <tr>
            <td align="left"   valign="top">
                
                <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="5"  width="100%">
                   
                    
                    <%--<tr><td class="subheader_img" colspan="6">TARGET VS ACHIEVED COMPARISON REPORT</td></tr>--%>
                    <tr>
                    <td  align="left" colspan="2">
                        <table width="89%">
                            <tr>
                                <td class="title-bg-small" style="text-align:center;" align="center">
                                    TARGET
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                   <td  align="left" colspan="2">
                       <table width="89%">
                           <tr>
                               <td class="title-bg-small" style="text-align:center;" align="center">
                                   ACHIEVED
                               </td>
                           </tr>
                       </table>
                   </td>
                    </tr>
                 
                      <tr>
                        <td align="left" width="20%">
                           <span class="field-label">Select Academic Year</span> </td>
                        
                        <td align="left" width="30%">
                             <asp:DropDownList ID="ddlTargetAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               
                      </td>
                      <td align="left" width="20%" >
                            <span class="field-label">Select Academic Year</span></td>
                      
                        <td align="left" width="30%">
                             <asp:DropDownList ID="ddlAchievedAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               
                      </td>
                       </tr>  
                        
                    <tr>
                        <td align="left" width="20%" >
                            <span class="field-label">Report Setup</span></td>
                      
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlTargetReportSetup" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                            <td align="left" width="20%" >
                            <span class="field-label">Report Setup</span></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAchievedReportSetup" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
                    
                    
                    <tr>
                        <td align="left" width="20%" >
                            <span class="field-label">Report Schedule</span></td>
                       
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlTargetReportSchedule" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>

                        <td align="left" width="20%"  >
                           <span class="field-label">Report Schedule</span></td>
                       
                        <td align="left" width="30%"  >
                            <asp:DropDownList ID="ddlAchievedReportSchedule" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
                   
                    
                   <tr>
                        <td align="left" width="20%" >
                            <span class="field-label">Select Header</span></td>
                       
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlTargetHeader" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                            <td align="left" width="20%" >
                            <span class="field-label">Select Header</span></td>
                        
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlAchievedHeader" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                       <td colspan="4">
                           <br /><hr />
                       </td>                                                                        
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Grade</span></td>
                       
                        <td align="left"  >
                        <%--<asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll2(this);" runat="server"
                    Text="Select All" AutoPostBack="True" /><br />
                        <asp:CheckBoxList id="cblGrade" runat="server" AutoPostBack="True" BorderStyle="Solid"
                                BorderWidth="1px" Height="206px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid;
                                border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid;
                                border-bottom: #1b80b6 1px solid; text-align: left" Width="193px">
                            </asp:CheckBoxList>--%>
                            <asp:DropDownList ID="ddlGrade"  AutoPostBack="true" runat="server" >
                            </asp:DropDownList>
                            </td>
                         <td align="left" >
                           <span class="field-label"> Select Subject</span></td>
                        <td align="left" >
                         <asp:CheckBox ID="chkSubject" onclick="javascript:fnSelectAll1(this);" runat="server"
                    Text="Select All" AutoPostBack="True" /><br />
                            <div class="checkbox-list"  >
                           <asp:CheckBoxList id="cblSubject" runat="server" >
                            </asp:CheckBoxList></div></td>
                            
                    </tr>
                    
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" />
                                <asp:Button id="btnGeneratePDF" runat="server" CssClass="button" 
                                Text="Download PDF" ValidationGroup="groupM1" />
                                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                                </td>
                    </tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
                        
                        <input id="hfbDownload" runat="server"
                        type="hidden" value="=" />
               </td></tr>
           
    
</table>
    <asp:HiddenField id="h_STU_IDs" runat="server">
    </asp:HiddenField>
     
 <asp:HiddenField id="h_MnuCode" runat="server">
    </asp:HiddenField>

</div></div></div>
</asp:Content>





