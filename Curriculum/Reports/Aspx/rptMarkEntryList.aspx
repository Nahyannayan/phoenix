<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptMarkEntryList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptMarkEntryList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            MARK ENTRY LIST
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" valign="middle">

                            <table id="tblClm" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Term</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server" >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Width="183px">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" Width="183px">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblGrp" runat="server" Text="Label" CssClass="field-label"></asp:Label> </td>
                                    <td align="left">
                                        <asp:RadioButton
                                            ID="rdGroup" runat="server" AutoPostBack="True" Checked="True" GroupName="Grp"
                                            Text="Group" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rdSection" runat="server" AutoPostBack="True" GroupName="Grp"
                                            Text="Section" CssClass="field-label"></asp:RadioButton>
                                   

                                    
                                        <asp:DropDownList ID="ddlGroup" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Assesment</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlActivity" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Marks Entered</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlMarks" runat="server">
                                            <asp:ListItem>ALL</asp:ListItem>
                                            <asp:ListItem>YES</asp:ListItem>
                                            <asp:ListItem>NO</asp:ListItem>
                                            <asp:ListItem>PARTIAL</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>

