<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFormativeConsolidated.aspx.vb" Inherits="rptFormativeConsolidated" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            var SGR_IDs = document.getElementById('<%=ddlGroup.ClientID %>').value;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
                var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
                if (GRD_IDs == '') {
                    alert('Please select atleast one Grade')
                    return false;
                }
                var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID + "&SGR_IDs=" + SGR_IDs, "pop_stud");
               <%-- result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID + "&SGR_IDs=" + SGR_IDs, "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }

         function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudIDs.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_stud" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="FORMATIVE CONSOLIDATED"></asp:Label></div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                    <table class="matters" align="center" width="100%" style="border-collapse: collapse" cellpadding="4" cellspacing="0">
                       
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td align="left" width="20%"><span class="field-label">Report Card</span></td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlReportCard" runat="server"
                                     AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                     
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Schedule</span></td>
                          
                            <td align="left" width="30%">
                                <div class="checkbox-list">
                                <asp:CheckBoxList ID="ddlPrintedFor" runat="server"
                                    >
                                </asp:CheckBoxList></div></td>
                             <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span></td>
                          
                            <td align="left" width="30%" valign="middle">
                                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"
                                    >
                                </asp:DropDownList>
                            </td>
                        </tr>
                      
                        <tr>
                            <td align="left" width="20%" valign="top"><span class="field-label">Subject</span></td>
                           
                            <td align="left" width="30%" valign="top">
                                <asp:DropDownList ID="ddlSubject" runat="server"
                                    AutoPostBack="True" >
                                </asp:DropDownList></td>
                            <td align="left" valign="top" width="20%">
                                <asp:RadioButton ID="rdGroupWise" runat="server" Checked="True" GroupName="g1" CssClass="field-label"
                                    Text="Report By Group" AutoPostBack="True" /><br />
                                <asp:RadioButton ID="rdGroupMarkWise" runat="server" GroupName="g1" CssClass="field-label"
                                    Text="Report By Group(With Marks)" AutoPostBack="True" /><br />
                                <asp:RadioButton ID="rdStudWise" runat="server" GroupName="g1" CssClass="field-label"
                                    Text="Report By Student" AutoPostBack="True" />
                            </td>
                            <td></td>
                        </tr>
                   
                        <tr>
                            <td align="left" width="20%" valign="top"><span class="field-label">Group</span></td>
                          
                            <td align="left" valign="top" width="30%">
                                <asp:DropDownList ID="ddlGroup" runat="server">
                                </asp:DropDownList></td>
                        </tr>

                        <tr id="trstud1" runat="server">
                            <td align="left" width="20%" valign="top"><span class="field-label">Student</span></td>
                           
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS(); return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                                <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    PageSize="5"  OnPageIndexChanging="grdStudent_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stud. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DESCR" HeaderText="Student Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_new" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="text-align: center">
                                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                    <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="h_STU_IDs" runat="server" />
                    <asp:HiddenField ID="h_SBG_IDs" runat="server" />

                </div>
            </div>
        </div>
</asp:Content>

