<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGroupWiseStudents.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptGroupWiseStudents" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">

    function getSubject() {
        var sFeatures;
        sFeatures = "dialogWidth: 445px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        // url='../Students/ShowAcademicInfo.aspx?id='+mode;

        url = document.getElementById("<%=hfSubjectURL.ClientID %>").value


        //result = window.showModalDialog(url,"", sFeatures);
        var oWnd = radopen(url, "pop_rel");
        <%--if (result=='' || result==undefined)
            {    return false; 
            }   
             NameandCode = result.split('|');  
             document.getElementById("<%=txtSubject.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfSBG_ID.ClientID %>").value=NameandCode[1];
           
                       
                      
           }--%>
    }

     function OnClientClose(oWnd, args) {
          
         var arg = args.get_argument();

         if (arg) {
             NameandCode = arg.Result.split('|');             
             document.getElementById("<%=txtSubject.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfSBG_ID.ClientID %>").value=NameandCode[1];
               __doPostBack('<%= txtSubject.ClientID%>', 'TextChanged');
            }
        }

 

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        } 
          
 </script>   

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Teacher Wise Groups
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center"
        cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1">
                </asp:ValidationSummary>
                      </td>
        </tr>
        
        <tr>
            <td align="center">
                <table id="Table1" runat="server" align="center" width="100%"
                    cellpadding="5" cellspacing="0">
                   
                    <tr>
                        <td align="center" colspan="4">
                            <table id="Table4" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                 <tr>
                               
                                    <td  align="left" width="20%">
                                        <span class="field-label"> Academic Year</span></td>
                                    
                                    <td  align="left" width="30%">
                                        <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>    
                                         
                                         <td  align="left" width="20%">
                                            <span class="field-label">Grade</span></td>
                                    
                                        <td  align="Left" width="30%">
                                        <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td> 
                                      
                                </tr>
                                <tr>
                                  <td  align="left">
                                     <span class="field-label">Shift</span></td>
                                   
                                  <td  align="Left"  >
                                        <asp:DropDownList id="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>  
                                        
                                  <td  align="left"   >
                                     <span class="field-label">Stream</span></td>
                                  
                                  <td  align="Left"  >
                                        <asp:DropDownList id="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>  
                                </tr>
                                <tr>
                                  
                                           
                                         <td align="left" >
      <span class="field-label">Subject</span></td>
                                   
       <td align="left">
        <asp:TextBox id="txtSubject" runat="server" Enabled="False" MaxLength="100" tabIndex="2" OnTextChanged="txtSubject_TextChanged"
              ></asp:TextBox><asp:ImageButton id="imgSub" runat="server" ImageUrl="~/Images/forum_search.gif"
               OnClientClick="getSubject();return false;" tabIndex="18"></asp:ImageButton></td>
      
      
                                </tr>
                                 <tr>
                                      <td align="center" colspan="4">
                                         <table runat="server" id="tblGroup" width="100%">
                                          <tr>
                                              <td width="25%"></td>
                                            <td align="left" width="25%">
                                                
                                                 <asp:CheckBox id="chkAll" runat="server" CssClass="field-label" AutoPostBack="True" Text="Groups">
                                                 </asp:CheckBox>
                                               
                                            </td>
                                              <td></td>
                                              <td></td>
                                          </tr>
                                           <tr>
                                               <td width="25%"></td>
                                                <td align="left" width="25%">
                                                    <div  class="checkbox-list">
                                                        <asp:CheckBoxList id="lstGroup" runat="server" CssClass="field-label" RepeatLayout="Flow"  AutoPostBack="True">
                                                    </asp:CheckBoxList>
                                                        </div>
                                                </td>
                                               <td></td>
                                               <td></td>
                                            </tr>
                                        </table>
                                        </td>
                                        
                                   
                                </tr>
                                       </table>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="4" align="center">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Height="24px"
                                Text="Generate Report" ValidationGroup="groupM1" OnClick="btnGenerateReport_Click" />
                        </td>
                    </tr>
                </table>
               
                <input id="lstValues" runat="server" name="lstValues" style="left: 274px; width: 74px;
                    position: absolute; top: 161px; height: 10px" type="hidden" />
                <asp:HiddenField id="hfSBG_ID" runat="server" EnableViewState="False">
                </asp:HiddenField><asp:RequiredFieldValidator id="rfSub" runat="server" ControlToValidate="txtSubject"
                    Display="None" ErrorMessage="Please select a subject" ValidationGroup="groupM1">
    </asp:RequiredFieldValidator>
                <asp:HiddenField id="hfSubjectURL" runat="server">
                </asp:HiddenField>&nbsp;&nbsp;
            </td>
        </tr>
    </table>

        <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:radwindowmanager>

                </div>
            </div>
         </div>

</asp:Content>

