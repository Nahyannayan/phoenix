﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCommendation.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCommendation" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Card</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                  
                    <tr>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Report Schedule</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                  
                    <tr>
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Certification</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlCertificate" runat="server">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" /><asp:Button ID="btnDownload" runat="server" CssClass="button"
                                    Text="DownLoad Report in PDF" ValidationGroup="GR" /></td>
                    </tr>
                </table>


                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

