﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_rptKGComparison
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290370" And ViewState("MainMnu_code") <> "C290371") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                BindReportSchedule()
                BindSection()


                If rdYear.Checked = True Then
                    r6.Visible = False
                Else
                    r6.Visible = True
                End If

                If ViewState("MainMnu_code") = "C290371" Then
                    r5.Visible = False
                End If

                If ViewState("MainMnu_code") = "C290371" Then
                    r6.Visible = False
                Else
                    r6.Visible = True
                End If
            End If
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportSchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String

        If rdYear.Checked = True Then

            str_query = "SELECT DISTINCT ACY_DESCR AS RPF_DESCR,ACD_ID AS RPF_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B " _
                     & " ON B.ACD_ACY_ID=A.ACY_ID INNER JOIN RPT.REPORT_SETUP_M AS C ON B.ACD_ID=C.RSM_ACd_ID  " _
                     & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS D ON C.RSM_ID=D.RSG_RSM_ID " _
                     & " WHERE RSM_BSU_ID='" + Session("SBSUID") + "' AND RSG_GRD_ID IN('KG1','KG2','01','02')" _
                     & " ORDER BY ACY_DESCR"
        ElseIf rdTerm.Checked = True Then
            str_query = "SELECT  RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                                 & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                 & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                 & " AND RPF_DESCR IN('TERM 1 REPORT','TERM 2 REPORT')"
        ElseIf rdMonth.Checked = True Then
            str_query = "SELECT  RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                              & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                              & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                              & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                              & " AND RPF_DESCR NOT IN('TERM 1 REPORT','TERM 2 REPORT')"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstRpf.DataSource = ds
        lstRpf.DataTextField = "RPF_DESCR"
        lstRpf.DataValueField = "RPF_ID"
        lstRpf.DataBind()
    End Sub



    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + acdid


        str_query += " and grd_id in('kg1','kg2','01','02')"

        str_query += " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Function getRpfIds() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstRpf.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If lstRpf.SelectedIndex > 0 Then
                str = lstRpf.SelectedItem.Value
            End If
        End If
        Return str

    End Function


    'Function getRpfIds() As String
    '    Dim i As Integer
    '    Dim str As String
    '    For i = 0 To lstRpf.Items.Count - 1
    '        If lstRpf.Items(i).Selected = True Then
    '            If rdYear.Checked = True Then
    '                If str <> "" Then
    '                    str += "|"
    '                End If
    '                str += lstRpf.Items(i).Value.ToString
    '            ElseIf lstRpf.Items(i).Selected = True Then
    '                If str <> "" Then
    '                    str += "|"
    '                End If
    '                str += lstRpf.Items(i).Value.ToString
    '            End If
    '        End If
    '    Next
    '    Return str
    'End Function
    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
           " AND STU_SCT_ID in (" & vSCT_ID & ")"
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
     " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
     " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
     "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
     " AND STP_SCT_ID in (" & vSCT_ID & ")  "
        End If


        str_sql += " for xml path('')),1,0,''),0) "

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        Return str
    End Function
    Sub CallReport()
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim strSec As String
        Dim i As Integer
        If ViewState("MainMnu_code") <> "C290371" Then
            If h_STU_IDs.Value = "" Then
                If ddlSection.SelectedValue.ToString = "0" Then
                    For i = 1 To ddlSection.Items.Count - 1
                        If strSec <> "" Then
                            strSec += ","
                        End If
                        strSec = "'" + ddlSection.Items(i).Value.ToString + "'"
                    Next
                Else
                    strSec = ddlSection.SelectedValue.ToString
                End If
                h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAcademicYear.SelectedValue, grade(0), strSec)
            End If
        End If
        Dim param As New Hashtable
        Dim rptClass As New rptClass


        param.Add("@BSU_ID", Session("sbsuid"))
        If ViewState("MainMnu_code") <> "C290371" Then

            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@STU_IDS", h_STU_IDs.Value.Replace("___", "|"))
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            If rdYear.Checked = True Then
                param.Add("@ACD_IDS", getRpfIds)
                rptClass.reportPath = Server.MapPath("../Rpt/rptKGCOMAPRISON_YEAR.rpt")
            Else
                param.Add("@ACD_IDS", "0")
                param.Add("@RPF_IDS", getRpfIds())
                param.Add("@GRD_ID", grade(0))
                If rdTerm.Checked = True Then
                    param.Add("TYPE", "TERM")
                Else
                    param.Add("TYPE", "MONTHS")
                End If
                rptClass.reportPath = Server.MapPath("../Rpt/rptKGCOMAPRISON_REPORT.rpt")
            End If
        Else
            If rdYear.Checked = True Then
                param.Add("@GRD_ID", grade(0))
                param.Add("@ACD_IDS", getRpfIds())
                If chkGender.Checked = True Then
                    rptClass.reportPath = Server.MapPath("../Rpt/rptKGANALYSIS_YEAR_BYGENDER.rpt")
                Else
                    rptClass.reportPath = Server.MapPath("../Rpt/rptKGANALYSIS_YEAR.rpt")
                End If
            Else
                param.Add("@RPF_IDS", getRpfIds())
                param.Add("@GRD_ID", grade(0))
                param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
                param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                If chkCriteria.Checked = True Then
                    rptClass.reportPath = Server.MapPath("../Rpt/rptKGANALYSIS_TERM_BYGENDER_BYCRITERIA.rpt")
                ElseIf chkGender.Checked = True Then
                    rptClass.reportPath = Server.MapPath("../Rpt/rptKGANALYSIS_TERM_BYGENDER.rpt")
                Else
                    rptClass.reportPath = Server.MapPath("../Rpt/rptKGANALYSIS_TERM.rpt")
                End If

            End If
        End If
        rptClass.crDatabase = "oasis_curriculum"
        rptClass.reportParameters = param
        Session("rptClass") = rptClass

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If


    End Sub
#End Region
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindReportSchedule()
        BindSection()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindReportSchedule()
        BindSection()

    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub




    'Protected Sub chkRpf_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRpf.CheckedChanged
    '    Dim i As Integer
    '    For i = 0 To lstRpf.Items.Count - 1
    '        lstRpf.Items(i).Selected = chkRpf.Checked
    '    Next
    'End Sub

    Protected Sub rdYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdYear.CheckedChanged
        If rdYear.Checked = True Then
            If ViewState("MainMnu_code") = "C290371" Then
                tracc.Visible = False
            Else
                r6.Visible = False
            End If

            BindReportSchedule()



        End If
    End Sub

    Protected Sub rdTerm_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdTerm.CheckedChanged
        If rdTerm.Checked = True Then
            BindReportSchedule()
            r6.Visible = True
            tracc.Visible = True
        End If
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub rdMonth_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdMonth.CheckedChanged
        If rdMonth.Checked = True Then
            BindReportSchedule()
            r6.Visible = True
            tracc.Visible = True
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
