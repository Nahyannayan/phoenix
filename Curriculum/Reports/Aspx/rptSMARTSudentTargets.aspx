<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSMARTSudentTargets.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSMARTSudentTargets" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
        var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
        if (GRD_IDs == '') {
            alert('Please select atleast one Grade')
            return false;
        }


        var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=ALL", "RadWindow1");

    }

    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.NameCode.split('||');
            document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }




    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Progress Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">SMART Setup</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlTarget" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="20">
                        <td align="left" valign="middle"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="middle"><span class="field-label">Section</span>
                        </td>
                        <td align="left" colspan="1" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>

                        <tr id="trStudents" runat="server">
                            <td align="left" valign="top"><span class="field-label">Student</span></td>
                            <td align="left">
                                <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                            </td>
                            <td colspan="2">
                                <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stud. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_new" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr runat="server" id="trSubmitStatus">
                            <td align="left" valign="top"><span class="field-label">Status</span></td>
                            <td align="left" colspan="3" >
                                <asp:RadioButton ID="rdAll" runat="server"  CssClass="field-label" Checked="True" GroupName="G1" Text="All" />
                                <asp:RadioButton ID="rdSubmitted" runat="server" CssClass="field-label" GroupName="G1" Text="Submitted" />
                                <asp:RadioButton ID="rdNotSubmitted" runat="server" CssClass="field-label" GroupName="G1" Text="Not Submitted" /></td>

                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="text-align: center">
                                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" /> 
                <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                    Text="Download Report in Pdf" />
                            </td>
                        </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfReportType" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfReportFormat" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

