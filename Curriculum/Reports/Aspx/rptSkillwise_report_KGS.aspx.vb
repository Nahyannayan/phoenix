﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class Curriculum_Reports_Aspx_rptSkillwise_report_KGS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        '   ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300410") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))

                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))


                    BindReportType()
                    BindReportCardType()

                    BindReportPrintedFor()

                    GetAllGrade()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Private Function isPageExpired() As Boolean


        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)


        'for aks final report two fomats(one for ministry and one for school has to be issued
        If ddlReportType.SelectedItem.Text.Contains("FINAL") And Session("sbsuid") = "126008" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString
            str_sql += " UNION ALL  SELECT 0 ,'FINAL REPORT - MINISTRY' FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString

            '" AND ISNULL(RPF_bFINAL_REPORT ,0) = '" & bFINAL_REPORT & _
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)



            ddlReportPrintedFor.DataSource = ds
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        Else
            ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        End If

    End Sub

    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Sub BindReportCardType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_REPORTTYPE,''),ISNULL(RSM_REPORTFORMAT,'') FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + ddlReportType.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfReportType.Value = ds.Tables(0).Rows(0).Item(0)
        hfReportFormat.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportCardType()
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged

        BindReportType()
        BindReportCardType()

        BindReportPrintedFor()

        GetAllGrade()
        GetSectionForGrade()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
    Sub GetAllGrade()
        Dim str_Sql As String
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        If bSuperUsr = True Then
            str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
            " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
            " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAca_Year.SelectedValue.ToString & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue.ToString & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue.ToString & "'"
            If ViewState("GRD_ACCESS") > 0 Then
                str_Sql += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_ACD_ID=" + Session("Current_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                         & " UNION" _
                         & " SELECT SGR_GRD_ID FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID WHERE SGR_ACd_ID=" + ddlAca_Year.SelectedValue.ToString _
                         & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") + " AND SGS_TODATE IS NULL" _
                         & ")"
            End If

            str_Sql += "    ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
            " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
            " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAca_Year.SelectedValue.ToString & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue.ToString & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue.ToString & "'" & _
            " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EMPLOYEEID") & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_Sql)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        param.Add("@GRD_ID", grade(0))
        param.Add("@SCT_ID", ddlSection.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)

        Dim str As String = ""
        If ddlCriteria.SelectedValue = "between" Then
            str = ddlCriteria.SelectedValue + " " + txtVal1.Text + " and " + txtVal2.Text
        Else
            str = ddlCriteria.SelectedValue + " " + txtVal1.Text
        End If

        param.Add("@CRITERIA", str)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSkillwise_report_KGS.rpt")
        End With

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub



    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub ddlCriteria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCriteria.SelectedIndexChanged
        If ddlCriteria.SelectedValue = "between" Then
            txtVal2.Visible = True
            lblAnd.Visible = True
        Else
            txtVal2.Visible = False
            lblAnd.Visible = False
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
