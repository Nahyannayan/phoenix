<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSubjectPerformanceByTeacher.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSubjectPerformanceByTeacher" Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHeader" runat="server">Teacher Wise Groups</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" width="100%">
                                <tr>

                                    <td class="matters" align="left "  width="20%" ><span class="field-label">Academic Year</span></td>
                                    <td class="matters" align="left"  width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>&nbsp;</td>

                                    <td class="matters" align="left"  width="20%"><span class="field-label">Grade</span></td>
                                    <td class="matters" align="left" width="30%">
                                         <div class="checkbox-list">
                                        <asp:CheckBox ID="chkGrade" runat="server" Text="Select All" AutoPostBack="True" CssClass="field-label"></asp:CheckBox><br />
                                        <asp:CheckBoxList ID="lstGrade" runat="server"  
                                              RepeatLayout="Flow" 
                                             AutoPostBack="True">
                                        </asp:CheckBoxList>
                                             </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="matters" align="left"  ><span class="field-label">Subject</span></td>
                                    <td class="matters" align="left" >
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                    <td class="matters" align="left"  ><span class="field-label">Teacher</span></td>
                                    <td   class="matters"  >
                                        <div class="checkbox-list">
                                        <asp:CheckBox ID="chkTeacher" runat="server" AutoPostBack="True" Text="Select All" CssClass="field-label"></asp:CheckBox><br />
                                        <asp:CheckBoxList ID="lstTeacher" runat="server" 
                                    RepeatLayout="Flow" 
                                 AutoPostBack="True">
                                        </asp:CheckBoxList>
                                            </div>
                                    </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4"   valign="bottom" align="center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                Text="Generate Report" ValidationGroup="groupM1"   />
                        </td>
                    </tr>
                    </table>
                            <asp:HiddenField ID="hfTRV_ID" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfGrade" runat="server"></asp:HiddenField>

                    <input id="lstValues" runat="server" name="lstValues" 
                        type="hidden" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

