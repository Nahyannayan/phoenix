﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptReportAnalysisMark
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280321") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    BindReportType()
                    BindReportPrintedFor()
                    BindHeader()
                    GetAllGrade()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub
    Sub BindReportPrintedFor()
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub
    Sub BindHeader()
        Dim str_Sql As String = "SELECT RSD_ID, RSD_HEADER FROM RPT.REPORT_SETUP_D " & _
        " WHERE (ISNULL(RSD_bALLSUBJECTS, 0) = 1 AND ISNULL(RSD_bDIRECTENTRY, 0) = 0 " & _
        " OR ISNULL(RSD_bPERFORMANCE_INDICATOR, 0) = 1 )" & _
        " AND RSD_RSM_ID = " & ddlReportType.SelectedValue
        ddlHeader.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, _
         CommandType.Text, str_Sql)
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub
    Sub GetAllGrade()
        'Dim bSuperUsr As Boolean = False
        'If Session("CurrSuperUser") = "Y" Then
        '    bSuperUsr = True
        'End If
        Dim bSuperUsr As Boolean = True

        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

       

        GetStream()
        GetSectionForGrade()

        BindSubjects(ddlGrade.SelectedValue, ddlStream1.SelectedValue)

    End Sub
    Sub GetStream()
        Dim str_sql As String = "SELECT DISTINCT STM_ID, STM_DESCR FROM VW_GRADE_BSU_M INNER JOIN" & _
        " VW_STREAM_M ON VW_GRADE_BSU_M.GRM_STM_ID = VW_STREAM_M.STM_ID " & _
        " WHERE GRM_GRD_ID = '" & ddlGrade.SelectedValue & "'  AND GRM_ACD_ID = " & _
        ddlAca_Year.SelectedValue & " AND GRM_BSU_ID = '" & Session("sBSUID") & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
     

        ddlStream1.DataSource = ds
        ddlStream1.DataTextField = "STM_DESCR"
        ddlStream1.DataValueField = "STM_ID"
        ddlStream1.DataBind()


    End Sub
    Sub GetSectionForGrade()

        Dim bSuperUsr As Boolean = True
        ddlSection.DataSource = GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If

      
    End Sub
    Public Function GetSectionForGrade(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, Optional ByVal vEMP_ID As String = "", Optional ByVal bSuperUser As Boolean = True) As DataSet
        Dim str_Sql As String = String.Empty
        If bSuperUser Then
            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
            " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
            " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
            " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "') " _
            & " AND GRM_STM_ID = " & ddlStream1.SelectedValue & " order by SECTION_M.SCT_DESCR "
        Else
            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
            " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
            " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
            " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "') AND SECTION_M.SCT_EMP_ID = " & vEMP_ID _
            & " AND GRM_STM_ID = " & ddlStream1.SelectedValue & " order by SECTION_M.SCT_DESCR "
        End If

        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
    End Function
    Public Sub BindSubjects(ByVal GRD_ID As String, ByVal STM_ID As String)
        Dim str_sql As String = "SELECT SBG_ID, SBG_DESCR FROM SUBJECTS_GRADE_S " & _
        " WHERE SBG_PARENT_ID = 0  AND SBG_bMAJOR = 1 AND SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
        " AND SBG_ACD_ID = " & ddlAca_Year.SelectedValue & " AND SBG_GRD_ID ='" & GRD_ID & "' " & _
        " and SBG_STM_ID =" & STM_ID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
        chkListSubjects.DataSource = ds
        chkListSubjects.DataTextField = "SBG_DESCR"
        chkListSubjects.DataValueField = "SBG_ID"
        chkListSubjects.DataBind()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        Callreport()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindReportType()
        BindReportPrintedFor()
        BindHeader()
        GetAllGrade()
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()
        BindHeader()
        GetAllGrade()
    End Sub

    Protected Sub ddlReportPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportPrintedFor.SelectedIndexChanged
        BindHeader()
        GetAllGrade()
    End Sub

    Protected Sub ddlHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHeader.SelectedIndexChanged
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetStream()
        GetSectionForGrade()

        BindSubjects(ddlGrade.SelectedValue, ddlStream1.SelectedValue)
    End Sub

    Protected Sub ddlStream1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream1.SelectedIndexChanged
        GetSectionForGrade()

        BindSubjects(ddlGrade.SelectedValue, ddlStream1.SelectedValue)
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        BindSubjects(ddlGrade.SelectedValue, ddlStream1.SelectedValue)
    End Sub
    

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        Callreport()
    End Sub
    Private Function GetSelectedSubjects() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In chkListSubjects.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Value
            End If
        Next
        Return SBG_IDs
    End Function
    Sub Callreport()
        Dim param As New Hashtable
        'param.Add("@IMG_BSU_ID", Session("SBSUID"))
        'param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@STM_ID", "0")
        param.Add("@SBG_ID", GetSelectedSubjects())
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If rdList.Items(0).Selected Then
                .reportPath = Server.MapPath("../Rpt/rptSubjectAnalysisBynationalityacrossYear.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptSubjectAnalysisBynationalityacrossYearLevel.rpt")
            End If

        End With

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
