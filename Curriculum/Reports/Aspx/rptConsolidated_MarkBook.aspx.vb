﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Imports UtilityObj
Partial Class Curriculum_ConsolidatedReports_rptConsolidated_MarkBook
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim studClass As New studClass

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If USR_NAME = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()

                    BindPrintedFor()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    'ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    If ddlGroup.SelectedItem Is Nothing Then
                        h_SBG_ID.Value = ""
                    Else
                        h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
                    End If


                    'ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

                    BindSubjects()
                    BindGroups()
                    populateexamtype()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If

    End Sub
    Sub BindSubjects()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'ddlSubject.Items.Clear()
        lstSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID SBG_SBM_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SBG_GRD_ID='" + grade(0) + "'" _
                              & " AND SBG_STM_ID=" + grade(1) _
                              & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'ddlSubject.DataSource = ds
        'ddlSubject.DataTextField = "SBG_DESCR"
        'ddlSubject.DataValueField = "SBG_SBM_ID"
        'ddlSubject.DataBind()

        lstSubject.DataSource = ds
        lstSubject.DataTextField = "SBG_DESCR"
        lstSubject.DataValueField = "SBG_SBM_ID"
        lstSubject.DataBind()

    End Sub
    Sub BindGroups()
        ddlGroup.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim gradeCount As Integer

        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If


        If ViewState("GRD_ACCESS") > 0 Then
            str_query = " select COUNT(DISTINCT SCT_GRD_ID) FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_aCD_ID=" + ddlAcademicYear.SelectedValue + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & " AND SCT_GRD_ID='" + grade(0) + "'"


            gradeCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            gradeCount = 1
        End If


        If bSuperUsr = True Then
            gradeCount = 1
        Else
            gradeCount = 0
        End If




        If gradeCount = 0 Then
            PopulateGroupsByTeacher()
        Else



            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M " _
                                  & " INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                                  & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND SGR_GRD_ID='" + grade(0) + "'" _
                                  & "AND SBG_STM_ID=" + grade(1) _
                                  & " AND SBG_ID in (SELECT * FROM OASISFIN..FNSPLITME('" + GetSelectedSubjects() + "','|'))" _
                                  & " ORDER BY SGR_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlGroup.DataSource = ds
            ddlGroup.DataTextField = "SGR_DESCR"
            ddlGroup.DataValueField = "SGR_ID"
            ddlGroup.DataBind()

            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlGroup.Items.Insert(0, li)
        End If
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Sub PopulateGroupsByTeacher()
        ddlGroup.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SGS_EMP_ID=" + Session("EmployeeID") _
                                & " AND SGS_TODATE IS NULL"

        str_query += " AND SGR_SBG_ID in (SELECT * FROM OASISFIN..FNSPLITME('" + GetSelectedSubjects() + "','|'))"


        If grade(0) <> "" Then
            str_query += " AND SGR_GRD_ID='" + grade(0) + "'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

    End Sub

    Private Function GetSelectedSubjects() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In lstSubject.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Value
            End If
        Next
        Return SBG_IDs
    End Function
    Protected Sub populateexamtype()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "GET_EXTERNAL_EXAM_TYPES")
        If Not ds Is Nothing Then
            If ds.Tables.Count > 0 Then
                ddlExamtpe.DataSource = ds.Tables(0)
                ddlExamtpe.DataTextField = "VALUE"
                ddlExamtpe.DataValueField = "VALUE"
                ddlExamtpe.DataBind()
            End If
        End If
    End Sub
    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")
        Dim currFns As New currFunctions
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim gradeCount As Integer
        If ViewState("GRD_ACCESS") > 0 Then
            str_query = " select COUNT(DISTINCT SCT_GRD_ID) FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_aCD_ID=" + ddlAcademicYear.SelectedValue + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & " AND SCT_GRD_ID='" + grd_id.Split("|")(0) + "'"


            gradeCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            gradeCount = 1
        End If

        If gradeCount = 0 Then
            Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
            ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), lstSubject.SelectedValue.ToString)
            Return ddlGroup
        End If

        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + acd_id

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.Split("|")(0) + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        ddlGroup.Items.Add(New ListItem("ALL", "0"))
        ddlGroup.Items.FindByText("ALL").Selected = True
        Return ddlGroup
    End Function

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlGroup.SelectedIndex = -1 Then
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If
    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id
        If ViewState("MainMnu_code") = "C300008" Then
            'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
            str_query += "  AND SBG_PARENT_ID=0"

        End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        ddlSubject.Items.Add(New ListItem("ALL", "0"))
        ddlSubject.Items.FindByText("ALL").Selected = True
        Return ddlSubject
    End Function


    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                  & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                              & " order by grd_displayorder"
        ' 




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub
    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnGenReport_Click(sender As Object, e As EventArgs)
        '    If chkDtl.Checked = False And chkGraph.Checked = False Then
        ' lblError.Text = "Please select any of the type of report"
        'Exit Sub
        '  End If
        Dim reportScheduleParam As String = ""
        For Each ListItem1 As ListItem In ddlPrintedFor.Items
            If ListItem1.Selected = True Then
                If reportScheduleParam = "" Then
                    reportScheduleParam = ListItem1.Value
                Else
                    reportScheduleParam = reportScheduleParam + "|" + ListItem1.Value
                End If
            End If
        Next

        Dim externalExamTypes As String = ""
        For Each listitem2 As ListItem In ddlExamtpe.Items
            If listitem2.Selected = True Then
                If externalExamTypes = "" Then
                    externalExamTypes = listitem2.Value
                Else
                    externalExamTypes = externalExamTypes + "|" + listitem2.Value
                End If
            End If
        Next


        Dim sbg_ids As String = ""
        For Each listitem3 As ListItem In lstSubject.Items
            If listitem3.Selected = True Then
                If sbg_ids = "" Then
                    sbg_ids = listitem3.Value
                Else
                    sbg_ids = sbg_ids + "|" + listitem3.Value
                End If
            End If
        Next


        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0))
        param.Add("@RPF_ID", reportScheduleParam)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue)
        param.Add("@SBG_IDs", sbg_ids)
        param.Add("@SGR_ID", ddlGroup.SelectedValue)
        param.Add("@EXTRNL_EXAMTYPE", externalExamTypes)

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptConsolidatedMarkbook.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
   

    Protected Sub ddlReportCard_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindPrintedFor()
        BindSubjects()
        BindGroups()
        'ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ' ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindReportCard()

        BindPrintedFor()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        'ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        BindSubjects()
        If ddlGroup.SelectedItem Is Nothing Then
            h_SBG_ID.Value = ""
        Else
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If

        BindGroups()
        ' ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        'ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        'ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        BindSubjects()
        BindGroups()
    End Sub
    Protected Sub chkListSubjects_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindGroups()
    End Sub
End Class
