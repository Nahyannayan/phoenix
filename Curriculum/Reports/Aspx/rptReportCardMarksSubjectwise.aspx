<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReportCardMarksSubjectwise.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptReportCardMarksSubjectwise" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
      
      function openWin() {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
                                                
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.Students.split('|');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }
        

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
      
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
   </script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label id="lblHeader" runat="server" Text="Report Card Marks"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
    <table align="center" width="100%">
                   
                    <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Report Type</span></td>
                        
                        <td align="left" width="25%">
                            <asp:DropDownList id="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
        <tr>
            <td align="left" valign="middle">
                <span class="field-label">Report Printed For</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlReportPrintedFor" runat="server" OnSelectedIndexChanged="ddlReportPrintedFor_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged1">
                </asp:DropDownList></td>
            <td align="left" valign="middle">
                            <span class="field-label">Section</span></td>
           
            <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList></td>
        </tr>
                    <tr id ="trSubject" runat="server" >
                        <td align="left" valign="middle">
                            <span class="field-label">Subject</span></td>
                       
                        <td align="left" valign="middle">
                            <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr runat="server" id="trStudent">
                        <td align="left" valign="top" >
                            <span class="field-label">Student</span></td>
                        
                        <td align="left" colspan="10">
                            <asp:TextBox id="txtStudIDs" runat="server"></asp:TextBox>
                            <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click">
                            </asp:ImageButton>
                            <asp:GridView id="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="100%"  OnPageIndexChanging="grdStudent_PageIndexChanging" CssClass="table table-bordered table-row">
                                <columns>
<asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
<asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
</columns>
                                <headerstyle cssclass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
        <tr>
            <td align="left" colspan="12" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" class="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button id="btnCancel" runat="server" class="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
                </table>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField id="h_SUBJIDs" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SCTIDs" runat="server">
    </asp:HiddenField>

        </div>
    </div>
</div>

</asp:Content>

