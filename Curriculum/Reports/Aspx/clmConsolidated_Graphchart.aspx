﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="clmConsolidated_Graphchart.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_Graphchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblError" CssClass="error" Text ="" runat="server"></asp:Label>
                <table id="tblReport" runat="server" width="100%">


                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <span class="field-label">Report Card</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportCard_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Report Schedule</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <span class="field-label">Grade</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="trSubject" runat="server">
                        <td align="left" width="20%">
                            <span class="field-label">Subject</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged"></asp:DropDownList>
                            <%-- <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="lstSubject" runat="server" RepeatLayout="Flow">
                                </asp:CheckBoxList>
                            </div>--%>
                        </td>
                        <td>
                            <span class="field-label">Group</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Gender </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlgender" runat="server">
                                <asp:ListItem Text="All" Value="A" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>                                
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Nationality</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddNationality" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label ">SEN</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlsen" runat="server" OnSelectedIndexChanged="ddlsen_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="All" Value="-1" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <asp:RadioButtonList ID="radwv" runat="server" Visible="false" RepeatDirection ="Horizontal"> 
                                 <asp:ListItem text="Wave1" value="1"></asp:ListItem>
                                 <asp:ListItem text="Wave2" value="2"></asp:ListItem>
                                 <asp:ListItem text="Wave3" value="3"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label"> Enrollment From Date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEnrollDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton2" TargetControlID="txtEnrollDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label"> To Date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEnrollToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton1" TargetControlID="txtEnrollToDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:CheckBox ID="chkDtl" runat="server" Text="Detailed" OnCheckedChanged="chkDtl_CheckedChanged" AutoPostBack="true" CssClass="field-text"/>
                            <asp:CheckBox ID="chkGraph" runat="server" Text="Graph" Checked="true" CssClass="field-text" OnCheckedChanged="chkGraph_CheckedChanged" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%" id="tdheader1" runat="server">
                                        <span class="field-label ">Header</span>
                                    </td>
                                    <td align="left" width="30%" id="tdheader2" runat="server">
                                        <asp:DropDownList ID="ddlheader" runat="server" OnSelectedIndexChanged="ddlheader_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                     <td align="left"  id="tdcompare1" runat="server" visible="false">
                                        <span class="field-label ">Compare</span>
                                    </td>
                                    <td align="left"  id="tdcompare2" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlcompare" runat="server"></asp:DropDownList>
                                    </td>
                                    <td id="tdCalc1" runat="server" visible="false" >
                                        <span class="field-label ">Calc</span>
                                    </td>
                                    <td id="tdcalc2" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlcalc" runat="server">
                                            <asp:ListItem Text="Below At Greater" Value="5" Selected="true"></asp:ListItem>
                                            <asp:ListItem Text="Equal To" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Greater Than" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Less than" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Greater Than Or Equal To" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Less Than Or Equal To" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" id="tdval1" runat="server" >
                                        <span class="field-label">Value</span>
                                    </td>
                                    <td align="left" id="tdval2" runat="server">
                                        <asp:TextBox ID="txtValue" runat="server" ></asp:TextBox>
                                          <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                                ValidChars="." TargetControlID="txtValue" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            
                            <asp:Button ID="btnGenReport" runat="server" OnClick="btnGenReport_Click" CssClass="button" Text="Generate Report" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SBG_ID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>
