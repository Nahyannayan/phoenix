Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim rs As New ReportDocument

    Private Function isPageExpired() As Boolean


        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        '   ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT) _
                AndAlso (ViewState("MainMnu_code") <> "C400005") AndAlso (ViewState("MainMnu_code") <> "C300555") AndAlso (ViewState("MainMnu_code") <> "StudentProfile") AndAlso (ViewState("MainMnu_code") <> "PdfReport") _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

                    If ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
                        BindProfileReport(sender, e)
                    Else
                        BindReportType()
                        BindReportCardType()
                        If (ViewState("MainMnu_code") = "C400005") Then
                            BindReportPrintedFor(True)
                        Else
                            BindReportPrintedFor()
                        End If
                        GetAllGrade()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub


    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function


    Sub BindProfileReport(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rsmId As String = Encr_decrData.Decrypt(Request.QueryString("rsmId").Replace(" ", "+"))
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT RSM_ACD_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + rsmId + "'"
        Dim acdId As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        h_STU_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("StuId").Replace(" ", "+"))
        Dim grdId As String
        If acdId <> Session("Current_ACD_ID") Then
            str_query = "SELECT STP_GRD_ID FROM OASIS..STUDENT_PROMO_S WHERE STP_STU_ID='" + h_STU_IDs.Value + "'" _
                      & " AND STP_ACD_ID='" + acdId + "'"
            grdId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            grdId = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
        End If
        ' Dim acdId As String = Encr_decrData.Decrypt(Request.QueryString("acdId").Replace(" ", "+"))
        Dim rpfId As String = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))


        If Not ddlAca_Year.Items.FindByValue(acdId) Is Nothing Then
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(acdId).Selected = True
            ddlAca_Year_SelectedIndexChanged(sender, e)
        End If
        If Not ddlReportType.Items.FindByValue(rsmId) Is Nothing Then
            ddlReportType.ClearSelection()
            ddlReportType.Items.FindByValue(rsmId).Selected = True
            ddlReportType_SelectedIndexChanged(sender, e)
        End If
        If Not ddlReportPrintedFor.Items.FindByValue(rpfId) Is Nothing Then
            ddlReportPrintedFor.ClearSelection()
            ddlReportPrintedFor.Items.FindByValue(rpfId).Selected = True
        End If
        If Not ddlGrade.Items.FindByValue(grdId) Is Nothing Then
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByValue(grdId).Selected = True
        End If
        BindReportCardType()
        btnGenerateReport_Click(sender, e)
    End Sub

    Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)


        'for aks final report two fomats(one for ministry and one for school has to be issued
        If ddlReportType.SelectedItem.Text.Contains("FINAL") And Session("sbsuid") = "126008" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString
            str_sql += " UNION ALL  SELECT 0 ,'FINAL REPORT - MINISTRY' FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString

            '" AND ISNULL(RPF_bFINAL_REPORT ,0) = '" & bFINAL_REPORT & _
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)



            ddlReportPrintedFor.DataSource = ds
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        Else
            ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        End If

    End Sub

    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Sub BindReportCardType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_REPORTTYPE,''),ISNULL(RSM_REPORTFORMAT,'') FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + ddlReportType.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfReportType.Value = ds.Tables(0).Rows(0).Item(0)
        hfReportFormat.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    'Sub GetAllGrade()
    '    Dim bSuperUsr As Boolean = False
    '    If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
    '        bSuperUsr = True
    '    End If
    '    If Session("sbsuid") = "125017" Then
    '        bSuperUsr = True
    '    End If
    '    ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
    '    ddlGrade.DataTextField = "GRM_DISPLAY"
    '    ddlGrade.DataValueField = "GRD_ID"
    '    ddlGrade.DataBind()
    '    GetSectionForGrade()
    'End Sub

    Sub GetAllGrade()
        Dim str_Sql As String
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        If bSuperUsr = True Then
            str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
            " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
            " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAca_Year.SelectedValue.ToString & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue.ToString & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue.ToString & "'"
            If ViewState("GRD_ACCESS") > 0 Then
                str_Sql += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_ACD_ID=" + Session("Current_ACD_ID") + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                         & " UNION" _
                         & " SELECT SGR_GRD_ID FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID WHERE SGR_ACd_ID=" + ddlAca_Year.SelectedValue.ToString _
                         & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") + " AND SGS_TODATE IS NULL" _
                         & ")"
            End If

            str_Sql += "    ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
            " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
            " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
            " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
            " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
            " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
            " INNER JOIN VW_SECTION_M ON SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
            " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & ddlAca_Year.SelectedValue.ToString & "') " & _
            " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & ddlReportType.SelectedValue.ToString & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & ddlReportPrintedFor.SelectedValue.ToString & "'" & _
            " AND VW_SECTION_M.SCT_EMP_ID = " & Session("EMPLOYEEID") & _
            " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_Sql)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hfbDownload.Value = 0
        CallReports()
    End Sub

    Private Function GetAllCBSEBusinessUnit(ByVal BSU_ID As String) As String
        Dim strQuery As String = " SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_CLM_ID = 1 AND BSU_ID = '" & BSU_ID & "'"
        Dim vBSU_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Return vBSU_ID
    End Function

    Private Sub GenerateTermTestReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If Session("sBSUID") = "151001" Then
                param.Add("RPT_ASSESSMENT_HEADER1", "Test 1")
                param.Add("RPT_ASSESSMENT_HEADER2", "Test 2")
                .reportPath = Server.MapPath("../Rpt/rptCBSE_OOW_UNIT_TEST_REPORT_11_12.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And Session("SBSUID") = "131001" Then
                If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "FIRST TERM TEST" Then
                    param.Add("RPT_ASSESSMENT_HEADER1", "Test 1(Apr) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(May) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(June) out of 20")
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12.rpt")
                Else
                    param.Add("RPT_ASSESSMENT_HEADER1", "Test 1(Nov) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(Dec) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(Jan) out of 20")
                    param.Add("RPT_ASSESSMENT_HEADER4", "Test 3(Feb) out of 10")
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12_term2.rpt")
                End If
            ElseIf Session("sBSUID") = "121014" And ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                param.Add("RPT_ASSESSMENT_HEADER1", "Test 1")
                param.Add("RPT_ASSESSMENT_HEADER2", "Test 2")
                .reportPath = Server.MapPath("../Rpt/rptCBSE_OOW_UNIT_TEST_REPORT_11_12.rpt")
            ElseIf hfReportFormat.Value = "FORMAT2" Then
                param.Add("RPT_ASSESSMENT_HEADER", "Marks out of 25")
                .reportPath = Server.MapPath("../Rpt/rptCBSE_UNIT_TEST_REPORT_11_12_FORMAT2.rpt")
            Else
                param.Add("RPT_ASSESSMENT_HEADER", "Marks out of 25")
                .reportPath = Server.MapPath("../Rpt/rptCBSE_UNIT_TEST_REPORT_11_12.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEFormativeAssessmentReport_11_12()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case GetReportType()
                Case "GRADE_11_12_OOW"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_OOW.rpt")
                Case "GRADE_11_12_OOW_12"
                    If ddlReportPrintedFor.SelectedValue = 10653 Then

                        .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_09_12_OOW.rpt")
                    Else
                        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                        param.Add("UNIT_TEST_HEADING", "Unit Test")
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD12_OOW.rpt")
                    End If
                   
                Case "GRADE_11_12_FORMAT_OOW"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT1_OOW.rpt")
                Case "GRADE_11_12_FORMAT1"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT1.rpt")
                Case "GRADE_11_12_FORMAT_NMS"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    If ((ddlReportPrintedFor.SelectedValue = 10627) Or (ddlReportPrintedFor.SelectedValue = 10632)) Then
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT_NMS_2020_21.rpt")
                    ElseIf (ddlReportPrintedFor.SelectedValue = 10633) Then
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT_NMS_21.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT_NMS.rpt")
                    End If

                Case "GRADE_11_12_FORMAT2"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT2.rpt")
                Case "GRADE_11_12_FORMAT3", "GRADE_11_12_FORMAT7"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT3.rpt")
                Case "GRADE_11_12_FORMAT4"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT4.rpt")
                Case "GRADE_11_12_FORMAT5"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT5.rpt")
                Case "GRADE_11_12_FORMAT6"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT6.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                'ReportLoadSelection()
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateMLSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
       

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            Select Case hfReportFormat.Value

                Case "MLS_KG_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue
                        Case "PK"
                            .reportPath = Server.MapPath("../Rpt/MLS/rptMLS_ProgressReport_PK.rpt")
                        Case "KG1"
                            .reportPath = Server.MapPath("../Rpt/MLS/rptMLS_ProgressReport_KG1.rpt")
                        Case "KG2"
                            .reportPath = Server.MapPath("../Rpt/MLS/rptMLS_ProgressReport_KG2.rpt")
                    End Select
                Case "MLS_1"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_1.rpt")
                Case "MLS_2"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_2.rpt")
                Case "MLS_P"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_Primary.rpt")
                Case "MLS_S"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_Secondary.rpt")
                Case "MLS_T1"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_Term1.rpt")
                Case Else
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MLS/rptREPORT_MLS_3.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub Generate_CBSE_AssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            'Select GetReportType()
            'Case "ASSESSMENT_REPORT_11_NMS"

            '.reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_11_NMS.rpt")
            'Case Else
            If ddlAca_Year.SelectedValue = 1777 Then

                If ddlReportPrintedFor.SelectedValue = 10627 Then
                    GenerateCBSEFormativeAssessmentReport_11_12()
                ElseIf ddlReportPrintedFor.SelectedValue = 10630 Then
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_10.rpt")
                Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_10_NMS.rpt")
                End If
            ElseIf ddlAca_Year.SelectedValue = 1774 Then
                If ((ddlGrade.SelectedValue = "09") Or (ddlGrade.SelectedValue = "10")) Then

                    If ddlReportPrintedFor.SelectedValue = 10651 Then
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_10.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_09_12_OOW.rpt")
                    End If
                Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_08_OOW.rpt")
                End If
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_10.rpt")
            End If

                'End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_RLP_AssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            'Select GetReportType()
            'Case "ASSESSMENT_REPORT_11_NMS"

            '.reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_11_NMS.rpt")
            'Case Else
            If ddlAca_Year.SelectedValue = 1776 Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_RLP_TMS.rpt")
            Else

                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_RLP.rpt")
            End If
            'End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub Generate_GHS_AssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            'Select GetReportType()
            'Case "ASSESSMENT_REPORT_11_NMS"

            '.reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_11_NMS.rpt")
            'Case Else
           

            .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_GHS_01.rpt")

            'End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_TMS_AssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            'Select GetReportType()
            'Case "ASSESSMENT_REPORT_11_NMS"

            '.reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_11_NMS.rpt")
            'Case Else
            If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("SA1") Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_TMS_SA.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_TMS.rpt")
            End If

            'End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_AssessmentReport_NMS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            'Select GetReportType()
            'Case "ASSESSMENT_REPORT_11_NMS"
            If ddlGrade.SelectedValue = "11" Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_11_NMS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_01_10.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateFormativeAssessmentReport()
        If ((Session("SBSUID") = "121012") And (ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM")) And (ddlAca_Year.SelectedValue >= 1276)) Then
            Generate_CBSE_TermReport01_04()
        End If
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Select Case ddlReportPrintedFor.SelectedItem.Text
            Case "FORMATIVE ASSESSMENT 1"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT 1")

        End Select


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            Select Case hfReportFormat.Value
                Case "FORMATIVE_ASSESSMENT_GEP"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_GEP.rpt")
                Case "ASSESSMENT_GEP"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_ASSESSMENT_REPORT_GEP.rpt")
                Case "ASSESSMENT_KGS"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") Then
                        Generate_CBSE_TermReport01_04()
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
                    End If

                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCumulativeReport_OOEHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("TERM1_HEADING", "TERM I (20%)")
        param.Add("TERM2_HEADING", "TERM II (30%)")
        param.Add("TERM3_HEADING", "TERM III (50%)")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case ddlGrade.SelectedValue
                'Case "11", "12"
                '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TWS_GRD11-12.rpt")
                'Case "04"
                '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD04.rpt")
                Case "07"
                    '.reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TWS_DUBAI_GRD09.rpt")
                    .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD05_08.rpt")
                    'Case "09"
                    '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD09.rpt")
                    'Case "10"
                    '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD10.rpt")
                    'Case "01", "02", "03"
                    '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD01-03.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateFinalReport_TWS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)


        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param


            Select Case hfReportFormat.Value
                Case "FINAL_REPORTS_15-16"
                    .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06_2015-16.rpt")

                Case "FINAL_REPORTS_19-20"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06_19_20.rpt")
                        Case "07", "08"
                            param.Add("ATTEND_TOTAL", "")
                            .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD07_08_19_20.rpt")
                        Case "09", "10"
                            param.Add("ATTEND_TOTAL", "")
                            .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_19_20.rpt")
                    End Select

                Case Else
                    Select Case ddlGrade.SelectedValue

                        Case "01", "02", "03", "04", "05", "06"
                            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                            End If
                        Case "07", "08"
                            param.Add("ATTEND_TOTAL", "")
                            If ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD07_08_2009-2010.rpt")
                            ElseIf ((ddlAca_Year.SelectedItem.Text = "2014-2015") Or (ddlAca_Year.SelectedItem.Text = "2015-2016") Or (ddlAca_Year.SelectedItem.Text = "2016-2017") Or (ddlAca_Year.SelectedItem.Text = "2017-2018") Or (ddlAca_Year.SelectedItem.Text = "2018-2019")) Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2014-15.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD07_08.rpt")
                            End If
                        Case Else
                            param.Add("ATTEND_TOTAL", "")
                            If ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2009-2010.rpt")
                            ElseIf ddlAca_Year.SelectedItem.Text = "2014-2015" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2014-15.rpt")
                            ElseIf ((ddlAca_Year.SelectedItem.Text = "2015-2016") Or (ddlAca_Year.SelectedItem.Text = "2016-2017")) Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2014-15.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10.rpt")

                            End If

                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function

    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        If Session("Current_ACD_ID") = ddlAca_Year.SelectedValue.ToString Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
           " AND STU_SCT_ID in (" & vSCT_ID & ")"
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
     " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
     " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
     "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
     " AND STP_SCT_ID in (" & vSCT_ID & ")  "
        End If

        If ddlGender.SelectedValue <> "ALL" Then
            str_sql += " AND STU_GENDER='" + ddlGender.SelectedValue + "'"
        End If

        If chkSen.Checked = True Then
            str_sql += " AND ISNULL(STU_bSEN_KHDA,0)=1"
        End If
        str_sql += " for xml path('')),1,0,''),0) "

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        Return str
    End Function

    Private Sub GenerateTermlyReport_TWS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        If ddlGrade.SelectedValue <> "KG1" And ddlGrade.SelectedValue <> "KG2" Then
            param.Add("ATTEND_TOTAL", "0")
        Else
            param.Add("GRD_ID", ddlGrade.SelectedValue)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportType.Value

                Case "TWS_TERMLY_REPORT_2020"
                    Select Case ddlGrade.SelectedValue
                        
                        Case "01", "02", "03", "04", "05", "06", "07", "08"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_8__2020.rpt")
                        Case "09", "10"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09_10__2020.rpt")
                        Case "11"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11_13__2020.rpt")
                        Case "12", "13"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11_13__2020.rpt")
                    End Select

                Case "TWS_TERMLY_REPORT_2019"
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptKinderGarten_TWS_2019.rpt")
                        Case "01"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_2019.rpt")
                        Case "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD02_6__2019.rpt")
                        Case "07", "08"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD07_8__2019.rpt")
                        Case "09", "10"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09_10__2019.rpt")
                        Case "11"
                            If ddlReportPrintedFor.SelectedItem.Text = "FIRST TERM REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11_13__2019.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11_13_2019_term2.rpt")
                            End If
                        Case "12", "13"
                            If ddlReportPrintedFor.SelectedItem.Text = "FIRST TERM REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD12__2019.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11_13_2019_term2.rpt")
                            End If

                    End Select
                Case "TWS_INTERIM_REPORT_APP"
                    param.Add("@bSLT", "false")
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .Photos = GetPhotoClass()
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTWS_PROGRESSREPORT_APP_FS1-FS2.rpt")
                        Case "09", "10", "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTWS_INTERIMREPORT_9-10.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTWS_INTERIMREPORT_APP_1-8.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2016"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2016.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2017"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2017.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2018"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2018.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2014"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2014.rpt")
                        Case "07", "08"
                            If ddlReportPrintedFor.SelectedItem.Text = "SECOND TERM REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD07_08.rpt")
                            End If

                        Case "09", "10", "11"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2015"
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2014.rpt")
                        Case "07", "08"

                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")


                        Case "09", "10", "11"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2012"
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTWS_PROGRESSREPORT_FS1_FS2.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                        Case "07", "08"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD07_08.rpt")
                        Case "09"
                            param("Max_Mark_UT") = "25"
                            param("Max_Mark_Exam") = "75"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD09_10.rpt")
                        Case "10"
                            param("Max_Mark_UT") = "20"
                            param("Max_Mark_Exam") = "80"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD09_10.rpt")
                        Case "11"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD11.rpt")
                        Case "12", "13"
                            If ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM2_REPORT_TWS_DUBAI_GRD12_13.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD12_13.rpt")
                            End If
                    End Select
                Case Else
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTWS_PROGRESSREPORT_FS1_FS2.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                            End If
                        Case "07", "08"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD07.rpt")
                        Case "09"
                            param("Max_Mark_UT") = "25"
                            param("Max_Mark_Exam") = "75"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09.rpt")
                        Case "10"
                            param("Max_Mark_UT") = "20"
                            param("Max_Mark_Exam") = "80"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09.rpt")
                        Case "11"
                            .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11.rpt")
                        Case "12", "13"
                            If ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM2_REPORT_TWS_DUBAI_GRD12_13.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD12_13.rpt")
                            End If
                    End Select

            End Select

            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")\
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateFinalReport_DMHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If ddlAca_Year.SelectedValue = 1053 Then

                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_07_2015-16.rpt")
                    Case "08"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD08_2015-16.rpt")
                    Case "09", "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "10"

                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")

                    Case "12"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                End Select
            ElseIf ddlAca_Year.SelectedValue = 1097 Then
                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_07_2016_17.rpt")
                    Case "08", "09", "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            ElseIf ddlAca_Year.SelectedValue = 1268 Then
                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_2017-18.rpt")
                    Case "06", "07", "08", "09"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD11_2017-18.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            ElseIf ((ddlAca_Year.SelectedValue = 1363)) Then
                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_2017-18.rpt")
                    Case "06", "07", "08", "09", "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                        'Case "11"
                        '    .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD11_2017-18.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            ElseIf ((ddlAca_Year.SelectedValue = 1365)) Then
                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_2019-20.rpt")
                    Case "06", "07", "08", "09"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2019-20.rpt")
                    Case "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                        'Case "11"
                        '    .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD11_2017-18.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            Else

                Select Case ddlGrade.SelectedValue
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07", "08"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_06.rpt")
                    Case "09", "11"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11.rpt")
                    Case "10"
                        If ddlAca_Year.SelectedValue <= 962 Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                        End If
                    Case "12"
                        If ((ddlAca_Year.SelectedValue = 1097) Or (ddlAca_Year.SelectedValue = 1268)) Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                        End If

                End Select
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateTermlyReport_DMHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        '    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())

        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "CA_REPORTS_05_09"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptCA_REPORT_DMHS_DUBAI_GRD05_09.rpt")
                Case "TERM_REPORTS_05_08_NEW"
                    If ((ddlAca_Year.SelectedItem.Value = "1053") Or (ddlAca_Year.SelectedItem.Value = "1097") Or (ddlAca_Year.SelectedItem.Value = "1268") Or (ddlAca_Year.SelectedItem.Value = "1363") Or (ddlAca_Year.SelectedItem.Value = "1365")) Then
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2015-16.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08.rpt")
                    End If
                Case "TERM_REPORTS_01_05_2020"
                    If ((ddlGrade.SelectedValue = "04") Or (ddlGrade.SelectedValue = "05")) Then
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD04_05_20120_21.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD01_03_20120_21.rpt")
                    End If

                Case "TERM_REPORTS_05_12_2020"

                    If ((ddlGrade.SelectedValue = "12") And (ddlReportPrintedFor.SelectedItem.Text.ToUpper = "MID YEAR REPORT")) Then
                        param("RPT_CAPTION") = "TERM 1 REPORT"
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                    ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 2 REPORT" Then
                        If ddlGrade.SelectedValue = "09" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_2020-21.rpt")
                        ElseIf ddlGrade.SelectedValue = "05" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_2020 -21.rpt")
                        ElseIf ddlGrade.SelectedValue = "10" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12_2015-16.rpt")
                        ElseIf ddlGrade.SelectedValue = "12" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD06_08_2020-21.rpt")
                        End If
                    Else
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_12_2020_21.rpt")
                    End If
                Case "TERM_REPORTS_04_2020"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD04_2020_21.rpt")
                Case "TERM_REPORTS_01_03_2020"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD01_03 _20120_21.rpt")
                Case "TERM_REPORTS_05_NEW"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2016-17.rpt")
                Case "TERM_REPORTS_05_07_NEW_TERM2"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_07_2015-16_TERM2.rpt")
                Case "TERMREPORT_01_02_2016"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD01_04 _2015-16.rpt")
                Case "TERM_REPORTS_09_12_NEW"
                    If ((ddlAca_Year.SelectedItem.Value = "1053") Or (ddlAca_Year.SelectedItem.Value = "1097") Or (ddlAca_Year.SelectedItem.Value = "1268") Or (ddlAca_Year.SelectedItem.Value = "1363") Or (ddlAca_Year.SelectedItem.Value = "1365")) Then
                        If ddlGrade.SelectedValue = "11" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                        ElseIf ddlGrade.SelectedValue = "09" Then
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12_2015-16.rpt")
                        ElseIf ddlGrade.SelectedValue = "10" Then
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 3 REPORT" Then
                                param("RPT_CAPTION") = "PRILIMINARY REPORT"
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12_2015-16.rpt")
                            End If

                        ElseIf ddlGrade.SelectedValue = "12" Then
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 3 REPORT" Then
                                param("RPT_CAPTION") = "PRILIMINARY REPORT"
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                            End If
                        End If

                    Else
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                    End If
                Case "TERM_REPORTS_11_12_NEW"
                    If ddlGrade.SelectedValue = "12" Then
                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 3 REPORT" Then
                            param("RPT_CAPTION") = "PRILIMINARY REPORT"
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                        End If
                    Else
                        .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                    End If
                Case "TERM_REPORTS_11_NEW"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2016-17.rpt")

                    ''added by nahyan on 27nov2016
                Case "TERM_REPORTS_05_07_NEW_TERM2_2017"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_07_2016-17_TERM2.rpt")
                Case Else
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04"
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                        Case "05", "06", "07", "08"
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2012-13.rpt")
                        Case "09", "11"
                            .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                        Case "10", "12"
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper().Contains("TERM 3") Then
                                param("RPT_CAPTION") = "PRELIMINARY REPORT"
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper().Contains("FINAL") Then
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                            End If
                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateMidTermReport_DMHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value.ToUpper
                Case "KG_MIDTERMREPORT"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptMID_TERM_REPORT_DMHS_KG.rpt")
                Case "DMHS_BRIDGEREPORT"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHS_IB_BRIDGEREPORT.rpt")
                Case "DMHS_BRIDGEREPORT_2"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHS_IB_BRIDGEREPORT_2.rpt")
                Case "IB_MIDTERMREPORT"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHS_IB_MIDTERMFEEDBACKREPORT.rpt")
                Case "IB_TERMREPORT"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHS_IB_TERM1.rpt")
                Case "MTR_2018"
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHSMidTermReviewReport_2018-19.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHSMidTermReviewReport.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEMidTermReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case ddlGrade.SelectedValue
                Case "KG1", "KG2"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_MIDTERMFEEDBACKREPORT_KG.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_MIDTERMFEEDBACKREPORT.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateJCReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("RPT_CAPTION", ddlReportType.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportType.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportType.Value
                Case "JC_INTERIM_2012"
                    If ViewState("MainMnu_code") = "C300555" Then
                        param.Add("@bSLT", "true")
                    Else
                        param.Add("@bSLT", "false")
                    End If
                    Select Case ddlGrade.SelectedValue
                        Case "07", "08", "09"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2012-13_GRD07-09.rpt")
                        Case "10"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2012-13_GRD10.rpt")
                        Case "12"
                            If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2010_GRD12.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2011_GRD12.rpt")
                            End If
                    End Select
                Case "JC_PROGRESS_2012"
                    If ViewState("MainMnu_code") = "C300555" Then
                        param.Add("@bSLT", "true")
                    Else
                        param.Add("@bSLT", "false")
                    End If
                    Select Case ddlGrade.SelectedValue
                        Case "07", "08"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012-13_GRD07_08.rpt")
                        Case "09"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012_GRD07_08.rpt")
                        Case "10", "11"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012-13_GRD10-11.rpt")
                        Case "12"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012-13_GRD12.rpt")
                        Case "13"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012-13_GRD13.rpt")
                    End Select
                Case Else

                    Select Case GetReportType()
                        Case "JC_INTERIM_REPORT"
                            Select Case ddlGrade.SelectedValue
                                Case "07"
                                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2010_GRD07.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2011_GRD07-09.rpt")
                                    End If
                                Case "08", "09"
                                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2010_GRD08_09.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2011_GRD07-09.rpt")
                                    End If
                                Case "10"
                                    .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2010_GRD10.rpt")
                                Case "12"
                                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2010_GRD12.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_INTERIM_REPORT_2011_GRD12.rpt")
                                    End If
                            End Select
                        Case "JC_PROGRESS_REPORT"
                            Select Case ddlGrade.SelectedValue
                                Case "07", "08"
                                    .reportPath = Server.MapPath("../Rpt/rptJC_PROGRESS_REPORT_2010_GRD07_09.rpt")
                                Case "09"
                                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2010_GRD07_09.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012_GRD09.rpt")
                                    End If
                                Case "10", "11"
                                    .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2010_GRD11.rpt")
                                Case "12"
                                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2010_GRD12.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2012_GRD12.rpt")
                                    End If
                                Case "13"
                                    If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2011_GRD13.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/JC/rptJC_PROGRESS_REPORT_2010_GRD13.rpt")
                                    End If
                            End Select
                            '.reportPath = Server.MapPath("../Rpt/rptJC_PROGRESS_REPORT.rpt")
                        Case "JC_FINAL_REPORT_2010_2011"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_07_08_10_11_2011.rpt")
                        Case "JC_FINAL_REPORT_09_2010_2011"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_09_2011.rpt")
                        Case "JC_FINAL_REPORT_12_2010_2011"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_12_2011.rpt")
                        Case "JC_FINAL_REPORT"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT.rpt")
                        Case "JC_FINAL_REPORT_10"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_10.rpt")
                        Case "JC_FINAL_REPORT_11"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_11.rpt")
                        Case "JC_FINAL_REPORT_13_2012"
                            .reportPath = Server.MapPath("../Rpt/JC/rptJC_FINAL_REPORT_13_2012.rpt")

                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateAISReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value.ToUpper
                Case "AIS_FSREPORT"
                    .reportPath = Server.MapPath("../Rpt/AIS/rptAIS_PastoralReport_KG1_KG2.rpt")
                Case "AIS_PASTORAL_01-06"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue)
                    .reportPath = Server.MapPath("../Rpt/AIS/rptAIS_PastoralReport_01_06.rpt")
                Case "AIS_PASTORAL_07-13"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue)
                    .reportPath = Server.MapPath("../Rpt/AIS/rptAIS_PastoralReport_07_13.rpt")
            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWinProgressReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "WIN_REPORTS_FS_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_FS_2020.rpt")
                Case "WIN_REPORTS_13_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_13_2020.rpt")
                Case "WIN_REPORTS_01_10"
                    .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                Case "WIN_REPORTS_10_13"
                    .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_10_13_2014-15.rpt")
                Case Else
                    Select Case ddlGrade.SelectedValue.ToString
                        Case "KG1"
                            .Photos = GetPhotoClass()
                            param.Add("@bSLT", "false")
                            .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_APP_FS1_FS2.rpt")
                        Case "KG2"
                            .Photos = GetPhotoClass()
                            param.Add("@bSLT", "false")
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL REPORT") Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_FS2_TERM3.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_APP_FS1_FS2.rpt")
                            End If
                        Case "01", "02", "03", "04", "05", "06"
                            If ddlReportPrintedFor.SelectedValue = 4655 Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                            ElseIf ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_01_06_2012-13.rpt")
                            End If
                        Case "07", "08", "09"
                            If ddlReportPrintedFor.SelectedValue = 3672 Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_07_10.rpt")
                            End If
                        Case "10"
                            If ddlReportPrintedFor.SelectedValue = 3672 Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_10_2013-14.rpt")
                            ElseIf ddlAca_Year.SelectedItem.Text = "2010-2011" Or ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_07_10.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_10.rpt")
                            End If
                        Case "11", "12", "13"
                            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_11_13_2011.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIN/rptWIN_PROGRESSREPORT_11_13.rpt")
                            End If

                    End Select
            End Select
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateOOLReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "OOL_IGCSE_PROGRESSREPORTS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("../Rpt/OOL/rptOOL_ProgressReport_2015-16.rpt")
                Case "OOL_IGCSE_FINALREPORT"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("../Rpt/OOL/rptOOL_FinalReport_2015-16.rpt")

            End Select
            .reportParameters = param
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCHSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "CHS_MIDTERM_KG"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT.rpt")
                Case "CHS_MIDTERM_KG_2015"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT_2015-16.rpt")
                Case "CHS_MIDTERM_KG_2016"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT_2016-17.rpt")
                Case "CHS_MIDTERM"
                    param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue.ToString
                        Case "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_11-13.rpt")
                        Case Else
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport.rpt")
                    End Select
                Case "CHS_MIDTERM_2015"
                    param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue.ToString
                        Case "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_11-13_2015-16.rpt")
                        Case Else
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_2015-16.rpt")
                    End Select
                Case "CHS_MIDTERM_2016"
                    param.Add("grd_id", ddlGrade.SelectedValue.ToString)

                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_2016-17.rpt")

                Case "CHS_SETTLING"
                    param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_SettlingReport.rpt")
                Case "CHS_FINAL"
                    param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue.ToString
                        Case "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport_11-13.rpt")
                        Case "KG1", "KG2"
                            param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                            param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT.rpt")
                        Case Else
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport1-10_2011-12.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport1-10.rpt")
                            End If
                    End Select

                Case Else
                    If ddlReportType.SelectedItem.Text.Contains("MIDYEAR ASSESSMENT") Then
                        param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                        Select Case ddlGrade.SelectedValue.ToString
                            Case "11", "12", "13"
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_11-13_2013-14.rpt")
                            Case Else
                                param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                param.Add("@IMG_TYPE", "LOGO")
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_MidYearReport_2013-14.rpt")
                        End Select
                    ElseIf ddlReportType.SelectedItem.Text.Contains("SETTLING") Then
                        param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                        param.Add("@IMG_BSU_ID", Session("SBSUID"))
                        param.Add("@IMG_TYPE", "LOGO")
                        .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_SettlingReport_2013-14.rpt")
                    ElseIf ddlReportType.SelectedItem.Text.Contains("FINAL") Then
                        param.Add("grd_id", ddlGrade.SelectedValue.ToString)
                        Select Case ddlGrade.SelectedValue.ToString
                            Case "11", "12", "13"
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport_11-13_2013-14.rpt")
                            Case "KG1", "KG2"
                                param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                                param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                                .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT_2013-14.rpt")
                            Case Else
                                param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                param.Add("@IMG_TYPE", "LOGO")
                                If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport1-10_2011-12.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_FinalReport1-10_2013-14.rpt")
                                End If

                        End Select
                    ElseIf ddlReportType.SelectedItem.Text.Contains("KINDERGARTEN") Then
                        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                        .reportPath = Server.MapPath("../Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT.rpt")
                    End If
            End Select
            .reportParameters = param
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWSOReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()

            Select Case hfReportFormat.Value
                Case "FS_INTERIMREPORT"
                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_FS_INTERIMREPORT.rpt")
                Case "INTERIMREPORT_01_06"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "OCTOBER INTERIM REPORT" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "FEBRUARY INTERIM REPORT" Then
                        .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09_withprojected.rpt")
                    End If
                Case "INTERIMREPORT_07_10_12"
                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09_withprojected.rpt")
                Case "INTERIMREPORT_07_12"
                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_7-12.rpt")
                Case "WSO_FULLREPORT"
                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSOFullReport.rpt")
                Case "WSO_PASTORALREPORT"
                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSOPastoralReport.rpt")
                Case Else
                    If ddlReportType.SelectedItem.Text.Contains("INTERIM REPORT FS1-FS2") Then
                        .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_FS_INTERIMREPORT_2011-12.rpt")
                    ElseIf ddlReportType.SelectedItem.Text.Contains("FINALREPORT FS1-FS2") Then
                        If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                            .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_FS_FINALREPORT_2011-12.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_FS_FINALREPORT.rpt")
                        End If
                    ElseIf ddlReportType.SelectedItem.Text.Contains("INTERIM REPORT I-IX") Or ddlReportType.SelectedItem.Text.Contains("INTERIM REPORT I-X") Then
                        Select Case ddlGrade.SelectedValue
                            Case "01", "02", "03", "04", "05", "06"
                                If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "OCTOBER INTERIM REPORT" Then
                                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09_2012-13.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09_withprojected_2012-13.rpt")
                                End If

                            Case Else
                                .reportPath = Server.MapPath("../Rpt/WSO/rptWSO_PROGRESS_REPORT_GRD_01-09_withprojected.rpt")
                        End Select
                    ElseIf ddlReportType.SelectedItem.Text.Contains("FINAL") Then
                        If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                            .reportPath = Server.MapPath("../Rpt/WSO/rptWSOFinalReport_2011-2012.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/WSO/rptWSOFinalReport.rpt")
                        End If

                    End If
            End Select
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateKSBReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        ' param.Add("GRD_ID", ddlGrade.SelectedValue)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            If ddlReportType.SelectedItem.Text.Contains("STUDENT PROGERSS REPORT") Then
                .reportPath = Server.MapPath("../Rpt/rptKSB_StudentProgressReport.rpt")
            End If

            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWSDReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "WSD_INTERIM_REPORT_APP"
                    param.Add("@bSLT", "false")
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .Photos = GetPhotoClass()
                            .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_PROGRESSREPORT_APP_FS1-FS2.rpt")
                    End Select
                Case "WSD_FINAL_REPORT_APP"
                    param.Add("@bSLT", "false")
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_FINALREPORT_APP_FS2.rpt")
                Case "PROGRESS_REPORTS_01_05"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_PROGRESSREPORT_01_06_2012-13.rpt")
                Case "PROGRESS_REPORTS_2012"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_TERMREPORT_01_06_2012.rpt")
                Case "PROGRESS_REPORTS_2016"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_TERMREPORT_01_10.rpt")
                Case "PROGRESS_REPORTS"
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            param.Add("GRD_ID", ddlGrade.SelectedValue)
                            param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_PROGRESSREPORT_FS1_FS2.rpt")
                        Case "07", "08", "09", "10"
                            param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_PROGRESSREPORT_06_10.rpt")
                        Case "06"
                            If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                param.Add("GRD_ID", ddlGrade.SelectedValue)
                                param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                                .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_TERMREPORT_01_06.rpt")
                            Else
                                param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                                .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_PROGRESSREPORT_06_10.rpt")
                            End If
                        Case Else
                            param.Add("GRD_ID", ddlGrade.SelectedValue)
                            param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_TERMREPORT_01_06.rpt")
                    End Select
                Case Else
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSD/rptWSD_FinalReport.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWSAReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORTS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_PROGRESSREPORT_01_06.rpt")
                Case "PROGRESS_REPORTS_2015"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_2015-16.rpt")
                Case "PROGRESS_REPORTS_2016"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    If ddlReportPrintedFor.SelectedItem.Text.Contains("END OF YEAR REPORT") Then
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_ENDOFYEARREPORT_2016-17.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_2016-17.rpt")
                    End If
                Case "PROGRESS_REPORTS_2017"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)

                    .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_ENDOFYEARREPORT_2016-17.rpt")

                Case "PROGRESS_REPORTS_TERM2"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_Term2.rpt")
                Case "PROGRESS_REPORTS_2014"
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    param.Add("CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_new.rpt")
                Case "PROGRESS_REPORTS_2014_TERM2"
                    param.Add("@bSLT", "0")
                    If ddlGrade.SelectedValue = "KG1" Or ddlGrade.SelectedValue = "KG2" Then
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_INTERIMREPORT_APP_FS1_FS2_Term2.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_INTERIMREPORT_APP_01-06_Term2.rpt")
                    End If
                Case "PROGRESS_REPORTS_2014_TERM3"
                    param.Add("@bSLT", "0")
                    If ddlGrade.SelectedValue = "KG1" Or ddlGrade.SelectedValue = "KG2" Then
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_INTERIMREPORT_APP_FS1_FS2_Term3.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSA/rptWSA_INTERIMREPORT_APP_01-06_Term3.rpt")
                    End If
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCINReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORTS_2015"
                    Select Case ddlGrade.SelectedValue
                        Case "07", "08", "09"
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_PROGRESS_07_09.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_PROGRESS_10_13.rpt")
                    End Select
                Case "PROGRESS_REPORTS"
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_FS_ProgressReport.rpt")
                        Case Else
                            param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_ENDOFTERMREPORT.rpt")
                    End Select
                Case "ENDOFYEAR_REPORTS"
                    Select Case ddlGrade.SelectedValue
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_Finalreport_KG1_KG2.rpt")
                        Case "10", "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_Finalreport_10-12.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/CIN/rptCIN_Finalreport_01_09.rpt")
                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateBIMReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value

                Case "BIM_REPORT_2019"

                    .reportPath = Server.MapPath("../Rpt/BIM/rptBIM_ProgressReport.rpt")
                Case "BIM_REPORT_2019_T2"

                    .reportPath = Server.MapPath("../Rpt/BIM/rptBIM_ProgressReport_term2.rpt")
                Case "BIM_REPORT_2019_P"
                    .reportPath = Server.MapPath("../Rpt/BIM/rptBIM_ProgressReport_Primary.rpt")
                Case "BIM_REPORT_P_T1"
                    .reportPath = Server.MapPath("../Rpt/BIM/rptBIM_ProgressReport_Primary_term1.rpt")
                Case "BIM_REPORT_T1"
                    .reportPath = Server.MapPath("../Rpt/BIM/rptBIM_ProgressReport_term1.rpt")
            End Select

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateTBSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value

                Case "TBS_REPORT_2019"

                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport.rpt")
                Case "TBS_REPORT_2019_T3"

                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport_term3.rpt")
                Case "TBS_REPORT_PRIMARY"

                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport_Primary.rpt")
                Case "TBS_REPORT_FS_T1"
                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport_FS_term1.rpt")
                Case "TBS_REPORT_PRIMARY_T1"
                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport_Primary_term1.rpt")
                Case "TBS_REPORT_T1"
                    .reportPath = Server.MapPath("../Rpt/TBS/rptTBS_ProgressReport_term1.rpt")
            End Select

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateMILReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value

                Case "MIL_REPORT_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue
                        Case "PK"
                            .reportPath = Server.MapPath("../Rpt/MIL/rptMIL_ProgressReport_PK.rpt")
                        Case "KG1"
                            .reportPath = Server.MapPath("../Rpt/MIL/rptMIL_ProgressReport_KG1.rpt")
                        Case "KG2"
                            .reportPath = Server.MapPath("../Rpt/MIL/rptMIL_ProgressReport_KG2.rpt")
                    End Select
                Case "MIL_T1"
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    .reportPath = Server.MapPath("../Rpt/MIL/rptREPORT_MIL_Term1.rpt")
            End Select

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateGFMReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "GFM_REPORT_2019"

                    .reportPath = Server.MapPath("../Rpt/GFM/rptGFM_ProgressReport.rpt")
                Case "GFM_FINAL_REPORT_2019"

                    .reportPath = Server.MapPath("../Rpt/GFM/rptGFM_ProgressReport_Final.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateNSGReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "NSG_REPORT_1_6"
                    .reportPath = Server.MapPath("../Rpt/NSG/rptNSG_ProgressReport_Final_1_6.rpt")
                Case "NSG_REPORT_7_10"
                    .reportPath = Server.MapPath("../Rpt/NSG/rptNSG_ProgressReport_Final_7_10.rpt")
                Case "NSG_REPORT_11"
                    .reportPath = Server.MapPath("../Rpt/NSG/rptNSG_ProgressReport_Final_11.rpt")
                Case "NSG_REPORT_FS"
                    .reportPath = Server.MapPath("../Rpt/NSG/rptNSG_ProgressReport_Final_FS.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCIKReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "CIK_TERM2A_SECONDARY"
                    .reportPath = Server.MapPath("../Rpt/CIK/rptCIKSecondaryDataReportTerm2a.rpt")
                Case "CIK_TERM2A_SECONDARY_Y10"
                    .reportPath = Server.MapPath("../Rpt/CIK/rptCIKSecondaryDataReportTerm2a_Y10.rpt")
                Case "CIK_TERM2A_SECONDARY_Y13"
                    .reportPath = Server.MapPath("../Rpt/CIK/rptCIKSecondaryDataReportTerm2a_Y11_12_13.rpt")
                Case "CIK_FINAL_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CIK/rptCIKFinalReport_07_09.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateBENCHMARKReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/DMHS/rptDMHS_BENCHMARKREPORT.rpt")

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateBehaviiourReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptMOE_BEHAVIOUR_TERMREPORT.rpt")

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateRDSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "RDS_FINAL_FS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/RDS/rptRDS_FinalReport_FS_19_20.rpt")
                Case "RDS_FINAL_1_6"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/RDS/rptRDS_FinalReport_1_6_19_20.rpt")
                Case "RDS_MIDTERM"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/RDS/rptRDS_MidtermReport_KS2_2020.rpt")
                Case "RDS_TERM_FS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/RDS/rptRDS_TermReport_FS.rpt")
                Case "RDS_TERM_1_6"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/RDS/rptRDS_TermReport_1_6.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
   
    Private Sub GenerateProgressTrackerReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
          
            .reportPath = Server.MapPath("../Rpt/rptProgressTracker_Parent.rpt")


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateGIPReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "GIP_TERM_1"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_TermReport_2020_21.rpt")
                Case "GIP_TERM_FS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_TermReport_FS.rpt")
                Case "GIP_TERM_1_9"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_TermReport_1_9.rpt")
                Case "GIP_TERM_10_13"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_TermReport_10_13.rpt")
                Case "GIP_FINAL_FS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_FinalReport_FS_19-20.rpt")
                Case "GIP_FINAL_1_6"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_FinalReport_1_6_19-20.rpt")
                Case "GIP_FINAL_7_9"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_FinalReport_7_9_19-20.rpt")
                Case "GIP_FINAL_10_12"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_FinalReport_10_12_19-20.rpt")
                Case "GIP_TERMREPORT_1_6"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_ProgressReport_1_6.rpt")
                Case "GIP_TERMREPORT_7_10"
                    .reportPath = Server.MapPath("../Rpt/GIP/rptGIP_ProgressReport_7_10.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateISPReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value

                Case "ISP_PROGRESSREPORT_KG"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    Select Case ddlGrade.SelectedValue
                        Case "PK"
                            .reportPath = Server.MapPath("../Rpt/ISP/rptISP_FS_ProgressReport.rpt")
                        Case Else
                            '    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/ISP/rptISP_KG_ProgressReport.rpt")
                    End Select
                Case "ISP_PROGRESSREPORT_1_2"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/ISP/rptISP_ProgressReport_1_2.rpt")
                Case "ISP_TERMREPORT_GRADE3-4"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    ' param.Add("@IMG_TYPE", "LOGO")
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("UserName", Session("sUsr_name"))
                    param.Add("SA_HEADER", "SA")
                    param.Add("FA1_PERC", "10%")
                    param.Add("FA2_PERC", "10%")
                    param.Add("SA_PERC", "20%")
                    param.Add("TOTAL_PERC", "60%")
                    param.Add("TOTAL_FA_PERC", "20%")

                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                        param.Add("FA_HEADER1", "FA1")
                        param.Add("FA_HEADER2", "FA2")
                    Else
                        param.Add("FA_HEADER1", "FA3")
                        param.Add("FA_HEADER2", "FA4")
                    End If
                    .reportPath = Server.MapPath("../Rpt/ISP/rptISP_TERMREPORT_03_04.rpt")
                Case "ISP_FINALREPORT_GRADE1-2"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    ' param.Add("@IMG_TYPE", "LOGO")
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

                    param.Add("UserName", Session("sUsr_name"))
                    param.Add("SA_HEADER", "SA")
                    param.Add("FA1_PERC", "10%")
                    param.Add("FA2_PERC", "10%")
                    param.Add("SA_PERC", "20%")
                    param.Add("TOTAL_PERC", "60%")
                    param.Add("TOTAL_FA_PERC", "20%")

                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                        param.Add("FA_HEADER1", "FA1")
                        param.Add("FA_HEADER2", "FA2")
                    Else
                        param.Add("FA_HEADER1", "FA3")
                        param.Add("FA_HEADER2", "FA4")
                    End If
                    .reportPath = Server.MapPath("../Rpt/ISP/rptISP_FINALREPORT_01_02.rpt")
                Case "ISP_UNITTEST_11_12"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/ISP/rptISPUnitTest11_12.rpt")
                Case "ISP_TERMEXAM_11_12"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/ISP/ISP_TermReport_11_12.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '      Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateISPFinalReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateNMSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "NMS_TERMREPORT"
                    .reportPath = Server.MapPath("../Rpt/NMS/rptNMS_PROGRESSREPORT_01_06.rpt")
                Case "NMS_TERMREPORT_2014"
                    .reportPath = Server.MapPath("../Rpt/NMS/rptNMS_PROGRESSREPORT_01_06_2014.rpt")
                Case "NMS_STRATEGIESREPORT"
                    .reportPath = Server.MapPath("../Rpt/NMS/rptNMS_STRATEGIESREPORT_01_06.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWSSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "INTERIMREPORT_6_8"
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORT06-08_20_21.rpt")
                Case "INTERIMREPORT_2_5"
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORT02-05_20_21.rpt")
                Case "INTERIMREPORT_KG2_1"
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORTKG2-01_20_21.rpt")
                Case "INTERIMREPORT_KG1"
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORTKG1_20_21.rpt")
                Case "FS_REPORTS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT_TERM1.rpt")
                Case "FS_REPORTS_TERM2"
                    .Photos = GetPhotoClass()
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)

                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT.rpt")

                Case "FS_REPORTS_TERM2_NEW"
                    .Photos = GetPhotoClass()
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)

                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT_KG2_TERM2.rpt")

                    'Case "FS_REPORTS_TERM2"
                    '    .Photos = GetPhotoClass()
                    '    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    '    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    '    .reportPath = Server.MapPath("../Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_FS_13-14.rpt")
                Case "FS2_FINALREPORT"
                    .Photos = GetPhotoClass()
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_KINDERGARTENFINALREPORT.rpt")
                Case "FS_MNISTRY"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_FS_13-14.rpt")
                Case "FINAL_REPORT_1-9"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    If ((ddlGrade.SelectedItem.Text = "09") Or (ddlGrade.SelectedItem.Text = "10")) Then
                        .reportPath = Server.MapPath("../Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_Grade09_13-14.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_13-14.rpt")
                    End If
                Case "MOE_REPORT_SHARJAH_FS"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/rptMOEReportFormat_KG.rpt")
                Case "MOE_REPORT_SHARJAH"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/rptMOEReportFormat.rpt")
                Case "PROGRESS_REPORTS"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_PROGRESSREPORT_02_06.rpt")
                Case "PROGRESS_REPORTS_1-5"
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_PROGRESSREPORT_01_05.rpt")
                Case "PROGRESS_REPORTS_6-9"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_PROGRESSREPORT_06_09.rpt")
                Case "PROGRESS_REPORTS_APP"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORT_APP_1-8.rpt")
                Case "PROGRESS_REPORTS_APP_09_13"
                    param.Add("@bSLT", "false")
                    param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
                    param.Add("ATTEND_TOTAL", "0")
                    .reportPath = Server.MapPath("../Rpt/WSS/rptWSS_INTERIMREPORT_9-10.rpt")
                Case "FINAL_REPORT"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY.rpt")
                Case "TERM_1"
                    param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSS/rptTERMREPORT_WSS_2020_21.rpt")
            End Select

            .reportParameters = param
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateWGPReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value
                Case "MIDTERM_PRIMARY"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WGP/rptWGP_MidTerm_2020-21.rpt")
                

            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateWSRReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value
                Case "TERM_KG1"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_KGTerm_2020-21.rpt")
                Case "TERM_PRIMARY"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_primaryTerm_2020-21.rpt")
                Case "TERM_SECONDARY"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_SecondaryTerm_2020_21.rpt")
                Case "MIDTERM_PRIMARY"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_primaryMidTerm_2020-21.rpt")
                Case "MIDTERM_SECONDARY"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_SecondaryMidTerm_2020_21.rpt")
                Case "FS_REPORTS_2013-14"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_KINDERGARTENPROGRESSREPORT_2013-14.rpt")
                Case "FS_REPORTS"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_KINDERGARTENPROGRESSREPORT.rpt")
                Case "PROGRESS_REPORTS"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    If ddlAca_Year.SelectedItem.Text = "2014-2015" Then
                        If ddlReportPrintedFor.SelectedItem.Text = "FINAL REPORT" Then
                            .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2014-15.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07.rpt")
                        End If
                    ElseIf ddlAca_Year.SelectedItem.Text = "2015-2016" Then
                        .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2015-16.rpt")
                    ElseIf ddlAca_Year.SelectedItem.Text = "2016-2017" Then
                        .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2016-17.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07.rpt")
                    End If
                Case "PROGRESS_REPORTS_18"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2016-17.rpt")
                Case "FS1_FINALREPORT"
                    .Photos = GetPhotoClass()
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_FS1_FINALREPORT.rpt")
                Case "FS2_FINALREPORT"
                    .Photos = GetPhotoClass()
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_FS2FINALREPORT.rpt")

                Case "INTERIM_REPORTS"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("../Rpt/WSR/rptWSR_INTERIMREPORT_APP_1-8.rpt")

            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateWISReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORT"
                    Select Case ddlGrade.SelectedValue
                        Case "10", "11", "12", "13"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWISStudentProgressReport_10_13.rpt")
                        Case "07", "08", "09"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWISStudentProgressReport_07_09.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWIS_StudentProgressReport_01_06.rpt")
                    End Select
                Case "FINAL_REPORT"
                    .Photos = GetPhotoClass()
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWIS_FinalRFeport_01_06.rpt")
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWIS_FinalRFeport_FS1_FS2.rpt")
                    End Select
                Case Else
                    Select Case ddlGrade.SelectedValue
                        Case "11"
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SNAPSHOT REPORT 1" Then
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y11.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y11_withpredicted.rpt")
                            End If
                        Case "12"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y13.rpt")
                        Case "13"
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "FINAL REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISFinalReport_Y13.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y13.rpt")
                            End If
                        Case "07"
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "FINAL REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISFinalReport_Y7.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y08.rpt")
                            End If
                        Case "08", "09"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y08.rpt")
                        Case "10"
                            .reportPath = Server.MapPath("../Rpt/WIS/rptWISSnapShotReport_Y10.rpt")
                    End Select
            End Select
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCIAReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORT"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    Select Case ddlGrade.SelectedValue
                        Case "01", "02", "03", "04"
                            .reportPath = Server.MapPath("../Rpt/CIA/rptCIA_StudentProgressReport_01_04.rpt")
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/CIA/rptCIA_StudentProgressReport_FS1_FS2.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/CIA/rptCIA_StudentProgressReport_05_08.rpt")
                    End Select
                Case "FINAL_REPORT"
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/CIA/rptCIA_FinalReport_01_08.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateMTSReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "MTS_PRIMARY_T1"
                    If ((ddlGrade.SelectedValue = "KG1") Or (ddlGrade.SelectedValue = "KG2")) Then
                        .reportPath = Server.MapPath("../Rpt/MTS/rptMTSProgressReport_FS_term1.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/MTS/rptMTSProgressReport_Primary_term1.rpt")
                    End If
                Case "MTS_KS4_T1"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTSProgressReport_KS3_KS4_term1.rpt")
                Case "MTS_KS5_T1"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTSProgressReport_KS5_term1.rpt")
                Case "MTS_PRIMARY_2020"
                    .reportPath = Server.MapPath("../Rpt/MTS/Primary_2020_Report.rpt")
                Case "MTS_KS5_2020"
                    .reportPath = Server.MapPath("../Rpt/MTS/KS5_2020_Report.rpt")
                Case "MTS_KS3_KS4_2020"
                    .reportPath = Server.MapPath("../Rpt/MTS/KS3_KS4_2020_Report.rpt")
                Case "MTS_FS_2020"
                    .reportPath = Server.MapPath("../Rpt/MTS/FS_2020_Report.rpt")
                Case "MTS_MOCK"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTS_MockExam.rpt")
                Case "MTS_PROGRESS REPORT_01_05"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTS_StudentProgressReport_01_05.rpt")
                Case "MTS_PROGRESS REPORT_06_08"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTS_StudentProgressReport_06_08.rpt")
                Case "MTS_PROGRESS REPORT_11_13"
                    .reportPath = Server.MapPath("../Rpt/MTS/rptMTS_StudentProgressReport_11_13.rpt")

            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    'Private Sub GenerateCISProgressReport()
    '    Dim param As New Hashtable
    '    param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
    '    param.Add("@BSU_ID", Session("SBSUID"))
    '    param.Add("@RSM_ID", ddlReportType.SelectedValue)
    '    param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
    '    param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = "oasis_curriculum"
    '        .reportParameters = param
    '        .Photos = GetPhotoClass()
    '        Select Case hfReportFormat.Value
    '            Case "CIS_MONTHLY_REPORT" 'Monthly Progress Report
    '                .reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESS_REPORT_GRD07.rpt")
    '            Case "CIS_FINAL_REPORT_FS1_FS2" ' Final Report
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_FINAL_REPORT_FS1_FS2.rpt")
    '            Case "CIS_FINAL_REPORT_01_08"
    '                param.Add("@bSLT", "false")
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_InterimReprot_APP_1-8.rpt")
    '            Case "CIS_FINAL_REPORT_01_08_OLD"
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT.rpt")
    '            Case "CIS_PROGRESS_REPORT_FS1_FS2"
    '                param.Add("GRD_ID", ddlGrade.SelectedValue)
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT_FS1_FS2.rpt")
    '            Case "CIS_PROGRESS_REPORT_FS1_FS2_OLD"
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT_FS1_FS2_2010.rpt")
    '            Case "CIS_INTERIM_REPORT_1_8"
    '                If ViewState("MainMnu_code") <> "C300555" Then
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD1-8.rpt")
    '                Else
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD1-8_SLT.rpt")
    '                End If
    '            Case "CIS_INTERIM_REPORT_9_12"
    '                If ViewState("MainMnu_code") <> "C300555" Then
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13.rpt")
    '                Else
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13_SLT.rpt")
    '                End If
    '            Case "CIS_INTERIM_REPORT_9_10_new"
    '                If ViewState("MainMnu_code") <> "C300555" Then
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
    '                Else
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_new_SLT.rpt")
    '                End If
    '            Case "CIS_INTERIM_REPORT_13"
    '                If ViewState("MainMnu_code") <> "C300555" Then
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD13.rpt")
    '                Else
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD13_SLT.rpt")
    '                End If
    '            Case "CIS_INTERIM_REPORT_11_13_new"
    '                If ViewState("MainMnu_code") <> "C300555" Then
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11-13_new.rpt")
    '                Else
    '                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11-13_new_SLT.rpt")
    '                End If
    '            Case Else
    '                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT.rpt")
    '        End Select
    '    End With
    '    Session("rptClass") = rptClass
    '    If hfbDownload.Value = 0 Then
    '        If ViewState("MainMnu_code") = "StudentProfile" Then
    '            Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
    '        ElseIf ViewState("MainMnu_code") = "PdfReport" Then
    '            LoadReports(rptClass)
    '        Else
    '            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    '        End If
    '    Else
    '        LoadReports(rptClass)
    '    End If

    'End Sub



    Private Sub GenerateGIMProgressReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "EYFS_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_FS.rpt")
                Case "KS3_O_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_KS3WO.rpt")
                Case "KS3_W_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_KS3WC.rpt")
                Case "KS4_O_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_KS4WO.rpt")
                Case "KS4_W_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_KS4WC.rpt")
                Case "PRIMARY_T"
                    .reportPath = Server.MapPath("../Rpt/GIM/rptGIM_Y_6.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If

    End Sub




    Private Sub GenerateCISProgressReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "CIS_LANG_REPORT_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_LanguageReport_20_21.rpt")
                Case "CIS_INTERIM_07_13_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIM_REPORT_07-13_2020.rpt")
                Case "CIS_INTERIM_01_06_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIM_REPORT_01-06_2020.rpt")
                Case "CIS_TERMREPORT_10_13"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_TERM_REPORT_10-13.rpt")
                Case "CIS_TERMREPORT_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_InterimReport_19_20.rpt")
                Case "CIS_FORECAST_2021"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptForcast_Grades_A_Lavel.rpt")
                Case "CIS_RESULT"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_result.rpt")
                Case "CIS_TERMREPORT3_2020"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_Term3Report_19_20.rpt")
                Case "CIS_LANG_REPORT"
                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_LanguageReport_19_20.rpt")
                Case "CIS_MOCKREPORT_11_13"

                    If ddlReportPrintedFor.SelectedItem.Text = "MOCK REPORT 2" Then
                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_2015-16_TERM2.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_2015-16.rpt")
                    End If


                Case "CIS_INTERIM_REPORT_APP_KS1-3"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORT_APP_1-8.rpt")
                Case "CIS_INTERIM_REPORT_9_12_2012"
                    If ddlAca_Year.SelectedItem.Text <> "2012-2013" Then
                        If ViewState("MainMnu_code") <> "C300555" Then
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2013-14).rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2013-14)_SLT.rpt")
                        End If
                    ElseIf ViewState("MainMnu_code") <> "C300555" Then
                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13).rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13)_SLT.rpt")
                    End If
                Case "CIS_INTERIM_REPORT_11_2012"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "OCT-SENIOR SCHOOL 1ST INTERIM REPORT" Then
                        If ViewState("MainMnu_code") <> "C300555" Then
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13).rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13)_SLT.rpt")
                        End If
                    Else
                        If ViewState("MainMnu_code") <> "C300555" Then
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11(2012-13).rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11(2012-13)_SLT.rpt")
                        End If
                    End If
                Case "CIS_PROGRESS_REPORT_APP_FS1_FS2"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESSREPORT_APP_FS1-FS2.rpt")
                Case "CIS_FINAL_REPORT_APP_KS1-3"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_FINALREPORT_APP_1-8.rpt")
                Case "CIS_BTECREPORTS"
                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13_BTEC.rpt")
                Case Else
                    Select Case GetReportType()
                        Case "CIS_MONTHLY_REPORT" 'Monthly Progress Report
                            .reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESS_REPORT_GRD07.rpt")
                        Case "CIS_FINAL_REPORT" ' Final Report
                            Select Case ddlGrade.SelectedValue
                                Case "KG1", "KG2"
                                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_FINAL_REPORT_FS1_FS2.rpt")
                                Case "01", "02", "03", "04", "05", "06", "07", "08"
                                    If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                                        param.Add("@bSLT", "false")
                                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_FINALREPORT_APP_1-8.rpt")
                                    Else
                                        .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT.rpt")
                                    End If
                                Case Else
                                    .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT.rpt")
                            End Select

                        Case "CIS_PROGRESS_REPORT_FS1_FS2"
                            If ddlAca_Year.SelectedItem.Text = "2009-2010" Or ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT_FS1_FS2_2010.rpt")
                            Else
                                param.Add("GRD_ID", ddlGrade.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_PROGRESS_REPORT_FS1_FS2.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_1_8"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                'If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "MARCH INTERIM REPORT" And ddlGrade.SelectedValue = "08" Then
                                '    .reportPath = Server.MapPath("../Rpt/rptCIS_INTERIMREPORTGRD-8_mock.rpt")
                                'Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD1-8.rpt")
                                'End If

                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD1-8_SLT.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_9_12"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13.rpt")
                                '.reportPath = Server.MapPath("../Rpt/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13_SLT.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_9_10_new"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
                                '.reportPath = Server.MapPath("../Rpt/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-10_new_SLT.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_13"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD13.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD13_SLT.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_11_13_new"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11-13_new.rpt")
                                '.reportPath = Server.MapPath("../Rpt/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD11-13_new_SLT.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_APP"
                            param.Add("@bSLT", "false")
                            .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORT_APP_1-8.rpt")
                        Case "CIS_INTERIM_REPORT_9_12_2012"
                            If ViewState("MainMnu_code") <> "C300555" Then
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13).rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/CIS/rptCIS_INTERIMREPORTGRD9-13(2012-13)_SLT.rpt")
                            End If
                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If

    End Sub

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAca_Year.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportType.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Public Function GetAsOnDateForAttendance() As String
        Dim acd_id As String = ddlAca_Year.SelectedValue
        Dim grd_id As String = ddlGrade.SelectedValue 'CONVERT(VARCHAR(11), GETDATE(), 106) 
        Dim str_sql As String = " SELECT DISTINCT CONVERT(VARCHAR(11)," & _
        " ISNULL(MAX(OASIS.dbo.TERM_GRADEWISE.TRM_END_DT),GETDATE()), 103)  " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & acd_id & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & grd_id & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & ddlReportPrintedFor.SelectedValue

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

    End Function

    Sub BindBFinalReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M " _
                                 & " WHERE RSM_ID='" + ddlReportType.SelectedValue.ToString + "'"
        hfbFinalReport.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.ToLower
    End Sub

    Public Function GetTotalGradeAttendance() As String
        Dim acd_id As String = ddlAca_Year.SelectedValue
        Dim grd_id As String = ddlGrade.SelectedValue
        Dim str_sql As String = " SELECT DISTINCT ISNULL(OASIS.dbo.TERM_GRADEWISE.TRM_TOT_DAYS,0) " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & acd_id & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & grd_id & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & ddlReportPrintedFor.SelectedValue

        Dim total_att = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If total_att Is Nothing Then
            Return 0
        Else
            Return total_att
        End If
    End Function

    Private Sub GenerateAKNSProgressReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        'param.Add("ATTEND_TOTAL", GetTotalGradeAttendance())


        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
      

                    Dim rptClass As New rptClass
                    With rptClass
                        .crDatabase = "oasis_curriculum"
                        .reportParameters = param
                        Select Case hfReportFormat.Value

                            Case "AKN_QUARTER_REPORTS_2020-21"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_QUARTER1_20_21.rpt")
                Case "AKN_QUARTER_REPORTS_FS_2020-21"
                    param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                    param.Add("accYear", ddlAca_Year.SelectedItem.Text)
                    param.Add("GRD_ID", ddlGrade.SelectedValue)
                    .Photos = GetPhotoClass()
                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_QUARTER1_FS_20_21.rpt")

                            Case "AKN_QUARTER_REPORTS_01-10"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                If ddlReportPrintedFor.SelectedItem.Text.Contains("QUARTER 2") Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_10_QUARTER2.rpt")

                                Else
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_10_QUARTER.rpt")
                                End If
                            Case "AKN_FINALREPORTS_2017-18"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_2017-18.rpt")
                            Case "AKN_TERMREPORTS_2014-15"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15.rpt")
                            Case "AKN_FINALREPORTS_01-03"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD01_03.rpt")
                            Case "AKN_FINALREPORTS_04-10"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD04_10.rpt")
                            Case "AKN_FINALREPORTS_11-12"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD11_12.rpt")
                            Case "AKN_SEMREPORTS_01-03_SEM1"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                If ddlReportPrintedFor.SelectedItem.Text.Contains("SEMESTER 1") Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_03_SEM1.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_03_SEM2.rpt")
                                End If

                            Case "AKN_SEMREPORTS_04-10_SEM1"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                If ddlReportPrintedFor.SelectedItem.Text.Contains("SEMESTER 1") Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD04_10_SEM1.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD04_10_SEM2.rpt")
                                End If

                            Case "AKN_SEMREPORTS_11-12_SEM1"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                If ddlReportPrintedFor.SelectedItem.Text.Contains("SEMESTER 1") Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD11_12_SEM1.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD11_12_SEM2.rpt")
                                End If

                            Case "AKN_TERMREPORTS_2014-15_TERM2"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_TERM2.rpt")
                            Case "AKN_TERMREPORTS_2014-15_TERM3"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_TERM3.rpt")
                            Case "AKN_REPORTS_2014-15_FINAL"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_FINALREPORT.rpt")
                            Case "AKN_REPORTS_2015-16_FINAL"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2015-16_FINALREPORT.rpt")
                            Case "AKN_MIDTERREPORTS"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                Select Case ddlGrade.SelectedValue.ToString
                                    Case "KG1", "KG2"
                                        .Photos = GetPhotoClass()
                                        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT.rpt")
                                    Case "01", "02"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_MidtermReport01_02.rpt")
                                    Case "03", "04", "05", "06", "07", "08", "09"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_MidtermReport03_09.rpt")
                                    Case "10"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_MidtermReport_10.rpt")
                                    Case "11", "12"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_MidtermReport11_12.rpt")
                                End Select
                            Case "AKN_TERMREPORTS_2014"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                Select Case ddlGrade.SelectedValue.ToString
                                    Case "09", "10", "11", "12"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD9_12_2013-14.rpt")
                                    Case Else
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_8_2013-14.rpt")
                                End Select
                            Case "AKN_FINALREPORTS_2014"

                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                Select Case ddlGrade.SelectedValue.ToString
                                    Case "09", "10", "11", "12"
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptFINALREPORT_AKNS_GRD9_12_2013-14.rpt")
                                    Case Else
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptFINALREPORT_AKNS_GRD01_8_2013-14.rpt")
                                End Select
                            Case "AKN_CRITERIAREPORTS"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .Photos = GetPhotoClass()
                                param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT.rpt")
                            Case "AKN_CRITERIAREPORTS_2016"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                .Photos = GetPhotoClass()
                                param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                                .reportPath = Server.MapPath("../Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT_2016-17.rpt")
                            Case "AKN_TERMREPORTS"
                                param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 2 REPORT" Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS(2012-13)_GRD01_12_TERM2.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS(2012-13)_GRD01_12.rpt")
                                End If
                            Case "AKN_FINALREPORTS"
                                param.Add("@RPF_ID", ddlReportPrintedFor.Items(0).Value)
                                If ddlReportPrintedFor.SelectedItem.Text = "FINAL REPORT" Then
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_ALLTERMS.rpt")
                                Else
                                    param.Add("@GRD_ID", ddlGrade.SelectedValue)
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_MINISTRY.rpt")
                                End If
                            Case Else
                                If hfbFinalReport.Value = "false" Then
                                    param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
                                    .reportPath = Server.MapPath("../Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12.rpt")

                                Else
                                    param.Add("@RPF_ID", ddlReportPrintedFor.Items(0).Value)
                                    If ddlReportPrintedFor.SelectedItem.Text = "FINAL REPORT" Then
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS.rpt")
                                    Else
                                        param.Add("@GRD_ID", ddlGrade.SelectedValue)
                                        .reportPath = Server.MapPath("../Rpt/AKNS/rptFINAL_REPORT_AKNS_MINISTRY.rpt")
                                    End If
                                End If
                        End Select
                    End With
                    Session("rptClass") = rptClass
                    ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                    If hfbDownload.Value = 0 Then
                        If ViewState("MainMnu_code") = "StudentProfile" Then
                            Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
                        ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                            LoadReports(rptClass)
                        Else
                            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                            ReportLoadSelection()
                        End If
                    Else
                        LoadReports(rptClass)
                    End If
    End Sub


    Private Sub GenerateTermlyReport_TMS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("ATTEND_TOTAL", GetTotalGradeAttendance())
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case ddlGrade.SelectedValue
                Case "11", "12"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD11-12.rpt")
                Case "04"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD04.rpt")
                Case "05", "06", "07", "08"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD05-08.rpt")
                Case "09"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD09.rpt")
                Case "10"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD10.rpt")
                Case "01", "02", "03"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_TMS_GRD01-03.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Private Sub GenerateRehearsalReport_10()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        If ddlAca_Year.SelectedValue = "624" Then
            param.Add("RPT_ASSESSMENT_HEADER", "EXAM")
        Else
            param.Add("RPT_ASSESSMENT_HEADER", "REHEARSAL EXAM")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateRehearsalReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "GRADE_11_12_MODEL_FORMAT1"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL.rpt")
                Case "GRADE_11_12_MODEL_FORMAT2"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL_FORMAT1.rpt")
                Case "GRADE_11_12_MODEL_FORMAT3"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL_FORMAT2.rpt")
                Case "GRADE_11_12_REHERSAL_FORMAT1"
                    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_CBSE_GRD11_12_REHERSAL.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            Else
                '     Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateTermlyReport_OOEHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If hfbFinalReport.Value = "false" Then
                param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())

                Select Case ddlGrade.SelectedValue
                    Case "01", "02"
                        param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD01_02.rpt")
                    Case "03", "04"
                        param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD03_04.rpt")
                    Case "05", "06", "07", "08"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt")
                    Case "09", "10"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD09_10.rpt")
                    Case "11", "12"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        If (ddlAca_Year.SelectedValue = 879 Or ddlAca_Year.SelectedValue = 965) And (ddlReportPrintedFor.SelectedItem.Text = "TERM 1 REPORT" Or ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT") Then
                            .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                        Else
                            If Session("SBSUID") = "121013" Then
                                If ddlAca_Year.SelectedItem.Text = "2015-2016" Then
                                    If ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT" Then
                                        If ddlGrade.SelectedValue = "11" Then
                                            .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                        Else
                                            .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_TERM2.rpt")
                                        End If

                                    Else



                                        If ddlGrade.SelectedValue = "11" Then
                                            If ddlReportPrintedFor.SelectedItem.Text.Contains("TERM 2") Then
                                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                            Else
                                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16.rpt")
                                            End If
                                        Else
                                            .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                        End If
                                    End If
                                ElseIf ((ddlAca_Year.SelectedItem.Text = "2016-2017") Or (ddlAca_Year.SelectedItem.Text = "2017-2018") Or (ddlAca_Year.SelectedItem.Text = "2018-2019") Or (ddlAca_Year.SelectedItem.Text = "2019-2020") Or (ddlAca_Year.SelectedItem.Text = "2020-2021")) Then
                                    If ddlReportPrintedFor.SelectedItem.Text = "MID TERM REPORT" Then
                                        .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                                    ElseIf (((ddlReportPrintedFor.SelectedItem.Text = "TERM 1 REPORT") Or (ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT")) And ((ddlAca_Year.SelectedValue = 1548) Or (ddlAca_Year.SelectedValue = 1773))) Then
                                        .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                                    ElseIf ddlReportPrintedFor.SelectedItem.Text = "TERM 1 REPORT" Then
                                        .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_TERM1.rpt")
                                    ElseIf ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT" Then
                                        If ddlGrade.SelectedValue = "11" Then
                                            If ddlAca_Year.SelectedItem.Text = "2018-2019" Then
                                                .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_TERM1.rpt")
                                            Else
                                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                            End If

                                        Else
                                            .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                        End If
                                    Else
                                        If ddlGrade.SelectedValue = "11" Then
                                            If ddlReportPrintedFor.SelectedItem.Text.Contains("TERM 2") Then
                                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                            Else
                                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16.rpt")
                                            End If
                                        Else
                                            .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                        End If
                                    End If
                                End If
                                '.reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16.rpt")
                            ElseIf ddlAca_Year.SelectedValue = "1145" And ddlReportPrintedFor.SelectedItem.Text = "TERM 2 REPORT" And ddlGrade.SelectedValue = "12" Then
                                .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                            Else

                                .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12.rpt")
                            End If

                        End If
                        'If ddlAca_Year.SelectedValue = 879 And ddlReportPrintedFor.SelectedItem.Text <> "MID TERM REPORT" Then
                        '    .reportPath = Server.MapPath("../Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                        'Else
                        '    .reportPath = Server.MapPath("../Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12.rpt")
                        'End If

                End Select
            Else

                Select Case ddlGrade.SelectedValue
                    Case "01", "02"
                        'param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        'param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD01_02.rpt")
                    Case "03", "04"
                        'param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        'param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD03_04.rpt")
                    Case "05", "06", "07", "08"
                        param.Add("TERM1_HEADING", "Term I 20%")
                        param.Add("TERM2_HEADING", "Term II 30%")
                        param.Add("TERM3_HEADING", "Term III 50%")
                        .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD05_08.rpt")
                    Case "09"
                        param.Add("TERM1_HEADING", "Term I 20%")
                        param.Add("TERM2_HEADING", "Term II 20%")
                        param.Add("TERM3_HEADING1", "FA 20%")
                        param.Add("TERM3_HEADING2", "SA 40%")
                        .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD09.rpt")
                    Case "10", "11", "12"
                        'param.Add("UT1_HEADING", "Unit Test I (5)")
                        'param.Add("UT2_HEADING", "Unit Test II (5)")
                        'param.Add("UT3_HEADING", "Rehearsal I (30)")
                        'param.Add("UT4_HEADING", "Rehearsal II (30)")
                        'param.Add("TERM1_HEADING", "Term I (30)")
                        'param.Add("TERM2_HEADING", "Term II (50)")
                        '.reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_OOEHS_DUBAI_GRD10_12.rpt")
                        If ((ddlGrade.SelectedValue = "11" And ddlAca_Year.SelectedValue = "1143")) Then
                            param.Add("TERM1_HEADING", "Term I 20%")
                            param.Add("TERM2_HEADING", "Term II 20%")
                            param.Add("TERM3_HEADING", "Term III 50%")
                            param.Add("TEST_HEADING", "TEST 10%")
                            .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD11_2016-17.rpt")

                        ElseIf ((ddlGrade.SelectedValue = "11" And ddlAca_Year.SelectedValue = "1548")) Then
                            param.Add("TERM1_HEADING", "Term I 20%")
                            param.Add("TERM2_HEADING", "Term II 20%")
                            param.Add("TERM3_HEADING", "Term III 50%")
                            param.Add("TEST_HEADING", "TEST 10%")
                            .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD_11_2020.rpt")
                        Else
                            param.Add("TERM1_HEADING", "Term I 20%")
                            param.Add("TERM2_HEADING", "Term II 20%")
                            param.Add("TERM3_HEADING", "Term III 50%")
                            param.Add("TEST_HEADING", "TEST 10%")
                            .reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD10_12.rpt")
                        End If
                End Select


            End If

            '.reportPath = Server.MapPath("../Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD09.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateOOFIGCSEReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            Select Case ddlGrade.SelectedValue
                Case "09"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                        If (ddlAca_Year.SelectedItem.Text = "2017-2018") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
                        ElseIf (ddlAca_Year.SelectedItem.Text = "2018-2019") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4.rpt")
                        End If
                    Else
                        If (ddlAca_Year.SelectedItem.Text = "2017-2018") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_2017-18_TERM2.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4.rpt")
                        End If

                    End If
                Case "10"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                        If (ddlAca_Year.SelectedItem.Text = "2017-2018") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
                        ElseIf (ddlAca_Year.SelectedItem.Text = "2018-2019") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4_S5_2018_19.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4.rpt")
                        End If

                    Else
                        .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4_S5_Term2.rpt")
                    End If
                Case "11"
                    'If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                    '    .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S5.rpt")
                    'Else
                    '    .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4_S5_Term2.rpt")
                    'End If
                    If ((ddlAca_Year.SelectedItem.Text = "2016-2017") Or (ddlAca_Year.SelectedItem.Text = "2016-2017")) Then
                        .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S5_2016_17.rpt")
                    Else
                        If (ddlAca_Year.SelectedItem.Text = "2017-2018") Then
                            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
                                .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S5_2016_17.rpt")
                            Else
                                .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4_S5_Term2.rpt")
                            End If

                        ElseIf (ddlAca_Year.SelectedItem.Text = "2018-2019") Then
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4_S5_Term2.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S5.rpt")
                        End If

                    End If

            End Select
        End With
        Session("rptClass") = rptClass

        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateOOFIGCSEReports_J5_S2()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())
        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()


            If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then

                .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/OOF_IGCSE/rptOOF_IGCSE_TERM_REPORT_S4.rpt")

            End If



        End With
        Session("rptClass") = rptClass

        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateGWAReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .Photos = GetPhotoClass()
            Select Case GetReportType()
                Case "GWA_ELEMENTARYREPORT"
                    If ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlReportPrintedFor.SelectedItem.Text.ToUpper = "QUARTER 2" Then
                        .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_ElementarySchool_Q2.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_ElementarySchool.rpt")
                    End If
                Case "GWA_TERMLYREPORT"
                    .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_TERMLYREPORT.rpt")
                Case "GWA_MIDTERMREPORT"
                    If ddlAca_Year.SelectedItem.Text = "2009-2010" Or ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                        .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_MidSemester.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_MidSemesterNew.rpt")
                    End If
                Case "GWA_MYPREPORT"
                    .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_MYPREPORT.rpt")
                Case "GWA_ALSREPORT"
                    .reportPath = Server.MapPath("../Rpt/GWA/rptGWA_ALSREPORT.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateGAAReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            'Select Case GetReportType()
            '   Case "GAA_PROGRESSREPORT"
            'If ddlAca_Year.SelectedItem.Text <> "2009-2010" And ddlAca_Year.SelectedItem.Text <> "2011-2012" And (ddlGrade.SelectedValue = "KG1") Then
            '    .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG1.rpt")
            'Else

            If ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2009-2010.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2010-2011.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                Select Case ddlGrade.SelectedValue.ToString
                    Case "PK"
                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 3 REPORT" Then
                            .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG1_2011-2012.rpt")
                        End If
                    Case "KG1"
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                    Case "KG2"
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2(5)_2011-2012.rpt")
                    Case "06", "07", "08"
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012_06_08.rpt")
                    Case Else
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")

                End Select
            ElseIf ddlAca_Year.SelectedItem.Text = "2012-2013" Then
                Select Case ddlGrade.SelectedValue.ToString
                    Case "PK"
                        '.reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 3 REPORT" Then
                            .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        Else
                            .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG1_2011-2012.rpt")
                        End If
                    Case "KG1"
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                    Case "06", "07", "08"
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012_06_08.rpt")
                    Case Else
                        .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")
                End Select
            ElseIf ddlAca_Year.SelectedItem.Text = "2013-2014" Then
                '  Select Case ddlGrade.SelectedValue.ToString
                ' Case "PK", "KG1"
                .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2013-2014.rpt")
                '  Case Else
                ' .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")
                'End Select

            ElseIf ddlAca_Year.SelectedItem.Text = "2014-2015" Then
                .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2015-16.rpt")
            End If

            'Case "GWA_TERMLYREPORT"
            '    .reportPath = Server.MapPath("../Rpt/rptGWA_TERMLYREPORT.rpt")
            'Case "GWA_MIDTERMREPORT"
            '    .reportPath = Server.MapPath("../Rpt/rptGWA_MidSemester.rpt")
            ' End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Sub GenerateWAAReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            If ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                'If ddlGrade.SelectedValue.ToString = "PK" Then
                '    .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG1_2011-2012.rpt")
                'ElseIf ddlGrade.SelectedValue.ToString = "KG1" Then
                '    .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                'Else
                '    .reportPath = Server.MapPath("../Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")
                'End If 
                .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool_2011-2012.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2012-2013" Then
                Select Case ddlGrade.SelectedValue.ToString
                    Case "PK", "KG1"
                        .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool_KG1-KG2(4).rpt")
                    Case Else
                        .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool_2011-2012.rpt")
                End Select
            ElseIf ddlAca_Year.SelectedItem.Text = "2013-2014" Or ddlAca_Year.SelectedItem.Text = "2014-2015" Then
                Select Case ddlGrade.SelectedValue.ToString
                    Case "PK", "KG1"
                        .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool_KG1-KG2(4).rpt")
                    Case Else
                        .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool.rpt")
                End Select
            Else
                .reportPath = Server.MapPath("../Rpt/WAA/rptWAA_ElementarySchool_2015-16.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Sub GenerateMAKReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "MAK_REPORT_18"
                    .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_2015-16.rpt")
                Case "MAK_REPORT_19"

                    Select Case ddlGrade.SelectedValue.ToString
                        Case "PK"
                            .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_PK_2019-2020.rpt")
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_KG_2019-2020.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_2019-2020.rpt")
                    End Select
                Case "MAK_REPORT_20"

                    Select Case ddlGrade.SelectedValue.ToString
                        Case "PK"
                            .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_PK_2019-2020.rpt")
                        
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/MAK/rptMAK_ElementarySchool_2020-2021.rpt")
                    End Select


            End Select


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Sub GenerateGISReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPF_DESCR", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "GIS_6_8_REPORT"
                    .reportPath = Server.MapPath("../Rpt/GIS/rptGIS_ProgressReport_6_8.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/GIS/rptGIS_ElementarySchool.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '     Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO

        Dim arrList As ArrayList

        arrList = New ArrayList(h_STU_IDs.Value.Replace("|", "___").Split("___"))

        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function



    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportCardType()
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'code added by dhanya
        BindReportType()
        BindReportCardType()
        If (ViewState("MainMnu_code") = "C400005") Then
            BindReportPrintedFor(True)
        Else
            BindReportPrintedFor()
        End If
        GetAllGrade()
        GetSectionForGrade()
    End Sub


    Sub LoadReports(ByVal rptClass)


        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If

                If ViewState("MainMnu_code") = "PdfReport" Then
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    rs.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                    rs.Close()
                    rs.Dispose()
                Else
                    Try
                        exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                    Catch ee As Exception
                    End Try
                    rs.Close()
                    rs.Dispose()
                End If
                ' Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            rs.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                Dim hop As New CrystalDecisions.Shared.ExportOptions
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.ContentType = "application/pdf"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        ''HttpContext.Current.Response.Flush()
        ''HttpContext.Current.Response.Close()
        'HttpContext.Current.Response.End()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        System.IO.File.Delete(tempFileNameUsed)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub", "rptWSOInterimSub", "rptCISImageSub", "rptWINImageSub", "rptWSSImageSub", "rptWISImageSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If Session("sbsuid") = "125017" Then
                            If Not Session("noimg") Is Nothing Then
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                            Else
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/NOIMG/no_img_gwa.jpg")
                            End If

                        ElseIf Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function


    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\" + Session("sbsuid") + "\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Sub CallReports()
        Try
            BindBFinalReport()
            If h_STU_IDs.Value = "" Then
                Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
                h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, str_sec)
            End If
            Select Case hfReportType.Value
                Case "CBSE_FORMATIVE"
                    GenerateFormativeAssessmentReport()
                Case "CBSE_SUMMATIVE"
                    GenerateSummativeAssessmentReport()
                Case "CBSE_I_SUMMATIVE"
                    GenerateCBSE_I_SummativeAssessmentReport()
                Case "CBSE_I_FINALREPORT"
                    GenerateCBSE_I_FINALREPORT()
                Case "CBSE_I_TERMREPORT_01_03"
                    GenerateCBSE_I_TermReport01_03()
                Case "CBSE_TERMTEST_11-12"
                    GenerateTermTestReport()
                Case "CBSE_KG_REPORT"
                    GenerateCBSEKinderGarten()
                Case "CBSE_I_KG_REPORT"
                    GenerateCBSE_I_KinderGarten()
                Case "CBSE_I_TERMREPORT_01_02"
                    GenerateCBSE_I_TERMREPORT_01_02()
                Case "CBSE_MIDTERMFEEDBACK"
                    GenerateCBSEMidTermReport()
                Case "IGCSE_SUMMATIVE"
                    GenerateSummativeAssessmentReport()
                Case "IGCSE_FORMATIVE"
                    GenerateFormativeAssessmentReport()
                Case "IGCSE_KG_REPORT"
                    GenerateCBSEKinderGarten()
                Case "IGCSE_TERMREPORT_09-11"
                    GenerateOOFIGCSEReports()
                Case "IGCSE_TERMREPORT_J5-S2"
                    GenerateOOFIGCSEReports_J5_S2()
                Case "IGCSE_FINALREPORT"
                    GenerateIGCSEFinalREportS3S4()
                Case "CBSE_REHEARSAL_10"
                    GenerateRehearsalReport_10()
                Case "CBSE_REHEARSAL_12"
                    GenerateRehearsalReport()
                Case "TWS_FINAL_REPOT"
                    GenerateFinalReport_TWS()
                Case "TWS_TERMLY_REPORT"
                    GenerateTermlyReport_TWS()
                Case "AIS_REPORTS"
                    GenerateAISReports()
                Case "FPS_PROGESSREPORTS_SIMS"
                    GenerateProgressreports_SIMS()
                Case "DMHS_MIDTERMREPORTS"
                    GenerateMidTermReport_DMHS_Dubai()
                Case "DMHS_TERMREPORTS"
                    GenerateTermlyReport_DMHS_Dubai()
                Case "DMHS_FINALREPORTS"
                    GenerateFinalReport_DMHS_Dubai()
                    'Case "CIS_REPORTS"
                    '    GenerateCISProgressReport()
                Case "WSS_REPORTS"
                    GenerateWSSReports()
                Case "WSR_REPORTS"
                    GenerateWSRReports()
                Case "WGP_REPORTS"
                    GenerateWGPReports()
                Case "CIN_REPORTS"
                    GenerateCINReports()
                Case "BIM_REPORTS"
                    GenerateBIMReports()
                Case "MLS_REPORTS"
                    GenerateMLSReports()
                Case "MIL_REPORTS"
                    GenerateMILReports()
                Case "TBS_REPORTS"
                    GenerateTBSReports()
                Case "GIM_REPORTS"
                    GenerateGIMProgressReport()
                Case "GFM_REPORTS"
                    GenerateGFMReports()
                Case "RDS_REPORTS"
                    GenerateRDSReports()
                Case "PROGRESSTRACKER"
                    GenerateProgressTrackerReports()
                Case "NSG_REPORTS"
                    GenerateNSGReports()
                Case "ISP_REPORTS"
                    GenerateISPReports()
                Case "ISP_REPORTS_FINAL"
                    GenerateISPFinalReports()
                Case "NMS_REPORTS"
                    GenerateNMSReports()
                Case "CIA_REPORTS"
                    GenerateCIAReports()
                Case "GIS_REPORTS"
                    GenerateGISReports()
                Case "WSA_REPORTS"
                    GenerateWSAReports()
                Case "MTS_REPORTS"
                    GenerateMTSReports()
                Case "OOL_IGCE_REPORTS"
                    GenerateOOLReports()
                Case "GIP_REPORTS"
                    GenerateGIPReports()
                Case "MAG_REPORTS"
                    Generate_TERMREPORT_MAG()
                Case "DMHS_BENCHMARK"
                    GenerateBENCHMARKReports()
                Case "CBSE_FINAL_FORMAT_01_04"
                    Generate_CBSE_FinalReport01_04()
                Case "CBSE_FINAL_FORMAT_05_08"
                    Generate_CBSE_FinalReport05_08()
                Case "CBSE_FINAL_FORMAT_09"
                    Generate_CBSE_FinalReport09()
                Case "CBSE_FINAL_FORMAT_GHS"
                    Generate_CBSE_FinalReport_GHS()
                Case "MOE_BEHAVIOUR_REPORT"
                    GenerateBehaviiourReports()
                Case "CBSE_OOW_11_12_NEW"
                    GenerateCBSEFormativeAssessmentReport_11_12()
                Case "CBSE_NEW_FORMAT_GHS"
                    Generate_CBSE_TermReport_GHS()
                Case "IGCSE_NMS"
                    Generate_IGCSE_TermReport_NMS()
                Case "CBSE_RLP_REPORT"
                    Generate_RLP_AssessmentReport()
                Case "CBSE_GHS_REPORT_01"
                    Generate_GHS_AssessmentReport()
                Case "TMS_03_04_REPORT"
                    Generate_TMS_AssessmentReport()
                Case "CBSE_NEW_FORMAT_01_04"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") Then
                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("MID") Then
                            Generate_CBSE_AssessmentReport()
                        Else
                            Generate_CBSE_TermReport01_04()
                        End If

                    Else
                        Generate_CBSE_AssessmentReport()
                    End If
                Case "CBSE_NEW_FORMAT_09_10"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") Then
                        Generate_CBSE_TermReport_09_10()
                    Else
                        Generate_CBSE_AssessmentReport()
                    End If
                Case "CBSE_NEW_FORMAT_05_10"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") Then
                        Generate_CBSE_TermReport_05_10()
                    Else
                        Generate_CBSE_AssessmentReport()
                    End If
                Case "CBSE_NEW_FORMAT_05_10_NMS"
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") Then
                        Generate_CBSE_TermReport_05_10()
                    Else
                        Generate_CBSE_AssessmentReport_NMS()
                    End If
                Case Else
                    If GetAllCBSEBusinessUnit(Session("sBSUID")) <> "" Then

                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("REHEARSAL") _
                         Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("MODEL") _
                         Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("REVISION") Then
                            Select Case ViewState("MainMnu_code")
                                Case CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT, "StudentProfile", "C300555", "PdfReport"
                                    Select Case Session("sBSUID")
                                        Case GetAllCBSEBusinessUnit(Session("sBSUID"))
                                            If ddlGrade.SelectedValue = "12" Then
                                                If ddlAca_Year.SelectedValue = "1144" Then
                                                    GenerateCBSEFormativeAssessmentReport_11_12()
                                                Else
                                                    GenerateRehearsalReport()
                                                End If
                                            ElseIf ddlGrade.SelectedValue = "10" Then
                                                GenerateRehearsalReport_10()
                                            End If
                                    End Select
                            End Select

                        ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") OrElse _
                    ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") OrElse _
                    ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("QUARTERLY") OrElse ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("HALF") _
                    OrElse ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("ASSESSMENT") _
                    OrElse ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("ANNUAL") Then
                            Select Case ViewState("MainMnu_code")
                                Case CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT, "StudentProfile", "C300555", "PdfReport"
                                    Select Case ddlGrade.SelectedValue
                                        Case "01", "02", "03", "04"
                                            Select Case Session("sBSUID")
                                                Case "121013"
                                                    If ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                                        GenerateTermlyReport_OOEHS_Dubai()
                                                    Else
                                                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                                            GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                        Else
                                                            GenerateCBSETermReport01_04()
                                                        End If
                                                    End If
                                                Case Else
                                                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                                        GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                    Else
                                                        GenerateCBSETermReport01_04()
                                                    End If
                                            End Select
                                        Case Else
                                            Select Case Session("sBSUID")
                                                Case "111001"
                                                    If ddlGrade.SelectedValue = "05" And ((ddlAca_Year.SelectedValue = "1271") Or (ddlAca_Year.SelectedValue = "1406") Or (ddlAca_Year.SelectedValue = "1540") Or (ddlAca_Year.SelectedValue = "1769")) Then
                                                        If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                                            GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                        Else
                                                            GenerateCBSETermReport01_04()
                                                        End If
                                                    ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("ANNUAL") Then
                                                        GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                    Else
                                                        GenerateCBSEFormativeAssessmentReport_11_12()
                                                    End If

                                                Case "121013"
                                                    If ddlAca_Year.SelectedItem.Text = "2009-2010" Then
                                                        GenerateTermlyReport_OOEHS_Dubai()
                                                    ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                                        Select Case ddlGrade.SelectedValue
                                                            Case "11", "12"
                                                                GenerateTermlyReport_OOEHS_Dubai()
                                                            Case Else
                                                                GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                        End Select
                                                    Else
                                                        GenerateTermlyReport_OOEHS_Dubai()
                                                    End If
                                                Case "121014"
                                                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                                        GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                    Else
                                                        GenerateCBSEFormativeAssessmentReport_11_12()
                                                    End If
                                                Case Else
                                                    If ddlAca_Year.SelectedValue = "1145" And ddlReportPrintedFor.SelectedItem.Text.ToUpper() = "TERM 2 REPORT" And ddlGrade.SelectedValue = "12" Then
                                                        GenerateTermlyReport_OOEHS_Dubai()
                                                    ElseIf ddlAca_Year.SelectedValue = "1402" And ddlGrade.SelectedValue = "11" Then
                                                        GenerateTermlyReport_OOEHS_Dubai()
                                                    ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("ANNUAL") Then
                                                        If ddlAca_Year.SelectedValue = "1551" Then
                                                            GenerateCBSEFormativeAssessmentReport_11_12()
                                                        Else

                                                            GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                                        End If
                                                    Else
                                                            GenerateCBSEFormativeAssessmentReport_11_12()

                                                    End If
                                            End Select
                                    End Select
                            End Select

                        End If
                    Else
                        Select Case ViewState("MainMnu_code")
                            Case CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT, "StudentProfile", "C300555", "PdfReport"
                                Select Case Session("sBSUID")
                                    'Case GetAllCBSEBusinessUnit(Session("sBSUID"))
                                    '    GenerateFormativeAssessmentReport()
                                    Case "125005"
                                        GenerateCISProgressReport()
                                    Case "123006"
                                        GenerateTermlyReport_TMS()
                                    Case "121013"
                                        GenerateTermlyReport_OOEHS_Dubai()
                                    Case "121012"
                                        'OOIS, Dubai
                                        GenerateTermlyReport_OOEHS_Dubai()
                                    Case "125002"
                                        GenerateJCReports()

                                    Case "125010"
                                        If hfbFinalReport.Value = "false" Then
                                            GenerateTermlyReport_TWS()
                                        Else
                                            GenerateFinalReport_TWS()
                                        End If
                                    Case "125017"
                                        GenerateGWAReports()
                                    Case "114003"
                                        GenerateGAAReports()
                                    Case "114004"
                                        GenerateWAAReports()
                                    Case "223006"
                                        GenerateMAKReports()
                                    Case "125011"
                                        GenerateWinProgressReport()
                                    Case "126008"
                                        GenerateAKNSProgressReport()
                                    Case "115002"
                                        GenerateCHSReports()
                                    Case "125018"
                                        GenerateWSOReports()
                                    Case "125015"
                                        GenerateWISReports()
                                    Case "125012"
                                        GenerateWSDReports()
                                    Case "610010"
                                        GenerateKSBReports()
                                    Case "450030"
                                        GenerateCIKReports()
                                End Select
                            Case "C400005"
                                Select Case Session("sBSUID")
                                    Case "121013"
                                        GenerateCumulativeReport_OOEHS_Dubai()
                                End Select
                        End Select
                    End If
            End Select
        Catch ex As Exception
            ' lblerror.Text = ex.Message
        End Try
    End Sub
    Private Sub Generate_CBSE_FinalReport_GHS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case ddlGrade.SelectedValue.ToString
                Case "01", "02"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_01_02_GHS.rpt")
                Case "09", "10"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_09_10_GHS.rpt")

                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_03_08_GHS.rpt")
            End Select



        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_TermReport_GHS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)



        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM 1") Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM1_REPORT_01_10_GHS.rpt")
            Else
                Select Case ddlGrade.SelectedValue.ToString
                    Case "01", "02", "09", "10"
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM1_REPORT_01_10_GHS.rpt")

                    Case Else
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM1_REPORT_01_10_GHS_TERM2.rpt")
                End Select
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_TermReport_09_10()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
      


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_09_OOB.rpt")



        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_IGCSE_TermReport_NMS()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
           
            .reportPath = Server.MapPath("../Rpt/rptIGCSE_TERM_REPORT_10_NMS.rpt")





        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub





    Private Sub Generate_CBSE_TermReport_05_10()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        'If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Then
        '    param.Add("RPT_CAPTION", "TERM I REPORT")
        '    param.Add("FA1_PERC", "10%")
        '    param.Add("FA2_PERC", "10%")
        '    param.Add("TOTAL_FA_PERC", "20%")
        '    If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
        '        param.Add("SA_PERC", "20%")
        '        param.Add("TOTAL_PERC", "40%")
        '    ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
        '        param.Add("SA_PERC", "20%")
        '        param.Add("TOTAL_PERC", "40%")
        '    Else
        '        param.Add("SA_PERC", "30%")
        '        param.Add("TOTAL_PERC", "50%")
        '    End If
        '    param.Add("FA_HEADER1", "FA1")
        '    param.Add("FA_HEADER2", "FA2")
        'Else
        '    param.Add("RPT_CAPTION", "TERM II REPORT")
        '    param.Add("FA1_PERC", "10%")
        '    param.Add("FA2_PERC", "10%")
        '    param.Add("TOTAL_FA_PERC", "20%")
        '    If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
        '        param.Add("SA_PERC", "40%")
        '        param.Add("TOTAL_PERC", "60%")
        '    ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
        '        param.Add("SA_PERC", "40%")
        '        param.Add("TOTAL_PERC", "60%")
        '    Else
        '        param.Add("SA_PERC", "30%")
        '        param.Add("TOTAL_PERC", "50%")
        '    End If
        '    param.Add("FA_HEADER1", "FA3")
        '    param.Add("FA_HEADER2", "FA4")
        'End If


        'Dim vSA_HEADER As String = "SA"

        'If ddlGrade.SelectedValue = "10" Then
        '    If ddlReportPrintedFor.SelectedItem.Text.ToUpper <> "SUMMATIVE REPORT 1" Then
        '        param.Add("SA_HEADER", "Rehearsal Exam")
        '    Else
        '        param.Add("SA_HEADER", vSA_HEADER)
        '    End If
        'Else
        '    param.Add("SA_HEADER", vSA_HEADER)
        'End If


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If ddlAca_Year.SelectedValue = 1777 Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_05_10_NMS.rpt")
            ElseIf ddlAca_Year.SelectedValue = 1776 Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_05_10_TMS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_05_10.rpt")
            End If




        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Sub GenerateProgressreports_SIMS()
        Dim tempDir As String = ConfigurationManager.ConnectionStrings("STU_REPORTCARDS").ConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim stu As String = h_STU_IDs.Value.Replace("___", "|")
        str_query = "select stu_no from student_m where stu_id in (select id from oasisfin.[dbo].[fnSplitMe] (" + stu + " ,'|' )) "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1

            Dim tempFileName As String = "/" + Session("SBSUID") + "/" + ddlReportPrintedFor.SelectedValue + "/" + ds.Tables(0).Rows(i)("STU_NO") + ".pdf"
            Dim tempFileNameUsed As String = tempDir + tempFileName
            If hfbDownload.Value = "1" Then
                'HttpContext.Current.Response.ContentType = "application/pdf"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
                'HttpContext.Current.Response.End
                Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            Else
                Response.ClearContent()
                Response.ClearHeaders()
                Response.ContentType = "application/pdf"
                Response.WriteFile(tempFileNameUsed)
                Response.Flush()
                Response.Close()
            End If
        Next
    End Sub
    Private Sub GenerateSummativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Then

            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")
            If Session("SBSUID") = "131003" Then
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
                param.Add("FA_HEADER1", "FA1")
                param.Add("FA_HEADER2", "PA1")
            Else

                If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                    param.Add("SA_PERC", "20%")
                    param.Add("TOTAL_PERC", "40%")
                ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
                    param.Add("SA_PERC", "20%")
                    param.Add("TOTAL_PERC", "40%")
                Else
                    param.Add("SA_PERC", "30%")
                    param.Add("TOTAL_PERC", "50%")
                End If
                param.Add("FA_HEADER1", "FA1")
                param.Add("FA_HEADER2", "FA2")
            End If
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")
            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            Else
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
            End If
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If


        Dim vSA_HEADER As String = "SA"

        If ddlGrade.SelectedValue = "10" Then
            If ddlReportPrintedFor.SelectedItem.Text.ToUpper <> "SUMMATIVE REPORT 1" Then
                param.Add("SA_HEADER", "Rehearsal Exam")
            Else
                param.Add("SA_HEADER", vSA_HEADER)
            End If
        Else
            param.Add("SA_HEADER", vSA_HEADER)
        End If


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If Session("SBSUID") = "131003" Then
                .reportPath = Server.MapPath("../Rpt/rptGEP_SUMMATIVE_ASSESSMENT_REPORT_03_05.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
            ElseIf ddlAca_Year.SelectedItem.Text = "2017-2018" And ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 2" Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10_2017-18.rpt")
            Else
                If ddlGrade.SelectedValue = "10" And ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_10_new.rpt")
                Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10_new.rpt")
                    '.reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
                End If

            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSE_I_SummativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Then
            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")

            param.Add("SA_PERC", "30%")
            param.Add("TOTAL_PERC", "50%")

            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")

            param.Add("SA_PERC", "30%")
            param.Add("TOTAL_PERC", "50%")

            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If


        Dim vSA_HEADER As String = "SA"


        param.Add("SA_HEADER", vSA_HEADER)


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSE_I_FINALREPORT()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", "")
        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)

        Dim rptClass As New rptClass
        With rptClass
            .Photos = GetPhotoClass()
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            Select Case hfReportFormat.Value
                Case "CBSE_I_FINALREPORT_01_03"
                    .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_FINALREPORT_01_03_2013.rpt")
                Case "CBSE_I_FINALREPORT_04_05"
                    .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_FINALREPORT_04_05_2013.rpt")
                Case Else
                    Select Case ddlGrade.SelectedValue.ToString
                        Case "01", "02", "03"
                            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_FINALREPORT_01_03.rpt")
                        Case "04", "05"
                            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_FINALREPORT_04_05.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_FINALREPORT_06_10.rpt")
                    End Select
            End Select

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEKinderGarten()
        Dim param As New Hashtable

        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))


        If hfReportFormat.Value.ToUpper <> "NMS_KG_REPORT" Then
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value.ToUpper
                Case "OIS_KG_REPORT"
                    ' If ddlGrade.SelectedValue = "KG1" Then
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_OIS.rpt")
                    '  Else
                    '.reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_OIS_KG2.rpt")
                    'End If
                Case "UIS_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_UIS.rpt")
                Case "OOS_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_OOS.rpt")
                Case "GMS_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_GMS.rpt")
                Case "GEP_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_GEP.rpt")
                Case "OOS_KG_REPORT_NEW"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_OOS_2018-19.rpt")
                Case "KGS_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_KinderGarten_KGS.rpt")
                Case "OOD_KG_REPORT"
                    Select Case ddlReportPrintedFor.SelectedItem.Text.ToUpper
                        Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER"
                            .reportPath = Server.MapPath("../Rpt/cbse/rptCBSE_KinderGarten_Term1_OOD.rpt")
                        Case "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY", "END OF FEBRUARY"
                            .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten_Term2.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten.rpt")
                    End Select
                Case "NMS_KG_REPORT"
                    .reportPath = Server.MapPath("../Rpt/NMS/rptNMS_PROGRESSREPORT_KG1_KG2.rpt")
                Case Else
                    If Session("clm") = 1 Then
                        Select Case ddlReportPrintedFor.SelectedItem.Text.ToUpper
                            Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER"
                                .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten_Term1.rpt")
                            Case "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY", "END OF FEBRUARY"
                                .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten_Term2.rpt")
                            Case Else
                                .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten.rpt")
                        End Select
                    Else
                        Select Case ddlReportPrintedFor.SelectedItem.Text.ToUpper
                            Case "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER", "END OF OCTOBER", "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY"
                                If ddlGrade.SelectedValue = "KG1" Then
                                    .reportPath = Server.MapPath("../Rpt/rptIGCSE_KG1_Term1.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/rptIGCSE_KG2_Term1.rpt")
                                End If
                            Case "END OF FEBRUARY", "END OF MARCH", "END OF APRIL", "END OF MAY", "END OF JUNE"
                                If ddlAca_Year.SelectedItem.Text = "2012-2013" Then
                                    .reportPath = Server.MapPath("../Rpt/rptIGCSE_KG1_KG2_Term2_2012-13.rpt")
                                Else
                                    .reportPath = Server.MapPath("../Rpt/rptIGCSE_KG1_KG2_Term2.rpt")
                                End If
                            Case Else
                                .reportPath = Server.MapPath("../Rpt/rptCBSE_KinderGarten.rpt")
                        End Select
                    End If
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSE_I_KinderGarten()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()

            If ddlAca_Year.SelectedValue = "1231" Then
                .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_KG_TERMREPORT_GHS.rpt")
            ElseIf ((ddlReportType.SelectedValue = "4232") Or (ddlReportType.SelectedValue = "4685")) Then
                .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_KG_TERMREPORT_GHS_new.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_KG_TERMREPORT.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_TERMREPORT_MAG()
        Dim param As New Hashtable
       
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "GRADE_1_4_FORMAT"
                    .reportPath = Server.MapPath("../Rpt/MAG/rptTERMREPORT_MAG_01_04.rpt")
                Case "GRADE_5_FORMAT"
                    .reportPath = Server.MapPath("../Rpt/MAG/rptTERMREPORT_MAG_05.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/MAG/rptTERMREPORT_MAG_06.rpt")

            End Select




        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub




    Private Sub GenerateCBSE_I_TERMREPORT_01_02()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass()


            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_01_TERMREPORT.rpt")



        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEFinalReport01_04(ByVal GRD_ID As String)

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        'param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            '.Photos = GetPhotoClass()
            Select Case GRD_ID
                Case "01", "02", "03", "04"
                    If ((ddlAca_Year.SelectedValue = "1065") Or (ddlAca_Year.SelectedValue = "1147")) Then
                        .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_01_04_2015-16.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_01_04.rpt")
                    End If

                Case "11", "12"
                    If ((ddlAca_Year.SelectedValue = 1063)) Then
                        GenerateTermlyReport_OOEHS_Dubai()

                    Else
                        Select Case GetReportType()
                            Case "GRADE_11_12_FORMAT1"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT1.rpt")
                            Case "GRADE_11_12_FORMAT3"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT3.rpt")
                            Case "GRADE_11_12_FORMAT8"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                            Case "GRADE_11_12_FORMAT9"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT9.rpt")
                            Case "GRADE_11_12_FORMAT10"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT10.rpt")
                            Case "GRADE_11_12_FORMAT11"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT11.rpt")
                            Case "GRADE_11_FORMAT_NEW1"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT_NEW1.rpt")
                            Case "GRADE_11_FORMAT_OOW"
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT_OOW.rpt")
                            Case Else
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT2.rpt")
                        End Select
                    End If
                Case "05", "06", "07", "08"
                    If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                        .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
                    ElseIf ((ddlAca_Year.SelectedValue = 1271) Or (ddlAca_Year.SelectedValue = 1406) Or (ddlAca_Year.SelectedValue = 1540) Or (ddlAca_Year.SelectedValue = 1769)) Then
                        .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_01_04.rpt")
                    ElseIf ddlAca_Year.SelectedValue = 1353 Then
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_FINALREPORT_05_08_2017-18.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
                    End If
                Case "09"
                    Select Case hfReportFormat.Value
                        Case "CBSE_FINALREPORT_09"
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                            .reportPath = Server.MapPath("../Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                            .Photos = GetPhotoClass()
                        Case "CBSE_FINALREPORT_09_2013"
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                            .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSEFinalReport_05_10_2013.rpt")
                            .Photos = GetPhotoClass()
                        Case Else
                            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
                            Else
                                If Session("sbsuid") = "121012" Or Session("sbsuid") = "121013" Or Session("sbsuid") = "121014" Or Session("sbsuid") = "123006" Then
                                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
                                Else
                                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                    param.Add("@IMG_TYPE", "LOGO")
                                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                                    .reportPath = Server.MapPath("../Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                                    .Photos = GetPhotoClass()
                                End If
                            End If
                    End Select
                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
            End Select
            .reportParameters = param
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateIGCSEFinalREportS3S4()

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        'param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"


            Select Case GetReportType()
                Case "GRADE_11_12_FORMAT1"
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT1.rpt")
                Case "GRADE_11_12_FORMAT3"
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT3.rpt")
                Case "GRADE_11_12_FORMAT8"
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                Case "GRADE_11_12_FORMAT9"
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT9.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT2.rpt")
            End Select

            .reportParameters = param
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '       Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_TermReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))
        'param.Add("SA_HEADER", "SA")
        'param.Add("FA1_PERC", "10%")
        'param.Add("FA2_PERC", "10%")
        'param.Add("SA_PERC", "20%")
        'param.Add("TOTAL_PERC", "60%")
        'param.Add("TOTAL_FA_PERC", "20%")

        'If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
        '    param.Add("FA_HEADER1", "FA1")
        '    param.Add("FA_HEADER2", "FA2")
        'Else
        '    param.Add("FA_HEADER1", "FA3")
        '    param.Add("FA_HEADER2", "FA4")
        'End If

        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If (((ddlAca_Year.SelectedValue = 1546) Or (ddlAca_Year.SelectedValue = 1770)) And (ddlGrade.SelectedValue.ToString = "01")) Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_01_04_Grade.rpt")
            ElseIf (ddlAca_Year.SelectedValue = 1777) Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_01_04_NMS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_TERM_REPORT_01_04.rpt")
            End If



        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_FinalReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            If ((ddlAca_Year.SelectedValue = 1546) And (ddlGrade.SelectedValue = "01")) Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_01_KGS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_01_04.rpt")
            End If






        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_FinalReport05_08()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_05_08.rpt")


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_FinalReport09()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("../Rpt/rptCBSE_FINAL_REPORT_09_10 .rpt")


        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCBSETermReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If Session("SBSUID") = "131003" Then
                .reportPath = Server.MapPath("../Rpt/rptGEP_SUMMATIVE_ASSESSMENT_REPORT_01_02.rpt")
            ElseIf ((ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 2 REPORT") And ((ddlAca_Year.SelectedValue = "1065") Or (ddlAca_Year.SelectedValue = "1147"))) Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04_2015-16.rpt")
            ElseIf ddlAca_Year.SelectedValue = "1533" Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04_OOF_IGCSE.rpt")
            ElseIf ((ddlAca_Year.SelectedValue = "1547") Or (ddlAca_Year.SelectedValue = "1772")) Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04_OIS.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSE_I_TermReport01_03()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_01_04.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReports()
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            rs.Dispose()

        Catch ex As Exception

        End Try
        'GC.Collect()
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
    Protected Sub btnGenerateReport_Click1(sender As Object, e As EventArgs) Handles btnGenerateReport.Click

    End Sub
End Class
