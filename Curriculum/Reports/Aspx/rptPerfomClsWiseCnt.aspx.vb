﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System

Imports System.Collections.Generic
Imports System.Collections
Imports DevExpress.Data.Linq
Imports System.Linq
Imports Telerik.Web.UI
Imports GemBox.Spreadsheet
Imports System.IO
Partial Class Curriculum_Reports_ASPX_rptPerfomClsWiseCnt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                'If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C102619")) Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else
                'calling pageright class to get the access rights

                lblReportCaption.Text = Mainclass.GetMenuCaption(ViewState("MainMnu_code"))
                    If ViewState("MainMnu_code") = "C102619" Then
                        lblSecGrp.Text = "Section"
                        ddlSection.Visible = True
                    ElseIf ViewState("MainMnu_code") = "C103619" Then
                        lblSecGrp.Text = "Group"
                        ddlGroup.Visible = True
                    End If
                    'GETDATA()

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))





                    'New Code Added By Nikunj - 13/Jan/2020
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindTerm()

                ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")
                'If Session("CurrSuperUser") = "Y" Then
                'ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                'Else
                '    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                'End If
                BindSubjects()
                BindSection()

                'End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            If Session("QData") IsNot Nothing Then
                ' GetDataByParameters()
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(cmdExp1)

    End Sub
    Sub BindSubjects()
        Dim grd_id() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                       & " AND SBG_STM_ID=" + grd_id(1)
        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                & " AND SBG_STM_ID=" + grd_id(1) _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    'Added By Nikunj - 13/Jan/2020
    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION FROM [dbo].[term_sub_master] " _
                                & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
                                & " AND TRM_ACD_ID =" + ddlAcademicYear.SelectedItem.Value _
                                & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TSM_DESCRIPTION"
        ddlTerm.DataValueField = "TSM_ID"
        ddlTerm.DataBind()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"



        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                                  & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                  & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                  & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " _
                                  & " WHERE GRM_ACD_ID='" + acdid + "'"

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND GRM_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE gsa_acd_id=" + acdid + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY  ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                        & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID" _
                        & " ,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                        & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                        & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID  WHERE " _
      & "  grm_acd_id=" + acdid & " AND d.SGS_EMP_ID = " & Session("EmployeeId")

        End If


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = " |0"
        ddl.Items.Insert(0, li)
        Return ddl
    End Function
    'Added By Nikunj - 13/Jan/2020
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String) As DropDownList
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_SBM_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        Return ddlSubject
    End Function
    'Added By Nikunj - 13/Jan/2020
    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String) As DropDownList
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        Return ddlSubject
    End Function
    'Added By Nikunj - 13/Jan/2020
    Sub BindSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SCT_ID,SCT_DESCR FROM VW_SECTION_M INNER JOIN VW_GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE SCT_ACD_ID='" + ddlAcademicYear.SelectedValue + "'" _
                                & " AND SCT_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        'If Session("CurrSuperUser") = "Y" Then
        '    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        'Else
        '    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        'End If
        BindSubjects()
        BindSection()
        BindGroup()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)
        'txtTopic.Text = ""
        BindGroup()
        GetSelectedTerm()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTerm()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection()

        ' GetDataByParameters()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        ' GetDataByParameters()
    End Sub
    'Added By Nikunj - 13/Jan/2020
    Function GetSelectedTerm() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlTerm.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str
    End Function
    'Added By Nikunj - 13/Jan/2020
    Function GetSelectedGroups() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlGroup.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str
    End Function

    Sub GetDataByParameters()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim PSection As String = "0"
        If (ddlSection.SelectedItem.Text.ToString = "ALL") Then
            PSection = "0"
        Else
            PSection = ddlSection.SelectedItem.Text
        End If

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@TERM", GetSelectedTerm())
        pParms(2) = New SqlClient.SqlParameter("@GRADE", grade(0))
        pParms(3) = New SqlClient.SqlParameter("@SUBJECTS", ddlSubject.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@SECTION", PSection)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_PUPIL_TRACKER_ATTAINMENT_COUNT", pParms)

        If Not ds Is Nothing Then
            If ds.Tables(0) Is Nothing OrElse ds.Tables(0).Rows.Count > 0 Then
                'ds.WriteXml(Server.MapPath("~/file.xml"))
                'Call DataSetToExcel.Convert(ds, Response, Server.MapPath("~/file.xml"), "ExportData")


                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                If dtEXCEL.Rows.Count > 0 Then
                    Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT")
                    ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    '  ws.HeadersFooters.AlignWithMargins = True
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                    Dim pathSave As String
                    pathSave = "Performance Analytics" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                    ef.Save(cvVirtualPath & pathSave)
                    Dim path = cvVirtualPath & pathSave

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If
            Else
                lblError.Text = "No Records To display with this filter condition....!!!"
                lblError.Focus()
            End If
        End If

        ' Session("QData") = ds.Tables(0)
    End Sub
    Protected Sub cmdExp1_Click(ByVal sender As Object, ByVal e As EventArgs)
        Call GetDataByParameters()
    End Sub

    'Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
    '    GetDataByParameters()
    'End Sub
End Class
