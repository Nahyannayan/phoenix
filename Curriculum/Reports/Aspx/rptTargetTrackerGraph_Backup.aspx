﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTargetTrackerGraph_Backup.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTargetTrackerGraph" title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs+"&ACD_ID="+ACD_IDs,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
   </script>
   <script language="javascript" type="text/javascript">


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);

}
}


function OnTreeClick1(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);
}
}


function CheckUncheckChildren(childContainer, check)
{
var childChkBoxes = childContainer.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = check;
}
}


function CheckUncheckParents(srcChild, check)
{
var parentDiv = GetParentByTagName("div", srcChild);
var parentNodeTable = parentDiv.previousSibling;
if(parentNodeTable)
{
var checkUncheckSwitch;
if(check) //checkbox checked
{
var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
if(isAllSiblingsChecked)
checkUncheckSwitch = true;
else
return; //do not need to check parent if any(one or more) child not checked
}
else //checkbox unchecked
{
checkUncheckSwitch = false;
}
var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
if(inpElemsInParentTable.length > 0)
{
var parentNodeChkBox = inpElemsInParentTable[0];
parentNodeChkBox.checked = checkUncheckSwitch;
//do the same recursively
CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
}
}
}

function AreAllSiblingsChecked(chkBox)
{
var parentDiv = GetParentByTagName("div", chkBox);
var childCount = parentDiv.childNodes.length;
for(var i=0;i<childCount;i++)
{
if(parentDiv.childNodes[i].nodeType == 1)
{
//check if the child node is an element node
if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
{
var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
//if any of sibling nodes are not checked, return false
if(!prevChkBox.checked)
{
return false;
}
}
}
}
return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
{
var parent = childElementObj.parentNode;
while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
{
parent = parent.parentNode;
}
return parent;
}

function UnCheckAll() 
{ 
var oTree=document.getElementById("<%=tvReport.ClientId %>");
childChkBoxes = oTree.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = false;
}

return true;
}


function printTable() {

    var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    disp_setting += "scrollbars=yes,width=1280, height=600, left=100, top=25";
     var content_vlue = document.getElementById('tblChart').innerHTML;
     var docprint = window.open("", "", disp_setting);
    docprint.document.open();
      docprint.document.write('<html><head>');
    docprint.document.write('<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />');
    docprint.document.write('</head><body onLoad="self.print();self.close();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
     docprint.document.write(content_vlue);
    docprint.document.write('</center></body></html>');
    docprint.document.close();
    docprint.focus();
}

</script>
   
 <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" >
                 
              
           <tr>
            <td align="center"  class="matters" style="font-weight: bold;  height: 215px" valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblClm" runat="server" align="center" border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"  class="BlueTableView" style="width: 850px">
                     <tr >
              
                <td class="subheader_img" colspan="7" >
                    TARGET TRACKER GRAPH</td>
              </tr>
             
                   <tr>
                      
                    
                        <td align="left" class="matters">
                            Select Academic Year</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>               
                      </td>
                      
                       <td align="left" class="matters" style="width: 184px">
                            Grade</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters" colspan="2" >
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Width="183px">
                        </asp:DropDownList></td>
                       </tr>   
                    <tr>
                       
                       
                
                        <td align="left" class="matters" style="width: 184px">
                            Select Section</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters" >
                            <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" Width="107px">
                            </asp:DropDownList></td>
                            
                              <td align="left" class="matters" >
                            Select Report Schedule</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" colspan="2">
                            <asp:TreeView id="tvReport" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                ExpandDepth="1" Height="200px" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                PopulateNodesFromClient="False" ShowCheckBoxes="All" style="overflow: auto; text-align: left"
                                Width="293px">
                                <parentnodestyle font-bold="False" />
                                <hovernodestyle font-underline="True" />
                                <selectednodestyle backcolor="White" borderstyle="Solid" borderwidth="1px" font-underline="False"
                                    horizontalpadding="3px" verticalpadding="1px" />
                                <nodestyle font-names="Verdana" font-size="8pt" forecolor="#1B80B6" horizontalpadding="5px"
                                    nodespacing="1px" verticalpadding="2px" />
                            </asp:TreeView></td>
                    </tr>
                   
                          <tr>
                        <td align="left" class="matters" style="width: 184px">
                            Subject</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters">
                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" Width="183px">
                        </asp:DropDownList></td>
                       
                
                        <td align="left" class="matters" style="width: 184px">
                           Group</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters" colspan="2">
                            <asp:DropDownList ID="ddlGroup" AutoPostBack="true" runat="server" 
                                Width="150px">
                            </asp:DropDownList></td>
                    </tr>
                    
                          <tr>
                        <td align="left" class="matters" style="width: 184px">
                            Student ID</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters" >
                        <asp:TextBox ID="txtStudentID" runat="server"  Width="183px"> </asp:TextBox></td>
                       
                
                        <td align="left" class="matters" style="width: 184px">
                           Student Name</td>
                        <td class="matters">
                            :</td>
                        <td align="left" class="matters" >
                            <asp:TextBox ID="txtStudentName"  runat="server"     Width="150px"> </asp:TextBox></td>
                            <td>
                             <asp:Button ID="btnSearch" runat="server" Text="View" CssClass="button" TabIndex="4" Width="51px"  />
                            </td>
                    </tr>
                     
                     
                 
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
               </td></tr>
           
    
</table>
    <asp:HiddenField id="h_STU_IDs" runat="server">
    </asp:HiddenField>
    
    <asp:HiddenField id="hfbDownload" runat="server">
    </asp:HiddenField>
     



    <asp:HiddenField id="hfPageindex" runat="server">
    </asp:HiddenField>
         



</asp:Content>

