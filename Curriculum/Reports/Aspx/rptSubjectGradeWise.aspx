<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSubjectGradeWise.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSubjectGradeWise" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);
}
}


function CheckUncheckChildren(childContainer, check)
{
var childChkBoxes = childContainer.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = check;
}
}


function CheckUncheckParents(srcChild, check)
{
var parentDiv = GetParentByTagName("div", srcChild);
var parentNodeTable = parentDiv.previousSibling;
if(parentNodeTable)
{
var checkUncheckSwitch;
if(check) //checkbox checked
{
var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
if(isAllSiblingsChecked)
checkUncheckSwitch = true;
else
return; //do not need to check parent if any(one or more) child not checked
}
else //checkbox unchecked
{
checkUncheckSwitch = false;
}
var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
if(inpElemsInParentTable.length > 0)
{
var parentNodeChkBox = inpElemsInParentTable[0];
parentNodeChkBox.checked = checkUncheckSwitch;
//do the same recursively
CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
}
}
}

function AreAllSiblingsChecked(chkBox)
{
var parentDiv = GetParentByTagName("div", chkBox);
var childCount = parentDiv.childNodes.length;
for(var i=0;i<childCount;i++)
{
if(parentDiv.childNodes[i].nodeType == 1)
{
//check if the child node is an element node
if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
{
var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
//if any of sibling nodes are not checked, return false
if(!prevChkBox.checked)
{
return false;
}
}
}
}
return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
{
var parent = childElementObj.parentNode;
while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
{
parent = parent.parentNode;
}
return parent;
}


function UnCheckAll() 
{ 
var oTree=document.getElementById("<%=tvGrade.ClientId %>");
childChkBoxes = oTree.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = false;
}

return true;
} 




</script>

   

    <table align="center" style="width: 65%">
        <tr align="left">
            <td>
                &nbsp;</td>
        </tr>
        <tr align="left">
            <td style="width: 457px" >
                <table align="center" border="1" bordercolor="#1b80b6" class="BlueTableView"  cellpadding="5" cellspacing="0" style="width: 542px">
                    <tr class="subheader_img">
                        <td align="center" colspan="3"  style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    <asp:Label ID="lblCaption" runat="server" Text="Step 1 :Select Academic Year/Grade"></asp:Label></font></div>
                        </td>
                    </tr>
                      
                    <tr>
                        <td align="left" class="matters" >
                            Academic Year</td>
                        <td class="matters" align="center">
                            :</td>
                        <td align="left" class="matters"  style="text-align: left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    
                       <tr> <td align="center" class="matters" colspan="3">
                         <asp:TreeView id="tvGrade" runat="server" BorderStyle="Solid" BorderWidth="1px"
                               EnableClientScript="True" ExpandDepth="1" Height="215px" NodeIndent="15" onclick="return OnTreeClick(event);"
                               ShowCheckBoxes="All" style="overflow: auto; text-align: left" Width="180px">
                               <parentnodestyle font-bold="True" font-names="Verdana" font-size="6pt" forecolor="#1B80B6" />
                               <selectednodestyle backcolor="White" borderstyle="Solid" borderwidth="1px" font-underline="False"
                                   horizontalpadding="3px" verticalpadding="1px" />
                               <nodestyle font-names="Verdana" font-size="7pt" forecolor="#1B80B6" horizontalpadding="3px"
                                   nodespacing="1px" verticalpadding="2px" />
                           </asp:TreeView>
                   </td>
                    </tr>
                    
                  
         
                    
                      
                    
                   
                    <tr>
                        <td align="center" class="matters" colspan="3" >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" OnClick="btnGenerateReport_Click" />&nbsp;
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
    </table>

</asp:Content>

