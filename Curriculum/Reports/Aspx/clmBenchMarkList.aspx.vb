﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports CURRICULUM
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports GemBox.Spreadsheet
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_clmBenchMarkList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280176") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    lstAcademiyear = PopulateAcademicYear(lstAcademiyear, Session("clm"), Session("sbsuid"))
                    PopulateGrade(ddlGrade, Session("Current_ACD_ID").ToString)
                    BindSection()

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ''lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        'PopulateGrade(ddlGrade, ddlGrade.SelectedValue.ToString)
        BindSection()

    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim ds As New DataSet

        Dim lstrExportType As Integer
        Dim strAcdIds As String = ""
        'For i = 0 To lstAcademiyear.Items.Count - 1
        '    If lstAcademiyear.Items(i).Selected = True Then
        '        If strAcdIds <> "" Then
        '            strAcdIds += "|"
        '        End If
        '        strAcdIds += lstAcademiyear.Items(i).Value

        '    End If
        'Next


        Dim collection As IList(Of RadComboBoxItem) = lstAcademiyear.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                strAcdIds += item.Value
                If strAcdIds <> "" Then
                    strAcdIds += "|"
                End If
            Next
            strAcdIds = strAcdIds.TrimEnd("|")
        Else
            If lstAcademiyear.SelectedIndex > 0 Then
                strAcdIds = lstAcademiyear.SelectedItem.Value
            End If
        End If



        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@GRD_ID", ddlGrade.SelectedItem.Value)
        param(1) = New SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
        param(2) = New SqlParameter("@ACD_IDs", strAcdIds)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RPT].[rptbenchmark]", param)


        Dim dtEXCEL As New DataTable
        dtEXCEL = ds.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("CurriculumBenchmarklist")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True


            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "CurriculumBenchmarklist.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "CurriculumBenchmarklist" + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()

        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " , GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid

        'If ViewState("MainMnu_code") <> "C330061" Then
        '    If Session("sbsuid") <> "125005" And Session("sbsuid") <> "125010" And Session("sbsuid") <> "135010" And Session("sbsuid") <> "125018" And Session("sbsuid") <> "125002" And Session("sbsuid") <> "126008" And Session("sbsuid") <> "125011" And Session("sbsuid") <> "115002" Then
        '        If Session("sbsuid") = "151001" And Session("clm") <> 1 Then
        '        Else
        '            str_query += "  and grm_grd_id<>'10'"
        '        End If
        '    End If
        'End If

        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateAcademicYear(ByVal lstAcademiyear As RadComboBox, ByVal clm As String, ByVal bsuid As String)
        lstAcademiyear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim param(14) As SqlClient.SqlParameter
        Dim ds As DataSet
        param(0) = New SqlParameter("@clmid", clm)
        param(1) = New SqlParameter("@currAcdId", Session("Current_ACD_ID").ToString)
        param(2) = New SqlClient.SqlParameter("@bsuId", bsuid)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.populateBenchmarkAcademicyear", param)


        lstAcademiyear.DataSource = ds
        lstAcademiyear.DataTextField = "acy_descr"
        lstAcademiyear.DataValueField = "acd_id"
        lstAcademiyear.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then


            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            lstAcademiyear.ClearSelection()
            '' lstAcademiyear.Items(lstAcademiyear.Items.IndexOf(li)).Selected = True
        End If


        Return lstAcademiyear
    End Function
    Sub BindSection()
        Dim li As New ListItem
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + lstAcademiyear.SelectedItem.Value.ToString

        ''  Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + ddlGrade.SelectedItem.Value + "'"

        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Insert(0, New ListItem("All", "0"))

        Dim sct_ids As String
        Dim i As Integer
        For i = 0 To ddlSection.Items.Count - 1
            If sct_ids <> "" Then
                sct_ids += ","
            End If
            sct_ids += ddlSection.Items(i).Value
        Next



    End Sub

#End Region
End Class
