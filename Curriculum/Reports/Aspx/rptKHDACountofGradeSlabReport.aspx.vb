﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM


Partial Class Curriculum_Reports_Aspx_rptKHDACountofGradeSlabReport

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                h_MnuCode.Value = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400101" And ViewState("MainMnu_code") <> "C290372" And ViewState("MainMnu_code") <> "C400119") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    cblGrade = PopulateGrade(cblGrade, ddlAcademicYear.SelectedValue.ToString)


                    'BindSection()
                    'BindReports()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGeneratePDF)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As CheckBoxList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                  & " ,grm_grd_id AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"

        '& " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()

        Return ddl
    End Function

    Protected Sub cblGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblGrade.SelectedIndexChanged
        Dim grade As String()
        Dim str As String = ""
        Dim i As Integer
        For i = 0 To cblGrade.Items.Count - 1
            If cblGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += cblGrade.Items(i).Value
            End If
        Next
        'grade = cblGrade.SelectedValue.Split("|")
        BindSubjects(str)
        BindReports()
        BindHeaders()
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        Dim grade As String()
        Dim str As String = ""
        Dim i As Integer
        For i = 0 To cblGrade.Items.Count - 1
            If cblGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += cblGrade.Items(i).Text
            End If
        Next
        'grade = cblGrade.SelectedValue.Split("|")
        BindSubjects(str)
        BindReports()
    End Sub

    Sub BindSubjects(ByVal grades As String)

        cblSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S AS A" _
        '                        & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        '                        & " AND SBG_PARENT_ID=0 AND SBG_GRD_ID NOT IN ('KG1','KG2') AND SBG_GRD_ID='" + grade + "'"


        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@GRD_IDS", grades)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_ANALYSISREPORT_SUBJECTS", param)

        cblSubject.DataSource = ds
        cblSubject.DataTextField = "SBG_DESCR"
        cblSubject.DataValueField = "SBG_DESCR"
        cblSubject.DataBind()
    End Sub

    Sub BindReports()
        ddlCurSchedule.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str As String = ""
        Dim i As Integer
        'Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
        '                        & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
        '                        & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
        '                        & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
        '                        & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"

        For i = 0 To cblGrade.Items.Count - 1
            If cblGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += cblGrade.Items(i).Text
            End If
        Next

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@GRD_IDS", str)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_CLMSCHEDULES", param)

        ddlCurSchedule.DataSource = ds
        ddlCurSchedule.DataTextField = "RPF_DESCR"
        ddlCurSchedule.DataValueField = "RPF_DESCR"
        ddlCurSchedule.DataBind()


    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnGeneratePDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneratePDF.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        Dim strSbg As String = ""
        Dim strGrd As String = ""

        Dim i As Integer
        For i = 0 To cblSubject.Items.Count - 1
            If cblSubject.Items(i).Selected = True Then
                If strSbg <> "" Then
                    strSbg += "|"
                End If
                strSbg += cblSubject.Items(i).Text
            End If
        Next

        For i = 0 To cblGrade.Items.Count - 1
            If cblGrade.Items(i).Selected = True Then
                If strGrd <> "" Then
                    strGrd += "|"
                End If
                strGrd += cblGrade.Items(i).Text
            End If
        Next

        
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_IDS", strGrd)
        param.Add("@SBG_DESCR", strSbg)
        param.Add("@RPF_DESCR", ddlCurSchedule.SelectedItem.Text)
        param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
        param.Add("@ACD_DESCR", ddlAcademicYear.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/KHDA/rptKHDACountofGradeSlabsReport.rpt")
        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If

    End Sub

    Protected Sub ddlCurSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurSchedule.SelectedIndexChanged
        BindHeaders()
    End Sub

    Protected Sub BindHeaders()

        ddlHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_HEADER FROM RPT.REPORT_SETUP_D AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSD_RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS C ON A.RSD_RSM_ID=C.RSM_ID" _
                             & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND RPF_DESCR='" + ddlCurSchedule.SelectedItem.Text + "' AND ISNULL(RSD_BDIRECTENTRY,1)=0 AND ISNULL(RSD_bALLSUBJECTS,0)=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_HEADER"
        ddlHeader.DataBind()

    End Sub

End Class
