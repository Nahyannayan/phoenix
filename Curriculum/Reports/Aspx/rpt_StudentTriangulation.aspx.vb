﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_rpt_GradePerformance_Summary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C500393") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    'BindSubjects()
                    BindGrade()
                    BindReportType()
                    BindSection("")

                    'BindReport()
                    'BindHeader()
                    trRange.Visible = False
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
                Session("Comparision") = Nothing
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubjects(grades As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_GRD_ID NOT IN('KG1','KG2') " _
                                & " AND SBG_GRD_ID  IN ('" + grades + "')" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_PARENT_ID=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M  " _
                                  & " INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID WHERE" _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"

        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("Select", "00", True))
    End Sub

    Sub BindReport(grades As String)
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        str_query = "SELECT DISTINCT RPF_DESCR AS RPF_DESCR,RPF_ID FROM RPT.REPORT_SETUP_M AS A" _
                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID" _
                 & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND RPF_DESCR NOT LIKE '%REVIEW REPORT%' AND RPF_DESCR NOT LIKE '%FEEDBACK REPORT%' " _
                 & " AND RSG_GRD_ID NOT IN ('KG1','KG2')" _
                 & " AND RSG_GRD_ID IN ('" + grades + "')" _
                 & " ORDER BY RPF_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
        'ddlPrintedFor.Items.Insert(0, New ListItem("Select ", "NA"))

    End Sub


    Function GetSelectedHeader() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstHeader.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Text
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function
    Function GetSelectedReport() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Text
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function
    Function GetSelectedReportValue() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function
    Function GetSelectedHeaderValue() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstHeader.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function

    Sub CallReport()
        Dim criteria1 As String = Val(txt1From.Text).ToString + "|" + Val(txt1To.Text).ToString
        Dim criteria2 As String = Val(txt2From.Text).ToString + "|" + Val(txt2To.Text).ToString
        Dim criteria3 As String = Val(txt3From.Text).ToString + "|" + Val(txt3To.Text).ToString

        Dim param As New Hashtable
        
        'If ddlrpType.SelectedValue <> "1" And ddlrpType.SelectedValue <> "5" And ddlrpType.SelectedValue <> "3" And ddlrpType.SelectedValue <> "6" And ddlrpType.SelectedValue <> "4" And ddlrpType.SelectedValue <> "8" Then
        '    param.Add("@IMG_BSU_ID", Session("SBSUID"))
        '    param.Add("@IMG_TYPE", "LOGO")
        'End If

        'param.Add("@BSU_ID", Session("SBSUID"))
        If ddlrpType.SelectedValue <> "19" Then
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            param.Add("@RPF_ID", GetSelectedReportValue())
            param.Add("@SBG_ID", ddlSubject.SelectedItem.Value)
            param.Add("@SGR_ID", ddlGroup.SelectedItem.Value)
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
        End If
        If ddlrpType.SelectedValue = "16" Or ddlrpType.SelectedValue = "17" Or ddlrpType.SelectedValue = "20" Then
            param.Add("@FromMark1", IIf(chkRange.Checked, txt1From.Text, DBNull.Value))
            param.Add("@ToMark1", IIf(chkRange.Checked, txt1To.Text, DBNull.Value))
            param.Add("@FromMark2", IIf(chkRange.Checked, txt2From.Text, DBNull.Value))
            param.Add("@ToMark2", IIf(chkRange.Checked, txt2To.Text, DBNull.Value))
            param.Add("@FromMark3", IIf(chkRange.Checked, txt3From.Text, DBNull.Value))
            param.Add("@ToMark3", IIf(chkRange.Checked, txt3To.Text, DBNull.Value))
        End If
        If ddlrpType.SelectedValue = "18" Or ddlrpType.SelectedValue = "19" Then
            Dim dt As DataTable = Session("Comparision")
            For i As Integer = 1 To 9
                If i <= dt.Rows.Count Then
                    param.Add("@FromExam" + CType(i, String), dt.Rows(i - 1)("Eid1"))
                    param.Add("@ToExam" + CType(i, String), dt.Rows(i - 1)("Eid2"))
                Else
                    param.Add("@FromExam" + CType(i, String), 0)
                    param.Add("@ToExam" + CType(i, String), 0)
                End If
            Next
        End If
        If ddlrpType.SelectedValue = "19" Then
            param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
            param.Add("@SBG_IDS", ddlSubject.SelectedItem.Value)
            param.Add("@STU_TYPE", "All")
            param.Add("@SGR_ID", ddlGroup.SelectedItem.Value)
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
            param.Add("@RPF_IDS", GetSelectedReportValue())
            param.Add("@RSD_IDS", GetSelectedHeaderValue())
            param.Add("@bCRITERIA", False)
            param.Add("@bINCLUDETC", False)
            param.Add("@bDISPLAYGRADE", False)
        End If
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ddlrpType.SelectedValue = "12" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationSkillwise.rpt")
            ElseIf ddlrpType.SelectedValue = "13" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationSkillwiseGraph.rpt")
            ElseIf ddlrpType.SelectedValue = "14" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationConsolidated.rpt")
            ElseIf ddlrpType.SelectedValue = "15" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationAverage.rpt")
            ElseIf ddlrpType.SelectedValue = "16" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationSPL.rpt")
            ElseIf ddlrpType.SelectedValue = "17" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationProgress.rpt")
            ElseIf ddlrpType.SelectedValue = "18" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationTracker.rpt")
            ElseIf ddlrpType.SelectedValue = "19" Then
                .reportPath = Server.MapPath("../Rpt/rptStudentTriangulationReport.rpt")
            ElseIf ddlrpType.SelectedValue = "20" Then
                .reportPath = Server.MapPath("../Rpt/rptSkillwiseSubjectPerformance.rpt")
            ElseIf ddlrpType.SelectedValue = "21" Then
                .reportPath = Server.MapPath("../Rpt/rptGradeTriangulationSkillwiseGraph.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub BindHeader(grades As String, ReportCard As String)
        lstHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSD_RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS C ON A.RSD_RSM_ID=C.RSM_ID" _
                             & " INNER JOIN RPT.REPORTSETUP_GRADE_S as D on D.RSG_RSM_ID=C.RSM_ID" _
                             & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND RPF_DESCR IN (" + ReportCard + ") AND ISNULL(RSD_BDIRECTENTRY,1)=0 AND ISNULL(RSD_bALLSUBJECTS,0)=1 " _
                             & " AND RSG_GRD_ID NOT IN ('KG1','KG2') AND " _
                             & " RSG_GRD_ID IN ('" + grades + "')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub
    Sub BindReportType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT * FROM [RPT].[ANALYSIS_REPORT_TYPE] WHERE ART_MNU_CODE='" + ViewState("MainMnu_code") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlrpType.DataSource = ds
        ddlrpType.DataTextField = "ART_NAME"
        ddlrpType.DataValueField = "ART_ID"
        ddlrpType.DataBind()
        ddlrpType.Items.Insert(0, New ListItem("Select ", "0"))

    End Sub

    Sub BindComparisions(grades As String, ReportCard As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT A.RPF_DESCR AS RPF_DESCR ," _
        & "                 A.RPF_ID AS RPF_ID " _
        & "FROM (                                                              " _
        & "	SELECT DISTINCT RPF_DESCR AS RPF_DESCR ,RPF_ID AS RPF_ID          " _
        & "	FROM RPT.REPORT_SETUP_M AS A                                      " _
        & "	INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON                        " _
        & "	A.RSM_ID=B.RPF_RSM_ID                                             " _
        & "	INNER JOIN RPT.REPORTSETUP_GRADE_S ON                             " _
        & "	RSM_ID=RSG_RSM_ID AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        & "	AND RPF_DESCR IN (" + ReportCard + ") AND RPF_DESCR NOT LIKE '%REVIEW REPORT%' AND                          " _
        & "	RPF_DESCR NOT LIKE '%FEEDBACK REPORT%'  AND                       " _
        & "	RSG_GRD_ID NOT IN ('KG1','KG2') AND                               " _
        & "	RSG_GRD_ID IN ('" + grades + "')                                    " _
        & "	)A                                                                " _
        & "ORDER BY A.RPF_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlExam1.DataSource = ds
        ddlExam1.DataTextField = "RPF_DESCR"
        ddlExam1.DataValueField = "RPF_ID"
        ddlExam1.DataBind()
        ddlExam1.Items.Insert(0, New ListItem("Select ", "NA"))

        ddlExam2.DataSource = ds
        ddlExam2.DataTextField = "RPF_DESCR"
        ddlExam2.DataValueField = "RPF_ID"
        ddlExam2.DataBind()
        ddlExam2.Items.Insert(0, New ListItem("Select ", "NA"))

    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindReportType()
        BindSection("")
    End Sub
    Protected Sub ddlrpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlrpType.SelectedIndexChanged
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ART_bRANGE FROM [RPT].[ANALYSIS_REPORT_TYPE] WHERE ART_ID= " + ddlrpType.SelectedItem.Value
        Dim range As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If (range) Then
            chkRange.Checked = True
            trRange.Visible = True
        Else
            chkRange.Checked = False
            trRange.Visible = False
        End If
        Dim str_query1 As String = "SELECT ART_bCompare FROM [RPT].[ANALYSIS_REPORT_TYPE] WHERE ART_ID= " + ddlrpType.SelectedItem.Value
        Dim compare As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query1)
        If (compare) Then
            chkCompare.Checked = True
            trComparision.Visible = True
            trGrid.Visible = True
        Else
            chkCompare.Checked = False
            trComparision.Visible = False
            trGrid.Visible = False
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub chkRange_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRange.CheckedChanged
        If chkRange.Checked = True Then
            trRange.Visible = True
        Else
            trRange.Visible = False
        End If
    End Sub
    Protected Sub chkCompare_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCompare.CheckedChanged
        If chkCompare.Checked = True Then
            trComparision.Visible = True
            trGrid.Visible = True
        Else
            trComparision.Visible = False
            trGrid.Visible = False
        End If
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        'Dim grades As String = "'" + GetSelectedGrades().Replace("|", "','") + "'"
        Dim grades As String = ddlGrade.SelectedItem.Text
        Dim Reportcard As String = "'" + GetSelectedReport().Replace("|", "','") + "'"
        BindReport(grades)
        BindSubjects(grades)
        BindSection("")
        BindGroup()
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        'Dim grades As String = "'" + GetSelectedGrades().Replace("|", "','") + "'"
        Dim grades As String = ddlGrade.SelectedItem.Text
        Dim Reportcard As String = "'" + GetSelectedReport().Replace("|", "','") + "'"
        BindHeader(grades, Reportcard)
        BindComparisions(grades, Reportcard)
        Session("Comparision") = Nothing
    End Sub

    Protected Sub btnSetExams_Click(sender As Object, e As EventArgs)
        trGrid.Visible = True
        Dim dt As New DataTable
        If Session("Comparision") Is Nothing Then
            dt.Columns.Add("Exam1", Type.GetType("System.String"))
            dt.Columns.Add("Exam2", Type.GetType("System.String"))
            dt.Columns.Add("EId1", GetType(Integer))
            dt.Columns.Add("EId2", GetType(Integer))
        Else
            dt = Session("Comparision")
        End If
        Dim NewRow As DataRow = dt.NewRow()
        NewRow("Exam1") = ddlExam1.SelectedItem.Text
        NewRow("Exam2") = ddlExam2.SelectedItem.Text
        NewRow("EId1") = ddlExam1.SelectedItem.Value
        NewRow("EId2") = ddlExam2.SelectedItem.Value
        dt.Rows.Add(NewRow)
        gvComparision.DataSource = dt
        gvComparision.DataBind()
        Session("Comparision") = dt

    End Sub
    Sub BindSection(ByVal sctid As String)

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND GRM_GRD_ID='" + ddlGrade.SelectedItem.Value + "' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        'Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        If sctid <> "" Then
            str_query += " and sct_id=" + sctid
        End If
        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    Sub BindGroup()
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        strCondition += " AND SGR_GRD_ID='" + grade(0) + "'"
        strCondition += " AND SGR_SBG_ID='" & ddlSubject.SelectedValue & "'"
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
            " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
            " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " & _
         " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "DESCR2"
        ddlGroup.DataValueField = "ID"
        ddlGroup.DataBind()
        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlGroup.Items.Insert(0, li)
    End Sub
    Protected Sub rblChoice_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (rblChoice.SelectedItem.Value) Then
            trGroup.Visible = True
            trSection.Visible = False
            ddlSection.ClearSelection()
        Else
            trGroup.Visible = False
            trSection.Visible = True
            ddlGroup.ClearSelection()
        End If
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindGroup()
    End Sub
End Class