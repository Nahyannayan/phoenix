Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptTeacherWiseGroups
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" OrElse ((ViewState("MainMnu_code") <> "C970015") _
            AndAlso (ViewState("MainMnu_code") <> CURRICULUM.CURR_CONSTANTS.MNU_RPT_SUBJECTLIST_BY_TEACHER)) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindStream()
                BindDepartMent()
                BindGrade()
                BindTeacher()

                Select Case ViewState("MainMnu_code")
                    Case "C970015"
                        lblHeader.Text = "Teacher Wise Groups"
                    Case CURRICULUM.CURR_CONSTANTS.MNU_RPT_SUBJECTLIST_BY_TEACHER
                        lblHeader.Text = "Subject List by Teacher"
                End Select
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindDepartMent()
        lstDPT.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT DPT_ID,DPT_DESCR FROM DEPARTMENT_M AS A " _
                               & " INNER JOIN EMPLOYEE_M AS B ON A.DPT_ID=B.EMP_DPT_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                               & " ORDER BY DPT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstDPT.DataSource = ds
        lstDPT.DataTextField = "DPT_DESCR"
        lstDPT.DataValueField = "DPT_ID"
        lstDPT.DataBind()
    End Sub

    Sub BindStream()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT STM_ID,STM_DESCR FROM STREAM_M AS A INNER JOIN" _
                                & " GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & "  ORDER BY STM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If
    End Sub
    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER " _
                                  & " FROM GRADE_BSU_M AS A INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID" _
                                  & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND " _
                                  & " GRM_STM_ID=" + ddlStream.SelectedValue.ToString + " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindTeacher()
        lstTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') AS EMP_NAME," _
                                & " EMP_FNAME, EMP_MNAME, EMP_LNAME, EMP_ID" _
                                & " FROM EMPLOYEE_M AS A INNER JOIN " _
                                & " GROUPS_TEACHER_S AS B ON A.EMP_ID=B.SGS_EMP_ID AND SGS_TODATE IS NULL" _
                                & " INNER JOIN GROUPS_M AS C ON B.SGS_SGR_ID=C.SGR_ID" _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString


        str_query = FilterTeacher(str_query)

        str_query += " ORDER BY EMP_FNAME,EMP_LNAME,EMP_MNAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstTeacher.DataSource = ds
        lstTeacher.DataTextField = "emp_name"
        lstTeacher.DataValueField = "emp_id"
        lstTeacher.DataBind()

        ' Dim dv As DataView = ds.Tables(0).DefaultView
        'Session("dvTeacher") = dv
    End Sub

    Function FilterTeacher(ByVal str_query As String) As String
          Dim i As Integer
        Dim dpts As String
        Dim grds As String

      
        For i = 0 To lstDPT.Items.Count - 1
            If lstDPT.Items(i).Selected = True Then
                If dpts <> "" Then
                    dpts += ","
                End If
                dpts += lstDPT.Items(i).Value
            End If
        Next

        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If grds <> "" Then
                    grds += ","
                End If
                grds += "'" + lstGrade.Items(i).Value + "'"
            End If
        Next


        If dpts <> "" Then
            str_query += " AND EMP_DPT_ID IN (" + dpts + ")"
        End If

        If grds <> "" Then
            str_query += " AND  SGR_GRD_ID IN(" + grds + ")"
        End If

        Return str_query
    End Function
    Sub CallReport()

        Dim grdXML As String = ""
        Dim grdTempXML As String
        Dim empXML As String = ""
        Dim empTempXML As String = ""
        Dim i As Integer

        'IF NO GRADE IS SELECTED THE QUERY INCLUDES ALL GRADES
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                grdXML += "<ID><GRM_DISPLAY>" + lstGrade.Items(i).Text + "</GRM_DISPLAY><GRD_ID>" + lstGrade.Items(i).Value + "</GRD_ID></ID>"
            End If
            grdTempXML += "<ID><GRM_DISPLAY>" + lstGrade.Items(i).Text + "</GRM_DISPLAY><GRD_ID>" + lstGrade.Items(i).Value + "</GRD_ID></ID>"
        Next
        If grdXML = "" Then
            grdXML = grdTempXML
        End If

        grdXML = "<IDS>" + grdXML + "</IDS>"


        For i = 0 To lstTeacher.Items.Count - 1
            If lstTeacher.Items(i).Selected = True Then
                empXML += "<ID><EMP_ID>" + lstTeacher.Items(i).Value + "</EMP_ID></ID>"
            End If
            empTempXML += "<ID><EMP_ID>" + lstTeacher.Items(i).Value + "</EMP_ID></ID>"
        Next

        If empXML = "" Then
            empXML = empTempXML
        End If

        empXML = "<IDS>" + empXML + "</IDS>"

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@STM_ID", ddlStream.SelectedValue.ToString)
        param.Add("@EMP_XML", empXML)
        param.Add("@GRD_XML", grdXML)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTeacherWiseGroups.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub GenerateSubjectListByTeacherReport()

        Dim empXML As String = ""
        Dim empTempXML As String = ""
        Dim i As Integer
        For i = 0 To lstTeacher.Items.Count - 1
            If lstTeacher.Items(i).Selected = True Then
                empXML += lstTeacher.Items(i).Value & "|"
            End If
            empTempXML += lstTeacher.Items(i).Value + "|"
        Next
        If empXML = "" Then
            empXML = empTempXML
        End If
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@EMP_ID", empXML)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
 
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSubjectListByTeacher.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region

    Protected Sub lstDPT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDPT.SelectedIndexChanged
        'FilterTeacher()
        BindTeacher()
    End Sub

    Protected Sub lstGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrade.SelectedIndexChanged
        '  FilterTeacher()
        BindTeacher()
    End Sub

    Protected Sub chkTeacher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTeacher.CheckedChanged
        If chkTeacher.Checked = True Then
            For i As Integer = 0 To lstTeacher.Items.Count - 1
                lstTeacher.Items(i).Selected = True
            Next
        Else
            lstTeacher.ClearSelection()
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindStream()
        BindDepartMent()
        BindGrade()
        BindTeacher()

    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindDepartMent()
        BindGrade()
        BindTeacher()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case ViewState("MainMnu_code")
            Case "C970015"
                CallReport()
            Case CURRICULUM.CURR_CONSTANTS.MNU_RPT_SUBJECTLIST_BY_TEACHER
                GenerateSubjectListByTeacherReport()
        End Select

    End Sub
End Class
