<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGradeWiseGroups.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptGradeWiseGroups" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript"  type="text/javascript">
var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvSubjects.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


    function change_chk_state(chkThis)
         {
                  
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                     if (chkstate=true)
                     {
                     
                     }
                 }
              }
          }
function test(val)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
                
                  function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
     </script>
<table id="tbl_ShowScreen" runat="server" align="center" border="0" bordercolor="#1b80b6"
        cellpadding="5" cellspacing="0" style="width: 650px">
        <tr>
            <td align="left" style="width: 761px;">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label>
                      </td>
        </tr>
        
        <tr>
            <td align="center" style="width: 761px">
                <table id="Table1" runat="server" align="center" border="1" class="BlueTableView" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 354px">
                    <tr class="subheader_img">
                        <td align="left" colspan="2" style="height: 16px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Grade Wise Groups</span></font></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="18" style="width: 70px">
                            <table id="Table4" runat="server" align="center" border="0" bordercolor="#1b80b6"
                                cellpadding="12" cellspacing="0" style="width: 420px; height: 137px">
                                 <tr>
                                 <td></td>
                                    <td class="matters" style="height: 8px; width: 168px;">
                                        <asp:Label id="lbaC" runat="server" Text="Academic Year  :" Width="109px"></asp:Label>
                                    </td>
                                    <td class="matters" style="height: 8px; width: 103px;">
                                        <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>    
                                        <td class="matters" style="height: 8px">
                                        <asp:Label id="lbst" runat="server" Text="Stream  :" Width="73px" style="text-align: left"></asp:Label>
                                    </td>
                                    <td class="matters" align="Left" style="height: 8px">
                                        <asp:DropDownList id="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>    
                                        
                                </tr>
                                
                                 <tr>
                                    
                                    <td align="center" class="matters" style="height: 228px" colspan="2">
                                         <table border=".5" bordercolor="#1b80b6" style="height: 393px">
                                          <tr class="subheader_img">
                                            <td align="left"  style="height: 16px" valign="middle">
                                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                             <span style="font-family: Verdana">Grade</span></font></td></tr>
                                           <tr>
                                                <td align="left">
                                                        <asp:CheckBoxList id="lstGrade" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        Height="371px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                                        vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                                        text-align: left" Width="165px" AutoPostBack="True">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                        </td>
                                        
                                         <td align="center" class="matters" colspan="4" style="height: 228px">
                                           <table border=".5" bordercolor="#1b80b6" style="height: 396px">
                                          <tr class="subheader_img">
                                            <td align="left"  style="height: 16px" valign="middle">
                                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                             <span style="font-family: Verdana">Subjects
                                                </span></font></td></tr>
                                             <tr>
          <td align="center" class="matters" colspan="4" style="" valign="top">                              
             <asp:GridView id="gvSubjects" runat="server" AutoGenerateColumns="False"
                        CssClass="gridstyle" Width="300px" AllowPaging="True">
                        <rowstyle cssclass="griditem" height="25px" />
                        <columns>
<asp:TemplateField HeaderText="Available"><EditItemTemplate>
     <asp:CheckBox ID="chkSelect" runat="server"  />
    
</EditItemTemplate>
<HeaderTemplate>
    <table>
    <tr>
    <td align="center">
    Select </td>
    </tr>
    <tr>
    <td align="center">
    <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
    ToolTip="Click here to select/deselect all rows" /></td>
    </tr>
    </table>
    
</HeaderTemplate>
<ItemTemplate>
    <asp:CheckBox id="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox> 
    
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="sbg_id" Visible="False"><ItemTemplate>
         <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="Grade" ><ItemTemplate>
         <asp:Label ID="lblGrm" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
        
</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText="Subjects"><EditItemTemplate>
         <asp:Label ID="Label1" runat="server" Text='<%# Eval("SBG_DESCR") %>'></asp:Label>
         
</EditItemTemplate>
<HeaderTemplate>
         <table style="width: 100%; height: 100%">
         <tr><td align="center">
         <asp:Label ID="lblID" runat="server" CssClass="gridheader_text" EnableViewState="False"
         Text="Subject"></asp:Label></td></tr>
         <tr><td style="width: 100px">
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
         <tr><td style="width: 100px; height: 12px">
         <div id="Div1" class="chromestyle">
         <ul>
         <li><a href="#" rel="dropmenu">
         <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif"  /><span
         style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
         </li></ul></div></td>
         <td style="width: 100px; height: 12px">
         <asp:TextBox ID="txtSubject" runat="server" Width="92px"></asp:TextBox></td>
         <td style="width: 100px; height: 12px" valign="middle">
         <asp:ImageButton ID="btnSubject" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
         OnClick="btnSubject_Click"  />&nbsp;</td>
         </tr>
         </table>
         </td>
          </tr>
         </table>
         
</HeaderTemplate>
<ItemTemplate>
         <asp:Label ID="lblSubject" runat="server"  CommandName="Select"  Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Parent" ShowHeader="False"><HeaderTemplate>
     <table style="width: 100%; height: 100%">
     <tr>
     <td align="center" style="width: 100px; height: 14px;">
     <asp:Label ID="lblParent" runat="server" CssClass="gridheader_text" Text="Parent Subject"
     Width="214px"></asp:Label></td>
     </tr>
      <tr>
      <td style="width: 100px">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
      <td style="width: 100px; height: 12px">
       <div id="Div2" class="chromestyle">
       <ul>
       <li><a href="#" rel="dropmenu1">
        <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif"  /><span
        style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
        </li>
        </ul>
        </div>
        </td>
        <td style="width: 100px; height: 12px">
        <asp:TextBox ID="txtParent" runat="server" Width="150px"></asp:TextBox></td>
        <td style="width: 100px; height: 12px" valign="middle">
        <asp:ImageButton ID="btnParent" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
       OnClick="btnParent_Click"  /></td>
       </tr>
       </table>
        </td>
         </tr>
         </table>
          
</HeaderTemplate>
<ItemTemplate>
          <asp:Label ID="lblParent" runat="server"  Text='<%# BIND("SBG_PARENTS") %>'    Width="214px"></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</columns>
                        <headerstyle cssclass="gridheader_pop" height="25px" />
                        <alternatingrowstyle cssclass="griditem_alternative" />
                    </asp:GridView>
        </td>
         </tr>      
                                        </table>
                                    </td>
                                </tr>
                              <!--  <tr>
                                    <td class="matters" colspan="4">
                                        &nbsp;</td>
                                </tr>-->
                                <tr>
                                   
                                                                   </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="12" style="height: 19px" valign="bottom">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Height="24px"
                                Text="Generate Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                &nbsp;
                <asp:HiddenField id="hfTRV_ID" runat="server">
                </asp:HiddenField>
                <input id="lstValues" runat="server" name="lstValues" style="left: 274px; width: 74px;
                    position: absolute; top: 161px; height: 10px" type="hidden" />
                &nbsp;
            </td>
        </tr>
    </table>
      <div>
        <div id="dropmenu" class="dropmenudiv" style="left: 153px; width: 110px; top: 1px">
            <a href="javascript:test('LI');">
                <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" />
                Any Where</a> <a href="javascript:test('NLI');">
                    <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" />
                    Not In</a> <a href="javascript:test('SW');">
                        <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif" />
                        Starts With</a> <a href="javascript:test('NSW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />
                            Not Start With</a> <a href="javascript:test('EW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />
                                Ends With</a> <a href="javascript:test('NEW');">
                                    <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />
                                    Not Ends With</a>
        </div>
        <div id="dropmenu1" class="dropmenudiv" style="left: 0px; width: 110px; top: -1px">
            <a href="javascript:test1('LI');">
                <img alt="Like" class="img_left" src="../../../Images/operations/like.gif" />
                Any Where</a> <a href="javascript:test1('NLI');">
                    <img alt="Like" class="img_left" src="../../../Images/operations/notlike.gif" />
                    Not In</a> <a href="javascript:test1('SW');">
                        <img alt="Like" class="img_left" src="../../../Images/operations/startswith.gif" />
                        Starts With</a> <a href="javascript:test1('NSW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />
                            Not Start With</a> <a href="javascript:test1('EW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />
                                Ends With</a> <a href="javascript:test1('NEW');">
                                    <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />
                                    Not Ends With</a>
        </div>
     <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                            type="hidden" value="=" />
   
  <script type="text/javascript">

cssdropdown.startchrome("Div1")
cssdropdown.startchrome("Div2")

</script>
</asp:Content>

