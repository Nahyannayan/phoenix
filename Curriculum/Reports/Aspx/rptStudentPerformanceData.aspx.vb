Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Imports Telerik.Web.UI
Partial Class Curriculum_Reports_Aspx_clmConsolidatedFeedback
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code1") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330273") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights


                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    'BindReportCard()

                    ' BindPrintedFor()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")


                    BindSection("")
                    If Session("CurrSuperUser") = "Y" Then
                        LstSubject = PopulateSubjects(LstSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        LstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), LstSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If


                    ' BindHeader()


                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGeneratePDF)
    End Sub

#Region "Private methods"
    Sub checkEmpFormtutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sctId As String = ""
        Dim grdId As String
        Dim stmId As String = ""
        Dim str_query As String = "SELECT ISNULL(SCT_ID,0),ISNULL(SCT_GRD_ID,''),ISNULL(GRM_STM_ID,0) " _
                                 & " FROM SECTION_M INNER JOIN GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                 & "  SCT_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                 & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sctId = reader.GetValue(0).ToString
            grdId = reader.GetString(1)
            stmId = reader.GetValue(2).ToString

            If grdId <> "" Then
                If Not ddlGrade.Items.FindByValue(grdId + "|" + stmId) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(grdId + "|" + stmId).Selected = True
                End If
                lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
                BindSection(sctId)

            End If
        End While

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " and RSM_DESCR like '%FEEDBACK%' ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindReportCardForFormTutor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON RSM_ID=RSG_RSM_ID " _
                                & " INNER JOIN OASIS..SECTION_M AS C ON B.RSG_GRD_ID=C.SCT_GRD_ID AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'" _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub


    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " WHERE grm_acd_id=" + acdid _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Function PopulateSubjects(ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id
        If ViewState("MainMnu_code") = "C300008" Then
            'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
            str_query += "  AND SBG_PARENT_ID=0"

        End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                    & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "


        str_query += " WHERE SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To LstSubject.Items.Count - 1
            If LstSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += LstSubject.Items(i).Value
            End If
        Next
        Return subs
        'Dim str As String = ""
        'Dim collection As IList(Of RadComboBoxItem) = lstSubject.CheckedItems

        'If (collection.Count <> 0) Then
        '    For Each item As RadComboBoxItem In collection
        '        str += item.Value
        '        If str <> "" Then
        '            str += "|"
        '        End If
        '    Next
        '    str = str.TrimEnd("|")
        'Else
        '    If lstSubject.SelectedIndex > 0 Then
        '        str = lstSubject.SelectedItem.Value
        '    End If
        'End If
        'Return str

    End Function


    Sub BindSection(ByVal sctid As String)

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        If sctid <> "" Then
            str_query += " and sct_id=" + sctid
        End If
        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Insert(0, li)


    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub

#End Region

    'Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
    '    BindPrintedFor()
    '    BindHeader()
    '    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

    '    Dim grade As String()
    '    grade = ddlGrade.SelectedValue.Split("|")
    '    If Session("CurrSuperUser") = "Y" Then
    '        lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
    '    Else
    '        lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
    '    End If
    '    BindSection("")


    'End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'BindReportCard()

        'BindPrintedFor()
        'BindHeader()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            hfbDownload.Value = 0
            CallReport()
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Or Session("formtutor") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        BindSection("")

    End Sub



    'Protected Sub chkSubject_CheckedChanged(sender As Object, e As EventArgs) Handles chkSubject.CheckedChanged
    '    If chkSubject.Checked Then
    '        trAsset.Visible = True
    '        trSubject.Visible = True
    '        If Session("CurrSuperUser") = "Y" Then
    '            LstSubject = PopulateSubjects(LstSubject, ddlAcademicYear.SelectedValue.ToString)
    '        Else
    '            LstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), LstSubject, ddlAcademicYear.SelectedValue.ToString)
    '        End If
    '    Else
    '        trAsset.Visible = False
    '        trSubject.Visible = False
    '    End If
    'End Sub

    Protected Sub LstSubject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstSubject.SelectedIndexChanged
        'For Each item As ListItem In LstSubject.Items
        '    If item.Selected = True Then
        '        If LstSubject.SelectedIndex > 0 Then
        '            For i As Integer = 0 To LstSubject.SelectedIndex - 1
        '                LstSubject.Items(i).Enabled = False
        '            Next
        '            For j As Integer = LstSubject.SelectedIndex To LstSubject.Items.Count - 1
        '                LstSubject.Items(j).Enabled = False
        '            Next
        '        Else
        '            For j As Integer = 0 To LstSubject.Items.Count - 1
        '                LstSubject.Items(j).Enabled = False
        '            Next
        '        End If
        '        'Else
        '        '    For j As Integer = 0 To LstSubject.Items.Count - 1
        '        '        LstSubject.Items(j).Enabled = True
        '        '    Next
        '    End If
        'Next
        'If LstSubject.SelectedIndex >= 0 Then
        '    LstSubject.Items(LstSubject.SelectedIndex).Enabled = True
        'Else
        '    For Each item As ListItem In LstSubject.Items
        '        For i As Integer = 0 To LstSubject.Items.Count - 1
        '            LstSubject.Items(i).Enabled = True
        '        Next
        '    Next
        'End If
        BindGroup()
    End Sub

    Protected Sub btnGeneratePDF_Click(sender As Object, e As EventArgs) Handles btnGeneratePDF.Click
        If Page.IsValid Then
            hfbDownload.Value = 1
            CallReport()
        End If
    End Sub

    Sub CallReport()

        Dim param As New Hashtable
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        'CHANGE MENU CODE
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        'param.Add("@ENGSBG_ID", "0")
        'param.Add("@MATSBG_ID", "0")
        'param.Add("@SCISBG_ID", "0")
        param.Add("@GRD_ID", grade(0))
        param.Add("@SBG_ID", getSubjects())
        'param.Add("@RPF_ID", ddlPrintedFor.SelectedValue) 'Need to check
        param.Add("@TYPE", ddlAssest.SelectedValue)
        param.Add("@bsu_id", Session("sbsuid"))
        If rblChoice.SelectedValue = 0 Then
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
            param.Add("@GRP_ID", "0")
        Else
            param.Add("@SCT_ID", "0")
            param.Add("@GRP_ID", ddlGroup.SelectedItem.Value)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            'If (chkSubject.Checked) Then
            '    .reportPath = Server.MapPath("../RPT/rptFeedback_ESM.rpt")

            'Else
            '    .reportPath = Server.MapPath("../RPT/rptFeedback_GEN.rpt")

            'End If
            .reportPath = Server.MapPath("../RPT/rptStudentPerformanceData.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub


    Sub BindGroup()
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        strCondition += " AND SGR_GRD_ID='" + grade(0) + "'"
        strCondition += " AND SGR_SBG_ID='" & LstSubject.SelectedValue & "'"
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
            " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
            " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " & _
         " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'"
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "DESCR2"
        ddlGroup.DataValueField = "ID"
        ddlGroup.DataBind()
    End Sub


    Protected Sub rblChoice_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (rblChoice.SelectedItem.Value) Then
            trGroup.Visible = True
            trSection.Visible = False
            ddlSection.ClearSelection()
        Else
            trGroup.Visible = False
            trSection.Visible = True
            ddlGroup.ClearSelection()
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class