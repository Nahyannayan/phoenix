<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_StudentTriangulation.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_GradePerformance_Summary"
    Title="GEMS OASIS" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll3(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 26) == 'ctl00_cphMasterpage_lstHeader') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_lstGrade') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

        function fnSelectAll1(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_ddlSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center"
                    cellpadding="4" cellspacing="0" style="width: 100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>

                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>

                        <td align="left"  width="20%" valign="middle"><span class="field-label">Grade</span>
                        </td>

                        <td align="left"   width="30%" valign="middle">

                            <%--                <telerik:RadComboBox RenderMode="Lightweight" ID="lstGrade" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"  AutoPostBack="true"
                    Width="193px" Label="Select Grades:" OnSelectedIndexChanged="lstGrade_SelectedIndexChanged"></telerik:RadComboBox>--%>
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged" Width="193px">
                            </asp:DropDownList>

                        </td>

                        <td align="left"  width="20%" valign="middle"><span class="field-label">Report Card</span>
                        </td>

                        <td align="left"   width="30%" valign="middle">
                            <telerik:RadComboBox RenderMode="Lightweight" ID="ddlPrintedFor" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                Width="197px" Label="Select Report Cards:" OnSelectedIndexChanged="ddlPrintedFor_SelectedIndexChanged">
                            </telerik:RadComboBox>

                            <%--   <telerik:RadDropDownList ID="ddlPrintedFor" runat="server" Width="197px" AutoPostBack="True" Label="Select Report Card:">
                </telerik:RadDropDownList>--%>
                        </td>

                    </tr>
                    <tr id="trSubject" runat="server">

                        <td align="left" width="20%"  valign="middle"><span class="field-label">Report Header</span>
                        </td>

                        <%--<td align="left" valign="middle" style="height: 12px" >
                <asp:DropDownList ID="ddlHeader" runat="server" Width="197px">
                </asp:DropDownList>
            </td>--%>
                        <td align="left"  width="30%" valign="middle">
                            <%--  <asp:CheckBox ID="CheckBox1" onclick="javascript:fnSelectAll3(this);" runat="server"
                    Text="Select All" />--%>

                            <telerik:RadComboBox RenderMode="Lightweight" ID="lstHeader" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                Width="193px" Label="Select header:">
                            </telerik:RadComboBox>

                            <%--<asp:CheckBoxList ID="lstHeader" runat="server" BorderStyle="Solid" BorderWidth="1px"
                    Height="206px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                    vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                    text-align: left" Width="193px">
                </asp:CheckBoxList>--%>
                        </td>

                        <td align="left"  width="20%" valign="middle"><span class="field-label">Subject</span>
                        </td>

                        <td align="left"  width="30%" valign="middle">
                            <%--       <asp:CheckBox ID="chkSubjectSelect" onclick="javascript:fnSelectAll1(this);" runat="server"
                    Text="Select All" />--%>

                            <%--                   <telerik:RadComboBox RenderMode="Lightweight" ID="ddlSubject" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"  AutoPostBack="true"
                    Width="100%" Label="Select Subject:" ></telerik:RadComboBox>--%>
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged" Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="trReportType" runat="server">
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>

                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlrpType" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                    </tr>

                    <tr runat="server">
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Choose</span></td>

                        <td align="left"  width="30%" valign="middle" colspan="4">
                            <asp:RadioButtonList ID="rblChoice" runat="server" OnSelectedIndexChanged="rblChoice_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0" Selected="True">Section-wise</asp:ListItem>
                                <asp:ListItem Value="1">Group-wise</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr runat="server" id="trSection">
                        <td align="left" valign="middle" width="20%"><span class="field-label">Section</span></td>

                        <td align="left"  width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="172px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trGroup" visible="false">
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Subject Group</span></td>

                        <td align="left"  width="30%" valign="middle" colspan="4">
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" Width="172px">
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="trFilter" runat="server" visible="false">
                        <td align="left" ><span class="field-label">Filter By</span></td>
                        <td align="left" colspan="4">
                            <asp:DropDownList ID="ddlType" runat="server" Width="199px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                                <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="middle">
                            <asp:CheckBox ID="chkRange" runat="server" AutoPostBack="true" Text="Set Range" CssClass="field-label" />
                            <asp:CheckBox ID="chkCompare" runat="server" AutoPostBack="true" Text="Compare Exams" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr id="trComparision" runat="server" visible="false">
                        <td align="left"><span class="field-label">Compare Exams</span></td>

                        <td align="left">
                            <%--                <telerik:RadComboBox RenderMode="Lightweight" ID="lstComparision" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" 
                    Width="100%" Label="Select the exams to compare:"></telerik:RadComboBox>--%>

                            <asp:DropDownList ID="ddlExam1" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td><span class="field-label">VS</span></td>
                        <td>
                            <%--     <br />--%>
                            <asp:DropDownList ID="ddlExam2" runat="server">
                            </asp:DropDownList>

                            <asp:Button ID="btnSetExams" runat="server" Text="Set" CssClass="button" Width="100px" OnClick="btnSetExams_Click" />
                        </td>
                    </tr>
                    <tr id="trRange" runat="server" visible="false">
                        <td align="center" valign="middle" colspan="4">
                            <table style="border-collapse: collapse">
                                <tr style="background-color: rgba(175, 255, 164, 0.65);">
                                    <td><span class="field-label">Exceeding Expectation </span>
                                    </td>
                                    <td><span class="field-label">From :</span>
                                        <asp:TextBox ID="txt1From" Text="70" runat="server"></asp:TextBox></td>
                                    <td><span class="field-label">Up To:</span>
                                        <asp:TextBox ID="txt1To" Text="100" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="background-color: rgba(251, 255, 0, 0.65);">
                                    <td><span class="field-label">Meeting Expectation  </span>
                                    </td>
                                    <td><span class="field-label">From :</span>
                                        <asp:TextBox ID="txt2From" Text="50" runat="server"></asp:TextBox></td>
                                    <td>
                                        <span class="field-label">Up To:</span>
                                        <asp:TextBox ID="txt2To" Text="70" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="background-color: rgba(255, 134, 134, 0.65);">
                                    <td><span class="field-label">Approaching Expectation</span>
                                    </td>
                                    <td><span class="field-label">From :</span>
                                        <asp:TextBox ID="txt3From" Text="0" runat="server"></asp:TextBox></td>
                                    <td>
                                        <span class="field-label">Up To:</span>
                                        <asp:TextBox ID="txt3To" Text="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1"  />
                            &nbsp;<asp:Button ID="btnDownload" runat="server" CssClass="button" TabIndex="7"
                                Text="Download Report In PDF" ValidationGroup="groupM1" />
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                    <tr runat="server" id="trGrid" visible="false">
                        <td colspan="14">
                            <%--<h3>Exam Comparision</h3>--%>
                            <asp:GridView ID="gvComparision" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" BorderStyle="None" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Exam 1 " >
                                        <ItemTemplate>
                                            <asp:Label ID="lblExam1" runat="server" Text='<%# Bind("Exam1")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exam 2">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllblExam2" runat="server" Text='<%# Bind("Exam2")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />

                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
