<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReportAnalysis.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px ; ";
            sFeatures += "help: no; "; 
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
        if (GRD_IDs == '') {
            alert('Please select atleast one Grade')
            return false;
        }

        var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs, "RadWindow1");

    }

    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
           
            NameandCode = arg.NameCode.split('||');
            
            document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
            __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
              //  document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
          if (GRD_IDs == '') {
              alert('Please select atleast one Grade')
              return false;
          }
          result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs, "", sFeatures)
          if (result != '' && result != undefined) {
              document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }
        function GetSUBJECT() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var stream;
            var NameandCode;
            var result;
            var GRD_IDs;
            var hasSection = document.getElementById('<%=h_HasSection.ClientID %>').value;
            var ACD_ID = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
          var STM_Hide = document.getElementById('<%=h_HideStream.ClientID %>').value;
          if (hasSection == 'false') {
              GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            }
            else {
                GRD_IDs = document.getElementById('<%=ddlGrade1.ClientID %>').value;
            }
            if (STM_Hide == '1') {
                stream = document.getElementById('<%=ddlStream.ClientID %>').value;
           }
           else if (STM_Hide == '4') {
               stream = document.getElementById('<%=ddlStream1.ClientID %>').value;
           }
           else {
               stream = '1';
           }
            var url = "../../clmPopupForm.aspx?multiselect=false&ID=SUBJECT&GRD_IDS=" + GRD_IDs + "&STM_ID=" + stream + "&ACD_ID=" + ACD_ID;
            //result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=false&ID=SUBJECT&GRD_IDS=" + GRD_IDs + "&STM_ID=" + stream + "&ACD_ID=" + ACD_ID, "", sFeatures);
            var oWnd = radopen(url, "pop_sub");
      <%-- NameandCode = result.split("___")
       if (result != '' && result != undefined) {
           document.getElementById('<%=h_SBG_IDs.ClientID %>').value = NameandCode[0];
               document.getElementById('<%=txtSubject.ClientID %>').value = NameandCode[2];
               return false;
           }
           else {
               return false;
           }--%>
        }


         function OnClientClose1(oWnd, args) {
            
         var arg = args.get_argument();
        
         if (arg) {
            
             NameandCode = arg.NameCode.split('||');
            
             document.getElementById('<%=h_SBG_IDs.ClientID %>').value = NameandCode[0].split('___')[0];
               document.getElementById('<%=txtSubject.ClientID %>').value = NameandCode[1];
               __doPostBack('<%= h_SBG_IDs.ClientID%>', 'TextChanged');
            }
        }



    </script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label ID="lblHeader" runat="server" Text="Result Analysis"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_sub" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>

    <table align="center" width="100%">
        <tr>
            <td class="subheader_img" colspan="4">
                <asp:Button ID="btnCheck" Style="display: none;" runat="server" /></td>
        </tr>
        <tr>
            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" width="20%"><span class="field-label">Report Type</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        
        <tr runat="server" id="trRPFID">
            <td align="left"><span class="field-label">Report Printed For</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlReportPrintedFor" runat="server">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="trHeader">
            <td align="left"><span class="field-label">Report Header</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlHeader" runat="server">
                </asp:DropDownList>
                <asp:CheckBox ID="chkGetAllHeaders" runat="server" AutoPostBack="True"
                    Text="Get All Headers" />
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="trGrade">
            <td align="left"><span class="field-label">Grade</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlGrade1" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trGradeSection" runat="server">
            <td align="left"><span class="field-label">Grade</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlStream1" runat="server"
                    AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="left"><span class="field-label">Section</span>
            </td>
            
            <td align="left">
                <asp:DropDownList ID="ddlSection" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr runat="server" id="trTopRows">
            <td align="left"><span class="field-label">Top N Number</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtTopNNumber" Text="10" runat="server"></asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="trSubject">
            <td align="left"><span class="field-label">Subject</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgSubject" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSUBJECT();return false;" /></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="trSubjectChkList">
            <td align="left"><span class="field-label">Subject</span></td>
            
            <td align="left">
                <div class="checkbox-list">
                <asp:CheckBoxList ID="chkListSubjects" runat="server">
                </asp:CheckBoxList>
                </div>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="trSelStudent" valign="top">
            <td align="left" valign="top"><span class="field-label">Student</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtStudIDs" runat="server" Width="25%" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton></td>
            <td align="left" colspan="2">
                <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="100%" CssClass="table table-bordered table-row" OnPageIndexChanging="grdStudent_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Stud. No">
                            <ItemTemplate>
                                <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trFilter" runat="server">
            <td align="left"><span class="field-label">Filter By</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlType" runat="server">
                    <asp:ListItem>ALL</asp:ListItem>
                    <asp:ListItem>SEN</asp:ListItem>
                    <asp:ListItem>EAL</asp:ListItem>
                    <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                    <asp:ListItem>EMIRATI</asp:ListItem>
                    <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnGenerateReport" CssClass="button" runat="server" SkinID="ButtonNormal" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button ID="btnDownload" CssClass="button" runat="server" SkinID="ButtonNormal" Text="Download in PDF" OnClick="btnGenerateReport_Click" />
                <asp:Button ID="btnCancel" CssClass="button" runat="server" SkinID="ButtonNormal" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="h_GRD_IDs" runat="server" />
    <asp:HiddenField ID="h_STU_IDs" runat="server" />
    <asp:HiddenField ID="h_SBG_IDs" runat="server" />
    <asp:HiddenField ID="h_HasSection" runat="server" />
    <asp:HiddenField ID="h_HideStream" runat="server" />
    <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
        
                </div>
            </div>
         </div>

 
</asp:Content>

