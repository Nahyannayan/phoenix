<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptObjTrackObjectiveCompletedStatus.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptObjTrackObjectiveCompletedStatus" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];

                if (curr_elem.type == 'checkbox' && curr_elem.name != 'ctl00$cphMasterpage$chkTC') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" align="center" width="100%" cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Grade</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr id="Tr1" runat="server">
                        <td align="left">
                            <span class="field-label">Section</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left">
                            <span class="field-label">Subject</span></td>

                        <td align="left">&nbsp;
                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="ddlSubject" runat="server" RepeatLayout="Flow">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Completed Dates (From)</span></td>

                        <td align="left" valign="middle">
                            <asp:TextBox ID="txtFrom" runat="server">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                        <td align="left" valign="middle">
                            <span class="field-label">Completed Dates (To)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTo" runat="server">
                            </asp:TextBox><asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="12">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnDownload" runat="server" CssClass="button"
                    Text="Download in PDF" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfbDownload" runat="server" />

                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                    TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFrom"
                    Display="None" ErrorMessage="Please enter the from date" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="calDate3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgTo"
                    TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTo"
                    Display="None" ErrorMessage="Please enter the to date" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="calDate4" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

