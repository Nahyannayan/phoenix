<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGroupListBySubject.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select Grade')
                return false;
            }
            //result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_IDs ,"", sFeatures)            

            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_SBG_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtSubject.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtSubject.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSUBJECT() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_SBG_IDs.ClientID %>').value = result;//NameandCode[0];
                }
                else {
                    return false;
                }
            }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="lblHeader" runat="server" Text="Group List By Subject"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                <table align="center"  cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAca_Year" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                       
                        <td align="left" >
                            <asp:CheckBoxList ID="chkGRD_ID" runat="server" CellPadding="8"
                                CellSpacing="2" RepeatColumns="8"
                                RepeatDirection="Horizontal" AutoPostBack="True">
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr id="21">
                        <td align="left" width="20%" valign="top"><span class="field-label">Subject</span></td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtSubject" runat="server" OnTextChanged="txtSubject_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgSubject" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgSubject_Click"></asp:ImageButton><br />
                            <asp:GridView ID="grdSubject" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" OnPageIndexChanging="grdSubject_PageIndexChanging" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="GRADE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSUBJ_ID" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SBG_DESCR" HeaderText="Subject Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnCancel" runat="server"  CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SBG_IDs" runat="server" />
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

