<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTermlyAttendaceReport.aspx.vb" Inherits="rptTermlyAttendaceReport" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Exam Attendance Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table align="center" width="100%" cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                  
                    <tr>
                         <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" Width="331px">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Printed For</span></td>
                       
                        <td align="left" colspan="4">
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server" Width="264px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="20">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span></td>
                       
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Section</span>
                        </td>
                        
                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:RadioButton ID="radAll" runat="server" Checked="True" Text="All" CssClass="field-label"
                                GroupName="FilterStatus" />
                            <asp:RadioButton ID="radAbsent" runat="server" Text="Absent" CssClass="field-label"
                                GroupName="FilterStatus" />
                            <asp:RadioButton ID="radApprovedLeave" runat="server" Text="Approved Leave" CssClass="field-label"
                                GroupName="FilterStatus" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STU_IDs" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

