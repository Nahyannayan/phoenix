﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rpt_RA_AssessmentComparisonBySkill
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C500392")) _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))

                    BindGrade()
                    BindAssessment()
                    BindSubjects()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Menthods"
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY " _
                       & " ,GRM_GRD_ID,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                       & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                       & " and GRM_GRD_ID NOT IN('KG1','KG2') AND "

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                     & ")"
        End If

        str_query += "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub

    Sub BindAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_curriculumConnectionString
        Dim str_query As String = "SELECT CAD_ID,CAD_DESC FROM ACT.ACTIVITY_D WHERE ISNULL(CAD_bAOL,0)=1" _
                              & " AND CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubActivity.DataSource = ds
        ddlSubActivity.DataTextField = "CAD_DESC"
        ddlSubActivity.DataValueField = "CAD_ID"
        ddlSubActivity.DataBind()
    End Sub

    Sub BindSubjects()
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_SBM_ID,SBG_DESCR FROM " _
                               & " SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                               & " AND SBG_PARENT_ID=0" _
                               & " ORDER BY SBG_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlsubject.datasource = ds
        ddlsubject.datatextfielD = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlsubject.databind()
    End Sub

    Sub CallReport()
        Dim grade() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", grade(0))
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@CAD_ID", ddlSubACTIVITY.SelectedValue.ToString)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("assessment", ddlSubActivity.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rpt_RA_AssessmentComparisonBySkill.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindAssessment()
        BindSubjects()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx')", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank')", True)
        End If
    End Sub
End Class
