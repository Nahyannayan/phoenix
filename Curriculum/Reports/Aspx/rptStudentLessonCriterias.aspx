<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptStudentLessonCriterias.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStudentLessonCriterias"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">
        function GetSubjects() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var trmId;
            var sbgId;
            var trmId;
            trmId = document.getElementById('<%=ddlTerm.ClientID %>').value;
            sbgId = document.getElementById('<%=ddlSubject.ClientID %>').value.split("|");
            var oWnd = radopen("../../showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0],  "pop_sub");

            //result = window.showModalDialog("../../showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "", sFeatures)

            <%--if (result != "" && result != "undefined") {

                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];

            }
            return false;--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
               
                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_sub" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            LESSON PLANNER
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr >
                        <td  align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table id="tblClm" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" >
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>                                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td  align="left" width="20%"><span class="field-label">Term</span>
                                    </td>
                                   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Grade</span>
                                    </td>
                                  
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"  width="20%"><span class="field-label">Section</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Select Subject</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%" ><span class="field-label">Select Topic</span>
                                    </td>
                                  
                                    <td align="left" >
                                        <asp:TextBox ID="txtTopic" runat="server" AutoPostBack="True" Enabled="False"  ></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSubjects(); return false;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button"  Text="Download Report in PDF"
                                            ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_TopicID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>
