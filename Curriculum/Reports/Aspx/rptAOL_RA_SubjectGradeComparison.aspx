﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="rptAOL_RA_SubjectGradeComparison.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptAOL_RA_SubjectGradeComparison" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');

            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
    }
    else {
        return false;
    }
}


function fnSelectAll(master_box) {
    var curr_elem;
    var checkbox_checked_status;
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        curr_elem = document.forms[0].elements[i];
        if (curr_elem.type == 'checkbox') {
            curr_elem.checked = !master_box.checked;
        }
    }
    master_box.checked = !master_box.checked;
}
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Wise Performance
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" width="20%"><span class="field-label">Grade</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="r6" runat="server">
                        <td ><span class="field-label">Report Schedule</span></td>

                        <td align="left" valign="middle"  width="30%">
                            <asp:CheckBox ID="chkRpf" runat="server" Text="Select All" AutoPostBack="True" CssClass="field-label" /><br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstRpf" runat="server" AutoPostBack="True">
                            </asp:CheckBoxList></div>
                        </td>
                        <td align="left" valign="middle" width="20%">
                            <asp:CheckBox ID="chkOverAll" runat="server" Text="OverAll Grade" CssClass="field-label"></asp:CheckBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="rr1" runat="server">
                        <td align="left" valign="middle"  colspan="4">
                            <asp:RadioButton ID="rdGroup" runat="server" GroupName="g1" CssClass="field-label"
                                Text="Report By Group" Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="rdStudent" GroupName="g1" runat="server" CssClass="field-label"
                                Text="Report By Student" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr id="r1" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Subject</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True"
                               >
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="r2" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Group</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlGroup" runat="server">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="r3" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Section</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="r4" runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Subject</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:CheckBox ID="chkAll" runat="server" Text="Select All " AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                           <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstSubject" runat="server" AutoPostBack="True">

                            </asp:CheckBoxList>
                                </div></td>
                         <td colspan="2"></td>
                    </tr>

                    <tr id="r5" runat="server">
                        <td align="left" valign="top" width="20%"><span class="field-label">Student</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton></td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                         
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                Text="DownLoad Report in PDF" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <asp:HiddenField ID="hfSubjects" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

