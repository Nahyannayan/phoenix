﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptAOL_RA_SubjectGradeComparison
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290030" And ViewState("MainMnu_code") <> "C290035" And ViewState("MainMnu_code") <> "C290040" And ViewState("MainMnu_code") <> "C290070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                BindReportSchedule()





                If ViewState("MainMnu_code") = "C290070" Then
                    r3.Visible = True
                    r4.Visible = False
                    r5.Visible = True
                    chkOverAll.Visible = False
                    BindSection()
                Else
                    r3.Visible = False
                    r4.Visible = False
                    r5.Visible = False
                End If


                If ViewState("MainMnu_code") = "C290040" Or ViewState("MainMnu_code") = "C290070" Then
                    rr1.Visible = False
                    r2.Visible = False
                    PopulateSubjects()
                Else
                    rr1.Visible = True
                    BindSection()
                    PopulateSubjects()
                    BindGroup()
                End If
                If ViewState("MainMnu_code") = "C290030" Then
                    r6.Visible = False
                Else
                    r6.Visible = True
                End If
            End If
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportSchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT  RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                             & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                             & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstRpf.DataSource = ds
        lstRpf.DataTextField = "RPF_DESCR"
        lstRpf.DataValueField = "RPF_ID"
        lstRpf.DataBind()
    End Sub



    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + acdid

        If ViewState("MainMnu_code") = "C290070" Then
            str_query += " and grd_id in('kg1','kg2')"
        End If
        str_query += " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub PopulateSubjects()
        ddlSubject.Items.Clear()
        lstSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If


        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        lstSubject.DataSource = ds
        lstSubject.DataTextField = "SBG_DESCR"
        lstSubject.DataValueField = "SBG_ID"
        lstSubject.DataBind()

        If ViewState("MainMnu_code") = "C290070" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlSubject.Items.Insert(0, li)
        End If
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE SGR_SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += lstSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function

    Function getRpfIds() As String
        Dim i As Integer
        Dim str As String
        For i = 0 To lstRpf.Items.Count - 1
            If lstRpf.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstRpf.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        If ViewState("MainMnu_code") = "C290070" Then
            param.Add("@RPF_IDS", getRpfIds())
            param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
            param.Add("@GRD_ID", grade(0))
            param.Add("@RSM_ID", "0")
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            param.Add("@BSU_ID", Session("sbsuid"))
            param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
            rptClass.reportPath = Server.MapPath("../Rpt/rptCBSEKGComparisonBySubject.rpt")
        ElseIf ViewState("MainMnu_code") <> "C290040" Then
            If rdGroup.Checked = True Then
                param.Add("@SGR_ID", ddlGroup.SelectedItem.Value)
                param.Add("Subject", ddlSubject.SelectedItem.Text)
                param.Add("group", ddlGroup.SelectedItem.Text)
                If ViewState("MainMnu_code") = "C290030" Then
                    rptClass.reportPath = Server.MapPath("../Rpt/rptAOL_RA_SubjectGradeComparison.rpt")
                Else
                    param.Add("@bOVERALL", chkOverAll.Checked)
                    param.Add("@RPF_IDs", getRpfIds())
                    rptClass.reportPath = Server.MapPath("../Rpt/rpt_RA_FaSAComparisonByGroup.rpt")
                End If
            Else
                param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
                param.Add("@SBG_ID", getSubjects)
                If ViewState("MainMnu_code") = "C290030" Then
                    rptClass.reportPath = Server.MapPath("../Rpt/rptAOLSubjectComparisonByStudent.rpt")
                Else
                    param.Add("@bOVERALL", chkOverAll.Checked)
                    param.Add("@RPF_IDs", getRpfIds())
                    rptClass.reportPath = Server.MapPath("../Rpt/rpt_RA_FASA_ComparisonByStudent.rpt")
                End If
            End If

        Else
            param.Add("@RPF_IDs", getRpfIds())
            param.Add("@bOVERALL", chkOverAll.Checked)
            param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
            param.Add("@GRD_ID", grade(0))
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
            rptClass.reportPath = Server.MapPath("../Rpt/rpt_RA_FASAComparisonBySection.rpt")
        End If

        rptClass.crDatabase = "oasis_curriculum"
        rptClass.reportParameters = param
        Session("rptClass") = rptClass

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If


    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Protected Sub rdGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGroup.CheckedChanged
        r1.Visible = True
        r2.Visible = True
        r3.Visible = False
        r4.Visible = False
        r5.Visible = False
    End Sub

    Protected Sub rdStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdStudent.CheckedChanged
        r3.Visible = True
        r4.Visible = True
        r5.Visible = True
        r1.Visible = False
        r2.Visible = False
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindReportSchedule()
        BindSection()
        PopulateSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindReportSchedule()
        BindSection()
        PopulateSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Dim i As Integer
        For i = 0 To lstSubject.Items.Count - 1
            lstSubject.Items(i).Selected = chkAll.Checked
        Next
    End Sub


    Protected Sub chkRpf_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRpf.CheckedChanged
        Dim i As Integer
        For i = 0 To lstRpf.Items.Count - 1
            lstRpf.Items(i).Selected = chkRpf.Checked
        Next
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
