Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class rptFormativeConsolidated
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C304006") AndAlso (ViewState("MainMnu_code") <> "StudentProfile")) _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    BindReportCard()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
                    trstud1.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub





    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS D ON A.GRM_STM_ID=D.STM_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If h_STU_IDs.Value = "" Then
                Dim str_sec As String '= GetSelectedSection(ddlSection.SelectedValue)
                h_STU_IDs.Value = GetAllStudentsInGroup(Session("sBSU_ID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, ddlGroup.SelectedValue)
            End If
          CallReport
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub CallReport()
        Dim param As New Hashtable

        Dim strRpf As String = ""
        Dim strSgr As String = ""
        Dim i As Integer

        For i = 0 To ddlPrintedFor.Items.Count - 1
            If ddlPrintedFor.Items(i).Selected = True Then
                If strRpf <> "" Then
                    strRpf += "|"
                End If
                strRpf += ddlPrintedFor.Items(i).Value.ToString
            End If
        Next

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RPF_ID", strRpf)
        ' param.Add("UserName", Session("sUsr_name"))
        param.Add("subject", ddlSubject.SelectedItem.Text)

        If rdStudWise.Checked = True Then
            param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
            param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        End If

        If rdGroupWise.Checked = True Or rdGroupMarkWise.Checked = True Then
            If ddlGroup.SelectedValue = "0" Then
                For i = 0 To ddlGroup.Items.Count - 1
                    If ddlGroup.Items(i).Value <> "0" Then
                        If strSgr <> "" Then
                            strSgr += "|"
                        End If
                        strSgr += ddlGroup.Items(i).Value.ToString
                    End If
                Next
                param.Add("@SGR_ID", strSgr)
            Else
                param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)
            End If


        End If

        ' param.Add("RPT_CAPTION", ddlPrintedFor.SelectedItem.Text)

        ' param.Add("TERM", getTerm)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If rdStudWise.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptFormativeAssesmentConsolidated.rpt")
            End If
            If rdGroupWise.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptFormativeAssesmentConsolidated_Group.rpt")
            End If
            If rdGroupMarkWise.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptFormativeAssesmentConsolidated_Marks_Group.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Function getTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT TRM_DESCRIPTION,TRM_STARTDATE,TRM_ENDDATE FROM VW_TRM_M AS A " _
                            & " INNER JOIN RPT.REPORT_RULE_M AS B ON A.TRM_ID=B.RRM_TRM_ID" _
                            & " WHERE RRM_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                            & " AND RRM_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim strTerm As String = ""

        With ds.Tables(0).Rows(0)
            strTerm = .Item(0) + ": " + Format(.Item(1), "dd MMMM yyyy") + " TO " + Format(.Item(2), "dd MMMM yyyy")
        End With
        Return strTerm.ToUpper
    End Function
    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function


    Private Function GetAllStudentsInGroup(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSGR_ID As String) As String
        Dim str_sql As String = String.Empty
        str_sql = "SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(SSD_STU_ID,'') as varchar) + '|' FROM " & _
       " GROUPS_M INNER JOIN STUDENT_GROUPS_S ON GROUPS_M.SGR_ACD_ID = STUDENT_GROUPS_S.SSD_ACD_ID AND " & _
       " GROUPS_M.SGR_GRD_ID = STUDENT_GROUPS_S.SSD_GRD_ID AND GROUPS_M.SGR_SBM_ID = STUDENT_GROUPS_S.SSD_SBM_ID " & _
       " AND GROUPS_M.SGR_SBG_ID = STUDENT_GROUPS_S.SSD_SBG_ID AND GROUPS_M.SGR_ID = STUDENT_GROUPS_S.SSD_SGR_ID " & _
       " WHERE SGR_ACD_ID ='" & vACD_ID & "' AND SGR_GRD_ID = '" & vGRD_ID & _
       "' AND SGR_BSU_ID = '" & Session("sbsuid") & "'" & _
       " AND SGR_ID in (" & vSGR_ID & ") for xml path('')),1,0,''),0) "

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAcademicYear.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportCard.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("___"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Sub BindReportCard()
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSM_DESCR LIKE '%FORMATIVE%'" _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub
    Sub BindPrintedFor()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " AND RPF_DESCR LIKE '%FORMATIVE%'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""

        Dim grade As String() = Grdid.Split("|")

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & grade(0) & "' AND SBG_STM_ID=" + grade(1)
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddl.Items.Insert(0, li)
        Return ddl
    End Function


    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "") As DropDownList
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""

        Dim grade As String() = Grdid.Split("|")

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "' AND SGR_STM_ID=" + grade(1)
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + acdid + "'"
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"

            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()

        Dim li As New ListItem
        If rdGroupWise.Checked = True Or rdGroupMarkWise.Checked = True Then
            li.Text = "ALL"
        Else
            li.Text = "--"
        End If

        li.Value = "0"
        ddl.Items.Insert(0, li)
        Return ddl
    End Function


    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        h_STU_IDs.Value = "0"
        GridBindStudents(h_STU_IDs.Value)
    End Sub



    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        h_STU_IDs.Value = "0"
        GridBindStudents(h_STU_IDs.Value)

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        h_STU_IDs.Value = "0"
        GridBindStudents(h_STU_IDs.Value)
    End Sub

 
    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub rdGroupWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGroupWise.CheckedChanged
        If rdGroupWise.Checked = True Then
            trstud1.Visible = False
            PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub rdStudWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdStudWise.CheckedChanged
        If rdStudWise.Checked = True Then
            trstud1.Visible = True
            PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub rdGroupMarkWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGroupMarkWise.CheckedChanged
        If rdGroupMarkWise.Checked = True Then
            trstud1.Visible = False
            PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
End Class
