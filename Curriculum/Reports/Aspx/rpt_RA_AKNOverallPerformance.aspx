﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_AKNOverallPerformance.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_AKNOverallPerformance" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="Label1" runat="server" Text="AKNS Overall Performers"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table   id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Report Schedule</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server"  >
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Criteria</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlCriteria" runat="server" AutoPostBack="True"
                               >
                                <asp:ListItem Value="TOP">HIGH ACHIEVERS</asp:ListItem>
                                <asp:ListItem Value="BOTTOM">LOW ACHIEVERS</asp:ListItem>
                                <asp:ListItem Value="ABOVE">MARKS ABOVE</asp:ListItem>
                                <asp:ListItem Value="BELOW">MARKS BELOW</asp:ListItem>
                                <asp:ListItem Value="BETWEEN">MARKS BETWEEN</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="trMark" runat="server">
                        
                           <td> <span class="field-label">Marks Range(From)</span></td>
                             <td><asp:TextBox ID="txtMark1" runat="server"  ></asp:TextBox></td>
                <td><asp:Label ID="lblAnd" runat="server" CssClass="field-label" Text="Marks Range(To)"></asp:Label>
                           </td>
                        <td>
                <asp:TextBox ID="txtMark2" runat="server" Visible="False" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle"><span class="field-label">No of Records</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:TextBox ID="txtRecord" runat="server">10</asp:TextBox>
                        </td>
                        <td align="left" colspan="2" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                Text="Generate Report" ValidationGroup="groupM1"  /> </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

