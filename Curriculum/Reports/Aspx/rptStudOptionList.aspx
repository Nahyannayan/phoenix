<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudOptionList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStudOptionList" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%" >
                    
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label">Academic Year</span> </td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                         <td align="left" width="20%">
                            <span class="field-label">Grade</span></td>
            
                        <td align="left" width="30%">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
                    </tr>
       
        <tr id="Tr1" runat="server">
            <td align="left">
                <span class="field-label">Section</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlSection" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" /></td>
        </tr>
                </table>

                </div>
            </div>
        </div>

    


</asp:Content>

