Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptCommendationList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280100") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                          BindReportCard()

                    BindPrintedFor()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

                    BindGrading()
                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If

                    BindSection("")

                    'If ViewState("MainMnu_code") = "C100290" Then
                    '    tblrule.Rows(6).Visible = False
                    'End If


                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
    End Sub

#Region "Private methods"
    Sub BindGrading()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GSD_DESC FROM RPT.REPORT_RULE_M AS A " _
                                & " INNER JOIN ACT.GRADING_SLAB_D AS B ON A.RRM_GSM_SLAB_ID=B.GSD_GSM_SLAB_ID" _
                                & " WHERE RRM_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "' ORDER BY GSD_DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrading.DataSource = ds
        lstGrading.DataTextField = "GSD_DESC"
        lstGrading.DataValueField = "GSD_DESC"
        lstGrading.DataBind()
    End Sub
    Sub checkEmpFormtutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sctId As String = ""
        Dim grdId As String
        Dim stmId As String = ""
        Dim str_query As String = "SELECT ISNULL(SCT_ID,0),ISNULL(SCT_GRD_ID,''),ISNULL(GRM_STM_ID,0) " _
                                 & " FROM SECTION_M INNER JOIN GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                 & "  SCT_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                 & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sctId = reader.GetValue(0).ToString
            grdId = reader.GetString(1)
            stmId = reader.GetValue(2).ToString

            If grdId <> "" Then
                If Not ddlGrade.Items.FindByValue(grdId + "|" + stmId) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(grdId + "|" + stmId).Selected = True
                End If
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                BindSection(sctId)

            End If
        End While

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSM_ID NOT IN(SELECT RSG_RSM_ID FROM RPT.REPORTSETUP_GRADE_S" _
                                & " WHERE RSG_GRD_ID IN('01','02','03','04'))" _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindReportCardForFormTutor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON RSM_ID=RSG_RSM_ID " _
                                & " INNER JOIN OASIS..SECTION_M AS C ON B.RSG_GRD_ID=C.SCT_GRD_ID AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'" _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub


    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Function PopulateSubjects(ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + acd_id
        ' If ViewState("MainMnu_code") = "C300008" Then
        'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
        str_query += "  AND SBG_PARENT_ID=0 AND SBG_bMAJOR=1"

        '  End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID "

        If Session("sbsuid") = "125005" Then
            str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                        & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "

        End If


        'If ViewState("MainMnu_code") = "C300008" Then
        'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
        str_query += "  AND SBG_PARENT_ID=0 AND SBG_bMAJOR=1"

        '  End If


        If Session("sbsuid") = "125005" Then
            str_query += " WHERE SBG_ACD_ID=" + acd_id

        Else
            str_query += " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                             & " AND SGS_TODATE IS NULL"

        End If

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To ddlSubject.Items.Count - 1
            If ddlSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += ddlSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function

    Sub CallReport()

        'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M " _
        '                         & " WHERE RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"

        'Dim rptFinal As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.ToLower

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue.ToString)
        param.Add("@RPF_ID", ddlPrintedFor.SelectedValue.ToString)
        '   param.Add("@RSD_ID", IIf(ddlHeader.SelectedValue = "", "0", ddlHeader.SelectedValue))
        Dim strSub As String = getSubjects()
        If strSub = "" Then
            lblerror.Text = "Please select a subject"
            Exit Sub
        End If
       
        param.Add("@SBG_IDS", getSubjects())

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("grade", ddlGrade.SelectedItem.Text)

        If ddlGrade.SelectedValue = "" Then
            param.Add("@GRD_ID", "0")
            param.Add("@STM_ID", "0")
        Else
            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            param.Add("@GRD_ID", grade(0))
            param.Add("@STM_ID", grade(1))
        End If

        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)

        If ddlSection.SelectedValue = "" Then
            param.Add("@SCT_ID", 0)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        End If

        param.Add("rptCard", ddlReportCard.SelectedItem.Text)
        param.Add("rptSchedule", ddlPrintedFor.SelectedItem.Text)
        param.Add("@GRADINGS", getGradings)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptCommendationList.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Function getGradings() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstGrading.Items.Count - 1
            If lstGrading.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrading.Items(i).Text
            End If
        Next
        Return str
    End Function

    Sub BindSection(ByVal sctid As String)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            If sctid <> "" Then
                str_query += " and sct_id=" + sctid
            End If
            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            If sctid = "" Then
                ddlSection.Items.Insert(0, li)
            End If
        End If
    End Sub

#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()

        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindGrading()

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")
      

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
             BindReportCard()

        BindPrintedFor()

        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
       
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        BindGrading()

             If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")





    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")

       
        If Session("CurrSuperUser") = "Y" Or Session("formtutor") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        BindSection("")

    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Dim i As Integer
        For i = 0 To ddlSubject.Items.Count - 1
            ddlSubject.Items(i).Selected = chkAll.Checked
        Next
    End Sub
End Class
