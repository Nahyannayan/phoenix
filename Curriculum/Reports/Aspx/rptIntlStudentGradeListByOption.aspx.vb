Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptIntlStudentGradeListByOption
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400061") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()
                BindPrintedFor()
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                ViewState("display") = "textbox"
                BindOption()


                If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                Else
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                End If

                ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindOption()
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindOption()

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub


#Region "Private Methods"

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'show all subjects for teachers also


        'Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
        '                         & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
        '                         & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
        '                         & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
        '                         & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS D ON A.SBG_ID=D.SGO_SBG_ID" _
        '                         & " WHERE SBG_ACD_ID='" + acd_id + "' AND SGS_EMP_ID='" + emp_id + "'" _
        '                         & " AND SGS_TODATE IS NULL" _
        '                         & " AND SGO_OPT_ID='" + ddlOption.SelectedValue.ToString + "'"

        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                            & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                            & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS C ON A.SBG_ID=C.SGO_SBG_ID" _
                            & " WHERE SBG_ACD_ID='" + acd_id + "'" _
                            & " AND SGO_OPT_ID='" + ddlOption.SelectedValue.ToString + "'"



        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                                 & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS C ON A.SBG_ID=C.SGO_SBG_ID" _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'" _
                                 & " AND SGO_OPT_ID='" + ddlOption.SelectedValue.ToString + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "")
        ddl.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""

        Dim grade As String() = Grdid.Split("|")
        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "'"
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If

        'show all subjects for teachers also

        'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
        '    strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
        '    str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
        '      " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
        '      " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + acdid + "'"

        '    str_sql += strCondition
        'Else
        str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                  & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"

        str_sql += strCondition & " ORDER BY SGR_DESCR"
        'End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds

        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindOption()
        ddlOption.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        If grade(0) <> "" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT  DISTINCT OPT_ID,OPT_DESCR FROM OPTIONS_M AS A " _
                                   & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS C ON A.OPT_ID=C.SGO_OPT_ID" _
                                   & " INNER JOIN SUBJECTS_GRADE_S AS B ON B.SBG_ID=C.SGO_SBG_ID AND" _
                                   & " SBG_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                   & " AND SBG_GRD_ID='" + grade(0) + "'" _
                                   & " AND SBG_STM_ID='" + grade(1) + "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlOption.DataSource = ds
            ddlOption.DataTextField = "OPT_DESCR"
            ddlOption.DataValueField = "OPT_ID"
            ddlOption.DataBind()
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
        '                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
        '                      & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
        '                      & " where grm_acd_id=" + acdid _
        '                      & " order by grd_displayorder"

        Dim str_query As String

        'If Session("CurrSuperUser") = "Y" Then
        str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                    & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                    & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                    & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
                    & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                    & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                    & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                    & " ORDER BY RSG_DISPLAYORDER"
        'ElseIf studClass.isEmpTeacher(Session("EmployeeId")) = True Then

        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " INNER JOIN GROUPS_M AS D ON A.GRM_GRD_ID=D.SGR_GRD_ID AND A.GRM_ACD_ID=D.SGR_GRD_ID" _
        '                 & " INNER JOIN GROUPS_TEACHER_S AS E ON D.SGR_ID=E.SGS_SGR_ID AND SGS_EMP_ID='" + Session("EMPLOYEEID") + "'" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"

        'Else
        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"
        'End If




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function getGroupTeacher() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                & " WHERE SGS_SGR_ID = " + ddlGroup.SelectedValue _
                & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,'')"
        Dim teacher As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return teacher
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)
        param.Add("sbg", ddlSubject.SelectedItem.Text)
        param.Add("group", ddlGroup.SelectedItem.Text)
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)
        param.Add("@RPF_ID", ddlReportSchedule.SelectedValue.ToString)
        param.Add("@RSM_ID", ddlReportCard.SelectedValue.ToString)
        param.Add("reportcard", ddlReportCard.SelectedItem.Text)
        param.Add("schedule", ddlReportSchedule.SelectedItem.Text)
        param.Add("teacher", getGroupTeacher)
        'param
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .reportPath = Server.MapPath("../Rpt/rptINTLSubjectGradeListByOption.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindOption()
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub

    Protected Sub ddlOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOption.SelectedIndexChanged
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

    End Sub
End Class
