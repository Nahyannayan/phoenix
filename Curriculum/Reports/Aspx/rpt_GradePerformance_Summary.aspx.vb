﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_rpt_GradePerformance_Summary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

          'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C500390") Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else

            '        Response.Redirect("~\noAccess.aspx")
            '    End If

            'Else
            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Try
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                'BindSubjects()
                BindGrade()
                BindReportType()
                'BindReport()
                'BindHeader()
                trRange.Visible = False
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
        'End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubjects(grades As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_GRD_ID NOT IN('KG1','KG2') " _
                                & " AND SBG_GRD_ID  IN (" + grades + ")" _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_PARENT_ID=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_DESCR"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M  " _
                                  & " INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID WHERE" _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindReport(grades As String)
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        str_query = "SELECT DISTINCT RPF_DESCR AS RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID" _
                 & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND RPF_DESCR NOT LIKE '%REVIEW REPORT%' AND RPF_DESCR NOT LIKE '%FEEDBACK REPORT%' " _
                 & " AND RSG_GRD_ID NOT IN ('KG1','KG2')" _
                 & " AND RSG_GRD_ID IN (" + grades + ")" _
                 & " ORDER BY RPF_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_DESCR"
        ddlPrintedFor.DataBind()
        ddlPrintedFor.Items.Insert(0, New ListItem("Select ", "NA"))

    End Sub

    Protected Sub lstGrade_CheckedChanged(sender As Object, e As EventArgs) Handles lstGrade.SelectedIndexChanged
        Dim grades As String = "'" + GetGrades().Replace("|", "','") + "'"
        BindReport(grades)
        BindSubjects(grades)
    End Sub
    'Function GetGrades() As String
    '    Dim str As String = ""
    '    Dim i As Integer
    '    For i = 0 To lstGrade.Items.Count - 1
    '        If lstGrade.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += "|"
    '            End If
    '            str += lstGrade.Items(i).Value
    '        End If
    '    Next
    '    Return str
    'End Function

    Function GetGrades() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstGrade.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If lstGrade.SelectedIndex > 0 Then
                str = lstGrade.SelectedItem.Value
            End If
        End If
        Return str

    End Function
    'Function GetHeader() As String
    '    Dim str As String = ""
    '    Dim i As Integer
    '    For i = 0 To lstHeader.Items.Count - 1
    '        If lstHeader.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += "|"
    '            End If
    '            str += lstHeader.Items(i).Value
    '        End If
    '    Next
    '    Return str
    'End Function


    Function GetHeader() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = lstHeader.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If lstHeader.SelectedIndex > 0 Then
                str = lstHeader.SelectedItem.Value
            End If
        End If
        Return str

    End Function



    Sub CallReport()
        Dim criteria As String = Val(txtFrom.Text).ToString + "|" + Val(txtTo.Text).ToString

        Dim param As New Hashtable
        Dim strSbg As String = ""

        'Dim i As Integer
        'For i = 0 To ddlSubject.Items.Count - 1
        '    If ddlSubject.Items(i).Selected = True Then
        '        If strSbg <> "" Then
        '            strSbg += "|"
        '        End If
        '        strSbg += ddlSubject.Items(i).Text
        '    End If
        'Next

        Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                strSbg += item.Text
                If strSbg <> "" Then
                    strSbg += "|"
                End If
            Next
            strSbg = strSbg.TrimEnd("|")
        Else
            If ddlSubject.SelectedIndex > 0 Then
                strSbg = ddlSubject.SelectedItem.Text
            End If
        End If

        If ddlrpType.SelectedValue <> "1" And ddlrpType.SelectedValue <> "5" And ddlrpType.SelectedValue <> "3" And ddlrpType.SelectedValue <> "6" And ddlrpType.SelectedValue <> "4" And ddlrpType.SelectedValue <> "8" Then
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
        End If
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBG_DESCR", strSbg)
        param.Add("@RPF_DESCR", ddlPrintedFor.SelectedItem.Text)
        param.Add("@RSD_HEADER", GetHeader())
        param.Add("@GRD_IDS", GetGrades())
        param.Add("@TYPE", ddlType.SelectedValue.ToString)
        param.Add("@RPT_TYPE", ddlrpType.SelectedValue.ToString)
        param.Add("@MARK_FROM", IIf(chkRange.Checked, txtFrom.Text, DBNull.Value))
        param.Add("@MARK_TO", IIf(chkRange.Checked, txtTo.Text, DBNull.Value))
        '  param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        ' param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ddlrpType.SelectedValue = "1" Or ddlrpType.SelectedValue = "5" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary1_5.rpt")
            ElseIf ddlrpType.SelectedValue = "2" Or ddlrpType.SelectedValue = "7" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary2_7.rpt")
            ElseIf ddlrpType.SelectedValue = "3" Or ddlrpType.SelectedValue = "6" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary3_6.rpt")
            ElseIf ddlrpType.SelectedValue = "4" Or ddlrpType.SelectedValue = "8" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary4_8.rpt")
            ElseIf ddlrpType.SelectedValue = "10" Or ddlrpType.SelectedValue = "11" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary10_11.rpt")
            ElseIf ddlrpType.SelectedValue = "9" Then
                .reportPath = Server.MapPath("../Rpt/rpt_GradePerformance_Summary9.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()



        End If
    End Sub
    Sub BindHeader(grades As String)
        lstHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_HEADER FROM RPT.REPORT_SETUP_D AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSD_RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS C ON A.RSD_RSM_ID=C.RSM_ID" _
                             & " INNER JOIN RPT.REPORTSETUP_GRADE_S as D on D.RSG_RSM_ID=C.RSM_ID" _
                             & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND RPF_DESCR='" + ddlPrintedFor.SelectedItem.Text + "' AND ISNULL(RSD_BDIRECTENTRY,1)=0 AND ISNULL(RSD_bALLSUBJECTS,0)=1 AND " _
                             & " RSG_GRD_ID IN (" + grades + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_HEADER"
        lstHeader.DataBind()
    End Sub
    Sub BindReportType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT * FROM [RPT].[ANALYSIS_REPORT_TYPE] WHERE ART_MNU_CODE='" + ViewState("MainMnu_code") + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlrpType.DataSource = ds
        ddlrpType.DataTextField = "ART_NAME"
        ddlrpType.DataValueField = "ART_ID"
        ddlrpType.DataBind()

    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'BindSubjects()
        BindGrade()
        BindReportType()
        'BindReport()
        'BindHeader()
    End Sub
    Protected Sub ddlrpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlrpType.SelectedIndexChanged
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ART_bRANGE FROM [RPT].[ANALYSIS_REPORT_TYPE] WHERE ART_ID= " + ddlrpType.SelectedItem.Value
        Dim bool As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If (bool) Then
            chkRange.Checked = True
            trRange.Visible = True
        Else
            chkRange.Checked = False
            trRange.Visible = False
        End If
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        Dim grades As String = "'" + GetGrades().Replace("|", "','") + "'"
        BindHeader(grades)
    End Sub

    Protected Sub chkRange_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRange.CheckedChanged
        If chkRange.Checked = True Then
            trRange.Visible = True
        Else
            trRange.Visible = False
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx')", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank')", True)
        End If
    End Sub
End Class
