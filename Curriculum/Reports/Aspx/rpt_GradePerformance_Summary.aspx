<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_GradePerformance_Summary.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_GradePerformance_Summary"
    Title="GEMS OASIS" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function fnSelectAll3(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 26) == 'ctl00_cphMasterpage_lstHeader') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_lstGrade') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

        function fnSelectAll1(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_ddlSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Result Analysis Comparison"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="middle" class="matters" width="20%"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" valign="middle" class="matters" width="30%">
                            <%--        <asp:CheckBoxList ID="lstGrade" runat="server" BorderWidth="0px" AutoPostBack="true"
                                RepeatLayout="Flow"  >
                            </asp:CheckBoxList>--%>

                            <telerik:RadComboBox RenderMode="Lightweight" AutoPostBack="true" Width="100%" runat="server" ID="lstGrade" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Grades(s)">
                            </telerik:RadComboBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Report Card</span>
                        </td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr id="trSubject" runat="server">

                        <td align="left" valign="middle" class="matters"><span class="field-label">Report Header</span>
                        </td>
                        <%--<td align="left" valign="middle" style="height: 12px" class="matters">
                <asp:DropDownList ID="ddlHeader" runat="server" Width="197px">
                </asp:DropDownList>
            </td>--%>
                        <td align="left" valign="middle" class="matters">
                            <%--  <asp:CheckBox ID="CheckBox1" onclick="javascript:fnSelectAll3(this);" runat="server"
                    Text="Select All" />--%>

                            <%--     <asp:CheckBoxList ID="lstHeader" runat="server" BorderWidth="0px"
                                RepeatLayout="Flow" >
                            </asp:CheckBoxList>--%>
                            <telerik:RadComboBox RenderMode="Lightweight" AutoPostBack="true" Width="100%" runat="server" ID="lstHeader" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Header(s)">
                            </telerik:RadComboBox>

                        </td>

                        <td align="left" valign="middle" class="matters"><span class="field-label">Subject</span>
                        </td>
                        <td align="left" valign="middle">
                            <%--   <asp:CheckBox ID="chkSubjectSelect" onclick="javascript:fnSelectAll1(this);" runat="server"
                                Text="Select All" />
                            <br />
                            <asp:CheckBoxList ID="ddlSubject" runat="server" BorderColor="#8dc24c" BorderWidth="1px"
                                RepeatLayout="Flow"  >
                            </asp:CheckBoxList>--%>
                            <telerik:RadComboBox RenderMode="Lightweight" AutoPostBack="true" Width="100%" runat="server" ID="ddlSubject" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Subject(s)">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr id="trReportType" runat="server">
                        <td align="left" class="matters"><span class="field-label">Report Type</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlrpType" runat="server"  AutoPostBack="true">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trFilter" runat="server">
                        <td align="left" class="matters"><span class="field-label">Filter By</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                                <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4" valign="middle">
                            <asp:CheckBox ID="chkRange" runat="server" AutoPostBack="true" CssClass="field-label" Text="Set Range" /></td>
                    </tr>
                    <tr id="trRange" runat="server">
                        <td class="matters"><span class="field-label">Mark Range(From)</span>
                         </td>
                        <td><asp:TextBox ID="txtFrom" Text="70" runat="server"></asp:TextBox></td>
                             <td class="matters"><span class="field-label">Mark Range(To)</span>
                         </td>
                        <td> 
                            <asp:TextBox ID="txtTo" Text="100" runat="server"></asp:TextBox>
                                    </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            &nbsp;<asp:Button ID="btnDownload" runat="server" CssClass="button" TabIndex="7"
                                Text="Download Report In PDF" ValidationGroup="groupM1" />
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
