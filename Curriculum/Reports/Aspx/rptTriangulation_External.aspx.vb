﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports GemBox.Spreadsheet
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptTriangulation_External
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Session("StuMarks") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"

                'check for the usr_name and the menucode are valid otherwise redirect to login page


                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C970056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then


                        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                        If Session("sbsuid") = "125017" Then
                            Dim studClass As New studClass
                            ddlAcdID = studClass.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sbsuid"))
                        Else
                            ddlAcdID = ActivityFunctions.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sBsuid"))
                        End If

                        If ddlAcdID.Items.Count >= 1 Then

                            callStream_Bind()
                            'PopulateGrades(ddlGrade, ddlReportId)
                            BindSubjectCombo()


                        End If
                    End If
                End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                  



            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try

        End If
        Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)

        ScriptManager1.RegisterPostBackControl(btnGenerateExcel)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdID.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuId"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            'Dim di As ListItem
            'di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(sender As Object, e As EventArgs)
        PopulateGrades(ddlGrade, ddlStream)
    End Sub
    Private Function PopulateGrades(ByVal ddlGrades As DropDownList, ByVal ddlStream As DropDownList)
        ddlGrades.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""


        ' Session("CurrSuperUser") = "Y"
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
            str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER,GRM_DISPLAY " _
                          & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
                          & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
                         & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & Session("Current_ACD_ID") & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' AND GRM_ACD_ID=" + ddlAcdID.SelectedValue.ToString _
                          & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Else
            str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_BSU_M.GRM_DISPLAY,GRD_DISPLAYORDER  " _
                      & " from VW_GRADE_M  " _
                    & " INNER JOIN VW_GRADE_BSU_M ON GRD_ID=GRM_GRD_ID AND GRM_ACD_ID=" + ddlAcdID.SelectedValue.ToString _
                    & " WHERE  VW_GRADE_BSU_M.GRM_STM_ID='" + ddlStream.SelectedValue + "' "

            If ViewState("GRD_ACCESS") > 0 Then
                str_Sql += " AND GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_ACD_ID=" + ddlAcdID.SelectedValue.ToString + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If
            str_Sql += " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        End If



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrades.DataSource = ds
        ddlGrades.DataTextField = "GRM_DISPLAY"
        ddlGrades.DataValueField = "ID"
        ddlGrades.DataBind()

        Return ddlGrades
    End Function
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            cmbSubject.ClearSelection()

            BindSubjectCombo()

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        cmbSubject.ClearSelection()

        cmbSubjectGroup.ClearSelection()
        BindSubjectCombo()

    End Sub
    Protected Sub cmbSubject_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("SBG_DESCR").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("SBG_ID").ToString()
    End Sub
    Protected Sub cmbSubject_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        CType(cmbSubject.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbSubject.Items.Count)
    End Sub
    Protected Sub BindSubjectCombo()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcdID.SelectedValue.ToString)
        param(1) = New SqlParameter("@GRDID", grade(0))

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.LOAD_SUBJECT_COMBO", param)
        cmbSubject.DataSource = ds
        cmbSubject.DataBind()
    End Sub
    Function GetCheckedSubject() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = cmbSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            'If cmbSubject.SelectedIndex > -1 Then
            Try
                str = cmbSubject.SelectedItem.Value
            Catch ex As Exception
            End Try

            'Else
            '    str = ""
            'End If
        End If

        Return str

    End Function



    Protected Sub cmbSubject_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        cmbSubjectGroup.ClearSelection()
        BindSubjectGroupCombo()

    End Sub
    '-----------------------SUBJECT GROUP START------------------------------------------
    Protected Sub cmbSubjectGroup_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("DESCR2").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("ID").ToString()
    End Sub
    Protected Sub cmbSubjectGroup_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        ' CType(cmbSubjectGroup.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbSubjectGroup.Items.Count)
    End Sub
    Protected Sub BindSubjectGroupCombo()
        Dim str_Sql As String
        Dim str_query_header As String
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString

        Dim ds As DataSet
        'Dim param(2) As SqlParameter
        'param(0) = New SqlParameter("@ACD_ID", ddlAcdID.SelectedValue.ToString)
        'param(1) = New SqlParameter("@GRDID", grade(0))

        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.LOAD_SUBJECT_COMBO", param)


        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then

            str_query_header = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID and  (GTS.SGS_TODATE is null) "
            If GetCheckedSubject() <> "" Then
                str_query_header += "WHERE SGR_SBG_ID IN ('" & GetCheckedSubject() & "') "
                str_query_header += " AND SGS_EMP_ID=" & Session("EmployeeId") & " "
            Else
                str_query_header += " WHERE 1=1 "
            End If
            str_Sql = str_query_header.Split("||")(0)
        Else
            str_Sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_GRD_ID='" & grade(0) & "' AND SGR_SBG_ID=" & GetCheckedSubject()

        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        cmbSubjectGroup.DataSource = ds
        cmbSubjectGroup.DataBind()


    End Sub
    Function GetCheckedSubjectGroup() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = cmbSubjectGroup.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If cmbSubjectGroup.SelectedIndex > -1 Then
                str = cmbSubjectGroup.SelectedItem.Value
            Else
                str = ""
            End If
        End If

        Return str

    End Function
    Private Sub GenerateExcelFile()
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection()
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@acd_id", ddlAcdID.SelectedValue)
                pParms(1) = New SqlClient.SqlParameter("@GRD_XML", Nothing)
                pParms(2) = New SqlClient.SqlParameter("@SGR_ID", cmbSubjectGroup.SelectedValue)


                Dim dsUserAndSubject As DataSet = SqlHelper.ExecuteDataset(connection, "[RPT].[getTriangulationreport_cis]", pParms)
                Dim dtConvertToExcel As DataTable = dsUserAndSubject.Tables(0)
                ' Dim dtSubjects As DataTable = dsUserAndSubject.Tables(1)

                'For Each dr As DataRow In dtSubjects.Rows
                '    dtConvertToExcel.Columns.Add(dr(0).ToString)
                'Next


                ' Dim ds As New DataSet
                Dim dtEXCEL As New DataTable

                dtEXCEL = dtConvertToExcel

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim stuFilename As String = Left(dt, Len(dt) - 2) & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '  ws.HeadersFooters.AlignWithMargins = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End Using
        Catch ex As Exception
            Dim a As String = ex.Message
        End Try

    End Sub

    Protected Sub btnGenerateExcel_Click(sender As Object, e As EventArgs) Handles btnGenerateExcel.Click
        GenerateExcelFile()
    End Sub
End Class
