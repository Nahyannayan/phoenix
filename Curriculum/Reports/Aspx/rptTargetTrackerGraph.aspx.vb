﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM
Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing
Imports System.Drawing.Printing
Partial Class Curriculum_Reports_Aspx_rptTargetTrackerGraph
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400301") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()

                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()

                    BindSubjects()
                    BindGroup()

                End If
                'trChart.Visible = False
                btnPrint.Visible = False
                imgStud.Visible = False
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'lblError.Text = "Request could not be processed"
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnPrint)
    End Sub

#Region "Private Methods"


    Sub BindChart(ByVal stuid As String, ByVal stuname As String)

        Dim sbgid As String() = ddlSubject.SelectedValue.Split("|")
        Dim grade() As String = ddlGrade.SelectedValue.Split("|")

        Dim strrpf As String = getReportCards()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "exec [RPT].[rptTARGETTRACKER_GRAPH]" _
                                & " @BSU_ID='" + Session("sbsuid") + "'," _
                                & " @ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & " @GRD_ID='" + grade(0) + "'," _
                                & " @SBM_ID=" + sbgid(0) + "," _
                                & " @SBG_DESCR='" + ddlSubject.SelectedItem.Text + "'," _
                                & " @RPF_DESCR='" + strrpf + "'," _
                                & " @STM_ID=" + grade(1) + "," _
                                & " @STUDENT_IDS=" + stuid
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As DataView = ds.Tables(0).DefaultView

        Dim tt As Title = New Title("Target Tracker Graph - " + ddlSubject.SelectedItem.Text + " (" + stuname + ")", Docking.Top, New System.Drawing.Font("", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105))


        Chart1.Titles.Add(tt)       

        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(0, 1, "w")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(1.1, 2, "w+")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(2.1, 3, "1c")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(3.1, 4, "1b")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(4.1, 5, "1a")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(5.1, 6, "2c")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(6.1, 7, "2b")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(7.1, 8, "2a")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(8.1, 9, "3c")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(9.1, 10, "3b")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(10.1, 11, "3a")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(11.1, 12, "4c")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(12.1, 13, "4b")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(13.1, 14, "4a")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(14.1, 15, "5c")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(15.1, 16, "5b")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(16.1, 17, "5a")
        'Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(17.1, 18, "6c")

        Chart1.ChartAreas("ChartArea1").AxisY.LineWidth = 1
        Chart1.ChartAreas("ChartArea1").AxisY.Interval = 1

        'Chart1.BorderlineDashStyle = ChartDashStyle.Solid
        'Chart1.BorderWidth = 3
        'Chart1.BorderColor = Color.Blue



        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(0, 1.5, "w")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(1.6, 2.5, "w+")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(2.6, 3.5, "1c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(3.6, 4.5, "1b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(4.6, 5.5, "1a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(5.6, 6.5, "2c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(6.6, 7.5, "2b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(7.6, 8.5, "2a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(8.6, 9.5, "3c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(9.6, 10.5, "3b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(10.6, 11.5, "3a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(11.6, 12.5, "4c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(12.6, 13.5, "4b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(13.6, 14.5, "4a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(14.6, 15.5, "5c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(15.6, 16.5, "5b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(16.6, 17.5, "5a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(17.6, 18.5, "6c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(18.6, 19.5, "6b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(19.6, 20.5, "6a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(20.6, 21.5, "7c")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(21.6, 22.5, "7b")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(22.6, 23.5, "7a")
        Chart1.ChartAreas("ChartArea1").AxisY.CustomLabels.Add(23.6, 24.5, "8c")


        Chart1.ChartAreas("ChartArea1").BackColor = Drawing.Color.LightGray
        Chart1.ChartAreas("ChartArea1").BackImage = "~/Images/biggridheadgb.jpg"

       


        Chart1.Series("S1").ChartType = SeriesChartType.Line
        Chart1.Series("S2").ChartType = SeriesChartType.Line

        Chart1.Series("S1")("ShowMarkerLines") = "True"
        Chart1.Series("S1").MarkerStyle = MarkerStyle.Circle
        Chart1.Series("S1").MarkerSize = 15


        Chart1.Series("S2")("ShowMarkerLines") = "True"
        Chart1.Series("S2").MarkerStyle = MarkerStyle.Diamond
        Chart1.Series("S2").MarkerSize = 15


        dv.RowFilter = "LEVEL='ATTAINED LEVEL'"
        dv.Sort = "RPF_DESCR"

        Chart1.Series("S1").Points.DataBindXY(dv, "RPF_DESCR", dv, "VAL")

        Dim dv1 As DataView = ds.Tables(0).DefaultView
        dv1.RowFilter = "LEVEL='BENCHMARK LEVEL'"
        dv1.Sort = "RPF_DESCR"




        Chart1.Series("S2").Points.DataBindXY(dv1, "RPF_DESCR", dv1, "VAL")




        Chart1.ChartAreas("ChartArea1").AxisX.Interval = 1

        Chart1.Series("S1").BorderWidth = "3"
        Chart1.Series("S2").BorderWidth = "3"

        Chart1.Series("S1").Color = Color.DarkRed
        Chart1.Series("S2").Color = Color.BlueViolet

        Chart1.Series("S1")("EmptyPointValue") = "Zero"
        Chart1.Series("S2")("EmptyPointValue") = "Zero"

        Chart1.Series("S1").IsVisibleInLegend = False
        Chart1.Series("S2").IsVisibleInLegend = False


        Dim legendItem As New LegendItem()
        legendItem.Name = "ATTAINED LEVEL"
        legendItem.ImageStyle = LegendImageStyle.Marker
        legendItem.ShadowOffset = 1
        legendItem.Color = Color.DarkRed
        legendItem.MarkerStyle = MarkerStyle.Circle
        legendItem.MarkerSize = 15

        Chart1.Legends("Legend1").CustomItems.Add(legendItem)

        legendItem = New LegendItem()
        legendItem.Name = "BENCHMARK LEVEL"
        legendItem.ImageStyle = LegendImageStyle.Marker
        legendItem.ShadowOffset = 1
        legendItem.Color = Color.BlueViolet
        legendItem.MarkerStyle = MarkerStyle.Diamond
        legendItem.MarkerSize = 15
        legendItem.MarkerBorderColor = Color.BlueViolet

   

        Chart1.Legends("Legend1").CustomItems.Add(legendItem)

     

          
        Chart1.ChartAreas("ChartArea1").Position.X = 1
        Chart1.ChartAreas("ChartArea1").Position.Y = 15
        Chart1.ChartAreas("ChartArea1").Position.Width = 75
        Chart1.ChartAreas("ChartArea1").Position.Height = 75


        Chart1.ChartAreas("ChartArea2").Position.X = 80
        Chart1.ChartAreas("ChartArea2").Position.Y = 25
        Chart1.ChartAreas("ChartArea2").Position.Width = 10
        Chart1.ChartAreas("ChartArea2").Position.Height = 20

        Dim imgFileName As String = "\" + Session("susr_name") + "_StuPhoto_" + Now.ToString.Replace("/", "_").Replace(" ", "").Replace(":", "_") + ".bmp"
        Dim imgFilePath As String = Server.MapPath("~/Curriculum/ReportDownloads") + imgFileName
        Dim strPath As String = GetPhotoPath()
        If ViewState("chartphoto") <> "" And (Not ViewState("chartphoto") Is Nothing) Then
            System.IO.File.Delete(ViewState("chartphoto"))
            ViewState("chartphoto") = ""
        End If
        Try
            If strPath <> "" Then
                System.IO.File.Copy(strPath, imgFilePath)
                Chart1.ChartAreas("ChartArea2").BackImage = "~/Curriculum/ReportDownloads" + imgFileName
                Chart1.ChartAreas("ChartArea2").BackImageWrapMode = ChartImageWrapMode.Scaled
                ViewState("chartphoto") = imgFilePath
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub PrintCharts()
        Dim pD As New PrintDocument


        pD.DefaultPageSettings.Landscape = True
       
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + ddlAcademicYear.SelectedValue.ToString + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub
    Sub BindPhoto()
        Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
        Dim lblStuPhotoPath As Label = gvStudent.Rows(0).FindControl("lblStuPhotoPath")
        If lblStuPhotoPath.Text <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + lblStuPhotoPath.Text
            imgStud.ImageUrl = strImagePath


        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

    Function GetPhotoPath()
        Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
        Dim lblStuPhotoPath As Label = gvStudent.Rows(0).FindControl("lblStuPhotoPath")
        If lblStuPhotoPath.Text <> "" Then
            Dim strImagePath As String = WebConfigurationManager.AppSettings("UploadPhotoPath") + lblStuPhotoPath.Text
            Return strImagePath
        Else
            Return ""
        End If
        'imgStud.AlternateText = "No Image found"
    End Function
    Sub BindStudents()
        Dim sbgid As String() = ddlSubject.SelectedValue.Split("|")
        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,  " _
                               & " ISNULL(STU_PHOTOPATH,'') as STU_PHOTOPATH" _
                               & " FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                               & " WHERE SSD_SBG_ID='" + sbgid(1) + "'"


        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If


        If ddlGroup.SelectedValue <> "0" Then
            str_query += " AND SSD_SGR_ID='" + ddlGroup.SelectedValue.ToString + "'"
        End If

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
        End If
        If txtStudentName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtStudentName.Text + "%'"
        End If

        str_query += " ORDER BY STU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStudent.DataSource = ds
        gvStudent.DataBind()

        gvStudent.HeaderRow.Visible = False
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")

        Dim STR_QUERY As String = "EXEC RPT.GETREPORTCARDBYYEAR " _
                               & "'" + Session("SBSUID") + "'," _
                               & "'" + grade(0) + "'," _
                               & Session("CLM").ToString
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACY_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRPF As String = ""

        Dim bRSM As Boolean = False

        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        strRPF += cNode.Text + "_" + ccNode.Text
                    End If
                Next
            Next
        Next


        Return strRPF.Trim
    End Function

    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindSubjects()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CONVERT(VARCHAR(100),SBG_SBM_ID)+'|'+CONVERT(VARCHAR(100),SBG_ID) AS SBG_ID" _
                                 & " ,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()



    End Sub

    Sub BindGroup()
        Dim sbgid As String() = ddlSubject.SelectedValue.Split("|")
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SGR_SBG_ID='" + sbgid(1) + "'"

        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGroup.Items.Insert(0, li)

    End Sub


#End Region

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
        BindReports()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindReports()
        BindSubjects()
        BindGroup()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStudents()
        hfPageindex.Value = gvStudent.PageIndex
        Dim lblStuid As Label
        Dim lblStuName As Label
        If gvStudent.Rows.Count > 0 Then
            lblStuid = gvStudent.Rows(0).FindControl("lblStuId")
            lblStuName = gvStudent.Rows(0).FindControl("lblStuName")
            If Not lblStuid Is Nothing Then
                BindChart(lblStuid.Text, lblStuName.Text)
            End If
        End If
        btnPrint.Visible = True
        '  imgStud.Visible = True
        '  BindPhoto()
    End Sub

    Protected Sub gvStudent_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudent.PageIndexChanged
        If gvStudent.Rows.Count > 0 Then
            Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
            Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
            BindChart(lblStuId.Text, lblStuName.Text)
            'BindPhoto()
        End If
    End Sub

    Protected Sub gvStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudent.PageIndexChanging
        gvStudent.PageIndex = e.NewPageIndex
        BindStudents()
        hfPageindex.Value = gvStudent.PageIndex
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim imgFileName As String = Server.MapPath("~/Curriculum/ReportDownloads") + "\" + Session("susr_name") + "Chart" + Now.ToString.Replace("/", "_").Replace(" ", "").Replace(":", "_") + ".bmp"
        hfPageindex.Value = gvStudent.PageIndex
        BindStudents()
        Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
        Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
        BindChart(lblStuId.Text, lblStuName.Text)
        Chart1.SaveImage(imgFileName, ChartImageFormat.Bmp)

        'HttpContext.Current.Response.ContentType = "image/bmp"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(imgFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(imgFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        Dim bytes() As Byte = File.ReadAllBytes(imgFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "image/bmp"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(imgFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        System.IO.File.Delete(imgFileName)
    End Sub
  
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If ViewState("chartphoto") <> "" And (Not ViewState("chartphoto") Is Nothing) Then
                System.IO.File.Delete(ViewState("chartphoto"))
                ViewState("chartphoto") = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
