<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptIntlSubjectGradeAnalysis.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptIntlSubjectGradeAnalysis" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters" ><span class="field-label">Report Schedule</span></td>
                        <td align="left" valign="middle"  class="matters">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server"  >
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters" ><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle"  class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" valign="middle" class="matters"><span class="field-label">Subject</span></td>
                        <td align="left" valign="middle" class="matters">
                         <%--   <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                            <br />
                            <asp:CheckBoxList ID="ddlSubject" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                Height="206px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                Width="193px">
                            </asp:CheckBoxList>--%>
                             <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="ddlSubject"
                                  CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Subject(s)">  </telerik:RadComboBox>


                        </td>
                        <td align="left" class="matters" valign="middle"><span class="field-label">By Gender</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:CheckBox ID="chkGender" runat="server" /></td>
                    </tr>
                    <tr runat="server" id="trFilter">
                        <td align="left" class="matters" valign="middle"><span class="field-label">Filter By</span></td>
                        <td align="left" class="matters" valign="middle">
                            <asp:DropDownList ID="ddlType" runat="server"  >
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trSubject1" runat="server">
                        <td align="left" valign="middle" class="matters" colspan="4">
                            <asp:RadioButtonList ID="rdHeader" runat="server" RepeatDirection="Horizontal">
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                Text="DownLoad Report in PDF" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfbDownload" runat="server" />


                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>

</asp:Content>

