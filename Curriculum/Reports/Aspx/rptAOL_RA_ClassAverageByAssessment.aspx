﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="rptAOL_RA_ClassAverageByAssessment.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptAOL_RA_ClassAverageByAssessment" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Class Average Comparison By Assessment
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table  id="tblrule" runat="server" align="center"  cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                         <td align="left" width="20%" valign="middle"  ><span class="field-label">Grade</span></td>
                        
                        <td align="left" valign="middle"  >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                    </tr>
                   
                    <tr>
                        <td align="left" width="20%"  valign="middle"><span class="field-label">Section</span></td>
                        
                        <td align="left"  valign="middle">
                            <asp:CheckBox ID="chkAllSection" runat="server" Text="Select All " AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstSection" runat="server" AutoPostBack="True" >
                            </asp:CheckBoxList></div></td>
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Subject</span></td>
                       
                        <td align="left"  valign="middle">
                            <asp:CheckBox ID="chkAll" runat="server" Text="Select All " AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstSubject" runat="server" AutoPostBack="True" >
                            </asp:CheckBoxList></div></td>
                    </tr>
                   

                    <tr runat="server">
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Assessment</span></td>
                       
                        <td align="left"  valign="middle">
                            <asp:CheckBox ID="chkAllAssessment" runat="server" Text="Select All " AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstAssessment" runat="server" AutoPostBack="True" >
                            </asp:CheckBoxList></div></td>
                        <td colspan="2">

                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1"  />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" 
                                Text="DownLoad Report in PDF" ValidationGroup="groupM1"  />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>


                <asp:HiddenField ID="hfbDownload" runat="server" />


                <asp:HiddenField ID="hfSubjects" runat="server"></asp:HiddenField>


                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>
        </div>
    </div>
</asp:Content>

