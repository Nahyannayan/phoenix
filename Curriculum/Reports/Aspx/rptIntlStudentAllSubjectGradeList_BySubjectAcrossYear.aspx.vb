Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptIntlStudentAllSubjectGradeList_BySubjectAcrossYear
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400118") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()
                    BindSubjects()
                    BindGroups()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function



    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRPF As String = ""

        Dim bRSM As Boolean = False

        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        strRPF += cNode.Text + "_" + ccNode.Text
                    End If
                Next
            Next
        Next


        Return strRPF.Trim
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        'If Session("SBSUID") <> "125010" Then
        '    param.Add("UserName", Session("sUsr_name"))
        '    param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        '    param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        '    param.Add("grade", ddlGrade.SelectedItem.Text)

        'End If
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        param.Add("@GRD_ID", grade(0))
        param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)
        param.Add("@RPF_DESCR", getReportCards())
        param.Add("@STM_ID", grade(1))
        param.Add("@bEVALUATION", Not (chkDisplayALL.Checked))
        param.Add("@SBM_ID", GetSelectedSubjects())
        param.Add("@SBG_DESCR", GetSelectedSubjects_Text())
        param.Add("@TYPE", ddlType.SelectedValue)

        'param.Add("@SGR_ID", ddlGroup.SelectedValue.ToString)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If chkDisplayALL.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptIntlStudentPerformanceBySubjectAcrossYear.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptIntlStudentPerformanceBySubjectAcrossYear_Evaluation.rpt")
            End If
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            ReportLoadSelection()
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")


        Dim STR_QUERY As String = "EXEC RPT.GETREPORTCARDBYYEAR " _
                               & "'" + Session("SBSUID") + "'," _
                               & "'" + grade(0) + "'," _
                               & Session("CLM").ToString
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACY_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub


    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindSubjects()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        'ddlSubject.Items.Clear()
        chkListSubjects.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_SBM_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SBG_GRD_ID='" + grade(0) + "'" _
                              & " AND SBG_STM_ID=" + grade(1) _
                              & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'ddlSubject.DataSource = ds
        'ddlSubject.DataTextField = "SBG_DESCR"
        'ddlSubject.DataValueField = "SBG_SBM_ID"
        'ddlSubject.DataBind()

        chkListSubjects.DataSource = ds
        chkListSubjects.DataTextField = "SBG_DESCR"
        chkListSubjects.DataValueField = "SBG_SBM_ID"
        chkListSubjects.DataBind()

    End Sub

    Sub BindGroups()
        ddlGroup.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M " _
                              & " INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                              & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SGR_GRD_ID='" + grade(0) + "'" _
                              & "AND SBG_STM_ID=" + grade(1) _
                              & " AND SBG_SBM_ID in (SELECT * FROM OASISFIN..FNSPLITME('" + GetSelectedSubjects() + "','|'))" _
                              & " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGroup.Items.Insert(0, li)
    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try

            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindSubjects()
            BindGroups()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindSubjects()
            BindGroups()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub


    Private Function GetSelectedSubjects() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In chkListSubjects.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Value
            End If
        Next
        Return SBG_IDs
    End Function
    Private Function GetSelectedSubjects_Text() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In chkListSubjects.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Text
            End If
        Next
        Return SBG_IDs
    End Function
    Protected Sub chkListSubjects_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindGroups()
    End Sub
End Class
