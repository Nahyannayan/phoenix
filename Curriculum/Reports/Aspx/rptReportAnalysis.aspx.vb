Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280010") _
                AndAlso (ViewState("MainMnu_code") <> "C280015") _
                AndAlso (ViewState("MainMnu_code") <> "C280020") _
                AndAlso (ViewState("MainMnu_code") <> "C280025") _
                AndAlso (ViewState("MainMnu_code") <> "C280030") _
                AndAlso (ViewState("MainMnu_code") <> "C280035") _
                AndAlso (ViewState("MainMnu_code") <> "C280040") _
                AndAlso (ViewState("MainMnu_code") <> "C280045") _
                 AndAlso (ViewState("MainMnu_code") <> "C280050") _
                 AndAlso (ViewState("MainMnu_code") <> "C280055") _
                AndAlso (ViewState("MainMnu_code") <> "C280060") _
                AndAlso (ViewState("MainMnu_code") <> "C280065") _
                AndAlso (ViewState("MainMnu_code") <> "C280070") _
                AndAlso (ViewState("MainMnu_code") <> "C280075") _
                AndAlso (ViewState("MainMnu_code") <> "C280125") _
                 AndAlso (ViewState("MainMnu_code") <> "C280130") AndAlso (ViewState("MainMnu_code") <> "C500375") AndAlso (ViewState("MainMnu_code") <> "C280320") _
                    AndAlso (ViewState("MainMnu_code") <> "C280120" AndAlso (ViewState("MainMnu_code") <> "C280150") AndAlso (ViewState("MainMnu_code") <> "C400365")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    SetupFilterScreen(ViewState("MainMnu_code"))
                    BindReportType()
                    BindReportPrintedFor()
                    BindHeader()
                    GetAllGrade()

                    'If ViewState("MainMnu_code") = "C280070" Or ViewState("MainMnu_code") = "C280150" Then
                    '    trHeader.Visible = False
                    'End If

                    'If ViewState("MainMnu_code") = "C280120" Or ViewState("MainMnu_code") = "C280130   " Then
                    '    trHeader.Visible = True
                    'End If

                    If Session("sbsuid") = "125011" Then
                        trFilter.Visible = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

    Private Sub SetupFilterScreen(ByVal mnuCode As String)
        chkGetAllHeaders.Visible = False
        trSubjectChkList.Visible = False
        trFilter.Visible = True
        Select Case mnuCode
            Case "C280010"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trSelStudent.Visible = False
                trSubject.Visible = False
                trTopRows.Visible = False
            Case "C280015"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = True
                trTopRows.Visible = True
            Case "C280020"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = True
                trTopRows.Visible = True
            Case "C280025", "C280125"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trSelStudent.Visible = True
                trSubject.Visible = False
                trHeader.Visible = False
                trTopRows.Visible = False
            Case "C280030"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = True
                trHeader.Visible = True
                trTopRows.Visible = False
            Case "C280035"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trRPFID.Visible = False
                chkGetAllHeaders.Visible = True
                'trSelStudent.Visible = False
                trSubject.Visible = False
                trHeader.Visible = True
                trTopRows.Visible = False
            Case "C280040"
                trGradeSection.Visible = True
                trGrade.Visible = False
                'trSelStudent.Visible = False
                trSubject.Visible = False
                trHeader.Visible = True
                trTopRows.Visible = False
            Case "C280045"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trSelStudent.Visible = False
                trSubject.Visible = True
                trHeader.Visible = True
                trTopRows.Visible = False
                h_HasSection.Value = "false"
            Case "C280050"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trSelStudent.Visible = False
                trHeader.Visible = True
                h_HasSection.Value = "false"
                trTopRows.Visible = False
            Case "C280055", "C280060"
                trGradeSection.Visible = True
                trGrade.Visible = False
                trSelStudent.Visible = False
                trHeader.Visible = True
                h_HasSection.Value = "false"
                trTopRows.Visible = True
            Case "C280065", "C280070", "C280120", "C280150", "C280130", "C280320"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = False
                trTopRows.Visible = False
                trSubjectChkList.Visible = True
            Case "C400365"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = False
                trTopRows.Visible = False
                trSubjectChkList.Visible = True
                trFilter.Visible = True
            Case "C280075"
                trGradeSection.Visible = False
                trGrade.Visible = True
                trSelStudent.Visible = False
                trSubject.Visible = False
                trTopRows.Visible = True
                trSubjectChkList.Visible = True
            Case "C500375"
                trHeader.Visible = False
                trGradeSection.Visible = False
                trSubject.Visible = False
                trTopRows.Visible = False
                trSubjectChkList.Visible = False
                trSelStudent.Visible = False
        End Select
    End Sub

    Sub BindReportPrintedFor()
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub

    Sub BindHeader()
        Dim str_Sql As String = "SELECT RSD_ID, RSD_HEADER FROM RPT.REPORT_SETUP_D " & _
        " WHERE (ISNULL(RSD_bALLSUBJECTS, 0) = 1 AND ISNULL(RSD_bDIRECTENTRY, 0) = 0 " & _
        " OR ISNULL(RSD_bPERFORMANCE_INDICATOR, 0) = 1 )" & _
        " AND RSD_RSM_ID = " & ddlReportType.SelectedValue
        ddlHeader.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, _
         CommandType.Text, str_Sql)
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub

    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
        BindReportPrintedFor()
        BindHeader()
        GetAllGrade()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
  
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Sub GetAllGrade()
        'Dim bSuperUsr As Boolean = False
        'If Session("CurrSuperUser") = "Y" Then
        '    bSuperUsr = True
        'End If
        Dim bSuperUsr As Boolean = True

        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

        ddlGrade1.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade1.DataTextField = "GRM_DISPLAY"
        ddlGrade1.DataValueField = "GRD_ID"
        ddlGrade1.DataBind()

        GetStream()
        GetSectionForGrade()
        If trSubjectChkList.Visible Then
            BindSubjects(ddlGrade1.SelectedValue, ddlStream.SelectedValue)
        End If
    End Sub

    Sub GetStream()
        Dim str_sql As String = "SELECT DISTINCT STM_ID, STM_DESCR FROM VW_GRADE_BSU_M INNER JOIN" & _
        " VW_STREAM_M ON VW_GRADE_BSU_M.GRM_STM_ID = VW_STREAM_M.STM_ID " & _
        " WHERE GRM_GRD_ID = '" & ddlGrade1.SelectedValue & "'  AND GRM_ACD_ID = " & _
        ddlAca_Year.SelectedValue & " AND GRM_BSU_ID = '" & Session("sBSUID") & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()

        ddlStream1.DataSource = ds
        ddlStream1.DataTextField = "STM_DESCR"
        ddlStream1.DataValueField = "STM_ID"
        ddlStream1.DataBind()

        If ddlStream.Items.Count > 1 Then
            If trGrade.Visible = True Then
                h_HideStream.Value = "1"
                ddlStream.Visible = True
            Else
                h_HideStream.Value = "4"
                ddlStream1.Visible = True
            End If

            'Response.Write("javascript:document.getElementById('<%=ddlStream.ClientID %>').style.display = 'inline'")
        Else
            If trGrade.Visible = True Then
                h_HideStream.Value = "0"
                ddlStream.Visible = False
            Else
                h_HideStream.Value = "3"
                ddlStream1.Visible = False
            End If

            'Response.Write("javascript:document.getElementById('<%=ddlStream.ClientID %>').style.display = 'none'")
        End If
    End Sub

    Sub GetSectionForGrade()

        'Dim bSuperUsr As Boolean = False
        'If Session("CurrSuperUser") = "Y" Then
        '    bSuperUsr = True
        'End If

        Dim bSuperUsr As Boolean = True
        ddlSection.DataSource = GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        'If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
        'ddlSection.Items.Add(New ListItem("ALL", "ALL"))
        'ddlSection.Items.FindByText("ALL").Selected = True
        'End If

        If ViewState("MainMnu_code") = "C280010" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Public Function GetSectionForGrade(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, Optional ByVal vEMP_ID As String = "", Optional ByVal bSuperUser As Boolean = True) As DataSet
        Dim str_Sql As String = String.Empty
        If bSuperUser Then
            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
            " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
            " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
            " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "') " _
            & " AND GRM_STM_ID = " & ddlStream1.SelectedValue & " order by SECTION_M.SCT_DESCR "
        Else
            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
            " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
            " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
            " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "') AND SECTION_M.SCT_EMP_ID = " & vEMP_ID _
            & " AND GRM_STM_ID = " & ddlStream1.SelectedValue & " order by SECTION_M.SCT_DESCR "
        End If

        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            hfbDownload.Value = 0
            CallReports()
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Sub CallReports()
        If h_STU_IDs.Value = "" Then
            Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
            h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, str_sec)
        End If
        Select Case ViewState("MainMnu_code")
            Case "C280010"
                ClassPerformanceAcrossSubjects()
            Case "C280015"
                TopAchievers_LowPerformersGradewise(True)
            Case "C280020"
                TopAchievers_LowPerformersGradewise(False)
            Case "C280025"
                StudentsPerformanceAcrossGrades(h_STU_IDs.Value)
            Case "C280030"
                SectionWisePerformancePerSubject()
            Case "C280035"
                StudentsPerformanceAcrossTerm()
                'May need a different Filtering screen
            Case "C280040"
                StudentsPerformanceForTerm()
            Case "C280045"
                AllStudentsPerformanceForSubject()
            Case "C280055"
                TopAchievers_LowPerformersGradewise_Subjectwise(True)
            Case "C280050"
                GradeDistributionAcrossGrade()
            Case "C280060"
                TopAchievers_LowPerformersGradewise_Subjectwise(False)
            Case "C280065"
                TopPerformers()
            Case "C280070"
                GRADEDISTRIBUTION_SUBJECT_GRADE_WISE()
            Case "C280120", "C280150", "C280130", "C280320"
                GRADEDISTRIBUTION_SUBJECT_GRADE_WISE_BYGENDER()
            Case "C400365"
                SubjectGradeAnalysisAcrossYear()

            Case "C280075"
                Top_OverAllPerformers_Gradewise()
            Case "C280125"
                GenerateHLAPGraph()
            Case "C500375"
                GenerateOverallPerformanceAnalysis()
        End Select
    End Sub

    Sub GenerateHLAPGraph()
        Dim param As New Hashtable
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_HLAP_GRAPH_STUDWISE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Sub GenerateOverallPerformanceAnalysis()
        Dim param As New Hashtable

        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue.ToString)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("schedule", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_TWSPerformancereport.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub


    Private Sub GRADEDISTRIBUTION_SUBJECT_GRADE_WISE()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@SBG_ID", GetSelectedSubjects())
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param.Add("@STM_ID", ddlStream.SelectedValue)
        param.Add("@RSD_ID_P", ddlHeader.SelectedValue.ToString)
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlStream.Visible = True Then
            param.Add("Grade", ddlGrade1.SelectedItem.Text + " " + ddlStream.SelectedItem.Text)
        Else
            param.Add("Grade", ddlGrade1.SelectedItem.Text)
        End If

        param.Add("rptschedule", ddlReportPrintedFor.SelectedItem.Text)
        '  param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysis.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub


    Private Sub GRADEDISTRIBUTION_SUBJECT_GRADE_WISE_BYGENDER()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@SBG_ID", GetSelectedSubjects())
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param.Add("@STM_ID", ddlStream.SelectedValue)

        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlStream.Visible = True Then
            param.Add("Grade", ddlGrade1.SelectedItem.Text + " " + ddlStream.SelectedItem.Text)
        Else
            param.Add("Grade", ddlGrade1.SelectedItem.Text)
        End If

        param.Add("rptschedule", ddlReportPrintedFor.SelectedItem.Text)
        '  param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ViewState("MainMnu_code") = "C280120" Then
                param.Add("@RSD_ID_P", ddlHeader.SelectedValue.ToString)
                param.Add("@TYPE", ddlType.SelectedValue.ToString)
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysisByGender.rpt")
            ElseIf ViewState("MainMnu_code") = "C280130" Then
                param.Add("@RSD_ID_P", ddlHeader.SelectedValue.ToString)
                param.Add("@TYPE", ddlType.SelectedValue.ToString)
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysis_ByGroup.rpt")
            ElseIf ViewState("MainMnu_code") = "C280320" Then
                param.Add("@RSD_ID_P", ddlHeader.SelectedValue.ToString)
                param.Add("@TYPE", ddlType.SelectedValue.ToString)
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysis_ByNationality.rpt")
                 ElseIf ViewState("MainMnu_code") = "C280320" Then
            Else
                param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
                param.Add("@TYPE", ddlType.SelectedValue.ToString)
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysisByGenderAcrossYear.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Sub SubjectGradeAnalysisAcrossYear()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param.Add("@STM_ID", "0")
        param.Add("@SBG_ID", GetSelectedSubjects())
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
        param.Add("@bGENDER", "true")
        param.Add("@TYPE", ddlType.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/SEF Reports/rptIntlSEFSubjectGradeAnalysisAcrossYear.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub



    Private Sub TopPerformers()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@SBG_IDs", GetSelectedSubjects())
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@COUNT", 20)
        param.Add("@TYPE", ddlType.SelectedValue)

        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_TOP_PERFORMERS_GRADEWISE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Function GetSelectedSubjects() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In chkListSubjects.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Value
            End If
        Next
        Return SBG_IDs
    End Function

    Private Sub AllStudentsPerformanceForSubject()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@SBG_ID", h_SBG_IDs.Value)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_SUBJECTWISE_MARK_STUDENT.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub GradeDistributionAcrossGrade()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param("@SBG_ID") = h_SBG_IDs.Value
        param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_SUBJECTWISE_NOSTUDENTS_GRADE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub Top_OverAllPerformers_Gradewise()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade1.SelectedValue.Split("_")(0))
        param("@SBG_IDs") = GetSelectedSubjects()
        param.Add("@COUNT", txtTopNNumber.Text)
        'param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@TYPE", ddlType.SelectedValue.ToString)
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_TOP_OVERALLPERFORMERS_GRADEWISE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub TopAchievers_LowPerformersGradewise_Subjectwise(ByVal bTop As Boolean)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param("@SBG_ID") = h_SBG_IDs.Value
        param.Add("@COUNT", txtTopNNumber.Text)
        param.Add("@SCT_ID", ddlSection.SelectedValue)
        param.Add("@bAchievers", bTop)
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If bTop Then
            param.Add("REPORT_CAPTION", "TOP 10 ACHIEVERS(PER SUBJECT ACROSS GRADE)")
        Else
            param.Add("REPORT_CAPTION", "TOP 10 LOW PERFORMERS(PER SUBJECT ACROSS GRADE)")
        End If
        'param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_PRICIPAL_TOP_LOW_PERFORMERS_SUBJECTWISE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub StudentsPerformanceAcrossTerm()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", GetAllRPFIDForRSMID(ddlReportType.SelectedValue))
        If chkGetAllHeaders.Checked Then
            param.Add("@RSD_ID", 0) 'ddlHeader.SelectedValue)
        Else
            param.Add("@RSD_ID", ddlHeader.SelectedValue)
        End If
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case Session("SBSUID")
                Case "125005"
                    .reportPath = Server.MapPath("../Rpt/rpt_RA_INT_FORMTUTOR_MARK_STUDENT.rpt")
                Case Else
                    If chkGetAllHeaders.Checked Then
                        .reportPath = Server.MapPath("../Rpt/rpt_RA_FORMTUTOR_COMPARE_ALL_HEADER_TERMS_STUDENT.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rpt_RA_FORMTUTOR_COMPARE_TERMS_STUDENT.rpt")
                    End If
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Function GetAllRPFIDForRSMID(ByVal vRSM_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim RPF_IDs As String = String.Empty
        str_sql = "select RPF_ID from RPT.REPORT_PRINTEDFOR_M where RPF_RSM_ID = '" & vRSM_ID & "'"
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        While drReader.Read
            RPF_IDs = RPF_IDs & "|" & drReader(0)
        End While
        Return RPF_IDs
    End Function

    Private Sub StudentsPerformanceForTerm()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case Session("SBSUID")
                Case "125005"
                    .reportPath = Server.MapPath("../Rpt/rpt_RA_INT_FORMTUTOR_MARK_STUDENT.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/rpt_RA_FORMTUTOR_MARK_STUDENT.rpt")
                    '.reportPath = Server.MapPath("../Rpt/rpt_RA_HLAP_GRAPH_STUDWISE.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    'rpt_RA_INT_FORMTUTOR_MARK_STUDENT.rpt

    Private Sub SectionWisePerformancePerSubject()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param("@SBG_ID") = h_SBG_IDs.Value
        param("RPT_CAPTION") = ddlReportPrintedFor.SelectedItem.Text
        'param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@TYPE", ddlType.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_SUPERVISOR_MARK_SECTION.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub StudentsPerformanceAcrossGrades(ByVal vSTU_IDs As String)
        Dim param As New Hashtable
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@STU_ID", vSTU_IDs.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_PRICIPAL_STUDENTS_PERFORMANCE_ACROSS_GRADES.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        'Dim bSuperUsr As Boolean = False
        'If Session("CurrSuperUser") = "Y" Then
        '    bSuperUsr = True
        'End If

        Dim bSuperUsr As Boolean = True
        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    str_sec_ids = dr("SCT_ID").ToString + comma
                    comma = ", "
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function

    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        str_sql = "SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        " STUDENT_M  " & _
        " WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        "' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        " AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,''),'') "
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Private Sub TopAchievers_LowPerformersGradewise(ByVal bTop As Boolean)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade1.SelectedValue)
        param.Add("@COUNT", txtTopNNumber.Text)
        param("@SBG_ID") = h_SBG_IDs.Value
        param.Add("@SCT_ID", 0)
        param.Add("@bAchievers", bTop)
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If bTop Then
            param.Add("REPORT_CAPTION", "TOP " & txtTopNNumber.Text & " ACHIEVERS(GRADE WISE)")
        Else
            param.Add("REPORT_CAPTION", "TOP " & txtTopNNumber.Text & " LOW PERFORMERS(GRADE WISE)")
        End If
        'param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_PRICIPAL_TOP_LOW_PERFORMERS_SUBJECTWISE.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Private Sub ClassPerformanceAcrossSubjects()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@RSD_ID", ddlHeader.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", GetSelectedSection(ddlSection.SelectedValue))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("schedule", ddlReportPrintedFor.SelectedItem.Text)
        param.Add("header", ddlHeader.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case Session("SBSUID")
                Case "125005"
                    .reportPath = Server.MapPath("../Rpt/rpt_RA_INT_CLASS_PERFORMANCE_ACROSS_SUBJECT.rpt")
                Case Else
                    '.reportPath = Server.MapPath("../Rpt/rptTest.rpt")
                    param.Add("@IMG_BSU_ID", Session("sbsuid"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("../Rpt/rpt_RA_FORMTUTOR_CLASS_PERFORMANCE_ACROSS_SUBJECT.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing

        Else
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAca_Year.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportType.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function


    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("___"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()
        BindHeader()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub chkGetAllHeaders_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGetAllHeaders.CheckedChanged
        ddlHeader.Enabled = Not chkGetAllHeaders.Checked
    End Sub

    Protected Sub ddlGrade1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade1.SelectedIndexChanged
        If trSubjectChkList.Visible Then
            GetStream()
            BindSubjects(ddlGrade1.SelectedValue, ddlStream.SelectedValue)
        End If
    End Sub

    Public Sub BindSubjects(ByVal GRD_ID As String, ByVal STM_ID As String)
        Dim str_sql As String = "SELECT SBG_ID, SBG_DESCR FROM SUBJECTS_GRADE_S " & _
        " WHERE SBG_bMAJOR = 1 AND SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
        " AND SBG_ACD_ID = " & ddlAca_Year.SelectedValue & " AND SBG_GRD_ID ='" & GRD_ID & "' " & _
        " and SBG_STM_ID =" & STM_ID

        If ViewState("MainMnu_code") <> "C400365" Then
            str_sql += " AND SBG_PARENT_ID = 0"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
        chkListSubjects.DataSource = ds
        chkListSubjects.DataTextField = "SBG_DESCR"
        chkListSubjects.DataValueField = "SBG_ID"
        chkListSubjects.DataBind()
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        If trSubjectChkList.Visible Then
            BindSubjects(ddlGrade1.SelectedValue, ddlStream.SelectedValue)
        End If
    End Sub

    Protected Sub ddlStream1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream1.SelectedIndexChanged
        GetSectionForGrade()
        If trSubjectChkList.Visible Then
            BindSubjects(ddlGrade1.SelectedValue, ddlStream.SelectedValue)
        End If
    End Sub
    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Try
            hfbDownload.Value = 1
            CallReports()
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
