﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_AKN_NewReports.aspx.vb" Inherits="Curriculum_Reports_Aspx_AKNNewReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <table class="BlueTable" id="tblrule" runat="server" align="center"  cellpadding="4" cellspacing="0" style="width: 100%">
        
        <tr>
            <td align="left" > <span class="field-label">Academic Year</span></td>
          
            <td align="left" >
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
        
            <td align="left" > <span class="field-label">Report Card</span></td>
            
            <td align="left" >
                <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" Width="303px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" valign="middle" > <span class="field-label">Report Schedule</span></td>
            
            <td align="left" valign="middle"  >
                <asp:DropDownList ID="ddlPrintedFor" runat="server" Width="197px">
                </asp:DropDownList></td>
        

            <td align="left" valign="middle"  > <span class="field-label">Grade</span></td>
           
            <td align="left" valign="middle"  >
                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" Width="172px">
                </asp:DropDownList></td>
        </tr>

        <tr id="trCriteria">
            <td align="left"  valign="middle" width="20%"> <span class="field-label">Report Header</span></td>
          
            <td align="left"  valign="middle">
                <asp:DropDownList ID="ddlReportHeader" runat="server" AutoPostBack="True" Width="172px">
                </asp:DropDownList></td>
        </tr>


        <tr id="trSection">
            <td align="left"  valign="middle"  width="20%"> <span class="field-label">Section</span></td>
           
            <td align="left"  valign="middle">
                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="105px">
                </asp:DropDownList></td>
        </tr>

        <%--<tr >
            <td align="left"  valign="middle">
                Report</td>
            <td align="center"  valign="middle">
                :</td>
            <td align="left"  valign="middle">
                <asp:RadioButton ID="radGrade" runat="server" GroupName="RadReport" Text="Student Rank for all Grades" Checked="true" />
                        <asp:RadioButton ID="radSection" runat="server" GroupName="RadReport" Checked="false" Text="Section Wise Average" />

            </td>
        </tr>--%>

        <%--  
        <tr id="trMark" runat="server" >
            <td align="center"  valign="middle" colspan="3">
                <asp:TextBox ID="txtMark1" runat="server" Width="54px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblAnd" runat="server" Text="And"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtMark2" runat="server" Visible="False" Width="54px"></asp:TextBox>
            </td>
        </tr>--%>
        <%--<tr >
            <td align="left"  valign="middle">
                No of Records</td>
            <td align="center"  valign="middle">
                :</td>
            <td align="left"  valign="middle">
                <asp:TextBox ID="txtRecord" runat="server">10</asp:TextBox>
            </td>
        </tr>--%>
        <tr>
            <td align="left" colspan="4" style="text-align: center">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                    Text="Generate Report" ValidationGroup="groupM1" Width="136px" />&nbsp;
                   <asp:Button ID="btnDownload" runat="server" CssClass="button" Height="24px" Text="Download in PDF" />
            </td>
        </tr>
    </table>
                </div>
            </div>
         </div>
    <asp:HiddenField ID="hfbDownload" runat="server" />
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>

</asp:Content>

