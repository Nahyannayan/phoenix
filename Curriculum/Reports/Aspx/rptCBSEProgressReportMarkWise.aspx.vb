Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim rs As New ReportDocument

    Private Function isPageExpired() As Boolean


        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        '   ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT) _
                AndAlso (ViewState("MainMnu_code") <> "C300300") AndAlso (ViewState("MainMnu_code") <> "C300555") AndAlso (ViewState("MainMnu_code") <> "StudentProfile") AndAlso (ViewState("MainMnu_code") <> "PdfReport") _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")


                    If ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
                        BindProfileReport(sender, e)
                    Else
                        BindReportType()
                        BindReportCardType()
                        If (ViewState("MainMnu_code") = "C400005") Then
                            BindReportPrintedFor(True)
                        Else
                            BindReportPrintedFor()
                        End If
                        GetAllGrade()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub

    Sub BindProfileReport(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rsmId As String = Encr_decrData.Decrypt(Request.QueryString("rsmId").Replace(" ", "+"))
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT RSM_ACD_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + rsmId + "'"
        Dim acdId As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        h_STU_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("StuId").Replace(" ", "+"))
        Dim grdId As String
        If acdId <> Session("Current_ACD_ID") Then
            str_query = "SELECT STP_GRD_ID FROM OASIS..STUDENT_PROMO_S WHERE STP_STU_ID='" + h_STU_IDs.Value + "'" _
                      & " AND STP_ACD_ID='" + acdId + "'"
            grdId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Else
            grdId = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
        End If
        ' Dim acdId As String = Encr_decrData.Decrypt(Request.QueryString("acdId").Replace(" ", "+"))
        Dim rpfId As String = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))


        If Not ddlAca_Year.Items.FindByValue(acdId) Is Nothing Then
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(acdId).Selected = True
            ddlAca_Year_SelectedIndexChanged(sender, e)
        End If
        If Not ddlReportType.Items.FindByValue(rsmId) Is Nothing Then
            ddlReportType.ClearSelection()
            ddlReportType.Items.FindByValue(rsmId).Selected = True
            ddlReportType_SelectedIndexChanged(sender, e)
        End If
        If Not ddlReportPrintedFor.Items.FindByValue(rpfId) Is Nothing Then
            ddlReportPrintedFor.ClearSelection()
            ddlReportPrintedFor.Items.FindByValue(rpfId).Selected = True
        End If
        If Not ddlGrade.Items.FindByValue(grdId) Is Nothing Then
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByValue(grdId).Selected = True
        End If
        BindReportCardType()
        btnGenerateReport_Click(sender, e)
    End Sub

    Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)


        'for aks final report two fomats(one for ministry and one for school has to be issued
        If ddlReportType.SelectedItem.Text.Contains("FINAL") And Session("sbsuid") = "126008" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString
            str_sql += " UNION ALL  SELECT 0 ,'FINAL REPORT - MINISTRY' FROM " & _
                       "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString

            '" AND ISNULL(RPF_bFINAL_REPORT ,0) = '" & bFINAL_REPORT & _
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)



            ddlReportPrintedFor.DataSource = ds
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        Else
            ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        End If

    End Sub

    Sub BindReportType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT RSM_ID, RSM_DESCR FROM RPT.REPORT_SETUP_M " & _
            " WHERE RSM_BSU_ID = '" & Session("sBSUID") & "' AND RSM_ACD_ID =" & ddlAca_Year.SelectedValue & " and RSM_progressmark=1 ORDER BY RSM_DISPLAYORDER"


        ddlReportType.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Sub BindReportCardType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_REPORTTYPE,''),ISNULL(RSM_REPORTFORMAT,'') FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + ddlReportType.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfReportType.Value = ds.Tables(0).Rows(0).Item(0)
        hfReportFormat.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Sub GetAllGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Or ViewState("MainMnu_code") = "PdfReport" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Or ViewState("MainMnu_code") = "StudentProfile" Then
            bSuperUsr = True
        End If
        If Session("sbsuid") = "125017" Then
            bSuperUsr = True
        End If
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hfbDownload.Value = 0
        CallReports()
    End Sub

    Private Function GetAllCBSEBusinessUnit(ByVal BSU_ID As String) As String
        Dim strQuery As String = " SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_CLM_ID = 1 AND BSU_ID = '" & BSU_ID & "'"
        Dim vBSU_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Return vBSU_ID
    End Function










    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function

    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        If Session("Current_ACD_ID") = ddlAca_Year.SelectedValue.ToString Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
           " AND STU_SCT_ID in (" & vSCT_ID & ")"
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
     " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
     " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
     "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
     " AND STP_SCT_ID in (" & vSCT_ID & ")  "
        End If

        If ddlGender.SelectedValue <> "ALL" Then
            str_sql += " AND STU_GENDER='" + ddlGender.SelectedValue + "'"
        End If

        str_sql += " for xml path('')),1,0,''),0) "

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        Return str
    End Function

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAca_Year.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportType.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Public Function GetAsOnDateForAttendance() As String
        Dim acd_id As String = ddlAca_Year.SelectedValue
        Dim grd_id As String = ddlGrade.SelectedValue 'CONVERT(VARCHAR(11), GETDATE(), 106) 
        Dim str_sql As String = " SELECT DISTINCT CONVERT(VARCHAR(11)," & _
        " ISNULL(MAX(OASIS.dbo.TERM_GRADEWISE.TRM_END_DT),GETDATE()), 103)  " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & acd_id & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & grd_id & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & ddlReportPrintedFor.SelectedValue

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

    End Function

    Sub BindBFinalReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M " _
                                 & " WHERE RSM_ID='" + ddlReportType.SelectedValue.ToString + "'"
        hfbFinalReport.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.ToLower
    End Sub

    Public Function GetTotalGradeAttendance() As String
        Dim acd_id As String = ddlAca_Year.SelectedValue
        Dim grd_id As String = ddlGrade.SelectedValue
        Dim str_sql As String = " SELECT DISTINCT ISNULL(OASIS.dbo.TERM_GRADEWISE.TRM_TOT_DAYS,0) " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & acd_id & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & grd_id & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & ddlReportPrintedFor.SelectedValue

        Dim total_att = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If total_att Is Nothing Then
            Return 0
        Else
            Return total_att
        End If
    End Function

    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO

        Dim arrList As ArrayList

        arrList = New ArrayList(h_STU_IDs.Value.Replace("|", "___").Split("___"))

        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function



    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportCardType()
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'code added by dhanya
        BindReportType()
        BindReportCardType()
        If (ViewState("MainMnu_code") = "C400005") Then
            BindReportPrintedFor(True)
        Else
            BindReportPrintedFor()
        End If
        GetAllGrade()
        GetSectionForGrade()
    End Sub
    Sub LoadReports(ByVal rptClass)


        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If

                If ViewState("MainMnu_code") = "PdfReport" Then
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    rs.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                    rs.Close()
                    rs.Dispose()
                Else
                    Try
                        exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                    Catch ee As Exception
                    End Try
                    rs.Close()
                    rs.Dispose()
                End If
                ' Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            rs.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    'Sub LoadReports(ByVal rptClass)
    '    Try
    '        Dim iRpt As New DictionaryEntry
    '        Dim i As Integer
    '        Dim newWindow As String


    '        Dim rptStr As String = ""

    '        Dim crParameterDiscreteValue As ParameterDiscreteValue
    '        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '        Dim crParameterFieldLocation As ParameterFieldDefinition
    '        Dim crParameterValues As ParameterValues


    '        With rptClass





    '            'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
    '            rs.ReportDocument.Load(.reportPath)


    '            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '            myConnectionInfo.ServerName = .crInstanceName
    '            myConnectionInfo.DatabaseName = .crDatabase
    '            myConnectionInfo.UserID = .crUser
    '            myConnectionInfo.Password = .crPassword


    '            SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
    '            SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


    '            'If .subReportCount <> 0 Then
    '            '    For i = 0 To .subReportCount - 1
    '            '        rs.ReportDocument.Subreports(i).VerifyDatabase()
    '            '    Next
    '            'End If


    '            crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
    '            If .reportParameters.Count <> 0 Then
    '                For Each iRpt In .reportParameters
    '                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '                    crParameterValues = crParameterFieldLocation.CurrentValues
    '                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '                    crParameterDiscreteValue.Value = iRpt.Value
    '                    crParameterValues.Add(crParameterDiscreteValue)
    '                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '                Next
    '            End If

    '            '  rs.ReportDocument.VerifyDatabase()


    '            If .selectionFormula <> "" Then
    '                rs.ReportDocument.RecordSelectionFormula = .selectionFormula
    '            End If

    '            If ViewState("MainMnu_code") = "PdfReport" Then
    '                Response.ClearContent()
    '                Response.ClearHeaders()
    '                Response.ContentType = "application/pdf"
    '                rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
    '                rs.ReportDocument.Close()
    '                rs.Dispose()
    '            Else
    '                Try
    '                    exportReport(rs.ReportDocument, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
    '                Catch ee As Exception
    '                End Try
    '                rs.ReportDocument.Close()
    '                rs.Dispose()
    '            End If
    '            ' Response.Flush()
    '            'Response.Close()
    '        End With
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        rs.ReportDocument.Close()
    '        rs.Dispose()
    '    End Try
    '    'GC.Collect()
    'End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                Dim hop As New CrystalDecisions.Shared.ExportOptions
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.ContentType = "application/pdf"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        ''HttpContext.Current.Response.Flush()
        ''HttpContext.Current.Response.Close()
        'HttpContext.Current.Response.End()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        System.IO.File.Delete(tempFileNameUsed)
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub", "rptWSOInterimSub", "rptCISImageSub", "rptWINImageSub", "rptWSSImageSub", "rptWISImageSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If Session("sbsuid") = "125017" Then
                            If Not Session("noimg") Is Nothing Then
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                            Else
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/NOIMG/no_img_gwa.jpg")
                            End If

                        ElseIf Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function



    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function


    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\" + Session("sbsuid") + "\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Sub CallReports()
        Try
            BindBFinalReport()
            If h_STU_IDs.Value = "" Then
                Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
                h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, str_sec)
            End If
            Select Case hfReportType.Value
                Case "CBSE_FORMATIVE"
                    GenerateFormativeAssessmentReport()
                Case "CBSE_SUMMATIVE"
                    GenerateSummativeAssessmentReport()

                Case Else
                    If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("REHEARSAL") _
 Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("MODEL") _
 Or ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("REVISION") Then

                        If ddlGrade.SelectedValue = "10" Then
                            GenerateRehearsalReport_10()
                        End If

                    ElseIf ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("TERM") OrElse _
                    ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") OrElse _
                    ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("QUARTERLY") Then

                        Select Case ddlGrade.SelectedValue
                            Case "01", "02", "03", "04"

                                If ddlReportPrintedFor.SelectedItem.Text.ToUpper.Contains("FINAL") Then
                                    GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                                Else
                                    GenerateCBSETermReport01_04()
                                End If

                            Case Else

                                GenerateCBSEFinalReport01_04(ddlGrade.SelectedValue)
                        End Select


                    End If
            End Select
        Catch ex As Exception
            ' lblerror.Text = ex.Message
        End Try
    End Sub

    Private Sub GenerateSummativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Then
            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")
            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                param.Add("SA_PERC", "20%")
                param.Add("TOTAL_PERC", "40%")
            ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
                param.Add("SA_PERC", "20%")
                param.Add("TOTAL_PERC", "40%")
            Else
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
            End If
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")
            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            ElseIf ddlAca_Year.SelectedItem.Text = "2011-2012" And ddlGrade.SelectedValue = "10" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            Else
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
            End If
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If


        Dim vSA_HEADER As String = "SA"

        If ddlGrade.SelectedValue = "10" Then
            If ddlReportPrintedFor.SelectedItem.Text.ToUpper <> "SUMMATIVE REPORT 1" Then
                param.Add("SA_HEADER", "Rehearsal Exam")
            Else
                param.Add("SA_HEADER", vSA_HEADER)
            End If
        Else
            param.Add("SA_HEADER", vSA_HEADER)
        End If


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
            Else
                If ddlGrade.SelectedValue = "10" And ddlAca_Year.SelectedItem.Text = "2011-2012" Then
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_10_new.rpt")
                Else
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10_MARK.rpt")
                    '.reportPath = Server.MapPath("../Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
                End If

            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateFormativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Select Case ddlReportPrintedFor.SelectedItem.Text
            Case "FORMATIVE ASSESSMENT 1"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT 1")

        End Select

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            Select Case GetReportType()
                Case "FORMATIVE_ASSESSMENT_KGS"
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03_KGS_MARK.rpt")
                Case Else
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03_MARK.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateRehearsalReport_10()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        If ddlAca_Year.SelectedValue = "624" Then
            param.Add("RPT_ASSESSMENT_HEADER", "EXAM")
        Else
            param.Add("RPT_ASSESSMENT_HEADER", "REHEARSAL EXAM")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03_MARK.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCBSEFinalReport01_04(ByVal GRD_ID As String)

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        'param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            '.Photos = GetPhotoClass()
            Select Case GRD_ID
                Case "01", "02", "03", "04"
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptFINAL_REPORT_CBSE_01_04_MARK.rpt")
                Case "11", "12"
                    Select Case GetReportType()
                        Case "GRADE_11_12_FORMAT1"
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT1.rpt")
                        Case "GRADE_11_12_FORMAT3"
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT3.rpt")
                        Case "GRADE_11_12_FORMAT8"
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                        Case "GRADE_11_12_FORMAT9"
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT9.rpt")
                        Case "GRADE_11_12_FORMAT10"
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT10.rpt")
                        Case Else
                            .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_11_FORMAT2.rpt")
                    End Select
                Case "05", "06", "07", "08"

                    .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_FINALREPORT_05_08_MARK.rpt")

                    'Case "09"
                    '    Select Case hfReportFormat.Value
                    '        Case "CBSE_FINALREPORT_09"
                    '            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    '            param.Add("@IMG_TYPE", "LOGO")
                    '            param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    '            .reportPath = Server.MapPath("../Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                    '            .Photos = GetPhotoClass()
                    '        Case Else
                    '            If ddlAca_Year.SelectedItem.Text = "2010-2011" Then
                    '                .reportPath = Server.MapPath("../Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
                    '            Else
                    '                If Session("sbsuid") = "121012" Or Session("sbsuid") = "121013" Or Session("sbsuid") = "121014" Or Session("sbsuid") = "123006" Then
                    '                    .reportPath = Server.MapPath("../Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
                    '                Else
                    '                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    '                    param.Add("@IMG_TYPE", "LOGO")
                    '                    param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
                    '                    .reportPath = Server.MapPath("../Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                    '                    .Photos = GetPhotoClass()
                    '                End If
                    '            End If
                    '    End Select
                Case Else
                    .reportPath = Server.MapPath("../Rpt/CBSE/rptFINAL_REPORT_CBSE_05_09_MARK.rpt")
            End Select
            .reportParameters = param
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCBSETermReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("../Rpt/CBSE/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04_MARK.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub



    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReports()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            rs.Dispose()

        Catch ex As Exception

        End Try
        'GC.Collect()
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx')", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank')", True)
        End If
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
