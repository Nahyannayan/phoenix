﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptAOL_RA_ClassAverageByAssessment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290190" And ViewState("MainMnu_code") <> "C500190") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                BindAssessment()
                BindSection()
                PopulateSubjects()
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

   



    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY " _
                          & " ,grm_grd_id,grd_displayorder FROM " _
                          & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                          & " where grm_acd_id=" + acdid _
                          & " AND GRD_ID NOT IN('KG1','KG2','11','12')" _
                          & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindSection()
      
        lstSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                   & "  AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSection.DataSource = ds
        lstSection.DataTextField = "SCT_DESCR"
        lstSection.DataValueField = "SCT_ID"
        lstSection.DataBind()
     
    End Sub

    Sub PopulateSubjects()
        lstSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "'" _
                                 & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        
        lstSubject.DataSource = ds
        lstSubject.DataTextField = "SBG_DESCR"
        lstSubject.DataValueField = "SBG_ID"
        lstSubject.DataBind()

        
    End Sub

    Sub BindAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CAD_ID,CAD_DESC FROM ACT.ACTIVITY_D AS A " _
                                & " INNER JOIN ACT.ACTIVITY_SCHEDULE AS B ON A.CAD_ID=B.CAS_CAD_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.CAS_SBG_ID=C.SBG_ID" _
                                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " ORDER BY CAD_DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lstAssessment.DataSource = ds
        lstAssessment.DataTextField = "CAD_DESC"
        lstAssessment.DataValueField = "CAD_ID"
        lstAssessment.DataBind()
    End Sub

    

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += lstSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function

    Function getAssessment() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To lstAssessment.Items.Count - 1
            If lstAssessment.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += lstAssessment.Items(i).Value
            End If
        Next
        Return subs

    End Function

    Function getSections() As String
        Dim subs As String = ""
        Dim i As Integer
        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += lstSection.Items(i).Value()
            End If
        Next
        Return subs
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@SBG_IDS", getSubjects)
        param.Add("@SCT_IDS", getSections)
        param.Add("@CAD_IDS", getAssessment)
        param.Add("grade", ddlGrade.SelectedItem.Text)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        rptClass.crDatabase = "oasis_curriculum"
        rptClass.reportParameters = param
        rptClass.reportPath = Server.MapPath("../Rpt/rpt_RA_AOLClassAverageByAssessment.rpt")
        Session("rptClass") = rptClass

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region
   
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()
        PopulateSubjects()
        BindAssessment()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
          BindSection()
        PopulateSubjects()
        BindAssessment()
    End Sub

   

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Dim i As Integer
        For i = 0 To lstSubject.Items.Count - 1
            lstSubject.Items(i).Selected = chkAll.Checked
        Next
    End Sub
    Protected Sub chkAllAssessment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllAssessment.CheckedChanged
        Dim i As Integer
        For i = 0 To lstAssessment.Items.Count - 1
            lstAssessment.Items(i).Selected = chkAllAssessment.Checked
        Next
    End Sub


    Protected Sub chkAllSection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllSection.CheckedChanged
        Dim i As Integer
        For i = 0 To lstSection.Items.Count - 1
            lstSection.Items(i).Selected = chkAllSection.Checked
        Next
    End Sub
End Class
