<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTeacherWiseGroups.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTeacherWiseGroups" Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="lblHeader" runat="server">Teacher Wise Groups</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" WIDTH="100%"
                    cellpadding="5" cellspacing="0" >
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" >
                            <table id="Table1" runat="server" align="center" 
                                cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td align="center"  >
                                        <table id="Table4" runat="server" align="center"
                                            cellpadding="12" cellspacing="0" width="100%">
                                            <tr>
                                               
                                                <td align="left" width="20%">
                                                    <asp:Label ID="lbaC" runat="server" Text="Academic Year" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%" >
                                                    <asp:Label ID="lbst" runat="server" Text="Stream" CssClass="field-label" Style="text-align: left"></asp:Label>
                                                </td>
                                                <td  align="Left" width="30%" >
                                                    <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>

                                            </tr>

                                            <tr>
                                                <td   width="20%"  >
                                                    <span class="field-label">Grade</span>

                                                </td>
                                                 <td align="left" width="30%" >
                                                                <div class="checkbox-list">
                                                                <asp:CheckBoxList ID="lstGrade" runat="server" AutoPostBack="True">
                                                                </asp:CheckBoxList></div>
                                                            </td>
                                                <td   width="20%"  >
                                                   <span class="field-label"> Department</span></td>
                                                  <td align="left">
                                                            <div class="checkbox-list">
                                                            <asp:CheckBoxList ID="lstDPT" runat="server" AutoPostBack="True">
                                                            </asp:CheckBoxList></div>
                                                        </td>
                                                </tr>
                                            <tr>
                                                <td   width="20%"  > <span class="field-label"> Teacher</span></td>
                                                <td width="30%">
                                                     <asp:CheckBox id="chkTeacher" runat="server" AutoPostBack="True" Text="Check All" ></asp:CheckBox> 
                                                    <div class="checkbox-list">
                                                                <asp:CheckBoxList ID="lstTeacher" Width="100%" runat="server"  AutoPostBack="True">
                                                                </asp:CheckBoxList></div>
                                                </td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <!--  <tr>
                                    <td  colspan="4">
                                        &nbsp;</td>
                                </tr>-->
                                            <tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="center" valign="bottom">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                           
                <asp:HiddenField ID="hfTRV_ID" runat="server"></asp:HiddenField>
                            <input id="lstValues" runat="server" name="lstValues" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px"
                                type="hidden" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

