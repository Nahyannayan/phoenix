<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentPerformanceData.aspx.vb" Inherits="Curriculum_Reports_Aspx_clmConsolidatedFeedback" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

        function fnSelectAll_bkp(chkObj) {
            var multi = document.getElementById("<%=lstSubject.ClientID%>");
    if (multi != null && multi.options != null) {
        if (chkObj.checked)
            for (i = 0; i < multi.options.length; i++)
                multi.options[i].selected = true;
        else
            for (i = 0; i < multi.options.length; i++)
                multi.options[i].selected = false;
    }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Student Performance Data
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table  id="tblrule" runat="server" align="center" width="100%" cellpadding="4" cellspacing="0" >
                    <%-- <tr>
            <td valign="middle" class="subheader_img" colspan="3">
                <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label></td>
        </tr>--%>
                    <tr>
                        <td width="10%" align="left" ><span class="field-label">Academic Year</span></td>
                        
                        <td width="20%" align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr visible="false">
                        <td align="left" width="10%" ><span class="field-label">Report Card</span></td>
                        
                        <td align="left" width="20%" >
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr visible="false">
                        <td align="left" width="10%" valign="middle"  ><span class="field-label">Report Schedule</span></td>
                        
                        <td align="left"  width="20%" valign="middle"  >
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="10%" valign="middle"  ><span class="field-label">Grade</span></td>
                        
                        <td align="left" width="20%" valign="middle"  >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left" width="10%" valign="middle" ><span class="field-label">Options</span></td>
                        
                        <td align="left" width="20%" valign="middle" >
                            <asp:CheckBox ID="chkCAT" runat="server" Text="CAT 4" Checked="true" />&nbsp;<asp:CheckBox ID="chkSubject" runat="server" Text="Subject" AutoPostBack="true" /></td>
                    </tr>
                    <tr runat="server" id="trAsset" style="display: none;">
                        <td align="left"  width="10%"  valign="middle"><span class="field-label">Asset</span></td>
                       
                        <td align="left" width="20%"  valign="middle">
                            <asp:DropDownList ID="ddlAssest" runat="server" AutoPostBack="True" >
                                <asp:ListItem Text="Science Percentile" Value="SCI" />
                                <asp:ListItem Text="Math Percentile" Value="MAT" />
                                <asp:ListItem Text="English Percentile" Value="ENG" />
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trSubject" runat="server" style="display: none;">
                        <td align="left" width="10%" valign="middle" ><span class="field-label">Subject</span></td>
                        
                        <td align="left" width="20%" valign="middle" >&nbsp;
                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" Style="display: none;" />
                            <br />
                             <div class="checkbox-list" >
                            <asp:CheckBoxList ID="LstSubject" runat="server" >
                            </asp:CheckBoxList></div>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left" width="10%" valign="middle" ><span class="field-label">Header</span></td>
                       
                        <td align="left" width="20%" valign="middle" >
                            <div class="checkbox-list" >
                                 <asp:CheckBoxList ID="lstHeader" runat="server" >
                            </asp:CheckBoxList>
                            </div>                           
                            <asp:CheckBox ID="chkCriterias" Text="Include Subject Criterias" runat="server" /></td>
                    </tr>
                    <tr runat="server" style="display: none;">
                        <td align="left" width="10%"  valign="middle"><span class="field-label">Choose</span></td>
                        
                        <td align="left" width="20%"  valign="middle">
                            <asp:RadioButtonList ID="rblChoice" runat="server" OnSelectedIndexChanged="rblChoice_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0" Selected="True">Section-wise</asp:ListItem>
                                <asp:ListItem Value="1">Group-wise</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr runat="server" id="trSection">
                        <td align="left" width="10%"  valign="middle"><span class="field-label">Section</span></td>
                       
                        <td align="left" width="20%" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trGroup" style="display: none;">
                        <td align="left" width="10%"  valign="middle"><span class="field-label">Subject Group</span></td>
                        
                        <td align="left" width="20%"  valign="middle">
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" style="display: none;">
                        <td align="left" width="10%"  valign="middle"><span class="field-label">Include TC Students</span></td>
                        
                        <td align="left" width="20%"   valign="middle">
                            <asp:CheckBox ID="chkTc" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="trfilter" style="display: none;">
                        <td align="left" width="10%"  valign="middle"><span class="field-label">Filter By</span></td>
                        
                        <td align="left" width="20%"  valign="middle">
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnGeneratePDF" runat="server" CssClass="button" 
                    Text="Generate PDF" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfbDownload" runat="server" />

                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

