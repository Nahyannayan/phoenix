<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptLessonPlanSetup.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptLessonPlanSetup" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Lesson Plan Setup"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">

                    <tr>

                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td>

                            <table id="tblClm" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Subject</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                            Text="Download Report in PDF" ValidationGroup="groupM1" /></td>
                                </tr>

                            </table>
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>

