﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptSMARTSudentTargets
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300105" And ViewState("MainMnu_code") <> "C300107" And ViewState("MainMnu_code") <> "C300115" And ViewState("MainMnu_code") <> "C300116") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    studClass.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")

                    BindTargetSetup()
                    GetAllGrade()

                    If ViewState("MainMnu_code") = "C300107" Or ViewState("MainMnu_code") = "C300116" Then
                        trStudents.Visible = False
                    Else
                        trSubmitStatus.Visible = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub CallReport()
       
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue.ToString)
        If ViewState("MainMnu_code") = "C300115" Then
            param.Add("@SWM_ID", ddlTarget.SelectedValue.ToString)
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
        ElseIf ViewState("MainMnu_code") = "C300116" Then
            param.Add("@SWM_ID", ddlTarget.SelectedValue.ToString)
        Else
            param.Add("@TGM_ID", ddlTarget.SelectedValue.ToString)
        End If

        param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
        If ViewState("MainMnu_code") = "C300105" Or ViewState("MainMnu_code") = "C300115" Then
            param.Add("@STU_ID", IIf(h_STU_IDs.Value = "", "0", h_STU_IDs.Value.Replace("___", "|")))
        End If

        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            If ViewState("MainMnu_code") = "C300105" Then
                .reportPath = Server.MapPath("../Rpt/rptSmartTargetPrint.rpt")
            ElseIf ViewState("MainMnu_code") = "C300115" Then
                .reportPath = Server.MapPath("../Rpt/rptSWOCStudentDetails.rpt")
            Else
                param.Add("@IMG_BSU_ID", Session("SBSUID"))
                param.Add("@IMG_TYPE", "LOGO")
                If rdAll.Checked = True Then
                    param.Add("@SUBMITTED", "ALL")
                ElseIf rdSubmitted.Checked = True Then
                    param.Add("@SUBMITTED", "YES")
                Else
                    param.Add("@SUBMITTED", "NO")
                End If
                param.Add("@STATUS", "")
                If ViewState("MainMnu_code") = "C300116" Then
                    .reportPath = Server.MapPath("../Rpt/rptSWOCSubmitStatus.rpt")
                Else
                    .reportPath = Server.MapPath("../Rpt/rptSMARTSubmitStatus.rpt")
                End If
            End If
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub


    Sub BindTargetSetup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim ds As DataSet
        If ViewState("MainMnu_code") = "C300115" Then
            str_query = "SELECT SWM_ID,SWM_DESCR FROM SWOC.SWOC_SCHEDULE_M WHERE SWM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                                 & " ORDER BY SWM_ORDER"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlTarget.DataSource = ds
            ddlTarget.DataTextField = "SWM_DESCR"
            ddlTarget.DataValueField = "SWM_ID"
            ddlTarget.DataBind()
        Else
            str_query = "SELECT TGM_ID,TGM_DESCR FROM SMART.TARGET_SETUP_M WHERE TGM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                                 & " ORDER BY TGM_ORDER"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlTarget.DataSource = ds
            ddlTarget.DataTextField = "TGM_DESCR"
            ddlTarget.DataValueField = "TGM_ID"
            ddlTarget.DataBind()
        End If


    End Sub

    Sub GetAllGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                 & " WHERE GRM_ACd_ID=" + ddlAca_Year.SelectedValue.ToString + " AND GRD_ID NOT IN ('KG1','KG2') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

        If ViewState("MainMnu_code") = "C300107" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlGrade.Items.Insert(0, li)
        End If

        GetSectionForGrade()
    End Sub
    Sub GetSectionForGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ' If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
        ddlSection.Items.Add(New ListItem("ALL", "0"))
        ddlSection.Items.FindByText("ALL").Selected = True
        'End If
    End Sub

#End Region

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindTargetSetup()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If h_STU_IDs.Value <> "" Then
            grdStudent.DataBind()
            h_STU_IDs.Value = ""
        End If
        grdStudent.DataBind()
        GetSectionForGrade()
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        If h_STU_IDs.Value <> "" Then
            grdStudent.DataBind()
            h_STU_IDs.Value = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    'Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
    '    If h_STU_IDs.Value <> "" Then
    '        grdStudent.Visible = True
    '        GridBindStudents(h_STU_IDs.Value)
    '    Else
    '        grdStudent.Visible = False
    '    End If

    'End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
