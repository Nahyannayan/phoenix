﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_RA_AssessmentComparisonBySkill.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_AssessmentComparisonBySkill"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="Label1" runat="server" Text="Assessment Comparison By Skill"></asp:Label>
            <asp:Button ID="btnCheck" runat="server" Style="display: none" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>

                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Assessment</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSubActivity" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left" valign="middle"><span class="field-label">Subject</span>
                        </td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />

                            <asp:Button ID="btnDownload" runat="server" Text="Download Report in PDF" CssClass="button"
                                TabIndex="4" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>
