﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_CBSEOverallPerformance.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_CBSEOverallPerformance" Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>OverAll Performers
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server"   width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label" >Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label" >Report Card</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label" >Report Schedule</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle"><span class="field-label" >Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" Width="172px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label" >Section</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>

                    <tr id="trSubject" runat="server">
                        <td align="left" valign="middle"><span class="field-label" >Subject</span></td>

                        <td align="left" valign="middle">
                            <%--  &nbsp;
                <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                <br />
                <asp:CheckBoxList id="ddlSubject" runat="server" BorderStyle="Solid" BorderWidth="1px"
                Height="206px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                text-align: left" Width="193px">
                </asp:CheckBoxList>--%>
                            <telerik:RadComboBox RenderMode="Lightweight" ID="ddlSubject" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                Width="100%">
                            </telerik:RadComboBox>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle"><span class="field-label" >Criteria</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlCriteria" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="TOP">HIGH ACHIEVERS</asp:ListItem>
                                <asp:ListItem Value="BOTTOM">LOW ACHIEVERS</asp:ListItem>
                                <asp:ListItem Value="ABOVE">AVERAGE ABOVE</asp:ListItem>
                                <asp:ListItem Value="BELOW">AVERAGE BELOW</asp:ListItem>
                                <asp:ListItem Value="BETWEEN">AVERAGE BETWEEN</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>

                    <tr id="trMark" runat="server">
                        <td align="center" class="matters" valign="middle" colspan="2">
                            <asp:TextBox ID="txtMark1" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblAnd" runat="server" Text="And"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtMark2" runat="server" Visible="False"></asp:TextBox>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label" >No of Records</span></td>

                        <td align="left" valign="middle">
                            <asp:TextBox ID="txtRecord" runat="server">10</asp:TextBox>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                         
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                         
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

