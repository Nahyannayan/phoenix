<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGroupStudentSubjectGradeList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptGroupStudentSubjectGradeList" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);

}
}


function OnTreeClick1(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);
}
}


function CheckUncheckChildren(childContainer, check)
{
var childChkBoxes = childContainer.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = check;
}
}


function CheckUncheckParents(srcChild, check)
{
var parentDiv = GetParentByTagName("div", srcChild);
var parentNodeTable = parentDiv.previousSibling;
if(parentNodeTable)
{
var checkUncheckSwitch;
if(check) //checkbox checked
{
var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
if(isAllSiblingsChecked)
checkUncheckSwitch = true;
else
return; //do not need to check parent if any(one or more) child not checked
}
else //checkbox unchecked
{
checkUncheckSwitch = false;
}
var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
if(inpElemsInParentTable.length > 0)
{
var parentNodeChkBox = inpElemsInParentTable[0];
parentNodeChkBox.checked = checkUncheckSwitch;
//do the same recursively
CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
}
}
}

function AreAllSiblingsChecked(chkBox)
{
var parentDiv = GetParentByTagName("div", chkBox);
var childCount = parentDiv.childNodes.length;
for(var i=0;i<childCount;i++)
{
if(parentDiv.childNodes[i].nodeType == 1)
{
//check if the child node is an element node
if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
{
var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
//if any of sibling nodes are not checked, return false
if(!prevChkBox.checked)
{
return false;
}
}
}
}
return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
{
var parent = childElementObj.parentNode;
while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
{
parent = parent.parentNode;
}
return parent;
}

function UnCheckAll() 
{ 
var oTree=document.getElementById("<%=tvReport.ClientId %>");
childChkBoxes = oTree.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = false;
}

return true;
} 
</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Grade List -By Group
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table id="tbl_ShowScreen" runat="server" align="center" width="100%" >
                 
              
             
           <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblClm" runat="server" align="center" width="100%">
                     
                      <tr>
                        <td align="left">
                            <span class="field-label">Select Academic Year</span></td>
                        
                        <td align="left" >
                             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>               
                      </td>
                           <td align="left">
                            <span class="field-label">Grade</span></td>
                        
                        <td align="left" >
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Width="183px">
                        </asp:DropDownList></td>
                       </tr>   
                   
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Subject</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" Width="183px">
                            </asp:DropDownList></td>
                         <td align="left">
                            <span class="field-label">Select Group</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlGroup" runat="server" Width="187px">
                        </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Report Card</span></td>
                        
                        <td align="left">
                            <div class="checkbox-list">
                            <asp:TreeView id="tvReport" runat="server"
                                ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                PopulateNodesFromClient="False"
                                ShowCheckBoxes="All" >
                                <parentnodestyle font-bold="False" />
                                <hovernodestyle font-underline="True" />
                                <selectednodestyle />
                                <nodestyle  />
                            </asp:TreeView>
                                </div>
                                </td>
                    </tr>
                    
                      <tr>
                    <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            <asp:Button id="btnGenComments" runat="server" CssClass="button"
                                Text="Generate Comments" ValidationGroup="groupM1" />
                            <asp:Button id="btnDownloadPDF" runat="server" CssClass="button"
                                Text="Download Report in PDF" ValidationGroup="groupM1" />
                            <asp:Button id="btnDownLoadExcel" runat="server" CssClass="button"
                                Text="Download Report in Excel" ValidationGroup="groupM1" /></td>
                    </tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />&nbsp;<cr:crystalreportsource id="rs" runat="server" cacheduration="1"> </cr:crystalreportsource></td></tr>
           
    
</table>
     

                </div>
            </div>
         </div>

</asp:Content>

