﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rpt_RA_KG_ProgressTracker.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_KG_ProgressTracker" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<link href="../../../cssfiles/title.css" rel="stylesheet" />--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <script src="../../../FusionCharts/FusionCharts.js"></script>
    <script src="../../DashBoards/FusionCharts/jquery.min.js"></script>
</head>
<body>

    <form id="form1" runat="server">
        <script type="text/javascript">
            function GridCreated(sender, args) {
                var scrollArea = sender.GridDataDiv;
                var parent = $get("gridContainer");
                var gridHeader = sender.GridHeaderDiv;
                scrollArea.style.height = parent.clientHeight -
                  gridHeader.clientHeight + "px";
            }
</script>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>

        <div>
            <table border="0">
                <tr>
                    <td class="title-bg">
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td>

                        <telerik:RadGrid ID="rgProgress" runat="server" AutoGenerateColumns="False" Skin="Office2007" CellSpacing="0" GridLines="None" Width="800PX">
                            <ClientSettings>
                               <Scrolling AllowScroll="true" ScrollHeight="300px" UseStaticHeaders="true" />
                                <ClientEvents OnGridCreated="GridCreated" />
                            </ClientSettings>
                            <MasterTableView>
                                <GroupByExpressions>
                                    <telerik:GridGroupByExpression>
                                        <SelectFields>
                                            <telerik:GridGroupByField FieldAlias="Subject" FieldName="Subject"></telerik:GridGroupByField>
                                        </SelectFields>
                                        <GroupByFields>
                                            <telerik:GridGroupByField FieldName="SubjectAlias" SortOrder="Ascending"></telerik:GridGroupByField>
                                        </GroupByFields>

                                    </telerik:GridGroupByExpression>
                                </GroupByExpressions>
                                <Columns>

                                    <telerik:GridBoundColumn DataField="SKILLS" HeaderText="SKILLS" UniqueName="SKILLS" >
                                    </telerik:GridBoundColumn>
                                     <telerik:GridNumericColumn DataField="EEA" HeaderText="Always Exceeds Expectation" UniqueName="EEA"  >
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="EE" HeaderText="EXCEEDS EXPECTATIONS" UniqueName="EE" >
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="MEA" HeaderText="MEETS EXPECTATIONS ALWAYS" UniqueName="MEA" >
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="MEM" HeaderText="MEETS EXPECTATIONS MOSTLY" UniqueName="MEM" >
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="WT" HeaderText="WORKING TOWARDS EXPECTATIONS" UniqueName="WT" >
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="TOTAL" HeaderText="TOTAL" UniqueName="TOTAL" >
                                    </telerik:GridNumericColumn>
                                </Columns>

                            </MasterTableView>
                            <ClientSettings ReorderColumnsOnClient="True" AllowDragToGroup="True" AllowColumnsReorder="True">
                                <Selecting AllowRowSelect="True"></Selecting>
                                <Resizing AllowRowResize="True" AllowColumnResize="True" EnableRealTimeResize="True"
                                    ResizeGridOnColumnResize="true"  ></Resizing>
                                
                            </ClientSettings>
                            <GroupingSettings ShowUnGroupButton="true"></GroupingSettings>
                        </telerik:RadGrid>

                    </td>
                    <td>
                        <div >
                            <span class="field-label"><asp:Literal ID="ltOverall" runat="server"></asp:Literal></span>
                        </div>
                    </td>
                </tr>

            </table>
            <table width="100%" border="0">
                <tr>

                    <td>

                        <span class="field-label"><asp:Literal ID="ltOther" runat="server"></asp:Literal></span>
                    </td>
                </tr>

            </table>
            <asp:HiddenField ID="hfSbgIds" runat="server" />
            <asp:HiddenField ID="hfRPF_ID" runat="server" />
            <asp:HiddenField ID="hfRSM_ID" runat="server" />
            <asp:HiddenField ID="hfACD_ID" runat="server" />
            <asp:HiddenField ID="hfGRD_ID" runat="server" />
            <asp:HiddenField ID="hfSCT_ID" runat="server" />
        </div>
    </form>
   
</body>
</html>
