<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptChangeStreamOptReq.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptChangeStreamOptReq" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Change Stream and Option Request
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" />

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  valign="top">
                            <table align="center"  class="BlueTableView"  cellpadding="5" cellspacing="0" width="100%"
                                >                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="center" colspan="2" >
                                        <asp:RadioButton ID="rdStream" runat="server" GroupName="g1" Text="Change Stream" Checked="True" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rdoption" runat="server" Text="Change Option"  GroupName="g1" CssClass="field-label"></asp:RadioButton></td>

                                </tr>

                             
                                <tr>
                                    <td colspan="4">

                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Status</span> </td>
                                                <td align="left" >
                                                    <asp:RadioButton ID="rdAccept" runat="server" GroupName="g2" Text="Approved" CssClass="field-label"></asp:RadioButton>
                                                <br />
                                                    <asp:RadioButton ID="rdReject" runat="server" GroupName="g2" Text="Rejected" CssClass="field-label"></asp:RadioButton>
                                                <br />
                                                    <asp:RadioButton ID="rdPending" runat="server" GroupName="g2" Text="Pending" CssClass="field-label"></asp:RadioButton>
                                                <br />
                                                    <asp:RadioButton ID="rdAll" runat="server" GroupName="g2" Text="All"  Checked="True" CssClass="field-label"></asp:RadioButton></td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                 Text="Generate Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" >
                            <asp:HiddenField ID="hfSBM_ID" runat="server" />
                        </td>

                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

