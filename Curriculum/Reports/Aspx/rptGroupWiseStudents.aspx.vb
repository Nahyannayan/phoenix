Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptGroupWiseStudents
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C970025") And (ViewState("MainMnu_code") <> "T000160")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                PopulateGradeStream()
                updateSubjectURL()
                tblGroup.Visible = False
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
              End If


    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

   
 
    Sub PopulateGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE " _
                        & " SGR_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                        & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND SGR_SHF_ID=" + ddlShift.SelectedValue.ToString _
                        & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString _
                        & " AND SGR_SBG_ID=" + hfSBG_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGroup.DataSource = ds
        lstGroup.DataTextField = "SGR_DESCR"
        lstGroup.DataValueField = "SGR_ID"
        lstGroup.DataBind()
        If lstGroup.Items.Count = 0 Then
            tblGroup.Visible = False
        Else
            tblGroup.Visible = True
        End If
    End Sub

    Public Sub PopulateGradeStream()

        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "' and grm_shf_id=" + ddlShift.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If


    End Sub

    Sub updateSubjectURL()
        hfSubjectURL.Value = "../../../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + ddlGrade.SelectedValue + "&stmid=" + ddlStream.SelectedValue.ToString
    End Sub

    Sub CallReport()

        Dim grpXML As String = ""
        Dim grpSelected As Boolean
               Dim i As Integer

        'IF no group is seelecte dthen the report should didplay all groups in that grade
        Dim selectedGroups As Integer

        '  If chkAll.Checked = False Then
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                grpXML += "<ID><SGR_ID>" + lstGroup.Items(i).Value + "</SGR_ID></ID>"
                selectedGroups += 1
            End If
        Next
        '  End If

        'if all groups are selected then query requires only subject(no need to pass xml)
        If txtSubject.Text <> "" Then
            grpXML = "<IDS>" + grpXML + "</IDS>"
            grpSelected = True
        Else
            grpXML = "<IDS><ID><SGR_ID>0</SGR_ID></ID></IDS>"
            grpSelected = False
        End If


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@SHF_ID", ddlShift.SelectedValue.ToString)
        param.Add("@STM_ID", ddlStream.SelectedValue.ToString)
        param.Add("@SGR_XML", grpXML)
        param.Add("@SBG_ID", hfSBG_ID.Value)
        param.Add("@bGRPSELECT", grpSelected.ToString)
        param.Add("AccYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("Grade", ddlGrade.SelectedItem.Text)
        param.Add("Shift", ddlShift.SelectedItem.Text)
        param.Add("Stream", ddlStream.SelectedItem.Text)
        param.Add("Subject", txtSubject.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptGroupWiseStudents.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
        PopulateGradeStream()
        updateSubjectURL()
        txtSubject.Text = ""
        hfSBG_ID.Value = ""
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        updateSubjectURL()
        txtSubject.Text = ""
        hfSBG_ID.Value = ""
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
        PopulateGradeStream()
        updateSubjectURL()
        txtSubject.Text = ""
        hfSBG_ID.Value = ""
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        PopulateGradeStream()
        updateSubjectURL()
        txtSubject.Text = ""
        hfSBG_ID.Value = ""
    End Sub


    Protected Sub imgSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSub.Click
        If hfSBG_ID.Value <> "" Then
            PopulateGroup()
            tblGroup.Visible = True
        End If
    End Sub

  
    Protected Sub btnGenerateReport_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        If chkAll.Checked = True Then
            For i As Integer = 0 To lstGroup.Items.Count - 1
                lstGroup.Items(i).Selected = True
            Next
        Else
            lstGroup.ClearSelection()

        End If
    End Sub

    Protected Sub txtSubject_TextChanged(sender As Object, e As EventArgs)
        If hfSBG_ID.Value <> "" Then
            PopulateGroup()
            tblGroup.Visible = True
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
