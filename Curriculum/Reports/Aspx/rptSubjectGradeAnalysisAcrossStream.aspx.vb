﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptSubjectGradeAnalysisAcrossStream
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280121") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    BindReportType()
                    BindReportPrintedFor()
                    BindHeader()
                    GetAllGrade()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub
    Sub BindReportPrintedFor()
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub
    Sub BindHeader()
        Dim str_Sql As String = "SELECT RSD_ID, RSD_HEADER FROM RPT.REPORT_SETUP_D " & _
        " WHERE (ISNULL(RSD_bALLSUBJECTS, 0) = 1 AND ISNULL(RSD_bDIRECTENTRY, 0) = 0 " & _
        " OR ISNULL(RSD_bPERFORMANCE_INDICATOR, 0) = 1 )" & _
        " AND RSD_RSM_ID = " & ddlReportType.SelectedValue
        ddlHeader.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, _
         CommandType.Text, str_Sql)
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub
    Sub GetAllGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT distinct GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID " _
                               & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSG_GRD_ID=GRD_ID " _
                               & " WHERE GRM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                               & " AND RSG_RSM_ID=" + ddlReportType.SelectedValue.ToString _
                               & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        ddlStream.Visible = False
        GetStream()
        BindSubjects(ddlGrade.SelectedValue, ddlStream.SelectedValue)
    End Sub

    Sub GetStream()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_sql As String = "SELECT DISTINCT STM_ID, STM_DESCR FROM VW_GRADE_BSU_M INNER JOIN" & _
        " VW_STREAM_M ON VW_GRADE_BSU_M.GRM_STM_ID = VW_STREAM_M.STM_ID " & _
        " WHERE GRM_GRD_ID = '" & ddlGrade.SelectedValue & "'  AND GRM_ACD_ID = " & _
        ddlAca_Year.SelectedValue & " AND GRM_BSU_ID = '" & Session("sBSUID") & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlStream.Items.Insert(0, li)
    End Sub

    Public Sub BindSubjects(ByVal GRD_ID As String, ByVal STM_ID As String)
        Dim str_sql As String
        Dim ds As DataSet

        If ddlStream.SelectedValue.ToString = "0" Then
            str_sql = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S AS A WHERE SBG_PARENT_ID = 0 AND SBG_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                    & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                    & " AND (SELECT COUNT(SBG_ID) FROM SUBJECTS_GRADE_S AS B WHERE B.SBG_DESCR=A.SBG_DESCR)>1 "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
            chkListSubjects.DataSource = ds
            chkListSubjects.DataTextField = "SBG_DESCR"
            chkListSubjects.DataValueField = "SBG_DESCR"
            chkListSubjects.DataBind()
        Else
            str_sql = "SELECT SBG_ID, SBG_DESCR FROM SUBJECTS_GRADE_S " & _
            " WHERE SBG_PARENT_ID = 0  AND SBG_bMAJOR = 1 AND SBG_BSU_ID = '" & Session("sBSUID") & "' " & _
            " AND SBG_ACD_ID = " & ddlAca_Year.SelectedValue & " AND SBG_GRD_ID ='" & GRD_ID & "' " & _
            " and SBG_STM_ID =" & STM_ID
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnection, CommandType.Text, str_sql)
            chkListSubjects.DataSource = ds
            chkListSubjects.DataTextField = "SBG_DESCR"
            chkListSubjects.DataValueField = "SBG_ID"
            chkListSubjects.DataBind()
        End If

    End Sub
    Private Function GetSelectedSubjects() As String
        Dim SBG_IDs As String = String.Empty
        For Each lst As ListItem In chkListSubjects.Items
            If lst.Selected Then
                If SBG_IDs <> "" Then
                    SBG_IDs += "|"
                End If
                SBG_IDs += lst.Value
            End If
        Next
        Return SBG_IDs
    End Function
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)

        param.Add("@GRD_ID", ddlGrade.SelectedValue)

        param.Add("@RSD_ID_P", ddlHeader.SelectedValue.ToString)
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlStream.Visible = True And ddlStream.SelectedValue <> "0" Then
            param.Add("Grade", ddlGrade.SelectedItem.Text + " " + ddlStream.SelectedItem.Text)
        Else
            param.Add("Grade", ddlGrade.SelectedItem.Text)
        End If

        param.Add("rptSchedule", ddlReportPrintedFor.SelectedItem.Text)
        '  param.Add("UserName", Session("sUsr_name"))

        If ddlStream.SelectedValue.ToString = "0" Then
            param.Add("@SBG_DESCR", GetSelectedSubjects())
            If chkGradeType.Checked = True Then
                param.Add("@GRADETYPE", "")
            Else
                param.Add("@GRADETYPE", "GRADES")
            End If

        Else
            param.Add("@SBG_ID", GetSelectedSubjects())
            param.Add("@STM_ID", ddlStream.SelectedValue)
        End If
        param.Add("@TYPE", ddlType.SelectedValue)
      

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ddlStream.SelectedValue = "0" Or chkGradeType.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysisByGender_Stream.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptSubjectGradeAnalysisByGender.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindReportType()
        BindReportPrintedFor()
        GetAllGrade()
        If ddlGrade.SelectedValue = "11" Or ddlGrade.SelectedValue = "12" Then
            ddlStream.Visible = True
        Else
            ddlStream.Visible = False
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetStream()
        If ddlGrade.SelectedValue = "11" Or ddlGrade.SelectedValue = "12" Then
            ddlStream.Visible = True
        Else
            ddlStream.Visible = False
        End If
        BindSubjects(ddlGrade.SelectedValue.ToString, ddlStream.SelectedValue.ToString)
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindHeader()
        GetAllGrade()
        BindReportPrintedFor()
        If ddlGrade.SelectedValue = "11" Or ddlGrade.SelectedValue = "12" Then
            ddlStream.Visible = True
        Else
            ddlStream.Visible = False
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    
End Class
