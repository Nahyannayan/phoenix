<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCommendationList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCommendationList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Commendation List Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                         <td align="left" width="20%" ><span class="field-label">Report Card</span></td>
                      
                        <td align="left" >
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr>
                        <td align="left" width="20%" valign="middle"  ><span class="field-label">Report Schedule</span></td>
                       
                        <td align="left" valign="middle"  >
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" >
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle"  ><span class="field-label">Grade</span></td>
                        
                        <td align="left" valign="middle" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr>
                        <td align="left"  width="20%" valign="middle"><span class="field-label">Grading</span></td>
                       
                        <td align="left"   valign="middle">
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstGrading" runat="server" >
                            </asp:CheckBoxList></div></td>
                         <td align="left" width="20%" valign="middle" ><span class="field-label">Subject</span></td>
                        
                        <td align="left" valign="middle"  >
                            <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="True" Text="Select All"></asp:CheckBox>
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="ddlSubject" runat="server" >
                            </asp:CheckBoxList></div></td>
                    </tr>
                  
                    <tr runat="server">
                        <td align="left" width="20%"  valign="middle"><span class="field-label">Section</span></td>
                       
                        <td align="left"   valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

