﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTriangulation_External.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTriangulation_External" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>
                .table td select, table td input[type=text], table td input[type=date], table td textarea {
            width:auto;
                min-width: 100% !important;
        }

        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Triangulation Report -External"></asp:Label>
        </div>
         <div class="card-body">
            <div class="table-responsive">

                  <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcdID" runat="server" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        
                    </tr>
                    <tr>
                         <td align="left"><span class="field-label">Stream</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Grade</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                      
                    </tr>
                    <tr>
                          <td align="left" colspan="1"><span class="field-label">Subject </span></td>

                        <td align="left" colspan="1">
                            <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbSubject" Width="100%" AutoPostBack="true"
                                MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains" OnSelectedIndexChanged="cmbSubject_SelectedIndexChanged"
                                HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                OnDataBound="cmbSubject_DataBound" OnItemDataBound="cmbSubject_ItemDataBound"
                                EmptyMessage="Start typing to search...">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col2">Subject Name</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>

                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "SBG_DESCR")%></li>
                                    </ul>
                                </ItemTemplate>
                                <FooterTemplate>
                                    A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                    items
                                </FooterTemplate>
                            </telerik:RadComboBox>
                            </td>
                        <td align="left"><span class="field-label">Subject   Group</span></td>

                        <td align="left">
                            <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbSubjectGroup" Width="100%"
                                MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                OnDataBound="cmbSubjectGroup_DataBound" OnItemDataBound="cmbSubjectGroup_ItemDataBound"
                                EmptyMessage="Start typing to search...">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col2">Description</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>

                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "DESCR2")%></li>
                                    </ul>
                                </ItemTemplate>
                                <FooterTemplate>
                                    A total of
                                 <asp:Literal runat="server" ID="Literal1" />
                                    items
                                </FooterTemplate>
                            </telerik:RadComboBox>

                        </td>
                     
                    </tr>
                     <tr>
            <td align="left" colspan="5" style="text-align: center">
                <asp:Button ID="btnGenerateExcel" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="30" Text="Generate Excel" />
                
            </td>

        </tr>
                </table>
                </div>
             </div>
        </div>
     <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>
</asp:Content>

