﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptConsolidated_MarkBook.aspx.vb" Inherits="Curriculum_ConsolidatedReports_rptConsolidated_MarkBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblError" CssClass="error" Text ="" runat="server"></asp:Label>
                <table id="tblReport" runat="server" width="100%">


                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <span class="field-label">Report Card</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportCard_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Report Schedule</span></td>

                        <td align="left" width="30%">
                            <div id="divreportschdl" runat="server" class="checkbox-list">
                                <asp:CheckBoxList ID="ddlPrintedFor" runat="server">
                                </asp:CheckBoxList>
                            </div>
                          </td>
                        <td align="left" width="20%">
                            <span class="field-label">Grade</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="trSubject" runat="server">
                        <td align="left" width="20%">
                            <span class="field-label">Subject</span></td>

                        <td align="left" width="30%">
                            <%--<asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged"></asp:DropDownList>--%>
                             <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" AutoPostBack="True" />
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="lstSubject" runat="server" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="chkListSubjects_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td>
                            <span class="field-label">Group</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">
                                Externam Exam Type
                            </span>
                        </td>
                        <td>
                            <div class="checkbox-list">
                                <asp:checkboxlist ID="ddlExamtpe" runat="server" >
                                </asp:checkboxlist>
                            </div>
                            
                        </td>
                    </tr>
                 
                    <tr>
                        <td colspan="4" align="center">
                            
                            <asp:Button ID="btnGenReport" runat="server" OnClick="btnGenReport_Click" CssClass="button" Text="Generate Report" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SBG_ID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>
