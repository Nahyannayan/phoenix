Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptSubjectGradeWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C970010") And (ViewState("MainMnu_code") <> "T000160")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))




                BindGradeTree()


            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindGradeTree()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT STM_ID,STM_DESCR,GRM_DISPLAY, " _
                  & " GRM_GRD_ID AS GRD_ID,GRD_DISPLAYORDER FROM  STREAM_M " _
                  & "  AS A INNER JOIN GRADE_BSU_M AS B ON B.GRM_STM_ID=A.STM_ID" _
                  & " INNER JOIN GRADE_M AS C ON B.GRM_GRD_ID=C.GRD_ID WHERE" _
                  & " GRM_ACD_ID=80 ORDER BY STM_DESCR,GRD_DISPLAYORDER FOR XML AUTO"

        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dcParent(1) As DataColumn
        Dim reader As SqlDataReader

        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        'Dim str As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Dim xl As New SqlString


        str = str.Replace("STM_ID", "ID")
        str = str.Replace("STM_DESCR", "TEXT")
        str = str.Replace("GRD_ID", "ID")
        str = str.Replace("GRM_DISPLAY", "TEXT")

        Dim st As String
        For i As Integer = 1 To 14
            st = "<C GRD_DISPLAYORDER=""" + i.ToString + """/>"
            str = str.Replace(st, "")
        Next




        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))



        tvGrade.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvGrade.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvGrade.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
    End Sub
    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                tNode = inTreeNode.ChildNodes(i)
                If xNode.HasChildNodes Then
                    AddNode(xNode, tNode)
                End If
            Next
        Else
            inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
        End If
    End Sub




    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pickupPoints As String = ""
        Dim busNos As String = ""
        Dim trips As String = ""
        Dim sectionIds As String = ""
        Dim i As Integer = 0


        '''''''Get the ppickupponts

        Dim treeNode As TreeNode
        Dim mainNode As TreeNode
        Dim childNode As TreeNode


        Dim grdXML As String

        mainNode = tvGrade.Nodes(0)
        For Each treeNode In mainNode.ChildNodes
            For Each childNode In treeNode.ChildNodes
                If childNode.Checked = True Then
                    grdXML += "<ID><GRM_DISPLAY>" + childNode.Text + "</GRM_DISPLAY><GRD_ID>" + childNode.Value + "</GRD_ID><STM_ID>" + treeNode.Value + "</STM_ID></ID>"
                End If
            Next
        Next


        grdXML = "<IDS>" + grdXML + "</IDS>"

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_XML", grdXML)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSubjectGradeWise.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CallReport()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGradeTree()
    End Sub
End Class
