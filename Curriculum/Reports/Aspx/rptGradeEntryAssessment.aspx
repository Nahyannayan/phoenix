<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGradeEntryAssessment.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptGradeEntryAssessment" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Grade Entry Assessment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblClm" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>

                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Report card Setup</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%" ><span class="field-label">Report card schedule</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlReportPrintedFor" runat="server" >
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>
                                   
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList></td>


                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblGrp" runat="server" CssClass="field-label" Text="Groups"></asp:Label>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGroup" runat="server" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Status</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlMarks" runat="server" >
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Completed</asp:ListItem>
                                            <asp:ListItem>Partial</asp:ListItem>
                                            <asp:ListItem>Not Completed</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                            Text="DownLoad Report in PDF" ValidationGroup="GR" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>


            </div>

        </div>

    </div>
</asp:Content>

