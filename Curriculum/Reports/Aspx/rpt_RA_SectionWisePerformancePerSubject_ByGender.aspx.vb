﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rpt_RA_SectionWisePerformancePerSubject_ByGender
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C500380") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, Session("Sbsuid"), ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindSubjects()
                    BindGrade()
                    BindReport()
                    BindHeader()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSubjects()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_GRD_ID NOT IN('KG1','KG2') " _
                                & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_DESCR"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID FROM VW_GRADE_BSU_M  " _
                                  & " INNER JOIN SUBJECTS_GRADE_S ON GRM_GRD_ID=SBG_GRD_ID AND GRM_ACD_ID=SBG_ACD_ID WHERE" _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND SBG_DESCR='" + ddlSubject.SelectedItem.Text + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindReport()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        str_query = "SELECT DISTINCT RPF_DESCR AS RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID" _
                 & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " AND RPF_DESCR NOT LIKE '%REVIEW REPORT%' " _
                 & " AND RSG_GRD_ID NOT IN ('KG1','KG2')" _
                 & " ORDER BY RPF_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_DESCR"
        ddlPrintedFor.DataBind()

    End Sub

    Function GetGrades() As String
        Dim str As String = ""
        Dim i As Integer
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrade.Items(i).Value
            End If
        Next
        Return str
    End Function

    Sub BindHeader()
        ddlHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_HEADER FROM RPT.REPORT_SETUP_D AS A " _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSD_RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS C ON A.RSD_RSM_ID=C.RSM_ID" _
                             & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND RPF_DESCR='" + ddlPrintedFor.SelectedItem.Text + "' AND ISNULL(RSD_BDIRECTENTRY,1)=0 AND ISNULL(RSD_bALLSUBJECTS,0)=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_HEADER"
        ddlHeader.DataBind()

    End Sub

    Sub CallReport()

        Dim criteria As String = Val(txtWellAbove1.Text).ToString + "|" + Val(txtWellAbove2.Text).ToString + "|" + Val(txtAbove1.Text).ToString + "|" _
                                & Val(txtAbove2.Text).ToString + "|" _
                                & Val(txtMeet1.Text).ToString + "|" _
                                & Val(txtMeet2.Text).ToString + "|" _
                                & Val(txtBelow1.Text).ToString + "|" _
                                & Val(txtBelow2.Text).ToString + "|" _
                                 & Val(txtWBelow1.Text).ToString + "|" _
                                & Val(txtWBelow2.Text).ToString
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBG_DESCR", ddlSubject.SelectedItem.Text)
        param.Add("@RPF_DESCR", ddlPrintedFor.SelectedItem.Text)
        param.Add("@GRD_IDS", GetGrades())
        param.Add("@CRITERIA", criteria)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("schedule", ddlAcademicYear.SelectedItem.Text)
        param.Add("@RSD_HEADER", ddlHeader.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_SectionWisePerformancePerSubjectByGender.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSubjects()
        BindGrade()
        BindReport()
        BindHeader()
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub


    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGrade()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

  
    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        BindHeader()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
