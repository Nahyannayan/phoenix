﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptIntlNCChart
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400120") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                BindReportSchedle()
                BindSubjects()
                If rdStudent.Checked = True Then
                    trGrade.Visible = True
                    trSection.Visible = True
                Else
                    trGrade.Visible = False
                    trSection.Visible = False
                End If
                BindGrade()
                BindSection()
                '    Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportSchedle()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RSM_DISPLAYORDER,RPF_DISPLAYORDER " _
                           & " FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                           & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID" _
                           & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                           & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"



                           
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim li As ListItem

        For i = 0 To ds.Tables(0).Rows.Count - 1
            li = New ListItem
            li.Text = ds.Tables(0).Rows(i).Item(0)
            li.Text = ds.Tables(0).Rows(i).Item(0)
            If ddlReportSchedule.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)) Is Nothing Then
                ddlReportSchedule.Items.Add(li)
            End If
        Next

    End Sub


    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON SBG_GRD_ID=RSG_GRD_ID " _
                                 & " INNER JOIN RPT.REPORT_SETUP_M ON RSG_RSM_ID=RSM_ID AND RSM_ACD_ID=SBG_ACD_ID" _
                                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSM_ID=RPF_RSM_ID" _
                                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND SBG_GRD_ID IN('01','02','03','04','05','06','07','08','09')" _
                                 & " AND SBG_bMRK_DISPLAY=0" _
                                 & " AND RPF_DESCR='" + ddlReportSchedule.SelectedItem.Text + "'" _
                                 & " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_DESCR"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("sbsuid") <> "125018" And Session("sbsuid") <> "125002" Then
            str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_ID,GRD_DISPLAYORDER FROM " _
                                 & " VW_GRADE_BSU_M AS A INNER JOIN VW_GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID" _
                                 & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.GRM_GRD_ID=C.SBG_GRD_ID AND A.GRM_ACD_ID=C.SBG_ACD_ID" _
                                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON SBG_GRD_ID=RSG_GRD_ID " _
                                 & " INNER JOIN RPT.REPORT_SETUP_M ON RSG_RSM_ID=RSM_ID AND RSM_ACD_ID=SBG_ACD_ID" _
                                 & " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSM_ID=RPF_RSM_ID" _
                                 & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_DESCR='" + ddlSubject.SelectedValue.ToString + "'" _
                                 & " AND SBG_GRD_ID IN('01','02','03','04','05','06','07','08')" _
                                  & " AND RPF_DESCR='" + ddlReportSchedule.SelectedItem.Text + "'" _
                                 & " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_ID,GRD_DISPLAYORDER FROM " _
                     & " VW_GRADE_BSU_M AS A INNER JOIN VW_GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID" _
                     & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.GRM_GRD_ID=C.SBG_GRD_ID AND A.GRM_ACD_ID=C.SBG_ACD_ID" _
                     & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON SBG_GRD_ID=RSG_GRD_ID " _
                     & " INNER JOIN RPT.REPORT_SETUP_M ON RSG_RSM_ID=RSM_ID AND RSM_ACD_ID=SBG_ACD_ID" _
                     & " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSM_ID=RPF_RSM_ID" _
                     & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_DESCR='" + ddlSubject.SelectedValue.ToString + "'" _
                     & " AND SBG_GRD_ID IN('01','02','03','04','05','06','07','08','09')" _
                      & " AND RPF_DESCR='" + ddlReportSchedule.SelectedItem.Text + "'" _
                     & " ORDER BY GRD_DISPLAYORDER"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_ID"
        ddlGrade.DataBind()
    End Sub


    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM VW_SECTION_M WHERE SCT_GRM_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SCT_DESCR<>'TEMP'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@RPF_DESCR", ddlReportSchedule.SelectedValue.ToString)
        param.Add("@SBG_DESCR", ddlSubject.SelectedValue.ToString)
        param.Add("@RSD_HEADER", "")
        param.Add("@bGENDER", chkGender.Checked.ToString)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@TYPE", ddlType.SelectedValue)
        param.Add("type", ddlType.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            If rdSection.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptIntlNCChartBySection.rpt")
            ElseIf rdKeyStage.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptIntlNCChartByKeyStage.rpt")
            ElseIf rdStudent.Checked = True Then
                param.Add("grm_display", ddlGrade.SelectedItem.Text)
                param.Add("sct_descr", ddlSection.SelectedItem.Text)
                param.Add("@SCT_ID", ddlSection.SelectedValue.ToString)
                .reportPath = Server.MapPath("../Rpt/rptIntlNCChartByStudent.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptIntlNCChart.rpt")
            End If
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportSchedle()
        BindSubjects()
        BindGrade()
        BindSection()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub rdStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdStudent.CheckedChanged
        If rdStudent.Checked = True Then
            trGrade.Visible = True
            trSection.Visible = True
        Else
            trGrade.Visible = False
            trSection.Visible = False
        End If
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlReportSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportSchedule.SelectedIndexChanged
        BindSubjects()
        BindGrade()
        BindSection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
