﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rpt_RA_ComparisonBySubjectAcrossYear
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C290160" And ViewState("MainMnu_code") <> "C500160" And ViewState("MainMnu_code") <> "C500300") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindSubjects()
                    BindPrintedFor()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If

    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindSubjects()
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S AS A" _
                                & " WHERE SBG_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SBG_PARENT_ID=0" _
                                & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID FROM VW_GRADE_BSU_M WHERE " _
                                  & " GRM_GRD_ID NOT IN('KG1','KG2') AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindPrintedFor()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If ViewState("MainMnu_code") = "C500300" Then
            str_query = "SELECT DISTINCT RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                             & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                             & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                             & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND RSG_GRD_ID='" + ddlGrade.SelectedValue + "'"
        Else
            If ddlGrade.SelectedValue <> "11" And ddlGrade.SelectedValue <> "12" And ViewState("MainMnu_code") <> "C500160" Then

                str_query = "SELECT DISTINCT RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                            & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                            & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                            & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                            & " AND (RPF_DESCR LIKE '%FORMATIVE%' OR RPF_DESCR LIKE '%SUMMATIVE%' OR RPF_DESCR LIKE '%FINAL%')" _
                            & " AND RSG_GRD_ID<>'10'"
            Else
                str_query = "SELECT DISTINCT RPF_DESCR FROM RPT.REPORT_SETUP_M AS A" _
                                  & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                                  & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                                  & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND RSG_GRD_ID='" + ddlGrade.SelectedValue + "'"
            End If

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_DESCR"
        ddlPrintedFor.DataBind()
    End Sub

    
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)

        Dim rpf As String = ""
        Dim i As Integer

        For i = 0 To ddlPrintedFor.Items.Count - 1
            If ddlPrintedFor.Items(i).Selected = True Then
                If rpf <> "" Then
                    rpf += "|"
                End If
                rpf += ddlPrintedFor.Items(i).Value
            End If
        Next

        param.Add("@RPF_DESCR", rpf)
        param.Add("Assessment", ddlPrintedFor.SelectedItem.Text)
        param.Add("Grades", ddlGrade.SelectedItem.Text)
        param.Add("Subject", ddlSubject.SelectedItem.Text)
        param.Add("@TYPE", ddlType.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .reportPath = Server.MapPath("../Rpt/rpt_RA_AssessmentComparisonBySubject_AcrossYear.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubjects()
        BindPrintedFor()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        BindPrintedFor()
    End Sub
End Class
