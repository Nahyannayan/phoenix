<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOptionListConsolidated.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptOptionListConsolidated" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Student Option Request Count"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" class="matters" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                Text="Generate Report" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

