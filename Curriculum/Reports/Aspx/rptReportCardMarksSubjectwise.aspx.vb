Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptReportCardMarksSubjectwise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C300008") _
                AndAlso (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_RPT_FORMTUTOR_COMMENTS)) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    BindReportType()
                    BindReportPrintedFor()
                    GetAllGrade()
                    FillSubjects()
                    btnCancel.Text = "Cancel"
                    trStudent.Visible = False
                    Select Case ViewState("MainMnu_code")
                        Case CURR_CONSTANTS.MNU_RPT_FORMTUTOR_COMMENTS
                            trSubject.Visible = False
                            lblHeader.Text = "Form Tutor Comments"
                    End Select

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub BindReportPrintedFor()
        ddlReportPrintedFor.Items.Clear()
        If Not ddlReportType.SelectedIndex = -1 Then
            ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue)
            ddlReportPrintedFor.DataTextField = "RPF_DESCR"
            ddlReportPrintedFor.DataValueField = "RPF_ID"
            ddlReportPrintedFor.DataBind()
        Else
            ddlReportPrintedFor.SelectedIndex = -1
        End If
    End Sub

    Sub BindReportType()
        ddlReportType.Items.Clear()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
        BindReportPrintedFor()
        GetAllGrade()
        FillSubjects()
        GetSectionForGrade()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Sub GetAllGrade()
        ddlGrade.Items.Clear()
        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"))
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        ddlSection.Items.Clear()
        If Not ddlGrade.SelectedIndex = -1 Then
            ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue)
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Select Case ViewState("MainMnu_code")
            '    Case CURR_CONSTANTS.MNU_RPT_MONTHLY_PROGRESS_REPORT
            '        Select Case Session("sBSUID")
            '            Case "125005"
            '                GenerateMonthlyProgressReport()
            '            Case "123006"
            '                GenerateTermlyReport_TMS()
            '            Case "121013"
            '                GenerateTermlyReport_OOEHS_Dubai()
            '        End Select
            'End Select
            Select Case ViewState("MainMnu_code")

                Case CURR_CONSTANTS.MNU_RPT_FORMTUTOR_COMMENTS
                    GenerateFormTutorComments()
                Case Else
                    GenerateReportCardSubjectwise()
            End Select
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub GenerateFormTutorComments()
        Dim param As New Hashtable
        Try
            Dim strStudents As String = ""
            Dim strSubject As String = String.Empty
            Dim strSection As String = String.Empty
            If ddlSection.SelectedItem.Text = "ALL" Then
                For Each lst2 As ListItem In ddlSection.Items
                    If strSection <> "" Then
                        strSection += ","
                    End If
                    strSection += lst2.Value
                Next
                '   strSection = strSection.Substring(0, Len(strSection) - 5)
                h_SCTIDs.Value = strSection
            Else
                h_SCTIDs.Value = ddlSection.SelectedValue.ToString
            End If
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@BSU_ID", Session("SBSUID"))
            param.Add("@ACD_ID", Convert.ToInt32(ddlAca_Year.SelectedValue))
            param.Add("@GRD_ID", ddlGrade.SelectedValue)
            param.Add("@RSM_ID", Convert.ToInt32(ddlReportType.SelectedValue))
            param.Add("@RPF_ID", Convert.ToInt32(ddlReportPrintedFor.SelectedValue))
            param.Add("@SCT_ID", h_SCTIDs.Value)
            param.Add("User_name", Session("sUsr_name"))
            'param.Add("@IMG_BSU_ID", Session("SBSUID"))
            'param.Add("@IMG_TYPE", "LOGO")

            Dim rptClass As New rptClass

            With rptClass
                .crDatabase = "oasis_curriculum"
                .reportParameters = param
                .reportPath = Server.MapPath("../Rpt/rptGeneralReportComments.rpt")
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportPrintedFor()
        GetAllGrade()
        FillSubjects()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillSubjects()
        GetSectionForGrade()
    End Sub
    Private Sub GenerateReportCardSubjectwise()
        Dim param As New Hashtable
        Try
            Dim strStudents As String = ""
            Dim strSubject As String = String.Empty
            Dim strSection As String = String.Empty
            'lblerror.Text = ""
            'If h_STU_IDs.Value = "" Then
            '    lblerror.Text = "Please Select Students"
            '    Exit Sub
            'End If
            'strStudents = GetStudentDetailsXML(h_STU_IDs.Value)
            If ddlSubject.SelectedItem.Text = "ALL" Then
                For Each lst1 As ListItem In ddlSubject.Items
                    strSubject += lst1.Value & ","
                Next
                strSubject = strSubject.Substring(0, Len(strSubject) - 5)
                h_SUBJIDs.Value = strSubject
            End If
            If ddlSection.SelectedItem.Text = "ALL" Then
                For Each lst2 As ListItem In ddlSection.Items
                    strSection += lst2.Value & ","
                Next
                strSection = strSection.Substring(0, Len(strSection) - 5)
                h_SCTIDs.Value = strSection
            Else
                h_SCTIDs.Value = ddlSection.SelectedItem.Text
            End If
            param.Add("@BSU_ID", Session("SBSUID"))
            param.Add("@ACD_ID", Convert.ToInt32(ddlAca_Year.SelectedValue))
            param.Add("@SBG_ID", h_SUBJIDs.Value)
            param.Add("@GRD_ID", ddlGrade.SelectedValue)
            param.Add("@RSM_ID", Convert.ToInt32(ddlReportType.SelectedValue))
            param.Add("@RPF_ID", Convert.ToInt32(ddlReportPrintedFor.SelectedValue))
            param.Add("@SCT_ID", h_SCTIDs.Value)
            'param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", ","))
            'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
            'param.Add("UserName", Session("sUsr_name"))
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            'param.Add("UserName", Session("sUsr_name"))
            param.Add("AcdYear", ddlAca_Year.SelectedItem.Text)
            param.Add("Grade", ddlGrade.SelectedItem.Text)
            param.Add("Subject", ddlSubject.SelectedItem.Text)

            Dim rptClass As New rptClass

            With rptClass
                .crDatabase = "oasis_curriculum"
                .reportParameters = param
                '.Photos = GetPhotoClass()
                ' Select Case ddlReportType.SelectedValue
                'Case "2"
                .reportPath = Server.MapPath("../Rpt/rptReportCardSubjectwise.rpt")
                ' Case "3"
                'Select Case ddlGrade.SelectedValue
                'Case "KG1", "KG2"
                '.reportPath = Server.MapPath("../Rpt/rptFINAL_PROGRESS_REPORT_CIS_A.rpt")
                ' Case Else
                '.reportPath = Server.MapPath("../Rpt/rptFINAL_PROGRESS_REPORT_CIS_B.rpt")
                'End Select
                'End Select
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try


    End Sub
    Sub FillSubjects()
        Dim str_Sql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCritirea As String = ""
        ddlSubject.Items.Clear()
        If Not ddlGrade.SelectedIndex = -1 Then
            strCritirea = " AND SBG_GRD_ID='" & ddlGrade.SelectedItem.Value & "'"
        End If

        'str_Sql = "SELECT DISTINCT SBG_ID,SBG_DESCR,SBG_PARENTS_SHORT FROM " _
        '    & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
        '    & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
        str_Sql = "SELECT  DISTINCT SBG_ID," _
                    & " case SBG_PARENTS_SHORT " _
                 & " when 'NA' " _
              & " then SBG_DESCR else SBG_DESCR + '--' + SBG_PARENTS_SHORT END as Subject " _
            & " FROM  SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
            & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & ddlAca_Year.SelectedItem.Value & "'"


        str_Sql = str_Sql + strCritirea
        'If H_RPT_ID.Value <> "" Then
        '    str_critirea = " WHERE RPF_RSM_ID=" & H_RPT_ID.Value & ""
        'End If
        'str_Sql = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M"
        'str_Sql = str_Sql & str_critirea & " ORDER BY RPF_ID"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlSubject.DataSource = ds

        ddlSubject.DataTextField = "Subject"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        ddlSubject.Items.Add(New ListItem("ALL", "ALL"))
        ddlSubject.Items.FindByText("ALL").Selected = True
        'If Not ddlRepSch.SelectedIndex = -1 Then
        '    H_SCH_ID.Value = ddlRepSch.SelectedItem.Value
        'End If

    End Sub

    Protected Sub ddlReportPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GetAllGrade()
        'FillSubjects()
    End Sub
    Private Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_ID As XmlElement
        Dim XMLSTU_DET As XmlElement
        Dim XMLbExclude As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")

            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            'XMLbExclude = xmlDoc.CreateElement("bExclude")
            'XMLbExclude.InnerText = iDictEnum.Value
            'XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

    Protected Sub ddlGrade_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        FillSubjects()
        GetSectionForGrade()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            Dim studCl As New studClass
            studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
            txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
            BindReportType()
            BindReportPrintedFor()
            GetAllGrade()
            FillSubjects()
            grdStudent.DataSource = Nothing
            grdStudent.DataBind()
            trStudent.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlSubject.SelectedIndex = -1 Then
            If Not ddlSubject.SelectedItem.Text = "ALL" Then
                h_SUBJIDs.Value = ddlSubject.SelectedItem.Value
            End If
        End If
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Not ddlSection.SelectedIndex = -1 Then
        '    If Not ddlSection.SelectedItem.Text = "ALL" Then
        '        h_SCTIDs.Value = ddlSection.SelectedItem.Value
        '    End If
        'End If
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
