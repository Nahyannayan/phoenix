﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptObjTrackObjectiveTrackerSnapshot
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C310050") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindSubjects()
                BindGroup()
                h_SGR_IDs.Value = ddlGroup.SelectedValue.ToString
                BindDates()
            End If


            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            If Session("SBSUID") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') "
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') "
            End If
           


            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            If Session("SBSUID") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                        & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                        & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                        & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                        & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') " _
                        & " ORDER BY GRD_DISPLAYORDER"
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                        & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                                        & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                                        & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                        & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                                        & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') " _
                                        & " ORDER BY GRD_DISPLAYORDER"
            End If
           
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"

        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Sub GetAllStudentsInSection()
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " _
           & " STUDENT_M INNER JOIN STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID" _
           & " WHERE STU_ACD_ID ='" & ddlAcademicYear.SelectedValue.ToString & "' AND STU_GRD_ID = '" & ddlGrade.SelectedValue.ToString _
           & "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " _
           & " AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString + " for xml path('')),1,0,''),0) "
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " _
             & " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " _
             & " INNER JOIN STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID " _
             & " WHERE STP_ACD_ID ='" & ddlAcademicYear.SelectedValue.ToString & "' AND STP_GRD_ID = '" & ddlGrade.SelectedValue.ToString _
             & "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " _
             & " AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString _
             & " for xml path('')),1,0,''),0) "
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        h_STU_IDs.Value = str
    End Sub

    Sub BindDates()
        lstBaseLine.Items.Clear()
        lstComparison.Items.Clear()


        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT TOP 20 SCO_CLOSINGDATE FROM OBJTRACK.GRADE_CLOSING_S" _
                                 & " WHERE SCO_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND SCO_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                 & " ORDER BY SCO_CLOSINGDATE DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim li1, li2 As ListItem
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            li1 = New ListItem
            li1.Text = Format(ds.Tables(0).Rows(i).Item(0), "dd-MMM-yyyy")
            li1.Value = Format(ds.Tables(0).Rows(i).Item(0), "dd-MMM-yyyy")
            lstBaseLine.Items.Add(li1)


            li2 = New ListItem
            li2.Text = Format(ds.Tables(0).Rows(i).Item(0), "dd-MMM-yyyy")
            li2.Value = Format(ds.Tables(0).Rows(i).Item(0), "dd-MMM-yyyy")
            lstComparison.Items.Add(li2)
        Next
    End Sub

    Sub CallReport()
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBM_ID", sbg_id(1))
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@BASELINEDATE", lstBaseLine.SelectedValue)
        param.Add("@COMPARISONDATE", lstComparison.SelectedValue)
        param.Add("Subject", ddlSubject.SelectedItem.Text)
        param.Add("basedate", lstBaseLine.SelectedValue)
        param.Add("comparedate", lstComparison.SelectedValue)
        ' param.Add("UserName", Session("susr_name"))
        '  param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptObjTrackObjectiveTrackerSnapshot.rpt")
        End With
        Session("rptClass") = rptClass

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubjects()
        BindGroup()
        h_SGR_IDs.Value = ddlGroup.SelectedValue.ToString
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        BindGroup()
        h_SGR_IDs.Value = ddlGroup.SelectedValue.ToString
        BindDates()
    End Sub


    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
        h_SGR_IDs.Value = ddlGroup.SelectedValue.ToString

    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        h_SGR_IDs.Value = ddlGroup.SelectedValue.ToString
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        If h_STU_IDs.Value = "" Then
            GetAllStudentsInSection()
        End If
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1

        CallReport()
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
