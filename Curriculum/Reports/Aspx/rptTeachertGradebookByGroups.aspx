﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTeachertGradebookByGroups.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTeachertGradebookByGroups" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
             <style>
      .col1
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 40%;
    line-height: 14px;
    float: left;
}
.col2
 {
    margin: 0;
    padding: 0 5px 0 0;
    width: 60%;
    line-height: 14px;
    float: left;
}
        
        .demo-container label {
    padding-right: 10px;
    width: 100%;
    display: inline-block;
}
 .rcbHeader ul,
.rcbFooter ul,
.rcbItem ul,
.rcbHovered ul,
.rcbDisabled ul {
    margin: 0;
    padding: 0;
    width: 90%;
    display: inline-block;
    list-style-type: none;
}
    .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
        display: inline;
        float: left;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
    }

    .RadComboBox_Default .rcbInner {
        padding: 10px;
        border-color: #dee2da !important;
        border-radius: 6px !important;
        box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        width: 80%;
        background-image: none !important;
        background-color: transparent !important;
    }

    .RadComboBox_Default .rcbInput {
        font-family: 'Nunito', sans-serif !important;
    }

    .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
        border: 0 !important;
        box-shadow: none;
    }

    .RadComboBox_Default .rcbActionButton {
        border: 0px;
        background-image: none !important;
        height: 100% !important;
        color: transparent !important;
        background-color: transparent !important;
    }
</style>   
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Teacher Gradebook - Export Excel"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">

                    <tr>
                        <td valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblClm" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters"  width="20%" ><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" width="20%" ><span class="field-label">Select Grade</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"  >
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters" ><span class="field-label">Select Subject</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server"  >
                                        </asp:DropDownList></td>

                                    <td align="left" class="matters" ><span class="field-label">Select Term</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlTerm" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" ><span class="field-label">Select Group</span></td>
                                    <td align="left" class="matters"  >
                                       <%-- <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" />
                                        <br />
                                        <asp:CheckBoxList ID="lstGroup" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="124px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                            Width="193px">
                                        </asp:CheckBoxList>--%>
                                         <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="lstGroup" 
                                 CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Group(s)">  </telerik:RadComboBox>

                                    </td>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"  
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button"  
                                            Text="Download with Assement Marks" ValidationGroup="groupM1" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>

