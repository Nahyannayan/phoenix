<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentAssessment.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStudentAssessment" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
    
    function openWin() {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID%>').value;
            
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
                                      
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[1];
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }
        

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    
    
function GetSTUDENTS() {
   
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs =document.getElementById('<%=ddlAcademicYear.ClientID%>').value;
            
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures);
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
    </script>
    
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Student Assessment Report <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
        
        <tr>
            <td align="center"  valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center" width="100%">
                    <tr id="trAcd1" runat="server">
                        <td align="left" width="20%">
                           <span class="field-label">Academic Year</span>
                        </td>
                       
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged" Width="108px">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  width="20%">
                            <span class="field-label">Grade</span>
                        </td>
                        
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged" Width="68px">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr id="trAcd2" runat="server">
                        <td align="left">
                            <span class="field-label">Section</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Subject</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged ="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <span class="field-label">Student</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                OnClick="imgStudent_Click"></asp:ImageButton></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="grdStudent" runat="server" CssClass="table table-bordered table-row" AllowPaging="false" AutoGenerateColumns="False"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                   <tr>
            <td align="left" colspan="14" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" />&nbsp;
                   <asp:Button id="btnDownload" runat="server" CssClass="button"
                   Text="DownLoad Report in PDF" ValidationGroup="groupM1" /></td>
        </tr>
                          
                            
                            
                            
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
               
               
                <asp:HiddenField ID="hfSGR_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfACY_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfQSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSBM_ID" runat="server"></asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>
    
    <asp:HiddenField ID="hfbDownload" runat="server" />
    

    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>


    
                </div>
            </div>
        </div>

</asp:Content>

