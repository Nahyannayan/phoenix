﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_RA_SectionWisePerformancePerSubject_ByGender.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_SectionWisePerformancePerSubject_ByGender"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%--<script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
    </script>--%>
    <style type="text/css">
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default {
            width: 100% !important;
        }

            .RadComboBox_Default .rcbInner {
                padding: 10px;
                border-color: #dee2da !important;
                border-radius: 6px !important;
                box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
                width: 80% !important;
                background-image: none !important;
                background-color: transparent !important;
            }

            .RadComboBox_Default .rcbInput {
                font-family: 'Nunito', sans-serif !important;
            }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script>
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" width="100%"
                    cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Subject</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" Width="172px">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="trSubject" runat="server">
                        <td align="left">
                            <span class="field-label">Grade</span>
                        </td>

                        <td align="left">

                            <%-- <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server"
                    Text="Select All" />
                <br />
                <asp:CheckBoxList ID="lstGrade1" runat="server" BorderStyle="Solid" BorderWidth="1px"
                    Height="206px" RepeatLayout="Flow">
                </asp:CheckBoxList>--%>
                            <telerik:RadComboBox RenderMode="Lightweight" ID="lstGrade" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true" EmptyMessage="Select Grade(s)">
                            </telerik:RadComboBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Report Card</span>
                        </td>

                        <td align="left">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Report Header</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlHeader" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Well Above Curriculum Expectations (From) </span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWellAbove1" Text="95" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Well Above Curriculum Expectations (To) </span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWellAbove2" Text="100" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Above Curriculum Expectations (From)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAbove1" Text="90" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Above Curriculum Expectations (To)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAbove2" Text="94" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Meets Curriculum Expectations(From)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMeet1" Text="65" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Meets Curriculum Expectations(To)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMeet2" Text="89" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Below Curriculum Expectations(From)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBelow1" Text="25" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Below Curriculum Expectations(To)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBelow2" Text="65" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Well Below Curriculum Expectations(From)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWBelow1" Text="0" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Well Below Curriculum Expectations(To)</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWBelow2" Text="25" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                            &nbsp;<asp:Button ID="btnDownload" runat="server" CssClass="button" TabIndex="7"
                                Text="Download Report In PDF" ValidationGroup="groupM1" />
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
