﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSubjectGradeAnalysisAcrossStream.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSubjectGradeAnalysisAcrossStream" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script>
    function fnSelectAll(master_box) {
        var curr_elem;
        var checkbox_checked_status;
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            curr_elem = document.forms[0].elements[i];
            if (curr_elem.type == 'checkbox') {
                curr_elem.checked = !master_box.checked;
            }
        }
        master_box.checked = !master_box.checked;
    }
</script>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:Label id="lblHeader" runat="server" Text="Result Analysis"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
  <table  align="center"  style="border-collapse:collapse"  cellpadding="4" cellspacing="0" width="100%" >
                    
                    <tr>
                        <td align="left">
                           <span class="field-label"> Academic Year</span></td>
                       
                        <td align="left">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Report Type</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlReportType" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
        <tr runat ="server" id="trRPFID">
            <td align="left">
                <span class="field-label">Report Printed For</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlReportPrintedFor" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr runat="server" id="trHeader" >
            <td align="left">
                <span class="field-label">Report Header</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlHeader" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat ="server" id="trGrade">
            <td align="left" valign="middle">
                <span class="field-label">Grade</span></td>
            
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id ="trSubjectChkList">
            <td align="left" valign="top">
                <span class="field-label">Subject</span></td>
            
            <td align="left" valign="top">
                <div class="checkbox-list">
            <asp:CheckBox id="chkAll" runat="server" Text="Select All "  onclick="javascript:fnSelectAll(this);" AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                &nbsp;<br />
                <asp:CheckBoxList ID="chkListSubjects" runat="server"  AutoPostBack="True"  RepeatLayout="Flow">
                </asp:CheckBoxList></div>
                                        </td>
        </tr>
         <tr id="trFilter" runat="server">
                        <td align="left"  >
                            <span class="field-label">Filter By</span></td>
                       
                        <td align="left"   >
                            <asp:DropDownList ID="ddlType" runat="server" Width="199px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                                <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
      <tr runat="server">
          <td align="left"  colspan="3" >
              <asp:CheckBox ID="chkGradeType" runat="server" Text="A1+A2,B1+B2,....." CssClass="field-label"/></td>
      </tr>
        <tr>
            <td align="left" colspan="3" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" SkinID="ButtonNormal" 
                    Text="Generate Report"  CssClass="button" />
                <asp:Button id="btnDownload" runat="server" SkinID="ButtonNormal" 
                    Text="Download in PDF"  CssClass="button" />
                </td>
        </tr>
                </table>
                </div>
            </div>
         </div>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField  id="h_SBG_IDs" runat="server"/>
    <asp:HiddenField ID="h_HasSection" runat="server" />
    <asp:HiddenField ID="h_HideStream" runat="server" />
    <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
</asp:Content>

