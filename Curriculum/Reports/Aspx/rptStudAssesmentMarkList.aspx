<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudAssesmentMarkList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStudAssesmentMarkList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>

                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>

                        <td align="left" width="20%"><span class="field-label">Term</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                 
                    <tr>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span></td>

                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                         <td align="left" width="20%" valign="middle"><span class="field-label">Section</span></td>

                        <td align="left"  valign="middle" width="30%">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr runat="server">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Subject</span></td>

                        <td align="left"  valign="middle" width="30%">
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="ddlSubject" runat="server">
                            </asp:CheckBoxList></div>
                                </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

