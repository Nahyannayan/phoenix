Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_rptBtecLevelTracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                h_MnuCode.Value = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400170") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlPrevAcademicYear = studClass.PopulateAcademicYear(ddlPrevAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlPrevGrade = PopulateGrade(ddlPrevGrade, ddlPrevAcademicYear.SelectedValue.ToString)

                    ddlCurrAcademicYear = studClass.PopulateAcademicYear(ddlCurrAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlCurrGrade = PopulateGrade(ddlCurrGrade, ddlCurrAcademicYear.SelectedValue.ToString)

                    Dim grade As String()
                    grade = ddlPrevGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()

                    BindCurrSection()
                    BindCurrReports()

                    BindPrevHeader()
                    BindCurrHeader()

                    BindSubjects()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id  " _
                              & " and grm_grd_id in('09','10','11','12','13') " _
                              & " and grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function getReportCards() As String
        Dim str As String = ""
        Dim i As Integer


        str += ddlPrevReportSchedule.Items(i).Text + "|" + ddlCurrReportSchedule.SelectedItem.Text

        Return str
    End Function


    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlPrevGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlPrevAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrevReportSchedule.DataSource = ds
        ddlPrevReportSchedule.DataTextField = "RPF_DESCR"
        ddlPrevReportSchedule.DataValueField = "RPF_ID"
        ddlPrevReportSchedule.DataBind()
    End Sub

    Sub BindCurrReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlCurrGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlCurrAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurrReportSchedule.DataSource = ds
        ddlCurrReportSchedule.DataTextField = "RPF_DESCR"
        ddlCurrReportSchedule.DataValueField = "RPF_ID"
        ddlCurrReportSchedule.DataBind()
    End Sub


    Sub BindSection()
        'Dim li As New ListItem
        'li.Text = "ALL"
        'li.Value = "0"

        'ddlPrevSection.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String
        'If ddlPrevGrade.SelectedValue = "" Then
        '    ddlPrevSection.Items.Add(li)
        'Else
        '    str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
        '               & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
        '               & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlPrevAcademicYear.SelectedValue.ToString

        '    Dim grade As String() = ddlPrevGrade.SelectedValue.Split("|")
        '    str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        '    str_query += " ORDER BY SCT_DESCR"

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    ddlPrevSection.DataSource = ds
        '    ddlPrevSection.DataTextField = "SCT_DESCR"
        '    ddlPrevSection.DataValueField = "SCT_ID"
        '    ddlPrevSection.DataBind()
        'ddlPrevSection.Items.Insert(0, li)
        'End If
    End Sub


    Sub BindCurrSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlCurrSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlCurrGrade.SelectedValue = "" Then
            ddlCurrSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlCurrAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlCurrGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCurrSection.DataSource = ds
            ddlCurrSection.DataTextField = "SCT_DESCR"
            ddlCurrSection.DataValueField = "SCT_ID"
            ddlCurrSection.DataBind()
            ddlCurrSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindSubjects()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlCurrGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlCurrAcademicYear.SelectedValue.ToString _
                                & " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_DESCR LIKE '%BTEC%'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindPrevHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_ID,RSD_HEADER,RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D " _
                              & " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSD_RSM_ID=RPF_RSM_ID" _
                              & " WHERE RPF_ID=" + ddlPrevReportSchedule.SelectedValue.ToString _
                              & " AND ISNULL(RSD_bDIRECTENTRY,0)=1 AND RSD_CSSCLASS='TEXTBOXSMALL' ORDER BY RSD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPrevHeader.DataSource = DS
        ddlPrevHeader.DataTextField = "RSD_HEADER"
        ddlPrevHeader.DataValueField = "RSD_ID"
        ddlPrevHeader.DataBind()
    End Sub
    Sub BindCurrHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSD_ID,RSD_HEADER,RSD_DISPLAYORDER FROM RPT.REPORT_SETUP_D " _
                              & " INNER JOIN RPT.REPORT_PRINTEDFOR_M ON RSD_RSM_ID=RPF_RSM_ID" _
                              & " WHERE RPF_ID=" + ddlCurrReportSchedule.SelectedValue.ToString _
                              & " AND ISNULL(RSD_bDIRECTENTRY,0)=1 AND RSD_CSSCLASS='TEXTBOXSMALL'  ORDER BY RSD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurrHeader.DataSource = ds
        ddlCurrHeader.DataTextField = "RSD_HEADER"
        ddlCurrHeader.DataValueField = "RSD_ID"
        ddlCurrHeader.DataBind()
    End Sub


    Sub CallReport()
        Dim param As New Hashtable
        Dim currgrade As String() = ddlCurrGrade.SelectedValue.ToString.Split("|")
        Dim prevgrade As String() = ddlPrevGrade.SelectedValue.ToString.Split("|")
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlCurrAcademicYear.SelectedValue.ToString)
        param.Add("@ACD_ID_PREV", ddlPrevAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", currgrade(0))
        param.Add("@GRD_ID_PREV", prevgrade(0))
        param.Add("@RPF_ID", ddlCurrReportSchedule.SelectedValue.ToString)
        param.Add("@RPF_ID_PREV", ddlPrevReportSchedule.SelectedValue.ToString)
        param.Add("@RSD_ID", ddlCurrHeader.SelectedValue.ToString)
        param.Add("@RSD_ID_PREV", ddlPrevHeader.SelectedValue.ToString)
        param.Add("@SCT_ID", ddlCurrSection.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("prevreport", ddlPrevAcademicYear.SelectedItem.Text + " " + ddlPrevGrade.SelectedItem.Text + " " + ddlPrevReportSchedule.SelectedItem.Text)
        param.Add("currreport", ddlCurrAcademicYear.SelectedItem.Text + " " + ddlCurrGrade.SelectedItem.Text + " " + ddlCurrReportSchedule.SelectedItem.Text)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptBtecLevelTracking.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub


#End Region

    Protected Sub ddlPrevAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevAcademicYear.SelectedIndexChanged
        Try

            ddlPrevGrade = PopulateGrade(ddlPrevGrade, ddlPrevAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlPrevGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindPrevHeader()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlPrevGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlPrevGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
            BindPrevHeader()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub




    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            CallReport()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlCurrAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrAcademicYear.SelectedIndexChanged
        Try

            ddlCurrGrade = PopulateGrade(ddlCurrGrade, ddlCurrAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlCurrGrade.SelectedValue.Split("|")
            BindCurrSection()
            BindCurrReports()
            BindCurrHeader()
            BindSubjects()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlCurrGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlCurrGrade.SelectedValue.Split("|")
            BindCurrSection()
            BindCurrReports()
            BindSubjects()
            BindCurrHeader()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

 
    Protected Sub ddlCurrReportSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrReportSchedule.SelectedIndexChanged
        BindCurrHeader()
    End Sub

    Protected Sub ddlPrevReportSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevReportSchedule.SelectedIndexChanged
        BindPrevHeader()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
